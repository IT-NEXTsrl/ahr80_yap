* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsgp_kdm                                                        *
*              Duplicazione contratti                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-01-27                                                      *
* Last revis.: 2012-10-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsgp_kdm",oParentObject))

* --- Class definition
define class tgsgp_kdm as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 787
  Height = 526+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-24"
  HelpContextID=186019479
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=61

  * --- Constant Properties
  _IDX = 0
  CCOMMAST_IDX = 0
  GRUPPACQ_IDX = 0
  CONTI_IDX = 0
  AGENTI_IDX = 0
  cPrg = "gsgp_kdm"
  cComment = "Duplicazione contratti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_COFLGCIC = space(1)
  w_SECODMOD = space(20)
  w_COFLGTIP = space(1)
  o_COFLGTIP = space(1)
  w_COFLMODE = space(1)
  o_COFLMODE = space(1)
  w_COCHKCOD = space(15)
  w_COOLDCOD = space(15)
  o_COOLDCOD = space(15)
  w_COOLDDES = space(40)
  w_TSTCICLO = space(1)
  w_TSTCTYPE = space(1)
  w_COPRIORI = 0
  w_DACODGRU = space(15)
  w_DACODAGE = space(15)
  w_DACODINT = space(15)
  w_A_CODGRU = space(15)
  w_A_CODINT = space(15)
  w_A_CODAGE = space(15)
  w_CODATFIN = ctod('  /  /  ')
  w_COFLGSTA = space(1)
  w_CODATREG = ctod('  /  /  ')
  w_CODATINI = ctod('  /  /  ')
  w_APCODICE = space(15)
  o_APCODICE = space(15)
  w_COTIPINT = space(1)
  o_COTIPINT = space(1)
  w_DADESINT = space(40)
  w_A_DESINT = space(40)
  w_AGCODFOR = space(15)
  w_NWDATREG = ctod('  /  /  ')
  w_NWFLGSTA = space(1)
  w_NWDATINI = ctod('  /  /  ')
  w_NWDATFIN = ctod('  /  /  ')
  w_NWPRIORI = 0
  w_COD_PREF = space(6)
  w_COD_SERI = 0
  w_FLOVERWR = .F.
  w_OBTEST = ctod('  /  /  ')
  w_CONEWCOD = space(15)
  w_CONEWDES = space(40)
  w_COCODINT = space(15)
  w_COCODAGE = space(5)
  w_COCODGRU = space(15)
  w_ANTIPCON = space(1)
  w_ANCODINI = space(15)
  w_ANCODFIN = space(15)
  w_MCCODICE = space(15)
  w_NACODNAZ = space(3)
  w_CCCODICE = space(5)
  w_ZOCODZON = space(3)
  w_LUCODICE = space(3)
  w_CTCODICE = space(3)
  w_CSCODICE = space(5)
  w_GPCODICE = space(5)
  w_VACODVAL = space(3)
  w_AGCODAG1 = space(5)
  w_AGCODAG2 = space(5)
  w_PACODICE = space(5)
  w_BANUMCOR = space(15)
  w_ANDESINI = space(40)
  w_ANDESFIN = space(40)
  w_MCDESCRI = space(15)
  w_TSFLMODE = space(1)
  w_PUNTUALE = space(1)
  o_PUNTUALE = space(1)
  w_TIPSELE = space(1)

  * --- Children pointers
  GSGP_MA2 = .NULL.
  w_ZoomCli = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsgp_kdm
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    local l_CICLO
    l_CICLO=this.oParentObject
    return(lower('GSGP_KDM'+IIF(Type('l_CICLO')='C',','+l_CICLO,'')))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    *set procedure to GSGP_MA2 additive
    with this
      .Pages(1).addobject("oPag","tgsgp_kdmPag1","gsgp_kdm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni principali")
      .Pages(2).addobject("oPag","tgsgp_kdmPag2","gsgp_kdm",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCOFLGTIP_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    *release procedure GSGP_MA2
    * --- Area Manuale = Init Page Frame
    * --- gsgp_kdm
    with this.parent
       .cComment = ah_MsgFormat("Duplicazione contratti ")+IIF(oParentObject='V', ah_MsgFormat("(Vendite)"), ah_MsgFormat("(Acquisti)"))
    endwith
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomCli = this.oPgFrm.Pages(2).oPag.ZoomCli
    DoDefault()
    proc Destroy()
      this.w_ZoomCli = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CCOMMAST'
    this.cWorkTables[2]='GRUPPACQ'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='AGENTI'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return

  function CreateChildren()
    this.GSGP_MA2 = CREATEOBJECT('stdDynamicChild',this,'GSGP_MA2',this.oPgFrm.Page1.oPag.oLinkPC_1_73)
    this.GSGP_MA2.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSGP_MA2)
      this.GSGP_MA2.DestroyChildrenChain()
      this.GSGP_MA2=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_73')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSGP_MA2.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSGP_MA2.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSGP_MA2.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSGP_MA2.SetKey(;
            .w_APCODICE,"CA__GUID";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSGP_MA2.ChangeRow(this.cRowID+'      1',1;
             ,.w_APCODICE,"CA__GUID";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSGP_MA2)
        i_f=.GSGP_MA2.BuildFilter()
        if !(i_f==.GSGP_MA2.cQueryFilter)
          i_fnidx=.GSGP_MA2.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSGP_MA2.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSGP_MA2.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSGP_MA2.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSGP_MA2.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.Save_GSGP_MA2(.f.)
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSGP_BDC(this,"GENER")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    if  this.GSGP_MA2.IsAChildUpdated()
      return cp_YesNo(MSG_DISCARD_CHANGES_QP)
    endif
    return .t.
  endfunc
  proc Save_GSGP_MA2(i_ask)
    if this.GSGP_MA2.IsAChildUpdated() and (!i_ask or cp_YesNo(MSG_SAVE_CHANGES_QP+' (LinkPC)'))
      cp_BeginTrs()
      this.GSGP_MA2.mReplace(.t.)
      cp_EndTrs()
    endif
  endproc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_COFLGCIC=space(1)
      .w_SECODMOD=space(20)
      .w_COFLGTIP=space(1)
      .w_COFLMODE=space(1)
      .w_COCHKCOD=space(15)
      .w_COOLDCOD=space(15)
      .w_COOLDDES=space(40)
      .w_TSTCICLO=space(1)
      .w_TSTCTYPE=space(1)
      .w_COPRIORI=0
      .w_DACODGRU=space(15)
      .w_DACODAGE=space(15)
      .w_DACODINT=space(15)
      .w_A_CODGRU=space(15)
      .w_A_CODINT=space(15)
      .w_A_CODAGE=space(15)
      .w_CODATFIN=ctod("  /  /  ")
      .w_COFLGSTA=space(1)
      .w_CODATREG=ctod("  /  /  ")
      .w_CODATINI=ctod("  /  /  ")
      .w_APCODICE=space(15)
      .w_COTIPINT=space(1)
      .w_DADESINT=space(40)
      .w_A_DESINT=space(40)
      .w_AGCODFOR=space(15)
      .w_NWDATREG=ctod("  /  /  ")
      .w_NWFLGSTA=space(1)
      .w_NWDATINI=ctod("  /  /  ")
      .w_NWDATFIN=ctod("  /  /  ")
      .w_NWPRIORI=0
      .w_COD_PREF=space(6)
      .w_COD_SERI=0
      .w_FLOVERWR=.f.
      .w_OBTEST=ctod("  /  /  ")
      .w_CONEWCOD=space(15)
      .w_CONEWDES=space(40)
      .w_COCODINT=space(15)
      .w_COCODAGE=space(5)
      .w_COCODGRU=space(15)
      .w_ANTIPCON=space(1)
      .w_ANCODINI=space(15)
      .w_ANCODFIN=space(15)
      .w_MCCODICE=space(15)
      .w_NACODNAZ=space(3)
      .w_CCCODICE=space(5)
      .w_ZOCODZON=space(3)
      .w_LUCODICE=space(3)
      .w_CTCODICE=space(3)
      .w_CSCODICE=space(5)
      .w_GPCODICE=space(5)
      .w_VACODVAL=space(3)
      .w_AGCODAG1=space(5)
      .w_AGCODAG2=space(5)
      .w_PACODICE=space(5)
      .w_BANUMCOR=space(15)
      .w_ANDESINI=space(40)
      .w_ANDESFIN=space(40)
      .w_MCDESCRI=space(15)
      .w_TSFLMODE=space(1)
      .w_PUNTUALE=space(1)
      .w_TIPSELE=space(1)
        .w_COFLGCIC = oParentObject
        .w_SECODMOD = 'CONTI               '
        .w_COFLGTIP = 'O'
        .w_COFLMODE = 'N'
        .w_COCHKCOD = "ZZ_AU���[/#@'?^"
        .w_COOLDCOD = ''
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_COOLDCOD))
          .link_1_8('Full')
        endif
          .DoRTCalc(7,10,.f.)
        .w_DACODGRU = ''
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_DACODGRU))
          .link_1_13('Full')
        endif
        .w_DACODAGE = ''
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_DACODAGE))
          .link_1_14('Full')
        endif
        .w_DACODINT = ''
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_DACODINT))
          .link_1_15('Full')
        endif
        .w_A_CODGRU = ''
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_A_CODGRU))
          .link_1_16('Full')
        endif
        .w_A_CODINT = ''
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_A_CODINT))
          .link_1_17('Full')
        endif
        .w_A_CODAGE = ''
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_A_CODAGE))
          .link_1_18('Full')
        endif
          .DoRTCalc(17,20,.f.)
        .w_APCODICE = SYS(2015)
          .DoRTCalc(22,25,.f.)
        .w_NWDATREG = .w_CODATREG
        .w_NWFLGSTA = .w_COFLGSTA
        .w_NWDATINI = .w_CODATINI
        .w_NWDATFIN = .w_CODATFIN
        .w_NWPRIORI = .w_COPRIORI
          .DoRTCalc(31,32,.f.)
        .w_FLOVERWR = .F.
      .oPgFrm.Page2.oPag.ZoomCli.Calculate()
        .w_OBTEST = i_DATSYS
          .DoRTCalc(35,35,.f.)
        .w_CONEWDES = ''
          .DoRTCalc(37,39,.f.)
        .w_ANTIPCON = .w_COTIPINT
        .w_ANCODINI = .w_DACODINT
        .w_ANCODFIN = .w_A_CODINT
          .DoRTCalc(43,55,.f.)
        .w_ANDESINI = .w_DADESINT
        .w_ANDESFIN = .w_A_DESINT
          .DoRTCalc(58,59,.f.)
        .w_PUNTUALE = 'N'
        .w_TIPSELE = 'T'
      .GSGP_MA2.NewDocument()
      .GSGP_MA2.ChangeRow('1',1,.w_APCODICE,"CA__GUID")
      if not(.GSGP_MA2.bLoaded)
        .GSGP_MA2.SetKey(.w_APCODICE,"CA__GUID")
      endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_9.enabled = this.oPgFrm.Page2.oPag.oBtn_2_9.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_10.enabled = this.oPgFrm.Page2.oPag.oBtn_2_10.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_11.enabled = this.oPgFrm.Page2.oPag.oBtn_2_11.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_12.enabled = this.oPgFrm.Page2.oPag.oBtn_2_12.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_13.enabled = this.oPgFrm.Page2.oPag.oBtn_2_13.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_14.enabled = this.oPgFrm.Page2.oPag.oBtn_2_14.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    this.GSGP_MA2.SetStatus(i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSGP_MA2.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_COFLGTIP<>.w_COFLGTIP
            .w_COOLDCOD = ''
          .link_1_8('Full')
        endif
        .DoRTCalc(7,10,.t.)
        if .o_COTIPINT<>.w_COTIPINT
            .w_DACODGRU = ''
          .link_1_13('Full')
        endif
        if .o_COTIPINT<>.w_COTIPINT
            .w_DACODAGE = ''
          .link_1_14('Full')
        endif
        if .o_COTIPINT<>.w_COTIPINT
            .w_DACODINT = ''
          .link_1_15('Full')
        endif
        if .o_COTIPINT<>.w_COTIPINT
            .w_A_CODGRU = ''
          .link_1_16('Full')
        endif
        if .o_COTIPINT<>.w_COTIPINT
            .w_A_CODINT = ''
          .link_1_17('Full')
        endif
        if .o_COTIPINT<>.w_COTIPINT
            .w_A_CODAGE = ''
          .link_1_18('Full')
        endif
        .DoRTCalc(17,25,.t.)
        if .o_COFLGTIP<>.w_COFLGTIP.or. .o_COOLDCOD<>.w_COOLDCOD
            .w_NWDATREG = .w_CODATREG
        endif
        if .o_COFLGTIP<>.w_COFLGTIP.or. .o_COOLDCOD<>.w_COOLDCOD
            .w_NWFLGSTA = .w_COFLGSTA
        endif
        if .o_COFLGTIP<>.w_COFLGTIP.or. .o_COOLDCOD<>.w_COOLDCOD
            .w_NWDATINI = .w_CODATINI
        endif
        if .o_COFLGTIP<>.w_COFLGTIP.or. .o_COOLDCOD<>.w_COOLDCOD
            .w_NWDATFIN = .w_CODATFIN
        endif
        if .o_COFLGTIP<>.w_COFLGTIP.or. .o_COOLDCOD<>.w_COOLDCOD
            .w_NWPRIORI = .w_COPRIORI
        endif
        .oPgFrm.Page2.oPag.ZoomCli.Calculate()
        .DoRTCalc(31,39,.t.)
            .w_ANTIPCON = .w_COTIPINT
            .w_ANCODINI = .w_DACODINT
            .w_ANCODFIN = .w_A_CODINT
        .DoRTCalc(43,55,.t.)
            .w_ANDESINI = .w_DADESINT
            .w_ANDESFIN = .w_A_DESINT
        if .o_COFLMODE<>.w_COFLMODE
          .Calculate_UCEYTKIWSL()
        endif
        .DoRTCalc(58,60,.t.)
        if .o_PUNTUALE<>.w_PUNTUALE
            .w_TIPSELE = 'T'
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .w_APCODICE<>.o_APCODICE
          .Save_GSGP_MA2(.t.)
          .GSGP_MA2.NewDocument()
          .GSGP_MA2.ChangeRow('1',1,.w_APCODICE,"CA__GUID")
          if not(.GSGP_MA2.bLoaded)
            .GSGP_MA2.SetKey(.w_APCODICE,"CA__GUID")
          endif
        endif
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZoomCli.Calculate()
    endwith
  return

  proc Calculate_QEIPPQYKYU()
    with this
          * --- Reset dati al cambio contratto
          .w_COD_PREF = ""
          .w_COD_SERI = 0
          gsgp_bdc(this;
              ,'CHGCO';
             )
    endwith
  endproc
  proc Calculate_LQBGUGXAXR()
    with this
          * --- Verifica per esecuzione ricerca
          gsgp_bdc(this;
              ,"SRCAU";
             )
    endwith
  endproc
  proc Calculate_UCEYTKIWSL()
    with this
          * --- Verifiche contratto su attivazione flag modello
          .w_COOLDCOD = IIF(.w_COFLMODE<>'S' OR .w_TSFLMODE=.w_COFLMODE, .w_COOLDCOD, '' )
          .link_1_8('Full')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCOFLGTIP_1_4.enabled = this.oPgFrm.Page1.oPag.oCOFLGTIP_1_4.mCond()
    this.oPgFrm.Page1.oPag.oDACODGRU_1_13.enabled = this.oPgFrm.Page1.oPag.oDACODGRU_1_13.mCond()
    this.oPgFrm.Page1.oPag.oDACODAGE_1_14.enabled = this.oPgFrm.Page1.oPag.oDACODAGE_1_14.mCond()
    this.oPgFrm.Page1.oPag.oDACODINT_1_15.enabled = this.oPgFrm.Page1.oPag.oDACODINT_1_15.mCond()
    this.oPgFrm.Page1.oPag.oA_CODGRU_1_16.enabled = this.oPgFrm.Page1.oPag.oA_CODGRU_1_16.mCond()
    this.oPgFrm.Page1.oPag.oA_CODINT_1_17.enabled = this.oPgFrm.Page1.oPag.oA_CODINT_1_17.mCond()
    this.oPgFrm.Page1.oPag.oA_CODAGE_1_18.enabled = this.oPgFrm.Page1.oPag.oA_CODAGE_1_18.mCond()
    this.oPgFrm.Page2.oPag.oFLOVERWR_2_8.enabled = this.oPgFrm.Page2.oPag.oFLOVERWR_2_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_9.enabled = this.oPgFrm.Page2.oPag.oBtn_2_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDACODGRU_1_13.visible=!this.oPgFrm.Page1.oPag.oDACODGRU_1_13.mHide()
    this.oPgFrm.Page1.oPag.oDACODAGE_1_14.visible=!this.oPgFrm.Page1.oPag.oDACODAGE_1_14.mHide()
    this.oPgFrm.Page1.oPag.oDACODINT_1_15.visible=!this.oPgFrm.Page1.oPag.oDACODINT_1_15.mHide()
    this.oPgFrm.Page1.oPag.oA_CODGRU_1_16.visible=!this.oPgFrm.Page1.oPag.oA_CODGRU_1_16.mHide()
    this.oPgFrm.Page1.oPag.oA_CODINT_1_17.visible=!this.oPgFrm.Page1.oPag.oA_CODINT_1_17.mHide()
    this.oPgFrm.Page1.oPag.oA_CODAGE_1_18.visible=!this.oPgFrm.Page1.oPag.oA_CODAGE_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oDADESINT_1_38.visible=!this.oPgFrm.Page1.oPag.oDADESINT_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_39.visible=!this.oPgFrm.Page1.oPag.oStr_1_39.mHide()
    this.oPgFrm.Page1.oPag.oA_DESINT_1_40.visible=!this.oPgFrm.Page1.oPag.oA_DESINT_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_41.visible=!this.oPgFrm.Page1.oPag.oStr_1_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_43.visible=!this.oPgFrm.Page1.oPag.oStr_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oPUNTUALE_1_71.visible=!this.oPgFrm.Page1.oPag.oPUNTUALE_1_71.mHide()
    this.oPgFrm.Page1.oPag.oTIPSELE_1_72.visible=!this.oPgFrm.Page1.oPag.oTIPSELE_1_72.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_73.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_73.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.ZoomCli.Event(cEvent)
        if lower(cEvent)==lower("w_COOLDCOD Changed")
          .Calculate_QEIPPQYKYU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("ActivatePage 2")
          .Calculate_LQBGUGXAXR()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COOLDCOD
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CCOMMAST_IDX,3]
    i_lTable = "CCOMMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CCOMMAST_IDX,2], .t., this.CCOMMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CCOMMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COOLDCOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CCOMMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" COCODICE like "+cp_ToStrODBC(trim(this.w_COOLDCOD)+"%");

          i_ret=cp_SQL(i_nConn,"select COCODICE,CODESCRI,COFLGCIC,COFLGTIP,CODATREG,COPRIORI,CODATINI,CODATFIN,COFLGSTA,COTIPINT,COFLMODE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COCODICE',trim(this.w_COOLDCOD))
          select COCODICE,CODESCRI,COFLGCIC,COFLGTIP,CODATREG,COPRIORI,CODATINI,CODATFIN,COFLGSTA,COTIPINT,COFLMODE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COOLDCOD)==trim(_Link_.COCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COOLDCOD) and !this.bDontReportError
            deferred_cp_zoom('CCOMMAST','*','COCODICE',cp_AbsName(oSource.parent,'oCOOLDCOD_1_8'),i_cWhere,'',"Contratti",'GSGP_KDM.CCOMMAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODICE,CODESCRI,COFLGCIC,COFLGTIP,CODATREG,COPRIORI,CODATINI,CODATFIN,COFLGSTA,COTIPINT,COFLMODE";
                     +" from "+i_cTable+" "+i_lTable+" where COCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODICE',oSource.xKey(1))
            select COCODICE,CODESCRI,COFLGCIC,COFLGTIP,CODATREG,COPRIORI,CODATINI,CODATFIN,COFLGSTA,COTIPINT,COFLMODE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COOLDCOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODICE,CODESCRI,COFLGCIC,COFLGTIP,CODATREG,COPRIORI,CODATINI,CODATFIN,COFLGSTA,COTIPINT,COFLMODE";
                   +" from "+i_cTable+" "+i_lTable+" where COCODICE="+cp_ToStrODBC(this.w_COOLDCOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODICE',this.w_COOLDCOD)
            select COCODICE,CODESCRI,COFLGCIC,COFLGTIP,CODATREG,COPRIORI,CODATINI,CODATFIN,COFLGSTA,COTIPINT,COFLMODE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COOLDCOD = NVL(_Link_.COCODICE,space(15))
      this.w_COOLDDES = NVL(_Link_.CODESCRI,space(40))
      this.w_TSTCICLO = NVL(_Link_.COFLGCIC,space(1))
      this.w_TSTCTYPE = NVL(_Link_.COFLGTIP,space(1))
      this.w_CODATREG = NVL(cp_ToDate(_Link_.CODATREG),ctod("  /  /  "))
      this.w_COPRIORI = NVL(_Link_.COPRIORI,0)
      this.w_CODATINI = NVL(cp_ToDate(_Link_.CODATINI),ctod("  /  /  "))
      this.w_CODATFIN = NVL(cp_ToDate(_Link_.CODATFIN),ctod("  /  /  "))
      this.w_COFLGSTA = NVL(_Link_.COFLGSTA,space(1))
      this.w_COTIPINT = NVL(_Link_.COTIPINT,space(1))
      this.w_TSFLMODE = NVL(_Link_.COFLMODE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COOLDCOD = space(15)
      endif
      this.w_COOLDDES = space(40)
      this.w_TSTCICLO = space(1)
      this.w_TSTCTYPE = space(1)
      this.w_CODATREG = ctod("  /  /  ")
      this.w_COPRIORI = 0
      this.w_CODATINI = ctod("  /  /  ")
      this.w_CODATFIN = ctod("  /  /  ")
      this.w_COFLGSTA = space(1)
      this.w_COTIPINT = space(1)
      this.w_TSFLMODE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TSTCICLO=.w_COFLGCIC AND .w_TSTCTYPE=.w_COFLGTIP AND (.w_COFLMODE<>'S' OR .w_COFLMODE=.w_TSFLMODE)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Contratto inesistente o incongruente")
        endif
        this.w_COOLDCOD = space(15)
        this.w_COOLDDES = space(40)
        this.w_TSTCICLO = space(1)
        this.w_TSTCTYPE = space(1)
        this.w_CODATREG = ctod("  /  /  ")
        this.w_COPRIORI = 0
        this.w_CODATINI = ctod("  /  /  ")
        this.w_CODATFIN = ctod("  /  /  ")
        this.w_COFLGSTA = space(1)
        this.w_COTIPINT = space(1)
        this.w_TSFLMODE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CCOMMAST_IDX,2])+'\'+cp_ToStr(_Link_.COCODICE,1)
      cp_ShowWarn(i_cKey,this.CCOMMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COOLDCOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODGRU
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUPPACQ_IDX,3]
    i_lTable = "GRUPPACQ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2], .t., this.GRUPPACQ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUPPACQ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GACODICE like "+cp_ToStrODBC(trim(this.w_DACODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GACODICE,GADESGRP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GACODICE',trim(this.w_DACODGRU))
          select GACODICE,GADESGRP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODGRU)==trim(_Link_.GACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DACODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRUPPACQ','*','GACODICE',cp_AbsName(oSource.parent,'oDACODGRU_1_13'),i_cWhere,'',"GRUPPI DI ACQUISTO",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GACODICE,GADESGRP";
                     +" from "+i_cTable+" "+i_lTable+" where GACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GACODICE',oSource.xKey(1))
            select GACODICE,GADESGRP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GACODICE,GADESGRP";
                   +" from "+i_cTable+" "+i_lTable+" where GACODICE="+cp_ToStrODBC(this.w_DACODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GACODICE',this.w_DACODGRU)
            select GACODICE,GADESGRP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODGRU = NVL(_Link_.GACODICE,space(15))
      this.w_DADESINT = NVL(_Link_.GADESGRP,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DACODGRU = space(15)
      endif
      this.w_DADESINT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2])+'\'+cp_ToStr(_Link_.GACODICE,1)
      cp_ShowWarn(i_cKey,this.GRUPPACQ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODAGE
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_DACODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGCODFOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_DACODAGE))
          select AGCODAGE,AGDESAGE,AGCODFOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DACODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oDACODAGE_1_14'),i_cWhere,'',"AGENTI",'GSGP_AGE.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGCODFOR";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGCODFOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGCODFOR";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_DACODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_DACODAGE)
            select AGCODAGE,AGDESAGE,AGCODFOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODAGE = NVL(_Link_.AGCODAGE,space(15))
      this.w_DADESINT = NVL(_Link_.AGDESAGE,space(40))
      this.w_AGCODFOR = NVL(_Link_.AGCODFOR,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_DACODAGE = space(15)
      endif
      this.w_DADESINT = space(40)
      this.w_AGCODFOR = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!Empty(.w_AGCODFOR)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido o senza rif.fornitore")
        endif
        this.w_DACODAGE = space(15)
        this.w_DADESINT = space(40)
        this.w_AGCODFOR = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DACODINT
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DACODINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_DACODINT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPINT);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPINT;
                     ,'ANCODICE',trim(this.w_DACODINT))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DACODINT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DACODINT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oDACODINT_1_15'),i_cWhere,'GSAR_BZC',"ELENCO CLIENTI/FORNITORI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPINT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPINT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DACODINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_DACODINT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPINT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPINT;
                       ,'ANCODICE',this.w_DACODINT)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DACODINT = NVL(_Link_.ANCODICE,space(15))
      this.w_DADESINT = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DACODINT = space(15)
      endif
      this.w_DADESINT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DACODINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=A_CODGRU
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUPPACQ_IDX,3]
    i_lTable = "GRUPPACQ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2], .t., this.GRUPPACQ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_A_CODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUPPACQ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GACODICE like "+cp_ToStrODBC(trim(this.w_A_CODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GACODICE,GADESGRP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GACODICE',trim(this.w_A_CODGRU))
          select GACODICE,GADESGRP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_A_CODGRU)==trim(_Link_.GACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_A_CODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRUPPACQ','*','GACODICE',cp_AbsName(oSource.parent,'oA_CODGRU_1_16'),i_cWhere,'',"GRUPPI DI ACQUISTO",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GACODICE,GADESGRP";
                     +" from "+i_cTable+" "+i_lTable+" where GACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GACODICE',oSource.xKey(1))
            select GACODICE,GADESGRP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_A_CODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GACODICE,GADESGRP";
                   +" from "+i_cTable+" "+i_lTable+" where GACODICE="+cp_ToStrODBC(this.w_A_CODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GACODICE',this.w_A_CODGRU)
            select GACODICE,GADESGRP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_A_CODGRU = NVL(_Link_.GACODICE,space(15))
      this.w_A_DESINT = NVL(_Link_.GADESGRP,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_A_CODGRU = space(15)
      endif
      this.w_A_DESINT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2])+'\'+cp_ToStr(_Link_.GACODICE,1)
      cp_ShowWarn(i_cKey,this.GRUPPACQ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_A_CODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=A_CODINT
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_A_CODINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_A_CODINT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPINT);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPINT;
                     ,'ANCODICE',trim(this.w_A_CODINT))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_A_CODINT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_A_CODINT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oA_CODINT_1_17'),i_cWhere,'GSAR_BZC',"ELENCO CLIENTI/FORNITORI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPINT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPINT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_A_CODINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_A_CODINT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPINT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPINT;
                       ,'ANCODICE',this.w_A_CODINT)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_A_CODINT = NVL(_Link_.ANCODICE,space(15))
      this.w_A_DESINT = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_A_CODINT = space(15)
      endif
      this.w_A_DESINT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_A_CODINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=A_CODAGE
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_A_CODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_A_CODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGCODFOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_A_CODAGE))
          select AGCODAGE,AGDESAGE,AGCODFOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_A_CODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_A_CODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oA_CODAGE_1_18'),i_cWhere,'',"AGENTI",'GSGP_AGE.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGCODFOR";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGCODFOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_A_CODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGCODFOR";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_A_CODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_A_CODAGE)
            select AGCODAGE,AGDESAGE,AGCODFOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_A_CODAGE = NVL(_Link_.AGCODAGE,space(15))
      this.w_A_DESINT = NVL(_Link_.AGDESAGE,space(40))
      this.w_AGCODFOR = NVL(_Link_.AGCODFOR,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_A_CODAGE = space(15)
      endif
      this.w_A_DESINT = space(40)
      this.w_AGCODFOR = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!Empty(.w_AGCODFOR)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido o senza rif.fornitore")
        endif
        this.w_A_CODAGE = space(15)
        this.w_A_DESINT = space(40)
        this.w_AGCODFOR = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_A_CODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOFLGTIP_1_4.RadioValue()==this.w_COFLGTIP)
      this.oPgFrm.Page1.oPag.oCOFLGTIP_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOFLMODE_1_5.RadioValue()==this.w_COFLMODE)
      this.oPgFrm.Page1.oPag.oCOFLMODE_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOOLDCOD_1_8.value==this.w_COOLDCOD)
      this.oPgFrm.Page1.oPag.oCOOLDCOD_1_8.value=this.w_COOLDCOD
    endif
    if not(this.oPgFrm.Page1.oPag.oCOOLDDES_1_9.value==this.w_COOLDDES)
      this.oPgFrm.Page1.oPag.oCOOLDDES_1_9.value=this.w_COOLDDES
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPRIORI_1_12.value==this.w_COPRIORI)
      this.oPgFrm.Page1.oPag.oCOPRIORI_1_12.value=this.w_COPRIORI
    endif
    if not(this.oPgFrm.Page1.oPag.oDACODGRU_1_13.value==this.w_DACODGRU)
      this.oPgFrm.Page1.oPag.oDACODGRU_1_13.value=this.w_DACODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oDACODAGE_1_14.value==this.w_DACODAGE)
      this.oPgFrm.Page1.oPag.oDACODAGE_1_14.value=this.w_DACODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oDACODINT_1_15.value==this.w_DACODINT)
      this.oPgFrm.Page1.oPag.oDACODINT_1_15.value=this.w_DACODINT
    endif
    if not(this.oPgFrm.Page1.oPag.oA_CODGRU_1_16.value==this.w_A_CODGRU)
      this.oPgFrm.Page1.oPag.oA_CODGRU_1_16.value=this.w_A_CODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oA_CODINT_1_17.value==this.w_A_CODINT)
      this.oPgFrm.Page1.oPag.oA_CODINT_1_17.value=this.w_A_CODINT
    endif
    if not(this.oPgFrm.Page1.oPag.oA_CODAGE_1_18.value==this.w_A_CODAGE)
      this.oPgFrm.Page1.oPag.oA_CODAGE_1_18.value=this.w_A_CODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATFIN_1_22.value==this.w_CODATFIN)
      this.oPgFrm.Page1.oPag.oCODATFIN_1_22.value=this.w_CODATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCOFLGSTA_1_23.RadioValue()==this.w_COFLGSTA)
      this.oPgFrm.Page1.oPag.oCOFLGSTA_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATREG_1_24.value==this.w_CODATREG)
      this.oPgFrm.Page1.oPag.oCODATREG_1_24.value=this.w_CODATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATINI_1_25.value==this.w_CODATINI)
      this.oPgFrm.Page1.oPag.oCODATINI_1_25.value=this.w_CODATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDADESINT_1_38.value==this.w_DADESINT)
      this.oPgFrm.Page1.oPag.oDADESINT_1_38.value=this.w_DADESINT
    endif
    if not(this.oPgFrm.Page1.oPag.oA_DESINT_1_40.value==this.w_A_DESINT)
      this.oPgFrm.Page1.oPag.oA_DESINT_1_40.value=this.w_A_DESINT
    endif
    if not(this.oPgFrm.Page2.oPag.oNWDATREG_2_1.value==this.w_NWDATREG)
      this.oPgFrm.Page2.oPag.oNWDATREG_2_1.value=this.w_NWDATREG
    endif
    if not(this.oPgFrm.Page2.oPag.oNWFLGSTA_2_2.RadioValue()==this.w_NWFLGSTA)
      this.oPgFrm.Page2.oPag.oNWFLGSTA_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNWDATINI_2_3.value==this.w_NWDATINI)
      this.oPgFrm.Page2.oPag.oNWDATINI_2_3.value=this.w_NWDATINI
    endif
    if not(this.oPgFrm.Page2.oPag.oNWDATFIN_2_4.value==this.w_NWDATFIN)
      this.oPgFrm.Page2.oPag.oNWDATFIN_2_4.value=this.w_NWDATFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oNWPRIORI_2_5.value==this.w_NWPRIORI)
      this.oPgFrm.Page2.oPag.oNWPRIORI_2_5.value=this.w_NWPRIORI
    endif
    if not(this.oPgFrm.Page2.oPag.oCOD_PREF_2_6.value==this.w_COD_PREF)
      this.oPgFrm.Page2.oPag.oCOD_PREF_2_6.value=this.w_COD_PREF
    endif
    if not(this.oPgFrm.Page2.oPag.oCOD_SERI_2_7.value==this.w_COD_SERI)
      this.oPgFrm.Page2.oPag.oCOD_SERI_2_7.value=this.w_COD_SERI
    endif
    if not(this.oPgFrm.Page2.oPag.oFLOVERWR_2_8.RadioValue()==this.w_FLOVERWR)
      this.oPgFrm.Page2.oPag.oFLOVERWR_2_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPUNTUALE_1_71.value==this.w_PUNTUALE)
      this.oPgFrm.Page1.oPag.oPUNTUALE_1_71.value=this.w_PUNTUALE
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPSELE_1_72.value==this.w_TIPSELE)
      this.oPgFrm.Page1.oPag.oTIPSELE_1_72.value=this.w_TIPSELE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_COOLDCOD)) or not(.w_TSTCICLO=.w_COFLGCIC AND .w_TSTCTYPE=.w_COFLGTIP AND (.w_COFLMODE<>'S' OR .w_COFLMODE=.w_TSFLMODE)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOOLDCOD_1_8.SetFocus()
            i_bnoObbl = !empty(.w_COOLDCOD)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Contratto inesistente o incongruente")
          case   not(!Empty(.w_AGCODFOR))  and not(.w_COTIPINT <> 'A')  and (.w_COTIPINT = 'A')  and not(empty(.w_DACODAGE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDACODAGE_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non valido o senza rif.fornitore")
          case   not(!Empty(.w_AGCODFOR))  and not(.w_COTIPINT <> 'A')  and (.w_COTIPINT = 'A')  and not(empty(.w_A_CODAGE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oA_CODAGE_1_18.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non valido o senza rif.fornitore")
          case   (empty(.w_NWDATREG))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNWDATREG_2_1.SetFocus()
            i_bnoObbl = !empty(.w_NWDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NWDATINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNWDATINI_2_3.SetFocus()
            i_bnoObbl = !empty(.w_NWDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NWDATFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNWDATFIN_2_4.SetFocus()
            i_bnoObbl = !empty(.w_NWDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NWPRIORI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oNWPRIORI_2_5.SetFocus()
            i_bnoObbl = !empty(.w_NWPRIORI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_COD_SERI>=0)
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCOD_SERI_2_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il seriale deve essere maggiore o uguale a zero")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSGP_MA2.CheckForm()
      if i_bres
        i_bres=  .GSGP_MA2.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_COFLGTIP = this.w_COFLGTIP
    this.o_COFLMODE = this.w_COFLMODE
    this.o_COOLDCOD = this.w_COOLDCOD
    this.o_APCODICE = this.w_APCODICE
    this.o_COTIPINT = this.w_COTIPINT
    this.o_PUNTUALE = this.w_PUNTUALE
    * --- GSGP_MA2 : Depends On
    this.GSGP_MA2.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsgp_kdmPag1 as StdContainer
  Width  = 948
  height = 526
  stdWidth  = 948
  stdheight = 526
  resizeXpos=524
  resizeYpos=377
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCOFLGTIP_1_4 as StdCombo with uid="UAXBGNRICU",rtseq=3,rtrep=.f.,left=234,top=16,width=144,height=21;
    , ToolTipText = "Tipo contratto";
    , HelpContextID = 64430710;
    , cFormVar="w_COFLGTIP",RowSource=""+"Promozione Congiunta,"+"Quantit� limitata,"+"Obiettivo (PFA),"+"Assortimento,"+"Referenziamento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOFLGTIP_1_4.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'Q',;
    iif(this.value =3,'O',;
    iif(this.value =4,'A',;
    iif(this.value =5,'R',;
    space(1)))))))
  endfunc
  func oCOFLGTIP_1_4.GetRadio()
    this.Parent.oContained.w_COFLGTIP = this.RadioValue()
    return .t.
  endfunc

  func oCOFLGTIP_1_4.SetRadio()
    this.Parent.oContained.w_COFLGTIP=trim(this.Parent.oContained.w_COFLGTIP)
    this.value = ;
      iif(this.Parent.oContained.w_COFLGTIP=='P',1,;
      iif(this.Parent.oContained.w_COFLGTIP=='Q',2,;
      iif(this.Parent.oContained.w_COFLGTIP=='O',3,;
      iif(this.Parent.oContained.w_COFLGTIP=='A',4,;
      iif(this.Parent.oContained.w_COFLGTIP=='R',5,;
      0)))))
  endfunc

  func oCOFLGTIP_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.F.)
    endwith
   endif
  endfunc

  add object oCOFLMODE_1_5 as StdCheck with uid="HSIXEYAIOG",rtseq=4,rtrep=.f.,left=385, top=15, caption="Solo contratti modello",;
    ToolTipText = "Se attivo filtra solo i contratti con flag modello attivato",;
    HelpContextID = 13163925,;
    cFormVar="w_COFLMODE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOFLMODE_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCOFLMODE_1_5.GetRadio()
    this.Parent.oContained.w_COFLMODE = this.RadioValue()
    return .t.
  endfunc

  func oCOFLMODE_1_5.SetRadio()
    this.Parent.oContained.w_COFLMODE=trim(this.Parent.oContained.w_COFLMODE)
    this.value = ;
      iif(this.Parent.oContained.w_COFLMODE=='S',1,;
      0)
  endfunc

  add object oCOOLDCOD_1_8 as StdField with uid="PLWZVDYWJU",rtseq=6,rtrep=.f.,;
    cFormVar = "w_COOLDCOD", cQueryName = "COOLDCOD",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Contratto inesistente o incongruente",;
    ToolTipText = "Codice contratto da duplicare",;
    HelpContextID = 44544618,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=234, Top=43, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CCOMMAST", oKey_1_1="COCODICE", oKey_1_2="this.w_COOLDCOD"

  func oCOOLDCOD_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOOLDCOD_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOOLDCOD_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CCOMMAST','*','COCODICE',cp_AbsName(this.parent,'oCOOLDCOD_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Contratti",'GSGP_KDM.CCOMMAST_VZM',this.parent.oContained
  endproc

  add object oCOOLDDES_1_9 as StdField with uid="RESTIOTFGC",rtseq=7,rtrep=.f.,;
    cFormVar = "w_COOLDDES", cQueryName = "COOLDDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 207113607,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=383, Top=43, InputMask=replicate('X',40)

  add object oCOPRIORI_1_12 as StdField with uid="AZBTDPIWIQ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_COPRIORI", cQueryName = "COPRIORI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Priorit�",;
    HelpContextID = 16924049,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=642, Top=73

  add object oDACODGRU_1_13 as StdField with uid="WCFJXXVIOX",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DACODGRU", cQueryName = "DACODGRU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo d'acquisto d'inizio selezione",;
    HelpContextID = 156638069,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=234, Top=296, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="GRUPPACQ", oKey_1_1="GACODICE", oKey_1_2="this.w_DACODGRU"

  func oDACODGRU_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COTIPINT = 'G')
    endwith
   endif
  endfunc

  func oDACODGRU_1_13.mHide()
    with this.Parent.oContained
      return (.w_COTIPINT <> 'G')
    endwith
  endfunc

  func oDACODGRU_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACODGRU_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACODGRU_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUPPACQ','*','GACODICE',cp_AbsName(this.parent,'oDACODGRU_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"GRUPPI DI ACQUISTO",'',this.parent.oContained
  endproc

  add object oDACODAGE_1_14 as StdField with uid="OUOLKYMKTT",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DACODAGE", cQueryName = "DACODAGE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido o senza rif.fornitore",;
    ToolTipText = "Codice agente d'inizio selezione",;
    HelpContextID = 257301381,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=234, Top=296, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_DACODAGE"

  func oDACODAGE_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COTIPINT = 'A')
    endwith
   endif
  endfunc

  func oDACODAGE_1_14.mHide()
    with this.Parent.oContained
      return (.w_COTIPINT <> 'A')
    endwith
  endfunc

  func oDACODAGE_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACODAGE_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACODAGE_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oDACODAGE_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'',"AGENTI",'GSGP_AGE.AGENTI_VZM',this.parent.oContained
  endproc

  add object oDACODINT_1_15 as StdField with uid="PLRRBKMMOF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DACODINT", cQueryName = "DACODINT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice intestatario d'inizio selezione",;
    HelpContextID = 145351818,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=234, Top=296, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPINT", oKey_2_1="ANCODICE", oKey_2_2="this.w_DACODINT"

  func oDACODINT_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COTIPINT $ 'CF')
    endwith
   endif
  endfunc

  func oDACODINT_1_15.mHide()
    with this.Parent.oContained
      return (!.w_COTIPINT $ 'CF')
    endwith
  endfunc

  func oDACODINT_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oDACODINT_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDACODINT_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPINT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPINT)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oDACODINT_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"ELENCO CLIENTI/FORNITORI",'',this.parent.oContained
  endproc
  proc oDACODINT_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPINT
     i_obj.w_ANCODICE=this.parent.oContained.w_DACODINT
     i_obj.ecpSave()
  endproc

  add object oA_CODGRU_1_16 as StdField with uid="DIQZYTRUIW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_A_CODGRU", cQueryName = "A_CODGRU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice gruppo d'acquisto di fine selezione",;
    HelpContextID = 156630437,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=234, Top=323, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="GRUPPACQ", oKey_1_1="GACODICE", oKey_1_2="this.w_A_CODGRU"

  func oA_CODGRU_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COTIPINT = 'G')
    endwith
   endif
  endfunc

  func oA_CODGRU_1_16.mHide()
    with this.Parent.oContained
      return (.w_COTIPINT <> 'G')
    endwith
  endfunc

  func oA_CODGRU_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oA_CODGRU_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oA_CODGRU_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUPPACQ','*','GACODICE',cp_AbsName(this.parent,'oA_CODGRU_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"GRUPPI DI ACQUISTO",'',this.parent.oContained
  endproc

  add object oA_CODINT_1_17 as StdField with uid="RJYANLUPXR",rtseq=15,rtrep=.f.,;
    cFormVar = "w_A_CODINT", cQueryName = "A_CODINT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice intestatario di fine selezione",;
    HelpContextID = 145359450,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=234, Top=323, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPINT", oKey_2_1="ANCODICE", oKey_2_2="this.w_A_CODINT"

  func oA_CODINT_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COTIPINT $ 'CF')
    endwith
   endif
  endfunc

  func oA_CODINT_1_17.mHide()
    with this.Parent.oContained
      return (!.w_COTIPINT $ 'CF')
    endwith
  endfunc

  func oA_CODINT_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oA_CODINT_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oA_CODINT_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPINT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPINT)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oA_CODINT_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"ELENCO CLIENTI/FORNITORI",'',this.parent.oContained
  endproc
  proc oA_CODINT_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPINT
     i_obj.w_ANCODICE=this.parent.oContained.w_A_CODINT
     i_obj.ecpSave()
  endproc

  add object oA_CODAGE_1_18 as StdField with uid="WHDABTNWDL",rtseq=16,rtrep=.f.,;
    cFormVar = "w_A_CODAGE", cQueryName = "A_CODAGE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido o senza rif.fornitore",;
    ToolTipText = "Codice agente di fine selezione",;
    HelpContextID = 257293749,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=234, Top=323, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_A_CODAGE"

  func oA_CODAGE_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COTIPINT = 'A')
    endwith
   endif
  endfunc

  func oA_CODAGE_1_18.mHide()
    with this.Parent.oContained
      return (.w_COTIPINT <> 'A')
    endwith
  endfunc

  func oA_CODAGE_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oA_CODAGE_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oA_CODAGE_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oA_CODAGE_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"AGENTI",'GSGP_AGE.AGENTI_VZM',this.parent.oContained
  endproc


  add object oBtn_1_19 as StdButton with uid="OYZVKJEIXX",left=726, top=296, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la ricerca";
    , HelpContextID = 69978554;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        GSGP_BDC(this.Parent.oContained,"SEARC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_19.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_COOLDCOD))
      endwith
    endif
  endfunc


  add object oBtn_1_20 as StdButton with uid="BFZOYYNSIR",left=726, top=80, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aprire il contratto selezionato";
    , HelpContextID = 193397510;
    , caption='\<Apri';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        GSGP_BDC(this.Parent.oContained,"OPEN")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_COOLDCOD))
      endwith
    endif
  endfunc


  add object oBtn_1_21 as StdButton with uid="ELGVTCNZJZ",left=726, top=349, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 259905514;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCODATFIN_1_22 as StdField with uid="ZUTCSEJDCC",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODATFIN", cQueryName = "CODATFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di termine validit� del contratto",;
    HelpContextID = 110887540,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=423, Top=103


  add object oCOFLGSTA_1_23 as StdCombo with uid="MMIICPCQWR",rtseq=18,rtrep=.f.,left=423,top=75,width=98,height=21, enabled=.f.;
    , ToolTipText = "Status contratto";
    , HelpContextID = 220781977;
    , cFormVar="w_COFLGSTA",RowSource=""+"Da elaborare,"+"Sospeso,"+"Elaborato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOFLGSTA_1_23.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'S',;
    iif(this.value =3,'E',;
    space(1)))))
  endfunc
  func oCOFLGSTA_1_23.GetRadio()
    this.Parent.oContained.w_COFLGSTA = this.RadioValue()
    return .t.
  endfunc

  func oCOFLGSTA_1_23.SetRadio()
    this.Parent.oContained.w_COFLGSTA=trim(this.Parent.oContained.w_COFLGSTA)
    this.value = ;
      iif(this.Parent.oContained.w_COFLGSTA=='D',1,;
      iif(this.Parent.oContained.w_COFLGSTA=='S',2,;
      iif(this.Parent.oContained.w_COFLGSTA=='E',3,;
      0)))
  endfunc

  add object oCODATREG_1_24 as StdField with uid="CZKFWVWCMZ",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODATREG", cQueryName = "CODATREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di stipulazione del contratto",;
    HelpContextID = 224656787,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=234, Top=73

  add object oCODATINI_1_25 as StdField with uid="LDKMNCDZBF",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CODATINI", cQueryName = "CODATINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit� del contratto",;
    HelpContextID = 161219183,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=234, Top=103

  add object oDADESINT_1_38 as StdField with uid="EXUSEIEYJP",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DADESINT", cQueryName = "DADESINT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 160429194,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=383, Top=296, InputMask=replicate('X',40)

  func oDADESINT_1_38.mHide()
    with this.Parent.oContained
      return (!.w_COTIPINT $ 'ACFG')
    endwith
  endfunc

  add object oA_DESINT_1_40 as StdField with uid="XSTMUIAKRJ",rtseq=24,rtrep=.f.,;
    cFormVar = "w_A_DESINT", cQueryName = "A_DESINT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 160436826,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=383, Top=323, InputMask=replicate('X',40)

  func oA_DESINT_1_40.mHide()
    with this.Parent.oContained
      return (!.w_COTIPINT $ 'ACFG')
    endwith
  endfunc

  add object oPUNTUALE_1_71 as StdField with uid="JHXGTCWECN",rtseq=60,rtrep=.f.,;
    cFormVar = "w_PUNTUALE", cQueryName = "PUNTUALE",;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Se attivo: i dettagli dovranno avere set di attributi uguali al set di selezione",;
    HelpContextID = 29337915,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=806, Top=256, InputMask=replicate('X',1)

  func oPUNTUALE_1_71.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oTIPSELE_1_72 as StdField with uid="SIHPDBXAHG",rtseq=61,rtrep=.f.,;
    cFormVar = "w_TIPSELE", cQueryName = "TIPSELE",;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Condizioni di validit�",;
    HelpContextID = 71385802,;
   bGlobalFont=.t.,;
    Height=21, Width=124, Left=806, Top=308, InputMask=replicate('X',1)

  func oTIPSELE_1_72.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc


  add object oLinkPC_1_73 as stdDynamicChildContainer with uid="TBTQMNLCLW",left=806, top=359, width=65, height=43, bOnScreen=.t.;


  func oLinkPC_1_73.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (1=1)
     endwith
    endif
  endfunc

  add object oStr_1_3 as StdString with uid="LZNNKKECBN",Visible=.t., Left=151, Top=16,;
    Alignment=1, Width=77, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="ZVIWRYBPYI",Visible=.t., Left=12, Top=45,;
    Alignment=1, Width=216, Height=18,;
    Caption="Codice contratto da duplicare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="KUXEFNNMEL",Visible=.t., Left=12, Top=75,;
    Alignment=1, Width=216, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="NQBBHSFCMK",Visible=.t., Left=323, Top=75,;
    Alignment=1, Width=95, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="XOCAQANXPE",Visible=.t., Left=539, Top=75,;
    Alignment=1, Width=98, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="SHBINKPQGF",Visible=.t., Left=12, Top=105,;
    Alignment=1, Width=216, Height=18,;
    Caption="Validit� dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="JBREECPBJS",Visible=.t., Left=323, Top=105,;
    Alignment=1, Width=95, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="ADWWWTUWOI",Visible=.t., Left=13, Top=261,;
    Alignment=0, Width=465, Height=18,;
    Caption="Selezione intestatari"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="LTHHELREKC",Visible=.t., Left=12, Top=297,;
    Alignment=1, Width=216, Height=18,;
    Caption="Da gruppo di acquisto:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (.w_COTIPINT <> 'G')
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="EEKCQNHTIM",Visible=.t., Left=12, Top=297,;
    Alignment=1, Width=216, Height=18,;
    Caption="Da cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_COTIPINT <> 'C')
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="QEPYBAXTQF",Visible=.t., Left=12, Top=297,;
    Alignment=1, Width=216, Height=18,;
    Caption="Da fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (.w_COTIPINT <> 'F')
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="MMGOKAXCUA",Visible=.t., Left=12, Top=324,;
    Alignment=1, Width=216, Height=18,;
    Caption="A gruppo di acquisto:"  ;
  , bGlobalFont=.t.

  func oStr_1_39.mHide()
    with this.Parent.oContained
      return (.w_COTIPINT <> 'G')
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="BYPUENWTIE",Visible=.t., Left=12, Top=324,;
    Alignment=1, Width=216, Height=18,;
    Caption="A cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_41.mHide()
    with this.Parent.oContained
      return (.w_COTIPINT <> 'C')
    endwith
  endfunc

  add object oStr_1_42 as StdString with uid="DHDTJZJRST",Visible=.t., Left=12, Top=324,;
    Alignment=1, Width=216, Height=18,;
    Caption="A fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (.w_COTIPINT <> 'F')
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="PCWARUFVXT",Visible=.t., Left=12, Top=297,;
    Alignment=1, Width=216, Height=18,;
    Caption="Da agente:"  ;
  , bGlobalFont=.t.

  func oStr_1_43.mHide()
    with this.Parent.oContained
      return (.w_COTIPINT <> 'A')
    endwith
  endfunc

  add object oStr_1_44 as StdString with uid="LXLQDWMDFF",Visible=.t., Left=12, Top=324,;
    Alignment=1, Width=216, Height=18,;
    Caption="A agente:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (.w_COTIPINT <> 'A')
    endwith
  endfunc

  add object oBox_1_32 as StdBox with uid="MWKRFVDORY",left=13, top=278, width=748,height=2
enddefine
define class tgsgp_kdmPag2 as StdContainer
  Width  = 948
  height = 526
  stdWidth  = 948
  stdheight = 526
  resizeXpos=388
  resizeYpos=362
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNWDATREG_2_1 as StdField with uid="BBHYAMYDFN",rtseq=26,rtrep=.f.,;
    cFormVar = "w_NWDATREG", cQueryName = "NWDATREG",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di stipulazione del contratto",;
    HelpContextID = 224654563,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=489, Top=11


  add object oNWFLGSTA_2_2 as StdCombo with uid="ICCRUDLGJI",rtseq=27,rtrep=.f.,left=676,top=13,width=98,height=21;
    , ToolTipText = "Status contratto";
    , HelpContextID = 220779753;
    , cFormVar="w_NWFLGSTA",RowSource=""+"Da elaborare,"+"Sospeso", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oNWFLGSTA_2_2.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oNWFLGSTA_2_2.GetRadio()
    this.Parent.oContained.w_NWFLGSTA = this.RadioValue()
    return .t.
  endfunc

  func oNWFLGSTA_2_2.SetRadio()
    this.Parent.oContained.w_NWFLGSTA=trim(this.Parent.oContained.w_NWFLGSTA)
    this.value = ;
      iif(this.Parent.oContained.w_NWFLGSTA=='D',1,;
      iif(this.Parent.oContained.w_NWFLGSTA=='S',2,;
      0))
  endfunc

  add object oNWDATINI_2_3 as StdField with uid="GBMAZETGPN",rtseq=28,rtrep=.f.,;
    cFormVar = "w_NWDATINI", cQueryName = "NWDATINI",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit� del contratto",;
    HelpContextID = 161221407,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=489, Top=41

  add object oNWDATFIN_2_4 as StdField with uid="HZRAWBIYCD",rtseq=29,rtrep=.f.,;
    cFormVar = "w_NWDATFIN", cQueryName = "NWDATFIN",;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di termine validit� del contratto",;
    HelpContextID = 110889764,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=676, Top=41

  add object oNWPRIORI_2_5 as StdField with uid="MUWGQMHNVF",rtseq=30,rtrep=.f.,;
    cFormVar = "w_NWPRIORI", cQueryName = "NWPRIORI",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Priorit�",;
    HelpContextID = 16921825,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=489, Top=71

  add object oCOD_PREF_2_6 as StdField with uid="IKNZPVKIWR",rtseq=31,rtrep=.f.,;
    cFormVar = "w_COD_PREF", cQueryName = "COD_PREF",;
    bObbl = .f. , nPag = 2, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso di creazione nuovi codici contratto",;
    HelpContextID = 226885012,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=44, Top=65, InputMask=replicate('X',6)

  add object oCOD_SERI_2_7 as StdField with uid="ENVTINFFWG",rtseq=32,rtrep=.f.,;
    cFormVar = "w_COD_SERI", cQueryName = "COD_SERI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il seriale deve essere maggiore o uguale a zero",;
    ToolTipText = "Progressivo di creazione nuovi codici contratto",;
    HelpContextID = 173407633,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=173, Top=65, cSayPict='"999999999"', cGetPict='"999999999"'

  func oCOD_SERI_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_COD_SERI>=0)
    endwith
    return bRes
  endfunc

  add object oFLOVERWR_2_8 as StdCheck with uid="WTCMPQEFOU",rtseq=33,rtrep=.f.,left=44, top=90, caption="Sovrascrivi codici gi� valorizzati",;
    ToolTipText = "Se attivo durante la generazione automatica dei codici contratto i codici gi� presenti verranno sovrascritti",;
    HelpContextID = 29470632,;
    cFormVar="w_FLOVERWR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oFLOVERWR_2_8.RadioValue()
    return(iif(this.value =1,.T.,;
    .F.))
  endfunc
  func oFLOVERWR_2_8.GetRadio()
    this.Parent.oContained.w_FLOVERWR = this.RadioValue()
    return .t.
  endfunc

  func oFLOVERWR_2_8.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLOVERWR==.T.,1,;
      0)
  endfunc

  func oFLOVERWR_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!empty(.w_COD_PREF) AND !EMPTY(.w_COD_SERI))
    endwith
   endif
  endfunc


  add object oBtn_2_9 as StdButton with uid="QVGACCPVJW",left=281, top=42, width=48,height=45,;
    CpPicture="BMP\INS_RAP.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per valorizzare automaticamente i codici contratto per i clienti selezionati con il criterio di composizione impostato";
    , HelpContextID = 193397510;
    , caption='\<Car. Rap.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_9.Click()
      with this.Parent.oContained
        GSGP_BDC(this.Parent.oContained,"FASTL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_COD_PREF) AND !EMPTY(.w_COD_SERI))
      endwith
    endif
  endfunc


  add object oBtn_2_10 as StdButton with uid="FOVWBPHLAF",left=673, top=472, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per avviare la duplicazione dei contratti";
    , HelpContextID = 259905514;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_10.Click()
      with this.Parent.oContained
        GSGP_BDC(this.Parent.oContained,"GENER")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_11 as StdButton with uid="ZBCIXMVCJL",left=726, top=472, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 259905514;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_11.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_12 as StdButton with uid="GQMUPTTTCY",left=10, top=472, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per selezionare tutti gli intestatari";
    , HelpContextID = 259905514;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_12.Click()
      with this.Parent.oContained
        GSGP_BDC(this.Parent.oContained,"SEL_S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_13 as StdButton with uid="ZCYHLDNLVF",left=58, top=472, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per deselezionare tutti gli intestatari";
    , HelpContextID = 259905514;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_13.Click()
      with this.Parent.oContained
        GSGP_BDC(this.Parent.oContained,"SEL_D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_2_14 as StdButton with uid="GUQBLQMZBT",left=106, top=472, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per invertite la selezione degli intestatari";
    , HelpContextID = 259905514;
    , caption='\<Inv. Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_14.Click()
      with this.Parent.oContained
        GSGP_BDC(this.Parent.oContained,"SEL_I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomCli as cp_szoombox with uid="SOINNCHIBN",left=0, top=127, width=783,height=338,;
    caption='ZoomCli',;
   bGlobalFont=.t.,;
    cZoomFile="GSGP_KDM",bOptions=.f.,bAdvOptions=.t.,bReadOnly=.f.,bQueryOnLoad=.f.,cTable="CONTI",cZoomOnZoom="",cMenuFile="",bNoZoomGridShape=.f.,bQueryOnDblClick=.t.,;
    nPag=2;
    , HelpContextID = 196793238

  add object oStr_2_15 as StdString with uid="OQOIFBWTRH",Visible=.t., Left=339, Top=13,;
    Alignment=1, Width=144, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_2_16 as StdString with uid="CWOWOWLGOH",Visible=.t., Left=578, Top=13,;
    Alignment=1, Width=95, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_2_17 as StdString with uid="XUEUQDEFKJ",Visible=.t., Left=339, Top=73,;
    Alignment=1, Width=144, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="JWTNHFPTGN",Visible=.t., Left=339, Top=43,;
    Alignment=1, Width=144, Height=18,;
    Caption="Validit� dal:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="ZBDWNJOFFF",Visible=.t., Left=578, Top=43,;
    Alignment=1, Width=95, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="EARNCETBBV",Visible=.t., Left=22, Top=17,;
    Alignment=0, Width=294, Height=18,;
    Caption="Criterio composizione codice"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="RUDIKTPRYX",Visible=.t., Left=37, Top=45,;
    Alignment=0, Width=117, Height=18,;
    Caption="Prefisso"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="EDMWYIDUDO",Visible=.t., Left=167, Top=45,;
    Alignment=0, Width=107, Height=18,;
    Caption="Seriale"  ;
  , bGlobalFont=.t.

  add object oBox_2_21 as StdBox with uid="PTQFMPRQSG",left=10, top=10, width=326,height=108
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsgp_kdm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
