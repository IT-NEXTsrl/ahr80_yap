* #%&%#Build:  59
* Translated!
* --- file che viene sostituito dalle funzioni generate con il template
*     dei batch
*     deve esistere altrimenti la "set proc to cp_func" da errore
* --- START GET_GPFA_CODE
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: get_gpfa_code                                                   *
*              Calcola codice per GPFA                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-01                                                      *
* Last revis.: 2012-10-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func get_gpfa_code
param pPARAM

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_MACODICE
  m.w_MACODICE=space(20)
  private w_MACODATT
  m.w_MACODATT=space(10)
* --- WorkFile variables
  private PAR_GPFA_idx
  PAR_GPFA_idx=0
  private MODDATTR_idx
  MODDATTR_idx=0
  private MODMATTR_idx
  MODMATTR_idx=0
  private GRU_ATTR_idx
  GRU_ATTR_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 2
  private i_func_name
  i_func_name = "get_gpfa_code"
if vartype(__get_gpfa_code_hook__)='O'
  __get_gpfa_code_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'get_gpfa_code('+Transform(pPARAM)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'get_gpfa_code')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if get_gpfa_code_OpenTables()
  get_gpfa_code_Pag1()
endif
cp_CloseFuncTables()
if used('_Curs_PAR_GPFA')
  use in _Curs_PAR_GPFA
endif
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'get_gpfa_code('+Transform(pPARAM)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'get_gpfa_code')
Endif
*--- Activity log
if vartype(__get_gpfa_code_hook__)='O'
  __get_gpfa_code_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure get_gpfa_code_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Calcola il codice del gruppo famiglia da utilizzare nel modulo GPFA
  * --- pPARAM = "NEW" crea un nuovo record in MODMATTR e MODDATTR
  m.w_MACODICE = ""
  m.w_MACODATT = ""
  * --- Controlla se esiste un modello attributi utilizzato per il modulo GPFA
  * --- Select from PAR_GPFA
  i_nConn=i_TableProp[PAR_GPFA_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[PAR_GPFA_idx,2])
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select MACODICE from "+i_cTable+" PAR_GPFA ";
         ,"_Curs_PAR_GPFA")
  else
    select MACODICE from (i_cTable);
      into cursor _Curs_PAR_GPFA
  endif
  if used('_Curs_PAR_GPFA')
    select _Curs_PAR_GPFA
    locate for 1=1
    do while not(eof())
    m.w_MACODICE = _Curs_PAR_GPFA.MACODICE
      select _Curs_PAR_GPFA
      continue
    enddo
    use
  endif
  if EMPTY( m.w_MACODICE ) AND NOT EMPTY(m.pPARAM)
    * --- Non esiste un modello attributi per il modulo GPFA ed � stat richiesta la sua creazione
    if m.pPARAM="NEW"
      * --- pPARAM = "C" crea un nuovo record in MODMATTR, GRU_ATTR e MODDATTR
      i_Rows = 0
      do while i_Rows <> 1
        * --- Calcola una chiave per i modelli attributo
        m.w_MACODICE = SYS( 2015 )
        * --- Try
        get_gpfa_code_Try_02D0F210()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        * --- End
      enddo
      i_Rows = 0
      do while i_Rows <> 1
        * --- Calcola una chiave per il gruppo famiglie attributi articoli
        m.w_MACODATT = SYS( 2015 )
        * --- Try
        get_gpfa_code_Try_02D097E0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        * --- End
      enddo
      * --- Inserisce la riga di dettaglio del modello attributi indicando il gruppo famiglie inserito precedentemente
      * --- Insert into MODDATTR
      i_nConn=i_TableProp[MODDATTR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[MODDATTR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      func_SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,MODDATTR_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"MACODICE"+",CPROWNUM"+",CPROWORD"+",MACODATT"+",MAAUTOMA"+",MAGRUDEF"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(m.w_MACODICE),'MODDATTR','MACODICE');
        +","+cp_NullLink(cp_ToStrODBC(1),'MODDATTR','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(10),'MODDATTR','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(m.w_MACODATT),'MODDATTR','MACODATT');
        +","+cp_NullLink(cp_ToStrODBC("N"),'MODDATTR','MAAUTOMA');
        +","+cp_NullLink(cp_ToStrODBC("S"),'MODDATTR','MAGRUDEF');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'MACODICE',m.w_MACODICE,'CPROWNUM',1,'CPROWORD',10,'MACODATT',m.w_MACODATT,'MAAUTOMA',"N",'MAGRUDEF',"S")
        insert into (i_cTable) (MACODICE,CPROWNUM,CPROWORD,MACODATT,MAAUTOMA,MAGRUDEF &i_ccchkf. );
           values (;
             m.w_MACODICE;
             ,1;
             ,10;
             ,m.w_MACODATT;
             ,"N";
             ,"S";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Indica che il modello attributi creato � utilizzato per il modulo GPFA
      * --- Delete from PAR_GPFA
      i_nConn=i_TableProp[PAR_GPFA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[PAR_GPFA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"1 = "+cp_ToStrODBC(1);
               )
      else
        delete from (i_cTable) where;
              1 = 1;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Insert into PAR_GPFA
      i_nConn=i_TableProp[PAR_GPFA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[PAR_GPFA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      func_SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,PAR_GPFA_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"MACODICE"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(m.w_MACODICE),'PAR_GPFA','MACODICE');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'MACODICE',m.w_MACODICE)
        insert into (i_cTable) (MACODICE &i_ccchkf. );
           values (;
             m.w_MACODICE;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
  endif
  * --- Read from MODDATTR
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[MODDATTR_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[MODDATTR_idx,2])
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "MACODATT"+;
      " from "+i_cTable+" MODDATTR where ";
          +"MACODICE = "+cp_ToStrODBC(m.w_MACODICE);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      MACODATT;
      from (i_cTable) where;
          MACODICE = m.w_MACODICE;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_MACODATT = NVL(cp_ToDate(_read_.MACODATT),cp_NullValue(_read_.MACODATT))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  do case
    case m.pPARAM="NEW"
      i_retcode = 'stop'
      i_retval = m.w_MACODATT
      return
    case m.pPARAM="MODELLO"
      i_retcode = 'stop'
      i_retval = m.w_MACODICE
      return
    case m.pPARAM="GRUPPO"
      i_retcode = 'stop'
      i_retval = m.w_MACODATT
      return
  endcase
endproc
  proc get_gpfa_code_Try_02D0F210()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg
  * --- Inserisce la testata del modello attributi
  * --- Insert into MODMATTR
  i_nConn=i_TableProp[MODMATTR_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[MODMATTR_idx,2])
  i_commit = .f.
  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    cp_BeginTrs()
    i_commit = .t.
  endif
  i_ccchkf=''
  i_ccchkv=''
  func_SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,MODMATTR_idx,i_nConn)
  if i_nConn<>0
    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                " ("+"MACODICE"+",MADESCRI"+i_ccchkf+") values ("+;
    cp_NullLink(cp_ToStrODBC(m.w_MACODICE),'MODMATTR','MACODICE');
    +","+cp_NullLink(cp_ToStrODBC("Attributi per gestione premi"),'MODMATTR','MADESCRI');
         +i_ccchkv+")")
  else
    cp_CheckDeletedKey(i_cTable,0,'MACODICE',m.w_MACODICE,'MADESCRI',"Attributi per gestione premi")
    insert into (i_cTable) (MACODICE,MADESCRI &i_ccchkf. );
       values (;
         m.w_MACODICE;
         ,"Attributi per gestione premi";
         &i_ccchkv. )
    i_Rows=iif(bTrsErr,0,1)
  endif
  if i_commit
    cp_EndTrs(.t.)
  endif
  if i_Rows<0 or bTrsErr
    * --- Error: insert not accepted
    i_Error=MSG_INSERT_ERROR
    return
  endif
    return
  proc get_gpfa_code_Try_02D097E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg
  * --- Inserisce la testata del gruppo famiglie attributi articoli
  * --- Insert into GRU_ATTR
  i_nConn=i_TableProp[GRU_ATTR_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[GRU_ATTR_idx,2])
  i_commit = .f.
  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
    cp_BeginTrs()
    i_commit = .t.
  endif
  i_ccchkf=''
  i_ccchkv=''
  func_SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,GRU_ATTR_idx,i_nConn)
  if i_nConn<>0
    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                " ("+"GRCODICE"+",GRDESCRI"+",GRAUTOMA"+i_ccchkf+") values ("+;
    cp_NullLink(cp_ToStrODBC(m.w_MACODATT),'GRU_ATTR','GRCODICE');
    +","+cp_NullLink(cp_ToStrODBC("Gruppo per gestione premi"),'GRU_ATTR','GRDESCRI');
    +","+cp_NullLink(cp_ToStrODBC("N"),'GRU_ATTR','GRAUTOMA');
         +i_ccchkv+")")
  else
    cp_CheckDeletedKey(i_cTable,0,'GRCODICE',m.w_MACODATT,'GRDESCRI',"Gruppo per gestione premi",'GRAUTOMA',"N")
    insert into (i_cTable) (GRCODICE,GRDESCRI,GRAUTOMA &i_ccchkf. );
       values (;
         m.w_MACODATT;
         ,"Gruppo per gestione premi";
         ,"N";
         &i_ccchkv. )
    i_Rows=iif(bTrsErr,0,1)
  endif
  if i_commit
    cp_EndTrs(.t.)
  endif
  if i_Rows<0 or bTrsErr
    * --- Error: insert not accepted
    i_Error=MSG_INSERT_ERROR
    return
  endif
    return


  function get_gpfa_code_OpenTables()
    dimension i_cWorkTables[max(1,4)]
    i_cWorkTables[1]='PAR_GPFA'
    i_cWorkTables[2]='MODDATTR'
    i_cWorkTables[3]='MODMATTR'
    i_cWorkTables[4]='GRU_ATTR'
    return(cp_OpenFuncTables(4))
* --- END GET_GPFA_CODE
* --- START CHK_FAM_GRU
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: chk_fam_gru                                                     *
*              Controlla gruppo della famiglia                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-10-10                                                      *
* Last revis.: 2012-10-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func chk_fam_gru
param pFAM,pGRU

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_CHECKEDVALUE
  m.w_CHECKEDVALUE=space(10)
  private w_RETURNEDVALUE
  m.w_RETURNEDVALUE=.f.
* --- WorkFile variables
  private GRUDATTR_idx
  GRUDATTR_idx=0
  private i_nOpenedTables
  i_nOpenedTables=0
  private i_cWorkTables
  dimension i_cWorkTables[1]
  private i_nTrs
  i_nTrs=iif(type("nTrsConnCnt")="U",0,iif(nTrsConnCnt=0,0,1))
  private i_runtime_filters
  i_runtime_filters = 2
  private i_func_name
  i_func_name = "chk_fam_gru"
if vartype(__chk_fam_gru_hook__)='O'
  __chk_fam_gru_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'chk_fam_gru('+Transform(pFAM)+','+Transform(pGRU)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'chk_fam_gru')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
if chk_fam_gru_OpenTables()
  chk_fam_gru_Pag1()
endif
cp_CloseFuncTables()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'chk_fam_gru('+Transform(pFAM)+','+Transform(pGRU)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'chk_fam_gru')
Endif
*--- Activity log
if vartype(__chk_fam_gru_hook__)='O'
  __chk_fam_gru_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure chk_fam_gru_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  m.w_CHECKEDVALUE = ""
  m.w_RETURNEDVALUE = .F.
  * --- Read from GRUDATTR
  i_nOldArea=select()
  if used('_read_')
    select _read_
    use
  endif
  i_nConn=i_TableProp[GRUDATTR_idx,3]
  i_cTable=cp_SetAzi(i_TableProp[GRUDATTR_idx,2])
  if i_nConn<>0
    cp_sqlexec(i_nConn,"select "+;
      "GRCODICE"+;
      " from "+i_cTable+" GRUDATTR where ";
          +"GRCODICE = "+cp_ToStrODBC(m.pGRU);
          +" and GRFAMATT = "+cp_ToStrODBC(m.pFAM);
           ,"_read_")
    i_Rows=iif(used('_read_'),reccount(),0)
  else
    select;
      GRCODICE;
      from (i_cTable) where;
          GRCODICE = m.pGRU;
          and GRFAMATT = m.pFAM;
       into cursor _read_
    i_Rows=_tally
  endif
  if used('_read_')
    locate for 1=1
    m.w_CHECKEDVALUE = NVL(cp_ToDate(_read_.GRCODICE),cp_NullValue(_read_.GRCODICE))
    use
  else
    * --- Error: sql sentence error.
    i_Error = MSG_READ_ERROR
    return
  endif
  select (i_nOldArea)
  if NOT EMPTY( m.w_CHECKEDVALUE )
    m.w_RETURNEDVALUE = .T.
  endif
  i_retcode = 'stop'
  i_retval = m.w_RETURNEDVALUE
  return
endproc


  function chk_fam_gru_OpenTables()
    dimension i_cWorkTables[max(1,1)]
    i_cWorkTables[1]='GRUDATTR'
    return(cp_OpenFuncTables(1))
* --- END CHK_FAM_GRU
* --- START GET_DATE
* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: get_date                                                        *
*              Maschera es. conto scalare                                      *
*                                                                              *
*      Author: Nicola                                                          *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-06-15                                                      *
* Last revis.: 2012-10-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
func get_date
param pPERIODO,pTIPO,pANNO,pINIZIO

private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
* --- Local variables
  private w_MESE1
  m.w_MESE1=0
  private w_MESE2
  m.w_MESE2=0
  private w_TEMP
  m.w_TEMP=ctod("  /  /  ")
* --- WorkFile variables
  private i_runtime_filters
  i_runtime_filters = 1
  private i_func_name
  i_func_name = "get_date"
if vartype(__get_date_hook__)='O'
  __get_date_hook__.pre()
endif
* --- Call function
*--- Activity log Tracciatura dell'apertura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCO", 'get_date('+Transform(pPERIODO)+','+Transform(pTIPO)+','+Transform(pANNO)+','+Transform(pINIZIO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'get_date')
Endif
*--- Activity log
local i_oldarea
i_oldarea=select()
get_date_Pag1()
*--- Activity log Tracciatura della chiusura della funzione
If i_nACTIVATEPROFILER>0
  cp_ActivityLogger(Datetime(), Seconds(), Seconds(), "FCD", 'get_date('+Transform(pPERIODO)+','+Transform(pTIPO)+','+Transform(pANNO)+','+Transform(pINIZIO)+')', Iif(Type("i_curform")="C",i_curform,Space(10)), 0, 0, "", 'get_date')
Endif
*--- Activity log
if vartype(__get_date_hook__)='O'
  __get_date_hook__.post()
endif
select (i_oldarea)
return(i_retval)


procedure get_date_Pag1
  local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable
  * --- Calcola l'intervallo di date dato il periodo, il tipo periodo e l'anno
  * --- Un uteriore parametro calcola o la data iniziale o quella finale
  * --- A seconda del tipo del periodo
  do case
    case m.pTIPO="M"
      * --- Caso mensile
      if m.pPERIODO>12
        i_retcode = 'stop'
        i_retval = cp_CharToDate("//")
        return
      endif
      m.w_MESE1 = m.pPERIODO
      m.w_MESE2 = m.pPERIODO
    case m.pTIPO="B"
      * --- Caso bimestrale
      if m.pPERIODO>6
        i_retcode = 'stop'
        i_retval = cp_CharToDate("//")
        return
      endif
      m.w_MESE1 = iif(m.pPERIODO=0,1,(m.pPERIODO-1)*2+1)
      m.w_MESE2 = m.pPERIODO*2
    case m.pTIPO="T"
      * --- Caso bimestrale
      if m.pPERIODO>4
        i_retcode = 'stop'
        i_retval = cp_CharToDate("//")
        return
      endif
      m.w_MESE1 = iif(m.pPERIODO=0,1,(m.pPERIODO-1)*3+1)
      m.w_MESE2 = m.pPERIODO*3
    case m.pTIPO="S"
      * --- Caso bimestrale
      if m.pPERIODO>2
        i_retcode = 'stop'
        i_retval = cp_CharToDate("//")
        return
      endif
      m.w_MESE1 = iif(m.pPERIODO=0,1,(m.pPERIODO-1)*6+1)
      m.w_MESE2 = m.pPERIODO*6
    case m.pTIPO="A"
      * --- Periodo annuale
      m.w_MESE1 = 1
      m.w_MESE2 = 12
    case m.pTIPO="Q"
      * --- Caso Quadrimestrale
      if m.pPERIODO>3
        i_retcode = 'stop'
        i_retval = cp_CharToDate("//")
        return
      endif
      m.w_MESE1 = iif(m.pPERIODO=0,1,(m.pPERIODO-1)*4+1)
      m.w_MESE2 = m.pPERIODO*4
  endcase
  * --- Aggiungo i Giorni
  if m.pINIZIO=1
    i_retcode = 'stop'
    i_retval = cp_CharToDate("01/"+ALLTRIM(STR(m.w_MESE1,2,0))+"/"+m.pANNO)
    return
  else
    if m.w_MESE2=12
      i_retcode = 'stop'
      i_retval = cp_CharToDate("31/12/"+m.pANNO)
      return
    else
      m.w_MESE2 = m.w_MESE2+1
      m.w_TEMP = cp_CharToDate("01/"+ALLTRIM(STR(m.w_MESE2,2,0))+"/"+m.pANNO)
      i_retcode = 'stop'
      i_retval = m.w_TEMP-1
      return
    endif
  endif
endproc


* --- END GET_DATE
