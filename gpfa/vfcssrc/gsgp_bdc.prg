* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsgp_bdc                                                        *
*              Gestione duplicazione contratti                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-01-27                                                      *
* Last revis.: 2012-10-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsgp_bdc",oParentObject,m.pOPER)
return(i_retval)

define class tgsgp_bdc as StdBatch
  * --- Local variables
  pOPER = space(5)
  w_PADRE = .NULL.
  w_FLERRORS = .f.
  w_NFLTATTR = 0
  w_APPOGUID = space(15)
  w_CONTANUM = 0
  w_oERRORLOG = .NULL.
  w_OBJCONTR = .NULL.
  w_COPRMDOC = space(1)
  * --- WorkFile variables
  TMPMASTI_idx=0
  CCOMMAST_idx=0
  CCOMDETT_idx=0
  ASLISCON_idx=0
  PREMICON_idx=0
  ASCONCON_idx=0
  CCOMMATT_idx=0
  CCOMDATT_idx=0
  CCOMMATA_idx=0
  CCOMDECL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Duplicazione massiva
    this.w_PADRE = this.oParentObject
    this.w_FLERRORS = .F.
    do case
      case this.pOPER=="COPYC"
        if EMPTY(this.oParentObject.w_COOLDCOD)
          ah_ErrorMsg("Codice contratto da duplicare non indicato. Impossibile proseguire.")
          this.w_FLERRORS = .T.
        endif
        if !this.w_FLERRORS AND EMPTY(this.oParentObject.w_CONEWCOD)
          ah_ErrorMsg("Codice nuovo contratto non indicato. Impossibile proseguire.")
          this.w_FLERRORS = .T.
        endif
        if !this.w_FLERRORS
          * --- Read from CCOMMAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CCOMMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CCOMMAST_idx,2],.t.,this.CCOMMAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" CCOMMAST where ";
                  +"COCODICE = "+cp_ToStrODBC(this.oParentObject.w_CONEWCOD);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  COCODICE = this.oParentObject.w_CONEWCOD;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows>0
            ah_ErrorMsg("Codice nuovo contratto gi� presente. Impossibile proseguire.")
            this.w_FLERRORS = .T.
          endif
        endif
        if !this.w_FLERRORS
          * --- Try
          local bErr_0255B298
          bErr_0255B298=bTrsErr
          this.Try_0255B298()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            this.w_FLERRORS = .T.
            ah_ErrorMsg("Errore durante la duplicazione del contratto.%0%1", , , MESSAGE())
          endif
          bTrsErr=bTrsErr or bErr_0255B298
          * --- End
        endif
        if !this.w_FLERRORS
          this.w_PADRE.ecpQuit()     
        endif
      case this.pOPER=="SEARC" OR this.pOPER=="SRCAU"
        if this.pOPER=="SEARC" OR this.oParentObject.w_COCHKCOD<>this.oParentObject.w_COOLDCOD
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_PADRE.oPgfrm.ActivePage = 2
          this.oParentObject.w_COCHKCOD = this.oParentObject.w_COOLDCOD
        endif
      case this.pOPER=="GENER"
        if EMPTY(this.oParentObject.w_COOLDCOD)
          ah_ErrorMsg("Codice contratto da duplicare non indicato. Impossibile proseguire.")
          this.w_FLERRORS = .T.
        endif
        if !this.w_FLERRORS
          Select (this.oParentObject.w_ZoomCli.cCursor)
          GO TOP
          this.w_CONTANUM = -1
          COUNT FOR XCHK=1 TO this.w_CONTANUM
          if this.w_CONTANUM<=0
            ah_ErrorMsg("Nessun codice intestatario selezionato. Impossibile proseguire")
            this.w_FLERRORS = .T.
          endif
        endif
        if !this.w_FLERRORS
          * --- Creazione cursore messaggi di errore
          this.w_oERRORLOG=createobject("AH_ERRORLOG")
          Select (this.oParentObject.w_ZoomCli.cCursor)
          GO TOP
          SCAN FOR XCHK=1 AND EMPTY(NVL(ANCODCON, " "))
          this.w_oERRORLOG.AddMsgLog("Per l'intestatario %1 non � stato indicato il codice contratto da generare", ALLTRIM(ANCODICE))     
          this.w_FLERRORS = .T.
          ENDSCAN
          if this.w_FLERRORS
            this.w_oERRORLOG.PrintLog(this.oParentObject,"Duplicazione contratti")     
          endif
          this.w_oERRORLOG = .NULL.
        endif
        if !this.w_FLERRORS
          * --- Creazione cursore messaggi di errore
          this.w_oERRORLOG=createobject("AH_ERRORLOG")
          Select (this.oParentObject.w_ZoomCli.cCursor)
          GO TOP
          SCAN FOR XCHK=1 AND !EMPTY(NVL(ANCODCON, " "))
          * --- Read from CCOMMAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CCOMMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CCOMMAST_idx,2],.t.,this.CCOMMAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" CCOMMAST where ";
                  +"COCODICE = "+cp_ToStrODBC(ANCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  COCODICE = ANCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows>0
            this.w_oERRORLOG.AddMsgLog("Per l'intestatario %1 � stato indicato come codice nuovo contratto %2 che risulta gi� presente.", ALLTRIM(ANCODICE), ALLTRIM(ANCODCON))     
            this.w_FLERRORS = .T.
          endif
          Select (this.oParentObject.w_ZoomCli.cCursor)
          ENDSCAN
          if this.w_FLERRORS
            this.w_oERRORLOG.PrintLog(this.oParentObject,"Duplicazione contratti")     
          endif
          this.w_oERRORLOG = .NULL.
        endif
        if !this.w_FLERRORS
          * --- Try
          local bErr_035B2AE0
          bErr_035B2AE0=bTrsErr
          this.Try_035B2AE0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            this.w_FLERRORS = .T.
            ah_ErrorMsg("Errore durante la duplicazione dei contratti.%0%1", , , MESSAGE())
          endif
          bTrsErr=bTrsErr or bErr_035B2AE0
          * --- End
        endif
      case this.pOPER=="FASTL"
        Select (this.oParentObject.w_ZoomCli.cCursor)
        GO TOP
        SCAN FOR XCHK=1 AND ( this.oParentObject.w_FLOVERWR OR EMPTY(NVL(ANCODCON," ")) )
        REPLACE ANCODCON WITH ALLTRIM(this.oParentObject.w_COD_PREF)+ALLTRIM(STR(this.oParentObject.w_COD_SERI))
        this.oParentObject.w_COD_SERI = this.oParentObject.w_COD_SERI + 1
        Select (this.oParentObject.w_ZoomCli.cCursor)
        ENDSCAN
        * --- Refresh della griglia
        Select (this.oParentObject.w_ZoomCli.cCursor)
        GO TOP
        this.oParentObject.w_ZoomCli.Grd.Refresh()     
      case this.pOPER=="SEL_S" OR this.pOPER=="SEL_D" OR this.pOPER=="SEL_I"
        Select (this.oParentObject.w_ZoomCli.cCursor)
        this.w_NFLTATTR = RECNO()
        UPDATE (this.oParentObject.w_ZoomCli.cCursor) SET XCHK=ICASE(this.pOper=="SEL_S",1,this.pOper=="SEL_D",0,IIF(XCHK=1,0,1))
        Select (this.oParentObject.w_ZoomCli.cCursor)
        if this.w_NFLTATTR<RECCOUNT()
          GO this.w_NFLTATTR
        endif
      case this.pOPER=="OPEN"
        if this.oParentObject.w_COFLGTIP="O"
          this.w_OBJCONTR = GSGP_MCO(this.oParentObject.w_COFLGCIC)
        else
          * --- Solo contratti a obiettivo/PFA
        endif
        if this.w_OBJCONTR.bSec1
          this.w_OBJCONTR.ecpFilter()     
          this.w_OBJCONTR.w_COCODICE = this.oParentObject.w_COOLDCOD
          this.w_OBJCONTR.ecpSave()     
        else
          ah_ErrorMsg("Impossibile aprire la gestione dei contratti")
        endif
        this.w_OBJCONTR = .NULL.
      case this.pOPER=="CHGCO"
        * --- Elimino le righe degli attributi
        this.oParentObject.GSGP_MA2.FirstRow()     
        this.oParentObject.GSGP_MA2.SetRow()     
        do while Not this.oParentObject.GSGP_MA2.Eof_Trs() AND this.oParentObject.GSGP_MA2.FullRow()
          this.oParentObject.GSGP_MA2.DeleteRow()     
          if Not this.oParentObject.GSGP_MA2.Eof_Trs() AND this.oParentObject.GSGP_MA2.FullRow()
            this.oParentObject.GSGP_MA2.NextRow()     
            this.oParentObject.GSGP_MA2.SetRow()     
          endif
        enddo
        if !EMPTY(this.oParentObject.w_COOLDCOD) AND this.oParentObject.GSGP_MA2.Eof_Trs()
          this.oParentObject.GSGP_MA2.InitRow()     
        endif
        this.oParentObject.GSGP_MA2.Refresh()     
    endcase
    USE IN SELECT("_Conti_")
    USE IN SELECT("Flt_Attr")
    USE IN SELECT("Grp_Attr")
    USE IN SELECT("_Gruppo_")
    USE IN SELECT("_NAttrib_")
    USE IN SELECT("ContaJ")
    USE IN SELECT("__Tmp__")
    USE IN SELECT("_Selez_")
    USE IN SELECT("_SelezChk_")
  endproc
  proc Try_0255B298()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Duplicazione avvenuta correttamente", 64)
    return
  proc Try_035B2AE0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    Select (this.oParentObject.w_ZoomCli.cCursor)
    GO TOP
    SCAN FOR XCHK=1 AND !EMPTY(NVL(ANCODCON, " "))
    this.oParentObject.w_CONEWCOD = ALLTRIM(ANCODCON)
    do case
      case this.oParentObject.w_COTIPINT $ "CF"
        this.oParentObject.w_COCODINT = ALLTRIM(ANCODICE)
      case this.oParentObject.w_COTIPINT = "A"
        this.oParentObject.w_COCODAGE = ALLTRIM(ANCODICE)
      otherwise
        this.oParentObject.w_COCODGRU = ALLTRIM(ANCODICE)
    endcase
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    Select (this.oParentObject.w_ZoomCli.cCursor)
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    UPDATE (this.oParentObject.w_ZoomCli.cCursor) SET XCHK=0, ANCODCON=SPACE(15) WHERE XCHK=1
    Select (this.oParentObject.w_ZoomCli.cCursor)
    GO TOP
    ah_ErrorMsg("Duplicazione contratti avvenuta correttamente", 64)
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.oParentObject.w_COTIPINT $ "CF"
        * --- Eseguo la query per estrazione dei dati in base ai filtri dichiarati in prima pagina
        *     ad esclusione degli attributi che verranno analizzati in seguito
        vq_exec("..\gpfa\exe\query\GSGP1KDM",this,"_Conti_")
        * --- Memorizzo gli attributi impostati come filtro
        select t_CACODMOD,t_CACODGRU,t_CACODFAM,t_CAVALATT from (this.oParentObject.GSGP_MA2.cTrsName); 
 where Not Empty (t_CAVALATT) into Cursor "Flt_Attr"
        this.w_NFLTATTR = RECCOUNT("Flt_Attr")
        if this.w_NFLTATTR>0
          if this.oParentObject.w_TIPSELE = "T"
            * --- Conto i record (attributi) per ogni intestatario
            *     (serivir� pi� avanti per scartare intestatari non validi se attivo in maschera il flag Selezione Puntuale)
            Select GUID, count(*) as NUM from "_Conti_" where ASMODFAM is not null group by GUID into cursor "_NAttrib_"
          endif
          * --- La join consente gi� di scartare tutti intestatari che non hanno neppure un attributo in comune
          Select _Conti_.* from "_Conti_" inner join "Flt_Attr" on; 
 ASMODFAM=t_CACODMOD and ASCODATT=t_CACODGRU and ASCODFAM=t_CACODFAM and ASVALATT=t_CAVALATT; 
 Into Cursor "_Conti_"
          SELECT "_Conti_"
          =wrcursor("_Conti_")
          * --- 'AT = Almeno uno / Tutti validi
          if this.oParentObject.w_TIPSELE $ "AT"
            if this.oParentObject.w_TIPSELE = "T"
              * --- Dal risultato della join conto per ogni dettaglio i record (attributri) rimasti.
              *     Se NON coincidono col totale di attributi di selezione l'intestatario NON � valido,
              *     � presente cio� almeno un attributo di selezione non soddisfatto.
              *     Se coincidono occorre controllare il flag PUNTUALE nella maschera:
              *     se ATTIVO confronto il totale dei record (attributi) di CONTAJ (risulatto della join)
              *     con il totale dei record di _NAttrib_ (totale di attributi per Intestatario), se coincide il dettaglio viene ACCETTATO
              *     se NON coincide il dettaglio viene SCARTATO.
              *     Se il flag puntuale � disattivo il dettaglio � ACCETTATO
              Select GUID, count(*) as NUM from "_Conti_" where ASMODFAM is not null group by GUID into cursor "ContaJ"
              Select "ContaJ"
              GO TOP
              SCAN
              this.w_APPOGUID = ContaJ.GUID
              if this.w_NFLTATTR>ContaJ.Num
                * --- Se sono presenti piu attributi di selezione rspetto a quelli associati all'intestatario scarto il dettaglio in questione
                DELETE FROM "_Conti_" WHERE GUID=this.w_APPOGUID
                Select "_Conti_"
                GO TOP
              else
                if this.oParentObject.w_PUNTUALE="S"
                  Select "_NAttrib_"
                  Locate for GUID=this.w_APPOGUID
                  this.w_CONTANUM = _NAttrib_.NUM 
                  Select "ContaJ"
                  if this.w_CONTANUM > ContaJ.NUM
                    * --- Se sono presenti piu attributi di selezione rspetto a quelli associati all'intestatario scarto il dettaglio in questione
                    DELETE FROM "_Conti_" WHERE GUID=this.w_APPOGUID
                    Select "_Conti_"
                    GO TOP
                  endif
                endif
              endif
              Select "ContaJ"
              ENDSCAN
            else
              * --- La join precedente basta per selezionare 'almeno uno'
            endif
          else
            * --- Almeno uno per gruppo o per famiglia
            if this.oParentObject.w_TIPSELE="G"
              Select distinct ANTIPCON, ANCODICE, GUID, ASMODFAM, ASCODATT From "_Conti_" into cursor "_Gruppo_"
              Select Count(*) as CONTA, ANTIPCON, ANCODICE, GUID from "_Gruppo_" Group By GUID Into Cursor "_Gruppo_"
              * --- Count per GRUPPO
              select t_CACODMOD,t_CACODGRU from (this.oParentObject.GSGP_MA2.cTrsName); 
 where Not Empty (t_CAVALATT) Group by t_CACODMOD,t_CACODGRU into Cursor "Grp_Attr"
            else
              Select distinct ANTIPCON, ANCODICE, GUID, ASMODFAM, ASCODATT, ASCODFAM From "_Conti_" into cursor "_Gruppo_"
              Select Count(*) as CONTA, ANTIPCON, ANCODICE, GUID from "_Gruppo_" Group By GUID Into Cursor "_Gruppo_"
              * --- Count per GRUPPO e FAMIGLIA
              select t_CACODMOD,t_CACODGRU, t_CACODFAM from (this.oParentObject.GSGP_MA2.cTrsName); 
 where Not Empty (t_CAVALATT) Group by t_CACODMOD,t_CACODGRU,t_CACODFAM into Cursor "Grp_Attr"
            endif
            this.w_NFLTATTR = RECCOUNT("Grp_Attr")
            if this.w_NFLTATTR > 0
              * --- Sono eliminati tutti i record che non soddisfano
              Delete from _Conti_ where ANTIPCON+ANCODICE+GUID not in; 
 (Select ANTIPCON+ANCODICE+GUID from _Gruppo_ where CONTA= this.w_NFLTATTR)
            endif
          endif
        endif
        Select "_Conti_"
        GO TOP
      case this.oParentObject.w_COTIPINT = "A"
        * --- Eseguo la query per estrazione dei dati in base ai filtri dichiarati in prima pagina
        *     ad esclusione degli attributi che non possono essere impostati per gli agenti
        vq_exec("..\gpfa\exe\query\GSGP2KDM",this,"_Conti_")
      otherwise
        * --- Eseguo la query per estrazione dei dati in base ai filtri dichiarati in prima pagina
        *     ad esclusione degli attributi che non possono essere impostati per i gruppi d'acquisto
        vq_exec("..\gpfa\exe\query\GSGP3KDM",this,"_Conti_")
    endcase
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Svuoto lo zoom
    Select (this.oParentObject.w_ZoomCli.cCursor)
    ZAP
    SELECT ANTIPCON, ANCODICE, MAX(ANDESCRI) AS ANDESCRI, SPACE(15) AS ANCODCON, 0 AS XCHK FROM "_Conti_" WHERE NOT DELETED() GROUP BY ANTIPCON, ANCODICE INTO CURSOR "__Tmp__"
    Select "__Tmp__"
    SCAN
    Scatter Memvar
    Select (this.oParentObject.w_ZoomCli.cCursor)
    Append blank
    Gather memvar
    ENDSCAN
    USE IN SELECT("__Tmp__")
    * --- Refresh della griglia
    Select (this.oParentObject.w_ZoomCli.cCursor)
    GO TOP
    this.oParentObject.w_ZoomCli.Grd.Refresh()     
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Create temporary table TMPMASTI
    i_nIdx=cp_AddTableDef('TMPMASTI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.CCOMMAST_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.CCOMMAST_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          +" where COCODICE="+cp_ToStrODBC(this.oParentObject.w_COOLDCOD)+"";
          )
    this.TMPMASTI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Aggiornamento chiave primaria e altri dati di testata obbligatori
    * --- Write into TMPMASTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPMASTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMASTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"COCODICE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CONEWCOD),'TMPMASTI','COCODICE');
      +",COFLGSTA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NWFLGSTA),'TMPMASTI','COFLGSTA');
      +",CODATREG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NWDATREG),'TMPMASTI','CODATREG');
      +",CODATINI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NWDATINI),'TMPMASTI','CODATINI');
      +",CODATFIN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NWDATFIN),'TMPMASTI','CODATFIN');
      +",COPRIORI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NWPRIORI),'TMPMASTI','COPRIORI');
      +",COVALFAT ="+cp_NullLink(cp_ToStrODBC(0),'TMPMASTI','COVALFAT');
      +",COSCOCOM ="+cp_NullLink(cp_ToStrODBC(0),'TMPMASTI','COSCOCOM');
      +",COVALPRE ="+cp_NullLink(cp_ToStrODBC(0),'TMPMASTI','COVALPRE');
      +",COTOTPRE ="+cp_NullLink(cp_ToStrODBC(0),'TMPMASTI','COTOTPRE');
      +",CODATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'TMPMASTI','CODATGEN');
      +",COFLMODE ="+cp_NullLink(cp_ToStrODBC("N"),'TMPMASTI','COFLMODE');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          COCODICE = this.oParentObject.w_CONEWCOD;
          ,COFLGSTA = this.oParentObject.w_NWFLGSTA;
          ,CODATREG = this.oParentObject.w_NWDATREG;
          ,CODATINI = this.oParentObject.w_NWDATINI;
          ,CODATFIN = this.oParentObject.w_NWDATFIN;
          ,COPRIORI = this.oParentObject.w_NWPRIORI;
          ,COVALFAT = 0;
          ,COSCOCOM = 0;
          ,COVALPRE = 0;
          ,COTOTPRE = 0;
          ,CODATGEN = cp_CharToDate("  -  -    ");
          ,COFLMODE = "N";
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    this.w_COPRMDOC = IIF(this.oParentObject.w_COTIPINT="A","A",IIF(this.oParentObject.w_COFLGCIC="V" ,"P","F"))
    if !EMPTY(this.oParentObject.w_CONEWDES)
      * --- Write into TMPMASTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPMASTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMASTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CODESCRI ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CONEWDES),'TMPMASTI','CODESCRI');
            +i_ccchkf ;
        +" where ";
            +"1 = "+cp_ToStrODBC(1);
               )
      else
        update (i_cTable) set;
            CODESCRI = this.oParentObject.w_CONEWDES;
            &i_ccchkf. ;
         where;
            1 = 1;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if !EMPTY(this.oParentObject.w_COCODINT) AND this.oParentObject.w_COTIPINT $ "CF" 
      * --- Write into TMPMASTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPMASTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMASTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"COTIPINT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COTIPINT),'TMPMASTI','COTIPINT');
        +",COCODINT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COCODINT),'TMPMASTI','COCODINT');
        +",COCODAGE ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'TMPMASTI','COCODAGE');
        +",COCODGRU ="+cp_NullLink(cp_ToStrODBC(SPACE(15)),'TMPMASTI','COCODGRU');
        +",COPRMDOC ="+cp_NullLink(cp_ToStrODBC(this.w_COPRMDOC),'TMPMASTI','COPRMDOC');
            +i_ccchkf ;
        +" where ";
            +"1 = "+cp_ToStrODBC(1);
               )
      else
        update (i_cTable) set;
            COTIPINT = this.oParentObject.w_COTIPINT;
            ,COCODINT = this.oParentObject.w_COCODINT;
            ,COCODAGE = SPACE(5);
            ,COCODGRU = SPACE(15);
            ,COPRMDOC = this.w_COPRMDOC;
            &i_ccchkf. ;
         where;
            1 = 1;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if !EMPTY(this.oParentObject.w_COCODAGE) AND this.oParentObject.w_COTIPINT="A"
      * --- Write into TMPMASTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPMASTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMASTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"COTIPINT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COTIPINT),'TMPMASTI','COTIPINT');
        +",COCODAGE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COCODAGE),'TMPMASTI','COCODAGE');
        +",COCODINT ="+cp_NullLink(cp_ToStrODBC(SPACE(15)),'TMPMASTI','COCODINT');
        +",COCODGRU ="+cp_NullLink(cp_ToStrODBC(SPACE(15)),'TMPMASTI','COCODGRU');
        +",COPRMDOC ="+cp_NullLink(cp_ToStrODBC(this.w_COPRMDOC),'TMPMASTI','COPRMDOC');
            +i_ccchkf ;
        +" where ";
            +"1 = "+cp_ToStrODBC(1);
               )
      else
        update (i_cTable) set;
            COTIPINT = this.oParentObject.w_COTIPINT;
            ,COCODAGE = this.oParentObject.w_COCODAGE;
            ,COCODINT = SPACE(15);
            ,COCODGRU = SPACE(15);
            ,COPRMDOC = this.w_COPRMDOC;
            &i_ccchkf. ;
         where;
            1 = 1;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if !EMPTY(this.oParentObject.w_COCODGRU) AND this.oParentObject.w_COTIPINT="G"
      * --- Write into TMPMASTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPMASTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMASTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"COTIPINT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COTIPINT),'TMPMASTI','COTIPINT');
        +",COCODGRU ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COCODGRU),'TMPMASTI','COCODGRU');
        +",COCODINT ="+cp_NullLink(cp_ToStrODBC(SPACE(15)),'TMPMASTI','COCODINT');
        +",COCODAGE ="+cp_NullLink(cp_ToStrODBC(SPACE(5)),'TMPMASTI','COCODAGE');
        +",COPRMDOC ="+cp_NullLink(cp_ToStrODBC(this.w_COPRMDOC),'TMPMASTI','COPRMDOC');
            +i_ccchkf ;
        +" where ";
            +"1 = "+cp_ToStrODBC(1);
               )
      else
        update (i_cTable) set;
            COTIPINT = this.oParentObject.w_COTIPINT;
            ,COCODGRU = this.oParentObject.w_COCODGRU;
            ,COCODINT = SPACE(15);
            ,COCODAGE = SPACE(5);
            ,COPRMDOC = this.w_COPRMDOC;
            &i_ccchkf. ;
         where;
            1 = 1;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Insert into CCOMMAST
    i_nConn=i_TableProp[this.CCOMMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCOMMAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where 1=1",this.CCOMMAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMPMASTI
    i_nIdx=cp_GetTableDefIdx('TMPMASTI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMASTI')
    endif
    * --- Create temporary table TMPMASTI
    i_nIdx=cp_AddTableDef('TMPMASTI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.CCOMDETT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.CCOMDETT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          +" where COCODICE="+cp_ToStrODBC(this.oParentObject.w_COOLDCOD)+"";
          )
    this.TMPMASTI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Aggiornamento chiave primaria
    * --- Write into TMPMASTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPMASTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMASTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"COCODICE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CONEWCOD),'TMPMASTI','COCODICE');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          COCODICE = this.oParentObject.w_CONEWCOD;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Insert into CCOMDETT
    i_nConn=i_TableProp[this.CCOMDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCOMDETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where 1=1",this.CCOMDETT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMPMASTI
    i_nIdx=cp_GetTableDefIdx('TMPMASTI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMASTI')
    endif
    * --- Create temporary table TMPMASTI
    i_nIdx=cp_AddTableDef('TMPMASTI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.ASLISCON_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.ASLISCON_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          +" where CLCODICE="+cp_ToStrODBC(this.oParentObject.w_COOLDCOD)+"";
          )
    this.TMPMASTI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Aggiornamento chiave primaria
    * --- Write into TMPMASTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPMASTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMASTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CLCODICE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CONEWCOD),'TMPMASTI','CLCODICE');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          CLCODICE = this.oParentObject.w_CONEWCOD;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Insert into ASLISCON
    i_nConn=i_TableProp[this.ASLISCON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ASLISCON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where 1=1",this.ASLISCON_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMPMASTI
    i_nIdx=cp_GetTableDefIdx('TMPMASTI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMASTI')
    endif
    * --- Create temporary table TMPMASTI
    i_nIdx=cp_AddTableDef('TMPMASTI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.PREMICON_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          +" where CCCODICE="+cp_ToStrODBC(this.oParentObject.w_COOLDCOD)+"";
          )
    this.TMPMASTI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Aggiornamento chiave primaria
    * --- Write into TMPMASTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPMASTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMASTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CCCODICE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CONEWCOD),'TMPMASTI','CCCODICE');
      +",PCQTACON ="+cp_NullLink(cp_ToStrODBC(0),'TMPMASTI','PCQTACON');
      +",PCVALCON ="+cp_NullLink(cp_ToStrODBC(0),'TMPMASTI','PCVALCON');
      +",PCVALPRE ="+cp_NullLink(cp_ToStrODBC(0),'TMPMASTI','PCVALPRE');
      +",PCQTAPRE ="+cp_NullLink(cp_ToStrODBC(0),'TMPMASTI','PCQTAPRE');
      +",PCPUNPRE ="+cp_NullLink(cp_ToStrODBC(0),'TMPMASTI','PCPUNPRE');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          CCCODICE = this.oParentObject.w_CONEWCOD;
          ,PCQTACON = 0;
          ,PCVALCON = 0;
          ,PCVALPRE = 0;
          ,PCQTAPRE = 0;
          ,PCPUNPRE = 0;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Insert into PREMICON
    i_nConn=i_TableProp[this.PREMICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where 1=1",this.PREMICON_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMPMASTI
    i_nIdx=cp_GetTableDefIdx('TMPMASTI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMASTI')
    endif
    * --- Create temporary table TMPMASTI
    i_nIdx=cp_AddTableDef('TMPMASTI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.ASCONCON_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.ASCONCON_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          +" where CCCODICE="+cp_ToStrODBC(this.oParentObject.w_COOLDCOD)+"";
          )
    this.TMPMASTI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Aggiornamento chiave primaria
    * --- Write into TMPMASTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPMASTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMASTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CCCODICE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CONEWCOD),'TMPMASTI','CCCODICE');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          CCCODICE = this.oParentObject.w_CONEWCOD;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Insert into ASCONCON
    i_nConn=i_TableProp[this.ASCONCON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ASCONCON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where 1=1",this.ASCONCON_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMPMASTI
    i_nIdx=cp_GetTableDefIdx('TMPMASTI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMASTI')
    endif
    * --- Create temporary table TMPMASTI
    i_nIdx=cp_AddTableDef('TMPMASTI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.CCOMMATT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.CCOMMATT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          +" where CA__GUID="+cp_ToStrODBC(this.oParentObject.w_COOLDCOD)+"";
          )
    this.TMPMASTI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Aggiornamento chiave primaria
    * --- Write into TMPMASTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPMASTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMASTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CA__GUID ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CONEWCOD),'TMPMASTI','CA__GUID');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          CA__GUID = this.oParentObject.w_CONEWCOD;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Insert into CCOMMATT
    i_nConn=i_TableProp[this.CCOMMATT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCOMMATT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where 1=1",this.CCOMMATT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMPMASTI
    i_nIdx=cp_GetTableDefIdx('TMPMASTI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMASTI')
    endif
    * --- Create temporary table TMPMASTI
    i_nIdx=cp_AddTableDef('TMPMASTI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.CCOMDATT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.CCOMDATT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          +" where CA__GUID="+cp_ToStrODBC(this.oParentObject.w_COOLDCOD)+"";
          )
    this.TMPMASTI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Aggiornamento chiave primaria
    * --- Write into TMPMASTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPMASTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMASTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CA__GUID ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CONEWCOD),'TMPMASTI','CA__GUID');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          CA__GUID = this.oParentObject.w_CONEWCOD;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Insert into CCOMDATT
    i_nConn=i_TableProp[this.CCOMDATT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCOMDATT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where 1=1",this.CCOMDATT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMPMASTI
    i_nIdx=cp_GetTableDefIdx('TMPMASTI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMASTI')
    endif
    * --- Create temporary table TMPMASTI
    i_nIdx=cp_AddTableDef('TMPMASTI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.CCOMMATA_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.CCOMMATA_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
          +" where CA__GUID="+cp_ToStrODBC(this.oParentObject.w_COOLDCOD)+"";
          )
    this.TMPMASTI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Aggiornamento chiave primaria
    * --- Write into TMPMASTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPMASTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMASTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CA__GUID ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CONEWCOD),'TMPMASTI','CA__GUID');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          CA__GUID = this.oParentObject.w_CONEWCOD;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Insert into CCOMMATA
    i_nConn=i_TableProp[this.CCOMMATA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCOMMATA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where 1=1",this.CCOMMATA_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMPMASTI
    i_nIdx=cp_GetTableDefIdx('TMPMASTI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPMASTI')
    endif
    if this.oParentObject.w_TSFLMODE="S"
      * --- Create temporary table TMPMASTI
      i_nIdx=cp_AddTableDef('TMPMASTI') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      i_nConn=i_TableProp[this.CCOMDECL_idx,3] && recupera la connessione
      i_cTable=cp_SetAzi(i_TableProp[this.CCOMDECL_idx,2])
      cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
            +" where DCCODICE="+cp_ToStrODBC(this.oParentObject.w_COOLDCOD)+"";
            )
      this.TMPMASTI_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      * --- Aggiornamento chiave primaria
      * --- Write into TMPMASTI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPMASTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPMASTI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DCCODICE ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CONEWCOD),'TMPMASTI','DCCODICE');
            +i_ccchkf ;
        +" where ";
            +"1 = "+cp_ToStrODBC(1);
               )
      else
        update (i_cTable) set;
            DCCODICE = this.oParentObject.w_CONEWCOD;
            &i_ccchkf. ;
         where;
            1 = 1;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Insert into CCOMDECL
      i_nConn=i_TableProp[this.CCOMDECL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CCOMDECL_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_cTempTable=cp_SetAzi(i_TableProp[this.TMPMASTI_idx,2])
        i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable+" where 1=1",this.CCOMDECL_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Drop temporary table TMPMASTI
      i_nIdx=cp_GetTableDefIdx('TMPMASTI')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPMASTI')
      endif
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='*TMPMASTI'
    this.cWorkTables[2]='CCOMMAST'
    this.cWorkTables[3]='CCOMDETT'
    this.cWorkTables[4]='ASLISCON'
    this.cWorkTables[5]='PREMICON'
    this.cWorkTables[6]='ASCONCON'
    this.cWorkTables[7]='CCOMMATT'
    this.cWorkTables[8]='CCOMDATT'
    this.cWorkTables[9]='CCOMMATA'
    this.cWorkTables[10]='CCOMDECL'
    return(this.OpenAllTables(10))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
