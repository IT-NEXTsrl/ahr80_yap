* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsgp_bdd                                                        *
*              Elimina documenti                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-05-28                                                      *
* Last revis.: 2012-10-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper,pSERIAL
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsgp_bdd",oParentObject,m.pOper,m.pSERIAL)
return(i_retval)

define class tgsgp_bdd as StdBatch
  * --- Local variables
  pOper = space(1)
  pSERIAL = space(10)
  w_MVSERIAL = space(10)
  w_MESSAGE = space(10)
  w_oERRORLOG = .NULL.
  w_CURSORNAME = space(10)
  w_MVTIPDOC = space(5)
  w_MVNUMDOC = 0
  w_MVNUMDOC_2 = 0
  w_MVALFDOC = space(10)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVDATDOC_2 = ctod("  /  /  ")
  * --- WorkFile variables
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina Documenti
    * --- R = Elimina la Singola Riga; T = Elimina Tutti i Documenti
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    if this.pOper = "R"
      this.w_MVSERIAL = this.pSERIAL
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.w_CURSORNAME = SYS( 2015 )
      this.oparentobject.exec_select(this.w_CURSORNAME,"t_DPSERDOC")
      select ( this.w_CURSORNAME )
      GO TOP
      do while NOT EOF()
        this.w_MVSERIAL = t_DPSERDOC
        if NOT EMPTY( this.w_MVSERIAL )
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        select ( this.w_CURSORNAME )
        SKIP
      enddo
      select ( this.w_CURSORNAME )
      USE
    endif
    if this.w_oERRORLOG.IsFullLog()
      this.w_oERRORLOG.PrintLog(this.oParentObject,"Documenti non cancellati",.T., "Si vuole visualizzare l'elenco dei documenti non cancellati?")     
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_MESSAGE = ""
    this.w_MESSAGE = GSAR_BDK( this, this.w_MVSERIAL )
    if NOT EMPTY( this.w_MESSAGE )
      * --- Read from DOC_MAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC"+;
          " from "+i_cTable+" DOC_MAST where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVTIPDOC,MVNUMDOC,MVALFDOC,MVDATDOC;
          from (i_cTable) where;
              MVSERIAL = this.w_MVSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MVTIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
        this.w_MVNUMDOC_2 = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
        this.w_MVALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
        this.w_MVDATDOC_2 = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_MVTIPDOC = ALLTRIM( this.w_MVTIPDOC )
      this.w_MVNUMDOC = ALLTRIM( STR( this.w_MVNUMDOC_2 ) )
      if EMPTY( NVL( this.w_MVALFDOC, "" ) )
        this.w_MVALFDOC = ""
      else
        this.w_MVALFDOC = "/ " + ALLTRIM(this.w_MVALFDOC)
      endif
      this.w_MVDATDOC = DTOC( CP_TODATE( NVL( this.w_MVDATDOC_2, CTOD("  -  -  ") ) ) )
      this.w_oERRORLOG.AddMsgLog("Errore nella cancellazione del documento %1 n. %2 %3 del %4%0%5", this.w_MVTIPDOC, this.w_MVNUMDOC, this.w_MVALFDOC, this.w_MVDATDOC, this.w_MESSAGE)     
    endif
    this.w_MESSAGE = ""
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper,pSERIAL)
    this.pOper=pOper
    this.pSERIAL=pSERIAL
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper,pSERIAL"
endproc
