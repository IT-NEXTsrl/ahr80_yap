* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsgp_bgo                                                        *
*              Gestione stampa contratti                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-02-04                                                      *
* Last revis.: 2012-10-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsgp_bgo",oParentObject,m.pOPER)
return(i_retval)

define class tgsgp_bgo as StdBatch
  * --- Local variables
  pOPER = space(5)
  w_OBJ_MASK = .NULL.
  w_OBJ_CTRL = .NULL.
  w_OSOURCE = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pOPER=="PRINT"
        this.w_OBJ_MASK = GSGP_SCO(this.oParentObject.w_COFLGCIC)
        * --- Controllo se ha passato il test di accesso
        if this.w_OBJ_MASK.bSec1
          this.w_OBJ_MASK.w_COFLGTIP = this.oParentObject.w_COFLGTIP
          this.w_OBJ_CTRL = this.w_OBJ_MASK.GetCtrl("w_COFLGTIP")
          if this.w_OBJ_CTRL.mCond()
            this.w_OBJ_CTRL.SetRadio()     
            this.w_OBJ_CTRL.bUpd = .t.
            this.w_OBJ_CTRL.Valid()     
          endif
          this.w_OBJ_CTRL = .NULL.
          this.w_OBJ_MASK.w_COFLGSTA = this.oParentObject.w_COFLGSTA
          this.w_OBJ_CTRL = this.w_OBJ_MASK.GetCtrl("w_COFLGSTA")
          if this.w_OBJ_CTRL.mCond()
            this.w_OBJ_CTRL.SetRadio()     
            this.w_OBJ_CTRL.bUpd = .t.
            this.w_OBJ_CTRL.Valid()     
          endif
          this.w_OBJ_CTRL = .NULL.
          this.w_OSOURCE.xKey( 1 ) = this.oParentObject.w_COCODICE
          this.w_OBJ_CTRL = this.w_OBJ_MASK.GetCtrl("w_COCODINI")
          if this.w_OBJ_CTRL.mCond()
            this.w_OBJ_CTRL.ecpDrop(this.w_OSOURCE)     
          endif
          this.w_OBJ_CTRL = .NULL.
          this.w_OBJ_CTRL = this.w_OBJ_MASK.GetCtrl("w_COCODFIN")
          if this.w_OBJ_CTRL.mCond()
            this.w_OBJ_CTRL.ecpDrop(this.w_OSOURCE)     
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    this.w_OSOURCE=createobject("_Osource")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
