* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsgp_agp                                                        *
*              Generazione premi                                               *
*                                                                              *
*      Author: Zucchetti - AT                                                  *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-23                                                      *
* Last revis.: 2017-11-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsgp_agp
* --- Da Menu' viene Passato il Parametro che identifica il Ciclo
PARAMETERS pFLVEAC
* --- Fine Area Manuale
return(createobject("tgsgp_agp"))

* --- Class definition
define class tgsgp_agp as StdForm
  Top    = 2
  Left   = 9

  * --- Standard Properties
  Width  = 652
  Height = 447+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-11-22"
  HelpContextID=225865367
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=96

  * --- Constant Properties
  GENPREMI_IDX = 0
  ESERCIZI_IDX = 0
  TIP_DOCU_IDX = 0
  VALUTE_IDX = 0
  CONTI_IDX = 0
  DES_DIVE_IDX = 0
  ZONE_IDX = 0
  AGENTI_IDX = 0
  BUSIUNIT_IDX = 0
  DET_DIFF_IDX = 0
  CAU_CONT_IDX = 0
  CACOCLFO_IDX = 0
  TRADDOCU_IDX = 0
  AZIENDA_IDX = 0
  MASTRI_IDX = 0
  PAG_AMEN_IDX = 0
  CATECOMM_IDX = 0
  GENERDOC_IDX = 0
  INVENTAR_IDX = 0
  LISTINI_IDX = 0
  GRUPPACQ_IDX = 0
  CCOMMAST_IDX = 0
  CONTROPA_IDX = 0
  ART_ICOL_IDX = 0
  CCOMCMAG_IDX = 0
  cFile = "GENPREMI"
  cKeySelect = "GPSERIAL"
  cKeyWhere  = "GPSERIAL=this.w_GPSERIAL"
  cKeyWhereODBC = '"GPSERIAL="+cp_ToStrODBC(this.w_GPSERIAL)';

  cKeyWhereODBCqualified = '"GENPREMI.GPSERIAL="+cp_ToStrODBC(this.w_GPSERIAL)';

  cPrg = "gsgp_agp"
  cComment = "Generazione premi"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(5)
  o_CODAZI = space(5)
  w_GPFLGCIC = space(1)
  w_MVCODESE = space(4)
  w_GPSERIAL = space(10)
  o_GPSERIAL = space(10)
  w_GPDESCRI = space(40)
  w_GPPERLIQ = space(1)
  w_GPDATREG = ctod('  /  /  ')
  o_GPDATREG = ctod('  /  /  ')
  w_GPSTATUS = space(1)
  o_GPSTATUS = space(1)
  w_GPVALORI = space(1)
  w_GPINVENT = space(6)
  w_GPVALCON = space(3)
  o_GPVALCON = space(3)
  w_GPCODLIS = space(5)
  w_GPGRUCAU = space(5)
  w_GPDOCFIS = space(1)
  o_GPDOCFIS = space(1)
  w_GPDOCINT = space(1)
  o_GPDOCINT = space(1)
  w_GPCONTRA = space(15)
  o_GPCONTRA = space(15)
  w_GPCNOVAL = space(1)
  w_GPTIPINT = space(1)
  o_GPTIPINT = space(1)
  w_GPTIPINT = space(1)
  w_GPCODINT = space(15)
  w_GPCODAGE = space(5)
  w_GPCODGRU = space(15)
  w_GPCODGRU = space(15)
  w_COPRMDOC = space(1)
  o_COPRMDOC = space(1)
  w_GPDATINI = ctod('  /  /  ')
  w_GPDATFIN = ctod('  /  /  ')
  w_GPTIPDOC = space(5)
  w_GPTIPDOC = space(5)
  w_GPTIPDOC = space(5)
  w_GPTIPDOC = space(5)
  w_GPTIPDOC = space(5)
  w_GPCODESE = space(4)
  w_GPNUMDOC = 0
  w_GPALFDOC = space(10)
  w_GPDATDOC = ctod('  /  /  ')
  w_GPPUNPRE = space(1)
  w_GPDATCIV = ctod('  /  /  ')
  w_GPDATDIV = ctod('  /  /  ')
  w_GPCAUINT = space(5)
  w_GPCAUINT = space(5)
  w_MVPRD = space(2)
  w_MVALFDOC = space(2)
  w_MVNUMDOC = 0
  w_DESORI = space(35)
  w_MVANNDOC = space(4)
  w_PRDATINV = ctod('  /  /  ')
  w_GPDESINT = space(40)
  w_GPDESGRU = space(40)
  w_GPDESAGE = space(40)
  w_DATACON = ctod('  /  /  ')
  w_DESINT = space(35)
  w_LSFLGCIC = space(1)
  w_LSFLSTAT = space(1)
  w_LORDO = space(1)
  w_LSVALLIS = space(3)
  w_COFLGTIP = space(1)
  w_COFLGCIC = space(1)
  w_GPCODVAL = space(3)
  w_MVVALNAZ = space(3)
  w_FLVEAC1 = space(1)
  w_FLVEAC2 = space(1)
  w_COPRMLIQ = space(1)
  w_CLADOC = space(2)
  w_CLADOC2 = space(2)
  w_MVCODPAG = space(5)
  w_COGRUCAU = space(5)
  w_COBASPRO = space(1)
  w_MVCAUCON = space(5)
  w_MVCODIVA = space(5)
  w_COARTFOR = space(20)
  w_CONOTART = space(20)
  w_COPUNUNM = space(3)
  w_CAUDF1 = space(5)
  w_CAUDF2 = space(5)
  w_CAUDF3 = space(5)
  w_CAUDF4 = space(5)
  w_OBTEST = ctod('  /  /  ')
  w_GPTIPDOC = space(5)
  w_DESFOR = space(40)
  w_UMFORF = space(3)
  w_COPUNART = space(20)
  w_DESFM = space(40)
  w_COBASPRO = space(1)
  w_SERGDOC = space(10)
  w_MVSCOPAG = 0
  w_PSSERIAL = space(10)
  w_CAUDAG = space(5)
  w_COFLGSTA = space(1)
  w_FLINTE = space(1)
  w_COGRUCAA = space(5)
  w_DESGCAU = space(35)
  w_FLSIMU = space(1)
  w_CSFLGCIC = space(1)
  w_VALCON = space(1)
  w_PROVVFIS = space(1)
  w_PROVVINT = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_GPSERIAL = this.W_GPSERIAL

  * --- Children pointers
  GSGP_MGP = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsgp_agp
  *Variabile di lavoro indicante il ciclo
  pCiclo=' '
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
      return(lower('GSGP_AGP,'+this.pCiclo))
  EndFunc
  
  
  *Log generazione documenti
  w_oERRORLOG = .NULL.
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'GENPREMI','gsgp_agp')
    stdPageFrame::Init()
    *set procedure to GSGP_MGP additive
    with this
      .Pages(1).addobject("oPag","tgsgp_agpPag1","gsgp_agp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generale")
      .Pages(1).HelpContextID = 167593781
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oGPSERIAL_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSGP_MGP
    * --- Area Manuale = Init Page Frame
    * --- gsgp_agp
    *Valorizzo variabile di lavoro
    WITH THIS.PARENT
     .pCiclo = pFlveac
     .cComment = ah_msgformat("Generazione premi ")  +IIF(.pCICLO = 'V', ah_msgformat("(vendite)"),ah_msgformat("(acquisti)"))
     .cAutoZoom = IIF(.pCiclo = 'V', 'GSGPVAGP','GSGPAAGP')
    ENDWITH
    
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[25]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='TIP_DOCU'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='DES_DIVE'
    this.cWorkTables[6]='ZONE'
    this.cWorkTables[7]='AGENTI'
    this.cWorkTables[8]='BUSIUNIT'
    this.cWorkTables[9]='DET_DIFF'
    this.cWorkTables[10]='CAU_CONT'
    this.cWorkTables[11]='CACOCLFO'
    this.cWorkTables[12]='TRADDOCU'
    this.cWorkTables[13]='AZIENDA'
    this.cWorkTables[14]='MASTRI'
    this.cWorkTables[15]='PAG_AMEN'
    this.cWorkTables[16]='CATECOMM'
    this.cWorkTables[17]='GENERDOC'
    this.cWorkTables[18]='INVENTAR'
    this.cWorkTables[19]='LISTINI'
    this.cWorkTables[20]='GRUPPACQ'
    this.cWorkTables[21]='CCOMMAST'
    this.cWorkTables[22]='CONTROPA'
    this.cWorkTables[23]='ART_ICOL'
    this.cWorkTables[24]='CCOMCMAG'
    this.cWorkTables[25]='GENPREMI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(25))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GENPREMI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GENPREMI_IDX,3]
  return

  function CreateChildren()
    this.GSGP_MGP = CREATEOBJECT('stdLazyChild',this,'GSGP_MGP')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSGP_MGP)
      this.GSGP_MGP.DestroyChildrenChain()
      this.GSGP_MGP=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSGP_MGP.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSGP_MGP.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSGP_MGP.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSGP_MGP.SetKey(;
            .w_GPSERIAL,"DGPERIAL";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSGP_MGP.ChangeRow(this.cRowID+'      1',1;
             ,.w_GPSERIAL,"DGPERIAL";
             )
    endwith
    return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_GPSERIAL = NVL(GPSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_12_joined
    link_1_12_joined=.f.
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_1_20_joined
    link_1_20_joined=.f.
    local link_1_21_joined
    link_1_21_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from GENPREMI where GPSERIAL=KeySet.GPSERIAL
    *
    i_nConn = i_TableProp[this.GENPREMI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENPREMI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GENPREMI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GENPREMI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' GENPREMI '
      link_1_12_joined=this.AddJoinedLink_1_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_21_joined=this.AddJoinedLink_1_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'GPSERIAL',this.w_GPSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_MVCODESE = g_CODESE
        .w_COPRMDOC = space(1)
        .w_MVPRD = space(2)
        .w_DESORI = space(35)
        .w_PRDATINV = ctod("  /  /  ")
        .w_GPDESINT = space(40)
        .w_GPDESGRU = space(40)
        .w_GPDESAGE = space(40)
        .w_DATACON = ctod("  /  /  ")
        .w_DESINT = space(35)
        .w_LSFLGCIC = space(1)
        .w_LSFLSTAT = space(1)
        .w_LORDO = space(1)
        .w_LSVALLIS = space(3)
        .w_COFLGTIP = space(1)
        .w_COFLGCIC = space(1)
        .w_GPCODVAL = space(3)
        .w_MVVALNAZ = space(3)
        .w_FLVEAC1 = space(1)
        .w_FLVEAC2 = space(1)
        .w_COPRMLIQ = space(1)
        .w_CLADOC = space(2)
        .w_CLADOC2 = space(2)
        .w_COGRUCAU = space(5)
        .w_COBASPRO = space(1)
        .w_MVCAUCON = space(5)
        .w_MVCODIVA = space(5)
        .w_CONOTART = space(20)
        .w_COPUNUNM = space(3)
        .w_CAUDF1 = space(5)
        .w_CAUDF2 = space(5)
        .w_CAUDF3 = space(5)
        .w_CAUDF4 = space(5)
        .w_DESFOR = space(40)
        .w_UMFORF = space(3)
        .w_DESFM = space(40)
        .w_COBASPRO = space(1)
        .w_SERGDOC = space(10)
        .w_MVSCOPAG = 0
        .w_CAUDAG = space(5)
        .w_COFLGSTA = space(1)
        .w_FLINTE = space(1)
        .w_COGRUCAA = space(5)
        .w_DESGCAU = space(35)
        .w_FLSIMU = space(1)
        .w_CSFLGCIC = space(1)
        .w_VALCON = space(1)
        .w_PROVVFIS = 'S'
        .w_PROVVINT = 'S'
        .w_CODAZI = i_CODAZI
          .link_1_1('Load')
        .w_GPFLGCIC = NVL(GPFLGCIC,space(1))
        .w_GPSERIAL = NVL(GPSERIAL,space(10))
        .op_GPSERIAL = .w_GPSERIAL
        .w_GPDESCRI = NVL(GPDESCRI,space(40))
        .w_GPPERLIQ = NVL(GPPERLIQ,space(1))
        .w_GPDATREG = NVL(cp_ToDate(GPDATREG),ctod("  /  /  "))
        .w_GPSTATUS = NVL(GPSTATUS,space(1))
        .w_GPVALORI = NVL(GPVALORI,space(1))
        .w_GPINVENT = NVL(GPINVENT,space(6))
          .link_1_10('Load')
        .w_GPVALCON = NVL(GPVALCON,space(3))
          * evitabile
          *.link_1_11('Load')
        .w_GPCODLIS = NVL(GPCODLIS,space(5))
          if link_1_12_joined
            this.w_GPCODLIS = NVL(LSCODLIS112,NVL(this.w_GPCODLIS,space(5)))
            this.w_LSFLGCIC = NVL(LSFLGCIC112,space(1))
            this.w_LSFLSTAT = NVL(LSFLSTAT112,space(1))
            this.w_LORDO = NVL(LSIVALIS112,space(1))
            this.w_LSVALLIS = NVL(LSVALLIS112,space(3))
          else
          .link_1_12('Load')
          endif
        .w_GPGRUCAU = NVL(GPGRUCAU,space(5))
          .link_1_13('Load')
        .w_GPDOCFIS = NVL(GPDOCFIS,space(1))
        .w_GPDOCINT = NVL(GPDOCINT,space(1))
        .w_GPCONTRA = NVL(GPCONTRA,space(15))
          if link_1_16_joined
            this.w_GPCONTRA = NVL(COCODICE116,NVL(this.w_GPCONTRA,space(15)))
            this.w_DATACON = NVL(cp_ToDate(CODATREG116),ctod("  /  /  "))
            this.w_COFLGTIP = NVL(COFLGTIP116,space(1))
            this.w_COFLGCIC = NVL(COFLGCIC116,space(1))
            this.w_VALCON = NVL(COCODVAL116,space(1))
            this.w_COPRMDOC = NVL(COPRMDOC116,space(1))
            this.w_COPRMLIQ = NVL(COPRMLIQ116,space(1))
            this.w_GPTIPINT = NVL(COTIPINT116,space(1))
            this.w_GPCODINT = NVL(COCODINT116,space(15))
            this.w_GPCODGRU = NVL(COCODGRU116,space(15))
            this.w_GPCODAGE = NVL(COCODAGE116,space(5))
            this.w_COFLGSTA = NVL(COFLGSTA116,space(1))
          else
          .link_1_16('Load')
          endif
        .w_GPCNOVAL = NVL(GPCNOVAL,space(1))
        .w_GPTIPINT = NVL(GPTIPINT,space(1))
        .w_GPTIPINT = NVL(GPTIPINT,space(1))
        .w_GPCODINT = NVL(GPCODINT,space(15))
          if link_1_20_joined
            this.w_GPCODINT = NVL(ANCODICE120,NVL(this.w_GPCODINT,space(15)))
            this.w_GPDESINT = NVL(ANDESCRI120,space(40))
          else
          .link_1_20('Load')
          endif
        .w_GPCODAGE = NVL(GPCODAGE,space(5))
          if link_1_21_joined
            this.w_GPCODAGE = NVL(AGCODAGE121,NVL(this.w_GPCODAGE,space(5)))
            this.w_GPDESAGE = NVL(AGDESAGE121,space(40))
          else
          .link_1_21('Load')
          endif
        .w_GPCODGRU = NVL(GPCODGRU,space(15))
          .link_1_22('Load')
        .w_GPCODGRU = NVL(GPCODGRU,space(15))
          .link_1_23('Load')
        .w_GPDATINI = NVL(cp_ToDate(GPDATINI),ctod("  /  /  "))
        .w_GPDATFIN = NVL(cp_ToDate(GPDATFIN),ctod("  /  /  "))
        .w_GPTIPDOC = NVL(GPTIPDOC,space(5))
          .link_1_27('Load')
        .w_GPTIPDOC = NVL(GPTIPDOC,space(5))
          .link_1_28('Load')
        .w_GPTIPDOC = NVL(GPTIPDOC,space(5))
          .link_1_29('Load')
        .w_GPTIPDOC = NVL(GPTIPDOC,space(5))
          .link_1_30('Load')
        .w_GPTIPDOC = NVL(GPTIPDOC,space(5))
          .link_1_31('Load')
        .w_GPCODESE = NVL(GPCODESE,space(4))
          .link_1_32('Load')
        .w_GPNUMDOC = NVL(GPNUMDOC,0)
        .w_GPALFDOC = NVL(GPALFDOC,space(10))
        .w_GPDATDOC = NVL(cp_ToDate(GPDATDOC),ctod("  /  /  "))
        .w_GPPUNPRE = NVL(GPPUNPRE,space(1))
        .w_GPDATCIV = NVL(cp_ToDate(GPDATCIV),ctod("  /  /  "))
        .w_GPDATDIV = NVL(cp_ToDate(GPDATDIV),ctod("  /  /  "))
        .w_GPCAUINT = NVL(GPCAUINT,space(5))
          .link_1_39('Load')
        .w_GPCAUINT = NVL(GPCAUINT,space(5))
          .link_1_40('Load')
        .w_MVALFDOC = .w_GPALFDOC
        .w_MVNUMDOC = .w_GPNUMDOC
        .w_MVANNDOC = STR(YEAR(.w_GPDATDOC), 4, 0)
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .w_MVCODPAG = .w_MVCODPAG
          .link_1_98('Load')
        .w_COARTFOR = .w_COARTFOR
          .link_1_103('Load')
        .w_OBTEST = .w_GPDATREG
        .w_GPTIPDOC = NVL(GPTIPDOC,space(5))
          .link_1_111('Load')
        .w_COPUNART = .w_COPUNART
          .link_1_114('Load')
        .w_PSSERIAL = .w_GPSERIAL
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'GENPREMI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_50.enabled = this.oPgFrm.Page1.oPag.oBtn_1_50.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsgp_agp
    this.mEnableControls()
    if this.w_GPFLGCIC <> this.pCiclo
      this.BlankRec()
    endif
    
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(5)
      .w_GPFLGCIC = space(1)
      .w_MVCODESE = space(4)
      .w_GPSERIAL = space(10)
      .w_GPDESCRI = space(40)
      .w_GPPERLIQ = space(1)
      .w_GPDATREG = ctod("  /  /  ")
      .w_GPSTATUS = space(1)
      .w_GPVALORI = space(1)
      .w_GPINVENT = space(6)
      .w_GPVALCON = space(3)
      .w_GPCODLIS = space(5)
      .w_GPGRUCAU = space(5)
      .w_GPDOCFIS = space(1)
      .w_GPDOCINT = space(1)
      .w_GPCONTRA = space(15)
      .w_GPCNOVAL = space(1)
      .w_GPTIPINT = space(1)
      .w_GPTIPINT = space(1)
      .w_GPCODINT = space(15)
      .w_GPCODAGE = space(5)
      .w_GPCODGRU = space(15)
      .w_GPCODGRU = space(15)
      .w_COPRMDOC = space(1)
      .w_GPDATINI = ctod("  /  /  ")
      .w_GPDATFIN = ctod("  /  /  ")
      .w_GPTIPDOC = space(5)
      .w_GPTIPDOC = space(5)
      .w_GPTIPDOC = space(5)
      .w_GPTIPDOC = space(5)
      .w_GPTIPDOC = space(5)
      .w_GPCODESE = space(4)
      .w_GPNUMDOC = 0
      .w_GPALFDOC = space(10)
      .w_GPDATDOC = ctod("  /  /  ")
      .w_GPPUNPRE = space(1)
      .w_GPDATCIV = ctod("  /  /  ")
      .w_GPDATDIV = ctod("  /  /  ")
      .w_GPCAUINT = space(5)
      .w_GPCAUINT = space(5)
      .w_MVPRD = space(2)
      .w_MVALFDOC = space(2)
      .w_MVNUMDOC = 0
      .w_DESORI = space(35)
      .w_MVANNDOC = space(4)
      .w_PRDATINV = ctod("  /  /  ")
      .w_GPDESINT = space(40)
      .w_GPDESGRU = space(40)
      .w_GPDESAGE = space(40)
      .w_DATACON = ctod("  /  /  ")
      .w_DESINT = space(35)
      .w_LSFLGCIC = space(1)
      .w_LSFLSTAT = space(1)
      .w_LORDO = space(1)
      .w_LSVALLIS = space(3)
      .w_COFLGTIP = space(1)
      .w_COFLGCIC = space(1)
      .w_GPCODVAL = space(3)
      .w_MVVALNAZ = space(3)
      .w_FLVEAC1 = space(1)
      .w_FLVEAC2 = space(1)
      .w_COPRMLIQ = space(1)
      .w_CLADOC = space(2)
      .w_CLADOC2 = space(2)
      .w_MVCODPAG = space(5)
      .w_COGRUCAU = space(5)
      .w_COBASPRO = space(1)
      .w_MVCAUCON = space(5)
      .w_MVCODIVA = space(5)
      .w_COARTFOR = space(20)
      .w_CONOTART = space(20)
      .w_COPUNUNM = space(3)
      .w_CAUDF1 = space(5)
      .w_CAUDF2 = space(5)
      .w_CAUDF3 = space(5)
      .w_CAUDF4 = space(5)
      .w_OBTEST = ctod("  /  /  ")
      .w_GPTIPDOC = space(5)
      .w_DESFOR = space(40)
      .w_UMFORF = space(3)
      .w_COPUNART = space(20)
      .w_DESFM = space(40)
      .w_COBASPRO = space(1)
      .w_SERGDOC = space(10)
      .w_MVSCOPAG = 0
      .w_PSSERIAL = space(10)
      .w_CAUDAG = space(5)
      .w_COFLGSTA = space(1)
      .w_FLINTE = space(1)
      .w_COGRUCAA = space(5)
      .w_DESGCAU = space(35)
      .w_FLSIMU = space(1)
      .w_CSFLGCIC = space(1)
      .w_VALCON = space(1)
      .w_PROVVFIS = space(1)
      .w_PROVVINT = space(1)
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CODAZI))
          .link_1_1('Full')
          endif
        .w_GPFLGCIC = .pCiclo
        .w_MVCODESE = g_CODESE
          .DoRTCalc(4,5,.f.)
        .w_GPPERLIQ = 'S'
        .w_GPDATREG = i_DATSYS
        .w_GPSTATUS = 'S'
        .w_GPVALORI = 'A'
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_GPINVENT))
          .link_1_10('Full')
          endif
        .w_GPVALCON = iif(empty(.w_VALCON), g_PERVAL, .w_VALCON)
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_GPVALCON))
          .link_1_11('Full')
          endif
        .w_GPCODLIS = SPACE(5)
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_GPCODLIS))
          .link_1_12('Full')
          endif
        .w_GPGRUCAU = IIF(.w_GPFLGCIC='V', .w_COGRUCAU,  .w_COGRUCAA)
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_GPGRUCAU))
          .link_1_13('Full')
          endif
        .w_GPDOCFIS = IIF(.w_GPSTATUS='D','S','N')
        .w_GPDOCINT = IIF(.w_GPSTATUS='D','S','N')
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_GPCONTRA))
          .link_1_16('Full')
          endif
        .w_GPCNOVAL = 'N'
        .w_GPTIPINT = iif(Empty(.w_GPCONTRA), iif(.pCiclo='V','C','F'), .w_GPTIPINT)
        .w_GPTIPINT = iif(Empty(.w_GPCONTRA), iif(.pCiclo='V','C','F'), .w_GPTIPINT)
        .w_GPCODINT = IIF(!.w_GPTIPINT $ 'CF','',.w_GPCODINT)
        .DoRTCalc(20,20,.f.)
          if not(empty(.w_GPCODINT))
          .link_1_20('Full')
          endif
        .w_GPCODAGE = IIF(.w_GPTIPINT <> 'A','',.w_GPCODAGE)
        .DoRTCalc(21,21,.f.)
          if not(empty(.w_GPCODAGE))
          .link_1_21('Full')
          endif
        .w_GPCODGRU = IIF(.w_GPTIPINT <> 'G','',.w_GPCODGRU)
        .DoRTCalc(22,22,.f.)
          if not(empty(.w_GPCODGRU))
          .link_1_22('Full')
          endif
        .w_GPCODGRU = IIF(.w_GPTIPINT <> 'G','',.w_GPCODGRU)
        .DoRTCalc(23,23,.f.)
          if not(empty(.w_GPCODGRU))
          .link_1_23('Full')
          endif
          .DoRTCalc(24,24,.f.)
        .w_GPDATINI = g_INIESE
        .w_GPDATFIN = .w_GPDATREG
        .w_GPTIPDOC = iif(Empty(.w_GPCONTRA) Or .w_GPDOCFIS <>'S','', ICASE(.w_COPRMDOC='F',.w_CAUDF1,.w_COPRMDOC='N',.w_CAUDF2,.w_COPRMDOC='P',.w_CAUDF3,.w_COPRMDOC='C',.w_CAUDF4,.w_COPRMDOC='A',.w_CAUDAG,''))
        .DoRTCalc(27,27,.f.)
          if not(empty(.w_GPTIPDOC))
          .link_1_27('Full')
          endif
        .w_GPTIPDOC = iif(Empty(.w_GPCONTRA) Or .w_GPDOCFIS <>'S','', ICASE(.w_COPRMDOC='F',.w_CAUDF1,.w_COPRMDOC='N',.w_CAUDF2,.w_COPRMDOC='P',.w_CAUDF3,.w_COPRMDOC='V',.w_CAUDF4,.w_COPRMDOC='A',.w_CAUDAG,''))
        .DoRTCalc(28,28,.f.)
          if not(empty(.w_GPTIPDOC))
          .link_1_28('Full')
          endif
        .w_GPTIPDOC = iif(Empty(.w_GPCONTRA) Or .w_GPDOCFIS <>'S','', ICASE(.w_COPRMDOC='F',.w_CAUDF1,.w_COPRMDOC='N',.w_CAUDF2,.w_COPRMDOC='P',.w_CAUDF3,.w_COPRMDOC='V',.w_CAUDF4,.w_COPRMDOC='A',.w_CAUDAG,''))
        .DoRTCalc(29,29,.f.)
          if not(empty(.w_GPTIPDOC))
          .link_1_29('Full')
          endif
        .w_GPTIPDOC = iif(Empty(.w_GPCONTRA) Or .w_GPDOCFIS <>'S','', ICASE(.w_COPRMDOC='F',.w_CAUDF1,.w_COPRMDOC='N',.w_CAUDF2,.w_COPRMDOC='P',.w_CAUDF3,.w_COPRMDOC='V',.w_CAUDF4,.w_COPRMDOC='A',.w_CAUDAG,''))
        .DoRTCalc(30,30,.f.)
          if not(empty(.w_GPTIPDOC))
          .link_1_30('Full')
          endif
        .w_GPTIPDOC = iif(Empty(.w_GPCONTRA) Or .w_GPDOCFIS <>'S','', ICASE(.w_COPRMDOC='F',.w_CAUDF1,.w_COPRMDOC='N',.w_CAUDF2,.w_COPRMDOC='P',.w_CAUDF3,.w_COPRMDOC='V',.w_CAUDF4,.w_COPRMDOC='A',.w_CAUDAG,''))
        .DoRTCalc(31,31,.f.)
          if not(empty(.w_GPTIPDOC))
          .link_1_31('Full')
          endif
        .w_GPCODESE = g_CODESE
        .DoRTCalc(32,32,.f.)
          if not(empty(.w_GPCODESE))
          .link_1_32('Full')
          endif
        .w_GPNUMDOC = IIF(Empty(.w_GPCONTRA) Or .w_GPDOCFIS <> 'S', 0, .w_GPNUMDOC)
        .w_GPALFDOC = ''
        .w_GPDATDOC = .w_GPDATREG
        .w_GPPUNPRE = 'S'
        .w_GPDATCIV = .w_GPDATREG
        .w_GPDATDIV = .w_GPDATREG
        .w_GPCAUINT = ''
        .DoRTCalc(39,39,.f.)
          if not(empty(.w_GPCAUINT))
          .link_1_39('Full')
          endif
        .w_GPCAUINT = ''
        .DoRTCalc(40,40,.f.)
          if not(empty(.w_GPCAUINT))
          .link_1_40('Full')
          endif
          .DoRTCalc(41,41,.f.)
        .w_MVALFDOC = .w_GPALFDOC
        .w_MVNUMDOC = .w_GPNUMDOC
          .DoRTCalc(44,44,.f.)
        .w_MVANNDOC = STR(YEAR(.w_GPDATDOC), 4, 0)
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
          .DoRTCalc(46,64,.f.)
        .w_MVCODPAG = .w_MVCODPAG
        .DoRTCalc(65,65,.f.)
          if not(empty(.w_MVCODPAG))
          .link_1_98('Full')
          endif
          .DoRTCalc(66,69,.f.)
        .w_COARTFOR = .w_COARTFOR
        .DoRTCalc(70,70,.f.)
          if not(empty(.w_COARTFOR))
          .link_1_103('Full')
          endif
          .DoRTCalc(71,76,.f.)
        .w_OBTEST = .w_GPDATREG
        .w_GPTIPDOC = iif(Empty(.w_GPCONTRA) Or .w_GPDOCFIS <>'S','', ICASE(.w_COPRMDOC='F',.w_CAUDF1,.w_COPRMDOC='N',.w_CAUDF2,.w_COPRMDOC='P',.w_CAUDF3,.w_COPRMDOC='V',.w_CAUDF4,.w_COPRMDOC='A',.w_CAUDAG,''))
        .DoRTCalc(78,78,.f.)
          if not(empty(.w_GPTIPDOC))
          .link_1_111('Full')
          endif
          .DoRTCalc(79,80,.f.)
        .w_COPUNART = .w_COPUNART
        .DoRTCalc(81,81,.f.)
          if not(empty(.w_COPUNART))
          .link_1_114('Full')
          endif
          .DoRTCalc(82,85,.f.)
        .w_PSSERIAL = .w_GPSERIAL
          .DoRTCalc(87,94,.f.)
        .w_PROVVFIS = 'S'
        .w_PROVVINT = 'S'
      endif
    endwith
    cp_BlankRecExtFlds(this,'GENPREMI')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_50.enabled = this.oPgFrm.Page1.oPag.oBtn_1_50.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsgp_agp
    this.mEnableControls()
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GENPREMI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENPREMI_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEGEPR","i_codazi,w_GPSERIAL")
      .op_codazi = .w_codazi
      .op_GPSERIAL = .w_GPSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oGPSERIAL_1_4.enabled = !i_bVal
      .Page1.oPag.oGPDESCRI_1_5.enabled = i_bVal
      .Page1.oPag.oGPPERLIQ_1_6.enabled = i_bVal
      .Page1.oPag.oGPDATREG_1_7.enabled = i_bVal
      .Page1.oPag.oGPSTATUS_1_8.enabled = i_bVal
      .Page1.oPag.oGPVALORI_1_9.enabled = i_bVal
      .Page1.oPag.oGPINVENT_1_10.enabled = i_bVal
      .Page1.oPag.oGPVALCON_1_11.enabled = i_bVal
      .Page1.oPag.oGPCODLIS_1_12.enabled = i_bVal
      .Page1.oPag.oGPGRUCAU_1_13.enabled = i_bVal
      .Page1.oPag.oGPDOCFIS_1_14.enabled = i_bVal
      .Page1.oPag.oGPDOCINT_1_15.enabled = i_bVal
      .Page1.oPag.oGPCONTRA_1_16.enabled = i_bVal
      .Page1.oPag.oGPCNOVAL_1_17.enabled = i_bVal
      .Page1.oPag.oGPTIPINT_1_18.enabled = i_bVal
      .Page1.oPag.oGPTIPINT_1_19.enabled = i_bVal
      .Page1.oPag.oGPCODINT_1_20.enabled = i_bVal
      .Page1.oPag.oGPCODAGE_1_21.enabled = i_bVal
      .Page1.oPag.oGPCODGRU_1_22.enabled = i_bVal
      .Page1.oPag.oGPCODGRU_1_23.enabled = i_bVal
      .Page1.oPag.oGPDATINI_1_25.enabled = i_bVal
      .Page1.oPag.oGPDATFIN_1_26.enabled = i_bVal
      .Page1.oPag.oGPTIPDOC_1_27.enabled = i_bVal
      .Page1.oPag.oGPTIPDOC_1_28.enabled = i_bVal
      .Page1.oPag.oGPTIPDOC_1_29.enabled = i_bVal
      .Page1.oPag.oGPTIPDOC_1_30.enabled = i_bVal
      .Page1.oPag.oGPTIPDOC_1_31.enabled = i_bVal
      .Page1.oPag.oGPCODESE_1_32.enabled = i_bVal
      .Page1.oPag.oGPALFDOC_1_34.enabled = i_bVal
      .Page1.oPag.oGPDATDOC_1_35.enabled = i_bVal
      .Page1.oPag.oGPPUNPRE_1_36.enabled = i_bVal
      .Page1.oPag.oGPDATCIV_1_37.enabled = i_bVal
      .Page1.oPag.oGPDATDIV_1_38.enabled = i_bVal
      .Page1.oPag.oGPCAUINT_1_39.enabled = i_bVal
      .Page1.oPag.oGPCAUINT_1_40.enabled = i_bVal
      .Page1.oPag.oBtn_1_49.enabled = i_bVal
      .Page1.oPag.oBtn_1_50.enabled = .Page1.oPag.oBtn_1_50.mCond()
      .Page1.oPag.oObj_1_51.enabled = i_bVal
    endwith
    this.GSGP_MGP.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'GENPREMI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSGP_MGP.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GENPREMI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPFLGCIC,"GPFLGCIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPSERIAL,"GPSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPDESCRI,"GPDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPPERLIQ,"GPPERLIQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPDATREG,"GPDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPSTATUS,"GPSTATUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPVALORI,"GPVALORI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPINVENT,"GPINVENT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPVALCON,"GPVALCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCODLIS,"GPCODLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPGRUCAU,"GPGRUCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPDOCFIS,"GPDOCFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPDOCINT,"GPDOCINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCONTRA,"GPCONTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCNOVAL,"GPCNOVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPTIPINT,"GPTIPINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPTIPINT,"GPTIPINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCODINT,"GPCODINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCODAGE,"GPCODAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCODGRU,"GPCODGRU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCODGRU,"GPCODGRU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPDATINI,"GPDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPDATFIN,"GPDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPTIPDOC,"GPTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPTIPDOC,"GPTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPTIPDOC,"GPTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPTIPDOC,"GPTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPTIPDOC,"GPTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCODESE,"GPCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPNUMDOC,"GPNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPALFDOC,"GPALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPDATDOC,"GPDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPPUNPRE,"GPPUNPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPDATCIV,"GPDATCIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPDATDIV,"GPDATDIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCAUINT,"GPCAUINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCAUINT,"GPCAUINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPTIPDOC,"GPTIPDOC",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GENPREMI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENPREMI_IDX,2])
    i_lTable = "GENPREMI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.GENPREMI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GENPREMI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GENPREMI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.GENPREMI_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEGEPR","i_codazi,w_GPSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into GENPREMI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GENPREMI')
        i_extval=cp_InsertValODBCExtFlds(this,'GENPREMI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(GPFLGCIC,GPSERIAL,GPDESCRI,GPPERLIQ,GPDATREG"+;
                  ",GPSTATUS,GPVALORI,GPINVENT,GPVALCON,GPCODLIS"+;
                  ",GPGRUCAU,GPDOCFIS,GPDOCINT,GPCONTRA,GPCNOVAL"+;
                  ",GPTIPINT,GPCODINT,GPCODAGE,GPCODGRU,GPDATINI"+;
                  ",GPDATFIN,GPTIPDOC,GPCODESE,GPNUMDOC,GPALFDOC"+;
                  ",GPDATDOC,GPPUNPRE,GPDATCIV,GPDATDIV,GPCAUINT "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_GPFLGCIC)+;
                  ","+cp_ToStrODBC(this.w_GPSERIAL)+;
                  ","+cp_ToStrODBC(this.w_GPDESCRI)+;
                  ","+cp_ToStrODBC(this.w_GPPERLIQ)+;
                  ","+cp_ToStrODBC(this.w_GPDATREG)+;
                  ","+cp_ToStrODBC(this.w_GPSTATUS)+;
                  ","+cp_ToStrODBC(this.w_GPVALORI)+;
                  ","+cp_ToStrODBCNull(this.w_GPINVENT)+;
                  ","+cp_ToStrODBCNull(this.w_GPVALCON)+;
                  ","+cp_ToStrODBCNull(this.w_GPCODLIS)+;
                  ","+cp_ToStrODBCNull(this.w_GPGRUCAU)+;
                  ","+cp_ToStrODBC(this.w_GPDOCFIS)+;
                  ","+cp_ToStrODBC(this.w_GPDOCINT)+;
                  ","+cp_ToStrODBCNull(this.w_GPCONTRA)+;
                  ","+cp_ToStrODBC(this.w_GPCNOVAL)+;
                  ","+cp_ToStrODBC(this.w_GPTIPINT)+;
                  ","+cp_ToStrODBCNull(this.w_GPCODINT)+;
                  ","+cp_ToStrODBCNull(this.w_GPCODAGE)+;
                  ","+cp_ToStrODBCNull(this.w_GPCODGRU)+;
                  ","+cp_ToStrODBC(this.w_GPDATINI)+;
                  ","+cp_ToStrODBC(this.w_GPDATFIN)+;
                  ","+cp_ToStrODBCNull(this.w_GPTIPDOC)+;
                  ","+cp_ToStrODBCNull(this.w_GPCODESE)+;
                  ","+cp_ToStrODBC(this.w_GPNUMDOC)+;
                  ","+cp_ToStrODBC(this.w_GPALFDOC)+;
                  ","+cp_ToStrODBC(this.w_GPDATDOC)+;
                  ","+cp_ToStrODBC(this.w_GPPUNPRE)+;
                  ","+cp_ToStrODBC(this.w_GPDATCIV)+;
                  ","+cp_ToStrODBC(this.w_GPDATDIV)+;
                  ","+cp_ToStrODBCNull(this.w_GPCAUINT)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GENPREMI')
        i_extval=cp_InsertValVFPExtFlds(this,'GENPREMI')
        cp_CheckDeletedKey(i_cTable,0,'GPSERIAL',this.w_GPSERIAL)
        INSERT INTO (i_cTable);
              (GPFLGCIC,GPSERIAL,GPDESCRI,GPPERLIQ,GPDATREG,GPSTATUS,GPVALORI,GPINVENT,GPVALCON,GPCODLIS,GPGRUCAU,GPDOCFIS,GPDOCINT,GPCONTRA,GPCNOVAL,GPTIPINT,GPCODINT,GPCODAGE,GPCODGRU,GPDATINI,GPDATFIN,GPTIPDOC,GPCODESE,GPNUMDOC,GPALFDOC,GPDATDOC,GPPUNPRE,GPDATCIV,GPDATDIV,GPCAUINT  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_GPFLGCIC;
                  ,this.w_GPSERIAL;
                  ,this.w_GPDESCRI;
                  ,this.w_GPPERLIQ;
                  ,this.w_GPDATREG;
                  ,this.w_GPSTATUS;
                  ,this.w_GPVALORI;
                  ,this.w_GPINVENT;
                  ,this.w_GPVALCON;
                  ,this.w_GPCODLIS;
                  ,this.w_GPGRUCAU;
                  ,this.w_GPDOCFIS;
                  ,this.w_GPDOCINT;
                  ,this.w_GPCONTRA;
                  ,this.w_GPCNOVAL;
                  ,this.w_GPTIPINT;
                  ,this.w_GPCODINT;
                  ,this.w_GPCODAGE;
                  ,this.w_GPCODGRU;
                  ,this.w_GPDATINI;
                  ,this.w_GPDATFIN;
                  ,this.w_GPTIPDOC;
                  ,this.w_GPCODESE;
                  ,this.w_GPNUMDOC;
                  ,this.w_GPALFDOC;
                  ,this.w_GPDATDOC;
                  ,this.w_GPPUNPRE;
                  ,this.w_GPDATCIV;
                  ,this.w_GPDATDIV;
                  ,this.w_GPCAUINT;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.GENPREMI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GENPREMI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.GENPREMI_IDX,i_nConn)
      *
      * update GENPREMI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'GENPREMI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " GPFLGCIC="+cp_ToStrODBC(this.w_GPFLGCIC)+;
             ",GPDESCRI="+cp_ToStrODBC(this.w_GPDESCRI)+;
             ",GPPERLIQ="+cp_ToStrODBC(this.w_GPPERLIQ)+;
             ",GPDATREG="+cp_ToStrODBC(this.w_GPDATREG)+;
             ",GPSTATUS="+cp_ToStrODBC(this.w_GPSTATUS)+;
             ",GPVALORI="+cp_ToStrODBC(this.w_GPVALORI)+;
             ",GPINVENT="+cp_ToStrODBCNull(this.w_GPINVENT)+;
             ",GPVALCON="+cp_ToStrODBCNull(this.w_GPVALCON)+;
             ",GPCODLIS="+cp_ToStrODBCNull(this.w_GPCODLIS)+;
             ",GPGRUCAU="+cp_ToStrODBCNull(this.w_GPGRUCAU)+;
             ",GPDOCFIS="+cp_ToStrODBC(this.w_GPDOCFIS)+;
             ",GPDOCINT="+cp_ToStrODBC(this.w_GPDOCINT)+;
             ",GPCONTRA="+cp_ToStrODBCNull(this.w_GPCONTRA)+;
             ",GPCNOVAL="+cp_ToStrODBC(this.w_GPCNOVAL)+;
             ",GPTIPINT="+cp_ToStrODBC(this.w_GPTIPINT)+;
             ",GPCODINT="+cp_ToStrODBCNull(this.w_GPCODINT)+;
             ",GPCODAGE="+cp_ToStrODBCNull(this.w_GPCODAGE)+;
             ",GPCODGRU="+cp_ToStrODBCNull(this.w_GPCODGRU)+;
             ",GPDATINI="+cp_ToStrODBC(this.w_GPDATINI)+;
             ",GPDATFIN="+cp_ToStrODBC(this.w_GPDATFIN)+;
             ",GPTIPDOC="+cp_ToStrODBCNull(this.w_GPTIPDOC)+;
             ",GPCODESE="+cp_ToStrODBCNull(this.w_GPCODESE)+;
             ",GPNUMDOC="+cp_ToStrODBC(this.w_GPNUMDOC)+;
             ",GPALFDOC="+cp_ToStrODBC(this.w_GPALFDOC)+;
             ",GPDATDOC="+cp_ToStrODBC(this.w_GPDATDOC)+;
             ",GPPUNPRE="+cp_ToStrODBC(this.w_GPPUNPRE)+;
             ",GPDATCIV="+cp_ToStrODBC(this.w_GPDATCIV)+;
             ",GPDATDIV="+cp_ToStrODBC(this.w_GPDATDIV)+;
             ",GPCAUINT="+cp_ToStrODBCNull(this.w_GPCAUINT)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'GENPREMI')
        i_cWhere = cp_PKFox(i_cTable  ,'GPSERIAL',this.w_GPSERIAL  )
        UPDATE (i_cTable) SET;
              GPFLGCIC=this.w_GPFLGCIC;
             ,GPDESCRI=this.w_GPDESCRI;
             ,GPPERLIQ=this.w_GPPERLIQ;
             ,GPDATREG=this.w_GPDATREG;
             ,GPSTATUS=this.w_GPSTATUS;
             ,GPVALORI=this.w_GPVALORI;
             ,GPINVENT=this.w_GPINVENT;
             ,GPVALCON=this.w_GPVALCON;
             ,GPCODLIS=this.w_GPCODLIS;
             ,GPGRUCAU=this.w_GPGRUCAU;
             ,GPDOCFIS=this.w_GPDOCFIS;
             ,GPDOCINT=this.w_GPDOCINT;
             ,GPCONTRA=this.w_GPCONTRA;
             ,GPCNOVAL=this.w_GPCNOVAL;
             ,GPTIPINT=this.w_GPTIPINT;
             ,GPCODINT=this.w_GPCODINT;
             ,GPCODAGE=this.w_GPCODAGE;
             ,GPCODGRU=this.w_GPCODGRU;
             ,GPDATINI=this.w_GPDATINI;
             ,GPDATFIN=this.w_GPDATFIN;
             ,GPTIPDOC=this.w_GPTIPDOC;
             ,GPCODESE=this.w_GPCODESE;
             ,GPNUMDOC=this.w_GPNUMDOC;
             ,GPALFDOC=this.w_GPALFDOC;
             ,GPDATDOC=this.w_GPDATDOC;
             ,GPPUNPRE=this.w_GPPUNPRE;
             ,GPDATCIV=this.w_GPDATCIV;
             ,GPDATDIV=this.w_GPDATDIV;
             ,GPCAUINT=this.w_GPCAUINT;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSGP_MGP : Saving
      this.GSGP_MGP.ChangeRow(this.cRowID+'      1',0;
             ,this.w_GPSERIAL,"DGPERIAL";
             )
      this.GSGP_MGP.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSGP_MGP : Deleting
    this.GSGP_MGP.ChangeRow(this.cRowID+'      1',0;
           ,this.w_GPSERIAL,"DGPERIAL";
           )
    this.GSGP_MGP.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.GENPREMI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GENPREMI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.GENPREMI_IDX,i_nConn)
      *
      * delete GENPREMI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'GPSERIAL',this.w_GPSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GENPREMI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GENPREMI_IDX,2])
    if i_bUpd
      with this
        if .o_GPSERIAL<>.w_GPSERIAL
            .w_CODAZI = i_CODAZI
          .link_1_1('Full')
        endif
        .DoRTCalc(2,10,.t.)
        if .o_GPCONTRA<>.w_GPCONTRA
            .w_GPVALCON = iif(empty(.w_VALCON), g_PERVAL, .w_VALCON)
          .link_1_11('Full')
        endif
        if .o_GPVALCON<>.w_GPVALCON
            .w_GPCODLIS = SPACE(5)
          .link_1_12('Full')
        endif
        if .o_GPSTATUS<>.w_GPSTATUS.or. .o_CODAZI<>.w_CODAZI
            .w_GPGRUCAU = IIF(.w_GPFLGCIC='V', .w_COGRUCAU,  .w_COGRUCAA)
          .link_1_13('Full')
        endif
        if .o_GPSTATUS<>.w_GPSTATUS
            .w_GPDOCFIS = IIF(.w_GPSTATUS='D','S','N')
        endif
        if .o_GPSTATUS<>.w_GPSTATUS
            .w_GPDOCINT = IIF(.w_GPSTATUS='D','S','N')
        endif
        .DoRTCalc(16,16,.t.)
        if .o_GPCONTRA<>.w_GPCONTRA
            .w_GPCNOVAL = 'N'
        endif
        if .o_GPCONTRA<>.w_GPCONTRA
            .w_GPTIPINT = iif(Empty(.w_GPCONTRA), iif(.pCiclo='V','C','F'), .w_GPTIPINT)
        endif
        if .o_GPCONTRA<>.w_GPCONTRA
            .w_GPTIPINT = iif(Empty(.w_GPCONTRA), iif(.pCiclo='V','C','F'), .w_GPTIPINT)
        endif
        if .o_GPTIPINT<>.w_GPTIPINT.or. .o_GPCONTRA<>.w_GPCONTRA
            .w_GPCODINT = IIF(!.w_GPTIPINT $ 'CF','',.w_GPCODINT)
          .link_1_20('Full')
        endif
        if .o_GPTIPINT<>.w_GPTIPINT.or. .o_GPCONTRA<>.w_GPCONTRA
            .w_GPCODAGE = IIF(.w_GPTIPINT <> 'A','',.w_GPCODAGE)
          .link_1_21('Full')
        endif
        if .o_GPTIPINT<>.w_GPTIPINT.or. .o_GPCONTRA<>.w_GPCONTRA
            .w_GPCODGRU = IIF(.w_GPTIPINT <> 'G','',.w_GPCODGRU)
          .link_1_22('Full')
        endif
        if .o_GPTIPINT<>.w_GPTIPINT.or. .o_GPCONTRA<>.w_GPCONTRA
            .w_GPCODGRU = IIF(.w_GPTIPINT <> 'G','',.w_GPCODGRU)
          .link_1_23('Full')
        endif
        .DoRTCalc(24,25,.t.)
        if .o_GPDATREG<>.w_GPDATREG
            .w_GPDATFIN = .w_GPDATREG
        endif
        if .o_GPDOCFIS<>.w_GPDOCFIS.or. .o_COPRMDOC<>.w_COPRMDOC
            .w_GPTIPDOC = iif(Empty(.w_GPCONTRA) Or .w_GPDOCFIS <>'S','', ICASE(.w_COPRMDOC='F',.w_CAUDF1,.w_COPRMDOC='N',.w_CAUDF2,.w_COPRMDOC='P',.w_CAUDF3,.w_COPRMDOC='C',.w_CAUDF4,.w_COPRMDOC='A',.w_CAUDAG,''))
          .link_1_27('Full')
        endif
        if .o_GPDOCFIS<>.w_GPDOCFIS.or. .o_COPRMDOC<>.w_COPRMDOC
            .w_GPTIPDOC = iif(Empty(.w_GPCONTRA) Or .w_GPDOCFIS <>'S','', ICASE(.w_COPRMDOC='F',.w_CAUDF1,.w_COPRMDOC='N',.w_CAUDF2,.w_COPRMDOC='P',.w_CAUDF3,.w_COPRMDOC='V',.w_CAUDF4,.w_COPRMDOC='A',.w_CAUDAG,''))
          .link_1_28('Full')
        endif
        if .o_GPDOCFIS<>.w_GPDOCFIS.or. .o_COPRMDOC<>.w_COPRMDOC
            .w_GPTIPDOC = iif(Empty(.w_GPCONTRA) Or .w_GPDOCFIS <>'S','', ICASE(.w_COPRMDOC='F',.w_CAUDF1,.w_COPRMDOC='N',.w_CAUDF2,.w_COPRMDOC='P',.w_CAUDF3,.w_COPRMDOC='V',.w_CAUDF4,.w_COPRMDOC='A',.w_CAUDAG,''))
          .link_1_29('Full')
        endif
        if .o_GPDOCFIS<>.w_GPDOCFIS.or. .o_COPRMDOC<>.w_COPRMDOC
            .w_GPTIPDOC = iif(Empty(.w_GPCONTRA) Or .w_GPDOCFIS <>'S','', ICASE(.w_COPRMDOC='F',.w_CAUDF1,.w_COPRMDOC='N',.w_CAUDF2,.w_COPRMDOC='P',.w_CAUDF3,.w_COPRMDOC='V',.w_CAUDF4,.w_COPRMDOC='A',.w_CAUDAG,''))
          .link_1_30('Full')
        endif
        if .o_GPDOCFIS<>.w_GPDOCFIS.or. .o_COPRMDOC<>.w_COPRMDOC
            .w_GPTIPDOC = iif(Empty(.w_GPCONTRA) Or .w_GPDOCFIS <>'S','', ICASE(.w_COPRMDOC='F',.w_CAUDF1,.w_COPRMDOC='N',.w_CAUDF2,.w_COPRMDOC='P',.w_CAUDF3,.w_COPRMDOC='V',.w_CAUDF4,.w_COPRMDOC='A',.w_CAUDAG,''))
          .link_1_31('Full')
        endif
        .DoRTCalc(32,32,.t.)
        if .o_GPDOCFIS<>.w_GPDOCFIS.or. .o_GPCONTRA<>.w_GPCONTRA
            .w_GPNUMDOC = IIF(Empty(.w_GPCONTRA) Or .w_GPDOCFIS <> 'S', 0, .w_GPNUMDOC)
        endif
        if .o_GPDOCFIS<>.w_GPDOCFIS.or. .o_GPCONTRA<>.w_GPCONTRA
            .w_GPALFDOC = ''
        endif
        if .o_GPDOCFIS<>.w_GPDOCFIS
            .w_GPDATDOC = .w_GPDATREG
        endif
        .DoRTCalc(36,36,.t.)
        if .o_GPDOCFIS<>.w_GPDOCFIS.or. .o_GPDATREG<>.w_GPDATREG
            .w_GPDATCIV = .w_GPDATREG
        endif
        if .o_GPDATREG<>.w_GPDATREG
            .w_GPDATDIV = .w_GPDATREG
        endif
        if .o_GPDOCINT<>.w_GPDOCINT
            .w_GPCAUINT = ''
          .link_1_39('Full')
        endif
        if .o_GPDOCINT<>.w_GPDOCINT
            .w_GPCAUINT = ''
          .link_1_40('Full')
        endif
        .DoRTCalc(41,41,.t.)
            .w_MVALFDOC = .w_GPALFDOC
            .w_MVNUMDOC = .w_GPNUMDOC
        .DoRTCalc(44,44,.t.)
            .w_MVANNDOC = STR(YEAR(.w_GPDATDOC), 4, 0)
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
        .DoRTCalc(46,64,.t.)
            .w_MVCODPAG = .w_MVCODPAG
          .link_1_98('Full')
        .DoRTCalc(66,69,.t.)
            .w_COARTFOR = .w_COARTFOR
          .link_1_103('Full')
        .DoRTCalc(71,76,.t.)
            .w_OBTEST = .w_GPDATREG
        if .o_GPCONTRA<>.w_GPCONTRA
            .w_GPTIPDOC = iif(Empty(.w_GPCONTRA) Or .w_GPDOCFIS <>'S','', ICASE(.w_COPRMDOC='F',.w_CAUDF1,.w_COPRMDOC='N',.w_CAUDF2,.w_COPRMDOC='P',.w_CAUDF3,.w_COPRMDOC='V',.w_CAUDF4,.w_COPRMDOC='A',.w_CAUDAG,''))
          .link_1_111('Full')
        endif
        .DoRTCalc(79,80,.t.)
            .w_COPUNART = .w_COPUNART
          .link_1_114('Full')
        .DoRTCalc(82,85,.t.)
            .w_PSSERIAL = .w_GPSERIAL
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEGEPR","i_codazi,w_GPSERIAL")
          .op_GPSERIAL = .w_GPSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(87,96,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_51.Calculate()
    endwith
  return

  proc Calculate_DGXFLDEGZA()
    with this
          * --- Generazione premi
          gsgp_bgp(this;
             )
    endwith
  endproc
  proc Calculate_ZCSZNWLFKM()
    with this
          * --- Gsgp_bcq (calcola date)
          GSGP_BCQ(this;
              ,'AAGP';
             )
    endwith
  endproc
  proc Calculate_CCQNKRUSCY()
    with this
          * --- Generazione premi (temporaneo documenti fiscali)
          gsgp_btm(this;
              ,'FISC';
             )
    endwith
  endproc
  proc Calculate_FHUWEHKZVF()
    with this
          * --- Generazione premi (temporaneo documenti interni)
          gsgp_btm(this;
              ,'INTE';
             )
    endwith
  endproc
  proc Calculate_TLGRXGSSKB()
    with this
          * --- Gsgp_bcq (ripristina i contratti in da elaborare)
          GSGP_BCQ(this;
              ,'DAEL';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oGPSTATUS_1_8.enabled = this.oPgFrm.Page1.oPag.oGPSTATUS_1_8.mCond()
    this.oPgFrm.Page1.oPag.oGPINVENT_1_10.enabled = this.oPgFrm.Page1.oPag.oGPINVENT_1_10.mCond()
    this.oPgFrm.Page1.oPag.oGPVALCON_1_11.enabled = this.oPgFrm.Page1.oPag.oGPVALCON_1_11.mCond()
    this.oPgFrm.Page1.oPag.oGPDOCFIS_1_14.enabled = this.oPgFrm.Page1.oPag.oGPDOCFIS_1_14.mCond()
    this.oPgFrm.Page1.oPag.oGPDOCINT_1_15.enabled = this.oPgFrm.Page1.oPag.oGPDOCINT_1_15.mCond()
    this.oPgFrm.Page1.oPag.oGPCNOVAL_1_17.enabled = this.oPgFrm.Page1.oPag.oGPCNOVAL_1_17.mCond()
    this.oPgFrm.Page1.oPag.oGPTIPINT_1_18.enabled = this.oPgFrm.Page1.oPag.oGPTIPINT_1_18.mCond()
    this.oPgFrm.Page1.oPag.oGPTIPINT_1_19.enabled = this.oPgFrm.Page1.oPag.oGPTIPINT_1_19.mCond()
    this.oPgFrm.Page1.oPag.oGPCODINT_1_20.enabled = this.oPgFrm.Page1.oPag.oGPCODINT_1_20.mCond()
    this.oPgFrm.Page1.oPag.oGPCODAGE_1_21.enabled = this.oPgFrm.Page1.oPag.oGPCODAGE_1_21.mCond()
    this.oPgFrm.Page1.oPag.oGPCODGRU_1_22.enabled = this.oPgFrm.Page1.oPag.oGPCODGRU_1_22.mCond()
    this.oPgFrm.Page1.oPag.oGPCODGRU_1_23.enabled = this.oPgFrm.Page1.oPag.oGPCODGRU_1_23.mCond()
    this.oPgFrm.Page1.oPag.oGPTIPDOC_1_27.enabled = this.oPgFrm.Page1.oPag.oGPTIPDOC_1_27.mCond()
    this.oPgFrm.Page1.oPag.oGPTIPDOC_1_28.enabled = this.oPgFrm.Page1.oPag.oGPTIPDOC_1_28.mCond()
    this.oPgFrm.Page1.oPag.oGPTIPDOC_1_29.enabled = this.oPgFrm.Page1.oPag.oGPTIPDOC_1_29.mCond()
    this.oPgFrm.Page1.oPag.oGPTIPDOC_1_30.enabled = this.oPgFrm.Page1.oPag.oGPTIPDOC_1_30.mCond()
    this.oPgFrm.Page1.oPag.oGPTIPDOC_1_31.enabled = this.oPgFrm.Page1.oPag.oGPTIPDOC_1_31.mCond()
    this.oPgFrm.Page1.oPag.oGPCODESE_1_32.enabled = this.oPgFrm.Page1.oPag.oGPCODESE_1_32.mCond()
    this.oPgFrm.Page1.oPag.oGPALFDOC_1_34.enabled = this.oPgFrm.Page1.oPag.oGPALFDOC_1_34.mCond()
    this.oPgFrm.Page1.oPag.oGPDATDOC_1_35.enabled = this.oPgFrm.Page1.oPag.oGPDATDOC_1_35.mCond()
    this.oPgFrm.Page1.oPag.oGPPUNPRE_1_36.enabled = this.oPgFrm.Page1.oPag.oGPPUNPRE_1_36.mCond()
    this.oPgFrm.Page1.oPag.oGPDATCIV_1_37.enabled = this.oPgFrm.Page1.oPag.oGPDATCIV_1_37.mCond()
    this.oPgFrm.Page1.oPag.oGPDATDIV_1_38.enabled = this.oPgFrm.Page1.oPag.oGPDATDIV_1_38.mCond()
    this.oPgFrm.Page1.oPag.oGPCAUINT_1_39.enabled = this.oPgFrm.Page1.oPag.oGPCAUINT_1_39.mCond()
    this.oPgFrm.Page1.oPag.oGPCAUINT_1_40.enabled = this.oPgFrm.Page1.oPag.oGPCAUINT_1_40.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_52.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_52.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oGPINVENT_1_10.visible=!this.oPgFrm.Page1.oPag.oGPINVENT_1_10.mHide()
    this.oPgFrm.Page1.oPag.oGPTIPINT_1_18.visible=!this.oPgFrm.Page1.oPag.oGPTIPINT_1_18.mHide()
    this.oPgFrm.Page1.oPag.oGPTIPINT_1_19.visible=!this.oPgFrm.Page1.oPag.oGPTIPINT_1_19.mHide()
    this.oPgFrm.Page1.oPag.oGPCODINT_1_20.visible=!this.oPgFrm.Page1.oPag.oGPCODINT_1_20.mHide()
    this.oPgFrm.Page1.oPag.oGPCODAGE_1_21.visible=!this.oPgFrm.Page1.oPag.oGPCODAGE_1_21.mHide()
    this.oPgFrm.Page1.oPag.oGPCODGRU_1_22.visible=!this.oPgFrm.Page1.oPag.oGPCODGRU_1_22.mHide()
    this.oPgFrm.Page1.oPag.oGPCODGRU_1_23.visible=!this.oPgFrm.Page1.oPag.oGPCODGRU_1_23.mHide()
    this.oPgFrm.Page1.oPag.oGPTIPDOC_1_27.visible=!this.oPgFrm.Page1.oPag.oGPTIPDOC_1_27.mHide()
    this.oPgFrm.Page1.oPag.oGPTIPDOC_1_28.visible=!this.oPgFrm.Page1.oPag.oGPTIPDOC_1_28.mHide()
    this.oPgFrm.Page1.oPag.oGPTIPDOC_1_29.visible=!this.oPgFrm.Page1.oPag.oGPTIPDOC_1_29.mHide()
    this.oPgFrm.Page1.oPag.oGPTIPDOC_1_30.visible=!this.oPgFrm.Page1.oPag.oGPTIPDOC_1_30.mHide()
    this.oPgFrm.Page1.oPag.oGPTIPDOC_1_31.visible=!this.oPgFrm.Page1.oPag.oGPTIPDOC_1_31.mHide()
    this.oPgFrm.Page1.oPag.oGPCAUINT_1_39.visible=!this.oPgFrm.Page1.oPag.oGPCAUINT_1_39.mHide()
    this.oPgFrm.Page1.oPag.oGPCAUINT_1_40.visible=!this.oPgFrm.Page1.oPag.oGPCAUINT_1_40.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_52.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_52.mHide()
    this.oPgFrm.Page1.oPag.oPRDATINV_1_66.visible=!this.oPgFrm.Page1.oPag.oPRDATINV_1_66.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_67.visible=!this.oPgFrm.Page1.oPag.oStr_1_67.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_68.visible=!this.oPgFrm.Page1.oPag.oStr_1_68.mHide()
    this.oPgFrm.Page1.oPag.oGPDESINT_1_71.visible=!this.oPgFrm.Page1.oPag.oGPDESINT_1_71.mHide()
    this.oPgFrm.Page1.oPag.oGPDESGRU_1_72.visible=!this.oPgFrm.Page1.oPag.oGPDESGRU_1_72.mHide()
    this.oPgFrm.Page1.oPag.oGPDESAGE_1_73.visible=!this.oPgFrm.Page1.oPag.oGPDESAGE_1_73.mHide()
    this.oPgFrm.Page1.oPag.oGPTIPDOC_1_111.visible=!this.oPgFrm.Page1.oPag.oGPTIPDOC_1_111.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_51.Event(cEvent)
        if lower(cEvent)==lower("Record Inserted")
          .Calculate_DGXFLDEGZA()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("New record") or lower(cEvent)==lower("w_GPPERLIQ Changed")
          .Calculate_ZCSZNWLFKM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("TmpDocFis")
          .Calculate_CCQNKRUSCY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("TmpDocInt")
          .Calculate_FHUWEHKZVF()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Delete end")
          .Calculate_TLGRXGSSKB()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COCAUDF1,COCAUDF2,COCAUDF3,COCAUDF4,COCAUAGE,COMODPAG,COGRUCAU,COGRUCAA,COBASPRO,COCATCON,COCODIVA,COARTFOR,CONOTART,COPUNUNM,COPUNART";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_CODAZI)
            select COCODAZI,COCAUDF1,COCAUDF2,COCAUDF3,COCAUDF4,COCAUAGE,COMODPAG,COGRUCAU,COGRUCAA,COBASPRO,COCATCON,COCODIVA,COARTFOR,CONOTART,COPUNUNM,COPUNART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_CAUDF1 = NVL(_Link_.COCAUDF1,space(5))
      this.w_CAUDF2 = NVL(_Link_.COCAUDF2,space(5))
      this.w_CAUDF3 = NVL(_Link_.COCAUDF3,space(5))
      this.w_CAUDF4 = NVL(_Link_.COCAUDF4,space(5))
      this.w_CAUDAG = NVL(_Link_.COCAUAGE,space(5))
      this.w_MVCODPAG = NVL(_Link_.COMODPAG,space(5))
      this.w_COGRUCAU = NVL(_Link_.COGRUCAU,space(5))
      this.w_COGRUCAA = NVL(_Link_.COGRUCAA,space(5))
      this.w_COBASPRO = NVL(_Link_.COBASPRO,space(1))
      this.w_MVCAUCON = NVL(_Link_.COCATCON,space(5))
      this.w_MVCODIVA = NVL(_Link_.COCODIVA,space(5))
      this.w_COARTFOR = NVL(_Link_.COARTFOR,space(20))
      this.w_CONOTART = NVL(_Link_.CONOTART,space(20))
      this.w_COPUNUNM = NVL(_Link_.COPUNUNM,space(3))
      this.w_COPUNART = NVL(_Link_.COPUNART,space(20))
      this.w_COBASPRO = NVL(_Link_.COBASPRO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_CAUDF1 = space(5)
      this.w_CAUDF2 = space(5)
      this.w_CAUDF3 = space(5)
      this.w_CAUDF4 = space(5)
      this.w_CAUDAG = space(5)
      this.w_MVCODPAG = space(5)
      this.w_COGRUCAU = space(5)
      this.w_COGRUCAA = space(5)
      this.w_COBASPRO = space(1)
      this.w_MVCAUCON = space(5)
      this.w_MVCODIVA = space(5)
      this.w_COARTFOR = space(20)
      this.w_CONOTART = space(20)
      this.w_COPUNUNM = space(3)
      this.w_COPUNART = space(20)
      this.w_COBASPRO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPINVENT
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.INVENTAR_IDX,3]
    i_lTable = "INVENTAR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2], .t., this.INVENTAR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPINVENT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsma_ain',True,'INVENTAR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" INNUMINV like "+cp_ToStrODBC(trim(this.w_GPINVENT)+"%");
                   +" and INCODESE="+cp_ToStrODBC(this.w_MVCODESE);

          i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by INCODESE,INNUMINV","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'INCODESE',this.w_MVCODESE;
                     ,'INNUMINV',trim(this.w_GPINVENT))
          select INCODESE,INNUMINV,INDATINV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by INCODESE,INNUMINV into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPINVENT)==trim(_Link_.INNUMINV) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPINVENT) and !this.bDontReportError
            deferred_cp_zoom('INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(oSource.parent,'oGPINVENT_1_10'),i_cWhere,'gsma_ain',"Elenco inventari",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MVCODESE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                     +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(oSource.xKey(2));
                     +" and INCODESE="+cp_ToStrODBC(this.w_MVCODESE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',oSource.xKey(1);
                       ,'INNUMINV',oSource.xKey(2))
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPINVENT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select INCODESE,INNUMINV,INDATINV";
                   +" from "+i_cTable+" "+i_lTable+" where INNUMINV="+cp_ToStrODBC(this.w_GPINVENT);
                   +" and INCODESE="+cp_ToStrODBC(this.w_MVCODESE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'INCODESE',this.w_MVCODESE;
                       ,'INNUMINV',this.w_GPINVENT)
            select INCODESE,INNUMINV,INDATINV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPINVENT = NVL(_Link_.INNUMINV,space(6))
      this.w_PRDATINV = NVL(cp_ToDate(_Link_.INDATINV),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_GPINVENT = space(6)
      endif
      this.w_PRDATINV = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_PRDATINV <= .w_GPDATREG
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GPINVENT = space(6)
        this.w_PRDATINV = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.INVENTAR_IDX,2])+'\'+cp_ToStr(_Link_.INCODESE,1)+'\'+cp_ToStr(_Link_.INNUMINV,1)
      cp_ShowWarn(i_cKey,this.INVENTAR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPINVENT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPVALCON
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPVALCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_GPVALCON)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_GPVALCON))
          select VACODVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPVALCON)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPVALCON) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oGPVALCON_1_11'),i_cWhere,'GSAR_AVL',"Elenco valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPVALCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_GPVALCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_GPVALCON)
            select VACODVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPVALCON = NVL(_Link_.VACODVAL,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_GPVALCON = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPVALCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPCODLIS
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_GPCODLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSFLGCIC,LSFLSTAT,LSIVALIS,LSVALLIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_GPCODLIS))
          select LSCODLIS,LSFLGCIC,LSFLSTAT,LSIVALIS,LSVALLIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCODLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSFLGCIC like "+cp_ToStrODBC(trim(this.w_GPCODLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSFLGCIC,LSFLSTAT,LSIVALIS,LSVALLIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSFLGCIC like "+cp_ToStr(trim(this.w_GPCODLIS)+"%");

            select LSCODLIS,LSFLGCIC,LSFLSTAT,LSIVALIS,LSVALLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_GPCODLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oGPCODLIS_1_12'),i_cWhere,'',"Elenco listini",'GSMAASAV.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSFLGCIC,LSFLSTAT,LSIVALIS,LSVALLIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSFLGCIC,LSFLSTAT,LSIVALIS,LSVALLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSFLGCIC,LSFLSTAT,LSIVALIS,LSVALLIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_GPCODLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_GPCODLIS)
            select LSCODLIS,LSFLGCIC,LSFLSTAT,LSIVALIS,LSVALLIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_LSFLGCIC = NVL(_Link_.LSFLGCIC,space(1))
      this.w_LSFLSTAT = NVL(_Link_.LSFLSTAT,space(1))
      this.w_LORDO = NVL(_Link_.LSIVALIS,space(1))
      this.w_LSVALLIS = NVL(_Link_.LSVALLIS,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODLIS = space(5)
      endif
      this.w_LSFLGCIC = space(1)
      this.w_LSFLSTAT = space(1)
      this.w_LORDO = space(1)
      this.w_LSVALLIS = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_12.LSCODLIS as LSCODLIS112"+ ",link_1_12.LSFLGCIC as LSFLGCIC112"+ ",link_1_12.LSFLSTAT as LSFLSTAT112"+ ",link_1_12.LSIVALIS as LSIVALIS112"+ ",link_1_12.LSVALLIS as LSVALLIS112"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_12 on GENPREMI.GPCODLIS=link_1_12.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_12"
          i_cKey=i_cKey+'+" and GENPREMI.GPCODLIS=link_1_12.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GPGRUCAU
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CCOMCMAG_IDX,3]
    i_lTable = "CCOMCMAG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CCOMCMAG_IDX,2], .t., this.CCOMCMAG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CCOMCMAG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPGRUCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CCOMCMAG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODICE like "+cp_ToStrODBC(trim(this.w_GPGRUCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSFLSIMU,CSFLGCIC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODICE',trim(this.w_GPGRUCAU))
          select CSCODICE,CSDESCRI,CSFLSIMU,CSFLGCIC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPGRUCAU)==trim(_Link_.CSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPGRUCAU) and !this.bDontReportError
            deferred_cp_zoom('CCOMCMAG','*','CSCODICE',cp_AbsName(oSource.parent,'oGPGRUCAU_1_13'),i_cWhere,'',"Gruppi di causali",'GSGP_AGP.CCOMCMAG_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSFLSIMU,CSFLGCIC";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',oSource.xKey(1))
            select CSCODICE,CSDESCRI,CSFLSIMU,CSFLGCIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPGRUCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSFLSIMU,CSFLGCIC";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(this.w_GPGRUCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_GPGRUCAU)
            select CSCODICE,CSDESCRI,CSFLSIMU,CSFLGCIC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPGRUCAU = NVL(_Link_.CSCODICE,space(5))
      this.w_DESGCAU = NVL(_Link_.CSDESCRI,space(35))
      this.w_FLSIMU = NVL(_Link_.CSFLSIMU,space(1))
      this.w_CSFLGCIC = NVL(_Link_.CSFLGCIC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GPGRUCAU = space(5)
      endif
      this.w_DESGCAU = space(35)
      this.w_FLSIMU = space(1)
      this.w_CSFLGCIC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_CSFLGCIC='E' Or .w_CSFLGCIC = .w_GPFLGCIC) And (.w_GPSTATUS='S'  Or .w_GPSTATUS='D' And .w_FLSIMU <> 'S')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice gruppo non valido oppure generazione definitiva e gruppo di causali di simulazione")
        endif
        this.w_GPGRUCAU = space(5)
        this.w_DESGCAU = space(35)
        this.w_FLSIMU = space(1)
        this.w_CSFLGCIC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CCOMCMAG_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)
      cp_ShowWarn(i_cKey,this.CCOMCMAG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPGRUCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPCONTRA
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CCOMMAST_IDX,3]
    i_lTable = "CCOMMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CCOMMAST_IDX,2], .t., this.CCOMMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CCOMMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCONTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CCOMMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" COCODICE like "+cp_ToStrODBC(trim(this.w_GPCONTRA)+"%");

          i_ret=cp_SQL(i_nConn,"select COCODICE,CODATREG,COFLGTIP,COFLGCIC,COCODVAL,COPRMDOC,COPRMLIQ,COTIPINT,COCODINT,COCODGRU,COCODAGE,COFLGSTA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by COCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'COCODICE',trim(this.w_GPCONTRA))
          select COCODICE,CODATREG,COFLGTIP,COFLGCIC,COCODVAL,COPRMDOC,COPRMLIQ,COTIPINT,COCODINT,COCODGRU,COCODAGE,COFLGSTA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by COCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCONTRA)==trim(_Link_.COCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCONTRA) and !this.bDontReportError
            deferred_cp_zoom('CCOMMAST','*','COCODICE',cp_AbsName(oSource.parent,'oGPCONTRA_1_16'),i_cWhere,'',"Contratti obiettivo",'GSGP_AGP.CCOMMAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODICE,CODATREG,COFLGTIP,COFLGCIC,COCODVAL,COPRMDOC,COPRMLIQ,COTIPINT,COCODINT,COCODGRU,COCODAGE,COFLGSTA";
                     +" from "+i_cTable+" "+i_lTable+" where COCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODICE',oSource.xKey(1))
            select COCODICE,CODATREG,COFLGTIP,COFLGCIC,COCODVAL,COPRMDOC,COPRMLIQ,COTIPINT,COCODINT,COCODGRU,COCODAGE,COFLGSTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCONTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODICE,CODATREG,COFLGTIP,COFLGCIC,COCODVAL,COPRMDOC,COPRMLIQ,COTIPINT,COCODINT,COCODGRU,COCODAGE,COFLGSTA";
                   +" from "+i_cTable+" "+i_lTable+" where COCODICE="+cp_ToStrODBC(this.w_GPCONTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODICE',this.w_GPCONTRA)
            select COCODICE,CODATREG,COFLGTIP,COFLGCIC,COCODVAL,COPRMDOC,COPRMLIQ,COTIPINT,COCODINT,COCODGRU,COCODAGE,COFLGSTA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCONTRA = NVL(_Link_.COCODICE,space(15))
      this.w_DATACON = NVL(cp_ToDate(_Link_.CODATREG),ctod("  /  /  "))
      this.w_COFLGTIP = NVL(_Link_.COFLGTIP,space(1))
      this.w_COFLGCIC = NVL(_Link_.COFLGCIC,space(1))
      this.w_VALCON = NVL(_Link_.COCODVAL,space(1))
      this.w_COPRMDOC = NVL(_Link_.COPRMDOC,space(1))
      this.w_COPRMLIQ = NVL(_Link_.COPRMLIQ,space(1))
      this.w_GPTIPINT = NVL(_Link_.COTIPINT,space(1))
      this.w_GPCODINT = NVL(_Link_.COCODINT,space(15))
      this.w_GPCODGRU = NVL(_Link_.COCODGRU,space(15))
      this.w_GPCODAGE = NVL(_Link_.COCODAGE,space(5))
      this.w_COFLGSTA = NVL(_Link_.COFLGSTA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GPCONTRA = space(15)
      endif
      this.w_DATACON = ctod("  /  /  ")
      this.w_COFLGTIP = space(1)
      this.w_COFLGCIC = space(1)
      this.w_VALCON = space(1)
      this.w_COPRMDOC = space(1)
      this.w_COPRMLIQ = space(1)
      this.w_GPTIPINT = space(1)
      this.w_GPCODINT = space(15)
      this.w_GPCODGRU = space(15)
      this.w_GPCODAGE = space(5)
      this.w_COFLGSTA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_COFLGSTA='D' And .w_COFLGCIC=.pCiclo And .w_COFLGTIP='O' And (.w_GPPERLIQ = 'A' And .w_COPRMLIQ $ 'FA' Or .w_GPPERLIQ <> 'A' And .w_GPPERLIQ=.w_COPRMLIQ)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GPCONTRA = space(15)
        this.w_DATACON = ctod("  /  /  ")
        this.w_COFLGTIP = space(1)
        this.w_COFLGCIC = space(1)
        this.w_VALCON = space(1)
        this.w_COPRMDOC = space(1)
        this.w_COPRMLIQ = space(1)
        this.w_GPTIPINT = space(1)
        this.w_GPCODINT = space(15)
        this.w_GPCODGRU = space(15)
        this.w_GPCODAGE = space(5)
        this.w_COFLGSTA = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CCOMMAST_IDX,2])+'\'+cp_ToStr(_Link_.COCODICE,1)
      cp_ShowWarn(i_cKey,this.CCOMMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCONTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 12 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CCOMMAST_IDX,3] and i_nFlds+12<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CCOMMAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.COCODICE as COCODICE116"+ ",link_1_16.CODATREG as CODATREG116"+ ",link_1_16.COFLGTIP as COFLGTIP116"+ ",link_1_16.COFLGCIC as COFLGCIC116"+ ",link_1_16.COCODVAL as COCODVAL116"+ ",link_1_16.COPRMDOC as COPRMDOC116"+ ",link_1_16.COPRMLIQ as COPRMLIQ116"+ ",link_1_16.COTIPINT as COTIPINT116"+ ",link_1_16.COCODINT as COCODINT116"+ ",link_1_16.COCODGRU as COCODGRU116"+ ",link_1_16.COCODAGE as COCODAGE116"+ ",link_1_16.COFLGSTA as COFLGSTA116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on GENPREMI.GPCONTRA=link_1_16.COCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+12
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and GENPREMI.GPCONTRA=link_1_16.COCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+12
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GPCODINT
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_GPCODINT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_GPTIPINT);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_GPTIPINT;
                     ,'ANCODICE',trim(this.w_GPCODINT))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCODINT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCODINT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oGPCODINT_1_20'),i_cWhere,'GSAR_BZC',"Elenco clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_GPTIPINT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_GPTIPINT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_GPCODINT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_GPTIPINT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_GPTIPINT;
                       ,'ANCODICE',this.w_GPCODINT)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODINT = NVL(_Link_.ANCODICE,space(15))
      this.w_GPDESINT = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODINT = space(15)
      endif
      this.w_GPDESINT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.ANCODICE as ANCODICE120"+ ",link_1_20.ANDESCRI as ANDESCRI120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on GENPREMI.GPCODINT=link_1_20.ANCODICE"+" and GENPREMI.GPTIPINT=link_1_20.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and GENPREMI.GPCODINT=link_1_20.ANCODICE(+)"'+'+" and GENPREMI.GPTIPINT=link_1_20.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GPCODAGE
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_GPCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_GPCODAGE))
          select AGCODAGE,AGDESAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oGPCODAGE_1_21'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_GPCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_GPCODAGE)
            select AGCODAGE,AGDESAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_GPDESAGE = NVL(_Link_.AGDESAGE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODAGE = space(5)
      endif
      this.w_GPDESAGE = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_21.AGCODAGE as AGCODAGE121"+ ",link_1_21.AGDESAGE as AGDESAGE121"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_21 on GENPREMI.GPCODAGE=link_1_21.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_21"
          i_cKey=i_cKey+'+" and GENPREMI.GPCODAGE=link_1_21.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GPCODGRU
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUPPACQ_IDX,3]
    i_lTable = "GRUPPACQ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2], .t., this.GRUPPACQ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUPPACQ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GACODICE like "+cp_ToStrODBC(trim(this.w_GPCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GACODICE,GADESGRP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GACODICE',trim(this.w_GPCODGRU))
          select GACODICE,GADESGRP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCODGRU)==trim(_Link_.GACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRUPPACQ','*','GACODICE',cp_AbsName(oSource.parent,'oGPCODGRU_1_22'),i_cWhere,'',"Gruppi di acquisto",'GSGPVMGA.GRUPPACQ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GACODICE,GADESGRP";
                     +" from "+i_cTable+" "+i_lTable+" where GACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GACODICE',oSource.xKey(1))
            select GACODICE,GADESGRP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GACODICE,GADESGRP";
                   +" from "+i_cTable+" "+i_lTable+" where GACODICE="+cp_ToStrODBC(this.w_GPCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GACODICE',this.w_GPCODGRU)
            select GACODICE,GADESGRP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODGRU = NVL(_Link_.GACODICE,space(15))
      this.w_GPDESGRU = NVL(_Link_.GADESGRP,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODGRU = space(15)
      endif
      this.w_GPDESGRU = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2])+'\'+cp_ToStr(_Link_.GACODICE,1)
      cp_ShowWarn(i_cKey,this.GRUPPACQ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPCODGRU
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUPPACQ_IDX,3]
    i_lTable = "GRUPPACQ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2], .t., this.GRUPPACQ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUPPACQ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GACODICE like "+cp_ToStrODBC(trim(this.w_GPCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GACODICE,GADESGRP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GACODICE',trim(this.w_GPCODGRU))
          select GACODICE,GADESGRP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCODGRU)==trim(_Link_.GACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRUPPACQ','*','GACODICE',cp_AbsName(oSource.parent,'oGPCODGRU_1_23'),i_cWhere,'',"Gruppi di acquisto",'GSGPAMGA.GRUPPACQ_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GACODICE,GADESGRP";
                     +" from "+i_cTable+" "+i_lTable+" where GACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GACODICE',oSource.xKey(1))
            select GACODICE,GADESGRP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GACODICE,GADESGRP";
                   +" from "+i_cTable+" "+i_lTable+" where GACODICE="+cp_ToStrODBC(this.w_GPCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GACODICE',this.w_GPCODGRU)
            select GACODICE,GADESGRP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODGRU = NVL(_Link_.GACODICE,space(15))
      this.w_GPDESGRU = NVL(_Link_.GADESGRP,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODGRU = space(15)
      endif
      this.w_GPDESGRU = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2])+'\'+cp_ToStr(_Link_.GACODICE,1)
      cp_ShowWarn(i_cKey,this.GRUPPACQ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPTIPDOC
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_GPTIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_GPTIPDOC))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPTIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPTIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oGPTIPDOC_1_27'),i_cWhere,'GSVE_ATD',"Causali documenti",'GSGP1DOC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_GPTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_GPTIPDOC)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESORI = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLVEAC1 = NVL(_Link_.TDFLVEAC,space(1))
      this.w_CLADOC = NVL(_Link_.TDCATDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_GPTIPDOC = space(5)
      endif
      this.w_DESORI = space(35)
      this.w_FLVEAC1 = space(1)
      this.w_CLADOC = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CLADOC = 'FA' AND .w_FLVEAC1='V'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente")
        endif
        this.w_GPTIPDOC = space(5)
        this.w_DESORI = space(35)
        this.w_FLVEAC1 = space(1)
        this.w_CLADOC = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPTIPDOC
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_GPTIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_GPTIPDOC))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPTIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPTIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oGPTIPDOC_1_28'),i_cWhere,'GSVE_ATD',"Causali documenti",'GSGP2DOC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_GPTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_GPTIPDOC)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESORI = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLVEAC1 = NVL(_Link_.TDFLVEAC,space(1))
      this.w_CLADOC = NVL(_Link_.TDCATDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_GPTIPDOC = space(5)
      endif
      this.w_DESORI = space(35)
      this.w_FLVEAC1 = space(1)
      this.w_CLADOC = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CLADOC = 'NC' AND .w_FLVEAC1 = 'V'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente")
        endif
        this.w_GPTIPDOC = space(5)
        this.w_DESORI = space(35)
        this.w_FLVEAC1 = space(1)
        this.w_CLADOC = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPTIPDOC
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAC_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_GPTIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_GPTIPDOC))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPTIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPTIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oGPTIPDOC_1_29'),i_cWhere,'GSAC_ATD',"Causali documenti",'GSGP3DOC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_GPTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_GPTIPDOC)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESORI = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLVEAC1 = NVL(_Link_.TDFLVEAC,space(1))
      this.w_CLADOC = NVL(_Link_.TDCATDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_GPTIPDOC = space(5)
      endif
      this.w_DESORI = space(35)
      this.w_FLVEAC1 = space(1)
      this.w_CLADOC = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CLADOC = 'DI' AND .w_FLVEAC1 = 'A'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente")
        endif
        this.w_GPTIPDOC = space(5)
        this.w_DESORI = space(35)
        this.w_FLVEAC1 = space(1)
        this.w_CLADOC = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPTIPDOC
  func Link_1_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAC_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_GPTIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_GPTIPDOC))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPTIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPTIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oGPTIPDOC_1_30'),i_cWhere,'GSAC_ATD',"Causali documenti",'GSGP4DOC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_GPTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_GPTIPDOC)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESORI = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLVEAC1 = NVL(_Link_.TDFLVEAC,space(1))
      this.w_CLADOC = NVL(_Link_.TDCATDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_GPTIPDOC = space(5)
      endif
      this.w_DESORI = space(35)
      this.w_FLVEAC1 = space(1)
      this.w_CLADOC = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CLADOC = 'DI'  And .w_FLVEAC1='A'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente")
        endif
        this.w_GPTIPDOC = space(5)
        this.w_DESORI = space(35)
        this.w_FLVEAC1 = space(1)
        this.w_CLADOC = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPTIPDOC
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAC_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_GPTIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_GPTIPDOC))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPTIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPTIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oGPTIPDOC_1_31'),i_cWhere,'GSAC_ATD',"Causali documenti",'GSGPADOC.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_GPTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_GPTIPDOC)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESORI = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLVEAC1 = NVL(_Link_.TDFLVEAC,space(1))
      this.w_CLADOC = NVL(_Link_.TDCATDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_GPTIPDOC = space(5)
      endif
      this.w_DESORI = space(35)
      this.w_FLVEAC1 = space(1)
      this.w_CLADOC = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CLADOC = 'DI' AND .w_FLVEAC1='A'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente")
        endif
        this.w_GPTIPDOC = space(5)
        this.w_DESORI = space(35)
        this.w_FLVEAC1 = space(1)
        this.w_CLADOC = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPCODESE
  func Link_1_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_GPCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_GPCODESE))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oGPCODESE_1_32'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_GPCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_GPCODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_MVVALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODESE = space(4)
      endif
      this.w_MVVALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPCAUINT
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCAUINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_GPCAUINT)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_GPCAUINT))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCAUINT)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCAUINT) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oGPCAUINT_1_39'),i_cWhere,'GSVE_ATD',"Causali documenti",'GSGP1AGP.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCAUINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_GPCAUINT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_GPCAUINT)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCAUINT = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESINT = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLVEAC2 = NVL(_Link_.TDFLVEAC,space(1))
      this.w_CLADOC2 = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLINTE = NVL(_Link_.TDFLINTE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GPCAUINT = space(5)
      endif
      this.w_DESINT = space(35)
      this.w_FLVEAC2 = space(1)
      this.w_CLADOC2 = space(2)
      this.w_FLINTE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CLADOC2 = 'DI'  And .w_FLINTE <> 'N' And (.w_GPTIPINT <> 'A' Or .w_GPTIPINT='A' And .w_FLINTE = 'F')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente")
        endif
        this.w_GPCAUINT = space(5)
        this.w_DESINT = space(35)
        this.w_FLVEAC2 = space(1)
        this.w_CLADOC2 = space(2)
        this.w_FLINTE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCAUINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPCAUINT
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCAUINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAC_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_GPCAUINT)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDPROVVI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_GPCAUINT))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDPROVVI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCAUINT)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCAUINT) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oGPCAUINT_1_40'),i_cWhere,'GSAC_ATD',"Causali documenti",'GSGP1AGP.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDPROVVI";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDPROVVI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCAUINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDPROVVI";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_GPCAUINT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_GPCAUINT)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDFLINTE,TDPROVVI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCAUINT = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESINT = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLVEAC2 = NVL(_Link_.TDFLVEAC,space(1))
      this.w_CLADOC2 = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLINTE = NVL(_Link_.TDFLINTE,space(1))
      this.w_PROVVINT = NVL(_Link_.TDPROVVI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GPCAUINT = space(5)
      endif
      this.w_DESINT = space(35)
      this.w_FLVEAC2 = space(1)
      this.w_CLADOC2 = space(2)
      this.w_FLINTE = space(1)
      this.w_PROVVINT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CLADOC2 = 'DI'  And .w_FLINTE <> 'N' And (.w_GPTIPINT <> 'A' Or .w_GPTIPINT='A' And .w_FLINTE = 'F')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale documento inesistente o incongruente")
        endif
        this.w_GPCAUINT = space(5)
        this.w_DESINT = space(35)
        this.w_FLVEAC2 = space(1)
        this.w_CLADOC2 = space(2)
        this.w_FLINTE = space(1)
        this.w_PROVVINT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCAUINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MVCODPAG
  func Link_1_98(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MVCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MVCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PASCONTO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_MVCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_MVCODPAG)
            select PACODICE,PASCONTO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MVCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_MVSCOPAG = NVL(_Link_.PASCONTO,0)
    else
      if i_cCtrl<>'Load'
        this.w_MVCODPAG = space(5)
      endif
      this.w_MVSCOPAG = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MVCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COARTFOR
  func Link_1_103(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COARTFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COARTFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARUNMIS1";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_COARTFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_COARTFOR)
            select ARCODART,ARDESART,ARUNMIS1;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COARTFOR = NVL(_Link_.ARCODART,space(20))
      this.w_DESFOR = NVL(_Link_.ARDESART,space(40))
      this.w_UMFORF = NVL(_Link_.ARUNMIS1,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_COARTFOR = space(20)
      endif
      this.w_DESFOR = space(40)
      this.w_UMFORF = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COARTFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPTIPDOC
  func Link_1_111(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDALFDOC,TDPROVVI";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_GPTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_GPTIPDOC)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDCATDOC,TDALFDOC,TDPROVVI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESORI = NVL(_Link_.TDDESDOC,space(35))
      this.w_FLVEAC1 = NVL(_Link_.TDFLVEAC,space(1))
      this.w_CLADOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_GPALFDOC = NVL(_Link_.TDALFDOC,space(10))
      this.w_PROVVFIS = NVL(_Link_.TDPROVVI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GPTIPDOC = space(5)
      endif
      this.w_DESORI = space(35)
      this.w_FLVEAC1 = space(1)
      this.w_CLADOC = space(2)
      this.w_GPALFDOC = space(10)
      this.w_PROVVFIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COPUNART
  func Link_1_114(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COPUNART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COPUNART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_COPUNART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_COPUNART)
            select ARCODART,ARDESART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COPUNART = NVL(_Link_.ARCODART,space(20))
      this.w_DESFM = NVL(_Link_.ARDESART,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COPUNART = space(20)
      endif
      this.w_DESFM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COPUNART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oGPSERIAL_1_4.value==this.w_GPSERIAL)
      this.oPgFrm.Page1.oPag.oGPSERIAL_1_4.value=this.w_GPSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDESCRI_1_5.value==this.w_GPDESCRI)
      this.oPgFrm.Page1.oPag.oGPDESCRI_1_5.value=this.w_GPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oGPPERLIQ_1_6.RadioValue()==this.w_GPPERLIQ)
      this.oPgFrm.Page1.oPag.oGPPERLIQ_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDATREG_1_7.value==this.w_GPDATREG)
      this.oPgFrm.Page1.oPag.oGPDATREG_1_7.value=this.w_GPDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oGPSTATUS_1_8.RadioValue()==this.w_GPSTATUS)
      this.oPgFrm.Page1.oPag.oGPSTATUS_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGPVALORI_1_9.RadioValue()==this.w_GPVALORI)
      this.oPgFrm.Page1.oPag.oGPVALORI_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGPINVENT_1_10.value==this.w_GPINVENT)
      this.oPgFrm.Page1.oPag.oGPINVENT_1_10.value=this.w_GPINVENT
    endif
    if not(this.oPgFrm.Page1.oPag.oGPVALCON_1_11.value==this.w_GPVALCON)
      this.oPgFrm.Page1.oPag.oGPVALCON_1_11.value=this.w_GPVALCON
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCODLIS_1_12.value==this.w_GPCODLIS)
      this.oPgFrm.Page1.oPag.oGPCODLIS_1_12.value=this.w_GPCODLIS
    endif
    if not(this.oPgFrm.Page1.oPag.oGPGRUCAU_1_13.value==this.w_GPGRUCAU)
      this.oPgFrm.Page1.oPag.oGPGRUCAU_1_13.value=this.w_GPGRUCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDOCFIS_1_14.RadioValue()==this.w_GPDOCFIS)
      this.oPgFrm.Page1.oPag.oGPDOCFIS_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDOCINT_1_15.RadioValue()==this.w_GPDOCINT)
      this.oPgFrm.Page1.oPag.oGPDOCINT_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCONTRA_1_16.value==this.w_GPCONTRA)
      this.oPgFrm.Page1.oPag.oGPCONTRA_1_16.value=this.w_GPCONTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCNOVAL_1_17.RadioValue()==this.w_GPCNOVAL)
      this.oPgFrm.Page1.oPag.oGPCNOVAL_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGPTIPINT_1_18.RadioValue()==this.w_GPTIPINT)
      this.oPgFrm.Page1.oPag.oGPTIPINT_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGPTIPINT_1_19.RadioValue()==this.w_GPTIPINT)
      this.oPgFrm.Page1.oPag.oGPTIPINT_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCODINT_1_20.value==this.w_GPCODINT)
      this.oPgFrm.Page1.oPag.oGPCODINT_1_20.value=this.w_GPCODINT
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCODAGE_1_21.value==this.w_GPCODAGE)
      this.oPgFrm.Page1.oPag.oGPCODAGE_1_21.value=this.w_GPCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCODGRU_1_22.value==this.w_GPCODGRU)
      this.oPgFrm.Page1.oPag.oGPCODGRU_1_22.value=this.w_GPCODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCODGRU_1_23.value==this.w_GPCODGRU)
      this.oPgFrm.Page1.oPag.oGPCODGRU_1_23.value=this.w_GPCODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDATINI_1_25.value==this.w_GPDATINI)
      this.oPgFrm.Page1.oPag.oGPDATINI_1_25.value=this.w_GPDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDATFIN_1_26.value==this.w_GPDATFIN)
      this.oPgFrm.Page1.oPag.oGPDATFIN_1_26.value=this.w_GPDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGPTIPDOC_1_27.value==this.w_GPTIPDOC)
      this.oPgFrm.Page1.oPag.oGPTIPDOC_1_27.value=this.w_GPTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oGPTIPDOC_1_28.value==this.w_GPTIPDOC)
      this.oPgFrm.Page1.oPag.oGPTIPDOC_1_28.value=this.w_GPTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oGPTIPDOC_1_29.value==this.w_GPTIPDOC)
      this.oPgFrm.Page1.oPag.oGPTIPDOC_1_29.value=this.w_GPTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oGPTIPDOC_1_30.value==this.w_GPTIPDOC)
      this.oPgFrm.Page1.oPag.oGPTIPDOC_1_30.value=this.w_GPTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oGPTIPDOC_1_31.value==this.w_GPTIPDOC)
      this.oPgFrm.Page1.oPag.oGPTIPDOC_1_31.value=this.w_GPTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCODESE_1_32.value==this.w_GPCODESE)
      this.oPgFrm.Page1.oPag.oGPCODESE_1_32.value=this.w_GPCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oGPNUMDOC_1_33.value==this.w_GPNUMDOC)
      this.oPgFrm.Page1.oPag.oGPNUMDOC_1_33.value=this.w_GPNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oGPALFDOC_1_34.value==this.w_GPALFDOC)
      this.oPgFrm.Page1.oPag.oGPALFDOC_1_34.value=this.w_GPALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDATDOC_1_35.value==this.w_GPDATDOC)
      this.oPgFrm.Page1.oPag.oGPDATDOC_1_35.value=this.w_GPDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oGPPUNPRE_1_36.RadioValue()==this.w_GPPUNPRE)
      this.oPgFrm.Page1.oPag.oGPPUNPRE_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDATCIV_1_37.value==this.w_GPDATCIV)
      this.oPgFrm.Page1.oPag.oGPDATCIV_1_37.value=this.w_GPDATCIV
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDATDIV_1_38.value==this.w_GPDATDIV)
      this.oPgFrm.Page1.oPag.oGPDATDIV_1_38.value=this.w_GPDATDIV
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCAUINT_1_39.value==this.w_GPCAUINT)
      this.oPgFrm.Page1.oPag.oGPCAUINT_1_39.value=this.w_GPCAUINT
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCAUINT_1_40.value==this.w_GPCAUINT)
      this.oPgFrm.Page1.oPag.oGPCAUINT_1_40.value=this.w_GPCAUINT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESORI_1_44.value==this.w_DESORI)
      this.oPgFrm.Page1.oPag.oDESORI_1_44.value=this.w_DESORI
    endif
    if not(this.oPgFrm.Page1.oPag.oPRDATINV_1_66.value==this.w_PRDATINV)
      this.oPgFrm.Page1.oPag.oPRDATINV_1_66.value=this.w_PRDATINV
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDESINT_1_71.value==this.w_GPDESINT)
      this.oPgFrm.Page1.oPag.oGPDESINT_1_71.value=this.w_GPDESINT
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDESGRU_1_72.value==this.w_GPDESGRU)
      this.oPgFrm.Page1.oPag.oGPDESGRU_1_72.value=this.w_GPDESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDESAGE_1_73.value==this.w_GPDESAGE)
      this.oPgFrm.Page1.oPag.oGPDESAGE_1_73.value=this.w_GPDESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATACON_1_76.value==this.w_DATACON)
      this.oPgFrm.Page1.oPag.oDATACON_1_76.value=this.w_DATACON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINT_1_83.value==this.w_DESINT)
      this.oPgFrm.Page1.oPag.oDESINT_1_83.value=this.w_DESINT
    endif
    if not(this.oPgFrm.Page1.oPag.oGPTIPDOC_1_111.value==this.w_GPTIPDOC)
      this.oPgFrm.Page1.oPag.oGPTIPDOC_1_111.value=this.w_GPTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGCAU_1_128.value==this.w_DESGCAU)
      this.oPgFrm.Page1.oPag.oDESGCAU_1_128.value=this.w_DESGCAU
    endif
    cp_SetControlsValueExtFlds(this,'GENPREMI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_GPSERIAL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPSERIAL_1_4.SetFocus()
            i_bnoObbl = !empty(.w_GPSERIAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GPDATREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPDATREG_1_7.SetFocus()
            i_bnoObbl = !empty(.w_GPDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GPINVENT)) or not(.w_PRDATINV <= .w_GPDATREG))  and not(.w_GPVALORI ='L')  and (.w_GPVALORI <> 'L')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPINVENT_1_10.SetFocus()
            i_bnoObbl = !empty(.w_GPINVENT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GPVALCON))  and (Empty(.w_GPCONTRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPVALCON_1_11.SetFocus()
            i_bnoObbl = !empty(.w_GPVALCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GPCODLIS))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPCODLIS_1_12.SetFocus()
            i_bnoObbl = !empty(.w_GPCODLIS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non valido oppure valuta listino incongruente")
          case   ((empty(.w_GPGRUCAU)) or not((.w_CSFLGCIC='E' Or .w_CSFLGCIC = .w_GPFLGCIC) And (.w_GPSTATUS='S'  Or .w_GPSTATUS='D' And .w_FLSIMU <> 'S')))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPGRUCAU_1_13.SetFocus()
            i_bnoObbl = !empty(.w_GPGRUCAU)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice gruppo non valido oppure generazione definitiva e gruppo di causali di simulazione")
          case   not(.w_COFLGSTA='D' And .w_COFLGCIC=.pCiclo And .w_COFLGTIP='O' And (.w_GPPERLIQ = 'A' And .w_COPRMLIQ $ 'FA' Or .w_GPPERLIQ <> 'A' And .w_GPPERLIQ=.w_COPRMLIQ))  and not(empty(.w_GPCONTRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPCONTRA_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GPTIPINT))  and not(.pCiclo='A')  and (Empty(.w_GPCONTRA) And .cfunction <> 'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPTIPINT_1_18.SetFocus()
            i_bnoObbl = !empty(.w_GPTIPINT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GPTIPINT))  and not(.pCiclo='V')  and (Empty(.w_GPCONTRA) And .cfunction <> 'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPTIPINT_1_19.SetFocus()
            i_bnoObbl = !empty(.w_GPTIPINT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GPDATINI)) or not(.w_GPDATREG>=.w_GPDATINI AND .w_GPDATINI<=.w_GPDATFIN And (.w_GPPERLIQ = 'A' Or .w_GPPERLIQ <> 'A' And Not Empty(.w_GPDATINI))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPDATINI_1_25.SetFocus()
            i_bnoObbl = !empty(.w_GPDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di inizio selezione deve essere minore o uguale della data di fine selezione")
          case   ((empty(.w_GPDATFIN)) or not(.w_GPDATREG>=.w_GPDATFIN And (.w_GPPERLIQ = 'A' Or .w_GPPERLIQ <> 'A' And Not Empty(.w_GPDATFIN))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPDATFIN_1_26.SetFocus()
            i_bnoObbl = !empty(.w_GPDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GPTIPDOC)) or not(.w_CLADOC = 'FA' AND .w_FLVEAC1='V'))  and not(Empty(.w_GPCONTRA) Or .w_COPRMDOC<>'F')  and (Not Empty(.w_GPCONTRA) And .w_GPDOCFIS='S' And .w_COPRMDOC='F')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPTIPDOC_1_27.SetFocus()
            i_bnoObbl = !empty(.w_GPTIPDOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente")
          case   ((empty(.w_GPTIPDOC)) or not(.w_CLADOC = 'NC' AND .w_FLVEAC1 = 'V'))  and not(Empty(.w_GPCONTRA) Or .w_COPRMDOC<>'N')  and (Not Empty(.w_GPCONTRA) And .w_GPDOCFIS='S'  And .w_COPRMDOC='N')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPTIPDOC_1_28.SetFocus()
            i_bnoObbl = !empty(.w_GPTIPDOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente")
          case   ((empty(.w_GPTIPDOC)) or not(.w_CLADOC = 'DI' AND .w_FLVEAC1 = 'A'))  and not(Empty(.w_GPCONTRA) Or .w_COPRMDOC<>'P')  and (Not Empty(.w_GPCONTRA) And .w_GPDOCFIS='S' And .w_COPRMDOC='P')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPTIPDOC_1_29.SetFocus()
            i_bnoObbl = !empty(.w_GPTIPDOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente")
          case   ((empty(.w_GPTIPDOC)) or not(.w_CLADOC = 'DI'  And .w_FLVEAC1='A'))  and not(Empty(.w_GPCONTRA) Or .w_COPRMDOC<>'C')  and (Not Empty(.w_GPCONTRA) And .w_GPDOCFIS='S' And .w_COPRMDOC='C')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPTIPDOC_1_30.SetFocus()
            i_bnoObbl = !empty(.w_GPTIPDOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente")
          case   ((empty(.w_GPTIPDOC)) or not(.w_CLADOC = 'DI' AND .w_FLVEAC1='A'))  and not(Empty(.w_GPCONTRA) Or .w_COPRMDOC<>'A')  and (Not Empty(.w_GPCONTRA) And .w_GPDOCFIS='S' And .w_COPRMDOC='A')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPTIPDOC_1_31.SetFocus()
            i_bnoObbl = !empty(.w_GPTIPDOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente")
          case   (empty(.w_GPDATDOC))  and (.w_GPDOCFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPDATDOC_1_35.SetFocus()
            i_bnoObbl = !empty(.w_GPDATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GPDATCIV)) or not(.w_GPDATREG>=.w_GPDATCIV))  and (.w_GPDOCFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPDATCIV_1_37.SetFocus()
            i_bnoObbl = !empty(.w_GPDATCIV)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di competenza IVA deve essere minore o uguale della data documento")
          case   (empty(.w_GPDATDIV))  and (.w_GPDOCFIS='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPDATDIV_1_38.SetFocus()
            i_bnoObbl = !empty(.w_GPDATDIV)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GPCAUINT)) or not(.w_CLADOC2 = 'DI'  And .w_FLINTE <> 'N' And (.w_GPTIPINT <> 'A' Or .w_GPTIPINT='A' And .w_FLINTE = 'F')))  and not(NOT( .w_FLVEAC2 = "V" OR EMPTY( .w_GPCAUINT ) ))  and (.w_GPDOCINT='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPCAUINT_1_39.SetFocus()
            i_bnoObbl = !empty(.w_GPCAUINT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente")
          case   ((empty(.w_GPCAUINT)) or not(.w_CLADOC2 = 'DI'  And .w_FLINTE <> 'N' And (.w_GPTIPINT <> 'A' Or .w_GPTIPINT='A' And .w_FLINTE = 'F')))  and not(.w_FLVEAC2 = "V" OR EMPTY( .w_GPCAUINT ))  and (.w_GPDOCINT='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPCAUINT_1_40.SetFocus()
            i_bnoObbl = !empty(.w_GPCAUINT)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale documento inesistente o incongruente")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSGP_MGP.CheckForm()
      if i_bres
        i_bres=  .GSGP_MGP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODAZI = this.w_CODAZI
    this.o_GPSERIAL = this.w_GPSERIAL
    this.o_GPDATREG = this.w_GPDATREG
    this.o_GPSTATUS = this.w_GPSTATUS
    this.o_GPVALCON = this.w_GPVALCON
    this.o_GPDOCFIS = this.w_GPDOCFIS
    this.o_GPDOCINT = this.w_GPDOCINT
    this.o_GPCONTRA = this.w_GPCONTRA
    this.o_GPTIPINT = this.w_GPTIPINT
    this.o_COPRMDOC = this.w_COPRMDOC
    * --- GSGP_MGP : Depends On
    this.GSGP_MGP.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsgp_agpPag1 as StdContainer
  Width  = 648
  height = 447
  stdWidth  = 648
  stdheight = 447
  resizeXpos=297
  resizeYpos=328
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGPSERIAL_1_4 as StdField with uid="CULQSXWOFI",rtseq=4,rtrep=.f.,;
    cFormVar = "w_GPSERIAL", cQueryName = "GPSERIAL",enabled=.f.,nZero=10,;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 69143630,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=84, Left=187, Top=9, InputMask=replicate('X',10)

  add object oGPDESCRI_1_5 as StdField with uid="PYDFYZZABP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_GPDESCRI", cQueryName = "GPDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali note descrittive",;
    HelpContextID = 168819793,;
   bGlobalFont=.t.,;
    Height=21, Width=327, Left=278, Top=9, InputMask=replicate('X',40)


  add object oGPPERLIQ_1_6 as StdCombo with uid="LCRBGHUPEG",rtseq=6,rtrep=.f.,left=187,top=41,width=111,height=21;
    , HelpContextID = 249611191;
    , cFormVar="w_GPPERLIQ",RowSource=""+"Altra,"+"Semestrale,"+"Trimestrale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGPPERLIQ_1_6.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'S',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oGPPERLIQ_1_6.GetRadio()
    this.Parent.oContained.w_GPPERLIQ = this.RadioValue()
    return .t.
  endfunc

  func oGPPERLIQ_1_6.SetRadio()
    this.Parent.oContained.w_GPPERLIQ=trim(this.Parent.oContained.w_GPPERLIQ)
    this.value = ;
      iif(this.Parent.oContained.w_GPPERLIQ=='A',1,;
      iif(this.Parent.oContained.w_GPPERLIQ=='S',2,;
      iif(this.Parent.oContained.w_GPPERLIQ=='T',3,;
      0)))
  endfunc

  add object oGPDATREG_1_7 as StdField with uid="RVQDJDBMZX",rtseq=7,rtrep=.f.,;
    cFormVar = "w_GPDATREG", cQueryName = "GPDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di registrazione",;
    HelpContextID = 83624877,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=335, Top=41


  add object oGPSTATUS_1_8 as StdCombo with uid="HTQCFDGSLI",rtseq=8,rtrep=.f.,left=515,top=42,width=90,height=21;
    , ToolTipText = "Tipo generazione";
    , HelpContextID = 98563001;
    , cFormVar="w_GPSTATUS",RowSource=""+"Definitiva,"+"Simulata", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGPSTATUS_1_8.RadioValue()
    return(iif(this.value =1,'D',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oGPSTATUS_1_8.GetRadio()
    this.Parent.oContained.w_GPSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oGPSTATUS_1_8.SetRadio()
    this.Parent.oContained.w_GPSTATUS=trim(this.Parent.oContained.w_GPSTATUS)
    this.value = ;
      iif(this.Parent.oContained.w_GPSTATUS=='D',1,;
      iif(this.Parent.oContained.w_GPSTATUS=='S',2,;
      0))
  endfunc

  func oGPSTATUS_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction='Load')
    endwith
   endif
  endfunc


  add object oGPVALORI_1_9 as StdCombo with uid="OUAYVALQQJ",rtseq=9,rtrep=.f.,left=187,top=73,width=160,height=21;
    , HelpContextID = 243457105;
    , cFormVar="w_GPVALORI",RowSource=""+"Costo medio pond. anno,"+"Costo medio pond. periodo,"+"Costo ultimo,"+"Costo standard ultimo,"+"LIFO continuo,"+"FIFO continuo,"+"LIFO scatti,"+"Da listino", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGPVALORI_1_9.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    iif(this.value =8,'L',;
    space(1))))))))))
  endfunc
  func oGPVALORI_1_9.GetRadio()
    this.Parent.oContained.w_GPVALORI = this.RadioValue()
    return .t.
  endfunc

  func oGPVALORI_1_9.SetRadio()
    this.Parent.oContained.w_GPVALORI=trim(this.Parent.oContained.w_GPVALORI)
    this.value = ;
      iif(this.Parent.oContained.w_GPVALORI=='A',1,;
      iif(this.Parent.oContained.w_GPVALORI=='B',2,;
      iif(this.Parent.oContained.w_GPVALORI=='C',3,;
      iif(this.Parent.oContained.w_GPVALORI=='D',4,;
      iif(this.Parent.oContained.w_GPVALORI=='E',5,;
      iif(this.Parent.oContained.w_GPVALORI=='F',6,;
      iif(this.Parent.oContained.w_GPVALORI=='G',7,;
      iif(this.Parent.oContained.w_GPVALORI=='L',8,;
      0))))))))
  endfunc

  add object oGPINVENT_1_10 as StdField with uid="XNFQWFNMLW",rtseq=10,rtrep=.f.,;
    cFormVar = "w_GPINVENT", cQueryName = "GPINVENT",nZero=6,;
    bObbl = .t. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Inventario di riferimento",;
    HelpContextID = 131509318,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=187, Top=103, cSayPict='v_ZR+"999999"', cGetPict='v_ZR+"999999"', InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="INVENTAR", cZoomOnZoom="gsma_ain", oKey_1_1="INCODESE", oKey_1_2="this.w_MVCODESE", oKey_2_1="INNUMINV", oKey_2_2="this.w_GPINVENT"

  func oGPINVENT_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPVALORI <> 'L')
    endwith
   endif
  endfunc

  func oGPINVENT_1_10.mHide()
    with this.Parent.oContained
      return (.w_GPVALORI ='L')
    endwith
  endfunc

  func oGPINVENT_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPINVENT_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPINVENT_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.INVENTAR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStrODBC(this.Parent.oContained.w_MVCODESE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"INCODESE="+cp_ToStr(this.Parent.oContained.w_MVCODESE)
    endif
    do cp_zoom with 'INVENTAR','*','INCODESE,INNUMINV',cp_AbsName(this.parent,'oGPINVENT_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'gsma_ain',"Elenco inventari",'',this.parent.oContained
  endproc
  proc oGPINVENT_1_10.mZoomOnZoom
    local i_obj
    i_obj=gsma_ain()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.INCODESE=w_MVCODESE
     i_obj.w_INNUMINV=this.parent.oContained.w_GPINVENT
     i_obj.ecpSave()
  endproc

  add object oGPVALCON_1_11 as StdField with uid="VHXVJGKBBV",rtseq=11,rtrep=.f.,;
    cFormVar = "w_GPVALCON", cQueryName = "GPVALCON",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 176348236,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=367, Top=197, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_GPVALCON"

  func oGPVALCON_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_GPCONTRA))
    endwith
   endif
  endfunc

  func oGPVALCON_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPVALCON_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPVALCON_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oGPVALCON_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Elenco valute",'',this.parent.oContained
  endproc
  proc oGPVALCON_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_GPVALCON
     i_obj.ecpSave()
  endproc

  add object oGPCODLIS_1_12 as StdField with uid="BWALYSOXDX",rtseq=12,rtrep=.f.,;
    cFormVar = "w_GPCODLIS", cQueryName = "GPCODLIS",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido oppure valuta listino incongruente",;
    ToolTipText = "Codice listino utilizzato per i servizi ed articoli con combo valorizzazione da listino",;
    HelpContextID = 235533241,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=543, Top=103, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_GPCODLIS"

  func oGPCODLIS_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCODLIS_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPCODLIS_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oGPCODLIS_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco listini",'GSMAASAV.LISTINI_VZM',this.parent.oContained
  endproc

  add object oGPGRUCAU_1_13 as StdField with uid="ELSHHSNQWJ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_GPGRUCAU", cQueryName = "GPGRUCAU",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice gruppo non valido oppure generazione definitiva e gruppo di causali di simulazione",;
    ToolTipText = "Codice gruppo causali di magazzino",;
    HelpContextID = 165858373,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=187, Top=134, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CCOMCMAG", oKey_1_1="CSCODICE", oKey_1_2="this.w_GPGRUCAU"

  func oGPGRUCAU_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPGRUCAU_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPGRUCAU_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CCOMCMAG','*','CSCODICE',cp_AbsName(this.parent,'oGPGRUCAU_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi di causali",'GSGP_AGP.CCOMCMAG_VZM',this.parent.oContained
  endproc

  add object oGPDOCFIS_1_14 as StdCheck with uid="GDODSAFHXV",rtseq=14,rtrep=.f.,left=187, top=164, caption="Generazione documenti fiscali",;
    ToolTipText = "Se attivo: devono essere generati documenti fiscali",;
    HelpContextID = 133825465,;
    cFormVar="w_GPDOCFIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGPDOCFIS_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGPDOCFIS_1_14.GetRadio()
    this.Parent.oContained.w_GPDOCFIS = this.RadioValue()
    return .t.
  endfunc

  func oGPDOCFIS_1_14.SetRadio()
    this.Parent.oContained.w_GPDOCFIS=trim(this.Parent.oContained.w_GPDOCFIS)
    this.value = ;
      iif(this.Parent.oContained.w_GPDOCFIS=='S',1,;
      0)
  endfunc

  func oGPDOCFIS_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPSTATUS='D')
    endwith
   endif
  endfunc

  add object oGPDOCINT_1_15 as StdCheck with uid="VVOZMZPJBU",rtseq=15,rtrep=.f.,left=420, top=164, caption="Generazione documenti interni",;
    ToolTipText = "Se attivo: devono essere generati documenti interni",;
    HelpContextID = 84278342,;
    cFormVar="w_GPDOCINT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGPDOCINT_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGPDOCINT_1_15.GetRadio()
    this.Parent.oContained.w_GPDOCINT = this.RadioValue()
    return .t.
  endfunc

  func oGPDOCINT_1_15.SetRadio()
    this.Parent.oContained.w_GPDOCINT=trim(this.Parent.oContained.w_GPDOCINT)
    this.value = ;
      iif(this.Parent.oContained.w_GPDOCINT=='S',1,;
      0)
  endfunc

  func oGPDOCINT_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPSTATUS='D')
    endwith
   endif
  endfunc

  add object oGPCONTRA_1_16 as StdField with uid="TERCCHJVLV",rtseq=16,rtrep=.f.,;
    cFormVar = "w_GPCONTRA", cQueryName = "GPCONTRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 156634201,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=57, Top=197, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CCOMMAST", oKey_1_1="COCODICE", oKey_1_2="this.w_GPCONTRA"

  func oGPCONTRA_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCONTRA_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPCONTRA_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CCOMMAST','*','COCODICE',cp_AbsName(this.parent,'oGPCONTRA_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Contratti obiettivo",'GSGP_AGP.CCOMMAST_VZM',this.parent.oContained
  endproc

  add object oGPCNOVAL_1_17 as StdCheck with uid="NYOZICCHVC",rtseq=17,rtrep=.f.,left=422, top=197, caption="Solo contratti non pi� validi",;
    ToolTipText = "Se attivo: solo contratti non pi� validi",;
    HelpContextID = 122096718,;
    cFormVar="w_GPCNOVAL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGPCNOVAL_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGPCNOVAL_1_17.GetRadio()
    this.Parent.oContained.w_GPCNOVAL = this.RadioValue()
    return .t.
  endfunc

  func oGPCNOVAL_1_17.SetRadio()
    this.Parent.oContained.w_GPCNOVAL=trim(this.Parent.oContained.w_GPCNOVAL)
    this.value = ;
      iif(this.Parent.oContained.w_GPCNOVAL=='S',1,;
      0)
  endfunc

  func oGPCNOVAL_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_GPCONTRA))
    endwith
   endif
  endfunc


  add object oGPTIPINT_1_18 as StdCombo with uid="NWAEYDETWA",rtseq=18,rtrep=.f.,left=57,top=229,width=126,height=21;
    , ToolTipText = "Tipo intestatario";
    , HelpContextID = 70974534;
    , cFormVar="w_GPTIPINT",RowSource=""+"Cliente,"+"Agente,"+"Gruppo di acquisto", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oGPTIPINT_1_18.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'A',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oGPTIPINT_1_18.GetRadio()
    this.Parent.oContained.w_GPTIPINT = this.RadioValue()
    return .t.
  endfunc

  func oGPTIPINT_1_18.SetRadio()
    this.Parent.oContained.w_GPTIPINT=trim(this.Parent.oContained.w_GPTIPINT)
    this.value = ;
      iif(this.Parent.oContained.w_GPTIPINT=='C',1,;
      iif(this.Parent.oContained.w_GPTIPINT=='A',2,;
      iif(this.Parent.oContained.w_GPTIPINT=='G',3,;
      0)))
  endfunc

  func oGPTIPINT_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_GPCONTRA) And .cfunction <> 'Edit')
    endwith
   endif
  endfunc

  func oGPTIPINT_1_18.mHide()
    with this.Parent.oContained
      return (.pCiclo='A')
    endwith
  endfunc

  func oGPTIPINT_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_GPCODINT)
        bRes2=.link_1_20('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oGPTIPINT_1_19 as StdCombo with uid="OLHKUIRXMM",rtseq=19,rtrep=.f.,left=57,top=229,width=126,height=21;
    , ToolTipText = "Tipo intestatario";
    , HelpContextID = 70974534;
    , cFormVar="w_GPTIPINT",RowSource=""+"Fornitore,"+"Gruppo di acquisto", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oGPTIPINT_1_19.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'G',;
    space(1))))
  endfunc
  func oGPTIPINT_1_19.GetRadio()
    this.Parent.oContained.w_GPTIPINT = this.RadioValue()
    return .t.
  endfunc

  func oGPTIPINT_1_19.SetRadio()
    this.Parent.oContained.w_GPTIPINT=trim(this.Parent.oContained.w_GPTIPINT)
    this.value = ;
      iif(this.Parent.oContained.w_GPTIPINT=='F',1,;
      iif(this.Parent.oContained.w_GPTIPINT=='G',2,;
      0))
  endfunc

  func oGPTIPINT_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_GPCONTRA) And .cfunction <> 'Edit')
    endwith
   endif
  endfunc

  func oGPTIPINT_1_19.mHide()
    with this.Parent.oContained
      return (.pCiclo='V')
    endwith
  endfunc

  func oGPTIPINT_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_GPCODINT)
        bRes2=.link_1_20('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oGPCODINT_1_20 as StdField with uid="MOROYLUOGG",rtseq=20,rtrep=.f.,;
    cFormVar = "w_GPCODINT", cQueryName = "GPCODINT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 83233862,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=184, Top=229, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_GPTIPINT", oKey_2_1="ANCODICE", oKey_2_2="this.w_GPCODINT"

  func oGPCODINT_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_GPCONTRA) And .w_GPTIPINT $ 'CF')
    endwith
   endif
  endfunc

  func oGPCODINT_1_20.mHide()
    with this.Parent.oContained
      return (!.w_GPTIPINT $ 'CF')
    endwith
  endfunc

  func oGPCODINT_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCODINT_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPCODINT_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_GPTIPINT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_GPTIPINT)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oGPCODINT_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti/fornitori",'',this.parent.oContained
  endproc
  proc oGPCODINT_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_GPTIPINT
     i_obj.w_ANCODICE=this.parent.oContained.w_GPCODINT
     i_obj.ecpSave()
  endproc

  add object oGPCODAGE_1_21 as StdField with uid="RVVTSZBHMS",rtseq=21,rtrep=.f.,;
    cFormVar = "w_GPCODAGE", cQueryName = "GPCODAGE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 50983851,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=184, Top=229, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_GPCODAGE"

  func oGPCODAGE_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_GPCONTRA) And .w_GPTIPINT = 'A')
    endwith
   endif
  endfunc

  func oGPCODAGE_1_21.mHide()
    with this.Parent.oContained
      return (.w_GPTIPINT <> 'A')
    endwith
  endfunc

  func oGPCODAGE_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCODAGE_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPCODAGE_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oGPCODAGE_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oGPCODGRU_1_22 as StdField with uid="VKLUFFPOFW",rtseq=22,rtrep=.f.,;
    cFormVar = "w_GPCODGRU", cQueryName = "GPCODGRU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 116788293,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=184, Top=229, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="GRUPPACQ", oKey_1_1="GACODICE", oKey_1_2="this.w_GPCODGRU"

  func oGPCODGRU_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_GPCONTRA) And .w_GPTIPINT = 'G' And .w_GPFLGCIC='V')
    endwith
   endif
  endfunc

  func oGPCODGRU_1_22.mHide()
    with this.Parent.oContained
      return (.w_GPTIPINT <> 'G' Or .w_GPFLGCIC='A')
    endwith
  endfunc

  func oGPCODGRU_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCODGRU_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPCODGRU_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUPPACQ','*','GACODICE',cp_AbsName(this.parent,'oGPCODGRU_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi di acquisto",'GSGPVMGA.GRUPPACQ_VZM',this.parent.oContained
  endproc

  add object oGPCODGRU_1_23 as StdField with uid="ATLPGNQRTA",rtseq=23,rtrep=.f.,;
    cFormVar = "w_GPCODGRU", cQueryName = "GPCODGRU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 116788293,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=184, Top=229, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="GRUPPACQ", oKey_1_1="GACODICE", oKey_1_2="this.w_GPCODGRU"

  func oGPCODGRU_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_GPCONTRA) And .w_GPTIPINT = 'G' And .w_GPFLGCIC='A')
    endwith
   endif
  endfunc

  func oGPCODGRU_1_23.mHide()
    with this.Parent.oContained
      return (.w_GPTIPINT <> 'G' Or .w_GPFLGCIC='V')
    endwith
  endfunc

  func oGPCODGRU_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCODGRU_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPCODGRU_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUPPACQ','*','GACODICE',cp_AbsName(this.parent,'oGPCODGRU_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi di acquisto",'GSGPAMGA.GRUPPACQ_VZM',this.parent.oContained
  endproc

  add object oGPDATINI_1_25 as StdField with uid="ZQOYPKTEYJ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_GPDATINI", cQueryName = "GPDATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di inizio selezione deve essere minore o uguale della data di fine selezione",;
    ToolTipText = "Data documenti di inizio selezione",;
    HelpContextID = 67370065,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=216, Top=263

  func oGPDATINI_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_GPDATREG>=.w_GPDATINI AND .w_GPDATINI<=.w_GPDATFIN And (.w_GPPERLIQ = 'A' Or .w_GPPERLIQ <> 'A' And Not Empty(.w_GPDATINI)))
    endwith
    return bRes
  endfunc

  add object oGPDATFIN_1_26 as StdField with uid="UAMOYZTCMA",rtseq=26,rtrep=.f.,;
    cFormVar = "w_GPDATFIN", cQueryName = "GPDATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documenti di fine selezione",;
    HelpContextID = 150733748,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=364, Top=262

  func oGPDATFIN_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_GPDATREG>=.w_GPDATFIN And (.w_GPPERLIQ = 'A' Or .w_GPPERLIQ <> 'A' And Not Empty(.w_GPDATFIN)))
    endwith
    return bRes
  endfunc

  add object oGPTIPDOC_1_27 as StdField with uid="RNHTUGAGSB",rtseq=27,rtrep=.f.,;
    cFormVar = "w_GPTIPDOC", cQueryName = "GPTIPDOC",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente",;
    ToolTipText = "Codice causale di eventuale selezione documento (vuoto = no selezione)",;
    HelpContextID = 154860631,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=182, Top=310, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_GPTIPDOC"

  func oGPTIPDOC_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_GPCONTRA) And .w_GPDOCFIS='S' And .w_COPRMDOC='F')
    endwith
   endif
  endfunc

  func oGPTIPDOC_1_27.mHide()
    with this.Parent.oContained
      return (Empty(.w_GPCONTRA) Or .w_COPRMDOC<>'F')
    endwith
  endfunc

  func oGPTIPDOC_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPTIPDOC_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPTIPDOC_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oGPTIPDOC_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'GSGP1DOC.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oGPTIPDOC_1_27.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_GPTIPDOC
     i_obj.ecpSave()
  endproc

  add object oGPTIPDOC_1_28 as StdField with uid="PYULRBSOGJ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_GPTIPDOC", cQueryName = "GPTIPDOC",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente",;
    ToolTipText = "Codice causale di eventuale selezione documento (vuoto = no selezione)",;
    HelpContextID = 154860631,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=182, Top=310, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_GPTIPDOC"

  func oGPTIPDOC_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_GPCONTRA) And .w_GPDOCFIS='S'  And .w_COPRMDOC='N')
    endwith
   endif
  endfunc

  func oGPTIPDOC_1_28.mHide()
    with this.Parent.oContained
      return (Empty(.w_GPCONTRA) Or .w_COPRMDOC<>'N')
    endwith
  endfunc

  func oGPTIPDOC_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPTIPDOC_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPTIPDOC_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oGPTIPDOC_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'GSGP2DOC.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oGPTIPDOC_1_28.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_GPTIPDOC
     i_obj.ecpSave()
  endproc

  add object oGPTIPDOC_1_29 as StdField with uid="HMZKXBTQUK",rtseq=29,rtrep=.f.,;
    cFormVar = "w_GPTIPDOC", cQueryName = "GPTIPDOC",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente",;
    ToolTipText = "Codice causale di eventuale selezione documento (vuoto = no selezione)",;
    HelpContextID = 154860631,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=182, Top=310, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAC_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_GPTIPDOC"

  func oGPTIPDOC_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_GPCONTRA) And .w_GPDOCFIS='S' And .w_COPRMDOC='P')
    endwith
   endif
  endfunc

  func oGPTIPDOC_1_29.mHide()
    with this.Parent.oContained
      return (Empty(.w_GPCONTRA) Or .w_COPRMDOC<>'P')
    endwith
  endfunc

  func oGPTIPDOC_1_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPTIPDOC_1_29.ecpDrop(oSource)
    this.Parent.oContained.link_1_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPTIPDOC_1_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oGPTIPDOC_1_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAC_ATD',"Causali documenti",'GSGP3DOC.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oGPTIPDOC_1_29.mZoomOnZoom
    local i_obj
    i_obj=GSAC_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_GPTIPDOC
     i_obj.ecpSave()
  endproc

  add object oGPTIPDOC_1_30 as StdField with uid="PWSEMLMYFQ",rtseq=30,rtrep=.f.,;
    cFormVar = "w_GPTIPDOC", cQueryName = "GPTIPDOC",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente",;
    ToolTipText = "Codice causale di eventuale selezione documento (vuoto = no selezione)",;
    HelpContextID = 154860631,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=182, Top=310, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAC_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_GPTIPDOC"

  func oGPTIPDOC_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_GPCONTRA) And .w_GPDOCFIS='S' And .w_COPRMDOC='C')
    endwith
   endif
  endfunc

  func oGPTIPDOC_1_30.mHide()
    with this.Parent.oContained
      return (Empty(.w_GPCONTRA) Or .w_COPRMDOC<>'C')
    endwith
  endfunc

  func oGPTIPDOC_1_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPTIPDOC_1_30.ecpDrop(oSource)
    this.Parent.oContained.link_1_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPTIPDOC_1_30.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oGPTIPDOC_1_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAC_ATD',"Causali documenti",'GSGP4DOC.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oGPTIPDOC_1_30.mZoomOnZoom
    local i_obj
    i_obj=GSAC_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_GPTIPDOC
     i_obj.ecpSave()
  endproc

  add object oGPTIPDOC_1_31 as StdField with uid="HTQXUXFRQL",rtseq=31,rtrep=.f.,;
    cFormVar = "w_GPTIPDOC", cQueryName = "GPTIPDOC",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente",;
    ToolTipText = "Codice causale di eventuale selezione documento (vuoto = no selezione)",;
    HelpContextID = 154860631,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=182, Top=310, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAC_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_GPTIPDOC"

  func oGPTIPDOC_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_GPCONTRA) And .w_GPDOCFIS='S' And .w_COPRMDOC='A')
    endwith
   endif
  endfunc

  func oGPTIPDOC_1_31.mHide()
    with this.Parent.oContained
      return (Empty(.w_GPCONTRA) Or .w_COPRMDOC<>'A')
    endwith
  endfunc

  func oGPTIPDOC_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPTIPDOC_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPTIPDOC_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oGPTIPDOC_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAC_ATD',"Causali documenti",'GSGPADOC.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oGPTIPDOC_1_31.mZoomOnZoom
    local i_obj
    i_obj=GSAC_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_GPTIPDOC
     i_obj.ecpSave()
  endproc

  add object oGPCODESE_1_32 as StdField with uid="JZGWLWCIBT",rtseq=32,rtrep=.f.,;
    cFormVar = "w_GPCODESE", cQueryName = "GPCODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio dei documenti da generare",;
    HelpContextID = 150342741,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=557, Top=310, InputMask=replicate('X',4), cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_GPCODESE"

  func oGPCODESE_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPDOCFIS='S')
    endwith
   endif
  endfunc

  func oGPCODESE_1_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCODESE_1_32.ecpDrop(oSource)
    this.Parent.oContained.link_1_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oGPNUMDOC_1_33 as StdField with uid="QNSFQXEZBS",rtseq=33,rtrep=.f.,;
    cFormVar = "w_GPNUMDOC", cQueryName = "GPNUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 157244503,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=182, Top=340, cSayPict='"999999"', cGetPict='"999999"'

  add object oGPALFDOC_1_34 as StdField with uid="CGUAFFNJEH",rtseq=34,rtrep=.f.,;
    cFormVar = "w_GPALFDOC", cQueryName = "GPALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento",;
    HelpContextID = 165227607,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=258, Top=340, InputMask=replicate('X',10)

  func oGPALFDOC_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty(.w_GPCONTRA) And .w_GPDOCFIS='S')
    endwith
   endif
  endfunc

  add object oGPDATDOC_1_35 as StdField with uid="MJNJJHLPZQ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_GPDATDOC", cQueryName = "GPDATDOC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 151256151,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=384, Top=340

  func oGPDATDOC_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPDOCFIS='S')
    endwith
   endif
  endfunc

  add object oGPPUNPRE_1_36 as StdCheck with uid="XWKBHZBBIL",rtseq=36,rtrep=.f.,left=478, top=340, caption="Dettaglia punti premio",;
    ToolTipText = "Se attivo: dettaglia punti premio",;
    HelpContextID = 223296597,;
    cFormVar="w_GPPUNPRE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGPPUNPRE_1_36.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGPPUNPRE_1_36.GetRadio()
    this.Parent.oContained.w_GPPUNPRE = this.RadioValue()
    return .t.
  endfunc

  func oGPPUNPRE_1_36.SetRadio()
    this.Parent.oContained.w_GPPUNPRE=trim(this.Parent.oContained.w_GPPUNPRE)
    this.value = ;
      iif(this.Parent.oContained.w_GPPUNPRE=='S',1,;
      0)
  endfunc

  func oGPPUNPRE_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPDOCFIS='S')
    endwith
   endif
  endfunc

  add object oGPDATCIV_1_37 as StdField with uid="INDDUBBHAZ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_GPDATCIV", cQueryName = "GPDATCIV",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di competenza IVA deve essere minore o uguale della data documento",;
    ToolTipText = "Data di competenza IVA",;
    HelpContextID = 100402108,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=182, Top=370

  func oGPDATCIV_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPDOCFIS='S')
    endwith
   endif
  endfunc

  func oGPDATCIV_1_37.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_GPDATREG>=.w_GPDATCIV)
    endwith
    return bRes
  endfunc

  add object oGPDATDIV_1_38 as StdField with uid="RKGJVENOLG",rtseq=38,rtrep=.f.,;
    cFormVar = "w_GPDATDIV", cQueryName = "GPDATDIV",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale data di inizio scadenze per pagamenti in data diversa",;
    HelpContextID = 117179324,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=454, Top=370

  func oGPDATDIV_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPDOCFIS='S')
    endwith
   endif
  endfunc

  add object oGPCAUINT_1_39 as StdField with uid="EBPITHTDFY",rtseq=39,rtrep=.f.,;
    cFormVar = "w_GPCAUINT", cQueryName = "GPCAUINT",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente",;
    ToolTipText = "Codice causale di eventuale selezione documento (vuoto = no selezione)",;
    HelpContextID = 66325574,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=178, Top=422, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_GPCAUINT"

  func oGPCAUINT_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPDOCINT='S')
    endwith
   endif
  endfunc

  func oGPCAUINT_1_39.mHide()
    with this.Parent.oContained
      return (NOT( .w_FLVEAC2 = "V" OR EMPTY( .w_GPCAUINT ) ))
    endwith
  endfunc

  func oGPCAUINT_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCAUINT_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPCAUINT_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oGPCAUINT_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Causali documenti",'GSGP1AGP.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oGPCAUINT_1_39.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_GPCAUINT
     i_obj.ecpSave()
  endproc

  add object oGPCAUINT_1_40 as StdField with uid="KINTIAQLLP",rtseq=40,rtrep=.f.,;
    cFormVar = "w_GPCAUINT", cQueryName = "GPCAUINT",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale documento inesistente o incongruente",;
    ToolTipText = "Codice causale di eventuale selezione documento (vuoto = no selezione)",;
    HelpContextID = 66325574,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=179, Top=422, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSAC_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_GPCAUINT"

  func oGPCAUINT_1_40.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPDOCINT='S')
    endwith
   endif
  endfunc

  func oGPCAUINT_1_40.mHide()
    with this.Parent.oContained
      return (.w_FLVEAC2 = "V" OR EMPTY( .w_GPCAUINT ))
    endwith
  endfunc

  func oGPCAUINT_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCAUINT_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPCAUINT_1_40.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oGPCAUINT_1_40'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAC_ATD',"Causali documenti",'GSGP1AGP.TIP_DOCU_VZM',this.parent.oContained
  endproc
  proc oGPCAUINT_1_40.mZoomOnZoom
    local i_obj
    i_obj=GSAC_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_GPCAUINT
     i_obj.ecpSave()
  endproc

  add object oDESORI_1_44 as StdField with uid="FLQQBKHLMF",rtseq=44,rtrep=.f.,;
    cFormVar = "w_DESORI", cQueryName = "DESORI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 68491210,;
   bGlobalFont=.t.,;
    Height=21, Width=246, Left=247, Top=309, InputMask=replicate('X',35)


  add object oBtn_1_49 as StdButton with uid="VHHERDESXR",left=545, top=400, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 225885926;
    , tabstop=.f., caption='\<Conferma';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_49.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_50 as StdButton with uid="INZUXVURLA",left=596, top=400, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermare";
    , HelpContextID = 165275834;
    , tabstop=.f., caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_50.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_51 as cp_runprogram with uid="XOYLMIOPQE",left=0, top=460, width=580,height=19,;
    caption='GSVE_BS3',;
   bGlobalFont=.t.,;
    prg='GSVE_BS3("GSCC_AGP")',;
    cEvent = "w_GPCONTRA Changed,w_GPTIPDOC Changed,w_GPALFDOC Changed,w_GPDATDOC Changed",;
    nPag=1;
    , HelpContextID = 172939623


  add object oLinkPC_1_52 as StdButton with uid="LMVYKUXFJF",left=495, top=400, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al dettaglio dei documenti generati";
    , HelpContextID = 233969718;
    , tabstop=.f., caption='\<Vis. Doc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_52.Click()
      this.Parent.oContained.GSGP_MGP.LinkPCClick()
    endproc

  func oLinkPC_1_52.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_GPSERIAL)  AND .cFunction<>'Load')
      endwith
    endif
  endfunc

  func oLinkPC_1_52.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.cFunction='Load' Or .w_GPSTATUS = 'S')
     endwith
    endif
  endfunc

  add object oPRDATINV_1_66 as StdField with uid="QHLQSCYHSP",rtseq=46,rtrep=.f.,;
    cFormVar = "w_PRDATINV", cQueryName = "PRDATINV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fino alla quale sono stati considerati i movimenti dell'inventario prec.",;
    HelpContextID = 67369396,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=308, Top=103

  func oPRDATINV_1_66.mHide()
    with this.Parent.oContained
      return (.w_GPVALORI ='L')
    endwith
  endfunc

  add object oGPDESINT_1_71 as StdField with uid="LSJCZENCFK",rtseq=47,rtrep=.f.,;
    cFormVar = "w_GPDESINT", cQueryName = "GPDESINT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 68156486,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=305, Top=229, InputMask=replicate('X',40)

  func oGPDESINT_1_71.mHide()
    with this.Parent.oContained
      return (!.w_GPTIPINT $ 'CF')
    endwith
  endfunc

  add object oGPDESGRU_1_72 as StdField with uid="QJBTDEUKIE",rtseq=48,rtrep=.f.,;
    cFormVar = "w_GPDESGRU", cQueryName = "GPDESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 101710917,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=305, Top=229, InputMask=replicate('X',40)

  func oGPDESGRU_1_72.mHide()
    with this.Parent.oContained
      return (.w_GPTIPINT <> 'G')
    endwith
  endfunc

  add object oGPDESAGE_1_73 as StdField with uid="QYTDYAWXAL",rtseq=49,rtrep=.f.,;
    cFormVar = "w_GPDESAGE", cQueryName = "GPDESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 66061227,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=305, Top=229, InputMask=replicate('X',40)

  func oGPDESAGE_1_73.mHide()
    with this.Parent.oContained
      return (.w_GPTIPINT <> 'A')
    endwith
  endfunc

  add object oDATACON_1_76 as StdField with uid="FRQGGPGDRD",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DATACON", cQueryName = "DATACON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 252906442,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=231, Top=197

  add object oDESINT_1_83 as StdField with uid="IZCAQWHNBM",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DESINT", cQueryName = "DESINT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 156964810,;
   bGlobalFont=.t.,;
    Height=21, Width=246, Left=243, Top=422, InputMask=replicate('X',35)

  add object oGPTIPDOC_1_111 as StdField with uid="BMHKOSEEVV",rtseq=78,rtrep=.f.,;
    cFormVar = "w_GPTIPDOC", cQueryName = "GPTIPDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice causale di eventuale selezione documento (vuoto = no selezione)",;
    HelpContextID = 154860631,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=182, Top=309, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_GPTIPDOC"

  func oGPTIPDOC_1_111.mHide()
    with this.Parent.oContained
      return (!Empty(.w_GPCONTRA))
    endwith
  endfunc

  func oGPTIPDOC_1_111.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESGCAU_1_128 as StdField with uid="DOMLBDQFOY",rtseq=91,rtrep=.f.,;
    cFormVar = "w_DESGCAU", cQueryName = "DESGCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 49473590,;
   bGlobalFont=.t.,;
    Height=21, Width=246, Left=253, Top=134, InputMask=replicate('X',35)

  add object oStr_1_46 as StdString with uid="JFNTXDMPWD",Visible=.t., Left=17, Top=9,;
    Alignment=1, Width=167, Height=18,;
    Caption="Num. generazione:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_47 as StdString with uid="MEXXGTUDGE",Visible=.t., Left=301, Top=41,;
    Alignment=1, Width=31, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="HTWWDEEDHW",Visible=.t., Left=495, Top=310,;
    Alignment=1, Width=61, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="CGGDXATUCC",Visible=.t., Left=45, Top=340,;
    Alignment=1, Width=135, Height=18,;
    Caption="Primo numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="IBNBCPVDZI",Visible=.t., Left=44, Top=370,;
    Alignment=1, Width=133, Height=15,;
    Caption="Data competenza IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="BSIIGTZWFW",Visible=.t., Left=279, Top=370,;
    Alignment=1, Width=169, Height=15,;
    Caption="Scadenze in data diversa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="DHUUGVQKER",Visible=.t., Left=28, Top=310,;
    Alignment=1, Width=151, Height=18,;
    Caption="Documento da generare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="KAYMDJYASG",Visible=.t., Left=34, Top=263,;
    Alignment=1, Width=178, Height=18,;
    Caption="Selezione documenti da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="HBGVLNYIVS",Visible=.t., Left=5, Top=176,;
    Alignment=0, Width=72, Height=15,;
    Caption="Selezioni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_61 as StdString with uid="ISKRIPAFXA",Visible=.t., Left=299, Top=262,;
    Alignment=1, Width=63, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="IWKNTCQVXG",Visible=.t., Left=250, Top=340,;
    Alignment=0, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="WGLNKPFNQI",Visible=.t., Left=468, Top=42,;
    Alignment=1, Width=43, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="ILPUWZZZOG",Visible=.t., Left=17, Top=41,;
    Alignment=1, Width=167, Height=18,;
    Caption="Periodicit� liquidazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="QEQQYDZBBW",Visible=.t., Left=17, Top=73,;
    Alignment=1, Width=167, Height=18,;
    Caption="Valorizzazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="VHLIVUMPLP",Visible=.t., Left=17, Top=103,;
    Alignment=1, Width=167, Height=18,;
    Caption="Inventario di riferimento:"  ;
  , bGlobalFont=.t.

  func oStr_1_67.mHide()
    with this.Parent.oContained
      return (.w_GPVALORI ='L')
    endwith
  endfunc

  add object oStr_1_68 as StdString with uid="VOOOWWXHXD",Visible=.t., Left=262, Top=103,;
    Alignment=1, Width=42, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  func oStr_1_68.mHide()
    with this.Parent.oContained
      return (.w_GPVALORI ='L')
    endwith
  endfunc

  add object oStr_1_69 as StdString with uid="DRCSBLYEGE",Visible=.t., Left=406, Top=103,;
    Alignment=1, Width=135, Height=18,;
    Caption="Listino di riferimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="UORQPACSKM",Visible=.t., Left=7, Top=229,;
    Alignment=1, Width=49, Height=18,;
    Caption="Intest.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_74 as StdString with uid="DWUHAQTTGU",Visible=.t., Left=3, Top=197,;
    Alignment=1, Width=53, Height=18,;
    Caption="Contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="ZYEWEDESQS",Visible=.t., Left=195, Top=197,;
    Alignment=1, Width=31, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="BCWEONNCTI",Visible=.t., Left=315, Top=197,;
    Alignment=1, Width=49, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_78 as StdString with uid="KWOCLHQOCW",Visible=.t., Left=5, Top=284,;
    Alignment=0, Width=157, Height=18,;
    Caption="Creazione documenti fiscali"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_80 as StdString with uid="MFKAGIKDOS",Visible=.t., Left=346, Top=340,;
    Alignment=1, Width=31, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="NTTGWVILGD",Visible=.t., Left=5, Top=393,;
    Alignment=0, Width=162, Height=18,;
    Caption="Creazione documenti interni"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_84 as StdString with uid="SMVDKHRHBV",Visible=.t., Left=24, Top=422,;
    Alignment=1, Width=151, Height=18,;
    Caption="Documento da generare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_129 as StdString with uid="CTDAZAFWEN",Visible=.t., Left=16, Top=134,;
    Alignment=1, Width=168, Height=18,;
    Caption="Gruppo causali di magazzino:"  ;
  , bGlobalFont=.t.

  add object oBox_1_59 as StdBox with uid="CLQPJBMMTF",left=2, top=190, width=621,height=2

  add object oBox_1_79 as StdBox with uid="BZZHVPBUJP",left=2, top=300, width=621,height=1

  add object oBox_1_82 as StdBox with uid="XHEIUMTFNH",left=2, top=407, width=485,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsgp_agp','GENPREMI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".GPSERIAL=GENPREMI.GPSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
