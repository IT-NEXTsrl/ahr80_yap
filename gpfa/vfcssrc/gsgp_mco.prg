* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsgp_mco                                                        *
*              Contratti a obiettivo/PFA                                       *
*                                                                              *
*      Author: Zucchetti - AT                                                  *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-04                                                      *
* Last revis.: 2014-05-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsgp_mco
* --- Da Menu' viene Passato il Parametro che identifica il Ciclo
PARAMETERS pFLVEAC
* --- Fine Area Manuale
return(createobject("tgsgp_mco"))

* --- Class definition
define class tgsgp_mco as StdTrsForm
  Top    = 2
  Left   = 8

  * --- Standard Properties
  Width  = 800
  Height = 464+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-05-29"
  HelpContextID=171339415
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=127

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  CCOMMAST_IDX = 0
  CCOMDETT_IDX = 0
  VALUTE_IDX = 0
  CONTI_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  LISTINI_IDX = 0
  AGENTI_IDX = 0
  GRUPPACQ_IDX = 0
  VOCIIVA_IDX = 0
  CONTROPA_IDX = 0
  cFile = "CCOMMAST"
  cFileDetail = "CCOMDETT"
  cKeySelect = "COCODICE"
  cKeyWhere  = "COCODICE=this.w_COCODICE"
  cKeyDetail  = "COCODICE=this.w_COCODICE"
  cKeyWhereODBC = '"COCODICE="+cp_ToStrODBC(this.w_COCODICE)';

  cKeyDetailWhereODBC = '"COCODICE="+cp_ToStrODBC(this.w_COCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"CCOMDETT.COCODICE="+cp_ToStrODBC(this.w_COCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CCOMDETT.CPROWORD '
  cPrg = "gsgp_mco"
  cComment = "Contratti a obiettivo/PFA"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 9
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_COCODICE = space(15)
  o_COCODICE = space(15)
  w_AZIENDA = space(10)
  w_CODESCRI = space(40)
  w_COFLGCIC = space(1)
  w_CODATREG = ctod('  /  /  ')
  o_CODATREG = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_COFLGSTA = space(2)
  w_COCODVAL = space(3)
  o_COCODVAL = space(3)
  w_CODATCAM = ctod('  /  /  ')
  o_CODATCAM = ctod('  /  /  ')
  w_COVALCAM = 0
  w_CODATINI = ctod('  /  /  ')
  w_CODATFIN = ctod('  /  /  ')
  w_COTIPINT = space(1)
  o_COTIPINT = space(1)
  w_COTIPINT = space(1)
  w_COCODAGE = space(5)
  w_COCODINT = space(15)
  w_COCODGRU = space(15)
  w_COPRMRIF = space(1)
  w_CODATGEN = ctod('  /  /  ')
  w_COTIPSTO = space(1)
  w_COPRIORI = 0
  w_COPRMDOC = space(1)
  w_COSCAPRE = space(1)
  w_COPRMDOC = space(1)
  w_COPRMLIQ = space(1)
  w_APFLGEXD = space(1)
  w_COCODIVA = space(5)
  w_COFLGEXD = space(1)
  w_COFLGVAL = space(1)
  w_COFLGLIS = space(1)
  w_COFLESAG = space(1)
  w_COFLGINC = 0
  w_COFLMODE = space(1)
  o_COFLMODE = space(1)
  w_CPROWORD = 0
  w_COKEYART = space(20)
  o_COKEYART = space(20)
  w_COCODART = space(20)
  o_COCODART = space(20)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_COUNIMIS = space(3)
  o_COUNIMIS = space(3)
  w_COUMALTE = 0
  w_COCALPRM = space(1)
  w_CODESINT = space(40)
  w_DESART = space(40)
  w_COFLGTIP = space(1)
  w_SIMBOLO = space(5)
  w_DESIVA = space(35)
  w_COPRICAL = space(10)
  w_COSCOGL1 = 0
  w_COSCOFL1 = 0
  w_COSCOGL2 = 0
  w_COSCOFL2 = 0
  w_COSCOGL3 = 0
  w_COSCOFL3 = 0
  w_COSCOGL4 = 0
  w_COSCOFL4 = 0
  w_COSCOGL5 = 0
  w_COSCOFL5 = 0
  w_COSCOGL6 = 0
  w_COSCOFL6 = 0
  w_COVALPUN = 0
  w_COVALMCH = 0
  o_COVALMCH = 0
  w_CONOTGEN = space(0)
  w_CONOTCON = space(0)
  w_CONOTMCH = space(0)
  w_SIMBOLO = space(5)
  w_CODI = space(15)
  w_CODESCRI = space(40)
  w_DATA = ctod('  /  /  ')
  w_DATAC = ctod('  /  /  ')
  w_DECUNI = 0
  o_DECUNI = 0
  w_DATOBSO = ctod('  /  /  ')
  w_SIMBOLO = space(5)
  w_CODI = space(15)
  w_CODESCRI = space(40)
  w_DATA = ctod('  /  /  ')
  w_DATAC = ctod('  /  /  ')
  w_FLFRAZ = space(1)
  w_MOLTIP = 0
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_COKEYSAL = space(40)
  w_COATTRIB = 0
  w_SHOWMAL = space(1)
  w_SHOWMAC = space(1)
  w_RESCHK = 0
  w_SHOWMPC = space(1)
  w_COFLGSTA = space(2)
  w_CODESGRU = space(40)
  w_CODESAGE = space(40)
  w_COVALFAT = 0
  w_COVALMCH = 0
  w_COSCOCOM = 0
  o_COSCOCOM = 0
  w_COVALPRE = 0
  o_COVALPRE = 0
  w_VALTCONT = 0
  w_COTOTPRE = 0
  o_COTOTPRE = 0
  w_INC1 = 0
  w_INC2 = 0
  w_INC3 = 0
  w_INC4 = 0
  w_INC5 = 0
  w_COTOTPUN = 0
  w_CALCPICP = 0
  w_CODAZI = space(5)
  w_LABE1 = space(20)
  w_LABE2 = space(20)
  w_LABE3 = space(20)
  w_LABE4 = space(20)
  w_LABE5 = space(20)
  w_LABE6 = space(20)
  w_TIPDOC = space(3)
  w_MVSERIAL = space(3)
  w_TIPDOC2 = space(3)
  w_MVSERIAL2 = space(3)
  w_RIFEST = space(26)
  w_TIPRIG = space(1)
  w_CAOVAL = 0
  w_DATEMU = ctod('  /  /  ')
  w_SRV = space(1)
  w_FLUMDIF = space(1)
  w_COPRMDOC = space(1)
  w_GPSERIAL = space(3)
  w_GENERA = space(4)
  w_AGCODFOR = space(15)
  w_CODKEY = space(20)
  w_COTIPPRE = space(1)

  * --- Children pointers
  GSGP_MAL = .NULL.
  GSGP_MAC = .NULL.
  GSGP_MPC = .NULL.
  GSGP_MDL = .NULL.
  w_DOCORI = .NULL.
  w_DOCDES = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsgp_mco
  *Variabile di lavoro indicante il ciclo
  pCiclo=' '
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
      return(lower('GSGP_MCO,'+this.pCiclo))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=4, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CCOMMAST','gsgp_mco')
    stdPageFrame::Init()
    *set procedure to GSGP_MAL additive
    *set procedure to GSGP_MAC additive
    *set procedure to GSGP_MPC additive
    *set procedure to GSGP_MDL additive
    with this
      .Pages(1).addobject("oPag","tgsgp_mcoPag1","gsgp_mco",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Contratto")
      .Pages(1).HelpContextID = 47303050
      .Pages(2).addobject("oPag","tgsgp_mcoPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Gestione commerciale")
      .Pages(2).HelpContextID = 217220542
      .Pages(3).addobject("oPag","tgsgp_mcoPag3")
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Dati consuntivi")
      .Pages(3).HelpContextID = 55414572
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCOCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSGP_MAL
    *release procedure GSGP_MAC
    *release procedure GSGP_MPC
    *release procedure GSGP_MDL
    * --- Area Manuale = Init Page Frame
    * --- gsgp_mco
    *Valorizzo variabile di lavoro
    WITH THIS.PARENT
     .pCiclo = pFlveac
     .cComment = ah_msgformat("Contratti a obiettivo/PFA ")  +IIF(.pCICLO = 'V', ah_msgformat("(Vendite)"),ah_msgformat("(Acquisti)"))
     .cAutoZoom = IIF(.pCiclo = 'V', 'GSGPVMCO','GSGPAMCO')
    ENDWITH
    
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_DOCORI = this.oPgFrm.Pages(3).oPag.DOCORI
    this.w_DOCDES = this.oPgFrm.Pages(3).oPag.DOCDES
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_DOCORI = .NULL.
      this.w_DOCDES = .NULL.
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[12]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='KEY_ARTI'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='UNIMIS'
    this.cWorkTables[6]='LISTINI'
    this.cWorkTables[7]='AGENTI'
    this.cWorkTables[8]='GRUPPACQ'
    this.cWorkTables[9]='VOCIIVA'
    this.cWorkTables[10]='CONTROPA'
    this.cWorkTables[11]='CCOMMAST'
    this.cWorkTables[12]='CCOMDETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(12))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CCOMMAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CCOMMAST_IDX,3]
  return

  function CreateChildren()
    this.GSGP_MAL = CREATEOBJECT('stdLazyChild',this,'GSGP_MAL')
    this.GSGP_MAC = CREATEOBJECT('stdLazyChild',this,'GSGP_MAC')
    this.GSGP_MPC = CREATEOBJECT('stdLazyChild',this,'GSGP_MPC')
    this.GSGP_MDL = CREATEOBJECT('stdLazyChild',this,'GSGP_MDL')
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSGP_MAL)
      this.GSGP_MAL.DestroyChildrenChain()
      this.GSGP_MAL=.NULL.
    endif
    if !ISNULL(this.GSGP_MAC)
      this.GSGP_MAC.DestroyChildrenChain()
      this.GSGP_MAC=.NULL.
    endif
    if !ISNULL(this.GSGP_MPC)
      this.GSGP_MPC.DestroyChildrenChain()
      this.GSGP_MPC=.NULL.
    endif
    if !ISNULL(this.GSGP_MDL)
      this.GSGP_MDL.DestroyChildrenChain()
      this.GSGP_MDL=.NULL.
    endif
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSGP_MAL.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSGP_MAC.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSGP_MPC.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSGP_MDL.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSGP_MAL.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSGP_MAC.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSGP_MPC.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSGP_MDL.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSGP_MAL.NewDocument()
    this.GSGP_MAC.NewDocument()
    this.GSGP_MPC.NewDocument()
    this.GSGP_MDL.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSGP_MAL.ChangeRow(this.cRowID+'      1',1;
             ,.w_COCODICE,"CLCODICE";
             )
      .GSGP_MAC.ChangeRow(this.cRowID+'      1',1;
             ,.w_COCODICE,"CCCODICE";
             )
      .GSGP_MPC.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_COCODICE,"CCCODICE";
             ,.w_CPROWNUM,"PCROWNUM";
             )
      .GSGP_MDL.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_COCODICE,"CA__GUID";
             ,.w_CPROWNUM,"CAROWRIF";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_COCODICE = NVL(COCODICE,space(15))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_15_joined
    link_1_15_joined=.f.
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_2_2_joined
    link_2_2_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from CCOMMAST where COCODICE=KeySet.COCODICE
    *
    i_nConn = i_TableProp[this.CCOMMAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCOMMAST_IDX,2],this.bLoadRecFilter,this.CCOMMAST_IDX,"gsgp_mco")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CCOMMAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CCOMMAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"CCOMDETT.","CCOMMAST.")
      i_cTable = i_cTable+' CCOMMAST '
      link_1_15_joined=this.AddJoinedLink_1_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'COCODICE',this.w_COCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_CODESINT = space(40)
        .w_SIMBOLO = space(5)
        .w_DESIVA = space(35)
        .w_SIMBOLO = space(5)
        .w_DECUNI = 0
        .w_DATOBSO = ctod("  /  /  ")
        .w_SIMBOLO = space(5)
        .w_SHOWMAL = 'S'
        .w_SHOWMAC = 'S'
        .w_RESCHK = 0
        .w_SHOWMPC = 'S'
        .w_CODESGRU = space(40)
        .w_CODESAGE = space(40)
        .w_CODAZI = i_CODAZI
        .w_LABE1 = space(20)
        .w_LABE2 = space(20)
        .w_LABE3 = space(20)
        .w_LABE4 = space(20)
        .w_LABE5 = space(20)
        .w_LABE6 = space(20)
        .w_CAOVAL = 0
        .w_DATEMU = ctod("  /  /  ")
        .w_AGCODFOR = space(15)
        .w_COCODICE = NVL(COCODICE,space(15))
        .w_AZIENDA = i_CODAZI
          .link_1_2('Load')
        .w_CODESCRI = NVL(CODESCRI,space(40))
        .w_COFLGCIC = NVL(COFLGCIC,space(1))
        .w_CODATREG = NVL(cp_ToDate(CODATREG),ctod("  /  /  "))
        .w_OBTEST = .w_CODATREG
        .w_COFLGSTA = NVL(COFLGSTA,space(2))
        .w_COCODVAL = NVL(COCODVAL,space(3))
          .link_1_8('Load')
        .w_CODATCAM = NVL(cp_ToDate(CODATCAM),ctod("  /  /  "))
        .w_COVALCAM = NVL(COVALCAM,0)
        .w_CODATINI = NVL(cp_ToDate(CODATINI),ctod("  /  /  "))
        .w_CODATFIN = NVL(cp_ToDate(CODATFIN),ctod("  /  /  "))
        .w_COTIPINT = NVL(COTIPINT,space(1))
        .w_COTIPINT = NVL(COTIPINT,space(1))
        .w_COCODAGE = NVL(COCODAGE,space(5))
          if link_1_15_joined
            this.w_COCODAGE = NVL(AGCODAGE115,NVL(this.w_COCODAGE,space(5)))
            this.w_CODESAGE = NVL(AGDESAGE115,space(40))
            this.w_AGCODFOR = NVL(AGCODFOR115,space(15))
          else
          .link_1_15('Load')
          endif
        .w_COCODINT = NVL(COCODINT,space(15))
          if link_1_16_joined
            this.w_COCODINT = NVL(ANCODICE116,NVL(this.w_COCODINT,space(15)))
            this.w_CODESINT = NVL(ANDESCRI116,space(40))
          else
          .link_1_16('Load')
          endif
        .w_COCODGRU = NVL(COCODGRU,space(15))
          .link_1_17('Load')
        .w_COPRMRIF = NVL(COPRMRIF,space(1))
        .w_CODATGEN = NVL(cp_ToDate(CODATGEN),ctod("  /  /  "))
        .w_COTIPSTO = NVL(COTIPSTO,space(1))
        .w_COPRIORI = NVL(COPRIORI,0)
        .w_COPRMDOC = NVL(COPRMDOC,space(1))
        .w_COSCAPRE = NVL(COSCAPRE,space(1))
        .w_COPRMDOC = NVL(COPRMDOC,space(1))
        .w_COPRMLIQ = NVL(COPRMLIQ,space(1))
        .w_APFLGEXD = .w_COFLGEXD
        .w_COCODIVA = NVL(COCODIVA,space(5))
          .link_1_27('Load')
        .w_COFLGEXD = NVL(COFLGEXD,space(1))
        .w_COFLGVAL = NVL(COFLGVAL,space(1))
        .w_COFLGLIS = NVL(COFLGLIS,space(1))
        .w_COFLESAG = NVL(COFLESAG,space(1))
        .w_COFLGINC = NVL(COFLGINC,0)
        .w_COFLMODE = NVL(COFLMODE,space(1))
        .w_COFLGTIP = NVL(COFLGTIP,space(1))
        .w_COPRICAL = NVL(COPRICAL,space(10))
        .w_COSCOGL1 = NVL(COSCOGL1,0)
        .w_COSCOFL1 = NVL(COSCOFL1,0)
        .w_COSCOGL2 = NVL(COSCOGL2,0)
        .w_COSCOFL2 = NVL(COSCOFL2,0)
        .w_COSCOGL3 = NVL(COSCOGL3,0)
        .w_COSCOFL3 = NVL(COSCOFL3,0)
        .w_COSCOGL4 = NVL(COSCOGL4,0)
        .w_COSCOFL4 = NVL(COSCOFL4,0)
        .w_COSCOGL5 = NVL(COSCOGL5,0)
        .w_COSCOFL5 = NVL(COSCOFL5,0)
        .w_COSCOGL6 = NVL(COSCOGL6,0)
        .w_COSCOFL6 = NVL(COSCOFL6,0)
        .w_COVALPUN = NVL(COVALPUN,0)
        .w_COVALMCH = NVL(COVALMCH,0)
        .w_CONOTGEN = NVL(CONOTGEN,space(0))
        .w_CONOTCON = NVL(CONOTCON,space(0))
        .w_CONOTMCH = NVL(CONOTMCH,space(0))
        .w_CODI = .w_COCODICE
        .w_CODESCRI = NVL(CODESCRI,space(40))
        .w_DATA = .w_CODATREG
        .w_DATAC = .w_CODATGEN
        .w_CODI = .w_COCODICE
        .w_CODESCRI = NVL(CODESCRI,space(40))
        .w_DATA = .w_CODATREG
        .w_DATAC = .w_CODATGEN
        .w_COFLGSTA = NVL(COFLGSTA,space(2))
        .oPgFrm.Page3.oPag.DOCORI.Calculate()
        .oPgFrm.Page3.oPag.DOCDES.Calculate()
        .w_COVALFAT = NVL(COVALFAT,0)
        .w_COVALMCH = NVL(COVALMCH,0)
        .w_COSCOCOM = NVL(COSCOCOM,0)
        .w_COVALPRE = NVL(COVALPRE,0)
        .w_VALTCONT = .w_COVALMCH+.w_COTOTPRE
        .w_COTOTPRE = NVL(COTOTPRE,0)
        .w_INC1 = IIF(.w_COVALFAT=0,0,cp_ROUND(.w_COVALMCH/.w_COVALFAT * 100,2))
        .w_INC2 = cp_ROUND(.w_COSCOCOM/.w_COVALFAT * 100,2)
        .w_INC3 = cp_ROUND(.w_COVALPRE/.w_COVALFAT * 100,2)
        .w_INC4 = IIF(.w_COVALFAT=0,0,cp_ROUND(.w_COTOTPRE/.w_COVALFAT * 100,2))
        .w_INC5 = IIF(.w_COVALFAT=0,0,cp_ROUND(.w_VALTCONT/.w_COVALFAT * 100,2))
        .w_COTOTPUN = NVL(COTOTPUN,0)
        .w_CALCPICP = DEFPIP(.w_DECUNI)
          .link_4_43('Load')
        .oPgFrm.Page2.oPag.oObj_4_50.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE1),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_51.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE2),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_52.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE3),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_53.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE4),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_54.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE5),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_55.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE6),20)+':')
        .w_TIPDOC = 'Z'+.w_COFLGCIC+.w_DocOri.getVar('MVCLADOC')
        .w_MVSERIAL = .w_DocOri.getVar('MVSERIAL')
        .w_TIPDOC2 = 'Y'+.w_DocDes.getVar('MVFLVEAC')+.w_DocDes.getVar('MVCLADOC')
        .w_MVSERIAL2 = .w_DocDes.getVar('MVSERIAL')
        .w_RIFEST = 'Contratto: ' + alltrim(.w_COCODICE)
        .w_COPRMDOC = NVL(COPRMDOC,space(1))
        .w_GPSERIAL = .w_DocDes.getVar('MVGENPRE')
        .w_GENERA = .w_COFLGCIC+'GPR'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_COTIPPRE = NVL(COTIPPRE,space(1))
        cp_LoadRecExtFlds(this,'CCOMMAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from CCOMDETT where COCODICE=KeySet.COCODICE
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.CCOMDETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCOMDETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('CCOMDETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "CCOMDETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" CCOMDETT"
        link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'COCODICE',this.w_COCODICE  )
        select * from (i_cTable) CCOMDETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_UNMIS1 = space(3)
          .w_UNMIS2 = space(3)
          .w_UNMIS3 = space(3)
          .w_DESART = space(40)
          .w_FLFRAZ = space(1)
          .w_MOLTIP = 0
          .w_OPERAT = space(1)
          .w_OPERA3 = space(1)
          .w_MOLTI3 = 0
          .w_TIPRIG = space(1)
          .w_SRV = space(1)
          .w_FLUMDIF = space(1)
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_COKEYART = NVL(COKEYART,space(20))
          if link_2_2_joined
            this.w_COKEYART = NVL(CACODICE202,NVL(this.w_COKEYART,space(20)))
            this.w_COCODART = NVL(CACODART202,space(20))
            this.w_DESART = NVL(CADESART202,space(40))
            this.w_UNMIS3 = NVL(CAUNIMIS202,space(3))
            this.w_OPERA3 = NVL(CAOPERAT202,space(1))
            this.w_MOLTI3 = NVL(CAMOLTIP202,0)
            this.w_TIPRIG = NVL(CA__TIPO202,space(1))
          else
          .link_2_2('Load')
          endif
          .w_COCODART = NVL(COCODART,space(20))
          if link_2_3_joined
            this.w_COCODART = NVL(ARCODART203,NVL(this.w_COCODART,space(20)))
            this.w_UNMIS1 = NVL(ARUNMIS1203,space(3))
            this.w_MOLTIP = NVL(ARMOLTIP203,0)
            this.w_UNMIS2 = NVL(ARUNMIS2203,space(3))
            this.w_OPERAT = NVL(AROPERAT203,space(1))
            this.w_FLUMDIF = NVL(ARFLSERG203,space(1))
          else
          .link_2_3('Load')
          endif
          .w_COUNIMIS = NVL(COUNIMIS,space(3))
          if link_2_7_joined
            this.w_COUNIMIS = NVL(UMCODICE207,NVL(this.w_COUNIMIS,space(3)))
            this.w_FLFRAZ = NVL(UMFLFRAZ207,space(1))
          else
          .link_2_7('Load')
          endif
          .w_COUMALTE = NVL(COUMALTE,0)
          .w_COCALPRM = NVL(COCALPRM,space(1))
          .w_COKEYSAL = NVL(COKEYSAL,space(40))
          .w_COATTRIB = NVL(COATTRIB,0)
        .w_CODKEY = .w_COCODART
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_AZIENDA = i_CODAZI
        .w_OBTEST = .w_CODATREG
        .w_APFLGEXD = .w_COFLGEXD
        .w_CODI = .w_COCODICE
        .w_DATA = .w_CODATREG
        .w_DATAC = .w_CODATGEN
        .w_CODI = .w_COCODICE
        .w_DATA = .w_CODATREG
        .w_DATAC = .w_CODATGEN
        .oPgFrm.Page3.oPag.DOCORI.Calculate()
        .oPgFrm.Page3.oPag.DOCDES.Calculate()
        .w_VALTCONT = .w_COVALMCH+.w_COTOTPRE
        .w_INC1 = IIF(.w_COVALFAT=0,0,cp_ROUND(.w_COVALMCH/.w_COVALFAT * 100,2))
        .w_INC2 = cp_ROUND(.w_COSCOCOM/.w_COVALFAT * 100,2)
        .w_INC3 = cp_ROUND(.w_COVALPRE/.w_COVALFAT * 100,2)
        .w_INC4 = IIF(.w_COVALFAT=0,0,cp_ROUND(.w_COTOTPRE/.w_COVALFAT * 100,2))
        .w_INC5 = IIF(.w_COVALFAT=0,0,cp_ROUND(.w_VALTCONT/.w_COVALFAT * 100,2))
        .w_CALCPICP = DEFPIP(.w_DECUNI)
        .oPgFrm.Page2.oPag.oObj_4_50.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE1),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_51.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE2),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_52.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE3),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_53.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE4),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_54.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE5),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_55.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE6),20)+':')
        .w_TIPDOC = 'Z'+.w_COFLGCIC+.w_DocOri.getVar('MVCLADOC')
        .w_MVSERIAL = .w_DocOri.getVar('MVSERIAL')
        .w_TIPDOC2 = 'Y'+.w_DocDes.getVar('MVFLVEAC')+.w_DocDes.getVar('MVCLADOC')
        .w_MVSERIAL2 = .w_DocDes.getVar('MVSERIAL')
        .w_RIFEST = 'Contratto: ' + alltrim(.w_COCODICE)
        .w_GPSERIAL = .w_DocDes.getVar('MVGENPRE')
        .w_GENERA = .w_COFLGCIC+'GPR'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page3.oPag.oBtn_5_49.enabled = .oPgFrm.Page3.oPag.oBtn_5_49.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_81.enabled = .oPgFrm.Page1.oPag.oBtn_1_81.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsgp_mco
    if this.w_COFLGCIC <> this.pCiclo Or this.w_COFLGTIP <> 'O'
      this.BlankRec()
    endif
    
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_COCODICE=space(15)
      .w_AZIENDA=space(10)
      .w_CODESCRI=space(40)
      .w_COFLGCIC=space(1)
      .w_CODATREG=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_COFLGSTA=space(2)
      .w_COCODVAL=space(3)
      .w_CODATCAM=ctod("  /  /  ")
      .w_COVALCAM=0
      .w_CODATINI=ctod("  /  /  ")
      .w_CODATFIN=ctod("  /  /  ")
      .w_COTIPINT=space(1)
      .w_COTIPINT=space(1)
      .w_COCODAGE=space(5)
      .w_COCODINT=space(15)
      .w_COCODGRU=space(15)
      .w_COPRMRIF=space(1)
      .w_CODATGEN=ctod("  /  /  ")
      .w_COTIPSTO=space(1)
      .w_COPRIORI=0
      .w_COPRMDOC=space(1)
      .w_COSCAPRE=space(1)
      .w_COPRMDOC=space(1)
      .w_COPRMLIQ=space(1)
      .w_APFLGEXD=space(1)
      .w_COCODIVA=space(5)
      .w_COFLGEXD=space(1)
      .w_COFLGVAL=space(1)
      .w_COFLGLIS=space(1)
      .w_COFLESAG=space(1)
      .w_COFLGINC=0
      .w_COFLMODE=space(1)
      .w_CPROWORD=10
      .w_COKEYART=space(20)
      .w_COCODART=space(20)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_UNMIS3=space(3)
      .w_COUNIMIS=space(3)
      .w_COUMALTE=0
      .w_COCALPRM=space(1)
      .w_CODESINT=space(40)
      .w_DESART=space(40)
      .w_COFLGTIP=space(1)
      .w_SIMBOLO=space(5)
      .w_DESIVA=space(35)
      .w_COPRICAL=space(10)
      .w_COSCOGL1=0
      .w_COSCOFL1=0
      .w_COSCOGL2=0
      .w_COSCOFL2=0
      .w_COSCOGL3=0
      .w_COSCOFL3=0
      .w_COSCOGL4=0
      .w_COSCOFL4=0
      .w_COSCOGL5=0
      .w_COSCOFL5=0
      .w_COSCOGL6=0
      .w_COSCOFL6=0
      .w_COVALPUN=0
      .w_COVALMCH=0
      .w_CONOTGEN=space(0)
      .w_CONOTCON=space(0)
      .w_CONOTMCH=space(0)
      .w_SIMBOLO=space(5)
      .w_CODI=space(15)
      .w_CODESCRI=space(40)
      .w_DATA=ctod("  /  /  ")
      .w_DATAC=ctod("  /  /  ")
      .w_DECUNI=0
      .w_DATOBSO=ctod("  /  /  ")
      .w_SIMBOLO=space(5)
      .w_CODI=space(15)
      .w_CODESCRI=space(40)
      .w_DATA=ctod("  /  /  ")
      .w_DATAC=ctod("  /  /  ")
      .w_FLFRAZ=space(1)
      .w_MOLTIP=0
      .w_OPERAT=space(1)
      .w_OPERA3=space(1)
      .w_MOLTI3=0
      .w_COKEYSAL=space(40)
      .w_COATTRIB=0
      .w_SHOWMAL=space(1)
      .w_SHOWMAC=space(1)
      .w_RESCHK=0
      .w_SHOWMPC=space(1)
      .w_COFLGSTA=space(2)
      .w_CODESGRU=space(40)
      .w_CODESAGE=space(40)
      .w_COVALFAT=0
      .w_COVALMCH=0
      .w_COSCOCOM=0
      .w_COVALPRE=0
      .w_VALTCONT=0
      .w_COTOTPRE=0
      .w_INC1=0
      .w_INC2=0
      .w_INC3=0
      .w_INC4=0
      .w_INC5=0
      .w_COTOTPUN=0
      .w_CALCPICP=0
      .w_CODAZI=space(5)
      .w_LABE1=space(20)
      .w_LABE2=space(20)
      .w_LABE3=space(20)
      .w_LABE4=space(20)
      .w_LABE5=space(20)
      .w_LABE6=space(20)
      .w_TIPDOC=space(3)
      .w_MVSERIAL=space(3)
      .w_TIPDOC2=space(3)
      .w_MVSERIAL2=space(3)
      .w_RIFEST=space(26)
      .w_TIPRIG=space(1)
      .w_CAOVAL=0
      .w_DATEMU=ctod("  /  /  ")
      .w_SRV=space(1)
      .w_FLUMDIF=space(1)
      .w_COPRMDOC=space(1)
      .w_GPSERIAL=space(3)
      .w_GENERA=space(4)
      .w_AGCODFOR=space(15)
      .w_CODKEY=space(20)
      .w_COTIPPRE=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_AZIENDA))
         .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        .w_COFLGCIC = .pCiclo
        .w_CODATREG = i_datsys
        .w_OBTEST = .w_CODATREG
        .w_COFLGSTA = IIF(EMPTY(.w_COFLGSTA) OR .w_COFLMODE='S', 'S', .w_COFLGSTA )
        .w_COCODVAL = g_PERVAL
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_COCODVAL))
         .link_1_8('Full')
        endif
        .w_CODATCAM = i_datsys
        .w_COVALCAM = GETCAM(.w_COCODVAL, .w_CODATCAM, 7)
        .w_CODATINI = I_DATSYS
        .w_CODATFIN = Cp_CharToDate('30-12-2999')
        .w_COTIPINT = iif(.w_COFLGCIC='V','C','F')
        .w_COTIPINT = iif(.w_COFLGCIC='V','C','F')
        .w_COCODAGE = ''
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_COCODAGE))
         .link_1_15('Full')
        endif
        .w_COCODINT = ''
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_COCODINT))
         .link_1_16('Full')
        endif
        .w_COCODGRU = ''
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_COCODGRU))
         .link_1_17('Full')
        endif
        .w_COPRMRIF = 'U'
        .DoRTCalc(19,19,.f.)
        .w_COTIPSTO = 'F'
        .w_COPRIORI = 1
        .w_COPRMDOC = IIF(.w_COTIPINT='A','A',IIF(.w_COFLGCIC='V' ,'P','F'))
        .w_COSCAPRE = 'S'
        .w_COPRMDOC = IIF(.w_COTIPINT='A','A',IIF(.w_COFLGCIC='V' ,'P','F'))
        .w_COPRMLIQ = 'F'
        .w_APFLGEXD = .w_COFLGEXD
        .w_COCODIVA = .w_COCODIVA
        .DoRTCalc(27,27,.f.)
        if not(empty(.w_COCODIVA))
         .link_1_27('Full')
        endif
        .w_COFLGEXD = 'S'
        .w_COFLGVAL = 'S'
        .w_COFLGLIS = 'S'
        .w_COFLESAG = 'N'
        .DoRTCalc(32,32,.f.)
        .w_COFLMODE = 'N'
        .DoRTCalc(34,35,.f.)
        if not(empty(.w_COKEYART))
         .link_2_2('Full')
        endif
        .DoRTCalc(36,36,.f.)
        if not(empty(.w_COCODART))
         .link_2_3('Full')
        endif
        .DoRTCalc(37,39,.f.)
        .w_COUNIMIS = iif(! EMPTY(nvl(.w_COKEYART,'')), IIF(NOT EMPTY(.w_UNMIS3), .w_UNMIS3, .w_UNMIS1), '')
        .DoRTCalc(40,40,.f.)
        if not(empty(.w_COUNIMIS))
         .link_2_7('Full')
        endif
        .w_COUMALTE = 0
        .w_COCALPRM = 'Q'
        .DoRTCalc(43,44,.f.)
        .w_COFLGTIP = 'O'
        .DoRTCalc(46,49,.f.)
        .w_COSCOFL1 = 0
        .DoRTCalc(51,51,.f.)
        .w_COSCOFL2 = 0
        .DoRTCalc(53,53,.f.)
        .w_COSCOFL3 = 0
        .DoRTCalc(55,55,.f.)
        .w_COSCOFL4 = 0
        .DoRTCalc(57,57,.f.)
        .w_COSCOFL5 = 0
        .DoRTCalc(59,59,.f.)
        .w_COSCOFL6 = 0
        .DoRTCalc(61,66,.f.)
        .w_CODI = .w_COCODICE
        .DoRTCalc(68,68,.f.)
        .w_DATA = .w_CODATREG
        .w_DATAC = .w_CODATGEN
        .DoRTCalc(71,73,.f.)
        .w_CODI = .w_COCODICE
        .DoRTCalc(75,75,.f.)
        .w_DATA = .w_CODATREG
        .w_DATAC = .w_CODATGEN
        .DoRTCalc(78,82,.f.)
        .w_COKEYSAL = .w_COCODART
        .w_COATTRIB = 2
        .w_SHOWMAL = 'S'
        .w_SHOWMAC = 'S'
        .w_RESCHK = 0
        .w_SHOWMPC = 'S'
        .oPgFrm.Page3.oPag.DOCORI.Calculate()
        .oPgFrm.Page3.oPag.DOCDES.Calculate()
        .DoRTCalc(89,95,.f.)
        .w_VALTCONT = .w_COVALMCH+.w_COTOTPRE
        .w_COTOTPRE = .w_COSCOCOM+.w_COVALPRE
        .w_INC1 = IIF(.w_COVALFAT=0,0,cp_ROUND(.w_COVALMCH/.w_COVALFAT * 100,2))
        .w_INC2 = cp_ROUND(.w_COSCOCOM/.w_COVALFAT * 100,2)
        .w_INC3 = cp_ROUND(.w_COVALPRE/.w_COVALFAT * 100,2)
        .w_INC4 = IIF(.w_COVALFAT=0,0,cp_ROUND(.w_COTOTPRE/.w_COVALFAT * 100,2))
        .w_INC5 = IIF(.w_COVALFAT=0,0,cp_ROUND(.w_VALTCONT/.w_COVALFAT * 100,2))
        .DoRTCalc(103,103,.f.)
        .w_CALCPICP = DEFPIP(.w_DECUNI)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(105,105,.f.)
        if not(empty(.w_CODAZI))
         .link_4_43('Full')
        endif
        .oPgFrm.Page2.oPag.oObj_4_50.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE1),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_51.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE2),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_52.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE3),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_53.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE4),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_54.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE5),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_55.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE6),20)+':')
        .DoRTCalc(106,111,.f.)
        .w_TIPDOC = 'Z'+.w_COFLGCIC+.w_DocOri.getVar('MVCLADOC')
        .w_MVSERIAL = .w_DocOri.getVar('MVSERIAL')
        .w_TIPDOC2 = 'Y'+.w_DocDes.getVar('MVFLVEAC')+.w_DocDes.getVar('MVCLADOC')
        .w_MVSERIAL2 = .w_DocDes.getVar('MVSERIAL')
        .w_RIFEST = 'Contratto: ' + alltrim(.w_COCODICE)
        .DoRTCalc(117,121,.f.)
        .w_COPRMDOC = IIF(.w_COTIPINT='A','A',IIF(.w_COFLGCIC='V' ,'P','F'))
        .w_GPSERIAL = .w_DocDes.getVar('MVGENPRE')
        .w_GENERA = .w_COFLGCIC+'GPR'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(125,125,.f.)
        .w_CODKEY = .w_COCODART
        .w_COTIPPRE = " "
      endif
    endwith
    cp_BlankRecExtFlds(this,'CCOMMAST')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page3.oPag.oBtn_5_49.enabled = this.oPgFrm.Page3.oPag.oBtn_5_49.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_81.enabled = this.oPgFrm.Page1.oPag.oBtn_1_81.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsgp_mco
    if Upper(This.cFunction)<>"FILTER" AND (this.w_COFLGCIC<>this.pCiclo Or this.w_COFLGTIP <> 'O')
      this.BlankRec()
    Endif
    
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCOCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oCODESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oCODATREG_1_5.enabled = i_bVal
      .Page1.oPag.oCOFLGSTA_1_7.enabled = i_bVal
      .Page1.oPag.oCOCODVAL_1_8.enabled = i_bVal
      .Page1.oPag.oCODATCAM_1_9.enabled = i_bVal
      .Page1.oPag.oCOVALCAM_1_10.enabled = i_bVal
      .Page1.oPag.oCODATINI_1_11.enabled = i_bVal
      .Page1.oPag.oCODATFIN_1_12.enabled = i_bVal
      .Page1.oPag.oCOTIPINT_1_13.enabled = i_bVal
      .Page1.oPag.oCOTIPINT_1_14.enabled = i_bVal
      .Page1.oPag.oCOCODAGE_1_15.enabled = i_bVal
      .Page1.oPag.oCOCODINT_1_16.enabled = i_bVal
      .Page1.oPag.oCOCODGRU_1_17.enabled = i_bVal
      .Page1.oPag.oCOTIPSTO_1_20.enabled = i_bVal
      .Page1.oPag.oCOPRIORI_1_21.enabled = i_bVal
      .Page1.oPag.oCOPRMDOC_1_22.enabled = i_bVal
      .Page1.oPag.oCOSCAPRE_1_23.enabled = i_bVal
      .Page1.oPag.oCOPRMDOC_1_24.enabled = i_bVal
      .Page1.oPag.oCOPRMLIQ_1_25.enabled = i_bVal
      .Page1.oPag.oCOFLGEXD_1_28.enabled = i_bVal
      .Page1.oPag.oCOFLGVAL_1_29.enabled = i_bVal
      .Page1.oPag.oCOFLGLIS_1_30.enabled = i_bVal
      .Page1.oPag.oCOFLESAG_1_31.enabled = i_bVal
      .Page1.oPag.oCOFLGINC_1_32.enabled = i_bVal
      .Page1.oPag.oCOFLMODE_1_33.enabled = i_bVal
      .Page2.oPag.oCOSCOGL1_4_1.enabled = i_bVal
      .Page2.oPag.oCOSCOFL1_4_2.enabled = i_bVal
      .Page2.oPag.oCOSCOGL2_4_3.enabled = i_bVal
      .Page2.oPag.oCOSCOFL2_4_4.enabled = i_bVal
      .Page2.oPag.oCOSCOGL3_4_5.enabled = i_bVal
      .Page2.oPag.oCOSCOFL3_4_6.enabled = i_bVal
      .Page2.oPag.oCOSCOGL4_4_7.enabled = i_bVal
      .Page2.oPag.oCOSCOFL4_4_8.enabled = i_bVal
      .Page2.oPag.oCOSCOGL5_4_9.enabled = i_bVal
      .Page2.oPag.oCOSCOFL5_4_10.enabled = i_bVal
      .Page2.oPag.oCOSCOGL6_4_11.enabled = i_bVal
      .Page2.oPag.oCOSCOFL6_4_12.enabled = i_bVal
      .Page2.oPag.oCOVALPUN_4_13.enabled = i_bVal
      .Page2.oPag.oCOVALMCH_4_14.enabled = i_bVal
      .Page2.oPag.oCONOTGEN_4_15.enabled = i_bVal
      .Page2.oPag.oCONOTCON_4_16.enabled = i_bVal
      .Page2.oPag.oCONOTMCH_4_17.enabled = i_bVal
      .Page3.oPag.oBtn_5_49.enabled = .Page3.oPag.oBtn_5_49.mCond()
      .Page1.oPag.oBtn_1_81.enabled = .Page1.oPag.oBtn_1_81.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCOCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCOCODICE_1_1.enabled = .t.
        .Page1.oPag.oCODATREG_1_5.enabled = .t.
        .Page1.oPag.oCODATINI_1_11.enabled = .t.
        .Page1.oPag.oCODATFIN_1_12.enabled = .t.
      endif
    endwith
    this.GSGP_MAL.SetStatus(i_cOp)
    this.GSGP_MAC.SetStatus(i_cOp)
    this.GSGP_MPC.SetStatus(i_cOp)
    this.GSGP_MDL.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CCOMMAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSGP_MAL.SetChildrenStatus(i_cOp)
  *  this.GSGP_MAC.SetChildrenStatus(i_cOp)
  *  this.GSGP_MPC.SetChildrenStatus(i_cOp)
  *  this.GSGP_MDL.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CCOMMAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODICE,"COCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODESCRI,"CODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFLGCIC,"COFLGCIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATREG,"CODATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFLGSTA,"COFLGSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODVAL,"COCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATCAM,"CODATCAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COVALCAM,"COVALCAM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATINI,"CODATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATFIN,"CODATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPINT,"COTIPINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPINT,"COTIPINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODAGE,"COCODAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODINT,"COCODINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODGRU,"COCODGRU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPRMRIF,"COPRMRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODATGEN,"CODATGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPSTO,"COTIPSTO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPRIORI,"COPRIORI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPRMDOC,"COPRMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSCAPRE,"COSCAPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPRMDOC,"COPRMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPRMLIQ,"COPRMLIQ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODIVA,"COCODIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFLGEXD,"COFLGEXD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFLGVAL,"COFLGVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFLGLIS,"COFLGLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFLESAG,"COFLESAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFLGINC,"COFLGINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFLMODE,"COFLMODE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFLGTIP,"COFLGTIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPRICAL,"COPRICAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSCOGL1,"COSCOGL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSCOFL1,"COSCOFL1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSCOGL2,"COSCOGL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSCOFL2,"COSCOFL2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSCOGL3,"COSCOGL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSCOFL3,"COSCOFL3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSCOGL4,"COSCOGL4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSCOFL4,"COSCOFL4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSCOGL5,"COSCOGL5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSCOFL5,"COSCOFL5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSCOGL6,"COSCOGL6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSCOFL6,"COSCOFL6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COVALPUN,"COVALPUN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COVALMCH,"COVALMCH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CONOTGEN,"CONOTGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CONOTCON,"CONOTCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CONOTMCH,"CONOTMCH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODESCRI,"CODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CODESCRI,"CODESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COFLGSTA,"COFLGSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COVALFAT,"COVALFAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COVALMCH,"COVALMCH",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COSCOCOM,"COSCOCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COVALPRE,"COVALPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTOTPRE,"COTOTPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTOTPUN,"COTOTPUN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPRMDOC,"COPRMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COTIPPRE,"COTIPPRE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CCOMMAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCOMMAST_IDX,2])
    i_lTable = "CCOMMAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CCOMMAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSGP_SCO with this
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(6);
      ,t_COKEYART C(20);
      ,t_COUNIMIS C(3);
      ,t_COUMALTE N(3);
      ,t_COCALPRM N(3);
      ,t_DESART C(40);
      ,t_COATTRIB N(3);
      ,CPROWNUM N(10);
      ,t_COCODART C(20);
      ,t_UNMIS1 C(3);
      ,t_UNMIS2 C(3);
      ,t_UNMIS3 C(3);
      ,t_FLFRAZ C(1);
      ,t_MOLTIP N(10,5);
      ,t_OPERAT C(1);
      ,t_OPERA3 C(1);
      ,t_MOLTI3 N(10,4);
      ,t_COKEYSAL C(40);
      ,t_TIPRIG C(1);
      ,t_SRV C(1);
      ,t_FLUMDIF C(1);
      ,t_CODKEY C(20);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsgp_mcobodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOKEYART_2_2.controlsource=this.cTrsName+'.t_COKEYART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOUNIMIS_2_7.controlsource=this.cTrsName+'.t_COUNIMIS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOUMALTE_2_8.controlsource=this.cTrsName+'.t_COUMALTE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOCALPRM_2_9.controlsource=this.cTrsName+'.t_COCALPRM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESART_2_12.controlsource=this.cTrsName+'.t_DESART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCOATTRIB_2_19.controlsource=this.cTrsName+'.t_COATTRIB'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(46)
    this.AddVLine(215)
    this.AddVLine(483)
    this.AddVLine(540)
    this.AddVLine(578)
    this.AddVLine(660)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CCOMMAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCOMMAST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CCOMMAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CCOMMAST')
        i_extval=cp_InsertValODBCExtFlds(this,'CCOMMAST')
        local i_cFld
        i_cFld=" "+;
                  "(COCODICE,CODESCRI,COFLGCIC,CODATREG,COFLGSTA"+;
                  ",COCODVAL,CODATCAM,COVALCAM,CODATINI,CODATFIN"+;
                  ",COTIPINT,COCODAGE,COCODINT,COCODGRU,COPRMRIF"+;
                  ",CODATGEN,COTIPSTO,COPRIORI,COPRMDOC,COSCAPRE"+;
                  ",COPRMLIQ,COCODIVA,COFLGEXD,COFLGVAL,COFLGLIS"+;
                  ",COFLESAG,COFLGINC,COFLMODE,COFLGTIP,COPRICAL"+;
                  ",COSCOGL1,COSCOFL1,COSCOGL2,COSCOFL2,COSCOGL3"+;
                  ",COSCOFL3,COSCOGL4,COSCOFL4,COSCOGL5,COSCOFL5"+;
                  ",COSCOGL6,COSCOFL6,COVALPUN,COVALMCH,CONOTGEN"+;
                  ",CONOTCON,CONOTMCH,COVALFAT,COSCOCOM,COVALPRE"+;
                  ",COTOTPRE,COTOTPUN,COTIPPRE"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_COCODICE)+;
                    ","+cp_ToStrODBC(this.w_CODESCRI)+;
                    ","+cp_ToStrODBC(this.w_COFLGCIC)+;
                    ","+cp_ToStrODBC(this.w_CODATREG)+;
                    ","+cp_ToStrODBC(this.w_COFLGSTA)+;
                    ","+cp_ToStrODBCNull(this.w_COCODVAL)+;
                    ","+cp_ToStrODBC(this.w_CODATCAM)+;
                    ","+cp_ToStrODBC(this.w_COVALCAM)+;
                    ","+cp_ToStrODBC(this.w_CODATINI)+;
                    ","+cp_ToStrODBC(this.w_CODATFIN)+;
                    ","+cp_ToStrODBC(this.w_COTIPINT)+;
                    ","+cp_ToStrODBCNull(this.w_COCODAGE)+;
                    ","+cp_ToStrODBCNull(this.w_COCODINT)+;
                    ","+cp_ToStrODBCNull(this.w_COCODGRU)+;
                    ","+cp_ToStrODBC(this.w_COPRMRIF)+;
                    ","+cp_ToStrODBC(this.w_CODATGEN)+;
                    ","+cp_ToStrODBC(this.w_COTIPSTO)+;
                    ","+cp_ToStrODBC(this.w_COPRIORI)+;
                    ","+cp_ToStrODBC(this.w_COPRMDOC)+;
                    ","+cp_ToStrODBC(this.w_COSCAPRE)+;
                    ","+cp_ToStrODBC(this.w_COPRMLIQ)+;
                    ","+cp_ToStrODBCNull(this.w_COCODIVA)+;
                    ","+cp_ToStrODBC(this.w_COFLGEXD)+;
                    ","+cp_ToStrODBC(this.w_COFLGVAL)+;
                    ","+cp_ToStrODBC(this.w_COFLGLIS)+;
                    ","+cp_ToStrODBC(this.w_COFLESAG)+;
                    ","+cp_ToStrODBC(this.w_COFLGINC)+;
                    ","+cp_ToStrODBC(this.w_COFLMODE)+;
                    ","+cp_ToStrODBC(this.w_COFLGTIP)+;
                    ","+cp_ToStrODBC(this.w_COPRICAL)+;
                    ","+cp_ToStrODBC(this.w_COSCOGL1)+;
                    ","+cp_ToStrODBC(this.w_COSCOFL1)+;
                    ","+cp_ToStrODBC(this.w_COSCOGL2)+;
                    ","+cp_ToStrODBC(this.w_COSCOFL2)+;
                    ","+cp_ToStrODBC(this.w_COSCOGL3)+;
                    ","+cp_ToStrODBC(this.w_COSCOFL3)+;
                    ","+cp_ToStrODBC(this.w_COSCOGL4)+;
                    ","+cp_ToStrODBC(this.w_COSCOFL4)+;
                    ","+cp_ToStrODBC(this.w_COSCOGL5)+;
                    ","+cp_ToStrODBC(this.w_COSCOFL5)+;
                    ","+cp_ToStrODBC(this.w_COSCOGL6)+;
                    ","+cp_ToStrODBC(this.w_COSCOFL6)+;
                    ","+cp_ToStrODBC(this.w_COVALPUN)+;
                    ","+cp_ToStrODBC(this.w_COVALMCH)+;
                    ","+cp_ToStrODBC(this.w_CONOTGEN)+;
                    ","+cp_ToStrODBC(this.w_CONOTCON)+;
                    ","+cp_ToStrODBC(this.w_CONOTMCH)+;
                    ","+cp_ToStrODBC(this.w_COVALFAT)+;
                    ","+cp_ToStrODBC(this.w_COSCOCOM)+;
                    ","+cp_ToStrODBC(this.w_COVALPRE)+;
                    ","+cp_ToStrODBC(this.w_COTOTPRE)+;
                    ","+cp_ToStrODBC(this.w_COTOTPUN)+;
                    ","+cp_ToStrODBC(this.w_COTIPPRE)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CCOMMAST')
        i_extval=cp_InsertValVFPExtFlds(this,'CCOMMAST')
        cp_CheckDeletedKey(i_cTable,0,'COCODICE',this.w_COCODICE)
        INSERT INTO (i_cTable);
              (COCODICE,CODESCRI,COFLGCIC,CODATREG,COFLGSTA,COCODVAL,CODATCAM,COVALCAM,CODATINI,CODATFIN,COTIPINT,COCODAGE,COCODINT,COCODGRU,COPRMRIF,CODATGEN,COTIPSTO,COPRIORI,COPRMDOC,COSCAPRE,COPRMLIQ,COCODIVA,COFLGEXD,COFLGVAL,COFLGLIS,COFLESAG,COFLGINC,COFLMODE,COFLGTIP,COPRICAL,COSCOGL1,COSCOFL1,COSCOGL2,COSCOFL2,COSCOGL3,COSCOFL3,COSCOGL4,COSCOFL4,COSCOGL5,COSCOFL5,COSCOGL6,COSCOFL6,COVALPUN,COVALMCH,CONOTGEN,CONOTCON,CONOTMCH,COVALFAT,COSCOCOM,COVALPRE,COTOTPRE,COTOTPUN,COTIPPRE &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_COCODICE;
                  ,this.w_CODESCRI;
                  ,this.w_COFLGCIC;
                  ,this.w_CODATREG;
                  ,this.w_COFLGSTA;
                  ,this.w_COCODVAL;
                  ,this.w_CODATCAM;
                  ,this.w_COVALCAM;
                  ,this.w_CODATINI;
                  ,this.w_CODATFIN;
                  ,this.w_COTIPINT;
                  ,this.w_COCODAGE;
                  ,this.w_COCODINT;
                  ,this.w_COCODGRU;
                  ,this.w_COPRMRIF;
                  ,this.w_CODATGEN;
                  ,this.w_COTIPSTO;
                  ,this.w_COPRIORI;
                  ,this.w_COPRMDOC;
                  ,this.w_COSCAPRE;
                  ,this.w_COPRMLIQ;
                  ,this.w_COCODIVA;
                  ,this.w_COFLGEXD;
                  ,this.w_COFLGVAL;
                  ,this.w_COFLGLIS;
                  ,this.w_COFLESAG;
                  ,this.w_COFLGINC;
                  ,this.w_COFLMODE;
                  ,this.w_COFLGTIP;
                  ,this.w_COPRICAL;
                  ,this.w_COSCOGL1;
                  ,this.w_COSCOFL1;
                  ,this.w_COSCOGL2;
                  ,this.w_COSCOFL2;
                  ,this.w_COSCOGL3;
                  ,this.w_COSCOFL3;
                  ,this.w_COSCOGL4;
                  ,this.w_COSCOFL4;
                  ,this.w_COSCOGL5;
                  ,this.w_COSCOFL5;
                  ,this.w_COSCOGL6;
                  ,this.w_COSCOFL6;
                  ,this.w_COVALPUN;
                  ,this.w_COVALMCH;
                  ,this.w_CONOTGEN;
                  ,this.w_CONOTCON;
                  ,this.w_CONOTMCH;
                  ,this.w_COVALFAT;
                  ,this.w_COSCOCOM;
                  ,this.w_COVALPRE;
                  ,this.w_COTOTPRE;
                  ,this.w_COTOTPUN;
                  ,this.w_COTIPPRE;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CCOMDETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCOMDETT_IDX,2])
      *
      * insert into CCOMDETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(COCODICE,CPROWORD,COKEYART,COCODART,COUNIMIS"+;
                  ",COUMALTE,COCALPRM,COKEYSAL,COATTRIB,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_COCODICE)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_COKEYART)+","+cp_ToStrODBCNull(this.w_COCODART)+","+cp_ToStrODBCNull(this.w_COUNIMIS)+;
             ","+cp_ToStrODBC(this.w_COUMALTE)+","+cp_ToStrODBC(this.w_COCALPRM)+","+cp_ToStrODBC(this.w_COKEYSAL)+","+cp_ToStrODBC(this.w_COATTRIB)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'COCODICE',this.w_COCODICE)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_COCODICE,this.w_CPROWORD,this.w_COKEYART,this.w_COCODART,this.w_COUNIMIS"+;
                ",this.w_COUMALTE,this.w_COCALPRM,this.w_COKEYSAL,this.w_COATTRIB,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.CCOMMAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCOMMAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update CCOMMAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'CCOMMAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " CODESCRI="+cp_ToStrODBC(this.w_CODESCRI)+;
             ",COFLGCIC="+cp_ToStrODBC(this.w_COFLGCIC)+;
             ",CODATREG="+cp_ToStrODBC(this.w_CODATREG)+;
             ",COFLGSTA="+cp_ToStrODBC(this.w_COFLGSTA)+;
             ",COCODVAL="+cp_ToStrODBCNull(this.w_COCODVAL)+;
             ",CODATCAM="+cp_ToStrODBC(this.w_CODATCAM)+;
             ",COVALCAM="+cp_ToStrODBC(this.w_COVALCAM)+;
             ",CODATINI="+cp_ToStrODBC(this.w_CODATINI)+;
             ",CODATFIN="+cp_ToStrODBC(this.w_CODATFIN)+;
             ",COTIPINT="+cp_ToStrODBC(this.w_COTIPINT)+;
             ",COCODAGE="+cp_ToStrODBCNull(this.w_COCODAGE)+;
             ",COCODINT="+cp_ToStrODBCNull(this.w_COCODINT)+;
             ",COCODGRU="+cp_ToStrODBCNull(this.w_COCODGRU)+;
             ",COPRMRIF="+cp_ToStrODBC(this.w_COPRMRIF)+;
             ",CODATGEN="+cp_ToStrODBC(this.w_CODATGEN)+;
             ",COTIPSTO="+cp_ToStrODBC(this.w_COTIPSTO)+;
             ",COPRIORI="+cp_ToStrODBC(this.w_COPRIORI)+;
             ",COPRMDOC="+cp_ToStrODBC(this.w_COPRMDOC)+;
             ",COSCAPRE="+cp_ToStrODBC(this.w_COSCAPRE)+;
             ",COPRMLIQ="+cp_ToStrODBC(this.w_COPRMLIQ)+;
             ",COCODIVA="+cp_ToStrODBCNull(this.w_COCODIVA)+;
             ",COFLGEXD="+cp_ToStrODBC(this.w_COFLGEXD)+;
             ",COFLGVAL="+cp_ToStrODBC(this.w_COFLGVAL)+;
             ",COFLGLIS="+cp_ToStrODBC(this.w_COFLGLIS)+;
             ",COFLESAG="+cp_ToStrODBC(this.w_COFLESAG)+;
             ",COFLGINC="+cp_ToStrODBC(this.w_COFLGINC)+;
             ",COFLMODE="+cp_ToStrODBC(this.w_COFLMODE)+;
             ",COFLGTIP="+cp_ToStrODBC(this.w_COFLGTIP)+;
             ",COPRICAL="+cp_ToStrODBC(this.w_COPRICAL)+;
             ",COSCOGL1="+cp_ToStrODBC(this.w_COSCOGL1)+;
             ",COSCOFL1="+cp_ToStrODBC(this.w_COSCOFL1)+;
             ",COSCOGL2="+cp_ToStrODBC(this.w_COSCOGL2)+;
             ",COSCOFL2="+cp_ToStrODBC(this.w_COSCOFL2)+;
             ",COSCOGL3="+cp_ToStrODBC(this.w_COSCOGL3)+;
             ",COSCOFL3="+cp_ToStrODBC(this.w_COSCOFL3)+;
             ",COSCOGL4="+cp_ToStrODBC(this.w_COSCOGL4)+;
             ",COSCOFL4="+cp_ToStrODBC(this.w_COSCOFL4)+;
             ",COSCOGL5="+cp_ToStrODBC(this.w_COSCOGL5)+;
             ",COSCOFL5="+cp_ToStrODBC(this.w_COSCOFL5)+;
             ",COSCOGL6="+cp_ToStrODBC(this.w_COSCOGL6)+;
             ",COSCOFL6="+cp_ToStrODBC(this.w_COSCOFL6)+;
             ",COVALPUN="+cp_ToStrODBC(this.w_COVALPUN)+;
             ",COVALMCH="+cp_ToStrODBC(this.w_COVALMCH)+;
             ",CONOTGEN="+cp_ToStrODBC(this.w_CONOTGEN)+;
             ",CONOTCON="+cp_ToStrODBC(this.w_CONOTCON)+;
             ",CONOTMCH="+cp_ToStrODBC(this.w_CONOTMCH)+;
             ",COVALFAT="+cp_ToStrODBC(this.w_COVALFAT)+;
             ",COSCOCOM="+cp_ToStrODBC(this.w_COSCOCOM)+;
             ",COVALPRE="+cp_ToStrODBC(this.w_COVALPRE)+;
             ",COTOTPRE="+cp_ToStrODBC(this.w_COTOTPRE)+;
             ",COTOTPUN="+cp_ToStrODBC(this.w_COTOTPUN)+;
             ",COTIPPRE="+cp_ToStrODBC(this.w_COTIPPRE)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'CCOMMAST')
          i_cWhere = cp_PKFox(i_cTable  ,'COCODICE',this.w_COCODICE  )
          UPDATE (i_cTable) SET;
              CODESCRI=this.w_CODESCRI;
             ,COFLGCIC=this.w_COFLGCIC;
             ,CODATREG=this.w_CODATREG;
             ,COFLGSTA=this.w_COFLGSTA;
             ,COCODVAL=this.w_COCODVAL;
             ,CODATCAM=this.w_CODATCAM;
             ,COVALCAM=this.w_COVALCAM;
             ,CODATINI=this.w_CODATINI;
             ,CODATFIN=this.w_CODATFIN;
             ,COTIPINT=this.w_COTIPINT;
             ,COCODAGE=this.w_COCODAGE;
             ,COCODINT=this.w_COCODINT;
             ,COCODGRU=this.w_COCODGRU;
             ,COPRMRIF=this.w_COPRMRIF;
             ,CODATGEN=this.w_CODATGEN;
             ,COTIPSTO=this.w_COTIPSTO;
             ,COPRIORI=this.w_COPRIORI;
             ,COPRMDOC=this.w_COPRMDOC;
             ,COSCAPRE=this.w_COSCAPRE;
             ,COPRMLIQ=this.w_COPRMLIQ;
             ,COCODIVA=this.w_COCODIVA;
             ,COFLGEXD=this.w_COFLGEXD;
             ,COFLGVAL=this.w_COFLGVAL;
             ,COFLGLIS=this.w_COFLGLIS;
             ,COFLESAG=this.w_COFLESAG;
             ,COFLGINC=this.w_COFLGINC;
             ,COFLMODE=this.w_COFLMODE;
             ,COFLGTIP=this.w_COFLGTIP;
             ,COPRICAL=this.w_COPRICAL;
             ,COSCOGL1=this.w_COSCOGL1;
             ,COSCOFL1=this.w_COSCOFL1;
             ,COSCOGL2=this.w_COSCOGL2;
             ,COSCOFL2=this.w_COSCOFL2;
             ,COSCOGL3=this.w_COSCOGL3;
             ,COSCOFL3=this.w_COSCOFL3;
             ,COSCOGL4=this.w_COSCOGL4;
             ,COSCOFL4=this.w_COSCOFL4;
             ,COSCOGL5=this.w_COSCOGL5;
             ,COSCOFL5=this.w_COSCOFL5;
             ,COSCOGL6=this.w_COSCOGL6;
             ,COSCOFL6=this.w_COSCOFL6;
             ,COVALPUN=this.w_COVALPUN;
             ,COVALMCH=this.w_COVALMCH;
             ,CONOTGEN=this.w_CONOTGEN;
             ,CONOTCON=this.w_CONOTCON;
             ,CONOTMCH=this.w_CONOTMCH;
             ,COVALFAT=this.w_COVALFAT;
             ,COSCOCOM=this.w_COSCOCOM;
             ,COVALPRE=this.w_COVALPRE;
             ,COTOTPRE=this.w_COTOTPRE;
             ,COTOTPUN=this.w_COTOTPUN;
             ,COTIPPRE=this.w_COTIPPRE;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD)) And not(Empty(t_COUNIMIS)) And (not(Empty(t_COKEYART)) Or t_COATTRIB=1)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.CCOMDETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.CCOMDETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Deleting row children
              this.GSGP_MPC.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_COCODICE,"CCCODICE";
                     ,this.w_CPROWNUM,"PCROWNUM";
                     )
              this.GSGP_MDL.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_COCODICE,"CA__GUID";
                     ,this.w_CPROWNUM,"CAROWRIF";
                     )
              this.GSGP_MPC.mDelete()
              this.GSGP_MDL.mDelete()
              *
              * delete from CCOMDETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CCOMDETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",COKEYART="+cp_ToStrODBCNull(this.w_COKEYART)+;
                     ",COCODART="+cp_ToStrODBCNull(this.w_COCODART)+;
                     ",COUNIMIS="+cp_ToStrODBCNull(this.w_COUNIMIS)+;
                     ",COUMALTE="+cp_ToStrODBC(this.w_COUMALTE)+;
                     ",COCALPRM="+cp_ToStrODBC(this.w_COCALPRM)+;
                     ",COKEYSAL="+cp_ToStrODBC(this.w_COKEYSAL)+;
                     ",COATTRIB="+cp_ToStrODBC(this.w_COATTRIB)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,COKEYART=this.w_COKEYART;
                     ,COCODART=this.w_COCODART;
                     ,COUNIMIS=this.w_COUNIMIS;
                     ,COUMALTE=this.w_COUMALTE;
                     ,COCALPRM=this.w_COCALPRM;
                     ,COKEYSAL=this.w_COKEYSAL;
                     ,COATTRIB=this.w_COATTRIB;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask header and footer children to save themselves
      * --- GSGP_MAL : Saving
      this.GSGP_MAL.ChangeRow(this.cRowID+'      1',0;
             ,this.w_COCODICE,"CLCODICE";
             )
      this.GSGP_MAL.mReplace()
      * --- GSGP_MAC : Saving
      this.GSGP_MAC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_COCODICE,"CCCODICE";
             )
      this.GSGP_MAC.mReplace()
      * --- Ask children belonging to rows to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (not(Empty(t_CPROWORD)) And not(Empty(t_COUNIMIS)) And (not(Empty(t_COKEYART)) Or t_COATTRIB=1))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        this.GSGP_MPC.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_COCODICE,"CCCODICE";
             ,this.w_CPROWNUM,"PCROWNUM";
             )
        this.GSGP_MPC.mReplace()
        this.GSGP_MDL.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_COCODICE,"CA__GUID";
             ,this.w_CPROWNUM,"CAROWRIF";
             )
        this.GSGP_MDL.mReplace()
        this.GSGP_MPC.bSaveContext=.f.
        this.GSGP_MDL.bSaveContext=.f.
      endscan
     this.GSGP_MPC.bSaveContext=.t.
     this.GSGP_MDL.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)2
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- gsgp_mco
    * --- Riposiziona sul Primo record del Temporaneo
    this.Firstrow()
    this.SetRow()
    this.SaveDependsOn()
    if not(bTrsErr)
        * --- Controlli finali sotto transazione
        this.NotifyEvent('CHKTR')
    this.Firstrow()
    this.SetRow()
    this.SaveDependsOn()
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSGP_MAL : Deleting
    this.GSGP_MAL.ChangeRow(this.cRowID+'      1',0;
           ,this.w_COCODICE,"CLCODICE";
           )
    this.GSGP_MAL.mDelete()
    * --- GSGP_MAC : Deleting
    this.GSGP_MAC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_COCODICE,"CCCODICE";
           )
    this.GSGP_MAC.mDelete()
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD)) And not(Empty(t_COUNIMIS)) And (not(Empty(t_COKEYART)) Or t_COATTRIB=1)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSGP_MPC : Deleting
        this.GSGP_MPC.bSaveContext=.f.
        this.GSGP_MPC.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_COCODICE,"CCCODICE";
               ,this.w_CPROWNUM,"PCROWNUM";
               )
        this.GSGP_MPC.bSaveContext=.t.
        this.GSGP_MPC.mDelete()
        * --- GSGP_MDL : Deleting
        this.GSGP_MDL.bSaveContext=.f.
        this.GSGP_MDL.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_COCODICE,"CA__GUID";
               ,this.w_CPROWNUM,"CAROWRIF";
               )
        this.GSGP_MDL.bSaveContext=.t.
        this.GSGP_MDL.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.CCOMDETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CCOMDETT_IDX,2])
        *
        * delete CCOMDETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.CCOMMAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.CCOMMAST_IDX,2])
        *
        * delete CCOMMAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD)) And not(Empty(t_COUNIMIS)) And (not(Empty(t_COKEYART)) Or t_COATTRIB=1)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CCOMMAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCOMMAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_COCODICE<>.w_COCODICE
          .w_AZIENDA = i_CODAZI
          .link_1_2('Full')
        endif
        .DoRTCalc(3,5,.t.)
        if .o_CODATREG<>.w_CODATREG
          .w_OBTEST = .w_CODATREG
        endif
        if .o_COFLMODE<>.w_COFLMODE
          .w_COFLGSTA = IIF(EMPTY(.w_COFLGSTA) OR .w_COFLMODE='S', 'S', .w_COFLGSTA )
        endif
        .DoRTCalc(8,9,.t.)
        if .o_COCODVAL<>.w_COCODVAL.or. .o_CODATCAM<>.w_CODATCAM
          .w_COVALCAM = GETCAM(.w_COCODVAL, .w_CODATCAM, 7)
        endif
        .DoRTCalc(11,14,.t.)
        if .o_COTIPINT<>.w_COTIPINT
          .w_COCODAGE = ''
          .link_1_15('Full')
        endif
        if .o_COTIPINT<>.w_COTIPINT
          .w_COCODINT = ''
          .link_1_16('Full')
        endif
        if .o_COTIPINT<>.w_COTIPINT
          .w_COCODGRU = ''
          .link_1_17('Full')
        endif
        .DoRTCalc(18,25,.t.)
        if .o_COCODICE<>.w_COCODICE
          .w_APFLGEXD = .w_COFLGEXD
        endif
          .w_COCODIVA = .w_COCODIVA
          .link_1_27('Full')
        .DoRTCalc(28,30,.t.)
        if .o_COTIPINT<>.w_COTIPINT
          .w_COFLESAG = 'N'
        endif
        .DoRTCalc(32,35,.t.)
          .link_2_3('Full')
        .DoRTCalc(37,39,.t.)
        if .o_COKEYART<>.w_COKEYART
          .w_COUNIMIS = iif(! EMPTY(nvl(.w_COKEYART,'')), IIF(NOT EMPTY(.w_UNMIS3), .w_UNMIS3, .w_UNMIS1), '')
          .link_2_7('Full')
        endif
        if .o_COUNIMIS<>.w_COUNIMIS.or. .o_COKEYART<>.w_COKEYART
          .w_COUMALTE = 0
        endif
        .DoRTCalc(42,66,.t.)
          .w_CODI = .w_COCODICE
        .DoRTCalc(68,68,.t.)
          .w_DATA = .w_CODATREG
          .w_DATAC = .w_CODATGEN
        .DoRTCalc(71,73,.t.)
          .w_CODI = .w_COCODICE
        .DoRTCalc(75,75,.t.)
          .w_DATA = .w_CODATREG
          .w_DATAC = .w_CODATGEN
        .DoRTCalc(78,82,.t.)
        if .o_COCODART<>.w_COCODART.or. .o_COKEYART<>.w_COKEYART
          .w_COKEYSAL = .w_COCODART
        endif
        .oPgFrm.Page3.oPag.DOCORI.Calculate()
        .oPgFrm.Page3.oPag.DOCDES.Calculate()
        .DoRTCalc(84,95,.t.)
        if .o_COVALMCH<>.w_COVALMCH.or. .o_COTOTPRE<>.w_COTOTPRE
          .w_VALTCONT = .w_COVALMCH+.w_COTOTPRE
        endif
        if .o_COSCOCOM<>.w_COSCOCOM.or. .o_COVALPRE<>.w_COVALPRE
          .w_COTOTPRE = .w_COSCOCOM+.w_COVALPRE
        endif
        if .o_COVALMCH<>.w_COVALMCH
          .w_INC1 = IIF(.w_COVALFAT=0,0,cp_ROUND(.w_COVALMCH/.w_COVALFAT * 100,2))
        endif
          .w_INC2 = cp_ROUND(.w_COSCOCOM/.w_COVALFAT * 100,2)
          .w_INC3 = cp_ROUND(.w_COVALPRE/.w_COVALFAT * 100,2)
          .w_INC4 = IIF(.w_COVALFAT=0,0,cp_ROUND(.w_COTOTPRE/.w_COVALFAT * 100,2))
          .w_INC5 = IIF(.w_COVALFAT=0,0,cp_ROUND(.w_VALTCONT/.w_COVALFAT * 100,2))
        .DoRTCalc(103,103,.t.)
        if .o_DECUNI<>.w_DECUNI.or. .o_COCODVAL<>.w_COCODVAL
          .w_CALCPICP = DEFPIP(.w_DECUNI)
        endif
          .link_4_43('Full')
        .oPgFrm.Page2.oPag.oObj_4_50.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE1),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_51.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE2),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_52.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE3),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_53.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE4),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_54.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE5),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_55.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE6),20)+':')
        .DoRTCalc(106,111,.t.)
          .w_TIPDOC = 'Z'+.w_COFLGCIC+.w_DocOri.getVar('MVCLADOC')
          .w_MVSERIAL = .w_DocOri.getVar('MVSERIAL')
          .w_TIPDOC2 = 'Y'+.w_DocDes.getVar('MVFLVEAC')+.w_DocDes.getVar('MVCLADOC')
          .w_MVSERIAL2 = .w_DocDes.getVar('MVSERIAL')
        if .o_COCODICE<>.w_COCODICE
          .w_RIFEST = 'Contratto: ' + alltrim(.w_COCODICE)
        endif
        .DoRTCalc(117,121,.t.)
        if .o_COTIPINT<>.w_COTIPINT
          .w_COPRMDOC = IIF(.w_COTIPINT='A','A',IIF(.w_COFLGCIC='V' ,'P','F'))
        endif
          .w_GPSERIAL = .w_DocDes.getVar('MVGENPRE')
          .w_GENERA = .w_COFLGCIC+'GPR'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(125,125,.t.)
          .w_CODKEY = .w_COCODART
        * --- Area Manuale = Calculate
        * --- gsgp_mco
        Local Old_area
        old_area=select()
        select (this.ctrsname)
        this.w_SRV = i_SRV
        Select( old_area )
        
        
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(127,127,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_COCODART with this.w_COCODART
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_FLFRAZ with this.w_FLFRAZ
      replace t_MOLTIP with this.w_MOLTIP
      replace t_OPERAT with this.w_OPERAT
      replace t_OPERA3 with this.w_OPERA3
      replace t_MOLTI3 with this.w_MOLTI3
      replace t_COKEYSAL with this.w_COKEYSAL
      replace t_TIPRIG with this.w_TIPRIG
      replace t_SRV with this.w_SRV
      replace t_FLUMDIF with this.w_FLUMDIF
      replace t_CODKEY with this.w_CODKEY
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page3.oPag.DOCORI.Calculate()
        .oPgFrm.Page3.oPag.DOCDES.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_50.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE1),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_51.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE2),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_52.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE3),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_53.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE4),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_54.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE5),20)+':')
        .oPgFrm.Page2.oPag.oObj_4_55.Calculate(RIGHT(SPACE(20)+ALLTRIM(.w_LABE6),20)+':')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_OBFFRSASTG()
    with this
          * --- GSGP_BCQ(CHKF) Controlli finali
          GSGP_BCQ(this;
              ,'CHFI';
             )
    endwith
  endproc
  proc Calculate_GWIRFUIOQW()
    with this
          * --- GSGP_BCQ(LIST) - flag listino 
          GSGP_BCQ(this;
              ,'LIST';
             )
    endwith
  endproc
  proc Calculate_IVRNBPGRHP()
    with this
          * --- GSGP_BCQ(CONT) - flag vendite 
          GSGP_BCQ(this;
              ,'CONT';
             )
    endwith
  endproc
  proc Calculate_FCIJOYUOPK()
    with this
          * --- GSGP_BCQ(CHKF) Controlli sotto trans
          GSGP_BCQ(this;
              ,'CHTR';
             )
    endwith
  endproc
  proc Calculate_EWWLJLHQKB()
    with this
          * --- GSGP_BCQ(SCIN) - sconti incond
          GSGP_BCQ(this;
              ,'SCIN';
             )
    endwith
  endproc
  proc Calculate_TYYHCHCJHE()
    with this
          * --- GSGP_BCQ(LPRM) - calcolo premi
          GSGP_BCQ(this;
              ,'LPRM';
             )
    endwith
  endproc
  proc Calculate_ALJXXMWKUJ()
    with this
          * --- GSGP_BCQ(ZOM1) documento origine premio
          GSGP_BCQ(this;
              ,.w_TIPDOC;
             )
    endwith
  endproc
  proc Calculate_GQXYFZQEUC()
    with this
          * --- GSGP_BCQ(ZOM2) documento generati
          GSGP_BCQ(this;
              ,.w_TIPDOC2;
             )
    endwith
  endproc
  proc Calculate_UKSGBVCSUC()
    with this
          * --- GSGP_BCQ(SOSPE) Contratto sospeso
          GSGP_BCQ(this;
              ,'SOSP';
             )
    endwith
  endproc
  proc Calculate_XGGRGOMVJX()
    with this
          * --- Imposto srv = A
          .w_SRV = 'A'
    endwith
  endproc
  proc Calculate_AQCHUXIILS()
    with this
          * --- GSGP_BCQ(ROWC) Controllo riga
          GSGP_BCQ(this;
              ,'ROWC';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCOFLGSTA_1_7.enabled = this.oPgFrm.Page1.oPag.oCOFLGSTA_1_7.mCond()
    this.oPgFrm.Page1.oPag.oCODATCAM_1_9.enabled = this.oPgFrm.Page1.oPag.oCODATCAM_1_9.mCond()
    this.oPgFrm.Page1.oPag.oCOVALCAM_1_10.enabled = this.oPgFrm.Page1.oPag.oCOVALCAM_1_10.mCond()
    this.oPgFrm.Page1.oPag.oCOTIPINT_1_13.enabled = this.oPgFrm.Page1.oPag.oCOTIPINT_1_13.mCond()
    this.oPgFrm.Page1.oPag.oCOTIPINT_1_14.enabled = this.oPgFrm.Page1.oPag.oCOTIPINT_1_14.mCond()
    this.oPgFrm.Page1.oPag.oCOCODAGE_1_15.enabled = this.oPgFrm.Page1.oPag.oCOCODAGE_1_15.mCond()
    this.oPgFrm.Page1.oPag.oCOCODINT_1_16.enabled = this.oPgFrm.Page1.oPag.oCOCODINT_1_16.mCond()
    this.oPgFrm.Page1.oPag.oCOCODGRU_1_17.enabled = this.oPgFrm.Page1.oPag.oCOCODGRU_1_17.mCond()
    this.oPgFrm.Page1.oPag.oCOTIPSTO_1_20.enabled = this.oPgFrm.Page1.oPag.oCOTIPSTO_1_20.mCond()
    this.oPgFrm.Page1.oPag.oCOPRMDOC_1_22.enabled = this.oPgFrm.Page1.oPag.oCOPRMDOC_1_22.mCond()
    this.oPgFrm.Page1.oPag.oCOSCAPRE_1_23.enabled = this.oPgFrm.Page1.oPag.oCOSCAPRE_1_23.mCond()
    this.oPgFrm.Page1.oPag.oCOPRMDOC_1_24.enabled = this.oPgFrm.Page1.oPag.oCOPRMDOC_1_24.mCond()
    this.oPgFrm.Page1.oPag.oCOPRMLIQ_1_25.enabled = this.oPgFrm.Page1.oPag.oCOPRMLIQ_1_25.mCond()
    this.oPgFrm.Page1.oPag.oCOFLESAG_1_31.enabled = this.oPgFrm.Page1.oPag.oCOFLESAG_1_31.mCond()
    this.oPgFrm.Page1.oPag.oCOFLMODE_1_33.enabled = this.oPgFrm.Page1.oPag.oCOFLMODE_1_33.mCond()
    this.oPgFrm.Page3.oPag.oBtn_5_49.enabled = this.oPgFrm.Page3.oPag.oBtn_5_49.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_34.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_34.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_1_35.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_35.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOKEYART_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOKEYART_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOUNIMIS_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOUNIMIS_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOUMALTE_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCOUMALTE_2_8.mCond()
    this.oPgFrm.Page1.oPag.oLinkPC_2_10.enabled =this.oPgFrm.Page1.oPag.oLinkPC_2_10.mCond()
    *--- Nascondo il figlio se non editabile
    if Type("this.GSGP_MPC.visible")=='L' And this.GSGP_MPC.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_10.enabled
      this.GSGP_MPC.HideChildrenChain()
    endif 
    this.oPgFrm.Page1.oPag.oLinkPC_2_11.enabled =this.oPgFrm.Page1.oPag.oLinkPC_2_11.mCond()
    *--- Nascondo il figlio se non editabile
    if Type("this.GSGP_MDL.visible")=='L' And this.GSGP_MDL.visible And !this.oPgFrm.Page1.oPag.oLinkPC_2_11.enabled
      this.GSGP_MDL.HideChildrenChain()
    endif 
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(3).enabled=not(this.w_COFLMODE='S')
    this.oPgFrm.Page1.oPag.oCOFLGSTA_1_7.visible=!this.oPgFrm.Page1.oPag.oCOFLGSTA_1_7.mHide()
    this.oPgFrm.Page1.oPag.oCODATCAM_1_9.visible=!this.oPgFrm.Page1.oPag.oCODATCAM_1_9.mHide()
    this.oPgFrm.Page1.oPag.oCOVALCAM_1_10.visible=!this.oPgFrm.Page1.oPag.oCOVALCAM_1_10.mHide()
    this.oPgFrm.Page1.oPag.oCOTIPINT_1_13.visible=!this.oPgFrm.Page1.oPag.oCOTIPINT_1_13.mHide()
    this.oPgFrm.Page1.oPag.oCOTIPINT_1_14.visible=!this.oPgFrm.Page1.oPag.oCOTIPINT_1_14.mHide()
    this.oPgFrm.Page1.oPag.oCOCODAGE_1_15.visible=!this.oPgFrm.Page1.oPag.oCOCODAGE_1_15.mHide()
    this.oPgFrm.Page1.oPag.oCOCODINT_1_16.visible=!this.oPgFrm.Page1.oPag.oCOCODINT_1_16.mHide()
    this.oPgFrm.Page1.oPag.oCOCODGRU_1_17.visible=!this.oPgFrm.Page1.oPag.oCOCODGRU_1_17.mHide()
    this.oPgFrm.Page1.oPag.oCOPRMRIF_1_18.visible=!this.oPgFrm.Page1.oPag.oCOPRMRIF_1_18.mHide()
    this.oPgFrm.Page1.oPag.oCOPRMDOC_1_22.visible=!this.oPgFrm.Page1.oPag.oCOPRMDOC_1_22.mHide()
    this.oPgFrm.Page1.oPag.oCOPRMDOC_1_24.visible=!this.oPgFrm.Page1.oPag.oCOPRMDOC_1_24.mHide()
    this.oPgFrm.Page1.oPag.oCOFLGEXD_1_28.visible=!this.oPgFrm.Page1.oPag.oCOFLGEXD_1_28.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_34.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_34.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_35.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_35.mHide()
    this.oPgFrm.Page1.oPag.oCODESINT_1_44.visible=!this.oPgFrm.Page1.oPag.oCODESINT_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oCOFLGSTA_1_67.visible=!this.oPgFrm.Page1.oPag.oCOFLGSTA_1_67.mHide()
    this.oPgFrm.Page1.oPag.oCODESGRU_1_69.visible=!this.oPgFrm.Page1.oPag.oCODESGRU_1_69.mHide()
    this.oPgFrm.Page1.oPag.oCODESAGE_1_70.visible=!this.oPgFrm.Page1.oPag.oCODESAGE_1_70.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_72.visible=!this.oPgFrm.Page1.oPag.oStr_1_72.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_73.visible=!this.oPgFrm.Page1.oPag.oStr_1_73.mHide()
    this.oPgFrm.Page1.oPag.oCOPRMDOC_1_78.visible=!this.oPgFrm.Page1.oPag.oCOPRMDOC_1_78.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_81.visible=!this.oPgFrm.Page1.oPag.oBtn_1_81.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("CHKFI")
          .Calculate_OBFFRSASTG()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_COFLGLIS Changed")
          .Calculate_GWIRFUIOQW()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_COFLGVAL Changed")
          .Calculate_IVRNBPGRHP()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CHKTR")
          .Calculate_FCIJOYUOPK()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_COFLGINC Changed")
          .Calculate_EWWLJLHQKB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_COCALPRM Changed")
          .Calculate_TYYHCHCJHE()
          bRefresh=.t.
        endif
      .oPgFrm.Page3.oPag.DOCORI.Event(cEvent)
      .oPgFrm.Page3.oPag.DOCDES.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_50.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_51.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_52.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_53.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_54.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_55.Event(cEvent)
        if lower(cEvent)==lower("w_docori selected")
          .Calculate_ALJXXMWKUJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_docdes selected")
          .Calculate_GQXYFZQEUC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("SOSPE")
          .Calculate_UKSGBVCSUC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init Row")
          .Calculate_XGGRGOMVJX()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_COKEYART Changed")
          .Calculate_AQCHUXIILS()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COCODIVA";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_AZIENDA)
            select COCODAZI,COCODIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.COCODAZI,space(10))
      this.w_COCODIVA = NVL(_Link_.COCODIVA,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(10)
      endif
      this.w_COCODIVA = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCODVAL
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_COCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECUNI,VADTOBSO,VACAOVAL,VADATEUR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_COCODVAL))
          select VACODVAL,VASIMVAL,VADECUNI,VADTOBSO,VACAOVAL,VADATEUR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VASIMVAL like "+cp_ToStrODBC(trim(this.w_COCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECUNI,VADTOBSO,VACAOVAL,VADATEUR";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VASIMVAL like "+cp_ToStr(trim(this.w_COCODVAL)+"%");

            select VACODVAL,VASIMVAL,VADECUNI,VADTOBSO,VACAOVAL,VADATEUR;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_COCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCOCODVAL_1_8'),i_cWhere,'GSAR_AVL',"VALUTE",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECUNI,VADTOBSO,VACAOVAL,VADATEUR";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VASIMVAL,VADECUNI,VADTOBSO,VACAOVAL,VADATEUR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECUNI,VADTOBSO,VACAOVAL,VADATEUR";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_COCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_COCODVAL)
            select VACODVAL,VASIMVAL,VADECUNI,VADTOBSO,VACAOVAL,VADATEUR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMBOLO = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_DATEMU = NVL(cp_ToDate(_Link_.VADATEUR),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_COCODVAL = space(3)
      endif
      this.w_SIMBOLO = space(5)
      this.w_DECUNI = 0
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_CAOVAL = 0
      this.w_DATEMU = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta inesistente o obsoleta ")
        endif
        this.w_COCODVAL = space(3)
        this.w_SIMBOLO = space(5)
        this.w_DECUNI = 0
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_CAOVAL = 0
        this.w_DATEMU = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCODAGE
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_COCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGCODFOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_COCODAGE))
          select AGCODAGE,AGDESAGE,AGCODFOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCOCODAGE_1_15'),i_cWhere,'',"AGENTI",'GSGP_AGE.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGCODFOR";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGCODFOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGCODFOR";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_COCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_COCODAGE)
            select AGCODAGE,AGDESAGE,AGCODFOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_CODESAGE = NVL(_Link_.AGDESAGE,space(40))
      this.w_AGCODFOR = NVL(_Link_.AGCODFOR,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_COCODAGE = space(5)
      endif
      this.w_CODESAGE = space(40)
      this.w_AGCODFOR = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!Empty(.w_AGCODFOR)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido o senza rif.fornitore")
        endif
        this.w_COCODAGE = space(5)
        this.w_CODESAGE = space(40)
        this.w_AGCODFOR = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_15.AGCODAGE as AGCODAGE115"+ ",link_1_15.AGDESAGE as AGDESAGE115"+ ",link_1_15.AGCODFOR as AGCODFOR115"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_15 on CCOMMAST.COCODAGE=link_1_15.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_15"
          i_cKey=i_cKey+'+" and CCOMMAST.COCODAGE=link_1_15.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCODINT
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCODINT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPINT);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPINT;
                     ,'ANCODICE',trim(this.w_COCODINT))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODINT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCODINT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCODINT_1_16'),i_cWhere,'GSAR_BZC',"ELENCO CLIENTI/FORNITORI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPINT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPINT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCODINT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPINT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPINT;
                       ,'ANCODICE',this.w_COCODINT)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODINT = NVL(_Link_.ANCODICE,space(15))
      this.w_CODESINT = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COCODINT = space(15)
      endif
      this.w_CODESINT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.ANCODICE as ANCODICE116"+ ",link_1_16.ANDESCRI as ANDESCRI116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on CCOMMAST.COCODINT=link_1_16.ANCODICE"+" and CCOMMAST.COTIPINT=link_1_16.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and CCOMMAST.COCODINT=link_1_16.ANCODICE(+)"'+'+" and CCOMMAST.COTIPINT=link_1_16.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCODGRU
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUPPACQ_IDX,3]
    i_lTable = "GRUPPACQ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2], .t., this.GRUPPACQ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUPPACQ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GACODICE like "+cp_ToStrODBC(trim(this.w_COCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GACODICE,GADESGRP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GACODICE',trim(this.w_COCODGRU))
          select GACODICE,GADESGRP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODGRU)==trim(_Link_.GACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRUPPACQ','*','GACODICE',cp_AbsName(oSource.parent,'oCOCODGRU_1_17'),i_cWhere,'',"GRUPPI DI ACQUISTO",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GACODICE,GADESGRP";
                     +" from "+i_cTable+" "+i_lTable+" where GACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GACODICE',oSource.xKey(1))
            select GACODICE,GADESGRP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GACODICE,GADESGRP";
                   +" from "+i_cTable+" "+i_lTable+" where GACODICE="+cp_ToStrODBC(this.w_COCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GACODICE',this.w_COCODGRU)
            select GACODICE,GADESGRP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODGRU = NVL(_Link_.GACODICE,space(15))
      this.w_CODESGRU = NVL(_Link_.GADESGRP,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COCODGRU = space(15)
      endif
      this.w_CODESGRU = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2])+'\'+cp_ToStr(_Link_.GACODICE,1)
      cp_ShowWarn(i_cKey,this.GRUPPACQ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCODIVA
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_COCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_COCODIVA)
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_COCODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COKEYART
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COKEYART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_COKEYART)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CADESART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_COKEYART))
          select CACODICE,CACODART,CADESART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COKEYART)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COKEYART) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oCOKEYART_2_2'),i_cWhere,'GSMA_BZA',"CHIAVI ARTICOLO",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CADESART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CACODART,CADESART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COKEYART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CADESART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_COKEYART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_COKEYART)
            select CACODICE,CACODART,CADESART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COKEYART = NVL(_Link_.CACODICE,space(20))
      this.w_COCODART = NVL(_Link_.CACODART,space(20))
      this.w_DESART = NVL(_Link_.CADESART,space(40))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_TIPRIG = NVL(_Link_.CA__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COKEYART = space(20)
      endif
      this.w_COCODART = space(20)
      this.w_DESART = space(40)
      this.w_UNMIS3 = space(3)
      this.w_OPERA3 = space(1)
      this.w_MOLTI3 = 0
      this.w_TIPRIG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!.w_TIPRIG $ 'DA'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_COKEYART = space(20)
        this.w_COCODART = space(20)
        this.w_DESART = space(40)
        this.w_UNMIS3 = space(3)
        this.w_OPERA3 = space(1)
        this.w_MOLTI3 = 0
        this.w_TIPRIG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COKEYART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.CACODICE as CACODICE202"+ ",link_2_2.CACODART as CACODART202"+ ",link_2_2.CADESART as CADESART202"+ ",link_2_2.CAUNIMIS as CAUNIMIS202"+ ",link_2_2.CAOPERAT as CAOPERAT202"+ ",link_2_2.CAMOLTIP as CAMOLTIP202"+ ",link_2_2.CA__TIPO as CA__TIPO202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on CCOMDETT.COKEYART=link_2_2.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and CCOMDETT.COKEYART=link_2_2.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COCODART
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARMOLTIP,ARUNMIS2,AROPERAT,ARFLSERG";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_COCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_COCODART)
            select ARCODART,ARUNMIS1,ARMOLTIP,ARUNMIS2,AROPERAT,ARFLSERG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODART = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_FLUMDIF = NVL(_Link_.ARFLSERG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COCODART = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_MOLTIP = 0
      this.w_UNMIS2 = space(3)
      this.w_OPERAT = space(1)
      this.w_FLUMDIF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.ARCODART as ARCODART203"+ ",link_2_3.ARUNMIS1 as ARUNMIS1203"+ ",link_2_3.ARMOLTIP as ARMOLTIP203"+ ",link_2_3.ARUNMIS2 as ARUNMIS2203"+ ",link_2_3.AROPERAT as AROPERAT203"+ ",link_2_3.ARFLSERG as ARFLSERG203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on CCOMDETT.COCODART=link_2_3.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and CCOMDETT.COCODART=link_2_3.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=COUNIMIS
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_COUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_COUNIMIS))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oCOUNIMIS_2_7'),i_cWhere,'GSAR_AUM',"UNITA' DI MISURA",'GSMAUMVM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_COUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_COUNIMIS)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COUNIMIS = space(3)
      endif
      this.w_FLFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF(! EMPTY(nvl(.w_COKEYART,'')),CHKUNIMI(IIF(.w_FLUMDIF='S' AND .w_COUNIMIS <>'   ','***',.w_COUNIMIS), .w_UNMIS1, .w_UNMIS2, .w_UNMIS3),.T.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_COUNIMIS = space(3)
        this.w_FLFRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.UMCODICE as UMCODICE207"+ ",link_2_7.UMFLFRAZ as UMFLFRAZ207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on CCOMDETT.COUNIMIS=link_2_7.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and CCOMDETT.COUNIMIS=link_2_7.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_4_43(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COLABEL1,COLABEL2,COLABEL3,COLABEL4,COLABEL5,COLABEL6";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_CODAZI)
            select COCODAZI,COLABEL1,COLABEL2,COLABEL3,COLABEL4,COLABEL5,COLABEL6;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.COCODAZI,space(5))
      this.w_LABE1 = NVL(_Link_.COLABEL1,space(20))
      this.w_LABE2 = NVL(_Link_.COLABEL2,space(20))
      this.w_LABE3 = NVL(_Link_.COLABEL3,space(20))
      this.w_LABE4 = NVL(_Link_.COLABEL4,space(20))
      this.w_LABE5 = NVL(_Link_.COLABEL5,space(20))
      this.w_LABE6 = NVL(_Link_.COLABEL6,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_LABE1 = space(20)
      this.w_LABE2 = space(20)
      this.w_LABE3 = space(20)
      this.w_LABE4 = space(20)
      this.w_LABE5 = space(20)
      this.w_LABE6 = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCOCODICE_1_1.value==this.w_COCODICE)
      this.oPgFrm.Page1.oPag.oCOCODICE_1_1.value=this.w_COCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESCRI_1_3.value==this.w_CODESCRI)
      this.oPgFrm.Page1.oPag.oCODESCRI_1_3.value=this.w_CODESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATREG_1_5.value==this.w_CODATREG)
      this.oPgFrm.Page1.oPag.oCODATREG_1_5.value=this.w_CODATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCOFLGSTA_1_7.RadioValue()==this.w_COFLGSTA)
      this.oPgFrm.Page1.oPag.oCOFLGSTA_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODVAL_1_8.value==this.w_COCODVAL)
      this.oPgFrm.Page1.oPag.oCOCODVAL_1_8.value=this.w_COCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATCAM_1_9.value==this.w_CODATCAM)
      this.oPgFrm.Page1.oPag.oCODATCAM_1_9.value=this.w_CODATCAM
    endif
    if not(this.oPgFrm.Page1.oPag.oCOVALCAM_1_10.value==this.w_COVALCAM)
      this.oPgFrm.Page1.oPag.oCOVALCAM_1_10.value=this.w_COVALCAM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATINI_1_11.value==this.w_CODATINI)
      this.oPgFrm.Page1.oPag.oCODATINI_1_11.value=this.w_CODATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATFIN_1_12.value==this.w_CODATFIN)
      this.oPgFrm.Page1.oPag.oCODATFIN_1_12.value=this.w_CODATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCOTIPINT_1_13.RadioValue()==this.w_COTIPINT)
      this.oPgFrm.Page1.oPag.oCOTIPINT_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOTIPINT_1_14.RadioValue()==this.w_COTIPINT)
      this.oPgFrm.Page1.oPag.oCOTIPINT_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODAGE_1_15.value==this.w_COCODAGE)
      this.oPgFrm.Page1.oPag.oCOCODAGE_1_15.value=this.w_COCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODINT_1_16.value==this.w_COCODINT)
      this.oPgFrm.Page1.oPag.oCOCODINT_1_16.value=this.w_COCODINT
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODGRU_1_17.value==this.w_COCODGRU)
      this.oPgFrm.Page1.oPag.oCOCODGRU_1_17.value=this.w_COCODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPRMRIF_1_18.RadioValue()==this.w_COPRMRIF)
      this.oPgFrm.Page1.oPag.oCOPRMRIF_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATGEN_1_19.value==this.w_CODATGEN)
      this.oPgFrm.Page1.oPag.oCODATGEN_1_19.value=this.w_CODATGEN
    endif
    if not(this.oPgFrm.Page1.oPag.oCOTIPSTO_1_20.RadioValue()==this.w_COTIPSTO)
      this.oPgFrm.Page1.oPag.oCOTIPSTO_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPRIORI_1_21.value==this.w_COPRIORI)
      this.oPgFrm.Page1.oPag.oCOPRIORI_1_21.value=this.w_COPRIORI
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPRMDOC_1_22.RadioValue()==this.w_COPRMDOC)
      this.oPgFrm.Page1.oPag.oCOPRMDOC_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOSCAPRE_1_23.RadioValue()==this.w_COSCAPRE)
      this.oPgFrm.Page1.oPag.oCOSCAPRE_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPRMDOC_1_24.RadioValue()==this.w_COPRMDOC)
      this.oPgFrm.Page1.oPag.oCOPRMDOC_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPRMLIQ_1_25.RadioValue()==this.w_COPRMLIQ)
      this.oPgFrm.Page1.oPag.oCOPRMLIQ_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODIVA_1_27.value==this.w_COCODIVA)
      this.oPgFrm.Page1.oPag.oCOCODIVA_1_27.value=this.w_COCODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oCOFLGEXD_1_28.RadioValue()==this.w_COFLGEXD)
      this.oPgFrm.Page1.oPag.oCOFLGEXD_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOFLGVAL_1_29.RadioValue()==this.w_COFLGVAL)
      this.oPgFrm.Page1.oPag.oCOFLGVAL_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOFLGLIS_1_30.RadioValue()==this.w_COFLGLIS)
      this.oPgFrm.Page1.oPag.oCOFLGLIS_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOFLESAG_1_31.RadioValue()==this.w_COFLESAG)
      this.oPgFrm.Page1.oPag.oCOFLESAG_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOFLGINC_1_32.RadioValue()==this.w_COFLGINC)
      this.oPgFrm.Page1.oPag.oCOFLGINC_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOFLMODE_1_33.RadioValue()==this.w_COFLMODE)
      this.oPgFrm.Page1.oPag.oCOFLMODE_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESINT_1_44.value==this.w_CODESINT)
      this.oPgFrm.Page1.oPag.oCODESINT_1_44.value=this.w_CODESINT
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMBOLO_1_48.value==this.w_SIMBOLO)
      this.oPgFrm.Page1.oPag.oSIMBOLO_1_48.value=this.w_SIMBOLO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_1_50.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_1_50.value=this.w_DESIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oCOSCOGL1_4_1.value==this.w_COSCOGL1)
      this.oPgFrm.Page2.oPag.oCOSCOGL1_4_1.value=this.w_COSCOGL1
    endif
    if not(this.oPgFrm.Page2.oPag.oCOSCOFL1_4_2.RadioValue()==this.w_COSCOFL1)
      this.oPgFrm.Page2.oPag.oCOSCOFL1_4_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCOSCOGL2_4_3.value==this.w_COSCOGL2)
      this.oPgFrm.Page2.oPag.oCOSCOGL2_4_3.value=this.w_COSCOGL2
    endif
    if not(this.oPgFrm.Page2.oPag.oCOSCOFL2_4_4.RadioValue()==this.w_COSCOFL2)
      this.oPgFrm.Page2.oPag.oCOSCOFL2_4_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCOSCOGL3_4_5.value==this.w_COSCOGL3)
      this.oPgFrm.Page2.oPag.oCOSCOGL3_4_5.value=this.w_COSCOGL3
    endif
    if not(this.oPgFrm.Page2.oPag.oCOSCOFL3_4_6.RadioValue()==this.w_COSCOFL3)
      this.oPgFrm.Page2.oPag.oCOSCOFL3_4_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCOSCOGL4_4_7.value==this.w_COSCOGL4)
      this.oPgFrm.Page2.oPag.oCOSCOGL4_4_7.value=this.w_COSCOGL4
    endif
    if not(this.oPgFrm.Page2.oPag.oCOSCOFL4_4_8.RadioValue()==this.w_COSCOFL4)
      this.oPgFrm.Page2.oPag.oCOSCOFL4_4_8.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCOSCOGL5_4_9.value==this.w_COSCOGL5)
      this.oPgFrm.Page2.oPag.oCOSCOGL5_4_9.value=this.w_COSCOGL5
    endif
    if not(this.oPgFrm.Page2.oPag.oCOSCOFL5_4_10.RadioValue()==this.w_COSCOFL5)
      this.oPgFrm.Page2.oPag.oCOSCOFL5_4_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCOSCOGL6_4_11.value==this.w_COSCOGL6)
      this.oPgFrm.Page2.oPag.oCOSCOGL6_4_11.value=this.w_COSCOGL6
    endif
    if not(this.oPgFrm.Page2.oPag.oCOSCOFL6_4_12.RadioValue()==this.w_COSCOFL6)
      this.oPgFrm.Page2.oPag.oCOSCOFL6_4_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCOVALPUN_4_13.value==this.w_COVALPUN)
      this.oPgFrm.Page2.oPag.oCOVALPUN_4_13.value=this.w_COVALPUN
    endif
    if not(this.oPgFrm.Page2.oPag.oCOVALMCH_4_14.value==this.w_COVALMCH)
      this.oPgFrm.Page2.oPag.oCOVALMCH_4_14.value=this.w_COVALMCH
    endif
    if not(this.oPgFrm.Page2.oPag.oCONOTGEN_4_15.value==this.w_CONOTGEN)
      this.oPgFrm.Page2.oPag.oCONOTGEN_4_15.value=this.w_CONOTGEN
    endif
    if not(this.oPgFrm.Page2.oPag.oCONOTCON_4_16.value==this.w_CONOTCON)
      this.oPgFrm.Page2.oPag.oCONOTCON_4_16.value=this.w_CONOTCON
    endif
    if not(this.oPgFrm.Page2.oPag.oCONOTMCH_4_17.value==this.w_CONOTMCH)
      this.oPgFrm.Page2.oPag.oCONOTMCH_4_17.value=this.w_CONOTMCH
    endif
    if not(this.oPgFrm.Page2.oPag.oSIMBOLO_4_24.value==this.w_SIMBOLO)
      this.oPgFrm.Page2.oPag.oSIMBOLO_4_24.value=this.w_SIMBOLO
    endif
    if not(this.oPgFrm.Page2.oPag.oCODI_4_25.value==this.w_CODI)
      this.oPgFrm.Page2.oPag.oCODI_4_25.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page2.oPag.oCODESCRI_4_26.value==this.w_CODESCRI)
      this.oPgFrm.Page2.oPag.oCODESCRI_4_26.value=this.w_CODESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oDATA_4_28.value==this.w_DATA)
      this.oPgFrm.Page2.oPag.oDATA_4_28.value=this.w_DATA
    endif
    if not(this.oPgFrm.Page2.oPag.oDATAC_4_29.value==this.w_DATAC)
      this.oPgFrm.Page2.oPag.oDATAC_4_29.value=this.w_DATAC
    endif
    if not(this.oPgFrm.Page3.oPag.oSIMBOLO_5_4.value==this.w_SIMBOLO)
      this.oPgFrm.Page3.oPag.oSIMBOLO_5_4.value=this.w_SIMBOLO
    endif
    if not(this.oPgFrm.Page3.oPag.oCODI_5_5.value==this.w_CODI)
      this.oPgFrm.Page3.oPag.oCODI_5_5.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page3.oPag.oCODESCRI_5_6.value==this.w_CODESCRI)
      this.oPgFrm.Page3.oPag.oCODESCRI_5_6.value=this.w_CODESCRI
    endif
    if not(this.oPgFrm.Page3.oPag.oDATA_5_7.value==this.w_DATA)
      this.oPgFrm.Page3.oPag.oDATA_5_7.value=this.w_DATA
    endif
    if not(this.oPgFrm.Page3.oPag.oDATAC_5_8.value==this.w_DATAC)
      this.oPgFrm.Page3.oPag.oDATAC_5_8.value=this.w_DATAC
    endif
    if not(this.oPgFrm.Page1.oPag.oCOFLGSTA_1_67.RadioValue()==this.w_COFLGSTA)
      this.oPgFrm.Page1.oPag.oCOFLGSTA_1_67.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESGRU_1_69.value==this.w_CODESGRU)
      this.oPgFrm.Page1.oPag.oCODESGRU_1_69.value=this.w_CODESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESAGE_1_70.value==this.w_CODESAGE)
      this.oPgFrm.Page1.oPag.oCODESAGE_1_70.value=this.w_CODESAGE
    endif
    if not(this.oPgFrm.Page3.oPag.oCOVALFAT_5_13.value==this.w_COVALFAT)
      this.oPgFrm.Page3.oPag.oCOVALFAT_5_13.value=this.w_COVALFAT
    endif
    if not(this.oPgFrm.Page3.oPag.oCOVALMCH_5_15.value==this.w_COVALMCH)
      this.oPgFrm.Page3.oPag.oCOVALMCH_5_15.value=this.w_COVALMCH
    endif
    if not(this.oPgFrm.Page3.oPag.oCOSCOCOM_5_17.value==this.w_COSCOCOM)
      this.oPgFrm.Page3.oPag.oCOSCOCOM_5_17.value=this.w_COSCOCOM
    endif
    if not(this.oPgFrm.Page3.oPag.oCOVALPRE_5_19.value==this.w_COVALPRE)
      this.oPgFrm.Page3.oPag.oCOVALPRE_5_19.value=this.w_COVALPRE
    endif
    if not(this.oPgFrm.Page3.oPag.oVALTCONT_5_22.value==this.w_VALTCONT)
      this.oPgFrm.Page3.oPag.oVALTCONT_5_22.value=this.w_VALTCONT
    endif
    if not(this.oPgFrm.Page3.oPag.oCOTOTPRE_5_23.value==this.w_COTOTPRE)
      this.oPgFrm.Page3.oPag.oCOTOTPRE_5_23.value=this.w_COTOTPRE
    endif
    if not(this.oPgFrm.Page3.oPag.oINC1_5_29.value==this.w_INC1)
      this.oPgFrm.Page3.oPag.oINC1_5_29.value=this.w_INC1
    endif
    if not(this.oPgFrm.Page3.oPag.oINC2_5_31.value==this.w_INC2)
      this.oPgFrm.Page3.oPag.oINC2_5_31.value=this.w_INC2
    endif
    if not(this.oPgFrm.Page3.oPag.oINC3_5_33.value==this.w_INC3)
      this.oPgFrm.Page3.oPag.oINC3_5_33.value=this.w_INC3
    endif
    if not(this.oPgFrm.Page3.oPag.oINC4_5_35.value==this.w_INC4)
      this.oPgFrm.Page3.oPag.oINC4_5_35.value=this.w_INC4
    endif
    if not(this.oPgFrm.Page3.oPag.oINC5_5_37.value==this.w_INC5)
      this.oPgFrm.Page3.oPag.oINC5_5_37.value=this.w_INC5
    endif
    if not(this.oPgFrm.Page2.oPag.oCOTOTPUN_4_41.value==this.w_COTOTPUN)
      this.oPgFrm.Page2.oPag.oCOTOTPUN_4_41.value=this.w_COTOTPUN
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPRMDOC_1_78.RadioValue()==this.w_COPRMDOC)
      this.oPgFrm.Page1.oPag.oCOPRMDOC_1_78.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOKEYART_2_2.value==this.w_COKEYART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOKEYART_2_2.value=this.w_COKEYART
      replace t_COKEYART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOKEYART_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOUNIMIS_2_7.value==this.w_COUNIMIS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOUNIMIS_2_7.value=this.w_COUNIMIS
      replace t_COUNIMIS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOUNIMIS_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOUMALTE_2_8.RadioValue()==this.w_COUMALTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOUMALTE_2_8.SetRadio()
      replace t_COUMALTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOUMALTE_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCALPRM_2_9.RadioValue()==this.w_COCALPRM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCALPRM_2_9.SetRadio()
      replace t_COCALPRM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCALPRM_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESART_2_12.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESART_2_12.value=this.w_DESART
      replace t_DESART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESART_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOATTRIB_2_19.RadioValue()==this.w_COATTRIB)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOATTRIB_2_19.SetRadio()
      replace t_COATTRIB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOATTRIB_2_19.value
    endif
    cp_SetControlsValueExtFlds(this,'CCOMMAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_COCODICE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCOCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_COCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODATREG))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCODATREG_1_5.SetFocus()
            i_bnoObbl = !empty(.w_CODATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_COCODVAL) or not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCOCODVAL_1_8.SetFocus()
            i_bnoObbl = !empty(.w_COCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta inesistente o obsoleta ")
          case   (empty(.w_CODATINI))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCODATINI_1_11.SetFocus()
            i_bnoObbl = !empty(.w_CODATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODATFIN) or not(! .w_CODATFIN<.w_CODATINI))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCODATFIN_1_12.SetFocus()
            i_bnoObbl = !empty(.w_CODATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data fine contratto inferiore alla data di inizio")
          case   not(!Empty(.w_AGCODFOR))  and not(.w_COTIPINT <> 'A')  and (.w_COTIPINT = 'A')  and not(empty(.w_COCODAGE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCOCODAGE_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non valido o senza rif.fornitore")
          case   (empty(.w_COTIPSTO) or not(.w_COFLGINC = 0 Or .w_COFLGINC > 0 And .w_COTIPSTO = 'F'))  and (.w_COFLGSTA $ 'SD')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCOTIPSTO_1_20.SetFocus()
            i_bnoObbl = !empty(.w_COTIPSTO)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attivo il flag solo sconti incondizionati: impostare tipo storno forfettario")
          case   (empty(.w_COPRIORI) or not(.w_COPRIORI > 0))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCOPRIORI_1_21.SetFocus()
            i_bnoObbl = !empty(.w_COPRIORI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_COSCOGL1 >=0)
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oCOSCOGL1_4_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_COSCOGL2 >=0)
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oCOSCOGL2_4_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_COSCOGL3 >=0)
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oCOSCOGL3_4_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_COSCOGL4 >=0)
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oCOSCOGL4_4_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_COSCOGL5 >=0)
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oCOSCOGL5_4_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_COSCOGL6 >=0)
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oCOSCOGL6_4_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      *i_bRes = i_bRes .and. .GSGP_MAL.CheckForm()
      if i_bres
        i_bres=  .GSGP_MAL.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSGP_MAC.CheckForm()
      if i_bres
        i_bres=  .GSGP_MAC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsgp_mco
      * --- Controlli Finali
      if i_bRes
         .w_RESCHK=0
         ah_msg('Controlli finali...')
         .NotifyEvent('CHKFI')
         WAIT CLEAR
         if .w_RESCHK<>0
        	 	i_bRes=.f.
         endif
      endif
      if i_bres And .w_COTIPINT='A'And .w_COFLGEXD <>'S'
          do ah_ErrorMsg with "Tipologia intestatario agente, il flag applicazione %0extra documentale deve essere attivo",48
          i_bres=.f.
      endif
      If i_bres And .w_COFLGSTA = 'S'
        .NotifyEvent('SOSPE')
        WAIT CLEAR
        if .w_RESCHK<>0
         	i_bRes=.f.
        endif
      endif
      if i_bres And OCCURS("'",.w_COCODICE)>0
          do ah_ErrorMsg with "Il codice del contratto non deve contenere l'apice",48
          i_bres=.f.
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_CPROWORD) and (not(Empty(.w_CPROWORD)) And not(Empty(.w_COUNIMIS)) And (not(Empty(.w_COKEYART)) Or .w_COATTRIB=1))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   not(!.w_TIPRIG $ 'DA') and (.w_COATTRIB <> 1 And (Empty(.w_CODATGEN) Or .w_SRV = 'A')) and not(empty(.w_COKEYART)) and (not(Empty(.w_CPROWORD)) And not(Empty(.w_COUNIMIS)) And (not(Empty(.w_COKEYART)) Or .w_COATTRIB=1))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOKEYART_2_2
          i_bRes = .f.
          i_bnoChk = .f.
        case   (empty(.w_COUNIMIS) or not(IIF(! EMPTY(nvl(.w_COKEYART,'')),CHKUNIMI(IIF(.w_FLUMDIF='S' AND .w_COUNIMIS <>'   ','***',.w_COUNIMIS), .w_UNMIS1, .w_UNMIS2, .w_UNMIS3),.T.))) and (Empty(.w_CODATGEN) Or .w_SRV = 'A') and (not(Empty(.w_CPROWORD)) And not(Empty(.w_COUNIMIS)) And (not(Empty(.w_COKEYART)) Or .w_COATTRIB=1))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOUNIMIS_2_7
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
      endcase
      i_bRes = i_bRes .and. .GSGP_MPC.CheckForm()
      i_bRes = i_bRes .and. .GSGP_MDL.CheckForm()
      if not(Empty(.w_CPROWORD)) And not(Empty(.w_COUNIMIS)) And (not(Empty(.w_COKEYART)) Or .w_COATTRIB=1)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_COCODICE = this.w_COCODICE
    this.o_CODATREG = this.w_CODATREG
    this.o_COCODVAL = this.w_COCODVAL
    this.o_CODATCAM = this.w_CODATCAM
    this.o_COTIPINT = this.w_COTIPINT
    this.o_COFLMODE = this.w_COFLMODE
    this.o_COKEYART = this.w_COKEYART
    this.o_COCODART = this.w_COCODART
    this.o_COUNIMIS = this.w_COUNIMIS
    this.o_COVALMCH = this.w_COVALMCH
    this.o_DECUNI = this.w_DECUNI
    this.o_COSCOCOM = this.w_COSCOCOM
    this.o_COVALPRE = this.w_COVALPRE
    this.o_COTOTPRE = this.w_COTOTPRE
    * --- GSGP_MAL : Depends On
    this.GSGP_MAL.SaveDependsOn()
    * --- GSGP_MAC : Depends On
    this.GSGP_MAC.SaveDependsOn()
    * --- GSGP_MPC : Depends On
    this.GSGP_MPC.SaveDependsOn()
    * --- GSGP_MDL : Depends On
    this.GSGP_MDL.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)) And not(Empty(t_COUNIMIS)) And (not(Empty(t_COKEYART)) Or t_COATTRIB=1))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(999999,cp_maxroword()+10)
      .w_COKEYART=space(20)
      .w_COCODART=space(20)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_UNMIS3=space(3)
      .w_COUNIMIS=space(3)
      .w_COUMALTE=0
      .w_COCALPRM=space(1)
      .w_DESART=space(40)
      .w_FLFRAZ=space(1)
      .w_MOLTIP=0
      .w_OPERAT=space(1)
      .w_OPERA3=space(1)
      .w_MOLTI3=0
      .w_COKEYSAL=space(40)
      .w_COATTRIB=0
      .w_TIPRIG=space(1)
      .w_SRV=space(1)
      .w_FLUMDIF=space(1)
      .w_CODKEY=space(20)
      .DoRTCalc(1,35,.f.)
      if not(empty(.w_COKEYART))
        .link_2_2('Full')
      endif
      .DoRTCalc(36,36,.f.)
      if not(empty(.w_COCODART))
        .link_2_3('Full')
      endif
      .DoRTCalc(37,39,.f.)
        .w_COUNIMIS = iif(! EMPTY(nvl(.w_COKEYART,'')), IIF(NOT EMPTY(.w_UNMIS3), .w_UNMIS3, .w_UNMIS1), '')
      .DoRTCalc(40,40,.f.)
      if not(empty(.w_COUNIMIS))
        .link_2_7('Full')
      endif
        .w_COUMALTE = 0
        .w_COCALPRM = 'Q'
      .DoRTCalc(43,82,.f.)
        .w_COKEYSAL = .w_COCODART
        .w_COATTRIB = 2
      .DoRTCalc(85,125,.f.)
        .w_CODKEY = .w_COCODART
    endwith
    this.DoRTCalc(127,127,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_COKEYART = t_COKEYART
    this.w_COCODART = t_COCODART
    this.w_UNMIS1 = t_UNMIS1
    this.w_UNMIS2 = t_UNMIS2
    this.w_UNMIS3 = t_UNMIS3
    this.w_COUNIMIS = t_COUNIMIS
    this.w_COUMALTE = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOUMALTE_2_8.RadioValue(.t.)
    this.w_COCALPRM = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCALPRM_2_9.RadioValue(.t.)
    this.w_DESART = t_DESART
    this.w_FLFRAZ = t_FLFRAZ
    this.w_MOLTIP = t_MOLTIP
    this.w_OPERAT = t_OPERAT
    this.w_OPERA3 = t_OPERA3
    this.w_MOLTI3 = t_MOLTI3
    this.w_COKEYSAL = t_COKEYSAL
    this.w_COATTRIB = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOATTRIB_2_19.RadioValue(.t.)
    this.w_TIPRIG = t_TIPRIG
    this.w_SRV = t_SRV
    this.w_FLUMDIF = t_FLUMDIF
    this.w_CODKEY = t_CODKEY
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_COKEYART with this.w_COKEYART
    replace t_COCODART with this.w_COCODART
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_COUNIMIS with this.w_COUNIMIS
    replace t_COUMALTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOUMALTE_2_8.ToRadio()
    replace t_COCALPRM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOCALPRM_2_9.ToRadio()
    replace t_DESART with this.w_DESART
    replace t_FLFRAZ with this.w_FLFRAZ
    replace t_MOLTIP with this.w_MOLTIP
    replace t_OPERAT with this.w_OPERAT
    replace t_OPERA3 with this.w_OPERA3
    replace t_MOLTI3 with this.w_MOLTI3
    replace t_COKEYSAL with this.w_COKEYSAL
    replace t_COATTRIB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCOATTRIB_2_19.ToRadio()
    replace t_TIPRIG with this.w_TIPRIG
    replace t_SRV with this.w_SRV
    replace t_FLUMDIF with this.w_FLUMDIF
    replace t_CODKEY with this.w_CODKEY
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsgp_mcoPag1 as StdContainer
  Width  = 796
  height = 464
  stdWidth  = 796
  stdheight = 464
  resizeXpos=351
  resizeYpos=396
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOCODICE_1_1 as StdField with uid="KCWLDNMVFQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_COCODICE", cQueryName = "COCODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice identificativo del contratto",;
    HelpContextID = 137760149,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=52, Top=7, InputMask=replicate('X',15)

  add object oCODESCRI_1_3 as StdField with uid="KULNKFRRDZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODESCRI", cQueryName = "CODESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 223346065,;
   bGlobalFont=.t.,;
    Height=21, Width=297, Left=175, Top=7, InputMask=replicate('X',40)

  add object oCODATREG_1_5 as StdField with uid="CSWMSOQJBD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODATREG", cQueryName = "CODATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di stipulazione del contratto",;
    HelpContextID = 29098605,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=564, Top=7


  add object oCOFLGSTA_1_7 as StdCombo with uid="HTQCFDGSLI",rtseq=7,rtrep=.f.,left=689,top=7,width=94,height=21;
    , ToolTipText = "Status contratto";
    , HelpContextID = 235462041;
    , cFormVar="w_COFLGSTA",RowSource=""+"Da elaborare,"+"Sospeso", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOFLGSTA_1_7.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COFLGSTA,&i_cF..t_COFLGSTA),this.value)
    return(iif(xVal =1,'D',;
    iif(xVal =2,'S',;
    space(2))))
  endfunc
  func oCOFLGSTA_1_7.GetRadio()
    this.Parent.oContained.w_COFLGSTA = this.RadioValue()
    return .t.
  endfunc

  func oCOFLGSTA_1_7.ToRadio()
    this.Parent.oContained.w_COFLGSTA=trim(this.Parent.oContained.w_COFLGSTA)
    return(;
      iif(this.Parent.oContained.w_COFLGSTA=='D',1,;
      iif(this.Parent.oContained.w_COFLGSTA=='S',2,;
      0)))
  endfunc

  func oCOFLGSTA_1_7.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOFLGSTA_1_7.mCond()
    with this.Parent.oContained
      return (.w_COFLGSTA $ 'SD' AND .w_COFLMODE<>'S')
    endwith
  endfunc

  func oCOFLGSTA_1_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COFLGSTA = 'E')
    endwith
    endif
  endfunc

  add object oCOCODVAL_1_8 as StdField with uid="YYKFIEQAXB",rtseq=8,rtrep=.f.,;
    cFormVar = "w_COCODVAL", cQueryName = "COCODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta inesistente o obsoleta ",;
    ToolTipText = "Codice della valuta a cui � riferito il contratto",;
    HelpContextID = 188091790,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=52, Top=35, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_COCODVAL"

  func oCOCODVAL_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODVAL_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODVAL_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCOCODVAL_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"VALUTE",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oCOCODVAL_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_COCODVAL
    i_obj.ecpSave()
  endproc

  add object oCODATCAM_1_9 as StdField with uid="FVWWIQPPGS",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODATCAM", cQueryName = "CODATCAM",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del cambio della valuta",;
    HelpContextID = 222559629,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=243, Top=35

  func oCODATCAM_1_9.mCond()
    with this.Parent.oContained
      return (.w_CODATCAM<.w_DATEMU OR .w_CAOVAL=0)
    endwith
  endfunc

  func oCODATCAM_1_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(.w_CODATCAM<.w_DATEMU OR .w_CAOVAL=0))
    endwith
    endif
  endfunc

  add object oCOVALCAM_1_10 as StdField with uid="KNOSMQSBZP",rtseq=10,rtrep=.f.,;
    cFormVar = "w_COVALCAM", cQueryName = "COVALCAM",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio della valuta",;
    HelpContextID = 230874509,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=375, Top=35

  func oCOVALCAM_1_10.mCond()
    with this.Parent.oContained
      return (.w_CODATCAM<.w_DATEMU OR .w_CAOVAL=0)
    endwith
  endfunc

  func oCOVALCAM_1_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!(.w_CODATCAM<.w_DATEMU OR .w_CAOVAL=0))
    endwith
    endif
  endfunc

  add object oCODATINI_1_11 as StdField with uid="UESCPSNKNW",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODATINI", cQueryName = "CODATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit� del contratto",;
    HelpContextID = 121896337,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=564, Top=35

  add object oCODATFIN_1_12 as StdField with uid="DVQPBUOODF",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODATFIN", cQueryName = "CODATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data fine contratto inferiore alla data di inizio",;
    ToolTipText = "Data di termine validit� del contratto",;
    HelpContextID = 96207476,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=689, Top=35

  func oCODATFIN_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (! .w_CODATFIN<.w_CODATINI)
    endwith
    return bRes
  endfunc


  add object oCOTIPINT_1_13 as StdCombo with uid="NWAEYDETWA",rtseq=13,rtrep=.f.,left=52,top=63,width=119,height=21;
    , DisabledBackColor=rgb(255,255,255);
    , ToolTipText = "Tipo intestatario";
    , HelpContextID = 125500806;
    , cFormVar="w_COTIPINT",RowSource=""+"Cliente,"+"Agente,"+"Gruppo di acquisto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOTIPINT_1_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COTIPINT,&i_cF..t_COTIPINT),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'A',;
    iif(xVal =3,'G',;
    space(1)))))
  endfunc
  func oCOTIPINT_1_13.GetRadio()
    this.Parent.oContained.w_COTIPINT = this.RadioValue()
    return .t.
  endfunc

  func oCOTIPINT_1_13.ToRadio()
    this.Parent.oContained.w_COTIPINT=trim(this.Parent.oContained.w_COTIPINT)
    return(;
      iif(this.Parent.oContained.w_COTIPINT=='C',1,;
      iif(this.Parent.oContained.w_COTIPINT=='A',2,;
      iif(this.Parent.oContained.w_COTIPINT=='G',3,;
      0))))
  endfunc

  func oCOTIPINT_1_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOTIPINT_1_13.mCond()
    with this.Parent.oContained
      return (.cfunction <> 'Edit')
    endwith
  endfunc

  func oCOTIPINT_1_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COFLGCIC='A')
    endwith
    endif
  endfunc

  func oCOTIPINT_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_COCODINT)
        bRes2=.link_1_16('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oCOTIPINT_1_14 as StdCombo with uid="OLHKUIRXMM",rtseq=14,rtrep=.f.,left=52,top=63,width=119,height=21;
    , DisabledBackColor=rgb(255,255,255);
    , ToolTipText = "Tipo intestatario";
    , HelpContextID = 125500806;
    , cFormVar="w_COTIPINT",RowSource=""+"Fornitore,"+"Gruppo di acquisto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOTIPINT_1_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COTIPINT,&i_cF..t_COTIPINT),this.value)
    return(iif(xVal =1,'F',;
    iif(xVal =2,'G',;
    space(1))))
  endfunc
  func oCOTIPINT_1_14.GetRadio()
    this.Parent.oContained.w_COTIPINT = this.RadioValue()
    return .t.
  endfunc

  func oCOTIPINT_1_14.ToRadio()
    this.Parent.oContained.w_COTIPINT=trim(this.Parent.oContained.w_COTIPINT)
    return(;
      iif(this.Parent.oContained.w_COTIPINT=='F',1,;
      iif(this.Parent.oContained.w_COTIPINT=='G',2,;
      0)))
  endfunc

  func oCOTIPINT_1_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOTIPINT_1_14.mCond()
    with this.Parent.oContained
      return (.cfunction <> 'Edit')
    endwith
  endfunc

  func oCOTIPINT_1_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COFLGCIC='V')
    endwith
    endif
  endfunc

  func oCOTIPINT_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_COCODINT)
        bRes2=.link_1_16('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCOCODAGE_1_15 as StdField with uid="RVVTSZBHMS",rtseq=15,rtrep=.f.,;
    cFormVar = "w_COCODAGE", cQueryName = "COCODAGE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido o senza rif.fornitore",;
    HelpContextID = 264893035,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=177, Top=63, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_COCODAGE"

  func oCOCODAGE_1_15.mCond()
    with this.Parent.oContained
      return (.w_COTIPINT = 'A')
    endwith
  endfunc

  func oCOCODAGE_1_15.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COTIPINT <> 'A')
    endwith
    endif
  endfunc

  func oCOCODAGE_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODAGE_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODAGE_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCOCODAGE_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"AGENTI",'GSGP_AGE.AGENTI_VZM',this.parent.oContained
  endproc

  add object oCOCODINT_1_16 as StdField with uid="MOROYLUOGG",rtseq=16,rtrep=.f.,;
    cFormVar = "w_COCODINT", cQueryName = "COCODINT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 137760134,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=177, Top=63, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPINT", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCODINT"

  func oCOCODINT_1_16.mCond()
    with this.Parent.oContained
      return (.w_COTIPINT $ 'CF')
    endwith
  endfunc

  func oCOCODINT_1_16.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_COTIPINT $ 'CF')
    endwith
    endif
  endfunc

  func oCOCODINT_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODINT_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODINT_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPINT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPINT)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCODINT_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"ELENCO CLIENTI/FORNITORI",'',this.parent.oContained
  endproc
  proc oCOCODINT_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPINT
     i_obj.w_ANCODICE=this.parent.oContained.w_COCODINT
    i_obj.ecpSave()
  endproc

  add object oCOCODGRU_1_17 as StdField with uid="VKLUFFPOFW",rtseq=17,rtrep=.f.,;
    cFormVar = "w_COCODGRU", cQueryName = "COCODGRU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 171314565,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=177, Top=63, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="GRUPPACQ", oKey_1_1="GACODICE", oKey_1_2="this.w_COCODGRU"

  func oCOCODGRU_1_17.mCond()
    with this.Parent.oContained
      return (.w_COTIPINT = 'G')
    endwith
  endfunc

  func oCOCODGRU_1_17.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COTIPINT <> 'G')
    endwith
    endif
  endfunc

  func oCOCODGRU_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODGRU_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODGRU_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUPPACQ','*','GACODICE',cp_AbsName(this.parent,'oCOCODGRU_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"GRUPPI DI ACQUISTO",'',this.parent.oContained
  endproc


  add object oCOPRMRIF_1_18 as StdCombo with uid="BYBEMCKPBT",rtseq=18,rtrep=.f.,left=670,top=63,width=124,height=21, enabled=.f.;
    , DisabledBackColor=rgb(255,255,255);
    , ToolTipText = "Corresponsione premio gruppo";
    , HelpContextID = 22921836;
    , cFormVar="w_COPRMRIF",RowSource=""+"Ripartita per cliente,"+"Unitaria per centrale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOPRMRIF_1_18.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COPRMRIF,&i_cF..t_COPRMRIF),this.value)
    return(iif(xVal =1,'R',;
    iif(xVal =2,'U' ,;
    space(1))))
  endfunc
  func oCOPRMRIF_1_18.GetRadio()
    this.Parent.oContained.w_COPRMRIF = this.RadioValue()
    return .t.
  endfunc

  func oCOPRMRIF_1_18.ToRadio()
    this.Parent.oContained.w_COPRMRIF=trim(this.Parent.oContained.w_COPRMRIF)
    return(;
      iif(this.Parent.oContained.w_COPRMRIF=='R',1,;
      iif(this.Parent.oContained.w_COPRMRIF=='U',2,;
      0)))
  endfunc

  func oCOPRMRIF_1_18.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOPRMRIF_1_18.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COTIPINT <> 'G')
    endwith
    endif
  endfunc

  add object oCODATGEN_1_19 as StdField with uid="PBDGPWVSNX",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODATGEN", cQueryName = "CODATREG,CODATGEN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di generazione dei consuntivi",;
    HelpContextID = 155450764,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=143, Top=95


  add object oCOTIPSTO_1_20 as StdCombo with uid="ETEQVHCVQK",rtseq=20,rtrep=.f.,left=438,top=95,width=118,height=21;
    , ToolTipText = "Tipo storno";
    , HelpContextID = 226164107;
    , cFormVar="w_COTIPSTO",RowSource=""+"Forfettario,"+"Ripartiz. articoli", bObbl = .t. , nPag = 1;
    , sErrorMsg = "Attivo il flag solo sconti incondizionati: impostare tipo storno forfettario";
  , bGlobalFont=.t.


  func oCOTIPSTO_1_20.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COTIPSTO,&i_cF..t_COTIPSTO),this.value)
    return(iif(xVal =1,'F',;
    iif(xVal =2,'R',;
    space(1))))
  endfunc
  func oCOTIPSTO_1_20.GetRadio()
    this.Parent.oContained.w_COTIPSTO = this.RadioValue()
    return .t.
  endfunc

  func oCOTIPSTO_1_20.ToRadio()
    this.Parent.oContained.w_COTIPSTO=trim(this.Parent.oContained.w_COTIPSTO)
    return(;
      iif(this.Parent.oContained.w_COTIPSTO=='F',1,;
      iif(this.Parent.oContained.w_COTIPSTO=='R',2,;
      0)))
  endfunc

  func oCOTIPSTO_1_20.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOTIPSTO_1_20.mCond()
    with this.Parent.oContained
      return (.w_COFLGSTA $ 'SD')
    endwith
  endfunc

  func oCOTIPSTO_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_COFLGINC = 0 Or .w_COFLGINC > 0 And .w_COTIPSTO = 'F')
    endwith
    return bRes
  endfunc

  add object oCOPRIORI_1_21 as StdField with uid="ZIUWHKTUTQ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_COPRIORI", cQueryName = "COPRIORI",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 31604113,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=293, Top=95, cSayPict='"9999"', cGetPict='"9999"'

  func oCOPRIORI_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_COPRIORI > 0)
    endwith
    return bRes
  endfunc


  add object oCOPRMDOC_1_22 as StdCombo with uid="CNPPTYZZZZ",rtseq=22,rtrep=.f.,left=438,top=129,width=118,height=21;
    , ToolTipText = "Tipo documento";
    , HelpContextID = 211959191;
    , cFormVar="w_COPRMDOC",RowSource=""+"Ns fattura,"+"Vs nota di credito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOPRMDOC_1_22.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COPRMDOC,&i_cF..t_COPRMDOC),this.value)
    return(iif(xVal =1,'F',;
    iif(xVal =2,'C',;
    space(1))))
  endfunc
  func oCOPRMDOC_1_22.GetRadio()
    this.Parent.oContained.w_COPRMDOC = this.RadioValue()
    return .t.
  endfunc

  func oCOPRMDOC_1_22.ToRadio()
    this.Parent.oContained.w_COPRMDOC=trim(this.Parent.oContained.w_COPRMDOC)
    return(;
      iif(this.Parent.oContained.w_COPRMDOC=='F',1,;
      iif(this.Parent.oContained.w_COPRMDOC=='C',2,;
      0)))
  endfunc

  func oCOPRMDOC_1_22.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOPRMDOC_1_22.mCond()
    with this.Parent.oContained
      return (.w_COFLGSTA $ 'SD')
    endwith
  endfunc

  func oCOPRMDOC_1_22.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COFLGCIC='V' Or .w_COTIPINT = 'A')
    endwith
    endif
  endfunc


  add object oCOSCAPRE_1_23 as StdCombo with uid="MSAEJPYWMS",rtseq=23,rtrep=.f.,left=129,top=129,width=119,height=21;
    , HelpContextID = 24186261;
    , cFormVar="w_COSCAPRE",RowSource=""+"Su scaglione max,"+"Progressivo,"+"Cumulativo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOSCAPRE_1_23.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COSCAPRE,&i_cF..t_COSCAPRE),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'P',;
    iif(xVal =3,'C',;
    space(1)))))
  endfunc
  func oCOSCAPRE_1_23.GetRadio()
    this.Parent.oContained.w_COSCAPRE = this.RadioValue()
    return .t.
  endfunc

  func oCOSCAPRE_1_23.ToRadio()
    this.Parent.oContained.w_COSCAPRE=trim(this.Parent.oContained.w_COSCAPRE)
    return(;
      iif(this.Parent.oContained.w_COSCAPRE=='S',1,;
      iif(this.Parent.oContained.w_COSCAPRE=='P',2,;
      iif(this.Parent.oContained.w_COSCAPRE=='C',3,;
      0))))
  endfunc

  func oCOSCAPRE_1_23.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOSCAPRE_1_23.mCond()
    with this.Parent.oContained
      return (.w_COFLGSTA $ 'SD')
    endwith
  endfunc


  add object oCOPRMDOC_1_24 as StdCombo with uid="FTPSQLWDIP",rtseq=24,rtrep=.f.,left=438,top=129,width=118,height=21;
    , ToolTipText = "Tipo documento";
    , HelpContextID = 211959191;
    , cFormVar="w_COPRMDOC",RowSource=""+"Vs fattura,"+"Ns nota di credito", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOPRMDOC_1_24.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COPRMDOC,&i_cF..t_COPRMDOC),this.value)
    return(iif(xVal =1,'P',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oCOPRMDOC_1_24.GetRadio()
    this.Parent.oContained.w_COPRMDOC = this.RadioValue()
    return .t.
  endfunc

  func oCOPRMDOC_1_24.ToRadio()
    this.Parent.oContained.w_COPRMDOC=trim(this.Parent.oContained.w_COPRMDOC)
    return(;
      iif(this.Parent.oContained.w_COPRMDOC=='P',1,;
      iif(this.Parent.oContained.w_COPRMDOC=='N',2,;
      0)))
  endfunc

  func oCOPRMDOC_1_24.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOPRMDOC_1_24.mCond()
    with this.Parent.oContained
      return (.w_COFLGSTA $ 'SD')
    endwith
  endfunc

  func oCOPRMDOC_1_24.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COFLGCIC='A'  Or .w_COTIPINT = 'A')
    endwith
    endif
  endfunc


  add object oCOPRMLIQ_1_25 as StdCombo with uid="LCRBGHUPEG",rtseq=25,rtrep=.f.,left=438,top=163,width=118,height=21;
    , HelpContextID = 190694007;
    , cFormVar="w_COPRMLIQ",RowSource=""+"A Scadenza,"+"Annuale,"+"Semestrale,"+"Trimestrale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOPRMLIQ_1_25.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COPRMLIQ,&i_cF..t_COPRMLIQ),this.value)
    return(iif(xVal =1,'F',;
    iif(xVal =2,'A' ,;
    iif(xVal =3,'S',;
    iif(xVal =4,'T',;
    space(1))))))
  endfunc
  func oCOPRMLIQ_1_25.GetRadio()
    this.Parent.oContained.w_COPRMLIQ = this.RadioValue()
    return .t.
  endfunc

  func oCOPRMLIQ_1_25.ToRadio()
    this.Parent.oContained.w_COPRMLIQ=trim(this.Parent.oContained.w_COPRMLIQ)
    return(;
      iif(this.Parent.oContained.w_COPRMLIQ=='F',1,;
      iif(this.Parent.oContained.w_COPRMLIQ=='A',2,;
      iif(this.Parent.oContained.w_COPRMLIQ=='S',3,;
      iif(this.Parent.oContained.w_COPRMLIQ=='T',4,;
      0)))))
  endfunc

  func oCOPRMLIQ_1_25.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOPRMLIQ_1_25.mCond()
    with this.Parent.oContained
      return (.w_COFLGSTA $ 'SD')
    endwith
  endfunc

  add object oCOCODIVA_1_27 as StdField with uid="UAQZECSHZX",rtseq=27,rtrep=.f.,;
    cFormVar = "w_COCODIVA", cQueryName = "COCODIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 130675303,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=130, Top=198, InputMask=replicate('X',5), cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_COCODIVA"

  func oCOCODIVA_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oCOFLGEXD_1_28 as StdCheck with uid="SWZUHXHTMA",rtseq=28,rtrep=.f.,left=561, top=91, caption="Applicazione extra documentale",;
    HelpContextID = 66527850,;
    cFormVar="w_COFLGEXD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOFLGEXD_1_28.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COFLGEXD,&i_cF..t_COFLGEXD),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCOFLGEXD_1_28.GetRadio()
    this.Parent.oContained.w_COFLGEXD = this.RadioValue()
    return .t.
  endfunc

  func oCOFLGEXD_1_28.ToRadio()
    this.Parent.oContained.w_COFLGEXD=trim(this.Parent.oContained.w_COFLGEXD)
    return(;
      iif(this.Parent.oContained.w_COFLGEXD=='S',1,;
      0))
  endfunc

  func oCOFLGEXD_1_28.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOFLGEXD_1_28.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (1=1)
    endwith
    endif
  endfunc

  add object oCOFLGVAL_1_29 as StdCheck with uid="GAUSEMAWYW",rtseq=29,rtrep=.f.,left=561, top=111, caption="Valido su tutte le vendite",;
    ToolTipText = "Valido su tutte le vendite",;
    HelpContextID = 185130382,;
    cFormVar="w_COFLGVAL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOFLGVAL_1_29.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COFLGVAL,&i_cF..t_COFLGVAL),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCOFLGVAL_1_29.GetRadio()
    this.Parent.oContained.w_COFLGVAL = this.RadioValue()
    return .t.
  endfunc

  func oCOFLGVAL_1_29.ToRadio()
    this.Parent.oContained.w_COFLGVAL=trim(this.Parent.oContained.w_COFLGVAL)
    return(;
      iif(this.Parent.oContained.w_COFLGVAL=='S',1,;
      0))
  endfunc

  func oCOFLGVAL_1_29.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCOFLGLIS_1_30 as StdCheck with uid="XBDGHNLJMZ",rtseq=30,rtrep=.f.,left=561, top=131, caption="Valido su tutti i listini",;
    ToolTipText = "Valido su tutti i listini",;
    HelpContextID = 183968377,;
    cFormVar="w_COFLGLIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOFLGLIS_1_30.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COFLGLIS,&i_cF..t_COFLGLIS),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCOFLGLIS_1_30.GetRadio()
    this.Parent.oContained.w_COFLGLIS = this.RadioValue()
    return .t.
  endfunc

  func oCOFLGLIS_1_30.ToRadio()
    this.Parent.oContained.w_COFLGLIS=trim(this.Parent.oContained.w_COFLGLIS)
    return(;
      iif(this.Parent.oContained.w_COFLGLIS=='S',1,;
      0))
  endfunc

  func oCOFLGLIS_1_30.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCOFLESAG_1_31 as StdCheck with uid="PDVCCDTLWO",rtseq=31,rtrep=.f.,left=561, top=151, caption="Escluso da premio agente",;
    ToolTipText = "Escluso da premio agente",;
    HelpContextID = 237559187,;
    cFormVar="w_COFLESAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOFLESAG_1_31.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COFLESAG,&i_cF..t_COFLESAG),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oCOFLESAG_1_31.GetRadio()
    this.Parent.oContained.w_COFLESAG = this.RadioValue()
    return .t.
  endfunc

  func oCOFLESAG_1_31.ToRadio()
    this.Parent.oContained.w_COFLESAG=trim(this.Parent.oContained.w_COFLESAG)
    return(;
      iif(this.Parent.oContained.w_COFLESAG=='S',1,;
      0))
  endfunc

  func oCOFLESAG_1_31.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOFLESAG_1_31.mCond()
    with this.Parent.oContained
      return (.w_COTIPINT<>'A')
    endwith
  endfunc

  add object oCOFLGINC_1_32 as StdCheck with uid="CCVPVCHVII",rtseq=32,rtrep=.f.,left=561, top=171, caption="Solo sconti incondizionati",;
    ToolTipText = "Solo sconti incondizionati",;
    HelpContextID = 134798743,;
    cFormVar="w_COFLGINC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOFLGINC_1_32.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COFLGINC,&i_cF..t_COFLGINC),this.value)
    return(iif(xVal =1,1,;
    0))
  endfunc
  func oCOFLGINC_1_32.GetRadio()
    this.Parent.oContained.w_COFLGINC = this.RadioValue()
    return .t.
  endfunc

  func oCOFLGINC_1_32.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_COFLGINC==1,1,;
      0))
  endfunc

  func oCOFLGINC_1_32.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCOFLMODE_1_33 as StdCheck with uid="UDZYVPUZLR",rtseq=33,rtrep=.f.,left=561, top=191, caption="Modello",;
    ToolTipText = "Se attivo il contratto sar� identificato come modello e sar� utilizzabile solo per la generazione di altri contratti",;
    HelpContextID = 27843989,;
    cFormVar="w_COFLMODE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOFLMODE_1_33.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COFLMODE,&i_cF..t_COFLMODE),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oCOFLMODE_1_33.GetRadio()
    this.Parent.oContained.w_COFLMODE = this.RadioValue()
    return .t.
  endfunc

  func oCOFLMODE_1_33.ToRadio()
    this.Parent.oContained.w_COFLMODE=trim(this.Parent.oContained.w_COFLMODE)
    return(;
      iif(this.Parent.oContained.w_COFLMODE=='S',1,;
      0))
  endfunc

  func oCOFLMODE_1_33.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOFLMODE_1_33.mCond()
    with this.Parent.oContained
      return (.cfunction <> 'Edit')
    endwith
  endfunc


  add object oLinkPC_1_34 as StdButton with uid="YXMLFTKYAW",left=680, top=219, width=48,height=45,;
    CpPicture="BMP\INVENTAR.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai listini associati";
    , HelpContextID = 171339510;
    , TabStop=.f., Caption='\<Listini';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_34.Click()
      this.Parent.oContained.GSGP_MAL.LinkPCClick()
    endproc

  func oLinkPC_1_34.mCond()
    with this.Parent.oContained
      return (.w_COFLGLIS <>'S' )
    endwith
  endfunc

  func oLinkPC_1_34.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cfunction = 'Query' and .w_COFLGLIS ='S' )
    endwith
   endif
  endfunc


  add object oLinkPC_1_35 as StdButton with uid="EISNPJBKYX",left=732, top=219, width=48,height=45,;
    CpPicture="BMP\CONTRATT.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai contratti associati";
    , HelpContextID = 171339510;
    , TabStop=.f., Caption='\<Contratti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_35.Click()
      this.Parent.oContained.GSGP_MAC.LinkPCClick()
    endproc

  func oLinkPC_1_35.mCond()
    with this.Parent.oContained
      return (.w_COFLGVAL <> 'S')
    endwith
  endfunc

  func oLinkPC_1_35.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cfunction = 'Query' and .w_COFLGVAL ='S' )
    endwith
   endif
  endfunc

  add object oCODESINT_1_44 as StdField with uid="LSJCZENCFK",rtseq=43,rtrep=.f.,;
    cFormVar = "w_CODESINT", cQueryName = "CODESINT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 122682758,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=298, Top=63, InputMask=replicate('X',40)

  func oCODESINT_1_44.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (!.w_COTIPINT $ 'CF')
    endwith
    endif
  endfunc

  add object oSIMBOLO_1_48 as StdField with uid="RBQNIZUXOO",rtseq=46,rtrep=.f.,;
    cFormVar = "w_SIMBOLO", cQueryName = "SIMBOLO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Simbolo della valuta",;
    HelpContextID = 76706522,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=105, Top=35, InputMask=replicate('X',5), DisabledBackColor=rgb(255,255,255)

  add object oDESIVA_1_50 as StdField with uid="GGZCSUJDAS",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 253433802,;
   bGlobalFont=.t.,;
    Height=21, Width=260, Left=198, Top=198, InputMask=replicate('X',35)


  add object oCOFLGSTA_1_67 as StdCombo with uid="ZQQKCVUBTK",rtseq=89,rtrep=.f.,left=689,top=7,width=94,height=21, enabled=.f.;
    , ToolTipText = "Status contratto";
    , HelpContextID = 235462041;
    , cFormVar="w_COFLGSTA",RowSource=""+"Elaborato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOFLGSTA_1_67.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COFLGSTA,&i_cF..t_COFLGSTA),this.value)
    return(iif(xVal =1,'E',;
    space(2)))
  endfunc
  func oCOFLGSTA_1_67.GetRadio()
    this.Parent.oContained.w_COFLGSTA = this.RadioValue()
    return .t.
  endfunc

  func oCOFLGSTA_1_67.ToRadio()
    this.Parent.oContained.w_COFLGSTA=trim(this.Parent.oContained.w_COFLGSTA)
    return(;
      iif(this.Parent.oContained.w_COFLGSTA=='E',1,;
      0))
  endfunc

  func oCOFLGSTA_1_67.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOFLGSTA_1_67.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COFLGSTA $ 'SD')
    endwith
    endif
  endfunc

  add object oCODESGRU_1_69 as StdField with uid="QJBTDEUKIE",rtseq=90,rtrep=.f.,;
    cFormVar = "w_CODESGRU", cQueryName = "CODESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 156237189,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=298, Top=63, InputMask=replicate('X',40)

  func oCODESGRU_1_69.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COTIPINT <> 'G')
    endwith
    endif
  endfunc

  add object oCODESAGE_1_70 as StdField with uid="QYTDYAWXAL",rtseq=91,rtrep=.f.,;
    cFormVar = "w_CODESAGE", cQueryName = "CODESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 11534955,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=298, Top=63, InputMask=replicate('X',40)

  func oCODESAGE_1_70.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COTIPINT <> 'A')
    endwith
    endif
  endfunc


  add object oCOPRMDOC_1_78 as StdCombo with uid="WDTSNSLIFV",rtseq=122,rtrep=.f.,left=438,top=129,width=118,height=21, enabled=.f.;
    , ToolTipText = "Tipo documento";
    , HelpContextID = 211959191;
    , cFormVar="w_COPRMDOC",RowSource=""+"Vs fattura", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOPRMDOC_1_78.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COPRMDOC,&i_cF..t_COPRMDOC),this.value)
    return(iif(xVal =1,'A',;
    space(1)))
  endfunc
  func oCOPRMDOC_1_78.GetRadio()
    this.Parent.oContained.w_COPRMDOC = this.RadioValue()
    return .t.
  endfunc

  func oCOPRMDOC_1_78.ToRadio()
    this.Parent.oContained.w_COPRMDOC=trim(this.Parent.oContained.w_COPRMDOC)
    return(;
      iif(this.Parent.oContained.w_COPRMDOC=='A',1,;
      0))
  endfunc

  func oCOPRMDOC_1_78.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOPRMDOC_1_78.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COTIPINT <> 'A')
    endwith
    endif
  endfunc


  add object oBtn_1_81 as StdButton with uid="OBVDCCTZGL",left=628, top=219, width=48,height=45,;
    CpPicture="BMP\NEWOFFE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per duplicare il contratto";
    , HelpContextID = 197531594;
    , caption='\<Duplica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_81.Click()
      do GSGP_KDC with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_81.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Query' OR EMPTY(.w_COCODICE))
    endwith
   endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=1, top=264, width=728,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWORD",Label1="Riga",Field2="COKEYART",Label2="Articolo",Field3="DESART",Label3="Descrizione",Field4="COUNIMIS",Label4="U.M.",Field5="COUMALTE",Label5="Altern.",Field6="COCALPRM",Label6="Calcolo prem.",Field7="COATTRIB",Label7="Attributi",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 49440646

  add object oStr_1_36 as StdString with uid="NAGJLGSAFN",Visible=.t., Left=525, Top=7,;
    Alignment=1, Width=37, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="IVNMAVFCJD",Visible=.t., Left=479, Top=35,;
    Alignment=1, Width=83, Height=15,;
    Caption="Validit� dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="MWIXMFKRMX",Visible=.t., Left=659, Top=35,;
    Alignment=1, Width=28, Height=15,;
    Caption="al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="LFLWEDJHCB",Visible=.t., Left=3, Top=7,;
    Alignment=1, Width=48, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_40 as StdString with uid="IOHOUUKFJU",Visible=.t., Left=2, Top=35,;
    Alignment=1, Width=49, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="WGLNKPFNQI",Visible=.t., Left=644, Top=7,;
    Alignment=1, Width=43, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="PIRDUUBFKM",Visible=.t., Left=224, Top=95,;
    Alignment=1, Width=69, Height=18,;
    Caption="Priorit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="UORQPACSKM",Visible=.t., Left=2, Top=63,;
    Alignment=1, Width=49, Height=18,;
    Caption="Intest.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="JLYCKRYNOH",Visible=.t., Left=2, Top=95,;
    Alignment=1, Width=139, Height=18,;
    Caption="Dati consuntivi al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="ZNXBHJSATC",Visible=.t., Left=336, Top=95,;
    Alignment=1, Width=100, Height=18,;
    Caption="Tipo storno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="RNGQDDHTCY",Visible=.t., Left=5, Top=198,;
    Alignment=1, Width=122, Height=18,;
    Caption="Cod. IVA premio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="MBRBQBUSLZ",Visible=.t., Left=11, Top=129,;
    Alignment=1, Width=115, Height=18,;
    Caption="Calc. premio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_53 as StdString with uid="WENKXPFZVX",Visible=.t., Left=560, Top=63,;
    Alignment=1, Width=107, Height=18,;
    Caption="Corresp. premio:"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (.w_COTIPINT <> 'G')
    endwith
  endfunc

  add object oStr_1_54 as StdString with uid="DDVBHHQPQB",Visible=.t., Left=254, Top=129,;
    Alignment=1, Width=182, Height=18,;
    Caption="Doc. liquidazione premio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="ILPUWZZZOG",Visible=.t., Left=211, Top=162,;
    Alignment=1, Width=225, Height=18,;
    Caption="Periodicit� liquidazione premio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_72 as StdString with uid="PEYXXJRVPW",Visible=.t., Left=324, Top=35,;
    Alignment=1, Width=47, Height=18,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_72.mHide()
    with this.Parent.oContained
      return (!(.w_CODATCAM<.w_DATEMU OR .w_CAOVAL=0))
    endwith
  endfunc

  add object oStr_1_73 as StdString with uid="HCMEDPWJZC",Visible=.t., Left=156, Top=35,;
    Alignment=1, Width=84, Height=18,;
    Caption="Data cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_73.mHide()
    with this.Parent.oContained
      return (!(.w_CODATCAM<.w_DATEMU OR .w_CAOVAL=0))
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-8,top=284,;
    width=724+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-7,top=285,width=723+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='KEY_ARTI|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oCOKEYART_2_2
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oCOUNIMIS_2_7
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_10 as StdButton with uid="BJVKQPAYQN",width=48,height=45,;
   left=732, top=289,;
    CpPicture="BMP\CERIFICA.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere al dettaglio premi";
    , HelpContextID = 171339510;
    , TabStop=.f., Caption='\<Premi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_10.Click()
      this.Parent.oContained.GSGP_MPC.LinkPCClick()
    endproc

  func oLinkPC_2_10.mCond()
    with this.Parent.oContained
      return (.w_COFLGINC = 0  And ! Empty(.w_COUNIMIS))
    endwith
  endfunc

  add object oLinkPC_2_11 as StdButton with uid="UWDPSMYMBF",width=48,height=45,;
   left=732, top=335,;
    CpPicture="BMP\DIBA24.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere al dettaglio attributi";
    , HelpContextID = 171339510;
    , TabStop=.f., Caption='\<Attributi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_2_11.Click()
      this.Parent.oContained.GSGP_MDL.LinkPCClick()
    endproc

  func oLinkPC_2_11.mCond()
    with this.Parent.oContained
      return (Empty(.w_COKEYART) And ! Empty(.w_COUNIMIS))
    endwith
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tgsgp_mcoPag2 as StdContainer
    Width  = 796
    height = 464
    stdWidth  = 796
    stdheight = 464
  resizeXpos=694
  resizeYpos=387
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOSCOGL1_4_1 as StdField with uid="ZEUSLFPKLG",rtseq=49,rtrep=.f.,;
    cFormVar = "w_COSCOGL1", cQueryName = "COSCOGL1",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 107934295,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=191, Top=57, cGetPict='"99.9999"'

  func oCOSCOGL1_4_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_COSCOGL1 >=0)
    endwith
    return bRes
  endfunc

  add object oCOSCOFL1_4_2 as StdCheck with uid="UNATZKFYWP",rtseq=50,rtrep=.f.,left=294, top=57, caption="Primo sconto",;
    HelpContextID = 91157079,;
    cFormVar="w_COSCOFL1", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCOSCOFL1_4_2.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COSCOFL1,&i_cF..t_COSCOFL1),this.value)
    return(iif(xVal =1,1,;
    0))
  endfunc
  func oCOSCOFL1_4_2.GetRadio()
    this.Parent.oContained.w_COSCOFL1 = this.RadioValue()
    return .t.
  endfunc

  func oCOSCOFL1_4_2.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_COSCOFL1==1,1,;
      0))
  endfunc

  func oCOSCOFL1_4_2.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCOSCOGL2_4_3 as StdField with uid="QJIUTEUIIE",rtseq=51,rtrep=.f.,;
    cFormVar = "w_COSCOGL2", cQueryName = "COSCOGL2",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 107934296,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=191, Top=80, cGetPict='"99.9999"'

  func oCOSCOGL2_4_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_COSCOGL2 >=0)
    endwith
    return bRes
  endfunc

  add object oCOSCOFL2_4_4 as StdCheck with uid="CIANXPEIDY",rtseq=52,rtrep=.f.,left=294, top=80, caption="Secondo sconto",;
    HelpContextID = 91157080,;
    cFormVar="w_COSCOFL2", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCOSCOFL2_4_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COSCOFL2,&i_cF..t_COSCOFL2),this.value)
    return(iif(xVal =1,1,;
    0))
  endfunc
  func oCOSCOFL2_4_4.GetRadio()
    this.Parent.oContained.w_COSCOFL2 = this.RadioValue()
    return .t.
  endfunc

  func oCOSCOFL2_4_4.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_COSCOFL2==1,1,;
      0))
  endfunc

  func oCOSCOFL2_4_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCOSCOGL3_4_5 as StdField with uid="KWFVSSOOJB",rtseq=53,rtrep=.f.,;
    cFormVar = "w_COSCOGL3", cQueryName = "COSCOGL3",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 107934297,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=191, Top=102, cGetPict='"99.9999"'

  func oCOSCOGL3_4_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_COSCOGL3 >=0)
    endwith
    return bRes
  endfunc

  add object oCOSCOFL3_4_6 as StdCheck with uid="HDJODQUSZP",rtseq=54,rtrep=.f.,left=294, top=102, caption="Terzo sconto",;
    HelpContextID = 91157081,;
    cFormVar="w_COSCOFL3", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCOSCOFL3_4_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COSCOFL3,&i_cF..t_COSCOFL3),this.value)
    return(iif(xVal =1,1,;
    0))
  endfunc
  func oCOSCOFL3_4_6.GetRadio()
    this.Parent.oContained.w_COSCOFL3 = this.RadioValue()
    return .t.
  endfunc

  func oCOSCOFL3_4_6.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_COSCOFL3==1,1,;
      0))
  endfunc

  func oCOSCOFL3_4_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCOSCOGL4_4_7 as StdField with uid="LLAIBNKJYD",rtseq=55,rtrep=.f.,;
    cFormVar = "w_COSCOGL4", cQueryName = "COSCOGL4",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 107934298,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=191, Top=124, cGetPict='"99.9999"'

  func oCOSCOGL4_4_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_COSCOGL4 >=0)
    endwith
    return bRes
  endfunc

  add object oCOSCOFL4_4_8 as StdCheck with uid="YKDYDALCXS",rtseq=56,rtrep=.f.,left=294, top=124, caption="Quarto sconto",;
    HelpContextID = 91157082,;
    cFormVar="w_COSCOFL4", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCOSCOFL4_4_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COSCOFL4,&i_cF..t_COSCOFL4),this.value)
    return(iif(xVal =1,1,;
    0))
  endfunc
  func oCOSCOFL4_4_8.GetRadio()
    this.Parent.oContained.w_COSCOFL4 = this.RadioValue()
    return .t.
  endfunc

  func oCOSCOFL4_4_8.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_COSCOFL4==1,1,;
      0))
  endfunc

  func oCOSCOFL4_4_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCOSCOGL5_4_9 as StdField with uid="HDBGCZHUIT",rtseq=57,rtrep=.f.,;
    cFormVar = "w_COSCOGL5", cQueryName = "COSCOGL5",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 107934299,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=191, Top=146, cGetPict='"99.9999"'

  func oCOSCOGL5_4_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_COSCOGL5 >=0)
    endwith
    return bRes
  endfunc

  add object oCOSCOFL5_4_10 as StdCheck with uid="YJTMHXVGBF",rtseq=58,rtrep=.f.,left=294, top=146, caption="Quinto sconto",;
    HelpContextID = 91157083,;
    cFormVar="w_COSCOFL5", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCOSCOFL5_4_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COSCOFL5,&i_cF..t_COSCOFL5),this.value)
    return(iif(xVal =1,1,;
    0))
  endfunc
  func oCOSCOFL5_4_10.GetRadio()
    this.Parent.oContained.w_COSCOFL5 = this.RadioValue()
    return .t.
  endfunc

  func oCOSCOFL5_4_10.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_COSCOFL5==1,1,;
      0))
  endfunc

  func oCOSCOFL5_4_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCOSCOGL6_4_11 as StdField with uid="EIMXPAZBWO",rtseq=59,rtrep=.f.,;
    cFormVar = "w_COSCOGL6", cQueryName = "COSCOGL6",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 107934300,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=191, Top=168, cGetPict='"99.9999"'

  func oCOSCOGL6_4_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_COSCOGL6 >=0)
    endwith
    return bRes
  endfunc

  add object oCOSCOFL6_4_12 as StdCheck with uid="EDBPPHNVNT",rtseq=60,rtrep=.f.,left=294, top=168, caption="Sesto sconto",;
    HelpContextID = 91157084,;
    cFormVar="w_COSCOFL6", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oCOSCOFL6_4_12.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COSCOFL6,&i_cF..t_COSCOFL6),this.value)
    return(iif(xVal =1,1,;
    0))
  endfunc
  func oCOSCOFL6_4_12.GetRadio()
    this.Parent.oContained.w_COSCOFL6 = this.RadioValue()
    return .t.
  endfunc

  func oCOSCOFL6_4_12.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_COSCOFL6==1,1,;
      0))
  endfunc

  func oCOSCOFL6_4_12.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oCOVALPUN_4_13 as StdField with uid="HGOHKSWWUS",rtseq=61,rtrep=.f.,;
    cFormVar = "w_COVALPUN", cQueryName = "COVALPUN",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 12770700,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=639, Top=117, cSayPict="v_PU(38+VVP)", cGetPict="v_GU(38+VVP)"

  add object oCOVALMCH_4_14 as StdField with uid="SFZPDGOVSF",rtseq=62,rtrep=.f.,;
    cFormVar = "w_COVALMCH", cQueryName = "COVALMCH",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    HelpContextID = 63102354,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=639, Top=146, cSayPict="v_PU(38+VVP)", cGetPict="v_GU(38+VVP)"

  add object oCONOTGEN_4_15 as StdMemo with uid="MCWPKRWFZL",rtseq=63,rtrep=.f.,;
    cFormVar = "w_CONOTGEN", cQueryName = "CONOTGEN",;
    bObbl = .f. , nPag = 4, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 154492300,;
   bGlobalFont=.t.,;
    Height=95, Width=435, Left=3, Top=211

  add object oCONOTCON_4_16 as StdMemo with uid="JBRNOTUOLV",rtseq=64,rtrep=.f.,;
    cFormVar = "w_CONOTCON", cQueryName = "CONOTCON",;
    bObbl = .f. , nPag = 4, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 221601164,;
   bGlobalFont=.t.,;
    Height=108, Width=435, Left=3, Top=329

  add object oCONOTMCH_4_17 as StdMemo with uid="PDOQUPMQQW",rtseq=65,rtrep=.f.,;
    cFormVar = "w_CONOTMCH", cQueryName = "CONOTMCH",;
    bObbl = .f. , nPag = 4, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 53829010,;
   bGlobalFont=.t.,;
    Height=225, Width=252, Left=526, Top=211

  add object oSIMBOLO_4_24 as StdField with uid="WDPQATWXDZ",rtseq=66,rtrep=.f.,;
    cFormVar = "w_SIMBOLO", cQueryName = "SIMBOLO",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Simbolo della valuta",;
    HelpContextID = 76706522,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=577, Top=4, InputMask=replicate('X',5)

  add object oCODI_4_25 as StdField with uid="AZSYHGFDNN",rtseq=67,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 176423462,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=68, Top=4, InputMask=replicate('X',15)

  add object oCODESCRI_4_26 as StdField with uid="XZRQTUNWLS",rtseq=68,rtrep=.f.,;
    cFormVar = "w_CODESCRI", cQueryName = "CODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 223346065,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=232, Top=4, InputMask=replicate('X',40)

  add object oDATA_4_28 as StdField with uid="PMNVNQSCOW",rtseq=69,rtrep=.f.,;
    cFormVar = "w_DATA", cQueryName = "DATA",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 175961142,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=706, Top=4

  add object oDATAC_4_29 as StdField with uid="QDPHHJPWXR",rtseq=70,rtrep=.f.,;
    cFormVar = "w_DATAC", cQueryName = "DATAC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 246215734,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=706, Top=33

  add object oCOTOTPUN_4_41 as StdField with uid="POESNZNYKS",rtseq=103,rtrep=.f.,;
    cFormVar = "w_COTOTPUN", cQueryName = "COTOTPUN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale punti premio",;
    HelpContextID = 3472780,;
   bGlobalFont=.t.,;
    Height=21, Width=90, Left=639, Top=88


  add object oObj_4_50 as cp_calclbl with uid="MNOEXWAKDG",left=3, top=57, width=184,height=14,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=4;
    , HelpContextID = 187533850


  add object oObj_4_51 as cp_calclbl with uid="YMBVHANBGV",left=3, top=80, width=184,height=14,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=4;
    , HelpContextID = 187533850


  add object oObj_4_52 as cp_calclbl with uid="QKCOATPRFI",left=3, top=102, width=184,height=14,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=4;
    , HelpContextID = 187533850


  add object oObj_4_53 as cp_calclbl with uid="IAOPJOARZH",left=3, top=124, width=184,height=14,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=4;
    , HelpContextID = 187533850


  add object oObj_4_54 as cp_calclbl with uid="OOJOIIHWDD",left=3, top=146, width=184,height=14,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=4;
    , HelpContextID = 187533850


  add object oObj_4_55 as cp_calclbl with uid="IAGWMGRZSK",left=3, top=168, width=184,height=14,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=4;
    , HelpContextID = 187533850

  add object oStr_4_18 as StdString with uid="GKYEAAOTBQ",Visible=.t., Left=72, Top=32,;
    Alignment=0, Width=151, Height=18,;
    Caption="Sconti incondizionati"  ;
  , bGlobalFont=.t.

  add object oStr_4_19 as StdString with uid="FCPUJQHXWC",Visible=.t., Left=257, Top=57,;
    Alignment=0, Width=19, Height=18,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_4_20 as StdString with uid="SFXXLZSEUH",Visible=.t., Left=7, Top=312,;
    Alignment=0, Width=121, Height=18,;
    Caption="Note contratto"  ;
  , bGlobalFont=.t.

  add object oStr_4_21 as StdString with uid="UDEZHSDBLV",Visible=.t., Left=667, Top=4,;
    Alignment=1, Width=37, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_22 as StdString with uid="CPTXPEWGZN",Visible=.t., Left=2, Top=4,;
    Alignment=1, Width=64, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_23 as StdString with uid="HMMFODCDUN",Visible=.t., Left=551, Top=33,;
    Alignment=1, Width=153, Height=18,;
    Caption="Dati consuntivi al:"  ;
  , bGlobalFont=.t.

  add object oStr_4_27 as StdString with uid="WNGRKQJYJW",Visible=.t., Left=552, Top=4,;
    Alignment=1, Width=24, Height=18,;
    Caption="in:"  ;
  , bGlobalFont=.t.

  add object oStr_4_30 as StdString with uid="WEXKLQDXBJ",Visible=.t., Left=7, Top=194,;
    Alignment=0, Width=167, Height=18,;
    Caption="Note in generazione"  ;
  , bGlobalFont=.t.

  add object oStr_4_31 as StdString with uid="EWISZFJRPE",Visible=.t., Left=475, Top=117,;
    Alignment=1, Width=160, Height=18,;
    Caption="Valore unitario punti:"  ;
  , bGlobalFont=.t.

  add object oStr_4_32 as StdString with uid="DKOTCYIAOT",Visible=.t., Left=475, Top=146,;
    Alignment=1, Width=160, Height=18,;
    Caption="Valore canvass:"  ;
  , bGlobalFont=.t.

  add object oStr_4_33 as StdString with uid="TSWOBEVNCH",Visible=.t., Left=520, Top=194,;
    Alignment=0, Width=140, Height=18,;
    Caption="Note canvass"  ;
  , bGlobalFont=.t.

  add object oStr_4_34 as StdString with uid="TUQWQVPOPY",Visible=.t., Left=257, Top=80,;
    Alignment=0, Width=19, Height=18,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_4_35 as StdString with uid="VYGCJDKCTI",Visible=.t., Left=257, Top=102,;
    Alignment=0, Width=19, Height=18,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_4_36 as StdString with uid="MRHAETANKT",Visible=.t., Left=257, Top=124,;
    Alignment=0, Width=19, Height=18,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_4_37 as StdString with uid="KKXZPZYYSQ",Visible=.t., Left=257, Top=146,;
    Alignment=0, Width=19, Height=18,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_4_38 as StdString with uid="BBWPZJFSUE",Visible=.t., Left=257, Top=168,;
    Alignment=0, Width=19, Height=18,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_4_40 as StdString with uid="USMBGVBMBI",Visible=.t., Left=284, Top=34,;
    Alignment=0, Width=74, Height=18,;
    Caption="validit�"  ;
  , bGlobalFont=.t.

  add object oStr_4_42 as StdString with uid="XKOVPZMTYS",Visible=.t., Left=481, Top=88,;
    Alignment=1, Width=154, Height=18,;
    Caption="Totale punti premio:"  ;
  , bGlobalFont=.t.

  add object oBox_4_39 as StdBox with uid="CQFBYHOZPF",left=4, top=50, width=380,height=2
enddefine

  define class tgsgp_mcoPag3 as StdContainer
    Width  = 796
    height = 464
    stdWidth  = 796
    stdheight = 464
  resizeXpos=238
  resizeYpos=381
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSIMBOLO_5_4 as StdField with uid="PNDMXBIHLP",rtseq=73,rtrep=.f.,;
    cFormVar = "w_SIMBOLO", cQueryName = "SIMBOLO",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Simbolo della valuta",;
    HelpContextID = 76706522,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=542, Top=11, InputMask=replicate('X',5)

  add object oCODI_5_5 as StdField with uid="DZBITNLHAA",rtseq=74,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 176423462,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=69, Top=11, InputMask=replicate('X',15)

  add object oCODESCRI_5_6 as StdField with uid="HHVUGQTYNM",rtseq=75,rtrep=.f.,;
    cFormVar = "w_CODESCRI", cQueryName = "CODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 223346065,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=233, Top=11, InputMask=replicate('X',40)

  add object oDATA_5_7 as StdField with uid="ELJUNDUVTM",rtseq=76,rtrep=.f.,;
    cFormVar = "w_DATA", cQueryName = "DATA",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 175961142,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=706, Top=11

  add object oDATAC_5_8 as StdField with uid="FDMIEYRTKM",rtseq=77,rtrep=.f.,;
    cFormVar = "w_DATAC", cQueryName = "DATAC",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 246215734,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=706, Top=37


  add object DOCORI as cp_zoombox with uid="APQWNQXGNM",left=4, top=62, width=779,height=180,;
    caption='DM',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSGP_MCO",bOptions=.f.,bAdvOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bQueryOnDblClick=.f.,cZoomOnZoom="",bRetriveAllRows=.f.,cMenuFile="",bNoZoomGridShape=.f.,;
    cEvent = "ActivatePage 3",;
    nPag=5;
    , HelpContextID = 171360310


  add object DOCDES as cp_zoombox with uid="BZDOSFELIL",left=4, top=262, width=261,height=173,;
    caption='DM',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSGP1MCO",bOptions=.f.,bAdvOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bQueryOnDblClick=.f.,cZoomOnZoom="",bRetriveAllRows=.f.,cMenuFile="",bNoZoomGridShape=.f.,;
    cEvent = "ActivatePage 3",;
    nPag=5;
    , HelpContextID = 171360310

  add object oCOVALFAT_5_13 as StdField with uid="CTRFYBICSY",rtseq=92,rtrep=.f.,;
    cFormVar = "w_COVALFAT", cQueryName = "COVALFAT",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 180542854,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=658, Top=269, cSayPict="v_PU(38+VVP)", cGetPict="v_GU(38+VVP)"

  add object oCOVALMCH_5_15 as StdField with uid="EAKFOWLLDV",rtseq=93,rtrep=.f.,;
    cFormVar = "w_COVALMCH", cQueryName = "COVALMCH",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 63102354,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=658, Top=298, cSayPict="v_PU(38+VVP)", cGetPict="v_GU(38+VVP)"

  add object oCOSCOCOM_5_17 as StdField with uid="ZXGBVMLUCT",rtseq=94,rtrep=.f.,;
    cFormVar = "w_COSCOCOM", cQueryName = "COSCOCOM",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 227609997,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=658, Top=327, cSayPict="v_PU(38+VVP)", cGetPict="v_GU(38+VVP)"

  add object oCOVALPRE_5_19 as StdField with uid="TZIMUDBTNV",rtseq=95,rtrep=.f.,;
    cFormVar = "w_COVALPRE", cQueryName = "COVALPRE",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 12770709,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=658, Top=356, cSayPict="v_PU(38+VVP)", cGetPict="v_GU(38+VVP)"

  add object oVALTCONT_5_22 as StdField with uid="ITBAOJFCZS",rtseq=96,rtrep=.f.,;
    cFormVar = "w_VALTCONT", cQueryName = "VALTCONT",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 37784150,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=658, Top=414, cSayPict="v_PU(38+VVP)", cGetPict="v_GU(38+VVP)"

  add object oCOTOTPRE_5_23 as StdField with uid="LWMGQELEUW",rtseq=97,rtrep=.f.,;
    cFormVar = "w_COTOTPRE", cQueryName = "COTOTPRE",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 3472789,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=658, Top=385, cSayPict="v_PU(38+VVP)", cGetPict="v_GU(38+VVP)"

  add object oINC1_5_29 as StdField with uid="EMSDNHTPUQ",rtseq=98,rtrep=.f.,;
    cFormVar = "w_INC1", cQueryName = "INC1",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 174846342,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=430, Top=298, cGetPict='"99.99"'

  add object oINC2_5_31 as StdField with uid="VUNLBNUOOR",rtseq=99,rtrep=.f.,;
    cFormVar = "w_INC2", cQueryName = "INC2",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 174911878,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=430, Top=327, cGetPict='"99.99"'

  add object oINC3_5_33 as StdField with uid="HOPXHPLPEJ",rtseq=100,rtrep=.f.,;
    cFormVar = "w_INC3", cQueryName = "INC3",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 174977414,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=430, Top=356, cGetPict='"99.99"'

  add object oINC4_5_35 as StdField with uid="NOWMVKQYHI",rtseq=101,rtrep=.f.,;
    cFormVar = "w_INC4", cQueryName = "INC4",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 175042950,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=430, Top=385, cGetPict='"99.99"'

  add object oINC5_5_37 as StdField with uid="SMKGCHFRVC",rtseq=102,rtrep=.f.,;
    cFormVar = "w_INC5", cQueryName = "INC5",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    HelpContextID = 175108486,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=430, Top=410, cGetPict='"99.99"'


  add object oBtn_5_49 as StdButton with uid="PKKSUWHQWC",left=266, top=276, width=20,height=19,;
    caption="...", nPag=5;
    , ToolTipText = "Visualizza la generazione premi associata";
    , HelpContextID = 171540438;
  , bGlobalFont=.t.

    proc oBtn_5_49.Click()
      with this.Parent.oContained
        GSGP_BCQ(this.Parent.oContained,.w_GENERA, .w_GPSERIAL)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_5_49.mCond()
    with this.Parent.oContained
      return (!Empty( .w_GPSERIAL) )
    endwith
  endfunc

  add object oStr_5_1 as StdString with uid="AWZOSQBIRS",Visible=.t., Left=663, Top=11,;
    Alignment=1, Width=37, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_5_2 as StdString with uid="ZFUVYUUDPG",Visible=.t., Left=3, Top=11,;
    Alignment=1, Width=64, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_3 as StdString with uid="XNPPHUHTDY",Visible=.t., Left=547, Top=37,;
    Alignment=1, Width=153, Height=18,;
    Caption="Dati consuntivi al:"  ;
  , bGlobalFont=.t.

  add object oStr_5_12 as StdString with uid="IIMWOUKZUH",Visible=.t., Left=515, Top=269,;
    Alignment=1, Width=141, Height=18,;
    Caption="Valore fatturato globale:"  ;
  , bGlobalFont=.t.

  add object oStr_5_14 as StdString with uid="JTLNBOQCJR",Visible=.t., Left=523, Top=298,;
    Alignment=1, Width=133, Height=18,;
    Caption="Valore canvass:"  ;
  , bGlobalFont=.t.

  add object oStr_5_16 as StdString with uid="HNBNVYUXAA",Visible=.t., Left=512, Top=327,;
    Alignment=1, Width=144, Height=18,;
    Caption="Valore sconto comm.:"  ;
  , bGlobalFont=.t.

  add object oStr_5_18 as StdString with uid="RLGUOPCIPR",Visible=.t., Left=513, Top=356,;
    Alignment=1, Width=143, Height=18,;
    Caption="Valore obiettivi raggiunti:"  ;
  , bGlobalFont=.t.

  add object oStr_5_20 as StdString with uid="TVESJOPXUF",Visible=.t., Left=513, Top=385,;
    Alignment=1, Width=143, Height=18,;
    Caption="Valore totale storni:"  ;
  , bGlobalFont=.t.

  add object oStr_5_21 as StdString with uid="KZMKQJFWZV",Visible=.t., Left=523, Top=414,;
    Alignment=1, Width=133, Height=18,;
    Caption="Valore tot.contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_5_24 as StdString with uid="OKOHGMPGDJ",Visible=.t., Left=268, Top=298,;
    Alignment=1, Width=160, Height=18,;
    Caption="Incidenza fissa su fatt.:"  ;
  , bGlobalFont=.t.

  add object oStr_5_25 as StdString with uid="WFKCLVDKUM",Visible=.t., Left=268, Top=327,;
    Alignment=1, Width=160, Height=18,;
    Caption="Incidenza sconto su fatt.:"  ;
  , bGlobalFont=.t.

  add object oStr_5_26 as StdString with uid="RXCYGXJGRQ",Visible=.t., Left=268, Top=414,;
    Alignment=1, Width=160, Height=18,;
    Caption="Incidenza globale su fatt.:"  ;
  , bGlobalFont=.t.

  add object oStr_5_27 as StdString with uid="BBVDKQSOCD",Visible=.t., Left=268, Top=356,;
    Alignment=1, Width=160, Height=18,;
    Caption="Incidenza obiettivi su fatt.:"  ;
  , bGlobalFont=.t.

  add object oStr_5_28 as StdString with uid="CEOJLNULDW",Visible=.t., Left=268, Top=385,;
    Alignment=1, Width=160, Height=18,;
    Caption="Incidenza storni su fatt.:"  ;
  , bGlobalFont=.t.

  add object oStr_5_30 as StdString with uid="UQDKVZKYAV",Visible=.t., Left=493, Top=301,;
    Alignment=2, Width=18, Height=18,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_5_32 as StdString with uid="BOPJEEEGEM",Visible=.t., Left=493, Top=327,;
    Alignment=2, Width=18, Height=18,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_5_34 as StdString with uid="TRGMVTKXSF",Visible=.t., Left=493, Top=356,;
    Alignment=2, Width=18, Height=18,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_5_36 as StdString with uid="HIYMAOUBTF",Visible=.t., Left=493, Top=385,;
    Alignment=2, Width=18, Height=18,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_5_38 as StdString with uid="KTPWJJCOQX",Visible=.t., Left=493, Top=414,;
    Alignment=2, Width=18, Height=18,;
    Caption="%"  ;
  , bGlobalFont=.t.

  add object oStr_5_46 as StdString with uid="HQTUAZJBUQ",Visible=.t., Left=7, Top=243,;
    Alignment=0, Width=385, Height=18,;
    Caption="Documenti generati dal contratto e dettaglio generazione premi "    , forecolor=rgb(0,0,255);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_47 as StdString with uid="RZCKYDGVNO",Visible=.t., Left=7, Top=43,;
    Alignment=0, Width=374, Height=18,;
    Caption="Documenti considerati dalla generazione premi"    , forecolor=rgb(0,128,0);
  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_5_11 as StdBox with uid="KFKHXBSJZW",left=267, top=296, width=529,height=1
enddefine

* --- Defining Body row
define class tgsgp_mcoBodyRow as CPBodyRowCnt
  Width=714
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="LVRCRFITEU",rtseq=34,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 17112214,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=43, Left=-2, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oCOKEYART_2_2 as StdTrsField with uid="LSSWCAWSYD",rtseq=35,rtrep=.t.,;
    cFormVar="w_COKEYART",value=space(20),;
    HelpContextID = 250580358,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=167, Left=43, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_COKEYART"

  func oCOKEYART_2_2.mCond()
    with this.Parent.oContained
      return (.w_COATTRIB <> 1 And (Empty(.w_CODATGEN) Or .w_SRV = 'A'))
    endwith
  endfunc

  func oCOKEYART_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOKEYART_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCOKEYART_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oCOKEYART_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"CHIAVI ARTICOLO",'',this.parent.oContained
  endproc
  proc oCOKEYART_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_COKEYART
    i_obj.ecpSave()
  endproc

  add object oCOUNIMIS_2_7 as StdTrsField with uid="WPYCIGVRZA",rtseq=40,rtrep=.t.,;
    cFormVar="w_COUNIMIS",value=space(3),;
    HelpContextID = 203035257,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
   bGlobalFont=.t.,;
    Height=17, Width=53, Left=481, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_COUNIMIS"

  func oCOUNIMIS_2_7.mCond()
    with this.Parent.oContained
      return (Empty(.w_CODATGEN) Or .w_SRV = 'A')
    endwith
  endfunc

  func oCOUNIMIS_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOUNIMIS_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCOUNIMIS_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oCOUNIMIS_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"UNITA' DI MISURA",'GSMAUMVM.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oCOUNIMIS_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_COUNIMIS
    i_obj.ecpSave()
  endproc

  add object oCOUMALTE_2_8 as StdTrsCheck with uid="UTKGURMJUV",rtrep=.t.,;
    cFormVar="w_COUMALTE",  caption="",;
    HelpContextID = 90631573,;
    Left=538, Top=0, Width=35,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , Autosize=.F.;
   , bGlobalFont=.t.


  func oCOUMALTE_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COUMALTE,&i_cF..t_COUMALTE),this.value)
    return(iif(xVal =1,1,;
    0))
  endfunc
  func oCOUMALTE_2_8.GetRadio()
    this.Parent.oContained.w_COUMALTE = this.RadioValue()
    return .t.
  endfunc

  func oCOUMALTE_2_8.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_COUMALTE==1,1,;
      0))
  endfunc

  func oCOUMALTE_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oCOUMALTE_2_8.mCond()
    with this.Parent.oContained
      return ( (Empty(.w_CODATGEN) Or .w_SRV = 'A') And .w_COUNIMIS= .w_UNMIS1 And Not Empty(.w_COKEYART) And .w_COATTRIB <>1)
    endwith
  endfunc

  add object oCOCALPRM_2_9 as StdTrsCombo with uid="CPSQKBZXIC",rtrep=.t.,;
    cFormVar="w_COCALPRM", RowSource=""+"Su quantit�,"+"Su valore" , ;
    HelpContextID = 12848525,;
    Height=21, Width=76, Left=576, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oCOCALPRM_2_9.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COCALPRM,&i_cF..t_COCALPRM),this.value)
    return(iif(xVal =1,'Q',;
    iif(xVal =2,'V',;
    space(1))))
  endfunc
  func oCOCALPRM_2_9.GetRadio()
    this.Parent.oContained.w_COCALPRM = this.RadioValue()
    return .t.
  endfunc

  func oCOCALPRM_2_9.ToRadio()
    this.Parent.oContained.w_COCALPRM=trim(this.Parent.oContained.w_COCALPRM)
    return(;
      iif(this.Parent.oContained.w_COCALPRM=='Q',1,;
      iif(this.Parent.oContained.w_COCALPRM=='V',2,;
      0)))
  endfunc

  func oCOCALPRM_2_9.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDESART_2_12 as StdTrsField with uid="MZBHDLYTDQ",rtseq=44,rtrep=.t.,;
    cFormVar="w_DESART",value=space(40),enabled=.f.,;
    HelpContextID = 207820746,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=8, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=16, Width=266, Left=212, Top=0, InputMask=replicate('X',40)

  add object oCOATTRIB_2_19 as StdTrsCombo with uid="YSOFRVDTBH",rtrep=.t.,;
    cFormVar="w_COATTRIB", RowSource=""+"Si,"+"No" , enabled=.f.,;
    HelpContextID = 30331496,;
    Height=21, Width=49, Left=660, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2  , DisabledBackColor=rgb(255,255,255);
, bIsInHeader=.f.;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.


  func oCOATTRIB_2_19.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..COATTRIB,&i_cF..t_COATTRIB),this.value)
    return(iif(xVal =1,1,;
    iif(xVal =2,2,;
    0)))
  endfunc
  func oCOATTRIB_2_19.GetRadio()
    this.Parent.oContained.w_COATTRIB = this.RadioValue()
    return .t.
  endfunc

  func oCOATTRIB_2_19.ToRadio()
    
    return(;
      iif(this.Parent.oContained.w_COATTRIB==1,1,;
      iif(this.Parent.oContained.w_COATTRIB==2,2,;
      0)))
  endfunc

  func oCOATTRIB_2_19.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=8
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsgp_mco','CCOMMAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".COCODICE=CCOMMAST.COCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
