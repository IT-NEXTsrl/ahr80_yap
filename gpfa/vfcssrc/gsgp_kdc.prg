* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsgp_kdc                                                        *
*              Duplicazione contratto                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-01-27                                                      *
* Last revis.: 2012-10-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsgp_kdc",oParentObject))

* --- Class definition
define class tgsgp_kdc as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 787
  Height = 165
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-10"
  HelpContextID=82415977
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=32

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  AGENTI_IDX = 0
  GRUPPACQ_IDX = 0
  cPrg = "gsgp_kdc"
  cComment = "Duplicazione contratto"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_COCODICE = space(15)
  w_CODESCRI = space(40)
  w_COOLDCOD = space(15)
  w_COOLDDES = space(40)
  w_CONEWCOD = space(15)
  w_CONEWDES = space(40)
  w_COFLGCIC = space(1)
  w_FL_CICLO = space(1)
  w_COFLGTIP = space(1)
  w_FL_TIPOC = space(1)
  w_COORIINT = space(1)
  w_COCODINT = space(15)
  w_CODESINT = space(40)
  w_COTIPINT = space(1)
  o_COTIPINT = space(1)
  w_COTIPINT = space(1)
  w_COTIPINT = space(1)
  w_CODESINT = space(40)
  w_COCODINT = space(15)
  w_COCODAGE = space(15)
  w_AGCODFOR = space(15)
  w_COCODGRU = space(15)
  w_OBTEST = ctod('  /  /  ')
  w_NWFLGSTA = space(1)
  w_NWDATREG = ctod('  /  /  ')
  w_NWDATFIN = ctod('  /  /  ')
  w_NWDATINI = ctod('  /  /  ')
  w_NWPRIORI = 0
  w_CODATREG = ctod('  /  /  ')
  w_CODATINI = ctod('  /  /  ')
  w_CODATFIN = ctod('  /  /  ')
  w_COPRIORI = 0
  w_TSFLMODE = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsgp_kdcPag1","gsgp_kdc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCONEWCOD_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='AGENTI'
    this.cWorkTables[3]='GRUPPACQ'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        GSGP_BDC(this,"COPYC")
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_COCODICE=space(15)
      .w_CODESCRI=space(40)
      .w_COOLDCOD=space(15)
      .w_COOLDDES=space(40)
      .w_CONEWCOD=space(15)
      .w_CONEWDES=space(40)
      .w_COFLGCIC=space(1)
      .w_FL_CICLO=space(1)
      .w_COFLGTIP=space(1)
      .w_FL_TIPOC=space(1)
      .w_COORIINT=space(1)
      .w_COCODINT=space(15)
      .w_CODESINT=space(40)
      .w_COTIPINT=space(1)
      .w_COTIPINT=space(1)
      .w_COTIPINT=space(1)
      .w_CODESINT=space(40)
      .w_COCODINT=space(15)
      .w_COCODAGE=space(15)
      .w_AGCODFOR=space(15)
      .w_COCODGRU=space(15)
      .w_OBTEST=ctod("  /  /  ")
      .w_NWFLGSTA=space(1)
      .w_NWDATREG=ctod("  /  /  ")
      .w_NWDATFIN=ctod("  /  /  ")
      .w_NWDATINI=ctod("  /  /  ")
      .w_NWPRIORI=0
      .w_CODATREG=ctod("  /  /  ")
      .w_CODATINI=ctod("  /  /  ")
      .w_CODATFIN=ctod("  /  /  ")
      .w_COPRIORI=0
      .w_TSFLMODE=space(1)
      .w_COCODICE=oParentObject.w_COCODICE
      .w_CODESCRI=oParentObject.w_CODESCRI
      .w_COFLGCIC=oParentObject.w_COFLGCIC
      .w_COFLGTIP=oParentObject.w_COFLGTIP
      .w_CODATREG=oParentObject.w_CODATREG
      .w_CODATINI=oParentObject.w_CODATINI
      .w_CODATFIN=oParentObject.w_CODATFIN
      .w_COPRIORI=oParentObject.w_COPRIORI
          .DoRTCalc(1,2,.f.)
        .w_COOLDCOD = .w_COCODICE
        .w_COOLDDES = .w_CODESCRI
          .DoRTCalc(5,7,.f.)
        .w_FL_CICLO = .w_COFLGCIC
          .DoRTCalc(9,9,.f.)
        .w_FL_TIPOC = .w_COFLGTIP
        .w_COORIINT = this.oParentObject.w_COTIPINT
        .w_COCODINT = ''
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_COCODINT))
          .link_1_17('Full')
        endif
          .DoRTCalc(13,13,.f.)
        .w_COTIPINT = iif(.w_COFLGCIC='V','C','F')
        .w_COTIPINT = .w_COORIINT
        .w_COTIPINT = .w_COORIINT
          .DoRTCalc(17,17,.f.)
        .w_COCODINT = ''
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_COCODINT))
          .link_1_23('Full')
        endif
        .w_COCODAGE = ''
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_COCODAGE))
          .link_1_24('Full')
        endif
          .DoRTCalc(20,20,.f.)
        .w_COCODGRU = ''
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_COCODGRU))
          .link_1_26('Full')
        endif
        .w_OBTEST = i_DATSYS
        .w_NWFLGSTA = 'S'
        .w_NWDATREG = .w_CODATREG
        .w_NWDATFIN = .w_CODATFIN
        .w_NWDATINI = .w_CODATINI
        .w_NWPRIORI = .w_COPRIORI
          .DoRTCalc(28,31,.f.)
        .w_TSFLMODE = this.oParentObject.w_COFLMODE
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_COCODICE=.w_COCODICE
      .oParentObject.w_CODESCRI=.w_CODESCRI
      .oParentObject.w_COFLGCIC=.w_COFLGCIC
      .oParentObject.w_COFLGTIP=.w_COFLGTIP
      .oParentObject.w_CODATREG=.w_CODATREG
      .oParentObject.w_CODATINI=.w_CODATINI
      .oParentObject.w_CODATFIN=.w_CODATFIN
      .oParentObject.w_COPRIORI=.w_COPRIORI
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_COOLDCOD = .w_COCODICE
            .w_COOLDDES = .w_CODESCRI
        .DoRTCalc(5,7,.t.)
            .w_FL_CICLO = .w_COFLGCIC
        .DoRTCalc(9,9,.t.)
            .w_FL_TIPOC = .w_COFLGTIP
        .DoRTCalc(11,11,.t.)
        if .o_COTIPINT<>.w_COTIPINT
            .w_COCODINT = ''
          .link_1_17('Full')
        endif
        .DoRTCalc(13,17,.t.)
        if .o_COTIPINT<>.w_COTIPINT
            .w_COCODINT = ''
          .link_1_23('Full')
        endif
        if .o_COTIPINT<>.w_COTIPINT
            .w_COCODAGE = ''
          .link_1_24('Full')
        endif
        .DoRTCalc(20,20,.t.)
        if .o_COTIPINT<>.w_COTIPINT
            .w_COCODGRU = ''
          .link_1_26('Full')
        endif
        .DoRTCalc(22,23,.t.)
            .w_NWDATREG = .w_CODATREG
            .w_NWDATFIN = .w_CODATFIN
            .w_NWDATINI = .w_CODATINI
            .w_NWPRIORI = .w_COPRIORI
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(28,32,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCOCODINT_1_17.enabled = this.oPgFrm.Page1.oPag.oCOCODINT_1_17.mCond()
    this.oPgFrm.Page1.oPag.oCOCODINT_1_23.enabled = this.oPgFrm.Page1.oPag.oCOCODINT_1_23.mCond()
    this.oPgFrm.Page1.oPag.oCOCODAGE_1_24.enabled = this.oPgFrm.Page1.oPag.oCOCODAGE_1_24.mCond()
    this.oPgFrm.Page1.oPag.oCOCODGRU_1_26.enabled = this.oPgFrm.Page1.oPag.oCOCODGRU_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oCOCODINT_1_17.visible=!this.oPgFrm.Page1.oPag.oCOCODINT_1_17.mHide()
    this.oPgFrm.Page1.oPag.oCODESINT_1_18.visible=!this.oPgFrm.Page1.oPag.oCODESINT_1_18.mHide()
    this.oPgFrm.Page1.oPag.oCOTIPINT_1_20.visible=!this.oPgFrm.Page1.oPag.oCOTIPINT_1_20.mHide()
    this.oPgFrm.Page1.oPag.oCOTIPINT_1_21.visible=!this.oPgFrm.Page1.oPag.oCOTIPINT_1_21.mHide()
    this.oPgFrm.Page1.oPag.oCODESINT_1_22.visible=!this.oPgFrm.Page1.oPag.oCODESINT_1_22.mHide()
    this.oPgFrm.Page1.oPag.oCOCODINT_1_23.visible=!this.oPgFrm.Page1.oPag.oCOCODINT_1_23.mHide()
    this.oPgFrm.Page1.oPag.oCOCODAGE_1_24.visible=!this.oPgFrm.Page1.oPag.oCOCODAGE_1_24.mHide()
    this.oPgFrm.Page1.oPag.oCOCODGRU_1_26.visible=!this.oPgFrm.Page1.oPag.oCOCODGRU_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_32.visible=!this.oPgFrm.Page1.oPag.oStr_1_32.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COCODINT
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCODINT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPINT);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPINT;
                     ,'ANCODICE',trim(this.w_COCODINT))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODINT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCODINT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCODINT_1_17'),i_cWhere,'GSAR_BZC',"ELENCO CLIENTI/FORNITORI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPINT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPINT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCODINT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPINT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPINT;
                       ,'ANCODICE',this.w_COCODINT)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODINT = NVL(_Link_.ANCODICE,space(15))
      this.w_CODESINT = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COCODINT = space(15)
      endif
      this.w_CODESINT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCODINT
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODINT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCODINT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPINT);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPINT;
                     ,'ANCODICE',trim(this.w_COCODINT))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODINT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCODINT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCODINT_1_23'),i_cWhere,'GSAR_BZC',"ELENCO CLIENTI/FORNITORI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPINT<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPINT);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODINT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCODINT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPINT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPINT;
                       ,'ANCODICE',this.w_COCODINT)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODINT = NVL(_Link_.ANCODICE,space(15))
      this.w_CODESINT = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COCODINT = space(15)
      endif
      this.w_CODESINT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODINT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCODAGE
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_COCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGCODFOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_COCODAGE))
          select AGCODAGE,AGDESAGE,AGCODFOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCOCODAGE_1_24'),i_cWhere,'',"AGENTI",'GSGP_AGE.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGCODFOR";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGCODFOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGCODFOR";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_COCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_COCODAGE)
            select AGCODAGE,AGDESAGE,AGCODFOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODAGE = NVL(_Link_.AGCODAGE,space(15))
      this.w_CODESINT = NVL(_Link_.AGDESAGE,space(40))
      this.w_AGCODFOR = NVL(_Link_.AGCODFOR,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_COCODAGE = space(15)
      endif
      this.w_CODESINT = space(40)
      this.w_AGCODFOR = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!Empty(.w_AGCODFOR)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non valido o senza rif.fornitore")
        endif
        this.w_COCODAGE = space(15)
        this.w_CODESINT = space(40)
        this.w_AGCODFOR = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCODGRU
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUPPACQ_IDX,3]
    i_lTable = "GRUPPACQ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2], .t., this.GRUPPACQ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUPPACQ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GACODICE like "+cp_ToStrODBC(trim(this.w_COCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GACODICE,GADESGRP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GACODICE',trim(this.w_COCODGRU))
          select GACODICE,GADESGRP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODGRU)==trim(_Link_.GACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRUPPACQ','*','GACODICE',cp_AbsName(oSource.parent,'oCOCODGRU_1_26'),i_cWhere,'',"GRUPPI DI ACQUISTO",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GACODICE,GADESGRP";
                     +" from "+i_cTable+" "+i_lTable+" where GACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GACODICE',oSource.xKey(1))
            select GACODICE,GADESGRP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GACODICE,GADESGRP";
                   +" from "+i_cTable+" "+i_lTable+" where GACODICE="+cp_ToStrODBC(this.w_COCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GACODICE',this.w_COCODGRU)
            select GACODICE,GADESGRP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODGRU = NVL(_Link_.GACODICE,space(15))
      this.w_CODESINT = NVL(_Link_.GADESGRP,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_COCODGRU = space(15)
      endif
      this.w_CODESINT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2])+'\'+cp_ToStr(_Link_.GACODICE,1)
      cp_ShowWarn(i_cKey,this.GRUPPACQ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOOLDCOD_1_4.value==this.w_COOLDCOD)
      this.oPgFrm.Page1.oPag.oCOOLDCOD_1_4.value=this.w_COOLDCOD
    endif
    if not(this.oPgFrm.Page1.oPag.oCOOLDDES_1_5.value==this.w_COOLDDES)
      this.oPgFrm.Page1.oPag.oCOOLDDES_1_5.value=this.w_COOLDDES
    endif
    if not(this.oPgFrm.Page1.oPag.oCONEWCOD_1_7.value==this.w_CONEWCOD)
      this.oPgFrm.Page1.oPag.oCONEWCOD_1_7.value=this.w_CONEWCOD
    endif
    if not(this.oPgFrm.Page1.oPag.oCONEWDES_1_8.value==this.w_CONEWDES)
      this.oPgFrm.Page1.oPag.oCONEWDES_1_8.value=this.w_CONEWDES
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODINT_1_17.value==this.w_COCODINT)
      this.oPgFrm.Page1.oPag.oCOCODINT_1_17.value=this.w_COCODINT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESINT_1_18.value==this.w_CODESINT)
      this.oPgFrm.Page1.oPag.oCODESINT_1_18.value=this.w_CODESINT
    endif
    if not(this.oPgFrm.Page1.oPag.oCOTIPINT_1_20.RadioValue()==this.w_COTIPINT)
      this.oPgFrm.Page1.oPag.oCOTIPINT_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCOTIPINT_1_21.RadioValue()==this.w_COTIPINT)
      this.oPgFrm.Page1.oPag.oCOTIPINT_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESINT_1_22.value==this.w_CODESINT)
      this.oPgFrm.Page1.oPag.oCODESINT_1_22.value=this.w_CODESINT
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODINT_1_23.value==this.w_COCODINT)
      this.oPgFrm.Page1.oPag.oCOCODINT_1_23.value=this.w_COCODINT
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODAGE_1_24.value==this.w_COCODAGE)
      this.oPgFrm.Page1.oPag.oCOCODAGE_1_24.value=this.w_COCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODGRU_1_26.value==this.w_COCODGRU)
      this.oPgFrm.Page1.oPag.oCOCODGRU_1_26.value=this.w_COCODGRU
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_CONEWCOD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONEWCOD_1_7.SetFocus()
            i_bnoObbl = !empty(.w_CONEWCOD)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(!Empty(.w_AGCODFOR))  and not(.w_COTIPINT <> 'A' OR .w_FL_TIPOC<>'O')  and (.w_COTIPINT = 'A' AND .w_FL_TIPOC='O')  and not(empty(.w_COCODAGE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCODAGE_1_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non valido o senza rif.fornitore")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_COTIPINT = this.w_COTIPINT
    return

enddefine

* --- Define pages as container
define class tgsgp_kdcPag1 as StdContainer
  Width  = 783
  height = 165
  stdWidth  = 783
  stdheight = 165
  resizeXpos=537
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOOLDCOD_1_4 as StdField with uid="PLWZVDYWJU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_COOLDCOD", cQueryName = "COOLDCOD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 223890838,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=229, Top=13, InputMask=replicate('X',15)

  add object oCOOLDDES_1_5 as StdField with uid="RESTIOTFGC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_COOLDDES", cQueryName = "COOLDDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 61321849,;
   bGlobalFont=.t.,;
    Height=21, Width=424, Left=350, Top=13, InputMask=replicate('X',40)

  add object oCONEWCOD_1_7 as StdField with uid="YJUWSTQGPZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CONEWCOD", cQueryName = "CONEWCOD",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 204430742,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=229, Top=41, InputMask=replicate('X',15)

  add object oCONEWDES_1_8 as StdField with uid="PAYGQURQII",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CONEWDES", cQueryName = "CONEWDES",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 80781945,;
   bGlobalFont=.t.,;
    Height=21, Width=424, Left=350, Top=41, InputMask=replicate('X',40)

  add object oCOCODINT_1_17 as StdField with uid="PLRRBKMMOF",rtseq=12,rtrep=.f.,;
    cFormVar = "w_COCODINT", cQueryName = "COCODINT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 123080070,;
   bGlobalFont=.t.,;
    Height=21, Width=137, Left=229, Top=75, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPINT", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCODINT"

  func oCOCODINT_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COTIPINT $ 'CF' AND .w_FL_TIPOC<>'O')
    endwith
   endif
  endfunc

  func oCOCODINT_1_17.mHide()
    with this.Parent.oContained
      return (!.w_COTIPINT $ 'CF' OR .w_FL_TIPOC='O')
    endwith
  endfunc

  func oCOCODINT_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODINT_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODINT_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPINT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPINT)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCODINT_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"ELENCO CLIENTI/FORNITORI",'',this.parent.oContained
  endproc
  proc oCOCODINT_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPINT
     i_obj.w_ANCODICE=this.parent.oContained.w_COCODINT
     i_obj.ecpSave()
  endproc

  add object oCODESINT_1_18 as StdField with uid="WANWKGXQEV",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODESINT", cQueryName = "CODESINT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 108002694,;
   bGlobalFont=.t.,;
    Height=21, Width=404, Left=370, Top=75, InputMask=replicate('X',40)

  func oCODESINT_1_18.mHide()
    with this.Parent.oContained
      return (.w_FL_TIPOC='O')
    endwith
  endfunc


  add object oCOTIPINT_1_20 as StdCombo with uid="GOVIRAKFCC",rtseq=15,rtrep=.f.,left=229,top=75,width=147,height=21;
    , ToolTipText = "Tipo intestatario";
    , HelpContextID = 110820742;
    , cFormVar="w_COTIPINT",RowSource=""+"Cliente,"+"Agente,"+"Gruppo di acquisto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOTIPINT_1_20.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'A',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oCOTIPINT_1_20.GetRadio()
    this.Parent.oContained.w_COTIPINT = this.RadioValue()
    return .t.
  endfunc

  func oCOTIPINT_1_20.SetRadio()
    this.Parent.oContained.w_COTIPINT=trim(this.Parent.oContained.w_COTIPINT)
    this.value = ;
      iif(this.Parent.oContained.w_COTIPINT=='C',1,;
      iif(this.Parent.oContained.w_COTIPINT=='A',2,;
      iif(this.Parent.oContained.w_COTIPINT=='G',3,;
      0)))
  endfunc

  func oCOTIPINT_1_20.mHide()
    with this.Parent.oContained
      return (.w_FL_CICLO='A' OR .w_FL_TIPOC<>'O')
    endwith
  endfunc

  func oCOTIPINT_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_COCODINT)
        bRes2=.link_1_17('Full')
      endif
      if .not. empty(.w_COCODINT)
        bRes2=.link_1_23('Full')
      endif
    endwith
    return bRes
  endfunc


  add object oCOTIPINT_1_21 as StdCombo with uid="LHNADHXXLK",rtseq=16,rtrep=.f.,left=229,top=75,width=147,height=21;
    , ToolTipText = "Tipo intestatario";
    , HelpContextID = 110820742;
    , cFormVar="w_COTIPINT",RowSource=""+"Fornitore,"+"Gruppo di acquisto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCOTIPINT_1_21.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'G',;
    space(1))))
  endfunc
  func oCOTIPINT_1_21.GetRadio()
    this.Parent.oContained.w_COTIPINT = this.RadioValue()
    return .t.
  endfunc

  func oCOTIPINT_1_21.SetRadio()
    this.Parent.oContained.w_COTIPINT=trim(this.Parent.oContained.w_COTIPINT)
    this.value = ;
      iif(this.Parent.oContained.w_COTIPINT=='F',1,;
      iif(this.Parent.oContained.w_COTIPINT=='G',2,;
      0))
  endfunc

  func oCOTIPINT_1_21.mHide()
    with this.Parent.oContained
      return (.w_FL_CICLO='V' OR .w_FL_TIPOC<>'O')
    endwith
  endfunc

  func oCOTIPINT_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_COCODINT)
        bRes2=.link_1_17('Full')
      endif
      if .not. empty(.w_COCODINT)
        bRes2=.link_1_23('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODESINT_1_22 as StdField with uid="EXUSEIEYJP",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODESINT", cQueryName = "CODESINT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 108002694,;
   bGlobalFont=.t.,;
    Height=21, Width=255, Left=519, Top=75, InputMask=replicate('X',40)

  func oCODESINT_1_22.mHide()
    with this.Parent.oContained
      return (.w_FL_TIPOC<>'O')
    endwith
  endfunc

  add object oCOCODINT_1_23 as StdField with uid="DBVXTVYYMJ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_COCODINT", cQueryName = "COCODINT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 123080070,;
   bGlobalFont=.t.,;
    Height=21, Width=137, Left=380, Top=75, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPINT", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCODINT"

  func oCOCODINT_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COTIPINT $ 'CF' AND .w_FL_TIPOC='O')
    endwith
   endif
  endfunc

  func oCOCODINT_1_23.mHide()
    with this.Parent.oContained
      return (!.w_COTIPINT $ 'CF' OR .w_FL_TIPOC<>'O')
    endwith
  endfunc

  func oCOCODINT_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODINT_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODINT_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPINT)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPINT)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCODINT_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"ELENCO CLIENTI/FORNITORI",'',this.parent.oContained
  endproc
  proc oCOCODINT_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPINT
     i_obj.w_ANCODICE=this.parent.oContained.w_COCODINT
     i_obj.ecpSave()
  endproc

  add object oCOCODAGE_1_24 as StdField with uid="OUOLKYMKTT",rtseq=19,rtrep=.f.,;
    cFormVar = "w_COCODAGE", cQueryName = "COCODAGE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non valido o senza rif.fornitore",;
    HelpContextID = 11137643,;
   bGlobalFont=.t.,;
    Height=21, Width=137, Left=380, Top=75, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_COCODAGE"

  func oCOCODAGE_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COTIPINT = 'A' AND .w_FL_TIPOC='O')
    endwith
   endif
  endfunc

  func oCOCODAGE_1_24.mHide()
    with this.Parent.oContained
      return (.w_COTIPINT <> 'A' OR .w_FL_TIPOC<>'O')
    endwith
  endfunc

  func oCOCODAGE_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODAGE_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODAGE_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCOCODAGE_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'',"AGENTI",'GSGP_AGE.AGENTI_VZM',this.parent.oContained
  endproc

  add object oCOCODGRU_1_26 as StdField with uid="WCFJXXVIOX",rtseq=21,rtrep=.f.,;
    cFormVar = "w_COCODGRU", cQueryName = "COCODGRU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 111800955,;
   bGlobalFont=.t.,;
    Height=21, Width=137, Left=380, Top=75, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="GRUPPACQ", oKey_1_1="GACODICE", oKey_1_2="this.w_COCODGRU"

  func oCOCODGRU_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COTIPINT = 'G' AND .w_FL_TIPOC='O')
    endwith
   endif
  endfunc

  func oCOCODGRU_1_26.mHide()
    with this.Parent.oContained
      return (.w_COTIPINT <> 'G' OR .w_FL_TIPOC<>'O')
    endwith
  endfunc

  func oCOCODGRU_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODGRU_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODGRU_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUPPACQ','*','GACODICE',cp_AbsName(this.parent,'oCOCODGRU_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'',"GRUPPI DI ACQUISTO",'',this.parent.oContained
  endproc


  add object oBtn_1_27 as StdButton with uid="ZMIZZNTUDJ",left=676, top=112, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare le modifiche";
    , HelpContextID = 27260906;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      with this.Parent.oContained
        GSGP_BDC(this.Parent.oContained,"COPYC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_27.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_CONEWCOD))
      endwith
    endif
  endfunc


  add object oBtn_1_28 as StdButton with uid="JWIBHYOYEX",left=726, top=112, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 75098554;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="ZVIWRYBPYI",Visible=.t., Left=7, Top=15,;
    Alignment=1, Width=216, Height=18,;
    Caption="Codice contratto da duplicare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="YFZWOECBVQ",Visible=.t., Left=7, Top=45,;
    Alignment=1, Width=216, Height=18,;
    Caption="Codice nuovo contratto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="LTHHELREKC",Visible=.t., Left=7, Top=75,;
    Alignment=1, Width=216, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (.w_FL_TIPOC<>'O')
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="EEKCQNHTIM",Visible=.t., Left=7, Top=75,;
    Alignment=1, Width=216, Height=18,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (.w_FL_TIPOC='O' OR .w_FL_CICLO='A')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="QEPYBAXTQF",Visible=.t., Left=7, Top=75,;
    Alignment=1, Width=216, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_FL_TIPOC='O' OR .w_FL_CICLO='V')
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="YOJIMWQUAT",Visible=.t., Left=4, Top=168,;
    Alignment=0, Width=464, Height=18,;
    Caption="Variabili utilizzate nel batch per aderire al comportamento della duplicazione massiva"  ;
  , bGlobalFont=.t.

  func oStr_1_32.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsgp_kdc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
