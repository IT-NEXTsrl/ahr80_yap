* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsgp_bgl                                                        *
*              Manutenzione listini                                            *
*                                                                              *
*      Author: Zucchetti Spa - AT                                              *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-01-10                                                      *
* Last revis.: 2014-03-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsgp_bgl",oParentObject,m.pOPER)
return(i_retval)

define class tgsgp_bgl as StdBatch
  * --- Local variables
  pOPER = space(4)
  w_AGTIPCON = space(1)
  w_NELAB = 0
  w_PUNPAD = .NULL.
  w_PUNLIS = .NULL.
  w_APART = space(20)
  w_CONTANUM = 0
  w_ZOOM = .NULL.
  w_CACODMOD = space(10)
  w_CACODGRU = space(10)
  w_CACODFAM = space(10)
  w_CAVALATT = space(10)
  w_CAFLPRIO = space(1)
  w_CADESCRI = space(35)
  w_PUN2PAD = .NULL.
  w_FPROV = space(1)
  w_PPROV = 0
  w_VPROV = 0
  w_DL__GUID = space(10)
  w_DLPRCALC = 0
  w_NEWCODCLF = space(15)
  w_NEWDATINI = ctod("  /  /  ")
  w_NEWDATFIN = ctod("  /  /  ")
  w_NEWQTAINI = 0
  w_NOMANUAL = space(1)
  w_NEWQTAFIN = 0
  w_NEWPREZZO = 0
  w_NEWMULTIP = 0
  w_NEWPREMUL = 0
  w_NEWSCONT1 = 0
  w_NEWSCONT2 = 0
  w_NEWSCONT3 = 0
  w_NEWSCONT4 = 0
  w_NEWSCONTV = 0
  w_NEWFLPROV = space(1)
  w_NEWPERPRO = 0
  w_NEWVALPRO = 0
  w_NEWTIPCON = space(1)
  w_CODART = space(20)
  w_ARTGUID = space(10)
  w_OKINS = .f.
  w_ZOOMN = space(10)
  w_UNIMIS = space(20)
  w_ERRMESS = .f.
  w_FLFRAZ = space(1)
  w_OK2MDA = .f.
  w_LAGRICINI = 0
  w_LAGRICFIN = 0
  w_LAGRICTIP = 0
  w_CODVAR = space(20)
  w_LNOVALU = space(1)
  w_OKSCONTV = .f.
  w_CONTACICLO = 0
  w_OKELAB = .f.
  w_CODLIS = space(5)
  w_DESLIS = space(30)
  w_LSVALLIS = space(3)
  w_LSDATCAM = ctod("  /  /  ")
  w_LSVALCAM = 0
  w_CAOVAL2 = 0
  w_LSPERTOL = 0
  w_LSTIPLIS = space(1)
  w_LSFLGCIC = space(1)
  w_LSDATINI = space(1)
  w_LSDATFIN = space(1)
  w_PRIORI = 0
  w_PROMO = space(1)
  w_PERRIC = 0
  w_DLPRIORI = 0
  w_RECO = 0
  w_CONTACLI = 0
  w_CONTAART = 0
  w_OLD_GUID = space(10)
  w_NFLTATTR = 0
  w_NEWAGGIOAPP = 0
  w_CALC = 0
  w_CONTA = 0
  w_TOTV = 0
  * --- WorkFile variables
  CARLATTR_idx=0
  CATECOMM_idx=0
  ZONE_idx=0
  CAT_SCMA_idx=0
  FAM_ARTI_idx=0
  GRUMERC_idx=0
  GRUPRO_idx=0
  CLA_RICA_idx=0
  MARCHI_idx=0
  ART_ICOL_idx=0
  ASS_ATTR_idx=0
  UNIMIS_idx=0
  LISTINI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione manutenzione listini - GSVE_KML
    * --- Variabili relative alle altre selezioni
    * --- Variabili per duplicazione listini
    * --- Controlli compilazione dati obbligatori
    this.w_PUNLIS = this.oParentObject
    * --- Gestione zoom dei bottoni (dettaglio articoli e clienti/fornitori)
    do case
      case this.pOPER="AZOM1"
        this.w_ZOOM = this.w_PUNLIS.w_ZoomArt
        this.w_ZOOMN = this.w_PUNLIS.w_ZoomCon
        this.w_ZOOMN.Visible = .f.
        Select __TMP__
        Go Top
        Scan
        Scatter Memvar
        Select (this.w_ZOOM.cCursor)
        Append blank
        Gather memvar
        Endscan
        SELECT __TMP__
        USE
        SELECT ( this.w_ZOOM.cCursor )
        GO TOP
        * --- Refresh della griglia
        this.w_zoom.grd.refresh
        i_retcode = 'stop'
        return
      case this.pOPER="CZOM1"
        this.w_ZOOM = this.w_PUNLIS.w_ZoomCon
        this.w_ZOOMN = this.w_PUNLIS.w_ZoomArt
        this.w_ZOOMN.Visible = .f.
        Select __TMP__
        Go Top
        Scan
        Scatter Memvar
        Select (this.w_ZOOM.cCursor)
        Append blank
        Gather memvar
        Endscan
        SELECT __TMP__
        USE
        SELECT ( this.w_ZOOM.cCursor )
        GO TOP
        * --- Refresh della griglia
        this.w_zoom.grd.refresh
        i_retcode = 'stop'
        return
    endcase
    if !this.pOPER $ "CANC-VIAR-STAR-VICL-STCL"
      do case
        case Empty(this.oParentObject.w_DLCODICE) And Empty(this.oParentObject.w_CODVAL) And this.oParentObject.w_LSNOVALU <> "S"
          AH_ERRORMSG("Specificare il codice della valuta o attivare il flag valido per ogni valuta",48)
          i_retcode = 'stop'
          return
        case Empty(this.oParentObject.w_DLDATINI) Or Empty(this.oParentObject.w_DLDATFIN)
          AH_ERRORMSG("Specificare le date di validit� del listino",48)
          i_retcode = 'stop'
          return
        case !this.oParentObject.w_DLFLCICL $ ("AVET")
          AH_ERRORMSG("Specificare il ciclo del listino",48)
          i_retcode = 'stop'
          return
        case !this.oParentObject.w_TIPLIS $ ("SPET")
          AH_ERRORMSG("Specificare il tipo del listino",48)
          i_retcode = 'stop'
          return
        case this.pOPER="AGG" And this.oParentObject.w_DLFLCICL ="V" And this.oParentObject.w_FLUCOA="S"And this.oParentObject.w_APCL <>"S" 
          this.w_CONTA = 1
          this.w_TOTV = 100
          this.w_CALC = 0
          do while this.w_CONTA <= 4
            y = ALLTRIM(STR( this.w_CONTA ))
            this.w_CALC = this.w_CALC + ( this.oparentobject.w_AGSCONT&y * this.w_TOTV/100 )
            this.w_TOTV = 100+ this.w_CALC
            this.w_CONTA = this.w_CONTA+1
          enddo
          * --- Listino U.C.A, devo avere somma algebrica degli sconti positiva in sequenza
          if this.w_CALC < 0
            AH_ERRORMSG("Listino di tipo U.C.A e somma degli sconti in sequenza non positiva",48)
            i_retcode = 'stop'
            return
          endif
        case this.pOPER="AGG" And this.oParentObject.w_DUPLICA = "S" And this.oParentObject.w_LISTIDET = "A" And Empty(this.oParentObject.w_ALTROLIS)
          AH_ERRORMSG("Duplicazione dettagli su altro listino: specificare il codice del listino",48)
          i_retcode = 'stop'
          return
      endcase
    endif
    this.w_ZOOM = this.w_PUNLIS.w_ZoomLis
    * --- Attributi di selezione
    this.w_PUNPAD = this.w_PUNLIS.GSVE1MDA
    * --- Attributi di aggiornamento
    this.w_PUN2PAD = this.w_PUNLIS.GSVE2MDA
    do case
      case right(this.pOPER,2)="AR"
        * --- Attributi di selezione
        select t_CACODMOD,t_CACODFAM,t_CACODGRU,t_CAVALATT from (this.w_PUNPAD.cTrsName); 
 where t_CACODMOD = "ART_ICOL" And Not Empty (t_CAVALATT) into Cursor Elab 
        this.w_NELAB = RECCOUNT("Elab")
        if this.w_NELAB >0
          * --- Bottoni visualizza e stampa articoli
          *     Elenco articoli con attributi associati
          vq_exec("..\GPFA\EXE\query\GSGP_BGL",this,"Artico")
          * --- Conto i record (attributi) per ogni articolo
          *     (serivir� pi� avanti per scartare gli articoli non validi se attivo in maschera 
          *     il flag Selezione Puntuale)
          if this.oParentObject.w_TIPSELE = "T"
            Select ARCODART, count(*) as NUM from Artico group by ARCODART into cursor CONTA
          endif
          * --- La join consente gi� di scartare tutti gli articoli che non hanno 
          *     neppure un attributo in comune.
          Select Artico.*,"S" as ACCETTA from Artico inner join Elab on; 
 ASMODFAM=t_CACODMOD and ASCODATT=t_CACODGRU and ASCODFAM=t_CACODFAM and ASVALATT=t_CAVALATT; 
 Into Cursor Artico 
          =wrcursor("Artico")
          if this.oParentObject.w_TIPSELE $ "AT"
            if this.oParentObject.w_TIPSELE ="T"
              * --- Dal risultato della join conto per ogni articolo i record (attributri) rimasti.
              *     Se NON coincidono col totale di attributi di selezione l'articolo NON � valido,
              *     � presente cio� almeno un attributo di selezione non soddisfatto.
              *     Se coincidono occorre controllare il flag PUNTUALE nella maschera:
              *     se ATTIVO confronto il totale dei record (attributi) di CONTAJ (risulatto della join)
              *     con il totale dei record di CONTA (totale di attributi per articolo), se coincide l'articolo viene ACCETTATO
              *     se NON coincide l'articolo viene SCARTATO.
              *     Se il flag puntuale � disattivo l'articolo � ACCETTATO
              Select ARCODART, count(*) as NUM from Artico group by ARCODART into cursor CONTAJ
              Select CONTAJ 
 Go top
              Scan
              this.w_APART = CONTAJ.ARCODART
              if this.w_NELAB > CONTAJ.NUM
                * --- Se sono presenti piu attributi di selezione rspetto a quelli associati
                *     all'articolo scarto l'articolo in questione
                Update Artico set ACCETTA="N" where ARCODART= this.w_APART
                Select Artico 
 Go Top
              else
                if this.oParentObject.w_PUNTUALE = "S"
                  Select Conta 
 Locate for ARCODART=this.w_APART
                  this.w_CONTANUM = CONTA.NUM 
                  Select CONTAJ
                  if this.w_CONTANUM > CONTAJ.NUM
                    Update Artico set ACCETTA="N" where ARCODART= this.w_APART
                    Select Artico 
 Go Top
                  endif
                endif
              endif
              Select CONTAJ
              EndScan
              Select Artico
            endif
          else
            * --- Almeno uno per gruppo o per famiglia
            if this.oParentObject.w_TIPSELE="G"
              * --- Count per GRUPPO
              Select distinct ARCODART, GUID, ASMODFAM, ASCODATT From "Artico" into cursor "_Gruppo_"
              Select Count(*) as CONTA, ARCODART,GUID from "_Gruppo_" Group By GUID Into Cursor "_Gruppo_"
              select t_CACODMOD,t_CACODGRU from (this.w_PUNPAD.cTrsName); 
 where t_CACODMOD = "ART_ICOL" And Not Empty (t_CAVALATT) Group by t_CACODMOD,t_CACODGRU into Cursor "Flt_Attr"
            else
              * --- Count per GRUPPO e FAMIGLIA
              Select distinct ARCODART, GUID, ASMODFAM, ASCODATT, ASCODFAM From "Artico" into cursor "_Gruppo_"
              Select Count(*) as CONTA, ARCODART,GUID from "_Gruppo_" Group By GUID Into Cursor "_Gruppo_"
              select t_CACODMOD,t_CACODGRU, t_CACODFAM from (this.w_PUNPAD.cTrsName); 
 where t_CACODMOD = "ART_ICOL" And Not Empty (t_CAVALATT) Group by t_CACODMOD,t_CACODGRU,t_CACODFAM into Cursor "Flt_Attr"
            endif
            this.w_NFLTATTR = RECCOUNT("Flt_Attr")
            if this.w_NFLTATTR > 0
              * --- Sono eliminati tutti i record che non soddisfano
              Delete from Artico where ARCODART+GUID not in; 
 (Select ARCODART+ GUID from _Gruppo_ where CONTA= this.w_NFLTATTR)
            endif
          endif
          * --- Trasferisco gli articoli nel cursore di stampa __tmp__
          Select * from Artico where ACCETTA="S" into cursor __tmp__ order by ARCODART group by ARCODART
          if left(this.pOPER,2)="VI"
            do GSVE1KML With "A"
          else
            cp_chprn("QUERY\GSVE_KML.FRX","",this)
          endif
        else
          Select Elab 
 Use
          AH_ERRORMSG("Nessun attributo con modello ' ART_ICOL' selezionato",48)
          i_retcode = 'stop'
          return
        endif
      case right(this.pOPER,2)="CL"
        * --- Attributi di selezione
        select t_CACODMOD,t_CACODFAM,t_CACODGRU,t_CAVALATT from (this.w_PUNPAD.cTrsName); 
 where t_CACODMOD = "CONTI" And Not Empty (t_CAVALATT) into Cursor Elab 
        this.w_NELAB = RECCOUNT("Elab")
        if this.w_NELAB >0
          * --- Bottoni visualizza e stampa CliFor
          *     Elenco CliFor con attributi associati
          vq_exec("query\GSVE1BGL",this,"CliFor")
          if this.oParentObject.w_TIPSELE="T"
            * --- Conto i record (attributi) per ogni CliFor
            *     (serivir� pi� avanti per scartare i CliFor non validi se attivo in maschera 
            *     il flag Selezione Puntuale)
            Select ANCODICE, count(*) as NUM from CliFor group by ANCODICE into cursor CONTA
          endif
          * --- La join consente gi� di scartare tutti i CliFor che non hanno 
          *     neppure un attributo in comune.
          Select CliFor.*,"S" as ACCETTA from CliFor inner join Elab on; 
 ASMODFAM=t_CACODMOD and ASCODATT=t_CACODGRU and ASCODFAM=t_CACODFAM and ASVALATT=t_CAVALATT; 
 Into Cursor CliFor 
          =wrcursor("CliFor")
          * --- 'AT' = Ameno uno /tutti
          if this.oParentObject.w_TIPSELE $ "AT"
            if this.oParentObject.w_TIPSELE="T"
              * --- Dal risultato della join conto per ogni CliFor i record (attributri) rimasti.
              *     Se NON coincidono col totale di attributi di selezione CliFor NON � valido,
              *     � presente cio� almeno un attributo di selezione non soddisfatto.
              *     Se coincidono occorre controllare il flag PUNTUALE nella maschera:
              *     se ATTIVO confronto il totale dei record (attributi) di CONTAJ (risulatto della join)
              *     con il totale dei record di CONTA (totale di attributi per CliFor), se coincide CliFor viene ACCETTATO
              *     se NON coincide CliFor viene SCARTATO.
              *     Se il flag puntuale � disattivo CliFor � ACCETTATO
              Select ANCODICE, count(*) as NUM from CliFor group by ANCODICE into cursor CONTAJ
              Select CONTAJ 
 Go top
              Scan
              this.w_APART = CONTAJ.ANCODICE
              if this.w_NELAB > CONTAJ.NUM
                * --- Se sono presenti piu attributi di selezione rspetto a quelli associati
                *     al CliFor scarto il CliFor in questione
                Update CliFor set ACCETTA="N" where ANCODICE= this.w_APART
                Select CliFor 
 Go Top
              else
                if this.oParentObject.w_PUNTUALE = "S"
                  Select Conta 
 Locate for ANCODICE=this.w_APART
                  this.w_CONTANUM = CONTA.NUM 
                  Select CONTAJ
                  if this.w_CONTANUM > CONTAJ.NUM
                    Update CliFor set ACCETTA="N" where ANCODICE= this.w_APART
                    Select CliFor 
 Go Top
                  endif
                endif
              endif
              Select CONTAJ
              EndScan
              Select CliFor
            endif
          else
            * --- Almeno uno per gruppo o per famiglia
            if this.oParentObject.w_TIPSELE="G"
              * --- Count per GRUPPO
              Select distinct ANTIPCON, ANCODICE, GUID, ASMODFAM, ASCODATT From "Clifor" into cursor "_Gruppo_"
              Select Count(*) as CONTA, ANTIPCON, ANCODICE, GUID from "_Gruppo_" Group By GUID Into Cursor "_Gruppo_"
              select t_CACODMOD,t_CACODGRU from (this.w_PUNPAD.cTrsName); 
 where t_CACODMOD = "CONTI" And Not Empty (t_CAVALATT) Group by t_CACODMOD,t_CACODGRU into Cursor "Flt_Attr"
            else
              * --- Count per GRUPPO e FAMIGLIA
              Select distinct ANTIPCON, ANCODICE, GUID, ASMODFAM, ASCODATT, ASCODFAM From "Clifor" into cursor "_Gruppo_"
              Select Count(*) as CONTA, ANTIPCON, ANCODICE, GUID from "_Gruppo_" Group By GUID Into Cursor "_Gruppo_"
              select t_CACODMOD,t_CACODGRU, t_CACODFAM from (this.w_PUNPAD.cTrsName); 
 where t_CACODMOD = "CONTI" And Not Empty (t_CAVALATT) Group by t_CACODMOD,t_CACODGRU,t_CACODFAM into Cursor "Flt_Attr"
            endif
            this.w_NFLTATTR = RECCOUNT("Flt_Attr")
            if this.w_NFLTATTR > 0
              * --- Sono eliminati tutti i record che non soddisfano
              Delete from Clifor where ANTIPCON+ANCODICE+GUID not in; 
 (Select ANTIPCON+ANCODICE+GUID from _Gruppo_ where CONTA= this.w_NFLTATTR)
            endif
          endif
          * --- Trasferisco i CliFor nel cursore di stampa __tmp__
          Select * from CliFor where ACCETTA="S" into cursor __tmp__ order by ANCODICE group by ANCODICE
          if left(this.pOPER,2)="VI"
            do GSVE1KML With "C"
          else
            cp_chprn("QUERY\GSVE1KML.FRX","",this)
          endif
        else
          Select Elab 
 Use
          AH_ERRORMSG("Nessun attributo con modello 'CONTI' selezionato",48)
          i_retcode = 'stop'
          return
        endif
    endcase
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura cursori
    USE IN SELECT("LISTINI")
    USE IN SELECT("LISTINDI")
    USE IN SELECT("Flt_Attr")
    USE IN SELECT("_Gruppo_")
    USE IN SELECT("DETTINDI")
    USE IN SELECT("ARTICO")
    USE IN SELECT("CLIFOR")
    USE IN SELECT("ARTICO2")
    USE IN SELECT("CLIFOR2")
    USE IN SELECT("CONTAJ")
    USE IN SELECT("CONTA")
    USE IN SELECT("Elab")
    USE IN SELECT("AppMM")
    USE IN SELECT("Attrindi")
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='CARLATTR'
    this.cWorkTables[2]='CATECOMM'
    this.cWorkTables[3]='ZONE'
    this.cWorkTables[4]='CAT_SCMA'
    this.cWorkTables[5]='FAM_ARTI'
    this.cWorkTables[6]='GRUMERC'
    this.cWorkTables[7]='GRUPRO'
    this.cWorkTables[8]='CLA_RICA'
    this.cWorkTables[9]='MARCHI'
    this.cWorkTables[10]='ART_ICOL'
    this.cWorkTables[11]='ASS_ATTR'
    this.cWorkTables[12]='UNIMIS'
    this.cWorkTables[13]='LISTINI'
    return(this.OpenAllTables(13))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
