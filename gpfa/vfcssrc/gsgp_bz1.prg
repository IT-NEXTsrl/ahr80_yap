* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsgp_bz1                                                        *
*              Lancia gruppo causali di magazzino (vendite)                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-11-07                                                      *
* Last revis.: 2013-03-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsgp_bz1",oParentObject)
return(i_retval)

define class tgsgp_bz1 as StdBatch
  * --- Local variables
  w_DXBTN = .f.
  w_COGRUCAU = space(5)
  w_PROG = space(8)
  w_CICLO = space(1)
  * --- WorkFile variables
  CON_TRAM_idx=0
  CCOMCMAG_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if Type("g_oMenu.oKey")<>"U"
      this.w_DXBTN = .T.
      this.w_COGRUCAU = Nvl(g_oMenu.oKey(1,3),"")
    else
      cCurs = i_curform.z1.cCursor
      this.w_COGRUCAU = &cCurs..CSCODICE
    endif
    * --- Puntatore al nome del Cursore da cui e' stato lanciato
    * --- Read from CCOMCMAG
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CCOMCMAG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CCOMCMAG_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CSFLGCIC"+;
        " from "+i_cTable+" CCOMCMAG where ";
            +"CSCODICE = "+cp_ToStrODBC(this.w_COGRUCAU);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CSFLGCIC;
        from (i_cTable) where;
            CSCODICE = this.w_COGRUCAU;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CICLO = NVL(cp_ToDate(_read_.CSFLGCIC),cp_NullValue(_read_.CSFLGCIC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_CICLO="V"
      this.w_PROG = "GSGP_MCM('V')"
    else
      this.w_PROG = "GSGP_MCM('A')"
    endif
    OpenGest(IIF(this.w_DXBTN,g_oMenu.cBatchType,"S"),this.w_PROG,"CSCODICE",this.w_COGRUCAU)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CON_TRAM'
    this.cWorkTables[2]='CCOMCMAG'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
