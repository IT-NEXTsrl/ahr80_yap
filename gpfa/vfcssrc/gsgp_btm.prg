* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsgp_btm                                                        *
*              Generazione documenti                                           *
*                                                                              *
*      Author: Zucchetti - AT                                                  *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-06                                                      *
* Last revis.: 2017-11-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPDOC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsgp_btm",oParentObject,m.pTIPDOC)
return(i_retval)

define class tgsgp_btm as StdBatch
  * --- Local variables
  pTIPDOC = space(4)
  w_APCONTR = space(15)
  w_MVTIPCON = space(1)
  w_CPROWNUM = 0
  w_RIGAKEY = 0
  w_DLCODART = space(20)
  w_CODKEY = space(41)
  w_CONOTGEN = space(0)
  w_CONOTDES = space(40)
  w_COKEYSAL = space(40)
  w_PROGR = 0
  w_MVCODCON = space(15)
  w_COKEYART = space(41)
  w_VALOREART = space(0)
  w_COUNIMIS = space(3)
  w_CLIFOR = space(15)
  w_GRUART = space(15)
  w_FLGEXD = space(1)
  w_VALCON = 0
  w_QTACON = 0
  w_FLGINC = space(1)
  w_OKSCAGL = .f.
  w_PCQTACON = 0
  w_MVALFDOC = space(2)
  w_PCVALCON = 0
  w_PCSCAPRM = 0
  w_CONTA = 0
  w_PCQTAPRE = 0
  w_PCPUNPRE = 0
  w_COVALFAT = 0
  w_TIPINT = space(1)
  w_COSCOCOM = 0
  w_DOCSCOM = 0
  w_NOAGG = .f.
  w_SC1 = 0
  w_SC2 = 0
  w_SC3 = 0
  w_SC4 = 0
  w_SC5 = 0
  w_SC6 = 0
  w_COPRMDOC = space(1)
  w_TOTPUN = 0
  w_COPREZZO = 0
  w_TOTVALPRE = 0
  w_TOTPREMIO = 0
  w_COTOTPRE = 0
  w_COVALPRE = 0
  w_APVALPRE = 0
  w_COPRMLIQ = space(1)
  w_CODATFIN = ctod("  /  /  ")
  w_PCVALPRE = 0
  w_MVTIPDOC = space(5)
  w_TIPINT = space(1)
  w_CODAGE = space(5)
  w_FLGVAL = space(1)
  w_FLGLIS = space(1)
  w_NUMLIS = 0
  w_NUMART = 0
  w_MVFLSCOR = space(1)
  w_MVCODBAN = space(5)
  w_MSERIAL = space(10)
  w_MVCODAGE = space(5)
  w_ANCODAG1 = space(5)
  w_ANCODAG2 = space(5)
  w_MVCODAG2 = space(5)
  w_TOTVALROW = 0
  w_MVPREZZO = 0
  w_COTIPSTO = space(1)
  w_ROWRIF = space(20)
  w_QTACONROW = 0
  w_VALCONROW = 0
  w_MVRIFEST = space(26)
  w_MVCLADOC = space(2)
  w_NumSERIAL = 0
  w_NUMRIG = 0
  w_MVFLVEAC = space(1)
  w_DATREG = ctod("  /  /  ")
  w_PARTECIPA = space(1)
  w_OBJ = .NULL.
  w_NDOCGENERATI = 0
  w_RESOCONTO = space(10)
  w_INSERISCIDOCUMENTOFISCALE = .f.
  w_GATIPCON = space(1)
  w_ANTIPCON = space(1)
  w_INTESTATARIO_OK = .f.
  w_ANCONRIF = space(15)
  w_TROVATOINTESTATARIOCOLLEGATO = .f.
  w_MVCODICE = space(20)
  w_MVTIPRIG = space(1)
  * --- WorkFile variables
  GRUPPACQ_idx=0
  AGENTI_idx=0
  CONTI_idx=0
  PREMICON_idx=0
  TIP_DOCU_idx=0
  CCOMMAST_idx=0
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione del documento fiscale
    this.w_NumSERIAL = VAL(this.oParentObject.w_SERGDOC)
    this.w_APCONTR = REPL("#",15)
    this.w_OBJ = CREATEOBJECT("DocumWriter",this,"DOC_MAST")
    this.w_INSERISCIDOCUMENTOFISCALE = .F.
    this.w_NDOCGENERATI = 0
    if this.pTIPDOC="FISC"
      * --- Select from GSGP_DOC
      do vq_exec with 'GSGP_DOC',this,'_Curs_GSGP_DOC','',.f.,.t.
      if used('_Curs_GSGP_DOC')
        select _Curs_GSGP_DOC
        locate for 1=1
        do while not(eof())
        * --- Genero Documenti : sono riempite le tabelle demporanee del generatore
        *     con i dati appena calcolati
        if this.w_APCONTR <> _Curs_GSGP_DOC.COCODICE
          this.w_APCONTR = _Curs_GSGP_DOC.COCODICE
          this.w_COPRMLIQ = _Curs_GSGP_DOC.COPRMLIQ
          this.w_COPRMDOC = _Curs_GSGP_DOC.COPRMDOC
          this.w_TIPINT = _Curs_GSGP_DOC.COTIPINT
          this.w_CODAGE = _Curs_GSGP_DOC.COCODAGE
          this.w_COTIPSTO = _Curs_GSGP_DOC.COTIPSTO
          this.w_TOTPUN = _Curs_GSGP_DOC.COTOTPUN
          this.w_NUMRIG = _Curs_GSGP_DOC.NUMRIG
          this.w_DOCSCOM = _Curs_GSGP_DOC.COSCOCOM
          this.w_TOTPREMIO = _Curs_GSGP_DOC.TOTVALPRE
          * --- Totale FATTURATO CONTRATTO
          this.w_VALCONROW = _Curs_GSGP_DOC.PESOVALROW
          * --- Totale venduto per riga contratto
          this.w_QTACONROW = _Curs_GSGP_DOC.PESOQTAROW
          this.w_CONOTGEN = _Curs_GSGP_DOC.CONOTGEN
          this.w_CONOTDES = LEFT(ALLTRIM(_Curs_GSGP_DOC.CONOTGEN),40)
          * --- Devo leggere la causale giusta (Contropartite)
          if Empty(this.oParentObject.w_GPCONTRA)
            do case
              case this.w_COPRMDOC = "F"
                this.w_MVTIPDOC = this.oParentObject.w_CAUDF1
              case this.w_COPRMDOC = "N"
                this.w_MVTIPDOC = this.oParentObject.w_CAUDF2
              case this.w_COPRMDOC = "P"
                this.w_MVTIPDOC = this.oParentObject.w_CAUDF3
              case this.w_COPRMDOC = "C"
                this.w_MVTIPDOC = this.oParentObject.w_CAUDF4
              case this.w_COPRMDOC = "A"
                this.w_MVTIPDOC = this.oParentObject.w_CAUDAG
            endcase
            * --- Read from TIP_DOCU
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TDFLVEAC,TDFLINTE,TDALFDOC"+;
                " from "+i_cTable+" TIP_DOCU where ";
                    +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TDFLVEAC,TDFLINTE,TDALFDOC;
                from (i_cTable) where;
                    TDTIPDOC = this.w_MVTIPDOC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MVFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
              this.w_MVTIPCON = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
              this.w_MVALFDOC = NVL(cp_ToDate(_read_.TDALFDOC),cp_NullValue(_read_.TDALFDOC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            this.w_MVTIPDOC = this.oParentObject.w_GPTIPDOC
            this.w_MVCLADOC = this.oParentObject.w_CLADOC
            this.w_MVALFDOC = this.oParentObject.w_GPALFDOC
            * --- Read from TIP_DOCU
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "TDFLVEAC,TDFLINTE"+;
                " from "+i_cTable+" TIP_DOCU where ";
                    +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                TDFLVEAC,TDFLINTE;
                from (i_cTable) where;
                    TDTIPDOC = this.w_MVTIPDOC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MVFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
              this.w_MVTIPCON = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          * --- Intestatrio del Documento
          this.w_MVCODCON = SPACE(15)
          do case
            case this.w_TIPINT = "G"
              if _Curs_GSGP_DOC.COPRMRIF = "U"
                * --- Lettura azienda impostata come centrale del gruppo
                * --- Read from GRUPPACQ
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.GRUPPACQ_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.GRUPPACQ_idx,2],.t.,this.GRUPPACQ_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "GACODCLI,GATIPCON"+;
                    " from "+i_cTable+" GRUPPACQ where ";
                        +"GACODICE = "+cp_ToStrODBC(_Curs_GSGP_DOC.COCODGRU);
                        +" and GAFLPRIF = "+cp_ToStrODBC("S");
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    GACODCLI,GATIPCON;
                    from (i_cTable) where;
                        GACODICE = _Curs_GSGP_DOC.COCODGRU;
                        and GAFLPRIF = "S";
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_MVCODCON = NVL(cp_ToDate(_read_.GACODCLI),cp_NullValue(_read_.GACODCLI))
                  this.w_GATIPCON = NVL(cp_ToDate(_read_.GATIPCON),cp_NullValue(_read_.GATIPCON))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              else
                * --- Da implementare
              endif
            case this.w_TIPINT $ "CF"
              * --- Cliente/Fornitore
              this.w_MVCODCON = _Curs_GSGP_DOC.COCODINT
            case this.w_TIPINT = "A"
              * --- Lettura fornitore in anagrafica agente
              * --- Read from AGENTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.AGENTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "AGCODFOR"+;
                  " from "+i_cTable+" AGENTI where ";
                      +"AGCODAGE = "+cp_ToStrODBC(this.w_CODAGE);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  AGCODFOR;
                  from (i_cTable) where;
                      AGCODAGE = this.w_CODAGE;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_MVCODCON = NVL(cp_ToDate(_read_.AGCODFOR),cp_NullValue(_read_.AGCODFOR))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if Empty(this.w_MVCODCON)
                * --- Non ho intestatrio passo a nuova fattura fiscale
                Skip
              endif
              this.w_MVCODAGE = this.w_CODAGE
          endcase
          do case
            case this.w_TIPINT="C"
              this.w_ANTIPCON = "C"
            case this.w_TIPINT="F"
              this.w_ANTIPCON = "F"
            case this.w_TIPINT="G"
              this.w_ANTIPCON = this.w_GATIPCON
            case this.w_TIPINT="A"
              this.w_ANTIPCON = "F"
          endcase
          if this.w_MVFLVEAC = "A" AND this.w_ANTIPCON = "C" OR this.w_MVFLVEAC = "V" AND this.w_ANTIPCON = "F"
            this.w_INTESTATARIO_OK = .F.
            * --- Se w_MVTIPDOC � del ciclo acquisti e l'intestatario � un cliente 
            *     oppure w_MVTIPDOC � del ciclo vendite e l'intestatario � un fornitore,
            *     bisogna utilizzare l'intestatatrio collegato
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANCONRIF"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
                    +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANCONRIF;
                from (i_cTable) where;
                    ANTIPCON = this.w_ANTIPCON;
                    and ANCODICE = this.w_MVCODCON;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ANCONRIF = NVL(cp_ToDate(_read_.ANCONRIF),cp_NullValue(_read_.ANCONRIF))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_ANCONRIF = NVL( this.w_ANCONRIF, SPACE( 15 ) )
            if NOT EMPTY( this.w_ANCONRIF )
              this.w_MVCODCON = this.w_ANCONRIF
              if this.w_ANTIPCON = "C"
                * --- Usa il fornitore collegato invece del cliente
                this.w_MVTIPCON = "F"
              else
                * --- Usa il cliente collegato invece del fornitore
                this.w_MVTIPCON = "C"
              endif
              this.w_TROVATOINTESTATARIOCOLLEGATO = .T.
            else
              if this.w_ANTIPCON = "C"
                * --- Non ha trovato il collegamento al fornitore nell'anagrafica del cliente
                this.oParentObject.w_oERRORLOG.AddMsgLog("Errore nella generazione del documento fiscale:%0%1", "Non � stato trovato il collegamento al fornitore nell'anagrafica del cliente")     
              else
                * --- Non ha trovato il collegamento al cliente nell'anagrafica del fornitore
                this.oParentObject.w_oERRORLOG.AddMsgLog("Errore nella generazione del documento fiscale:%0%1", "Non � stato trovato il collegamento al cliente nell'anagrafica del fornitore")     
              endif
              this.w_TROVATOINTESTATARIOCOLLEGATO = .F.
            endif
          else
            * --- Non deve cercare l'intestatario collegato
            this.w_INTESTATARIO_OK = .T.
          endif
          * --- Informazione associate all'intestatrio
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANSCORPO,ANCODBAN,ANCODAG1"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANSCORPO,ANCODBAN,ANCODAG1;
              from (i_cTable) where;
                  ANTIPCON = this.w_MVTIPCON;
                  and ANCODICE = this.w_MVCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MVFLSCOR = NVL(cp_ToDate(_read_.ANSCORPO),cp_NullValue(_read_.ANSCORPO))
            this.w_MVCODBAN = NVL(cp_ToDate(_read_.ANCODBAN),cp_NullValue(_read_.ANCODBAN))
            this.w_ANCODAG1 = NVL(cp_ToDate(_read_.ANCODAG1),cp_NullValue(_read_.ANCODAG1))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_TIPINT <> "A"
            this.w_MVCODAGE = this.w_ANCODAG1
            this.w_MVCODAG2 = this.w_ANCODAG2
          endif
          * --- Scrittura Master 
          this.w_NumSERIAL = this.w_NumSERIAL + 1
          this.w_MSERIAL = RIGHT("0000000000"+ALLTRIM(STR(this.w_NumSERIAL)) ,10)
          * --- la  procedura scrive il codice contratto in MVRIFEST.
          this.w_MVRIFEST = "Contratto: "+ alltrim(this.w_APCONTR)
          * --- Inserimento testata
          this.w_NDOCGENERATI = this.w_NDOCGENERATI + 1
          if this.w_NDOCGENERATI > 1
            * --- Chiude il documento precedente
            if this.w_TROVATOINTESTATARIOCOLLEGATO
              this.w_RESOCONTO = ""
              if this.w_INSERISCIDOCUMENTOFISCALE
                this.w_RESOCONTO = this.w_OBJ.CreateDocument()
                if EMPTY( this.w_RESOCONTO )
                  GSGP_BCQ (this, this.oParentObject.w_GPSERIAL, this.w_OBJ.w_MVSERIAL , this.w_OBJ.w_MVNUMDOC , this.w_OBJ.w_MVALFDOC , this.w_OBJ.w_MVDATDOC , this.w_OBJ.w_MVTIPCON , this.w_OBJ.w_MVCODCON, this.w_OBJ.w_MVFLVEAC, this.w_OBJ.w_MVCLADOC )
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                else
                  this.oParentObject.w_oERRORLOG.AddMsgLog("Errore nella generazione del documento fiscale:%0%1", alltrim( this.w_RESOCONTO ))     
                endif
              else
                this.w_RESOCONTO = "Impossibile generare documento senza righe di dettaglio"
                this.oParentObject.w_oERRORLOG.AddMsgLog("Errore nella generazione del documento fiscale:%0%1", alltrim( this.w_RESOCONTO ))     
              endif
            endif
            this.w_OBJ = .NULL.
            * --- Apre un altro documento
            this.w_OBJ = CREATEOBJECT("DocumWriter",this,"DOC_MAST")
            this.w_INSERISCIDOCUMENTOFISCALE = .F.
          endif
          this.w_OBJ.w_MVSERIAL = SPACE( 10 )
          this.w_OBJ.w_MVTIPDOC = this.w_MVTIPDOC
          this.w_OBJ.w_MVALFDOC = this.w_MVALFDOC
          this.w_OBJ.w_MVTIPCON = this.w_MVTIPCON
          this.w_OBJ.w_MVFLSCOR = this.w_MVFLSCOR
          this.w_OBJ.w_MVCODPAG = this.oParentObject.w_MVCODPAG
          this.w_OBJ.w_MVSCOPAG = this.oParentObject.w_MVSCOPAG
          this.w_OBJ.w_MVFLFOSC = "N"
          this.w_OBJ.w_MVDATREG = this.oParentObject.w_GPDATREG
          this.w_OBJ.w_MVDATDOC = this.oParentObject.w_GPDATREG
          this.w_OBJ.w_MVCODCON = this.w_MVCODCON
          this.w_OBJ.w_MVCODBAN = this.w_MVCODBAN
          this.w_OBJ.w_MVCODAGE = this.w_MVCODAGE
          this.w_OBJ.w_MVCODAG2 = this.w_MVCODAG2
          this.w_OBJ.w_MVRIFEST = this.w_MVRIFEST
          this.w_OBJ.w_MVCODVAL = this.oParentObject.w_GPVALCON
          this.w_OBJ.w_MVFLVEAC = this.w_MVFLVEAC
          this.w_OBJ.w_MVGENPRE = this.oParentObject.w_GPSERIAL
          this.w_OBJ.w_MVFLPROV = this.oParentObject.w_PROVVFIS
          * --- bNoCalcPrz = .F. esegue il calcolo da listino/contratto dei prezzi passati a 0
          this.w_OBJ.bNoCalcPrz = .F.
          * --- Dettaglio documento fiscale - scriver� 1 volta solo per contratto
          *     1) Riga descrittiva di riferimento 
          *     2) Riga forfettario sconto incondizionato
          *     3) Riga forfettaria sconto condizionato premio
          *     4) Riga fuori magazzino totale punti (quantit�)
          *     
          *     Saranno invece ripetute le righe con quantit� PCQTAPRE  e prezzo listino
          *     Ripartizione righe su articoli contratto
          this.w_CPROWNUM = 0
          if !Empty(this.oParentObject.w_CONOTART)
            this.w_CPROWNUM = this.w_CPROWNUM + 1
            * --- Inserisce riga
            this.w_OBJ.AddDetail(this.w_CPROWNUM, this.w_CPROWNUM*10, this.oParentObject.w_CONOTART, "   ", 0, 0 , "D")     
            this.w_OBJ.w_DOC_DETT.MVDESART = this.w_CONOTDES
            this.w_OBJ.w_DOC_DETT.MVDESSUP = this.w_CONOTGEN
            this.w_OBJ.w_DOC_DETT.MVFLOMAG = "X"
          endif
          if this.w_COTIPSTO = "F"
            * --- Tipo Storno :Forfettario (scrive 2 righe con articolo forfettario di rifermento)
            *     1) Valore SCONTI INCONDIZIONATI
            * --- Scrivo solo 1 riga per il contratto in esame
            if this.w_DOCSCOM <> 0
              this.w_CPROWNUM = this.w_CPROWNUM + 1
              * --- Inserisce riga
              this.w_INSERISCIDOCUMENTOFISCALE = .T.
              this.w_OBJ.AddDetail(this.w_CPROWNUM, this.w_CPROWNUM*10, this.oParentObject.w_COARTFOR, this.oParentObject.w_UMFORF, 1, this.w_DOCSCOM , "F")     
              this.w_OBJ.w_DOC_DETT.MVDESART = this.oParentObject.w_DESFOR
              this.w_OBJ.w_DOC_DETT.MVFLOMAG = "X"
              this.w_OBJ.w_DOC_DETT.MVCODIVA = this.oParentObject.w_MVCODIVA
            endif
            * --- 2) Valore SCONTI CONDIZIONATI DAL PREMIO RAGGIUNTO (TOTPREMIO)
            if this.w_TOTPREMIO <> 0
              this.w_CPROWNUM = this.w_CPROWNUM + 1
              this.w_INSERISCIDOCUMENTOFISCALE = .T.
              this.w_OBJ.AddDetail(this.w_CPROWNUM, this.w_CPROWNUM*10, this.oParentObject.w_COARTFOR, this.oParentObject.w_UMFORF, 1, this.w_TOTPREMIO , "F")     
              this.w_OBJ.w_DOC_DETT.MVDESART = this.oParentObject.w_DESFOR
              this.w_OBJ.w_DOC_DETT.MVFLOMAG = "X"
              this.w_OBJ.w_DOC_DETT.MVCODIVA = this.oParentObject.w_MVCODIVA
            endif
          endif
          if this.oParentObject.w_GPPUNPRE = "S" And this.w_TOTPUN > 0
            * --- Se � attivo il flag "Dettaglia punti premio"�raggruppa le righe che hanno 
            *     PCPUNPRE<>0 e PCQTAPRE=0 scrive COPUNART, 
            *     u.m.=COPUNUNM, qt�=sommaPCPUNPRE, prezzo non valorizzato (=0)
            this.w_CPROWNUM = this.w_CPROWNUM + 1
            this.w_INSERISCIDOCUMENTOFISCALE = .T.
            this.w_OBJ.AddDetail(this.w_CPROWNUM, this.w_CPROWNUM*10, this.oParentObject.w_COPUNART, this.oParentObject.w_COPUNUNM, this.w_TOTPUN, 0 , "F")     
            this.w_OBJ.w_DOC_DETT.MVDESART = this.oParentObject.w_DESFM
            this.w_OBJ.w_DOC_DETT.MVCODIVA = this.oParentObject.w_MVCODIVA
          endif
          * --- Totale premio raggruppato per contratto
          this.w_ROWRIF = ""
        endif
        * --- Pertecipa
        this.w_PARTECIPA = _Curs_GSGP_DOC.PARTECIPA
        * --- Totale venduto\fatturato per Articolo
        this.w_QTACON = _Curs_GSGP_DOC.PESOQTA
        this.w_VALCON = _Curs_GSGP_DOC.PESOVAL
        this.w_RIGAKEY = _Curs_GSGP_DOC.CPROWNUM
        this.w_MVPREZZO = 0
        if this.w_COTIPSTO = "R" And (this.w_TOTPREMIO + this.w_DOCSCOM) > 0
          if this.w_PARTECIPA="S"
            * --- il premio verr� ripartito nei diversi articoli venduti/acquistati in modo
            *      proporzionale alle quantit� o al valore: la procedura 
            *     deve verificare la valorizzazione di COBASPRO (Contropartite e vincoli) 
            *      per identificare il criterio .(cambia il valore del peso)
            if _Curs_GSGP_DOC.COFLGINC = 0
              if this.oParentObject.w_COBASPRO = "I"
                * --- Peso = Valore
                this.w_MVPREZZO = IIF(this.w_VALCONROW=0,0,this.w_VALCON/this.w_VALCONROW * (this.w_TOTPREMIO + this.w_DOCSCOM))
              else
                * --- Peso = Quantit�
                this.w_MVPREZZO = IIF(this.w_QTACONROW=0,0,this.w_QTACON/this.w_QTACONROW * (this.w_TOTPREMIO+ this.w_DOCSCOM))
              endif
            else
              if this.oParentObject.w_COBASPRO = "I"
                * --- Peso = Valore
                this.w_MVPREZZO = IIF(this.w_VALCONROW=0,0,this.w_VALCON/this.w_VALCONROW * this.w_DOCSCOM)
              else
                * --- Peso = Quantit�
                this.w_MVPREZZO = IIF(this.w_QTACONROW=0,0, this.w_QTACON/this.w_QTACONROW * this.w_DOCSCOM)
              endif
            endif
          endif
          this.w_CPROWNUM = this.w_CPROWNUM + 1
          this.w_MVCODICE = _Curs_GSGP_DOC.COKEYART
          * --- Read from KEY_ARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CA__TIPO"+;
              " from "+i_cTable+" KEY_ARTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.w_MVCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CA__TIPO;
              from (i_cTable) where;
                  CACODICE = this.w_MVCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MVTIPRIG = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_INSERISCIDOCUMENTOFISCALE = .T.
          this.w_OBJ.AddDetail(this.w_CPROWNUM, this.w_CPROWNUM*10, _Curs_GSGP_DOC.COKEYART, _Curs_GSGP_DOC.COUNIMIS, 1, this.w_MVPREZZO, this.w_MVTIPRIG)     
          this.w_OBJ.w_DOC_DETT.MVFLOMAG = "X"
          this.w_OBJ.w_DOC_DETT.MVCODIVA = this.oParentObject.w_MVCODIVA
          this.w_OBJ.w_DOC_DETT.MVCODMAG = g_MAGAZI
        endif
        * --- Per le righe che hanno PCQTAPRE<>0 e PCPUNPRE=0 scrive riga con 
        *     PCCDOART,u.m=PCUNMART,  qta=PCQTAPRE, prz=valore del listino valido 
        *     per il cliente/fornitore e flag sconto=PCFLGSCO
        if this.w_ROWRIF <> alltrim(str(this.w_RIGAKEY)) + this.w_APCONTR
          * --- Select from PREMICON
          i_nConn=i_TableProp[this.PREMICON_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" PREMICON ";
                +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And   PCROWNUM = "+cp_ToStrODBC(this.w_RIGAKEY)+" And   PCQTAPRE <> 0 And   PCPUNPRE = 0 ";
                 ,"_Curs_PREMICON")
          else
            select * from (i_cTable);
             where CCCODICE = this.w_APCONTR And   PCROWNUM = this.w_RIGAKEY And   PCQTAPRE <> 0 And   PCPUNPRE = 0 ;
              into cursor _Curs_PREMICON
          endif
          if used('_Curs_PREMICON')
            select _Curs_PREMICON
            locate for 1=1
            do while not(eof())
            this.w_CPROWNUM = this.w_CPROWNUM + 1
            this.w_MVCODICE = _Curs_PREMICON.PCCODRIC
            * --- Read from KEY_ARTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CA__TIPO"+;
                " from "+i_cTable+" KEY_ARTI where ";
                    +"CACODICE = "+cp_ToStrODBC(this.w_MVCODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CA__TIPO;
                from (i_cTable) where;
                    CACODICE = this.w_MVCODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MVTIPRIG = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_INSERISCIDOCUMENTOFISCALE = .T.
            this.w_OBJ.AddDetail(this.w_CPROWNUM, this.w_CPROWNUM*10, _Curs_PREMICON.PCCODRIC, _Curs_PREMICON.PCUNMART, _Curs_PREMICON.PCQTAPRE, 0, this.w_MVTIPRIG)     
            this.w_OBJ.w_DOC_DETT.MVFLOMAG = _Curs_PREMICON.PCFLGSCO
            this.w_OBJ.w_DOC_DETT.MVCODIVA = this.oParentObject.w_MVCODIVA
              select _Curs_PREMICON
              continue
            enddo
            use
          endif
        endif
        this.w_ROWRIF = alltrim(str(this.w_RIGAKEY)) + this.w_APCONTR
          select _Curs_GSGP_DOC
          continue
        enddo
        use
      endif
      * --- Chiude l'ultimo documento
      if this.w_INTESTATARIO_OK OR this.w_TROVATOINTESTATARIOCOLLEGATO
        this.w_RESOCONTO = ""
        if this.w_INSERISCIDOCUMENTOFISCALE
          this.w_RESOCONTO = this.w_OBJ.CreateDocument()
          if EMPTY( this.w_RESOCONTO )
            GSGP_BCQ (this, this.oParentObject.w_GPSERIAL, this.w_OBJ.w_MVSERIAL , this.w_OBJ.w_MVNUMDOC , this.w_OBJ.w_MVALFDOC , this.w_OBJ.w_MVDATDOC , this.w_OBJ.w_MVTIPCON , this.w_OBJ.w_MVCODCON, this.w_OBJ.w_MVFLVEAC, this.w_OBJ.w_MVCLADOC )
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          else
            this.oParentObject.w_oERRORLOG.AddMsgLog("Errore nella generazione del documento fiscale:%0%1", alltrim( this.w_RESOCONTO ))     
          endif
        else
          this.w_RESOCONTO = "Impossibile generare documento senza righe di dettaglio"
          this.oParentObject.w_oERRORLOG.AddMsgLog("Errore nella generazione del documento fiscale:%0%1", alltrim( this.w_RESOCONTO ))     
        endif
      endif
      this.w_OBJ = .NULL.
    else
      this.w_ROWRIF = REPL("#",20)
      * --- Select from GSGP_INT
      do vq_exec with 'GSGP_INT',this,'_Curs_GSGP_INT','',.f.,.t.
      if used('_Curs_GSGP_INT')
        select _Curs_GSGP_INT
        locate for 1=1
        do while not(eof())
        * --- Genera documenti
        if this.w_ROWRIF <> alltrim(str(_Curs_GSGP_INT.MESE)) + _Curs_GSGP_INT.COCODICE
          this.w_APCONTR = _Curs_GSGP_INT.COCODICE
          this.w_NUMRIG = _Curs_GSGP_INT.NUMRIG
          this.w_TIPINT = _Curs_GSGP_INT.COTIPINT
          * --- Sconto incond.
          this.w_DOCSCOM = _Curs_GSGP_INT.COSCOCOM
          * --- Totale premio calcolato
          this.w_TOTPREMIO = _Curs_GSGP_INT.TOTVALPRE
          * --- Totale FATTURATO\ VENDUTO CONTRATTO
          this.w_QTACONROW = _Curs_GSGP_INT.PESOQTAROW
          this.w_VALCONROW = _Curs_GSGP_INT.PESOVALROW
          this.w_MVTIPDOC = this.oParentObject.w_GPCAUINT
          this.w_MVCLADOC = "DI"
          this.w_CODAGE = _Curs_GSGP_INT.COCODAGE
          this.w_MVFLVEAC = this.oParentObject.w_GPFLGCIC
          this.w_MVTIPCON = this.oParentObject.w_FLINTE
          this.w_DATREG = GET_DATE(_Curs_GSGP_INT.MESE,"M",STR(_Curs_GSGP_INT.ANNO),2)
          this.w_CONOTDES = LEFT(ALLTRIM(_Curs_GSGP_INT.CONOTGEN),40)
          this.w_CONOTGEN = _Curs_GSGP_INT.CONOTGEN
          do case
            case this.w_TIPINT = "G"
              * --- Lettura azienda impostata come centrale del gruppo
              * --- Read from GRUPPACQ
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.GRUPPACQ_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.GRUPPACQ_idx,2],.t.,this.GRUPPACQ_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "GACODCLI"+;
                  " from "+i_cTable+" GRUPPACQ where ";
                      +"GACODICE = "+cp_ToStrODBC(_Curs_GSGP_INT.COCODGRU);
                      +" and GAFLPRIF = "+cp_ToStrODBC("S");
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  GACODCLI;
                  from (i_cTable) where;
                      GACODICE = _Curs_GSGP_INT.COCODGRU;
                      and GAFLPRIF = "S";
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_MVCODCON = NVL(cp_ToDate(_read_.GACODCLI),cp_NullValue(_read_.GACODCLI))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            case this.w_TIPINT $ "CF"
              * --- Cliente/Fornitore
              this.w_MVCODCON = _Curs_GSGP_INT.COCODINT
            case this.w_TIPINT = "A"
              * --- Lettura fornitore in anagrafica agente
              * --- Read from AGENTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.AGENTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "AGCODFOR"+;
                  " from "+i_cTable+" AGENTI where ";
                      +"AGCODAGE = "+cp_ToStrODBC(this.w_CODAGE);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  AGCODFOR;
                  from (i_cTable) where;
                      AGCODAGE = this.w_CODAGE;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_MVCODCON = NVL(cp_ToDate(_read_.AGCODFOR),cp_NullValue(_read_.AGCODFOR))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              this.w_MVCODAGE = this.w_CODAGE
          endcase
          * --- Informazione associate all'intestatrio
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANSCORPO,ANCODBAN,ANCODAG1"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_MVTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_MVCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANSCORPO,ANCODBAN,ANCODAG1;
              from (i_cTable) where;
                  ANTIPCON = this.w_MVTIPCON;
                  and ANCODICE = this.w_MVCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MVFLSCOR = NVL(cp_ToDate(_read_.ANSCORPO),cp_NullValue(_read_.ANSCORPO))
            this.w_MVCODBAN = NVL(cp_ToDate(_read_.ANCODBAN),cp_NullValue(_read_.ANCODBAN))
            this.w_ANCODAG1 = NVL(cp_ToDate(_read_.ANCODAG1),cp_NullValue(_read_.ANCODAG1))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_TIPINT <> "A"
            this.w_MVCODAGE = this.w_ANCODAG1
            this.w_MVCODAG2 = this.w_ANCODAG2
          endif
          * --- Scrittura Master 
          this.w_NumSERIAL = this.w_NumSERIAL + 1
          this.w_MSERIAL = RIGHT("0000000000"+ALLTRIM(STR(this.w_NumSERIAL)) ,10)
          * --- la  procedura scrive il codice contratto in MVRIFEST.
          this.w_MVRIFEST = "Contratto: "+ alltrim(this.w_APCONTR)
          * --- Inserimento testata
          this.w_NDOCGENERATI = this.w_NDOCGENERATI + 1
          if this.w_NDOCGENERATI > 1
            * --- Chiude il documento precedente
            this.w_RESOCONTO = ""
            this.w_RESOCONTO = this.w_OBJ.CreateDocument()
            if EMPTY( this.w_RESOCONTO )
              GSGP_BCQ (this, this.oParentObject.w_GPSERIAL, this.w_OBJ.w_MVSERIAL , this.w_OBJ.w_MVNUMDOC , this.w_OBJ.w_MVALFDOC , this.w_OBJ.w_MVDATDOC , this.w_OBJ.w_MVTIPCON , this.w_OBJ.w_MVCODCON, this.w_OBJ.w_MVFLVEAC, this.w_OBJ.w_MVCLADOC )
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              this.oParentObject.w_oERRORLOG.AddMsgLog("Errore nella generazione del documento interno di rettifica:%0%1", alltrim( this.w_RESOCONTO ))     
            endif
            this.w_OBJ = .NULL.
            * --- Apre un altro documento
            this.w_OBJ = CREATEOBJECT("DocumWriter",this,"DOC_MAST")
          endif
          this.w_OBJ.w_MVSERIAL = SPACE( 10 )
          this.w_OBJ.w_MVTIPDOC = this.w_MVTIPDOC
          this.w_OBJ.w_MVTIPCON = this.w_MVTIPCON
          this.w_OBJ.w_MVCODPAG = this.oParentObject.w_MVCODPAG
          this.w_OBJ.w_MVSCOPAG = this.oParentObject.w_MVSCOPAG
          this.w_OBJ.w_MVFLFOSC = "N"
          this.w_OBJ.w_MVDATREG = this.w_DATREG
          this.w_OBJ.w_MVDATDOC = this.w_DATREG
          this.w_OBJ.w_MVRIFEST = this.w_MVRIFEST
          this.w_OBJ.w_MVCODVAL = this.oParentObject.w_GPVALCON
          this.w_OBJ.w_MVFLVEAC = this.w_MVFLVEAC
          this.w_OBJ.w_MVGENPRE = this.oParentObject.w_GPSERIAL
          this.w_OBJ.w_MVCODCON = this.w_MVCODCON
          this.w_OBJ.w_MVCODAGE = this.w_MVCODAGE
          this.w_OBJ.w_MVCODAG2 = this.w_MVCODAG2
          this.w_OBJ.w_MVFLPROV = this.oParentObject.w_PROVVINT
          this.w_CPROWNUM = 0
          if !Empty(this.oParentObject.w_CONOTART)
            * --- Riga descrittiva
            this.w_CPROWNUM = this.w_CPROWNUM + 1
            this.w_OBJ.AddDetail(this.w_CPROWNUM, this.w_CPROWNUM*10, this.oParentObject.w_CONOTART, "   ", 0, 0 , "D")     
            this.w_OBJ.w_DOC_DETT.MVDESART = this.w_CONOTDES
            this.w_OBJ.w_DOC_DETT.MVDESSUP = this.w_CONOTGEN
            this.w_OBJ.w_DOC_DETT.MVFLOMAG = "X"
          endif
        endif
        * --- Pertecipa
        this.w_PARTECIPA = _Curs_GSGP_INT.PARTECIPA
        * --- Totale venduto\fatturato per Articolo
        this.w_QTACON = _Curs_GSGP_INT.PESOQTA
        this.w_VALCON = _Curs_GSGP_INT.PESOVAL
        this.w_RIGAKEY = _Curs_GSGP_INT.MESE
        this.w_APCONTR = _Curs_GSGP_INT.COCODICE
        this.w_ROWRIF = alltrim(str(this.w_RIGAKEY)) + this.w_APCONTR
        * --- il premio verr� ripartito nei diversi articoli venduti/acquistati in modo
        *      proporzionale alle quantit� o al valore: la procedura 
        *     deve verificare la valorizzazione di COBASPRO (Contropartite e vincoli) 
        *      per identificare il criterio .(cambia il valore del peso)
        if this.w_PARTECIPA<>"S"
          this.w_MVPREZZO = 0
        else
          if _Curs_GSGP_INT.COFLGINC = 0
            if this.oParentObject.w_COBASPRO = "I"
              * --- Peso = Valore
              this.w_MVPREZZO = IIF(this.w_VALCONROW=0,0,this.w_VALCON/this.w_VALCONROW * (this.w_TOTPREMIO + this.w_DOCSCOM))
            else
              * --- Peso = Quantit�
              this.w_MVPREZZO = IIF(this.w_QTACONROW=0,0,this.w_QTACON/this.w_QTACONROW * (this.w_TOTPREMIO+ this.w_DOCSCOM))
            endif
          else
            if this.oParentObject.w_COBASPRO = "I"
              * --- Peso = Valore
              this.w_MVPREZZO = IIF(this.w_VALCONROW=0,0,this.w_VALCON/this.w_VALCONROW * this.w_DOCSCOM)
            else
              * --- Peso = Quantit�
              this.w_MVPREZZO = IIF(this.w_QTACONROW=0,0, this.w_QTACON/this.w_QTACONROW * this.w_DOCSCOM)
            endif
          endif
        endif
        this.w_CPROWNUM = this.w_CPROWNUM + 1
        this.w_MVCODICE = _Curs_GSGP_INT.COKEYART
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CA__TIPO"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_MVCODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CA__TIPO;
            from (i_cTable) where;
                CACODICE = this.w_MVCODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVTIPRIG = NVL(cp_ToDate(_read_.CA__TIPO),cp_NullValue(_read_.CA__TIPO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_OBJ.AddDetail(this.w_CPROWNUM, this.w_CPROWNUM*10, _Curs_GSGP_INT.COKEYART, _Curs_GSGP_INT.COUNIMIS, 1, this.w_MVPREZZO, this.w_MVTIPRIG)     
        this.w_OBJ.w_DOC_DETT.MVCODIVA = this.oParentObject.w_MVCODIVA
        this.w_OBJ.w_DOC_DETT.MVCODMAG = g_MAGAZI
        this.w_OBJ.w_DOC_DETT.MVFLOMAG = "X"
          select _Curs_GSGP_INT
          continue
        enddo
        use
      endif
      * --- Chiude l'ultimo documento
      this.w_RESOCONTO = ""
      this.w_RESOCONTO = this.w_OBJ.CreateDocument()
      if EMPTY( this.w_RESOCONTO )
        GSGP_BCQ (this, this.oParentObject.w_GPSERIAL, this.w_OBJ.w_MVSERIAL , this.w_OBJ.w_MVNUMDOC , this.w_OBJ.w_MVALFDOC , this.w_OBJ.w_MVDATDOC , this.w_OBJ.w_MVTIPCON , this.w_OBJ.w_MVCODCON, this.w_OBJ.w_MVFLVEAC, this.w_OBJ.w_MVCLADOC )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        this.oParentObject.w_oERRORLOG.AddMsgLog("Errore nella generazione del documento interno di rettifica:%0%1", alltrim( this.w_RESOCONTO ))     
      endif
      this.w_OBJ = .NULL.
    endif
  endproc


  proc Init(oParentObject,pTIPDOC)
    this.pTIPDOC=pTIPDOC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='GRUPPACQ'
    this.cWorkTables[2]='AGENTI'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='PREMICON'
    this.cWorkTables[5]='TIP_DOCU'
    this.cWorkTables[6]='CCOMMAST'
    this.cWorkTables[7]='KEY_ARTI'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_GSGP_DOC')
      use in _Curs_GSGP_DOC
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_GSGP_INT')
      use in _Curs_GSGP_INT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPDOC"
endproc
