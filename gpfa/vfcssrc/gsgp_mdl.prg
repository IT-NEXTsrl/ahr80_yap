* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsgp_mdl                                                        *
*              Attributi articolo                                              *
*                                                                              *
*      Author: Zucchetti Spa - AT                                              *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-04                                                      *
* Last revis.: 2012-10-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsgp_mdl")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsgp_mdl")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsgp_mdl")
  return

* --- Class definition
define class tgsgp_mdl as StdPCForm
  Width  = 497
  Height = 188
  Top    = 10
  Left   = 10
  cComment = "Attributi articolo"
  cPrg = "gsgp_mdl"
  HelpContextID=188116631
  add object cnt as tcgsgp_mdl
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsgp_mdl as PCContext
  w_CA__GUID = space(15)
  w_CPROWORD = 0
  w_SRV = space(1)
  w_CACODMOD = space(20)
  w_FLCICL = space(1)
  w_GEST = space(10)
  w_CACODGRU = space(10)
  w_CACODFAM = space(10)
  w_CAVALATT = space(20)
  w_CADESCRI = space(50)
  w_AUTOMA = space(1)
  w_CAROWRIF = 0
  w_CONTA = 0
  w_TOTATTRIB = 0
  w_CODATGEN = space(8)
  w_CODFAM1 = space(10)
  w_FR_TABLE = space(30)
  w_FR__ZOOM = space(254)
  w_TIPO = space(1)
  w_CODFAM = space(10)
  w_ASCODATT = space(10)
  proc Save(i_oFrom)
    this.w_CA__GUID = i_oFrom.w_CA__GUID
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_SRV = i_oFrom.w_SRV
    this.w_CACODMOD = i_oFrom.w_CACODMOD
    this.w_FLCICL = i_oFrom.w_FLCICL
    this.w_GEST = i_oFrom.w_GEST
    this.w_CACODGRU = i_oFrom.w_CACODGRU
    this.w_CACODFAM = i_oFrom.w_CACODFAM
    this.w_CAVALATT = i_oFrom.w_CAVALATT
    this.w_CADESCRI = i_oFrom.w_CADESCRI
    this.w_AUTOMA = i_oFrom.w_AUTOMA
    this.w_CAROWRIF = i_oFrom.w_CAROWRIF
    this.w_CONTA = i_oFrom.w_CONTA
    this.w_TOTATTRIB = i_oFrom.w_TOTATTRIB
    this.w_CODATGEN = i_oFrom.w_CODATGEN
    this.w_CODFAM1 = i_oFrom.w_CODFAM1
    this.w_FR_TABLE = i_oFrom.w_FR_TABLE
    this.w_FR__ZOOM = i_oFrom.w_FR__ZOOM
    this.w_TIPO = i_oFrom.w_TIPO
    this.w_CODFAM = i_oFrom.w_CODFAM
    this.w_ASCODATT = i_oFrom.w_ASCODATT
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CA__GUID = this.w_CA__GUID
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_SRV = this.w_SRV
    i_oTo.w_CACODMOD = this.w_CACODMOD
    i_oTo.w_FLCICL = this.w_FLCICL
    i_oTo.w_GEST = this.w_GEST
    i_oTo.w_CACODGRU = this.w_CACODGRU
    i_oTo.w_CACODFAM = this.w_CACODFAM
    i_oTo.w_CAVALATT = this.w_CAVALATT
    i_oTo.w_CADESCRI = this.w_CADESCRI
    i_oTo.w_AUTOMA = this.w_AUTOMA
    i_oTo.w_CAROWRIF = this.w_CAROWRIF
    i_oTo.w_CONTA = this.w_CONTA
    i_oTo.w_TOTATTRIB = this.w_TOTATTRIB
    i_oTo.w_CODATGEN = this.w_CODATGEN
    i_oTo.w_CODFAM1 = this.w_CODFAM1
    i_oTo.w_FR_TABLE = this.w_FR_TABLE
    i_oTo.w_FR__ZOOM = this.w_FR__ZOOM
    i_oTo.w_TIPO = this.w_TIPO
    i_oTo.w_CODFAM = this.w_CODFAM
    i_oTo.w_ASCODATT = this.w_ASCODATT
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsgp_mdl as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 497
  Height = 188
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-17"
  HelpContextID=188116631
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  CCOMDATT_IDX = 0
  MODMATTR_IDX = 0
  FAM_ATTR_IDX = 0
  FAMDATTR_IDX = 0
  MODDATTR_IDX = 0
  GRUDATTR_IDX = 0
  GRU_ATTR_IDX = 0
  cFile = "CCOMDATT"
  cKeySelect = "CA__GUID,CAROWRIF"
  cKeyWhere  = "CA__GUID=this.w_CA__GUID and CAROWRIF=this.w_CAROWRIF"
  cKeyDetail  = "CA__GUID=this.w_CA__GUID and CACODMOD=this.w_CACODMOD and CACODGRU=this.w_CACODGRU and CACODFAM=this.w_CACODFAM and CAVALATT=this.w_CAVALATT and CAROWRIF=this.w_CAROWRIF"
  cKeyWhereODBC = '"CA__GUID="+cp_ToStrODBC(this.w_CA__GUID)';
      +'+" and CAROWRIF="+cp_ToStrODBC(this.w_CAROWRIF)';

  cKeyDetailWhereODBC = '"CA__GUID="+cp_ToStrODBC(this.w_CA__GUID)';
      +'+" and CACODMOD="+cp_ToStrODBC(this.w_CACODMOD)';
      +'+" and CACODGRU="+cp_ToStrODBC(this.w_CACODGRU)';
      +'+" and CACODFAM="+cp_ToStrODBC(this.w_CACODFAM)';
      +'+" and CAVALATT="+cp_ToStrODBC(this.w_CAVALATT)';
      +'+" and CAROWRIF="+cp_ToStrODBC(this.w_CAROWRIF)';

  cKeyWhereODBCqualified = '"CCOMDATT.CA__GUID="+cp_ToStrODBC(this.w_CA__GUID)';
      +'+" and CCOMDATT.CAROWRIF="+cp_ToStrODBC(this.w_CAROWRIF)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CCOMDATT.CPROWORD '
  cPrg = "gsgp_mdl"
  cComment = "Attributi articolo"
  i_nRowNum = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CA__GUID = space(15)
  w_CPROWORD = 0
  w_SRV = space(1)
  w_CACODMOD = space(20)
  o_CACODMOD = space(20)
  w_FLCICL = space(1)
  w_GEST = space(10)
  w_CACODGRU = space(10)
  w_CACODFAM = space(10)
  o_CACODFAM = space(10)
  w_CAVALATT = space(20)
  w_CADESCRI = space(50)
  w_AUTOMA = space(1)
  w_CAROWRIF = 0
  w_CONTA = 0
  w_TOTATTRIB = 0
  w_CODATGEN = ctod('  /  /  ')
  w_CODFAM1 = space(10)
  w_FR_TABLE = space(30)
  w_FR__ZOOM = space(254)
  w_TIPO = space(1)
  w_CODFAM = space(10)
  w_ASCODATT = space(10)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsgp_mdlPag1","gsgp_mdl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='MODMATTR'
    this.cWorkTables[2]='FAM_ATTR'
    this.cWorkTables[3]='FAMDATTR'
    this.cWorkTables[4]='MODDATTR'
    this.cWorkTables[5]='GRUDATTR'
    this.cWorkTables[6]='GRU_ATTR'
    this.cWorkTables[7]='CCOMDATT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CCOMDATT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CCOMDATT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsgp_mdl'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from CCOMDATT where CA__GUID=KeySet.CA__GUID
    *                            and CACODMOD=KeySet.CACODMOD
    *                            and CACODGRU=KeySet.CACODGRU
    *                            and CACODFAM=KeySet.CACODFAM
    *                            and CAVALATT=KeySet.CAVALATT
    *                            and CAROWRIF=KeySet.CAROWRIF
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.CCOMDATT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCOMDATT_IDX,2],this.bLoadRecFilter,this.CCOMDATT_IDX,"gsgp_mdl")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CCOMDATT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CCOMDATT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CCOMDATT '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CA__GUID',this.w_CA__GUID  ,'CAROWRIF',this.w_CAROWRIF  )
      select * from (i_cTable) CCOMDATT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TOTATTRIB = 0
        .w_CA__GUID = NVL(CA__GUID,space(15))
        .w_FLCICL = .oParentObject.w_COFLGCIC
        .w_CAROWRIF = NVL(CAROWRIF,0)
        .w_CODATGEN = .oParentObject.w_CODATGEN
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_ASCODATT = .w_CACODGRU
        cp_LoadRecExtFlds(this,'CCOMDATT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTATTRIB = 0
      scan
        with this
          .w_SRV = space(1)
        .w_AUTOMA = "N"
          .w_FR_TABLE = space(30)
          .w_FR__ZOOM = space(254)
          .w_TIPO = space(1)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CACODMOD = NVL(CACODMOD,space(20))
        .w_GEST = .w_CACODMOD
          .w_CACODGRU = NVL(CACODGRU,space(10))
          * evitabile
          *.link_2_5('Load')
          .w_CACODFAM = NVL(CACODFAM,space(10))
          * evitabile
          *.link_2_6('Load')
          .w_CAVALATT = NVL(CAVALATT,space(20))
          .w_CADESCRI = NVL(CADESCRI,space(50))
        .w_CONTA = iif(Empty(.w_CAVALATT),0,1)
        .w_CODFAM1 = .w_CACODFAM
          .link_2_12('Load')
        .w_CODFAM = .w_CACODFAM
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTATTRIB = .w_TOTATTRIB+.w_CONTA
          replace CACODMOD with .w_CACODMOD
          replace CACODGRU with .w_CACODGRU
          replace CACODFAM with .w_CACODFAM
          replace CAVALATT with .w_CAVALATT
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_FLCICL = .oParentObject.w_COFLGCIC
        .w_CODATGEN = .oParentObject.w_CODATGEN
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_CODFAM = .w_CACODFAM
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_3_2.enabled = .oPgFrm.Page1.oPag.oBtn_3_2.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_CA__GUID=space(15)
      .w_CPROWORD=10
      .w_SRV=space(1)
      .w_CACODMOD=space(20)
      .w_FLCICL=space(1)
      .w_GEST=space(10)
      .w_CACODGRU=space(10)
      .w_CACODFAM=space(10)
      .w_CAVALATT=space(20)
      .w_CADESCRI=space(50)
      .w_AUTOMA=space(1)
      .w_CAROWRIF=0
      .w_CONTA=0
      .w_TOTATTRIB=0
      .w_CODATGEN=ctod("  /  /  ")
      .w_CODFAM1=space(10)
      .w_FR_TABLE=space(30)
      .w_FR__ZOOM=space(254)
      .w_TIPO=space(1)
      .w_CODFAM=space(10)
      .w_ASCODATT=space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        .w_CACODMOD = get_GPFA_code("MODELLO")
        .w_FLCICL = .oParentObject.w_COFLGCIC
        .w_GEST = .w_CACODMOD
        .w_CACODGRU = get_GPFA_code("GRUPPO")
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CACODGRU))
         .link_2_5('Full')
        endif
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CACODFAM))
         .link_2_6('Full')
        endif
        .w_CAVALATT = ''
        .DoRTCalc(10,10,.f.)
        .w_AUTOMA = "N"
        .DoRTCalc(12,12,.f.)
        .w_CONTA = iif(Empty(.w_CAVALATT),0,1)
        .DoRTCalc(14,14,.f.)
        .w_CODATGEN = .oParentObject.w_CODATGEN
        .w_CODFAM1 = .w_CACODFAM
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CODFAM1))
         .link_2_12('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(17,19,.f.)
        .w_CODFAM = .w_CACODFAM
        .w_ASCODATT = .w_CACODGRU
      endif
    endwith
    cp_BlankRecExtFlds(this,'CCOMDATT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_3_2.enabled = this.oPgFrm.Page1.oPag.oBtn_3_2.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_3_2.enabled = .Page1.oPag.oBtn_3_2.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'CCOMDATT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CCOMDATT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CA__GUID,"CA__GUID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CAROWRIF,"CAROWRIF",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(6);
      ,t_CACODGRU C(10);
      ,t_CACODFAM C(10);
      ,t_CAVALATT C(20);
      ,t_CADESCRI C(50);
      ,CACODMOD C(20);
      ,CACODGRU C(10);
      ,CACODFAM C(10);
      ,CAVALATT C(20);
      ,t_SRV C(1);
      ,t_CACODMOD C(20);
      ,t_GEST C(10);
      ,t_AUTOMA C(1);
      ,t_CONTA N(10);
      ,t_CODFAM1 C(10);
      ,t_FR_TABLE C(30);
      ,t_FR__ZOOM C(254);
      ,t_TIPO C(1);
      ,t_CODFAM C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsgp_mdlbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCACODGRU_2_5.controlsource=this.cTrsName+'.t_CACODGRU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCACODFAM_2_6.controlsource=this.cTrsName+'.t_CACODFAM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALATT_2_7.controlsource=this.cTrsName+'.t_CAVALATT'
    this.oPgFRm.Page1.oPag.oCADESCRI_2_8.controlsource=this.cTrsName+'.t_CADESCRI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(46)
    this.AddVLine(198)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CCOMDATT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCOMDATT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CCOMDATT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCOMDATT_IDX,2])
      *
      * insert into CCOMDATT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CCOMDATT')
        i_extval=cp_InsertValODBCExtFlds(this,'CCOMDATT')
        i_cFldBody=" "+;
                  "(CA__GUID,CPROWORD,CACODMOD,CACODGRU,CACODFAM"+;
                  ",CAVALATT,CADESCRI,CAROWRIF,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CA__GUID)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_CACODMOD)+","+cp_ToStrODBCNull(this.w_CACODGRU)+","+cp_ToStrODBCNull(this.w_CACODFAM)+;
             ","+cp_ToStrODBC(this.w_CAVALATT)+","+cp_ToStrODBC(this.w_CADESCRI)+","+cp_ToStrODBC(this.w_CAROWRIF)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CCOMDATT')
        i_extval=cp_InsertValVFPExtFlds(this,'CCOMDATT')
        cp_CheckDeletedKey(i_cTable,0,'CA__GUID',this.w_CA__GUID,'CACODMOD',this.w_CACODMOD,'CACODGRU',this.w_CACODGRU,'CACODFAM',this.w_CACODFAM,'CAVALATT',this.w_CAVALATT,'CAROWRIF',this.w_CAROWRIF)
        INSERT INTO (i_cTable) (;
                   CA__GUID;
                  ,CPROWORD;
                  ,CACODMOD;
                  ,CACODGRU;
                  ,CACODFAM;
                  ,CAVALATT;
                  ,CADESCRI;
                  ,CAROWRIF;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CA__GUID;
                  ,this.w_CPROWORD;
                  ,this.w_CACODMOD;
                  ,this.w_CACODGRU;
                  ,this.w_CACODFAM;
                  ,this.w_CAVALATT;
                  ,this.w_CADESCRI;
                  ,this.w_CAROWRIF;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.CCOMDATT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCOMDATT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CACODMOD)) And not(Empty(t_CACODGRU)) And not(Empty(t_CACODFAM))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'CCOMDATT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CACODMOD="+cp_ToStrODBC(&i_TN.->CACODMOD)+;
                 " and CACODGRU="+cp_ToStrODBC(&i_TN.->CACODGRU)+;
                 " and CACODFAM="+cp_ToStrODBC(&i_TN.->CACODFAM)+;
                 " and CAVALATT="+cp_ToStrODBC(&i_TN.->CAVALATT)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'CCOMDATT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CACODMOD=&i_TN.->CACODMOD;
                      and CACODGRU=&i_TN.->CACODGRU;
                      and CACODFAM=&i_TN.->CACODFAM;
                      and CAVALATT=&i_TN.->CAVALATT;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CACODMOD)) And not(Empty(t_CACODGRU)) And not(Empty(t_CACODFAM))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and CACODMOD="+cp_ToStrODBC(&i_TN.->CACODMOD)+;
                            " and CACODGRU="+cp_ToStrODBC(&i_TN.->CACODGRU)+;
                            " and CACODFAM="+cp_ToStrODBC(&i_TN.->CACODFAM)+;
                            " and CAVALATT="+cp_ToStrODBC(&i_TN.->CAVALATT)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and CACODMOD=&i_TN.->CACODMOD;
                            and CACODGRU=&i_TN.->CACODGRU;
                            and CACODFAM=&i_TN.->CACODFAM;
                            and CAVALATT=&i_TN.->CAVALATT;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace CACODMOD with this.w_CACODMOD
              replace CACODGRU with this.w_CACODGRU
              replace CACODFAM with this.w_CACODFAM
              replace CAVALATT with this.w_CAVALATT
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CCOMDATT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'CCOMDATT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CADESCRI="+cp_ToStrODBC(this.w_CADESCRI)+;
                     ",CACODMOD="+cp_ToStrODBC(this.w_CACODMOD)+;
                     ",CACODGRU="+cp_ToStrODBC(this.w_CACODGRU)+;
                     ",CACODFAM="+cp_ToStrODBC(this.w_CACODFAM)+;
                     ",CAVALATT="+cp_ToStrODBC(this.w_CAVALATT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and CACODMOD="+cp_ToStrODBC(CACODMOD)+;
                             " and CACODGRU="+cp_ToStrODBC(CACODGRU)+;
                             " and CACODFAM="+cp_ToStrODBC(CACODFAM)+;
                             " and CAVALATT="+cp_ToStrODBC(CAVALATT)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'CCOMDATT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,CADESCRI=this.w_CADESCRI;
                     ,CACODMOD=this.w_CACODMOD;
                     ,CACODGRU=this.w_CACODGRU;
                     ,CACODFAM=this.w_CACODFAM;
                     ,CAVALATT=this.w_CAVALATT;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and CACODMOD=&i_TN.->CACODMOD;
                                      and CACODGRU=&i_TN.->CACODGRU;
                                      and CACODFAM=&i_TN.->CACODFAM;
                                      and CAVALATT=&i_TN.->CAVALATT;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CCOMDATT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCOMDATT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CACODMOD)) And not(Empty(t_CACODGRU)) And not(Empty(t_CACODFAM))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete CCOMDATT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and CACODMOD="+cp_ToStrODBC(&i_TN.->CACODMOD)+;
                            " and CACODGRU="+cp_ToStrODBC(&i_TN.->CACODGRU)+;
                            " and CACODFAM="+cp_ToStrODBC(&i_TN.->CACODFAM)+;
                            " and CAVALATT="+cp_ToStrODBC(&i_TN.->CAVALATT)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and CACODMOD=&i_TN.->CACODMOD;
                              and CACODGRU=&i_TN.->CACODGRU;
                              and CACODFAM=&i_TN.->CACODFAM;
                              and CAVALATT=&i_TN.->CAVALATT;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CACODMOD)) And not(Empty(t_CACODGRU)) And not(Empty(t_CACODFAM))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CCOMDATT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCOMDATT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
          .w_FLCICL = .oParentObject.w_COFLGCIC
        if .o_CACODMOD<>.w_CACODMOD
          .w_GEST = .w_CACODMOD
        endif
        .DoRTCalc(7,8,.t.)
        if .o_CACODFAM<>.w_CACODFAM
          .w_CAVALATT = ''
        endif
        .DoRTCalc(10,12,.t.)
          .w_TOTATTRIB = .w_TOTATTRIB-.w_conta
          .w_CONTA = iif(Empty(.w_CAVALATT),0,1)
          .w_TOTATTRIB = .w_TOTATTRIB+.w_conta
        .DoRTCalc(14,14,.t.)
          .w_CODATGEN = .oParentObject.w_CODATGEN
          .w_CODFAM1 = .w_CACODFAM
          .link_2_12('Full')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(17,19,.t.)
          .w_CODFAM = .w_CACODFAM
          .w_ASCODATT = .w_CACODGRU
        * --- Area Manuale = Calculate
        * --- gsgp_mdl
        If .w_TOTATTRIB > 0
         .oParentObject.w_COATTRIB=1
        else
         .oParentObject.w_COATTRIB=2
        endif
        .oParentObject.TrsFromWork()
         Local Old_area
         old_area=select()
         select (this.ctrsname)
         this.w_SRV = i_SRV
         Select( old_area )
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_SRV with this.w_SRV
      replace t_CACODMOD with this.w_CACODMOD
      replace t_GEST with this.w_GEST
      replace t_AUTOMA with this.w_AUTOMA
      replace t_CONTA with this.w_CONTA
      replace t_CODFAM1 with this.w_CODFAM1
      replace t_FR_TABLE with this.w_FR_TABLE
      replace t_FR__ZOOM with this.w_FR__ZOOM
      replace t_TIPO with this.w_TIPO
      replace t_CODFAM with this.w_CODFAM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_XGGRGOMVJX()
    with this
          * --- Imposto srv = a
          .w_SRV = 'A'
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCACODGRU_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCACODGRU_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCACODFAM_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCACODFAM_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCAVALATT_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCAVALATT_2_7.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_3.visible=!this.oPgFrm.Page1.oPag.oStr_1_3.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODGRU_2_5.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODGRU_2_5.mHide()
    this.oPgFrm.Page1.oPag.oCADESCRI_2_8.visible=!this.oPgFrm.Page1.oPag.oCADESCRI_2_8.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init Row")
          .Calculate_XGGRGOMVJX()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CACODGRU
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_ATTR_IDX,3]
    i_lTable = "GRU_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_ATTR_IDX,2], .t., this.GRU_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRU_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GRCODICE like "+cp_ToStrODBC(trim(this.w_CACODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GRCODICE',trim(this.w_CACODGRU))
          select GRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODGRU)==trim(_Link_.GRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRU_ATTR','*','GRCODICE',cp_AbsName(oSource.parent,'oCACODGRU_2_5'),i_cWhere,'',"Gruppi attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where GRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',oSource.xKey(1))
            select GRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where GRCODICE="+cp_ToStrODBC(this.w_CACODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',this.w_CACODGRU)
            select GRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODGRU = NVL(_Link_.GRCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CACODGRU = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.GRCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CACODFAM
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ATTR_IDX,3]
    i_lTable = "FAM_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2], .t., this.FAM_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FAM_ATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FRCODICE like "+cp_ToStrODBC(trim(this.w_CACODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FRCODICE',trim(this.w_CACODFAM))
          select FRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODFAM)==trim(_Link_.FRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ATTR','*','FRCODICE',cp_AbsName(oSource.parent,'oCACODFAM_2_6'),i_cWhere,'',"Famiglie attributi",'GSAR_AAT.FAM_ATTR_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where FRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FRCODICE',oSource.xKey(1))
            select FRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where FRCODICE="+cp_ToStrODBC(this.w_CACODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FRCODICE',this.w_CACODFAM)
            select FRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODFAM = NVL(_Link_.FRCODICE,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CACODFAM = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=chk_fam_gru( .w_CACODFAM, .w_CACODGRU )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CACODFAM = space(10)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.FRCODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAM1
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ATTR_IDX,3]
    i_lTable = "FAM_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2], .t., this.FAM_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAM1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAM1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FRCODICE,FR_TABLE,FR__ZOOM";
                   +" from "+i_cTable+" "+i_lTable+" where FRCODICE="+cp_ToStrODBC(this.w_CODFAM1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FRCODICE',this.w_CODFAM1)
            select FRCODICE,FR_TABLE,FR__ZOOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAM1 = NVL(_Link_.FRCODICE,space(10))
      this.w_FR_TABLE = NVL(_Link_.FR_TABLE,space(30))
      this.w_FR__ZOOM = NVL(_Link_.FR__ZOOM,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAM1 = space(10)
      endif
      this.w_FR_TABLE = space(30)
      this.w_FR__ZOOM = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.FRCODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAM1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCADESCRI_2_8.value==this.w_CADESCRI)
      this.oPgFrm.Page1.oPag.oCADESCRI_2_8.value=this.w_CADESCRI
      replace t_CADESCRI with this.oPgFrm.Page1.oPag.oCADESCRI_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODGRU_2_5.value==this.w_CACODGRU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODGRU_2_5.value=this.w_CACODGRU
      replace t_CACODGRU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODGRU_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODFAM_2_6.value==this.w_CACODFAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODFAM_2_6.value=this.w_CACODFAM
      replace t_CACODFAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODFAM_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALATT_2_7.value==this.w_CAVALATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALATT_2_7.value=this.w_CAVALATT
      replace t_CAVALATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALATT_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'CCOMDATT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_CPROWORD) and (not(Empty(.w_CACODMOD)) And not(Empty(.w_CACODGRU)) And not(Empty(.w_CACODFAM)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   not(chk_fam_gru( .w_CACODFAM, .w_CACODGRU )) and ((Empty(.w_CODATGEN) Or .oParentObject.w_SRV='A' ) And (.w_SRV='A'  Or .w_AUTOMA <> 'S')) and not(empty(.w_CACODFAM)) and (not(Empty(.w_CACODMOD)) And not(Empty(.w_CACODGRU)) And not(Empty(.w_CACODFAM)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODFAM_2_6
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_CACODMOD)) And not(Empty(.w_CACODGRU)) And not(Empty(.w_CACODFAM))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CACODMOD = this.w_CACODMOD
    this.o_CACODFAM = this.w_CACODFAM
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CACODMOD)) And not(Empty(t_CACODGRU)) And not(Empty(t_CACODFAM)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(999999,cp_maxroword()+10)
      .w_SRV=space(1)
      .w_CACODMOD=space(20)
      .w_GEST=space(10)
      .w_CACODGRU=space(10)
      .w_CACODFAM=space(10)
      .w_CAVALATT=space(20)
      .w_CADESCRI=space(50)
      .w_AUTOMA=space(1)
      .w_CONTA=0
      .w_CODFAM1=space(10)
      .w_FR_TABLE=space(30)
      .w_FR__ZOOM=space(254)
      .w_TIPO=space(1)
      .w_CODFAM=space(10)
      .DoRTCalc(1,3,.f.)
        .w_CACODMOD = get_GPFA_code("MODELLO")
      .DoRTCalc(5,5,.f.)
        .w_GEST = .w_CACODMOD
        .w_CACODGRU = get_GPFA_code("GRUPPO")
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_CACODGRU))
        .link_2_5('Full')
      endif
      .DoRTCalc(8,8,.f.)
      if not(empty(.w_CACODFAM))
        .link_2_6('Full')
      endif
        .w_CAVALATT = ''
      .DoRTCalc(10,10,.f.)
        .w_AUTOMA = "N"
      .DoRTCalc(12,12,.f.)
        .w_CONTA = iif(Empty(.w_CAVALATT),0,1)
      .DoRTCalc(14,15,.f.)
        .w_CODFAM1 = .w_CACODFAM
      .DoRTCalc(16,16,.f.)
      if not(empty(.w_CODFAM1))
        .link_2_12('Full')
      endif
      .DoRTCalc(17,19,.f.)
        .w_CODFAM = .w_CACODFAM
    endwith
    this.DoRTCalc(21,21,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_SRV = t_SRV
    this.w_CACODMOD = t_CACODMOD
    this.w_GEST = t_GEST
    this.w_CACODGRU = t_CACODGRU
    this.w_CACODFAM = t_CACODFAM
    this.w_CAVALATT = t_CAVALATT
    this.w_CADESCRI = t_CADESCRI
    this.w_AUTOMA = t_AUTOMA
    this.w_CONTA = t_CONTA
    this.w_CODFAM1 = t_CODFAM1
    this.w_FR_TABLE = t_FR_TABLE
    this.w_FR__ZOOM = t_FR__ZOOM
    this.w_TIPO = t_TIPO
    this.w_CODFAM = t_CODFAM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_SRV with this.w_SRV
    replace t_CACODMOD with this.w_CACODMOD
    replace t_GEST with this.w_GEST
    replace t_CACODGRU with this.w_CACODGRU
    replace t_CACODFAM with this.w_CACODFAM
    replace t_CAVALATT with this.w_CAVALATT
    replace t_CADESCRI with this.w_CADESCRI
    replace t_AUTOMA with this.w_AUTOMA
    replace t_CONTA with this.w_CONTA
    replace t_CODFAM1 with this.w_CODFAM1
    replace t_FR_TABLE with this.w_FR_TABLE
    replace t_FR__ZOOM with this.w_FR__ZOOM
    replace t_TIPO with this.w_TIPO
    replace t_CODFAM with this.w_CODFAM
    if i_srv='A'
      replace CACODMOD with this.w_CACODMOD
      replace CACODGRU with this.w_CACODGRU
      replace CACODFAM with this.w_CACODFAM
      replace CAVALATT with this.w_CAVALATT
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTATTRIB = .w_TOTATTRIB-.w_conta
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsgp_mdlPag1 as StdContainer
  Width  = 493
  height = 188
  stdWidth  = 493
  stdheight = 188
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=2, top=2, width=469,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=4,Field1="CPROWORD",Label1="Riga",Field2="CACODGRU",Label2="",Field3="CACODFAM",Label3="Famiglia",Field4="CAVALATT",Label4="Valore attributo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 32663430

  add object oStr_1_3 as StdString with uid="AJNMBKWFXU",Visible=.t., Left=69, Top=143,;
    Alignment=1, Width=90, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  func oStr_1_3.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-6,top=23,;
    width=469+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-5,top=24,width=468+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='GRU_ATTR|FAM_ATTR|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCADESCRI_2_8.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='GRU_ATTR'
        oDropInto=this.oBodyCol.oRow.oCACODGRU_2_5
      case cFile='FAM_ATTR'
        oDropInto=this.oBodyCol.oRow.oCACODFAM_2_6
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oCADESCRI_2_8 as StdTrsField with uid="VKUTIFLUFP",rtseq=10,rtrep=.t.,;
    cFormVar="w_CADESCRI",value=space(50),enabled=.f.,;
    HelpContextID = 206572433,;
    cTotal="", bFixedPos=.t., cQueryName = "CADESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=305, Left=164, Top=141, InputMask=replicate('X',50)

  func oCADESCRI_2_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.T.)
    endwith
    endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oBtn_3_2 as StdButton with uid="IAVGMLCBNY",width=48,height=45,;
   left=9, top=140,;
    CpPicture="BMP\VISUALIZ.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per visualizzare gli articoli che soddisfano la selezione";
    , HelpContextID = 257808362;
    , Caption='V\<isualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_2.Click()
      with this.Parent.oContained
        GSGP_BCQ(this.Parent.oContained,"VIAR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

* --- Defining Body row
define class tgsgp_mdlBodyRow as CPBodyRowCnt
  Width=459
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="NDZJTLQCYR",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 334998,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=-2, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oCACODGRU_2_5 as StdTrsField with uid="HFPYRNFAKL",rtseq=7,rtrep=.t.,;
    cFormVar="w_CACODGRU",value=space(10),isprimarykey=.t.,;
    HelpContextID = 154540933,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=16, Left=42, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="GRU_ATTR", oKey_1_1="GRCODICE", oKey_1_2="this.w_CACODGRU"

  func oCACODGRU_2_5.mCond()
    with this.Parent.oContained
      return (.F.)
    endwith
  endfunc

  func oCACODGRU_2_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.T.)
    endwith
    endif
  endfunc

  func oCACODGRU_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODGRU_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCACODGRU_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oCACODGRU_2_5.readonly and this.parent.oCACODGRU_2_5.isprimarykey)
    do cp_zoom with 'GRU_ATTR','*','GRCODICE',cp_AbsName(this.parent,'oCACODGRU_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi attributi",'',this.parent.oContained
   endif
  endproc

  add object oCACODFAM_2_6 as StdTrsField with uid="LSXLODPEFN",rtseq=8,rtrep=.t.,;
    cFormVar="w_CACODFAM",value=space(10),isprimarykey=.t.,;
    HelpContextID = 171318157,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=149, Left=42, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="FAM_ATTR", oKey_1_1="FRCODICE", oKey_1_2="this.w_CACODFAM"

  func oCACODFAM_2_6.mCond()
    with this.Parent.oContained
      return ((Empty(.w_CODATGEN) Or .oParentObject.w_SRV='A' ) And (.w_SRV='A'  Or .w_AUTOMA <> 'S'))
    endwith
  endfunc

  func oCACODFAM_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODFAM_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCACODFAM_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oCACODFAM_2_6.readonly and this.parent.oCACODFAM_2_6.isprimarykey)
    do cp_zoom with 'FAM_ATTR','*','FRCODICE',cp_AbsName(this.parent,'oCACODFAM_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie attributi",'GSAR_AAT.FAM_ATTR_VZM',this.parent.oContained
   endif
  endproc

  add object oCAVALATT_2_7 as StdTrsField with uid="RWWCXPJNYH",rtseq=9,rtrep=.t.,;
    cFormVar="w_CAVALATT",value=space(20),isprimarykey=.t.,;
    HelpContextID = 247655302,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=260, Left=194, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , ReadOnly=.t.

  func oCAVALATT_2_7.mCond()
    with this.Parent.oContained
      return ((Empty(.w_CODATGEN) Or .oParentObject.w_SRV='A' ) And (.w_SRV='A'  Or .w_AUTOMA <> 'S'))
    endwith
  endfunc

  proc oCAVALATT_2_7.mZoom
      with this.Parent.oContained
        GSAR_BMG(this.Parent.oContained,"ZAINS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsgp_mdl','CCOMDATT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CA__GUID=CCOMDATT.CA__GUID";
  +" and "+i_cAliasName2+".CAROWRIF=CCOMDATT.CAROWRIF";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsgp_mdl
* --- Class definition
define class tgsgp_mdl as StdPCForm
  Width  = 497
  Height = 188
  Top    = 10
  Left   = 10
  cComment = "attributi articolo"
  HelpContextID=188062359
  add object cnt as tcgsgp_mdl
  proc ecpQuit()
        if !this.cnt.bF10
          *if this.IsAChildUpdated()
            *this.cnt.bUpdated=!cp_YesNo(MSG_DISCARD_CHANGES_RECOVERY_DB_QP)
            * Zucchetti Aulla - Fine - Premendo Esc su figli da bottone e rispondendo Abbandoni
            * le modifiche al successivo rientro l'applicazione visualizza i dati abbandonati
            * Se abbandono le modifiche mi preparo per ricaricare i dati
            * dal database alla prossima apertura
            * if Not this.cnt.bUpdated
            * this.cnt.nDeferredFillRec=1
            * Endif
            * Zucchetti Aulla - Fine
          *endif
          if !this.cnt.bUpdated
            this.cnt.bDontReportError=.t.
            this.TerminateEdit()
            this.cnt.bUpdated=.f.
          endif
        endif
        *this.cnt.bUpdated=iif(this.cnt.bF10,this.cnt.bUpdated,.f.)
        this.LinkPCClick()
    Endproc

enddefine
* --- Fine Area Manuale
