* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsgp_mpc                                                        *
*              Premi contratto                                                 *
*                                                                              *
*      Author: Zucchetti - AT                                                  *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-27                                                      *
* Last revis.: 2012-10-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsgp_mpc")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsgp_mpc")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsgp_mpc")
  return

* --- Class definition
define class tgsgp_mpc as StdPCForm
  Width  = 862
  Height = 300
  Top    = 3
  Left   = 1
  cComment = "Premi contratto"
  cPrg = "gsgp_mpc"
  HelpContextID=147427689
  add object cnt as tcgsgp_mpc
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsgp_mpc as PCContext
  w_CCCODICE = space(15)
  w_PCROWNUM = 0
  w_CPROWORD = 0
  w_PCQTAPRM = 0
  w_PCVALPRM = 0
  w_PCSCAPRM = 0
  w_PCPERSCO = 0
  w_PCIMPSCO = 0
  w_PCSCOSCA = 0
  w_PCCODRIC = space(20)
  w_PCUNMART = space(3)
  w_PCQTASCO = 0
  w_PCCODART = space(20)
  w_PCCODVAR = space(20)
  w_PCKEYSAL = space(20)
  w_PCFLGSCO = space(1)
  w_PCPUNPEZ = 0
  w_PCPUNSCA = 0
  w_PCQTACON = 0
  w_PCVALCON = 0
  w_PCVALPRE = 0
  w_PCQTAPRE = 0
  w_PCPUNPRE = 0
  w_CALPRM = space(1)
  w_CICLO = space(1)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_MOLTIP = 0
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_OBTEST = space(8)
  w_CA__TIPO = space(1)
  proc Save(i_oFrom)
    this.w_CCCODICE = i_oFrom.w_CCCODICE
    this.w_PCROWNUM = i_oFrom.w_PCROWNUM
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_PCQTAPRM = i_oFrom.w_PCQTAPRM
    this.w_PCVALPRM = i_oFrom.w_PCVALPRM
    this.w_PCSCAPRM = i_oFrom.w_PCSCAPRM
    this.w_PCPERSCO = i_oFrom.w_PCPERSCO
    this.w_PCIMPSCO = i_oFrom.w_PCIMPSCO
    this.w_PCSCOSCA = i_oFrom.w_PCSCOSCA
    this.w_PCCODRIC = i_oFrom.w_PCCODRIC
    this.w_PCUNMART = i_oFrom.w_PCUNMART
    this.w_PCQTASCO = i_oFrom.w_PCQTASCO
    this.w_PCCODART = i_oFrom.w_PCCODART
    this.w_PCCODVAR = i_oFrom.w_PCCODVAR
    this.w_PCKEYSAL = i_oFrom.w_PCKEYSAL
    this.w_PCFLGSCO = i_oFrom.w_PCFLGSCO
    this.w_PCPUNPEZ = i_oFrom.w_PCPUNPEZ
    this.w_PCPUNSCA = i_oFrom.w_PCPUNSCA
    this.w_PCQTACON = i_oFrom.w_PCQTACON
    this.w_PCVALCON = i_oFrom.w_PCVALCON
    this.w_PCVALPRE = i_oFrom.w_PCVALPRE
    this.w_PCQTAPRE = i_oFrom.w_PCQTAPRE
    this.w_PCPUNPRE = i_oFrom.w_PCPUNPRE
    this.w_CALPRM = i_oFrom.w_CALPRM
    this.w_CICLO = i_oFrom.w_CICLO
    this.w_UNMIS1 = i_oFrom.w_UNMIS1
    this.w_UNMIS2 = i_oFrom.w_UNMIS2
    this.w_UNMIS3 = i_oFrom.w_UNMIS3
    this.w_MOLTIP = i_oFrom.w_MOLTIP
    this.w_OPERAT = i_oFrom.w_OPERAT
    this.w_OPERA3 = i_oFrom.w_OPERA3
    this.w_MOLTI3 = i_oFrom.w_MOLTI3
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_CA__TIPO = i_oFrom.w_CA__TIPO
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CCCODICE = this.w_CCCODICE
    i_oTo.w_PCROWNUM = this.w_PCROWNUM
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_PCQTAPRM = this.w_PCQTAPRM
    i_oTo.w_PCVALPRM = this.w_PCVALPRM
    i_oTo.w_PCSCAPRM = this.w_PCSCAPRM
    i_oTo.w_PCPERSCO = this.w_PCPERSCO
    i_oTo.w_PCIMPSCO = this.w_PCIMPSCO
    i_oTo.w_PCSCOSCA = this.w_PCSCOSCA
    i_oTo.w_PCCODRIC = this.w_PCCODRIC
    i_oTo.w_PCUNMART = this.w_PCUNMART
    i_oTo.w_PCQTASCO = this.w_PCQTASCO
    i_oTo.w_PCCODART = this.w_PCCODART
    i_oTo.w_PCCODVAR = this.w_PCCODVAR
    i_oTo.w_PCKEYSAL = this.w_PCKEYSAL
    i_oTo.w_PCFLGSCO = this.w_PCFLGSCO
    i_oTo.w_PCPUNPEZ = this.w_PCPUNPEZ
    i_oTo.w_PCPUNSCA = this.w_PCPUNSCA
    i_oTo.w_PCQTACON = this.w_PCQTACON
    i_oTo.w_PCVALCON = this.w_PCVALCON
    i_oTo.w_PCVALPRE = this.w_PCVALPRE
    i_oTo.w_PCQTAPRE = this.w_PCQTAPRE
    i_oTo.w_PCPUNPRE = this.w_PCPUNPRE
    i_oTo.w_CALPRM = this.w_CALPRM
    i_oTo.w_CICLO = this.w_CICLO
    i_oTo.w_UNMIS1 = this.w_UNMIS1
    i_oTo.w_UNMIS2 = this.w_UNMIS2
    i_oTo.w_UNMIS3 = this.w_UNMIS3
    i_oTo.w_MOLTIP = this.w_MOLTIP
    i_oTo.w_OPERAT = this.w_OPERAT
    i_oTo.w_OPERA3 = this.w_OPERA3
    i_oTo.w_MOLTI3 = this.w_MOLTI3
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_CA__TIPO = this.w_CA__TIPO
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsgp_mpc as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 862
  Height = 300
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-19"
  HelpContextID=147427689
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=34

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PREMICON_IDX = 0
  KEY_ARTI_IDX = 0
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  cFile = "PREMICON"
  cKeySelect = "CCCODICE,PCROWNUM"
  cKeyWhere  = "CCCODICE=this.w_CCCODICE and PCROWNUM=this.w_PCROWNUM"
  cKeyDetail  = "CCCODICE=this.w_CCCODICE and PCROWNUM=this.w_PCROWNUM"
  cKeyWhereODBC = '"CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';
      +'+" and PCROWNUM="+cp_ToStrODBC(this.w_PCROWNUM)';

  cKeyDetailWhereODBC = '"CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';
      +'+" and PCROWNUM="+cp_ToStrODBC(this.w_PCROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"PREMICON.CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';
      +'+" and PREMICON.PCROWNUM="+cp_ToStrODBC(this.w_PCROWNUM)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PREMICON.CPROWORD '
  cPrg = "gsgp_mpc"
  cComment = "Premi contratto"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CCCODICE = space(15)
  w_PCROWNUM = 0
  w_CPROWORD = 0
  w_PCQTAPRM = 0
  o_PCQTAPRM = 0
  w_PCVALPRM = 0
  o_PCVALPRM = 0
  w_PCSCAPRM = 0
  w_PCPERSCO = 0
  w_PCIMPSCO = 0
  w_PCSCOSCA = 0
  w_PCCODRIC = space(20)
  o_PCCODRIC = space(20)
  w_PCUNMART = space(3)
  w_PCQTASCO = 0
  w_PCCODART = space(20)
  w_PCCODVAR = space(20)
  w_PCKEYSAL = space(20)
  w_PCFLGSCO = space(1)
  w_PCPUNPEZ = 0
  w_PCPUNSCA = 0
  w_PCQTACON = 0
  w_PCVALCON = 0
  w_PCVALPRE = 0
  w_PCQTAPRE = 0
  w_PCPUNPRE = 0
  w_CALPRM = space(1)
  w_CICLO = space(1)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_MOLTIP = 0
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_MOLTI3 = 0
  w_OBTEST = ctod('  /  /  ')
  w_CA__TIPO = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsgp_mpcPag1","gsgp_mpc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='UNIMIS'
    this.cWorkTables[4]='PREMICON'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PREMICON_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PREMICON_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsgp_mpc'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_8_joined
    link_2_8_joined=.f.
    local link_2_11_joined
    link_2_11_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PREMICON where CCCODICE=KeySet.CCCODICE
    *                            and PCROWNUM=KeySet.PCROWNUM
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsgp_mpc
      * --- Setta Ordine per Quantita'
      i_cOrder = 'order by PCQTAPRM,PCVALPRM '
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PREMICON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PREMICON_IDX,2],this.bLoadRecFilter,this.PREMICON_IDX,"gsgp_mpc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PREMICON')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PREMICON.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PREMICON '
      link_2_8_joined=this.AddJoinedLink_2_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_11_joined=this.AddJoinedLink_2_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  ,'PCROWNUM',this.w_PCROWNUM  )
      select * from (i_cTable) PREMICON where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = ctod("  /  /  ")
        .w_CA__TIPO = space(1)
        .w_CCCODICE = NVL(CCCODICE,space(15))
        .w_PCROWNUM = NVL(PCROWNUM,0)
        .w_CALPRM = this.oParentObject.w_COCALPRM
        .w_CICLO = this.oParentObject.w_COFLGCIC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'PREMICON')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_UNMIS1 = space(3)
          .w_UNMIS2 = space(3)
          .w_UNMIS3 = space(3)
          .w_MOLTIP = 0
          .w_OPERAT = space(1)
          .w_OPERA3 = space(1)
          .w_MOLTI3 = 0
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_PCQTAPRM = NVL(PCQTAPRM,0)
          .w_PCVALPRM = NVL(PCVALPRM,0)
          .w_PCSCAPRM = NVL(PCSCAPRM,0)
          .w_PCPERSCO = NVL(PCPERSCO,0)
          .w_PCIMPSCO = NVL(PCIMPSCO,0)
          .w_PCSCOSCA = NVL(PCSCOSCA,0)
          .w_PCCODRIC = NVL(PCCODRIC,space(20))
          if link_2_8_joined
            this.w_PCCODRIC = NVL(CACODICE208,NVL(this.w_PCCODRIC,space(20)))
            this.w_PCCODART = NVL(CACODART208,space(20))
            this.w_PCKEYSAL = NVL(CACODART208,space(20))
            this.w_UNMIS3 = NVL(CAUNIMIS208,space(3))
            this.w_OPERA3 = NVL(CAOPERAT208,space(1))
            this.w_MOLTI3 = NVL(CAMOLTIP208,0)
            this.w_CA__TIPO = NVL(CA__TIPO208,space(1))
          else
          .link_2_8('Load')
          endif
          .w_PCUNMART = NVL(PCUNMART,space(3))
          * evitabile
          *.link_2_9('Load')
          .w_PCQTASCO = NVL(PCQTASCO,0)
          .w_PCCODART = NVL(PCCODART,space(20))
          if link_2_11_joined
            this.w_PCCODART = NVL(ARCODART211,NVL(this.w_PCCODART,space(20)))
            this.w_UNMIS1 = NVL(ARUNMIS1211,space(3))
            this.w_MOLTIP = NVL(ARMOLTIP211,0)
            this.w_UNMIS2 = NVL(ARUNMIS2211,space(3))
            this.w_OPERAT = NVL(AROPERAT211,space(1))
          else
          .link_2_11('Load')
          endif
          .w_PCCODVAR = NVL(PCCODVAR,space(20))
          .w_PCKEYSAL = NVL(PCKEYSAL,space(20))
          .w_PCFLGSCO = NVL(PCFLGSCO,space(1))
          .w_PCPUNPEZ = NVL(PCPUNPEZ,0)
          .w_PCPUNSCA = NVL(PCPUNSCA,0)
          .w_PCQTACON = NVL(PCQTACON,0)
          .w_PCVALCON = NVL(PCVALCON,0)
          .w_PCVALPRE = NVL(PCVALPRE,0)
          .w_PCQTAPRE = NVL(PCQTAPRE,0)
          .w_PCPUNPRE = NVL(PCPUNPRE,0)
        .oPgFrm.Page1.oPag.oObj_2_39.Calculate(IIF(.w_CICLO="V", AH_MsgFormat("Pezzi venduti:"), AH_MsgFormat("Pezzi acquistati:")))
        .oPgFrm.Page1.oPag.oObj_2_40.Calculate(IIF(.w_CICLO="V", AH_MsgFormat("Valore venduto:"), AH_MsgFormat("Valore acquistato:")))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_CALPRM = this.oParentObject.w_COCALPRM
        .w_CICLO = this.oParentObject.w_COFLGCIC
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_CCCODICE=space(15)
      .w_PCROWNUM=0
      .w_CPROWORD=10
      .w_PCQTAPRM=0
      .w_PCVALPRM=0
      .w_PCSCAPRM=0
      .w_PCPERSCO=0
      .w_PCIMPSCO=0
      .w_PCSCOSCA=0
      .w_PCCODRIC=space(20)
      .w_PCUNMART=space(3)
      .w_PCQTASCO=0
      .w_PCCODART=space(20)
      .w_PCCODVAR=space(20)
      .w_PCKEYSAL=space(20)
      .w_PCFLGSCO=space(1)
      .w_PCPUNPEZ=0
      .w_PCPUNSCA=0
      .w_PCQTACON=0
      .w_PCVALCON=0
      .w_PCVALPRE=0
      .w_PCQTAPRE=0
      .w_PCPUNPRE=0
      .w_CALPRM=space(1)
      .w_CICLO=space(1)
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_UNMIS3=space(3)
      .w_MOLTIP=0
      .w_OPERAT=space(1)
      .w_OPERA3=space(1)
      .w_MOLTI3=0
      .w_OBTEST=ctod("  /  /  ")
      .w_CA__TIPO=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,5,.f.)
        .w_PCSCAPRM = iif(.w_CALPRM='Q',.w_PCQTAPRM, .w_PCVALPRM)
        .DoRTCalc(7,10,.f.)
        if not(empty(.w_PCCODRIC))
         .link_2_8('Full')
        endif
        .w_PCUNMART = iif(! EMPTY(nvl(.w_PCCODRIC,'')), IIF(NOT EMPTY(.w_UNMIS3), .w_UNMIS3, .w_UNMIS1), '')
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_PCUNMART))
         .link_2_9('Full')
        endif
        .DoRTCalc(12,13,.f.)
        if not(empty(.w_PCCODART))
         .link_2_11('Full')
        endif
        .DoRTCalc(14,15,.f.)
        .w_PCFLGSCO = 'S'
        .DoRTCalc(17,23,.f.)
        .w_CALPRM = this.oParentObject.w_COCALPRM
        .w_CICLO = this.oParentObject.w_COFLGCIC
        .oPgFrm.Page1.oPag.oObj_2_39.Calculate(IIF(.w_CICLO="V", AH_MsgFormat("Pezzi venduti:"), AH_MsgFormat("Pezzi acquistati:")))
        .oPgFrm.Page1.oPag.oObj_2_40.Calculate(IIF(.w_CICLO="V", AH_MsgFormat("Valore venduto:"), AH_MsgFormat("Valore acquistato:")))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PREMICON')
    this.DoRTCalc(26,34,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oPCFLGSCO_2_14.enabled = i_bVal
      .Page1.oPag.oPCPUNPEZ_2_15.enabled = i_bVal
      .Page1.oPag.oPCPUNSCA_2_16.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'PREMICON',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PREMICON_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCODICE,"CCCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PCROWNUM,"PCROWNUM",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(6);
      ,t_PCQTAPRM N(18,5);
      ,t_PCVALPRM N(18,5);
      ,t_PCPERSCO N(6,3);
      ,t_PCIMPSCO N(18,5);
      ,t_PCSCOSCA N(18,5);
      ,t_PCCODRIC C(20);
      ,t_PCUNMART C(3);
      ,t_PCQTASCO N(12,4);
      ,t_PCFLGSCO N(3);
      ,t_PCPUNPEZ N(10);
      ,t_PCPUNSCA N(10);
      ,t_PCQTACON N(12,4);
      ,t_PCVALCON N(18,5);
      ,t_PCVALPRE N(18,5);
      ,t_PCQTAPRE N(12,4);
      ,t_PCPUNPRE N(10);
      ,CPROWNUM N(10);
      ,t_PCSCAPRM N(18,5);
      ,t_PCCODART C(20);
      ,t_PCCODVAR C(20);
      ,t_PCKEYSAL C(20);
      ,t_UNMIS1 C(3);
      ,t_UNMIS2 C(3);
      ,t_UNMIS3 C(3);
      ,t_MOLTIP N(10,5);
      ,t_OPERAT C(1);
      ,t_OPERA3 C(1);
      ,t_MOLTI3 N(10,4);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsgp_mpcbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPCQTAPRM_2_2.controlsource=this.cTrsName+'.t_PCQTAPRM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPCVALPRM_2_3.controlsource=this.cTrsName+'.t_PCVALPRM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPCPERSCO_2_5.controlsource=this.cTrsName+'.t_PCPERSCO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPCIMPSCO_2_6.controlsource=this.cTrsName+'.t_PCIMPSCO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPCSCOSCA_2_7.controlsource=this.cTrsName+'.t_PCSCOSCA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPCCODRIC_2_8.controlsource=this.cTrsName+'.t_PCCODRIC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPCUNMART_2_9.controlsource=this.cTrsName+'.t_PCUNMART'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPCQTASCO_2_10.controlsource=this.cTrsName+'.t_PCQTASCO'
    this.oPgFRm.Page1.oPag.oPCFLGSCO_2_14.controlsource=this.cTrsName+'.t_PCFLGSCO'
    this.oPgFRm.Page1.oPag.oPCPUNPEZ_2_15.controlsource=this.cTrsName+'.t_PCPUNPEZ'
    this.oPgFRm.Page1.oPag.oPCPUNSCA_2_16.controlsource=this.cTrsName+'.t_PCPUNSCA'
    this.oPgFRm.Page1.oPag.oPCQTACON_2_17.controlsource=this.cTrsName+'.t_PCQTACON'
    this.oPgFRm.Page1.oPag.oPCVALCON_2_18.controlsource=this.cTrsName+'.t_PCVALCON'
    this.oPgFRm.Page1.oPag.oPCVALPRE_2_19.controlsource=this.cTrsName+'.t_PCVALPRE'
    this.oPgFRm.Page1.oPag.oPCQTAPRE_2_20.controlsource=this.cTrsName+'.t_PCQTAPRE'
    this.oPgFRm.Page1.oPag.oPCPUNPRE_2_21.controlsource=this.cTrsName+'.t_PCPUNPRE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(43)
    this.AddVLine(168)
    this.AddVLine(219)
    this.AddVLine(366)
    this.AddVLine(505)
    this.AddVLine(661)
    this.AddVLine(711)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PREMICON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PREMICON_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PREMICON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PREMICON_IDX,2])
      *
      * insert into PREMICON
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PREMICON')
        i_extval=cp_InsertValODBCExtFlds(this,'PREMICON')
        i_cFldBody=" "+;
                  "(CCCODICE,PCROWNUM,CPROWORD,PCQTAPRM,PCVALPRM"+;
                  ",PCSCAPRM,PCPERSCO,PCIMPSCO,PCSCOSCA,PCCODRIC"+;
                  ",PCUNMART,PCQTASCO,PCCODART,PCCODVAR,PCKEYSAL"+;
                  ",PCFLGSCO,PCPUNPEZ,PCPUNSCA,PCQTACON,PCVALCON"+;
                  ",PCVALPRE,PCQTAPRE,PCPUNPRE,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CCCODICE)+","+cp_ToStrODBC(this.w_PCROWNUM)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_PCQTAPRM)+","+cp_ToStrODBC(this.w_PCVALPRM)+;
             ","+cp_ToStrODBC(this.w_PCSCAPRM)+","+cp_ToStrODBC(this.w_PCPERSCO)+","+cp_ToStrODBC(this.w_PCIMPSCO)+","+cp_ToStrODBC(this.w_PCSCOSCA)+","+cp_ToStrODBCNull(this.w_PCCODRIC)+;
             ","+cp_ToStrODBCNull(this.w_PCUNMART)+","+cp_ToStrODBC(this.w_PCQTASCO)+","+cp_ToStrODBCNull(this.w_PCCODART)+","+cp_ToStrODBC(this.w_PCCODVAR)+","+cp_ToStrODBC(this.w_PCKEYSAL)+;
             ","+cp_ToStrODBC(this.w_PCFLGSCO)+","+cp_ToStrODBC(this.w_PCPUNPEZ)+","+cp_ToStrODBC(this.w_PCPUNSCA)+","+cp_ToStrODBC(this.w_PCQTACON)+","+cp_ToStrODBC(this.w_PCVALCON)+;
             ","+cp_ToStrODBC(this.w_PCVALPRE)+","+cp_ToStrODBC(this.w_PCQTAPRE)+","+cp_ToStrODBC(this.w_PCPUNPRE)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PREMICON')
        i_extval=cp_InsertValVFPExtFlds(this,'PREMICON')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'CCCODICE',this.w_CCCODICE,'PCROWNUM',this.w_PCROWNUM)
        INSERT INTO (i_cTable) (;
                   CCCODICE;
                  ,PCROWNUM;
                  ,CPROWORD;
                  ,PCQTAPRM;
                  ,PCVALPRM;
                  ,PCSCAPRM;
                  ,PCPERSCO;
                  ,PCIMPSCO;
                  ,PCSCOSCA;
                  ,PCCODRIC;
                  ,PCUNMART;
                  ,PCQTASCO;
                  ,PCCODART;
                  ,PCCODVAR;
                  ,PCKEYSAL;
                  ,PCFLGSCO;
                  ,PCPUNPEZ;
                  ,PCPUNSCA;
                  ,PCQTACON;
                  ,PCVALCON;
                  ,PCVALPRE;
                  ,PCQTAPRE;
                  ,PCPUNPRE;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CCCODICE;
                  ,this.w_PCROWNUM;
                  ,this.w_CPROWORD;
                  ,this.w_PCQTAPRM;
                  ,this.w_PCVALPRM;
                  ,this.w_PCSCAPRM;
                  ,this.w_PCPERSCO;
                  ,this.w_PCIMPSCO;
                  ,this.w_PCSCOSCA;
                  ,this.w_PCCODRIC;
                  ,this.w_PCUNMART;
                  ,this.w_PCQTASCO;
                  ,this.w_PCCODART;
                  ,this.w_PCCODVAR;
                  ,this.w_PCKEYSAL;
                  ,this.w_PCFLGSCO;
                  ,this.w_PCPUNPEZ;
                  ,this.w_PCPUNSCA;
                  ,this.w_PCQTACON;
                  ,this.w_PCVALCON;
                  ,this.w_PCVALPRE;
                  ,this.w_PCQTAPRE;
                  ,this.w_PCPUNPRE;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.PREMICON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PREMICON_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_PCSCAPRM))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PREMICON')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PREMICON')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_PCSCAPRM))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PREMICON
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PREMICON')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",PCQTAPRM="+cp_ToStrODBC(this.w_PCQTAPRM)+;
                     ",PCVALPRM="+cp_ToStrODBC(this.w_PCVALPRM)+;
                     ",PCSCAPRM="+cp_ToStrODBC(this.w_PCSCAPRM)+;
                     ",PCPERSCO="+cp_ToStrODBC(this.w_PCPERSCO)+;
                     ",PCIMPSCO="+cp_ToStrODBC(this.w_PCIMPSCO)+;
                     ",PCSCOSCA="+cp_ToStrODBC(this.w_PCSCOSCA)+;
                     ",PCCODRIC="+cp_ToStrODBCNull(this.w_PCCODRIC)+;
                     ",PCUNMART="+cp_ToStrODBCNull(this.w_PCUNMART)+;
                     ",PCQTASCO="+cp_ToStrODBC(this.w_PCQTASCO)+;
                     ",PCCODART="+cp_ToStrODBCNull(this.w_PCCODART)+;
                     ",PCCODVAR="+cp_ToStrODBC(this.w_PCCODVAR)+;
                     ",PCKEYSAL="+cp_ToStrODBC(this.w_PCKEYSAL)+;
                     ",PCFLGSCO="+cp_ToStrODBC(this.w_PCFLGSCO)+;
                     ",PCPUNPEZ="+cp_ToStrODBC(this.w_PCPUNPEZ)+;
                     ",PCPUNSCA="+cp_ToStrODBC(this.w_PCPUNSCA)+;
                     ",PCQTACON="+cp_ToStrODBC(this.w_PCQTACON)+;
                     ",PCVALCON="+cp_ToStrODBC(this.w_PCVALCON)+;
                     ",PCVALPRE="+cp_ToStrODBC(this.w_PCVALPRE)+;
                     ",PCQTAPRE="+cp_ToStrODBC(this.w_PCQTAPRE)+;
                     ",PCPUNPRE="+cp_ToStrODBC(this.w_PCPUNPRE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PREMICON')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,PCQTAPRM=this.w_PCQTAPRM;
                     ,PCVALPRM=this.w_PCVALPRM;
                     ,PCSCAPRM=this.w_PCSCAPRM;
                     ,PCPERSCO=this.w_PCPERSCO;
                     ,PCIMPSCO=this.w_PCIMPSCO;
                     ,PCSCOSCA=this.w_PCSCOSCA;
                     ,PCCODRIC=this.w_PCCODRIC;
                     ,PCUNMART=this.w_PCUNMART;
                     ,PCQTASCO=this.w_PCQTASCO;
                     ,PCCODART=this.w_PCCODART;
                     ,PCCODVAR=this.w_PCCODVAR;
                     ,PCKEYSAL=this.w_PCKEYSAL;
                     ,PCFLGSCO=this.w_PCFLGSCO;
                     ,PCPUNPEZ=this.w_PCPUNPEZ;
                     ,PCPUNSCA=this.w_PCPUNSCA;
                     ,PCQTACON=this.w_PCQTACON;
                     ,PCVALCON=this.w_PCVALCON;
                     ,PCVALPRE=this.w_PCVALPRE;
                     ,PCQTAPRE=this.w_PCQTAPRE;
                     ,PCPUNPRE=this.w_PCPUNPRE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PREMICON_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PREMICON_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_PCSCAPRM))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PREMICON
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_PCSCAPRM))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PREMICON_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PREMICON_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
        if .o_PCQTAPRM<>.w_PCQTAPRM.or. .o_PCVALPRM<>.w_PCVALPRM
          .w_PCSCAPRM = iif(.w_CALPRM='Q',.w_PCQTAPRM, .w_PCVALPRM)
        endif
        .DoRTCalc(7,10,.t.)
        if .o_PCCODRIC<>.w_PCCODRIC
          .w_PCUNMART = iif(! EMPTY(nvl(.w_PCCODRIC,'')), IIF(NOT EMPTY(.w_UNMIS3), .w_UNMIS3, .w_UNMIS1), '')
          .link_2_9('Full')
        endif
        .DoRTCalc(12,12,.t.)
          .link_2_11('Full')
        .DoRTCalc(14,23,.t.)
          .w_CALPRM = this.oParentObject.w_COCALPRM
          .w_CICLO = this.oParentObject.w_COFLGCIC
        .oPgFrm.Page1.oPag.oObj_2_39.Calculate(IIF(.w_CICLO="V", AH_MsgFormat("Pezzi venduti:"), AH_MsgFormat("Pezzi acquistati:")))
        .oPgFrm.Page1.oPag.oObj_2_40.Calculate(IIF(.w_CICLO="V", AH_MsgFormat("Valore venduto:"), AH_MsgFormat("Valore acquistato:")))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(26,34,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_PCSCAPRM with this.w_PCSCAPRM
      replace t_PCCODART with this.w_PCCODART
      replace t_PCCODVAR with this.w_PCCODVAR
      replace t_PCKEYSAL with this.w_PCKEYSAL
      replace t_UNMIS1 with this.w_UNMIS1
      replace t_UNMIS2 with this.w_UNMIS2
      replace t_UNMIS3 with this.w_UNMIS3
      replace t_MOLTIP with this.w_MOLTIP
      replace t_OPERAT with this.w_OPERAT
      replace t_OPERA3 with this.w_OPERA3
      replace t_MOLTI3 with this.w_MOLTI3
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_39.Calculate(IIF(.w_CICLO="V", AH_MsgFormat("Pezzi venduti:"), AH_MsgFormat("Pezzi acquistati:")))
        .oPgFrm.Page1.oPag.oObj_2_40.Calculate(IIF(.w_CICLO="V", AH_MsgFormat("Valore venduto:"), AH_MsgFormat("Valore acquistato:")))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_39.Calculate(IIF(.w_CICLO="V", AH_MsgFormat("Pezzi venduti:"), AH_MsgFormat("Pezzi acquistati:")))
        .oPgFrm.Page1.oPag.oObj_2_40.Calculate(IIF(.w_CICLO="V", AH_MsgFormat("Valore venduto:"), AH_MsgFormat("Valore acquistato:")))
    endwith
  return
  proc Calculate_FKKRTURDLS()
    with this
          * --- Gsgp_bcq (scag)
          GSGP_BCQ(this;
              ,'SCAG';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPCQTAPRM_2_2.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPCQTAPRM_2_2.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPCVALPRM_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPCVALPRM_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPCPERSCO_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPCPERSCO_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPCIMPSCO_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPCIMPSCO_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPCSCOSCA_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPCSCOSCA_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPCCODRIC_2_8.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPCCODRIC_2_8.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPCUNMART_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPCUNMART_2_9.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPCQTASCO_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPCQTASCO_2_10.mCond()
    this.oPgFrm.Page1.oPag.oPCPUNPEZ_2_15.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPCPUNPEZ_2_15.mCond()
    this.oPgFrm.Page1.oPag.oPCPUNSCA_2_16.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPCPUNSCA_2_16.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCICLO_1_4.visible=!this.oPgFrm.Page1.oPag.oCICLO_1_4.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCQTAPRM_2_2.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCQTAPRM_2_2.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCVALPRM_2_3.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCVALPRM_2_3.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_PCQTAPRM Changed") or lower(cEvent)==lower("w_PCVALPRM Changed")
          .Calculate_FKKRTURDLS()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_2_39.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_40.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PCCODRIC
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCCODRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_PCCODRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_PCCODRIC))
          select CACODICE,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCCODRIC)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PCCODRIC) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oPCCODRIC_2_8'),i_cWhere,'GSMA_BZA',"Chiavi articolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCCODRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_PCCODRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_PCCODRIC)
            select CACODICE,CACODART,CAUNIMIS,CAOPERAT,CAMOLTIP,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCCODRIC = NVL(_Link_.CACODICE,space(20))
      this.w_PCCODART = NVL(_Link_.CACODART,space(20))
      this.w_PCKEYSAL = NVL(_Link_.CACODART,space(20))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_OPERA3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_CA__TIPO = NVL(_Link_.CA__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PCCODRIC = space(20)
      endif
      this.w_PCCODART = space(20)
      this.w_PCKEYSAL = space(20)
      this.w_UNMIS3 = space(3)
      this.w_OPERA3 = space(1)
      this.w_MOLTI3 = 0
      this.w_CA__TIPO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!.w_CA__TIPO $'DA'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PCCODRIC = space(20)
        this.w_PCCODART = space(20)
        this.w_PCKEYSAL = space(20)
        this.w_UNMIS3 = space(3)
        this.w_OPERA3 = space(1)
        this.w_MOLTI3 = 0
        this.w_CA__TIPO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCCODRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_8.CACODICE as CACODICE208"+ ",link_2_8.CACODART as CACODART208"+ ",link_2_8.CACODART as CACODART208"+ ",link_2_8.CAUNIMIS as CAUNIMIS208"+ ",link_2_8.CAOPERAT as CAOPERAT208"+ ",link_2_8.CAMOLTIP as CAMOLTIP208"+ ",link_2_8.CA__TIPO as CA__TIPO208"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_8 on PREMICON.PCCODRIC=link_2_8.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_8"
          i_cKey=i_cKey+'+" and PREMICON.PCCODRIC=link_2_8.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PCUNMART
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCUNMART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_PCUNMART)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_PCUNMART))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCUNMART)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PCUNMART) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oPCUNMART_2_9'),i_cWhere,'GSAR_AUM',"Unit� di misura",'GSMAUMVM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCUNMART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_PCUNMART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_PCUNMART)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCUNMART = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_PCUNMART = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF(! EMPTY(nvl(.w_PCCODRIC,'')),CHKUNIMI(.w_PCUNMART, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3),.T.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_PCUNMART = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCUNMART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PCCODART
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARUNMIS1,ARMOLTIP,ARUNMIS2,AROPERAT";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_PCCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_PCCODART)
            select ARCODART,ARUNMIS1,ARMOLTIP,ARUNMIS2,AROPERAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCCODART = NVL(_Link_.ARCODART,space(20))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_PCCODART = space(20)
      endif
      this.w_UNMIS1 = space(3)
      this.w_MOLTIP = 0
      this.w_UNMIS2 = space(3)
      this.w_OPERAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_11.ARCODART as ARCODART211"+ ",link_2_11.ARUNMIS1 as ARUNMIS1211"+ ",link_2_11.ARMOLTIP as ARMOLTIP211"+ ",link_2_11.ARUNMIS2 as ARUNMIS2211"+ ",link_2_11.AROPERAT as AROPERAT211"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_11 on PREMICON.PCCODART=link_2_11.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_11"
          i_cKey=i_cKey+'+" and PREMICON.PCCODART=link_2_11.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPCFLGSCO_2_14.RadioValue()==this.w_PCFLGSCO)
      this.oPgFrm.Page1.oPag.oPCFLGSCO_2_14.SetRadio()
      replace t_PCFLGSCO with this.oPgFrm.Page1.oPag.oPCFLGSCO_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPCPUNPEZ_2_15.value==this.w_PCPUNPEZ)
      this.oPgFrm.Page1.oPag.oPCPUNPEZ_2_15.value=this.w_PCPUNPEZ
      replace t_PCPUNPEZ with this.oPgFrm.Page1.oPag.oPCPUNPEZ_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPCPUNSCA_2_16.value==this.w_PCPUNSCA)
      this.oPgFrm.Page1.oPag.oPCPUNSCA_2_16.value=this.w_PCPUNSCA
      replace t_PCPUNSCA with this.oPgFrm.Page1.oPag.oPCPUNSCA_2_16.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPCQTACON_2_17.value==this.w_PCQTACON)
      this.oPgFrm.Page1.oPag.oPCQTACON_2_17.value=this.w_PCQTACON
      replace t_PCQTACON with this.oPgFrm.Page1.oPag.oPCQTACON_2_17.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPCVALCON_2_18.value==this.w_PCVALCON)
      this.oPgFrm.Page1.oPag.oPCVALCON_2_18.value=this.w_PCVALCON
      replace t_PCVALCON with this.oPgFrm.Page1.oPag.oPCVALCON_2_18.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPCVALPRE_2_19.value==this.w_PCVALPRE)
      this.oPgFrm.Page1.oPag.oPCVALPRE_2_19.value=this.w_PCVALPRE
      replace t_PCVALPRE with this.oPgFrm.Page1.oPag.oPCVALPRE_2_19.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPCQTAPRE_2_20.value==this.w_PCQTAPRE)
      this.oPgFrm.Page1.oPag.oPCQTAPRE_2_20.value=this.w_PCQTAPRE
      replace t_PCQTAPRE with this.oPgFrm.Page1.oPag.oPCQTAPRE_2_20.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPCPUNPRE_2_21.value==this.w_PCPUNPRE)
      this.oPgFrm.Page1.oPag.oPCPUNPRE_2_21.value=this.w_PCPUNPRE
      replace t_PCPUNPRE with this.oPgFrm.Page1.oPag.oPCPUNPRE_2_21.value
    endif
    if not(this.oPgFrm.Page1.oPag.oCICLO_1_4.value==this.w_CICLO)
      this.oPgFrm.Page1.oPag.oCICLO_1_4.value=this.w_CICLO
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCQTAPRM_2_2.value==this.w_PCQTAPRM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCQTAPRM_2_2.value=this.w_PCQTAPRM
      replace t_PCQTAPRM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCQTAPRM_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCVALPRM_2_3.value==this.w_PCVALPRM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCVALPRM_2_3.value=this.w_PCVALPRM
      replace t_PCVALPRM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCVALPRM_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCPERSCO_2_5.value==this.w_PCPERSCO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCPERSCO_2_5.value=this.w_PCPERSCO
      replace t_PCPERSCO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCPERSCO_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCIMPSCO_2_6.value==this.w_PCIMPSCO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCIMPSCO_2_6.value=this.w_PCIMPSCO
      replace t_PCIMPSCO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCIMPSCO_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCSCOSCA_2_7.value==this.w_PCSCOSCA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCSCOSCA_2_7.value=this.w_PCSCOSCA
      replace t_PCSCOSCA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCSCOSCA_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCCODRIC_2_8.value==this.w_PCCODRIC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCCODRIC_2_8.value=this.w_PCCODRIC
      replace t_PCCODRIC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCCODRIC_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCUNMART_2_9.value==this.w_PCUNMART)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCUNMART_2_9.value=this.w_PCUNMART
      replace t_PCUNMART with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCUNMART_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCQTASCO_2_10.value==this.w_PCQTASCO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCQTASCO_2_10.value=this.w_PCQTASCO
      replace t_PCQTASCO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCQTASCO_2_10.value
    endif
    cp_SetControlsValueExtFlds(this,'PREMICON')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   (empty(.w_PCQTAPRM) or not(.w_PCQTAPRM >=0)) and (.w_CALPRM = 'Q') and (not(Empty(.w_PCSCAPRM)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCQTAPRM_2_2
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   (empty(.w_PCVALPRM) or not(.w_PCVALPRM >=0)) and (.w_CALPRM = 'V') and (not(Empty(.w_PCSCAPRM)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCVALPRM_2_3
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   not(.w_PCPERSCO >=0) and (.w_PCSCAPRM > 0 And .w_PCIMPSCO=0 And .w_PCSCOSCA=0 And .w_PCQTASCO = 0 And .w_PCPUNPEZ= 0 And .w_PCPUNSCA=0) and (not(Empty(.w_PCSCAPRM)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCPERSCO_2_5
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(!.w_CA__TIPO $'DA') and (.w_PCSCAPRM > 0 And .w_PCIMPSCO=0 And .w_PCSCOSCA=0 And .w_PCPERSCO = 0 And .w_PCPUNPEZ= 0 And .w_PCPUNSCA=0) and not(empty(.w_PCCODRIC)) and (not(Empty(.w_PCSCAPRM)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCCODRIC_2_8
          i_bRes = .f.
          i_bnoChk = .f.
        case   not(IIF(! EMPTY(nvl(.w_PCCODRIC,'')),CHKUNIMI(.w_PCUNMART, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3),.T.)) and (Not Empty(.w_PCCODRIC)) and not(empty(.w_PCUNMART)) and (not(Empty(.w_PCSCAPRM)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPCUNMART_2_9
          i_bRes = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_PCSCAPRM))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PCQTAPRM = this.w_PCQTAPRM
    this.o_PCVALPRM = this.w_PCVALPRM
    this.o_PCCODRIC = this.w_PCCODRIC
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_PCSCAPRM)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(999999,cp_maxroword()+10)
      .w_PCQTAPRM=0
      .w_PCVALPRM=0
      .w_PCSCAPRM=0
      .w_PCPERSCO=0
      .w_PCIMPSCO=0
      .w_PCSCOSCA=0
      .w_PCCODRIC=space(20)
      .w_PCUNMART=space(3)
      .w_PCQTASCO=0
      .w_PCCODART=space(20)
      .w_PCCODVAR=space(20)
      .w_PCKEYSAL=space(20)
      .w_PCFLGSCO=space(1)
      .w_PCPUNPEZ=0
      .w_PCPUNSCA=0
      .w_PCQTACON=0
      .w_PCVALCON=0
      .w_PCVALPRE=0
      .w_PCQTAPRE=0
      .w_PCPUNPRE=0
      .w_UNMIS1=space(3)
      .w_UNMIS2=space(3)
      .w_UNMIS3=space(3)
      .w_MOLTIP=0
      .w_OPERAT=space(1)
      .w_OPERA3=space(1)
      .w_MOLTI3=0
      .DoRTCalc(1,5,.f.)
        .w_PCSCAPRM = iif(.w_CALPRM='Q',.w_PCQTAPRM, .w_PCVALPRM)
      .DoRTCalc(7,10,.f.)
      if not(empty(.w_PCCODRIC))
        .link_2_8('Full')
      endif
        .w_PCUNMART = iif(! EMPTY(nvl(.w_PCCODRIC,'')), IIF(NOT EMPTY(.w_UNMIS3), .w_UNMIS3, .w_UNMIS1), '')
      .DoRTCalc(11,11,.f.)
      if not(empty(.w_PCUNMART))
        .link_2_9('Full')
      endif
      .DoRTCalc(12,13,.f.)
      if not(empty(.w_PCCODART))
        .link_2_11('Full')
      endif
      .DoRTCalc(14,15,.f.)
        .w_PCFLGSCO = 'S'
        .oPgFrm.Page1.oPag.oObj_2_39.Calculate(IIF(.w_CICLO="V", AH_MsgFormat("Pezzi venduti:"), AH_MsgFormat("Pezzi acquistati:")))
        .oPgFrm.Page1.oPag.oObj_2_40.Calculate(IIF(.w_CICLO="V", AH_MsgFormat("Valore venduto:"), AH_MsgFormat("Valore acquistato:")))
    endwith
    this.DoRTCalc(17,34,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_PCQTAPRM = t_PCQTAPRM
    this.w_PCVALPRM = t_PCVALPRM
    this.w_PCSCAPRM = t_PCSCAPRM
    this.w_PCPERSCO = t_PCPERSCO
    this.w_PCIMPSCO = t_PCIMPSCO
    this.w_PCSCOSCA = t_PCSCOSCA
    this.w_PCCODRIC = t_PCCODRIC
    this.w_PCUNMART = t_PCUNMART
    this.w_PCQTASCO = t_PCQTASCO
    this.w_PCCODART = t_PCCODART
    this.w_PCCODVAR = t_PCCODVAR
    this.w_PCKEYSAL = t_PCKEYSAL
    this.w_PCFLGSCO = this.oPgFrm.Page1.oPag.oPCFLGSCO_2_14.RadioValue(.t.)
    this.w_PCPUNPEZ = t_PCPUNPEZ
    this.w_PCPUNSCA = t_PCPUNSCA
    this.w_PCQTACON = t_PCQTACON
    this.w_PCVALCON = t_PCVALCON
    this.w_PCVALPRE = t_PCVALPRE
    this.w_PCQTAPRE = t_PCQTAPRE
    this.w_PCPUNPRE = t_PCPUNPRE
    this.w_UNMIS1 = t_UNMIS1
    this.w_UNMIS2 = t_UNMIS2
    this.w_UNMIS3 = t_UNMIS3
    this.w_MOLTIP = t_MOLTIP
    this.w_OPERAT = t_OPERAT
    this.w_OPERA3 = t_OPERA3
    this.w_MOLTI3 = t_MOLTI3
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_PCQTAPRM with this.w_PCQTAPRM
    replace t_PCVALPRM with this.w_PCVALPRM
    replace t_PCSCAPRM with this.w_PCSCAPRM
    replace t_PCPERSCO with this.w_PCPERSCO
    replace t_PCIMPSCO with this.w_PCIMPSCO
    replace t_PCSCOSCA with this.w_PCSCOSCA
    replace t_PCCODRIC with this.w_PCCODRIC
    replace t_PCUNMART with this.w_PCUNMART
    replace t_PCQTASCO with this.w_PCQTASCO
    replace t_PCCODART with this.w_PCCODART
    replace t_PCCODVAR with this.w_PCCODVAR
    replace t_PCKEYSAL with this.w_PCKEYSAL
    replace t_PCFLGSCO with this.oPgFrm.Page1.oPag.oPCFLGSCO_2_14.ToRadio()
    replace t_PCPUNPEZ with this.w_PCPUNPEZ
    replace t_PCPUNSCA with this.w_PCPUNSCA
    replace t_PCQTACON with this.w_PCQTACON
    replace t_PCVALCON with this.w_PCVALCON
    replace t_PCVALPRE with this.w_PCVALPRE
    replace t_PCQTAPRE with this.w_PCQTAPRE
    replace t_PCPUNPRE with this.w_PCPUNPRE
    replace t_UNMIS1 with this.w_UNMIS1
    replace t_UNMIS2 with this.w_UNMIS2
    replace t_UNMIS3 with this.w_UNMIS3
    replace t_MOLTIP with this.w_MOLTIP
    replace t_OPERAT with this.w_OPERAT
    replace t_OPERA3 with this.w_OPERA3
    replace t_MOLTI3 with this.w_MOLTI3
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsgp_mpcPag1 as StdContainer
  Width  = 1058
  height = 300
  stdWidth  = 1058
  stdheight = 300
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCICLO_1_4 as StdField with uid="CLFNXWVOOW",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CICLO", cQueryName = "CICLO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    HelpContextID = 59315162,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=975, Top=-15, InputMask=replicate('X',1)

  func oCICLO_1_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (1=1)
    endwith
    endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=4, top=5, width=846,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=9,Field1="CPROWORD",Label1="Riga",Field2="PCQTAPRM",Label2="Scaglione premio",Field3="PCVALPRM",Label3="Scaglione premio",Field4="PCPERSCO",Label4="Sc.%",Field5="PCIMPSCO",Label5="Sc. a valore per pezzo ",Field6="PCSCOSCA",Label6="Sc. a valore per scagl.",Field7="PCCODRIC",Label7="Articolo",Field8="PCUNMART",Label8="U.M.",Field9="PCQTASCO",Label9="N.pezzi in omaggio",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 168663162

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=27,;
    width=842+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=28,width=841+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='KEY_ARTI|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oPCFLGSCO_2_14.Refresh()
      this.Parent.oPCPUNPEZ_2_15.Refresh()
      this.Parent.oPCPUNSCA_2_16.Refresh()
      this.Parent.oPCQTACON_2_17.Refresh()
      this.Parent.oPCVALCON_2_18.Refresh()
      this.Parent.oPCVALPRE_2_19.Refresh()
      this.Parent.oPCQTAPRE_2_20.Refresh()
      this.Parent.oPCPUNPRE_2_21.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='KEY_ARTI'
        oDropInto=this.oBodyCol.oRow.oPCCODRIC_2_8
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oPCUNMART_2_9
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oPCFLGSCO_2_14 as StdTrsCombo with uid="TNKFUICDAT",rtrep=.t.,;
    cFormVar="w_PCFLGSCO", RowSource=""+"Sconto merce,"+"Omaggio imponibile,"+"Omaggio imponibile + IVA" , ;
    HelpContextID = 251074373,;
    Height=25, Width=160, Left=154, Top=189,;
    cTotal="", cQueryName = "PCFLGSCO",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oPCFLGSCO_2_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PCFLGSCO,&i_cF..t_PCFLGSCO),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'O',;
    iif(xVal =3,'I',;
    space(1)))))
  endfunc
  func oPCFLGSCO_2_14.GetRadio()
    this.Parent.oContained.w_PCFLGSCO = this.RadioValue()
    return .t.
  endfunc

  func oPCFLGSCO_2_14.ToRadio()
    this.Parent.oContained.w_PCFLGSCO=trim(this.Parent.oContained.w_PCFLGSCO)
    return(;
      iif(this.Parent.oContained.w_PCFLGSCO=='S',1,;
      iif(this.Parent.oContained.w_PCFLGSCO=='O',2,;
      iif(this.Parent.oContained.w_PCFLGSCO=='I',3,;
      0))))
  endfunc

  func oPCFLGSCO_2_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oPCPUNPEZ_2_15 as StdTrsField with uid="OUDAWMGHAZ",rtseq=17,rtrep=.t.,;
    cFormVar="w_PCPUNPEZ",value=0,;
    HelpContextID = 208713552,;
    cTotal="", bFixedPos=.t., cQueryName = "PCPUNPEZ",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=488, Top=189, cSayPict=["9999999999"], cGetPict=["9999999999"]

  func oPCPUNPEZ_2_15.mCond()
    with this.Parent.oContained
      return (.w_PCSCAPRM > 0 And .w_PCIMPSCO=0 And .w_PCSCOSCA=0 And .w_PCQTASCO = 0 And .w_PCPERSCO= 0 And .w_PCPUNSCA=0)
    endwith
  endfunc

  add object oPCPUNSCA_2_16 as StdTrsField with uid="CJPCIUETMO",rtseq=18,rtrep=.t.,;
    cFormVar="w_PCPUNSCA",value=0,;
    HelpContextID = 259045175,;
    cTotal="", bFixedPos=.t., cQueryName = "PCPUNSCA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=746, Top=189, cSayPict=["9999999999"], cGetPict=["9999999999"]

  func oPCPUNSCA_2_16.mCond()
    with this.Parent.oContained
      return (.w_PCSCAPRM > 0 And .w_PCIMPSCO=0 And .w_PCSCOSCA=0 And .w_PCQTASCO = 0 And .w_PCPUNPEZ= 0 And .w_PCPERSCO=0)
    endwith
  endfunc

  add object oPCQTACON_2_17 as StdTrsField with uid="OZNBMTVIKH",rtseq=19,rtrep=.t.,;
    cFormVar="w_PCQTACON",value=0,enabled=.f.,;
    HelpContextID = 23083196,;
    cTotal="", bFixedPos=.t., cQueryName = "PCQTACON",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=154, Top=238, cSayPict=[v_PQ(12)], cGetPict=[v_GQ(12)]

  add object oPCVALCON_2_18 as StdTrsField with uid="VHIJVKNYXZ",rtseq=20,rtrep=.t.,;
    cFormVar="w_PCVALCON",value=0,enabled=.f.,;
    HelpContextID = 12773564,;
    cTotal="", bFixedPos=.t., cQueryName = "PCVALCON",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=429, Top=238, cSayPict=[v_PU(38+VVP)], cGetPict=[v_GU(38+VVP)]

  add object oPCVALPRE_2_19 as StdTrsField with uid="GCAGOQODCQ",rtseq=21,rtrep=.t.,;
    cFormVar="w_PCVALPRE",value=0,enabled=.f.,;
    HelpContextID = 205330235,;
    cTotal="", bFixedPos=.t., cQueryName = "PCVALPRE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=154, Top=264, cSayPict=[v_PU(38+VVP)], cGetPict=[v_GU(38+VVP)]

  add object oPCQTAPRE_2_20 as StdTrsField with uid="XHGJTXCOVV",rtseq=22,rtrep=.t.,;
    cFormVar="w_PCQTAPRE",value=0,enabled=.f.,;
    HelpContextID = 195020603,;
    cTotal="", bFixedPos=.t., cQueryName = "PCQTAPRE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=429, Top=264, cSayPict=[v_PQ(12)], cGetPict=[v_GQ(12)]

  add object oPCPUNPRE_2_21 as StdTrsField with uid="QOKLNNQHBD",rtseq=23,rtrep=.t.,;
    cFormVar="w_PCPUNPRE",value=0,enabled=.f.,;
    HelpContextID = 208713531,;
    cTotal="", bFixedPos=.t., cQueryName = "PCPUNPRE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=679, Top=264, cSayPict=["9999999999"], cGetPict=["9999999999"]

  add object oObj_2_39 as cp_calclbl with uid="QDFMPOCHDA",width=101,height=17,;
   left=52, top=238,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=2;
    , HelpContextID = 30569958

  add object oObj_2_40 as cp_calclbl with uid="XIOBGWBKZM",width=116,height=17,;
   left=311, top=238,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",alignment=1,;
    nPag=2;
    , HelpContextID = 30569958

  add object oStr_2_22 as StdString with uid="UCZCCTHLVS",Visible=.t., Left=41, Top=189,;
    Alignment=1, Width=112, Height=18,;
    Caption="Tipo omaggio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="QZMYTMNUQR",Visible=.t., Left=319, Top=189,;
    Alignment=1, Width=167, Height=18,;
    Caption="Punti premio per pezzo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="KZROGCXDPS",Visible=.t., Left=575, Top=189,;
    Alignment=1, Width=167, Height=18,;
    Caption="Punti premio per scaglione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="FQVTVXMPGB",Visible=.t., Left=45, Top=218,;
    Alignment=0, Width=101, Height=18,;
    Caption="Contatori"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_27 as StdString with uid="ZJJVTAAKGH",Visible=.t., Left=41, Top=264,;
    Alignment=1, Width=112, Height=18,;
    Caption="Premio a valore:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="CVDOEHPVNE",Visible=.t., Left=315, Top=264,;
    Alignment=1, Width=112, Height=18,;
    Caption="Premio a quantit�:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="QQAXSIIPLH",Visible=.t., Left=532, Top=264,;
    Alignment=1, Width=142, Height=18,;
    Caption="Premio a punti:"  ;
  , bGlobalFont=.t.

  add object oBox_2_26 as StdBox with uid="MBFDBWHOKV",left=46, top=232, width=716,height=1

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsgp_mpcBodyRow as CPBodyRowCnt
  Width=832
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="HNMTPVVDOG",rtseq=3,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 200991594,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=38, Left=-2, Top=0

  add object oPCQTAPRM_2_2 as StdTrsField with uid="ZMRUMTSJNQ",rtseq=4,rtrep=.t.,;
    cFormVar="w_PCQTAPRM",value=0,;
    HelpContextID = 195020611,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=122, Left=37, Top=0, cSayPict=[v_PQ(12)], cGetPict=[v_GQ(12)]

  func oPCQTAPRM_2_2.mCond()
    with this.Parent.oContained
      return (.w_CALPRM = 'Q')
    endwith
  endfunc

  func oPCQTAPRM_2_2.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CALPRM = 'V')
    endwith
    endif
  endfunc

  func oPCQTAPRM_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PCQTAPRM >=0)
    endwith
    return bRes
  endfunc

  add object oPCVALPRM_2_3 as StdTrsField with uid="TCKNBOHFDH",rtseq=5,rtrep=.t.,;
    cFormVar="w_PCVALPRM",value=0,;
    HelpContextID = 205330243,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=122, Left=37, Top=0, cSayPict=[v_PU(38+VVP)], cGetPict=[v_GU(38+VVP)]

  func oPCVALPRM_2_3.mCond()
    with this.Parent.oContained
      return (.w_CALPRM = 'V')
    endwith
  endfunc

  func oPCVALPRM_2_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CALPRM = 'Q')
    endwith
    endif
  endfunc

  func oPCVALPRM_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PCVALPRM >=0)
    endwith
    return bRes
  endfunc

  add object oPCPERSCO_2_5 as StdTrsField with uid="OPAYDPMHDV",rtseq=7,rtrep=.t.,;
    cFormVar="w_PCPERSCO",value=0,;
    HelpContextID = 262190917,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=47, Left=164, Top=0, cSayPict=["99.999"], cGetPict=["99.999"]

  func oPCPERSCO_2_5.mCond()
    with this.Parent.oContained
      return (.w_PCSCAPRM > 0 And .w_PCIMPSCO=0 And .w_PCSCOSCA=0 And .w_PCQTASCO = 0 And .w_PCPUNPEZ= 0 And .w_PCPUNSCA=0)
    endwith
  endfunc

  func oPCPERSCO_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PCPERSCO >=0)
    endwith
    return bRes
  endfunc

  add object oPCIMPSCO_2_6 as StdTrsField with uid="RECHKVTPNJ",rtseq=8,rtrep=.t.,;
    cFormVar="w_PCIMPSCO",value=0,;
    HelpContextID = 260589381,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=145, Left=213, Top=0, cSayPict=[v_PU(38+VVP)], cGetPict=[v_GU(38+VVP)]

  func oPCIMPSCO_2_6.mCond()
    with this.Parent.oContained
      return (.w_PCSCAPRM > 0 And .w_PCPERSCO=0 And .w_PCSCOSCA=0 And .w_PCQTASCO = 0 And .w_PCPUNPEZ= 0 And .w_PCPUNSCA=0)
    endwith
  endfunc

  add object oPCSCOSCA_2_7 as StdTrsField with uid="UXOEMHKPGX",rtseq=9,rtrep=.t.,;
    cFormVar="w_PCSCOSCA",value=0,;
    HelpContextID = 258926391,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=136, Left=361, Top=0, cSayPict=[v_PU(38+VVP)], cGetPict=[v_GU(38+VVP)]

  func oPCSCOSCA_2_7.mCond()
    with this.Parent.oContained
      return (.w_PCSCAPRM > 0 And .w_PCIMPSCO=0 And .w_PCPERSCO=0 And .w_PCQTASCO = 0 And .w_PCPUNPEZ= 0 And .w_PCPUNSCA=0)
    endwith
  endfunc

  add object oPCCODRIC_2_8 as StdTrsField with uid="NLANSTITCO",rtseq=10,rtrep=.t.,;
    cFormVar="w_PCCODRIC",value=space(20),;
    HelpContextID = 37099719,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=153, Left=500, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_BZA", oKey_1_1="CACODICE", oKey_1_2="this.w_PCCODRIC"

  func oPCCODRIC_2_8.mCond()
    with this.Parent.oContained
      return (.w_PCSCAPRM > 0 And .w_PCIMPSCO=0 And .w_PCSCOSCA=0 And .w_PCPERSCO = 0 And .w_PCPUNPEZ= 0 And .w_PCPUNSCA=0)
    endwith
  endfunc

  func oPCCODRIC_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCCODRIC_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPCCODRIC_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oPCCODRIC_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Chiavi articolo",'',this.parent.oContained
  endproc
  proc oPCCODRIC_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_PCCODRIC
    i_obj.ecpSave()
  endproc

  add object oPCUNMART_2_9 as StdTrsField with uid="WNAFFMJTJS",rtseq=11,rtrep=.t.,;
    cFormVar="w_PCUNMART",value=space(3),;
    HelpContextID = 224003914,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=655, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_PCUNMART"

  func oPCUNMART_2_9.mCond()
    with this.Parent.oContained
      return (Not Empty(.w_PCCODRIC))
    endwith
  endfunc

  func oPCUNMART_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCUNMART_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPCUNMART_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oPCUNMART_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'GSMAUMVM.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oPCUNMART_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_PCUNMART
    i_obj.ecpSave()
  endproc

  add object oPCQTASCO_2_10 as StdTrsField with uid="NHAQPLLGUY",rtseq=12,rtrep=.t.,;
    cFormVar="w_PCQTASCO",value=0,;
    HelpContextID = 245352261,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=122, Left=705, Top=0, cSayPict=[v_PQ(12)], cGetPict=[v_GQ(12)]

  func oPCQTASCO_2_10.mCond()
    with this.Parent.oContained
      return (.w_PCSCAPRM > 0 And .w_PCIMPSCO=0 And .w_PCSCOSCA=0 And .w_PCPERSCO = 0 And .w_PCPUNPEZ= 0 And .w_PCPUNSCA=0)
    endwith
  endfunc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsgp_mpc','PREMICON','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CCCODICE=PREMICON.CCCODICE";
  +" and "+i_cAliasName2+".PCROWNUM=PREMICON.PCROWNUM";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsgp_mpc
* --- Class definition
define class tgsgp_mpc as StdPCForm
  Width  = 862
  Height = 300
  Top    = 1
  Left   = 1
  cComment = "Premi contratto"
  HelpContextID=147481961
  add object cnt as tcgsgp_mpc
  Proc ShowChildrenChain(bFromBatch)
    this.cnt.ShowChildrenChain()
    If  !bFromBatch and TYPE('this.cnt.oParentObject.w_SHOWMPC') <> 'C' OR this.cnt.oParentObject.w_SHOWMPC = 'S'
     this.Show()
    Endif
    return
  Endproc
enddefine

* --- Fine Area Manuale
