* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsgp_bgp                                                        *
*              Generazione premi                                               *
*                                                                              *
*      Author: Zucchetti - AT                                                  *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-23                                                      *
* Last revis.: 2017-11-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsgp_bgp",oParentObject)
return(i_retval)

define class tgsgp_bgp as StdBatch
  * --- Local variables
  w_APCONTR = space(15)
  w_MVTIPCON = space(1)
  w_RIGAKEY = 0
  w_DLCODART = space(20)
  w_CODKEY = space(20)
  w_COKEYSAL = space(40)
  w_PROGR = 0
  w_MVCODCON = space(15)
  w_COKEYART = space(41)
  w_VALOREART = space(0)
  w_COUNIMIS = space(3)
  w_CLIFOR = space(15)
  w_GRUART = space(15)
  w_FLGEXD = space(1)
  w_VALCON = 0
  w_QTACON = 0
  w_FLGINC = space(1)
  w_OKSCAGL = .f.
  w_PCQTACON = 0
  w_PCVALCON = 0
  w_PCSCAPRM = 0
  w_CONTA = 0
  w_PCQTAPRE = 0
  w_PCPUNPRE = 0
  w_COVALFAT = 0
  w_COTOTQTA = 0
  w_COSCOCOM = 0
  w_DOCSCOM = 0
  w_NOAGG = .f.
  w_SC1 = 0
  w_SC2 = 0
  w_SC3 = 0
  w_SC4 = 0
  w_SC5 = 0
  w_SC6 = 0
  w_COTOTPUN = 0
  w_COPREZZO = 0
  w_TOTVALPRE = 0
  w_TOTPREMIO = 0
  w_COTOTPRE = 0
  w_COVALPRE = 0
  w_APVALPRE = 0
  w_COPRMLIQ = space(1)
  w_CODATINI = ctod("  /  /  ")
  w_CODATFIN = ctod("  /  /  ")
  w_PCVALPRE = 0
  w_MVTIPDOC = space(5)
  w_TIPINT = space(1)
  w_CODAGE = space(5)
  w_FLGVAL = space(1)
  w_FLGLIS = space(1)
  w_NUMLIS = 0
  w_NUMART = 0
  w_MVFLSCOR = space(1)
  w_MVCODBAN = space(5)
  w_MVCODAGE = space(5)
  w_ANCODAG1 = space(5)
  w_ANCODAG2 = space(5)
  w_MVCODAG2 = space(5)
  w_SERGDOC = space(10)
  w_TOTVALROW = 0
  w_MVPREZZO = 0
  w_COTIPSTO = space(1)
  w_ROWRIF = space(20)
  w_QTACONROW = 0
  w_VALCONROW = 0
  w_GDKEYRIF = space(15)
  w_OKVALFAT = .f.
  w_PSSERIAL = space(10)
  w_FLFRAZ = space(1)
  w_NUMRIG = 0
  w_UNIMIS = space(3)
  w_CODART = space(20)
  w_CODVAR = space(20)
  w_TIPORAG = space(6)
  w_RAGUMI = space(1)
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_PARTECIPA = space(1)
  * --- WorkFile variables
  PREMICON_idx=0
  CCOMMAST_idx=0
  INVENTAR_idx=0
  INVEDETT_idx=0
  DOC_MAST_idx=0
  TIP_DOCU_idx=0
  CAU_CONT_idx=0
  PREMIDOC_idx=0
  ASLISCON_idx=0
  CCOMDATT_idx=0
  TMPPREMI_idx=0
  AGENTI_idx=0
  GRUPPACQ_idx=0
  CONTI_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Generazione premi  + Documenti fiscali e Documenti interni di rettifica
    this.w_PSSERIAL = this.oParentObject.w_GPSERIAL
    this.w_APCONTR = REPL("#",15)
    * --- Creazione tabella temporanea necessaria per memorizzare per ogni riga contratto
    *     il keysal o codice ricerca con UM con il suo PESO (fatturato, quantit� venduta)
    *     necessario per la ripartizione del premio, la tabella sar� utilizzata per definire
    *     l'insieme esatto di documenti che hanno partecipato al calcolo del fatturato per 
    *     quel determinato contratto - zoom dati cinsuntivi
    * --- Create temporary table TMPPREMI
    i_nIdx=cp_AddTableDef('TMPPREMI') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('..\gpfa\exe\query\GSGP_TMP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPPREMI_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Ciclo sui contratti (anche dettagli).Ogni riga contratto determiner� un diverso
    *     tipo di raggruppamento (Articolo, Codici di ricerca, Attributi..)
    *     Per ciascuna riga sar� calcolato il premio finale e sar� eventualmente (dipende da w_GPDOCFIS, w_GPDOCINT)
    *     riempita la tabella temporanea TMPPREMI (nel caso di attributi avremo anche il dettaglio di ogni
    *     singolo articolo soddisfatto)  necessaria per la generazione documenti
    *     La Tabella PREMIDOC contiene l'insieme dei documenti di origine utilizzati per 
    *     la generazione del premio, non � temporanea sar� infatti utlizzata per popolare
    *     lo zoom dei dati consuntivi di ogni contratto
    * --- Select from GSGP_BGP
    do vq_exec with 'GSGP_BGP',this,'_Curs_GSGP_BGP','',.f.,.t.
    if used('_Curs_GSGP_BGP')
      select _Curs_GSGP_BGP
      locate for 1=1
      do while not(eof())
      if this.w_APCONTR <> _Curs_GSGP_BGP.COCODICE
        * --- E' cambiato il codice del contratto
        if this.w_APCONTR <> REPL("#",15)
          this.w_COPREZZO = this.w_COVALFAT * (1- this.w_SC1 /100)*(1 - this.w_SC2 /100)*(1- this.w_SC3 /100)*(1- this.w_SC4 /100)*(1- this.w_SC5 /100)*(1- this.w_SC6 /100)
          this.w_COSCOCOM = this.w_COVALFAT - this.w_COPREZZO
          this.w_COVALPRE = this.w_TOTVALPRE + (this.w_COTOTPUN * this.w_APVALPRE)
          this.w_COTOTPRE = this.w_COSCOCOM+this.w_COVALPRE
          if this.oParentObject.w_GPDOCFIS="S" Or this.oParentObject.w_GPDOCINT="S"
            * --- Aggiorno il totale premio valore  TOTVALPRE
            * --- Write into TMPPREMI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPPREMI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPPREMI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPREMI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"TOTVALPRE ="+cp_NullLink(cp_ToStrODBC(this.w_TOTVALPRE),'TMPPREMI','TOTVALPRE');
              +",PESOVALROW ="+cp_NullLink(cp_ToStrODBC(this.w_COVALFAT),'TMPPREMI','PESOVALROW');
              +",PESOQTAROW ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTQTA),'TMPPREMI','PESOQTAROW');
              +",COSCOCOM ="+cp_NullLink(cp_ToStrODBC(this.w_COSCOCOM),'TMPPREMI','COSCOCOM');
              +",COTOTPUN ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTPUN),'TMPPREMI','COTOTPUN');
              +",NUMRIG ="+cp_NullLink(cp_ToStrODBC(this.w_NUMRIG),'TMPPREMI','NUMRIG');
                  +i_ccchkf ;
              +" where ";
                  +"COCODICE = "+cp_ToStrODBC(this.w_APCONTR);
                     )
            else
              update (i_cTable) set;
                  TOTVALPRE = this.w_TOTVALPRE;
                  ,PESOVALROW = this.w_COVALFAT;
                  ,PESOQTAROW = this.w_COTOTQTA;
                  ,COSCOCOM = this.w_COSCOCOM;
                  ,COTOTPUN = this.w_COTOTPUN;
                  ,NUMRIG = this.w_NUMRIG;
                  &i_ccchkf. ;
               where;
                  COCODICE = this.w_APCONTR;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          * --- Verifico se sono presenti i documenti che hanno generato del fatturato 
          *     per il contratto in questione
          * --- Read from PREMIDOC
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PREMIDOC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PREMIDOC_idx,2],.t.,this.PREMIDOC_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" PREMIDOC where ";
                  +"SERGPREM = "+cp_ToStrODBC(this.oParentObject.w_GPSERIAL);
                  +" and RIFCONTR = "+cp_ToStrODBC(this.w_APCONTR);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  SERGPREM = this.oParentObject.w_GPSERIAL;
                  and RIFCONTR = this.w_APCONTR;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_ROWS > 0
            if this.oParentObject.w_GPSTATUS = "S"
              * --- Simulata
              * --- Write into CCOMMAST
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.CCOMMAST_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CCOMMAST_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.CCOMMAST_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"COVALFAT ="+cp_NullLink(cp_ToStrODBC(this.w_COVALFAT),'CCOMMAST','COVALFAT');
                +",COSCOCOM ="+cp_NullLink(cp_ToStrODBC(this.w_COSCOCOM),'CCOMMAST','COSCOCOM');
                +",COTOTPUN ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTPUN),'CCOMMAST','COTOTPUN');
                +",COVALPRE ="+cp_NullLink(cp_ToStrODBC(this.w_COVALPRE),'CCOMMAST','COVALPRE');
                +",COTOTPRE ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTPRE),'CCOMMAST','COTOTPRE');
                +",CODATGEN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GPDATREG),'CCOMMAST','CODATGEN');
                    +i_ccchkf ;
                +" where ";
                    +"COCODICE = "+cp_ToStrODBC(this.w_APCONTR);
                       )
              else
                update (i_cTable) set;
                    COVALFAT = this.w_COVALFAT;
                    ,COSCOCOM = this.w_COSCOCOM;
                    ,COTOTPUN = this.w_COTOTPUN;
                    ,COVALPRE = this.w_COVALPRE;
                    ,COTOTPRE = this.w_COTOTPRE;
                    ,CODATGEN = this.oParentObject.w_GPDATREG;
                    &i_ccchkf. ;
                 where;
                    COCODICE = this.w_APCONTR;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            else
              if this.w_COPRMLIQ $ "FA"
                * --- Imposto Elaborato
                * --- Write into CCOMMAST
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.CCOMMAST_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CCOMMAST_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.CCOMMAST_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"COVALFAT ="+cp_NullLink(cp_ToStrODBC(this.w_COVALFAT),'CCOMMAST','COVALFAT');
                  +",COSCOCOM ="+cp_NullLink(cp_ToStrODBC(this.w_COSCOCOM),'CCOMMAST','COSCOCOM');
                  +",COTOTPUN ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTPUN),'CCOMMAST','COTOTPUN');
                  +",COVALPRE ="+cp_NullLink(cp_ToStrODBC(this.w_COVALPRE),'CCOMMAST','COVALPRE');
                  +",COTOTPRE ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTPRE),'CCOMMAST','COTOTPRE');
                  +",CODATGEN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GPDATREG),'CCOMMAST','CODATGEN');
                  +",COFLGSTA ="+cp_NullLink(cp_ToStrODBC("E"),'CCOMMAST','COFLGSTA');
                      +i_ccchkf ;
                  +" where ";
                      +"COCODICE = "+cp_ToStrODBC(this.w_APCONTR);
                         )
                else
                  update (i_cTable) set;
                      COVALFAT = this.w_COVALFAT;
                      ,COSCOCOM = this.w_COSCOCOM;
                      ,COTOTPUN = this.w_COTOTPUN;
                      ,COVALPRE = this.w_COVALPRE;
                      ,COTOTPRE = this.w_COTOTPRE;
                      ,CODATGEN = this.oParentObject.w_GPDATREG;
                      ,COFLGSTA = "E";
                      &i_ccchkf. ;
                   where;
                      COCODICE = this.w_APCONTR;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              else
                if this.oParentObject.w_GPDATFIN=this.w_CODATFIN
                  * --- Imposto Elaborato
                  * --- Write into CCOMMAST
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.CCOMMAST_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.CCOMMAST_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.CCOMMAST_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"COVALFAT ="+cp_NullLink(cp_ToStrODBC(this.w_COVALFAT),'CCOMMAST','COVALFAT');
                    +",COSCOCOM ="+cp_NullLink(cp_ToStrODBC(this.w_COSCOCOM),'CCOMMAST','COSCOCOM');
                    +",COTOTPUN ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTPUN),'CCOMMAST','COTOTPUN');
                    +",COVALPRE ="+cp_NullLink(cp_ToStrODBC(this.w_COVALPRE),'CCOMMAST','COVALPRE');
                    +",COTOTPRE ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTPRE),'CCOMMAST','COTOTPRE');
                    +",CODATGEN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GPDATREG),'CCOMMAST','CODATGEN');
                    +",COFLGSTA ="+cp_NullLink(cp_ToStrODBC("E"),'CCOMMAST','COFLGSTA');
                        +i_ccchkf ;
                    +" where ";
                        +"COCODICE = "+cp_ToStrODBC(this.w_APCONTR);
                           )
                  else
                    update (i_cTable) set;
                        COVALFAT = this.w_COVALFAT;
                        ,COSCOCOM = this.w_COSCOCOM;
                        ,COTOTPUN = this.w_COTOTPUN;
                        ,COVALPRE = this.w_COVALPRE;
                        ,COTOTPRE = this.w_COTOTPRE;
                        ,CODATGEN = this.oParentObject.w_GPDATREG;
                        ,COFLGSTA = "E";
                        &i_ccchkf. ;
                     where;
                        COCODICE = this.w_APCONTR;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                else
                  * --- Write into CCOMMAST
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.CCOMMAST_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.CCOMMAST_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.CCOMMAST_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"COVALFAT ="+cp_NullLink(cp_ToStrODBC(this.w_COVALFAT),'CCOMMAST','COVALFAT');
                    +",COSCOCOM ="+cp_NullLink(cp_ToStrODBC(this.w_COSCOCOM),'CCOMMAST','COSCOCOM');
                    +",COTOTPUN ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTPUN),'CCOMMAST','COTOTPUN');
                    +",COVALPRE ="+cp_NullLink(cp_ToStrODBC(this.w_COVALPRE),'CCOMMAST','COVALPRE');
                    +",COTOTPRE ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTPRE),'CCOMMAST','COTOTPRE');
                    +",CODATGEN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GPDATREG),'CCOMMAST','CODATGEN');
                        +i_ccchkf ;
                    +" where ";
                        +"COCODICE = "+cp_ToStrODBC(this.w_APCONTR);
                           )
                  else
                    update (i_cTable) set;
                        COVALFAT = this.w_COVALFAT;
                        ,COSCOCOM = this.w_COSCOCOM;
                        ,COTOTPUN = this.w_COTOTPUN;
                        ,COVALPRE = this.w_COVALPRE;
                        ,COTOTPRE = this.w_COTOTPRE;
                        ,CODATGEN = this.oParentObject.w_GPDATREG;
                        &i_ccchkf. ;
                     where;
                        COCODICE = this.w_APCONTR;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              endif
            endif
          endif
        endif
        * --- Dati di testata del nuovo contratto a obiettivo
        this.w_APCONTR = _Curs_GSGP_BGP.COCODICE
        ah_msg( "Elaborazione contratto: %1",.t.,.f.,.f.,ALLTRIM(this.w_APCONTR) )
        this.w_CLIFOR = _Curs_GSGP_BGP.COCODINT
        this.w_CODAGE = _Curs_GSGP_BGP.COCODAGE
        this.w_GRUART = _Curs_GSGP_BGP.COCODGRU
        this.w_FLGEXD = _Curs_GSGP_BGP.COFLGEXD
        this.w_FLGINC = _Curs_GSGP_BGP.COFLGINC
        this.w_COPRMLIQ = _Curs_GSGP_BGP.COPRMLIQ
        this.w_CODATINI = _Curs_GSGP_BGP.CODATINI
        this.w_CODATFIN = _Curs_GSGP_BGP.CODATFIN
        this.w_SC1 = IIF(_Curs_GSGP_BGP.COSCOFL1=1,_Curs_GSGP_BGP.COSCOGL1,0)
        this.w_SC2 = IIF(_Curs_GSGP_BGP.COSCOFL2=1,_Curs_GSGP_BGP.COSCOGL2,0)
        this.w_SC3 = IIF(_Curs_GSGP_BGP.COSCOFL3=1,_Curs_GSGP_BGP.COSCOGL3,0)
        this.w_SC4 = IIF(_Curs_GSGP_BGP.COSCOFL4=1,_Curs_GSGP_BGP.COSCOGL4,0)
        this.w_SC5 = IIF(_Curs_GSGP_BGP.COSCOFL5=1,_Curs_GSGP_BGP.COSCOGL5,0)
        this.w_SC6 = IIF(_Curs_GSGP_BGP.COSCOFL6=1,_Curs_GSGP_BGP.COSCOGL6,0)
        this.w_APVALPRE = _Curs_GSGP_BGP.COVALPUN
        this.w_FLGVAL = _Curs_GSGP_BGP.COFLGVAL
        this.w_FLGLIS = _Curs_GSGP_BGP.COFLGLIS
        this.w_TIPINT = _Curs_GSGP_BGP.COTIPINT
        * --- Reset dati consuntivi
        this.w_COPREZZO = 0
        this.w_COSCOCOM = 0
        this.w_COVALPRE = 0
        this.w_TOTVALPRE = 0
        this.w_COTOTPRE = 0
        this.w_NUMRIG = 0
        this.w_COTOTQTA = 0
        this.w_COVALFAT = 0
        * --- Elimino da PREMIDOC i documenti di orgine associati al contratto
        *     (se il contratto ha gi� generato premi)
        * --- Delete from PREMIDOC
        i_nConn=i_TableProp[this.PREMIDOC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PREMIDOC_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"RIFCONTR = "+cp_ToStrODBC(this.w_APCONTR);
                +" and CPROWNUM = "+cp_ToStrODBC(-20);
                 )
        else
          delete from (i_cTable) where;
                RIFCONTR = this.w_APCONTR;
                and CPROWNUM = -20;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Estrazione righe documento da analizzare per il contratto in corso
        do case
          case empty(nvl(this.w_CODATINI,""))
            this.w_DATINI = this.oParentObject.w_GPDATINI
          case empty(nvl(this.oParentObject.w_GPDATINI,""))
            this.w_DATINI = this.w_CODATINI
          otherwise
            this.w_DATINI = max(this.w_CODATINI,this.oParentObject.w_GPDATINI)
        endcase
        do case
          case empty(nvl(this.w_CODATFIN,""))
            this.w_DATFIN = this.oParentObject.w_GPDATFIN
          case empty(nvl(this.oParentObject.w_GPDATFIN,""))
            this.w_DATFIN = this.w_CODATFIN
          otherwise
            this.w_DATFIN = min(this.w_CODATFIN,this.oParentObject.w_GPDATFIN)
        endcase
        if this.w_TIPINT <> "A"
          * --- Intestato a Cli\For oppure gruppo di acquisto
          * --- Insert into PREMIDOC
          i_nConn=i_TableProp[this.PREMIDOC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PREMIDOC_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\gpfa\exe\query\GSGP0BGP",this.PREMIDOC_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Intestato ad Agente
          * --- Insert into PREMIDOC
          i_nConn=i_TableProp[this.PREMIDOC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PREMIDOC_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\GPFA\EXE\QUERY\GSGP0ABGP",this.PREMIDOC_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          * --- Elimino le righe che hanno contratti di riga intestati al cliente/fornitore aventi 
          *     COFLESAG = 'S' - escludi da premio agente 
          * --- Delete from PREMIDOC
          i_nConn=i_TableProp[this.PREMIDOC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PREMIDOC_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_cWhere=i_cTable+".SERGPREM = "+i_cQueryTable+".SERGPREM";
                  +" and "+i_cTable+".MVSERIAL  = "+i_cQueryTable+".MVSERIAL ";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                  +" and "+i_cTable+".RIFCONTR = "+i_cQueryTable+".RIFCONTR";
                  +" and "+i_cTable+".MVTIPCON = "+i_cQueryTable+".MVTIPCON";
                  +" and "+i_cTable+".MVCODCON = "+i_cQueryTable+".MVCODCON";
          
            do vq_exec with '..\GPFA\EXE\QUERY\GSGP1ABGP',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
        if this.w_FLGVAL<>"S"
          * --- Si controlla se il contratto a effetto immediato � presente tra i contratti collegati
          * --- Delete from PREMIDOC
          i_nConn=i_TableProp[this.PREMIDOC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PREMIDOC_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_cWhere=i_cTable+".SERGPREM = "+i_cQueryTable+".SERGPREM";
                  +" and "+i_cTable+".MVSERIAL  = "+i_cQueryTable+".MVSERIAL ";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                  +" and "+i_cTable+".RIFCONTR = "+i_cQueryTable+".RIFCONTR";
          
            do vq_exec with '..\GPFA\EXE\QUERY\GSGP2ABGP',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
        if this.w_FLGLIS<>"S"
          * --- Si controlla che tutti i listini presenti sulla riga appartengano all'insieme dei listini collegati
          this.w_NUMLIS = 0
          * --- Select from ASLISCON
          i_nConn=i_TableProp[this.ASLISCON_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ASLISCON_idx,2],.t.,this.ASLISCON_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT(*) AS NUMLIS  from "+i_cTable+" ASLISCON ";
                +" where CLCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And CLDATINI <= "+cp_ToStrODBC(this.oParentObject.w_GPDATREG)+" And CLDATFIN >= "+cp_ToStrODBC(this.oParentObject.w_GPDATREG)+"";
                 ,"_Curs_ASLISCON")
          else
            select COUNT(*) AS NUMLIS from (i_cTable);
             where CLCODICE = this.w_APCONTR And CLDATINI <= this.oParentObject.w_GPDATREG And CLDATFIN >= this.oParentObject.w_GPDATREG;
              into cursor _Curs_ASLISCON
          endif
          if used('_Curs_ASLISCON')
            select _Curs_ASLISCON
            locate for 1=1
            do while not(eof())
            * --- Totale listini associati al contratto ad obiettivo in esame
            this.w_NUMLIS = NVL(NUMLIS,0)
              select _Curs_ASLISCON
              continue
            enddo
            use
          endif
          * --- Delete from PREMIDOC
          i_nConn=i_TableProp[this.PREMIDOC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PREMIDOC_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            local i_cQueryTable,i_cWhere
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_cWhere=i_cTable+".SERGPREM = "+i_cQueryTable+".SERGPREM";
                  +" and "+i_cTable+".MVSERIAL  = "+i_cQueryTable+".MVSERIAL ";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                  +" and "+i_cTable+".RIFCONTR = "+i_cQueryTable+".RIFCONTR";
          
            do vq_exec with '..\GPFA\EXE\QUERY\GSGP4ABGP',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
        endif
      endif
      * --- Per ogni riga contratto raggruppo le righe documento in funzione delle dimesioni 
      *     obiettivo caricate nel dettaglio contratto
      * --- Azzero il totale del premio della singola riga contratto
      this.w_TOTVALROW = 0
      this.w_RIGAKEY = _Curs_GSGP_BGP.CPROWNUM
      this.w_NUMRIG = this.w_NUMRIG + 1
      * --- Calcolo codice di ricerca interno
      this.w_CODKEY = _Curs_GSGP_BGP.COCODART
      this.w_COKEYSAL = _Curs_GSGP_BGP.COKEYSAL
      this.w_COKEYART = _Curs_GSGP_BGP.COKEYART
      this.w_COUNIMIS = _Curs_GSGP_BGP.COUNIMIS
      this.w_FLFRAZ = _Curs_GSGP_BGP.FLFRAZ
      * --- Devo azzerare tutti i valori
      * --- Write into PREMICON
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PREMICON_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PREMICON_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PCQTACON ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCQTACON');
        +",PCVALCON ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCVALCON');
        +",PCVALPRE ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCVALPRE');
        +",PCPUNPRE ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCPUNPRE');
        +",PCQTAPRE ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCQTAPRE');
            +i_ccchkf ;
        +" where ";
            +"CCCODICE = "+cp_ToStrODBC(this.w_APCONTR);
            +" and PCROWNUM = "+cp_ToStrODBC(this.w_RIGAKEY);
               )
      else
        update (i_cTable) set;
            PCQTACON = 0;
            ,PCVALCON = 0;
            ,PCVALPRE = 0;
            ,PCPUNPRE = 0;
            ,PCQTAPRE = 0;
            &i_ccchkf. ;
         where;
            CCCODICE = this.w_APCONTR;
            and PCROWNUM = this.w_RIGAKEY;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Una volta individuato l'insieme di righe documento valide per quel contratto
      *     per ogni riga del contratto calcolo il totale del fatturato/quant� lavorando 
      *     su questa tabella PREMIDOC
      * --- Calcolo venduto/acquistato (totale per riga contratto)
      * --- Cliente, Fornitore Grupo Acquisto
      if _Curs_GSGP_BGP.COATTRIB <> 1
        * --- Articolo fisso (o codice di ricerca)
        do case
          case alltrim(this.w_COKEYART) == alltrim(this.w_CODKEY) And _Curs_GSGP_BGP.COUMALTE=1
            * --- Codice di riicerca interno e attivo flag UM alternative
            *     ragruppo per MVKEYSAL=COKEYSAL
            VQ_EXEC ("..\GPFA\EXE\QUERY\GSGP1BGP",this,"Tmp_Arti")
            * --- Se ripartizione documenti interni devo indicare il tipo di raggruppamento
            *     (sar� calcolato il fatturato per mese)
            this.w_TIPORAG = "KEYSAL"
            this.w_RAGUMI = "N"
          case alltrim(this.w_COKEYART) == alltrim(this.w_CODKEY) And _Curs_GSGP_BGP.COUMALTE<>1
            * --- Codice di ricerca interno e  flag UM alternative disattivo
            *     ragruppo per MVKEYSAL=COKEYSAL e MVUNIMIS=COUNIMIS
            VQ_EXEC ("..\GPFA\EXE\QUERY\GSGP4BGP",this,"Tmp_Arti")
            * --- Se ripartizione documenti interni devo indicare il tipo di raggruppamento
            *     (sar� calcolato il fatturato per mese)
            this.w_TIPORAG = "KEYSAL"
            this.w_RAGUMI = "S"
          case Not (alltrim(this.w_COKEYART) == alltrim(this.w_CODKEY)) And _Curs_GSGP_BGP.COUMALTE=1
            * --- Codice di ricerca NON interno e flag UM alternative attivo
            *     ragruppo per MVCODICE=COKEYART
            VQ_EXEC ("..\GPFA\EXE\QUERY\GSGP5BGP",this,"Tmp_Arti")
            * --- Se ripartizione documenti interni devo indicare il tipo di raggruppamento
            *     (sar� calcolato il fatturato per mese)
            this.w_TIPORAG = "KEYART"
            this.w_RAGUMI = "N"
          case Not (alltrim(this.w_COKEYART) == alltrim(this.w_CODKEY)) And _Curs_GSGP_BGP.COUMALTE <> 1
            * --- Codice di ricerca NON interno e flag UM alternative disattivo
            *     ragruppo per MVCODICE=COKEYART MVUNIMIS=COUNIMIS
            VQ_EXEC ("..\GPFA\EXE\QUERY\GSGP6BGP",this,"Tmp_Arti")
            * --- Se ripartizione documenti interni devo indicare il tipo di raggruppamento
            *     (sar� calcolato il fatturato per mese)
            this.w_TIPORAG = "KEYART"
            this.w_RAGUMI = "S"
        endcase
        if Reccount("Tmp_Arti") > 0
          * --- Inserisco le informazioni Articolo o codice ricerca + peso (a valore e quantit�)
          *     necessari per generazione documenti (caso ripartizione), la tabella consente 
          *     anche di selezionare in PREMIDOC i soli documenti effettivamente utilizzati
          *     ai fini della generazione premio - zoom dati consuntivi contratti
          * --- Insert into TMPPREMI
          i_nConn=i_TableProp[this.TMPPREMI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPPREMI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPPREMI_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"COCODICE"+",CPROWNUM"+",COKEYART"+",COKEYSAL"+",COCODART"+",COCODVAR"+",COUNIMIS"+",COATTRIB"+",COPRMLIQ"+",COTIPINT"+",COCODAGE"+",COTIPSTO"+",COTOTPUN"+",COSCOCOM"+",COPRMRIF"+",COCODGRU"+",COCODINT"+",PESOVAL"+",PESOQTA"+",PESOVALROW"+",PESOQTAROW"+",TOTVALPRE"+",TOTVALROW"+",CONOTGEN"+",COFLGINC"+",COFLGCIC"+",COCODVAL"+",CODATINI"+",CODATFIN"+",COTIPDOC"+",COPRMDOC"+",COCODIVA"+",CODATGEN"+",NUMRIG"+",TIPORAG"+",RAGUMI"+",PARTECIPA"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_APCONTR),'TMPPREMI','COCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_RIGAKEY),'TMPPREMI','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_COKEYART),'TMPPREMI','COKEYART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_COKEYSAL),'TMPPREMI','COKEYSAL');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COCODART),'TMPPREMI','COCODART');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COCODVAR),'TMPPREMI','COCODVAR');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COUNIMIS),'TMPPREMI','COUNIMIS');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COATTRIB),'TMPPREMI','COATTRIB');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COPRMLIQ),'TMPPREMI','COPRMLIQ');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COTIPINT),'TMPPREMI','COTIPINT');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COCODAGE),'TMPPREMI','COCODAGE');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COTIPSTO),'TMPPREMI','COTIPSTO');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COTOTPUN),'TMPPREMI','COTOTPUN');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COSCOCOM),'TMPPREMI','COSCOCOM');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COPRMRIF),'TMPPREMI','COPRMRIF');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COCODGRU),'TMPPREMI','COCODGRU');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COCODINT),'TMPPREMI','COCODINT');
            +","+cp_NullLink(cp_ToStrODBC(Tmp_Arti.VALCON),'TMPPREMI','PESOVAL');
            +","+cp_NullLink(cp_ToStrODBC(Tmp_Arti.QTACON),'TMPPREMI','PESOQTA');
            +","+cp_NullLink(cp_ToStrODBC(Tmp_Arti.VALCON),'TMPPREMI','PESOVALROW');
            +","+cp_NullLink(cp_ToStrODBC(Tmp_Arti.QTACON),'TMPPREMI','PESOQTAROW');
            +","+cp_NullLink(cp_ToStrODBC(0),'TMPPREMI','TOTVALPRE');
            +","+cp_NullLink(cp_ToStrODBC(0),'TMPPREMI','TOTVALROW');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.CONOTGEN),'TMPPREMI','CONOTGEN');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COFLGINC),'TMPPREMI','COFLGINC');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COFLGCIC),'TMPPREMI','COFLGCIC');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COCODVAL),'TMPPREMI','COCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.CODATINI),'TMPPREMI','CODATINI');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.CODATFIN),'TMPPREMI','CODATFIN');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COTIPDOC),'TMPPREMI','COTIPDOC');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COPRMDOC),'TMPPREMI','COPRMDOC');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.COCODIVA),'TMPPREMI','COCODIVA');
            +","+cp_NullLink(cp_ToStrODBC(_Curs_GSGP_BGP.CODATGEN),'TMPPREMI','CODATGEN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIG),'TMPPREMI','NUMRIG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TIPORAG),'TMPPREMI','TIPORAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_RAGUMI),'TMPPREMI','RAGUMI');
            +","+cp_NullLink(cp_ToStrODBC("S"),'TMPPREMI','PARTECIPA');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'COCODICE',this.w_APCONTR,'CPROWNUM',this.w_RIGAKEY,'COKEYART',this.w_COKEYART,'COKEYSAL',this.w_COKEYSAL,'COCODART',_Curs_GSGP_BGP.COCODART,'COCODVAR',_Curs_GSGP_BGP.COCODVAR,'COUNIMIS',_Curs_GSGP_BGP.COUNIMIS,'COATTRIB',_Curs_GSGP_BGP.COATTRIB,'COPRMLIQ',_Curs_GSGP_BGP.COPRMLIQ,'COTIPINT',_Curs_GSGP_BGP.COTIPINT,'COCODAGE',_Curs_GSGP_BGP.COCODAGE,'COTIPSTO',_Curs_GSGP_BGP.COTIPSTO)
            insert into (i_cTable) (COCODICE,CPROWNUM,COKEYART,COKEYSAL,COCODART,COCODVAR,COUNIMIS,COATTRIB,COPRMLIQ,COTIPINT,COCODAGE,COTIPSTO,COTOTPUN,COSCOCOM,COPRMRIF,COCODGRU,COCODINT,PESOVAL,PESOQTA,PESOVALROW,PESOQTAROW,TOTVALPRE,TOTVALROW,CONOTGEN,COFLGINC,COFLGCIC,COCODVAL,CODATINI,CODATFIN,COTIPDOC,COPRMDOC,COCODIVA,CODATGEN,NUMRIG,TIPORAG,RAGUMI,PARTECIPA &i_ccchkf. );
               values (;
                 this.w_APCONTR;
                 ,this.w_RIGAKEY;
                 ,this.w_COKEYART;
                 ,this.w_COKEYSAL;
                 ,_Curs_GSGP_BGP.COCODART;
                 ,_Curs_GSGP_BGP.COCODVAR;
                 ,_Curs_GSGP_BGP.COUNIMIS;
                 ,_Curs_GSGP_BGP.COATTRIB;
                 ,_Curs_GSGP_BGP.COPRMLIQ;
                 ,_Curs_GSGP_BGP.COTIPINT;
                 ,_Curs_GSGP_BGP.COCODAGE;
                 ,_Curs_GSGP_BGP.COTIPSTO;
                 ,_Curs_GSGP_BGP.COTOTPUN;
                 ,_Curs_GSGP_BGP.COSCOCOM;
                 ,_Curs_GSGP_BGP.COPRMRIF;
                 ,_Curs_GSGP_BGP.COCODGRU;
                 ,_Curs_GSGP_BGP.COCODINT;
                 ,Tmp_Arti.VALCON;
                 ,Tmp_Arti.QTACON;
                 ,Tmp_Arti.VALCON;
                 ,Tmp_Arti.QTACON;
                 ,0;
                 ,0;
                 ,_Curs_GSGP_BGP.CONOTGEN;
                 ,_Curs_GSGP_BGP.COFLGINC;
                 ,_Curs_GSGP_BGP.COFLGCIC;
                 ,_Curs_GSGP_BGP.COCODVAL;
                 ,_Curs_GSGP_BGP.CODATINI;
                 ,_Curs_GSGP_BGP.CODATFIN;
                 ,_Curs_GSGP_BGP.COTIPDOC;
                 ,_Curs_GSGP_BGP.COPRMDOC;
                 ,_Curs_GSGP_BGP.COCODIVA;
                 ,_Curs_GSGP_BGP.CODATGEN;
                 ,this.w_NUMRIG;
                 ,this.w_TIPORAG;
                 ,this.w_RAGUMI;
                 ,"S";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      else
        * --- Numero di attributi di modello ART_ICOL presenti
        this.w_NUMART = 0
        * --- Select from CCOMDATT
        i_nConn=i_TableProp[this.CCOMDATT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CCOMDATT_idx,2],.t.,this.CCOMDATT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select COUNT(*) AS NUMART  from "+i_cTable+" CCOMDATT ";
              +" where CA__GUID = "+cp_ToStrODBC(this.w_APCONTR)+" And CAROWRIF = "+cp_ToStrODBC(this.w_RIGAKEY)+"";
               ,"_Curs_CCOMDATT")
        else
          select COUNT(*) AS NUMART from (i_cTable);
           where CA__GUID = this.w_APCONTR And CAROWRIF = this.w_RIGAKEY;
            into cursor _Curs_CCOMDATT
        endif
        if used('_Curs_CCOMDATT')
          select _Curs_CCOMDATT
          locate for 1=1
          do while not(eof())
          * --- Totale attributi di riga
          this.w_NUMART = NVL(NUMART,0)
            select _Curs_CCOMDATT
            continue
          enddo
          use
        endif
        * --- Inserisco le informazioni Articolo o codice ricerca + peso (a valore e quantit�)
        *     necessari per generazione documenti (caso ripartizione), la tabella consente 
        *     anche di selezionare in PREMIDOC i soli documenti effettivamente utilizzati
        *     ai fini della generazione premio - zoom dati consuntivi contratti
        * --- Insert into TMPPREMI
        i_nConn=i_TableProp[this.TMPPREMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPREMI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\GPFA\EXE\QUERY\GSGP1TMP",this.TMPPREMI_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Totale per attributi (necessario per la generazione premi)
        VQ_EXEC ("..\GPFA\EXE\QUERY\GSGP2TMP",this,"Tmp_Arti")
      endif
      * --- Calcolo del premio
      this.w_PARTECIPA = "S"
      if Reccount("Tmp_Arti") > 0
        this.w_VALCON = Tmp_Arti.VALCON
        this.w_QTACON = Tmp_Arti.QTACON
        if this.w_FLGINC = 0
          * --- Disattivo flag sconti incondizionati
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- Flag sconti incondizionato attivo (sommo per ogni riga il valore)
          this.w_COVALFAT = this.w_COVALFAT + this.w_VALCON
          this.w_COTOTQTA = this.w_COTOTQTA + this.w_QTACON
        endif
      endif
      if this.w_PARTECIPA<>"S"
        * --- Write into TMPPREMI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.TMPPREMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPPREMI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPREMI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PARTECIPA ="+cp_NullLink(cp_ToStrODBC(this.w_PARTECIPA),'TMPPREMI','PARTECIPA');
              +i_ccchkf ;
          +" where ";
              +"COCODICE = "+cp_ToStrODBC(this.w_APCONTR);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_RIGAKEY);
                 )
        else
          update (i_cTable) set;
              PARTECIPA = this.w_PARTECIPA;
              &i_ccchkf. ;
           where;
              COCODICE = this.w_APCONTR;
              and CPROWNUM = this.w_RIGAKEY;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_GSGP_BGP
        continue
      enddo
      use
    endif
    * --- Terminato l'ultimo contratto della lista, aggiorno i totali 
    this.w_COPREZZO = this.w_COVALFAT * (1- this.w_SC1 /100)*(1 - this.w_SC2 /100)*(1- this.w_SC3 /100)*(1- this.w_SC4 /100)*(1- this.w_SC5 /100)*(1- this.w_SC6 /100)
    this.w_COSCOCOM = this.w_COVALFAT - this.w_COPREZZO
    this.w_COVALPRE = this.w_TOTVALPRE + (this.w_COTOTPUN * this.w_APVALPRE)
    this.w_COTOTPRE = this.w_COSCOCOM+this.w_COVALPRE
    if this.oParentObject.w_GPDOCFIS="S" Or this.oParentObject.w_GPDOCINT="S"
      * --- Aggiorno il totale premio valore  TOTVALPRE
      * --- Write into TMPPREMI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPPREMI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPPREMI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPPREMI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"TOTVALPRE ="+cp_NullLink(cp_ToStrODBC(this.w_TOTVALPRE),'TMPPREMI','TOTVALPRE');
        +",PESOVALROW ="+cp_NullLink(cp_ToStrODBC(this.w_COVALFAT),'TMPPREMI','PESOVALROW');
        +",PESOQTAROW ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTQTA),'TMPPREMI','PESOQTAROW');
        +",COSCOCOM ="+cp_NullLink(cp_ToStrODBC(this.w_COSCOCOM),'TMPPREMI','COSCOCOM');
        +",COTOTPUN ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTPUN),'TMPPREMI','COTOTPUN');
        +",NUMRIG ="+cp_NullLink(cp_ToStrODBC(this.w_NUMRIG),'TMPPREMI','NUMRIG');
            +i_ccchkf ;
        +" where ";
            +"COCODICE = "+cp_ToStrODBC(this.w_APCONTR);
               )
      else
        update (i_cTable) set;
            TOTVALPRE = this.w_TOTVALPRE;
            ,PESOVALROW = this.w_COVALFAT;
            ,PESOQTAROW = this.w_COTOTQTA;
            ,COSCOCOM = this.w_COSCOCOM;
            ,COTOTPUN = this.w_COTOTPUN;
            ,NUMRIG = this.w_NUMRIG;
            &i_ccchkf. ;
         where;
            COCODICE = this.w_APCONTR;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Verifico se sono presenti i documenti che hanno generato del fatturato 
    *     per il contratto in questione
    * --- Read from PREMIDOC
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PREMIDOC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PREMIDOC_idx,2],.t.,this.PREMIDOC_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" PREMIDOC where ";
            +"SERGPREM = "+cp_ToStrODBC(this.oParentObject.w_GPSERIAL);
            +" and RIFCONTR = "+cp_ToStrODBC(this.w_APCONTR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            SERGPREM = this.oParentObject.w_GPSERIAL;
            and RIFCONTR = this.w_APCONTR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_Rows > 0
      this.oParentObject.w_oERRORLOG=createobject("AH_ErrorLog")
      if this.oParentObject.w_GPSTATUS = "S"
        * --- Simulata
        * --- Write into CCOMMAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CCOMMAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CCOMMAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CCOMMAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COVALFAT ="+cp_NullLink(cp_ToStrODBC(this.w_COVALFAT),'CCOMMAST','COVALFAT');
          +",COSCOCOM ="+cp_NullLink(cp_ToStrODBC(this.w_COSCOCOM),'CCOMMAST','COSCOCOM');
          +",COTOTPUN ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTPUN),'CCOMMAST','COTOTPUN');
          +",COVALPRE ="+cp_NullLink(cp_ToStrODBC(this.w_COVALPRE),'CCOMMAST','COVALPRE');
          +",COTOTPRE ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTPRE),'CCOMMAST','COTOTPRE');
          +",CODATGEN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GPDATREG),'CCOMMAST','CODATGEN');
              +i_ccchkf ;
          +" where ";
              +"COCODICE = "+cp_ToStrODBC(this.w_APCONTR);
                 )
        else
          update (i_cTable) set;
              COVALFAT = this.w_COVALFAT;
              ,COSCOCOM = this.w_COSCOCOM;
              ,COTOTPUN = this.w_COTOTPUN;
              ,COVALPRE = this.w_COVALPRE;
              ,COTOTPRE = this.w_COTOTPRE;
              ,CODATGEN = this.oParentObject.w_GPDATREG;
              &i_ccchkf. ;
           where;
              COCODICE = this.w_APCONTR;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        if this.w_COPRMLIQ $ "FA"
          * --- Imposto Elaborato
          * --- Write into CCOMMAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CCOMMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CCOMMAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CCOMMAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"COVALFAT ="+cp_NullLink(cp_ToStrODBC(this.w_COVALFAT),'CCOMMAST','COVALFAT');
            +",COSCOCOM ="+cp_NullLink(cp_ToStrODBC(this.w_COSCOCOM),'CCOMMAST','COSCOCOM');
            +",COTOTPUN ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTPUN),'CCOMMAST','COTOTPUN');
            +",COVALPRE ="+cp_NullLink(cp_ToStrODBC(this.w_COVALPRE),'CCOMMAST','COVALPRE');
            +",COTOTPRE ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTPRE),'CCOMMAST','COTOTPRE');
            +",CODATGEN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GPDATREG),'CCOMMAST','CODATGEN');
            +",COFLGSTA ="+cp_NullLink(cp_ToStrODBC("E"),'CCOMMAST','COFLGSTA');
                +i_ccchkf ;
            +" where ";
                +"COCODICE = "+cp_ToStrODBC(this.w_APCONTR);
                   )
          else
            update (i_cTable) set;
                COVALFAT = this.w_COVALFAT;
                ,COSCOCOM = this.w_COSCOCOM;
                ,COTOTPUN = this.w_COTOTPUN;
                ,COVALPRE = this.w_COVALPRE;
                ,COTOTPRE = this.w_COTOTPRE;
                ,CODATGEN = this.oParentObject.w_GPDATREG;
                ,COFLGSTA = "E";
                &i_ccchkf. ;
             where;
                COCODICE = this.w_APCONTR;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          if this.oParentObject.w_GPDATFIN=this.w_CODATFIN
            * --- Imposto Elaborato
            * --- Write into CCOMMAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CCOMMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CCOMMAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CCOMMAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"COVALFAT ="+cp_NullLink(cp_ToStrODBC(this.w_COVALFAT),'CCOMMAST','COVALFAT');
              +",COSCOCOM ="+cp_NullLink(cp_ToStrODBC(this.w_COSCOCOM),'CCOMMAST','COSCOCOM');
              +",COTOTPUN ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTPUN),'CCOMMAST','COTOTPUN');
              +",COVALPRE ="+cp_NullLink(cp_ToStrODBC(this.w_COVALPRE),'CCOMMAST','COVALPRE');
              +",COTOTPRE ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTPRE),'CCOMMAST','COTOTPRE');
              +",CODATGEN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GPDATREG),'CCOMMAST','CODATGEN');
              +",COFLGSTA ="+cp_NullLink(cp_ToStrODBC("E"),'CCOMMAST','COFLGSTA');
                  +i_ccchkf ;
              +" where ";
                  +"COCODICE = "+cp_ToStrODBC(this.w_APCONTR);
                     )
            else
              update (i_cTable) set;
                  COVALFAT = this.w_COVALFAT;
                  ,COSCOCOM = this.w_COSCOCOM;
                  ,COTOTPUN = this.w_COTOTPUN;
                  ,COVALPRE = this.w_COVALPRE;
                  ,COTOTPRE = this.w_COTOTPRE;
                  ,CODATGEN = this.oParentObject.w_GPDATREG;
                  ,COFLGSTA = "E";
                  &i_ccchkf. ;
               where;
                  COCODICE = this.w_APCONTR;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Write into CCOMMAST
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.CCOMMAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CCOMMAST_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.CCOMMAST_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"COVALFAT ="+cp_NullLink(cp_ToStrODBC(this.w_COVALFAT),'CCOMMAST','COVALFAT');
              +",COSCOCOM ="+cp_NullLink(cp_ToStrODBC(this.w_COSCOCOM),'CCOMMAST','COSCOCOM');
              +",COTOTPUN ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTPUN),'CCOMMAST','COTOTPUN');
              +",COVALPRE ="+cp_NullLink(cp_ToStrODBC(this.w_COVALPRE),'CCOMMAST','COVALPRE');
              +",COTOTPRE ="+cp_NullLink(cp_ToStrODBC(this.w_COTOTPRE),'CCOMMAST','COTOTPRE');
              +",CODATGEN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GPDATREG),'CCOMMAST','CODATGEN');
                  +i_ccchkf ;
              +" where ";
                  +"COCODICE = "+cp_ToStrODBC(this.w_APCONTR);
                     )
            else
              update (i_cTable) set;
                  COVALFAT = this.w_COVALFAT;
                  ,COSCOCOM = this.w_COSCOCOM;
                  ,COTOTPUN = this.w_COTOTPUN;
                  ,COVALPRE = this.w_COVALPRE;
                  ,COTOTPRE = this.w_COTOTPRE;
                  ,CODATGEN = this.oParentObject.w_GPDATREG;
                  &i_ccchkf. ;
               where;
                  COCODICE = this.w_APCONTR;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      endif
      * --- GENERAZIONE DOCUMENTI
      * --- Documento fiscale
      if this.oParentObject.w_GPDOCFIS="S" Or this.oParentObject.w_GPDOCINT="S"
        if AH_YESNO("Generazione premi terminata, confermi la generazione dei documenti?")
          this.w_GDKEYRIF = SPACE(10)
          * --- Preparazione tabelle temporanee generatore documentale
          if this.oParentObject.w_GPDOCFIS="S"
            ah_msg( "Generazione documenti fiscali")
            * --- Teporaneo documenti
            This.oParentObject.w_SERGDOC=this.w_SERGDOC
            This.oParentObject.NotifyEvent("TmpDocFis")
          endif
          if this.oParentObject.w_GPDOCINT="S"
            * --- Teporaneo documenti
            if this.oParentObject.w_GPDOCFIS="S"
              if AH_YESNO("Confermi la generazione dei documenti interni di rettifica?")
                ah_msg( "Generazione documenti interni")
                This.oParentObject.w_SERGDOC=this.w_SERGDOC
                This.oParentObject.NotifyEvent("TmpDocInt")
              endif
            else
              ah_msg( "Generazione documenti interni")
              This.oParentObject.w_SERGDOC=this.w_SERGDOC
              This.oParentObject.NotifyEvent("TmpDocInt")
            endif
          endif
        endif
      else
        AH_ERRORMSG("Generazione premi terminata.",64,"")
      endif
      if this.oParentObject.w_oERRORLOG.IsFullLog()
        this.oParentObject.w_oERRORLOG.PrintLog(this.oParentObject,"Errori di generazione dei documenti",.T., "Si vogliono visualizzare gli errori riscontrati in fase di generazione?")     
      endif
    else
      * --- Verifico se � presente o meno almeno un documento in assoluto
      * --- Read from PREMIDOC
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PREMIDOC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PREMIDOC_idx,2],.t.,this.PREMIDOC_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" PREMIDOC where ";
              +"SERGPREM = "+cp_ToStrODBC(this.oParentObject.w_GPSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              SERGPREM = this.oParentObject.w_GPSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows=0
        * --- Non sono presenti documenti di origine:
        AH_ERRORMSG("Non sono presenti documenti necessari al calcolo del premio.",64,"")
      endif
    endif
    * --- Nella tabella PREMIDOC restara soltanto l'elenco dei documenti (testate) con il codice del contratto
    *     elebarato.La tabella sar� utilizzata nello zoom dei dati consuntivi del contratto stesso
    * --- Insert into PREMIDOC
    i_nConn=i_TableProp[this.PREMIDOC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PREMIDOC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"..\GPFA\EXE\QUERY\GSGP7BGP",this.PREMIDOC_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Elimino tutti i dettagli elaborati (righe documento)
    * --- Delete from PREMIDOC
    i_nConn=i_TableProp[this.PREMIDOC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PREMIDOC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"SERGPREM = "+cp_ToStrODBC(this.oParentObject.w_GPSERIAL);
            +" and CPROWNUM > "+cp_ToStrODBC(0);
             )
    else
      delete from (i_cTable) where;
            SERGPREM = this.oParentObject.w_GPSERIAL;
            and CPROWNUM > 0;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Drop temporary table TMPPREMI
    i_nIdx=cp_GetTableDefIdx('TMPPREMI')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPPREMI')
    endif
    if Used("Tmp_Arti")
      Select Tmp_Arti 
 Use
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CALCOLO DEL PREMIO
    *     Entriamo nel dettaglio degli SCAGLIONI PREMIO
    * --- Calcolo premio Quantit�/Valore
    this.w_CONTA = 0
    this.w_PROGR = 0
    this.w_PCSCAPRM = 0
    this.w_OKVALFAT = .t.
    this.w_PARTECIPA = "S"
    if _Curs_GSGP_BGP.COCALPRM = "Q"
      do case
        case _Curs_GSGP_BGP.COSCAPRE="P"
          * --- Elenco scaglioni inferiori a QTACON
          * --- Select from PREMICON
          i_nConn=i_TableProp[this.PREMICON_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT(*) AS CONTA  from "+i_cTable+" PREMICON ";
                +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And PCROWNUM="+cp_ToStrODBC(this.w_RIGAKEY)+" And PCSCAPRM < "+cp_ToStrODBC(this.w_QTACON)+"";
                +" group by CCCODICE,PCROWNUM";
                 ,"_Curs_PREMICON")
          else
            select COUNT(*) AS CONTA from (i_cTable);
             where CCCODICE = this.w_APCONTR And PCROWNUM=this.w_RIGAKEY And PCSCAPRM < this.w_QTACON;
             group by CCCODICE,PCROWNUM;
              into cursor _Curs_PREMICON
          endif
          if used('_Curs_PREMICON')
            select _Curs_PREMICON
            locate for 1=1
            do while not(eof())
            this.w_CONTA = NVL(CONTA,0)
              select _Curs_PREMICON
              continue
            enddo
            use
          endif
          if this.w_CONTA > 0
            * --- Select from PREMICON
            i_nConn=i_TableProp[this.PREMICON_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" PREMICON ";
                  +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And PCROWNUM="+cp_ToStrODBC(this.w_RIGAKEY)+" And PCSCAPRM < "+cp_ToStrODBC(this.w_QTACON)+"";
                  +" order by PCSCAPRM";
                   ,"_Curs_PREMICON")
            else
              select * from (i_cTable);
               where CCCODICE = this.w_APCONTR And PCROWNUM=this.w_RIGAKEY And PCSCAPRM < this.w_QTACON;
               order by PCSCAPRM;
                into cursor _Curs_PREMICON
            endif
            if used('_Curs_PREMICON')
              select _Curs_PREMICON
              locate for 1=1
              do while not(eof())
              * --- Aggiorno sulle righe premio i campi PCQTACON, PCVALCON
              * --- Progressivo riga per determinare scaglione maggiore
              this.w_PROGR = this.w_PROGR+1
              if this.w_PROGR < this.w_CONTA
                * --- Non � lo scaglione maggiore 
                this.w_PCQTACON = _Curs_PREMICON.PCSCAPRM - this.w_PCSCAPRM
                this.w_PCVALCON = cp_ROUND((this.w_VALCON/this.w_QTACON) * this.w_PCQTACON, 5)
                this.w_PCSCAPRM = _Curs_PREMICON.PCSCAPRM
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                * --- Scagione a quantit� maggiore
                this.w_PCQTACON = this.w_QTACON - this.w_PCSCAPRM
                this.w_PCVALCON = cp_ROUND((this.w_VALCON/this.w_QTACON) * this.w_PCQTACON,5)
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
                select _Curs_PREMICON
                continue
              enddo
              use
            endif
          else
            * --- Non ho raggiunto lo scaglione scrivo comunque la quantit� venduta e il fatturato
            if this.oParentObject.w_GPSTATUS = "S"
              * --- Select from PREMICON
              i_nConn=i_TableProp[this.PREMICON_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select * from "+i_cTable+" PREMICON ";
                    +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And PCROWNUM="+cp_ToStrODBC(this.w_RIGAKEY)+"";
                    +" order by CPROWNUM";
                     ,"_Curs_PREMICON")
              else
                select * from (i_cTable);
                 where CCCODICE = this.w_APCONTR And PCROWNUM=this.w_RIGAKEY;
                 order by CPROWNUM;
                  into cursor _Curs_PREMICON
              endif
              if used('_Curs_PREMICON')
                select _Curs_PREMICON
                locate for 1=1
                do while not(eof())
                * --- Write into PREMICON
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PREMICON_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PREMICON_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PCQTACON ="+cp_NullLink(cp_ToStrODBC(this.w_QTACON),'PREMICON','PCQTACON');
                  +",PCVALCON ="+cp_NullLink(cp_ToStrODBC(this.w_VALCON),'PREMICON','PCVALCON');
                      +i_ccchkf ;
                  +" where ";
                      +"CCCODICE = "+cp_ToStrODBC(this.w_APCONTR);
                      +" and PCROWNUM = "+cp_ToStrODBC(this.w_RIGAKEY);
                      +" and CPROWNUM = "+cp_ToStrODBC(_Curs_PREMICON.CPROWNUM);
                         )
                else
                  update (i_cTable) set;
                      PCQTACON = this.w_QTACON;
                      ,PCVALCON = this.w_VALCON;
                      &i_ccchkf. ;
                   where;
                      CCCODICE = this.w_APCONTR;
                      and PCROWNUM = this.w_RIGAKEY;
                      and CPROWNUM = _Curs_PREMICON.CPROWNUM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- Primo record ..esco
                exit
                  select _Curs_PREMICON
                  continue
                enddo
                use
              endif
            endif
          endif
        case _Curs_GSGP_BGP.COSCAPRE="S"
          * --- Su Scagione max
          * --- Elenco scaglioni inferiori a QTACON
          * --- Select from PREMICON
          i_nConn=i_TableProp[this.PREMICON_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT(*) AS CONTA  from "+i_cTable+" PREMICON ";
                +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And PCROWNUM="+cp_ToStrODBC(this.w_RIGAKEY)+" And PCSCAPRM < "+cp_ToStrODBC(this.w_QTACON)+"";
                +" group by CCCODICE,PCROWNUM";
                 ,"_Curs_PREMICON")
          else
            select COUNT(*) AS CONTA from (i_cTable);
             where CCCODICE = this.w_APCONTR And PCROWNUM=this.w_RIGAKEY And PCSCAPRM < this.w_QTACON;
             group by CCCODICE,PCROWNUM;
              into cursor _Curs_PREMICON
          endif
          if used('_Curs_PREMICON')
            select _Curs_PREMICON
            locate for 1=1
            do while not(eof())
            this.w_CONTA = NVL(CONTA,0)
              select _Curs_PREMICON
              continue
            enddo
            use
          endif
          if this.w_CONTA > 0
            * --- Select from PREMICON
            i_nConn=i_TableProp[this.PREMICON_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" PREMICON ";
                  +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And PCROWNUM="+cp_ToStrODBC(this.w_RIGAKEY)+" And PCSCAPRM < "+cp_ToStrODBC(this.w_QTACON)+"";
                  +" order by PCSCAPRM desc";
                   ,"_Curs_PREMICON")
            else
              select * from (i_cTable);
               where CCCODICE = this.w_APCONTR And PCROWNUM=this.w_RIGAKEY And PCSCAPRM < this.w_QTACON;
               order by PCSCAPRM desc;
                into cursor _Curs_PREMICON
            endif
            if used('_Curs_PREMICON')
              select _Curs_PREMICON
              locate for 1=1
              do while not(eof())
              this.w_PCQTACON = this.w_QTACON
              this.w_PCVALCON = this.w_VALCON
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              exit
                select _Curs_PREMICON
                continue
              enddo
              use
            endif
          else
            * --- Non ho raggiunto lo scaglione scrivo comunque la quantit� venduta e il fatturato
            if this.oParentObject.w_GPSTATUS = "S"
              * --- Select from PREMICON
              i_nConn=i_TableProp[this.PREMICON_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select * from "+i_cTable+" PREMICON ";
                    +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And PCROWNUM="+cp_ToStrODBC(this.w_RIGAKEY)+"";
                    +" order by CPROWNUM";
                     ,"_Curs_PREMICON")
              else
                select * from (i_cTable);
                 where CCCODICE = this.w_APCONTR And PCROWNUM=this.w_RIGAKEY;
                 order by CPROWNUM;
                  into cursor _Curs_PREMICON
              endif
              if used('_Curs_PREMICON')
                select _Curs_PREMICON
                locate for 1=1
                do while not(eof())
                * --- Write into PREMICON
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PREMICON_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PREMICON_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PCQTACON ="+cp_NullLink(cp_ToStrODBC(this.w_QTACON),'PREMICON','PCQTACON');
                  +",PCVALCON ="+cp_NullLink(cp_ToStrODBC(this.w_VALCON),'PREMICON','PCVALCON');
                      +i_ccchkf ;
                  +" where ";
                      +"CCCODICE = "+cp_ToStrODBC(this.w_APCONTR);
                      +" and PCROWNUM = "+cp_ToStrODBC(this.w_RIGAKEY);
                      +" and CPROWNUM = "+cp_ToStrODBC(_Curs_PREMICON.CPROWNUM);
                         )
                else
                  update (i_cTable) set;
                      PCQTACON = this.w_QTACON;
                      ,PCVALCON = this.w_VALCON;
                      &i_ccchkf. ;
                   where;
                      CCCODICE = this.w_APCONTR;
                      and PCROWNUM = this.w_RIGAKEY;
                      and CPROWNUM = _Curs_PREMICON.CPROWNUM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- Primo record ..esco
                exit
                  select _Curs_PREMICON
                  continue
                enddo
                use
              endif
            endif
          endif
        case _Curs_GSGP_BGP.COSCAPRE="C"
          * --- Cumulativo
          * --- Elenco scaglioni inferiori a QTACON
          * --- Select from PREMICON
          i_nConn=i_TableProp[this.PREMICON_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT(*) AS CONTA  from "+i_cTable+" PREMICON ";
                +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And PCROWNUM="+cp_ToStrODBC(this.w_RIGAKEY)+" And PCSCAPRM < "+cp_ToStrODBC(this.w_QTACON)+"";
                +" group by CCCODICE,PCROWNUM";
                 ,"_Curs_PREMICON")
          else
            select COUNT(*) AS CONTA from (i_cTable);
             where CCCODICE = this.w_APCONTR And PCROWNUM=this.w_RIGAKEY And PCSCAPRM < this.w_QTACON;
             group by CCCODICE,PCROWNUM;
              into cursor _Curs_PREMICON
          endif
          if used('_Curs_PREMICON')
            select _Curs_PREMICON
            locate for 1=1
            do while not(eof())
            this.w_CONTA = NVL(CONTA,0)
              select _Curs_PREMICON
              continue
            enddo
            use
          endif
          if this.w_CONTA > 0
            * --- Select from PREMICON
            i_nConn=i_TableProp[this.PREMICON_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" PREMICON ";
                  +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And PCROWNUM="+cp_ToStrODBC(this.w_RIGAKEY)+" And PCSCAPRM < "+cp_ToStrODBC(this.w_QTACON)+"";
                  +" order by PCSCAPRM";
                   ,"_Curs_PREMICON")
            else
              select * from (i_cTable);
               where CCCODICE = this.w_APCONTR And PCROWNUM=this.w_RIGAKEY And PCSCAPRM < this.w_QTACON;
               order by PCSCAPRM;
                into cursor _Curs_PREMICON
            endif
            if used('_Curs_PREMICON')
              select _Curs_PREMICON
              locate for 1=1
              do while not(eof())
              * --- Aggiorno sulle righe premio i campi PCQTACON, PCVALCON
              * --- Progressivo riga per determinare scaglione maggiore
              this.w_PROGR = this.w_PROGR+1
              if this.w_PROGR < this.w_CONTA
                * --- Non � lo scaglione maggiore 
                this.w_PCQTACON = _Curs_PREMICON.PCSCAPRM
                this.w_PCVALCON = cp_ROUND((this.w_VALCON/this.w_QTACON) * this.w_PCQTACON,5)
                this.w_OKVALFAT = .f.
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                * --- Scagione a quantit� maggiore
                this.w_PCQTACON = this.w_QTACON
                this.w_PCVALCON = cp_ROUND((this.w_VALCON/this.w_QTACON) * this.w_PCQTACON,5)
                this.w_OKVALFAT = .t.
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
                select _Curs_PREMICON
                continue
              enddo
              use
            endif
          else
            * --- Non ho raggiunto lo scaglione scrivo comunque la quantit� venduta e il fatturato
            if this.oParentObject.w_GPSTATUS = "S"
              * --- Select from PREMICON
              i_nConn=i_TableProp[this.PREMICON_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select * from "+i_cTable+" PREMICON ";
                    +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And PCROWNUM="+cp_ToStrODBC(this.w_RIGAKEY)+"";
                    +" order by CPROWNUM";
                     ,"_Curs_PREMICON")
              else
                select * from (i_cTable);
                 where CCCODICE = this.w_APCONTR And PCROWNUM=this.w_RIGAKEY;
                 order by CPROWNUM;
                  into cursor _Curs_PREMICON
              endif
              if used('_Curs_PREMICON')
                select _Curs_PREMICON
                locate for 1=1
                do while not(eof())
                * --- Write into PREMICON
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PREMICON_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PREMICON_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PCQTACON ="+cp_NullLink(cp_ToStrODBC(this.w_QTACON),'PREMICON','PCQTACON');
                  +",PCVALCON ="+cp_NullLink(cp_ToStrODBC(this.w_VALCON),'PREMICON','PCVALCON');
                      +i_ccchkf ;
                  +" where ";
                      +"CCCODICE = "+cp_ToStrODBC(this.w_APCONTR);
                      +" and PCROWNUM = "+cp_ToStrODBC(this.w_RIGAKEY);
                      +" and CPROWNUM = "+cp_ToStrODBC(_Curs_PREMICON.CPROWNUM);
                         )
                else
                  update (i_cTable) set;
                      PCQTACON = this.w_QTACON;
                      ,PCVALCON = this.w_VALCON;
                      &i_ccchkf. ;
                   where;
                      CCCODICE = this.w_APCONTR;
                      and PCROWNUM = this.w_RIGAKEY;
                      and CPROWNUM = _Curs_PREMICON.CPROWNUM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- Primo record ..esco
                exit
                  select _Curs_PREMICON
                  continue
                enddo
                use
              endif
            endif
          endif
      endcase
    else
      * --- A valore
      do case
        case _Curs_GSGP_BGP.COSCAPRE="P"
          * --- Elenco scaglioni inferiori a VALCON
          * --- Select from PREMICON
          i_nConn=i_TableProp[this.PREMICON_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT(*) AS CONTA  from "+i_cTable+" PREMICON ";
                +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And PCROWNUM="+cp_ToStrODBC(this.w_RIGAKEY)+" And PCSCAPRM < "+cp_ToStrODBC(this.w_VALCON)+"";
                +" group by CCCODICE,PCROWNUM";
                 ,"_Curs_PREMICON")
          else
            select COUNT(*) AS CONTA from (i_cTable);
             where CCCODICE = this.w_APCONTR And PCROWNUM=this.w_RIGAKEY And PCSCAPRM < this.w_VALCON;
             group by CCCODICE,PCROWNUM;
              into cursor _Curs_PREMICON
          endif
          if used('_Curs_PREMICON')
            select _Curs_PREMICON
            locate for 1=1
            do while not(eof())
            this.w_CONTA = NVL(CONTA,0)
              select _Curs_PREMICON
              continue
            enddo
            use
          endif
          if this.w_CONTA > 0
            * --- Select from PREMICON
            i_nConn=i_TableProp[this.PREMICON_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" PREMICON ";
                  +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And PCROWNUM="+cp_ToStrODBC(this.w_RIGAKEY)+" And PCSCAPRM <"+cp_ToStrODBC(this.w_VALCON)+"";
                  +" order by PCSCAPRM";
                   ,"_Curs_PREMICON")
            else
              select * from (i_cTable);
               where CCCODICE = this.w_APCONTR And PCROWNUM=this.w_RIGAKEY And PCSCAPRM <this.w_VALCON;
               order by PCSCAPRM;
                into cursor _Curs_PREMICON
            endif
            if used('_Curs_PREMICON')
              select _Curs_PREMICON
              locate for 1=1
              do while not(eof())
              * --- Aggiorno sulle righe premio i campi PCQTACON, PCVALCON
              * --- Progressivo riga per determinare scaglione maggiore
              this.w_PROGR = this.w_PROGR+1
              if this.w_PROGR < this.w_CONTA
                * --- Non � lo scaglione maggiore 
                this.w_PCVALCON = _Curs_PREMICON.PCSCAPRM - this.w_PCSCAPRM
                this.w_PCQTACON = cp_ROUND((this.w_QTACON/this.w_VALCON) * this.w_PCVALCON, g_PERPQT)
                if this.w_FLFRAZ = "S"
                  this.w_PCQTACON = INT(this.w_PCQTACON)
                endif
                this.w_PCSCAPRM = _Curs_PREMICON.PCSCAPRM
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                * --- Scagione a quantit� maggiore
                this.w_PCVALCON = this.w_VALCON - this.w_PCSCAPRM
                this.w_PCQTACON = cp_ROUND((this.w_QTACON/this.w_VALCON) * this.w_PCVALCON, g_PERPQT)
                if this.w_FLFRAZ = "S"
                  this.w_PCQTACON = INT(this.w_PCQTACON)
                endif
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
                select _Curs_PREMICON
                continue
              enddo
              use
            endif
          else
            * --- Non ho raggiunto lo scaglione scrivo comunque la quantit� venduta e il fatturato
            if this.oParentObject.w_GPSTATUS = "S"
              * --- Select from PREMICON
              i_nConn=i_TableProp[this.PREMICON_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select * from "+i_cTable+" PREMICON ";
                    +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And PCROWNUM="+cp_ToStrODBC(this.w_RIGAKEY)+"";
                    +" order by CPROWNUM";
                     ,"_Curs_PREMICON")
              else
                select * from (i_cTable);
                 where CCCODICE = this.w_APCONTR And PCROWNUM=this.w_RIGAKEY;
                 order by CPROWNUM;
                  into cursor _Curs_PREMICON
              endif
              if used('_Curs_PREMICON')
                select _Curs_PREMICON
                locate for 1=1
                do while not(eof())
                * --- Write into PREMICON
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PREMICON_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PREMICON_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PCQTACON ="+cp_NullLink(cp_ToStrODBC(this.w_QTACON),'PREMICON','PCQTACON');
                  +",PCVALCON ="+cp_NullLink(cp_ToStrODBC(this.w_VALCON),'PREMICON','PCVALCON');
                      +i_ccchkf ;
                  +" where ";
                      +"CCCODICE = "+cp_ToStrODBC(this.w_APCONTR);
                      +" and PCROWNUM = "+cp_ToStrODBC(this.w_RIGAKEY);
                      +" and CPROWNUM = "+cp_ToStrODBC(_Curs_PREMICON.CPROWNUM);
                         )
                else
                  update (i_cTable) set;
                      PCQTACON = this.w_QTACON;
                      ,PCVALCON = this.w_VALCON;
                      &i_ccchkf. ;
                   where;
                      CCCODICE = this.w_APCONTR;
                      and PCROWNUM = this.w_RIGAKEY;
                      and CPROWNUM = _Curs_PREMICON.CPROWNUM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- Primo record ..esco
                exit
                  select _Curs_PREMICON
                  continue
                enddo
                use
              endif
            endif
          endif
        case _Curs_GSGP_BGP.COSCAPRE="S"
          * --- Su Scagione max
          * --- Select from PREMICON
          i_nConn=i_TableProp[this.PREMICON_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT(*) AS CONTA  from "+i_cTable+" PREMICON ";
                +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And PCROWNUM="+cp_ToStrODBC(this.w_RIGAKEY)+" And PCSCAPRM < "+cp_ToStrODBC(this.w_VALCON)+"";
                +" group by CCCODICE,PCROWNUM";
                 ,"_Curs_PREMICON")
          else
            select COUNT(*) AS CONTA from (i_cTable);
             where CCCODICE = this.w_APCONTR And PCROWNUM=this.w_RIGAKEY And PCSCAPRM < this.w_VALCON;
             group by CCCODICE,PCROWNUM;
              into cursor _Curs_PREMICON
          endif
          if used('_Curs_PREMICON')
            select _Curs_PREMICON
            locate for 1=1
            do while not(eof())
            this.w_CONTA = NVL(CONTA,0)
              select _Curs_PREMICON
              continue
            enddo
            use
          endif
          if this.w_CONTA >0
            * --- Select from PREMICON
            i_nConn=i_TableProp[this.PREMICON_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" PREMICON ";
                  +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And PCROWNUM="+cp_ToStrODBC(this.w_RIGAKEY)+" And PCSCAPRM < "+cp_ToStrODBC(this.w_VALCON)+"";
                  +" order by PCSCAPRM desc";
                   ,"_Curs_PREMICON")
            else
              select * from (i_cTable);
               where CCCODICE = this.w_APCONTR And PCROWNUM=this.w_RIGAKEY And PCSCAPRM < this.w_VALCON;
               order by PCSCAPRM desc;
                into cursor _Curs_PREMICON
            endif
            if used('_Curs_PREMICON')
              select _Curs_PREMICON
              locate for 1=1
              do while not(eof())
              this.w_PCQTACON = this.w_QTACON
              this.w_PCVALCON = this.w_VALCON
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              exit
                select _Curs_PREMICON
                continue
              enddo
              use
            endif
          else
            * --- Non ho raggiunto lo scaglione scrivo comunque la quantit� venduta e il fatturato
            if this.oParentObject.w_GPSTATUS = "S"
              * --- Select from PREMICON
              i_nConn=i_TableProp[this.PREMICON_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select * from "+i_cTable+" PREMICON ";
                    +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And PCROWNUM="+cp_ToStrODBC(this.w_RIGAKEY)+"";
                    +" order by CPROWNUM";
                     ,"_Curs_PREMICON")
              else
                select * from (i_cTable);
                 where CCCODICE = this.w_APCONTR And PCROWNUM=this.w_RIGAKEY;
                 order by CPROWNUM;
                  into cursor _Curs_PREMICON
              endif
              if used('_Curs_PREMICON')
                select _Curs_PREMICON
                locate for 1=1
                do while not(eof())
                * --- Write into PREMICON
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PREMICON_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PREMICON_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PCQTACON ="+cp_NullLink(cp_ToStrODBC(this.w_QTACON),'PREMICON','PCQTACON');
                  +",PCVALCON ="+cp_NullLink(cp_ToStrODBC(this.w_VALCON),'PREMICON','PCVALCON');
                      +i_ccchkf ;
                  +" where ";
                      +"CCCODICE = "+cp_ToStrODBC(this.w_APCONTR);
                      +" and PCROWNUM = "+cp_ToStrODBC(this.w_RIGAKEY);
                      +" and CPROWNUM = "+cp_ToStrODBC(_Curs_PREMICON.CPROWNUM);
                         )
                else
                  update (i_cTable) set;
                      PCQTACON = this.w_QTACON;
                      ,PCVALCON = this.w_VALCON;
                      &i_ccchkf. ;
                   where;
                      CCCODICE = this.w_APCONTR;
                      and PCROWNUM = this.w_RIGAKEY;
                      and CPROWNUM = _Curs_PREMICON.CPROWNUM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- Primo record ..esco
                exit
                  select _Curs_PREMICON
                  continue
                enddo
                use
              endif
            endif
          endif
        case _Curs_GSGP_BGP.COSCAPRE="C"
          * --- Cumulativo
          * --- Elenco scaglioni inferiori a VALCON
          * --- Select from PREMICON
          i_nConn=i_TableProp[this.PREMICON_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT(*) AS CONTA  from "+i_cTable+" PREMICON ";
                +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And PCROWNUM="+cp_ToStrODBC(this.w_RIGAKEY)+" And PCSCAPRM < "+cp_ToStrODBC(this.w_VALCON)+"";
                +" group by CCCODICE,PCROWNUM";
                 ,"_Curs_PREMICON")
          else
            select COUNT(*) AS CONTA from (i_cTable);
             where CCCODICE = this.w_APCONTR And PCROWNUM=this.w_RIGAKEY And PCSCAPRM < this.w_VALCON;
             group by CCCODICE,PCROWNUM;
              into cursor _Curs_PREMICON
          endif
          if used('_Curs_PREMICON')
            select _Curs_PREMICON
            locate for 1=1
            do while not(eof())
            this.w_CONTA = NVL(CONTA,0)
              select _Curs_PREMICON
              continue
            enddo
            use
          endif
          if this.w_CONTA > 0
            * --- Select from PREMICON
            i_nConn=i_TableProp[this.PREMICON_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" PREMICON ";
                  +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And PCROWNUM="+cp_ToStrODBC(this.w_RIGAKEY)+" And PCSCAPRM < "+cp_ToStrODBC(this.w_VALCON)+"";
                  +" order by PCSCAPRM";
                   ,"_Curs_PREMICON")
            else
              select * from (i_cTable);
               where CCCODICE = this.w_APCONTR And PCROWNUM=this.w_RIGAKEY And PCSCAPRM < this.w_VALCON;
               order by PCSCAPRM;
                into cursor _Curs_PREMICON
            endif
            if used('_Curs_PREMICON')
              select _Curs_PREMICON
              locate for 1=1
              do while not(eof())
              * --- Aggiorno sulle righe premio i campi PCQTACON, PCVALCON
              * --- Progressivo riga per determinare scaglione maggiore
              this.w_PROGR = this.w_PROGR+1
              if this.w_PROGR < this.w_CONTA
                * --- Non � lo scaglione maggiore 
                this.w_PCVALCON = _Curs_PREMICON.PCSCAPRM
                this.w_PCQTACON = cp_ROUND((this.w_QTACON/this.w_VALCON) * this.w_PCVALCON, g_PERPQT)
                if this.w_FLFRAZ = "S"
                  this.w_PCQTACON = INT(this.w_PCQTACON)
                endif
                this.w_OKVALFAT = .f.
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                * --- Scagione a quantit� maggiore
                this.w_PCVALCON = this.w_VALCON
                this.w_PCQTACON = cp_ROUND((this.w_QTACON/this.w_VALCON) * this.w_PCVALCON, g_PERPQT)
                if this.w_FLFRAZ = "S"
                  this.w_PCQTACON = INT(this.w_PCQTACON)
                endif
                this.w_OKVALFAT = .t.
                this.Page_3()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
                select _Curs_PREMICON
                continue
              enddo
              use
            endif
          else
            * --- Non ho raggiunto lo scaglione scrivo comunque la quantit� venduta e il fatturato
            if this.oParentObject.w_GPSTATUS = "S"
              * --- Select from PREMICON
              i_nConn=i_TableProp[this.PREMICON_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select * from "+i_cTable+" PREMICON ";
                    +" where CCCODICE = "+cp_ToStrODBC(this.w_APCONTR)+" And PCROWNUM="+cp_ToStrODBC(this.w_RIGAKEY)+"";
                    +" order by CPROWNUM";
                     ,"_Curs_PREMICON")
              else
                select * from (i_cTable);
                 where CCCODICE = this.w_APCONTR And PCROWNUM=this.w_RIGAKEY;
                 order by CPROWNUM;
                  into cursor _Curs_PREMICON
              endif
              if used('_Curs_PREMICON')
                select _Curs_PREMICON
                locate for 1=1
                do while not(eof())
                * --- Write into PREMICON
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.PREMICON_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.PREMICON_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"PCQTACON ="+cp_NullLink(cp_ToStrODBC(this.w_QTACON),'PREMICON','PCQTACON');
                  +",PCVALCON ="+cp_NullLink(cp_ToStrODBC(this.w_VALCON),'PREMICON','PCVALCON');
                      +i_ccchkf ;
                  +" where ";
                      +"CCCODICE = "+cp_ToStrODBC(this.w_APCONTR);
                      +" and PCROWNUM = "+cp_ToStrODBC(this.w_RIGAKEY);
                      +" and CPROWNUM = "+cp_ToStrODBC(_Curs_PREMICON.CPROWNUM);
                         )
                else
                  update (i_cTable) set;
                      PCQTACON = this.w_QTACON;
                      ,PCVALCON = this.w_VALCON;
                      &i_ccchkf. ;
                   where;
                      CCCODICE = this.w_APCONTR;
                      and PCROWNUM = this.w_RIGAKEY;
                      and CPROWNUM = _Curs_PREMICON.CPROWNUM;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
                * --- Primo record ..esco
                exit
                  select _Curs_PREMICON
                  continue
                enddo
                use
              endif
            endif
          endif
      endcase
    endif
    if this.w_CONTA = 0
      * --- Non ho raggiunto lo scaglione  non partecipa
      this.w_PARTECIPA = "N"
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo dei valori PCVALPRE, PCQTAPRE che saranno scritti sul
    *     dettaglio premio del contratto
    this.w_PCPUNPRE = 0
    this.w_PCVALPRE = 0
    this.w_PCQTAPRE = 0
    if _Curs_PREMICON.PCPERSCO <> 0
      this.w_PCVALPRE = this.w_PCVALCON * (_Curs_PREMICON.PCPERSCO/100) 
    else
      if _Curs_PREMICON.PCIMPSCO <> 0
        this.w_PCVALPRE = _Curs_PREMICON.PCIMPSCO * this.w_PCQTACON
      else
        if _Curs_PREMICON.PCSCOSCA <> 0
          this.w_PCVALPRE = _Curs_PREMICON.PCSCOSCA
        else
          if _Curs_PREMICON.PCQTASCO <> 0
            * --- Valorizzazioni
            this.w_VALOREART = 0
            do case
              case this.oParentObject.w_GPVALORI ="A"
                * --- Read from INVEDETT
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.INVEDETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2],.t.,this.INVEDETT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "DICOSMPA"+;
                    " from "+i_cTable+" INVEDETT where ";
                        +"DINUMINV = "+cp_ToStrODBC(this.oParentObject.w_GPINVENT);
                        +" and DICODESE = "+cp_ToStrODBC(this.oParentObject.w_MVCODESE);
                        +" and DICODICE = "+cp_ToStrODBC(_Curs_PREMICON.PCKEYSAL);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    DICOSMPA;
                    from (i_cTable) where;
                        DINUMINV = this.oParentObject.w_GPINVENT;
                        and DICODESE = this.oParentObject.w_MVCODESE;
                        and DICODICE = _Curs_PREMICON.PCKEYSAL;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_VALOREART = NVL(cp_ToDate(_read_.DICOSMPA),cp_NullValue(_read_.DICOSMPA))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              case this.oParentObject.w_GPVALORI ="B"
                * --- Read from INVEDETT
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.INVEDETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2],.t.,this.INVEDETT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "DICOSMPP"+;
                    " from "+i_cTable+" INVEDETT where ";
                        +"DINUMINV = "+cp_ToStrODBC(this.oParentObject.w_GPINVENT);
                        +" and DICODESE = "+cp_ToStrODBC(this.oParentObject.w_MVCODESE);
                        +" and DICODICE = "+cp_ToStrODBC(_Curs_PREMICON.PCKEYSAL);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    DICOSMPP;
                    from (i_cTable) where;
                        DINUMINV = this.oParentObject.w_GPINVENT;
                        and DICODESE = this.oParentObject.w_MVCODESE;
                        and DICODICE = _Curs_PREMICON.PCKEYSAL;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_VALOREART = NVL(cp_ToDate(_read_.DICOSMPP),cp_NullValue(_read_.DICOSMPP))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              case this.oParentObject.w_GPVALORI ="C"
                * --- Read from INVEDETT
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.INVEDETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2],.t.,this.INVEDETT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "DICOSULT"+;
                    " from "+i_cTable+" INVEDETT where ";
                        +"DINUMINV = "+cp_ToStrODBC(this.oParentObject.w_GPINVENT);
                        +" and DICODESE = "+cp_ToStrODBC(this.oParentObject.w_MVCODESE);
                        +" and DICODICE = "+cp_ToStrODBC(_Curs_PREMICON.PCKEYSAL);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    DICOSULT;
                    from (i_cTable) where;
                        DINUMINV = this.oParentObject.w_GPINVENT;
                        and DICODESE = this.oParentObject.w_MVCODESE;
                        and DICODICE = _Curs_PREMICON.PCKEYSAL;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_VALOREART = NVL(cp_ToDate(_read_.DICOSULT),cp_NullValue(_read_.DICOSULT))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              case this.oParentObject.w_GPVALORI ="D"
                * --- Read from INVEDETT
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.INVEDETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2],.t.,this.INVEDETT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "DICOSSTA"+;
                    " from "+i_cTable+" INVEDETT where ";
                        +"DINUMINV = "+cp_ToStrODBC(this.oParentObject.w_GPINVENT);
                        +" and DICODESE = "+cp_ToStrODBC(this.oParentObject.w_MVCODESE);
                        +" and DICODICE = "+cp_ToStrODBC(_Curs_PREMICON.PCKEYSAL);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    DICOSSTA;
                    from (i_cTable) where;
                        DINUMINV = this.oParentObject.w_GPINVENT;
                        and DICODESE = this.oParentObject.w_MVCODESE;
                        and DICODICE = _Curs_PREMICON.PCKEYSAL;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_VALOREART = NVL(cp_ToDate(_read_.DICOSSTA),cp_NullValue(_read_.DICOSSTA))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              case this.oParentObject.w_GPVALORI ="E"
                * --- Read from INVEDETT
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.INVEDETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2],.t.,this.INVEDETT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "DICOSLCO"+;
                    " from "+i_cTable+" INVEDETT where ";
                        +"DINUMINV = "+cp_ToStrODBC(this.oParentObject.w_GPINVENT);
                        +" and DICODESE = "+cp_ToStrODBC(this.oParentObject.w_MVCODESE);
                        +" and DICODICE = "+cp_ToStrODBC(_Curs_PREMICON.PCKEYSAL);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    DICOSLCO;
                    from (i_cTable) where;
                        DINUMINV = this.oParentObject.w_GPINVENT;
                        and DICODESE = this.oParentObject.w_MVCODESE;
                        and DICODICE = _Curs_PREMICON.PCKEYSAL;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_VALOREART = NVL(cp_ToDate(_read_.DICOSLCO),cp_NullValue(_read_.DICOSLCO))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              case this.oParentObject.w_GPVALORI ="F"
                * --- Read from INVEDETT
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.INVEDETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2],.t.,this.INVEDETT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "DICOSFCO"+;
                    " from "+i_cTable+" INVEDETT where ";
                        +"DINUMINV = "+cp_ToStrODBC(this.oParentObject.w_GPINVENT);
                        +" and DICODESE = "+cp_ToStrODBC(this.oParentObject.w_MVCODESE);
                        +" and DICODICE = "+cp_ToStrODBC(_Curs_PREMICON.PCKEYSAL);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    DICOSFCO;
                    from (i_cTable) where;
                        DINUMINV = this.oParentObject.w_GPINVENT;
                        and DICODESE = this.oParentObject.w_MVCODESE;
                        and DICODICE = _Curs_PREMICON.PCKEYSAL;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_VALOREART = NVL(cp_ToDate(_read_.DICOSFCO),cp_NullValue(_read_.DICOSFCO))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              case this.oParentObject.w_GPVALORI ="G"
                * --- Read from INVEDETT
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.INVEDETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.INVEDETT_idx,2],.t.,this.INVEDETT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "DICOSLSC"+;
                    " from "+i_cTable+" INVEDETT where ";
                        +"DINUMINV = "+cp_ToStrODBC(this.oParentObject.w_GPINVENT);
                        +" and DICODESE = "+cp_ToStrODBC(this.oParentObject.w_MVCODESE);
                        +" and DICODICE = "+cp_ToStrODBC(_Curs_PREMICON.PCKEYSAL);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    DICOSLSC;
                    from (i_cTable) where;
                        DINUMINV = this.oParentObject.w_GPINVENT;
                        and DICODESE = this.oParentObject.w_MVCODESE;
                        and DICODICE = _Curs_PREMICON.PCKEYSAL;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_VALOREART = NVL(cp_ToDate(_read_.DICOSLSC),cp_NullValue(_read_.DICOSLSC))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
            endcase
            if i_ROWS=0 Or this.oParentObject.w_GPVALORI = "L"
              this.w_UNIMIS = _Curs_PREMICON.PCUNMART
              this.w_CODART = _Curs_PREMICON.PCCODART
              this.w_CODVAR = _Curs_PREMICON.PCCODVAR
              * --- Select from GSGP_LIS
              do vq_exec with 'GSGP_LIS',this,'_Curs_GSGP_LIS','',.f.,.t.
              if used('_Curs_GSGP_LIS')
                select _Curs_GSGP_LIS
                locate for 1=1
                do while not(eof())
                this.w_VALOREART = _Curs_GSGP_LIS.LIPREZZO
                exit
                  select _Curs_GSGP_LIS
                  continue
                enddo
                use
              endif
            endif
            this.w_PCQTAPRE = _Curs_PREMICON.PCQTASCO
            this.w_PCVALPRE = _Curs_PREMICON.PCQTASCO * this.w_VALOREART
            * --- Leggere il valore da Inventario o Listino (sicuramente se servizio)
          else
            if _Curs_PREMICON.PCPUNPEZ <> 0
              this.w_PCPUNPRE = cp_ROUND(_Curs_PREMICON.PCPUNPEZ * this.w_PCQTACON,0)
            else
              if _Curs_PREMICON.PCPUNSCA <> 0
                this.w_PCPUNPRE = _Curs_PREMICON.PCPUNSCA
              endif
            endif
          endif
        endif
      endif
    endif
    * --- Totali consuntivi
    if this.w_OKVALFAT
      this.w_COVALFAT = this.w_COVALFAT + this.w_PCVALCON
      this.w_COTOTQTA = this.w_COTOTQTA + this.w_PCQTACON
    endif
    this.w_COTOTPUN = this.w_COTOTPUN + this.w_PCPUNPRE
    * --- Totale per documento
    this.w_TOTVALPRE = this.w_TOTVALPRE + this.w_PCVALPRE
    * --- Totale per riga
    this.w_TOTVALROW = this.w_TOTVALROW + this.w_PCVALPRE
    * --- Aggiorno i valori
    * --- Write into PREMICON
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PREMICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PREMICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PCQTACON ="+cp_NullLink(cp_ToStrODBC(this.w_PCQTACON),'PREMICON','PCQTACON');
      +",PCVALCON ="+cp_NullLink(cp_ToStrODBC(this.w_PCVALCON),'PREMICON','PCVALCON');
      +",PCVALPRE ="+cp_NullLink(cp_ToStrODBC(this.w_PCVALPRE),'PREMICON','PCVALPRE');
      +",PCPUNPRE ="+cp_NullLink(cp_ToStrODBC(this.w_PCPUNPRE),'PREMICON','PCPUNPRE');
      +",PCQTAPRE ="+cp_NullLink(cp_ToStrODBC(this.w_PCQTAPRE),'PREMICON','PCQTAPRE');
          +i_ccchkf ;
      +" where ";
          +"CCCODICE = "+cp_ToStrODBC(this.w_APCONTR);
          +" and PCROWNUM = "+cp_ToStrODBC(_Curs_PREMICON.PCROWNUM);
          +" and CPROWNUM = "+cp_ToStrODBC(_Curs_PREMICON.CPROWNUM);
             )
    else
      update (i_cTable) set;
          PCQTACON = this.w_PCQTACON;
          ,PCVALCON = this.w_PCVALCON;
          ,PCVALPRE = this.w_PCVALPRE;
          ,PCPUNPRE = this.w_PCPUNPRE;
          ,PCQTAPRE = this.w_PCQTAPRE;
          &i_ccchkf. ;
       where;
          CCCODICE = this.w_APCONTR;
          and PCROWNUM = _Curs_PREMICON.PCROWNUM;
          and CPROWNUM = _Curs_PREMICON.CPROWNUM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,14)]
    this.cWorkTables[1]='PREMICON'
    this.cWorkTables[2]='CCOMMAST'
    this.cWorkTables[3]='INVENTAR'
    this.cWorkTables[4]='INVEDETT'
    this.cWorkTables[5]='DOC_MAST'
    this.cWorkTables[6]='TIP_DOCU'
    this.cWorkTables[7]='CAU_CONT'
    this.cWorkTables[8]='PREMIDOC'
    this.cWorkTables[9]='ASLISCON'
    this.cWorkTables[10]='CCOMDATT'
    this.cWorkTables[11]='*TMPPREMI'
    this.cWorkTables[12]='AGENTI'
    this.cWorkTables[13]='GRUPPACQ'
    this.cWorkTables[14]='CONTI'
    return(this.OpenAllTables(14))

  proc CloseCursors()
    if used('_Curs_GSGP_BGP')
      use in _Curs_GSGP_BGP
    endif
    if used('_Curs_ASLISCON')
      use in _Curs_ASLISCON
    endif
    if used('_Curs_CCOMDATT')
      use in _Curs_CCOMDATT
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_PREMICON')
      use in _Curs_PREMICON
    endif
    if used('_Curs_GSGP_LIS')
      use in _Curs_GSGP_LIS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
