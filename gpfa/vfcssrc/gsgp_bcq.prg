* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsgp_bcq                                                        *
*              Contratti - controlli                                           *
*                                                                              *
*      Author: Zucchetti - AT                                                  *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-05-05                                                      *
* Last revis.: 2012-10-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER,pSERDOC,pNUMDOC,pALFDOC,pDATDOC,pTIPCLF,pCODCLF,pFLVEAC,pCATDOC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsgp_bcq",oParentObject,m.pOPER,m.pSERDOC,m.pNUMDOC,m.pALFDOC,m.pDATDOC,m.pTIPCLF,m.pCODCLF,m.pFLVEAC,m.pCATDOC)
return(i_retval)

define class tgsgp_bcq as StdBatch
  * --- Local variables
  pOPER = space(10)
  pSERDOC = space(10)
  pNUMDOC = 0
  pALFDOC = space(2)
  pDATDOC = ctod("  /  /  ")
  pTIPCLF = space(1)
  pCODCLF = space(15)
  pFLVEAC = space(1)
  pCATDOC = space(2)
  w_GSGP_MAL = .NULL.
  w_GSGP_MAC = .NULL.
  w_GSGP_MPC = .NULL.
  w_GSGP_MAA = .NULL.
  w_GSGP_MDC = .NULL.
  w_TRIG = 0
  w_OK = .f.
  w_PROG = .NULL.
  w_PADRE = .NULL.
  w_MESS = space(200)
  w_ANNORIFE = space(4)
  w_ANNOPREC = space(4)
  w_DAT1 = ctod("  /  /  ")
  w_DAT2 = ctod("  /  /  ")
  w_DAT3 = ctod("  /  /  ")
  w_DAT4 = ctod("  /  /  ")
  w_DAT5 = ctod("  /  /  ")
  w_DAT1P = ctod("  /  /  ")
  w_DAT2P = ctod("  /  /  ")
  w_DATAVUOTA = ctod("  /  /  ")
  w_PADRE = .NULL.
  w_ORECO = 0
  w_ROWNUM = 0
  w_MSGERR = space(100)
  w_RECORD = 0
  w_ORECO = 0
  w_PROG = .NULL.
  w_RECORD = 0
  w_NELAB = 0
  w_APART = space(20)
  w_FLFRAZ1 = space(1)
  w_QTAUM1 = 0
  w_DTPIARNUM = 0
  * --- WorkFile variables
  CCOMMATT_idx=0
  ASLISCON_idx=0
  ASCONCON_idx=0
  CCOMMAST_idx=0
  KEY_ARTI_idx=0
  PREMICON_idx=0
  DOCPREMI_idx=0
  GRUPPACQ_idx=0
  CCOMDATT_idx=0
  CCOMDETT_idx=0
  PREMIDOC_idx=0
  UNIMIS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione contratti commerciali - Controlli editing e finali
    *     Paremtro pOPER 
    * --- Solo se chiamato da generatore
    this.w_PADRE = This.oParentObject
    this.w_OK = .T.
    do case
      case this.pOPER="TIPO"
        * --- Controllo editing tipo (Quantit�, Promozione)
        this.w_TRIG = this.w_PADRE.NumRow()
        if this.w_TRIG > 0
          AH_ERRORMSG("Impossibile modificare la tipologia contratto, sono presenti righe di dettaglio valorizzate.",48,"")
          this.oParentObject.w_COFLGTIP = this.oParentObject.o_COFLGTIP
        endif
      case this.pOPER="LIST"
        * --- Controllo attivazione flag valido su tutti i listini)
        if this.oParentObject.w_COFLGLIS = "S"
          this.oParentObject.w_SHOWMAL = "N"
          this.oParentObject.GSGP_MAL.LinkPCClick()     
          this.w_GSGP_MAL = this.oParentObject.GSGP_MAL.cnt
          if this.w_GSGP_MAL.NumRow() > 0
            AH_ERRORMSG("Impossibile attivare il flag, sono presenti  listini associati",48,"")
            this.oParentObject.w_COFLGLIS = "N"
          endif
          * --- Devo ripetere la simulazione del click per evitare
          *     che il premere manualmente il bottone non abbia 
          *     la prima volta alcun effetto
          this.oParentObject.GSGP_MAL.LinkPCClick()     
          this.oParentObject.w_SHOWMAL = "S"
        endif
      case this.pOPER="CONT" And this.oParentObject.w_COFLGTIP = "O"
        * --- Controllo attivazione flag valido su tutte le vendite
        if this.oParentObject.w_COFLGVAL = "S"
          this.oParentObject.w_SHOWMAC = "N"
          this.oParentObject.GSGP_MAC.LinkPCClick()     
          this.w_GSGP_MAC = this.oParentObject.GSGP_MAC.cnt
          if this.w_GSGP_MAC.NumRow() > 0
            AH_ERRORMSG("Impossibile attivare il flag, sono presenti  contratti associati.",48,"")
            this.oParentObject.w_COFLGVAL = "N"
          endif
          * --- Devo ripetere la simulazione del click per evitare
          *     che il premere manualmente il bottone non abbia 
          *     la prima volta alcun effetto
          this.oParentObject.GSGP_MAC.LinkPCClick()     
          this.oParentObject.w_SHOWMAC = "S"
        endif
      case this.pOPER="SCIN" And this.oParentObject.w_COFLGTIP = "O"
        * --- Controllo attivazione flag sconti incondizionati
        if this.oParentObject.w_COFLGINC > 0
          this.oParentObject.w_SHOWMPC = "N"
          this.oParentObject.GSGP_MPC.LinkPCClick()     
          this.w_GSGP_MPC = this.oParentObject.GSGP_MPC.cnt
          if this.w_GSGP_MPC.NumRow() > 0
            AH_ERRORMSG("Impossibile attivare il flag, sono presenti righe valorizzate nel dettaglio premi.",48,"")
            this.oParentObject.w_COFLGINC = 0
          else
            if this.oParentObject.w_COTIPSTO <> "F" 
              AH_ERRORMSG("Impossibile attivare il flag, impostare il tipo storno a forfettario",48,"")
              this.oParentObject.w_COFLGINC = 0
            endif
          endif
          this.oParentObject.w_SHOWMPC = "S"
        endif
      case this.pOPER="LPRM" And this.oParentObject.w_COFLGTIP = "O"
        * --- Controllo modifica calcolo premio
        this.oParentObject.w_SHOWMPC = "N"
        this.oParentObject.GSGP_MPC.LinkPCClick()     
        this.w_GSGP_MPC = this.oParentObject.GSGP_MPC.cnt
        if this.w_GSGP_MPC.NumRow() > 0
          AH_ERRORMSG("Impossibile modificare il calcolo del premio, sono presenti righe valorizzate nel dettaglio premi.",48,"")
          if this.oParentObject.w_COCALPRM = "Q"
            this.oParentObject.w_COCALPRM = "V"
            this.w_GSGP_MPC.w_CALPRM = "V"
          else
            this.oParentObject.w_COCALPRM = "Q"
            this.w_GSGP_MPC.w_CALPRM = "Q"
          endif
        else
          this.w_GSGP_MPC.w_CALPRM = this.oParentObject.w_COCALPRM
        endif
        * --- Devo ripetere la simulazione del click per evitare
        *     che il premere manualmente il bottone non abbia 
        *     la prima volta alcun effetto
        this.oParentObject.GSGP_MPC.LinkPCClick()     
        this.oParentObject.w_SHOWMPC = "S"
      case this.pOPER="ATTR"
        * --- Chiamato dalla maschera attributi di testata tipo CONTI
        if this.w_PADRE.FullRow()
          * --- Rendo non editabile l'inteststario
          l_OBJ=this.w_PADRE.oParentObject.GetCtrl("w_COCODINT") 
 l_OBJ.enabled = .f.
        endif
      case this.pOPER="ADEL"
        * --- Chiamato dalla maschera attributi di testata tipo CONTI
        this.w_TRIG = this.w_PADRE.NumRow()
        if this.w_TRIG = 0
          * --- Riabilito l'inteststario
          l_OBJ=this.w_PADRE.oParentObject.GetCtrl("w_COCODINT") 
 l_OBJ.enabled = .t.
        endif
      case this.pOPER="EDIS"
        * --- Controlla la presenza di record attributi tipo CONTI No OBIETTIVO
        if this.oParentObject.w_COFLGTIP <> "O"
          if !Empty(this.oParentObject.w_COCODINT)
            * --- Riabilito l'inteststario
            l_OBJ=This.oParentObject.GetCtrl("w_COCODINT") 
 l_OBJ.enabled = .t.
          else
            * --- Non editabile
            l_OBJ=This.oParentObject.GetCtrl("w_COCODINT") 
 l_OBJ.enabled = .f.
          endif
        endif
      case this.pOPER="CHTR"
        * --- Se flag valido per tutti i listino non attivo allora devo inserire almeno un listino
        if this.oParentObject.w_COFLGLIS <> "S" 
          * --- Read from ASLISCON
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ASLISCON_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ASLISCON_idx,2],.t.,this.ASLISCON_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" ASLISCON where ";
                  +"CLCODICE = "+cp_ToStrODBC(this.oParentObject.w_COCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  CLCODICE = this.oParentObject.w_COCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_ROWS=0
            this.w_MESS = Ah_MsgFormat("Specificare almeno un listino associato oppure attivare il flag valido su tutti i listini! ")
            this.w_OK = .F.
          endif
        endif
        * --- Se flag valido per tutte le vendite non attivo allora devo inserire almeno un contratto di tipo PQ
        if this.w_OK And this.oParentObject.w_COFLGTIP = "O"
          * --- Solo Obiettivo
          if this.oParentObject.w_COFLGVAL <> "S" 
            * --- Read from ASCONCON
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ASCONCON_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ASCONCON_idx,2],.t.,this.ASCONCON_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "*"+;
                " from "+i_cTable+" ASCONCON where ";
                    +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_COCODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                *;
                from (i_cTable) where;
                    CCCODICE = this.oParentObject.w_COCODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_ROWS=0
              this.w_MESS = Ah_MsgFormat("Specificare almeno un contratto associato oppure attivare il flag valido su tutte le vendite! ")
              this.w_OK = .F.
            endif
          endif
        endif
        if this.w_OK And this.oParentObject.w_COFLGTIP <> "O" And this.oParentObject.w_COFLMODE<>"S"
          if Empty(this.oParentObject.w_COCODINT)
            * --- E' vuoto  l'intestatario, controllo che vi sia almeno un attributo
            * --- Read from CCOMMATT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CCOMMATT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CCOMMATT_idx,2],.t.,this.CCOMMATT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "*"+;
                " from "+i_cTable+" CCOMMATT where ";
                    +"CA__GUID = "+cp_ToStrODBC(this.oParentObject.w_COCODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                *;
                from (i_cTable) where;
                    CA__GUID = this.oParentObject.w_COCODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_Rows=0
              this.w_MESS = Ah_MsgFormat("Inserire il codice dell'intestatario oppure un attributo associato.")
              this.w_OK = .F.
            endif
          endif
        endif
        if this.w_OK And this.oParentObject.w_COFLGTIP = "O" And this.oParentObject.w_COFLGINC = 1
          * --- E' attivo il flag sconti incondizionati e sono presenti dei premi
          * --- Read from PREMICON
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PREMICON_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2],.t.,this.PREMICON_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" PREMICON where ";
                  +"CCCODICE = "+cp_ToStrODBC(this.oParentObject.w_COCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  CCCODICE = this.oParentObject.w_COCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows >0
            this.w_MESS = Ah_MsgFormat("Disattivare il flag sconti incondizionati, sono presenti righe valorizzate nel dettaglio premi.")
            this.w_OK = .F.
          endif
        endif
        if !this.w_OK
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        else
          * --- Calcolo priorit� necessaria per applicazione contratto sulla riga
          * --- Select from CCOMMATT
          i_nConn=i_TableProp[this.CCOMMATT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CCOMMATT_idx,2],.t.,this.CCOMMATT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT(*) AS CONTA  from "+i_cTable+" CCOMMATT ";
                +" where CA__GUID = "+cp_ToStrODBC(this.oParentObject.w_COCODICE)+"";
                 ,"_Curs_CCOMMATT")
          else
            select COUNT(*) AS CONTA from (i_cTable);
             where CA__GUID = this.oParentObject.w_COCODICE;
              into cursor _Curs_CCOMMATT
          endif
          if used('_Curs_CCOMMATT')
            select _Curs_CCOMMATT
            locate for 1=1
            do while not(eof())
            this.w_TRIG = NVL(CONTA,0)
              select _Curs_CCOMMATT
              continue
            enddo
            use
          endif
          this.oParentObject.w_COPRICAL = right( repl("0",10) + alltrim(str(this.oParentObject.w_COPRIORI,4,0))+iif(!Empty(this.oParentObject.w_COCODINT),"1","0")+ alltrim(str(this.w_TRIG,3,0)),10)
          * --- Write into CCOMMAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.CCOMMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CCOMMAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CCOMMAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"COPRICAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COPRICAL),'CCOMMAST','COPRICAL');
                +i_ccchkf ;
            +" where ";
                +"COCODICE = "+cp_ToStrODBC(this.oParentObject.w_COCODICE);
                   )
          else
            update (i_cTable) set;
                COPRICAL = this.oParentObject.w_COPRICAL;
                &i_ccchkf. ;
             where;
                COCODICE = this.oParentObject.w_COCODICE;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      case this.pOPER="CHFI"
        * --- Righe senza dettaglio
        this.w_TRIG = this.w_PADRE.NumRow()
        if this.w_TRIG=0
          AH_ERRORMSG("Impossibile confermare, contratto senza righe di dettaglio! ",48,"")
          this.oParentObject.w_RESCHK = -1
        endif
        if this.oParentObject.w_RESCHK=0 And this.oParentObject.w_COFLGTIP = "O" AND this.oParentObject.w_COFLMODE<>"S"
          * --- E' vuoto  l'intestatario, agente e gruppo 
          if Empty(this.oParentObject.w_COCODINT) And this.oParentObject.w_COTIPINT $ "CF" Or Empty(this.oParentObject.w_COCODGRU) And this.oParentObject.w_COTIPINT = "G" Or Empty(this.oParentObject.w_COCODAGE) And this.oParentObject.w_COTIPINT = "A" 
            AH_ERRORMSG("Impossibile confermare, inserire il codice dell' intestatario del contratto.",48,"")
            this.oParentObject.w_RESCHK = -1
          endif
        endif
        if this.oParentObject.w_RESCHK=0 And this.oParentObject.w_COFLGTIP <> "O" AND this.oParentObject.w_COFLMODE="S"
          this.w_TRIG = this.w_PADRE.GSGP_MTL.NumRow()
          if this.w_TRIG>0
            AH_ERRORMSG("Impossibile confermare un modello di contratto con attributi impostati.",48,"")
            this.oParentObject.w_RESCHK = -1
          endif
        endif
        if this.oParentObject.w_RESCHK=0 And ( this.oParentObject.w_COFLGTIP="A" OR (this.oParentObject.w_COFLGTIP="R" AND this.oParentObject.w_COTIPPRE="A" ) )
          this.oParentObject.GSGP_MAA.LinkPCClick(.T.)     
          this.w_GSGP_MAA = this.oParentObject.GSGP_MAA.cnt
          if this.w_GSGP_MAA.NumRow() = 0
            if this.oParentObject.w_COFLGTIP="A"
              AH_ERRORMSG("Impossibile confermare un contratto di tipo assortimento senza nessun attributo da assegnare all'intestatario impostato.",48,"")
            else
              AH_ERRORMSG("Impossibile confermare un contratto di tipo referenziamento senza nessun attributo da assegnare all'intestatario impostato.",48,"")
            endif
            this.oParentObject.w_RESCHK = -1
          endif
          this.oParentObject.GSGP_MAA.LinkPCClick()     
          if this.oParentObject.w_COFLGTIP="A"
            if this.oParentObject.w_RESCHK=0
              this.oParentObject.GSGP_MAL.LinkPCClick(.T.)     
              this.w_GSGP_MAL = this.oParentObject.GSGP_MAL.cnt
              if this.w_GSGP_MAL.NumRow() = 0
                AH_ERRORMSG("Impossibile confermare un contratto di tipo assortimento senza nessun listino impostato.",48,"")
                this.oParentObject.w_RESCHK = -1
              endif
              this.oParentObject.GSGP_MAL.LinkPCClick()     
            endif
            if this.oParentObject.w_RESCHK=0
              this.w_TRIG = this.w_PADRE.GSGP_MTL.NumRow()
              if this.w_TRIG=0 AND EMPTY(this.oParentObject.w_COCODINT) AND this.oParentObject.w_COFLMODE<>"S"
                AH_ERRORMSG("Impossibile confermare un contratto di tipo assortimento senza indicare intestatario o attributi di selezione",48,"")
                this.oParentObject.w_RESCHK = -1
              endif
            endif
          endif
        endif
      case Left(this.pOPER,1) $ "Z-Y" 
        * --- Dettaglio Documenti (lasciata in otherwise per gestire in modo parametrico qualsiasi tipo documento)
        do case
          case SUBSTR( this.pOPER, 2, 1 ) = "A"
            this.w_PROG = GSAC_MDV(SUBSTR(this.pOPER, 3, 2))
          case SUBSTR( this.pOPER, 2, 1 ) = "V"
            this.w_PROG = GSVE_MDV(SUBSTR(this.pOPER, 3, 2))
        endcase
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Carico il documento
        this.w_PROG.EcpFilter()     
        if Left(this.pOPER,1) = "Z" 
          this.w_PROG.w_MVSERIAL = this.oParentObject.w_MVSERIAL
        else
          this.w_PROG.w_MVSERIAL = this.oParentObject.w_MVSERIAL2
        endif
        this.w_PROG.EcpSave()     
      case this.pOPER="AAGP"
        * --- Calcola le date estrazione documenti in base alla periodicit� selezionata
        this.w_ANNORIFE = str(year(this.oParentObject.w_GPDATREG),4,0)
        this.w_ANNOPREC = str(year(this.oParentObject.w_GPDATREG)-1,4,0)
        this.w_DAT1P = cp_CharToDate("01-07-"+this.w_ANNOPREC)
        this.w_DAT2P = cp_CharToDate("01-10-"+this.w_ANNOPREC)
        this.w_DAT1 = cp_CharToDate("01-01-"+this.w_ANNORIFE)
        this.w_DAT2 = cp_CharToDate("01-04-"+this.w_ANNORIFE)
        this.w_DAT3 = cp_CharToDate("01-07-"+this.w_ANNORIFE)
        this.w_DAT4 = cp_CharToDate("01-10-"+this.w_ANNORIFE)
        this.w_DAT5 = cp_CharToDate("31-12"+this.w_ANNORIFE)
        do case
          case this.oParentObject.w_GPPERLIQ = "S"
            * --- Semestrale
            if this.oParentObject.w_GPDATREG < this.w_DAT3
              this.oParentObject.w_GPDATINI = this.w_DAT1P
              this.oParentObject.w_GPDATFIN = this.w_DAT1 - 1
            else
              this.oParentObject.w_GPDATINI = this.w_DAT1
              this.oParentObject.w_GPDATFIN = this.w_DAT3 - 1
            endif
          case this.oParentObject.w_GPPERLIQ = "T"
            * --- Trimestrale
            do case
              case this.oParentObject.w_GPDATREG < this.w_DAT2
                this.oParentObject.w_GPDATINI = this.w_DAT2P
                this.oParentObject.w_GPDATFIN = this.w_DAT1 - 1
              case this.oParentObject.w_GPDATREG < this.w_DAT3
                this.oParentObject.w_GPDATINI = this.w_DAT1
                this.oParentObject.w_GPDATFIN = this.w_DAT2- 1
              case this.oParentObject.w_GPDATREG < this.w_DAT4
                this.oParentObject.w_GPDATINI = this.w_DAT2
                this.oParentObject.w_GPDATFIN = this.w_DAT3 - 1
              otherwise
                this.oParentObject.w_GPDATINI = this.w_DAT3
                this.oParentObject.w_GPDATFIN = this.w_DAT4 -1
            endcase
          case this.oParentObject.w_GPPERLIQ = "A"
            * --- Altra
            this.oParentObject.w_GPDATINI = g_INIESE
            this.oParentObject.w_GPDATFIN = this.oParentObject.w_GPDATREG
        endcase
      case this.pOPER="SOSP"
        * --- Imposto a da elaborare un contratto sospeso
        if this.oParentObject.w_COFLMODE<>"S"
          if AH_YESNO("Si vuole impostare il contratto in stato 'Da elaborare' e renderlo applicabile sul documento?")
            this.oParentObject.w_COFLGSTA = "D"
            this.oParentObject.bUpdated = .t.
            this.oParentObject.bHeaderUpdated = .t.
          endif
        endif
      case this.pOPER="DAEL"
        * --- Ripristina i contratti associati alla generazione
        *     in stato da Elaborare
        * --- Azzero dati consuntivo contratto
        * --- Write into CCOMMAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CCOMMAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CCOMMAST_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="COCODICE"
          do vq_exec with 'GSGPDAEL',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.CCOMMAST_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="CCOMMAST.COCODICE = _t2.COCODICE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COFLGSTA ="+cp_NullLink(cp_ToStrODBC("D"),'CCOMMAST','COFLGSTA');
          +",CODATGEN ="+cp_NullLink(cp_ToStrODBC(this.w_DATAVUOTA),'CCOMMAST','CODATGEN');
          +",COTOTPRE ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COTOTPRE');
          +",COVALPRE ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COVALPRE');
          +",COVALFAT ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COVALFAT');
          +",COVALPUN ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COVALPUN');
          +",COTOTPUN ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COTOTPUN');
          +",COSCOCOM ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COSCOCOM');
              +i_ccchkf;
              +" from "+i_cTable+" CCOMMAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="CCOMMAST.COCODICE = _t2.COCODICE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CCOMMAST, "+i_cQueryTable+" _t2 set ";
          +"CCOMMAST.COFLGSTA ="+cp_NullLink(cp_ToStrODBC("D"),'CCOMMAST','COFLGSTA');
          +",CCOMMAST.CODATGEN ="+cp_NullLink(cp_ToStrODBC(this.w_DATAVUOTA),'CCOMMAST','CODATGEN');
          +",CCOMMAST.COTOTPRE ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COTOTPRE');
          +",CCOMMAST.COVALPRE ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COVALPRE');
          +",CCOMMAST.COVALFAT ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COVALFAT');
          +",CCOMMAST.COVALPUN ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COVALPUN');
          +",CCOMMAST.COTOTPUN ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COTOTPUN');
          +",CCOMMAST.COSCOCOM ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COSCOCOM');
              +Iif(Empty(i_ccchkf),"",",CCOMMAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="CCOMMAST.COCODICE = t2.COCODICE";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CCOMMAST set (";
              +"COFLGSTA,";
              +"CODATGEN,";
              +"COTOTPRE,";
              +"COVALPRE,";
              +"COVALFAT,";
              +"COVALPUN,";
              +"COTOTPUN,";
              +"COSCOCOM";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +cp_NullLink(cp_ToStrODBC("D"),'CCOMMAST','COFLGSTA')+",";
              +cp_NullLink(cp_ToStrODBC(this.w_DATAVUOTA),'CCOMMAST','CODATGEN')+",";
              +cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COTOTPRE')+",";
              +cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COVALPRE')+",";
              +cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COVALFAT')+",";
              +cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COVALPUN')+",";
              +cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COTOTPUN')+",";
              +cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COSCOCOM')+"";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="CCOMMAST.COCODICE = _t2.COCODICE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" CCOMMAST set ";
          +"COFLGSTA ="+cp_NullLink(cp_ToStrODBC("D"),'CCOMMAST','COFLGSTA');
          +",CODATGEN ="+cp_NullLink(cp_ToStrODBC(this.w_DATAVUOTA),'CCOMMAST','CODATGEN');
          +",COTOTPRE ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COTOTPRE');
          +",COVALPRE ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COVALPRE');
          +",COVALFAT ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COVALFAT');
          +",COVALPUN ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COVALPUN');
          +",COTOTPUN ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COTOTPUN');
          +",COSCOCOM ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COSCOCOM');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".COCODICE = "+i_cQueryTable+".COCODICE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"COFLGSTA ="+cp_NullLink(cp_ToStrODBC("D"),'CCOMMAST','COFLGSTA');
          +",CODATGEN ="+cp_NullLink(cp_ToStrODBC(this.w_DATAVUOTA),'CCOMMAST','CODATGEN');
          +",COTOTPRE ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COTOTPRE');
          +",COVALPRE ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COVALPRE');
          +",COVALFAT ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COVALFAT');
          +",COVALPUN ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COVALPUN');
          +",COTOTPUN ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COTOTPUN');
          +",COSCOCOM ="+cp_NullLink(cp_ToStrODBC(0),'CCOMMAST','COSCOCOM');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Azzero premi contratto
        * --- Write into PREMICON
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PREMICON_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PREMICON_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="CCCODICE"
          do vq_exec with 'GSGPDAEL',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PREMICON_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="PREMICON.CCCODICE = _t2.CCCODICE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PCPUNPRE ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCPUNPRE');
          +",PCQTAPRE ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCQTAPRE');
          +",PCVALPRE ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCVALPRE');
          +",PCVALCON ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCVALCON');
          +",PCQTACON ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCQTACON');
              +i_ccchkf;
              +" from "+i_cTable+" PREMICON, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="PREMICON.CCCODICE = _t2.CCCODICE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PREMICON, "+i_cQueryTable+" _t2 set ";
          +"PREMICON.PCPUNPRE ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCPUNPRE');
          +",PREMICON.PCQTAPRE ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCQTAPRE');
          +",PREMICON.PCVALPRE ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCVALPRE');
          +",PREMICON.PCVALCON ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCVALCON');
          +",PREMICON.PCQTACON ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCQTACON');
              +Iif(Empty(i_ccchkf),"",",PREMICON.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="PREMICON.CCCODICE = t2.CCCODICE";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PREMICON set (";
              +"PCPUNPRE,";
              +"PCQTAPRE,";
              +"PCVALPRE,";
              +"PCVALCON,";
              +"PCQTACON";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCPUNPRE')+",";
              +cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCQTAPRE')+",";
              +cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCVALPRE')+",";
              +cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCVALCON')+",";
              +cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCQTACON')+"";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="PREMICON.CCCODICE = _t2.CCCODICE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PREMICON set ";
          +"PCPUNPRE ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCPUNPRE');
          +",PCQTAPRE ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCQTAPRE');
          +",PCVALPRE ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCVALPRE');
          +",PCVALCON ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCVALCON');
          +",PCQTACON ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCQTACON');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".CCCODICE = "+i_cQueryTable+".CCCODICE";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PCPUNPRE ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCPUNPRE');
          +",PCQTAPRE ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCQTAPRE');
          +",PCVALPRE ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCVALPRE');
          +",PCVALCON ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCVALCON');
          +",PCQTACON ="+cp_NullLink(cp_ToStrODBC(0),'PREMICON','PCQTACON');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Elimino i record dalla tabella Documenti per generazione premi PREMIDOC
        * --- Delete from PREMIDOC
        i_nConn=i_TableProp[this.PREMIDOC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PREMIDOC_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"SERGPREM = "+cp_ToStrODBC(this.oParentObject.w_GPSERIAL);
                 )
        else
          delete from (i_cTable) where;
                SERGPREM = this.oParentObject.w_GPSERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      case this.pOPER="ROWC" And this.oParentObject.w_COFLGTIP <> "P"
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CACODART,CACODICE"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.oParentObject.w_COKEYART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CACODART,CACODICE;
            from (i_cTable) where;
                CACODICE = this.oParentObject.w_COKEYART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_COCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
          this.oParentObject.w_COKEYSAL = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_CODKEY = this.oParentObject.w_COCODART
        this.w_PADRE = This.oParentObject
        this.w_ROWNUM = this.w_PADRE.Get("CPROWNUM")
        if !Empty(this.oParentObject.w_COKEYART)
          if this.oParentObject.w_COKEYART=this.oParentObject.w_CODKEY
            * --- Codice di ricerca interno, non deve esistere alcun altro codice che punta allo stesso articolo
            this.w_ORECO = this.w_PADRE.Search("t_COKEYSAL =["+ this.oParentObject.w_COKEYSAL +"] And CPROWNUM <> "+cp_ToStrODBC(this.w_ROWNUM),0)
            if this.w_ORECO <> -1
              AH_ERRORMSG("E' gi� presente un codice di ricerca relativo allo stesso articolo.",48,"")
              this.oParentObject.w_COKEYART = SPACE(41)
              this.oParentObject.w_COKEYSAL = SPACE(20)
              this.oParentObject.w_COCODART = SPACE(20)
              this.oParentObject.w_DESART = SPACE(40)
            endif
          else
            * --- Codice di ricerca NON interno, non deve esistere il relativo codice interno
            this.w_ORECO = this.w_PADRE.Search("t_COKEYART =["+ this.oParentObject.w_CODKEY +"] And CPROWNUM <> "+cp_ToStrODBC(this.w_ROWNUM),0)
            if this.w_ORECO <> -1
              AH_ERRORMSG("E' gi� presente il codice di ricerca principale relativo allo stesso articolo.",48,"")
              this.oParentObject.w_COKEYART = SPACE(41)
              this.oParentObject.w_COKEYSAL = SPACE(20)
              this.oParentObject.w_COCODART = SPACE(20)
              this.oParentObject.w_DESART = SPACE(40)
            endif
          endif
        endif
      case this.pOPER="SCAG"
        * --- Controllo scaglioni (Dal dettaglio premi)
        * --- conto le righe del cursore del dettaglio (diverse da quella attualmente modificata)
        *     aventi scaglione uguale a quello appena impostato.
        if this.oParentObject.w_PCVALPRM > 0 Or this.oParentObject.w_PCQTAPRM > 0
          this.w_PADRE.MarkPos()     
          this.w_RECORD = this.w_PADRE.RowIndex()
          if this.oParentObject.w_CALPRM = "Q"
            * --- a quantit�
            this.w_ORECO = this.w_PADRE.Search(cp_ToStrODBC(this.oParentObject.w_PCQTAPRM) +" =  t_PCQTAPRM And  I_RECNO<> "+cp_ToStrODBC(this.w_RECORD) +"  And Not Deleted() ",0)
            if this.w_ORECO <> -1 And this.w_ORECO <> this.w_RECORD
              this.w_MSGERR = "Scaglione gi� presente nel dettaglio premi!"
              ah_ErrorMsg(this.w_MSGERR,48)
              this.w_PADRE.Set("w_PCQTAPRM", 0)     
              this.w_PADRE.Set("w_PCSCAPRM", 0)     
            else
              this.w_PADRE.Set("w_PCSCAPRM", this.oParentObject.w_PCQTAPRM)     
            endif
          else
            * --- a valore
            this.w_ORECO = this.w_PADRE.Search(cp_ToStrODBC(this.oParentObject.w_PCVALPRM) +" =  t_PCVALPRM And I_RECNO <> "+cp_ToStrODBC(this.w_RECORD) +"  And Not Deleted() ",0)
            if this.w_ORECO <> -1 And this.w_ORECO <> this.w_RECORD
              this.w_MSGERR = "Scaglione gi� presente nel dettaglio premi!"
              ah_ErrorMsg(this.w_MSGERR,48)
              this.w_PADRE.Set("w_PCVALPRM", 0)     
              this.w_PADRE.Set("w_PCSCAPRM", 0)     
            else
              this.w_PADRE.Set("w_PCSCAPRM", this.oParentObject.w_PCVALPRM)     
            endif
          endif
          this.w_PADRE.RePos()     
        endif
      case Right(this.pOPER,3)="GPR"
        * --- Bottoncino visualizza generazione premi
        if Type("pSERDOC")="C"
          if !Empty(this.pSERDOC)
            this.w_PROG = GSGP_AGP(Left(this.pOPER,1))
            * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
            if !(this.w_PROG.bSec1)
              i_retcode = 'stop'
              return
            endif
            * --- Carica record e lascia in interrograzione ...
            this.w_PROG.EcpFilter()     
            this.w_PROG.w_GPSERIAL = this.pSERDOC
            this.w_PROG.EcpSave()     
          endif
        endif
      case this.pOPER="CENT"
        if this.oParentObject.w_GAFLPRIF="S"
          this.w_PADRE.MarkPos()     
          this.w_RECORD = this.w_PADRE.RowIndex()
          * --- Nel Transitorio un flag attivo vale 1
          this.w_ORECO = this.w_PADRE.Search("t_GAFLPRIF = 1 And I_RECNO <> "+cp_ToStrODBC(this.w_RECORD) +"  And Not Deleted() ",0)
          if this.w_ORECO <> -1
            AH_ErrorMsg("%1 sar� il nuovo intestatario centrale",64,"", alltrim(this.oParentObject.w_GACODCLI))
            this.w_PADRE.SetRow(this.w_ORECO)     
            this.w_PADRE.Set("w_GAFLPRIF",0)     
          endif
          * --- Mi riposiziono nel Record 
          this.w_PADRE.RePos()     
        endif
      case this.pOPER="MGAR"
        * --- Read from GRUPPACQ
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.GRUPPACQ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GRUPPACQ_idx,2],.t.,this.GRUPPACQ_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" GRUPPACQ where ";
                +"GACODICE = "+cp_ToStrODBC(this.oParentObject.w_GACODICE);
                +" and GAFLPRIF = "+cp_ToStrODBC("S");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                GACODICE = this.oParentObject.w_GACODICE;
                and GAFLPRIF = "S";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows = 0
          this.w_MESS = Ah_MsgFormat("Attivare il flag centrale in una riga del gruppo di acquisto")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      case this.pOPER="VIAR"
        * --- Attributi di selezione
        select t_CACODMOD,t_CACODFAM,t_CACODGRU,t_CAVALATT from (this.w_PADRE.cTrsName); 
 where t_CACODMOD = get_GPFA_code("MODELLO") And Not Empty (t_CAVALATT) into Cursor Elab 
        this.w_NELAB = RECCOUNT("Elab")
        if this.w_NELAB >0
          * --- Bottoni visualizza e stampa articoli
          *     Elenco articoli con attributi associati
          vq_exec("..\gpfa\exe\query\GSGP2BGL",this,"Artico")
          * --- Conto i record (attributi) per ogni articolo
          *     (serivir� pi� avanti per scartare gli articoli non validi se attivo in maschera 
          *     il flag Selezione Puntuale)
          Select ARCODART, count(*) as NUM from Artico group by ARCODART into cursor CONTA
          * --- La join consente gi� di scartare tutti gli articoli che non hanno 
          *     neppure un attributo in comune.
          Select Artico.*,"S" as ACCETTA from Artico inner join Elab on; 
 ASMODFAM=t_CACODMOD and ASCODATT=t_CACODGRU and ASCODFAM=t_CACODFAM and ASVALATT=t_CAVALATT; 
 Into Cursor Artico 
          =wrcursor("Artico")
          * --- Dal risultato della join conto per ogni articolo i record (attributri) rimasti.
          *     Se NON coincidono col totale di attributi di selezione l'articolo NON � valido,
          *     � presente cio� almeno un attributo di selezione non soddisfatto.
          *     Se coincidono occorre controllare il flag PUNTUALE nella maschera:
          *     se ATTIVO confronto il totale dei record (attributi) di CONTAJ (risulatto della join)
          *     con il totale dei record di CONTA (totale di attributi per articolo), se coincide l'articolo viene ACCETTATO
          *     se NON coincide l'articolo viene SCARTATO.
          *     Se il flag puntuale � disattivo l'articolo � ACCETTATO
          Select ARCODART, count(*) as NUM from Artico group by ARCODART into cursor CONTAJ
          Select CONTAJ 
 Go top
          Scan
          this.w_APART = CONTAJ.ARCODART
          if this.w_NELAB > CONTAJ.NUM
            * --- Se sono presenti piu attributi di selezione rspetto a quelli associati
            *     all'articolo scarto l'articolo in questione
            Update Artico set ACCETTA="N" where ARCODART= this.w_APART
            Select Artico 
 Go Top
          endif
          Select CONTAJ
          EndScan
          Select Artico
          * --- Trasferisco gli articoli nel cursore di stampa __tmp__
          Select * from Artico where ACCETTA="S" into cursor __tmp__ order by ARCODART group by ARCODART
          do GSGP1KML With "A"
        else
          Select Elab 
 Use
          AH_ERRORMSG("Nessun attributo con modello ' ART_ICOL' selezionato",48)
          i_retcode = 'stop'
          return
        endif
        * --- Chiusura cursori
        if USED("Artico")
          Select ARTICO 
 Use
        endif
        if USED("CONTAJ")
          Select CONTAJ 
 Use
        endif
        if USED("CONTA")
          Select CONTA 
 Use
        endif
        if USED("Elab")
          Select Elab 
 Use
        endif
      case this.pOPER="Q2PC"
        if this.oParentObject.w_COQTA2PC<> 0 And this.oParentObject.w_COUNMIPC <> this.oParentObject.w_UNMIS1PC
          * --- Lettura Flag non Frazionabile e Forza UM secondaria (mi serve anche per la CALMMLIS)
          * --- Read from UNIMIS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.UNIMIS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "UMFLFRAZ,UMMODUM2"+;
              " from "+i_cTable+" UNIMIS where ";
                  +"UMCODICE = "+cp_ToStrODBC(this.oParentObject.w_UNMIS1PC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              UMFLFRAZ,UMMODUM2;
              from (i_cTable) where;
                  UMCODICE = this.oParentObject.w_UNMIS1PC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_NOFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
            w_MODUM2 = NVL(cp_ToDate(_read_.UMMODUM2),cp_NullValue(_read_.UMMODUM2))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_QTAUM1 = CALQTAADV(this.oParentObject.w_COQTA2PC, this.oParentObject.w_COUNMIPC, this.oParentObject.w_UNMIS2PC, this.oParentObject.w_OPERATPC, this.oParentObject.w_MOLTIPPC, this.oParentObject.w_FLUSEP, w_NOFRAZ, w_MODUM2, "", this.oParentObject.w_UNMIS3PC,this.oParentObject.w_OPERA3PC,this.oParentObject.w_MOLTI3PC,,, this.oparentobject, "COQTA2PC")
        endif
      otherwise
        if Type("pSERDOC")="C"
          * --- Select from DOCPREMI
          i_nConn=i_TableProp[this.DOCPREMI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOCPREMI_idx,2],.t.,this.DOCPREMI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select MAX(CPROWNUM) AS CPROWNUM  from "+i_cTable+" DOCPREMI ";
                +" where DGPERIAL="+cp_ToStrODBC(this.pOPER)+"";
                 ,"_Curs_DOCPREMI")
          else
            select MAX(CPROWNUM) AS CPROWNUM from (i_cTable);
             where DGPERIAL=this.pOPER;
              into cursor _Curs_DOCPREMI
          endif
          if used('_Curs_DOCPREMI')
            select _Curs_DOCPREMI
            locate for 1=1
            do while not(eof())
            this.w_DTPIARNUM = _Curs_DOCPREMI.CPROWNUM
              select _Curs_DOCPREMI
              continue
            enddo
            use
          endif
          this.w_DTPIARNUM = NVL(this.w_DTPIARNUM,0) + 1
          * --- Insert into DOCPREMI
          i_nConn=i_TableProp[this.DOCPREMI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOCPREMI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOCPREMI_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DGPERIAL"+",CPROWNUM"+",CPROWORD"+",DPSERDOC"+",DGNUMDOC"+",DGALFDOC"+",DGDATDOC"+",DGTIPCLF"+",DPCODCLF"+",DPFLVEAC"+",DPCATDOC"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.pOPER),'DOCPREMI','DGPERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DTPIARNUM),'DOCPREMI','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DTPIARNUM*10),'DOCPREMI','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.pSERDOC),'DOCPREMI','DPSERDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.pNUMDOC),'DOCPREMI','DGNUMDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.pALFDOC),'DOCPREMI','DGALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.pDATDOC),'DOCPREMI','DGDATDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.pTIPCLF),'DOCPREMI','DGTIPCLF');
            +","+cp_NullLink(cp_ToStrODBC(this.pCODCLF),'DOCPREMI','DPCODCLF');
            +","+cp_NullLink(cp_ToStrODBC(this.pFLVEAC),'DOCPREMI','DPFLVEAC');
            +","+cp_NullLink(cp_ToStrODBC(this.pCATDOC),'DOCPREMI','DPCATDOC');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DGPERIAL',this.pOPER,'CPROWNUM',this.w_DTPIARNUM,'CPROWORD',this.w_DTPIARNUM*10,'DPSERDOC',this.pSERDOC,'DGNUMDOC',this.pNUMDOC,'DGALFDOC',this.pALFDOC,'DGDATDOC',this.pDATDOC,'DGTIPCLF',this.pTIPCLF,'DPCODCLF',this.pCODCLF,'DPFLVEAC',this.pFLVEAC,'DPCATDOC',this.pCATDOC)
            insert into (i_cTable) (DGPERIAL,CPROWNUM,CPROWORD,DPSERDOC,DGNUMDOC,DGALFDOC,DGDATDOC,DGTIPCLF,DPCODCLF,DPFLVEAC,DPCATDOC &i_ccchkf. );
               values (;
                 this.pOPER;
                 ,this.w_DTPIARNUM;
                 ,this.w_DTPIARNUM*10;
                 ,this.pSERDOC;
                 ,this.pNUMDOC;
                 ,this.pALFDOC;
                 ,this.pDATDOC;
                 ,this.pTIPCLF;
                 ,this.pCODCLF;
                 ,this.pFLVEAC;
                 ,this.pCATDOC;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
    endcase
    if this.pOPER="ROWC"
      * --- Apre il postin dell'articolo legato al codice di ricerca
      AH_WARN( this.oParentObject.w_COKEYART )
    endif
  endproc


  proc Init(oParentObject,pOPER,pSERDOC,pNUMDOC,pALFDOC,pDATDOC,pTIPCLF,pCODCLF,pFLVEAC,pCATDOC)
    this.pOPER=pOPER
    this.pSERDOC=pSERDOC
    this.pNUMDOC=pNUMDOC
    this.pALFDOC=pALFDOC
    this.pDATDOC=pDATDOC
    this.pTIPCLF=pTIPCLF
    this.pCODCLF=pCODCLF
    this.pFLVEAC=pFLVEAC
    this.pCATDOC=pCATDOC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,12)]
    this.cWorkTables[1]='CCOMMATT'
    this.cWorkTables[2]='ASLISCON'
    this.cWorkTables[3]='ASCONCON'
    this.cWorkTables[4]='CCOMMAST'
    this.cWorkTables[5]='KEY_ARTI'
    this.cWorkTables[6]='PREMICON'
    this.cWorkTables[7]='DOCPREMI'
    this.cWorkTables[8]='GRUPPACQ'
    this.cWorkTables[9]='CCOMDATT'
    this.cWorkTables[10]='CCOMDETT'
    this.cWorkTables[11]='PREMIDOC'
    this.cWorkTables[12]='UNIMIS'
    return(this.OpenAllTables(12))

  proc CloseCursors()
    if used('_Curs_CCOMMATT')
      use in _Curs_CCOMMATT
    endif
    if used('_Curs_DOCPREMI')
      use in _Curs_DOCPREMI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER,pSERDOC,pNUMDOC,pALFDOC,pDATDOC,pTIPCLF,pCODCLF,pFLVEAC,pCATDOC"
endproc
