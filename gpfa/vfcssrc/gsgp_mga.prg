* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsgp_mga                                                        *
*              Gruppi di acquisto                                              *
*                                                                              *
*      Author: Zucchetti - AT                                                  *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-04                                                      *
* Last revis.: 2012-10-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gsgp_mga
* --- Da Menu' viene Passato il Parametro che identifica il Ciclo
PARAMETERS pFLVEAC
* --- Fine Area Manuale
return(createobject("tgsgp_mga"))

* --- Class definition
define class tgsgp_mga as StdTrsForm
  Top    = 19
  Left   = 14

  * --- Standard Properties
  Width  = 491
  Height = 216+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-23"
  HelpContextID=238448279
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  GRUPPACQ_IDX = 0
  CONTI_IDX = 0
  cFile = "GRUPPACQ"
  cKeySelect = "GACODICE"
  cKeyWhere  = "GACODICE=this.w_GACODICE"
  cKeyDetail  = "GACODICE=this.w_GACODICE"
  cKeyWhereODBC = '"GACODICE="+cp_ToStrODBC(this.w_GACODICE)';

  cKeyDetailWhereODBC = '"GACODICE="+cp_ToStrODBC(this.w_GACODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"GRUPPACQ.GACODICE="+cp_ToStrODBC(this.w_GACODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'GRUPPACQ.CPROWNUM '
  cPrg = "gsgp_mga"
  cComment = "Gruppi di acquisto"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_GA_CICLO = space(1)
  o_GA_CICLO = space(1)
  w_GATIPCON = space(1)
  w_GACODICE = space(15)
  w_GADESGRP = space(40)
  w_GACODCLI = space(15)
  w_ANDESCRI = space(40)
  w_GAFLPRIF = space(1)
  w_GSDATINI = ctod('  /  /  ')
  w_GSDATFIN = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsgp_mga
  *Variabile di lavoro indicante il ciclo
  pCiclo=' '
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
      return(lower('GSGP_MAQ,'+this.pCiclo))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'GRUPPACQ','gsgp_mga')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsgp_mgaPag1","gsgp_mga",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Gruppi")
      .Pages(1).HelpContextID = 22137498
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oGACODICE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gsgp_mga
    *Valorizzo variabile di lavoro
    WITH THIS.PARENT
     .pCiclo = pFlveac
     .cComment = ah_msgformat("Gruppi di acquisto ")  +IIF(.pCICLO = 'V', ah_msgformat("(vendite)"),ah_msgformat("(acquisti)"))
     .cAutoZoom = IIF(.pCiclo = 'V', 'GSGPVMGA','GSGPAMGA')
    ENDWITH
    
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='GRUPPACQ'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.GRUPPACQ_IDX,5],7]
    this.nPostItConn=i_TableProp[this.GRUPPACQ_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_GACODICE = NVL(GACODICE,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from GRUPPACQ where GACODICE=KeySet.GACODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.GRUPPACQ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2],this.bLoadRecFilter,this.GRUPPACQ_IDX,"gsgp_mga")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('GRUPPACQ')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "GRUPPACQ.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' GRUPPACQ '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'GACODICE',this.w_GACODICE  )
      select * from (i_cTable) GRUPPACQ where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = i_datsys
        .w_GA_CICLO = NVL(GA_CICLO,space(1))
        .w_GATIPCON = NVL(GATIPCON,space(1))
        .w_GACODICE = NVL(GACODICE,space(15))
        .w_GADESGRP = NVL(GADESGRP,space(40))
        .w_GSDATINI = NVL(cp_ToDate(GSDATINI),ctod("  /  /  "))
        .w_GSDATFIN = NVL(cp_ToDate(GSDATFIN),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'GRUPPACQ')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_ANDESCRI = space(40)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_GACODCLI = NVL(GACODCLI,space(15))
          .link_2_1('Load')
          .w_GAFLPRIF = NVL(GAFLPRIF,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsgp_mga
    if this.w_GA_CICLO <> this.pCiclo
      this.BlankRec()
    endif
    
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_GA_CICLO=space(1)
      .w_GATIPCON=space(1)
      .w_GACODICE=space(15)
      .w_GADESGRP=space(40)
      .w_GACODCLI=space(15)
      .w_ANDESCRI=space(40)
      .w_GAFLPRIF=space(1)
      .w_GSDATINI=ctod("  /  /  ")
      .w_GSDATFIN=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .w_GA_CICLO = .pCiclo
        .w_GATIPCON = iif(.w_GA_CICLO='A','F','C')
        .DoRTCalc(3,5,.f.)
        if not(empty(.w_GACODCLI))
         .link_2_1('Full')
        endif
        .DoRTCalc(6,9,.f.)
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'GRUPPACQ')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oGACODICE_1_3.enabled = i_bVal
      .Page1.oPag.oGADESGRP_1_5.enabled = i_bVal
      .Page1.oPag.oGSDATINI_3_1.enabled = i_bVal
      .Page1.oPag.oGSDATFIN_3_3.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oGACODICE_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oGACODICE_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'GRUPPACQ',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.GRUPPACQ_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GA_CICLO,"GA_CICLO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GATIPCON,"GATIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GACODICE,"GACODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GADESGRP,"GADESGRP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GSDATINI,"GSDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GSDATFIN,"GSDATFIN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GRUPPACQ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2])
    i_lTable = "GRUPPACQ"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.GRUPPACQ_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_GACODCLI C(15);
      ,t_ANDESCRI C(40);
      ,t_GAFLPRIF N(3);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsgp_mgabodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGACODCLI_2_1.controlsource=this.cTrsName+'.t_GACODCLI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oANDESCRI_2_2.controlsource=this.cTrsName+'.t_ANDESCRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oGAFLPRIF_2_3.controlsource=this.cTrsName+'.t_GAFLPRIF'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(156)
    this.AddVLine(426)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGACODCLI_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.GRUPPACQ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.GRUPPACQ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2])
      *
      * insert into GRUPPACQ
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'GRUPPACQ')
        i_extval=cp_InsertValODBCExtFlds(this,'GRUPPACQ')
        i_cFldBody=" "+;
                  "(GA_CICLO,GATIPCON,GACODICE,GADESGRP,GACODCLI"+;
                  ",GAFLPRIF,GSDATINI,GSDATFIN,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_GA_CICLO)+","+cp_ToStrODBC(this.w_GATIPCON)+","+cp_ToStrODBC(this.w_GACODICE)+","+cp_ToStrODBC(this.w_GADESGRP)+","+cp_ToStrODBCNull(this.w_GACODCLI)+;
             ","+cp_ToStrODBC(this.w_GAFLPRIF)+","+cp_ToStrODBC(this.w_GSDATINI)+","+cp_ToStrODBC(this.w_GSDATFIN)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'GRUPPACQ')
        i_extval=cp_InsertValVFPExtFlds(this,'GRUPPACQ')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'GACODICE',this.w_GACODICE)
        INSERT INTO (i_cTable) (;
                   GA_CICLO;
                  ,GATIPCON;
                  ,GACODICE;
                  ,GADESGRP;
                  ,GACODCLI;
                  ,GAFLPRIF;
                  ,GSDATINI;
                  ,GSDATFIN;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_GA_CICLO;
                  ,this.w_GATIPCON;
                  ,this.w_GACODICE;
                  ,this.w_GADESGRP;
                  ,this.w_GACODCLI;
                  ,this.w_GAFLPRIF;
                  ,this.w_GSDATINI;
                  ,this.w_GSDATFIN;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.GRUPPACQ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_GACODCLI))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'GRUPPACQ')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " GA_CICLO="+cp_ToStrODBC(this.w_GA_CICLO)+;
                 ",GATIPCON="+cp_ToStrODBC(this.w_GATIPCON)+;
                 ",GADESGRP="+cp_ToStrODBC(this.w_GADESGRP)+;
                 ",GSDATINI="+cp_ToStrODBC(this.w_GSDATINI)+;
                 ",GSDATFIN="+cp_ToStrODBC(this.w_GSDATFIN)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'GRUPPACQ')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  GA_CICLO=this.w_GA_CICLO;
                 ,GATIPCON=this.w_GATIPCON;
                 ,GADESGRP=this.w_GADESGRP;
                 ,GSDATINI=this.w_GSDATINI;
                 ,GSDATFIN=this.w_GSDATFIN;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_GACODCLI))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update GRUPPACQ
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'GRUPPACQ')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " GA_CICLO="+cp_ToStrODBC(this.w_GA_CICLO)+;
                     ",GATIPCON="+cp_ToStrODBC(this.w_GATIPCON)+;
                     ",GADESGRP="+cp_ToStrODBC(this.w_GADESGRP)+;
                     ",GACODCLI="+cp_ToStrODBCNull(this.w_GACODCLI)+;
                     ",GAFLPRIF="+cp_ToStrODBC(this.w_GAFLPRIF)+;
                     ",GSDATINI="+cp_ToStrODBC(this.w_GSDATINI)+;
                     ",GSDATFIN="+cp_ToStrODBC(this.w_GSDATFIN)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'GRUPPACQ')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      GA_CICLO=this.w_GA_CICLO;
                     ,GATIPCON=this.w_GATIPCON;
                     ,GADESGRP=this.w_GADESGRP;
                     ,GACODCLI=this.w_GACODCLI;
                     ,GAFLPRIF=this.w_GAFLPRIF;
                     ,GSDATINI=this.w_GSDATINI;
                     ,GSDATFIN=this.w_GSDATFIN;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsgp_mga
    if not(bTrsErr)
        * --- Controlli finali sotto transazione
        this.NotifyEvent('MGAR')
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.GRUPPACQ_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_GACODCLI))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete GRUPPACQ
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_GACODCLI))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.GRUPPACQ_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPPACQ_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
        if .o_GA_CICLO<>.w_GA_CICLO
          .w_GATIPCON = iif(.w_GA_CICLO='A','F','C')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(3,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_UKSGBVCSUC()
    with this
          * --- Gsgp_bcq (cent) centrale
          GSGP_BCQ(this;
              ,'CENT';
             )
    endwith
  endproc
  proc Calculate_JJUHXRKECI()
    with this
          * --- Gsgp_bcq (mgar) centrale
          GSGP_BCQ(this;
              ,'MGAR';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_GAFLPRIF Changed")
          .Calculate_UKSGBVCSUC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("MGAR")
          .Calculate_JJUHXRKECI()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=GACODCLI
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GACODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_GACODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_GATIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_GATIPCON;
                     ,'ANCODICE',trim(this.w_GACODCLI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GACODCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GACODCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oGACODCLI_2_1'),i_cWhere,'GSAR_BZC',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_GATIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_GATIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GACODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_GACODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_GATIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_GATIPCON;
                       ,'ANCODICE',this.w_GACODCLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GACODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_GACODCLI = space(15)
      endif
      this.w_ANDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GACODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oGACODICE_1_3.value==this.w_GACODICE)
      this.oPgFrm.Page1.oPag.oGACODICE_1_3.value=this.w_GACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oGADESGRP_1_5.value==this.w_GADESGRP)
      this.oPgFrm.Page1.oPag.oGADESGRP_1_5.value=this.w_GADESGRP
    endif
    if not(this.oPgFrm.Page1.oPag.oGSDATINI_3_1.value==this.w_GSDATINI)
      this.oPgFrm.Page1.oPag.oGSDATINI_3_1.value=this.w_GSDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGSDATFIN_3_3.value==this.w_GSDATFIN)
      this.oPgFrm.Page1.oPag.oGSDATFIN_3_3.value=this.w_GSDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGACODCLI_2_1.value==this.w_GACODCLI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGACODCLI_2_1.value=this.w_GACODCLI
      replace t_GACODCLI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGACODCLI_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oANDESCRI_2_2.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oANDESCRI_2_2.value=this.w_ANDESCRI
      replace t_ANDESCRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oANDESCRI_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGAFLPRIF_2_3.RadioValue()==this.w_GAFLPRIF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGAFLPRIF_2_3.SetRadio()
      replace t_GAFLPRIF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGAFLPRIF_2_3.value
    endif
    cp_SetControlsValueExtFlds(this,'GRUPPACQ')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (not(Empty(t_GACODCLI)));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if not(Empty(.w_GACODCLI))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_GA_CICLO = this.w_GA_CICLO
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_GACODCLI)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_GACODCLI=space(15)
      .w_ANDESCRI=space(40)
      .w_GAFLPRIF=space(1)
      .DoRTCalc(1,5,.f.)
      if not(empty(.w_GACODCLI))
        .link_2_1('Full')
      endif
    endwith
    this.DoRTCalc(6,10,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_GACODCLI = t_GACODCLI
    this.w_ANDESCRI = t_ANDESCRI
    this.w_GAFLPRIF = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGAFLPRIF_2_3.RadioValue(.t.)
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_GACODCLI with this.w_GACODCLI
    replace t_ANDESCRI with this.w_ANDESCRI
    replace t_GAFLPRIF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oGAFLPRIF_2_3.ToRadio()
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsgp_mgaPag1 as StdContainer
  Width  = 487
  height = 216
  stdWidth  = 487
  stdheight = 216
  resizeXpos=205
  resizeYpos=121
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGACODICE_1_3 as StdField with uid="UHMPNRZBJT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_GACODICE", cQueryName = "GACODICE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 70654805,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=73, Top=18, InputMask=replicate('X',15)

  add object oGADESGRP_1_5 as StdField with uid="OEBEWJGYCY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_GADESGRP", cQueryName = "GADESGRP",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 89131850,;
   bGlobalFont=.t.,;
    Height=21, Width=284, Left=194, Top=18, InputMask=replicate('X',40)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=4, top=43, width=483,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="GACODCLI",Label1="iif(w_GATIPCON='C', ah_msgformat('Cliente'), ah_msgformat('Fornitore'))",Field2="ANDESCRI",Label2="Descrizione",Field3="GAFLPRIF",Label3="Centrale",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 17668218

  add object oStr_1_4 as StdString with uid="FEJBIGTSRD",Visible=.t., Left=4, Top=18,;
    Alignment=1, Width=67, Height=18,;
    Caption="Codice:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-1,top=67,;
    width=472+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=0,top=68,width=471+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CONTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CONTI'
        oDropInto=this.oBodyCol.oRow.oGACODCLI_2_1
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oGSDATINI_3_1 as StdField with uid="KJNSYJJYJF",rtseq=8,rtrep=.f.,;
    cFormVar="w_GSDATINI",value=ctod("  /  /  "),;
    HelpContextID = 54786385,;
    cQueryName = "GSDATINI",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=203, Top=193

  add object oGSDATFIN_3_3 as StdField with uid="NRWESKVTRF",rtseq=9,rtrep=.f.,;
    cFormVar="w_GSDATFIN",value=ctod("  /  /  "),;
    HelpContextID = 163317428,;
    cQueryName = "GSDATFIN",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=400, Top=193

  add object oStr_3_2 as StdString with uid="FDLYYWOYMK",Visible=.t., Left=109, Top=193,;
    Alignment=1, Width=89, Height=18,;
    Caption="Valido dal:"  ;
  , bGlobalFont=.t.

  add object oStr_3_4 as StdString with uid="QKUNYBUZXQ",Visible=.t., Left=357, Top=193,;
    Alignment=1, Width=37, Height=18,;
    Caption="al:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsgp_mgaBodyRow as CPBodyRowCnt
  Width=462
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oGACODCLI_2_1 as StdTrsField with uid="FGGQBKHRDM",rtseq=5,rtrep=.t.,;
    cFormVar="w_GACODCLI",value=space(15),;
    HelpContextID = 171318097,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=146, Left=-2, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_GATIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_GACODCLI"

  func oGACODCLI_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oGACODCLI_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oGACODCLI_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_GATIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_GATIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oGACODCLI_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori",'',this.parent.oContained
  endproc
  proc oGACODCLI_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_GATIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_GACODCLI
    i_obj.ecpSave()
  endproc

  add object oANDESCRI_2_2 as StdTrsField with uid="ECWPUVGCIB",rtseq=6,rtrep=.t.,;
    cFormVar="w_ANDESCRI",value=space(40),enabled=.f.,;
    HelpContextID = 156237489,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=264, Left=147, Top=0, InputMask=replicate('X',40)

  add object oGAFLPRIF_2_3 as StdTrsCheck with uid="CTEFXFGLOA",rtrep=.t.,;
    cFormVar="w_GAFLPRIF",  caption="",;
    HelpContextID = 92738732,;
    Left=419, Top=-2, Width=38,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , Autosize=.F.;
   , bGlobalFont=.t.


  func oGAFLPRIF_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..GAFLPRIF,&i_cF..t_GAFLPRIF),this.value)
    return(iif(xVal =1,'S',;
    space(1)))
  endfunc
  func oGAFLPRIF_2_3.GetRadio()
    this.Parent.oContained.w_GAFLPRIF = this.RadioValue()
    return .t.
  endfunc

  func oGAFLPRIF_2_3.ToRadio()
    this.Parent.oContained.w_GAFLPRIF=trim(this.Parent.oContained.w_GAFLPRIF)
    return(;
      iif(this.Parent.oContained.w_GAFLPRIF=='S',1,;
      0))
  endfunc

  func oGAFLPRIF_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oGACODCLI_2_1.When()
    return(.t.)
  proc oGACODCLI_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oGACODCLI_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsgp_mga','GRUPPACQ','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".GACODICE=GRUPPACQ.GACODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
