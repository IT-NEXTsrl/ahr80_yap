* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsgp_ma2                                                        *
*              Dettaglio attributi master                                      *
*                                                                              *
*      Author: Zucchetti Spa - AT                                              *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-04-04                                                      *
* Last revis.: 2012-10-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsgp_ma2")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsgp_ma2")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsgp_ma2")
  return

* --- Class definition
define class tgsgp_ma2 as StdPCForm
  Width  = 471
  Height = 275
  Top    = 10
  Left   = 11
  cComment = "Dettaglio attributi master"
  cPrg = "gsgp_ma2"
  HelpContextID=130650473
  add object cnt as tcgsgp_ma2
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsgp_ma2 as PCContext
  w_CA__GUID = space(10)
  w_CPROWORD = 0
  w_SRV = space(1)
  w_CACODMOD = space(20)
  w_GEST = space(10)
  w_CACODGRU = space(10)
  w_CACODFAM = space(10)
  w_CAVALATT = space(20)
  w_CADESCRI = space(50)
  w_AUTOMA = space(1)
  w_CODFAM = space(10)
  w_TIPO = space(1)
  w_CAVALATT = space(20)
  w_CADBSAVE = space(1)
  w_TIPSELE = space(1)
  w_CICLO = space(1)
  w_ANTIPCON = space(1)
  w_PUNTUALE = space(1)
  w_ANCODINI = space(15)
  w_ANCODFIN = space(15)
  w_MCCODICE = space(15)
  w_NACODNAZ = space(3)
  w_CCCODICE = space(5)
  w_ZOCODZON = space(3)
  w_LUCODICE = space(3)
  w_CTCODICE = space(3)
  w_CSCODICE = space(5)
  w_GPCODICE = space(5)
  w_VACODVAL = space(3)
  w_AGCODAG1 = space(5)
  w_AGCODAG2 = space(5)
  w_PACODICE = space(5)
  w_BANUMCOR = space(15)
  w_SECODMOD = space(20)
  w_FR_TABLE = space(30)
  w_FR__ZOOM = space(254)
  w_FR_CAMPO = space(50)
  proc Save(i_oFrom)
    this.w_CA__GUID = i_oFrom.w_CA__GUID
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_SRV = i_oFrom.w_SRV
    this.w_CACODMOD = i_oFrom.w_CACODMOD
    this.w_GEST = i_oFrom.w_GEST
    this.w_CACODGRU = i_oFrom.w_CACODGRU
    this.w_CACODFAM = i_oFrom.w_CACODFAM
    this.w_CAVALATT = i_oFrom.w_CAVALATT
    this.w_CADESCRI = i_oFrom.w_CADESCRI
    this.w_AUTOMA = i_oFrom.w_AUTOMA
    this.w_CODFAM = i_oFrom.w_CODFAM
    this.w_TIPO = i_oFrom.w_TIPO
    this.w_CAVALATT = i_oFrom.w_CAVALATT
    this.w_CADBSAVE = i_oFrom.w_CADBSAVE
    this.w_TIPSELE = i_oFrom.w_TIPSELE
    this.w_CICLO = i_oFrom.w_CICLO
    this.w_ANTIPCON = i_oFrom.w_ANTIPCON
    this.w_PUNTUALE = i_oFrom.w_PUNTUALE
    this.w_ANCODINI = i_oFrom.w_ANCODINI
    this.w_ANCODFIN = i_oFrom.w_ANCODFIN
    this.w_MCCODICE = i_oFrom.w_MCCODICE
    this.w_NACODNAZ = i_oFrom.w_NACODNAZ
    this.w_CCCODICE = i_oFrom.w_CCCODICE
    this.w_ZOCODZON = i_oFrom.w_ZOCODZON
    this.w_LUCODICE = i_oFrom.w_LUCODICE
    this.w_CTCODICE = i_oFrom.w_CTCODICE
    this.w_CSCODICE = i_oFrom.w_CSCODICE
    this.w_GPCODICE = i_oFrom.w_GPCODICE
    this.w_VACODVAL = i_oFrom.w_VACODVAL
    this.w_AGCODAG1 = i_oFrom.w_AGCODAG1
    this.w_AGCODAG2 = i_oFrom.w_AGCODAG2
    this.w_PACODICE = i_oFrom.w_PACODICE
    this.w_BANUMCOR = i_oFrom.w_BANUMCOR
    this.w_SECODMOD = i_oFrom.w_SECODMOD
    this.w_FR_TABLE = i_oFrom.w_FR_TABLE
    this.w_FR__ZOOM = i_oFrom.w_FR__ZOOM
    this.w_FR_CAMPO = i_oFrom.w_FR_CAMPO
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CA__GUID = this.w_CA__GUID
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_SRV = this.w_SRV
    i_oTo.w_CACODMOD = this.w_CACODMOD
    i_oTo.w_GEST = this.w_GEST
    i_oTo.w_CACODGRU = this.w_CACODGRU
    i_oTo.w_CACODFAM = this.w_CACODFAM
    i_oTo.w_CAVALATT = this.w_CAVALATT
    i_oTo.w_CADESCRI = this.w_CADESCRI
    i_oTo.w_AUTOMA = this.w_AUTOMA
    i_oTo.w_CODFAM = this.w_CODFAM
    i_oTo.w_TIPO = this.w_TIPO
    i_oTo.w_CAVALATT = this.w_CAVALATT
    i_oTo.w_CADBSAVE = this.w_CADBSAVE
    i_oTo.w_TIPSELE = this.w_TIPSELE
    i_oTo.w_CICLO = this.w_CICLO
    i_oTo.w_ANTIPCON = this.w_ANTIPCON
    i_oTo.w_PUNTUALE = this.w_PUNTUALE
    i_oTo.w_ANCODINI = this.w_ANCODINI
    i_oTo.w_ANCODFIN = this.w_ANCODFIN
    i_oTo.w_MCCODICE = this.w_MCCODICE
    i_oTo.w_NACODNAZ = this.w_NACODNAZ
    i_oTo.w_CCCODICE = this.w_CCCODICE
    i_oTo.w_ZOCODZON = this.w_ZOCODZON
    i_oTo.w_LUCODICE = this.w_LUCODICE
    i_oTo.w_CTCODICE = this.w_CTCODICE
    i_oTo.w_CSCODICE = this.w_CSCODICE
    i_oTo.w_GPCODICE = this.w_GPCODICE
    i_oTo.w_VACODVAL = this.w_VACODVAL
    i_oTo.w_AGCODAG1 = this.w_AGCODAG1
    i_oTo.w_AGCODAG2 = this.w_AGCODAG2
    i_oTo.w_PACODICE = this.w_PACODICE
    i_oTo.w_BANUMCOR = this.w_BANUMCOR
    i_oTo.w_SECODMOD = this.w_SECODMOD
    i_oTo.w_FR_TABLE = this.w_FR_TABLE
    i_oTo.w_FR__ZOOM = this.w_FR__ZOOM
    i_oTo.w_FR_CAMPO = this.w_FR_CAMPO
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsgp_ma2 as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 471
  Height = 275
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-10-08"
  HelpContextID=130650473
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=37

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  CCOMMATT_IDX = 0
  MODMATTR_IDX = 0
  FAM_ATTR_IDX = 0
  MODDATTR_IDX = 0
  GRUDATTR_IDX = 0
  cFile = "CCOMMATT"
  cKeySelect = "CA__GUID"
  cKeyWhere  = "CA__GUID=this.w_CA__GUID"
  cKeyDetail  = "CA__GUID=this.w_CA__GUID and CACODMOD=this.w_CACODMOD and CACODGRU=this.w_CACODGRU and CACODFAM=this.w_CACODFAM and CAVALATT=this.w_CAVALATT and CAVALATT=this.w_CAVALATT"
  cKeyWhereODBC = '"CA__GUID="+cp_ToStrODBC(this.w_CA__GUID)';

  cKeyDetailWhereODBC = '"CA__GUID="+cp_ToStrODBC(this.w_CA__GUID)';
      +'+" and CACODMOD="+cp_ToStrODBC(this.w_CACODMOD)';
      +'+" and CACODGRU="+cp_ToStrODBC(this.w_CACODGRU)';
      +'+" and CACODFAM="+cp_ToStrODBC(this.w_CACODFAM)';
      +'+" and CAVALATT="+cp_ToStrODBC(this.w_CAVALATT)';
      +'+" and CAVALATT="+cp_ToStrODBC(this.w_CAVALATT)';

  cKeyWhereODBCqualified = '"CCOMMATT.CA__GUID="+cp_ToStrODBC(this.w_CA__GUID)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CCOMMATT.CPROWORD '
  cPrg = "gsgp_ma2"
  cComment = "Dettaglio attributi master"
  i_nRowNum = 0
  i_nRowPerPage = 9
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CA__GUID = space(10)
  w_CPROWORD = 0
  w_SRV = space(1)
  w_CACODMOD = space(20)
  o_CACODMOD = space(20)
  w_GEST = space(10)
  w_CACODGRU = space(10)
  w_CACODFAM = space(10)
  o_CACODFAM = space(10)
  w_CAVALATT = space(20)
  w_CADESCRI = space(50)
  w_AUTOMA = space(1)
  w_CODFAM = space(10)
  w_TIPO = space(1)
  w_CAVALATT = space(20)
  w_CADBSAVE = space(1)
  w_TIPSELE = space(1)
  w_CICLO = space(1)
  w_ANTIPCON = space(1)
  w_PUNTUALE = space(1)
  w_ANCODINI = space(15)
  w_ANCODFIN = space(15)
  w_MCCODICE = space(15)
  w_NACODNAZ = space(3)
  w_CCCODICE = space(5)
  w_ZOCODZON = space(3)
  w_LUCODICE = space(3)
  w_CTCODICE = space(3)
  w_CSCODICE = space(5)
  w_GPCODICE = space(5)
  w_VACODVAL = space(3)
  w_AGCODAG1 = space(5)
  w_AGCODAG2 = space(5)
  w_PACODICE = space(5)
  w_BANUMCOR = space(15)
  w_SECODMOD = space(20)
  w_FR_TABLE = space(30)
  w_FR__ZOOM = space(254)
  w_FR_CAMPO = space(50)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsgp_ma2Pag1","gsgp_ma2",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='MODMATTR'
    this.cWorkTables[2]='FAM_ATTR'
    this.cWorkTables[3]='MODDATTR'
    this.cWorkTables[4]='GRUDATTR'
    this.cWorkTables[5]='CCOMMATT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CCOMMATT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CCOMMATT_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsgp_ma2'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from CCOMMATT where CA__GUID=KeySet.CA__GUID
    *                            and CACODMOD=KeySet.CACODMOD
    *                            and CACODGRU=KeySet.CACODGRU
    *                            and CACODFAM=KeySet.CACODFAM
    *                            and CAVALATT=KeySet.CAVALATT
    *                            and CAVALATT=KeySet.CAVALATT
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.CCOMMATT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCOMMATT_IDX,2],this.bLoadRecFilter,this.CCOMMATT_IDX,"gsgp_ma2")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CCOMMATT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CCOMMATT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CCOMMATT '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CA__GUID',this.w_CA__GUID  )
      select * from (i_cTable) CCOMMATT where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CADBSAVE = space(1)
        .w_TIPSELE = 'T'
        .w_CICLO = space(1)
        .w_PUNTUALE = 'N'
        .w_ANCODINI = space(15)
        .w_ANCODFIN = space(15)
        .w_MCCODICE = space(15)
        .w_NACODNAZ = space(3)
        .w_CCCODICE = space(5)
        .w_ZOCODZON = space(3)
        .w_LUCODICE = space(3)
        .w_CTCODICE = space(3)
        .w_CSCODICE = space(5)
        .w_GPCODICE = space(5)
        .w_VACODVAL = space(3)
        .w_AGCODAG1 = space(5)
        .w_AGCODAG2 = space(5)
        .w_PACODICE = space(5)
        .w_BANUMCOR = space(15)
        .w_SECODMOD = space(20)
        .w_CA__GUID = NVL(CA__GUID,space(10))
        .w_ANTIPCON = IIF(.w_CICLO='V', 'C', 'F')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'CCOMMATT')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_SRV = space(1)
          .w_AUTOMA = space(1)
          .w_TIPO = space(1)
          .w_FR_TABLE = space(30)
          .w_FR__ZOOM = space(254)
          .w_FR_CAMPO = space(50)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_CACODMOD = NVL(CACODMOD,space(20))
        .w_GEST = .w_CACODMOD
          .w_CACODGRU = NVL(CACODGRU,space(10))
          .link_2_5('Load')
          .w_CACODFAM = NVL(CACODFAM,space(10))
          * evitabile
          *.link_2_6('Load')
          .w_CAVALATT = NVL(CAVALATT,space(20))
          .w_CADESCRI = NVL(CADESCRI,space(50))
        .w_CODFAM = .w_CACODFAM
          .link_2_10('Load')
          .w_CAVALATT = NVL(CAVALATT,space(20))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CACODMOD with .w_CACODMOD
          replace CACODGRU with .w_CACODGRU
          replace CACODFAM with .w_CACODFAM
          replace CAVALATT with .w_CAVALATT
          replace CAVALATT with .w_CAVALATT
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_ANTIPCON = IIF(.w_CICLO='V', 'C', 'F')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_4.enabled = .oPgFrm.Page1.oPag.oBtn_1_4.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_5.enabled = .oPgFrm.Page1.oPag.oBtn_1_5.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_CA__GUID=space(10)
      .w_CPROWORD=10
      .w_SRV=space(1)
      .w_CACODMOD=space(20)
      .w_GEST=space(10)
      .w_CACODGRU=space(10)
      .w_CACODFAM=space(10)
      .w_CAVALATT=space(20)
      .w_CADESCRI=space(50)
      .w_AUTOMA=space(1)
      .w_CODFAM=space(10)
      .w_TIPO=space(1)
      .w_CAVALATT=space(20)
      .w_CADBSAVE=space(1)
      .w_TIPSELE=space(1)
      .w_CICLO=space(1)
      .w_ANTIPCON=space(1)
      .w_PUNTUALE=space(1)
      .w_ANCODINI=space(15)
      .w_ANCODFIN=space(15)
      .w_MCCODICE=space(15)
      .w_NACODNAZ=space(3)
      .w_CCCODICE=space(5)
      .w_ZOCODZON=space(3)
      .w_LUCODICE=space(3)
      .w_CTCODICE=space(3)
      .w_CSCODICE=space(5)
      .w_GPCODICE=space(5)
      .w_VACODVAL=space(3)
      .w_AGCODAG1=space(5)
      .w_AGCODAG2=space(5)
      .w_PACODICE=space(5)
      .w_BANUMCOR=space(15)
      .w_SECODMOD=space(20)
      .w_FR_TABLE=space(30)
      .w_FR__ZOOM=space(254)
      .w_FR_CAMPO=space(50)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        .w_CACODMOD = this.oparentobject.w_SECODMOD
        .w_GEST = .w_CACODMOD
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CACODGRU))
         .link_2_5('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CACODFAM))
         .link_2_6('Full')
        endif
        .w_CAVALATT = ''
        .DoRTCalc(9,10,.f.)
        .w_CODFAM = .w_CACODFAM
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_CODFAM))
         .link_2_10('Full')
        endif
        .DoRTCalc(12,12,.f.)
        .w_CAVALATT = ''
        .DoRTCalc(14,14,.f.)
        .w_TIPSELE = 'T'
        .DoRTCalc(16,16,.f.)
        .w_ANTIPCON = IIF(.w_CICLO='V', 'C', 'F')
        .w_PUNTUALE = 'N'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CCOMMATT')
    this.DoRTCalc(19,37,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBtn_1_4.enabled = .Page1.oPag.oBtn_1_4.mCond()
      .Page1.oPag.oBtn_1_5.enabled = .Page1.oPag.oBtn_1_5.mCond()
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'CCOMMATT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CCOMMATT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CA__GUID,"CA__GUID",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(6);
      ,t_CACODGRU C(10);
      ,t_CACODFAM C(10);
      ,t_CAVALATT C(20);
      ,t_CADESCRI C(50);
      ,CACODMOD C(20);
      ,CACODGRU C(10);
      ,CACODFAM C(10);
      ,CAVALATT C(20);
      ,t_SRV C(1);
      ,t_CACODMOD C(20);
      ,t_GEST C(10);
      ,t_AUTOMA C(1);
      ,t_CODFAM C(10);
      ,t_TIPO C(1);
      ,t_FR_TABLE C(30);
      ,t_FR__ZOOM C(254);
      ,t_FR_CAMPO C(50);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsgp_ma2bodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCACODGRU_2_5.controlsource=this.cTrsName+'.t_CACODGRU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCACODFAM_2_6.controlsource=this.cTrsName+'.t_CACODFAM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALATT_2_7.controlsource=this.cTrsName+'.t_CAVALATT'
    this.oPgFRm.Page1.oPag.oCADESCRI_2_8.controlsource=this.cTrsName+'.t_CADESCRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALATT_2_12.controlsource=this.cTrsName+'.t_CAVALATT'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(49)
    this.AddVLine(148)
    this.AddVLine(247)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CCOMMATT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCOMMATT_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- gsgp_ma2
    if this.w_CADBSAVE='S'
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CCOMMATT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCOMMATT_IDX,2])
      *
      * insert into CCOMMATT
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CCOMMATT')
        i_extval=cp_InsertValODBCExtFlds(this,'CCOMMATT')
        i_cFldBody=" "+;
                  "(CA__GUID,CPROWORD,CACODMOD,CACODGRU,CACODFAM"+;
                  ",CAVALATT,CADESCRI,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CA__GUID)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_CACODMOD)+","+cp_ToStrODBCNull(this.w_CACODGRU)+","+cp_ToStrODBCNull(this.w_CACODFAM)+;
             ","+cp_ToStrODBC(this.w_CAVALATT)+","+cp_ToStrODBC(this.w_CADESCRI)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CCOMMATT')
        i_extval=cp_InsertValVFPExtFlds(this,'CCOMMATT')
        cp_CheckDeletedKey(i_cTable,0,'CA__GUID',this.w_CA__GUID,'CACODMOD',this.w_CACODMOD,'CACODGRU',this.w_CACODGRU,'CACODFAM',this.w_CACODFAM,'CAVALATT',this.w_CAVALATT,'CAVALATT',this.w_CAVALATT)
        INSERT INTO (i_cTable) (;
                   CA__GUID;
                  ,CPROWORD;
                  ,CACODMOD;
                  ,CACODGRU;
                  ,CACODFAM;
                  ,CAVALATT;
                  ,CADESCRI;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CA__GUID;
                  ,this.w_CPROWORD;
                  ,this.w_CACODMOD;
                  ,this.w_CACODGRU;
                  ,this.w_CACODFAM;
                  ,this.w_CAVALATT;
                  ,this.w_CADESCRI;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- gsgp_ma2
    endif
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.CCOMMATT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCOMMATT_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CACODMOD)) And not(Empty(t_CACODGRU)) And not(Empty(t_CACODFAM)) And not(Empty(t_CAVALATT))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'CCOMMATT')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CACODMOD="+cp_ToStrODBC(&i_TN.->CACODMOD)+;
                 " and CACODGRU="+cp_ToStrODBC(&i_TN.->CACODGRU)+;
                 " and CACODFAM="+cp_ToStrODBC(&i_TN.->CACODFAM)+;
                 " and CAVALATT="+cp_ToStrODBC(&i_TN.->CAVALATT)+;
                 " and CAVALATT="+cp_ToStrODBC(&i_TN.->CAVALATT)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'CCOMMATT')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CACODMOD=&i_TN.->CACODMOD;
                      and CACODGRU=&i_TN.->CACODGRU;
                      and CACODFAM=&i_TN.->CACODFAM;
                      and CAVALATT=&i_TN.->CAVALATT;
                      and CAVALATT=&i_TN.->CAVALATT;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CACODMOD)) And not(Empty(t_CACODGRU)) And not(Empty(t_CACODFAM)) And not(Empty(t_CAVALATT))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and CACODMOD="+cp_ToStrODBC(&i_TN.->CACODMOD)+;
                            " and CACODGRU="+cp_ToStrODBC(&i_TN.->CACODGRU)+;
                            " and CACODFAM="+cp_ToStrODBC(&i_TN.->CACODFAM)+;
                            " and CAVALATT="+cp_ToStrODBC(&i_TN.->CAVALATT)+;
                            " and CAVALATT="+cp_ToStrODBC(&i_TN.->CAVALATT)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and CACODMOD=&i_TN.->CACODMOD;
                            and CACODGRU=&i_TN.->CACODGRU;
                            and CACODFAM=&i_TN.->CACODFAM;
                            and CAVALATT=&i_TN.->CAVALATT;
                            and CAVALATT=&i_TN.->CAVALATT;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace CACODMOD with this.w_CACODMOD
              replace CACODGRU with this.w_CACODGRU
              replace CACODFAM with this.w_CACODFAM
              replace CAVALATT with this.w_CAVALATT
              replace CAVALATT with this.w_CAVALATT
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CCOMMATT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'CCOMMATT')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",CADESCRI="+cp_ToStrODBC(this.w_CADESCRI)+;
                     ",CACODMOD="+cp_ToStrODBC(this.w_CACODMOD)+;
                     ",CACODGRU="+cp_ToStrODBC(this.w_CACODGRU)+;
                     ",CACODFAM="+cp_ToStrODBC(this.w_CACODFAM)+;
                     ",CAVALATT="+cp_ToStrODBC(this.w_CAVALATT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and CACODMOD="+cp_ToStrODBC(CACODMOD)+;
                             " and CACODGRU="+cp_ToStrODBC(CACODGRU)+;
                             " and CACODFAM="+cp_ToStrODBC(CACODFAM)+;
                             " and CAVALATT="+cp_ToStrODBC(CAVALATT)+;
                             " and CAVALATT="+cp_ToStrODBC(CAVALATT)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'CCOMMATT')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,CADESCRI=this.w_CADESCRI;
                     ,CACODMOD=this.w_CACODMOD;
                     ,CACODGRU=this.w_CACODGRU;
                     ,CACODFAM=this.w_CACODFAM;
                     ,CAVALATT=this.w_CAVALATT;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and CACODMOD=&i_TN.->CACODMOD;
                                      and CACODGRU=&i_TN.->CACODGRU;
                                      and CACODFAM=&i_TN.->CACODFAM;
                                      and CAVALATT=&i_TN.->CAVALATT;
                                      and CAVALATT=&i_TN.->CAVALATT;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CCOMMATT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CCOMMATT_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CACODMOD)) And not(Empty(t_CACODGRU)) And not(Empty(t_CACODFAM)) And not(Empty(t_CAVALATT))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete CCOMMATT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and CACODMOD="+cp_ToStrODBC(&i_TN.->CACODMOD)+;
                            " and CACODGRU="+cp_ToStrODBC(&i_TN.->CACODGRU)+;
                            " and CACODFAM="+cp_ToStrODBC(&i_TN.->CACODFAM)+;
                            " and CAVALATT="+cp_ToStrODBC(&i_TN.->CAVALATT)+;
                            " and CAVALATT="+cp_ToStrODBC(&i_TN.->CAVALATT)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and CACODMOD=&i_TN.->CACODMOD;
                              and CACODGRU=&i_TN.->CACODGRU;
                              and CACODFAM=&i_TN.->CACODFAM;
                              and CAVALATT=&i_TN.->CAVALATT;
                              and CAVALATT=&i_TN.->CAVALATT;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CACODMOD)) And not(Empty(t_CACODGRU)) And not(Empty(t_CACODFAM)) And not(Empty(t_CAVALATT))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CCOMMATT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CCOMMATT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .w_CACODMOD = this.oparentobject.w_SECODMOD
        if .o_CACODMOD<>.w_CACODMOD
          .w_GEST = .w_CACODMOD
        endif
        .DoRTCalc(6,7,.t.)
        if .o_CACODFAM<>.w_CACODFAM
          .w_CAVALATT = ''
        endif
        .DoRTCalc(9,10,.t.)
          .w_CODFAM = .w_CACODFAM
          .link_2_10('Full')
        .DoRTCalc(12,12,.t.)
        if .o_CACODFAM<>.w_CACODFAM
          .w_CAVALATT = ''
        endif
        .DoRTCalc(14,16,.t.)
          .w_ANTIPCON = IIF(.w_CICLO='V', 'C', 'F')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- gsgp_ma2
        Local Old_area
        old_area=select()
        select (this.ctrsname)
        this.w_SRV = i_SRV
        Select( old_area )
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(18,37,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_SRV with this.w_SRV
      replace t_CACODMOD with this.w_CACODMOD
      replace t_GEST with this.w_GEST
      replace t_AUTOMA with this.w_AUTOMA
      replace t_CODFAM with this.w_CODFAM
      replace t_TIPO with this.w_TIPO
      replace t_FR_TABLE with this.w_FR_TABLE
      replace t_FR__ZOOM with this.w_FR__ZOOM
      replace t_FR_CAMPO with this.w_FR_CAMPO
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return
  proc Calculate_FKKRTURDLS()
    with this
          * --- Gsve_bli (chg)
          GSVE_BLI(this;
              ,'CHGV';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCACODGRU_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCACODGRU_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCACODFAM_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCACODFAM_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCAVALATT_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCAVALATT_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCAVALATT_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oCAVALATT_2_12.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_4.visible=!this.oPgFrm.Page1.oPag.oBtn_1_4.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_5.visible=!this.oPgFrm.Page1.oPag.oBtn_1_5.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALATT_2_7.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALATT_2_7.mHide()
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALATT_2_12.visible=!this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALATT_2_12.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_CAVALATT Changed")
          .Calculate_FKKRTURDLS()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CACODGRU
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    local i_bFillw_CACODMOD
    i_bFillw_CACODMOD=empty(this.w_CACODMOD) and i_cCtrl='Part'
    i_nConn = i_TableProp[this.MODDATTR_IDX,3]
    i_lTable = "MODDATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODDATTR_IDX,2], .t., this.MODDATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODDATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MODDATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODATT like "+cp_ToStrODBC(trim(this.w_CACODGRU)+"%");
                   +iif(!i_bFillw_CACODMOD," and MACODICE="+cp_ToStrODBC(this.w_CACODMOD),"");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MACODATT,MAAUTOMA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE,MACODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,iif(!i_bFillw_CACODMOD,'MACODICE','-'),this.w_CACODMOD;
                     ,'MACODATT',trim(this.w_CACODGRU))
          select MACODICE,MACODATT,MAAUTOMA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE,MACODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODGRU)==trim(_Link_.MACODATT) and !i_bFillw_CACODMOD 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODGRU) and !this.bDontReportError
            deferred_cp_zoom('MODDATTR','*','MACODICE,MACODATT',cp_AbsName(oSource.parent,'oCACODGRU_2_5'),i_cWhere,'',"Gruppi attributi",'gsar_mgg.MODDATTR_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        this.w_CACODMOD=oSource.xKey(1)
        if this.w_CACODMOD<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MACODATT,MAAUTOMA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select MACODICE,MACODATT,MAAUTOMA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MACODATT,MAAUTOMA";
                     +" from "+i_cTable+" "+i_lTable+" where MACODATT="+cp_ToStrODBC(oSource.xKey(2));
                     +" and MACODICE="+cp_ToStrODBC(this.w_CACODMOD);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1);
                       ,'MACODATT',oSource.xKey(2))
            select MACODICE,MACODATT,MAAUTOMA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MACODATT,MAAUTOMA";
                   +" from "+i_cTable+" "+i_lTable+" where MACODATT="+cp_ToStrODBC(this.w_CACODGRU);
                   +" and MACODICE="+cp_ToStrODBC(this.w_CACODMOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_CACODMOD;
                       ,'MACODATT',this.w_CACODGRU)
            select MACODICE,MACODATT,MAAUTOMA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      if i_bFillw_CACODMOD
        this.w_CACODMOD = NVL(_Link_.MACODICE,space(20))
      endif
      this.w_CACODGRU = NVL(_Link_.MACODATT,space(10))
      this.w_AUTOMA = NVL(_Link_.MAAUTOMA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CACODGRU = space(10)
      endif
      this.w_AUTOMA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODDATTR_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)+'\'+cp_ToStr(_Link_.MACODATT,1)
      cp_ShowWarn(i_cKey,this.MODDATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CACODFAM
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUDATTR_IDX,3]
    i_lTable = "GRUDATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUDATTR_IDX,2], .t., this.GRUDATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUDATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CACODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'GRUDATTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GRFAMATT like "+cp_ToStrODBC(trim(this.w_CACODFAM)+"%");
                   +" and GRCODICE="+cp_ToStrODBC(this.w_CACODGRU);

          i_ret=cp_SQL(i_nConn,"select GRCODICE,GRFAMATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GRCODICE,GRFAMATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GRCODICE',this.w_CACODGRU;
                     ,'GRFAMATT',trim(this.w_CACODFAM))
          select GRCODICE,GRFAMATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GRCODICE,GRFAMATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CACODFAM)==trim(_Link_.GRFAMATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CACODFAM) and !this.bDontReportError
            deferred_cp_zoom('GRUDATTR','*','GRCODICE,GRFAMATT',cp_AbsName(oSource.parent,'oCACODFAM_2_6'),i_cWhere,'',"Famiglie attributi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CACODGRU<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE,GRFAMATT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select GRCODICE,GRFAMATT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE,GRFAMATT";
                     +" from "+i_cTable+" "+i_lTable+" where GRFAMATT="+cp_ToStrODBC(oSource.xKey(2));
                     +" and GRCODICE="+cp_ToStrODBC(this.w_CACODGRU);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',oSource.xKey(1);
                       ,'GRFAMATT',oSource.xKey(2))
            select GRCODICE,GRFAMATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CACODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE,GRFAMATT";
                   +" from "+i_cTable+" "+i_lTable+" where GRFAMATT="+cp_ToStrODBC(this.w_CACODFAM);
                   +" and GRCODICE="+cp_ToStrODBC(this.w_CACODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',this.w_CACODGRU;
                       ,'GRFAMATT',this.w_CACODFAM)
            select GRCODICE,GRFAMATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CACODFAM = NVL(_Link_.GRFAMATT,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_CACODFAM = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUDATTR_IDX,2])+'\'+cp_ToStr(_Link_.GRCODICE,1)+'\'+cp_ToStr(_Link_.GRFAMATT,1)
      cp_ShowWarn(i_cKey,this.GRUDATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CACODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAM
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ATTR_IDX,3]
    i_lTable = "FAM_ATTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2], .t., this.FAM_ATTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FRCODICE,FRTIPOLO,FR_TABLE,FR__ZOOM,FR_CAMPO";
                   +" from "+i_cTable+" "+i_lTable+" where FRCODICE="+cp_ToStrODBC(this.w_CODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FRCODICE',this.w_CODFAM)
            select FRCODICE,FRTIPOLO,FR_TABLE,FR__ZOOM,FR_CAMPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAM = NVL(_Link_.FRCODICE,space(10))
      this.w_TIPO = NVL(_Link_.FRTIPOLO,space(1))
      this.w_FR_TABLE = NVL(_Link_.FR_TABLE,space(30))
      this.w_FR__ZOOM = NVL(_Link_.FR__ZOOM,space(254))
      this.w_FR_CAMPO = NVL(_Link_.FR_CAMPO,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAM = space(10)
      endif
      this.w_TIPO = space(1)
      this.w_FR_TABLE = space(30)
      this.w_FR__ZOOM = space(254)
      this.w_FR_CAMPO = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ATTR_IDX,2])+'\'+cp_ToStr(_Link_.FRCODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ATTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oCADESCRI_2_8.value==this.w_CADESCRI)
      this.oPgFrm.Page1.oPag.oCADESCRI_2_8.value=this.w_CADESCRI
      replace t_CADESCRI with this.oPgFrm.Page1.oPag.oCADESCRI_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODGRU_2_5.value==this.w_CACODGRU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODGRU_2_5.value=this.w_CACODGRU
      replace t_CACODGRU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODGRU_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODFAM_2_6.value==this.w_CACODFAM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODFAM_2_6.value=this.w_CACODFAM
      replace t_CACODFAM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCACODFAM_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALATT_2_7.value==this.w_CAVALATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALATT_2_7.value=this.w_CAVALATT
      replace t_CAVALATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALATT_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALATT_2_12.value==this.w_CAVALATT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALATT_2_12.value=this.w_CAVALATT
      replace t_CAVALATT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCAVALATT_2_12.value
    endif
    cp_SetControlsValueExtFlds(this,'CCOMMATT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_CPROWORD) and (not(Empty(.w_CACODMOD)) And not(Empty(.w_CACODGRU)) And not(Empty(.w_CACODFAM)) And not(Empty(.w_CAVALATT)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if not(Empty(.w_CACODMOD)) And not(Empty(.w_CACODGRU)) And not(Empty(.w_CACODFAM)) And not(Empty(.w_CAVALATT))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CACODMOD = this.w_CACODMOD
    this.o_CACODFAM = this.w_CACODFAM
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CACODMOD)) And not(Empty(t_CACODGRU)) And not(Empty(t_CACODFAM)) And not(Empty(t_CAVALATT)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(999999,cp_maxroword()+10)
      .w_SRV=space(1)
      .w_CACODMOD=space(20)
      .w_GEST=space(10)
      .w_CACODGRU=space(10)
      .w_CACODFAM=space(10)
      .w_CAVALATT=space(20)
      .w_CADESCRI=space(50)
      .w_AUTOMA=space(1)
      .w_CODFAM=space(10)
      .w_TIPO=space(1)
      .w_CAVALATT=space(20)
      .w_FR_TABLE=space(30)
      .w_FR__ZOOM=space(254)
      .w_FR_CAMPO=space(50)
      .DoRTCalc(1,3,.f.)
        .w_CACODMOD = this.oparentobject.w_SECODMOD
        .w_GEST = .w_CACODMOD
      .DoRTCalc(6,6,.f.)
      if not(empty(.w_CACODGRU))
        .link_2_5('Full')
      endif
      .DoRTCalc(7,7,.f.)
      if not(empty(.w_CACODFAM))
        .link_2_6('Full')
      endif
        .w_CAVALATT = ''
      .DoRTCalc(9,10,.f.)
        .w_CODFAM = .w_CACODFAM
      .DoRTCalc(11,11,.f.)
      if not(empty(.w_CODFAM))
        .link_2_10('Full')
      endif
      .DoRTCalc(12,12,.f.)
        .w_CAVALATT = ''
    endwith
    this.DoRTCalc(14,37,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_SRV = t_SRV
    this.w_CACODMOD = t_CACODMOD
    this.w_GEST = t_GEST
    this.w_CACODGRU = t_CACODGRU
    this.w_CACODFAM = t_CACODFAM
    this.w_CAVALATT = t_CAVALATT
    this.w_CADESCRI = t_CADESCRI
    this.w_AUTOMA = t_AUTOMA
    this.w_CODFAM = t_CODFAM
    this.w_TIPO = t_TIPO
    this.w_CAVALATT = t_CAVALATT
    this.w_FR_TABLE = t_FR_TABLE
    this.w_FR__ZOOM = t_FR__ZOOM
    this.w_FR_CAMPO = t_FR_CAMPO
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_SRV with this.w_SRV
    replace t_CACODMOD with this.w_CACODMOD
    replace t_GEST with this.w_GEST
    replace t_CACODGRU with this.w_CACODGRU
    replace t_CACODFAM with this.w_CACODFAM
    replace t_CAVALATT with this.w_CAVALATT
    replace t_CADESCRI with this.w_CADESCRI
    replace t_AUTOMA with this.w_AUTOMA
    replace t_CODFAM with this.w_CODFAM
    replace t_TIPO with this.w_TIPO
    replace t_CAVALATT with this.w_CAVALATT
    replace t_FR_TABLE with this.w_FR_TABLE
    replace t_FR__ZOOM with this.w_FR__ZOOM
    replace t_FR_CAMPO with this.w_FR_CAMPO
    if i_srv='A'
      replace CACODMOD with this.w_CACODMOD
      replace CACODGRU with this.w_CACODGRU
      replace CACODFAM with this.w_CACODFAM
      replace CAVALATT with this.w_CAVALATT
      replace CAVALATT with this.w_CAVALATT
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsgp_ma2Pag1 as StdContainer
  Width  = 467
  height = 275
  stdWidth  = 467
  stdheight = 275
  resizeXpos=358
  resizeYpos=35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_4 as StdButton with uid="NOEAHOMLEJ",left=409, top=225, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare i clienti/fornitori che soddisfano la selezione";
    , HelpContextID = 257296346;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      with this.Parent.oContained
        GSAR_BMG(this.Parent.oContained,"STACL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Upper(This.Parent.ocontained.opARENTOBJECT.class)<>'TGSVE_ALI')
    endwith
   endif
  endfunc


  add object oBtn_1_5 as StdButton with uid="GGGRGWTLAY",left=357, top=225, width=48,height=45,;
    CpPicture="BMP\VISUALIZ.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare i clienti/fornitori che soddisfano la selezione";
    , HelpContextID = 87853456;
    , Caption='V\<isualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        GSAR_BMG(this.Parent.oContained,"VISCL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Upper(This.Parent.ocontained.opARENTOBJECT.class)<>'TGSVE_ALI')
    endwith
   endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=3, width=450,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="CPROWORD",Label1="Riga",Field2="CACODGRU",Label2="Gruppo",Field3="CACODFAM",Label3="Famiglia",Field4="CAVALATT",Label4="Valore Attributo",Field5="CAVALATT",Label5="Valore Attributo",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 185440378

  add object oStr_1_2 as StdString with uid="AJNMBKWFXU",Visible=.t., Left=11, Top=202,;
    Alignment=1, Width=105, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-2,top=22,;
    width=446+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-1,top=23,width=445+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*9*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='MODDATTR|GRUDATTR|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oCADESCRI_2_8.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='MODDATTR'
        oDropInto=this.oBodyCol.oRow.oCACODGRU_2_5
      case cFile='GRUDATTR'
        oDropInto=this.oBodyCol.oRow.oCACODFAM_2_6
    endcase
    return(oDropInto)
  EndFunc


  add object oCADESCRI_2_8 as StdTrsField with uid="VKUTIFLUFP",rtseq=9,rtrep=.t.,;
    cFormVar="w_CADESCRI",value=space(50),enabled=.f.,;
    HelpContextID = 256904081,;
    cTotal="", bFixedPos=.t., cQueryName = "CADESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=327, Left=118, Top=200, InputMask=replicate('X',50)

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsgp_ma2BodyRow as CPBodyRowCnt
  Width=436
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="NDZJTLQCYR",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 50666646,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=40, Left=-2, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oCACODGRU_2_5 as StdTrsField with uid="HFPYRNFAKL",rtseq=6,rtrep=.t.,;
    cFormVar="w_CACODGRU",value=space(10),isprimarykey=.t.,;
    HelpContextID = 204872581,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=97, Left=40, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MODDATTR", oKey_1_1="MACODICE", oKey_1_2="this.w_CACODMOD", oKey_2_1="MACODATT", oKey_2_2="this.w_CACODGRU"

  func oCACODGRU_2_5.mCond()
    with this.Parent.oContained
      return (.oParentObject.cFunction = 'Load' Or .w_SRV='A'  Or .w_AUTOMA <> 'S')
    endwith
  endfunc

  func oCACODGRU_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
      if .not. empty(.w_CACODFAM)
        bRes2=.link_2_6('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCACODGRU_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCACODGRU_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oCACODGRU_2_5.readonly and this.parent.oCACODGRU_2_5.isprimarykey)
    if i_TableProp[this.parent.oContained.MODDATTR_idx,3]<>0
      i_cWhere = i_cWhere+iif(!empty(this.parent.oContained.w_CACODMOD),iif(empty(i_cWhere),''," and ")+"MACODICE="+cp_ToStrODBC(this.Parent.oContained.w_CACODMOD),'')
    else
      i_cWhere = i_cWhere+iif(!empty(this.parent.oContained.w_CACODMOD),iif(empty(i_cWhere),''," and ")+"MACODICE="+cp_ToStr(this.Parent.oContained.w_CACODMOD),'')
    endif
    do cp_zoom with 'MODDATTR','*','MACODICE,MACODATT',cp_AbsName(this.parent,'oCACODGRU_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Gruppi attributi",'gsar_mgg.MODDATTR_VZM',this.parent.oContained
   endif
  endproc

  add object oCACODFAM_2_6 as StdTrsField with uid="LSXLODPEFN",rtseq=7,rtrep=.t.,;
    cFormVar="w_CACODFAM",value=space(10),isprimarykey=.t.,;
    HelpContextID = 221649805,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=97, Left=139, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="GRUDATTR", oKey_1_1="GRCODICE", oKey_1_2="this.w_CACODGRU", oKey_2_1="GRFAMATT", oKey_2_2="this.w_CACODFAM"

  func oCACODFAM_2_6.mCond()
    with this.Parent.oContained
      return (.oParentObject.cFunction = 'Load'   Or .w_SRV='A'  Or   .w_AUTOMA <> 'S')
    endwith
  endfunc

  func oCACODFAM_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCACODFAM_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCACODFAM_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oCACODFAM_2_6.readonly and this.parent.oCACODFAM_2_6.isprimarykey)
    if i_TableProp[this.parent.oContained.GRUDATTR_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"GRCODICE="+cp_ToStrODBC(this.Parent.oContained.w_CACODGRU)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"GRCODICE="+cp_ToStr(this.Parent.oContained.w_CACODGRU)
    endif
    do cp_zoom with 'GRUDATTR','*','GRCODICE,GRFAMATT',cp_AbsName(this.parent,'oCACODFAM_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Famiglie attributi",'',this.parent.oContained
   endif
  endproc

  add object oCAVALATT_2_7 as StdTrsField with uid="RWWCXPJNYH",rtseq=8,rtrep=.t.,;
    cFormVar="w_CAVALATT",value=space(20),isprimarykey=.t.,;
    HelpContextID = 238883962,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=193, Left=238, Top=0, InputMask=replicate('X',20), bHasZoom = .t. , ReadOnly=.t.

  func oCAVALATT_2_7.mCond()
    with this.Parent.oContained
      return (.w_TIPO<>'L')
    endwith
  endfunc

  func oCAVALATT_2_7.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPO='L')
    endwith
    endif
  endfunc

  proc oCAVALATT_2_7.mZoom
      with this.Parent.oContained
        GSAR_BMG(this.Parent.oContained,"ZAINS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oCAVALATT_2_12 as StdTrsField with uid="TYHCSJWJWO",rtseq=13,rtrep=.t.,;
    cFormVar="w_CAVALATT",value=space(20),isprimarykey=.t.,;
    HelpContextID = 238883962,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=193, Left=238, Top=0, InputMask=replicate('X',20)

  func oCAVALATT_2_12.mCond()
    with this.Parent.oContained
      return (.w_TIPO='L')
    endwith
  endfunc

  func oCAVALATT_2_12.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPO<>'L')
    endwith
    endif
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=8
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsgp_ma2','CCOMMATT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CA__GUID=CCOMMATT.CA__GUID";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
