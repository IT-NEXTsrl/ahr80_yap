* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_bmq                                                        *
*              Set/reset flag esportazione                                     *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-23                                                      *
* Last revis.: 2012-11-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm_bmq",oParentObject)
return(i_retval)

define class tgslm_bmq as StdBatch
  * --- Local variables
  w_PNSERIAL = space(10)
  w_MESS1 = space(50)
  * --- WorkFile variables
  PNT_MAST_idx=0
  PAR_TITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- BATCH CHE REALIZZA LA FUNZIONALITA' SET FLAG ESPORTAZIONE (LANCIATO DA GSLM_KMP)
    if this.oParentObject.w_PNTIVA="S"
      if this.oParentObject.w_PROFIL=0
        vq_exec("..\LEMC\EXE\QUERY\GSLM_BMQ.VQR",this,"__TMP__")
      else
        vq_exec("..\LEMC\EXE\QUERY\GSLM_BMP.VQR",this,"__TMP__")
      endif
      SELECT __TMP__
      GO TOP
      do while NOT EOF()
        this.w_PNSERIAL = __TMP__.PNSERIAL
        * --- Write into PNT_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNNUMTRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'PNT_MAST','PNNUMTRA');
              +i_ccchkf ;
          +" where ";
              +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                 )
        else
          update (i_cTable) set;
              PNNUMTRA = this.oParentObject.w_PROFIL;
              &i_ccchkf. ;
           where;
              PNSERIAL = this.w_PNSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        skip 1
      enddo
    endif
    if this.oParentObject.w_PNTNOIVA="S"
      if this.oParentObject.w_PROFIL=0
        vq_exec("..\LEMC\EXE\QUERY\GSLM1BMQ.VQR",this,"__TMP__")
      else
        vq_exec("..\LEMC\EXE\QUERY\GSLM1BMP.VQR",this,"__TMP__")
      endif
      SELECT __TMP__
      GO TOP
      do while NOT EOF()
        this.w_PNSERIAL = __TMP__.PNSERIAL
        * --- Write into PNT_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNNUMTR2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'PNT_MAST','PNNUMTR2');
              +i_ccchkf ;
          +" where ";
              +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                 )
        else
          update (i_cTable) set;
              PNNUMTR2 = this.oParentObject.w_PROFIL;
              &i_ccchkf. ;
           where;
              PNSERIAL = this.w_PNSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        skip 1
      enddo
    endif
    * --- Write into PAR_TITE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PAR_TITE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="PTSERIAL,PTROWORD,CPROWNUM"
      do vq_exec with 'gslmlbmq',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PTNUMTRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'PAR_TITE','PTNUMTRA');
          +i_ccchkf;
          +" from "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE, "+i_cQueryTable+" _t2 set ";
      +"PAR_TITE.PTNUMTRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'PAR_TITE','PTNUMTRA');
          +Iif(Empty(i_ccchkf),"",",PAR_TITE.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="PAR_TITE.PTSERIAL = t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = t2.CPROWNUM";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set (";
          +"PTNUMTRA";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'PAR_TITE','PTNUMTRA')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="PAR_TITE.PTSERIAL = _t2.PTSERIAL";
              +" and "+"PAR_TITE.PTROWORD = _t2.PTROWORD";
              +" and "+"PAR_TITE.CPROWNUM = _t2.CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PAR_TITE set ";
      +"PTNUMTRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'PAR_TITE','PTNUMTRA');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".PTSERIAL = "+i_cQueryTable+".PTSERIAL";
              +" and "+i_cTable+".PTROWORD = "+i_cQueryTable+".PTROWORD";
              +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PTNUMTRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'PAR_TITE','PTNUMTRA');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.oParentObject.w_PROFIL=0
      this.w_MESS1 = "Reset flag esportazione eseguito con successo"
    else
      this.w_MESS1 = "Set flag esportazione eseguito con successo"
    endif
    ah_ErrorMsg(this.w_MESS1,,"")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PNT_MAST'
    this.cWorkTables[2]='PAR_TITE'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
