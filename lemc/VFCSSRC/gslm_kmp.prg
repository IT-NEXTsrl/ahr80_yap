* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_kmp                                                        *
*              Set/reset flag esportazione                                     *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_98]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-13                                                      *
* Last revis.: 2012-11-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gslm_kmp
* Esco se il modulo � in semplificata
IF NOT LMAVANZATO()
  Ah_ErrorMsg("La funzione � attiva con il modulo in modalit� avanzata")
 return
ENDIF
* --- Fine Area Manuale
return(createobject("tgslm_kmp",oParentObject))

* --- Class definition
define class tgslm_kmp as StdForm
  Top    = 80
  Left   = 127

  * --- Standard Properties
  Width  = 407
  Height = 219
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-30"
  HelpContextID=68567959
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  _IDX = 0
  cPrg = "gslm_kmp"
  cComment = "Set/reset flag esportazione"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_NUMINI = 0
  w_NUMFIN = 0
  w_PNTIVA = space(1)
  w_PNTNOIVA = space(1)
  w_PROFIL = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslm_kmpPag1","gslm_kmp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_NUMINI=0
      .w_NUMFIN=0
      .w_PNTIVA=space(1)
      .w_PNTNOIVA=space(1)
      .w_PROFIL=0
          .DoRTCalc(1,2,.f.)
        .w_NUMINI = 1
        .w_NUMFIN = 999999
        .w_PNTIVA = 'S'
        .w_PNTNOIVA = 'S'
      .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .w_PROFIL = 1
      .oPgFrm.Page1.oPag.oObj_1_16.Calculate(IIF(.w_PROFIL<>0,ah_MsgFormat("Questa procedura permette di assegnare il numero di esportazione %0verso lo studio."),ah_MsgFormat("Questa procedura permette di assegnare a 0 il numero di esportazione %0verso lo studio.")))
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate(IIF(.w_PROFIL<>0,ah_MsgFormat("Utilizzare questa utility per quelle registrazioni che%0non hanno un formato corretto per essere esportate allo studio."),ah_MsgFormat("Utilizzare questa utility per quelle registrazioni che%0devono essere selezionabili nelle esportazioni verso lo studio.")))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(IIF(.w_PROFIL<>0,ah_MsgFormat("Questa procedura permette di assegnare il numero di esportazione %0verso lo studio."),ah_MsgFormat("Questa procedura permette di assegnare a 0 il numero di esportazione %0verso lo studio.")))
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(IIF(.w_PROFIL<>0,ah_MsgFormat("Utilizzare questa utility per quelle registrazioni che%0non hanno un formato corretto per essere esportate allo studio."),ah_MsgFormat("Utilizzare questa utility per quelle registrazioni che%0devono essere selezionabili nelle esportazioni verso lo studio.")))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_16.Calculate(IIF(.w_PROFIL<>0,ah_MsgFormat("Questa procedura permette di assegnare il numero di esportazione %0verso lo studio."),ah_MsgFormat("Questa procedura permette di assegnare a 0 il numero di esportazione %0verso lo studio.")))
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(IIF(.w_PROFIL<>0,ah_MsgFormat("Utilizzare questa utility per quelle registrazioni che%0non hanno un formato corretto per essere esportate allo studio."),ah_MsgFormat("Utilizzare questa utility per quelle registrazioni che%0devono essere selezionabili nelle esportazioni verso lo studio.")))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPNTIVA_1_5.visible=!this.oPgFrm.Page1.oPag.oPNTIVA_1_5.mHide()
    this.oPgFrm.Page1.oPag.oPNTNOIVA_1_6.visible=!this.oPgFrm.Page1.oPag.oPNTNOIVA_1_6.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_16.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_1.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_1.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_2.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_2.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINI_1_3.value==this.w_NUMINI)
      this.oPgFrm.Page1.oPag.oNUMINI_1_3.value=this.w_NUMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFIN_1_4.value==this.w_NUMFIN)
      this.oPgFrm.Page1.oPag.oNUMFIN_1_4.value=this.w_NUMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPNTIVA_1_5.RadioValue()==this.w_PNTIVA)
      this.oPgFrm.Page1.oPag.oPNTIVA_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPNTNOIVA_1_6.RadioValue()==this.w_PNTNOIVA)
      this.oPgFrm.Page1.oPag.oPNTNOIVA_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPROFIL_1_8.value==this.w_PROFIL)
      this.oPgFrm.Page1.oPag.oPROFIL_1_8.value=this.w_PROFIL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_1.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_2.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NUMINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMINI_1_3.SetFocus()
            i_bnoObbl = !empty(.w_NUMINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NUMFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMFIN_1_4.SetFocus()
            i_bnoObbl = !empty(.w_NUMFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PROFIL>-1 AND .w_PROFIL<100)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPROFIL_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il numero deve essere compreso tra 0 e 99")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgslm_kmpPag1 as StdContainer
  Width  = 403
  height = 219
  stdWidth  = 403
  stdheight = 219
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_1_1 as StdField with uid="AFTBRRJBFY",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 230373066,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=96, Top=83

  add object oDATFIN_1_2 as StdField with uid="ELUIONDLWF",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 151926474,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=96, Top=108

  add object oNUMINI_1_3 as StdField with uid="PPBYHAENNI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_NUMINI", cQueryName = "NUMINI",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 230396458,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=350, Top=83, cSayPict='"999999"', cGetPict='"999999"'

  add object oNUMFIN_1_4 as StdField with uid="BXRQNLSQSP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_NUMFIN", cQueryName = "NUMFIN",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 151949866,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=350, Top=108, cSayPict='"999999"', cGetPict='"999999"'

  add object oPNTIVA_1_5 as StdCheck with uid="LSLIBYYWDN",rtseq=5,rtrep=.f.,left=96, top=133, caption="Primanota IVA",;
    ToolTipText = "Aggiorna il numero di esportazione delle registrazioni contabili che movimentano l'IVA",;
    HelpContextID = 87763210,;
    cFormVar="w_PNTIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPNTIVA_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPNTIVA_1_5.GetRadio()
    this.Parent.oContained.w_PNTIVA = this.RadioValue()
    return .t.
  endfunc

  func oPNTIVA_1_5.SetRadio()
    this.Parent.oContained.w_PNTIVA=trim(this.Parent.oContained.w_PNTIVA)
    this.value = ;
      iif(this.Parent.oContained.w_PNTIVA=='S',1,;
      0)
  endfunc

  func oPNTIVA_1_5.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S' AND IsAlt())
    endwith
  endfunc

  add object oPNTNOIVA_1_6 as StdCheck with uid="SIXHYICPKE",rtseq=6,rtrep=.f.,left=96, top=154, caption="Primanota no IVA",;
    ToolTipText = "Aggiorna il numero di esportazione delle registrazioni contabili che non movimentano l'IVA",;
    HelpContextID = 39442231,;
    cFormVar="w_PNTNOIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPNTNOIVA_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oPNTNOIVA_1_6.GetRadio()
    this.Parent.oContained.w_PNTNOIVA = this.RadioValue()
    return .t.
  endfunc

  func oPNTNOIVA_1_6.SetRadio()
    this.Parent.oContained.w_PNTNOIVA=trim(this.Parent.oContained.w_PNTNOIVA)
    this.value = ;
      iif(this.Parent.oContained.w_PNTNOIVA=='S',1,;
      0)
  endfunc

  func oPNTNOIVA_1_6.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S' AND IsAlt())
    endwith
  endfunc


  add object oObj_1_7 as cp_runprogram with uid="UBJGAFIWLE",left=1, top=236, width=104,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSLM_BMP",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 21869850

  add object oPROFIL_1_8 as StdField with uid="VMGXPVMOHJ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PROFIL", cQueryName = "PROFIL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il numero deve essere compreso tra 0 e 99",;
    ToolTipText = "Numero del trasferimento che si vuole associare alle registrazioni ('0' = da esportare)",;
    HelpContextID = 185496842,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=350, Top=133

  func oPROFIL_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PROFIL>-1 AND .w_PROFIL<100)
    endwith
    return bRes
  endfunc


  add object oBtn_1_13 as StdButton with uid="IFISWSZGLE",left=288, top=168, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 68596710;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      with this.Parent.oContained
        do gslm_bmq with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_13.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_PNTIVA='S' OR .w_PNTNOIVA='S')
      endwith
    endif
  endfunc


  add object oBtn_1_14 as StdButton with uid="RMAHAARJLI",left=346, top=168, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 75885382;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_16 as cp_calclbl with uid="JVRVKABIDK",left=6, top=6, width=389,height=35,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 21869850


  add object oObj_1_17 as cp_calclbl with uid="BUFMTZJEKG",left=6, top=42, width=389,height=35,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 21869850

  add object oStr_1_9 as StdString with uid="EPNVHUQPIO",Visible=.t., Left=6, Top=81,;
    Alignment=1, Width=88, Height=18,;
    Caption="Dalla data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="YMWAHNTHSX",Visible=.t., Left=6, Top=105,;
    Alignment=1, Width=88, Height=18,;
    Caption="Alla data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="LTLFRRITIE",Visible=.t., Left=234, Top=83,;
    Alignment=1, Width=113, Height=18,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="MBTMFYSQEF",Visible=.t., Left=234, Top=108,;
    Alignment=1, Width=113, Height=18,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="SDCZFZAMNO",Visible=.t., Left=234, Top=133,;
    Alignment=1, Width=113, Height=18,;
    Caption="Num. trasferimento:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslm_kmp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
