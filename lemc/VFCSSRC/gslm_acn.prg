* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_acn                                                        *
*              Codici norme studio                                             *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_66]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-18                                                      *
* Last revis.: 2008-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gslm_acn
* Esco se il modulo Ŕ in semplificata
IF NOT LMAVANZATO()
  Ah_ErrorMsg("La funzione Ŕ attiva con il modulo in modalitÓ avanzata")
  return
ENDIF
* --- Fine Area Manuale
return(createobject("tgslm_acn"))

* --- Class definition
define class tgslm_acn as StdForm
  Top    = 60
  Left   = 104

  * --- Standard Properties
  Width  = 546
  Height = 186+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-23"
  HelpContextID=158745495
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  STU_NORM_IDX = 0
  STU_CIVA_IDX = 0
  cFile = "STU_NORM"
  cKeySelect = "LMCODNOR"
  cKeyWhere  = "LMCODNOR=this.w_LMCODNOR"
  cKeyWhereODBC = '"LMCODNOR="+cp_ToStrODBC(this.w_LMCODNOR)';

  cKeyWhereODBCqualified = '"STU_NORM.LMCODNOR="+cp_ToStrODBC(this.w_LMCODNOR)';

  cPrg = "gslm_acn"
  cComment = "Codici norme studio"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LMCODNOR = space(2)
  w_LMDESNOR = space(40)
  w_LMDEANOR = space(20)
  w_LMINDETR = 0
  w_LMULTDET = 0
  w_LMCODIVA = space(2)
  w_LMTIPOLO = space(1)
  w_LMALLEGA = space(1)
  w_APPO = space(40)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'STU_NORM','gslm_acn')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslm_acnPag1","gslm_acn",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Codice")
      .Pages(1).HelpContextID = 183109850
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLMCODNOR_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='STU_CIVA'
    this.cWorkTables[2]='STU_NORM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.STU_NORM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.STU_NORM_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_LMCODNOR = NVL(LMCODNOR,space(2))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_11_joined
    link_1_11_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from STU_NORM where LMCODNOR=KeySet.LMCODNOR
    *
    i_nConn = i_TableProp[this.STU_NORM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('STU_NORM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "STU_NORM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' STU_NORM '
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LMCODNOR',this.w_LMCODNOR  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_APPO = space(40)
        .w_LMCODNOR = NVL(LMCODNOR,space(2))
        .w_LMDESNOR = NVL(LMDESNOR,space(40))
        .w_LMDEANOR = NVL(LMDEANOR,space(20))
        .w_LMINDETR = NVL(LMINDETR,0)
        .w_LMULTDET = NVL(LMULTDET,0)
        .w_LMCODIVA = NVL(LMCODIVA,space(2))
          if link_1_11_joined
            this.w_LMCODIVA = NVL(LMCODIVA111,NVL(this.w_LMCODIVA,space(2)))
            this.w_APPO = NVL(LMDESIVA111,space(40))
          else
          .link_1_11('Load')
          endif
        .w_LMTIPOLO = NVL(LMTIPOLO,space(1))
        .w_LMALLEGA = NVL(LMALLEGA,space(1))
        cp_LoadRecExtFlds(this,'STU_NORM')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LMCODNOR = space(2)
      .w_LMDESNOR = space(40)
      .w_LMDEANOR = space(20)
      .w_LMINDETR = 0
      .w_LMULTDET = 0
      .w_LMCODIVA = space(2)
      .w_LMTIPOLO = space(1)
      .w_LMALLEGA = space(1)
      .w_APPO = space(40)
      if .cFunction<>"Filter"
        .DoRTCalc(1,6,.f.)
          if not(empty(.w_LMCODIVA))
          .link_1_11('Full')
          endif
        .w_LMTIPOLO = 'A'
        .w_LMALLEGA = 'S'
      endif
    endwith
    cp_BlankRecExtFlds(this,'STU_NORM')
    this.DoRTCalc(9,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oLMCODNOR_1_1.enabled = i_bVal
      .Page1.oPag.oLMDESNOR_1_2.enabled = i_bVal
      .Page1.oPag.oLMDEANOR_1_5.enabled = i_bVal
      .Page1.oPag.oLMINDETR_1_7.enabled = i_bVal
      .Page1.oPag.oLMULTDET_1_9.enabled = i_bVal
      .Page1.oPag.oLMCODIVA_1_11.enabled = i_bVal
      .Page1.oPag.oLMTIPOLO_1_13.enabled = i_bVal
      .Page1.oPag.oLMALLEGA_1_15.enabled_(i_bVal)
      if i_cOp = "Edit"
        .Page1.oPag.oLMCODNOR_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oLMCODNOR_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'STU_NORM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.STU_NORM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCODNOR,"LMCODNOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMDESNOR,"LMDESNOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMDEANOR,"LMDEANOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMINDETR,"LMINDETR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMULTDET,"LMULTDET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCODIVA,"LMCODIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMTIPOLO,"LMTIPOLO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMALLEGA,"LMALLEGA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.STU_NORM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])
    i_lTable = "STU_NORM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.STU_NORM_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.STU_NORM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.STU_NORM_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into STU_NORM
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'STU_NORM')
        i_extval=cp_InsertValODBCExtFlds(this,'STU_NORM')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(LMCODNOR,LMDESNOR,LMDEANOR,LMINDETR,LMULTDET"+;
                  ",LMCODIVA,LMTIPOLO,LMALLEGA "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_LMCODNOR)+;
                  ","+cp_ToStrODBC(this.w_LMDESNOR)+;
                  ","+cp_ToStrODBC(this.w_LMDEANOR)+;
                  ","+cp_ToStrODBC(this.w_LMINDETR)+;
                  ","+cp_ToStrODBC(this.w_LMULTDET)+;
                  ","+cp_ToStrODBCNull(this.w_LMCODIVA)+;
                  ","+cp_ToStrODBC(this.w_LMTIPOLO)+;
                  ","+cp_ToStrODBC(this.w_LMALLEGA)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'STU_NORM')
        i_extval=cp_InsertValVFPExtFlds(this,'STU_NORM')
        cp_CheckDeletedKey(i_cTable,0,'LMCODNOR',this.w_LMCODNOR)
        INSERT INTO (i_cTable);
              (LMCODNOR,LMDESNOR,LMDEANOR,LMINDETR,LMULTDET,LMCODIVA,LMTIPOLO,LMALLEGA  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_LMCODNOR;
                  ,this.w_LMDESNOR;
                  ,this.w_LMDEANOR;
                  ,this.w_LMINDETR;
                  ,this.w_LMULTDET;
                  ,this.w_LMCODIVA;
                  ,this.w_LMTIPOLO;
                  ,this.w_LMALLEGA;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.STU_NORM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.STU_NORM_IDX,i_nConn)
      *
      * update STU_NORM
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'STU_NORM')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " LMDESNOR="+cp_ToStrODBC(this.w_LMDESNOR)+;
             ",LMDEANOR="+cp_ToStrODBC(this.w_LMDEANOR)+;
             ",LMINDETR="+cp_ToStrODBC(this.w_LMINDETR)+;
             ",LMULTDET="+cp_ToStrODBC(this.w_LMULTDET)+;
             ",LMCODIVA="+cp_ToStrODBCNull(this.w_LMCODIVA)+;
             ",LMTIPOLO="+cp_ToStrODBC(this.w_LMTIPOLO)+;
             ",LMALLEGA="+cp_ToStrODBC(this.w_LMALLEGA)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'STU_NORM')
        i_cWhere = cp_PKFox(i_cTable  ,'LMCODNOR',this.w_LMCODNOR  )
        UPDATE (i_cTable) SET;
              LMDESNOR=this.w_LMDESNOR;
             ,LMDEANOR=this.w_LMDEANOR;
             ,LMINDETR=this.w_LMINDETR;
             ,LMULTDET=this.w_LMULTDET;
             ,LMCODIVA=this.w_LMCODIVA;
             ,LMTIPOLO=this.w_LMTIPOLO;
             ,LMALLEGA=this.w_LMALLEGA;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.STU_NORM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.STU_NORM_IDX,i_nConn)
      *
      * delete STU_NORM
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'LMCODNOR',this.w_LMCODNOR  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.STU_NORM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LMCODIVA
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_CIVA_IDX,3]
    i_lTable = "STU_CIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_CIVA_IDX,2], .t., this.STU_CIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_CIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSLM_AIV',True,'STU_CIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODIVA like "+cp_ToStrODBC(trim(this.w_LMCODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select LMCODIVA,LMDESIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODIVA',trim(this.w_LMCODIVA))
          select LMCODIVA,LMDESIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMCODIVA)==trim(_Link_.LMCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LMCODIVA) and !this.bDontReportError
            deferred_cp_zoom('STU_CIVA','*','LMCODIVA',cp_AbsName(oSource.parent,'oLMCODIVA_1_11'),i_cWhere,'GSLM_AIV',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODIVA,LMDESIVA";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODIVA',oSource.xKey(1))
            select LMCODIVA,LMDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODIVA,LMDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODIVA="+cp_ToStrODBC(this.w_LMCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODIVA',this.w_LMCODIVA)
            select LMCODIVA,LMDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMCODIVA = NVL(_Link_.LMCODIVA,space(2))
      this.w_APPO = NVL(_Link_.LMDESIVA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_LMCODIVA = space(2)
      endif
      this.w_APPO = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_CIVA_IDX,2])+'\'+cp_ToStr(_Link_.LMCODIVA,1)
      cp_ShowWarn(i_cKey,this.STU_CIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.STU_CIVA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.STU_CIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.LMCODIVA as LMCODIVA111"+ ",link_1_11.LMDESIVA as LMDESIVA111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on STU_NORM.LMCODIVA=link_1_11.LMCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and STU_NORM.LMCODIVA=link_1_11.LMCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLMCODNOR_1_1.value==this.w_LMCODNOR)
      this.oPgFrm.Page1.oPag.oLMCODNOR_1_1.value=this.w_LMCODNOR
    endif
    if not(this.oPgFrm.Page1.oPag.oLMDESNOR_1_2.value==this.w_LMDESNOR)
      this.oPgFrm.Page1.oPag.oLMDESNOR_1_2.value=this.w_LMDESNOR
    endif
    if not(this.oPgFrm.Page1.oPag.oLMDEANOR_1_5.value==this.w_LMDEANOR)
      this.oPgFrm.Page1.oPag.oLMDEANOR_1_5.value=this.w_LMDEANOR
    endif
    if not(this.oPgFrm.Page1.oPag.oLMINDETR_1_7.value==this.w_LMINDETR)
      this.oPgFrm.Page1.oPag.oLMINDETR_1_7.value=this.w_LMINDETR
    endif
    if not(this.oPgFrm.Page1.oPag.oLMULTDET_1_9.value==this.w_LMULTDET)
      this.oPgFrm.Page1.oPag.oLMULTDET_1_9.value=this.w_LMULTDET
    endif
    if not(this.oPgFrm.Page1.oPag.oLMCODIVA_1_11.value==this.w_LMCODIVA)
      this.oPgFrm.Page1.oPag.oLMCODIVA_1_11.value=this.w_LMCODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oLMTIPOLO_1_13.RadioValue()==this.w_LMTIPOLO)
      this.oPgFrm.Page1.oPag.oLMTIPOLO_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLMALLEGA_1_15.RadioValue()==this.w_LMALLEGA)
      this.oPgFrm.Page1.oPag.oLMALLEGA_1_15.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'STU_NORM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_LMCODNOR))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLMCODNOR_1_1.SetFocus()
            i_bnoObbl = !empty(.w_LMCODNOR)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgslm_acnPag1 as StdContainer
  Width  = 542
  height = 186
  stdWidth  = 542
  stdheight = 186
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLMCODNOR_1_1 as StdField with uid="MGAAXTIBIE",rtseq=1,rtrep=.f.,;
    cFormVar = "w_LMCODNOR", cQueryName = "LMCODNOR",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice norma",;
    HelpContextID = 66468344,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=51, Left=160, Top=11, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  add object oLMDESNOR_1_2 as StdField with uid="MUNWFQVMYY",rtseq=2,rtrep=.f.,;
    cFormVar = "w_LMDESNOR", cQueryName = "LMDESNOR",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione norma",;
    HelpContextID = 51390968,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=160, Top=39, InputMask=replicate('X',40)

  add object oLMDEANOR_1_5 as StdField with uid="IMKMYNDMKF",rtseq=3,rtrep=.f.,;
    cFormVar = "w_LMDEANOR", cQueryName = "LMDEANOR",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione abbreviata",;
    HelpContextID = 70265336,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=160, Top=67, InputMask=replicate('X',20)

  add object oLMINDETR_1_7 as StdField with uid="RNADVUDPLM",rtseq=4,rtrep=.f.,;
    cFormVar = "w_LMINDETR", cQueryName = "LMINDETR",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di indetraibilitÓ",;
    HelpContextID = 217504248,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=160, Top=95, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oLMULTDET_1_9 as StdField with uid="ZCMMIKKBVD",rtseq=5,rtrep=.f.,;
    cFormVar = "w_LMULTDET", cQueryName = "LMULTDET",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ultima indetraibilitÓ",;
    HelpContextID = 217586166,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=369, Top=95, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oLMCODIVA_1_11 as StdField with uid="AKWGHLOYBF",rtseq=6,rtrep=.f.,;
    cFormVar = "w_LMCODIVA", cQueryName = "LMCODIVA",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice IVA associato alla norma",;
    HelpContextID = 150354441,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=497, Top=95, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="STU_CIVA", cZoomOnZoom="GSLM_AIV", oKey_1_1="LMCODIVA", oKey_1_2="this.w_LMCODIVA"

  func oLMCODIVA_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMCODIVA_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLMCODIVA_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STU_CIVA','*','LMCODIVA',cp_AbsName(this.parent,'oLMCODIVA_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSLM_AIV',"",'',this.parent.oContained
  endproc
  proc oLMCODIVA_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSLM_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LMCODIVA=this.parent.oContained.w_LMCODIVA
     i_obj.ecpSave()
  endproc


  add object oLMTIPOLO_1_13 as StdCombo with uid="SSMQPQBMJF",rtseq=7,rtrep=.f.,left=160,top=124,width=110,height=21;
    , ToolTipText = "Tipo registro IVA";
    , HelpContextID = 231003653;
    , cFormVar="w_LMTIPOLO",RowSource=""+"Acquisti,"+"Vendite,"+"Entrambi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oLMTIPOLO_1_13.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'V',;
    iif(this.value =3,'X',;
    space(1)))))
  endfunc
  func oLMTIPOLO_1_13.GetRadio()
    this.Parent.oContained.w_LMTIPOLO = this.RadioValue()
    return .t.
  endfunc

  func oLMTIPOLO_1_13.SetRadio()
    this.Parent.oContained.w_LMTIPOLO=trim(this.Parent.oContained.w_LMTIPOLO)
    this.value = ;
      iif(this.Parent.oContained.w_LMTIPOLO=='A',1,;
      iif(this.Parent.oContained.w_LMTIPOLO=='V',2,;
      iif(this.Parent.oContained.w_LMTIPOLO=='X',3,;
      0)))
  endfunc

  add object oLMALLEGA_1_15 as StdRadio with uid="LUIPZJGTBX",rtseq=8,rtrep=.f.,left=160, top=151, width=77,height=32;
    , ToolTipText = "Flag allegati";
    , cFormVar="w_LMALLEGA", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oLMALLEGA_1_15.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Si"
      this.Buttons(1).HelpContextID = 59155959
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="No"
      this.Buttons(2).HelpContextID = 59155959
      this.Buttons(2).Top=15
      this.SetAll("Width",75)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Flag allegati")
      StdRadio::init()
    endproc

  func oLMALLEGA_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oLMALLEGA_1_15.GetRadio()
    this.Parent.oContained.w_LMALLEGA = this.RadioValue()
    return .t.
  endfunc

  func oLMALLEGA_1_15.SetRadio()
    this.Parent.oContained.w_LMALLEGA=trim(this.Parent.oContained.w_LMALLEGA)
    this.value = ;
      iif(this.Parent.oContained.w_LMALLEGA=='S',1,;
      iif(this.Parent.oContained.w_LMALLEGA=='N',2,;
      0))
  endfunc

  add object oStr_1_3 as StdString with uid="CTURMVNHYD",Visible=.t., Left=5, Top=11,;
    Alignment=1, Width=152, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="ZAJXNWDUFE",Visible=.t., Left=5, Top=39,;
    Alignment=1, Width=152, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="CSMWPTGCBJ",Visible=.t., Left=5, Top=67,;
    Alignment=1, Width=152, Height=18,;
    Caption="Descrizione abbreviata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="LAYHJQMHEE",Visible=.t., Left=5, Top=95,;
    Alignment=1, Width=152, Height=18,;
    Caption="% di indetraibilitÓ:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="LYJRWXOZDI",Visible=.t., Left=219, Top=95,;
    Alignment=1, Width=147, Height=18,;
    Caption="% Ulteriore detraib.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="KXXCMRFRXN",Visible=.t., Left=425, Top=95,;
    Alignment=1, Width=70, Height=18,;
    Caption="Codice IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="FBVITLSAJY",Visible=.t., Left=5, Top=123,;
    Alignment=1, Width=152, Height=18,;
    Caption="Tipologia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="QIRHYUYBUC",Visible=.t., Left=5, Top=151,;
    Alignment=1, Width=152, Height=18,;
    Caption="Allegati:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslm_acn','STU_NORM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LMCODNOR=STU_NORM.LMCODNOR";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
