* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslmcbfv                                                        *
*              CONTROLLO FATTURE VENDITA                                       *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-24                                                      *
* Last revis.: 2012-11-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslmcbfv",oParentObject)
return(i_retval)

define class tgslmcbfv as StdBatch
  * --- Local variables
  w_PROFIL = 0
  w_REGNOVALIDA = .f.
  w_STRINGA = space(120)
  w_ACTUALPOS = 0
  w_PNSERIAL = space(10)
  w_TOTDOC = 0
  w_TOTDOCINVAL = 0
  w_ANCODSTU = space(6)
  w_APRNORM = space(2)
  w_APRCAU = space(3)
  w_IVASTU = space(2)
  w_IVASTUNOR = space(2)
  w_IVACEE = space(2)
  w_ESCI = .f.
  w_OKTRAS = .f.
  w_NOTRAS = 0
  w_IVCODIVA = space(5)
  w_PNCODCAU = space(5)
  w_PNCOMIVA = ctod("  /  /  ")
  w_PNDATREG = ctod("  /  /  ")
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_NUMRIGHE = 0
  w_PROGRIGA = 0
  w_CODCLI = space(15)
  w_CODATT = 0
  w_TIPOREG = space(1)
  w_NUMREG = 0
  w_CODSOGG = space(8)
  w_CODPIVA = space(16)
  w_CODFISC = space(16)
  w_PIUATTIV = space(1)
  w_CODSEZ = 0
  w_MESEAT = 0
  w_FATANNPREC = space(1)
  w_ESIGDIF = space(1)
  w_PAGESIGDIF = space(1)
  w_PNSERORI = space(10)
  w_CODFATTSI = space(1)
  w_IVAIND = .f.
  w_PERCIND = 0
  w_GESTNORMA = .f.
  w_NORMAIND100 = space(2)
  w_NORMANOIVA = space(2)
  w_NORMADIFF = space(2)
  w_FATTDIFF = .f.
  w_MESECOMP = space(4)
  w_ROWNUM = 0
  w_OLDREC = space(11)
  w_TIPREC = space(1)
  w_DATREGOR = ctod("  /  /  ")
  w_DATDOCOR = ctod("  /  /  ")
  w_DATREGIP = ctod("  /  /  ")
  w_DATDOCIP = ctod("  /  /  ")
  w_NORFATT = space(2)
  w_CODNORCAU = space(2)
  w_CODCAUOR = space(5)
  w_CODTRAC = space(10)
  * --- WorkFile variables
  CONTI_idx=0
  STU_NORM_idx=0
  STU_PNTT_idx=0
  STU_TRAS_idx=0
  ATTIDETT_idx=0
  ATTIMAST_idx=0
  AZIENDA_idx=0
  COD_NORM_idx=0
  VOCIIVA_idx=0
  CONTROPA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CONTROLLO VERSO LO STUDIO DELLE FATTURE DI VENDITA e NOTE DI CREDITO
    this.w_PROFIL = this.oparentobject.oparentobject.w_profil
    SELECT FATTVEND
    GO TOP
    * --- Ciclo sul cursore della fatture di vendita
    this.w_OLDREC = "@@@@@@@@@@@"
    do while NOT EOF()
      this.w_CODFATTSI = " "
      this.w_GESTNORMA = .F.
      this.w_FATTDIFF = .F.
      this.w_PROGRIGA = 0
      this.w_ROWNUM = 0
      * --- Messaggio a schermo
      this.w_REGNOVALIDA = .F.
      this.w_TIPREC = FATTVEND.TIPREC
      if (FATTVEND.PNSERIAL+FATTVEND.TIPREC) <> this.w_OLDREC
        if this.w_TIPREC="F"
          AH_MSG("fatture di vendita : reg. num. %1  - data %2",.t.,.f.,.f.,STR(FATTVEND.PNNUMRER,6,0),dtoc(FATTVEND.PNDATREG))
          * --- Scrittura su Log
          this.w_STRINGA = Ah_MsgFormat("%1fatture di vendita : reg. num. %2 - data %3","          ",STR(FATTVEND.PNNUMRER,6,0),dtoc(FATTVEND.PNDATREG))
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        else
          * --- Messaggio a schermo
          AH_MSG("Incassi : reg. num. %1  - data %2",.t.,.f.,.f.,STR(FATTVEND.PNNUMRER,6,0),dtoc(FATTVEND.PNDATREG))
          * --- Scrittura su Log
          this.w_STRINGA = Ah_MsgFormat("%1incassi : reg. num. %2 - data %3","          ",STR(FATTVEND.PNNUMRER,6,0),dtoc(FATTVEND.PNDATREG))
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        endif
      endif
      this.w_OLDREC = FATTVEND.PNSERIAL+FATTVEND.TIPREC
      skip 1
      SELECT FATTVEND
    enddo
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Butto giu' tutte le righe IVA della fattura in questione
    this.w_ESCI = .F.
    this.w_OKTRAS = .T.
    this.w_PROGRIGA = 0
    * --- Posiziono il puntatore al record
    SELECT FATTVEND
    this.w_ACTUALPOS = RECNO()
    do while FATTVEND.PNSERIAL=this.w_PNSERIAL and FATTVEND.TIPREC=this.w_TIPREC AND NOT this.w_ESCI
      this.w_PROGRIGA = this.w_PROGRIGA+1
      if NOT EMPTY(FATTVEND.IVCONTRO)
        * --- Incremento numero di record scritti
        this.oParentObject.w_FATTVEND = this.oParentObject.w_FATTVEND+1
        * --- Se sono in questo caso devo scrivere testata e dettaglio
        FWRITE (this.oParentObject.hFile , "RIGA" , 4 )
        this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
        * --- Recupero e scrittura del codice di Aliquota IVA
        this.w_APRCAU = SPACE(3)
        this.w_APRNORM = SPACE(2)
        this.w_IVASTU = SPACE(2)
        this.w_IVACEE = SPACE(2)
        this.w_PNCODCAU = NVL(FATTVEND.PNCODCAU, " ")
        this.w_IVCODIVA = NVL(FATTVEND.IVCODIVA," ")
        this.w_ROWNUM = NVL(FATTVEND.CPROWNUM, 0)
        * --- Leggo se il codice IVA in oggetto ha una percentuale di indetraibilit�
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIND"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_IVCODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIND;
            from (i_cTable) where;
                IVCODIVA = this.w_IVCODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PERCIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_PERCIND # 0 AND this.w_GESTNORMA
          * --- Sono in presenza di IVA detraibili, aggiorno l avariabile per l'inserimento di una nuova riga per la registrazione con IVA per cassa 
          this.w_IVAIND = .T.
          * --- Incremento il numero di righe associate alla registrazione di uno
          this.w_NUMRIGHE = this.w_NUMRIGHE+1
        else
          this.w_IVAIND = .F.
        endif
        * --- Read from STU_TRAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.STU_TRAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LMAPRNOR,LMAPRCAU,LMIVASTU,LMIVACEE"+;
            " from "+i_cTable+" STU_TRAS where ";
                +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
                +" and LMHOCCAU = "+cp_ToStrODBC(this.w_PNCODCAU);
                +" and LMHOCIVA = "+cp_ToStrODBC(this.w_IVCODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LMAPRNOR,LMAPRCAU,LMIVASTU,LMIVACEE;
            from (i_cTable) where;
                LMCODICE = this.oParentObject.w_ASSOCI;
                and LMHOCCAU = this.w_PNCODCAU;
                and LMHOCIVA = this.w_IVCODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_APRNORM = NVL(cp_ToDate(_read_.LMAPRNOR),cp_NullValue(_read_.LMAPRNOR))
          this.w_APRCAU = NVL(cp_ToDate(_read_.LMAPRCAU),cp_NullValue(_read_.LMAPRCAU))
          this.w_IVASTU = NVL(cp_ToDate(_read_.LMIVASTU),cp_NullValue(_read_.LMIVASTU))
          this.w_IVACEE = NVL(cp_ToDate(_read_.LMIVACEE),cp_NullValue(_read_.LMIVACEE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT FATTVEND
        if this.w_PAGESIGDIF="S"
          * --- Calcolo del codice norma per IVA ad esigibilit� differita
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Verifica se ho il codice IVA studio definito nella norma
        if NOT EMPTY(this.w_APRNORM)
          this.w_IVASTUNOR = SPACE(2)
          * --- Read from STU_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.STU_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_NORM_idx,2],.t.,this.STU_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LMCODIVA"+;
              " from "+i_cTable+" STU_NORM where ";
                  +"LMCODNOR = "+cp_ToStrODBC(this.w_APRNORM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LMCODIVA;
              from (i_cTable) where;
                  LMCODNOR = this.w_APRNORM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_IVASTUNOR = NVL(cp_ToDate(_read_.LMCODIVA),cp_NullValue(_read_.LMCODIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          SELECT FATTVEND
          if this.w_IVASTUNOR="XX"
            this.w_IVASTUNOR = this.w_IVASTU
          endif
        else
          this.w_IVASTUNOR = this.w_IVASTU
        endif
        * --- Controllo Codice IVA
        if (EMPTY(this.w_IVASTUNOR) or this.w_IVASTUNOR="XX")
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Codice IVA non definito %2 nella causale %3","               ",Alltrim(this.w_IVCODIVA),Alltrim(this.w_PNCODCAU))
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        * --- Controllo Codice IVA
        if (FATTVEND.PNTIPDOC="FE" OR FATTVEND.PNTIPDOC="NE") AND EMPTY(this.w_IVACEE)
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Codice CEE non definito nel codice %2 nella causale %3","               ",Alltrim(this.w_IVCODIVA),Alltrim(this.w_PNCODCAU))
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        * --- Scrittura del codice IVA
        FWRITE(this.oParentObject.hFile,right("00"+(this.w_IVASTUNOR),2),2)
        * --- Scrittura codice norma
        if this.w_FATTDIFF and this.w_APRNORM="IS"
          if ! empty(this.w_NORMADIFF)
            FWRITE(this.oParentObject.hFile,this.w_NORMADIFF,2)
          else
            FWRITE(this.oParentObject.hFile,"DS",2)
          endif
        else
          FWRITE(this.oParentObject.hFile,right(space(2)+IIF(EMPTY(this.w_APRNORM),"  ",this.w_APRNORM),2),2)
        endif
        * --- Codice CEE
        FWRITE(this.oParentObject.hFile,IIF(FATTVEND.PNTIPDOC="FE" OR FATTVEND.PNTIPDOC="NE" AND NOT EMPTY(this.w_IVACEE), left(this.w_IVACEE+"  ",2), "  "),2)
        * --- Centro di costo
        FWRITE(this.oParentObject.hFile,"0000",4)
        * --- Contropartita (Sottoconto)
        this.w_ANCODSTU = SPACE(6)
        this.w_PNTIPCLF = NVL(FATTVEND.IVTIPCOP," ")
        this.w_PNCODCLF = NVL(FATTVEND.IVCONTRO," ")
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODSTU"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODSTU;
            from (i_cTable) where;
                ANTIPCON = this.w_PNTIPCLF;
                and ANCODICE = this.w_PNCODCLF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT FATTVEND
        if EMPTY(this.w_ANCODSTU)
          this.w_ANCODSTU = "000000"
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Contropartita %2 non ha un codice sottoconto nello studio associato","               ",NVL(this.w_PNCODCLF, " "))
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        FWRITE(this.oParentObject.hFile,right("000000"+alltrim(this.w_ANCODSTU),6),6)
        * --- Imponibile
        if g_PERVAL=this.oParentObject.w_VALEUR
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(int(FATTVEND.IVIMPONI))),11,0))+RIGHT(STR(FATTVEND.IVIMPONI,12,2),2),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(FATTVEND.IVIMPONI),11,0)),11),11)
        endif
        * --- Segno imponibile
        if INLIST(FATTVEND.PNTIPDOC,"NC","NE","NU")
          FWRITE(this.oParentObject.hFile,IIF(FATTVEND.IVIMPONI>=0, "-", "+"),1)
        else
          FWRITE(this.oParentObject.hFile,IIF(FATTVEND.IVIMPONI>=0, "+", "-"),1)
        endif
        * --- Imposta
        if g_PERVAL=this.oParentObject.w_VALEUR
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(int(FATTVEND.IVIMPIVA))),11,0))+RIGHT(STR(FATTVEND.IVIMPIVA,12,2),2),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(FATTVEND.IVIMPIVA),11,0)),11),11)
        endif
        * --- Segno imposta
        if INLIST(FATTVEND.PNTIPDOC,"NC","NE","NU")
          FWRITE(this.oParentObject.hFile,IIF(FATTVEND.IVIMPIVA>=0, "-", "+"),1)
        else
          FWRITE(this.oParentObject.hFile,IIF(FATTVEND.IVIMPIVA>=0, "+", "-"),1)
        endif
        * --- Importo totale del documento - Lo inserisco solo nell'ultima riga IVA
        if this.w_PROGRIGA=this.w_NUMRIGHE
          if NOT EMPTY(this.w_TOTDOCINVAL) and g_PERVAL <> NVL(FATTVEND.PNCODVAL,"   ")
            * --- Importo in valuta
            FWRITE(this.oParentObject.hFile, ;
            RIGHT("0000000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOCINVAL)),13, 0))+RIGHT(STR(this.w_TOTDOCINVAL,15,2),2),13),13)
            * --- Segno
            if INLIST(FATTVEND.PNTIPDOC,"NC","NE","NU")
              FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOCINVAL>=0, "-", "+"),1)
            else
              FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOCINVAL>=0, "+", "-"),1)
            endif
          else
            FWRITE(this.oParentObject.hFile,repl("0",13),13)
            FWRITE(this.oParentObject.hFile," ",1)
          endif
        else
          * --- Lascio blank i campi di importo totale
          FWRITE(this.oParentObject.hFile,repl("0",13),13)
          FWRITE(this.oParentObject.hFile," ",1)
        endif
        * --- Codice Valuta 
        if g_PERVAL <> NVL(FATTVEND.PNCODVAL,"   ")
          FWRITE(this.oParentObject.hFile,NVL(FATTVEND.PNCODVAL,"   "),3)
        else
          FWRITE(this.oParentObject.hFile,"   ",3)
        endif
        * --- Causale di rettifica
        FWRITE(this.oParentObject.hFile,repl("0",5),5)
        * --- Flag prestazioni di servizi
        do case
          case FATTVEND.IVFLGSER = "S"
            FWRITE(this.oParentObject.hFile,FATTVEND.IVFLGSER,1)
          case FATTVEND.IVFLGSER = "B"
            FWRITE(this.oParentObject.hFile,"N",1)
          case FATTVEND.IVFLGSER = "E"
            FWRITE(this.oParentObject.hFile," ",1)
          otherwise
            FWRITE(this.oParentObject.hFile," ",1)
        endcase
        * --- CodTipoOper
        *     3000Euro
        *     Il campo pu� assumere i seguenti valori:
        *     - E = Esclusa forzata
        *     - N = Non concorre al calcolo del limite per la tipologia op.3000 euro indicata
        *     in testata fattura.
        *     - blk= Non definito
        FWRITE(this.oParentObject.hFile," ",1)
        * --- Filler
        this.oParentObject.w_FILLER = SPACE(132)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,132)
      else
        if NVL(FATTVEND.IVCODIVA,SPACE(5))<>SPACE(5) AND (NVL(FATTVEND.IVIMPONI,0)<>0 OR NVL(FATTVEND.IVIMPIVA,0)<>0 OR NVL(FATTVEND.IVCFLOMA," ")="S")
          * --- Marco questa registrazione come non valida
          this.w_REGNOVALIDA = .T.
        endif
      endif
      if this.w_REGNOVALIDA
        go this.w_ACTUALPOS
        this.w_NOTRAS = -1
        * --- Nel temporaneo di prima nota potrebbe essere presente la registrazione derivata da una fattura UE
        * --- Try
        local bErr_039B6F28
        bErr_039B6F28=bTrsErr
        this.Try_039B6F28()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Essendo w_NOTRAS=-1 sovrascrivo quello che era stato inserito dalla routine GSLM_BFA relativo alle fatture UE
          * --- Write into STU_PNTT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.STU_PNTT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_PNTT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LMNUMTRA ="+cp_NullLink(cp_ToStrODBC(this.w_NOTRAS),'STU_PNTT','LMNUMTRA');
            +",LMNUMTR2 ="+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
                +i_ccchkf ;
            +" where ";
                +"LMSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                   )
          else
            update (i_cTable) set;
                LMNUMTRA = this.w_NOTRAS;
                ,LMNUMTR2 = 0;
                &i_ccchkf. ;
             where;
                LMSERIAL = this.w_PNSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_039B6F28
        * --- End
        this.w_STRINGA = Ah_MsgFormat("%1Errore! Contropartita contabile non definita su riga castelletto IVA ","               ")
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        SELECT FATTVEND
        do while FATTVEND.PNSERIAL=this.w_PNSERIAL
          * --- Avanzo il puntatore
          skip 1
        enddo
        this.w_ESCI = .T.
        this.w_OKTRAS = .F.
      endif
      * --- Se sono in presenza di IVA detraibili procedo all'inserimento di una nuova riga, solo per registrazioni inerenti a IVA per cassa
      if this.w_IVAIND and this.w_GESTNORMA
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Avanzo il puntatore
      if .not. this.w_ESCI
        skip 1
      endif
    enddo
    if this.w_OKTRAS
      * --- Try
      local bErr_039F08B8
      bErr_039F08B8=bTrsErr
      this.Try_039F08B8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Essendo w_PROFIL=numero del trasferimento
        * --- (uguale a quanto inserito dalla routine GSLM_BFA relativo alle fatture UE in caso di successo)
        * --- (in caso di insuccesso il valore inserito dalla routine GSLM_BFA � -1)
        * --- Non bisogna scrivere niente
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_039F08B8
      * --- End
      SELECT FATTVEND
    endif
  endproc
  proc Try_039B6F28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_PNTT
    i_nConn=i_TableProp[this.STU_PNTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOTRAS),'STU_PNTT','LMNUMTRA');
      +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.w_NOTRAS,'LMNUMTR2',0)
      insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_NOTRAS;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='GSLM_BFV: Scrittura con errore in STU_PNTT'
      return
    endif
    return
  proc Try_039F08B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_PNTT
    i_nConn=i_TableProp[this.STU_PNTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PROFIL),'STU_PNTT','LMNUMTRA');
      +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.w_PROFIL,'LMNUMTR2',0)
      insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_PROFIL;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='GSLM_BFV: Scrittura in STU_PNTT'
      return
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo del codice norma
    * --- Dichiarazione variabili per calcolo della norma
    this.w_CODTRAC = "LEMCO"
    * --- Select from GSLM_NOR
    do vq_exec with 'GSLM_NOR',this,'_Curs_GSLM_NOR','',.f.,.t.
    if used('_Curs_GSLM_NOR')
      select _Curs_GSLM_NOR
      locate for 1=1
      do while not(eof())
      * --- Leggo le date di registrazione e documento
      this.w_DATREGOR = cp_todate(_Curs_GSLM_NOR.DATREGOR)
      this.w_DATDOCOR = cp_todate(_Curs_GSLM_NOR.DATDOCOR)
      this.w_DATREGIP = cp_todate(_Curs_GSLM_NOR.DATREGIP) 
      this.w_DATDOCIP = cp_todate(_Curs_GSLM_NOR.DATDOCIP)
      * --- Leggo il codice  causale della registrazione di origine
      this.w_CODCAUOR = _Curs_GSLM_NOR.PNCODCAU
      this.w_IVCODIVA = NVL(_Curs_GSLM_NOR.IVCODIVA," ")
      * --- Verifico se devo gestire l'iva per cassa
      * --- Read from STU_TRAS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.STU_TRAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "LMAPRNOR"+;
          " from "+i_cTable+" STU_TRAS where ";
              +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
              +" and LMHOCCAU = "+cp_ToStrODBC(this.w_CODCAUOR);
              +" and LMHOCIVA = "+cp_ToStrODBC(this.w_IVCODIVA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          LMAPRNOR;
          from (i_cTable) where;
              LMCODICE = this.oParentObject.w_ASSOCI;
              and LMHOCCAU = this.w_CODCAUOR;
              and LMHOCIVA = this.w_IVCODIVA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODNORCAU = NVL(cp_ToDate(_read_.LMAPRNOR),cp_NullValue(_read_.LMAPRNOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if upper(this.w_NORFATT) == upper(this.w_CODNORCAU)
        this.w_GESTNORMA = .T.
      else
        this.w_GESTNORMA = .F.
        if inlist(this.w_CODFATTSI,"S","I") 
          this.w_APRNORM = this.w_NORMANOIVA
        endif
      endif
      exit
        select _Curs_GSLM_NOR
        continue
      enddo
      use
    endif
    if this.w_GESTNORMA
      * --- Applico la norma in base alla modalit� di utilizzo che viene soddisfatta
      do case
        case year(this.w_DATREGOR) = year(this.w_DATREGIP) and abs((this.w_DATDOCOR - this.w_DATREGIP)) <= 365
          * --- Modalit� 2 nella tabella
          * --- Read from COD_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COD_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COD_NORM_idx,2],.t.,this.COD_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNNORMA2"+;
              " from "+i_cTable+" COD_NORM where ";
                  +"CNCODTRA = "+cp_ToStrODBC(this.w_CODTRAC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNNORMA2;
              from (i_cTable) where;
                  CNCODTRA = this.w_CODTRAC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APRNORM = NVL(cp_ToDate(_read_.CNNORMA2),cp_NullValue(_read_.CNNORMA2))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        case year(this.w_DATREGOR) < year(this.w_DATREGIP) and abs((this.w_DATDOCOR - this.w_DATREGIP)) <= 365
          * --- Modalit� 3 nella tabella
          * --- Read from COD_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COD_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COD_NORM_idx,2],.t.,this.COD_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNNORMA3"+;
              " from "+i_cTable+" COD_NORM where ";
                  +"CNCODTRA = "+cp_ToStrODBC(this.w_CODTRAC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNNORMA3;
              from (i_cTable) where;
                  CNCODTRA = this.w_CODTRAC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APRNORM = NVL(cp_ToDate(_read_.CNNORMA3),cp_NullValue(_read_.CNNORMA3))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        case year(this.w_DATREGOR) = year(this.w_DATREGIP) and abs((this.w_DATDOCOR - this.w_DATREGIP)) > 365
          * --- Modalit� 4 nella tabella
          * --- Read from COD_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COD_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COD_NORM_idx,2],.t.,this.COD_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNNORMA4"+;
              " from "+i_cTable+" COD_NORM where ";
                  +"CNCODTRA = "+cp_ToStrODBC(this.w_CODTRAC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNNORMA4;
              from (i_cTable) where;
                  CNCODTRA = this.w_CODTRAC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APRNORM = NVL(cp_ToDate(_read_.CNNORMA4),cp_NullValue(_read_.CNNORMA4))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        case year(this.w_DATREGOR) < year(this.w_DATREGIP) and abs((this.w_DATDOCOR - this.w_DATREGIP)) > 365
          * --- Modalit� 5 nella tabella
          * --- Read from COD_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COD_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COD_NORM_idx,2],.t.,this.COD_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNNORMA5"+;
              " from "+i_cTable+" COD_NORM where ";
                  +"CNCODTRA = "+cp_ToStrODBC(this.w_CODTRAC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNNORMA5;
              from (i_cTable) where;
                  CNCODTRA = this.w_CODTRAC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APRNORM = NVL(cp_ToDate(_read_.CNNORMA5),cp_NullValue(_read_.CNNORMA5))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
      endcase
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiungo la riga relativa a IVA indetraibile per codice iva sospesa e iva indetraibile 
    this.w_PROGRIGA = this.w_PROGRIGA+1
    if NOT EMPTY(fattvenU.IVCONTRO)
      * --- Incremento numero di record scritti
      this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
      this.oParentObject.w_FATTVEND = this.oParentObject.w_FATTVEND+1
      * --- Se sono in questo caso devo scrivere testata e dettaglio
      FWRITE (this.oParentObject.hFile , "RIGA" , 4 )
      * --- Scrittura del codice IVA
      FWRITE(this.oParentObject.hFile,right("00"+(this.w_IVASTUNOR),2),2)
      * --- Scrittura del Codice Norma
      if this.w_PAGESIGDIF#"S"
        if EMPTY(this.w_NORMAIND100)
          FWRITE(this.oParentObject.hFile,"90",2)
        else
          FWRITE(this.oParentObject.hFile,this.w_NORMAIND100,2)
        endif
      else
        if EMPTY(this.w_NORMANOIVA)
          FWRITE(this.oParentObject.hFile,"IG",2)
        else
          FWRITE(this.oParentObject.hFile,this.w_NORMANOIVA,2)
        endif
      endif
      * --- Codice CEE
      FWRITE(this.oParentObject.hFile,IIF(fattvenU.PNTIPDOC="FE" OR fattvenU.PNTIPDOC="NE" AND NOT EMPTY(this.w_IVACEE), right("  "+this.w_IVACEE,2), "  "),2)
      * --- Contropartita (Sottoconto)
      this.w_ANCODSTU = SPACE(6)
      this.w_PNTIPCLF = NVL(fattvenU.IVTIPCOP," ")
      this.w_PNCODCLF = NVL(fattvenU.IVCONTRO," ")
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCLF;
              and ANCODICE = this.w_PNCODCLF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT fattvenU
      if EMPTY(this.w_ANCODSTU)
        this.w_ANCODSTU = "000000"
        this.w_STRINGA = Ah_msgFormat("%1Errore! Contropartita %2 non ha un codice sottoconto nello studio associato","               ",NVL(this.w_PNCODCLF, " "))
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      * --- Imponibile
      if g_PERVAL=this.oParentObject.w_VALEUR
        if ! this.w_IVAIND
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(int(fattvenU.IVIMPONI))),11,0))+RIGHT(STR(fattvenU.IVIMPONI,12,2),2),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS((fattvenU.IVIMPONI/100)*this.w_PERCIND)),11,0))+RIGHT(STR((fattvenU.IVIMPONI/100)*this.w_PERCIND,12,2),2),11),11)
        endif
      else
        if ! this.w_IVAIND
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(fattvenU.IVIMPONI),11,0)),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS((fattvenU.IVIMPONI/100)*this.w_PERCIND),11,0)),11),11)
        endif
      endif
      if INLIST(fattvenU.PNTIPDOC,"NC","NE","NU")
        FWRITE(this.oParentObject.hFile,IIF(fattvenU.IVIMPONI>=0, "-", "+"),1)
      else
        FWRITE(this.oParentObject.hFile,IIF(fattvenU.IVIMPONI>=0, "+", "-"),1)
      endif
      * --- Imposta
      if g_PERVAL=this.oParentObject.w_VALEUR
        if ! this.w_IVAIND
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(fattvenU.IVIMPIVA)),11,0))+RIGHT(STR(fattvenU.IVIMPIVA,12,2),2),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS((fattvenU.IVIMPIVA/100)*this.w_PERCIND)),11,0))+RIGHT(STR((fattvenU.IVIMPIVA/100)*this.w_PERCIND,12,2),2),11),11)
        endif
      else
        if ! this.w_IVAIND
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(fattvenU.IVIMPIVA),11,0)),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS((fattvenU.IVIMPIVA/100)*this.w_PERCIND),11,0)),11),11)
        endif
      endif
      if INLIST(fattvenU.PNTIPDOC,"NC","NE","NU")
        FWRITE(this.oParentObject.hFile,IIF(fattvenU.IVIMPIVA>=0, "-", "+"),1)
      else
        FWRITE(this.oParentObject.hFile,IIF(fattvenU.IVIMPIVA>=0, "+", "-"),1)
      endif
      * --- Importo totale del documento - Lo inserisco solo nell'ultima riga IVA
      if this.w_PROGRIGA=this.w_NUMRIGHE
        if NOT EMPTY(this.w_TOTDOCINVAL) and g_PERVAL <> NVL(fattvenU.PNCODVAL,"   ")
          * --- Importo in valuta
          FWRITE(this.oParentObject.hFile,;
          RIGHT("0000000000000"+ALLTRIM(STR(INT(ABS(int(this.w_TOTDOCINVAL))),11, 0))+RIGHT(STR(this.w_TOTDOCINVAL,14,2),2),13),13)
          * --- Segno
          if fattvenU.PNTIPDOC="NE" or fattvenU.PNTIPDOC="NC" or fattvenU.PNTIPDOC="NU"
            FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOCINVAL>=0, "-", "+"),1)
          else
            FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOCINVAL>=0, "+", "-"),1)
          endif
        else
          FWRITE(this.oParentObject.hFile,repl("0",13),13)
          FWRITE(this.oParentObject.hFile," ",1)
        endif
      else
        * --- Lascio blank i campi di importo totale
        FWRITE(this.oParentObject.hFile,repl("0",13),13)
        FWRITE(this.oParentObject.hFile," ",1)
      endif
      * --- Codice Valuta
      if g_PERVAL <> NVL(fattvenU.PNCODVAL,"   ")
        FWRITE(this.oParentObject.hFile,NVL(fattvenU.PNCODVAL,"   "),3)
      else
        FWRITE(this.oParentObject.hFile,"   ",3)
      endif
      * --- Causale di rettifica
      FWRITE(this.oParentObject.hFile,repl("0",5),5)
      * --- Flag prestazioni di servizi
      do case
        case FATTVEND.IVFLGSER = "S"
          FWRITE(this.oParentObject.hFile,FATTVEND.IVFLGSER,1)
        case FATTVEND.IVFLGSER = "B"
          FWRITE(this.oParentObject.hFile,"N",1)
        case FATTVEND.IVFLGSER = "E"
          FWRITE(this.oParentObject.hFile," ",1)
        otherwise
          FWRITE(this.oParentObject.hFile," ",1)
      endcase
      * --- CodTipoOper
      *     3000Euro
      *     Il campo pu� assumere i seguenti valori:
      *     - E = Esclusa forzata
      *     - N = Non concorre al calcolo del limite per la tipologia op.3000 euro indicata
      *     in testata fattura.
      *     - blk= Non definito
      FWRITE(this.oParentObject.hFile," ",1)
      * --- Filler
      this.oParentObject.w_FILLER = SPACE(132)
      FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,132)
    else
      if NVL(fattvenU.IVCODIVA,SPACE(5))<>SPACE(5) AND (NVL(fattvenU.IVIMPONI,0)<>0 OR NVL(fattvenU.IVIMPIVA,0)<>0 OR NVL(fattvenU.IVCFLOMA," ")="S")
        * --- Marco questa registrazione come non valida
        this.w_REGNOVALIDA = .T.
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='STU_NORM'
    this.cWorkTables[3]='STU_PNTT'
    this.cWorkTables[4]='STU_TRAS'
    this.cWorkTables[5]='ATTIDETT'
    this.cWorkTables[6]='ATTIMAST'
    this.cWorkTables[7]='AZIENDA'
    this.cWorkTables[8]='COD_NORM'
    this.cWorkTables[9]='VOCIIVA'
    this.cWorkTables[10]='CONTROPA'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_GSLM_NOR')
      use in _Curs_GSLM_NOR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
