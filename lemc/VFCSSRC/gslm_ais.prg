* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_ais                                                        *
*              Operazioni INTRA studio                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_21]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-17                                                      *
* Last revis.: 2008-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gslm_ais
* Esco se il modulo � in semplificata
IF NOT LMAVANZATO()
  Ah_ErrorMsg("La funzione � attiva con il modulo in modalit� avanzata")
  return
ENDIF
* --- Fine Area Manuale
return(createobject("tgslm_ais"))

* --- Class definition
define class tgslm_ais as StdForm
  Top    = 53
  Left   = 64

  * --- Standard Properties
  Width  = 529
  Height = 97+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-23"
  HelpContextID=9026665
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  STU_INTR_IDX = 0
  cFile = "STU_INTR"
  cKeySelect = "LMCODOPE"
  cKeyWhere  = "LMCODOPE=this.w_LMCODOPE"
  cKeyWhereODBC = '"LMCODOPE="+cp_ToStrODBC(this.w_LMCODOPE)';

  cKeyWhereODBCqualified = '"STU_INTR.LMCODOPE="+cp_ToStrODBC(this.w_LMCODOPE)';

  cPrg = "gslm_ais"
  cComment = "Operazioni INTRA studio"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LMCODOPE = space(2)
  w_LMDESOPE = space(40)
  w_LMDESABB = space(20)
  w_LMTIPOPE = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'STU_INTR','gslm_ais')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslm_aisPag1","gslm_ais",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Operazione intra studio")
      .Pages(1).HelpContextID = 83416426
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLMCODOPE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='STU_INTR'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.STU_INTR_IDX,5],7]
    this.nPostItConn=i_TableProp[this.STU_INTR_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_LMCODOPE = NVL(LMCODOPE,space(2))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from STU_INTR where LMCODOPE=KeySet.LMCODOPE
    *
    i_nConn = i_TableProp[this.STU_INTR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STU_INTR_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('STU_INTR')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "STU_INTR.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' STU_INTR '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LMCODOPE',this.w_LMCODOPE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_LMCODOPE = NVL(LMCODOPE,space(2))
        .w_LMDESOPE = NVL(LMDESOPE,space(40))
        .w_LMDESABB = NVL(LMDESABB,space(20))
        .w_LMTIPOPE = NVL(LMTIPOPE,space(1))
        cp_LoadRecExtFlds(this,'STU_INTR')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LMCODOPE = space(2)
      .w_LMDESOPE = space(40)
      .w_LMDESABB = space(20)
      .w_LMTIPOPE = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,3,.f.)
        .w_LMTIPOPE = 'A'
      endif
    endwith
    cp_BlankRecExtFlds(this,'STU_INTR')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oLMCODOPE_1_1.enabled = i_bVal
      .Page1.oPag.oLMDESOPE_1_3.enabled = i_bVal
      .Page1.oPag.oLMDESABB_1_4.enabled = i_bVal
      .Page1.oPag.oLMTIPOPE_1_6.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oLMCODOPE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oLMCODOPE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'STU_INTR',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.STU_INTR_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCODOPE,"LMCODOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMDESOPE,"LMDESOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMDESABB,"LMDESABB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMTIPOPE,"LMTIPOPE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.STU_INTR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STU_INTR_IDX,2])
    i_lTable = "STU_INTR"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.STU_INTR_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.STU_INTR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_INTR_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.STU_INTR_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into STU_INTR
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'STU_INTR')
        i_extval=cp_InsertValODBCExtFlds(this,'STU_INTR')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(LMCODOPE,LMDESOPE,LMDESABB,LMTIPOPE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_LMCODOPE)+;
                  ","+cp_ToStrODBC(this.w_LMDESOPE)+;
                  ","+cp_ToStrODBC(this.w_LMDESABB)+;
                  ","+cp_ToStrODBC(this.w_LMTIPOPE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'STU_INTR')
        i_extval=cp_InsertValVFPExtFlds(this,'STU_INTR')
        cp_CheckDeletedKey(i_cTable,0,'LMCODOPE',this.w_LMCODOPE)
        INSERT INTO (i_cTable);
              (LMCODOPE,LMDESOPE,LMDESABB,LMTIPOPE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_LMCODOPE;
                  ,this.w_LMDESOPE;
                  ,this.w_LMDESABB;
                  ,this.w_LMTIPOPE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.STU_INTR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_INTR_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.STU_INTR_IDX,i_nConn)
      *
      * update STU_INTR
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'STU_INTR')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " LMDESOPE="+cp_ToStrODBC(this.w_LMDESOPE)+;
             ",LMDESABB="+cp_ToStrODBC(this.w_LMDESABB)+;
             ",LMTIPOPE="+cp_ToStrODBC(this.w_LMTIPOPE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'STU_INTR')
        i_cWhere = cp_PKFox(i_cTable  ,'LMCODOPE',this.w_LMCODOPE  )
        UPDATE (i_cTable) SET;
              LMDESOPE=this.w_LMDESOPE;
             ,LMDESABB=this.w_LMDESABB;
             ,LMTIPOPE=this.w_LMTIPOPE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.STU_INTR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_INTR_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.STU_INTR_IDX,i_nConn)
      *
      * delete STU_INTR
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'LMCODOPE',this.w_LMCODOPE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.STU_INTR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STU_INTR_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLMCODOPE_1_1.value==this.w_LMCODOPE)
      this.oPgFrm.Page1.oPag.oLMCODOPE_1_1.value=this.w_LMCODOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oLMDESOPE_1_3.value==this.w_LMDESOPE)
      this.oPgFrm.Page1.oPag.oLMDESOPE_1_3.value=this.w_LMDESOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oLMDESABB_1_4.value==this.w_LMDESABB)
      this.oPgFrm.Page1.oPag.oLMDESABB_1_4.value=this.w_LMDESABB
    endif
    if not(this.oPgFrm.Page1.oPag.oLMTIPOPE_1_6.RadioValue()==this.w_LMTIPOPE)
      this.oPgFrm.Page1.oPag.oLMTIPOPE_1_6.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'STU_INTR')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_LMCODOPE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLMCODOPE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_LMCODOPE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgslm_aisPag1 as StdContainer
  Width  = 525
  height = 97
  stdWidth  = 525
  stdheight = 97
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLMCODOPE_1_1 as StdField with uid="PAHPJTXCXJ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_LMCODOPE", cQueryName = "LMCODOPE",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice INTRA",;
    HelpContextID = 217463301,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=36, Left=159, Top=13, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  add object oLMDESOPE_1_3 as StdField with uid="SGPKQQORTO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_LMDESOPE", cQueryName = "LMDESOPE",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione INTRA",;
    HelpContextID = 202385925,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=194, Top=13, InputMask=replicate('X',40)

  add object oLMDESABB_1_4 as StdField with uid="WDIVFPKEDO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_LMDESABB", cQueryName = "LMDESABB",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione abbreviata",;
    HelpContextID = 99603960,;
   bGlobalFont=.t.,;
    Height=21, Width=168, Left=159, Top=42, InputMask=replicate('X',20)


  add object oLMTIPOPE_1_6 as StdCombo with uid="DBQRBVECVY",rtseq=4,rtrep=.f.,left=159,top=71,width=119,height=21;
    , ToolTipText = "Tipo registro IVA";
    , HelpContextID = 205203973;
    , cFormVar="w_LMTIPOPE",RowSource=""+"Acquisti,"+"Vendite", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oLMTIPOPE_1_6.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'V',;
    space(1))))
  endfunc
  func oLMTIPOPE_1_6.GetRadio()
    this.Parent.oContained.w_LMTIPOPE = this.RadioValue()
    return .t.
  endfunc

  func oLMTIPOPE_1_6.SetRadio()
    this.Parent.oContained.w_LMTIPOPE=trim(this.Parent.oContained.w_LMTIPOPE)
    this.value = ;
      iif(this.Parent.oContained.w_LMTIPOPE=='A',1,;
      iif(this.Parent.oContained.w_LMTIPOPE=='V',2,;
      0))
  endfunc

  add object oStr_1_2 as StdString with uid="ZTWKNTDVDS",Visible=.t., Left=3, Top=13,;
    Alignment=1, Width=152, Height=18,;
    Caption="Operazione:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="GOMXRAUJBD",Visible=.t., Left=3, Top=42,;
    Alignment=1, Width=152, Height=18,;
    Caption="Descrizione abbreviata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="TRXVQVNSHN",Visible=.t., Left=3, Top=71,;
    Alignment=1, Width=152, Height=18,;
    Caption="Tipo operazione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslm_ais','STU_INTR','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LMCODOPE=STU_INTR.LMCODOPE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
