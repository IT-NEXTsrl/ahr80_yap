* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_acc                                                        *
*              Causali contabili studio                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_25]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-18                                                      *
* Last revis.: 2017-02-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gslm_acc
* Esco se il modulo � in semplificata
IF NOT LMAVANZATO()
  Ah_ErrorMsg("La funzione � attiva con il modulo in modalit� avanzata")
 return
ENDIF
* --- Fine Area Manuale
return(createobject("tgslm_acc"))

* --- Class definition
define class tgslm_acc as StdForm
  Top    = 61
  Left   = 119

  * --- Standard Properties
  Width  = 476
  Height = 163+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-02-20"
  HelpContextID=109689961
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=7

  * --- Constant Properties
  STU_CAUS_IDX = 0
  cFile = "STU_CAUS"
  cKeySelect = "LMCODCAU"
  cKeyWhere  = "LMCODCAU=this.w_LMCODCAU"
  cKeyWhereODBC = '"LMCODCAU="+cp_ToStrODBC(this.w_LMCODCAU)';

  cKeyWhereODBCqualified = '"STU_CAUS.LMCODCAU="+cp_ToStrODBC(this.w_LMCODCAU)';

  cPrg = "gslm_acc"
  cComment = "Causali contabili studio"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LMCODCAU = space(10)
  o_LMCODCAU = space(10)
  w_LMDESCAU = space(29)
  w_LMINFDAR = space(6)
  w_LMSUPDAR = space(6)
  w_LMINFAVE = space(6)
  w_LMSUPAVE = space(6)
  w_LMCAUNOT = space(45)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'STU_CAUS','gslm_acc')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslm_accPag1","gslm_acc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Causale contabile studio")
      .Pages(1).HelpContextID = 5744033
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLMCODCAU_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='STU_CAUS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.STU_CAUS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.STU_CAUS_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_LMCODCAU = NVL(LMCODCAU,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from STU_CAUS where LMCODCAU=KeySet.LMCODCAU
    *
    i_nConn = i_TableProp[this.STU_CAUS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STU_CAUS_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('STU_CAUS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "STU_CAUS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' STU_CAUS '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LMCODCAU',this.w_LMCODCAU  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_LMCODCAU = NVL(LMCODCAU,space(10))
        .w_LMDESCAU = NVL(LMDESCAU,space(29))
        .w_LMINFDAR = NVL(LMINFDAR,space(6))
        .w_LMSUPDAR = NVL(LMSUPDAR,space(6))
        .w_LMINFAVE = NVL(LMINFAVE,space(6))
        .w_LMSUPAVE = NVL(LMSUPAVE,space(6))
        .w_LMCAUNOT = NVL(LMCAUNOT,space(45))
        cp_LoadRecExtFlds(this,'STU_CAUS')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LMCODCAU = space(10)
      .w_LMDESCAU = space(29)
      .w_LMINFDAR = space(6)
      .w_LMSUPDAR = space(6)
      .w_LMINFAVE = space(6)
      .w_LMSUPAVE = space(6)
      .w_LMCAUNOT = space(45)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'STU_CAUS')
    this.DoRTCalc(1,7,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oLMCODCAU_1_1.enabled = i_bVal
      .Page1.oPag.oLMDESCAU_1_3.enabled = i_bVal
      .Page1.oPag.oLMINFDAR_1_5.enabled = i_bVal
      .Page1.oPag.oLMSUPDAR_1_7.enabled = i_bVal
      .Page1.oPag.oLMINFAVE_1_9.enabled = i_bVal
      .Page1.oPag.oLMSUPAVE_1_11.enabled = i_bVal
      .Page1.oPag.oLMCAUNOT_1_13.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oLMCODCAU_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oLMCODCAU_1_1.enabled = .t.
        .Page1.oPag.oLMDESCAU_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'STU_CAUS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.STU_CAUS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCODCAU,"LMCODCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMDESCAU,"LMDESCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMINFDAR,"LMINFDAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMSUPDAR,"LMSUPDAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMINFAVE,"LMINFAVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMSUPAVE,"LMSUPAVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCAUNOT,"LMCAUNOT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.STU_CAUS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STU_CAUS_IDX,2])
    i_lTable = "STU_CAUS"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.STU_CAUS_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.STU_CAUS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_CAUS_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.STU_CAUS_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into STU_CAUS
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'STU_CAUS')
        i_extval=cp_InsertValODBCExtFlds(this,'STU_CAUS')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(LMCODCAU,LMDESCAU,LMINFDAR,LMSUPDAR,LMINFAVE"+;
                  ",LMSUPAVE,LMCAUNOT "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_LMCODCAU)+;
                  ","+cp_ToStrODBC(this.w_LMDESCAU)+;
                  ","+cp_ToStrODBC(this.w_LMINFDAR)+;
                  ","+cp_ToStrODBC(this.w_LMSUPDAR)+;
                  ","+cp_ToStrODBC(this.w_LMINFAVE)+;
                  ","+cp_ToStrODBC(this.w_LMSUPAVE)+;
                  ","+cp_ToStrODBC(this.w_LMCAUNOT)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'STU_CAUS')
        i_extval=cp_InsertValVFPExtFlds(this,'STU_CAUS')
        cp_CheckDeletedKey(i_cTable,0,'LMCODCAU',this.w_LMCODCAU)
        INSERT INTO (i_cTable);
              (LMCODCAU,LMDESCAU,LMINFDAR,LMSUPDAR,LMINFAVE,LMSUPAVE,LMCAUNOT  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_LMCODCAU;
                  ,this.w_LMDESCAU;
                  ,this.w_LMINFDAR;
                  ,this.w_LMSUPDAR;
                  ,this.w_LMINFAVE;
                  ,this.w_LMSUPAVE;
                  ,this.w_LMCAUNOT;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.STU_CAUS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_CAUS_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.STU_CAUS_IDX,i_nConn)
      *
      * update STU_CAUS
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'STU_CAUS')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " LMDESCAU="+cp_ToStrODBC(this.w_LMDESCAU)+;
             ",LMINFDAR="+cp_ToStrODBC(this.w_LMINFDAR)+;
             ",LMSUPDAR="+cp_ToStrODBC(this.w_LMSUPDAR)+;
             ",LMINFAVE="+cp_ToStrODBC(this.w_LMINFAVE)+;
             ",LMSUPAVE="+cp_ToStrODBC(this.w_LMSUPAVE)+;
             ",LMCAUNOT="+cp_ToStrODBC(this.w_LMCAUNOT)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'STU_CAUS')
        i_cWhere = cp_PKFox(i_cTable  ,'LMCODCAU',this.w_LMCODCAU  )
        UPDATE (i_cTable) SET;
              LMDESCAU=this.w_LMDESCAU;
             ,LMINFDAR=this.w_LMINFDAR;
             ,LMSUPDAR=this.w_LMSUPDAR;
             ,LMINFAVE=this.w_LMINFAVE;
             ,LMSUPAVE=this.w_LMSUPAVE;
             ,LMCAUNOT=this.w_LMCAUNOT;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.STU_CAUS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_CAUS_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.STU_CAUS_IDX,i_nConn)
      *
      * delete STU_CAUS
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'LMCODCAU',this.w_LMCODCAU  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.STU_CAUS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STU_CAUS_IDX,2])
    if i_bUpd
      with this
        if .o_LMCODCAU<>.w_LMCODCAU
          .Calculate_FIRXFFUGWY()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,7,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_FIRXFFUGWY()
    with this
          * --- Simulo zerofill LMCODCAU
          .w_LMCODCAU = IIF(g_TRAEXP<>'G',Left(IIF( ( Left( Alltrim( .w_LMCODCAU ) ,1 ) <>"0" And  Len( Alltrim( .w_LMCODCAU ) )=Len( "9999999999" ) ) Or Left( Alltrim( .w_LMCODCAU ) ,1 ) ="0" , .w_LMCODCAU , IIF(g_LEMC<>'S' OR g_TRAEXP='N','',Right( Repl('0',Len( "9999999999" ))+Alltrim( .w_LMCODCAU ) ,Len( "9999999999" ) )  ))+'      ',10),Alltrim( .w_LMCODCAU ))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oLMINFDAR_1_5.visible=!this.oPgFrm.Page1.oPag.oLMINFDAR_1_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oLMSUPDAR_1_7.visible=!this.oPgFrm.Page1.oPag.oLMSUPDAR_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oLMINFAVE_1_9.visible=!this.oPgFrm.Page1.oPag.oLMINFAVE_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oLMSUPAVE_1_11.visible=!this.oPgFrm.Page1.oPag.oLMSUPAVE_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Load")
          .Calculate_FIRXFFUGWY()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLMCODCAU_1_1.value==this.w_LMCODCAU)
      this.oPgFrm.Page1.oPag.oLMCODCAU_1_1.value=this.w_LMCODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oLMDESCAU_1_3.value==this.w_LMDESCAU)
      this.oPgFrm.Page1.oPag.oLMDESCAU_1_3.value=this.w_LMDESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oLMINFDAR_1_5.value==this.w_LMINFDAR)
      this.oPgFrm.Page1.oPag.oLMINFDAR_1_5.value=this.w_LMINFDAR
    endif
    if not(this.oPgFrm.Page1.oPag.oLMSUPDAR_1_7.value==this.w_LMSUPDAR)
      this.oPgFrm.Page1.oPag.oLMSUPDAR_1_7.value=this.w_LMSUPDAR
    endif
    if not(this.oPgFrm.Page1.oPag.oLMINFAVE_1_9.value==this.w_LMINFAVE)
      this.oPgFrm.Page1.oPag.oLMINFAVE_1_9.value=this.w_LMINFAVE
    endif
    if not(this.oPgFrm.Page1.oPag.oLMSUPAVE_1_11.value==this.w_LMSUPAVE)
      this.oPgFrm.Page1.oPag.oLMSUPAVE_1_11.value=this.w_LMSUPAVE
    endif
    if not(this.oPgFrm.Page1.oPag.oLMCAUNOT_1_13.value==this.w_LMCAUNOT)
      this.oPgFrm.Page1.oPag.oLMCAUNOT_1_13.value=this.w_LMCAUNOT
    endif
    cp_SetControlsValueExtFlds(this,'STU_CAUS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_LMCODCAU))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLMCODCAU_1_1.SetFocus()
            i_bnoObbl = !empty(.w_LMCODCAU)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LMCODCAU = this.w_LMCODCAU
    return

enddefine

* --- Define pages as container
define class tgslm_accPag1 as StdContainer
  Width  = 472
  height = 163
  stdWidth  = 472
  stdheight = 163
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLMCODCAU_1_1 as StdField with uid="NAIXTKQGHS",rtseq=1,rtrep=.f.,;
    cFormVar = "w_LMCODCAU", cQueryName = "LMCODCAU",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice causale",;
    HelpContextID = 251017717,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=72, Left=63, Top=8, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oLMDESCAU_1_3 as StdField with uid="RYWMQEJVTH",rtseq=2,rtrep=.f.,;
    cFormVar = "w_LMDESCAU", cQueryName = "LMDESCAU",;
    bObbl = .f. , nPag = 1, value=space(29), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 235940341,;
   bGlobalFont=.t.,;
    Height=21, Width=240, Left=221, Top=8, InputMask=replicate('X',29)

  add object oLMINFDAR_1_5 as StdField with uid="ZDUTJSEPKL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_LMINFDAR", cQueryName = "LMINFDAR",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Sottoconto limite inferiore dare",;
    HelpContextID = 232184312,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=63, Top=68, cSayPict="g_PICTLEN", cGetPict="g_PICTLEN", InputMask=replicate('X',6)

  func oLMINFDAR_1_5.mHide()
    with this.Parent.oContained
      return (G_TRAEXP='G')
    endwith
  endfunc

  add object oLMSUPDAR_1_7 as StdField with uid="EDLLGGDGIK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_LMSUPDAR", cQueryName = "LMSUPDAR",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Sottoconto limite superiore dare",;
    HelpContextID = 221198840,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=63, Top=95, cSayPict="g_PICTLEN", cGetPict="g_PICTLEN", InputMask=replicate('X',6)

  func oLMSUPDAR_1_7.mHide()
    with this.Parent.oContained
      return (G_TRAEXP='G')
    endwith
  endfunc

  add object oLMINFAVE_1_9 as StdField with uid="IYZWDSXOQM",rtseq=5,rtrep=.f.,;
    cFormVar = "w_LMINFAVE", cQueryName = "LMINFAVE",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Sottoconto limite inferiore avere",;
    HelpContextID = 254354939,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=221, Top=68, cSayPict="g_PICTLEN", cGetPict="g_PICTLEN", InputMask=replicate('X',6)

  func oLMINFAVE_1_9.mHide()
    with this.Parent.oContained
      return (G_TRAEXP='G')
    endwith
  endfunc

  add object oLMSUPAVE_1_11 as StdField with uid="QEAVNCQUEN",rtseq=6,rtrep=.f.,;
    cFormVar = "w_LMSUPAVE", cQueryName = "LMSUPAVE",;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Sottoconto limite inferiore avere",;
    HelpContextID = 265340411,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=221, Top=95, cSayPict="g_PICTLEN", cGetPict="g_PICTLEN", InputMask=replicate('X',6)

  func oLMSUPAVE_1_11.mHide()
    with this.Parent.oContained
      return (G_TRAEXP='G')
    endwith
  endfunc

  add object oLMCAUNOT_1_13 as StdField with uid="AUOJTBUEBY",rtseq=7,rtrep=.f.,;
    cFormVar = "w_LMCAUNOT", cQueryName = "LMCAUNOT",;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    ToolTipText = "Note aggiuntive",;
    HelpContextID = 49560054,;
   bGlobalFont=.t.,;
    Height=21, Width=380, Left=63, Top=134, InputMask=replicate('X',45)

  add object oStr_1_2 as StdString with uid="WMIIYBYFGK",Visible=.t., Left=10, Top=8,;
    Alignment=1, Width=50, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="MSFQZSSSRQ",Visible=.t., Left=150, Top=8,;
    Alignment=1, Width=70, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="LWWUZOBDVR",Visible=.t., Left=0, Top=68,;
    Alignment=1, Width=60, Height=18,;
    Caption="Inferiore:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (G_TRAEXP='G')
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="LYTPFSBWZZ",Visible=.t., Left=0, Top=95,;
    Alignment=1, Width=60, Height=18,;
    Caption="Superiore:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (G_TRAEXP='G')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="SKHMTYHAUI",Visible=.t., Left=157, Top=68,;
    Alignment=1, Width=62, Height=18,;
    Caption="Inferiore:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (G_TRAEXP='G')
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="YVJKROBYAN",Visible=.t., Left=151, Top=95,;
    Alignment=1, Width=68, Height=18,;
    Caption="Superiore:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (G_TRAEXP='G')
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="ALDEIAYWUE",Visible=.t., Left=28, Top=134,;
    Alignment=1, Width=32, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="ESUGKWDEIP",Visible=.t., Left=10, Top=43,;
    Alignment=0, Width=130, Height=15,;
    Caption="Sottoconto limite dare"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (G_TRAEXP='G')
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="JRMNVGOMOL",Visible=.t., Left=174, Top=43,;
    Alignment=0, Width=136, Height=15,;
    Caption="Sottoconto limite avere"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (G_TRAEXP='G')
    endwith
  endfunc

  add object oBox_1_15 as StdBox with uid="BHUAAXELDE",left=10, top=61, width=146,height=2

  add object oBox_1_17 as StdBox with uid="BCQOFARIPW",left=174, top=61, width=146,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslm_acc','STU_CAUS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LMCODCAU=STU_CAUS.LMCODCAU";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
