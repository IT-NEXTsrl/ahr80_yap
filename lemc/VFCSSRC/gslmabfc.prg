* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslmabfc                                                        *
*              Export fatture corrispettivo                                    *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-24                                                      *
* Last revis.: 2015-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslmabfc",oParentObject)
return(i_retval)

define class tgslmabfc as StdBatch
  * --- Local variables
  w_REGNOVALIDA = .f.
  w_STRINGA = space(120)
  w_ACTUALPOS = 0
  w_PNSERIAL = space(10)
  w_TOTDOC = 0
  w_TOTDOCINVAL = 0
  w_ANCODSTU = space(6)
  w_APRNORM = space(2)
  w_APRCAU = space(3)
  w_IVASTU = space(2)
  w_IVASTUNOR = space(2)
  w_IVACEE = space(2)
  w_ESCI = .f.
  w_OKTRAS = .f.
  w_NOTRAS = 0
  w_PNCODCAU = space(5)
  w_IVCODIVA = space(5)
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_NUMRIGHE = 0
  w_PROGRIGA = 0
  w_CODATT = 0
  w_TIPOREG = space(1)
  w_NUMREG = 0
  w_CODSOGG = space(8)
  w_CODPIVA = space(16)
  w_CODFISC = space(16)
  w_PIUATTIV = space(1)
  w_CODSEZ = 0
  w_MESEAT = 0
  w_FATANNPREC = space(1)
  w_ANNOCOMP = space(4)
  w_MESECOMP = space(4)
  w_Valuta = space(3)
  w_Valore = 0
  w_Lunghezza = 0
  w_StrInt = space(1)
  w_StrDec = space(1)
  w_Decimali = 0
  w_CODATTPR = space(5)
  w_ANNOCOMP = space(4)
  w_CAUCONT = space(10)
  w_ESIGDIF = space(1)
  w_PAGESIGDIF = space(1)
  w_TIPCLI = space(1)
  w_CODCLI = space(15)
  w_ANNOAT = 0
  w_ALMENOUNINCASSO = .f.
  w_FLGDIF = space(1)
  w_FLESIG = space(1)
  w_DATREGFAT = ctod("  /  /  ")
  w_DATREGINC = ctod("  /  /  ")
  w_DATDOCFAT = ctod("  /  /  ")
  w_SERFAT = space(10)
  w_DATREGSTO = ctod("  /  /  ")
  * --- WorkFile variables
  CONTI_idx=0
  STU_NORM_idx=0
  STU_PNTT_idx=0
  STU_TRAS_idx=0
  ATTIDETT_idx=0
  ATTIMAST_idx=0
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT VERSO LO STUDIO DELLE FATTURE COMPRESE NEI CORRISPETTIVI
    this.w_StrInt = space(1)
    this.w_StrDec = space(1)
    this.w_Valuta = g_PERVAL
    this.w_Decimali = GETVALUT( this.w_Valuta, "VADECTOT")
    * --- Leggo se l'azienda esercita piu' Attivita'
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZATTIVI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZATTIVI;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PIUATTIV = NVL(cp_ToDate(_read_.AZATTIVI),cp_NullValue(_read_.AZATTIVI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Ciclo sulla primanota
    SELECT FATTCORR
    GO TOP
    * --- Ciclo sul cursore della fatture comprese nei corrispettivi
    do while NOT EOF()
      * --- Messaggio a schermo
      this.w_REGNOVALIDA = .F.
      Ah_MSG("Export fatture corrispettivo : reg. num. %1 - data %2",.t.,.f.,.f.,alltrim(STR(FATTCORR.PNNUMRER,6,0)),dtoc(FATTCORR.PNDATREG))
      * --- Scrittura su log
      this.w_STRINGA = space(10)+Ah_MsgFormat("Export fatture corrispettivo : reg. num. %1 - data %2",alltrim(STR(FATTCORR.PNNUMRER,6,0))+"/"+alltrim(STR(FATTCORR.PNCODUTE,4,0)),dtoc(FATTCORR.PNDATREG))
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      * --- Salvo la posizione attuale del record e il seriale della registrazione
      this.w_ACTUALPOS = RECNO()
      this.w_PNSERIAL = FATTCORR.PNSERIAL
      this.w_TOTDOC = 0
      this.w_NUMRIGHE = 0
      * --- Calcolo l'importo totale del documento
      do while FATTCORR.PNSERIAL=this.w_PNSERIAL
        this.w_NUMRIGHE = this.w_NUMRIGHE+1
        this.w_TOTDOC = FATTCORR.IVIMPONI+FATTCORR.IVIMPIVA+this.w_TOTDOC
        skip 1
      enddo
      * --- Ripristino posizione
      go this.w_ACTUALPOS
      * --- Record di testata
      this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
      this.oParentObject.w_FATTCORR = this.oParentObject.w_FATTCORR+1
      this.w_TOTDOCINVAL = FATTCORR.PNTOTDOC
      FWRITE (this.oParentObject.hFile , "D30" , 3 )
      * --- Estraggo il codice attivit� dal registro iva usato sulla prima riga del castelletto IVA della Primanota
      this.w_TIPOREG = NVL(FATTCORR.IVTIPREG," ")
      this.w_NUMREG = NVL(FATTCORR.IVNUMREG,0)
      * --- Estraggo il codice attivit� contenente il tipo e numero registro IVA presenti in Primanota
      * --- Read from ATTIDETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATCODATT,ATCODSEZ"+;
          " from "+i_cTable+" ATTIDETT where ";
              +"ATNUMREG = "+cp_ToStrODBC(this.w_NUMREG);
              +" and ATTIPREG = "+cp_ToStrODBC(this.w_TIPOREG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATCODATT,ATCODSEZ;
          from (i_cTable) where;
              ATNUMREG = this.w_NUMREG;
              and ATTIPREG = this.w_TIPOREG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODATTPR = NVL(cp_ToDate(_read_.ATCODATT),cp_NullValue(_read_.ATCODATT))
        this.w_CODSEZ = NVL(cp_ToDate(_read_.ATCODSEZ),cp_NullValue(_read_.ATCODSEZ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggo il codice attivit� studio associato all'attivit� 
      * --- Read from ATTIMAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ATTIMAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIMAST_idx,2],.t.,this.ATTIMAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATATTIVA"+;
          " from "+i_cTable+" ATTIMAST where ";
              +"ATCODATT = "+cp_ToStrODBC(this.w_CODATTPR);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATATTIVA;
          from (i_cTable) where;
              ATCODATT = this.w_CODATTPR;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODATT = NVL(cp_ToDate(_read_.ATATTIVA),cp_NullValue(_read_.ATATTIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Codice attivit� IVA
      if this.w_CODATT=0
        this.w_STRINGA = Ah_MsgFormat("Errore! Inserire un codice attivit� valido")
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      FWRITE (this.oParentObject.hFile ,right("00"+alltrim(str(this.w_CODATT)),2), 2)
      * --- Sezione
      FWRITE(this.oParentObject.hFile,RIGHT("00"+ALLTRIM(STR(this.w_CODSEZ,2,0)),2),2)
      * --- Recupero e inserisco l' anno di competenza
      this.w_ANNOCOMP = iif(empty(dtos(nvl(FATTCORR.PNDATPLA,ctod("  -  -    ")))),space(8),dtos(nvl(FATTCORR.PNDATPLA,ctod("  -  -    "))))
      FWRITE(this.oParentObject.hFile,SUBSTR(this.w_ANNOCOMP,1,4),4)
      * --- Recupero e inserisco il mese di competenza
      this.w_MESECOMP = iif(empty(dtos(nvl(FATTCORR.PNDATPLA,ctod("  -  -    ")))),space(8),dtos(nvl(FATTCORR.PNDATPLA,ctod("  -  -    "))))
      FWRITE(this.oParentObject.hFile,SUBSTR(this.w_MESECOMP,5,2),2)
      * --- Recupero e inserisco il giorno di inizio mese
      FWRITE(this.oParentObject.hFile,"01",2)
      * --- Causali contabili
      this.w_CAUCONT = FATTCORR.PNCODCAU
      this.w_IVCODIVA = NVL(FATTCORR.IVCODIVA," ")
      * --- Read from STU_TRAS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.STU_TRAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "LMAPRCAU"+;
          " from "+i_cTable+" STU_TRAS where ";
              +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
              +" and LMHOCCAU = "+cp_ToStrODBC(this.w_CAUCONT);
              +" and LMHOCIVA = "+cp_ToStrODBC(this.w_IVCODIVA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          LMAPRCAU;
          from (i_cTable) where;
              LMCODICE = this.oParentObject.w_ASSOCI;
              and LMHOCCAU = this.w_CAUCONT;
              and LMHOCIVA = this.w_IVCODIVA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_APRCAU = NVL(cp_ToDate(_read_.LMAPRCAU),cp_NullValue(_read_.LMAPRCAU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Empty(nvl(this.w_APRCAU,""))
        this.w_STRINGA = Ah_MsgFormat("%1Errore! La causale %2 non ha una causale AGO associata","               ",NVL(this.w_CAUCONT, " "))
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      FWRITE (this.oParentObject.hFile , right(SPACE(10)+alltrim(this.w_APRCAU),10), 10)
      * --- Tipo fattura
      FWRITE(this.oParentObject.hFile,"001",3)
      * --- Numero Documento - Acquisto = Protocollo
      FWRITE(this.oParentObject.hFile,RIGHT("0000000000"+ALLTRIM(STR(FATTCORR.PNNUMPRO)),10),10)
      * --- Num Bis
      FWRITE(this.oParentObject.hFile,RIGHT(SPACE(5)+ALLTRIM(FATTCORR.PNALFPRO),5),5)
      * --- Numero Documento - Vendita = Num Doc
      if FATTCORR.PNNUMDOC=0
        this.w_STRINGA = Ah_MsgFormat("Errore! Manca il numero documento per la fattura di vendita : reg. num. %1  - data %2",STR(FATTCORR.PNNUMRER,6,0),dtoc(FATTCORR.PNDATREG))
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      FWRITE(this.oParentObject.hFile,RIGHT(repl("0",15)+ALLTRIM(STR(FATTCORR.PNNUMDOC)),15),15)
      * --- Date
      * --- Data registrazione
      FWRITE(this.oParentObject.hFile,dtos(FATTCORR.PNDATREG),8)
      * --- Data fattura
      if EMPTY(FATTCORR.PNDATDOC)
        this.w_STRINGA = Ah_MsgFormat("Errore! Manca la data documento per la fattura di vendita : reg. num. %1  - data %2",STR(FATTCORR.PNNUMRER,6,0),dtoc(FATTCORR.PNDATREG))
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      FWRITE(this.oParentObject.hFile,NVL(dtos(FATTCORR.PNDATDOC),"        "),8)
      * --- Data scadenza, Riscontro, Rateo - Non Gestite
      FWRITE(this.oParentObject.hFile,repl(" ",8),8)
      FWRITE(this.oParentObject.hFile,repl(" ",8),8)
      FWRITE(this.oParentObject.hFile,repl(" ",8),8)
      * --- Codice Pagamento e tipo pagamento - Non gestite
      FWRITE(this.oParentObject.hFile,"     ",5)
      FWRITE(this.oParentObject.hFile," ",1)
      * --- Codice cliente/fornitore
      this.w_ANCODSTU = SPACE(15)
      this.w_PNTIPCLF = NVL(FATTCORR.PNTIPCLF," ")
      this.w_PNCODCLF = NVL(FATTCORR.PNCODCLF," ")
      * --- Leggo i dati relativi all'esigibilit� differita imputati nella causale
      this.w_ESIGDIF = NVL(FATTCORR.CCFLIVDF,"N")
      this.w_PAGESIGDIF = NVL(FATTCORR.CCFLPDIF,"N")
      this.w_TIPCLI = "C"
      this.w_CODCLI = SPACE(15)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU,ANCONRIF,ANCODSOG,ANPARIVA,ANCODFIS"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU,ANCONRIF,ANCODSOG,ANPARIVA,ANCODFIS;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCLF;
              and ANCODICE = this.w_PNCODCLF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        this.w_CODCLI = NVL(cp_ToDate(_read_.ANCONRIF),cp_NullValue(_read_.ANCONRIF))
        this.w_CODSOGG = NVL(cp_ToDate(_read_.ANCODSOG),cp_NullValue(_read_.ANCODSOG))
        this.w_CODPIVA = NVL(cp_ToDate(_read_.ANPARIVA),cp_NullValue(_read_.ANPARIVA))
        this.w_CODFISC = NVL(cp_ToDate(_read_.ANCODFIS),cp_NullValue(_read_.ANCODFIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT FATTCORR
      * --- Codice Cli/For
      if this.oParentObject.w_LMAGOFLT="S"
        if LEN(FATTCORR.PNCODCLF)=15
          if g_CFNUME="S"
            FWRITE(this.oParentObject.hFile,this.w_PNTIPCLF+SUBSTR(FATTCORR.PNCODCLF,2,15),15)
          else
            FWRITE(this.oParentObject.hFile,right(Space(15)+alltrim(FATTCORR.PNCODCLF),15),15)
          endif
        else
          FWRITE(this.oParentObject.hFile,right(Space(15)+this.w_PNTIPCLF+alltrim(FATTCORR.PNCODCLF),15),15)
        endif
      else
        FWRITE(this.oParentObject.hFile,right(Space(15)+alltrim(FATTCORR.PNCODCLF),15),15)
      endif
      if EMPTY(this.w_ANCODSTU)
        this.w_STRINGA = Ah_MsgFormat("%1Errore! Fornitore %2 non ha un codice fornitore nello studio associato","               ",NVL(this.w_PNCODCLF, " "))
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      * --- Filler
      this.oParentObject.w_FILLER = SPACE(21)
      FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,21)
      * --- Descrizione alternativa aggiuntiva
      FWRITE(this.oParentObject.hFile,NVL(FATTCORR.DESSUP,SPACE(30)),30)
      * --- Flag Partita - Non Gestito
      if FATTCORR.CCFLPART="C"
        FWRITE(this.oParentObject.hFile,"0",1)
      else
        FWRITE(this.oParentObject.hFile,"1",1)
      endif
      * --- Filler
      this.oParentObject.w_FILLER = " "
      FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,1)
      * --- Mese stampa su reg.IVA per autotrasportatori
      if FATTCORR.CCFLAUTR = "S"
        this.w_ANNOAT = IIF(MONTH(FATTCORR.PNDATDOC)>=10,YEAR(FATTCORR.PNDATDOC)+1,YEAR(FATTCORR.PNDATDOC))
        this.w_MESEAT = IIF(INT((MONTH(FATTCORR.PNDATDOC)+2)/3)+1>4,MOD(INT((MONTH(FATTCORR.PNDATDOC)+2)/3)+1,4),INT((MONTH(FATTCORR.PNDATDOC)+2)/3)+1)*3
        FWRITE(this.oParentObject.hFile ,"01"+ RIGHT(" "+LTRIM(STR(this.w_MESEAT,2,0)),2)+ALLTRIM(str(this.w_ANNOAT)) , 8)
      else
        FWRITE(this.oParentObject.hFile , space(8) , 8)
      endif
      this.oParentObject.w_FILLER = SPACE(4)
      FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,4)
      * --- Importo altre ritenute- Non Gestito
      this.w_Valore = NVL(FATTCORR.ALTRERIT,0)
      this.w_Lunghezza = 16
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      FWRITE(this.oParentObject.hFile,this.w_VALORE,16)
      this.oParentObject.w_FILLER = SPACE(4)
      FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,4)
      * --- Importo ritenuta di acconto - Non Gestito
      this.w_Valore = NVL(FATTCORR.IRPEFRIT,0)
      this.w_Lunghezza = 16
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      FWRITE(this.oParentObject.hFile,this.w_VALORE,16)
      * --- Scrivo il codice fattura normale/Sospesa/Incasso
      this.w_DATREGFAT = cp_todate(FATTCORR.PNDATREG)
      this.w_DATDOCFAT = cp_todate(FATTCORR.PNDATDOC)
      this.w_FLESIG = FATTCORR.ANFLESIG
      this.w_FLGDIF = FATTCORR.PNFLGDIF
      this.w_SERFAT = FATTCORR.PNSERIAL
      * --- Se il flag pnflgdif � sulla fattura, sono nel caso di maturazione temporale
      if FATTCORR.CCFLIVDF="S"
        if this.w_FLGDIF="S"
          * --- Select from gslmasto
          do vq_exec with 'gslmasto',this,'_Curs_gslmasto','',.f.,.t.
          if used('_Curs_gslmasto')
            select _Curs_gslmasto
            locate for 1=1
            if not(eof())
            do while not(eof())
            this.w_DATREGSTO = cp_todate(_Curs_GSLMASTO.DATREGSTO)
            do case
              case (YEAR(this.w_DATREGFAT)=YEAR(this.w_DATREGSTO))
                FWRITE(this.oParentObject.hFile,"008",3)
              case (YEAR(this.w_DATREGFAT)<=YEAR(this.w_DATREGSTO)) AND (this.w_DATDOCFAT-this.w_DATREGSTO)>365
                FWRITE(this.oParentObject.hFile,"007",3)
            endcase
              select _Curs_gslmasto
              continue
            enddo
            else
              * --- Se non ho incassi significa che sono nel caso di fattura aperta (iva sospesa)
              if this.w_FLESIG="S"
                FWRITE(this.oParentObject.hFile,"002",3)
              else
                FWRITE(this.oParentObject.hFile,"001",3)
              endif
              select _Curs_gslmasto
            endif
            use
          endif
        else
          * --- cerco gli incassi
          * --- Select from GSLMAINC
          do vq_exec with 'GSLMAINC',this,'_Curs_GSLMAINC','',.f.,.t.
          if used('_Curs_GSLMAINC')
            select _Curs_GSLMAINC
            locate for 1=1
            if not(eof())
            do while not(eof())
            this.w_DATREGINC = cp_todate(_Curs_GSLMAINC.DATREGINC)
            do case
              case (YEAR(this.w_DATREGFAT)<=YEAR(this.w_DATREGINC)) AND (this.w_DATDOCFAT-this.w_DATREGINC)<=365
                if this.w_FLESIG="S"
                  FWRITE(this.oParentObject.hFile,"004",3)
                else
                  FWRITE(this.oParentObject.hFile,"006",3)
                endif
              case (YEAR(this.w_DATREGFAT)=YEAR(this.w_DATREGINC)) AND (this.w_DATDOCFAT-this.w_DATREGINC)<=365
                if this.w_FLESIG="S"
                  FWRITE(this.oParentObject.hFile,"003",3)
                else
                  FWRITE(this.oParentObject.hFile,"005",3)
                endif
            endcase
              select _Curs_GSLMAINC
              continue
            enddo
            else
              * --- Se non ho incassi significa che sono nel caso di fattura aperta (iva sospesa)
              if this.w_FLESIG="S"
                FWRITE(this.oParentObject.hFile,"002",3)
              else
                FWRITE(this.oParentObject.hFile,"001",3)
              endif
              select _Curs_GSLMAINC
            endif
            use
          endif
        endif
      else
        FWRITE(this.oParentObject.hFile,"   ",3)
      endif
      * --- Sottoconto
      FWRITE(this.oParentObject.hFile,left(alltrim(this.w_ANCODSTU)+repl("0",9),9),9)
      FWRITE(this.oParentObject.hFile,SPACE(9),9)
      FWRITE(this.oParentObject.hFile," ",1)
      * --- split payment
      if FATTCORR.CCSCIPAG="S"
        FWRITE(this.oParentObject.hFile,"001",3)
      else
        FWRITE(this.oParentObject.hFile,"   ",3)
      endif
      * --- Totale documento
      this.w_Valore = NVL(FATTCORR.PNIMPDAR,0)
      this.w_Lunghezza = 16
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      FWRITE(this.oParentObject.hFile,this.w_VALORE,16)
      FWRITE(this.oParentObject.hFile,SPACE(3),3)
      FWRITE(this.oParentObject.hFile , space(15) , 15)
      * --- Progressivo registrazione
      FWRITE(this.oParentObject.hFile,RIGHT("000000000000000"+ALLTRIM(STR(FATTCORR.PNNUMRER)),15),15)
      this.oParentObject.w_FILLER = SPACE(6)
      FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,6)
      * --- Ciclo su tutte le righe IVA
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT FATTCORR
    enddo
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Butto giu' tutte le righe IVA della fattura in questione
    this.w_ESCI = .F.
    this.w_OKTRAS = .T.
    this.w_PROGRIGA = 0
    * --- Posiziono il puntatore al record
    SELECT FATTCORR
    this.w_ACTUALPOS = RECNO()
    do while FATTCORR.PNSERIAL=this.w_PNSERIAL AND NOT this.w_ESCI
      this.w_PROGRIGA = this.w_PROGRIGA+1
      if NOT EMPTY(NVL(FATTCORR.IVCONTRO," "))
        * --- Incremento numero di record scritti
        this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
        this.oParentObject.w_FATTCORR = this.oParentObject.w_FATTCORR+1
        * --- Se sono in questo caso devo scrivere testata e dettaglio
        FWRITE (this.oParentObject.hFile , "D31" , 3 )
        * --- Recupero e scrittura del codice di Aliquota IVA
        this.w_APRCAU = SPACE(3)
        this.w_APRNORM = SPACE(2)
        this.w_IVASTU = SPACE(2)
        this.w_IVACEE = SPACE(2)
        this.w_PNCODCAU = NVL(FATTCORR.PNCODCAU," ")
        this.w_IVCODIVA = NVL(FATTCORR.IVCODIVA," ")
        * --- Read from STU_TRAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.STU_TRAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LMAPRNOR,LMAPRCAU,LMIVASTU,LMIVACEE"+;
            " from "+i_cTable+" STU_TRAS where ";
                +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
                +" and LMHOCCAU = "+cp_ToStrODBC(this.w_PNCODCAU);
                +" and LMHOCIVA = "+cp_ToStrODBC(this.w_IVCODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LMAPRNOR,LMAPRCAU,LMIVASTU,LMIVACEE;
            from (i_cTable) where;
                LMCODICE = this.oParentObject.w_ASSOCI;
                and LMHOCCAU = this.w_PNCODCAU;
                and LMHOCIVA = this.w_IVCODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_APRNORM = NVL(cp_ToDate(_read_.LMAPRNOR),cp_NullValue(_read_.LMAPRNOR))
          this.w_APRCAU = NVL(cp_ToDate(_read_.LMAPRCAU),cp_NullValue(_read_.LMAPRCAU))
          this.w_IVASTU = NVL(cp_ToDate(_read_.LMIVASTU),cp_NullValue(_read_.LMIVASTU))
          this.w_IVACEE = NVL(cp_ToDate(_read_.LMIVACEE),cp_NullValue(_read_.LMIVACEE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT FATTCORR
        * --- Ho trovato il mapping tra il codice Causale e il codice IVA
        * --- Verifica se ho il codice IVA studio definito nella norma
        if NOT EMPTY(this.w_APRNORM)
          this.w_IVASTUNOR = SPACE(2)
          * --- Read from STU_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.STU_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_NORM_idx,2],.t.,this.STU_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LMCODIVA"+;
              " from "+i_cTable+" STU_NORM where ";
                  +"LMCODNOR = "+cp_ToStrODBC(this.w_APRNORM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LMCODIVA;
              from (i_cTable) where;
                  LMCODNOR = this.w_APRNORM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_IVASTUNOR = NVL(cp_ToDate(_read_.LMCODIVA),cp_NullValue(_read_.LMCODIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          SELECT FATTCORR
          if this.w_IVASTUNOR="XX"
            this.w_IVASTUNOR = this.w_IVASTU
          endif
        else
          this.w_IVASTUNOR = this.w_IVASTU
        endif
        * --- Controllo Codice IVA
        if (EMPTY(this.w_IVASTUNOR) or this.w_IVASTUNOR="XX")
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Codice IVA non definito","                  ")
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        * --- Scrittura del codice IVA
        FWRITE(this.oParentObject.hFile,right("00"+this.w_IVASTUNOR,2),2)
        this.oParentObject.w_FILLER = SPACE(4)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,4)
        * --- Imponibile
        this.w_Valore = nvl(FATTCORR.IVIMPONI,0)
        this.w_Lunghezza = 16
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        FWRITE(this.oParentObject.hFile,this.w_VALORE,16)
        * --- segno imponibile
        FWRITE(this.oParentObject.hFile,"+",1)
        this.oParentObject.w_FILLER = SPACE(4)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,4)
        * --- Imposta
        this.w_Valore = nvl(FATTCORR.IVIMPIVA,0)
        this.w_Lunghezza = 16
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        FWRITE(this.oParentObject.hFile,this.w_VALORE,16)
        * --- segno imposta
        FWRITE(this.oParentObject.hFile,"+",1)
        * --- Scrittura del codice norma
        FWRITE(this.oParentObject.hFile,right("  "+IIF(EMPTY(this.w_APRNORM),"  ",this.w_APRNORM),2),2)
        * --- Centro di costo
        FWRITE(this.oParentObject.hFile,"   ",3)
        * --- Contropartita (Sottoconto)
        this.w_ANCODSTU = SPACE(9)
        this.w_PNTIPCLF = NVL(FATTCORR.IVTIPCOP," ")
        this.w_PNCODCLF = NVL(FATTCORR.IVCONTRO," ")
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODSTU"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODSTU;
            from (i_cTable) where;
                ANTIPCON = this.w_PNTIPCLF;
                and ANCODICE = this.w_PNCODCLF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT FATTCORR
        if EMPTY(this.w_ANCODSTU)
          this.w_ANCODSTU = "000000000"
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Contropartita %2 non ha un codice sottoconto nello studio associato","               ",NVL(this.w_PNCODCLF, " "))
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        FWRITE(this.oParentObject.hFile,left(alltrim(this.w_ANCODSTU)+repl("0",9),9),9)
        * --- Codice CEE
        FWRITE(this.oParentObject.hFile,IIF(FATTCORR.PNTIPDOC="FE" OR FATTCORR.PNTIPDOC="NE" AND NOT EMPTY(this.w_IVACEE), right("  "+this.w_IVACEE,2), "  "),2)
        * --- Flag prestazioni di servizi
        do case
          case FATTCORR.IVFLGSER = "S"
            FWRITE(this.oParentObject.hFile,"002",3)
          case FATTCORR.IVFLGSER = "B"
            FWRITE(this.oParentObject.hFile,"001",3)
          case FATTCORR.IVFLGSER = "E"
            FWRITE(this.oParentObject.hFile,"   ",3)
          otherwise
            FWRITE(this.oParentObject.hFile,"   ",3)
        endcase
        * --- Data uno + Data due - PARCE
        FWRITE(this.oParentObject.hFile,repl(" ",16),16)
        * --- Nel caso in cu la fattura sia soggetta a split payment ma 
        *     si voglia escludere una riga dell'applicazione.
        if FATTCORR.CCSCIPAG="S"
          if w_SPLPAY<>"S"
            FWRITE(this.oParentObject.hFile," ",1)
          else
            FWRITE(this.oParentObject.hFile,"1",1)
          endif
        else
          FWRITE(this.oParentObject.hFile," ",1)
        endif
        * --- Filler
        this.oParentObject.w_FILLER = SPACE(217)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,217)
        * --- Identificativo documento originario per note di retifica
      else
        if NVL(FATTCORR.IVCODIVA,SPACE(5))<>SPACE(5) AND (NVL(FATTCORR.IVIMPONI,0)<>0 OR NVL(FATTCORR.IVIMPIVA,0)<>0 OR NVL(FATTCORR.IVCFLOMA," ")="S")
          * --- Marco questa registrazione come non valida
          this.w_REGNOVALIDA = .T.
        endif
      endif
      if this.w_REGNOVALIDA
        go this.w_ACTUALPOS
        this.w_NOTRAS = -1
        * --- Insert into STU_PNTT
        i_nConn=i_TableProp[this.STU_PNTT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NOTRAS),'STU_PNTT','LMNUMTRA');
          +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.w_NOTRAS,'LMNUMTR2',0)
          insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.w_NOTRAS;
               ,0;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='GSLM_BFC: Scrittura con errore in STU_PNTT'
          return
        endif
        this.w_STRINGA = Ah_MsgFormat("%1Errore! Contropartita contabile non definita su riga castelletto IVA","               ")
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        SELECT FATTCORR
        do while FATTCORR.PNSERIAL=this.w_PNSERIAL
          * --- Avanzo il puntatore
          skip 1
        enddo
        this.w_ESCI = .T.
        this.w_OKTRAS = .F.
      endif
      * --- Avanzo il puntatore
      if .not. this.w_ESCI
        skip 1
      endif
    enddo
    if this.w_OKTRAS
      * --- Insert into STU_PNTT
      i_nConn=i_TableProp[this.STU_PNTT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'STU_PNTT','LMNUMTRA');
        +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.oParentObject.w_PROFIL,'LMNUMTR2',0)
        insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
           values (;
             this.w_PNSERIAL;
             ,this.oParentObject.w_PROFIL;
             ,0;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='GSLM_BFC: Scrittura in STU_PNTT'
        return
      endif
      SELECT FATTCORR
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Trasformazione degli importi nel formato richiesto dal tracciato R.I.D.
    * --- UTILIZZO
    * --- Prima della chiamata a questa pagina del batch devono essere impostate le variabili:
    * --- w_Valore          = importo da convertire in stringa
    * --- w_Lunghezza  = lunghezza della stringa
    * --- Il risultato della conversione � disponibile nella variabile w_Valore.
    * --- CONVENZIONI
    * --- La virgola deve essere eliminata
    * --- Se la valuta � Euro le ultime due cifre rappresentano i decimali
    * --- Il valore deve essere allineato a destra
    * --- Non � richiesto il riempimento con 0 delle cifre non significative
    * --- Calcolo parte intera
    this.w_StrInt = alltrim( str( int(this.w_Valore), this.w_Lunghezza, 0 ) )
    * --- Calcolo parte decimale
    this.w_StrDec = ""
    if this.w_Decimali > 0
      this.w_StrDec = right( str( this.w_Valore, this.w_Lunghezza, this.w_Decimali ) , this.w_Decimali )
    endif
    * --- Risultato
    this.w_Valore = right( repl("0",this.w_Lunghezza) + this.w_StrInt + this.w_StrDec , this.w_Lunghezza )
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='STU_NORM'
    this.cWorkTables[3]='STU_PNTT'
    this.cWorkTables[4]='STU_TRAS'
    this.cWorkTables[5]='ATTIDETT'
    this.cWorkTables[6]='ATTIMAST'
    this.cWorkTables[7]='AZIENDA'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_gslmasto')
      use in _Curs_gslmasto
    endif
    if used('_Curs_GSLMAINC')
      use in _Curs_GSLMAINC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
