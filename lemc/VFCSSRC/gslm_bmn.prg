* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_bmn                                                        *
*              Attivazione voce di menu                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-11-30                                                      *
* Last revis.: 2012-11-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pVoce
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm_bmn",oParentObject,m.pVoce)
return(i_retval)

define class tgslm_bmn as StdBatch
  * --- Local variables
  pVoce = space(10)
  w_CODAZI = space(5)
  w_TRAEXP = space(1)
  * --- WorkFile variables
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Attivazione voci di men�
    this.w_CODAZI = i_CODAZI
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZTRAEXP"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZTRAEXP;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TRAEXP = NVL(cp_ToDate(_read_.AZTRAEXP),cp_NullValue(_read_.AZTRAEXP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case VARTYPE(this.pVoce)<>"C"
      case ISALT() AND g_COGE<>"S" AND this.w_TRAEXP="C" AND UPPER(this.pVoce)=="IMPORT"
        * --- Import LEMSE
        do GSLMLKIM with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case UPPER(this.pVoce)=="IMPORT"
        * --- Import
        do GSLM_KIM with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case UPPER(this.pVoce)=="SEMPLIFICATO"
        * --- Export semplificato
        do GSLM_KEL with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case ISALT() AND g_COGE<>"S" AND this.w_TRAEXP="C" AND UPPER(this.pVoce)=="AVANZATO"
        * --- Export avanzato lemse
        do GSLMLKEX with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case UPPER(this.pVoce)=="AVANZATO"
        * --- Export avanzato
        do GSLM_KEX with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  proc Init(oParentObject,pVoce)
    this.pVoce=pVoce
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='AZIENDA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pVoce"
endproc
