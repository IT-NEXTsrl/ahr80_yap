* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslmlkex                                                        *
*              Export avanzato                                                 *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_149]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-23                                                      *
* Last revis.: 2012-11-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gslmlkex
* Esco se il modulo � in semplificata
IF NOT LMAVANZATO()
  do cplu_erm with "La funzione � attiva con il Modulo in modalit� Avanzata"
 return
ENDIF
* --- Fine Area Manuale
return(createobject("tgslmlkex",oParentObject))

* --- Class definition
define class tgslmlkex as StdForm
  Top    = 30
  Left   = 75

  * --- Standard Properties
  Width  = 478
  Height = 332
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-30"
  HelpContextID=64797801
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Constant Properties
  _IDX = 0
  STU_PARA_IDX = 0
  cPrg = "gslmlkex"
  cComment = "Export avanzato"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_READAZI = space(5)
  w_LPROFIL = space(2)
  o_LPROFIL = space(2)
  w_AZIENDA = space(5)
  w_AZINOM = space(40)
  w_NOREGTOEXP = .F.
  w_CLI = space(1)
  w_FOR = space(1)
  w_SOT = space(1)
  w_PNTIVA = space(1)
  o_PNTIVA = space(1)
  w_PNTNOIVA = space(1)
  w_TIPCLIFOR = space(10)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_PROFIL = 0
  w_NUMINI = 0
  w_NUMFIN = 0
  w_PERCORSO = space(50)
  w_APPOGGIO = space(100)
  w_CODCAU = space(3)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslmlkexPag1","gslmlkex",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCLI_1_6
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='STU_PARA'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_READAZI=space(5)
      .w_LPROFIL=space(2)
      .w_AZIENDA=space(5)
      .w_AZINOM=space(40)
      .w_NOREGTOEXP=.f.
      .w_CLI=space(1)
      .w_FOR=space(1)
      .w_SOT=space(1)
      .w_PNTIVA=space(1)
      .w_PNTNOIVA=space(1)
      .w_TIPCLIFOR=space(10)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_PROFIL=0
      .w_NUMINI=0
      .w_NUMFIN=0
      .w_PERCORSO=space(50)
      .w_APPOGGIO=space(100)
      .w_CODCAU=space(3)
        .w_READAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_READAZI))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,2,.f.)
        .w_AZIENDA = i_codazi
        .w_AZINOM = g_RAGAZI
          .DoRTCalc(5,9,.f.)
        .w_PNTNOIVA = IIF( .w_PNTIVA<>'S','N',.w_PNTNOIVA)
          .DoRTCalc(11,13,.f.)
        .w_PROFIL = VAL(.w_LPROFIL)+1
        .w_NUMINI = 0
        .w_NUMFIN = 0
      .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_30.Calculate(ah_MsgFormat("Questo programma consente il trasferimento dei movimenti di primanota verso%0la procedura per commercialisti Zucchetti attraverso il protocollo LEMSE"))
          .DoRTCalc(17,18,.f.)
        .w_CODCAU = '031'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_34.enabled = this.oPgFrm.Page1.oPag.oBtn_1_34.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,9,.t.)
        if .o_PNTIVA<>.w_PNTIVA
            .w_PNTNOIVA = IIF( .w_PNTIVA<>'S','N',.w_PNTNOIVA)
        endif
        .DoRTCalc(11,13,.t.)
        if .o_LPROFIL<>.w_LPROFIL
            .w_PROFIL = VAL(.w_LPROFIL)+1
        endif
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate(ah_MsgFormat("Questo programma consente il trasferimento dei movimenti di primanota verso%0la procedura per commercialisti Zucchetti attraverso il protocollo LEMSE"))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(15,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate(ah_MsgFormat("Questo programma consente il trasferimento dei movimenti di primanota verso%0la procedura per commercialisti Zucchetti attraverso il protocollo LEMSE"))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPNTIVA_1_9.enabled = this.oPgFrm.Page1.oPag.oPNTIVA_1_9.mCond()
    this.oPgFrm.Page1.oPag.oPNTNOIVA_1_10.enabled = this.oPgFrm.Page1.oPag.oPNTNOIVA_1_10.mCond()
    this.oPgFrm.Page1.oPag.oTIPCLIFOR_1_11.enabled = this.oPgFrm.Page1.oPag.oTIPCLIFOR_1_11.mCond()
    this.oPgFrm.Page1.oPag.oDATINI_1_12.enabled = this.oPgFrm.Page1.oPag.oDATINI_1_12.mCond()
    this.oPgFrm.Page1.oPag.oDATFIN_1_13.enabled = this.oPgFrm.Page1.oPag.oDATFIN_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_29.visible=!this.oPgFrm.Page1.oPag.oBtn_1_29.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_31.visible=!this.oPgFrm.Page1.oPag.oBtn_1_31.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_34.visible=!this.oPgFrm.Page1.oPag.oBtn_1_34.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=READAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_PARA_IDX,3]
    i_lTable = "STU_PARA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_PARA_IDX,2], .t., this.STU_PARA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_PARA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODAZI,LMPROFIL";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODAZI',this.w_READAZI)
            select LMCODAZI,LMPROFIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.LMCODAZI,space(5))
      this.w_LPROFIL = NVL(_Link_.LMPROFIL,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(5)
      endif
      this.w_LPROFIL = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_PARA_IDX,2])+'\'+cp_ToStr(_Link_.LMCODAZI,1)
      cp_ShowWarn(i_cKey,this.STU_PARA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAZIENDA_1_3.value==this.w_AZIENDA)
      this.oPgFrm.Page1.oPag.oAZIENDA_1_3.value=this.w_AZIENDA
    endif
    if not(this.oPgFrm.Page1.oPag.oAZINOM_1_4.value==this.w_AZINOM)
      this.oPgFrm.Page1.oPag.oAZINOM_1_4.value=this.w_AZINOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCLI_1_6.RadioValue()==this.w_CLI)
      this.oPgFrm.Page1.oPag.oCLI_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPNTIVA_1_9.RadioValue()==this.w_PNTIVA)
      this.oPgFrm.Page1.oPag.oPNTIVA_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPNTNOIVA_1_10.RadioValue()==this.w_PNTNOIVA)
      this.oPgFrm.Page1.oPag.oPNTNOIVA_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCLIFOR_1_11.RadioValue()==this.w_TIPCLIFOR)
      this.oPgFrm.Page1.oPag.oTIPCLIFOR_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_12.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_12.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_13.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_13.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPROFIL_1_14.value==this.w_PROFIL)
      this.oPgFrm.Page1.oPag.oPROFIL_1_14.value=this.w_PROFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oPERCORSO_1_17.value==this.w_PERCORSO)
      this.oPgFrm.Page1.oPag.oPERCORSO_1_17.value=this.w_PERCORSO
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAU_1_35.value==this.w_CODCAU)
      this.oPgFrm.Page1.oPag.oCODCAU_1_35.value=this.w_CODCAU
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATINI))  and (.w_PNTIVA='S' OR .w_PNTNOIVA='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_12.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATFIN))  and (.w_PNTIVA='S' OR .w_PNTNOIVA='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_13.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NUMINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMINI_1_15.SetFocus()
            i_bnoObbl = !empty(.w_NUMINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NUMFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMFIN_1_16.SetFocus()
            i_bnoObbl = !empty(.w_NUMFIN)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LPROFIL = this.w_LPROFIL
    this.o_PNTIVA = this.w_PNTIVA
    return

enddefine

* --- Define pages as container
define class tgslmlkexPag1 as StdContainer
  Width  = 474
  height = 332
  stdWidth  = 474
  stdheight = 332
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAZIENDA_1_3 as StdField with uid="SGSNRRZHJZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_AZIENDA", cQueryName = "AZIENDA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 88945158,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=136, Top=50, InputMask=replicate('X',5)

  add object oAZINOM_1_4 as StdField with uid="SGNIOVJVRB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_AZINOM", cQueryName = "AZINOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 241578502,;
   bGlobalFont=.t.,;
    Height=21, Width=278, Left=186, Top=50, InputMask=replicate('X',40)

  add object oCLI_1_6 as StdCheck with uid="JYLBZVCLJL",rtseq=6,rtrep=.f.,left=8, top=108, caption="Clienti",;
    HelpContextID = 64478170,;
    cFormVar="w_CLI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCLI_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCLI_1_6.GetRadio()
    this.Parent.oContained.w_CLI = this.RadioValue()
    return .t.
  endfunc

  func oCLI_1_6.SetRadio()
    this.Parent.oContained.w_CLI=trim(this.Parent.oContained.w_CLI)
    this.value = ;
      iif(this.Parent.oContained.w_CLI=='S',1,;
      0)
  endfunc

  add object oPNTIVA_1_9 as StdCheck with uid="WUBEJNQMUC",rtseq=9,rtrep=.f.,left=8, top=139, caption="Fatture emesse",;
    ToolTipText = "Se attivo vengono esportate le registrazioni contabili che movimentano l'IVA",;
    HelpContextID = 47306486,;
    cFormVar="w_PNTIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPNTIVA_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPNTIVA_1_9.GetRadio()
    this.Parent.oContained.w_PNTIVA = this.RadioValue()
    return .t.
  endfunc

  func oPNTIVA_1_9.SetRadio()
    this.Parent.oContained.w_PNTIVA=trim(this.Parent.oContained.w_PNTIVA)
    this.value = ;
      iif(this.Parent.oContained.w_PNTIVA=='S',1,;
      0)
  endfunc

  func oPNTIVA_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_NOREGTOEXP)
    endwith
   endif
  endfunc

  add object oPNTNOIVA_1_10 as StdCheck with uid="NHPLLYSJTN",rtseq=10,rtrep=.f.,left=8, top=170, caption="Incassi fatture",;
    ToolTipText = "Se attivo vengono esportate le registrazioni contabili che non movimentano l'IVA",;
    HelpContextID = 93923529,;
    cFormVar="w_PNTNOIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPNTNOIVA_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPNTNOIVA_1_10.GetRadio()
    this.Parent.oContained.w_PNTNOIVA = this.RadioValue()
    return .t.
  endfunc

  func oPNTNOIVA_1_10.SetRadio()
    this.Parent.oContained.w_PNTNOIVA=trim(this.Parent.oContained.w_PNTNOIVA)
    this.value = ;
      iif(this.Parent.oContained.w_PNTNOIVA=='S',1,;
      0)
  endfunc

  func oPNTNOIVA_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_NOREGTOEXP AND .w_PNTIVA='S')
    endwith
   endif
  endfunc


  add object oTIPCLIFOR_1_11 as StdCombo with uid="HMPDZDBXFS",rtseq=11,rtrep=.f.,left=231,top=108,width=132,height=21;
    , ToolTipText = "Tipo clienti/fornitori che devono essere esportati";
    , HelpContextID = 97806427;
    , cFormVar="w_TIPCLIFOR",RowSource=""+"Tutti,"+"Solo movimentati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCLIFOR_1_11.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'M',;
    space(10))))
  endfunc
  func oTIPCLIFOR_1_11.GetRadio()
    this.Parent.oContained.w_TIPCLIFOR = this.RadioValue()
    return .t.
  endfunc

  func oTIPCLIFOR_1_11.SetRadio()
    this.Parent.oContained.w_TIPCLIFOR=trim(this.Parent.oContained.w_TIPCLIFOR)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCLIFOR=='T',1,;
      iif(this.Parent.oContained.w_TIPCLIFOR=='M',2,;
      0))
  endfunc

  func oTIPCLIFOR_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PNTIVA='S' OR .w_PNTNOIVA='S')
    endwith
   endif
  endfunc

  add object oDATINI_1_12 as StdField with uid="SOAVOGTALV",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 173132086,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=231, Top=142

  func oDATINI_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PNTIVA='S' OR .w_PNTNOIVA='S')
    endwith
   endif
  endfunc

  add object oDATFIN_1_13 as StdField with uid="LKWTRQQDJU",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 251578678,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=231, Top=167

  func oDATFIN_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PNTIVA='S' OR .w_PNTNOIVA='S')
    endwith
   endif
  endfunc

  add object oPROFIL_1_14 as StdField with uid="EJPQIPAXBW",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PROFIL", cQueryName = "PROFIL",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 218008310,;
   bGlobalFont=.t.,;
    Height=21, Width=28, Left=231, Top=193, cSayPict='"99"', cGetPict='"99"'

  add object oPERCORSO_1_17 as StdField with uid="GGVZMRWYOR",rtseq=17,rtrep=.f.,;
    cFormVar = "w_PERCORSO", cQueryName = "PERCORSO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 56340037,;
   bGlobalFont=.t.,;
    Height=21, Width=337, Left=8, Top=260, InputMask=replicate('X',50)


  add object oBtn_1_18 as StdButton with uid="RMAHAARJLI",left=419, top=284, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 57480378;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_27 as cp_runprogram with uid="ZSPGUGIMGW",left=199, top=354, width=156,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSLM_BIV",;
    cEvent = "w_PNTIVA Changed,w_PNTNOIVA Changed",;
    nPag=1;
    , HelpContextID = 113199846


  add object oObj_1_28 as cp_runprogram with uid="GFBSISZIYX",left=42, top=354, width=156,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSLM_BEX",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 113199846


  add object oBtn_1_29 as StdButton with uid="DWMZURCFKM",left=368, top=284, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 64769050;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        GSLMLBEA(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not (.w_CLI='N' and .w_FOR='N' and .w_SOT='N' and .w_PNTIVA='N' and .w_PNTNOIVA='N'))
      endwith
    endif
  endfunc

  func oBtn_1_29.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_TRAEXP='A')
     endwith
    endif
  endfunc


  add object oObj_1_30 as cp_calclbl with uid="FGUYHAFZYG",left=6, top=5, width=461,height=39,;
    caption='',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=1;
    , HelpContextID = 64797706


  add object oBtn_1_31 as StdButton with uid="UHYDRUVPVZ",left=7, top=284, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 57157674;
    , Caption='\<Norm';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      with this.Parent.oContained
        GSLM_bno(this.Parent.oContained,200,.w_percorso,.w_APPOGGIO)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_31.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_APPOGGIO))
     endwith
    endif
  endfunc


  add object oBtn_1_34 as StdButton with uid="CZCHOQTNBH",left=57, top=284, width=48,height=45,;
    CpPicture="BMP\log.bmp", caption="", nPag=1;
    , ToolTipText = "File di controllo elenco fatture\incassi da esportare";
    , HelpContextID = 46046850;
    , Caption='\<Log';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_34.Click()
      with this.Parent.oContained
        GSLMLBEA(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_34.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_TRAEXP='A')
     endwith
    endif
  endfunc

  add object oCODCAU_1_35 as StdField with uid="XBRLBCVBGD",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODCAU", cQueryName = "CODCAU",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Primi tre caratteri codice causale",;
    HelpContextID = 91936550,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=329, Top=193, InputMask=replicate('X',3)

  add object oStr_1_19 as StdString with uid="QIIJNUYPIP",Visible=.t., Left=6, Top=50,;
    Alignment=1, Width=128, Height=15,;
    Caption="Azienda da trasferire:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="XQZDKMODZP",Visible=.t., Left=8, Top=81,;
    Alignment=0, Width=198, Height=15,;
    Caption="Parametri di export"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="ZVRPMNSISS",Visible=.t., Left=146, Top=142,;
    Alignment=1, Width=83, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="OGZBYCPMMU",Visible=.t., Left=8, Top=213,;
    Alignment=0, Width=231, Height=15,;
    Caption="Cartella di export"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="XULLRNYWIG",Visible=.t., Left=9, Top=239,;
    Alignment=0, Width=387, Height=15,;
    Caption="Il file creato verr� memorizzato nella seguente cartella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="CVJGTWOFGK",Visible=.t., Left=146, Top=169,;
    Alignment=1, Width=83, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="BZBSKYDEMB",Visible=.t., Left=160, Top=196,;
    Alignment=1, Width=69, Height=18,;
    Caption="Progressivo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="ZTOIVBGCGU",Visible=.t., Left=278, Top=196,;
    Alignment=0, Width=50, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oBox_1_25 as StdBox with uid="YVLCLZMKYK",left=8, top=232, width=455,height=2

  add object oBox_1_26 as StdBox with uid="ZGBQXDFARZ",left=8, top=99, width=455,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslmlkex','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
