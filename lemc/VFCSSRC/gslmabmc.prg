* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslmabmc                                                        *
*              EXPORT MOVIMENTI CONTABILI                                      *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][116][VRS_291]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-24                                                      *
* Last revis.: 2017-11-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pNoIva
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslmabmc",oParentObject,m.pNoIva)
return(i_retval)

define class tgslmabmc as StdBatch
  * --- Local variables
  pNoIva = space(1)
  w_STRINGA = space(120)
  w_ACTUALPOS = 0
  w_PNSERIAL = space(10)
  w_TOTDOC = 0
  w_ANCODSTU = space(6)
  w_APRCAU = space(3)
  w_PNCODCAU = space(5)
  w_PNTIPCON = space(1)
  w_PNCODCON = space(15)
  w_NUMDETPERREG = 0
  w_DESCRIZIONE = space(29)
  w_CAUCAD = space(3)
  w_CAUDAC = space(3)
  w_PNCAURIG = space(5)
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_COD_DARE = space(1)
  w_COD_AVER = space(1)
  w_SERDACOR = .f.
  w_CODSOGG = space(8)
  w_CODPIVA = space(16)
  w_CODFISC = space(16)
  w_CODSOGGD = space(8)
  w_CODPIVAD = space(16)
  w_CODFISCD = space(16)
  w_CODSOGGA = space(8)
  w_CODPIVADA = space(16)
  w_CODFISCDA = space(16)
  w_CONTROPA = space(15)
  w_CAUDOC = space(5)
  w_CODIVA = space(5)
  w_PNSERORI = space(10)
  w_Valuta = space(3)
  w_Valore = 0
  w_Lunghezza = 0
  w_StrInt = space(1)
  w_StrDec = space(1)
  w_Decimali = 0
  w_OKFOR = .f.
  w_CODPIVAA = space(12)
  w_CODFISCA = space(16)
  w_TIPOCONTO = space(1)
  w_CONRIF = space(15)
  w_TIPRIF = space(1)
  w_SOTTOCONTO = space(9)
  w_SOTTCLI = space(6)
  * --- WorkFile variables
  STU_PARA_idx=0
  CONTI_idx=0
  STU_TRAS_idx=0
  STU_PNTT_idx=0
  CAUIVA1_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT MOVIMENTI CONTABILI
    *     pNoIva='S' Tutto
    *     pNoIva='N' Solo giroconti automatici
    this.w_StrInt = space(1)
    this.w_StrDec = space(1)
    this.w_Valuta = g_PERVAL
    this.w_Decimali = GETVALUT( this.w_Valuta, "VADECTOT")
    * --- Cambiamento del formato delle date per rendere i cursori compatibili
    if this.pNoIva="S"
      * --- Se export anche prima nota senza IVA metto assieme queste registrazioni
      *     con i giroconti creati in automatico
      * --- Aggiunta nuova colonna 'Omaggio'  C(1),  inizializzata a zero ('0'). Tale campo indicher� la riga omaggio su cui sostituire la 
      *     contropartita con l'intestatario della registazione (PNCODCLF).
      SELECT PNSERIAL, PNNUMRER, CP_TODATE(PNDATREG) AS PNDATREG, PNCODCAU, PNCOMPET, PNTIPDOC, ; 
 PNNUMDOC, PNALFDOC, CP_TODATE(PNDATDOC) AS PNDATDOC, PNNUMPRO, PNALFPRO, PNCODVAL, ; 
 PNVALNAZ, PNTOTDOC, PNTIPCLF, PNCODCLF, CP_TODATE(PNCOMIVA) AS PNCOMIVA, PNTIPREG, ; 
 PNNUMREG, PNCAURIG, PNDESRIG, PNTIPCON, PNIMPDAR, PNCODCON, PNIMPAVE, ANTIPSOT, CCFLPDIF, CPROWNUM , PNDESSUP , ATATTIVA,NUMERO, " " As Giroconto,"0" As Omaggio,PNSERORI; 
 FROM MOCONTAB INTO CURSOR MOCONTAB NOFILTER
    else
      * --- Prendo solo i movimenti generati in automatico come giroconti
      if NOT used("MOVDAFAT")
        i_retcode = 'stop'
        return
      endif
    endif
    if used("MOCONTAB")
      =wrcursor("MOCONTAB")
       
 SELECT MOCONTAB 
 Go Top
      * --- Ciclo sul cursore dei movimenti contabili
      do while NOT EOF()
        * --- Messaggio a schermo
        ah_Msg("Export movimenti contabili: reg. num. %1 - data %2 %3",.t.,.f.,.f.,STR(MOCONTAB.PNNUMRER,6,0),dtoc(MOCONTAB.PNDATREG),MOCONTAB.Giroconto)
        * --- Scrittura su LOG
        this.w_STRINGA = ah_MsgFormat("%1Export movimenti contabili : reg. num. %2 - data %3 giroconto","           ",STR(MOCONTAB.PNNUMRER,6,0),dtoc(MOCONTAB.PNDATREG))
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        * --- Salvo la posizione attuale del record e il seriale della registrazione
        this.w_ACTUALPOS = RECNO()
        this.w_PNSERIAL = MOCONTAB.PNSERIAL
        this.w_TOTDOC = 0
        * --- Verifico la presenza di giroconti derivanti da righe iva Omaggio imponibile o Omaggio imponibile+ Iva per la registrazione attuale
        * --- Select from gslm1bmc
        do vq_exec with 'gslm1bmc',this,'_Curs_gslm1bmc','',.f.,.t.
        if used('_Curs_gslm1bmc')
          select _Curs_gslm1bmc
          locate for 1=1
          do while not(eof())
          * --- Marco la riga corrispondente all'omaggio, inserendo il valore '1' nella colonna 'Omaggio'
          this.w_CONTROPA = _Curs_gslm1bmc.IVCONTRO
          do case
            case _Curs_gslm1bmc.CCFLRIFE="C" and _Curs_gslm1bmc.CCTIPDOC="FA"
              update MOCONTAB set Omaggio="1" where PNSERIAL=this.w_PNSERIAL and PNCODCON = this.w_CONTROPA AND PNIMPAVE <> 0
            case _Curs_gslm1bmc.CCFLRIFE="C" and _Curs_gslm1bmc.CCTIPDOC="NC"
              update MOCONTAB set Omaggio="1" where PNSERIAL=this.w_PNSERIAL and PNCODCON = this.w_CONTROPA AND PNIMPDAR <> 0
            case _Curs_gslm1bmc.CCFLRIFE="F" and _Curs_gslm1bmc.CCTIPDOC="FA"
              update MOCONTAB set Omaggio="1" where PNSERIAL=this.w_PNSERIAL and PNCODCON = this.w_CONTROPA AND PNIMPDAR <> 0
            case _Curs_gslm1bmc.CCFLRIFE="F" and _Curs_gslm1bmc.CCTIPDOC="NC"
              update MOCONTAB set Omaggio="1" where PNSERIAL=this.w_PNSERIAL and PNCODCON = this.w_CONTROPA AND PNIMPAVE <> 0
            otherwise
              update MOCONTAB set Omaggio="1" where PNSERIAL=this.w_PNSERIAL and PNCODCON = this.w_CONTROPA
          endcase
          * --- Gestione omaggio imponibile + IVA
          if _Curs_gslm1bmc.IVFLOMAG == "E" 
            this.w_CAUDOC = _Curs_gslm1bmc.CCCODICE
            this.w_CODIVA = _Curs_gslm1bmc.IVCODIVA 
            * --- Leggo il conto iva dalla causale contabile di testata
            * --- Read from CAUIVA1
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAUIVA1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2],.t.,this.CAUIVA1_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "AICONDET"+;
                " from "+i_cTable+" CAUIVA1 where ";
                    +"AICODCAU = "+cp_ToStrODBC(this.w_CAUDOC);
                    +" and AICODIVA = "+cp_ToStrODBC(this.w_CODIVA);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                AICONDET;
                from (i_cTable) where;
                    AICODCAU = this.w_CAUDOC;
                    and AICODIVA = this.w_CODIVA;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CONTROPA = NVL(cp_ToDate(_read_.AICONDET),cp_NullValue(_read_.AICONDET))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            update MOCONTAB set Omaggio="1" where PNSERIAL=this.w_PNSERIAL and PNCODCON = this.w_CONTROPA and PNCAURIG<>this.w_CAUDOC 
          endif
            select _Curs_gslm1bmc
            continue
          enddo
          use
        endif
        SELECT MOCONTAB
        * --- Ripristino posizione
        go this.w_ACTUALPOS
        * --- Conto il numero di dettagli per questa registrazione
        this.w_NUMDETPERREG = 0
        * --- Ripristino posizione
        go this.w_ACTUALPOS
        * --- Seleziono la tipologia di registrazione contabile
        * --- Se ho pi� righe di dettaglio, allora � una registrazione complessa Studio
        do while MOCONTAB.PNSERIAL=this.w_PNSERIAL
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          skip 1
          * --- Decremento w_NUMDETPERREG
          this.w_NUMDETPERREG = this.w_NUMDETPERREG-1
          * --- Avanzo il puntatore
        enddo
        * --- Movimento Contabile senza IVA
        * --- Try
        local bErr_03832D60
        bErr_03832D60=bTrsErr
        this.Try_03832D60()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Se � gi� presente (per l'esportazione del moviemto contabile CON IVA) aggiorno solo il campo del progressivo NO IVA
          * --- accept error
          bTrsErr=.f.
          * --- Write into STU_PNTT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.STU_PNTT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_PNTT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LMNUMTR2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'STU_PNTT','LMNUMTR2');
                +i_ccchkf ;
            +" where ";
                +"LMSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                   )
          else
            update (i_cTable) set;
                LMNUMTR2 = this.oParentObject.w_PROFIL;
                &i_ccchkf. ;
             where;
                LMSERIAL = this.w_PNSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='GSLM_BMC: Scrittura in STU_PNTT'
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_03832D60
        * --- End
        SELECT MOCONTAB
      enddo
    endif
    this.w_STRINGA = ah_MsgFormat("(*) Movimenti di giroconto automatici")
    FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
    FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
    if used("MOVDAFAT")
      SELECT MOVDAFAT
      GO TOP
      * --- Ciclo sul cursore dei movimenti contabili derivanti da fatture di acquisto europee
      do while NOT EOF()
        * --- Messaggio a schermo
        ah_Msg("Export movimenti contabili: reg. num. %1 - data %2",.t.,.f.,.f.,STR(MOVDAFAT.PNNUMRER,6,0),dtoc(MOVDAFAT.PNDATREG))
        * --- Scrittura su LOG
        this.w_STRINGA = ah_msgFormat("%1Export movimenti contabili : reg. num. %2 - data %3","          ",STR(MOVDAFAT.PNNUMRER,6,0),dtoc(MOVDAFAT.PNDATREG))
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        * --- Salvo la posizione attuale del record e il seriale della registrazione
        this.w_ACTUALPOS = RECNO()
        this.w_PNSERIAL = MOVDAFAT.PNSERIAL
        this.w_TOTDOC = 0
        * --- Butto giu la registrazione derivante dalle Fatture EUROPEE di Acquisto
        this.w_OKFOR = .T.
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_OKFOR = .F.
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        skip 1
        * --- Decremento w_NUMDETPERREG
        this.w_NUMDETPERREG = this.w_NUMDETPERREG-1
        * --- Avanzo il puntatore
        * --- Try
        local bErr_0382DCF0
        bErr_0382DCF0=bTrsErr
        this.Try_0382DCF0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Essendo w_PROFIL=numero del trasferimento
          * --- (uguale a quanto inserito dalle routine GSLM_BFA relativo alle fatture UE in caso di successo)
          * --- (in caso di insuccesso il valore inserito dalla routine GSLM_BFA � -1)
          * --- Non bisogna scrivere niente
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_0382DCF0
        * --- End
        SELECT MOVDAFAT
      enddo
      USE
    endif
  endproc
  proc Try_03832D60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Provo ad inserire nella tabella temporanea il progressivo per il movimento contabile NO IVA
    * --- Insert into STU_PNTT
    i_nConn=i_TableProp[this.STU_PNTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTRA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'STU_PNTT','LMNUMTR2');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',0,'LMNUMTR2',this.oParentObject.w_PROFIL)
      insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,0;
           ,this.oParentObject.w_PROFIL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='GSLM_BMC: Scrittura in STU_PNTT'
      return
    endif
    return
  proc Try_0382DCF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_PNTT
    i_nConn=i_TableProp[this.STU_PNTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMSERIAL"+",LMNUMTRA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'STU_PNTT','LMNUMTRA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.oParentObject.w_PROFIL)
      insert into (i_cTable) (LMSERIAL,LMNUMTRA &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.oParentObject.w_PROFIL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='GSLM_BMC: Scrittura in STU_PNTT'
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Incremento numero di record scritti
    this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
    this.oParentObject.w_MOCONTAB = this.oParentObject.w_MOCONTAB+1
    * --- Tipo record
    FWRITE(this.oParentObject.hFile,"D50",3)
    * --- Data registrazione
    FWRITE(this.oParentObject.hFile,dtos(MOCONTAB.PNDATREG),8)
    * --- Recupero descrizione
    this.w_DESCRIZIONE = MOCONTAB.PNDESRIG
    if EMPTY(MOCONTAB.PNDESRIG)
      this.w_DESCRIZIONE = LOOKTAB("CAU_CONT","CCDESCRI","CCCODICE",MOCONTAB.PNCAURIG)
    endif
    * --- Recupero codice causale dello studio - Se riga in DARE --> Conto a Diversi
    * --- Recupero codice causale dello studio - Se riga in AVERE --> Diversi a conto
    this.w_CAUCAD = SPACE(3)
    this.w_CAUDAC = SPACE(3)
    * --- Read from STU_PARA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.STU_PARA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PARA_idx,2],.t.,this.STU_PARA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LMCAUDAC,LMCAUCAD"+;
        " from "+i_cTable+" STU_PARA where ";
            +"LMCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LMCAUDAC,LMCAUCAD;
        from (i_cTable) where;
            LMCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAUDAC = NVL(cp_ToDate(_read_.LMCAUDAC),cp_NullValue(_read_.LMCAUDAC))
      this.w_CAUCAD = NVL(cp_ToDate(_read_.LMCAUCAD),cp_NullValue(_read_.LMCAUCAD))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT MOCONTAB
    * --- Controllo correttezza causali
    if EMPTY(this.w_CAUDAC) AND MOCONTAB.PNIMPDAR=0
      this.w_STRINGA = ah_MsgFormat("%1Errore! Causale diversi a conto non definita nella tabella trasferimento studio","               ")
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      this.oParentObject.w_ERRORE = .T.
    endif
    if EMPTY(this.w_CAUCAD) AND MOCONTAB.PNIMPDAR<>0
      this.w_STRINGA = ah_MsgFormat("%1Errore! Causale conto a diversi non definita nella tabella trasferimento studio","               ")
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      this.oParentObject.w_ERRORE = .T.
    endif
    * --- Scrittura del Codice Causale
    if MOCONTAB.PNIMPDAR=0
      FWRITE(this.oParentObject.hFile,RIGHT(space(10)+alltrim(this.w_CAUDAC),10),10)
    else
      FWRITE(this.oParentObject.hFile,RIGHT(space(10)+alltrim(this.w_CAUCAD),10),10)
    endif
    * --- Descrizione
    FWRITE(this.oParentObject.hFile,left(nvl(this.w_DESCRIZIONE," ")+repl(" ",30),30),30)
    * --- filler
    FWRITE(this.oParentObject.hFile," ",1)
    * --- Descrizione supplementare
    FWRITE(this.oParentObject.hFile,left(nvl(MOCONTAB.PNDESSUP," ")+repl(" ",30),30),30)
    this.oParentObject.w_FILLER = SPACE(4)
    FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,4)
    * --- Importo
    this.w_Valore = nvl(MOCONTAB.PNIMPDAR,0)+nvl(MOCONTAB.PNIMPAVE,0)
    this.w_Lunghezza = 16
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    FWRITE(this.oParentObject.hFile,this.w_VALORE,16)
    * --- Centro di costo e Conto DARE
    if MOCONTAB.PNIMPDAR<>0
      * --- Centro di costo
      FWRITE(this.oParentObject.hFile,"   ",3)
      * --- Conto DARE
      if MOCONTAB.Omaggio <> "1"
        this.w_PNTIPCON = NVL(MOCONTAB.PNTIPCON," ")
        this.w_COD_DARE = this.w_PNTIPCON
        this.w_PNCODCON = NVL(MOCONTAB.PNCODCON," ")
      else
        this.w_PNTIPCON = IIF(NVL(MOCONTAB.PNTIPCLF,MOCONTAB.PNTIPCON)#"N",NVL(MOCONTAB.PNTIPCLF,MOCONTAB.PNTIPCON),MOCONTAB.PNTIPCON)
        this.w_COD_DARE = this.w_PNTIPCON
        this.w_PNCODCON = NVL(MOCONTAB.PNCODCLF,MOCONTAB.PNCODCON)
      endif
      this.w_ANCODSTU = SPACE(9)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU,ANCODSOG,ANPARIVA,ANCODFIS"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU,ANCODSOG,ANPARIVA,ANCODFIS;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCON;
              and ANCODICE = this.w_PNCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        this.w_CODSOGGD = NVL(cp_ToDate(_read_.ANCODSOG),cp_NullValue(_read_.ANCODSOG))
        this.w_CODPIVAD = NVL(cp_ToDate(_read_.ANPARIVA),cp_NullValue(_read_.ANPARIVA))
        this.w_CODFISCD = NVL(cp_ToDate(_read_.ANCODFIS),cp_NullValue(_read_.ANCODFIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT MOCONTAB
      if EMPTY(this.w_ANCODSTU)
        this.w_ANCODSTU = "000000000"
        do case
          case this.w_PNTIPCON="C"
            this.w_STRINGA = ah_MsgFormat("%1Errore! Cliente %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          case this.w_PNTIPCON="F"
            this.w_STRINGA = ah_Msgformat("%1Errore! Fornitore %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          otherwise
            this.w_STRINGA = ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        endcase
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      * --- Scrivo il Conto sul file
      if this.w_PNTIPCON="G"
        FWRITE(this.oParentObject.hFile,left(alltrim(this.w_ANCODSTU)+repl("0",9),9),9)
      else
        FWRITE(this.oParentObject.hFile,SPACE(9),9)
      endif
    else
      * --- Centro di costo e Conto DARE sono empty
      FWRITE(this.oParentObject.hFile,SPACE(12),12)
      this.w_COD_DARE = "G"
    endif
    * --- FILLER
    this.oParentObject.w_FILLER = SPACE(2)
    FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,2)
    if MOCONTAB.PNIMPDAR=0
      * --- Conto AVERE
      if MOCONTAB.Omaggio <> "1"
        this.w_PNTIPCON = NVL(MOCONTAB.PNTIPCON," ")
        this.w_COD_AVER = this.w_PNTIPCON
        this.w_PNCODCON = NVL(MOCONTAB.PNCODCON," ")
      else
        this.w_PNTIPCON = IIF(NVL(MOCONTAB.PNTIPCLF,MOCONTAB.PNTIPCON)#"N",NVL(MOCONTAB.PNTIPCLF,MOCONTAB.PNTIPCON),MOCONTAB.PNTIPCON)
        this.w_COD_DARE = this.w_PNTIPCON
        this.w_PNCODCON = NVL(MOCONTAB.PNCODCLF,MOCONTAB.PNCODCON)
      endif
      this.w_ANCODSTU = SPACE(9)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU,ANCODSOG,ANPARIVA,ANCODFIS"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU,ANCODSOG,ANPARIVA,ANCODFIS;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCON;
              and ANCODICE = this.w_PNCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        this.w_CODSOGGA = NVL(cp_ToDate(_read_.ANCODSOG),cp_NullValue(_read_.ANCODSOG))
        this.w_CODPIVAA = NVL(cp_ToDate(_read_.ANPARIVA),cp_NullValue(_read_.ANPARIVA))
        this.w_CODFISCA = NVL(cp_ToDate(_read_.ANCODFIS),cp_NullValue(_read_.ANCODFIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT MOCONTAB
      if EMPTY(this.w_ANCODSTU)
        this.w_ANCODSTU = "000000000"
        do case
          case this.w_PNTIPCON="C"
            this.w_STRINGA = ah_MsgFormat("%1Errore! Cliente %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          case this.w_PNTIPCON="F"
            this.w_STRINGA = ah_MsgFormat("%1Errore! Fornitore non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          otherwise
            this.w_STRINGA = ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        endcase
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      * --- Scrivo il Conto sul file
      if this.w_PNTIPCON="G"
        FWRITE(this.oParentObject.hFile,left(alltrim(this.w_ANCODSTU)+repl("0",9),9),9)
      else
        FWRITE(this.oParentObject.hFile,SPACE(9),9)
      endif
    else
      * --- Centro di costo e sottoconto DARE sono empty
      FWRITE(this.oParentObject.hFile,SPACE(9),9)
      this.w_COD_AVER = "G"
    endif
    * --- Tipo cliente/fornitore
    do case
      case this.w_PNTIPCON="C"
        FWRITE(this.oParentObject.hFile,"001",3)
      case this.w_PNTIPCON="F"
        FWRITE(this.oParentObject.hFile,"002",3)
      otherwise
        FWRITE(this.oParentObject.hFile,"   ",3)
    endcase
    * --- Codice soggetto cliente
    this.w_TIPOCONTO = IIF(this.w_PNTIPCON="G"," ",this.w_PNTIPCON)
    if this.w_PNTIPCON<>"G"
      if MOCONTAB.PNIMPDAR<>0
        if this.oParentObject.w_LMAGOFLT="S"
          if NOT EMPTY(this.w_PNCODCON)
            if LEN(alltrim(this.w_PNCODCON))=15
              if g_CFNUME="S"
                FWRITE(this.oParentObject.hFile,this.w_TIPOCONTO+SUBSTR(this.w_PNCODCON,2,14),15)
              else
                FWRITE(this.oParentObject.hFile,right(Space(15)+alltrim(this.w_PNCODCON),15),15)
              endif
            else
              FWRITE(this.oParentObject.hFile,right(Space(15)+alltrim(this.w_TIPOCONTO+this.w_PNCODCON),15),15)
            endif
          else
            FWRITE(this.oParentObject.hFile,right(Space(15)+alltrim(this.w_PNCODCON),15),15)
          endif
        else
          FWRITE(this.oParentObject.hFile,right(Space(15)+alltrim(this.w_PNCODCON),15),15)
        endif
      else
        FWRITE(this.oParentObject.hFile,SPACE(15),15)
      endif
    else
      FWRITE(this.oParentObject.hFile,SPACE(15),15)
    endif
    * --- FILLER
    this.oParentObject.w_FILLER = SPACE(21)
    FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,21)
    * --- Codice soggetto fornitore
    if this.w_PNTIPCON<>"G"
      if MOCONTAB.PNIMPDAR=0
        if this.oParentObject.w_LMAGOFLT="S"
          if NOT EMPTY(this.w_PNCODCON)
            if LEN(alltrim(this.w_PNCODCON))=15
              if g_CFNUME="S"
                FWRITE(this.oParentObject.hFile,this.w_TIPOCONTO+SUBSTR(this.w_PNCODCON,2,14),15)
              else
                FWRITE(this.oParentObject.hFile,right(Space(15)+alltrim(this.w_PNCODCON),15),15)
              endif
            else
              FWRITE(this.oParentObject.hFile,right(Space(15)+alltrim(this.w_TIPOCONTO+this.w_PNCODCON),15),15)
            endif
          else
            FWRITE(this.oParentObject.hFile,right(Space(15)+alltrim(this.w_PNCODCON),15),15)
          endif
        else
          FWRITE(this.oParentObject.hFile,right(Space(15)+alltrim(this.w_PNCODCON),15),15)
        endif
      else
        FWRITE(this.oParentObject.hFile,SPACE(15),15)
      endif
    else
      FWRITE(this.oParentObject.hFile,SPACE(15),15)
    endif
    * --- FILLER
    this.oParentObject.w_FILLER = SPACE(21)
    FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,21)
    * --- Codice codifica
    FWRITE(this.oParentObject.hFile,replicate(" ",15),15)
    * --- FILLER
    this.oParentObject.w_FILLER = SPACE(21)
    FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,21)
    * --- Codice del sottoconto
    FWRITE(this.oParentObject.hFile,replicate(" ",9),9)
    * --- Codice Attivit� IVA
    FWRITE(this.oParentObject.hFile,right("00"+alltrim(STR(MOCONTAB.ATATTIVA)),2),2)
    * --- Data competenza da
    FWRITE(this.oParentObject.hFile,replicate(" ",8),8)
    * --- Data competenza a
    FWRITE(this.oParentObject.hFile,replicate(" ",8),8)
    * --- Codice identificativo di provenienza della prima nota
    FWRITE(this.oParentObject.hFile,replicate(" ",3),3)
    * --- Progressivo univoco righe di primanota
    FWRITE(this.oParentObject.hFile,RIGHT(repl("0",15)+ALLTRIM(MOCONTAB.NUMERO),15),15)
    * --- FILLER
    this.oParentObject.w_FILLER = SPACE(19)
    FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,19)
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- SINGOLA RIGA DERIVANTE DAI CORRISPETTIVI CON SCORPORO
    this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
    this.oParentObject.w_MOCONTAB = this.oParentObject.w_MOCONTAB+1
    * --- Tipo record
    FWRITE(this.oParentObject.hFile,"D50",3)
    * --- Data registrazione
    FWRITE(this.oParentObject.hFile,dtos(MOVDAFAT.PNDATREG),8)
    * --- Recupero descrizione
    this.w_DESCRIZIONE = MOVDAFAT.PNDESRIG
    if EMPTY(MOVDAFAT.PNDESRIG)
      this.w_DESCRIZIONE = LOOKTAB("CAU_CONT","CCDESCRI","CCCODICE",MOVDAFAT.PNCAURIG)
    endif
    * --- Recupero codice causale dello studio
    this.w_APRCAU = SPACE(10)
    this.w_PNCAURIG = NVL(MOVDAFAT.PNCAURIG," ")
    * --- Read from STU_TRAS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.STU_TRAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LMAPRCAU"+;
        " from "+i_cTable+" STU_TRAS where ";
            +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
            +" and LMHOCCAU = "+cp_ToStrODBC(this.w_PNCAURIG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LMAPRCAU;
        from (i_cTable) where;
            LMCODICE = this.oParentObject.w_ASSOCI;
            and LMHOCCAU = this.w_PNCAURIG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_APRCAU = NVL(cp_ToDate(_read_.LMAPRCAU),cp_NullValue(_read_.LMAPRCAU))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT MOVDAFAT
    if EMPTY(this.w_APRCAU)
      this.w_APRCAU = "0000000000"
      * --- Non ho trovato associato il codice causale
      this.w_STRINGA = ah_MsgFormat("%1Errore! Causale %2 non associata nello studio","               ",NVL(this.w_PNCAURIG, " "))
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      this.oParentObject.w_ERRORE = .T.
    endif
    FWRITE(this.oParentObject.hFile,left(alltrim(nvl(this.w_APRCAU," "))+repl(" ",10),10),10)
    * --- Descrizione
    FWRITE(this.oParentObject.hFile,left(alltrim(nvl(this.w_DESCRIZIONE," "))+repl(" ",30),30),30)
    * --- filler
    FWRITE(this.oParentObject.hFile," ",1)
    * --- Descrizione supplementare
    FWRITE(this.oParentObject.hFile,left(nvl(MOVDAFAT.PNDESSUP," ")+repl(" ",30),30),30)
    this.oParentObject.w_FILLER = SPACE(4)
    FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,4)
    * --- Importo
    this.w_Valore = nvl(MOVDAFAT.PNIMPDAR,0)+nvl(MOVDAFAT.PNIMPAVE,0)
    this.w_Lunghezza = 16
    this.Page_4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    FWRITE(this.oParentObject.hFile,this.w_VALORE,16)
    * --- Centro di costo
    FWRITE(this.oParentObject.hFile,"   ",3)
    * --- Conto DARE
    * --- Scrivo il Conto sul file
    FWRITE(this.oParentObject.hFile,SPACE(9),9)
    * --- FILLER
    this.oParentObject.w_FILLER = SPACE(2)
    FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,2)
    * --- Scrivo il Conto sul file
    FWRITE(this.oParentObject.hFile,SPACE(9),9)
    * --- Tipo cliente/fornitore
    if this.w_OKFOR=.T.
      FWRITE(this.oParentObject.hFile,"002",3)
    else
      FWRITE(this.oParentObject.hFile,"001",3)
    endif
    * --- Codice soggetto fornitore
    this.w_PNTIPCLF = NVL(MOVDAFAT.PNTIPCLF," ")
    this.w_COD_DARE = this.w_PNTIPCLF
    this.w_PNCODCLF = NVL(MOVDAFAT.PNCODCLF," ")
    this.w_ANCODSTU = SPACE(9)
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANCODSTU,ANCODSOG,ANPARIVA,ANCODFIS,ANCONRIF,ANTIPRIF"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANCODSTU,ANCODSOG,ANPARIVA,ANCODFIS,ANCONRIF,ANTIPRIF;
        from (i_cTable) where;
            ANTIPCON = this.w_PNTIPCLF;
            and ANCODICE = this.w_PNCODCLF;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
      this.w_CODSOGGD = NVL(cp_ToDate(_read_.ANCODSOG),cp_NullValue(_read_.ANCODSOG))
      this.w_CODPIVAD = NVL(cp_ToDate(_read_.ANPARIVA),cp_NullValue(_read_.ANPARIVA))
      this.w_CODFISCD = NVL(cp_ToDate(_read_.ANCODFIS),cp_NullValue(_read_.ANCODFIS))
      this.w_CONRIF = NVL(cp_ToDate(_read_.ANCONRIF),cp_NullValue(_read_.ANCONRIF))
      this.w_TIPRIF = NVL(cp_ToDate(_read_.ANTIPRIF),cp_NullValue(_read_.ANTIPRIF))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT MOVDAFAT
    if EMPTY(this.w_ANCODSTU)
      this.w_ANCODSTU = "000000000"
      do case
        case this.w_PNTIPCON="C"
          this.w_STRINGA = ah_MsgFormat("%1Errore! Cliente %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        case this.w_PNTIPCON="F"
          this.w_STRINGA = ah_Msgformat("%1Errore! Fornitore %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        otherwise
          this.w_STRINGA = ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
      endcase
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      this.oParentObject.w_ERRORE = .T.
    endif
    if (this.w_OKFOR AND !(ALLTRIM(MOVDAFAT.PNTIPDOC)$"NU-NE") ) OR (! this.w_OKFOR AND ALLTRIM(MOVDAFAT.PNTIPDOC) $ "NU-NE" )
      if this.oParentObject.w_LMAGOFLT="S"
        if NOT EMPTY(this.w_PNCODCLF)
          if LEN(alltrim(this.w_PNCODCLF))=15
            if g_CFNUME="S"
              FWRITE(this.oParentObject.hFile,this.w_PNTIPCLF+SUBSTR(this.w_PNCODCLF,2,14),15)
            else
              FWRITE(this.oParentObject.hFile,right(Space(15)+alltrim(this.w_PNCODCLF),15),15)
            endif
          else
            FWRITE(this.oParentObject.hFile,right(Space(15)+alltrim(this.w_PNTIPCLF+this.w_PNCODCLF),15),15)
          endif
        else
          FWRITE(this.oParentObject.hFile,right(Space(15)+alltrim(this.w_PNCODCLF),15),15)
        endif
      else
        FWRITE(this.oParentObject.hFile,right(Space(15)+alltrim(this.w_PNCODCLF),15),15)
      endif
    else
      FWRITE(this.oParentObject.hFile,Space(15),15)
    endif
    * --- FILLER
    this.oParentObject.w_FILLER = SPACE(21)
    FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,21)
    * --- Codice soggetto CLIENTE
    if (! this.w_OKFOR AND !(ALLTRIM(MOVDAFAT.PNTIPDOC)$"NU-NE") ) OR ( this.w_OKFOR AND ALLTRIM(MOVDAFAT.PNTIPDOC) $ "NU-NE" )
      if this.oParentObject.w_LMAGOFLT="S"
        if NOT EMPTY(this.w_PNCODCLF)
          if LEN(alltrim(this.w_PNCODCLF))=15
            if g_CFNUME="S"
              FWRITE(this.oParentObject.hFile,this.w_PNTIPCLF+SUBSTR(this.w_PNCODCLF,2,14),15)
            else
              FWRITE(this.oParentObject.hFile,right(Space(15)+alltrim(this.w_PNCODCLF),15),15)
            endif
          else
            FWRITE(this.oParentObject.hFile,right(Space(15)+alltrim(this.w_PNTIPCLF+this.w_PNCODCLF),15),15)
          endif
        else
          FWRITE(this.oParentObject.hFile,right(Space(15)+alltrim(this.w_PNCODCLF),15),15)
        endif
      else
        FWRITE(this.oParentObject.hFile,right(Space(15)+alltrim(this.w_PNCODCLF),15),15)
      endif
    else
      FWRITE(this.oParentObject.hFile,Space(15),15)
    endif
    * --- FILLER
    this.oParentObject.w_FILLER = SPACE(21)
    FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,21)
    * --- Codice codifica
    FWRITE(this.oParentObject.hFile,replicate(" ",15),15)
    * --- FILLER
    this.oParentObject.w_FILLER = SPACE(21)
    FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,21)
    * --- Codice del sottoconto
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANCODSTU"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPRIF);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_CONRIF);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANCODSTU;
        from (i_cTable) where;
            ANTIPCON = this.w_TIPRIF;
            and ANCODICE = this.w_CONRIF;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SOTTCLI = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_SOTTOCONTO = IIF(this.w_OKFOR=.T.,this.w_ANCODSTU,this.w_SOTTCLI)
    FWRITE(this.oParentObject.hFile,left(alltrim(this.w_SOTTOCONTO)+repl("0",9),9),9)
    * --- Codice Attivit� IVA
    FWRITE(this.oParentObject.hFile,right("00"+alltrim(STR(MOVDAFAT.ATATTIVA)),2),2)
    * --- Data competenza da
    FWRITE(this.oParentObject.hFile,replicate(" ",8),8)
    * --- Data competenza a
    FWRITE(this.oParentObject.hFile,replicate(" ",8),8)
    * --- Codice identificativo di provenienza della prima nota
    FWRITE(this.oParentObject.hFile,replicate(" ",3),3)
    * --- Progressivo univoco righe di primanota
    FWRITE(this.oParentObject.hFile,RIGHT(repl("0",15)+ALLTRIM(MOVDAFAT.PNSERIAL+ALLTRIM(STR(MOVDAFAT.CPROWNUM))),15),15)
    * --- FILLER
    this.oParentObject.w_FILLER = SPACE(19)
    FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,19)
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Trasformazione degli importi nel formato richiesto dal tracciato R.I.D.
    * --- UTILIZZO
    * --- Prima della chiamata a questa pagina del batch devono essere impostate le variabili:
    * --- w_Valore          = importo da convertire in stringa
    * --- w_Lunghezza  = lunghezza della stringa
    * --- Il risultato della conversione � disponibile nella variabile w_Valore.
    * --- CONVENZIONI
    * --- La virgola deve essere eliminata
    * --- Se la valuta � Euro le ultime due cifre rappresentano i decimali
    * --- Il valore deve essere allineato a destra
    * --- Non � richiesto il riempimento con 0 delle cifre non significative
    * --- Calcolo parte intera
    this.w_StrInt = alltrim( str( int(this.w_Valore), this.w_Lunghezza, 0 ) )
    * --- Calcolo parte decimale
    this.w_StrDec = ""
    if this.w_Decimali > 0
      this.w_StrDec = right( str( this.w_Valore, this.w_Lunghezza, this.w_Decimali ) , this.w_Decimali )
    endif
    * --- Risultato
    this.w_Valore = right( repl("0",this.w_Lunghezza) + this.w_StrInt + this.w_StrDec , this.w_Lunghezza )
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pNoIva)
    this.pNoIva=pNoIva
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='STU_PARA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='STU_TRAS'
    this.cWorkTables[4]='STU_PNTT'
    this.cWorkTables[5]='CAUIVA1'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_gslm1bmc')
      use in _Curs_gslm1bmc
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pNoIva"
endproc
