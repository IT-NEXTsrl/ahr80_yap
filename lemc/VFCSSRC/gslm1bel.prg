* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm1bel                                                        *
*              Export LEMBI - contb                                            *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][116][VRS_179]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-17                                                      *
* Last revis.: 2000-09-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm1bel",oParentObject)
return(i_retval)

define class tgslm1bel as StdBatch
  * --- Local variables
  hFILE = 0
  hLOG = 0
  w_ERRORE = .f.
  w_CODAZI = space(5)
  w_AZCOFAZI = space(16)
  w_AZPIVAZI = space(12)
  w_AZRAGAZI = space(40)
  w_DITTA = space(6)
  w_DATA = space(10)
  w_STRINGA = space(120)
  w_CURRDIR = space(50)
  w_FILESALDI = space(50)
  w_FILELOG = space(50)
  w_PERCORSO = space(50)
  w_DECIMALI = 0
  w_TSSEZBIL = space(1)
  w_TSCODICE = space(20)
  w_TSDESPIA = space(40)
  w_TS__DARE = 0
  w_TS_AVERE = 0
  w_TSTIPCON = space(1)
  w_ANCODSTU = space(6)
  w_CONTOSEZ = space(15)
  w_ESTRINF = space(6)
  w_CLIFOR = space(1)
  w_CODPDC = space(4)
  w_MASTRO = space(10)
  w_MCODSTU = space(3)
  w_MPROGRE = space(8)
  w_DESPIA = space(30)
  * --- WorkFile variables
  AZIENDA_idx=0
  CONTI_idx=0
  STU_PARA_idx=0
  STUMPIAC_idx=0
  STU_PIAC_idx=0
  MASTRI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Export tramite protocollo LEMBI
    * --- Controllo se ci sono dati da stampare
    SELECT STU_BILA
    if not reccount()>0
      ah_ErrorMsg("Non ci sono dati da esportare",,"")
      return
    endif
    this.hFile = 0
    this.hLog = 0
    this.w_ERRORE = .F.
    this.w_CODAZI = i_CODAZI
    this.w_AZCOFAZI = SPACE(16)
    this.w_AZPIVAZI = SPACE(12)
    this.w_AZRAGAZI = SPACE(40)
    this.w_DITTA = SPACE(6)
    * --- Inizializzazione e preparazione percorso di default
    this.w_DECIMALI = 2
    this.w_PERCORSO = SYS(5)+SYS(2003)+"\"+"CBLEMBI"
    if !DIRECTORY(this.w_PERCORSO)
      * --- Crea la directory LEMBI all'interno della dir EXE
      this.w_CURRDIR = SYS(5)+SYS(2003)
      MKDIR "CBLEMBI"
    endif
    * --- Preparazione nome temporaneo di appoggio
    this.w_FILESALDI = this.w_PERCORSO+"\"+"BILCON.D"
    this.w_FILELOG = this.w_PERCORSO+"\"+"BILCON.LOG"
    * --- Creazione e apertura file di appoggio
    this.hFile = FCREATE(this.w_FILESALDI,0)
    this.hLog = FCREATE(this.w_FILELOG,0)
    * --- Controllo creazione file di appoggio
    if this.hFile=-1
      ah_Msg("Impossibile creare il file di appoggio",.t.)
      return
    endif
    FCLOSE(this.hFile)
    * --- Controllo creazione file di log
    if this.hLOG=-1
      ah_Msg("Impossibile creare il file di log",.t.)
      return
    endif
    FCLOSE(this.hLOG)
    * --- Riapertura dei file
    this.hFile = FOPEN(this.w_FILESALDI,1)
    if this.hFile=-1
      ah_Msg("Impossibile aprire il file di appoggio",.t.)
      return
    endif
    this.hLog = FOPEN(this.w_FILELOG,1)
    if this.hLOG=-1
      ah_Msg("Impossibile aprire il file di log",.t.)
      return
    endif
    * --- Testata file di LOG
    FWRITE(this.hLOG,REPL("-",80),80)
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    * --- recupero ragione sociale azienda
    this.w_STRINGA = ah_MsgFormat("-- AZIENDA: %1",UPPER(g_RAGAZI))
    FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    this.w_STRINGA = ah_MsgFormat("-- DATA EXPORT: %1",dtoc(this.oParentObject.w_PDATSTA))
    FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    this.w_STRINGA = ah_MsgFormat("-- ANNO COMPETENZA: %1",this.oParentObject.w_PCODESE)
    FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    if this.oParentObject.w_PEXPORT="B"
      this.w_STRINGA = ah_MsgFormat("-- TIPOLOGIA EXPORT: BASE")
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    else
      this.w_STRINGA = ah_MsgFormat("-- TIPOLOGIA EXPORT: AVANZATO")
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    endif
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    FWRITE(this.hLOG,REPL("-",80),80)
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    * --- Recupero il Codice Ditta per il trasferimento
    * --- Read from STU_PARA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.STU_PARA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PARA_idx,2],.t.,this.STU_PARA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LMAZISTU,LMCODPDC"+;
        " from "+i_cTable+" STU_PARA where ";
            +"LMCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LMAZISTU,LMCODPDC;
        from (i_cTable) where;
            LMCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DITTA = NVL(cp_ToDate(_read_.LMAZISTU),cp_NullValue(_read_.LMAZISTU))
      this.w_CODPDC = NVL(cp_ToDate(_read_.LMCODPDC),cp_NullValue(_read_.LMCODPDC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = 'Fallita la lettura dei parametri per il trasferimento'
      return
    endif
    select (i_nOldArea)
    * --- INIZIO File -- Record di testa (LEMIIN)
    * --- Codice tipo struttura
    FWRITE(this.hFile,"IINT",4)
    * --- Filler
    FWRITE(this.hFile,space(96),96)
    * --- INIZIO Ditta - (LEMIDI)
    * --- Tipo Struttura
    FWRITE(this.hFile,"IDIT",4)
    * --- Codice Ditta
    if EMPTY(NVL(this.w_DITTA," "))
      this.w_STRINGA = ah_MsgFormat("Errore! Codice ditta non definito nella tabella trasferimento studio")
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      this.w_ERRORE = .T.
    else
      FWRITE(this.hFile,RIGHT("000000"+this.w_DITTA,6),6)
    endif
    * --- Codice Fiscale o Partita IVa dell'azienda
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZRAGAZI,AZCOFAZI,AZPIVAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZRAGAZI,AZCOFAZI,AZPIVAZI;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AZRAGAZI = NVL(cp_ToDate(_read_.AZRAGAZI),cp_NullValue(_read_.AZRAGAZI))
      this.w_AZCOFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      this.w_AZPIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(NVL(this.w_AZCOFAZI," "))
      if EMPTY(NVL(this.w_AZPIVAZI," "))
        * --- Errore
        this.w_STRINGA = ah_MsgFormat("Errore! Codice fiscale o partita IVA non definiti per l'azienda")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_ERRORE = .T.
      else
        * --- Inserisco la partita IVA
        FWRITE(this.hFile,LEFT(this.w_AZPIVAZI+repl(" ",16),16),16)
      endif
    else
      * --- Inserisco il codice fiscale
      FWRITE(this.hFile,LEFT(this.w_AZCOFAZI+repl(" ",16),16),16)
    endif
    * --- Denominazione ditta
    FWRITE(this.hFile,LEFT(NVL(this.w_AZRAGAZI," ")+repl(" ",40),40),40)
    * --- Mese e Anno di Trasferimento
    this.w_DATA = dtos(this.oParentObject.w_DATFIN)
    FWRITE(this.hFile,RIGHT("00"+SUBSTR(this.w_DATA,5,2),2),2)
    FWRITE(this.hFile,RIGHT("0000"+SUBSTR(this.w_DATA,1,4),4),4)
    * --- Codice Piano dei Conti - Informazione non disponibile, lasciato vuoto
    FWRITE(this.hFile,RIGHT("0000"+ALLTRIM(this.w_CODPDC),4),4)
    * --- Filler
    FWRITE(this.hFile,repl(" ",24),24)
    * --- Export del cursore costruito nel batch gslm_bsb
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Chiusura struttura file
    * --- FINE Ditta - (LEMFDI)
    * --- Tipo Struttura
    FWRITE(this.hFile,"FDIT",4)
    * --- Filler
    FWRITE(this.hFile,space(96),96)
    * --- FINE File -- Record di coda (LEMFIN)
    * --- Codice tipo struttura
    FWRITE(this.hFile,"FINT",4)
    * --- Filler
    FWRITE(this.hFile,space(96),96)
    * --- Chiusura file
    FCLOSE(this.hFile)
    FCLOSE(this.hLog)
    if USED("STU_BILA")
       
 SELECT STU_BILA 
 USE
    endif
    if NOT this.w_ERRORE
      * --- Lancio la maschera di conferma
      do GSLM_KCS with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Lancio la maschera di errore
      do GSLM_KES with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Export del piano dei conti
    * --- Ciclo sul temporaneo
    SELECT STU_BILA
    GO TOP
    do while NOT EOF()
      this.w_TSSEZBIL = NVL(STU_BILA.TSSEZBIL," ")
      this.w_TSCODICE = NVL(STU_BILA.TSCODICE," ")
      this.w_TSDESPIA = NVL(STU_BILA.TSDESPIA," ")
      this.w_TS__DARE = NVL(STU_BILA.TS__DARE,0)
      this.w_TS_AVERE = NVL(STU_BILA.TS_AVERE,0)
      this.w_TSTIPCON = NVL(STU_BILA.TSTIPCON," ")
      this.w_ANCODSTU = SPACE(6)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU,ANCONSUP"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TSTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_TSCODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU,ANCONSUP;
          from (i_cTable) where;
              ANTIPCON = this.w_TSTIPCON;
              and ANCODICE = this.w_TSCODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        this.w_MASTRO = NVL(cp_ToDate(_read_.ANCONSUP),cp_NullValue(_read_.ANCONSUP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Messaggio a Video
      if this.w_TSTIPCON $ "G-M"
        if NOT EMPTY(this.w_ANCODSTU) AND this.w_TSTIPCON="G"
          * --- Leggo Descrizione Sottoconto per il file di Log
          * --- Read from MASTRI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MASTRI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MCCODSTU,MCPROSTU"+;
              " from "+i_cTable+" MASTRI where ";
                  +"MCCODICE = "+cp_ToStrODBC(this.w_MASTRO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MCCODSTU,MCPROSTU;
              from (i_cTable) where;
                  MCCODICE = this.w_MASTRO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MCODSTU = NVL(cp_ToDate(_read_.MCCODSTU),cp_NullValue(_read_.MCCODSTU))
            this.w_MPROGRE = NVL(cp_ToDate(_read_.MCPROSTU),cp_NullValue(_read_.MCPROSTU))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from STU_PIAC
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.STU_PIAC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_PIAC_idx,2],.t.,this.STU_PIAC_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LMDESSOT"+;
              " from "+i_cTable+" STU_PIAC where ";
                  +"LMCODCON = "+cp_ToStrODBC(this.w_MCODSTU);
                  +" and LMPROGRE = "+cp_ToStrODBC(this.w_MPROGRE);
                  +" and LMCODSOT = "+cp_ToStrODBC(this.w_ANCODSTU);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LMDESSOT;
              from (i_cTable) where;
                  LMCODCON = this.w_MCODSTU;
                  and LMPROGRE = this.w_MPROGRE;
                  and LMCODSOT = this.w_ANCODSTU;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESPIA = NVL(cp_ToDate(_read_.LMDESSOT),cp_NullValue(_read_.LMDESSOT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          this.w_DESPIA = this.w_TSDESPIA
        endif
        ah_Msg("Export sottoconto %1",.t.,.f.,.f.,this.w_TSCODICE)
        * --- Messaggio sul Log
        this.w_STRINGA = ah_MsgFormat("%1Export sottoconto %2 %3","          ",this.w_TSCODICE,this.w_DESPIA)
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      else
        if this.w_TSTIPCON="C"
          * --- Messaggio a Video
          ah_Msg("Export cliente %1",.t.,.f.,.f.,this.w_TSCODICE)
          * --- Messaggio sul Log
          this.w_STRINGA = ah_MsgFormat("%1Export cliente %2 %3","          ",this.w_TSCODICE,this.w_TSDESPIA)
          FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        else
          * --- Messaggio a Video
          ah_Msg("Export fornitore %1",.t.,.f.,.f.,this.w_TSCODICE)
          * --- Messaggio sul Log
          this.w_STRINGA = ah_MsgFormat("%1Export fornitore %2 %3","          ",this.w_TSCODICE,this.w_TSDESPIA)
          FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        endif
      endif
      * --- Cod. Tipo Struttura
      FWRITE(this.hFile,"DETT",4)
      * --- Sezione di Bilancio
      FWRITE(this.hFile,ALLTRIM(this.w_TSSEZBIL),1)
      * --- Descrizione del Conto
      FWRITE(this.hFile,LEFT(this.w_TSDESPIA,30),30)
      * --- Dare e Avere
      FWRITE(this.hFile,RIGHT("0000000000000"+ALLTRIM(STR(this.w_TS__DARE*(10^this.w_DECIMALI),13,0)),13),13)
      FWRITE(this.hFile,RIGHT("0000000000000"+ALLTRIM(STR(this.w_TS_AVERE*(10^this.w_DECIMALI),13,0)),13),13)
      * --- Codice Conto di Origine
      FWRITE(this.hFile,RIGHT(repl(" ",20)+this.w_TSCODICE,20),20)
      * --- Codice sottoconte CONGE
      if EMPTY(this.w_ANCODSTU)
        if INLIST(UPPER(this.w_TSDESPIA),"CLIENTI","FORNITORI")
          this.w_CLIFOR = UPPER(SUBSTR(this.w_TSDESPIA,1,1))
          * --- Ricavo il minimo dei limiiti inferiori per i clienti o fornitori
          * --- Select from STUMPIAC
          i_nConn=i_TableProp[this.STUMPIAC_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STUMPIAC_idx,2],.t.,this.STUMPIAC_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select MIN(LMMINCON) AS MINCON  from "+i_cTable+" STUMPIAC ";
                +" where LMCONCLI = "+cp_ToStrODBC(this.w_CLIFOR)+"";
                +" group by LMCODCON";
                 ,"_Curs_STUMPIAC")
          else
            select MIN(LMMINCON) AS MINCON from (i_cTable);
             where LMCONCLI = this.w_CLIFOR;
             group by LMCODCON;
              into cursor _Curs_STUMPIAC
          endif
          if used('_Curs_STUMPIAC')
            select _Curs_STUMPIAC
            locate for 1=1
            do while not(eof())
            this.w_ESTRINF = _Curs_STUMPIAC.MINCON
              select _Curs_STUMPIAC
              continue
            enddo
            use
          endif
          FWRITE(this.hFile,RIGHT("000000"+ALLTRIM(this.w_ESTRINF),6),6)
        else
          FWRITE(this.hFile,"000000",6)
        endif
      else
        FWRITE(this.hFile,RIGHT("000000"+ALLTRIM(this.w_ANCODSTU),6),6)
      endif
      * --- Codice Centro di Costo
      FWRITE(this.hFile,"0000",4)
      * --- Filler
      FWRITE(this.hFile,repl(" ",9),9)
      * --- Ripristino Working Area
      SELECT STU_BILA
      skip
    enddo
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='STU_PARA'
    this.cWorkTables[4]='STUMPIAC'
    this.cWorkTables[5]='STU_PIAC'
    this.cWorkTables[6]='MASTRI'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_STUMPIAC')
      use in _Curs_STUMPIAC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
