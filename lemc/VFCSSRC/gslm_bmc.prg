* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_bmc                                                        *
*              Export movimenti contabili                                      *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_252]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-24                                                      *
* Last revis.: 2015-06-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pNoIva
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm_bmc",oParentObject,m.pNoIva)
return(i_retval)

define class tgslm_bmc as StdBatch
  * --- Local variables
  pNoIva = space(1)
  w_STRINGA = space(120)
  w_ACTUALPOS = 0
  w_PNSERIAL = space(10)
  w_TOTDOC = 0
  w_ANCODSTU = space(5)
  w_APRCAU = space(3)
  w_PNCODCAU = space(5)
  w_PNTIPCON = space(1)
  w_PNCODCON = space(15)
  w_NUMDETPERREG = 0
  w_DESCRIZIONE = space(29)
  w_CAUCAD = space(3)
  w_CAUDAC = space(3)
  w_PNCAURIG = space(5)
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_COD_DARE = space(1)
  w_COD_AVER = space(1)
  w_SERDACOR = .f.
  w_RECORD = space(2)
  w_NEWTIPO = space(2)
  w_TIPO = space(1)
  * --- WorkFile variables
  STU_PARA_idx=0
  CONTI_idx=0
  STU_TRAS_idx=0
  STU_PNTT_idx=0
  runtime_filters = 1

  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT MOVIMENTI CONTABILI
    *     pNoIva='S' Tutto
    *     pNoIva='N' Solo giroconti automatici
    * --- Cambiamento del formato delle date per rendere i cursori compatibili
    if this.pNoIva="S"
      * --- Se export anche prima nota senza IVA metto assieme queste registrazioni
      *     con i giroconti creati in automatico
       
 SELECT PNSERIAL, PNNUMRER, CP_TODATE(PNDATREG) AS PNDATREG, PNCODCAU, PNCOMPET, PNTIPDOC, ; 
 PNNUMDOC, PNALFDOC, CP_TODATE(PNDATDOC) AS PNDATDOC, PNNUMPRO, PNALFPRO, PNCODVAL, ; 
 PNVALNAZ, PNTOTDOC, PNTIPCLF, PNCODCLF, CP_TODATE(PNCOMIVA) AS PNCOMIVA, PNTIPREG, ; 
 PNNUMREG, PNCAURIG, PNDESRIG, PNTIPCON, PNIMPDAR, PNCODCON, PNIMPAVE, ANTIPSOT," " As Giroconto,PTSERRIF,ANFLRITE,TIPO ; 
 FROM MOCONTAB INTO CURSOR MOCONTAB NOFILTER
      * --- Union tra i movimenti contabili e quelli derivanti dai corrispettivi con scorporo
      if used("MOVDACOR")
        SELECT * FROM MOCONTAB UNION ALL SELECT * ,"*" As Giroconto FROM MOVDACOR ORDER BY 3,2 INTO CURSOR MOCONTAB NOFILTER
      endif
    else
      * --- Prendo solo i movimenti generati in automatico come giroconti
      if used("MOVDACOR")
        SELECT *, "*" As Giroconto FROM MOVDACOR ORDER BY 3,2 INTO CURSOR MOCONTAB NOFILTER
      else
        if NOT used("MOVDAFAT")
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    if used("MOCONTAB")
       
 SELECT MOCONTAB 
 Go Top
      * --- Ciclo sul cursore dei movimenti contabili
      do while NOT EOF()
        * --- Messaggio a schermo
        ah_Msg("Export movimenti contabili: num. %1 - data %2 %3" ,.T.,.F.,.F.,alltrim(STR(MOCONTAB.PNNUMRER,6,0)),dtoc(MOCONTAB.PNDATREG),MOCONTAB.Giroconto)
        * --- Scrittura su LOG
        this.w_STRINGA = space(10)+Ah_MsgFormat("Export movimenti contabili: num. %1 - data %2 %3",alltrim(STR(MOCONTAB.PNNUMRER,6,0)),dtoc(MOCONTAB.PNDATREG),MOCONTAB.Giroconto)
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        * --- Salvo la posizione attuale del record e il seriale della registrazione
        this.w_ACTUALPOS = RECNO()
        this.w_PNSERIAL = MOCONTAB.PNSERIAL
        this.w_TOTDOC = 0
        this.w_RECORD = TIPO
        * --- Conto il numero di dettagli per questa registrazione
        this.w_NUMDETPERREG = 0
        do while MOCONTAB.PNSERIAL=this.w_PNSERIAL
          this.w_NUMDETPERREG = this.w_NUMDETPERREG+1
          skip 1
        enddo
        * --- Ripristino posizione
        go this.w_ACTUALPOS
        * --- Seleziono la tipologia di registrazione contabile
        do case
          case this.w_NUMDETPERREG=1
            * --- Le registrazione derivanti dalle Fatture EUROPEE di Acquisto (con w_NUMDETPERREG=1)
            * --- sono trattate a pag. 4 (FATTEURO). Il cursore � MOVDAFAT (non MOCONTAB).
            * --- Passo al record successivo
            skip 1
          case this.w_NUMDETPERREG=2
            * --- Butto giu la registrazione standard
            if this.w_RECORD="50" or !isalt()
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              this.Page_5()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              skip 2
            endif
          case .T.
            * --- Se ho pi� righe di dettaglio, allora � una registrazione complessa Studio
            do while MOCONTAB.PNSERIAL=this.w_PNSERIAL
              if this.w_RECORD="50" or !isalt()
                this.Page_2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              else
                this.Page_5()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
              * --- Decremento w_NUMDETPERREG
              this.w_NUMDETPERREG = this.w_NUMDETPERREG-1
              * --- Avanzo il puntatore
              skip 1
            enddo
        endcase
        this.w_SERDACOR = .F.
        if used("MOVDACOR")
          SELECT MOVDACOR
          LOCATE FOR PNSERIAL=this.w_PNSERIAL
          if FOUND()
            this.w_SERDACOR = .T.
          endif
        endif
        if this.w_SERDACOR
          * --- Movimento Contabile da Corrispettivi
          * --- Try
          local bErr_0286F6B0
          bErr_0286F6B0=bTrsErr
          this.Try_0286F6B0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- Essendo w_PROFIL=numero del trasferimento
            * --- (uguale a quanto inserito dalle routine GSLM_BFA relativo alle fatture UE in caso di successo)
            * --- (in caso di insuccesso il valore inserito dalla routine GSLM_BFA � -1)
            * --- Non bisogna scrivere niente
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_0286F6B0
          * --- End
        else
          * --- Movimento Contabile senza IVA
          * --- Try
          local bErr_0286F3B0
          bErr_0286F3B0=bTrsErr
          this.Try_0286F3B0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- Se � gi� presente (per l'esportazione del moviemto contabile CON IVA) aggiorno solo il campo del progressivo NO IVA
            * --- accept error
            bTrsErr=.f.
            * --- Write into STU_PNTT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.STU_PNTT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_PNTT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"LMNUMTR2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'STU_PNTT','LMNUMTR2');
                  +i_ccchkf ;
              +" where ";
                  +"LMSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                     )
            else
              update (i_cTable) set;
                  LMNUMTR2 = this.oParentObject.w_PROFIL;
                  &i_ccchkf. ;
               where;
                  LMSERIAL = this.w_PNSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='GSLM_BMC: Scrittura in STU_PNTT'
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_0286F3B0
          * --- End
        endif
        SELECT MOCONTAB
      enddo
    endif
    this.w_STRINGA = "(*) Movimenti di giroconto automatici"
    FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
    FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
    if used("MOVDACOR")
      SELECT MOVDACOR
      USE
    endif
    if used("MOVDAFAT")
      SELECT MOVDAFAT
      GO TOP
      * --- Ciclo sul cursore dei movimenti contabili derivanti da fatture di acquisto europee
      do while NOT EOF()
        * --- Messaggio a schermo
        ah_Msg("Export movimenti contabili: num. %1 - data %2",.T.,.F.,.F.,alltrim(STR(MOVDAFAT.PNNUMRER,6,0)),dtoc(MOVDAFAT.PNDATREG))
        * --- Scrittura su LOG
        this.w_STRINGA = space(10)+Ah_MsgFormat("Export movimenti contabili: num. %1 - data %2",alltrim(STR(MOVDAFAT.PNNUMRER,6,0)),dtoc(MOVDAFAT.PNDATREG))
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.w_TOTDOC = 0
        * --- Butto giu la registrazione derivante dalle Fatture EUROPEE di Acquisto
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Try
        local bErr_028704C0
        bErr_028704C0=bTrsErr
        this.Try_028704C0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- Essendo w_PROFIL=numero del trasferimento
          * --- (uguale a quanto inserito dalle routine GSLM_BFA relativo alle fatture UE in caso di successo)
          * --- (in caso di insuccesso il valore inserito dalla routine GSLM_BFA � -1)
          * --- Non bisogna scrivere niente
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_028704C0
        * --- End
        SELECT MOVDAFAT
      enddo
      USE
    endif
  endproc
  proc Try_0286F6B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_PNTT
    i_nConn=i_TableProp[this.STU_PNTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMSERIAL"+",LMNUMTRA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'STU_PNTT','LMNUMTRA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.oParentObject.w_PROFIL)
      insert into (i_cTable) (LMSERIAL,LMNUMTRA &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.oParentObject.w_PROFIL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='GSLM_BMC: Scrittura in STU_PNTT'
      return
    endif
    return
  proc Try_0286F3B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Provo ad inserire nella tabella temporanea il progressivo per il movimento contabile NO IVA
    * --- Insert into STU_PNTT
    i_nConn=i_TableProp[this.STU_PNTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTRA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'STU_PNTT','LMNUMTR2');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',0,'LMNUMTR2',this.oParentObject.w_PROFIL)
      insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,0;
           ,this.oParentObject.w_PROFIL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='GSLM_BMC: Scrittura in STU_PNTT'
      return
    endif
    return
  proc Try_028704C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_PNTT
    i_nConn=i_TableProp[this.STU_PNTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMSERIAL"+",LMNUMTRA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'STU_PNTT','LMNUMTRA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.oParentObject.w_PROFIL)
      insert into (i_cTable) (LMSERIAL,LMNUMTRA &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.oParentObject.w_PROFIL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='GSLM_BMC: Scrittura in STU_PNTT'
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Incremento numero di record scritti
    this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
    this.oParentObject.w_MOCONTAB = this.oParentObject.w_MOCONTAB+1
    * --- Tipo record
    FWRITE(this.oParentObject.hFile,"D50",3)
    * --- Data registrazione
    FWRITE(this.oParentObject.hFile,dtos(MOCONTAB.PNDATREG),8)
    * --- Recupero descrizione
    this.w_DESCRIZIONE = MOCONTAB.PNDESRIG
    if EMPTY(MOCONTAB.PNDESRIG)
      this.w_DESCRIZIONE = LOOKTAB("CAU_CONT","CCDESCRI","CCCODICE",MOCONTAB.PNCAURIG)
    endif
    * --- Recupero codice causale dello studio - Se riga in DARE --> Conto a Diversi
    * --- Recupero codice causale dello studio - Se riga in AVERE --> Diversi a conto
    this.w_CAUCAD = SPACE(3)
    this.w_CAUDAC = SPACE(3)
    * --- Read from STU_PARA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.STU_PARA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PARA_idx,2],.t.,this.STU_PARA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LMCAUDAC,LMCAUCAD"+;
        " from "+i_cTable+" STU_PARA where ";
            +"LMCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LMCAUDAC,LMCAUCAD;
        from (i_cTable) where;
            LMCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CAUDAC = NVL(cp_ToDate(_read_.LMCAUDAC),cp_NullValue(_read_.LMCAUDAC))
      this.w_CAUCAD = NVL(cp_ToDate(_read_.LMCAUCAD),cp_NullValue(_read_.LMCAUCAD))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT MOCONTAB
    * --- Controllo correttezza causali
    if EMPTY(this.w_CAUDAC) AND MOCONTAB.PNIMPDAR=0
      this.w_STRINGA = Ah_MsgFormat("%1Errore! Causale diversi a conto non definita nella tabella trasferimento studio","               ")
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      this.oParentObject.w_ERRORE = .T.
    endif
    if EMPTY(this.w_CAUCAD) AND MOCONTAB.PNIMPDAR<>0
      this.w_STRINGA = Ah_MsgFormat("%1Errore! Causale conto a diversi non definita nella tabella trasferimento studio","               ")
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      this.oParentObject.w_ERRORE = .T.
    endif
    * --- Scrittura del Codice Causale
    if MOCONTAB.PNIMPDAR=0
      FWRITE(this.oParentObject.hFile,RIGHT("   "+alltrim(this.w_CAUDAC),3),3)
    else
      FWRITE(this.oParentObject.hFile,RIGHT("   "+alltrim(this.w_CAUCAD),3),3)
    endif
    * --- Descrizione
    FWRITE(this.oParentObject.hFile,nvl(this.w_DESCRIZIONE,"")+repl(" ",29),29)
    * --- Importo
    this.w_TOTDOC = MOCONTAB.PNIMPDAR+MOCONTAB.PNIMPAVE
    if g_PERVAL=this.oParentObject.w_VALEUR
      FWRITE(this.oParentObject.hFile,RIGHT("0000000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOC)),13,0))+RIGHT(STR(this.w_TOTDOC,14,2),2),13),13)
    else
      FWRITE(this.oParentObject.hFile,RIGHT("0000000000000"+ALLTRIM(STR(ABS(this.w_TOTDOC),13,0)),13),13)
    endif
    * --- Segno
    FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOC>=0, "+", "-"),1)
    * --- Centro di costo e Conto DARE
    if MOCONTAB.PNIMPDAR<>0
      * --- Centro di costo
      FWRITE(this.oParentObject.hFile,"00",2)
      * --- Conto DARE
      this.w_PNTIPCON = MOCONTAB.PNTIPCON
      this.w_COD_DARE = this.w_PNTIPCON
      this.w_PNCODCON = MOCONTAB.PNCODCON
      this.w_ANCODSTU = SPACE(5)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCON;
              and ANCODICE = this.w_PNCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT MOCONTAB
      if EMPTY(this.w_ANCODSTU)
        this.w_ANCODSTU = "00000"
        do case
          case this.w_PNTIPCON="C"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Cliente %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          case this.w_PNTIPCON="F"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Fornitore %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          otherwise
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        endcase
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      * --- Scrivo il Conto sul file
      FWRITE(this.oParentObject.hFile,this.w_ANCODSTU,5)
    else
      * --- Centro di costo e Conto DARE sono empty
      FWRITE(this.oParentObject.hFile,"0000000",7)
      this.w_COD_DARE = "G"
    endif
    * --- Centro di costo e Conto AVERE
    if MOCONTAB.PNIMPDAR=0
      * --- Centro di costo
      FWRITE(this.oParentObject.hFile,"00",2)
      * --- Conto AVERE
      this.w_PNTIPCON = MOCONTAB.PNTIPCON
      this.w_COD_AVER = this.w_PNTIPCON
      this.w_PNCODCON = MOCONTAB.PNCODCON
      this.w_ANCODSTU = SPACE(5)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCON;
              and ANCODICE = this.w_PNCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT MOCONTAB
      if EMPTY(this.w_ANCODSTU)
        this.w_ANCODSTU = "00000"
        do case
          case this.w_PNTIPCON="C"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Cliente %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          case this.w_PNTIPCON="F"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Fornitore %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          otherwise
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        endcase
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      * --- Scrivo il Conto sul file
      FWRITE(this.oParentObject.hFile,this.w_ANCODSTU,5)
    else
      * --- Centro di costo e sottoconto DARE sono empty
      FWRITE(this.oParentObject.hFile,"0000000",7)
      this.w_COD_AVER = "G"
    endif
    * --- Tipo Codifica e Partita IVA DARE
    FWRITE(this.oParentObject.hFile,IIF(this.w_COD_DARE="C","P",IIF(this.w_COD_DARE="F","R"," ")),1)
    FWRITE(this.oParentObject.hFile,"00000000000",11)
    * --- Tipo Codifica e Partita IVA AVERE
    FWRITE(this.oParentObject.hFile,IIF(this.w_COD_AVER="C","P",IIF(this.w_COD_AVER="F","R"," ")),1)
    FWRITE(this.oParentObject.hFile,"00000000000",11)
    * --- FLAG DIVERSI DI REGISTRAZIONI COMPLESSE
    * --- Controllo se il record successivo appartiene ancora a questa registrazione
    if this.w_NUMDETPERREG=1
      FWRITE(this.oParentObject.hFile,"X",1)
    else
      FWRITE(this.oParentObject.hFile," ",1)
    endif
    * --- Recupero valuta di conto
    FWRITE(this.oParentObject.hFile , IIF(g_PERVAL=this.oParentObject.w_VALEUR, "E", " ") , 1)
    * --- FILLER
    this.oParentObject.w_FILLER = SPACE(103)
    FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,103)
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Movimenti di primanota standard
    this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
    this.oParentObject.w_MOCONTAB = this.oParentObject.w_MOCONTAB+1
    * --- Tipo record
    FWRITE(this.oParentObject.hFile,"D50",3)
    * --- Data registrazione
    FWRITE(this.oParentObject.hFile,dtos(MOCONTAB.PNDATREG),8)
    * --- Recupero descrizione
    this.w_DESCRIZIONE = MOCONTAB.PNDESRIG
    if EMPTY(MOCONTAB.PNDESRIG)
      this.w_DESCRIZIONE = LOOKTAB("CAU_CONT","CCDESCRI","CCCODICE",MOCONTAB.PNCAURIG)
    endif
    * --- Recupero codice causale dello studio
    this.w_APRCAU = SPACE(3)
    this.w_PNCAURIG = MOCONTAB.PNCAURIG
    * --- Read from STU_TRAS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.STU_TRAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LMAPRCAU"+;
        " from "+i_cTable+" STU_TRAS where ";
            +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
            +" and LMHOCCAU = "+cp_ToStrODBC(this.w_PNCAURIG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LMAPRCAU;
        from (i_cTable) where;
            LMCODICE = this.oParentObject.w_ASSOCI;
            and LMHOCCAU = this.w_PNCAURIG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_APRCAU = NVL(cp_ToDate(_read_.LMAPRCAU),cp_NullValue(_read_.LMAPRCAU))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT MOCONTAB
    if EMPTY(this.w_APRCAU)
      this.w_APRCAU = "000"
      * --- Non ho trovato associato il codice causale
      this.w_STRINGA = Ah_MsgFormat("%1Errore! Causale %2 non associata nello studio","               ",NVL(this.w_PNCAURIG, " "))
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      this.oParentObject.w_ERRORE = .T.
    endif
    FWRITE(this.oParentObject.hFile,this.w_APRCAU,3)
    * --- Descrizione
    FWRITE(this.oParentObject.hFile,nvl(this.w_DESCRIZIONE,"")+repl(" ",29),29)
    * --- Importo
    this.w_TOTDOC = MOCONTAB.PNIMPDAR+MOCONTAB.PNIMPAVE
    if g_PERVAL=this.oParentObject.w_VALEUR
      FWRITE(this.oParentObject.hFile,RIGHT("0000000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOC)),13,0))+RIGHT(STR(this.w_TOTDOC,14,2),2),13),13)
    else
      FWRITE(this.oParentObject.hFile,RIGHT("0000000000000"+ALLTRIM(STR(ABS(this.w_TOTDOC),13,0)),13),13)
    endif
    * --- Segno
    FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOC>=0, "+", "-"),1)
    if MOCONTAB.PNIMPDAR=0
      * --- Avanzo al record successivo
      skip 1
      * --- Centro di costo dare
      FWRITE(this.oParentObject.hFile,"00",2)
      * --- Conto dare
      this.w_PNTIPCON = MOCONTAB.PNTIPCON
      this.w_COD_DARE = this.w_PNTIPCON
      this.w_PNCODCON = MOCONTAB.PNCODCON
      this.w_ANCODSTU = SPACE(5)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCON;
              and ANCODICE = this.w_PNCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT MOCONTAB
      if EMPTY(this.w_ANCODSTU)
        this.w_ANCODSTU = "00000"
        do case
          case this.w_PNTIPCON="C"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Cliente %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          case this.w_PNTIPCON="F"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Fornitore %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          otherwise
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        endcase
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      * --- Scrivo il Conto sul file
      FWRITE(this.oParentObject.hFile,this.w_ANCODSTU,5)
      * --- Torno al record precedente
      skip -1
      * --- Centro di costo avere
      FWRITE(this.oParentObject.hFile,"00",2)
      * --- Conto avere
      this.w_PNTIPCON = MOCONTAB.PNTIPCON
      this.w_COD_AVER = this.w_PNTIPCON
      this.w_PNCODCON = MOCONTAB.PNCODCON
      this.w_ANCODSTU = SPACE(5)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCON;
              and ANCODICE = this.w_PNCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT MOCONTAB
      if EMPTY(this.w_ANCODSTU)
        this.w_ANCODSTU = "00000"
        do case
          case this.w_PNTIPCON="C"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Cliente %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          case this.w_PNTIPCON="F"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Fornitore %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          otherwise
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        endcase
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      * --- Scrivo il Conto sul file
      FWRITE(this.oParentObject.hFile,this.w_ANCODSTU,5)
      * --- Tipo codifica dare
      FWRITE(this.oParentObject.hFile,IIF(this.w_COD_DARE="C","P",IIF(this.w_COD_DARE="F","R"," ")),1)
      * --- Partita IVA dare
      FWRITE(this.oParentObject.hFile,"00000000000",11)
      * --- Tipo codifica avere
      FWRITE(this.oParentObject.hFile,IIF(this.w_COD_AVER="C","P",IIF(this.w_COD_AVER="F","R"," ")),1)
      * --- Partita IVA avere
      FWRITE(this.oParentObject.hFile,"00000000000",11)
      * --- Flag diversi
      FWRITE(this.oParentObject.hFile," ",1)
      * --- Recupero valuta di conto
      FWRITE(this.oParentObject.hFile , IIF(g_PERVAL=this.oParentObject.w_VALEUR, "E", " ") , 1)
      * --- FILLER
      this.oParentObject.w_FILLER = SPACE(103)
      FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,103)
    else
      * --- Centro di costo dare
      FWRITE(this.oParentObject.hFile,"00",2)
      * --- Conto dare
      this.w_PNTIPCON = MOCONTAB.PNTIPCON
      this.w_COD_DARE = this.w_PNTIPCON
      this.w_PNCODCON = MOCONTAB.PNCODCON
      this.w_ANCODSTU = SPACE(5)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCON;
              and ANCODICE = this.w_PNCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT MOCONTAB
      if EMPTY(this.w_ANCODSTU)
        this.w_ANCODSTU = "00000"
        do case
          case this.w_PNTIPCON="C"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Cliente %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          case this.w_PNTIPCON="F"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Fornitore %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          otherwise
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        endcase
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      * --- Scrivo il Conto sul file
      FWRITE(this.oParentObject.hFile,this.w_ANCODSTU,5)
      * --- Avanzo al record successivo
      skip 1
      * --- Centro di costo avere
      FWRITE(this.oParentObject.hFile,"00",2)
      * --- Conto avere
      this.w_PNTIPCON = MOCONTAB.PNTIPCON
      this.w_COD_AVER = this.w_PNTIPCON
      this.w_PNCODCON = MOCONTAB.PNCODCON
      this.w_ANCODSTU = SPACE(5)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCON;
              and ANCODICE = this.w_PNCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT MOCONTAB
      if EMPTY(this.w_ANCODSTU)
        this.w_ANCODSTU = "00000"
        do case
          case this.w_PNTIPCON="C"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Cliente %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          case this.w_PNTIPCON="F"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Fornitore %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          otherwise
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        endcase
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      * --- Scrivo il Conto sul file
      FWRITE(this.oParentObject.hFile,this.w_ANCODSTU,5)
      * --- Tipo codifica dare
      FWRITE(this.oParentObject.hFile,IIF(this.w_COD_DARE="C","P",IIF(this.w_COD_DARE="F","R"," ")),1)
      * --- Partita IVA dare
      FWRITE(this.oParentObject.hFile,"00000000000",11)
      * --- Tipo codifica avere
      FWRITE(this.oParentObject.hFile,IIF(this.w_COD_AVER="C","P",IIF(this.w_COD_AVER="F","R"," ")),1)
      * --- Partita IVA avere
      FWRITE(this.oParentObject.hFile,"00000000000",11)
      * --- Flag diversi
      FWRITE(this.oParentObject.hFile," ",1)
      * --- Recupero valuta di conto
      FWRITE(this.oParentObject.hFile , IIF(g_PERVAL=this.oParentObject.w_VALEUR, "E", " ") , 1)
      * --- FILLER
      this.oParentObject.w_FILLER = SPACE(103)
      FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,103)
    endif
    * --- Torno al record originale
    go this.w_ACTUALPOS
    skip 2
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- SINGOLA RIGA DERIVANTE DAI CORRISPETTIVI CON SCORPORO
    this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
    this.oParentObject.w_MOCONTAB = this.oParentObject.w_MOCONTAB+1
    * --- Tipo record
    FWRITE(this.oParentObject.hFile,"D50",3)
    * --- Data registrazione
    FWRITE(this.oParentObject.hFile,dtos(MOVDAFAT.PNDATREG),8)
    * --- Recupero descrizione
    this.w_DESCRIZIONE = MOVDAFAT.PNDESRIG
    if EMPTY(MOVDAFAT.PNDESRIG)
      this.w_DESCRIZIONE = LOOKTAB("CAU_CONT","CCDESCRI","CCCODICE",MOVDAFAT.PNCAURIG)
    endif
    * --- Recupero codice causale dello studio
    this.w_APRCAU = SPACE(3)
    this.w_PNCAURIG = MOVDAFAT.PNCAURIG
    * --- Read from STU_TRAS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.STU_TRAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LMAPRCAU"+;
        " from "+i_cTable+" STU_TRAS where ";
            +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
            +" and LMHOCCAU = "+cp_ToStrODBC(this.w_PNCAURIG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LMAPRCAU;
        from (i_cTable) where;
            LMCODICE = this.oParentObject.w_ASSOCI;
            and LMHOCCAU = this.w_PNCAURIG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_APRCAU = NVL(cp_ToDate(_read_.LMAPRCAU),cp_NullValue(_read_.LMAPRCAU))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT MOVDAFAT
    if EMPTY(this.w_APRCAU)
      this.w_APRCAU = "000"
      * --- Non ho trovato associato il codice causale
      this.w_STRINGA = Ah_MsgFormat("%1Errore! Causale %2 non associata nello studio","               ",NVL(this.w_PNCAURIG, " "))
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      this.oParentObject.w_ERRORE = .T.
    endif
    FWRITE(this.oParentObject.hFile,this.w_APRCAU,3)
    * --- Descrizione
    FWRITE(this.oParentObject.hFile,nvl(this.w_DESCRIZIONE,"")+repl(" ",29),29)
    * --- Importo
    this.w_TOTDOC = MOVDAFAT.PNIMPDAR+MOVDAFAT.PNIMPAVE
    if g_PERVAL=this.oParentObject.w_VALEUR
      FWRITE(this.oParentObject.hFile,RIGHT("0000000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOC)),13,0))+RIGHT(STR(this.w_TOTDOC,14,2),2),13),13)
    else
      FWRITE(this.oParentObject.hFile,RIGHT("0000000000000"+ALLTRIM(STR(ABS(this.w_TOTDOC),13,0)),13),13)
    endif
    * --- Segno
    FWRITE(this.oParentObject.hFile,MOVDAFAT.SEGNO,1)
    * --- Centro di costo dare
    FWRITE(this.oParentObject.hFile,"00",2)
    * --- Conto dare
    this.w_PNTIPCLF = MOVDAFAT.PNTIPCLF
    this.w_COD_DARE = this.w_PNTIPCLF
    this.w_PNCODCLF = MOVDAFAT.PNCODCLF
    this.w_ANCODSTU = SPACE(5)
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANCODSTU"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANCODSTU;
        from (i_cTable) where;
            ANTIPCON = this.w_PNTIPCLF;
            and ANCODICE = this.w_PNCODCLF;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT MOVDAFAT
    if EMPTY(this.w_ANCODSTU)
      this.w_ANCODSTU = "00000"
      do case
        case this.w_PNTIPCON="C"
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Cliente %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        case this.w_PNTIPCON="F"
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Fornitore %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        otherwise
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
      endcase
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      this.oParentObject.w_ERRORE = .T.
    endif
    FWRITE(this.oParentObject.hFile,this.w_ANCODSTU,5)
    * --- Centro di costo avere
    FWRITE(this.oParentObject.hFile,"00",2)
    * --- Conto Avere
    this.w_PNTIPCON = MOVDAFAT.PNTIPCON
    this.w_COD_AVER = this.w_PNTIPCON
    this.w_PNCODCON = MOVDAFAT.PNCODCON
    this.w_ANCODSTU = SPACE(5)
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANCODSTU"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
            +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANCODSTU;
        from (i_cTable) where;
            ANTIPCON = this.w_PNTIPCON;
            and ANCODICE = this.w_PNCODCON;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT MOVDAFAT
    if EMPTY(this.w_ANCODSTU)
      this.w_ANCODSTU = "00000"
      do case
        case this.w_PNTIPCON="C"
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Cliente %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        case this.w_PNTIPCON="F"
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Fornitore %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        otherwise
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
      endcase
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      this.oParentObject.w_ERRORE = .T.
    endif
    FWRITE(this.oParentObject.hFile,this.w_ANCODSTU,5)
    * --- Tipo codifica dare
    FWRITE(this.oParentObject.hFile,IIF(this.w_COD_DARE="C","P",IIF(this.w_COD_DARE="F","R"," ")),1)
    * --- Partita IVA dare
    FWRITE(this.oParentObject.hFile,"00000000000",11)
    * --- Tipo codifica avere
    FWRITE(this.oParentObject.hFile,IIF(this.w_COD_AVER="C","P",IIF(this.w_COD_AVER="F","R"," ")),1)
    * --- Partita IVA avere
    FWRITE(this.oParentObject.hFile,"00000000000",11)
    * --- Flag diversi
    FWRITE(this.oParentObject.hFile," ",1)
    * --- Recupero valuta di conto
    FWRITE(this.oParentObject.hFile , IIF(g_PERVAL=this.oParentObject.w_VALEUR, "E", " ") , 1)
    * --- FILLER
    this.oParentObject.w_FILLER = SPACE(103)
    FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,103)
    * --- Avanzo al record successivo
    skip 1
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Incremento numero di record scritti
    this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
    this.oParentObject.w_MOCONTAB = this.oParentObject.w_MOCONTAB+1
    this.w_NEWTIPO = MOCONTAB.TIPO
    if this.oParentObject.w_ORIGINETIPO<>this.w_NEWTIPO
      * --- Scrivo la coda le vecchio tipo
      this.oParentObject.w_MOCONTAB = this.oParentObject.w_MOCONTAB+1
      FWRITE (this.oParentObject.hFile , "EMOCONTAB" , 9 )
      FWRITE(this.oParentObject.hFile,RIGHT("00000000"+ALLTRIM(STR(this.oParentObject.w_MOCONTAB-2)),8),8)
      this.w_FILLER = SPACE(183)
      FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,183)
      * --- Incremento numero di record scritti
      this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
      this.w_FILLER = SPACE(191)
      FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,191)
      * --- Incremento numero di record scritti
      this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
      this.oParentObject.w_MOCONTAB = this.oParentObject.w_MOCONTAB+1
      * --- Scrivo la testata del nuovo tipo
      FWRITE (this.oParentObject.hFile , "FIPPROORD" , 9 )
      this.w_FILLER = SPACE(191)
      FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,191)
      * --- Incremento numero di record scritti
      this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
      this.oParentObject.w_MOCONTAB = this.oParentObject.w_MOCONTAB+1
    endif
    * --- Tipo record
    FWRITE(this.oParentObject.hFile,"D70",3)
    * --- Codice Mittente
    FWRITE(this.oParentObject.hFile,"AHREV",5)
    * --- Codice Chiave
    FWRITE(this.oParentObject.hFile,nvl(MOCONTAB.PTSERRIF,"")+repl(" ",13),13)
    * --- Data registrazione
    FWRITE(this.oParentObject.hFile,dtos(MOCONTAB.PNDATREG),8)
    * --- Codice tipo incassi/pagamenti
    do case
      case MOCONTAB.PNTIPCON="C"
        this.w_TIPO = "L"
      case MOCONTAB.PNTIPCON="F"
        this.w_TIPO = "P"
    endcase
    FWRITE(this.oParentObject.hFile,this.w_TIPO,1)
    * --- Recupero descrizione
    this.w_DESCRIZIONE = NVL(MOCONTAB.PNDESRIG,"")
    if EMPTY(NVL(MOCONTAB.PNDESRIG,""))
      this.w_DESCRIZIONE = LOOKTAB("CAU_CONT","CCDESCRI","CCCODICE",MOCONTAB.PNCAURIG)
    endif
    * --- Recupero codice causale dello studio
    this.w_APRCAU = SPACE(3)
    this.w_PNCAURIG = MOCONTAB.PNCAURIG
    * --- Read from STU_TRAS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.STU_TRAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LMAPRCAU"+;
        " from "+i_cTable+" STU_TRAS where ";
            +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
            +" and LMHOCCAU = "+cp_ToStrODBC(this.w_PNCAURIG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LMAPRCAU;
        from (i_cTable) where;
            LMCODICE = this.oParentObject.w_ASSOCI;
            and LMHOCCAU = this.w_PNCAURIG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_APRCAU = NVL(cp_ToDate(_read_.LMAPRCAU),cp_NullValue(_read_.LMAPRCAU))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT MOCONTAB
    if EMPTY(this.w_APRCAU)
      this.w_APRCAU = "000"
      * --- Non ho trovato associato il codice causale
      this.w_STRINGA = Ah_MsgFormat("%1Errore! Causale %2 non associata nello studio","               ",NVL(this.w_PNCAURIG, " "))
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      this.oParentObject.w_ERRORE = .T.
    endif
    FWRITE(this.oParentObject.hFile,this.w_APRCAU,3)
    * --- Descrizione
    FWRITE(this.oParentObject.hFile,nvl(this.w_DESCRIZIONE,"")+repl(" ",26),26)
    * --- Importo
    this.w_TOTDOC = MOCONTAB.PNIMPDAR+MOCONTAB.PNIMPAVE
    if g_PERVAL=this.oParentObject.w_VALEUR
      FWRITE(this.oParentObject.hFile,RIGHT("0000000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOC)),13,0))+RIGHT(STR(this.w_TOTDOC,14,2),2),13),13)
    else
      FWRITE(this.oParentObject.hFile,RIGHT("0000000000000"+ALLTRIM(STR(ABS(this.w_TOTDOC),13,0)),13),13)
    endif
    if MOCONTAB.PNIMPDAR=0
      * --- Avanzo al record successivo
      skip 1
      * --- Centro di costo dare
      FWRITE(this.oParentObject.hFile,"00",2)
      * --- Conto dare
      this.w_PNTIPCON = MOCONTAB.PNTIPCON
      this.w_COD_DARE = this.w_PNTIPCON
      this.w_PNCODCON = MOCONTAB.PNCODCON
      this.w_ANCODSTU = SPACE(5)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCON;
              and ANCODICE = this.w_PNCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT MOCONTAB
      if EMPTY(this.w_ANCODSTU)
        this.w_ANCODSTU = "00000"
        do case
          case this.w_PNTIPCON="C"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Cliente %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          case this.w_PNTIPCON="F"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Fornitore %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          otherwise
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        endcase
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      * --- Scrivo il Conto sul file
      FWRITE(this.oParentObject.hFile,this.w_ANCODSTU,5)
      * --- Torno al record precedente
      skip -1
      * --- Centro di costo avere
      FWRITE(this.oParentObject.hFile,"00",2)
      * --- Conto avere
      this.w_PNTIPCON = MOCONTAB.PNTIPCON
      this.w_COD_AVER = this.w_PNTIPCON
      this.w_PNCODCON = MOCONTAB.PNCODCON
      this.w_ANCODSTU = SPACE(5)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCON;
              and ANCODICE = this.w_PNCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT MOCONTAB
      if EMPTY(this.w_ANCODSTU)
        this.w_ANCODSTU = "00000"
        do case
          case this.w_PNTIPCON="C"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Cliente %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          case this.w_PNTIPCON="F"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Fornitore %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          otherwise
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        endcase
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      * --- Scrivo il Conto sul file
      FWRITE(this.oParentObject.hFile,this.w_ANCODSTU,5)
      * --- Tipo codifica dare
      FWRITE(this.oParentObject.hFile,IIF(this.w_COD_DARE="C","P",IIF(this.w_COD_DARE="F","R"," ")),1)
      * --- Partita IVA dare
      FWRITE(this.oParentObject.hFile,"00000000000",11)
      * --- Tipo codifica avere
      FWRITE(this.oParentObject.hFile,IIF(this.w_COD_AVER="C","P",IIF(this.w_COD_AVER="F","R"," ")),1)
      * --- Partita IVA avere
      FWRITE(this.oParentObject.hFile,"00000000000",11)
      * --- Flag diversi
      FWRITE(this.oParentObject.hFile," ",1)
      * --- Recupero valuta di conto
      FWRITE(this.oParentObject.hFile , IIF(g_PERVAL=this.oParentObject.w_VALEUR, "E", " ") , 1)
    else
      * --- Centro di costo dare
      FWRITE(this.oParentObject.hFile,"00",2)
      * --- Conto dare
      this.w_PNTIPCON = MOCONTAB.PNTIPCON
      this.w_COD_DARE = this.w_PNTIPCON
      this.w_PNCODCON = MOCONTAB.PNCODCON
      this.w_ANCODSTU = SPACE(5)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCON;
              and ANCODICE = this.w_PNCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT MOCONTAB
      if EMPTY(this.w_ANCODSTU)
        this.w_ANCODSTU = "00000"
        do case
          case this.w_PNTIPCON="C"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Cliente %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          case this.w_PNTIPCON="F"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Fornitore %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          otherwise
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        endcase
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      * --- Scrivo il Conto sul file
      FWRITE(this.oParentObject.hFile,this.w_ANCODSTU,5)
      * --- Avanzo al record successivo
      skip 1
      * --- Centro di costo avere
      FWRITE(this.oParentObject.hFile,"00",2)
      * --- Conto avere
      this.w_PNTIPCON = MOCONTAB.PNTIPCON
      this.w_COD_AVER = this.w_PNTIPCON
      this.w_PNCODCON = MOCONTAB.PNCODCON
      this.w_ANCODSTU = SPACE(5)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCON;
              and ANCODICE = this.w_PNCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT MOCONTAB
      if EMPTY(this.w_ANCODSTU)
        this.w_ANCODSTU = "00000"
        do case
          case this.w_PNTIPCON="C"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Cliente %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          case this.w_PNTIPCON="F"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Fornitore %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          otherwise
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        endcase
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      * --- Scrivo il Conto sul file
      FWRITE(this.oParentObject.hFile,this.w_ANCODSTU,5)
      * --- Tipo codifica dare
      FWRITE(this.oParentObject.hFile,IIF(this.w_COD_DARE="C","P",IIF(this.w_COD_DARE="F","R"," ")),1)
      * --- Partita IVA dare
      FWRITE(this.oParentObject.hFile,"00000000000",11)
      * --- Tipo codifica avere
      FWRITE(this.oParentObject.hFile,IIF(this.w_COD_AVER="C","P",IIF(this.w_COD_AVER="F","R"," ")),1)
      * --- Partita IVA avere
      FWRITE(this.oParentObject.hFile,"00000000000",11)
      * --- Flag diversi
      FWRITE(this.oParentObject.hFile," ",1)
      * --- Recupero valuta di conto
      FWRITE(this.oParentObject.hFile , IIF(g_PERVAL=this.oParentObject.w_VALEUR, "E", " ") , 1)
    endif
    * --- Importo al lordo della ritenuta
    if this.w_TIPO="L"
      this.w_TOTDOC = MOCONTAB.PNIMPDAR+MOCONTAB.PNIMPAVE
      if g_PERVAL=this.oParentObject.w_VALEUR
        FWRITE(this.oParentObject.hFile,RIGHT("0000000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOC)),13,0))+RIGHT(STR(this.w_TOTDOC,14,2),2),13),13)
      else
        FWRITE(this.oParentObject.hFile,RIGHT("0000000000000"+ALLTRIM(STR(ABS(this.w_TOTDOC),13,0)),13),13)
      endif
    endif
    * --- FILLER
    this.oParentObject.w_FILLER = SPACE(88)
    FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,88)
  endproc

  proc Pag1()
      this.Page_5()      
  endproc

  proc Init(oParentObject,pNoIva)
    this.pNoIva=pNoIva
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='STU_PARA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='STU_TRAS'
    this.cWorkTables[4]='STU_PNTT'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pNoIva"
endproc
