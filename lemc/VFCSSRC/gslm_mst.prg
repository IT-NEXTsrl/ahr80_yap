* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_mst                                                        *
*              Piano dei conti studio                                          *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_70]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-14                                                      *
* Last revis.: 2015-05-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gslm_mst
* Esco se il modulo non � attivo
IF LMDISACTIVATE()
  do cplu_erm with "La funzione � attiva con il Modulo in modalit� Semplificata o Avanzata"
  return
ENDIF
* --- Fine Area Manuale
return(createobject("tgslm_mst"))

* --- Class definition
define class tgslm_mst as StdTrsForm
  Top    = 5
  Left   = 45

  * --- Standard Properties
  Width  = 785
  Height = 404+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-05-18"
  HelpContextID=97107049
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=19

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  STUMPIAC_IDX = 0
  STU_PIAC_IDX = 0
  STU_PARA_IDX = 0
  cFile = "STUMPIAC"
  cFileDetail = "STU_PIAC"
  cKeySelect = "LMCODCON,LMPROGRE"
  cKeyWhere  = "LMCODCON=this.w_LMCODCON and LMPROGRE=this.w_LMPROGRE"
  cKeyDetail  = "LMCODCON=this.w_LMCODCON and LMPROGRE=this.w_LMPROGRE and LMCODSOT=this.w_LMCODSOT"
  cKeyWhereODBC = '"LMCODCON="+cp_ToStrODBC(this.w_LMCODCON)';
      +'+" and LMPROGRE="+cp_ToStrODBC(this.w_LMPROGRE)';

  cKeyDetailWhereODBC = '"LMCODCON="+cp_ToStrODBC(this.w_LMCODCON)';
      +'+" and LMPROGRE="+cp_ToStrODBC(this.w_LMPROGRE)';
      +'+" and LMCODSOT="+cp_ToStrODBC(this.w_LMCODSOT)';

  cKeyWhereODBCqualified = '"STU_PIAC.LMCODCON="+cp_ToStrODBC(this.w_LMCODCON)';
      +'+" and STU_PIAC.LMPROGRE="+cp_ToStrODBC(this.w_LMPROGRE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gslm_mst"
  cComment = "Piano dei conti studio"
  i_nRowNum = 0
  i_nRowPerPage = 11
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LMCODCON = space(10)
  w_AZIENDA = space(10)
  w_CODPIA = space(4)
  w_LMPROGRE = space(8)
  w_LMCODPIA = space(4)
  w_LMMINCON = space(6)
  w_LMMAXCON = space(6)
  w_LMTIPCON = space(1)
  w_LMCONORD = space(1)
  w_LMCONCLI = space(1)
  w_LMCODSOT = space(10)
  w_LMDESSOT = space(30)
  w_LMTIPSOT = space(1)
  w_LMDESABB = space(10)
  w_LMACQVEN = space(1)
  w_LMACQRIV = space(1)
  w_LMPERIND = 0
  w_LMDESCON = space(30)
  w_LMTIPCON = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'STUMPIAC','gslm_mst')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslm_mstPag1","gslm_mst",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Piano")
      .Pages(1).HelpContextID = 241516042
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLMCODCON_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='STU_PARA'
    this.cWorkTables[2]='STUMPIAC'
    this.cWorkTables[3]='STU_PIAC'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.STUMPIAC_IDX,5],7]
    this.nPostItConn=i_TableProp[this.STUMPIAC_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_LMCODCON = NVL(LMCODCON,space(10))
      .w_LMPROGRE = NVL(LMPROGRE,space(8))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from STUMPIAC where LMCODCON=KeySet.LMCODCON
    *                            and LMPROGRE=KeySet.LMPROGRE
    *
    i_nConn = i_TableProp[this.STUMPIAC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STUMPIAC_IDX,2],this.bLoadRecFilter,this.STUMPIAC_IDX,"gslm_mst")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('STUMPIAC')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "STUMPIAC.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"STU_PIAC.","STUMPIAC.")
      i_cTable = i_cTable+' STUMPIAC '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LMCODCON',this.w_LMCODCON  ,'LMPROGRE',this.w_LMPROGRE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_AZIENDA = i_CODAZI
        .w_CODPIA = space(4)
        .w_LMCODCON = NVL(LMCODCON,space(10))
          .link_1_2('Load')
        .w_LMPROGRE = NVL(LMPROGRE,space(8))
        .w_LMCODPIA = NVL(LMCODPIA,space(4))
        .w_LMMINCON = NVL(LMMINCON,space(6))
        .w_LMMAXCON = NVL(LMMAXCON,space(6))
        .w_LMTIPCON = NVL(LMTIPCON,space(1))
        .w_LMCONORD = NVL(LMCONORD,space(1))
        .w_LMCONCLI = NVL(LMCONCLI,space(1))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_LMDESCON = NVL(LMDESCON,space(30))
        .w_LMTIPCON = NVL(LMTIPCON,space(1))
        cp_LoadRecExtFlds(this,'STUMPIAC')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from STU_PIAC where LMCODCON=KeySet.LMCODCON
      *                            and LMPROGRE=KeySet.LMPROGRE
      *                            and LMCODSOT=KeySet.LMCODSOT
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.STU_PIAC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_PIAC_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('STU_PIAC')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "STU_PIAC.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" STU_PIAC"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'LMCODCON',this.w_LMCODCON  ,'LMPROGRE',this.w_LMPROGRE  )
        select * from (i_cTable) STU_PIAC where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_LMCODSOT = NVL(LMCODSOT,space(10))
          .w_LMDESSOT = NVL(LMDESSOT,space(30))
          .w_LMTIPSOT = NVL(LMTIPSOT,space(1))
          .w_LMDESABB = NVL(LMDESABB,space(10))
          .w_LMACQVEN = NVL(LMACQVEN,space(1))
          .w_LMACQRIV = NVL(LMACQRIV,space(1))
          .w_LMPERIND = NVL(LMPERIND,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace LMCODSOT with .w_LMCODSOT
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_LMCODCON=space(10)
      .w_AZIENDA=space(10)
      .w_CODPIA=space(4)
      .w_LMPROGRE=space(8)
      .w_LMCODPIA=space(4)
      .w_LMMINCON=space(6)
      .w_LMMAXCON=space(6)
      .w_LMTIPCON=space(1)
      .w_LMCONORD=space(1)
      .w_LMCONCLI=space(1)
      .w_LMCODSOT=space(10)
      .w_LMDESSOT=space(30)
      .w_LMTIPSOT=space(1)
      .w_LMDESABB=space(10)
      .w_LMACQVEN=space(1)
      .w_LMACQRIV=space(1)
      .w_LMPERIND=0
      .w_LMDESCON=space(30)
      .w_LMTIPCON=space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_AZIENDA = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_AZIENDA))
         .link_1_2('Full')
        endif
        .DoRTCalc(3,4,.f.)
        .w_LMCODPIA = .w_CODPIA
        .DoRTCalc(6,9,.f.)
        .w_LMCONCLI = 'G'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'STUMPIAC')
    this.DoRTCalc(11,19,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oLMCODCON_1_1.enabled = i_bVal
      .Page1.oPag.oLMPROGRE_1_4.enabled = i_bVal
      .Page1.oPag.oLMCODPIA_1_5.enabled = i_bVal
      .Page1.oPag.oLMMINCON_1_9.enabled = i_bVal
      .Page1.oPag.oLMMAXCON_1_10.enabled = i_bVal
      .Page1.oPag.oLMTIPCON_1_13.enabled = i_bVal
      .Page1.oPag.oLMCONORD_1_14.enabled = i_bVal
      .Page1.oPag.oLMCONCLI_1_15.enabled = i_bVal
      .Page1.oPag.oLMDESCON_1_20.enabled = i_bVal
      .Page1.oPag.oLMTIPCON_1_21.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oLMCODCON_1_1.enabled = .f.
        .Page1.oPag.oLMPROGRE_1_4.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oLMCODCON_1_1.enabled = .t.
        .Page1.oPag.oLMPROGRE_1_4.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'STUMPIAC',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.STUMPIAC_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCODCON,"LMCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMPROGRE,"LMPROGRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCODPIA,"LMCODPIA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMMINCON,"LMMINCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMMAXCON,"LMMAXCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMTIPCON,"LMTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCONORD,"LMCONORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCONCLI,"LMCONCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMDESCON,"LMDESCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMTIPCON,"LMTIPCON",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.STUMPIAC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STUMPIAC_IDX,2])
    i_lTable = "STUMPIAC"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.STUMPIAC_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_LMCODSOT C(10);
      ,t_LMDESSOT C(30);
      ,t_LMTIPSOT N(3);
      ,t_LMDESABB C(10);
      ,t_LMACQVEN N(3);
      ,t_LMACQRIV N(3);
      ,t_LMPERIND N(5,2);
      ,LMCODSOT C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgslm_mstbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLMCODSOT_2_1.controlsource=this.cTrsName+'.t_LMCODSOT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLMDESSOT_2_2.controlsource=this.cTrsName+'.t_LMDESSOT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLMTIPSOT_2_3.controlsource=this.cTrsName+'.t_LMTIPSOT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLMDESABB_2_4.controlsource=this.cTrsName+'.t_LMDESABB'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLMACQVEN_2_5.controlsource=this.cTrsName+'.t_LMACQVEN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLMACQRIV_2_6.controlsource=this.cTrsName+'.t_LMACQRIV'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLMPERIND_2_7.controlsource=this.cTrsName+'.t_LMPERIND'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(89)
    this.AddVLine(315)
    this.AddVLine(458)
    this.AddVLine(556)
    this.AddVLine(622)
    this.AddVLine(685)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMCODSOT_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.STUMPIAC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STUMPIAC_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into STUMPIAC
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'STUMPIAC')
        i_extval=cp_InsertValODBCExtFlds(this,'STUMPIAC')
        local i_cFld
        i_cFld=" "+;
                  "(LMCODCON,LMPROGRE,LMCODPIA,LMMINCON,LMMAXCON"+;
                  ",LMTIPCON,LMCONORD,LMCONCLI,LMDESCON"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_LMCODCON)+;
                    ","+cp_ToStrODBC(this.w_LMPROGRE)+;
                    ","+cp_ToStrODBC(this.w_LMCODPIA)+;
                    ","+cp_ToStrODBC(this.w_LMMINCON)+;
                    ","+cp_ToStrODBC(this.w_LMMAXCON)+;
                    ","+cp_ToStrODBC(this.w_LMTIPCON)+;
                    ","+cp_ToStrODBC(this.w_LMCONORD)+;
                    ","+cp_ToStrODBC(this.w_LMCONCLI)+;
                    ","+cp_ToStrODBC(this.w_LMDESCON)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'STUMPIAC')
        i_extval=cp_InsertValVFPExtFlds(this,'STUMPIAC')
        cp_CheckDeletedKey(i_cTable,0,'LMCODCON',this.w_LMCODCON,'LMPROGRE',this.w_LMPROGRE)
        INSERT INTO (i_cTable);
              (LMCODCON,LMPROGRE,LMCODPIA,LMMINCON,LMMAXCON,LMTIPCON,LMCONORD,LMCONCLI,LMDESCON &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_LMCODCON;
                  ,this.w_LMPROGRE;
                  ,this.w_LMCODPIA;
                  ,this.w_LMMINCON;
                  ,this.w_LMMAXCON;
                  ,this.w_LMTIPCON;
                  ,this.w_LMCONORD;
                  ,this.w_LMCONCLI;
                  ,this.w_LMDESCON;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.STU_PIAC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_PIAC_IDX,2])
      *
      * insert into STU_PIAC
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(LMCODCON,LMPROGRE,LMCODSOT,LMDESSOT,LMTIPSOT"+;
                  ",LMDESABB,LMACQVEN,LMACQRIV,LMPERIND,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_LMCODCON)+","+cp_ToStrODBC(this.w_LMPROGRE)+","+cp_ToStrODBC(this.w_LMCODSOT)+","+cp_ToStrODBC(this.w_LMDESSOT)+","+cp_ToStrODBC(this.w_LMTIPSOT)+;
             ","+cp_ToStrODBC(this.w_LMDESABB)+","+cp_ToStrODBC(this.w_LMACQVEN)+","+cp_ToStrODBC(this.w_LMACQRIV)+","+cp_ToStrODBC(this.w_LMPERIND)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,0,'LMCODCON',this.w_LMCODCON,'LMPROGRE',this.w_LMPROGRE,'LMCODSOT',this.w_LMCODSOT)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_LMCODCON,this.w_LMPROGRE,this.w_LMCODSOT,this.w_LMDESSOT,this.w_LMTIPSOT"+;
                ",this.w_LMDESABB,this.w_LMACQVEN,this.w_LMACQRIV,this.w_LMPERIND,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.STUMPIAC_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STUMPIAC_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update STUMPIAC
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'STUMPIAC')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " LMCODPIA="+cp_ToStrODBC(this.w_LMCODPIA)+;
             ",LMMINCON="+cp_ToStrODBC(this.w_LMMINCON)+;
             ",LMMAXCON="+cp_ToStrODBC(this.w_LMMAXCON)+;
             ",LMTIPCON="+cp_ToStrODBC(this.w_LMTIPCON)+;
             ",LMCONORD="+cp_ToStrODBC(this.w_LMCONORD)+;
             ",LMCONCLI="+cp_ToStrODBC(this.w_LMCONCLI)+;
             ",LMDESCON="+cp_ToStrODBC(this.w_LMDESCON)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'STUMPIAC')
          i_cWhere = cp_PKFox(i_cTable  ,'LMCODCON',this.w_LMCODCON  ,'LMPROGRE',this.w_LMPROGRE  )
          UPDATE (i_cTable) SET;
              LMCODPIA=this.w_LMCODPIA;
             ,LMMINCON=this.w_LMMINCON;
             ,LMMAXCON=this.w_LMMAXCON;
             ,LMTIPCON=this.w_LMTIPCON;
             ,LMCONORD=this.w_LMCONORD;
             ,LMCONCLI=this.w_LMCONCLI;
             ,LMDESCON=this.w_LMDESCON;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_LMTIPSOT)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.STU_PIAC_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.STU_PIAC_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from STU_PIAC
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and LMCODSOT="+cp_ToStrODBC(&i_TN.->LMCODSOT)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and LMCODSOT=&i_TN.->LMCODSOT;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = 0
              replace LMCODSOT with this.w_LMCODSOT
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update STU_PIAC
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " LMDESSOT="+cp_ToStrODBC(this.w_LMDESSOT)+;
                     ",LMTIPSOT="+cp_ToStrODBC(this.w_LMTIPSOT)+;
                     ",LMDESABB="+cp_ToStrODBC(this.w_LMDESABB)+;
                     ",LMACQVEN="+cp_ToStrODBC(this.w_LMACQVEN)+;
                     ",LMACQRIV="+cp_ToStrODBC(this.w_LMACQRIV)+;
                     ",LMPERIND="+cp_ToStrODBC(this.w_LMPERIND)+;
                     " ,LMCODSOT="+cp_ToStrODBC(this.w_LMCODSOT)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+;
                             " and LMCODSOT="+cp_ToStrODBC(LMCODSOT)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      LMDESSOT=this.w_LMDESSOT;
                     ,LMTIPSOT=this.w_LMTIPSOT;
                     ,LMDESABB=this.w_LMDESABB;
                     ,LMACQVEN=this.w_LMACQVEN;
                     ,LMACQRIV=this.w_LMACQRIV;
                     ,LMPERIND=this.w_LMPERIND;
                     ,LMCODSOT=this.w_LMCODSOT;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere;
                                      and LMCODSOT=&i_TN.->LMCODSOT;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_LMTIPSOT)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.STU_PIAC_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.STU_PIAC_IDX,2])
        *
        * delete STU_PIAC
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and LMCODSOT="+cp_ToStrODBC(&i_TN.->LMCODSOT)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and LMCODSOT=&i_TN.->LMCODSOT;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.STUMPIAC_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.STUMPIAC_IDX,2])
        *
        * delete STUMPIAC
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_LMTIPSOT)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.STUMPIAC_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STUMPIAC_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(3,19,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oLMMINCON_1_9.visible=!this.oPgFrm.Page1.oPag.oLMMINCON_1_9.mHide()
    this.oPgFrm.Page1.oPag.oLMMAXCON_1_10.visible=!this.oPgFrm.Page1.oPag.oLMMAXCON_1_10.mHide()
    this.oPgFrm.Page1.oPag.oLMTIPCON_1_13.visible=!this.oPgFrm.Page1.oPag.oLMTIPCON_1_13.mHide()
    this.oPgFrm.Page1.oPag.oLMCONORD_1_14.visible=!this.oPgFrm.Page1.oPag.oLMCONORD_1_14.mHide()
    this.oPgFrm.Page1.oPag.oLMTIPCON_1_21.visible=!this.oPgFrm.Page1.oPag.oLMTIPCON_1_21.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_PARA_IDX,3]
    i_lTable = "STU_PARA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_PARA_IDX,2], .t., this.STU_PARA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_PARA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODAZI,LMCODPDC";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODAZI',this.w_AZIENDA)
            select LMCODAZI,LMCODPDC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.LMCODAZI,space(10))
      this.w_CODPIA = NVL(_Link_.LMCODPDC,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(10)
      endif
      this.w_CODPIA = space(4)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_PARA_IDX,2])+'\'+cp_ToStr(_Link_.LMCODAZI,1)
      cp_ShowWarn(i_cKey,this.STU_PARA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oLMCODCON_1_1.value==this.w_LMCODCON)
      this.oPgFrm.Page1.oPag.oLMCODCON_1_1.value=this.w_LMCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oLMPROGRE_1_4.value==this.w_LMPROGRE)
      this.oPgFrm.Page1.oPag.oLMPROGRE_1_4.value=this.w_LMPROGRE
    endif
    if not(this.oPgFrm.Page1.oPag.oLMCODPIA_1_5.value==this.w_LMCODPIA)
      this.oPgFrm.Page1.oPag.oLMCODPIA_1_5.value=this.w_LMCODPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oLMMINCON_1_9.value==this.w_LMMINCON)
      this.oPgFrm.Page1.oPag.oLMMINCON_1_9.value=this.w_LMMINCON
    endif
    if not(this.oPgFrm.Page1.oPag.oLMMAXCON_1_10.value==this.w_LMMAXCON)
      this.oPgFrm.Page1.oPag.oLMMAXCON_1_10.value=this.w_LMMAXCON
    endif
    if not(this.oPgFrm.Page1.oPag.oLMTIPCON_1_13.RadioValue()==this.w_LMTIPCON)
      this.oPgFrm.Page1.oPag.oLMTIPCON_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLMCONORD_1_14.RadioValue()==this.w_LMCONORD)
      this.oPgFrm.Page1.oPag.oLMCONORD_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLMCONCLI_1_15.RadioValue()==this.w_LMCONCLI)
      this.oPgFrm.Page1.oPag.oLMCONCLI_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLMDESCON_1_20.value==this.w_LMDESCON)
      this.oPgFrm.Page1.oPag.oLMDESCON_1_20.value=this.w_LMDESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oLMTIPCON_1_21.RadioValue()==this.w_LMTIPCON)
      this.oPgFrm.Page1.oPag.oLMTIPCON_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMCODSOT_2_1.value==this.w_LMCODSOT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMCODSOT_2_1.value=this.w_LMCODSOT
      replace t_LMCODSOT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMCODSOT_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMDESSOT_2_2.value==this.w_LMDESSOT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMDESSOT_2_2.value=this.w_LMDESSOT
      replace t_LMDESSOT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMDESSOT_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMTIPSOT_2_3.RadioValue()==this.w_LMTIPSOT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMTIPSOT_2_3.SetRadio()
      replace t_LMTIPSOT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMTIPSOT_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMDESABB_2_4.value==this.w_LMDESABB)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMDESABB_2_4.value=this.w_LMDESABB
      replace t_LMDESABB with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMDESABB_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMACQVEN_2_5.RadioValue()==this.w_LMACQVEN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMACQVEN_2_5.SetRadio()
      replace t_LMACQVEN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMACQVEN_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMACQRIV_2_6.RadioValue()==this.w_LMACQRIV)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMACQRIV_2_6.SetRadio()
      replace t_LMACQRIV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMACQRIV_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMPERIND_2_7.value==this.w_LMPERIND)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMPERIND_2_7.value=this.w_LMPERIND
      replace t_LMPERIND with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMPERIND_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'STUMPIAC')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(.w_LMMINCON<=.w_LMMAXCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = cp_Translate(thisform.msgFmt("Intervallo sottoconto errato"))
          case   (empty(.w_LMCODCON))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oLMCODCON_1_1.SetFocus()
            i_bnoObbl = !empty(.w_LMCODCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_LMPROGRE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oLMPROGRE_1_4.SetFocus()
            i_bnoObbl = !empty(.w_LMPROGRE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_LMCODPIA))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oLMCODPIA_1_5.SetFocus()
            i_bnoObbl = !empty(.w_LMCODPIA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_LMMINCON))  and not(g_TRAEXP='G')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oLMMINCON_1_9.SetFocus()
            i_bnoObbl = !empty(.w_LMMINCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_LMMAXCON))  and not(g_TRAEXP='G')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oLMMAXCON_1_10.SetFocus()
            i_bnoObbl = !empty(.w_LMMAXCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_LMTIPCON))  and not(g_TRAEXP='G')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oLMTIPCON_1_13.SetFocus()
            i_bnoObbl = !empty(.w_LMTIPCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_LMCONCLI))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oLMCONCLI_1_15.SetFocus()
            i_bnoObbl = !empty(.w_LMCONCLI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_LMTIPCON))  and not(g_TRAEXP<>'G')
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oLMTIPCON_1_21.SetFocus()
            i_bnoObbl = !empty(.w_LMTIPCON)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_LMTIPSOT) and (NOT EMPTY(.w_LMTIPSOT))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMTIPSOT_2_3
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if NOT EMPTY(.w_LMTIPSOT)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_LMTIPSOT))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_LMCODSOT=space(10)
      .w_LMDESSOT=space(30)
      .w_LMTIPSOT=space(1)
      .w_LMDESABB=space(10)
      .w_LMACQVEN=space(1)
      .w_LMACQRIV=space(1)
      .w_LMPERIND=0
    endwith
    this.DoRTCalc(1,19,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_LMCODSOT = t_LMCODSOT
    this.w_LMDESSOT = t_LMDESSOT
    this.w_LMTIPSOT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMTIPSOT_2_3.RadioValue(.t.)
    this.w_LMDESABB = t_LMDESABB
    this.w_LMACQVEN = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMACQVEN_2_5.RadioValue(.t.)
    this.w_LMACQRIV = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMACQRIV_2_6.RadioValue(.t.)
    this.w_LMPERIND = t_LMPERIND
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_LMCODSOT with this.w_LMCODSOT
    replace t_LMDESSOT with this.w_LMDESSOT
    replace t_LMTIPSOT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMTIPSOT_2_3.ToRadio()
    replace t_LMDESABB with this.w_LMDESABB
    replace t_LMACQVEN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMACQVEN_2_5.ToRadio()
    replace t_LMACQRIV with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMACQRIV_2_6.ToRadio()
    replace t_LMPERIND with this.w_LMPERIND
    if i_srv='A'
      replace LMCODSOT with this.w_LMCODSOT
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgslm_mstPag1 as StdContainer
  Width  = 781
  height = 404
  stdWidth  = 781
  stdheight = 404
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLMCODCON_1_1 as StdField with uid="YUNDTQYRTK",rtseq=1,rtrep=.f.,;
    cFormVar = "w_LMCODCON", cQueryName = "LMCODCON",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 238434812,;
   bGlobalFont=.t.,;
    Height=21, Width=87, Left=185, Top=13, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oLMPROGRE_1_4 as StdField with uid="HLSRQWOIOJ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_LMPROGRE", cQueryName = "LMCODCON,LMPROGRE",;
    bObbl = .t. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    HelpContextID = 108893691,;
   bGlobalFont=.t.,;
    Height=21, Width=103, Left=185, Top=41, cSayPict="'99999999'", cGetPict="'99999999'", InputMask=replicate('X',8)

  add object oLMCODPIA_1_5 as StdField with uid="LRYLINPQXY",rtseq=5,rtrep=.f.,;
    cFormVar = "w_LMCODPIA", cQueryName = "LMCODPIA",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Rappresenta univocamente il codice del piano dei conti di apri usato per l'azienda",;
    HelpContextID = 20331017,;
   bGlobalFont=.t.,;
    Height=21, Width=45, Left=432, Top=41, InputMask=replicate('X',4)

  add object oLMMINCON_1_9 as StdField with uid="MKPHGGTNPH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_LMMINCON", cQueryName = "LMMINCON",;
    bObbl = .t. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Minimo sottoconto studio",;
    HelpContextID = 228301308,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=185, Top=72, cSayPict="g_PICTLEN", cGetPict="g_PICTLEN", InputMask=replicate('X',6)

  func oLMMINCON_1_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_TRAEXP='G')
    endwith
    endif
  endfunc

  add object oLMMAXCON_1_10 as StdField with uid="KSZNYEYWGM",rtseq=7,rtrep=.f.,;
    cFormVar = "w_LMMAXCON", cQueryName = "LMMAXCON",;
    bObbl = .t. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Massimo sottoconto studio",;
    HelpContextID = 218339836,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=185, Top=101, cSayPict="g_PICTLEN", cGetPict="g_PICTLEN", InputMask=replicate('X',6)

  func oLMMAXCON_1_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_TRAEXP='G')
    endwith
    endif
  endfunc


  add object oLMTIPCON_1_13 as StdCombo with uid="DBURXBWZFU",rtseq=8,rtrep=.f.,left=483,top=73,width=111,height=21;
    , ToolTipText = "Tipologia di conto";
    , HelpContextID = 226175484;
    , cFormVar="w_LMTIPCON",RowSource=""+"Attivit�,"+"Passivit�,"+"Costo,"+"Ricavo", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oLMTIPCON_1_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..LMTIPCON,&i_cF..t_LMTIPCON),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'P',;
    iif(xVal =3,'C',;
    iif(xVal =4,'R',;
    space(1))))))
  endfunc
  func oLMTIPCON_1_13.GetRadio()
    this.Parent.oContained.w_LMTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oLMTIPCON_1_13.ToRadio()
    this.Parent.oContained.w_LMTIPCON=trim(this.Parent.oContained.w_LMTIPCON)
    return(;
      iif(this.Parent.oContained.w_LMTIPCON=='A',1,;
      iif(this.Parent.oContained.w_LMTIPCON=='P',2,;
      iif(this.Parent.oContained.w_LMTIPCON=='C',3,;
      iif(this.Parent.oContained.w_LMTIPCON=='R',4,;
      0)))))
  endfunc

  func oLMTIPCON_1_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oLMTIPCON_1_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_TRAEXP='G')
    endwith
    endif
  endfunc

  add object oLMCONORD_1_14 as StdCheck with uid="YAQRVVUAME",rtseq=9,rtrep=.f.,left=625, top=73, caption="Conto d'ordine",;
    HelpContextID = 241812986,;
    cFormVar="w_LMCONORD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLMCONORD_1_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..LMCONORD,&i_cF..t_LMCONORD),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oLMCONORD_1_14.GetRadio()
    this.Parent.oContained.w_LMCONORD = this.RadioValue()
    return .t.
  endfunc

  func oLMCONORD_1_14.ToRadio()
    this.Parent.oContained.w_LMCONORD=trim(this.Parent.oContained.w_LMCONORD)
    return(;
      iif(this.Parent.oContained.w_LMCONORD=='S',1,;
      0))
  endfunc

  func oLMCONORD_1_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oLMCONORD_1_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_TRAEXP='G')
    endwith
    endif
  endfunc


  add object oLMCONCLI_1_15 as StdCombo with uid="IATJDUMPMA",rtseq=10,rtrep=.f.,left=483,top=101,width=111,height=22;
    , ToolTipText = "Cli/for/altro";
    , HelpContextID = 227949057;
    , cFormVar="w_LMCONCLI",RowSource=""+"Clienti,"+"Fornitori,"+"Altro", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oLMCONCLI_1_15.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..LMCONCLI,&i_cF..t_LMCONCLI),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'F',;
    iif(xVal =3,'G',;
    ' '))))
  endfunc
  func oLMCONCLI_1_15.GetRadio()
    this.Parent.oContained.w_LMCONCLI = this.RadioValue()
    return .t.
  endfunc

  func oLMCONCLI_1_15.ToRadio()
    this.Parent.oContained.w_LMCONCLI=trim(this.Parent.oContained.w_LMCONCLI)
    return(;
      iif(this.Parent.oContained.w_LMCONCLI=='C',1,;
      iif(this.Parent.oContained.w_LMCONCLI=='F',2,;
      iif(this.Parent.oContained.w_LMCONCLI=='G',3,;
      0))))
  endfunc

  func oLMCONCLI_1_15.SetRadio()
    this.value=this.ToRadio()
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=4, top=168, width=772,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="LMCODSOT",Label1="Codice",Field2="LMDESSOT",Label2="Descrizione",Field3="LMTIPSOT",Label3="Tipo sottoconto",Field4="LMDESABB",Label4="Descr. abbrev.",Field5="LMACQVEN",Label5="Acq. vent.",Field6="LMACQRIV",Label6="Acq. riv.",Field7="LMPERIND",Label7="% Inded.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218983802

  add object oLMDESCON_1_20 as StdField with uid="BSDJLJIUNU",rtseq=18,rtrep=.f.,;
    cFormVar = "w_LMDESCON", cQueryName = "LMDESCON",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 223357436,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=276, Top=13, InputMask=replicate('X',30)


  add object oLMTIPCON_1_21 as StdCombo with uid="GEBWOEBBTC",rtseq=19,rtrep=.f.,left=483,top=73,width=111,height=21;
    , ToolTipText = "Tipologia di conto";
    , HelpContextID = 226175484;
    , cFormVar="w_LMTIPCON",RowSource=""+"Attivit�,"+"Passivit�,"+"Costo,"+"Ricavo,"+"Conti d'ordine attivo,"+"Conti d'ordine passivo,"+"Conti transitori", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oLMTIPCON_1_21.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..LMTIPCON,&i_cF..t_LMTIPCON),this.value)
    return(iif(xVal =1,'A',;
    iif(xVal =2,'P',;
    iif(xVal =3,'C',;
    iif(xVal =4,'R',;
    iif(xVal =5,'O',;
    iif(xVal =6,'S',;
    iif(xVal =7,'T',;
    space(1)))))))))
  endfunc
  func oLMTIPCON_1_21.GetRadio()
    this.Parent.oContained.w_LMTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oLMTIPCON_1_21.ToRadio()
    this.Parent.oContained.w_LMTIPCON=trim(this.Parent.oContained.w_LMTIPCON)
    return(;
      iif(this.Parent.oContained.w_LMTIPCON=='A',1,;
      iif(this.Parent.oContained.w_LMTIPCON=='P',2,;
      iif(this.Parent.oContained.w_LMTIPCON=='C',3,;
      iif(this.Parent.oContained.w_LMTIPCON=='R',4,;
      iif(this.Parent.oContained.w_LMTIPCON=='O',5,;
      iif(this.Parent.oContained.w_LMTIPCON=='S',6,;
      iif(this.Parent.oContained.w_LMTIPCON=='T',7,;
      0))))))))
  endfunc

  func oLMTIPCON_1_21.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oLMTIPCON_1_21.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_TRAEXP<>'G')
    endwith
    endif
  endfunc

  add object oStr_1_6 as StdString with uid="JOKHMSYRFD",Visible=.t., Left=65, Top=13,;
    Alignment=1, Width=117, Height=18,;
    Caption="Conto studio:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="YXPSYORMDY",Visible=.t., Left=6, Top=72,;
    Alignment=1, Width=176, Height=18,;
    Caption="Sottoconto limite inferiore:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (g_TRAEXP='G')
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="GGSFFMBPKV",Visible=.t., Left=6, Top=101,;
    Alignment=1, Width=176, Height=18,;
    Caption="Sottoconto limite superiore:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (g_TRAEXP='G')
    endwith
  endfunc

  add object oStr_1_11 as StdString with uid="THFUXIVYGW",Visible=.t., Left=375, Top=72,;
    Alignment=1, Width=103, Height=18,;
    Caption="Tipo conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="FVNBVKZWAJ",Visible=.t., Left=354, Top=101,;
    Alignment=1, Width=124, Height=18,;
    Caption="Conto cli/for/altro:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="GKCEOKRQEG",Visible=.t., Left=6, Top=146,;
    Alignment=0, Width=135, Height=18,;
    Caption="Sottoconti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_17 as StdString with uid="DHCNVTSQEQ",Visible=.t., Left=65, Top=41,;
    Alignment=1, Width=117, Height=18,;
    Caption="Progressivo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="PCLUAKQTTY",Visible=.t., Left=314, Top=41,;
    Alignment=1, Width=115, Height=18,;
    Caption="Cod. pdc:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-6,top=190,;
    width=768+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-5,top=191,width=767+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*11*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgslm_mstBodyRow as CPBodyRowCnt
  Width=758
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oLMCODSOT_2_1 as StdTrsField with uid="GQUAOFVQBE",rtseq=11,rtrep=.t.,;
    cFormVar="w_LMCODSOT",value=space(10),isprimarykey=.t.,;
    ToolTipText = "Codice sottoconto",;
    HelpContextID = 30000650,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=85, Left=-2, Top=0, cSayPict=['9999999999'], cGetPict=['9999999999'], InputMask=replicate('X',10)

  add object oLMDESSOT_2_2 as StdTrsField with uid="SSVWEFHDJX",rtseq=12,rtrep=.t.,;
    cFormVar="w_LMDESSOT",value=space(30),;
    HelpContextID = 45078026,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=223, Left=84, Top=0, InputMask=replicate('X',30)

  add object oLMTIPSOT_2_3 as StdTrsCombo with uid="SZHMRRIUYK",rtrep=.t.,;
    cFormVar="w_LMTIPSOT", RowSource=""+"Sott. IVA erario,"+"Sott. IVA acquisti,"+"Sott. IVA vendite,"+"Sott. IVA corrispettivi,"+"Sott. IVA sospesa,"+"Sott. IVA sospesi acq.,"+"Sott. IVA sospesi vend.,"+"Sott. IVA detr. fine anno,"+"Sott. IVA sospesi autotr.,"+"Altro,"+"Cliente,"+"Fornitore" , ;
    ToolTipText = "Tipo sottoconto",;
    HelpContextID = 42259978,;
    Height=21, Width=139, Left=312, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oLMTIPSOT_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..LMTIPSOT,&i_cF..t_LMTIPSOT),this.value)
    return(iif(xVal =1,'I',;
    iif(xVal =2,'Q',;
    iif(xVal =3,'N',;
    iif(xVal =4,'C',;
    iif(xVal =5,'J',;
    iif(xVal =6,'S',;
    iif(xVal =7,'V',;
    iif(xVal =8,'Y',;
    iif(xVal =9,'T',;
    iif(xVal =10,'A',;
    iif(xVal =11,'L',;
    iif(xVal =12,'F',;
    'A')))))))))))))
  endfunc
  func oLMTIPSOT_2_3.GetRadio()
    this.Parent.oContained.w_LMTIPSOT = this.RadioValue()
    return .t.
  endfunc

  func oLMTIPSOT_2_3.ToRadio()
    this.Parent.oContained.w_LMTIPSOT=trim(this.Parent.oContained.w_LMTIPSOT)
    return(;
      iif(this.Parent.oContained.w_LMTIPSOT=='I',1,;
      iif(this.Parent.oContained.w_LMTIPSOT=='Q',2,;
      iif(this.Parent.oContained.w_LMTIPSOT=='N',3,;
      iif(this.Parent.oContained.w_LMTIPSOT=='C',4,;
      iif(this.Parent.oContained.w_LMTIPSOT=='J',5,;
      iif(this.Parent.oContained.w_LMTIPSOT=='S',6,;
      iif(this.Parent.oContained.w_LMTIPSOT=='V',7,;
      iif(this.Parent.oContained.w_LMTIPSOT=='Y',8,;
      iif(this.Parent.oContained.w_LMTIPSOT=='T',9,;
      iif(this.Parent.oContained.w_LMTIPSOT=='A',10,;
      iif(this.Parent.oContained.w_LMTIPSOT=='L',11,;
      iif(this.Parent.oContained.w_LMTIPSOT=='F',12,;
      0)))))))))))))
  endfunc

  func oLMTIPSOT_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oLMDESABB_2_4 as StdTrsField with uid="VTZLZWJDGZ",rtseq=14,rtrep=.t.,;
    cFormVar="w_LMDESABB",value=space(10),;
    ToolTipText = "Descrizione abbreviata sottoconto",;
    HelpContextID = 11523576,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=95, Left=454, Top=0, InputMask=replicate('X',10)

  add object oLMACQVEN_2_5 as StdTrsCheck with uid="HWPCDSHCZM",rtrep=.t.,;
    cFormVar="w_LMACQVEN",  caption="",;
    ToolTipText = "Flag acquisti per ventilazione",;
    HelpContextID = 93169156,;
    Left=552, Top=0, Width=60,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , Autosize=.f.;
   , bGlobalFont=.t.


  func oLMACQVEN_2_5.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..LMACQVEN,&i_cF..t_LMACQVEN),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oLMACQVEN_2_5.GetRadio()
    this.Parent.oContained.w_LMACQVEN = this.RadioValue()
    return .t.
  endfunc

  func oLMACQVEN_2_5.ToRadio()
    this.Parent.oContained.w_LMACQVEN=trim(this.Parent.oContained.w_LMACQVEN)
    return(;
      iif(this.Parent.oContained.w_LMACQVEN=='S',1,;
      0))
  endfunc

  func oLMACQVEN_2_5.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oLMACQRIV_2_6 as StdTrsCheck with uid="JUWCDGMICC",rtrep=.t.,;
    cFormVar="w_LMACQRIV",  caption="",;
    ToolTipText = "Flag acquisti destinati alla rivendita",;
    HelpContextID = 242375156,;
    Left=620, Top=0, Width=52,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , Autosize=.f.;
   , bGlobalFont=.t.


  func oLMACQRIV_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..LMACQRIV,&i_cF..t_LMACQRIV),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oLMACQRIV_2_6.GetRadio()
    this.Parent.oContained.w_LMACQRIV = this.RadioValue()
    return .t.
  endfunc

  func oLMACQRIV_2_6.ToRadio()
    this.Parent.oContained.w_LMACQRIV=trim(this.Parent.oContained.w_LMACQRIV)
    return(;
      iif(this.Parent.oContained.w_LMACQRIV=='S',1,;
      0))
  endfunc

  func oLMACQRIV_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oLMPERIND_2_7 as StdTrsField with uid="LDGQFQKPXV",rtseq=17,rtrep=.t.,;
    cFormVar="w_LMPERIND",value=0,;
    ToolTipText = "Percentuale indeducibilit�",;
    HelpContextID = 123693574,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=66, Left=687, Top=0, cSayPict=['999.99'], cGetPict=['999.99']
  add object oLast as LastKeyMover
  * ---
  func oLMCODSOT_2_1.When()
    return(.t.)
  proc oLMCODSOT_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oLMCODSOT_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=10
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslm_mst','STUMPIAC','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LMCODCON=STUMPIAC.LMCODCON";
  +" and "+i_cAliasName2+".LMPROGRE=STUMPIAC.LMPROGRE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
