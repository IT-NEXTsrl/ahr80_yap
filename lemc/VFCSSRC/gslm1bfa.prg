* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm1bfa                                                        *
*              EXPORT FATTURE ACQUISTO                                         *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-24                                                      *
* Last revis.: 2018-07-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm1bfa",oParentObject)
return(i_retval)

define class tgslm1bfa as StdBatch
  * --- Local variables
  w_REGNOVALIDA = .f.
  w_STRINGA = space(120)
  w_ACTUALPOS = 0
  w_PNSERIAL = space(10)
  w_TOTDOC = 0
  w_TOTDOCINVAL = 0
  w_ANCODSTU = space(6)
  w_APRNORM = space(2)
  w_APRCAU = space(3)
  w_IVASTU = space(2)
  w_IVASTUNOR = space(2)
  w_IVACEE = space(2)
  w_ESCI = .f.
  w_OKTRAS = .f.
  w_PNCOMIVA = ctod("  /  /  ")
  w_PNDATREG = ctod("  /  /  ")
  w_NOTRAS = 0
  w_PNCODCAU = space(5)
  w_IVCODIVA = space(5)
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_NUMRIGHE = 0
  w_PROGRIGA = 0
  w_TIPCLI = space(1)
  w_CODCLI = space(15)
  w_TOTIVA = 0
  w_TIPOREG = space(1)
  w_NUMREG = 0
  w_CODATT = 0
  w_CODSOGG = space(8)
  w_CODPIVA = space(16)
  w_CODFISC = space(16)
  w_PIUATTIV = space(1)
  w_CODSEZ = 0
  w_MESEAT = 0
  w_FATANNPREC = space(1)
  w_ESIGDIF = space(1)
  w_PAGESIGDIF = space(1)
  w_PNSERORI = space(10)
  w_CODFATTSI = space(1)
  w_IVAIND = .f.
  w_PERCIND = 0
  w_GESTNORMA = .f.
  w_NORMAIND100 = space(2)
  w_NORMANOIVA = space(2)
  w_FATTDIFF = .f.
  w_NORMADIFF = space(2)
  w_ANNOCOMP = space(4)
  w_MESECOMP = space(4)
  w_ROWNUM = 0
  w_IVACASSA = space(1)
  w_ACQSMARINO = space(1)
  w_OKNORM = .f.
  w_PNDATDOC = ctod("  /  /  ")
  w_CODATTPR = space(5)
  w_VENACQ = space(10)
  w_FLAG3000EU = space(1)
  w_NORMA87 = .f.
  w_NORMAEP = .f.
  w_NORMA33 = .f.
  w_DETIVA = space(1)
  w_DATREGOR = ctod("  /  /  ")
  w_DATDOCOR = ctod("  /  /  ")
  w_DATREGIP = ctod("  /  /  ")
  w_DATDOCIP = ctod("  /  /  ")
  w_NORFATT = space(2)
  w_CODNORCAU = space(2)
  w_CODCAUOR = space(5)
  w_CODTRAC = space(10)
  w_SPLIT = space(1)
  w_DETRAZIONE = .f.
  * --- WorkFile variables
  CONTI_idx=0
  STU_NORM_idx=0
  STU_PNTT_idx=0
  STU_TRAS_idx=0
  PNT_IVA_idx=0
  ATTIDETT_idx=0
  ATTIMAST_idx=0
  AZIENDA_idx=0
  COD_NORM_idx=0
  VOCIIVA_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT VERSO LO STUDIO DELLE FATTURE DI ACQUISTO e NOTE DI CREDITO
    CREATE CURSOR MOVDAFAT (PNSERIAL C(10), PNNUMRER N(6), ;
    PNDATREG D(8), PNCODCAU C(5), PNCOMPET C(4), PNTIPDOC C(2), ;
    PNNUMDOC N(6), PNALFDOC C(2), PNDATDOC D(8), PNNUMPRO N(6), ;
    PNALFPRO C(2), PNCODVAL C(3), PNVALNAZ C(3), PNTOTDOC N(18,4), ;
    PNTIPCLF C(1), PNCODCLF C(15), PNCOMIVA D(8), PNTIPREG C(1), ;
    PNNUMREG N(2), PNCAURIG C(5), PNDESRIG C(50), PNTIPCON C(1), ;
    PNIMPDAR N(18,4), PNCODCON C(15), PNIMPAVE N(18,4), ANTIPSOT C(1),SEGNO C(1), CHKESDIF C(1))
    this.w_IVAIND = .F.
    * --- Leggo se l'azienda esercita piu' Attivita'
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZATTIVI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZATTIVI;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PIUATTIV = NVL(cp_ToDate(_read_.AZATTIVI),cp_NullValue(_read_.AZATTIVI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Leggo codice norma esigibilit� differita
    * --- Read from COD_NORM
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.COD_NORM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COD_NORM_idx,2],.t.,this.COD_NORM_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CNNORMA1,CNNORMA8,CNNORMA9,CNNORM10"+;
        " from "+i_cTable+" COD_NORM where ";
            +"CNCODTRA = "+cp_ToStrODBC("LEMCO");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CNNORMA1,CNNORMA8,CNNORMA9,CNNORM10;
        from (i_cTable) where;
            CNCODTRA = "LEMCO";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NORFATT = NVL(cp_ToDate(_read_.CNNORMA1),cp_NullValue(_read_.CNNORMA1))
      this.w_NORMANOIVA = NVL(cp_ToDate(_read_.CNNORMA8),cp_NullValue(_read_.CNNORMA8))
      this.w_NORMAIND100 = NVL(cp_ToDate(_read_.CNNORMA9),cp_NullValue(_read_.CNNORMA9))
      this.w_NORMADIFF = NVL(cp_ToDate(_read_.CNNORM10),cp_NullValue(_read_.CNNORM10))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT FATTACQU
    GO TOP
    * --- Ciclo sul cursore della fatture di acquisto
    do while NOT EOF()
      this.w_CODFATTSI = " "
      this.w_GESTNORMA = .F.
      this.w_FATTDIFF = .F.
      this.w_PROGRIGA = 0
      * --- Messaggio a schermo
      this.w_REGNOVALIDA = .F.
      Ah_MSG ("Export fatture di acquisto : reg. num. %1 - data %2",.t.,.f.,.f.,STR(FATTACQU.PNNUMRER,6,0),dtoc(FATTACQU.PNDATREG))
      * --- Scrittura su Log
      this.w_STRINGA = Ah_MsgFormat("%1Export fatture di acquisto : reg. num. %2 - data %3","          ",STR(FATTACQU.PNNUMRER,6,0)+"/"+ALLTRIM(STR(FATTACQU.PNCODUTE,4,0)),dtoc(FATTACQU.PNDATREG))
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      * --- Salvo la posizione attuale del record e il seriale della registrazione
      this.w_ACTUALPOS = RECNO()
      this.w_PNSERIAL = FATTACQU.PNSERIAL
      this.w_TOTDOC = 0
      this.w_NUMRIGHE = 0
      this.w_TOTIVA = 0
      this.w_ROWNUM = 0
      * --- Calcolo l'importo totale del documento e il numero delle righe
      this.w_IVACASSA = " "
      do while FATTACQU.PNSERIAL=this.w_PNSERIAL
        this.w_NUMRIGHE = this.w_NUMRIGHE+1
        this.w_TOTDOC = FATTACQU.IVIMPONI+FATTACQU.IVIMPIVA+this.w_TOTDOC
        this.w_TOTIVA = FATTACQU.IVIMPIVA+this.w_TOTIVA
        this.w_PNCODCAU = NVL(FATTACQU.PNCODCAU," ")
        this.w_IVCODIVA = NVL(FATTACQU.IVCODIVA," ")
        this.w_PNDATREG = nvl(FATTACQU.PNDATREG,ctod("  -  -    "))
        this.w_PNDATDOC = nvl(FATTACQU.PNDATDOC,ctod("  -  -    "))
        * --- Verifico se devo gestire l'iva per cassa
        if ! this.w_GESTNORMA
          * --- Read from STU_TRAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.STU_TRAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LMAPRNOR"+;
              " from "+i_cTable+" STU_TRAS where ";
                  +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
                  +" and LMHOCCAU = "+cp_ToStrODBC(this.w_PNCODCAU);
                  +" and LMHOCIVA = "+cp_ToStrODBC(this.w_IVCODIVA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LMAPRNOR;
              from (i_cTable) where;
                  LMCODICE = this.oParentObject.w_ASSOCI;
                  and LMHOCCAU = this.w_PNCODCAU;
                  and LMHOCIVA = this.w_IVCODIVA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODNORCAU = NVL(cp_ToDate(_read_.LMAPRNOR),cp_NullValue(_read_.LMAPRNOR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if upper(this.w_NORFATT) == upper(this.w_CODNORCAU)
            this.w_GESTNORMA = .T.
          endif
        endif
        SELECT FATTACQU
        if this.oParentObject.w_LMCHKNO="S" AND NVL(FATTACQU.CCFLIVDF,"N") ="S"
          this.w_IVACASSA = "S"
        endif
        skip 1
      enddo
      * --- Ripristino posizione
      go this.w_ACTUALPOS
      * --- Record di testata
      this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
      this.oParentObject.w_FATTACQU = this.oParentObject.w_FATTACQU+1
      this.w_TOTDOCINVAL = FATTACQU.PNTOTDOC
      FWRITE (this.oParentObject.hFile , "D30" , 3 )
      * --- Estraggo il codice attivit� dal registro iva usato sulla prima riga del castelletto IVA della Primanota
      this.w_TIPOREG = NVL(FATTACQU.IVTIPREG," ")
      this.w_NUMREG = NVL(FATTACQU.IVNUMREG,0)
      * --- Estraggo il codice attivit� contenente il tipo e numero registro IVA presenti in Primanota
      * --- Read from ATTIDETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATCODATT,ATCODSEZ"+;
          " from "+i_cTable+" ATTIDETT where ";
              +"ATNUMREG = "+cp_ToStrODBC(this.w_NUMREG);
              +" and ATTIPREG = "+cp_ToStrODBC(this.w_TIPOREG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATCODATT,ATCODSEZ;
          from (i_cTable) where;
              ATNUMREG = this.w_NUMREG;
              and ATTIPREG = this.w_TIPOREG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODATTPR = NVL(cp_ToDate(_read_.ATCODATT),cp_NullValue(_read_.ATCODATT))
        this.w_CODSEZ = NVL(cp_ToDate(_read_.ATCODSEZ),cp_NullValue(_read_.ATCODSEZ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggo il codice attivit� studio associato all'attivit� 
      * --- Read from ATTIMAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ATTIMAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIMAST_idx,2],.t.,this.ATTIMAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATATTIVA"+;
          " from "+i_cTable+" ATTIMAST where ";
              +"ATCODATT = "+cp_ToStrODBC(this.w_CODATTPR);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATATTIVA;
          from (i_cTable) where;
              ATCODATT = this.w_CODATTPR;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODATT = NVL(cp_ToDate(_read_.ATATTIVA),cp_NullValue(_read_.ATATTIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_PIUATTIV # "S"
        * --- Se l'azienda non gestisce pi� attivit� assegno al codice attivit� '00'
        this.w_CODATT = 00
      endif
      * --- Codice attivit� IVA
      FWRITE (this.oParentObject.hFile , right("00"+alltrim(str(this.w_CODATT)),2), 2)
      * --- Recupero e inserisco l' anno e il mese della registrazione
      FWRITE(this.oParentObject.hFile,LEFT(dtos(FATTACQU.PNDATREG),6),6)
      * --- Recupero e inserisco l' anno di competenza
      this.w_ANNOCOMP = iif(empty(dtos(nvl(FATTACQU.PNDATPLA,ctod("  -  -    ")))),"00000000",dtos(nvl(FATTACQU.PNDATPLA,ctod("  -  -    "))))
      FWRITE(this.oParentObject.hFile,SUBSTR(this.w_ANNOCOMP,1,4),4)
      * --- Recupero e inserisco il mese di competenza
      this.w_MESECOMP = iif(empty(dtos(nvl(FATTACQU.PNDATPLA,ctod("  -  -    ")))),"00000000",dtos(nvl(FATTACQU.PNDATPLA,ctod("  -  -    "))))
      FWRITE(this.oParentObject.hFile,SUBSTR(this.w_MESECOMP,5,2),2)
      * --- Sezione
      FWRITE(this.oParentObject.hFile,RIGHT("00"+ALLTRIM(STR(this.w_CODSEZ,2,0)),2),2)
      * --- Tipo del documento - Fattura = 1 ; NC = 2
      FWRITE(this.oParentObject.hFile,IIF(FATTACQU.PNTIPDOC="NC" OR FATTACQU.PNTIPDOC="NE", "2", "1"),1)
      * --- Tipo fattura
      FWRITE(this.oParentObject.hFile,SPACE(2),2)
      * --- Numero Documento - Acquisto = Protocollo
      FWRITE(this.oParentObject.hFile,RIGHT("0000000"+ALLTRIM(STR(FATTACQU.PNNUMPRO)),7),7)
      * --- Num Bis
      FWRITE(this.oParentObject.hFile,RIGHT(" "+ALLTRIM(FATTACQU.PNALFPRO),1),1)
      * --- Numero fattura fornitore - Numero documento
      FWRITE(this.oParentObject.hFile,LEFT(ALLTRIM(STR(FATTACQU.PNNUMDOC))+" "+ALLTRIM(FATTACQU.PNALFDOC)+SPACE(10),10),10)
      * --- -- Date --
      *     Data registrazione
      FWRITE(this.oParentObject.hFile,dtos(FATTACQU.PNDATREG),8)
      * --- Data Fattura
      FWRITE(this.oParentObject.hFile,NVL(dtos(FATTACQU.PNDATDOC),"        "),8)
      * --- Data scadenza, Riscontro, Rateo - Non Gestite
      FWRITE(this.oParentObject.hFile,repl("0",8),8)
      FWRITE(this.oParentObject.hFile,repl("0",8),8)
      FWRITE(this.oParentObject.hFile,repl("0",8),8)
      * --- Codice Pagamento e tipo pagamento - Non gestite
      FWRITE(this.oParentObject.hFile,"000",3)
      FWRITE(this.oParentObject.hFile," ",1)
      * --- Codice Centro di Costo
      FWRITE(this.oParentObject.hFile,"0000",4)
      * --- Codice cliente/fornitore
      this.w_ANCODSTU = SPACE(6)
      this.w_PNTIPCLF = FATTACQU.PNTIPCLF
      this.w_PNCODCLF = FATTACQU.PNCODCLF
      this.w_TIPCLI = "C"
      this.w_CODCLI = SPACE(15)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU,ANCONRIF,ANCODSOG,ANPARIVA,ANCODFIS"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU,ANCONRIF,ANCODSOG,ANPARIVA,ANCODFIS;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCLF;
              and ANCODICE = this.w_PNCODCLF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        this.w_CODCLI = NVL(cp_ToDate(_read_.ANCONRIF),cp_NullValue(_read_.ANCONRIF))
        this.w_CODSOGG = NVL(cp_ToDate(_read_.ANCODSOG),cp_NullValue(_read_.ANCODSOG))
        this.w_CODPIVA = NVL(cp_ToDate(_read_.ANPARIVA),cp_NullValue(_read_.ANPARIVA))
        this.w_CODFISC = NVL(cp_ToDate(_read_.ANCODFIS),cp_NullValue(_read_.ANCODFIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT FATTACQU
      if NOT EMPTY(this.w_ANCODSTU)
        * --- Codice Cli/For
        FWRITE(this.oParentObject.hFile,right("000000"+alltrim(this.w_ANCODSTU),6),6)
        * --- Tipo codifica P/F/S
        FWRITE(this.oParentObject.hFile," ",1)
        * --- Codice codifica
        FWRITE(this.oParentObject.hFile,replicate(" ",16),16)
      else
        this.w_STRINGA = Ah_MsgFormat("%1Errore! Fornitore %2 non ha un codice fornitore nello studio associato","               ",NVL(this.w_PNCODCLF, " "))
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      * --- Descrizione alternativa aggiuntiva
      FWRITE(this.oParentObject.hFile,NVL(FATTACQU.DESSUP,SPACE(29)),29)
      * --- Tipo descrizione
      FWRITE(this.oParentObject.hFile,IIF(NOT EMPTY(NVL(FATTACQU.DESSUP," ")),"A"," "),1)
      * --- Flag Partita - Non Gestito
      FWRITE(this.oParentObject.hFile," ",1)
      * --- Mese stampa su reg.IVA per autotrasportatori
      if FATTACQU.CCFLAUTR = "S"
        this.w_MESEAT = IIF(INT((MONTH(FATTACQU.PNDATDOC)+2)/3)+1>4,MOD(INT((MONTH(FATTACQU.PNDATDOC)+2)/3)+1,4),INT((MONTH(FATTACQU.PNDATDOC)+2)/3)+1)*3
        FWRITE(this.oParentObject.hFile , RIGHT("0"+LTRIM(STR(this.w_MESEAT,2,0)),2) , 2)
      else
        FWRITE(this.oParentObject.hFile , "00" , 2)
      endif
      * --- Importo altre ritenute- Non Gestito
      FWRITE(this.oParentObject.hFile , "00000000000" , 11)
      * --- Segno importo altre ritenute
      FWRITE(this.oParentObject.hFile,"+",1)
      * --- Importo ritenuta di acconto - Non Gestito
      if isalt()
        FWRITE(this.oParentObject.hFile,right( "00000000000" + alltrim( str( int(ABS(FATTACQU.IMP_EUR)), 11, 0 ) ) + right( str( FATTACQU.IMP_EUR, 11, 2 ) , 2 ) , 11 ))
      else
        FWRITE(this.oParentObject.hFile , "00000000000" , 11)
      endif
      * --- Segno importo  ritenute di acconto
      FWRITE(this.oParentObject.hFile,"+",1)
      * --- Non Contabilizzare: a 'N' solo nel caso di Storno Iva Pagamamento Fattura Esigibilit� Differita
      * --- La query filtra le registrazioni in funzione del tipo documento, nel caso tipo documento='NO' deve essere  necessariamente un pagamento ad esigibilt� differita
      *     (CAU_CONT.CCFLPDIF='S')
      * --- Flag contabilizzazione
      FWRITE(this.oParentObject.hFile,IIF(FATTACQU.PNTIPDOC="NO", "N", " "),1)
      * --- Leggo i dati relativi all'esigibilit� differita imputati nella causale
      this.w_ESIGDIF = NVL(FATTACQU.CCFLIVDF,"N")
      this.w_PAGESIGDIF = NVL(FATTACQU.CCFLPDIF,"N")
      this.w_SPLIT = IIF(Nvl(FATTACQU.CCSCIPAG,"N")="S","S","N")
      if this.w_ESIGDIF="N" and this.w_PAGESIGDIF="N"
        * --- Codice mittente
        FWRITE(this.oParentObject.hFile,"AHREV",5)
        * --- Chiave fattura mittente
        FWRITE(this.oParentObject.hFile,padl(this.w_PNSERIAL,10,"0"),10)
      else
        * --- Gestione del codice legame per le registrazione con IVA sospesa
        if this.w_ESIGDIF="S"
          * --- Registrazione fattura di acquisto (Origine)
          if this.oParentObject.w_LMCHKNO="S" AND this.w_IVACASSA="S"
            this.w_CODFATTSI = "S"
          endif
          if this.w_GESTNORMA
            this.w_CODFATTSI = "S"
            * --- Verifico se il documento collegato � una 'fattura differita'
            * --- Codice mittente
            FWRITE(this.oParentObject.hFile,"AHREV",5)
            * --- Chiave fattura mittente
            FWRITE(this.oParentObject.hFile,padl(this.w_PNSERIAL,10,"0"),10)
            * --- Verifico se registrazione inerente ad una fattura di vendita/acquisto differita
            this.w_PNCOMIVA = nvl(FATTACQU.PNCOMIVA,ctod("  -  -    "))
            this.w_PNDATREG = nvl(FATTACQU.PNDATREG,ctod("  -  -    "))
            if month(this.w_PNDATREG) = 1
              this.w_FATTDIFF = month(this.w_PNCOMIVA) = 12
            else
              this.w_FATTDIFF = month(this.w_PNCOMIVA) < month(this.w_PNDATREG)
            endif
          else
            * --- Soggetti pubblici
            * --- Codice mittente
            FWRITE(this.oParentObject.hFile,"AHREV",5)
            * --- Chiave fattura mittente
            FWRITE(this.oParentObject.hFile,padl(this.w_PNSERIAL,10,"0"),10)
          endif
        else
          * --- Registrazione di incasso/pagamento
          * --- Select from GSLM_NOR
          do vq_exec with 'GSLM_NOR',this,'_Curs_GSLM_NOR','',.f.,.t.
          if used('_Curs_GSLM_NOR')
            select _Curs_GSLM_NOR
            locate for 1=1
            do while not(eof())
            * --- Leggo la chiave della registrazione di origine
            this.w_PNSERORI = _Curs_GSLM_NOR.SERIALOR
            * --- Leggo il codice  causale della registrazione di origine
            this.w_CODCAUOR = _Curs_GSLM_NOR.PNCODCAU
            this.w_IVCODIVA = NVL(_Curs_GSLM_NOR.IVCODIVA," ")
            * --- Verifico se devo gestire l'iva per cassa
            if ! this.w_GESTNORMA
              * --- Read from STU_TRAS
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.STU_TRAS_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "LMAPRNOR"+;
                  " from "+i_cTable+" STU_TRAS where ";
                      +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
                      +" and LMHOCCAU = "+cp_ToStrODBC(this.w_CODCAUOR);
                      +" and LMHOCIVA = "+cp_ToStrODBC(this.w_IVCODIVA);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  LMAPRNOR;
                  from (i_cTable) where;
                      LMCODICE = this.oParentObject.w_ASSOCI;
                      and LMHOCCAU = this.w_CODCAUOR;
                      and LMHOCIVA = this.w_IVCODIVA;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CODNORCAU = NVL(cp_ToDate(_read_.LMAPRNOR),cp_NullValue(_read_.LMAPRNOR))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if upper(this.w_NORFATT) == upper(this.w_CODNORCAU)
                this.w_GESTNORMA = .T.
              endif
            endif
            if this.w_PAGESIGDIF="S" AND this.oParentObject.w_LMCHKNO="S"
              this.w_CODFATTSI = "C"
            endif
              select _Curs_GSLM_NOR
              continue
            enddo
            use
          endif
          if this.w_CODFATTSI="C" AND this.oParentObject.w_LMCHKNO="S"
            * --- Codice mittente
            FWRITE(this.oParentObject.hFile,"AHREV",5)
            * --- Chiave fattura mittente
            FWRITE(this.oParentObject.hFile,padl(this.w_PNSERORI,10,"0"),10)
          else
            if this.w_GESTNORMA
              * --- Se la registrazione di incasso/pagamento fa riferimento ad una registrazione con IVA sospesa, valorizzo correttamente il flag 'codice fattura'
              this.w_CODFATTSI = "I"
              * --- Codice mittente
              FWRITE(this.oParentObject.hFile,"AHREV",5)
              * --- Chiave fattura mittente
              FWRITE(this.oParentObject.hFile,padl(this.w_PNSERORI,10,"0"),10)
            else
              * --- Soggetti pubblici
              * --- Codice mittente
              FWRITE(this.oParentObject.hFile,"AHREV",5)
              * --- Chiave fattura mittente
              FWRITE(this.oParentObject.hFile,padl(this.w_PNSERIAL,10,"0"),10)
            endif
          endif
        endif
      endif
      if (empty(nvl(FATTACQU.PN__ANNO,0)) OR empty(nvl(FATTACQU.PN__MESE,0))) OR (YEAR(FATTACQU.PNDATDOC) = FATTACQU.PN__ANNO and MONTH(FATTACQU.PNDATDOC) = FATTACQU.PN__MESE)
        this.w_FATANNPREC = " "
        this.oParentObject.w_FILLER = SPACE(6)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,6)
      else
        this.w_FATANNPREC = "S"
        * --- Anno fattura rif.
        FWRITE(this.oParentObject.hFile,ALLTRIM(STR(FATTACQU.PN__ANNO)),4)
        * --- Mese fattura rif.
        FWRITE(this.oParentObject.hFile,RIGHT("00"+ALLTRIM(STR(FATTACQU.PN__MESE)),2),2)
      endif
      * --- Flag - Nota variazione relativa a fatture anni precedenti
      FWRITE(this.oParentObject.hFile,this.w_FATANNPREC,1)
      * --- Verifico se la registrazione di primanota movienta sia registri di Vendita che di acquito
      this.w_VENACQ = ""
      * --- Read from PNT_IVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2],.t.,this.PNT_IVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVSERIAL"+;
          " from "+i_cTable+" PNT_IVA where ";
              +"IVSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
              +" and IVTIPREG = "+cp_ToStrODBC("V");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVSERIAL;
          from (i_cTable) where;
              IVSERIAL = this.w_PNSERIAL;
              and IVTIPREG = "V";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_VENACQ = NVL(cp_ToDate(_read_.IVSERIAL),cp_NullValue(_read_.IVSERIAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY (this.w_VENACQ)
        this.w_VENACQ = ""
        * --- Read from PNT_IVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PNT_IVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2],.t.,this.PNT_IVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVSERIAL"+;
            " from "+i_cTable+" PNT_IVA where ";
                +"IVSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                +" and IVTIPREG = "+cp_ToStrODBC("A");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVSERIAL;
            from (i_cTable) where;
                IVSERIAL = this.w_PNSERIAL;
                and IVTIPREG = "A";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_VENACQ = NVL(cp_ToDate(_read_.IVSERIAL),cp_NullValue(_read_.IVSERIAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.oParentObject.w_FILLER = IIF ( this.w_TIPOREG="V" AND NOT EMPTY (this.w_VENACQ),"VEN", IIF ( this.w_TIPOREG="A" AND NOT EMPTY (this.w_VENACQ) ,"ACQ",space(3))) +"N"
      FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,4)
      * --- Scrivo il codice fattura normale/Sospesa/Incasso
      FWRITE(this.oParentObject.hFile,this.w_CODFATTSI,1)
      * --- AIFT - gestioni 3000 euro 
      *     Se non c' � AIFT il campo � sbiancato
      *     Se non c' e intestatario si imposta CodTipoFatt3000Euro='E' anche se non c � AIFT
      *     Negli altri casi si imposta in base ai valori assunti dai campi OSFLGEXT e OSTIPOPE 
      *     - P = Contratto corrispettivi periodici 
      *     - F = Corrispettivo incluso forzatamente 
      *     - E = Corrispettivo escluso forzatamente 
      *     - Blk = Corrispettivo non classificato con alcuno dei valori precedenti 
      if g_AIFT ="S" AND NOT EMPTY ( NVL (FATTACQU.PNCODCLF ," ")) 
        this.w_FLAG3000EU = IIF ( NVL (FATTACQU.OSFLGEXT," ") = "I" , "F" , IIF ( NVL (FATTACQU.OSFLGEXT," ") = "F" , "E" , IIF ( NVL (FATTACQU.OSTIPOPE," ") = "P" , "P" , IIF ( NVL (FATTACQU.OSTIPOPE," ") = "C" , "C" , IIF ( NVL (FATTACQU.OSTIPFAT," ") = "A" , "A" , IIF ( NVL (FATTACQU.OSTIPFAT," ") = "S" , "S" , SPACE(1) ) ) ) ) ) )
        FWRITE(this.oParentObject.hFile, this.w_FLAG3000EU ,1)
      else
        if EMPTY ( NVL (FATTACQU.PNCODCLF ," ")) 
          FWRITE(this.oParentObject.hFile, "E" , 1)
        else
          FWRITE(this.oParentObject.hFile, SPACE (1) , 1)
        endif
      endif
      this.oParentObject.w_FILLER = SPACE(3)
      FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,3)
      if FATTACQU.PNTIPDOC="FE" OR FATTACQU.PNTIPDOC="NE" OR FATTACQU.PNTIPDOC="AU" OR FATTACQU.PNTIPDOC="NU"
        if NOT EMPTY(this.w_CODCLI) AND NOT EMPTY(this.oParentObject.w_CAUGIR)
          * --- Giroconto per il fornitore
          INSERT INTO MOVDAFAT (PNSERIAL, PNNUMRER, PNDATREG, PNCODCAU, ;
          PNCOMPET , PNTIPDOC, PNNUMDOC, PNALFDOC, PNDATDOC, PNNUMPRO, ;
          PNALFPRO, PNCODVAL, PNVALNAZ, PNTOTDOC, PNTIPCLF, PNCODCLF, ;
          PNCOMIVA, PNTIPREG, PNNUMREG, PNCAURIG, PNDESRIG, PNTIPCON, ;
          PNIMPDAR, PNCODCON, PNIMPAVE, ANTIPSOT,SEGNO, CHKESDIF) VALUES ;
          (FATTACQU.PNSERIAL, FATTACQU.PNNUMRER, FATTACQU.PNDATREG, this.oParentObject.w_CAUGIR, FATTACQU.PNCOMPET, "NO", ;
          NVL(FATTACQU.PNNUMDOC,0), NVL(FATTACQU.PNALFDOC," "), NVL(FATTACQU.PNDATDOC,CTOD("  -  -  ")), NVL(FATTACQU.PNNUMPRO,0), ;
          NVL(FATTACQU.PNALFPRO," "), NVL(FATTACQU.PNCODVAL," "), NVL(FATTACQU.PNVALNAZ," "), NVL(FATTACQU.PNTOTDOC,0), ;
          FATTACQU.PNTIPCLF, FATTACQU.PNCODCLF, NVL(FATTACQU.PNCOMIVA,CTOD("  -  -  ")), "N", 0, this.oParentObject.w_CAUGIR, ;
          " ", this.w_TIPCLI, this.w_TOTIVA, this.w_CODCLI, 0, "X",IIF(FATTACQU.PNTIPDOC="NC" OR FATTACQU.PNTIPDOC="NE" OR FATTACQU.PNTIPDOC="NU","-" ,"+"), ;
          FATTACQU.CCFLPDIF)
          SELECT FATTACQU
        else
          if EMPTY(this.oParentObject.w_CAUGIR)
            this.w_STRINGA = Ah_msgFormat("%1Errore! Causale giroconto non definita nella tabella trasferimento studio","               ")
          else
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Fornitore %2 non ha il collegamento al cliente corrispondente","               ",NVL(this.w_PNCODCLF, " "))
          endif
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
      endif
      * --- Ciclo su tutte le righe IVA
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT FATTACQU
    enddo
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Butto giu' tutte le righe IVA della fattura in questione
    this.w_ESCI = .F.
    this.w_OKTRAS = .T.
    this.w_PROGRIGA = 0
    this.w_NORMA87 = .F.
    this.w_NORMAEP = .F.
    this.w_NORMA33 = .F.
    * --- Posizionamento puntatore al file
    SELECT FATTACQU
    this.w_ACTUALPOS = RECNO()
    this.w_ACQSMARINO = IIF ( NVL (this.oParentObject.w_LMCAUSSM," ")=FATTACQU.PNCODCAU AND alltrim(NACODEST)= "037" , "S" , " ")
    do while FATTACQU.PNSERIAL=this.w_PNSERIAL AND NOT this.w_ESCI
      this.w_PROGRIGA = this.w_PROGRIGA+1
      if NOT EMPTY(NVL(FATTACQU.IVCONTRO,"  ")) AND NVL(FATTACQU.IVIMPONI,0)<>0
        * --- Incremento numero di record scritti
        this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
        this.oParentObject.w_FATTACQU = this.oParentObject.w_FATTACQU+1
        * --- Se sono in questo caso devo scrivere testata e dettaglio
        FWRITE (this.oParentObject.hFile , "D31" , 3 )
        * --- Recupero e scrittura del codice di Aliquota IVA
        this.w_APRCAU = SPACE(3)
        this.w_APRNORM = SPACE(2)
        this.w_IVASTU = SPACE(2)
        this.w_IVACEE = SPACE(2)
        this.w_PNCODCAU = NVL(FATTACQU.PNCODCAU," ")
        this.w_IVCODIVA = NVL(FATTACQU.IVCODIVA," ")
        this.w_ROWNUM = NVL(FATTACQU.CPROWNUM, 0)
        * --- Leggo se il codice IVA in oggetto ha una percentuale di indetraibilit�
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIND"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_IVCODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIND;
            from (i_cTable) where;
                IVCODIVA = this.w_IVCODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PERCIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_PERCIND # 0 and this.w_GESTNORMA
          * --- Sono in presenza di IVA detraibili, aggiorno l avariabile per l'inserimento di una nuova riga per la registrazione con IVA per cassa 
          this.w_IVAIND = .T.
          this.w_NUMRIGHE = this.w_NUMRIGHE+1
        else
          this.w_IVAIND = .F.
        endif
        * --- Read from STU_TRAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.STU_TRAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LMAPRNOR,LMAPRCAU,LMIVASTU,LMIVACEE,LMDETIVA"+;
            " from "+i_cTable+" STU_TRAS where ";
                +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
                +" and LMHOCCAU = "+cp_ToStrODBC(this.w_PNCODCAU);
                +" and LMHOCIVA = "+cp_ToStrODBC(this.w_IVCODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LMAPRNOR,LMAPRCAU,LMIVASTU,LMIVACEE,LMDETIVA;
            from (i_cTable) where;
                LMCODICE = this.oParentObject.w_ASSOCI;
                and LMHOCCAU = this.w_PNCODCAU;
                and LMHOCIVA = this.w_IVCODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_APRNORM = NVL(cp_ToDate(_read_.LMAPRNOR),cp_NullValue(_read_.LMAPRNOR))
          this.w_APRCAU = NVL(cp_ToDate(_read_.LMAPRCAU),cp_NullValue(_read_.LMAPRCAU))
          this.w_IVASTU = NVL(cp_ToDate(_read_.LMIVASTU),cp_NullValue(_read_.LMIVASTU))
          this.w_IVACEE = NVL(cp_ToDate(_read_.LMIVACEE),cp_NullValue(_read_.LMIVACEE))
          this.w_DETIVA = NVL(cp_ToDate(_read_.LMDETIVA),cp_NullValue(_read_.LMDETIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        do case
          case this.w_DETIVA="   "
            this.w_DETIVA = "1"
          case this.w_DETIVA="001"
            this.w_DETIVA = "1"
          case this.w_DETIVA="002"
            this.w_DETIVA = "2"
          case this.w_DETIVA="003"
            this.w_DETIVA = "3"
          case this.w_DETIVA="004"
            this.w_DETIVA = "4"
        endcase
        SELECT FATTACQU
        if this.w_PAGESIGDIF="S"
          * --- Calcolo del codice norma per IVA ad esigibilit� differita
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Verifica se ho il codice IVA studio definito nella norma
        if NOT EMPTY(this.w_APRNORM)
          this.w_IVASTUNOR = SPACE(2)
          * --- Read from STU_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.STU_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_NORM_idx,2],.t.,this.STU_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LMCODIVA"+;
              " from "+i_cTable+" STU_NORM where ";
                  +"LMCODNOR = "+cp_ToStrODBC(this.w_APRNORM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LMCODIVA;
              from (i_cTable) where;
                  LMCODNOR = this.w_APRNORM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_IVASTUNOR = NVL(cp_ToDate(_read_.LMCODIVA),cp_NullValue(_read_.LMCODIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          SELECT FATTACQU
          if this.w_IVASTUNOR="XX"
            this.w_IVASTUNOR = this.w_IVASTU
          endif
        else
          this.w_IVASTUNOR = this.w_IVASTU
        endif
        * --- Controllo Codice IVA
        if (EMPTY(this.w_IVASTUNOR) or this.w_IVASTUNOR="XX")
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Codice IVA non definito","               ")
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        * --- Controllo Codice IVA
        if (FATTACQU.PNTIPDOC="FE" OR FATTACQU.PNTIPDOC="NE") AND EMPTY(this.w_IVACEE)
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Codice CEE non definito","               ")
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        * --- Scrittura del codice IVA
        FWRITE(this.oParentObject.hFile,right("00"+(this.w_IVASTUNOR),2),2)
        * --- Imponibile
        if g_PERVAL=this.oParentObject.w_VALEUR
          if ! this.w_IVAIND
            FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(FATTACQU.IVIMPONI)),11,0))+RIGHT(STR(FATTACQU.IVIMPONI,12,2),2),11),11)
          else
            FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS((FATTACQU.IVIMPONI/100)*(100-this.w_PERCIND))),11,0))+RIGHT(STR((FATTACQU.IVIMPONI/100)*(100-this.w_PERCIND),12,2),2),11),11)
          endif
        else
          if ! this.w_IVAIND
            FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(FATTACQU.IVIMPONI),11,0)),11),11)
          else
            FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS((FATTACQU.IVIMPONI/100)*(100-this.w_PERCIND)),11,0)),11),11)
          endif
        endif
        if INLIST(FATTACQU.PNTIPDOC,"NC","NE","NU")
          FWRITE(this.oParentObject.hFile,IIF(FATTACQU.IVIMPONI>=0, "-", "+"),1)
        else
          FWRITE(this.oParentObject.hFile,IIF(FATTACQU.IVIMPONI>=0, "+", "-"),1)
        endif
        * --- Imposta
        if g_PERVAL=this.oParentObject.w_VALEUR
          if ! this.w_IVAIND
            FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(FATTACQU.IVIMPIVA)),11,0))+RIGHT(STR(FATTACQU.IVIMPIVA,12,2),2),11),11)
          else
            FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS((FATTACQU.IVIMPIVA/100)*(100-this.w_PERCIND))),11,0))+RIGHT(STR((FATTACQU.IVIMPIVA/100)*(100-this.w_PERCIND),12,2),2),11),11)
          endif
        else
          if ! this.w_IVAIND
            FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(FATTACQU.IVIMPIVA),11,0)),11),11)
          else
            FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS((FATTACQU.IVIMPIVA/100)*this.w_PERCIND),11,0)),11),11)
          endif
        endif
        if INLIST(FATTACQU.PNTIPDOC,"NC","NE","NU")
          FWRITE(this.oParentObject.hFile,IIF(FATTACQU.IVIMPIVA>=0, "-", "+"),1)
        else
          FWRITE(this.oParentObject.hFile,IIF(FATTACQU.IVIMPIVA>=0, "+", "-"),1)
        endif
        * --- Scrittura del Codice Norma
        if this.w_APRNORM="87"
          this.w_NORMA87 = .T.
        endif
        if this.w_APRNORM$"EN-EP"
          this.w_NORMAEP = .T.
        endif
        if this.w_APRNORM$"33-36-39-41-46-47-48" 
          this.w_NORMA33 = .T.
        endif
        if this.w_FATTDIFF and this.w_APRNORM="IS"
          if ! empty(this.w_NORMADIFF)
            FWRITE(this.oParentObject.hFile,this.w_NORMADIFF,2)
          else
            FWRITE(this.oParentObject.hFile,"DS",2)
          endif
        else
          FWRITE(this.oParentObject.hFile,right("  "+IIF(EMPTY(this.w_APRNORM),"  ",this.w_APRNORM),2),2)
        endif
        * --- Centro di costo
        FWRITE(this.oParentObject.hFile,"0000",4)
        * --- Contropartita (Sottoconto)
        this.w_ANCODSTU = SPACE(6)
        this.w_PNTIPCLF = NVL(FATTACQU.IVTIPCOP," ")
        this.w_PNCODCLF = NVL(FATTACQU.IVCONTRO," ")
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODSTU"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODSTU;
            from (i_cTable) where;
                ANTIPCON = this.w_PNTIPCLF;
                and ANCODICE = this.w_PNCODCLF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT FATTACQU
        if EMPTY(this.w_ANCODSTU)
          this.w_ANCODSTU = "000000"
          this.w_STRINGA = Ah_msgFormat("%1Errore! Contropartita %2 non ha un codice sottoconto nello studio associato","               ",NVL(this.w_PNCODCLF, " "))
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        FWRITE(this.oParentObject.hFile,right("000000"+alltrim(this.w_ANCODSTU),6),6)
        * --- Importo totale del documento - Lo inserisco solo nell'ultima riga IVA
        if this.w_PROGRIGA=this.w_NUMRIGHE
          * --- Importo totale
          if g_PERVAL=this.oParentObject.w_VALEUR
            FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOC)),11,0))+RIGHT(STR(this.w_TOTDOC,12,2),2),11),11)
          else
            FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(this.w_TOTDOC),11,0)),11),11)
          endif
          * --- Segno
          if FATTACQU.PNTIPDOC="NC" OR FATTACQU.PNTIPDOC="NE" OR FATTACQU.PNTIPDOC="NU"
            FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOC>=0, "-", "+"),1)
          else
            FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOC>=0, "+", "-"),1)
          endif
          if NOT EMPTY(this.w_TOTDOCINVAL) and g_PERVAL <> NVL(FATTACQU.PNCODVAL,"   ")
            * --- Importo in valuta
            FWRITE(this.oParentObject.hFile,;
            RIGHT("0000000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOCINVAL)),11, 0))+RIGHT(STR(this.w_TOTDOCINVAL,14,2),2),13),13)
            * --- Segno
            if FATTACQU.PNTIPDOC="NE" or FATTACQU.PNTIPDOC="NC" or FATTACQU.PNTIPDOC="NU"
              FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOCINVAL>=0, "-", "+"),1)
            else
              FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOCINVAL>=0, "+", "-"),1)
            endif
          else
            FWRITE(this.oParentObject.hFile,repl("0",13),13)
            FWRITE(this.oParentObject.hFile," ",1)
          endif
        else
          * --- Lascio blank i campi di importo totale
          FWRITE(this.oParentObject.hFile,repl("0",11),11)
          FWRITE(this.oParentObject.hFile," ",1)
          FWRITE(this.oParentObject.hFile,repl("0",13),13)
          FWRITE(this.oParentObject.hFile," ",1)
        endif
        * --- Codice Valuta
        if g_PERVAL <> NVL(FATTACQU.PNCODVAL,"   ")
          FWRITE(this.oParentObject.hFile,NVL(FATTACQU.PNCODVAL,"   "),3)
        else
          FWRITE(this.oParentObject.hFile,"   ",3)
        endif
        * --- Codice CEE
        FWRITE(this.oParentObject.hFile,IIF(FATTACQU.PNTIPDOC="FE" OR FATTACQU.PNTIPDOC="NE" AND NOT EMPTY(this.w_IVACEE), right("  "+this.w_IVACEE,2), "  "),2)
        * --- Data uno + Data due - PARCE
        FWRITE(this.oParentObject.hFile,repl("0",16),16)
        * --- Flag prestazioni di servizi
        do case
          case FATTACQU.IVFLGSER = "S"
            FWRITE(this.oParentObject.hFile,FATTACQU.IVFLGSER,1)
          case FATTACQU.IVFLGSER = "B"
            FWRITE(this.oParentObject.hFile,"N",1)
          case FATTACQU.IVFLGSER = "E"
            FWRITE(this.oParentObject.hFile," ",1)
          otherwise
            FWRITE(this.oParentObject.hFile," ",1)
        endcase
        * --- Codice contratto
        if g_AIFT="S"
          FWRITE(this.oParentObject.hFile, left ( NVL(FATTACQU.OSRIFCON," ") + space(30),30) , 30)
        else
          this.oParentObject.w_FILLER = SPACE(30)
          FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,30)
        endif
        * --- Identificativo documento originario per note di retifica
        if NVL (FATTACQU.OSTIPFAT, " ") ="N" AND g_AIFT="S"
          FWRITE(this.oParentObject.hFile,left ( NVL(FATTACQU.OSRIFFAT," ") + space (13) ,13),13)
        else
          this.oParentObject.w_FILLER = SPACE(13)
          FWRITE(this.oParentObject.hFile , this.oParentObject.w_FILLER ,13)
        endif
        * --- Filler
        this.oParentObject.w_FILLER = SPACE(5)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,5)
        * --- Operazioni di acquisto con San Marino
        FWRITE(this.oParentObject.hFile,this.w_ACQSMARINO,1)
        this.w_ACQSMARINO = SPACE(1)
        this.oParentObject.w_FILLER = SPACE(62)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,62)
      else
        if (NVL(FATTACQU.IVCODIVA,SPACE(5))<>SPACE(5) AND (NVL(FATTACQU.IVIMPONI,0)<>0 OR NVL(FATTACQU.IVIMPIVA,0)<>0 OR NVL(FATTACQU.IVCFLOMA," ")="S")) AND EMPTY(NVL(FATTACQU.IVCONTRO,"  ")) 
          * --- Marco questa registrazione come non valida
          this.w_REGNOVALIDA = .T.
        endif
      endif
      if this.w_REGNOVALIDA
        go this.w_ACTUALPOS
        this.w_NOTRAS = -1
        * --- Insert into STU_PNTT
        i_nConn=i_TableProp[this.STU_PNTT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NOTRAS),'STU_PNTT','LMNUMTRA');
          +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.w_NOTRAS,'LMNUMTR2',0)
          insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.w_NOTRAS;
               ,0;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='GSLM_BFA: Scrittura con errore in STU_PNTT'
          return
        endif
        this.w_STRINGA = Ah_MsgFormat("%1Errore! Contropartita contabile non definita su riga castelletto IVA","               ")
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        SELECT FATTACQU
        do while FATTACQU.PNSERIAL=this.w_PNSERIAL
          * --- Avanzo il puntatore
          skip 1
        enddo
        this.w_ESCI = .T.
        this.w_OKTRAS = .F.
      endif
      * --- Se sono in presenza di IVA detraibili procedo all'inserimento di una nuova riga, solo per registrazioni inerenti a IVA per cassa
      if this.w_IVAIND and this.w_GESTNORMA
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Avanzo il puntatore
      if .not. this.w_ESCI
        skip 1
      endif
    enddo
    if this.w_OKTRAS
      go this.w_ACTUALPOS
      do while FATTACQU.PNSERIAL=this.w_PNSERIAL
        skip 1
      enddo
      * --- Insert into STU_PNTT
      i_nConn=i_TableProp[this.STU_PNTT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'STU_PNTT','LMNUMTRA');
        +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.oParentObject.w_PROFIL,'LMNUMTR2',0)
        insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
           values (;
             this.w_PNSERIAL;
             ,this.oParentObject.w_PROFIL;
             ,0;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='GSLM_BFA: Scrittura in STU_PNTT'
        return
      endif
      SELECT FATTACQU
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo del codice norma
    * --- Dichiarazione variabili per calcolo della norma
    this.w_CODTRAC = "LEMCO"
    * --- Select from GSLM_NOR
    do vq_exec with 'GSLM_NOR',this,'_Curs_GSLM_NOR','',.f.,.t.
    if used('_Curs_GSLM_NOR')
      select _Curs_GSLM_NOR
      locate for 1=1
      do while not(eof())
      * --- Leggo le date di registrazione e documento
      this.w_DATREGOR = cp_todate(_Curs_GSLM_NOR.DATREGOR)
      this.w_DATDOCOR = cp_todate(_Curs_GSLM_NOR.DATDOCOR)
      this.w_DATREGIP = cp_todate(_Curs_GSLM_NOR.DATREGIP)
      this.w_DATDOCIP = cp_todate(_Curs_GSLM_NOR.DATDOCIP)
      * --- Leggo il codice  causale della registrazione di origine
      this.w_CODCAUOR = _Curs_GSLM_NOR.PNCODCAU
      this.w_IVCODIVA = NVL(_Curs_GSLM_NOR.IVCODIVA," ")
      * --- Verifico se devo gestire l'iva per cassa
      * --- Read from STU_TRAS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.STU_TRAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "LMAPRNOR"+;
          " from "+i_cTable+" STU_TRAS where ";
              +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
              +" and LMHOCCAU = "+cp_ToStrODBC(this.w_CODCAUOR);
              +" and LMHOCIVA = "+cp_ToStrODBC(this.w_IVCODIVA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          LMAPRNOR;
          from (i_cTable) where;
              LMCODICE = this.oParentObject.w_ASSOCI;
              and LMHOCCAU = this.w_CODCAUOR;
              and LMHOCIVA = this.w_IVCODIVA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODNORCAU = NVL(cp_ToDate(_read_.LMAPRNOR),cp_NullValue(_read_.LMAPRNOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if upper(this.w_NORFATT) == upper(this.w_CODNORCAU)
        this.w_GESTNORMA = .T.
      else
        this.w_GESTNORMA = .F.
        if inlist(this.w_CODFATTSI,"S","I") 
          this.w_APRNORM = this.w_NORMANOIVA
        endif
      endif
      exit
        select _Curs_GSLM_NOR
        continue
      enddo
      use
    endif
    if this.w_GESTNORMA
      * --- Applico la norma in base alla modalit� di utilizzo che viene soddisfatta
      do case
        case year(this.w_DATREGOR) = year(this.w_DATREGIP) and abs((this.w_DATDOCOR - this.w_DATREGIP)) <= 365
          * --- Modalit� 2 nella tabella
          * --- Read from COD_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COD_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COD_NORM_idx,2],.t.,this.COD_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNNORMA2"+;
              " from "+i_cTable+" COD_NORM where ";
                  +"CNCODTRA = "+cp_ToStrODBC(this.w_CODTRAC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNNORMA2;
              from (i_cTable) where;
                  CNCODTRA = this.w_CODTRAC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APRNORM = NVL(cp_ToDate(_read_.CNNORMA2),cp_NullValue(_read_.CNNORMA2))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        case year(this.w_DATREGOR) < year(this.w_DATREGIP) and abs((this.w_DATDOCOR - this.w_DATREGIP)) <= 365
          * --- Modalit� 3 nella tabella
          * --- Read from COD_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COD_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COD_NORM_idx,2],.t.,this.COD_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNNORMA3"+;
              " from "+i_cTable+" COD_NORM where ";
                  +"CNCODTRA = "+cp_ToStrODBC(this.w_CODTRAC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNNORMA3;
              from (i_cTable) where;
                  CNCODTRA = this.w_CODTRAC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APRNORM = NVL(cp_ToDate(_read_.CNNORMA3),cp_NullValue(_read_.CNNORMA3))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        case year(this.w_DATREGOR) = year(this.w_DATREGIP) and abs((this.w_DATDOCOR - this.w_DATREGIP)) > 365
          * --- Modalit� 4 nella tabella
          * --- Read from COD_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COD_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COD_NORM_idx,2],.t.,this.COD_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNNORMA4"+;
              " from "+i_cTable+" COD_NORM where ";
                  +"CNCODTRA = "+cp_ToStrODBC(this.w_CODTRAC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNNORMA4;
              from (i_cTable) where;
                  CNCODTRA = this.w_CODTRAC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APRNORM = NVL(cp_ToDate(_read_.CNNORMA4),cp_NullValue(_read_.CNNORMA4))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        case year(this.w_DATREGOR) < year(this.w_DATREGIP) and abs((this.w_DATDOCOR - this.w_DATREGIP)) > 365
          * --- Modalit� 5 nella tabella
          * --- Read from COD_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COD_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COD_NORM_idx,2],.t.,this.COD_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNNORMA5"+;
              " from "+i_cTable+" COD_NORM where ";
                  +"CNCODTRA = "+cp_ToStrODBC(this.w_CODTRAC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNNORMA5;
              from (i_cTable) where;
                  CNCODTRA = this.w_CODTRAC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APRNORM = NVL(cp_ToDate(_read_.CNNORMA5),cp_NullValue(_read_.CNNORMA5))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
      endcase
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiungo la riga relativa a IVA indetraibile per codice iva sospesa e iva indetraibile 
    this.w_PROGRIGA = this.w_PROGRIGA+1
    if NOT EMPTY(FATTACQU.IVCONTRO) AND NVL(FATTACQU.IVIMPONI,0)<>0
      * --- Incremento numero di record scritti
      this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
      this.oParentObject.w_FATTACQU = this.oParentObject.w_FATTACQU+1
      * --- Se sono in questo caso devo scrivere testata e dettaglio
      FWRITE (this.oParentObject.hFile , "D31" , 3 )
      * --- Scrittura del codice IVA
      FWRITE(this.oParentObject.hFile,right("00"+(this.w_IVASTUNOR),2),2)
      * --- Imponibile
      if g_PERVAL=this.oParentObject.w_VALEUR
        if ! this.w_IVAIND
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(FATTACQU.IVIMPONI)),11,0))+RIGHT(STR(FATTACQU.IVIMPONI,12,2),2),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS((FATTACQU.IVIMPONI/100)*this.w_PERCIND)),11,0))+RIGHT(STR((FATTACQU.IVIMPONI/100)*this.w_PERCIND,12,2),2),11),11)
        endif
      else
        if ! this.w_IVAIND
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(FATTACQU.IVIMPONI),11,0)),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS((FATTACQU.IVIMPONI/100)*this.w_PERCIND),11,0)),11),11)
        endif
      endif
      if FATTACQU.PNTIPDOC="NC" OR FATTACQU.PNTIPDOC="NE" or FATTACQU.PNTIPDOC="NU"
        FWRITE(this.oParentObject.hFile,IIF(FATTACQU.IVIMPONI>=0, "-", "+"),1)
      else
        FWRITE(this.oParentObject.hFile,IIF(FATTACQU.IVIMPONI>=0, "+", "-"),1)
      endif
      * --- Imposta
      if g_PERVAL=this.oParentObject.w_VALEUR
        if ! this.w_IVAIND
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(FATTACQU.IVIMPIVA)),11,0))+RIGHT(STR(FATTACQU.IVIMPIVA,12,2),2),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS((FATTACQU.IVIMPIVA/100)*this.w_PERCIND)),11,0))+RIGHT(STR((FATTACQU.IVIMPIVA/100)*this.w_PERCIND,12,2),2),11),11)
        endif
      else
        if ! this.w_IVAIND
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(FATTACQU.IVIMPIVA),11,0)),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS((FATTACQU.IVIMPIVA/100)*this.w_PERCIND),11,0)),11),11)
        endif
      endif
      if FATTACQU.PNTIPDOC="NC" OR FATTACQU.PNTIPDOC="NE" or FATTACQU.PNTIPDOC="NU"
        FWRITE(this.oParentObject.hFile,IIF(FATTACQU.IVIMPIVA>=0, "-", "+"),1)
      else
        FWRITE(this.oParentObject.hFile,IIF(FATTACQU.IVIMPIVA>=0, "+", "-"),1)
      endif
      * --- Scrittura del Codice Norma
      if this.w_PAGESIGDIF#"S"
        if EMPTY(this.w_NORMAIND100)
          FWRITE(this.oParentObject.hFile,"90",2)
        else
          FWRITE(this.oParentObject.hFile,this.w_NORMAIND100,2)
        endif
      else
        if EMPTY(this.w_NORMANOIVA)
          FWRITE(this.oParentObject.hFile,"IG",2)
        else
          FWRITE(this.oParentObject.hFile,this.w_NORMANOIVA,2)
        endif
      endif
      * --- Centro di costo
      FWRITE(this.oParentObject.hFile,"0000",4)
      * --- Contropartita (Sottoconto)
      this.w_ANCODSTU = SPACE(6)
      this.w_PNTIPCLF = NVL(FATTACQU.IVTIPCOP," ")
      this.w_PNCODCLF = NVL(FATTACQU.IVCONTRO," ")
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCLF;
              and ANCODICE = this.w_PNCODCLF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT FATTACQU
      if EMPTY(this.w_ANCODSTU)
        this.w_ANCODSTU = "000000"
        this.w_STRINGA = Ah_msgFormat("%1Errore! Contropartita %2 non ha un codice sottoconto nello studio associato","               ",NVL(this.w_PNCODCLF, " "))
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      FWRITE(this.oParentObject.hFile,right("000000"+alltrim(this.w_ANCODSTU),6),6)
      * --- Importo totale del documento - Lo inserisco solo nell'ultima riga IVA
      if this.w_PROGRIGA=this.w_NUMRIGHE
        * --- Importo totale
        if g_PERVAL=this.oParentObject.w_VALEUR
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOC)),11,0))+RIGHT(STR(this.w_TOTDOC,12,2),2),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(this.w_TOTDOC),11,0)),11),11)
        endif
        * --- Segno
        if FATTACQU.PNTIPDOC="NC" OR FATTACQU.PNTIPDOC="NE" OR FATTACQU.PNTIPDOC="NU"
          FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOC>=0, "-", "+"),1)
        else
          FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOC>=0, "+", "-"),1)
        endif
        if NOT EMPTY(this.w_TOTDOCINVAL) and g_PERVAL <> NVL(FATTACQU.PNCODVAL,"   ")
          * --- Importo in valuta
          FWRITE(this.oParentObject.hFile,;
          RIGHT("0000000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOCINVAL)),11, 0))+RIGHT(STR(this.w_TOTDOCINVAL,14,2),2),13),13)
          * --- Segno
          if FATTACQU.PNTIPDOC="NE" or FATTACQU.PNTIPDOC="NC" or FATTACQU.PNTIPDOC="NU"
            FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOCINVAL>=0, "-", "+"),1)
          else
            FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOCINVAL>=0, "+", "-"),1)
          endif
        else
          FWRITE(this.oParentObject.hFile,repl("0",13),13)
          FWRITE(this.oParentObject.hFile," ",1)
        endif
      else
        * --- Lascio blank i campi di importo totale
        FWRITE(this.oParentObject.hFile,repl("0",11),11)
        FWRITE(this.oParentObject.hFile," ",1)
        FWRITE(this.oParentObject.hFile,repl("0",13),13)
        FWRITE(this.oParentObject.hFile," ",1)
      endif
      * --- Codice Valuta
      if g_PERVAL <> NVL(FATTACQU.PNCODVAL,"   ")
        FWRITE(this.oParentObject.hFile,NVL(FATTACQU.PNCODVAL,"   "),3)
      else
        FWRITE(this.oParentObject.hFile,"   ",3)
      endif
      * --- Codice CEE
      FWRITE(this.oParentObject.hFile,IIF(FATTACQU.PNTIPDOC="FE" OR FATTACQU.PNTIPDOC="NE" AND NOT EMPTY(this.w_IVACEE), right("  "+this.w_IVACEE,2), "  "),2)
      * --- Data uno + Data due - PARCE
      FWRITE(this.oParentObject.hFile,repl("0",16),16)
      * --- Flag prestazioni di servizi
      do case
        case FATTACQU.IVFLGSER = "S"
          FWRITE(this.oParentObject.hFile,FATTACQU.IVFLGSER,1)
        case FATTACQU.IVFLGSER = "B"
          FWRITE(this.oParentObject.hFile,"N",1)
        case FATTACQU.IVFLGSER = "E"
          FWRITE(this.oParentObject.hFile," ",1)
        otherwise
          FWRITE(this.oParentObject.hFile," ",1)
      endcase
      * --- Codice contratto
      if g_AIFT="S"
        FWRITE(this.oParentObject.hFile, left ( NVL(FATTACQU.OSRIFCON," ") + space(30),30) , 30)
      else
        this.oParentObject.w_FILLER = SPACE(30)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,30)
      endif
      * --- Identificativo documento originario per note di retifica
      if NVL (FATTACQU.OSTIPFAT, " ") ="N"
        FWRITE(this.oParentObject.hFile,left ( NVL(FATTACQU.OSRIFFAT , " ") + space (13) ,13),13)
      else
        this.oParentObject.w_FILLER = SPACE(13)
        FWRITE(this.oParentObject.hFile , this.oParentObject.w_FILLER ,13)
      endif
      * --- Filler
      this.oParentObject.w_FILLER = SPACE(68)
      FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,68)
    else
      if (NVL(FATTACQU.IVCODIVA,SPACE(5))<>SPACE(5) AND (NVL(FATTACQU.IVIMPONI,0)<>0 OR NVL(FATTACQU.IVIMPIVA,0)<>0 OR NVL(FATTACQU.IVCFLOMA," ")="S")) AND EMPTY(NVL(FATTACQU.IVCONTRO,"  ")) 
        * --- Marco questa registrazione come non valida
        this.w_REGNOVALIDA = .T.
      endif
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Incremento numero di record scritti
    this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
    this.oParentObject.w_FATTACQU = this.oParentObject.w_FATTACQU+1
    * --- Se sono in questo caso devo scrivere testata e dettaglio
    FWRITE (this.oParentObject.hFile , "D33" , 3 )
    * --- Numero protocollo scanner
    FWRITE (this.oParentObject.hFile , Space(20) , 20 )
    * --- File allegato
    FWRITE (this.oParentObject.hFile , Space(30) , 30 )
    * --- Codice classificazione
    FWRITE (this.oParentObject.hFile , Space(2) , 2 )
    * --- Flag Split Payment
    FWRITE (this.oParentObject.hFile , this.w_SPLIT , 1 )
    * --- Filler
    FWRITE (this.oParentObject.hFile , " " , 1 )
    * --- Detrazione IVA
    * --- -	Verifica che la data documento sia maggiore o uguale a 01/01/2017 e l�anno della data registrazione sia maggiore dell�anno della data documento. Se la condizione � verificata mantenere il valore presente nel file altrimenti 
    *     impostare fisso 1;
    *     -	se CodTipoDocm = 4 Documento non IVA emettere messaggio di segnalazione �Msg157: Informazione Detrazione IVA acquisti incompatibile Documento non IVA: Movimento comunque trasferito� 
    *     ed importare il documento impostando TFAT/CodDetrIVAAcq = 1; 
    *     -	se CoDTipoIvaSplitPaym del file di interfaccia = S emettere messaggio di segnalazione �Msg158: Informazione Detrazione IVA acquisti incompatibile con fatt. split payment: Movimento comunque trasferito� 
    *     ed importare il documento impostando TFAT/CodDetrIVAAcq = 1;
    *     -	se CodFattSospIncs = S emettere messaggio di segnalazione �Msg159: Informazione Detrazione IVA acquisti incompatibile con fatt.soggetta a IVA per cassa: Movimento comunque trasferito� 
    *     ed importare il documento impostando TFAT/CodDetrIVAAcq = 1; se CodFattSospIncs = I oppure C importare il documento impostando TFAT/CodDetrIVAAcq = 1;
    *     -	se il campo L5STRU/CodNor di almeno una riga = 87 emettere messaggio di segnalazione �Msg160: Informazione Detrazione IVA acquisti incompatibile con fatt.a enti: Movimento comunque trasferito� 
    *     ed importare il documento impostando TFAT/CodDetrIVAAcq = 1; se il campo L5STRU/CodNor di almeno una riga = EN o EP importare il documento impostando TFAT/CodDetrIVAAcq = 1;
    *     -	se il campo L5STRU/CodNor di almeno una riga = 33, 36, 39, 41, 46, 47, 48 emettere messaggio di segnalazione �Msg161: Informazione Detrazione IVA acquisti incompatibile con fatt.acquisto esportatore:
    *      Movimento comunque trasferito� ed importare il documento impostando TFAT/CodDetrIVAAcq = 1.
    this.w_DETRAZIONE = .F.
    if this.w_DETIVA<>"1"
      if !(this.w_PNDATDOC>=cp_chartodate("01/01/2017") and YEAR(this.w_PNDATREG)>YEAR(this.w_PNDATDOC))
        this.w_DETRAZIONE = .T.
      endif
      if this.w_SPLIT="S"
        this.w_STRINGA = Ah_MsgFormat("Msg158: Informazione Detrazione IVA acquisti incompatibile con fatt. split payment: Movimento comunque trasferito")
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.w_DETRAZIONE = .T.
      endif
      if this.w_ESIGDIF="S"
        this.w_STRINGA = Ah_MsgFormat("Msg159: Informazione Detrazione IVA acquisti incompatibile con fatt.soggetta a IVA per cassa: Movimento comunque trasferito")
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.w_DETRAZIONE = .T.
      endif
      if this.w_NORMA87
        this.w_STRINGA = Ah_MsgFormat("Msg160: Informazione Detrazione IVA acquisti incompatibile con fatt.a enti: Movimento comunque trasferito")
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.w_DETRAZIONE = .T.
      endif
      if this.w_NORMAEP
        this.w_DETRAZIONE = .T.
      endif
      if this.w_NORMA33
        this.w_STRINGA = Ah_MsgFormat("Msg161: Informazione Detrazione IVA acquisti incompatibile con fatt.acquisto esportatore: Movimento comunque trasferito")
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.w_DETRAZIONE = .T.
      endif
    endif
    if this.w_DETRAZIONE
      FWRITE (this.oParentObject.hFile , "1" , 1 )
    else
      FWRITE (this.oParentObject.hFile ,this.w_DETIVA , 1 )
    endif
    * --- Filler 142
    FWRITE (this.oParentObject.hFile , Space(142) , 142 )
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='STU_NORM'
    this.cWorkTables[3]='STU_PNTT'
    this.cWorkTables[4]='STU_TRAS'
    this.cWorkTables[5]='PNT_IVA'
    this.cWorkTables[6]='ATTIDETT'
    this.cWorkTables[7]='ATTIMAST'
    this.cWorkTables[8]='AZIENDA'
    this.cWorkTables[9]='COD_NORM'
    this.cWorkTables[10]='VOCIIVA'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_GSLM_NOR')
      use in _Curs_GSLM_NOR
    endif
    if used('_Curs_GSLM_NOR')
      use in _Curs_GSLM_NOR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
