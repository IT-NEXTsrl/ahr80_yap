* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_bcs                                                        *
*              Elenco conti piano dei conti studio                             *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-28                                                      *
* Last revis.: 2006-02-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm_bcs",oParentObject)
return(i_retval)

define class tgslm_bcs as StdBatch
  * --- Local variables
  w_LMCODCON = space(3)
  w_LMDESCON = space(30)
  w_LMPROGRE = space(8)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Avvio lo zoom di selezione del conto dalla lista dei piani dei conti importati
    this.w_LMCODCON = space(3)
    vx_exec("..\lemc\exe\query\CONTIPIA.vzm",this)
    if ! empty(this.w_LMCODCON)
      this.oParentObject.w_MCCODSTU = NVL(this.w_LMCODCON," ")
      this.oParentObject.w_DESCON = NVL(this.w_LMDESCON," ")
      this.oParentObject.w_MCPROSTU = NVL(this.w_LMPROGRE," ")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
