* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_bfv                                                        *
*              Export fatture vendita                                          *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_299]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-24                                                      *
* Last revis.: 2008-09-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm_bfv",oParentObject)
return(i_retval)

define class tgslm_bfv as StdBatch
  * --- Local variables
  w_REGNOVALIDA = .f.
  w_STRINGA = space(120)
  w_ACTUALPOS = 0
  w_PNSERIAL = space(10)
  w_TOTDOC = 0
  w_TOTDOCINVAL = 0
  w_ANCODSTU = space(5)
  w_APRNORM = space(2)
  w_APRCAU = space(3)
  w_IVASTU = space(2)
  w_IVASTUNOR = space(2)
  w_IVACEE = space(2)
  w_ESCI = .f.
  w_OKTRAS = .f.
  w_NOTRAS = 0
  w_IVCODIVA = space(5)
  w_PNCODCAU = space(5)
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_NUMRIGHE = 0
  w_PROGRIGA = 0
  w_CODCLI = space(15)
  * --- WorkFile variables
  CONTI_idx=0
  STU_NORM_idx=0
  STU_PNTT_idx=0
  STU_TRAS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT VERSO LO STUDIO DELLE FATTURE DI VENDITA e NOTE DI CREDITO
    SELECT FATTVEND
    GO TOP
    * --- Ciclo sul cursore della fatture di vendita
    do while NOT EOF()
      * --- Messaggio a schermo
      this.w_REGNOVALIDA = .F.
      ah_Msg("Export fatture di vendita: reg. num. %1 - data %2",.T.,.F.,.F.,ALLTRIM(STR(FATTVEND.PNNUMRER,6,0)),dtoc(FATTVEND.PNDATREG))
      * --- Scrittura su Log
      this.w_STRINGA = SPACE(10)+Ah_MsgFormat("Export fatture di vendita: reg. num. %1 - data %2",ALLTRIM(STR(FATTVEND.PNNUMRER,6,0)),dtoc(FATTVEND.PNDATREG))
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      * --- Salvo la posizione attuale del record e il seriale della registrazione
      this.w_ACTUALPOS = RECNO()
      this.w_PNSERIAL = FATTVEND.PNSERIAL
      this.w_TOTDOC = 0
      this.w_NUMRIGHE = 0
      * --- Calcolo l'importo totale del documento
      do while FATTVEND.PNSERIAL=this.w_PNSERIAL
        this.w_NUMRIGHE = this.w_NUMRIGHE+1
        this.w_TOTDOC = FATTVEND.IVIMPONI+FATTVEND.IVIMPIVA+this.w_TOTDOC
        skip 1
      enddo
      this.w_TOTDOCINVAL = FATTVEND.PNTOTDOC
      * --- Ripristino posizione
      go this.w_ACTUALPOS
      * --- Record di testata
      this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
      this.oParentObject.w_FATTVEND = this.oParentObject.w_FATTVEND+1
      FWRITE (this.oParentObject.hFile , "D30" , 3 )
      * --- Recupero e inserisco l' anno e il mese della registrazione
      FWRITE(this.oParentObject.hFile,LEFT(dtos(FATTVEND.PNDATREG),6),6)
      * --- Recupero e inserisco l' anno di competenza
      FWRITE(this.oParentObject.hFile,FATTVEND.PNCOMPET,4)
      * --- Recupero e inserisco il mese di competenza
      FWRITE(this.oParentObject.hFile,SUBSTR(dtos(FATTVEND.PNCOMIVA),5,2),2)
      * --- Sezione
      FWRITE(this.oParentObject.hFile,RIGHT("00"+ALLTRIM(STR(FATTVEND.IVNUMREG-1,2,0)),2),2)
      * --- Tipo del documento - Fattura = 1 ; NC = 2
      FWRITE(this.oParentObject.hFile,IIF(FATTVEND.PNTIPDOC="NC" OR FATTVEND.PNTIPDOC="NE", "2", "1"),1)
      * --- Tipo fattura
      FWRITE(this.oParentObject.hFile,SPACE(2),2)
      * --- Numero Documento - Vendita = Numero Documento
      FWRITE(this.oParentObject.hFile,RIGHT("0000000"+ALLTRIM(STR(FATTVEND.PNNUMDOC)),7),7)
      FWRITE(this.oParentObject.hFile,RIGHT(" "+ALLTRIM(FATTVEND.PNALFDOC),1),1)
      * --- Numero fattura fornitore = Numero documento
      FWRITE(this.oParentObject.hFile,RIGHT(SPACE(7)+ALLTRIM(STR(FATTVEND.PNNUMDOC))+ALLTRIM(FATTVEND.PNALFDOC),7),7)
      * --- Date
      FWRITE(this.oParentObject.hFile,dtos(FATTVEND.PNDATREG),8)
      FWRITE(this.oParentObject.hFile,NVL(dtos(FATTVEND.PNDATDOC),"        "),8)
      * --- Data scadenza, Riscontro, Rateo - Non Gestite
      FWRITE(this.oParentObject.hFile,repl("0",8),8)
      FWRITE(this.oParentObject.hFile,repl("0",8),8)
      FWRITE(this.oParentObject.hFile,repl("0",8),8)
      * --- Codice Pagamento e tipo pagamento - Non gestite
      FWRITE(this.oParentObject.hFile,"000",3)
      FWRITE(this.oParentObject.hFile," ",1)
      * --- Codice cliente/fornitore
      this.w_ANCODSTU = SPACE(5)
      this.w_PNTIPCLF = FATTVEND.PNTIPCLF
      this.w_PNCODCLF = FATTVEND.PNCODCLF
      if this.w_PNTIPCLF="F"
        * --- Se l'intestatario � un fornitore, allora si tratta di un movimento derivante da una fattura ue d'acquisto
        * --- Il fornitore deve essere sostituito con il cliente collegato
        this.w_CODCLI = SPACE(15)
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCONRIF"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCONRIF;
            from (i_cTable) where;
                ANTIPCON = this.w_PNTIPCLF;
                and ANCODICE = this.w_PNCODCLF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODCLI = NVL(cp_ToDate(_read_.ANCONRIF),cp_NullValue(_read_.ANCONRIF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_PNTIPCLF = "C"
        this.w_PNCODCLF = this.w_CODCLI
      endif
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCLF;
              and ANCODICE = this.w_PNCODCLF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT FATTVEND
      if NOT EMPTY(this.w_ANCODSTU)
        * --- Codice Cli/For
        FWRITE(this.oParentObject.hFile,this.w_ANCODSTU,5)
        * --- Tipo codifica cli/for
        FWRITE(this.oParentObject.hFile,IIF(this.w_PNTIPCLF="C","P","R"),1)
        * --- Partita IVA
        FWRITE(this.oParentObject.hFile,"00000000000",11)
      else
        this.w_STRINGA = Ah_MsgFormat("%1Errore! Cliente %2 non ha un codice cliente nello studio associato","               ",NVL(this.w_PNCODCLF, " "))
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      SELECT FATTVEND
      * --- Descrizione alternativa aggiuntiva
      FWRITE(this.oParentObject.hFile,SPACE(29),29)
      FWRITE(this.oParentObject.hFile," ",1)
      * --- Flag Partita - Non Gestito
      FWRITE(this.oParentObject.hFile," ",1)
      * --- Mese stampa su reg.IVA per autotrasportatori
      FWRITE(this.oParentObject.hFile , "00" , 2)
      * --- Recupero valuta di conto
      FWRITE(this.oParentObject.hFile , IIF(g_PERVAL=this.oParentObject.w_VALEUR, "E", " ") , 1)
      * --- Importo altre ritenute- Non Gestito
      FWRITE(this.oParentObject.hFile , "00000000000" , 11)
      * --- Importo ritenuta di acconto - Non Gestito
      FWRITE(this.oParentObject.hFile , "00000000000" , 11)
      * --- Non Contabilizzare: a 'N' solo nel caso di Storno Iva Incasso Fattura Esigibilit� Differita
      * --- La query filtra le registrazioni in funzione del tipo documento, nel caso tipo documento='NO' deve essere  necessariamente un pagamento ad esigibilt� differita
      *     (CAU_CONT.CCFLPDIF='S')
      FWRITE(this.oParentObject.hFile,IIF(FATTVEND.PNTIPDOC="NO", "N", " "),1)
      * --- Filler
      this.oParentObject.w_FILLER = SPACE(47)
      FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,47)
      * --- Ciclo su tutte le righe IVA
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT FATTVEND
    enddo
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Butto giu' tutte le righe IVA della fattura in questione
    this.w_ESCI = .F.
    this.w_OKTRAS = .T.
    this.w_PROGRIGA = 0
    * --- Posiziono il puntatore al record
    SELECT FATTVEND
    this.w_ACTUALPOS = RECNO()
    do while FATTVEND.PNSERIAL=this.w_PNSERIAL AND NOT this.w_ESCI
      this.w_PROGRIGA = this.w_PROGRIGA+1
      if NOT EMPTY(FATTVEND.IVCONTRO)
        * --- Incremento numero di record scritti
        this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
        this.oParentObject.w_FATTVEND = this.oParentObject.w_FATTVEND+1
        * --- Se sono in questo caso devo scrivere testata e dettaglio
        FWRITE (this.oParentObject.hFile , "D31" , 3 )
        * --- Recupero e scrittura del codice di Aliquota IVA
        this.w_APRCAU = SPACE(3)
        this.w_APRNORM = SPACE(2)
        this.w_IVASTU = SPACE(2)
        this.w_IVACEE = SPACE(2)
        this.w_PNCODCAU = FATTVEND.PNCODCAU
        this.w_IVCODIVA = FATTVEND.IVCODIVA
        * --- Read from STU_TRAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.STU_TRAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LMAPRNOR,LMAPRCAU,LMIVASTU,LMIVACEE"+;
            " from "+i_cTable+" STU_TRAS where ";
                +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
                +" and LMHOCCAU = "+cp_ToStrODBC(this.w_PNCODCAU);
                +" and LMHOCIVA = "+cp_ToStrODBC(this.w_IVCODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LMAPRNOR,LMAPRCAU,LMIVASTU,LMIVACEE;
            from (i_cTable) where;
                LMCODICE = this.oParentObject.w_ASSOCI;
                and LMHOCCAU = this.w_PNCODCAU;
                and LMHOCIVA = this.w_IVCODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_APRNORM = NVL(cp_ToDate(_read_.LMAPRNOR),cp_NullValue(_read_.LMAPRNOR))
          this.w_APRCAU = NVL(cp_ToDate(_read_.LMAPRCAU),cp_NullValue(_read_.LMAPRCAU))
          this.w_IVASTU = NVL(cp_ToDate(_read_.LMIVASTU),cp_NullValue(_read_.LMIVASTU))
          this.w_IVACEE = NVL(cp_ToDate(_read_.LMIVACEE),cp_NullValue(_read_.LMIVACEE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT FATTVEND
        * --- Verifica se ho il codice IVA studio definito nella norma
        if NOT EMPTY(this.w_APRNORM)
          this.w_IVASTUNOR = SPACE(2)
          * --- Read from STU_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.STU_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_NORM_idx,2],.t.,this.STU_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LMCODIVA"+;
              " from "+i_cTable+" STU_NORM where ";
                  +"LMCODNOR = "+cp_ToStrODBC(this.w_APRNORM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LMCODIVA;
              from (i_cTable) where;
                  LMCODNOR = this.w_APRNORM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_IVASTUNOR = NVL(cp_ToDate(_read_.LMCODIVA),cp_NullValue(_read_.LMCODIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          SELECT FATTVEND
          if this.w_IVASTUNOR="XX"
            this.w_IVASTUNOR = this.w_IVASTU
          endif
        else
          this.w_IVASTUNOR = this.w_IVASTU
        endif
        * --- Controllo Codice IVA
        if (EMPTY(this.w_IVASTUNOR) or this.w_IVASTUNOR="XX")
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Codice IVA non definito","               ")
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        * --- Controllo Codice IVA
        if (FATTVEND.PNTIPDOC="FE" OR FATTVEND.PNTIPDOC="NE") AND EMPTY(this.w_IVACEE)
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Codice CEE non definito","               ")
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        * --- Scrittura del codice IVA
        FWRITE(this.oParentObject.hFile,(this.w_IVASTUNOR+"00"),4)
        * --- Imponibile
        if g_PERVAL=this.oParentObject.w_VALEUR
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(FATTVEND.IVIMPONI)),11,0))+RIGHT(STR(FATTVEND.IVIMPONI,12,2),2),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(FATTVEND.IVIMPONI),11,0)),11),11)
        endif
        if FATTVEND.PNTIPDOC="NC" OR FATTVEND.PNTIPDOC="NE"
          FWRITE(this.oParentObject.hFile,IIF(FATTVEND.IVIMPONI>=0, "-", "+"),1)
        else
          FWRITE(this.oParentObject.hFile,IIF(FATTVEND.IVIMPONI>=0, "+", "-"),1)
        endif
        * --- Imposta
        if g_PERVAL=this.oParentObject.w_VALEUR
          FWRITE(this.oParentObject.hFile,RIGHT("000000000"+ALLTRIM(STR(INT(ABS(FATTVEND.IVIMPIVA)),9,0))+RIGHT(STR(FATTVEND.IVIMPIVA,10,2),2),9),9)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("000000000"+ALLTRIM(STR(ABS(FATTVEND.IVIMPIVA),9,0)),9),9)
        endif
        if FATTVEND.PNTIPDOC="NC" OR FATTVEND.PNTIPDOC="NE"
          FWRITE(this.oParentObject.hFile,IIF(FATTVEND.IVIMPIVA>=0, "-", "+"),1)
        else
          FWRITE(this.oParentObject.hFile,IIF(FATTVEND.IVIMPIVA>=0, "+", "-"),1)
        endif
        * --- Scrittura codice norma
        FWRITE(this.oParentObject.hFile,IIF(EMPTY(this.w_APRNORM),"  ",this.w_APRNORM),2)
        * --- Centro di costo
        FWRITE(this.oParentObject.hFile,"00",2)
        * --- Contropartita (Sottoconto)
        this.w_ANCODSTU = SPACE(5)
        this.w_PNTIPCLF = FATTVEND.IVTIPCOP
        this.w_PNCODCLF = FATTVEND.IVCONTRO
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODSTU"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODSTU;
            from (i_cTable) where;
                ANTIPCON = this.w_PNTIPCLF;
                and ANCODICE = this.w_PNCODCLF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT FATTVEND
        if EMPTY(this.w_ANCODSTU)
          this.w_ANCODSTU = "00000"
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Contropartita %2 non ha un codice sottoconto nello studio associato","               ",NVL(this.w_PNCODCLF, " "))
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        FWRITE(this.oParentObject.hFile,this.w_ANCODSTU,5)
        * --- Importo totale del documento - Lo inserisco solo nell'ultima riga IVA
        if this.w_PROGRIGA=this.w_NUMRIGHE
          * --- Importo totale
          if g_PERVAL=this.oParentObject.w_VALEUR
            FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOC)),11,0))+RIGHT(STR(this.w_TOTDOC,12,2),2),11),11)
          else
            FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(this.w_TOTDOC),11,0)),11),11)
          endif
          * --- Segno
          if FATTVEND.PNTIPDOC="NC" OR FATTVEND.PNTIPDOC="NE"
            FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOC>=0, "-", "+"),1)
          else
            FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOC>=0, "+", "-"),1)
          endif
          if NOT EMPTY(this.w_TOTDOCINVAL)
            * --- Importo in valuta
            if g_PERVAL=this.oParentObject.w_VALEUR
              FWRITE(this.oParentObject.hFile,IIF(FATTVEND.PNTIPDOC="FE" OR FATTVEND.PNTIPDOC="NE", ;
              RIGHT("0000000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOCINVAL)),13, 0))+RIGHT(STR(this.w_TOTDOCINVAL,14,2),2),13), "0000000000000"),13)
            else
              FWRITE(this.oParentObject.hFile,IIF(FATTVEND.PNTIPDOC="FE" OR FATTVEND.PNTIPDOC="NE", ;
              RIGHT("0000000000000"+ALLTRIM(STR(ABS(this.w_TOTDOCINVAL),13, 0)),13), "0000000000000"),13)
            endif
            * --- Segno
            if FATTVEND.PNTIPDOC="NE"
              FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOCINVAL>=0, "-", "+"),1)
            else
              FWRITE(this.oParentObject.hFile,IIF(FATTVEND.PNTIPDOC="FE", IIF(this.w_TOTDOCINVAL>=0, "+", "-"), " "),1)
            endif
          else
            FWRITE(this.oParentObject.hFile,repl("0",13),13)
            FWRITE(this.oParentObject.hFile," ",1)
          endif
        else
          * --- Lascio blank i campi di importo totale
          FWRITE(this.oParentObject.hFile,repl("0",11),11)
          FWRITE(this.oParentObject.hFile," ",1)
          FWRITE(this.oParentObject.hFile,repl("0",13),13)
          FWRITE(this.oParentObject.hFile," ",1)
        endif
        * --- Codice Valuta 
        FWRITE(this.oParentObject.hFile,IIF(FATTVEND.PNTIPDOC="FE" OR FATTVEND.PNTIPDOC="NE" OR FATTVEND.PNTIPDOC="AU" OR FATTVEND.PNTIPDOC="NU" , LEFT(ALLTRIM(FATTVEND.PNCODVAL)+"   ",3), "   "),3)
        * --- Codice CEE
        FWRITE(this.oParentObject.hFile,IIF(FATTVEND.PNTIPDOC="FE" OR FATTVEND.PNTIPDOC="NE" AND NOT EMPTY(this.w_IVACEE), this.w_IVACEE, "  "),2)
        * --- Filler
        this.oParentObject.w_FILLER = SPACE(131)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,131)
      else
        if NVL(FATTVEND.IVCODIVA,SPACE(5))<>SPACE(5) AND (NVL(FATTVEND.IVIMPONI,0)<>0 OR NVL(FATTVEND.IVIMPIVA,0)<>0 OR NVL(FATTVEND.IVCFLOMA," ")="S")
          * --- Marco questa registrazione come non valida
          this.w_REGNOVALIDA = .T.
        endif
      endif
      if this.w_REGNOVALIDA
        go this.w_ACTUALPOS
        this.w_NOTRAS = -1
        * --- Nel temporaneo di prima nota potrebbe essere presente la registrazione derivata da una fattura UE
        * --- Try
        local bErr_039A8388
        bErr_039A8388=bTrsErr
        this.Try_039A8388()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Essendo w_NOTRAS=-1 sovrascrivo quello che era stato inserito dalla routine GSLM_BFA relativo alle fatture UE
          * --- Write into STU_PNTT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.STU_PNTT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_PNTT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LMNUMTRA ="+cp_NullLink(cp_ToStrODBC(this.w_NOTRAS),'STU_PNTT','LMNUMTRA');
            +",LMNUMTR2 ="+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
                +i_ccchkf ;
            +" where ";
                +"LMSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                   )
          else
            update (i_cTable) set;
                LMNUMTRA = this.w_NOTRAS;
                ,LMNUMTR2 = 0;
                &i_ccchkf. ;
             where;
                LMSERIAL = this.w_PNSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_039A8388
        * --- End
        this.w_STRINGA = Ah_MsgFormat("%1Errore! Contropartita contabile non definita su riga castelletto IVA ","               ")
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        SELECT FATTVEND
        do while FATTVEND.PNSERIAL=this.w_PNSERIAL
          * --- Avanzo il puntatore
          skip 1
        enddo
        this.w_ESCI = .T.
        this.w_OKTRAS = .F.
      endif
      * --- Avanzo il puntatore
      if .not. this.w_ESCI
        skip 1
      endif
    enddo
    if this.w_OKTRAS
      * --- Try
      local bErr_039B95C8
      bErr_039B95C8=bTrsErr
      this.Try_039B95C8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Essendo w_PROFIL=numero del trasferimento
        * --- (uguale a quanto inserito dalla routine GSLM_BFA relativo alle fatture UE in caso di successo)
        * --- (in caso di insuccesso il valore inserito dalla routine GSLM_BFA � -1)
        * --- Non bisogna scrivere niente
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_039B95C8
      * --- End
      SELECT FATTVEND
    endif
  endproc
  proc Try_039A8388()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_PNTT
    i_nConn=i_TableProp[this.STU_PNTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOTRAS),'STU_PNTT','LMNUMTRA');
      +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.w_NOTRAS,'LMNUMTR2',0)
      insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_NOTRAS;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='GSLM_BFV: Scrittura con errore in STU_PNTT'
      return
    endif
    return
  proc Try_039B95C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_PNTT
    i_nConn=i_TableProp[this.STU_PNTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'STU_PNTT','LMNUMTRA');
      +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.oParentObject.w_PROFIL,'LMNUMTR2',0)
      insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.oParentObject.w_PROFIL;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='GSLM_BFV: Scrittura in STU_PNTT'
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='STU_NORM'
    this.cWorkTables[3]='STU_PNTT'
    this.cWorkTables[4]='STU_TRAS'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
