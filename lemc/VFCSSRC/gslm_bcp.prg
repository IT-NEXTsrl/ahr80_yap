* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_bcp                                                        *
*              Cancellazione piano dei conti                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-02                                                      *
* Last revis.: 2013-04-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm_bcp",oParentObject)
return(i_retval)

define class tgslm_bcp as StdBatch
  * --- Local variables
  * --- WorkFile variables
  STU_PIAC_idx=0
  STUMPIAC_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cancellazione di tutti i conti associati al piano selezionato in GSLM_KLP
    * --- Eseguo la cancellazione
    if ! empty(this.oParentObject.w_PDCLIST)
      * --- Try
      local bErr_0302CA68
      bErr_0302CA68=bTrsErr
      this.Try_0302CA68()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("Cancellazione del piano dei conti %1 fallita %2",48,"",this.oParentObject.w_PDCLIST,chr(13)+message())
      endif
      bTrsErr=bTrsErr or bErr_0302CA68
      * --- End
    else
      ah_ErrorMsg("Selezionare il codice del piano dei conti che si vuole cancellare")
    endif
  endproc
  proc Try_0302CA68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Delete from STU_PIAC
    i_nConn=i_TableProp[this.STU_PIAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PIAC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      local i_cQueryTable,i_cWhere
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_cWhere=i_cTable+".LMCODCON = "+i_cQueryTable+".LMCODCON";
            +" and "+i_cTable+".LMPROGRE = "+i_cQueryTable+".LMPROGRE";
            +" and "+i_cTable+".LMCODSOT = "+i_cQueryTable+".LMCODSOT";
    
      do vq_exec with '..\EXE\QUERY\GSLM_BCP',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
            +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from STUMPIAC
    i_nConn=i_TableProp[this.STUMPIAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STUMPIAC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"LMCODPIA = "+cp_ToStrODBC(this.oParentObject.w_PDCLIST);
             )
    else
      delete from (i_cTable) where;
            LMCODPIA = this.oParentObject.w_PDCLIST;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Cancellazione del piano dei conti %1 eseguita correttamente",48,"",this.oParentObject.w_PDCLIST)
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='STU_PIAC'
    this.cWorkTables[2]='STUMPIAC'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
