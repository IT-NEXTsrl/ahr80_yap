* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_bce                                                        *
*              Conferma export studio                                          *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_2112]                                         *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-23                                                      *
* Last revis.: 2015-05-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm_bce",oParentObject)
return(i_retval)

define class tgslm_bce as StdBatch
  * --- Local variables
  w_PNSERIAL = space(10)
  w_PNNUMTRA = 0
  w_PNNUMTR2 = 0
  w_CODAZI = space(5)
  * --- WorkFile variables
  PNT_MAST_idx=0
  STU_PARA_idx=0
  STU_PNTT_idx=0
  PAR_TITE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- BATCH CHE CONFERMA L'EXPORT VERSO LO STUDIO LANCIATO DA GSLM_KCE
    this.w_CODAZI = i_CODAZI
    * --- begin transaction
    cp_BeginTrs()
    * --- Try
    local bErr_02AB0310
    bErr_02AB0310=bTrsErr
    this.Try_02AB0310()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_Msg("Impossibile scrivere in primanota",.T.)
      return
    endif
    bTrsErr=bTrsErr or bErr_02AB0310
    * --- End
  endproc
  proc Try_02AB0310()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Select from STU_PNTT
    i_nConn=i_TableProp[this.STU_PNTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2],.t.,this.STU_PNTT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" STU_PNTT ";
           ,"_Curs_STU_PNTT")
    else
      select * from (i_cTable);
        into cursor _Curs_STU_PNTT
    endif
    if used('_Curs_STU_PNTT')
      select _Curs_STU_PNTT
      locate for 1=1
      do while not(eof())
      this.w_PNSERIAL = _Curs_STU_PNTT.LMSERIAL
      this.w_PNNUMTRA = _Curs_STU_PNTT.LMNUMTRA
      this.w_PNNUMTR2 = _Curs_STU_PNTT.LMNUMTR2
      if this.w_PNNUMTRA<>0 AND this.w_PNNUMTR2=0
        * --- Se LMNUMTRA � diverso da 0 allora � stata trasferita una registrazione contabile che movimenta l'IVA;
        * --- in tal caso LMNUMTR2 vale 0 e non deve essere scritto in quanto la registrazione pu� essere stata trasferita in passato
        * --- Write into PNT_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNNUMTRA ="+cp_NullLink(cp_ToStrODBC(this.w_PNNUMTRA),'PNT_MAST','PNNUMTRA');
              +i_ccchkf ;
          +" where ";
              +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                 )
        else
          update (i_cTable) set;
              PNNUMTRA = this.w_PNNUMTRA;
              &i_ccchkf. ;
           where;
              PNSERIAL = this.w_PNSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      if this.w_PNNUMTRA=0 AND this.w_PNNUMTR2<>0
        * --- Se LMNUMTR2 � diverso da 0 allora � stata trasferita una registrazione contabile che non movimenta l'IVA;
        * --- in tal caso LMNUMTRA vale 0 e non deve essere scritto in quanto la registrazione pu� essere stata trasferita in passato
        * --- Write into PNT_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNNUMTR2 ="+cp_NullLink(cp_ToStrODBC(this.w_PNNUMTR2),'PNT_MAST','PNNUMTR2');
              +i_ccchkf ;
          +" where ";
              +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                 )
        else
          update (i_cTable) set;
              PNNUMTR2 = this.w_PNNUMTR2;
              &i_ccchkf. ;
           where;
              PNSERIAL = this.w_PNSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      if this.w_PNNUMTRA<>0 AND this.w_PNNUMTR2<>0
        * --- Se LMNUMTRA � diverso da 0 allora � stata trasferita una registrazione contabile che movimenta l'IVA;
        * --- se anche LMNUMTR2 � diverso da 0 allora � stata trasferita anche una registrazione contabile che non movimenta l'IVA;
        * --- in tal caso devono essere scritti sia LMNUMTRA che LMNUMTR2
        * --- Write into PNT_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PNT_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PNNUMTRA ="+cp_NullLink(cp_ToStrODBC(this.w_PNNUMTRA),'PNT_MAST','PNNUMTRA');
          +",PNNUMTR2 ="+cp_NullLink(cp_ToStrODBC(this.w_PNNUMTR2),'PNT_MAST','PNNUMTR2');
              +i_ccchkf ;
          +" where ";
              +"PNSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                 )
        else
          update (i_cTable) set;
              PNNUMTRA = this.w_PNNUMTRA;
              ,PNNUMTR2 = this.w_PNNUMTR2;
              &i_ccchkf. ;
           where;
              PNSERIAL = this.w_PNSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_STU_PNTT
        continue
      enddo
      use
    endif
    * --- Aggiornamento numero trasferimento studio
    * --- Write into STU_PARA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.STU_PARA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PARA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_PARA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"LMPROFIL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LMPROFIL),'STU_PARA','LMPROFIL');
      +",LMAGONUM ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'STU_PARA','LMAGONUM');
          +i_ccchkf ;
      +" where ";
          +"LMCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             )
    else
      update (i_cTable) set;
          LMPROFIL = this.oParentObject.w_LMPROFIL;
          ,LMAGONUM = this.oParentObject.w_PROFIL;
          &i_ccchkf. ;
       where;
          LMCODAZI = this.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_Msg("Operazione completata",.T.)
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='PNT_MAST'
    this.cWorkTables[2]='STU_PARA'
    this.cWorkTables[3]='STU_PNTT'
    this.cWorkTables[4]='PAR_TITE'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_STU_PNTT')
      use in _Curs_STU_PNTT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
