* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_bgc                                                        *
*              Generazione codici cli/for                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-22                                                      *
* Last revis.: 2013-04-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm_bgc",oParentObject)
return(i_retval)

define class tgslm_bgc as StdBatch
  * --- Local variables
  w_NUMUPD = 0
  w_FIELD1 = space(250)
  w_FIELD2 = space(250)
  w_ANCODSTU = space(5)
  w_STRINGA = space(100)
  * --- WorkFile variables
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSLM_KGC
    this.w_NUMUPD = 0
    if Used("__tmp__")
       
 Select("__Tmp__") 
 Use
    endif
    CREATE CURSOR __Tmp__ (MSG M(10))
    * --- Try
    local bErr_036931B0
    bErr_036931B0=bTrsErr
    this.Try_036931B0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("Generazione fallita: %1",,"",Message())
    endif
    bTrsErr=bTrsErr or bErr_036931B0
    * --- End
    * --- Se presenti conti con mastri privi di conti studio lo segnalo..
    if RecCount("__tmp__")>0 And ah_YesNo("Vuoi stampare l'elenco dei clienti\fornitori per i quali non � stato possibile inserire un progressivo?")
      CP_CHPRN("QUERY\GSVE_BCV.FRX")
    endif
    Use in Select("__tmp__")
  endproc
  proc Try_036931B0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Recupero l'elenco dei clienti / fornitori senza codice studio
    * --- Select from QUERY\GSAR2ACL
    do vq_exec with 'QUERY\GSAR2ACL',this,'_Curs_QUERY_GSAR2ACL','',.f.,.t.
    if used('_Curs_QUERY_GSAR2ACL')
      select _Curs_QUERY_GSAR2ACL
      locate for 1=1
      do while not(eof())
      if Empty( Nvl(_Curs_QUERY_GSAR2ACL.MCCODSTU,"") )
        this.w_FIELD1 = Ah_MsgFormat("Codice cliente %1 collegato a mastro %2 privo di conto studio. Impossibile determinare progressivo",Alltrim(_Curs_QUERY_GSAR2ACL.ANCODICE),Alltrim(Nvl(_Curs_QUERY_GSAR2ACL.ANCONSUP,"")))
        this.w_FIELD2 = Ah_MsgFormat("Codice fornitore %1 collegato a mastro %2 privo di conto studio. Impossibile determinare progressivo",Alltrim(_Curs_QUERY_GSAR2ACL.ANCODICE),Alltrim(Nvl(_Curs_QUERY_GSAR2ACL.ANCONSUP,"")))
        * --- Se non riesco a determinare il progressivo segnalo al termine con report...
         
 Insert into __Tmp__ (MSG) Values (iif(_Curs_QUERY_GSAR2ACL.ANTIPCON ="C",this.w_FIELD1,this.w_FIELD2))
      else
        * --- Calcolo il nuovo progressivo
        this.w_ANCODSTU = GSLM_BCF( this, _Curs_QUERY_GSAR2ACL.ANTIPCON ,_Curs_QUERY_GSAR2ACL.ANCODICE, Nvl(_Curs_QUERY_GSAR2ACL.MCCODSTU,""),NVL(_Curs_QUERY_GSAR2ACL.ANCONSUP,""),Null,Nvl(_Curs_QUERY_GSAR2ACL.MCPROSTU,0))
        if _Curs_QUERY_GSAR2ACL.ANTIPCON="C"
          this.w_STRINGA = "Generazione codice cliente %1"
        else
          this.w_STRINGA = "Generazione codice fornitore %1"
        endif
        ah_Msg(this.w_STRINGA,.T.,.F.,.F.,_Curs_QUERY_GSAR2ACL.ANCODICE)
        * --- Write into CONTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANCODSTU ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODSTU),'CONTI','ANCODSTU');
              +i_ccchkf ;
          +" where ";
              +"ANTIPCON = "+cp_ToStrODBC(_Curs_QUERY_GSAR2ACL.ANTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(_Curs_QUERY_GSAR2ACL.ANCODICE);
              +" and ANCONSUP = "+cp_ToStrODBC(_Curs_QUERY_GSAR2ACL.ANCONSUP);
                 )
        else
          update (i_cTable) set;
              ANCODSTU = this.w_ANCODSTU;
              &i_ccchkf. ;
           where;
              ANTIPCON = _Curs_QUERY_GSAR2ACL.ANTIPCON;
              and ANCODICE = _Curs_QUERY_GSAR2ACL.ANCODICE;
              and ANCONSUP = _Curs_QUERY_GSAR2ACL.ANCONSUP;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if i_ROWS > 0
          this.w_NUMUPD = this.w_NUMUPD + 1
        endif
      endif
        select _Curs_QUERY_GSAR2ACL
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Generazione terminata con successo su %1 clienti\fornitori",,"",alltrim(Str(this.w_NUMUPD,9,0)))
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONTI'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_QUERY_GSAR2ACL')
      use in _Curs_QUERY_GSAR2ACL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
