* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_kdt                                                        *
*              Import/export trascodifica                                      *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_33]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1992-07-06                                                      *
* Last revis.: 2009-12-29                                                      *
*                                                                              *
* EUROCONV 1.0 (26-06-1998) - Z                                                *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gslm_kdt
* Esco se il modulo � in semplificata
IF NOT LMAVANZATO()
  do cplu_erm with "La funzione � attiva con il Modulo in modalit� Avanzata"
 return
ENDIF
* --- Fine Area Manuale
return(createobject("tgslm_kdt",oParentObject))

* --- Class definition
define class tgslm_kdt as StdForm
  Top    = 76
  Left   = 126

  * --- Standard Properties
  Width  = 440
  Height = 424
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-29"
  HelpContextID=82426985
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  _IDX = 0
  cPrg = "gslm_kdt"
  cComment = "Import/export trascodifica"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_RADSELIE = space(1)
  w_DBF1 = space(200)
  w_MSG = space(0)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslm_kdtPag1","gslm_kdt",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oRADSELIE_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RADSELIE=space(1)
      .w_DBF1=space(200)
      .w_MSG=space(0)
        .w_RADSELIE = ' '
        .w_DBF1 = "..\LEMC\EXE\STU_TRAS.DBF"
      .oPgFrm.Page1.oPag.oObj_1_12.Calculate(IIF(.w_RADSELIE<>'I',"",ah_MsgFormat("Prima di procedere con l'importazione della tabella trascodifica%0assicurarsi che siano stati importati i seguenti archivi dello studio")))
    endwith
    this.DoRTCalc(3,3,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_1.enabled = this.oPgFrm.Page1.oPag.oBtn_1_1.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_2.enabled = this.oPgFrm.Page1.oPag.oBtn_1_2.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate(IIF(.w_RADSELIE<>'I',"",ah_MsgFormat("Prima di procedere con l'importazione della tabella trascodifica%0assicurarsi che siano stati importati i seguenti archivi dello studio")))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_12.Calculate(IIF(.w_RADSELIE<>'I',"",ah_MsgFormat("Prima di procedere con l'importazione della tabella trascodifica%0assicurarsi che siano stati importati i seguenti archivi dello studio")))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_1.enabled = this.oPgFrm.Page1.oPag.oBtn_1_1.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_12.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRADSELIE_1_3.RadioValue()==this.w_RADSELIE)
      this.oPgFrm.Page1.oPag.oRADSELIE_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDBF1_1_5.value==this.w_DBF1)
      this.oPgFrm.Page1.oPag.oDBF1_1_5.value=this.w_DBF1
    endif
    if not(this.oPgFrm.Page1.oPag.oMSG_1_11.value==this.w_MSG)
      this.oPgFrm.Page1.oPag.oMSG_1_11.value=this.w_MSG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DBF1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDBF1_1_5.SetFocus()
            i_bnoObbl = !empty(.w_DBF1)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgslm_kdtPag1 as StdContainer
  Width  = 436
  height = 424
  stdWidth  = 436
  stdheight = 424
  resizeXpos=242
  resizeYpos=359
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oBtn_1_1 as StdButton with uid="IFISWSZGLE",left=322, top=227, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 82398234;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_1.Click()
      with this.Parent.oContained
        do GSLM_BED with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_1.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_RADSELIE='I' OR .w_RADSELIE='E')
      endwith
    endif
  endfunc


  add object oBtn_1_2 as StdButton with uid="RMAHAARJLI",left=380, top=227, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 75109562;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oRADSELIE_1_3 as StdRadio with uid="OKXXQIMNCR",rtseq=1,rtrep=.f.,left=9, top=9, width=420,height=17;
    , ToolTipText = "Seleziona import/export";
    , cFormVar="w_RADSELIE", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oRADSELIE_1_3.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Importazione da file DBF"
      this.Buttons(1).HelpContextID = 71447973
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Importazione da file DBF","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Esportazione su file DBF"
      this.Buttons(2).HelpContextID = 71447973
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Esportazione su file DBF","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",17)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona import/export")
      StdRadio::init()
    endproc

  func oRADSELIE_1_3.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'E',;
    space(1))))
  endfunc
  func oRADSELIE_1_3.GetRadio()
    this.Parent.oContained.w_RADSELIE = this.RadioValue()
    return .t.
  endfunc

  func oRADSELIE_1_3.SetRadio()
    this.Parent.oContained.w_RADSELIE=trim(this.Parent.oContained.w_RADSELIE)
    this.value = ;
      iif(this.Parent.oContained.w_RADSELIE=='I',1,;
      iif(this.Parent.oContained.w_RADSELIE=='E',2,;
      0))
  endfunc

  add object oDBF1_1_5 as StdField with uid="ZPJIIFTPAA",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DBF1", cQueryName = "DBF1",;
    bObbl = .t. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    HelpContextID = 78910922,;
   bGlobalFont=.t.,;
    Height=21, Width=383, Left=46, Top=197, InputMask=replicate('X',200)

  add object oMSG_1_11 as StdMemo with uid="SDVKETLQKB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MSG", cQueryName = "MSG",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 82113594,;
   bGlobalFont=.t.,;
    Height=135, Width=416, Left=9, Top=276, tabstop = .f., readonly = .t.


  add object oObj_1_12 as cp_calclbl with uid="TVSLHFHUEH",left=42, top=45, width=381,height=36,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 95570662

  add object oStr_1_4 as StdString with uid="OIRNPUOGGW",Visible=.t., Left=11, Top=176,;
    Alignment=0, Width=205, Height=18,;
    Caption="Tabella trascodifica"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="OJVLVUPTEC",Visible=.t., Left=8, Top=197,;
    Alignment=1, Width=37, Height=18,;
    Caption="PATH:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="QBXCZCBBPL",Visible=.t., Left=42, Top=80,;
    Alignment=0, Width=367, Height=18,;
    Caption="Codici norme"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (.w_RADSELIE<>'I')
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="XHVWNLMKJF",Visible=.t., Left=42, Top=99,;
    Alignment=0, Width=367, Height=18,;
    Caption="Codici IVA"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_RADSELIE<>'I')
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="LSCKJRQIEU",Visible=.t., Left=42, Top=118,;
    Alignment=0, Width=367, Height=18,;
    Caption="Operazioni INTRA"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (.w_RADSELIE<>'I')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="ZCKJSVBPVQ",Visible=.t., Left=42, Top=137,;
    Alignment=0, Width=367, Height=18,;
    Caption="Causali contabili"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (.w_RADSELIE<>'I')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslm_kdt','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
