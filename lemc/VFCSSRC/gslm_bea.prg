* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_bea                                                        *
*              Export studio prog.principale                                   *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_2155]                                         *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-23                                                      *
* Last revis.: 2014-10-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm_bea",oParentObject)
return(i_retval)

define class tgslm_bea as StdBatch
  * --- Local variables
  w_PATH = space(50)
  w_STRINGA = space(120)
  w_CONDIZ = .f.
  w_CONFERMA = .f.
  w_FILETOSAVE = space(40)
  w_LOGTOSAVE = space(40)
  w_APPOGGIO = space(40)
  w_FILELOG = space(50)
  w_TIPOCLFO = space(1)
  w_TIPOCLFO1 = space(1)
  hFILE = 0
  hLOG = 0
  w_GOT = 0
  w_TRT = 0
  w_NUMCLI = 0
  w_NUMFOR = 0
  w_NUMSOT = 0
  w_FATTACQU = 0
  w_FATTVEND = 0
  w_FATTCORR = 0
  w_FATTVENT = 0
  w_CORRNORM = 0
  w_CORRVENT = 0
  w_MOCONTAB = 0
  w_ERRORE = .f.
  w_WARNING = .f.
  w_PNTNOTAOK = .f.
  w_ESCI = space(10)
  w_CODAZI = space(5)
  w_DITTA = space(6)
  w_LMPROFIL = space(2)
  w_ASSOCI = space(2)
  w_CAU1 = space(5)
  w_CAU2 = space(5)
  w_CAU3 = space(5)
  w_CAU4 = space(5)
  w_CAU5 = space(5)
  w_CAUGIR = space(5)
  w_CASFIT = space(5)
  w_FLANUN = space(1)
  w_OK = .f.
  w_VALEUR = space(3)
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_PROFIL = 0
  w_FILLER = space(200)
  w_ALMENOUNCLI = .f.
  w_ALMENOUNFOR = .f.
  w_ALMENOUNSOT = .f.
  w_RS = space(25)
  w_RS1 = space(25)
  w_RS2 = space(25)
  w_INDICE = space(10)
  w_IND = space(32)
  w_NUM = space(5)
  w_ALMENOUNFATTACQU = .f.
  w_ALMENOUNFATTVEND = .f.
  w_ALMENOUNFATTCORR = .f.
  w_ALMENOUNFATTVENT = .f.
  w_ALMENOUNCORRNORM = .f.
  w_ALMENOUNCORRVENT = .f.
  w_ALMENOUNMOCONTAB = .f.
  w_ISALT = space(0)
  w_ORIGINETIPO = space(2)
  w_TIPO = space(2)
  * --- WorkFile variables
  CAU_CONT_idx=0
  CONTI_idx=0
  PNT_MAST_idx=0
  STU_PARA_idx=0
  STU_PIAC_idx=0
  STU_PNTT_idx=0
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- BATCH CHE EFFETTUA L'EXPORT VERSO LO STUDIO LANCIATO DA GSLM_KEX
    this.w_PATH = this.oParentObject.w_PERCORSO
    * --- Handle dei file
    this.hFILE = -1
    this.hLOG = -1
    * --- Variabili necessarie alla compilazione del file
    * --- Gruppi omogenei trasferiti
    * --- Totale record trasferiti
    * --- Numero di clienti trasferiti
    * --- Numero di fornitori trasferiti
    * --- Numero di conti trasferiti
    * --- Numero di fatture di acquisto trasferite
    * --- Numero di fatture di vendita trasferite
    * --- Numero di fatture corrispetive trasferite
    * --- Numero di fatture ventilate trasferite
    * --- Numero di corrispetivi normali trasferiti
    * --- Numero di corrispetivi ventilati trasferiti
    * --- Numero di movimenti contabili trasferiti
    * --- Variabili usate per controllare errori e warning durante l'export
    * --- Variabile usata per indicare se la primanota era corretta
    this.w_ESCI = .F.
    * --- Causali che non devono essere esportate
    * --- Causale per il giroconto
    * --- Sottoconto Studio Cassa Fittizia
    * --- Flag Anagrafica Unica
    * --- Recupero i parametri per il trasferimento
    this.w_CODAZI = i_CODAZI
    * --- Read from STU_PARA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.STU_PARA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PARA_idx,2],.t.,this.STU_PARA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LMAZISTU,LMPROFIL,LMASSOCI,LMESCAU1,LMESCAU2,LMESCAU3,LMESCAU4,LMESCAU5,LMCAUGIR,LMCASFIT,LMFLANUN"+;
        " from "+i_cTable+" STU_PARA where ";
            +"LMCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LMAZISTU,LMPROFIL,LMASSOCI,LMESCAU1,LMESCAU2,LMESCAU3,LMESCAU4,LMESCAU5,LMCAUGIR,LMCASFIT,LMFLANUN;
        from (i_cTable) where;
            LMCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DITTA = NVL(cp_ToDate(_read_.LMAZISTU),cp_NullValue(_read_.LMAZISTU))
      this.w_LMPROFIL = NVL(cp_ToDate(_read_.LMPROFIL),cp_NullValue(_read_.LMPROFIL))
      this.w_ASSOCI = NVL(cp_ToDate(_read_.LMASSOCI),cp_NullValue(_read_.LMASSOCI))
      this.w_CAU1 = NVL(cp_ToDate(_read_.LMESCAU1),cp_NullValue(_read_.LMESCAU1))
      this.w_CAU2 = NVL(cp_ToDate(_read_.LMESCAU2),cp_NullValue(_read_.LMESCAU2))
      this.w_CAU3 = NVL(cp_ToDate(_read_.LMESCAU3),cp_NullValue(_read_.LMESCAU3))
      this.w_CAU4 = NVL(cp_ToDate(_read_.LMESCAU4),cp_NullValue(_read_.LMESCAU4))
      this.w_CAU5 = NVL(cp_ToDate(_read_.LMESCAU5),cp_NullValue(_read_.LMESCAU5))
      this.w_CAUGIR = NVL(cp_ToDate(_read_.LMCAUGIR),cp_NullValue(_read_.LMCAUGIR))
      this.w_CASFIT = NVL(cp_ToDate(_read_.LMCASFIT),cp_NullValue(_read_.LMCASFIT))
      this.w_FLANUN = NVL(cp_ToDate(_read_.LMFLANUN),cp_NullValue(_read_.LMFLANUN))
      use
    else
      * --- Error: sql sentence error.
      i_Error = 'Fallita la lettura dei parametri per il trasferimento'
      return
    endif
    select (i_nOldArea)
    * --- Codice Valuta Euro
    * --- Recupero il Codice Valuta Euro
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZVALEUR"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZVALEUR;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_VALEUR = NVL(cp_ToDate(_read_.AZVALEUR),cp_NullValue(_read_.AZVALEUR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Aggiorno il numero di spedizione
    this.w_PROFIL = VAL(this.w_LMPROFIL)+1
    this.w_LMPROFIL = RIGHT("00"+ALLTRIM(STR(this.w_PROFIL)),2)
    * --- Inizializzazione variabili necessarie alla compilazione del file
    this.w_GOT = 0
    this.w_TRT = 0
    this.w_NUMCLI = 0
    this.w_NUMFOR = 0
    this.w_NUMSOT = 0
    this.w_FATTACQU = 0
    this.w_FATTVEND = 0
    this.w_FATTCORR = 0
    this.w_FATTVENT = 0
    this.w_CORRNORM = 0
    this.w_CORRVENT = 0
    this.w_MOCONTAB = 0
    this.w_PNTNOTAOK = .T.
    * --- Inizializzazione variabile di conferma
    this.w_CONFERMA = .F.
    * --- Inizializzazione di ERASETMP e w_FILETOSAVE
    this.w_FILETOSAVE = ""
    this.w_LOGTOSAVE = ""
    this.w_ERRORE = .F.
    this.w_WARNING = .F.
    * --- Preparazione nome temporaneo di appoggio
    this.w_APPOGGIO = this.oParentObject.w_PERCORSO+"\"+"IM000000."+this.w_LMPROFIL
    * --- Creazione e apertura file di appoggio
    this.hFILE = FCREATE(this.w_APPOGGIO,0)
    * --- Preparazione nome temporaneo di log
    this.w_FILELOG = this.oParentObject.w_PERCORSO+"\"+"LOG00000.TMP"
    * --- Creazione e apertura file di log
    this.hLOG = FCREATE(this.w_FILELOG,0)
    * --- Controllo creazione file di appoggio
    if this.hFile=-1
      ah_Msg("Impossibile creare il file di appoggio",.T.)
      return
    endif
    FCLOSE(this.hFile)
    * --- Controllo creazione file di log
    if this.hLOG=-1
      ah_Msg("Impossibile creare il file di log",.T.)
      return
    endif
    FCLOSE(this.hLOG)
    * --- Riapertura dei file
    this.hFILE = FOPEN(this.w_APPOGGIO,1)
    if this.hFile=-1
      ah_Msg("Impossibile aprire il file di appoggio",.T.)
      return
    endif
    this.hLOG = FOPEN(this.w_FILELOG,1)
    if this.hLOG=-1
      ah_Msg("Impossibile aprire il file di log",.T.)
      return
    endif
    * --- Testata file di LOG
    FWRITE(this.hLOG,REPL("-",80),80)
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    * --- recupero ragione sociale azienda
    this.w_STRINGA = ah_MsgFormat("-- AZIENDA%1:%2",space(13),UPPER(g_RAGAZI))
    FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    this.w_STRINGA = ah_MsgFormat("-- DATA EXPORT%1:%2",space(9), dtoc(date()))
    FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    * --- Preparazione stringa elementi da esportare
    this.w_oMess=createobject("AH_Message")
    this.w_oPart = this.w_oMess.AddMsgPart("-- EXPORT DI%1:")
    this.w_oPart.AddParam(space(11))     
    if this.oParentObject.w_CLI="S"
      this.w_oPart = this.w_oMess.AddMsgPart("CLIENTI%1")
      this.w_oPart.AddParam(space(1))     
    endif
    if this.oParentObject.w_FOR="S"
      this.w_oPart = this.w_oMess.AddMsgPart("FORNITORI%1")
      this.w_oPart.AddParam(space(1))     
    endif
    if this.oParentObject.w_SOT="S"
      this.w_oPart = this.w_oMess.AddMsgPart("CONTI%1")
      this.w_oPart.AddParam(space(1))     
    endif
    if this.oParentObject.w_PNTIVA="S"
      this.w_oPart = this.w_oMess.AddMsgPart("PRIMANOTA IVA%1")
      this.w_oPart.AddParam(space(1))     
    endif
    if this.oParentObject.w_PNTNOIVA="S"
      this.w_oPart = this.w_oMess.AddMsgPart("PRIMANOTA NO IVA%1")
      this.w_oPart.AddParam(space(1))     
    endif
    this.w_STRINGA = this.w_oMess.ComposeMessage()
    FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    do case
      case this.oParentObject.w_CLI="S" and this.oParentObject.w_FOR="S"
        if this.oParentObject.w_TIPCLIFOR="T"
          this.w_STRINGA = Ah_MsgFormat("-- CLIENTI / FORNITORI: TUTTI")
        else
          this.w_STRINGA = Ah_MsgFormat("-- CLIENTI / FORNITORI: SOLO MOVIMENTATI")
        endif
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      case this.oParentObject.w_CLI="S" and this.oParentObject.w_FOR="N"
        if this.oParentObject.w_TIPCLIFOR="T"
          this.w_STRINGA = Ah_MsgFormat("-- CLIENTI: TUTTI")
        else
          this.w_STRINGA = Ah_MsgFormat("-- CLIENTI: SOLO MOVIMENTATI")
        endif
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      case this.oParentObject.w_CLI="N" and this.oParentObject.w_FOR="S"
        if this.oParentObject.w_TIPCLIFOR="T"
          this.w_STRINGA = Ah_MsgFormat("-- FORNITORI: TUTTI")
        else
          this.w_STRINGA = Ah_MsgFormat("-- FORNITORI: SOLO MOVIMENTATI")
        endif
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    endcase
    if this.oParentObject.w_PNTIVA="S" OR this.oParentObject.w_PNTNOIVA="S"
      this.w_STRINGA = Ah_MsgFormat("-- EXPORT PRIMANOTA: DA DATA %1 A DATA %2",dtoc(this.oParentObject.w_DATINI),dtoc(this.oParentObject.w_DATFIN))
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      this.w_STRINGA = Ah_MsgFormat("%1DA NUMERO %2 A NUMERO %3","                         ",STR(this.oParentObject.w_NUMINI,6,0),STR(this.oParentObject.w_NUMFIN,6,0))
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    endif
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    FWRITE(this.hLOG,REPL("-",80),80)
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    * --- Fine testata file di LOG
    * --- Eseguo la procedura di export
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Chiudo il file di appoggio
    FCLOSE(this.hFile)
    * --- Chiudo file di log
    FCLOSE(this.hLOG)
    * --- Esco se non ho trovato nulla da esportare
    this.w_CONDIZ = (NOT this.w_ALMENOUNCLI) AND (NOT this.w_ALMENOUNFOR) AND (NOT this.w_ALMENOUNSOT) AND (NOT this.w_ALMENOUNFATTACQU)
    this.w_CONDIZ = (NOT this.w_ALMENOUNFATTVEND) AND (NOT this.w_ALMENOUNFATTCORR) AND (NOT this.w_ALMENOUNFATTVENT) AND this.w_CONDIZ
    this.w_CONDIZ = (NOT this.w_ALMENOUNCORRNORM) AND (NOT this.w_ALMENOUNCORRVENT) AND (NOT this.w_ALMENOUNMOCONTAB) AND this.w_CONDIZ
    if this.w_CONDIZ
      Ah_ErrorMsg ("Per l'intervallo selezionato non sono presenti movimenti")
      return
    endif
    * --- Lancio la maschera di conferma.(Solo se la primanota era OK) Valorizza w_CONFERMA,w_FILETOSAVE e w_LOGTOSAVE
    if this.w_PNTNOTAOK AND NOT this.w_ERRORE
      * --- Lancio la maschera di conferma
      do GSLM_KCE with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Lancio la maschera di errore
      do GSLM_KER with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- PROCEDURA PRINCIPALE DI EXPORT FILE
    * --- Incremento il numero di record inseriti (Il record di Testa)
    this.w_TRT = this.w_TRT+1
    FWRITE(this.hFile,"T",1)
    * --- Alla fine aggiorno questi campi
    FWRITE(this.hFile,RIGHT("000"+ALLTRIM(STR(this.w_GOT)),3),3)
    FWRITE(this.hFile,RIGHT("0000000000000"+ALLTRIM(STR(this.w_TRT)),13),13)
    * --- Inserisco il codice ditta
    FWRITE(this.hFile,this.w_DITTA,6)
    * --- Inserisco il numero di spedizione
    FWRITE(this.hFile,this.w_LMPROFIL,2)
    * --- Recupero e inserisco la data di spedizione
    FWRITE(this.hFile,RIGHT(dtos(i_datsys),8),8)
    * --- Inserisco il tipo di spedizione
    FWRITE(this.hFile,"V",1)
    * --- Inserisco utente di provenienza - Non gestito
    FWRITE(this.hFile," ",1)
    * --- Inserisco utente di destinazione - Non gestito
    FWRITE(this.hFile," ",1)
    * --- Inserisco release cogen - Non gestito
    FWRITE(this.hFile,"    ",4)
    * --- Inserisco filler
    this.w_FILLER = SPACE(160)
    FWRITE(this.hFile,this.w_FILLER,160)
    * --- Controllo se ho almeno un cliente da esportare
    this.w_ALMENOUNCLI = .F.
    if this.oParentObject.w_CLI="S"
      this.w_TIPOCLFO = "C"
      if this.oParentObject.w_TIPCLIFOR="M"
        vq_exec("..\LEMC\EXE\QUERY\GSLM_ACF.VQR",this,"ALMUNCLI")
        * --- Deve esistere almeno un cliente movimentato in primanota (tra le data comprese in datini e datfin) e avente ancodstu non vuoto
        this.w_TIPOCLFO = "F"
        this.w_TIPOCLFO1 = "C"
        * --- Devo prendere anche i clienti collegati ai fornitori intestatari delle fatture europee d'acquisto
        vq_exec("..\LEMC\EXE\QUERY\GSLM2ACF.VQR",this,"UNCLICOLL")
        SELECT * FROM ALMUNCLI UNION SELECT * FROM UNCLICOLL INTO CURSOR ALMUNCLI
      else
        * --- Deve esistere almeno un cliente avente codice studio non vuoto
        vq_exec("..\LEMC\EXE\QUERY\GSLM1ACF.VQR",this,"ALMUNCLI")
      endif
      if RECCOUNT("ALMUNCLI") > 0
        this.w_ALMENOUNCLI = .T.
      endif
    endif
    * --- Controllo se ho almeno un fonitore da esportare
    this.w_ALMENOUNFOR = .F.
    if this.oParentObject.w_FOR="S"
      this.w_TIPOCLFO = "F"
      if this.oParentObject.w_TIPCLIFOR="M"
        vq_exec("..\LEMC\EXE\QUERY\GSLM_ACF.VQR",this,"ALMUNFOR")
        * --- Deve esistere almeno un fornitore movimentato in primanota (tra le data comprese in datini e datfin) e avente ancodstu non vuoto
      else
        vq_exec("..\LEMC\EXE\QUERY\GSLM1ACF.VQR",this,"ALMUNFOR")
        * --- Deve esistere almeno un fornitore avente codice studio non vuoto
      endif
      if RECCOUNT("ALMUNFOR") > 0
        this.w_ALMENOUNFOR = .T.
      endif
    endif
    * --- Aggiorno la variabile di gruppi omogenei trasferiti
    if this.w_ALMENOUNCLI .or. this.w_ALMENOUNFOR
      this.w_GOT = this.w_GOT+1
    endif
    * --- Scrittura su log
    if this.oParentObject.w_CLI="S"
      this.w_STRINGA = Ah_MsgFormat("-- INIZIO EXPORT CLIENTI --")
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    endif
    * --- Scrittura di clienti
    if this.w_ALMENOUNCLI
      * --- Testa cli/for
      * --- Incremento numero record inseriti (TESTA CLI/FOR)
      this.w_TRT = this.w_TRT+1
      this.w_NUMCLI = this.w_NUMCLI+1
      FWRITE(this.hFile,"FCLIEFORN",9)
      this.w_FILLER = SPACE(191)
      FWRITE(this.hFile,this.w_FILLER,191)
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if .not. this.w_ALMENOUNFOR
        * --- Coda cli/for
        * --- Incremento numero record inseriti (CODA CLI/FOR)
        this.w_TRT = this.w_TRT+1
        this.w_NUMCLI = this.w_NUMCLI+1
        FWRITE(this.hFile,"ECLIEFORN",9)
        FWRITE(this.hFile,RIGHT("00000000"+ALLTRIM(STR(this.w_NUMCLI+this.w_NUMFOR-2)),8),8)
        this.w_FILLER = SPACE(183)
        FWRITE(this.hFile,this.w_FILLER,183)
      endif
      * --- Scrittura su log
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      this.w_STRINGA = "-- FINE EXPORT CLIENTI --"
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    else
      if this.oParentObject.w_CLI="S"
        * --- Scrittura su log
        this.w_STRINGA = Ah_MsgFormat("%1NESSUN CLIENTE DA ESPORTARE","          ")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = "-- FINE EXPORT CLIENTI --"
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      endif
    endif
    if USED("ALMUNCLI")
      SELECT ALMUNCLI
      USE
    endif
    * --- Scrittura su log
    if this.oParentObject.w_FOR="S"
      this.w_STRINGA = Ah_MsgFormat("-- INIZIO EXPORT FORNITORI --")
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    endif
    * --- Eventuale export dei fornitori
    if this.w_ALMENOUNFOR
      if .not. this.w_ALMENOUNCLI
        * --- Testa cli/for
        * --- Incremento numero record inseriti (TESTA CLI/FOR)
        this.w_TRT = this.w_TRT+1
        this.w_NUMFOR = this.w_NUMFOR+1
        FWRITE(this.hFile,"FCLIEFORN",9)
        this.w_FILLER = SPACE(191)
        FWRITE(this.hFile,this.w_FILLER,191)
      endif
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Coda cli/for
      * --- Incremento numero record inseriti (CODA CLI/FOR)
      this.w_TRT = this.w_TRT+1
      this.w_NUMFOR = this.w_NUMFOR+1
      FWRITE(this.hFile,"ECLIEFORN",9)
      FWRITE(this.hFile,RIGHT("00000000"+ALLTRIM(STR(this.w_NUMCLI+this.w_NUMFOR-2)),8),8)
      this.w_FILLER = SPACE(183)
      FWRITE(this.hFile,this.w_FILLER,183)
      * --- Scrittura su log
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      this.w_STRINGA = Ah_MsgFormat("-- FINE EXPORT FORNITORI --")
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    else
      if this.oParentObject.w_FOR="S"
        * --- Scrittura su log
        this.w_STRINGA = Ah_MsgFormat("%1NESSUN FORNITORE DA ESPORTARE","          ")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = Ah_MsgFormat("-- FINE EXPORT FORNITORI --")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      endif
    endif
    if USED("ALMUNFOR")
      SELECT ALMUNFOR
      USE
    endif
    * --- Controllo se esiste almeno un conto da esportare
    this.w_ALMENOUNSOT = .F.
    if this.oParentObject.w_SOT="S"
      vq_exec("..\LEMC\EXE\QUERY\GSLM_AUC.VQR",this,"ALMUNCON")
      if RECCOUNT("ALMUNCON") > 0
        this.w_ALMENOUNSOT = .T.
      endif
    endif
    * --- Scrittura su log
    if this.oParentObject.w_SOT="S"
      this.w_STRINGA = Ah_MsgFormat("-- INIZIO EXPORT CONTI --")
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    endif
    * --- Eventuale export dei conti
    if this.w_ALMENOUNSOT
      * --- Testa export conti
      * --- Incremento numero record inseriti (TESTA CONTI)
      this.w_TRT = this.w_TRT+1
      this.w_NUMSOT = this.w_NUMSOT+1
      FWRITE(this.hFile,"FDESCONTI",9)
      this.w_FILLER = SPACE(191)
      FWRITE(this.hFile,this.w_FILLER,191)
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Coda conti
      * --- Incremento numero record inseriti (CODA CONTI)
      this.w_TRT = this.w_TRT+1
      this.w_NUMSOT = this.w_NUMSOT+1
      FWRITE(this.hFile,"EDESCONTI",9)
      FWRITE(this.hFile,RIGHT("00000000"+ALLTRIM(STR(this.w_NUMSOT-2)),8),8)
      this.w_FILLER = SPACE(183)
      FWRITE(this.hFile,this.w_FILLER,183)
      * --- Aggiorno la variabile di gruppi omogenei trasferiti
      this.w_GOT = this.w_GOT+1
      * --- Scrittura su log
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      this.w_STRINGA = Ah_MsgFormat("-- FINE EXPORT CONTI --")
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    else
      if this.oParentObject.w_SOT="S"
        * --- Scrittura su log
        this.w_STRINGA = Ah_MsgFormat("%1NESSUN CONTO DA ESPORTARE","          ")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = Ah_MsgFormat("-- FINE EXPORT CONTI --")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      endif
    endif
    if USED("ALMUNCON")
      SELECT ALMUNCON
      USE
    endif
    * --- Eventuale export della primanota
    if this.oParentObject.w_PNTIVA="S" OR this.oParentObject.w_PNTNOIVA="S"
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Coda del file
    * --- Incremento il numero di record inseriti (Il record di CODA)
    this.w_TRT = this.w_TRT+1
    FWRITE(this.hFile,"C",1)
    * --- Decremento di due w_TRT
    this.w_TRT = this.w_TRT-2
    * --- Alla fine aggiorno questi campi
    FWRITE(this.hFile,RIGHT("000"+ALLTRIM(STR(this.w_GOT)),3),3)
    FWRITE(this.hFile,RIGHT("0000000000000"+ALLTRIM(STR(this.w_TRT)),13),13)
    * --- Inserisco il codice ditta
    FWRITE(this.hFile,this.w_DITTA,6)
    * --- Inserisco il numero di spedizione
    FWRITE(this.hFile,this.w_LMPROFIL,2)
    * --- Recupero e inserisco la data di spedizione
    FWRITE(this.hFile,RIGHT(dtos(i_datsys),8),8)
    * --- Inserisco il tipo di spedizione
    FWRITE(this.hFile,"V",1)
    * --- Inserisco utente di provenienza - Non gestito
    FWRITE(this.hFile," ",1)
    * --- Inserisco utente di destinazione - Non gestito
    FWRITE(this.hFile," ",1)
    * --- Inserisco release cogen - Non gestito
    FWRITE(this.hFile,"    ",4)
    * --- Inserisco filler
    this.w_FILLER = SPACE(160)
    FWRITE(this.hFile,this.w_FILLER,160)
    * --- Aggiorno la testata del file
    * --- Chiudo il file di appoggio e lo riapro per posizionarmi in testa
    FCLOSE(this.hFile)
    this.hFile=FOPEN(this.w_APPOGGIO,1)
    * --- Sposto avanti di un byte
    FSEEK(this.hFile,1,0)
    * --- Scrivo il numero di file trasferiti
    FWRITE(this.hFile,RIGHT("000"+ALLTRIM(STR(this.w_GOT)),3),3)
    FWRITE(this.hFile,RIGHT("0000000000000"+ALLTRIM(STR(this.w_TRT)),13),13)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT DEI CLIENTI
    SELECT ALMUNCLI
    GO TOP
    do while NOT EOF()
      this.w_OK = .T.
      * --- Se il tipo di Codifica � 'Anagrafica Unica' controllo che la lunghezza della Partita IVA sia 11: Se diversa passo al record successivo...
      if this.w_FLANUN="S" AND LEN(ALLTRIM(ALMUNCLI.ANPARIVA)) <> 11
        this.w_STRINGA = Ah_MsgFormat("%1Export cliente %2 %3","          ", UPPER(ALMUNCLI.ANCODICE),UPPER(ALMUNCLI.ANDESCRI))
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = Ah_MsgFormat("%1ERRORE: la partita IVA non � presente o la sua lunghezza non � corretta","              ")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_OK = .F.
      endif
      if this.w_OK
        * --- Messaggio a Video
        ah_Msg("Export cliente: %1",.T.,.F.,.F.,ALMUNCLI.ANCODICE)
        * --- Scrittura su LOG
        this.w_STRINGA = Ah_MsgFormat("%1Export cliente %2 %3","          ",UPPER(ALMUNCLI.ANCODICE),UPPER(ALMUNCLI.ANDESCRI))
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        * --- Aggiorno il numero di record trasferiti
        this.w_TRT = this.w_TRT+2
        this.w_NUMCLI = this.w_NUMCLI+2
        * --- Prima parte cli/for
        FWRITE(this.hFile,"D10C",4)
        * --- Codice cliente
        FWRITE(this.hFile,ALMUNCLI.ANCODSTU,5)
        if this.w_FLANUN="S"
          * --- Tipo codifica
          FWRITE(this.hFile,"N",1)
          * --- Codifica cliente
          FWRITE(this.hFile,ALLTRIM(ALMUNCLI.ANPARIVA),11)
        else
          * --- Tipo codifica
          FWRITE(this.hFile,"P",1)
          * --- Codifica cliente
          FWRITE(this.hFile,ALLTRIM(this.w_DITTA+ALMUNCLI.ANCODSTU),11)
        endif
        * --- Ragione sociale
        if len(RTRIM(ALMUNCLI.ANDESCRI))<=25
          this.w_RS = NVL(SUBSTR(ALMUNCLI.ANDESCRI,1,25), "                         ")
          FWRITE(this.hFile,this.w_RS,25)
          FWRITE(this.hFile,SPACE(25),25)
        else
          * --- Cerco il primo spazio prima di 25
          this.w_INDICE = 25
          do while SUBSTR(RTRIM(ALMUNCLI.ANDESCRI),this.w_INDICE,1)<>" " AND this.w_INDICE>1
            this.w_INDICE = this.w_INDICE-1
          enddo
          * --- Se non � presente alcuno spazio, eseguo il troncamento alla 25� posizione
          if this.w_INDICE=1
            this.w_INDICE = 25
          endif
          * --- Spezzo a w_INDICE
          this.w_RS1 = SUBSTR(RTRIM(ALMUNCLI.ANDESCRI),1,this.w_INDICE)+SPACE(25)
          this.w_RS2 = SUBSTR(RTRIM(ALMUNCLI.ANDESCRI),this.w_INDICE+1,40)+SPACE(25)
          FWRITE(this.hFile,LEFT(this.w_RS1,25),25)
          FWRITE(this.hFile,LEFT(this.w_RS2,25),25)
        endif
        * --- Codice fiscale
        FWRITE(this.hFile,NVL(ALMUNCLI.ANCODFIS, "                "),16)
        * --- Controllo codice fiscale
        if CHKCFP (ALMUNCLI.ANCODFIS,"CF","",2)
          FWRITE(this.hFile,"E",1)
        else
          FWRITE(this.hFile," ",1)
        endif
        * --- Soggetto intracomunitario
        if ALMUNCLI.AFFLINTR="S"
          FWRITE(this.hFile,"S",1)
        else
          FWRITE(this.hFile," ",1)
        endif
        * --- filler
        this.w_FILLER = SPACE(111)
        FWRITE(this.hFile,this.w_FILLER,111)
        * --- Seconda parte cli/for
        FWRITE(this.hFile,"D11",3)
        * --- Indirizzo
        * --- Via e Numero Civico devono essere separati da virgola
        this.w_INDICE = 25
        do while (SUBSTR(ALMUNCLI.ANINDIRI,this.w_INDICE,1)<>"," and this.w_INDICE>0)
          this.w_INDICE = this.w_INDICE-1
        enddo
        if this.w_INDICE=0
          * --- Virgola non trovata
          if LEN(RTRIM(ALMUNCLI.ANINDIRI))>32
            this.w_STRINGA = Ah_MsgFormat("%1Attenzione! L'indirizzo del cliente %2 sar� tagliato al 32^ carattere","               ",ALMUNCLI.ANCODICE)
            FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
            FWRITE(this.hLOG,CHR(13)+CHR(10),2)
            this.w_WARNING = .T.
          endif
          FWRITE(this.hFile,NVL(ALMUNCLI.ANINDIRI, "                                "),32)
          FWRITE(this.hFile,"     ",5)
        else
          * --- Virgola trovata
          this.w_IND = SUBSTR(ALMUNCLI.ANINDIRI,1,this.w_INDICE-1)+SPACE(32)
          this.w_NUM = SUBSTR(ALMUNCLI.ANINDIRI,this.w_INDICE+1,5)+SPACE(5)
          if LEN(RTRIM(this.w_IND))>32
            this.w_STRINGA = Ah_MsgFormat("%1Attenzione! L'indirizzo del cliente %2 sar� tagliato al 32^ carattere","               ",ALMUNCLI.ANCODICE)
            FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
            FWRITE(this.hLOG,CHR(13)+CHR(10),2)
            this.w_WARNING = .T.
          endif
          if LEN(RTRIM(this.w_NUM))>5
            this.w_STRINGA = Ah_MsgFormat("%1Attenzione! Il numero civico del cliente %2 sar� tagliato al 5^ carattere","               ",ALMUNCLI.ANCODICE)
            FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
            FWRITE(this.hLOG,CHR(13)+CHR(10),2)
            this.w_WARNING = .T.
          endif
          FWRITE(this.hFile,this.w_IND,32)
          FWRITE(this.hFile,this.w_NUM,5)
        endif
        * --- CAP
        FWRITE(this.hFile,NVL(ALMUNCLI.AN___CAP, "     "),5)
        * --- Localit�
        if LEN(RTRIM(ALMUNCLI.ANLOCALI))>23
          this.w_STRINGA = Ah_MsgFormat("%1Attenzione! La localit� del cliente %2 sar� tagliata al 23^ carattere","               ",ALMUNCLI.ANCODICE)
          FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        endif
        FWRITE(this.hFile,NVL(ALMUNCLI.ANLOCALI, "                       "),23)
        FWRITE(this.hFile,NVL(ALMUNCLI.ANPROVIN, "  "),2)
        * --- Partita IVA
        FWRITE(this.hFile,NVL(ALMUNCLI.ANPARIVA, "           "),11)
        if CHKCFP (ALMUNCLI.ANPARIVA, "PI", "C",2,ALMUNCLI.ANCODICE,ALMUNCLI.ANNAZION)
          FWRITE(this.hFile,"E",1)
        else
          FWRITE(this.hFile," ",1)
        endif
        * --- Tipo Soggetto
        if ALMUNCLI.ANPERFIS="S"
          FWRITE(this.hFile,"P",1)
        else
          FWRITE(this.hFile,"S",1)
        endif
        * --- Allegato e Bolla Doganale
        FWRITE(this.hFile,"  ",2)
        * --- filler
        this.w_FILLER = SPACE(115)
        FWRITE(this.hFile,this.w_FILLER,115)
      endif
      skip
    enddo
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT DEI FORNITORI
    SELECT ALMUNFOR
    GO TOP
    do while NOT EOF()
      this.w_OK = .T.
      * --- Se il tipo di Codifica � 'Anagrafica Unica' controllo che la lunghezza della Partita IVA sia 11: Se diversa passo al record successivo...
      if this.w_FLANUN="S" AND LEN(ALLTRIM(ALMUNFOR.ANPARIVA)) <> 11
        this.w_STRINGA = Ah_MsgFormat("%1Export fornitore %2 %3","          ",UPPER(ALMUNFOR.ANCODICE),UPPER(ALMUNFOR.ANDESCRI))
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = Ah_MsgFormat("%1ERRORE: la Partita IVA non � presente o la sua lunghezza non � corretta","              ")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_OK = .F.
      endif
      if this.w_OK
        * --- Messaggio a video
        ah_Msg("Export fornitore: %1",.T.,.F.,.F.,ALMUNFOR.ANCODICE)
        * --- Scrittura su LOG
        this.w_STRINGA = Ah_MsgFormat("%1Export fornitore %2 %3","          ",UPPER(ALMUNFOR.ANCODICE),UPPER(ALMUNFOR.ANDESCRI))
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        * --- Aggiorno il numero di record trasferiti
        this.w_TRT = this.w_TRT+2
        this.w_NUMFOR = this.w_NUMFOR+2
        * --- Prima parte cli/for
        FWRITE(this.hFile,"D10F",4)
        * --- Codice fornitore
        FWRITE(this.hFile,ALMUNFOR.ANCODSTU,5)
        if this.w_FLANUN="S"
          if LEN(ALLTRIM(ALMUNFOR.ANPARIVA)) > 11
            this.w_STRINGA = Ah_MsgFormat("%1Non � stato possibile esportare il fornitore poich� la partita IVA supera gli 11 caratteri","              ")
            FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
            FWRITE(this.hLOG,CHR(13)+CHR(10),2)
          else
            * --- Tipo codifica
            FWRITE(this.hFile,"N",1)
            * --- Codifica cliente
            FWRITE(this.hFile,LEFT(ALLTRIM(ALMUNFOR.ANPARIVA)+REPLICATE("0",11),11),11)
          endif
        else
          * --- Tipo codifica
          FWRITE(this.hFile,"R",1)
          * --- Codifica cliente
          FWRITE(this.hFile,ALLTRIM(this.w_DITTA+ALMUNFOR.ANCODSTU),11)
        endif
        * --- Ragione sociale
        if len(RTRIM(ALMUNFOR.ANDESCRI))<=25
          this.w_RS = NVL(SUBSTR(ALMUNFOR.ANDESCRI,1,25), "                         ")
          FWRITE(this.hFile,this.w_RS,25)
          FWRITE(this.hFile,SPACE(25),25)
        else
          * --- Cerco il primo spazio prima di 25
          this.w_INDICE = 25
          do while SUBSTR(RTRIM(ALMUNFOR.ANDESCRI),this.w_INDICE,1)<>" " AND this.w_INDICE>1
            this.w_INDICE = this.w_INDICE-1
          enddo
          * --- Se non � presente alcuno spazio, eseguo il troncamento alla 25� posizione
          if this.w_INDICE=1
            this.w_INDICE = 25
          endif
          * --- Spezzo a w_INDICE
          this.w_RS1 = SUBSTR(RTRIM(ALMUNFOR.ANDESCRI),1,this.w_INDICE)+SPACE(25)
          this.w_RS2 = SUBSTR(RTRIM(ALMUNFOR.ANDESCRI),this.w_INDICE+1,40)+SPACE(25)
          FWRITE(this.hFile,LEFT(this.w_RS1,25),25)
          FWRITE(this.hFile,LEFT(this.w_RS2,25),25)
        endif
        * --- Codice fiscale
        FWRITE(this.hFile,NVL(ALMUNFOR.ANCODFIS, "                "),16)
        * --- Controllo codice fiscale
        if CHKCFP (ALMUNFOR.ANCODFIS, "CF", "",2)
          FWRITE(this.hFile,"E",1)
        else
          FWRITE(this.hFile," ",1)
        endif
        * --- Soggetto intracomunitario
        if ALMUNFOR.AFFLINTR="S"
          FWRITE(this.hFile,"S",1)
        else
          FWRITE(this.hFile," ",1)
        endif
        * --- Filler
        this.w_FILLER = SPACE(111)
        FWRITE(this.hFile,this.w_FILLER,111)
        * --- Seconda parte cli/for
        FWRITE(this.hFile,"D11",3)
        * --- Indirizzo
        * --- Via e Numero Civico devono essere separati da virgola
        this.w_INDICE = 25
        do while (SUBSTR(ALMUNFOR.ANINDIRI,this.w_INDICE,1)<>"," and this.w_INDICE>0)
          this.w_INDICE = this.w_INDICE-1
        enddo
        if this.w_INDICE=0
          * --- Virgola non trovata
          if LEN(RTRIM(ALMUNFOR.ANINDIRI))>32
            this.w_STRINGA = Ah_MsgFormat("%1Attenzione! L'indirizzo del fornitore %2 sar� tagliato al 32^ carattere","               ",ALMUNFOR.ANCODICE)
            FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
            FWRITE(this.hLOG,CHR(13)+CHR(10),2)
            this.w_WARNING = .T.
          endif
          FWRITE(this.hFile,NVL(ALMUNFOR.ANINDIRI, "                                "),32)
          FWRITE(this.hFile,"     ",5)
        else
          * --- Virgola trovata
          this.w_IND = SUBSTR(ALMUNFOR.ANINDIRI,1,this.w_INDICE-1)+SPACE(32)
          this.w_NUM = SUBSTR(ALMUNFOR.ANINDIRI,this.w_INDICE+1,5)+SPACE(5)
          if LEN(RTRIM(this.w_IND))>32
            this.w_STRINGA = Ah_MsgFormat("%1Attenzione! L'indirizzo del fornitore %2 sar� tagliato al 32^ carattere","               ",ALMUNFOR.ANCODICE)
            FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
            FWRITE(this.hLOG,CHR(13)+CHR(10),2)
            this.w_WARNING = .T.
          endif
          if LEN(RTRIM(this.w_NUM))>5
            this.w_STRINGA = Ah_MsgFormat("%1Attenzione! Il numero civico del fornitore %2 sar� tagliato al 5^ carattere","               ",ALMUNFOR.ANCODICE)
            FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
            FWRITE(this.hLOG,CHR(13)+CHR(10),2)
            this.w_WARNING = .T.
          endif
          FWRITE(this.hFile,this.w_IND,32)
          FWRITE(this.hFile,this.w_NUM,5)
        endif
        * --- CAP
        FWRITE(this.hFile,NVL(ALMUNFOR.AN___CAP, "     "),5)
        * --- Localit�
        if LEN(RTRIM(ALMUNFOR.ANLOCALI))>23
          this.w_STRINGA = Ah_MsgFormat("%1Attenzione! La localit� del fornitore %2 sar� tagliata al 23^ carattere","               ",ALMUNFOR.ANCODICE)
          FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        endif
        FWRITE(this.hFile,NVL(ALMUNFOR.ANLOCALI, "                       "),23)
        FWRITE(this.hFile,NVL(ALMUNFOR.ANPROVIN, "  "),2)
        * --- Partita IVA
        FWRITE(this.hFile,NVL(ALMUNFOR.ANPARIVA, "           "),11)
        if CHKCFP (ALMUNFOR.ANPARIVA, "PI", "F", 2, ALMUNFOR.ANCODICE, ALMUNFOR.ANNAZION)
          FWRITE(this.hFile,"E",1)
        else
          FWRITE(this.hFile," ",1)
        endif
        * --- Tipo Soggetto
        if ALMUNFOR.ANPERFIS="S"
          FWRITE(this.hFile,"P",1)
        else
          FWRITE(this.hFile,"S",1)
        endif
        * --- Allegato e Bolla Doganale
        FWRITE(this.hFile,"  ",2)
        * --- filler
        this.w_FILLER = SPACE(115)
        FWRITE(this.hFile,this.w_FILLER,115)
      endif
      skip
    enddo
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT DEI CONTI
    SELECT ALMUNCON
    GO TOP
    do while NOT EOF()
      * --- Messaggio a Video
      ah_Msg("Export conto: %1",.T.,.F.,.F.,ALMUNCON.LMCODSOT)
      * --- Scrittura su LOG
      this.w_STRINGA = Ah_MsgFormat("%1Export conto %2 %3","          ",UPPER(ALMUNCON.LMCODSOT),UPPER(ALMUNCON.LMDESSOT))
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      * --- Incremento numero di record scritti
      this.w_TRT = this.w_TRT+1
      this.w_NUMSOT = this.w_NUMSOT+1
      * --- Inserisco il tipo record
      FWRITE(this.hFile,"D20",3)
      * --- Scrittura del conto
      FWRITE(this.hFile,"00"+ALMUNCON.LMCODSOT,7)
      * --- Scrittura della descrizione
      FWRITE(this.hFile,ALMUNCON.LMDESSOT,30)
      * --- Scrittura del tipo conto
      FWRITE(this.hFile,ALMUNCON.LMTIPSOT,1)
      * --- Filler
      this.w_FILLER = SPACE(159)
      FWRITE(this.hFile,this.w_FILLER,159)
      skip
    enddo
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT DELLA PRIMANOTA
    this.w_ALMENOUNFATTACQU = .F.
    this.w_ALMENOUNFATTVEND = .F.
    this.w_ALMENOUNFATTCORR = .F.
    this.w_ALMENOUNFATTVENT = .F.
    this.w_ALMENOUNCORRNORM = .F.
    this.w_ALMENOUNCORRVENT = .F.
    this.w_ALMENOUNMOCONTAB = .F.
    * --- Delete from STU_PNTT
    i_nConn=i_TableProp[this.STU_PNTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- SCRITTURA DEL FILE DI LOG
    this.w_STRINGA = Ah_MsgFormat("---------- INIZIO EXPORT PRIMANOTA ----------")
    FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    if this.oParentObject.w_PNTIVA="S"
      * --- FATTURE DI ACQUISTO e NOTE DI CREDITO
      * --- Controllo se esiste almeno una fattura di acquisto nell'intervallo stabilito
      if ISALT()
        vq_exec("..\LEMC\EXE\QUERY\GSLM_FAA.VQR",this,"FATTACQU")
      else
        vq_exec("..\LEMC\EXE\QUERY\GSLM0FAA.VQR",this,"FATTACQU")
      endif
      if RECCOUNT("FATTACQU") > 0
        this.w_ALMENOUNFATTACQU = .T.
      endif
      * --- Scrittura sul log
      this.w_STRINGA = Ah_MsgFormat("-- INIZIO EXPORT FATTURE DI ACQUISTO --")
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      if this.w_ALMENOUNFATTACQU
        * --- Testa
        FWRITE (this.hFile , "FFATTACQU" , 9 )
        this.w_FILLER = SPACE(191)
        FWRITE(this.hFile,this.w_FILLER,191)
        * --- Incremento numero di record scritti totali
        this.w_TRT = this.w_TRT+1
        * --- Incremento numero di record scritti
        this.w_FATTACQU = this.w_FATTACQU+1
        * --- Lancio il batch che butta giu le fatture di acquisto e le Note di credito
        do GSLM_BFA with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Coda
        * --- Incremento numero di record scritti
        this.w_TRT = this.w_TRT+1
        this.w_FATTACQU = this.w_FATTACQU+1
        FWRITE (this.hFile , "EFATTACQU" , 9 )
        FWRITE(this.hFile,RIGHT("00000000"+ALLTRIM(STR(this.w_FATTACQU-2)),8),8)
        this.w_FILLER = SPACE(183)
        FWRITE(this.hFile,this.w_FILLER,183)
        * --- Aggiorno la variabile di gruppi omogeni trasferiti
        this.w_GOT = this.w_GOT+1
        * --- Scrittura sul log
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = Ah_MsgFormat("-- FINE EXPORT FATTURE DI ACQUISTO --")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      else
        * --- Scrittura sul log
        this.w_STRINGA = Ah_MsgFormat("%1NESSUNA FATTURE DI ACQUISTO DA ESPORTARE","          ")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = "-- FINE EXPORT FATTURE DI ACQUISTO --"
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      endif
      SELECT FATTACQU
      USE
      * --- FATTURE DI VENDITA e NOTE DI CREDITO
      * --- Controllo se esiste almeno una fattura di vendita nell'intervallo stabilito
      vq_exec("..\LEMC\EXE\QUERY\GSLM_FAV.VQR",this,"FATTVEND")
      if RECCOUNT("FATTVEND") > 0
        this.w_ALMENOUNFATTVEND = .T.
      endif
      * --- Scrittura sul log
      this.w_STRINGA = Ah_MsgFormat("-- INIZIO EXPORT FATTURE DI VENDITA --")
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      if this.w_ALMENOUNFATTVEND
        * --- Testa
        FWRITE (this.hFile , "FFATTVEND" , 9 )
        this.w_FILLER = SPACE(191)
        FWRITE(this.hFile,this.w_FILLER,191)
        * --- Incremento numero di record scritti
        this.w_TRT = this.w_TRT+1
        this.w_FATTVEND = this.w_FATTVEND+1
        * --- Lancio il batch che butta giu le fatture di vendita e le Note di credito
        do GSLM_BFV with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Coda
        * --- Incremento numero di record scritti
        this.w_TRT = this.w_TRT+1
        this.w_FATTVEND = this.w_FATTVEND+1
        FWRITE (this.hFile , "EFATTVEND" , 9 )
        FWRITE(this.hFile,RIGHT("00000000"+ALLTRIM(STR(this.w_FATTVEND-2)),8),8)
        this.w_FILLER = SPACE(183)
        FWRITE(this.hFile,this.w_FILLER,183)
        * --- Aggiorno la variabile di gruppi omogeni trasferiti
        this.w_GOT = this.w_GOT+1
        * --- Scrittura sul log
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = Ah_MsgFormat("-- FINE EXPORT FATTURE DI VENDITA --")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      else
        * --- Scrittura sul log
        this.w_STRINGA = Ah_MsgFormat("%1NESSUNA FATTURE DI VENDITA DA ESPORTARE","          ")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = "-- FINE EXPORT FATTURE DI VENDITA --"
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      endif
      SELECT FATTVEND
      USE
      * --- FATTURE CORRISPETTIVO
      * --- Controllo se esiste almeno una fattura corrispettiva nell'intervallo stabilito
      vq_exec("..\LEMC\EXE\QUERY\GSLM_FCC.VQR",this,"FATTCORR")
      if RECCOUNT("FATTCORR") > 0
        this.w_ALMENOUNFATTCORR = .T.
      endif
      * --- Scrittura sul log
      this.w_STRINGA = Ah_MsgFormat("-- INIZIO EXPORT FATTURE COMPRESE NEI CORRISPETTIVI --")
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      if this.w_ALMENOUNFATTCORR
        * --- Testa
        FWRITE (this.hFile , "FFATTCORR" , 9 )
        this.w_FILLER = SPACE(191)
        FWRITE(this.hFile,this.w_FILLER,191)
        * --- Incremento numero di record scritti
        this.w_TRT = this.w_TRT+1
        this.w_FATTCORR = this.w_FATTCORR+1
        * --- Lancio la procedura che butta giu le fatture comprese nei corrispettivi
        do GSLM_BFC with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Coda
        this.w_FATTCORR = this.w_FATTCORR+1
        FWRITE (this.hFile , "EFATTCORR" , 9 )
        FWRITE(this.hFile,RIGHT("00000000"+ALLTRIM(STR(this.w_FATTCORR-2)),8),8)
        this.w_FILLER = SPACE(183)
        FWRITE(this.hFile,this.w_FILLER,183)
        * --- Incremento numero di record scritti
        this.w_TRT = this.w_TRT+1
        * --- Aggiorno la variabile di gruppi omogeni trasferiti
        this.w_GOT = this.w_GOT+1
        * --- Scrittura sul log
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = Ah_MsgFormat("-- FINE EXPORT FATTURE COMPRESE NEI CORRISPETTIVI --")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      else
        * --- Scrittura sul log
        this.w_STRINGA = Ah_MsgFormat("%1NESSUNA FATTURA COMPRESA NEI CORRISPETTIVI DA ESPORTARE","          ")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = Ah_MsgFormat("-- FINE EXPORT FATTURE COMPRESE NEI CORRISPETTIVI --")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      endif
      SELECT FATTCORR
      USE
      * --- FATTURE VENTILATE
      * --- Controllo se esiste almeno una fattura ventilata nell'intervallo stabilito
      vq_exec("..\LEMC\EXE\QUERY\GSLM_FAE.VQR",this,"FATTVENT")
      if RECCOUNT("FATTVENT") > 0
        this.w_ALMENOUNFATTVENT = .T.
      endif
      * --- Scrittura sul log
      this.w_STRINGA = Ah_MsgFormat("-- INIZIO EXPORT FATTURE VENTILATE --")
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      if this.w_ALMENOUNFATTVENT
        * --- Testa
        FWRITE (this.hFile , "FFATTVENT" , 9 )
        this.w_FILLER = SPACE(191)
        FWRITE(this.hFile,this.w_FILLER,191)
        * --- Incremento numero di record scritti
        this.w_TRT = this.w_TRT+1
        this.w_FATTVENT = this.w_FATTVENT+1
        * --- Lancio la procedura che butta giu le fatture ventilate
        do GSLM_BFT with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Coda
        this.w_FATTVENT = this.w_FATTVENT+1
        FWRITE (this.hFile , "EFATTVENT" , 9 )
        FWRITE(this.hFile,RIGHT("00000000"+ALLTRIM(STR(this.w_FATTVENT-2)),8),8)
        this.w_FILLER = SPACE(183)
        FWRITE(this.hFile,this.w_FILLER,183)
        * --- Incremento numero di record scritti
        this.w_TRT = this.w_TRT+1
        * --- Aggiorno la variabile di gruppi omogeni trasferiti
        this.w_GOT = this.w_GOT+1
        * --- Scrittura sul log
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = Ah_MsgFormat("-- FINE EXPORT FATTURE VENTILATE --")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      else
        * --- Scrittura sul log
        this.w_STRINGA = Ah_MsgFormat("%1NESSUNA FATTURA VENTILATA DA ESPORTARE","          ")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = Ah_MsgFormat("-- FINE EXPORT FATTURE VENTILATE --")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      endif
      SELECT FATTVENT
      USE
      * --- CORRISPETTIVO NORMALE
      * --- Controllo se esiste almeno un corrispettivo normale nell'intervallo stabilito
      vq_exec("..\LEMC\EXE\QUERY\GSLM_COC.VQR",this,"CORRNORM")
      if RECCOUNT("CORRNORM") > 0
        this.w_ALMENOUNCORRNORM = .T.
      endif
      * --- Scrittura sul log
      this.w_STRINGA = Ah_MsgFormat("-- INIZIO EXPORT CORRISPETTIVI CON SCORPORO --")
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      if this.w_ALMENOUNCORRNORM
        * --- Testa
        FWRITE (this.hFile , "FCORRNORM" , 9 )
        this.w_FILLER = SPACE(191)
        FWRITE(this.hFile,this.w_FILLER,191)
        * --- Incremento numero di record scritti
        this.w_TRT = this.w_TRT+1
        this.w_CORRNORM = this.w_CORRNORM+1
        * --- Lancio la procedura che butta giu il corrispettivo normale
        do GSLM_BCN with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Coda
        this.w_CORRNORM = this.w_CORRNORM+1
        FWRITE (this.hFile , "ECORRNORM" , 9 )
        FWRITE(this.hFile,RIGHT("00000000"+ALLTRIM(STR(this.w_CORRNORM-2)),8),8)
        this.w_FILLER = SPACE(183)
        FWRITE(this.hFile,this.w_FILLER,183)
        * --- Incremento numero di record scritti
        this.w_TRT = this.w_TRT+1
        * --- Aggiorno la variabile di gruppi omogeni trasferiti
        this.w_GOT = this.w_GOT+1
        * --- Scrittura sul log
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = Ah_MsgFormat("-- FINE EXPORT CORRISPETTIVI CON SCORPORO --")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      else
        * --- Scrittura sul log
        this.w_STRINGA = Ah_MsgFormat("%1NESSUNA CORRISPETTIVO CON SCORPORO DA ESPORTARE","          ")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = Ah_MsgFormat("-- FINE EXPORT CORRISPETTIVI CON SCORPORO --")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      endif
      SELECT CORRNORM
      USE
      * --- CORRISPETTIVO VENTILATO
      * --- Controllo se esiste almeno un corrispettivo ventilato nell'intervallo stabilito
      vq_exec("..\LEMC\EXE\QUERY\GSLM_COE.VQR",this,"CORRVENT")
      if RECCOUNT("CORRVENT") > 0
        this.w_ALMENOUNCORRVENT = .T.
      endif
      * --- Scrittura sul log
      this.w_STRINGA = Ah_MsgFormat("-- INIZIO EXPORT CORRISPETTIVI VENTILATI --")
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      if this.w_ALMENOUNCORRVENT
        * --- Testa
        FWRITE (this.hFile , "FCORRVENT" , 9 )
        this.w_FILLER = SPACE(191)
        FWRITE(this.hFile,this.w_FILLER,191)
        * --- Incremento numero di record scritti
        this.w_TRT = this.w_TRT+1
        this.w_CORRVENT = this.w_CORRVENT+1
        * --- Lancio la procedura che butta giu il corrispettivo ventilato
        do GSLM_BCV with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Coda
        this.w_CORRVENT = this.w_CORRVENT+1
        FWRITE (this.hFile , "ECORRVENT" , 9 )
        FWRITE(this.hFile,RIGHT("00000000"+ALLTRIM(STR(this.w_CORRVENT-2)),8),8)
        this.w_FILLER = SPACE(183)
        FWRITE(this.hFile,this.w_FILLER,183)
        * --- Incremento numero di record scritti
        this.w_TRT = this.w_TRT+1
        * --- Aggiorno la variabile di gruppi omogeni trasferiti
        this.w_GOT = this.w_GOT+1
        * --- Scrittura sul log
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = Ah_MsgFormat("-- FINE EXPORT CORRISPETTIVI VENTILATI --")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      else
        * --- Scrittura sul log
        this.w_STRINGA = Ah_MsgFormat("%1NESSUNA CORRISPETTIVO VENTILATO DA ESPORTARE","          ")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = Ah_MsgFormat("-- FINE EXPORT CORRISPETTIVI VENTILATI --")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      endif
      SELECT CORRVENT
      USE
    endif
    * --- MOVIMENTI CONTABILI
    * --- Controllo se esiste almeno un movimento contabile (di giroconto creato dalle fatture UE)
    if this.w_ALMENOUNFATTACQU
      SELECT MOVDAFAT
      if RECCOUNT("MOVDAFAT") > 0
        this.w_ALMENOUNMOCONTAB = .T.
      endif
    endif
    * --- Controllo se esiste almeno un movimento contabile (di giroconto creato dai corrispettivi con scorporo)
    if this.w_ALMENOUNCORRNORM
      SELECT MOVDACOR
      if RECCOUNT("MOVDACOR") > 0
        this.w_ALMENOUNMOCONTAB = .T.
      endif
    endif
    if this.oParentObject.w_PNTNOIVA="S" OR this.w_ALMENOUNMOCONTAB
      if this.oParentObject.w_PNTNOIVA="S"
        * --- Controllo se esiste almeno un movimento contabile nell'intervallo stabilito
        if isalt()
          vq_exec("..\LEMC\EXE\QUERY\GSLM_NON.VQR",this,"MOCONTAB")
          this.w_ISALT = "S"
        else
          vq_exec("..\LEMC\EXE\QUERY\GSLM0NON.VQR",this,"MOCONTAB")
          this.w_ISALT = "S"
          this.w_ISALT = "N"
        endif
        if RECCOUNT("MOCONTAB") > 0
          this.w_ALMENOUNMOCONTAB = .T.
        endif
      endif
      * --- Scrittura sul log
      this.w_STRINGA = Ah_MsgFormat("-- INIZIO EXPORT MOVIMENTI CONTABILI --")
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      if this.w_ALMENOUNMOCONTAB
        SELECT MOCONTAB 
 GO TOP
        this.w_ORIGINETIPO = MOCONTAB.TIPO
        * --- Testa
        if this.w_ORIGINETIPO="50" or !isalt()
          FWRITE (this.hFile , "FMOCONTAB" , 9 )
        else
          FWRITE (this.hFile , "FIPPROORD" , 9 )
        endif
        this.w_FILLER = SPACE(191)
        FWRITE(this.hFile,this.w_FILLER,191)
        * --- Incremento numero di record scritti
        this.w_TRT = this.w_TRT+1
        this.w_MOCONTAB = this.w_MOCONTAB+1
        * --- Lancio la procedura che butta giu i movimenti contabili
        GSLM_BMC(this, this.oParentObject.w_PNTNOIVA )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Coda
        select mocontab 
 go bottom
        this.w_TIPO = MOCONTAB.TIPO
        this.w_MOCONTAB = this.w_MOCONTAB+1
        if this.w_TIPO="50" or !isalt()
          FWRITE (this.hFile , "EMOCONTAB" , 9 )
        else
          FWRITE (this.hFile , "EIPPROORD" , 9 )
        endif
        FWRITE(this.hFile,RIGHT("00000000"+ALLTRIM(STR(this.w_MOCONTAB-2)),8),8)
        this.w_FILLER = SPACE(183)
        FWRITE(this.hFile,this.w_FILLER,183)
        * --- Incremento numero di record scritti
        this.w_TRT = this.w_TRT+1
        * --- Aggiorno la variabile di gruppi omogeni trasferiti
        this.w_GOT = this.w_GOT+1
        * --- Scrittura sul log
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = Ah_MsgFormat("-- FINE EXPORT MOVIMENTI CONTABILI --")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      else
        * --- Scrittura sul log
        this.w_STRINGA = Ah_MsgFormat("%1NESSUN MOVIMENTO CONTABILE DA ESPORTARE","          ")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = Ah_MsgFormat("-- FINE EXPORT MOVIMENTI CONTABILI --")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      endif
      if used("MOCONTAB")
        SELECT MOCONTAB
        USE
      endif
    endif
    * --- SCRITTURA DEL FILE DI LOG
    this.w_STRINGA = Ah_MsgFormat("---------- FINE EXPORT PRIMANOTA ----------")
    FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    * --- Controllo se tutta la prima nota � andata a buon fine
    this.w_CONDIZ = this.w_ALMENOUNFATTACQU OR this.w_ALMENOUNFATTVEND OR this.w_ALMENOUNFATTCORR OR this.w_ALMENOUNFATTVENT
    this.w_CONDIZ = this.w_ALMENOUNCORRNORM OR this.w_ALMENOUNCORRVENT OR this.w_ALMENOUNMOCONTAB OR this.w_CONDIZ
    if this.w_CONDIZ
      * --- Select from STU_PNTT
      i_nConn=i_TableProp[this.STU_PNTT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2],.t.,this.STU_PNTT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" STU_PNTT ";
            +" where LMNUMTRA=-1";
             ,"_Curs_STU_PNTT")
      else
        select * from (i_cTable);
         where LMNUMTRA=-1;
          into cursor _Curs_STU_PNTT
      endif
      if used('_Curs_STU_PNTT')
        select _Curs_STU_PNTT
        locate for 1=1
        do while not(eof())
        this.w_PNTNOTAOK = .F.
          select _Curs_STU_PNTT
          continue
        enddo
        use
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='PNT_MAST'
    this.cWorkTables[4]='STU_PARA'
    this.cWorkTables[5]='STU_PIAC'
    this.cWorkTables[6]='STU_PNTT'
    this.cWorkTables[7]='AZIENDA'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_STU_PNTT')
      use in _Curs_STU_PNTT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
