* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslmabic                                                        *
*              Importa codici studio                                           *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-03                                                      *
* Last revis.: 2017-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslmabic",oParentObject)
return(i_retval)

define class tgslmabic as StdBatch
  * --- Local variables
  w_COD = space(2)
  w_DES = space(40)
  w_SIG = space(20)
  w_TIP = space(1)
  w_IVA = space(10)
  w_VEN = space(10)
  w_TVA = space(1)
  w_SE1 = space(10)
  w_INI = space(10)
  w_SE2 = space(10)
  w_FIN = space(10)
  w_ALI = space(1)
  w_COV = space(2)
  w_CODICE = space(10)
  w_ALL = space(1)
  w_CVA = space(2)
  w_MOD = space(10)
  w_AGR = space(10)
  w_DE1 = space(20)
  w_FOX1 = 0
  w_FOX2 = 0
  w_INIVAL = ctod("  /  /  ")
  w_FINVAL = ctod("  /  /  ")
  w_SECOLO = space(4)
  hFile = 0
  LUNRECORD = 0
  w_STATOINTRA = 0
  w_STATOIVA = 0
  w_STATONORMA = 0
  CF = space(10)
  PC = space(10)
  CC = space(10)
  MESS1 = space(10)
  MESS2 = space(10)
  MESS3 = space(10)
  w_STARTSEEK = 0
  w_DES1 = space(40)
  w_TIPO = space(1)
  w_INIVALCEE = ctod("  /  /  ")
  w_FINVALCEE = ctod("  /  /  ")
  w_CESSCEE = ctod("  /  /  ")
  w_TIPREG = space(3)
  w_TIPOREG = space(1)
  w_TIPOALIQUOTA = space(3)
  w_TIPOALIQ = space(1)
  w_DATCESS = ctod("  /  /  ")
  w_IVA1 = 0
  w_CVANORM = space(2)
  w_INIVALCEE = ctod("  /  /  ")
  w_FINVALCEE = ctod("  /  /  ")
  w_CESSCEE = ctod("  /  /  ")
  * --- WorkFile variables
  STU_CIVA_idx=0
  STU_INTR_idx=0
  STU_NORM_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch di import codici norme,iva e intra
    this.LUNRECORD = 500
    * --- Variabili di stato
    this.w_STATOINTRA = 2
    this.w_STATOIVA = 2
    this.w_STATONORMA = 2
    * --- Lancio finestra di GetFile()
    if NOT EMPTY(this.oParentObject.w_FILENAME1)
      w_IMPFILE=this.oParentObject.w_FILENAME1
    else
      w_IMPFILE=GetFile("D","File di Import","Conferma",0,"Seleziona File di Import")
    endif
    if .not. empty(w_IMPFILE)
      * --- Apertura file di import
      this.hFile = FOPEN(w_IMPFILE,0)
      if (this.hFile<>-1)
        * --- Import codici INTRA
        if this.oParentObject.w_CODINT="S"
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Import codici IVA
        if this.oParentObject.w_CODIVA="S"
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Import codici NORMA
        if this.oParentObject.w_CODNOR="S"
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Chiusura del file
        FCLOSE(this.hFile)
        * --- Messaggio sullo stato dell'import
        if (this.w_STATOINTRA=1 .and. this.w_STATOIVA=1 .and. this.w_STATONORMA=1)
          ah_ErrorMsg("Import dei codici norme, IVA e INTRA eseguito con successo",,"")
        else
          * --- Preparazione del messaggio da visualizzare
          do case
            case this.w_STATOINTRA=0
              this.MESS1 = "Import codici intracomunitari= Fallito%0"
            case this.w_STATOINTRA=1
              this.MESS1 = "Import codici intracomunitari= Successo%0"
            otherwise
              this.MESS1 = "Import codici intracomunitari= Non eseguito%0"
          endcase
          do case
            case this.w_STATOIVA=0
              this.MESS2 = "Import codici IVA= Fallito%0"
            case this.w_STATOIVA=1
              this.MESS2 = "Import codici IVA= Successo%0"
            otherwise
              this.MESS2 = "Import codici IVA= Non eseguito%0"
          endcase
          do case
            case this.w_STATONORMA=0
              this.MESS3 = "Import codici norme=Fallito"
            case this.w_STATONORMA=1
              this.MESS3 = "Import codici norme=Successo"
            otherwise
              this.MESS3 = "Import codici norme=Non eseguito"
          endcase
          ah_ErrorMsg(this.MESS1+this.MESS2+this.MESS3,,"")
        endif
      else
        this.MESS1 = "Impossibile aprire il file di import"
        ah_ErrorMsg(this.MESS1,,"")
      endif
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- IMPORT CODICI INTRA
    * --- Posiziono il file all'inizio del recorde che identifica i codici INTRA
    this.w_STARTSEEK = 0
    this.w_CODICE = FREAD(this.hFile,3)
    FSEEK(this.hFile,-3,1)
    do while ((this.w_CODICE<>"D25") .and. (.not. FEOF(this.hFile)))
      * --- Skippo un record
      FSEEK(this.hFile,this.LUNRECORD,this.w_STARTSEEK)
      * --- Rileggo il codice
      this.w_CODICE = FREAD(this.hFile,3)
      FSEEK(this.hFile,-3,1)
      this.w_STARTSEEK = 1
    enddo
    if this.w_CODICE<>"D25"
      this.w_STATOINTRA=0
      return
    endif
    * --- Ciclo di lettura
    do while ((this.w_CODICE="D25") .and. (.not. FEOF(this.hFile)))
      * --- Leggo il nuovo codice
      this.w_CODICE = FREAD(this.hFile,3)
      if this.w_CODICE<>"D25"
        FSEEK(this.hFile,-3,1)
        exit
      endif
      * --- Leggo il codice CEE
      this.w_COD = FREAD(this.hFile,2)
      * --- Messaggio a Video
      ah_Msg("Import codice INTRA: %1",.T.,.F.,.F.,this.w_COD)
      * --- Leggo la descrizione
      this.w_DES = FREAD(this.hFile,60)
      this.w_DES1 = LEFT(this.w_DES,40)
      * --- Leggo la descrizione sigla
      this.w_SIG = FREAD(this.hFile,10)
      * --- Leggo il tipo operazione
      this.w_TIP = FREAD(this.hFile,3)
      this.w_TIPO = IIF(this.w_TIP="001","A",IIF(this.w_TIP="002","V",""))
      * --- Data inizio validit�
      this.w_INIVALCEE = CP_CHARTODATE(FREAD(this.hFile,8))
      * --- Data fine validit�
      this.w_FINVALCEE = CP_CHARTODATE(FREAD(this.hFile,8))
      * --- Data cessazione
      this.w_CESSCEE = FREAD(this.hFile,8)
      * --- Filler
      FREAD(this.hFile,398)
      * --- Inserisco in tabella
      * --- Scrivo il record
      * --- Try
      local bErr_04666AA0
      bErr_04666AA0=bTrsErr
      this.Try_04666AA0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into STU_INTR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.STU_INTR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_INTR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_INTR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LMDESOPE ="+cp_NullLink(cp_ToStrODBC(this.w_DES1),'STU_INTR','LMDESOPE');
          +",LMDESABB ="+cp_NullLink(cp_ToStrODBC(this.w_SIG),'STU_INTR','LMDESABB');
          +",LMTIPOPE ="+cp_NullLink(cp_ToStrODBC(this.w_TIPO),'STU_INTR','LMTIPOPE');
              +i_ccchkf ;
          +" where ";
              +"LMCODOPE = "+cp_ToStrODBC(this.w_COD);
                 )
        else
          update (i_cTable) set;
              LMDESOPE = this.w_DES1;
              ,LMDESABB = this.w_SIG;
              ,LMTIPOPE = this.w_TIPO;
              &i_ccchkf. ;
           where;
              LMCODOPE = this.w_COD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Fallita la scrittura in Operazioni Intra Studio'
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_04666AA0
      * --- End
    enddo
    * --- Successo operazione
    this.w_STATOINTRA = 1
  endproc
  proc Try_04666AA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_INTR
    i_nConn=i_TableProp[this.STU_INTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_INTR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_INTR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMCODOPE"+",LMDESOPE"+",LMDESABB"+",LMTIPOPE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_COD),'STU_INTR','LMCODOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DES1),'STU_INTR','LMDESOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SIG),'STU_INTR','LMDESABB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPO),'STU_INTR','LMTIPOPE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMCODOPE',this.w_COD,'LMDESOPE',this.w_DES1,'LMDESABB',this.w_SIG,'LMTIPOPE',this.w_TIPO)
      insert into (i_cTable) (LMCODOPE,LMDESOPE,LMDESABB,LMTIPOPE &i_ccchkf. );
         values (;
           this.w_COD;
           ,this.w_DES1;
           ,this.w_SIG;
           ,this.w_TIPO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Fallito inserimento in Operazioni Intra Studio'
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- IMPORT CODICI IVA
    * --- Posiziono il file all'inizio del recorde che identifica i codici INTRA
    this.w_STARTSEEK = 0
    this.w_CODICE = FREAD(this.hFile,3)
    FSEEK(this.hFile,-3,1)
    do while ((this.w_CODICE<>"D23") .and. (.not. FEOF(this.hFile)))
      * --- Skippo un record
      FSEEK(this.hFile,this.LUNRECORD,this.w_STARTSEEK)
      * --- Rileggo il codice
      this.w_CODICE = FREAD(this.hFile,3)
      FSEEK(this.hFile,-3,1)
      this.w_STARTSEEK = 1
    enddo
    if this.w_CODICE<>"D23"
      this.w_STATOIVA=0
      return
    endif
    * --- Ciclo di lettura
    do while ((this.w_CODICE="D23") .and. (.not. FEOF(this.hFile)))
      * --- Leggo il nuovo codice
      this.w_CODICE = FREAD(this.hFile,3)
      if this.w_CODICE<>"D23"
        FSEEK(this.hFile,-3,1)
        exit
      endif
      * --- Leggo il codice IVA
      this.w_COD = FREAD(this.hFile,2)
      * --- Messaggio a Video
      ah_Msg("Import codice IVA: %1",.T.,.F.,.F.,this.w_COD)
      * --- Leggo la descrizione
      this.w_DES = FREAD(this.hFile,60)
      this.w_DES1 = LEFT(this.w_DES,40)
      * --- Leggo la percentuale IVA
      this.w_IVA = FREAD(this.hFile,5)
      * --- Leggo il codice IVA in ventilazione
      this.w_COV = FREAD(this.hFile,2)
      * --- Tipo di registrazione
      this.w_TIPREG = FREAD(this.hFile,3)
      this.w_TIPOREG = IIF(this.w_TIPREG="001","A",IIF(this.w_TIPREG="002","V","X"))
      * --- Tipo di aliquota
      this.w_TIPOALIQUOTA = FREAD(this.hFile,3)
      this.w_TIPOALIQ = IIF(this.w_TIPOALIQUOTA="001","A",IIF(this.w_TIPOALIQUOTA="002","N",""))
      * --- Leggo la data inizio attivita (secolo+anno+mese+giorno)
      this.w_INIVAL = CP_CHARTODATE(FREAD(this.hFile,8))
      * --- Leggo la data fine attivita (secolo+anno+mese+giorno)
      this.w_FINVAL = CP_CHARTODATE(FREAD(this.hFile,8))
      * --- Data cessazione
      this.w_DATCESS = FREAD(this.hFile,8)
      * --- Filler
      FREAD(this.hFile,398)
      this.w_IVA1 = CP_ROUND(VAL(LEFT(this.w_IVA,3)+","+RIGHT(this.w_IVA,2)),2)
      * --- Scrivo il record
      * --- Try
      local bErr_04680630
      bErr_04680630=bTrsErr
      this.Try_04680630()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into STU_CIVA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.STU_CIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_CIVA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_CIVA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LMCODVEN ="+cp_NullLink(cp_ToStrODBC(this.w_COV),'STU_CIVA','LMCODVEN');
          +",LMDESIVA ="+cp_NullLink(cp_ToStrODBC(this.w_DES1),'STU_CIVA','LMDESIVA');
          +",LMFINVAL ="+cp_NullLink(cp_ToStrODBC(this.w_FINVAL),'STU_CIVA','LMFINVAL');
          +",LMINIVAL ="+cp_NullLink(cp_ToStrODBC(this.w_INIVAL),'STU_CIVA','LMINIVAL');
          +",LMPERIVA ="+cp_NullLink(cp_ToStrODBC(this.w_IVA1),'STU_CIVA','LMPERIVA');
          +",LMTIPCOD ="+cp_NullLink(cp_ToStrODBC(this.w_TIPOALIQ),'STU_CIVA','LMTIPCOD');
          +",LMTIPREG ="+cp_NullLink(cp_ToStrODBC(this.w_TIPOREG),'STU_CIVA','LMTIPREG');
              +i_ccchkf ;
          +" where ";
              +"LMCODIVA = "+cp_ToStrODBC(this.w_COD);
                 )
        else
          update (i_cTable) set;
              LMCODVEN = this.w_COV;
              ,LMDESIVA = this.w_DES1;
              ,LMFINVAL = this.w_FINVAL;
              ,LMINIVAL = this.w_INIVAL;
              ,LMPERIVA = this.w_IVA1;
              ,LMTIPCOD = this.w_TIPOALIQ;
              ,LMTIPREG = this.w_TIPOREG;
              &i_ccchkf. ;
           where;
              LMCODIVA = this.w_COD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Fallita la scrittura in Codici IVA Studio'
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_04680630
      * --- End
    enddo
    * --- Inserisco in tabella il codice iva XX
    this.w_COD = ALLTRIM("XX")
    this.w_DES = Ah_MsgFormat("Codice IVA non definito")
    * --- Scrivo il record
    * --- Try
    local bErr_04671CF0
    bErr_04671CF0=bTrsErr
    this.Try_04671CF0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Write into STU_CIVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.STU_CIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STU_CIVA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_CIVA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"LMDESIVA ="+cp_NullLink(cp_ToStrODBC(this.w_DES1),'STU_CIVA','LMDESIVA');
            +i_ccchkf ;
        +" where ";
            +"LMCODIVA = "+cp_ToStrODBC(this.w_COD);
               )
      else
        update (i_cTable) set;
            LMDESIVA = this.w_DES1;
            &i_ccchkf. ;
         where;
            LMCODIVA = this.w_COD;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Fallita la scrittura in Codici IVA Studio'
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_04671CF0
    * --- End
    * --- Successo operazione
    this.w_STATOIVA = 1
  endproc
  proc Try_04680630()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_CIVA
    i_nConn=i_TableProp[this.STU_CIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_CIVA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_CIVA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMTIPREG"+",LMTIPCOD"+",LMPERIVA"+",LMINIVAL"+",LMFINVAL"+",LMDESIVA"+",LMCODVEN"+",LMCODIVA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_TIPOREG),'STU_CIVA','LMTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPOALIQ),'STU_CIVA','LMTIPCOD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IVA1),'STU_CIVA','LMPERIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INIVAL),'STU_CIVA','LMINIVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FINVAL),'STU_CIVA','LMFINVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DES1),'STU_CIVA','LMDESIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COV),'STU_CIVA','LMCODVEN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COD),'STU_CIVA','LMCODIVA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMTIPREG',this.w_TIPOREG,'LMTIPCOD',this.w_TIPOALIQ,'LMPERIVA',this.w_IVA1,'LMINIVAL',this.w_INIVAL,'LMFINVAL',this.w_FINVAL,'LMDESIVA',this.w_DES1,'LMCODVEN',this.w_COV,'LMCODIVA',this.w_COD)
      insert into (i_cTable) (LMTIPREG,LMTIPCOD,LMPERIVA,LMINIVAL,LMFINVAL,LMDESIVA,LMCODVEN,LMCODIVA &i_ccchkf. );
         values (;
           this.w_TIPOREG;
           ,this.w_TIPOALIQ;
           ,this.w_IVA1;
           ,this.w_INIVAL;
           ,this.w_FINVAL;
           ,this.w_DES1;
           ,this.w_COV;
           ,this.w_COD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Chiave gi� utilizzata'
      return
    endif
    return
  proc Try_04671CF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_CIVA
    i_nConn=i_TableProp[this.STU_CIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_CIVA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_CIVA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMDESIVA"+",LMCODIVA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_DES1),'STU_CIVA','LMDESIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COD),'STU_CIVA','LMCODIVA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMDESIVA',this.w_DES1,'LMCODIVA',this.w_COD)
      insert into (i_cTable) (LMDESIVA,LMCODIVA &i_ccchkf. );
         values (;
           this.w_DES1;
           ,this.w_COD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Chiave gi� utilizzata'
      return
    endif
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- IMPORT CODICI NORME
    * --- Posiziono il file all'inizio del recorde che identifica i codici INTRA
    this.w_STARTSEEK = 0
    this.w_CODICE = FREAD(this.hFile,3)
    FSEEK(this.hFile,-3,1)
    do while ((this.w_CODICE<>"D24") .and. (.not. FEOF(this.hFile)))
      * --- Skippo un record
      FSEEK(this.hFile,this.LUNRECORD,this.w_STARTSEEK)
      * --- Rileggo il codice
      this.w_CODICE = FREAD(this.hFile,3)
      FSEEK(this.hFile,-3,1)
      this.w_STARTSEEK = 1
    enddo
    if this.w_CODICE<>"D24"
      this.w_STATONORMA=0
      return
    endif
    * --- Ciclo di lettura
    do while ((this.w_CODICE="D24") .and. (.not. FEOF(this.hFile)))
      * --- Leggo il nuovo codice
      this.w_CODICE = FREAD(this.hFile,3)
      if this.w_CODICE<>"D24"
        FSEEK(this.hFile,-3,1)
        exit
      endif
      * --- Leggo il codice NORMA
      this.w_COD = FREAD(this.hFile,2)
      * --- Messaggio a Video
      ah_Msg("Import codice norma: %1",.T.,.F.,.F.,this.w_COD)
      * --- Leggo la descrizione
      this.w_DES = FREAD(this.hFile,60)
      this.w_DES1 = LEFT(this.w_DES,40)
      * --- Leggo l'ulteriore descrizione
      this.w_DE1 = FREAD(this.hFile,10)
      * --- Leggo la percentuale indetraibilit�
      this.w_IVA = FREAD(this.hFile,5)
      * --- Leggo il codice IVA
      this.w_CVA = FREAD(this.hFile,2)
      this.w_CVANORM = IIF(EMPTY(NVL(this.w_CVA,"")),"XX",this.w_CVA)
      * --- Leggo il test validit�
      this.w_TVA = FREAD(this.hFile,3)
      this.w_TIPO = IIF(this.w_TVA="001","A",IIF(this.w_TVA="002","V","X"))
      * --- Leggo la norma agricoltura
      this.w_AGR = FREAD(this.hFile,1)
      * --- Data inizio validit�
      this.w_INIVALCEE = CP_CHARTODATE(FREAD(this.hFile,8))
      * --- Data fine validit�
      this.w_FINVALCEE = CP_CHARTODATE(FREAD(this.hFile,8))
      * --- Data cessazione
      this.w_CESSCEE = FREAD(this.hFile,8)
      * --- Filler
      FREAD(this.hFile,390)
      this.w_FOX1 = CP_ROUND(VAL(LEFT(this.w_IVA,3)+","+RIGHT(this.w_IVA,2)),2)
      * --- Scrivo il record
      * --- Try
      local bErr_046C08F8
      bErr_046C08F8=bTrsErr
      this.Try_046C08F8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into STU_NORM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.STU_NORM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_NORM_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_NORM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LMDESNOR ="+cp_NullLink(cp_ToStrODBC(this.w_DES1),'STU_NORM','LMDESNOR');
          +",LMDEANOR ="+cp_NullLink(cp_ToStrODBC(this.w_DE1),'STU_NORM','LMDEANOR');
          +",LMINDETR ="+cp_NullLink(cp_ToStrODBC(this.w_FOX1),'STU_NORM','LMINDETR');
          +",LMCODIVA ="+cp_NullLink(cp_ToStrODBC(this.w_CVANORM),'STU_NORM','LMCODIVA');
          +",LMTIPOLO ="+cp_NullLink(cp_ToStrODBC(this.w_TIPO),'STU_NORM','LMTIPOLO');
              +i_ccchkf ;
          +" where ";
              +"LMCODNOR = "+cp_ToStrODBC(this.w_COD);
                 )
        else
          update (i_cTable) set;
              LMDESNOR = this.w_DES1;
              ,LMDEANOR = this.w_DE1;
              ,LMINDETR = this.w_FOX1;
              ,LMCODIVA = this.w_CVANORM;
              ,LMTIPOLO = this.w_TIPO;
              &i_ccchkf. ;
           where;
              LMCODNOR = this.w_COD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Fallita la scrittura in Codici Norme Studio'
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_046C08F8
      * --- End
    enddo
    * --- Successo operazione
    this.w_STATONORMA = 1
  endproc
  proc Try_046C08F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_NORM
    i_nConn=i_TableProp[this.STU_NORM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_NORM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_NORM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMCODNOR"+",LMDESNOR"+",LMDEANOR"+",LMINDETR"+",LMCODIVA"+",LMTIPOLO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_COD),'STU_NORM','LMCODNOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DES1),'STU_NORM','LMDESNOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DE1),'STU_NORM','LMDEANOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FOX1),'STU_NORM','LMINDETR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVANORM),'STU_NORM','LMCODIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPO),'STU_NORM','LMTIPOLO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMCODNOR',this.w_COD,'LMDESNOR',this.w_DES1,'LMDEANOR',this.w_DE1,'LMINDETR',this.w_FOX1,'LMCODIVA',this.w_CVANORM,'LMTIPOLO',this.w_TIPO)
      insert into (i_cTable) (LMCODNOR,LMDESNOR,LMDEANOR,LMINDETR,LMCODIVA,LMTIPOLO &i_ccchkf. );
         values (;
           this.w_COD;
           ,this.w_DES1;
           ,this.w_DE1;
           ,this.w_FOX1;
           ,this.w_CVANORM;
           ,this.w_TIPO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Chiave gi� utilizzata'
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='STU_CIVA'
    this.cWorkTables[2]='STU_INTR'
    this.cWorkTables[3]='STU_NORM'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
