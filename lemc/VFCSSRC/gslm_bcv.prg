* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_bcv                                                        *
*              Export corrispettivi ventilati                                  *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_139]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-24                                                      *
* Last revis.: 2008-09-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm_bcv",oParentObject)
return(i_retval)

define class tgslm_bcv as StdBatch
  * --- Local variables
  w_STRINGA = space(120)
  w_ACTUALPOS = 0
  w_PNSERIAL = space(10)
  w_TOTDOC = 0
  w_NOTRAS = 0
  w_ANCODSTU = space(5)
  w_PNTIPCON = space(1)
  w_PNCODCON = space(15)
  w_NUMDETPERREG = 0
  * --- WorkFile variables
  CONTI_idx=0
  STU_PNTT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT CORRISPETTIVI VENTILATI
    SELECT CORRVENT
    GO TOP
    * --- Ciclo sul cursore dei corrispettivi ventilati
    do while NOT EOF()
      * --- Messaggio a schermo
      ah_Msg("Export corrispettivi ventilati: reg. num. %1 - data %2",.T.,.F.,.F.,alltrim(STR(CORRVENT.PNNUMRER,6,0)),dtoc(CORRVENT.PNDATREG))
      * --- Scrittura su LOG
      this.w_STRINGA = space(10)+Ah_MsgFormat("Export corrispettivi ventilati: reg. num. %1 - data %2",ALLTRIM(STR(CORRVENT.PNNUMRER,6,0)),dtoc(CORRVENT.PNDATREG))
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      * --- Salvo la posizione attuale del record e il seriale della registrazione
      this.w_ACTUALPOS = RECNO()
      this.w_PNSERIAL = CORRVENT.PNSERIAL
      this.w_TOTDOC = 0
      * --- Conto il numero di dettagli per questa registrazione
      this.w_NUMDETPERREG = 0
      do while CORRVENT.PNSERIAL=this.w_PNSERIAL
        this.w_NUMDETPERREG = this.w_NUMDETPERREG+1
        skip 1
      enddo
      * --- Ripristino posizione
      go this.w_ACTUALPOS
      * --- Controllo se � una registrazione valida
      if (this.w_NUMDETPERREG%2<>0)
        * --- Marco questa registrazione come non valida
        go this.w_ACTUALPOS
        this.w_NOTRAS = -1
        * --- Insert into STU_PNTT
        i_nConn=i_TableProp[this.STU_PNTT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NOTRAS),'STU_PNTT','LMNUMTRA');
          +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.w_NOTRAS,'LMNUMTR2',0)
          insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.w_NOTRAS;
               ,0;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='GSLM_BCV: Scrittura con errore in STU_PNTT'
          return
        endif
        do while CORRVENT.PNSERIAL=this.w_PNSERIAL
          * --- Avanzo il puntatore
          skip 1
        enddo
      else
        do while CORRVENT.PNSERIAL=this.w_PNSERIAL
          * --- Registrazione valida
          * --- Incremento il numero di record trasferiti
          this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
          this.oParentObject.w_CORRVENT = this.oParentObject.w_CORRVENT+1
          * --- Tipo record
          FWRITE(this.oParentObject.hFile,"D40",3)
          * --- Recupero e inserisco l' anno e il mese della registrazione
          FWRITE(this.oParentObject.hFile,LEFT(dtos(CORRVENT.PNDATREG),6),6)
          * --- Recupero e inserisco l' anno di competenza
          FWRITE(this.oParentObject.hFile,CORRVENT.PNCOMPET,4)
          * --- Recupero e inserisco il mese di competenza
          FWRITE(this.oParentObject.hFile,SUBSTR(dtos(CORRVENT.PNCOMIVA),5,2),2)
          * --- Sezione
          FWRITE(this.oParentObject.hFile,RIGHT("00"+ALLTRIM(STR(CORRVENT.PNNUMREG-1,2,0)),2),2)
          * --- Tipo Corrispettivo
          FWRITE(this.oParentObject.hFile,"SC",2)
          * --- Data registrazione
          FWRITE(this.oParentObject.hFile,dtos(CORRVENT.PNDATREG),8)
          * --- Scrittura del codice IVA
          FWRITE(this.oParentObject.hFile,"    ",4)
          * --- Importo Totale
          this.w_TOTDOC = CORRVENT.PNIMPDAR-CORRVENT.PNIMPAVE
          if g_PERVAL=this.oParentObject.w_VALEUR
            FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOC)),11,0))+RIGHT(STR(this.w_TOTDOC,12,2),2),11),11)
          else
            FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(this.w_TOTDOC),11,0)),11),11)
          endif
          * --- Segno
          FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOC>=0, "+", "-"),1)
          * --- Numero documentio dal al
          FWRITE(this.oParentObject.hFile,"0000000000",10)
          * --- Scrittura codice norma
          FWRITE(this.oParentObject.hFile,"  ",2)
          * --- Codice centro di costo
          FWRITE(this.oParentObject.hFile,"00",2)
          * --- Contropartita
          if CORRVENT.ANTIPSOT="V"
            * --- Leggo il codice di contropartita
            this.w_PNTIPCON = CORRVENT.PNTIPCON
            this.w_PNCODCON = CORRVENT.PNCODCON
          else
            * --- Avanzo al record successivo
            skip 1
            * --- Leggo il codice di contropartita
            this.w_PNTIPCON = CORRVENT.PNTIPCON
            this.w_PNCODCON = CORRVENT.PNCODCON
            * --- Ripristino
            skip -1
          endif
          * --- Decodifico la contropartita
          this.w_ANCODSTU = SPACE(5)
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANCODSTU"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANCODSTU;
              from (i_cTable) where;
                  ANTIPCON = this.w_PNTIPCON;
                  and ANCODICE = this.w_PNCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          SELECT CORRVENT
          if EMPTY(this.w_ANCODSTU)
            this.w_ANCODSTU = "00000"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
            FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
            FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
            this.oParentObject.w_ERRORE = .T.
          endif
          FWRITE(this.oParentObject.hFile,this.w_ANCODSTU,5)
          * --- Codice Cassa
          if CORRVENT.ANTIPSOT<>"V"
            this.w_PNTIPCON = CORRVENT.PNTIPCON
            this.w_PNCODCON = CORRVENT.PNCODCON
          else
            * --- Avanzo al record successivo
            skip 1
            this.w_PNTIPCON = CORRVENT.PNTIPCON
            this.w_PNCODCON = CORRVENT.PNCODCON
            * --- Ripristino
            skip -1
          endif
          this.w_ANCODSTU = SPACE(5)
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANCODSTU"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANCODSTU;
              from (i_cTable) where;
                  ANTIPCON = this.w_PNTIPCON;
                  and ANCODICE = this.w_PNCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          SELECT CORRVENT
          if EMPTY(this.w_ANCODSTU)
            this.w_ANCODSTU = "00000"
            this.w_STRINGA = Ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
            FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
            FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
            this.oParentObject.w_ERRORE = .T.
          endif
          * --- Scrivo su file
          FWRITE(this.oParentObject.hFile,this.w_ANCODSTU,5)
          * --- Codice CEE
          FWRITE(this.oParentObject.hFile,"  ",2)
          * --- Flag corrispettivo a calendario
          FWRITE(this.oParentObject.hFile," ",1)
          * --- Recupero valuta di conto
          FWRITE(this.oParentObject.hFile , IIF(g_PERVAL=this.oParentObject.w_VALEUR, "E", " ") , 1)
          * --- Filler
          this.oParentObject.w_FILLER = SPACE(129)
          FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,129)
          * --- Avanzo di due record
          skip 2
        enddo
        * --- Insert into STU_PNTT
        i_nConn=i_TableProp[this.STU_PNTT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'STU_PNTT','LMNUMTRA');
          +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.oParentObject.w_PROFIL,'LMNUMTR2',0)
          insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.oParentObject.w_PROFIL;
               ,0;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='GSLM_BCV: Scrittura in STU_PNTT'
          return
        endif
        SELECT CORRVENT
      endif
    enddo
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='STU_PNTT'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
