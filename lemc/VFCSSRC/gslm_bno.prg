* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_bno                                                        *
*              Normalizzazione file ASCII                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30][VRS_31]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-23                                                      *
* Last revis.: 2012-11-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pLUNGREC,pPATH,pFILEORIG
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm_bno",oParentObject,m.pLUNGREC,m.pPATH,m.pFILEORIG)
return(i_retval)

define class tgslm_bno as StdBatch
  * --- Local variables
  pLUNGREC = 0
  pPATH = space(200)
  pFILEORIG = space(200)
  w_FILEDEST = space(200)
  w_OK = .f.
  w_RIGA = space(380)
  w_RIGAMOD = space(380)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine di linearizzazione file ASCII per import
    this.w_OK = .T.
    this.w_FILEDEST = alltrim(this.pPATH)+"\FILENORM.txt"
    * --- Usare stapdf
     
 L_Err=.f. 
 L_OLDERROR=On("Error") 
 on error L_err=.t.
    if file(this.w_FILEDEST)
      Delete File this.w_FILEDEST
    endif
    handle=FCreate(this.w_FILEDEST)
    if handle<0
      on error &L_OldError 
      ah_errormsg("Errore durante creazione file %1 errore %2-%3!","!","",this.w_FILEDEST,Message(),Message(1))
      i_retcode = 'stop'
      return
    endif
    Forig = Fopen(this.pFILEORIG)
    if Forig<0
      on error &L_OldError 
      ah_errormsg("Errore durante apertura file %1 errore %2-%3","!","",this.pFILEORIG,Message(),Message(1))
      i_retcode = 'stop'
      return
    endif
    do while ! feof(Forig)
      this.w_RIGA = Fgets(Forig,this.pLUNGREC)
      * --- Duplico la riga attuale, questo perch� nella stessa riga o sia il conto dare che il conto avere.
      *     Per come � gestito l'import di Primanota in AHR questo comporta la duplicazione della riga stessa
      *     la prima con il solo conto in dare e la seconda con il solo conto in avere, l'importo � sempre lo stesso.
      * --- Creo riga conto dare
      Fputs(Handle,this.w_RIGA,this.pLUNGREC)
    enddo
    if ! Fclose(handle) 
      ah_errormsg("Errore durante la chiusura del file %1","!","",this.w_FILEDEST)
    endif
    if ! Fclose(Forig) 
      ah_errormsg("Errore durante la chiusura del file %1","!","",w_FILEORIG)
    endif
    on error &L_OldError 
    if l_err=.t.
      ah_errormsg("Errore generico %1%2","!","",Message(),Message(1))
    endif
    FileAscii = this.w_FILEDEST
    * --- Cambio il nome al file Ascii di importazione
    =stapdf(Alltrim(this.w_FILEDEST))
  endproc


  proc Init(oParentObject,pLUNGREC,pPATH,pFILEORIG)
    this.pLUNGREC=pLUNGREC
    this.pPATH=pPATH
    this.pFILEORIG=pFILEORIG
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pLUNGREC,pPATH,pFILEORIG"
endproc
