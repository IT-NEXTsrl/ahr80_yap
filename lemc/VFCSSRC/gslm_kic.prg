* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_kic                                                        *
*              Import codici studio                                            *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_44]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-21                                                      *
* Last revis.: 2015-04-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gslm_kic
* Esco se il modulo � in semplificata
IF NOT LMAVANZATO()
  do cplu_erm with "La funzione � attiva con il Modulo in modalit� Avanzata"
  return
ENDIF


* --- Fine Area Manuale
return(createobject("tgslm_kic",oParentObject))

* --- Class definition
define class tgslm_kic as StdForm
  Top    = 104
  Left   = 147

  * --- Standard Properties
  Width  = 460
  Height = 238
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-04-30"
  HelpContextID=266976361
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gslm_kic"
  cComment = "Import codici studio"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CODNOR = space(1)
  w_CODIVA = space(1)
  w_FILENAME = space(255)
  o_FILENAME = space(255)
  w_CODINT = space(1)
  w_FILENAME1 = space(255)
  w_BTNREP = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslm_kicPag1","gslm_kic",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODNOR_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_BTNREP = this.oPgFrm.Pages(1).oPag.BTNREP
    DoDefault()
    proc Destroy()
      this.w_BTNREP = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODNOR=space(1)
      .w_CODIVA=space(1)
      .w_FILENAME=space(255)
      .w_CODINT=space(1)
      .w_FILENAME1=space(255)
        .w_CODNOR = ' '
        .w_CODIVA = ' '
          .DoRTCalc(3,3,.f.)
        .w_CODINT = ' '
        .w_FILENAME1 = .w_FILENAME
      .oPgFrm.Page1.oPag.BTNREP.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate(ah_MsgFormat("Questa procedura permette di importare le norme, i codici IVA e i codici IVA%0Intracomunitari del programma per commercialisti Zucchetti."))
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate(ah_MsgFormat("E' necessario selezionare il tipo di dati che si desidera importare e premere il%0bottone di ok. Premere annulla per interrompere la fase di import."))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_FILENAME<>.w_FILENAME
            .w_FILENAME1 = .w_FILENAME
        endif
        .oPgFrm.Page1.oPag.BTNREP.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(ah_MsgFormat("Questa procedura permette di importare le norme, i codici IVA e i codici IVA%0Intracomunitari del programma per commercialisti Zucchetti."))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(ah_MsgFormat("E' necessario selezionare il tipo di dati che si desidera importare e premere il%0bottone di ok. Premere annulla per interrompere la fase di import."))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.BTNREP.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate(ah_MsgFormat("Questa procedura permette di importare le norme, i codici IVA e i codici IVA%0Intracomunitari del programma per commercialisti Zucchetti."))
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate(ah_MsgFormat("E' necessario selezionare il tipo di dati che si desidera importare e premere il%0bottone di ok. Premere annulla per interrompere la fase di import."))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.visible=!this.oPgFrm.Page1.oPag.oBtn_1_7.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_12.visible=!this.oPgFrm.Page1.oPag.oBtn_1_12.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.BTNREP.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODNOR_1_1.RadioValue()==this.w_CODNOR)
      this.oPgFrm.Page1.oPag.oCODNOR_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODIVA_1_2.RadioValue()==this.w_CODIVA)
      this.oPgFrm.Page1.oPag.oCODIVA_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODINT_1_4.RadioValue()==this.w_CODINT)
      this.oPgFrm.Page1.oPag.oCODINT_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFILENAME1_1_5.value==this.w_FILENAME1)
      this.oPgFrm.Page1.oPag.oFILENAME1_1_5.value=this.w_FILENAME1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FILENAME = this.w_FILENAME
    return

enddefine

* --- Define pages as container
define class tgslm_kicPag1 as StdContainer
  Width  = 456
  height = 238
  stdWidth  = 456
  stdheight = 238
  resizeXpos=241
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODNOR_1_1 as StdCheck with uid="OOITYNLRQL",rtseq=1,rtrep=.f.,left=9, top=85, caption="Codici norme",;
    HelpContextID = 145172698,;
    cFormVar="w_CODNOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCODNOR_1_1.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCODNOR_1_1.GetRadio()
    this.Parent.oContained.w_CODNOR = this.RadioValue()
    return .t.
  endfunc

  func oCODNOR_1_1.SetRadio()
    this.Parent.oContained.w_CODNOR=trim(this.Parent.oContained.w_CODNOR)
    this.value = ;
      iif(this.Parent.oContained.w_CODNOR=='S',1,;
      0)
  endfunc

  add object oCODIVA_1_2 as StdCheck with uid="DZBOBMZZAU",rtseq=2,rtrep=.f.,left=9, top=108, caption="Codici IVA",;
    HelpContextID = 154937562,;
    cFormVar="w_CODIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCODIVA_1_2.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCODIVA_1_2.GetRadio()
    this.Parent.oContained.w_CODIVA = this.RadioValue()
    return .t.
  endfunc

  func oCODIVA_1_2.SetRadio()
    this.Parent.oContained.w_CODIVA=trim(this.Parent.oContained.w_CODIVA)
    this.value = ;
      iif(this.Parent.oContained.w_CODIVA=='S',1,;
      0)
  endfunc

  add object oCODINT_1_4 as StdCheck with uid="BNWDDWVQNH",rtseq=4,rtrep=.f.,left=9, top=131, caption="Codici INTRA",;
    HelpContextID = 112994522,;
    cFormVar="w_CODINT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCODINT_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCODINT_1_4.GetRadio()
    this.Parent.oContained.w_CODINT = this.RadioValue()
    return .t.
  endfunc

  func oCODINT_1_4.SetRadio()
    this.Parent.oContained.w_CODINT=trim(this.Parent.oContained.w_CODINT)
    this.value = ;
      iif(this.Parent.oContained.w_CODINT=='S',1,;
      0)
  endfunc

  add object oFILENAME1_1_5 as StdField with uid="LEMEBGUMIJ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_FILENAME1", cQueryName = "FILENAME1",;
    bObbl = .f. , nPag = 1, value=space(255), bMultilanguage =  .f.,;
    ToolTipText = "File di import",;
    HelpContextID = 163556181,;
   bGlobalFont=.t.,;
    Height=21, Width=378, Left=46, Top=156, InputMask=replicate('X',255)


  add object BTNREP as cp_askfile with uid="ISHGMEOMJK",left=429, top=158, width=19,height=18,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_FILENAME",;
    nPag=1;
    , ToolTipText = "Premere per selezionare il file di import";
    , HelpContextID = 266775338


  add object oBtn_1_7 as StdButton with uid="WXVPJXOUYP",left=349, top=186, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 266947610;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        do GSLMABIC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CODNOR='S' OR .w_CODIVA='S' OR .w_CODINT='S')
      endwith
    endif
  endfunc

  func oBtn_1_7.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (G_TRAEXP<>'G')
     endwith
    endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="YIWHNZPYKT",left=400, top=186, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 259658938;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_10 as cp_calclbl with uid="SMOKKPJMZK",left=5, top=4, width=445,height=37,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 179456742


  add object oObj_1_11 as cp_calclbl with uid="GTVMRWJWIQ",left=5, top=44, width=445,height=39,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 179456742


  add object oBtn_1_12 as StdButton with uid="TMNRTYRAAU",left=349, top=186, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 266947610;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        do GSLM_BIC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CODNOR='S' OR .w_CODIVA='S' OR .w_CODINT='S')
      endwith
    endif
  endfunc

  func oBtn_1_12.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_traexp='G')
     endwith
    endif
  endfunc

  add object oStr_1_9 as StdString with uid="HCENBKIIGO",Visible=.t., Left=7, Top=156,;
    Alignment=1, Width=38, Height=18,;
    Caption="Path:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslm_kic','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
