* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_kfr                                                        *
*              Raccordo codici cogen contb                                     *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_36]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-17                                                      *
* Last revis.: 2008-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gslm_kfr
* Esco se il modulo non � attivo
IF LMDISACTIVATE()
  do cplu_erm with "La funzione � attiva con il Modulo in modalit� Semplificata o Avanzata"
  return
ENDIF
* --- Fine Area Manuale
return(createobject("tgslm_kfr",oParentObject))

* --- Class definition
define class tgslm_kfr as StdForm
  Top    = 29
  Left   = 36

  * --- Standard Properties
  Width  = 498
  Height = 359+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-23"
  HelpContextID=48872553
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  STU_PARA_IDX = 0
  cPrg = "gslm_kfr"
  cComment = "Raccordo codici cogen contb"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_TIPO = space(1)
  o_TIPO = space(1)
  w_CHKAZI = space(1)
  w_PATH = space(254)
  w_ERRLOG = space(1)
  w_AZISTU = space(6)
  w_Msg = space(0)
  w_CODPDC = space(4)
  w_CHKPDC = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslm_kfrPag1","gslm_kfr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezione file di testo")
      .Pages(2).addobject("oPag","tgslm_kfrPag2","gslm_kfr",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Log elaborazione")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPO_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='STU_PARA'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_TIPO=space(1)
      .w_CHKAZI=space(1)
      .w_PATH=space(254)
      .w_ERRLOG=space(1)
      .w_AZISTU=space(6)
      .w_Msg=space(0)
      .w_CODPDC=space(4)
      .w_CHKPDC=space(1)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZI))
          .link_1_1('Full')
        endif
        .w_TIPO = 'C'
        .w_CHKAZI = Iif(  .w_TIPO='C' , 'S' ,' ' )
      .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
          .DoRTCalc(4,4,.f.)
        .w_ERRLOG = 'S'
          .DoRTCalc(6,8,.f.)
        .w_CHKPDC = Iif(  .w_TIPO='G' , 'S' ,' ' )
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate(ah_MsgFormat("E' possibile generare dagli applicativi apri un file di raccordo per convertire i codici%0sottoconto studio di clienti fornitori e conti generici al nuovo piano dei conti contb."))
      .oPgFrm.Page1.oPag.oObj_1_18.Calculate(ah_MsgFormat("Il raccordo per i clienti/fornitori � contenuto nel file cgracc.d generato dall'apposita%0funzione presente dalla versione 02.03.00 di apri."))
      .oPgFrm.Page1.oPag.oObj_1_19.Calculate(ah_MsgFormat("Per i conti generici, sempre in apri, occorre esportare in formato txt l'apposita stampa%0nella quale sono presenti i soli conti generici movimentati."))
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate(ah_MsgFormat("Per i restanti conti/clienti/fornitori � prevista una stampa, al termine dell'elaborazione,%0contenente l'elenco dei codici per i quali il campo sottoconto studio sar� sbiancato."))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
        if .o_TIPO<>.w_TIPO
            .w_CHKAZI = Iif(  .w_TIPO='C' , 'S' ,' ' )
        endif
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .DoRTCalc(4,8,.t.)
        if .o_TIPO<>.w_TIPO
            .w_CHKPDC = Iif(  .w_TIPO='G' , 'S' ,' ' )
        endif
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(ah_MsgFormat("E' possibile generare dagli applicativi apri un file di raccordo per convertire i codici%0sottoconto studio di clienti fornitori e conti generici al nuovo piano dei conti contb."))
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate(ah_MsgFormat("Il raccordo per i clienti/fornitori � contenuto nel file cgracc.d generato dall'apposita%0funzione presente dalla versione 02.03.00 di apri."))
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(ah_MsgFormat("Per i conti generici, sempre in apri, occorre esportare in formato txt l'apposita stampa%0nella quale sono presenti i soli conti generici movimentati."))
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(ah_MsgFormat("Per i restanti conti/clienti/fornitori � prevista una stampa, al termine dell'elaborazione,%0contenente l'elenco dei codici per i quali il campo sottoconto studio sar� sbiancato."))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate(ah_MsgFormat("E' possibile generare dagli applicativi apri un file di raccordo per convertire i codici%0sottoconto studio di clienti fornitori e conti generici al nuovo piano dei conti contb."))
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate(ah_MsgFormat("Il raccordo per i clienti/fornitori � contenuto nel file cgracc.d generato dall'apposita%0funzione presente dalla versione 02.03.00 di apri."))
        .oPgFrm.Page1.oPag.oObj_1_19.Calculate(ah_MsgFormat("Per i conti generici, sempre in apri, occorre esportare in formato txt l'apposita stampa%0nella quale sono presenti i soli conti generici movimentati."))
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(ah_MsgFormat("Per i restanti conti/clienti/fornitori � prevista una stampa, al termine dell'elaborazione,%0contenente l'elenco dei codici per i quali il campo sottoconto studio sar� sbiancato."))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_2.enabled = this.oPgFrm.Page2.oPag.oBtn_2_2.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCHKAZI_1_3.visible=!this.oPgFrm.Page1.oPag.oCHKAZI_1_3.mHide()
    this.oPgFrm.Page1.oPag.oAZISTU_1_11.visible=!this.oPgFrm.Page1.oPag.oAZISTU_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oCODPDC_1_13.visible=!this.oPgFrm.Page1.oPag.oCODPDC_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_14.visible=!this.oPgFrm.Page1.oPag.oStr_1_14.mHide()
    this.oPgFrm.Page1.oPag.oCHKPDC_1_15.visible=!this.oPgFrm.Page1.oPag.oCHKPDC_1_15.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_19.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_PARA_IDX,3]
    i_lTable = "STU_PARA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_PARA_IDX,2], .t., this.STU_PARA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_PARA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODAZI,LMAZISTU,LMCODPDC";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODAZI',this.w_CODAZI)
            select LMCODAZI,LMAZISTU,LMCODPDC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.LMCODAZI,space(5))
      this.w_AZISTU = NVL(_Link_.LMAZISTU,space(6))
      this.w_CODPDC = NVL(_Link_.LMCODPDC,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_AZISTU = space(6)
      this.w_CODPDC = space(4)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_PARA_IDX,2])+'\'+cp_ToStr(_Link_.LMCODAZI,1)
      cp_ShowWarn(i_cKey,this.STU_PARA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPO_1_2.RadioValue()==this.w_TIPO)
      this.oPgFrm.Page1.oPag.oTIPO_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHKAZI_1_3.RadioValue()==this.w_CHKAZI)
      this.oPgFrm.Page1.oPag.oCHKAZI_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPATH_1_8.value==this.w_PATH)
      this.oPgFrm.Page1.oPag.oPATH_1_8.value=this.w_PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oERRLOG_1_10.RadioValue()==this.w_ERRLOG)
      this.oPgFrm.Page1.oPag.oERRLOG_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAZISTU_1_11.value==this.w_AZISTU)
      this.oPgFrm.Page1.oPag.oAZISTU_1_11.value=this.w_AZISTU
    endif
    if not(this.oPgFrm.Page2.oPag.oMsg_2_1.value==this.w_Msg)
      this.oPgFrm.Page2.oPag.oMsg_2_1.value=this.w_Msg
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPDC_1_13.value==this.w_CODPDC)
      this.oPgFrm.Page1.oPag.oCODPDC_1_13.value=this.w_CODPDC
    endif
    if not(this.oPgFrm.Page1.oPag.oCHKPDC_1_15.RadioValue()==this.w_CHKPDC)
      this.oPgFrm.Page1.oPag.oCHKPDC_1_15.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPO = this.w_TIPO
    return

enddefine

* --- Define pages as container
define class tgslm_kfrPag1 as StdContainer
  Width  = 494
  height = 359
  stdWidth  = 494
  stdheight = 359
  resizeXpos=247
  resizeYpos=167
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPO_1_2 as StdCombo with uid="TUBTYAHEBE",rtseq=2,rtrep=.f.,left=73,top=200,width=121,height=21;
    , ToolTipText = "Raccordo per conti generici o per clienti/fornitori";
    , HelpContextID = 43347402;
    , cFormVar="w_TIPO",RowSource=""+"Clienti/fornitori,"+"Generici", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPO_1_2.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'G',;
    space(1))))
  endfunc
  func oTIPO_1_2.GetRadio()
    this.Parent.oContained.w_TIPO = this.RadioValue()
    return .t.
  endfunc

  func oTIPO_1_2.SetRadio()
    this.Parent.oContained.w_TIPO=trim(this.Parent.oContained.w_TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_TIPO=='C',1,;
      iif(this.Parent.oContained.w_TIPO=='G',2,;
      0))
  endfunc

  add object oCHKAZI_1_3 as StdCheck with uid="OPNJAMSOHX",rtseq=3,rtrep=.f.,left=208, top=199, caption="Codice azienda",;
    ToolTipText = "Se attivo verifica il codice azienda sul file txt con quello indicato a sinistra (letto dalla maschera trasf. studio)",;
    HelpContextID = 67354586,;
    cFormVar="w_CHKAZI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHKAZI_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCHKAZI_1_3.GetRadio()
    this.Parent.oContained.w_CHKAZI = this.RadioValue()
    return .t.
  endfunc

  func oCHKAZI_1_3.SetRadio()
    this.Parent.oContained.w_CHKAZI=trim(this.Parent.oContained.w_CHKAZI)
    this.value = ;
      iif(this.Parent.oContained.w_CHKAZI=='S',1,;
      0)
  endfunc

  func oCHKAZI_1_3.mHide()
    with this.Parent.oContained
      return (.w_TIPO<>'C')
    endwith
  endfunc


  add object oBtn_1_5 as StdButton with uid="IFISWSZGLE",left=383, top=312, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per avviare l'aggiornamento sottoconti studio";
    , HelpContextID = 48843802;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_5.Click()
      with this.Parent.oContained
        do GSLM_BFR with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_5.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty( .w_PATH ))
      endwith
    endif
  endfunc


  add object oBtn_1_6 as StdButton with uid="RMAHAARJLI",left=440, top=312, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 41555130;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPATH_1_8 as StdField with uid="UTRKGRKZWY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Inserisci il path+nome file",;
    HelpContextID = 43791882,;
   bGlobalFont=.t.,;
    Height=21, Width=397, Left=73, Top=275, InputMask=replicate('X',254)


  add object oObj_1_9 as cp_askfile with uid="OGPUROVHMF",left=472, top=275, width=16,height=18,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_PATH",;
    nPag=1;
    , ToolTipText = "Premere per selezionare un file";
    , HelpContextID = 48671530

  add object oERRLOG_1_10 as StdCheck with uid="DPFOHSETSY",rtseq=5,rtrep=.f.,left=9, top=330, caption="Log esiti negativi",;
    ToolTipText = "Se non attivo traccia nel log tutti i blocchi compresi quelli con esito positivo",;
    HelpContextID = 111691194,;
    cFormVar="w_ERRLOG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oERRLOG_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oERRLOG_1_10.GetRadio()
    this.Parent.oContained.w_ERRLOG = this.RadioValue()
    return .t.
  endfunc

  func oERRLOG_1_10.SetRadio()
    this.Parent.oContained.w_ERRLOG=trim(this.Parent.oContained.w_ERRLOG)
    this.value = ;
      iif(this.Parent.oContained.w_ERRLOG=='S',1,;
      0)
  endfunc

  add object oAZISTU_1_11 as StdField with uid="USPAJXFBKZ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_AZISTU", cQueryName = "AZISTU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Codice azienda studio",;
    HelpContextID = 139578874,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=433, Top=199, InputMask=replicate('X',6)

  func oAZISTU_1_11.mHide()
    with this.Parent.oContained
      return (.w_TIPO<>'C')
    endwith
  endfunc

  add object oCODPDC_1_13 as StdField with uid="SRTRWMZGZH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODPDC", cQueryName = "CODPDC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Rappresenta univocamente il codice del piano dei conti di apri usato per l'azienda",;
    HelpContextID = 190130394,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=433, Top=228, InputMask=replicate('X',4)

  func oCODPDC_1_13.mHide()
    with this.Parent.oContained
      return (.w_TIPO<>'G')
    endwith
  endfunc

  add object oCHKPDC_1_15 as StdCheck with uid="LDFPMVHLPL",rtseq=9,rtrep=.f.,left=208, top=227, caption="Codice piano",;
    ToolTipText = "Se attivo verifica il codice piano dei conti sul file txt con quello indicato a sinistra (letto dalla maschera trasf. studio)",;
    HelpContextID = 190103514,;
    cFormVar="w_CHKPDC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCHKPDC_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCHKPDC_1_15.GetRadio()
    this.Parent.oContained.w_CHKPDC = this.RadioValue()
    return .t.
  endfunc

  func oCHKPDC_1_15.SetRadio()
    this.Parent.oContained.w_CHKPDC=trim(this.Parent.oContained.w_CHKPDC)
    this.value = ;
      iif(this.Parent.oContained.w_CHKPDC=='S',1,;
      0)
  endfunc

  func oCHKPDC_1_15.mHide()
    with this.Parent.oContained
      return (.w_TIPO<>'G')
    endwith
  endfunc


  add object oObj_1_17 as cp_calclbl with uid="ULPXPSNXQR",left=5, top=8, width=486,height=40,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 129125094


  add object oObj_1_18 as cp_calclbl with uid="PYILMHCLWW",left=5, top=52, width=486,height=43,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 129125094


  add object oObj_1_19 as cp_calclbl with uid="FQXFILAYOH",left=5, top=98, width=486,height=36,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 129125094


  add object oObj_1_20 as cp_calclbl with uid="NNJGKKDAZS",left=5, top=137, width=486,height=44,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 129125094

  add object oStr_1_4 as StdString with uid="DJTCHTQDVL",Visible=.t., Left=9, Top=203,;
    Alignment=1, Width=61, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="GAAWAZFVYP",Visible=.t., Left=1, Top=275,;
    Alignment=1, Width=71, Height=15,;
    Caption="Path+file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="XZTNQVWELB",Visible=.t., Left=331, Top=203,;
    Alignment=1, Width=100, Height=18,;
    Caption="Azienda studio:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (.w_TIPO<>'C')
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="NMWSHBRRJP",Visible=.t., Left=330, Top=232,;
    Alignment=1, Width=101, Height=18,;
    Caption="Cod.pdc:"  ;
  , bGlobalFont=.t.

  func oStr_1_14.mHide()
    with this.Parent.oContained
      return (.w_TIPO<>'G')
    endwith
  endfunc

  add object oBox_1_16 as StdBox with uid="WWEWRYYPDV",left=1, top=185, width=491,height=2
enddefine
define class tgslm_kfrPag2 as StdContainer
  Width  = 494
  height = 359
  stdWidth  = 494
  stdheight = 359
  resizeXpos=245
  resizeYpos=185
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMsg_2_1 as StdMemo with uid="QYBPPIIQMH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 48419898,;
    FontName = "Courier New", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=299, Width=486, Left=4, Top=7, tabstop = .f., readonly = .t.


  add object oBtn_2_2 as StdButton with uid="QKNERQNDZN",left=442, top=312, width=48,height=45,;
    CpPicture="BMP\SAVE.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per salvare il log di elaborazione";
    , HelpContextID = 207394266;
    , Caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_2.Click()
      with this.Parent.oContained
        STRTOFILE(.w_MSG, PUTFILE('',cp_MsgFormat("Log_Elaboraz_Raccordo"),"TXT"))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_2.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty( .w_Msg ))
      endwith
    endif
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslm_kfr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
