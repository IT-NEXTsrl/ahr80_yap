* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_bmp                                                        *
*              Inizializza set flag esportazione                               *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-23                                                      *
* Last revis.: 2000-09-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm_bmp",oParentObject)
return(i_retval)

define class tgslm_bmp as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- BATCH CHE INIZIALIZZA LE SELEZIONI PER SET FLAG ESPORTAZIONE (LANCIATO DA GSLM_KMP)
    this.oParentObject.w_DATINI = CTOD("  -  -  ")
    this.oParentObject.w_DATFIN = CTOD("  -  -  ")
    vq_exec("..\LEMC\EXE\QUERY\GSLM_EVI.VQR",this,"__TMP__")
    * --- Recupero la data prima registrazione da trasferire
    this.oParentObject.w_DATINI = CP_TODATE(__TMP__.DATINI)
    * --- Recupero la data dell'ultima registrazione da trasferire
    this.oParentObject.w_DATFIN = CP_TODATE(__TMP__.DATFIN)
    * --- Ritorna alla maschera di export dopo aver valorizzato w_DATINI, w_DATFIN
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
