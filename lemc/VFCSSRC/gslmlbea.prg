* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslmlbea                                                        *
*              Export studio prog.principale                                   *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][116][VRS_2241]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-23                                                      *
* Last revis.: 2012-11-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEXEC
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslmlbea",oParentObject,m.pEXEC)
return(i_retval)

define class tgslmlbea as StdBatch
  * --- Local variables
  pEXEC = space(1)
  w_INCASSI = space(1)
  w_PATH = space(50)
  w_STRINGA = space(120)
  w_CONDIZ = .f.
  w_CONFERMA = .f.
  w_FILETOSAVE = space(40)
  w_LOGTOSAVE = space(40)
  w_APPOGGIO = space(40)
  w_FILELOG = space(50)
  w_TIPOCLFO = space(1)
  w_TIPOCLFO1 = space(1)
  hFILE = 0
  hLOG = 0
  w_GOT = 0
  w_TRT = 0
  w_NUMCLI = 0
  w_NUMFOR = 0
  w_NUMSOT = 0
  w_FATTACQU = 0
  w_FATTVEND = 0
  w_FATTCORR = 0
  w_FATTVENT = 0
  w_CORRNORM = 0
  w_CORRVENT = 0
  w_MOCONTAB = 0
  w_ERRORE = .f.
  w_WARNING = .f.
  w_PNTNOTAOK = .f.
  w_ESCI = space(10)
  w_CODAZI = space(5)
  w_DITTA = space(6)
  w_LMPROFIL = space(2)
  w_ASSOCI = space(2)
  w_CAU1 = space(5)
  w_CAU2 = space(5)
  w_CAU3 = space(5)
  w_CAU4 = space(5)
  w_CAU5 = space(5)
  w_CAUGIR = space(5)
  w_CASFIT = space(5)
  w_FLANUN = space(1)
  w_OK = .f.
  w_COND1 = .f.
  w_COND2 = .f.
  w_ISITALIAN = .f.
  w_DATANAS = space(8)
  w_CODCAU = space(3)
  w_VALEUR = space(3)
  w_FILTRO = 0
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_FILLER = space(200)
  w_ALMENOUNCLI = .f.
  w_ALMENOUNFOR = .f.
  w_ALMENOUNSOT = .f.
  w_ALMENOUNSOT = .f.
  w_RS = space(25)
  w_RS1 = space(25)
  w_RS2 = space(25)
  w_INDICE = space(10)
  w_IND = space(32)
  w_NUM = space(5)
  w_NACODISO = space(3)
  w_CODCATAS = space(4)
  w_NAZIONE = space(3)
  w_INDIRIZZO = space(100)
  w_ALMENOUNFATTACQU = .f.
  w_ALMENOUNFATTVEND = .f.
  w_ALMENOUNFATTCORR = .f.
  w_ALMENOUNFATTVENT = .f.
  w_ALMENOUNCORRNORM = .f.
  w_ALMENOUNCORRVENT = .f.
  w_ALMENOUNMOCONTAB = .f.
  * --- WorkFile variables
  CAU_CONT_idx=0
  CONTI_idx=0
  PNT_MAST_idx=0
  STU_PARA_idx=0
  STU_PIAC_idx=0
  STU_PNTT_idx=0
  AZIENDA_idx=0
  NAZIONI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- BATCH CHE EFFETTUA L'EXPORT VERSO LO STUDIO LANCIATO DA GSLM_KEX
    * --- C:controllo
    *     E: Export
    this.w_INCASSI = this.oParentObject.w_PNTNOIVA
    this.w_PATH = this.oParentObject.w_PERCORSO
    * --- Handle dei file
    this.hFILE = -1
    this.hLOG = -1
    * --- Variabili necessarie alla compilazione del file
    * --- Gruppi omogenei trasferiti
    * --- Totale record trasferiti
    * --- Numero di clienti trasferiti
    * --- Numero di fornitori trasferiti
    * --- Numero di conti trasferiti
    * --- Numero di fatture di acquisto trasferite
    * --- Numero di fatture di vendita trasferite
    * --- Numero di fatture corrispetive trasferite
    * --- Numero di fatture ventilate trasferite
    * --- Numero di corrispetivi normali trasferiti
    * --- Numero di corrispetivi ventilati trasferiti
    * --- Numero di movimenti contabili trasferiti
    * --- Variabili usate per controllare errori e warning durante l'export
    * --- Variabile usata per indicare se la primanota era corretta
    this.w_ESCI = .F.
    * --- Causali che non devono essere esportate
    * --- Causale per il giroconto
    * --- Sottoconto Studio Cassa Fittizia
    * --- Flag Anagrafica Unica
    this.w_CODCAU = This.oparentobject.w_CODCAU
    * --- Recupero i parametri per il trasferimento
    if this.pEXEC="E"
      this.w_CODAZI = i_CODAZI
      * --- Read from STU_PARA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.STU_PARA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STU_PARA_idx,2],.t.,this.STU_PARA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "LMAZISTU,LMASSOCI,LMESCAU1,LMESCAU2,LMESCAU3,LMESCAU4,LMESCAU5,LMCAUGIR,LMCASFIT,LMFLANUN"+;
          " from "+i_cTable+" STU_PARA where ";
              +"LMCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          LMAZISTU,LMASSOCI,LMESCAU1,LMESCAU2,LMESCAU3,LMESCAU4,LMESCAU5,LMCAUGIR,LMCASFIT,LMFLANUN;
          from (i_cTable) where;
              LMCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DITTA = NVL(cp_ToDate(_read_.LMAZISTU),cp_NullValue(_read_.LMAZISTU))
        this.w_ASSOCI = NVL(cp_ToDate(_read_.LMASSOCI),cp_NullValue(_read_.LMASSOCI))
        this.w_CAU1 = NVL(cp_ToDate(_read_.LMESCAU1),cp_NullValue(_read_.LMESCAU1))
        this.w_CAU2 = NVL(cp_ToDate(_read_.LMESCAU2),cp_NullValue(_read_.LMESCAU2))
        this.w_CAU3 = NVL(cp_ToDate(_read_.LMESCAU3),cp_NullValue(_read_.LMESCAU3))
        this.w_CAU4 = NVL(cp_ToDate(_read_.LMESCAU4),cp_NullValue(_read_.LMESCAU4))
        this.w_CAU5 = NVL(cp_ToDate(_read_.LMESCAU5),cp_NullValue(_read_.LMESCAU5))
        this.w_CAUGIR = NVL(cp_ToDate(_read_.LMCAUGIR),cp_NullValue(_read_.LMCAUGIR))
        this.w_CASFIT = NVL(cp_ToDate(_read_.LMCASFIT),cp_NullValue(_read_.LMCASFIT))
        this.w_FLANUN = NVL(cp_ToDate(_read_.LMFLANUN),cp_NullValue(_read_.LMFLANUN))
        use
      else
        * --- Error: sql sentence error.
        i_Error = 'Fallita la lettura dei parametri per il trasferimento'
        return
      endif
      select (i_nOldArea)
      * --- Se sono in export avanzato, sbianco la variabile contentenente il flag 'anagrafica unica'.
      if g_TRAEXP $ ("C-B")
        this.w_FLANUN = space(1)
      endif
      * --- Codice Valuta Euro
      * --- Recupero il Codice Valuta Euro
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZVALEUR"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZVALEUR;
          from (i_cTable) where;
              AZCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_VALEUR = NVL(cp_ToDate(_read_.AZVALEUR),cp_NullValue(_read_.AZVALEUR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Aggiorno il numero di spedizione
      this.w_LMPROFIL = IIF(RIGHT("00"+ALLTRIM(STR(this.oParentObject.w_PROFIL)),2)="00","01",RIGHT("00"+ALLTRIM(STR(this.oParentObject.w_PROFIL)),2))
      * --- Inizializzazione variabili necessarie alla compilazione del file
      this.w_GOT = 0
      this.w_TRT = 0
      this.w_NUMCLI = 0
      this.w_NUMFOR = 0
      this.w_NUMSOT = 0
      this.w_FATTACQU = 0
      this.w_FATTVEND = 0
      this.w_FATTCORR = 0
      this.w_FATTVENT = 0
      this.w_CORRNORM = 0
      this.w_CORRVENT = 0
      this.w_MOCONTAB = 0
      this.w_PNTNOTAOK = .T.
      * --- Inizializzazione variabile di conferma
      this.w_CONFERMA = .F.
      * --- Inizializzazione di ERASETMP e w_FILETOSAVE
      this.w_FILETOSAVE = ""
      this.w_LOGTOSAVE = ""
      this.w_ERRORE = .F.
      this.w_WARNING = .F.
      * --- Preparazione nome temporaneo di appoggio
      this.w_APPOGGIO = this.oParentObject.w_PERCORSO+"\"+"IV000000.D"
      This.oparentobject.w_APPOGGIO=this.w_APPOGGIO
      * --- Creazione e apertura file di appoggio
      this.hFILE = FCREATE(this.w_APPOGGIO,0)
      * --- Preparazione nome temporaneo di log
    endif
    this.w_FILELOG = this.oParentObject.w_PERCORSO+"\"+"LOG"+RIGHT("000000"+ALLTRIM(this.w_DITTA),6)+".TMP"
    * --- Creazione e apertura file di log
    this.hLOG = FCREATE(this.w_FILELOG,0)
    * --- Controllo creazione file di appoggio
    if this.hFile=-1 and this.pEXEC="E"
      ah_Msg("Impossibile creare il file di appoggio",.t.,.f.,.f.)
      return
    endif
    FCLOSE(this.hFile)
    * --- Controllo creazione file di log
    if this.hLOG=-1
      ah_Msg("Impossibile creare il file di log",.t.,.f.,.f.)
      return
    endif
    FCLOSE(this.hLOG)
    * --- Riapertura dei file
    if this.pEXEC="E"
      this.hFILE = FOPEN(this.w_APPOGGIO,1)
      if this.hFile=-1
        ah_Msg("Impossibile aprire il file di appoggio",.t.,.f.,.f.)
        return
      endif
    endif
    this.hLOG = FOPEN(this.w_FILELOG,1)
    if this.hLOG=-1
      ah_Msg("Impossibile aprire il file di log",.t.,.f.,.f.)
      return
    endif
    * --- Testata file di LOG
    FWRITE(this.hLOG,REPL("-",80),80)
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    * --- recupero ragione sociale azienda
    this.w_STRINGA = ah_MsgFormat("    Elenco Fatture\Incassi non esportati%0")
    FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    FWRITE(this.hLOG,REPL("-",80),80)
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    if this.pEXEC="E"
      this.w_STRINGA = ah_MsgFormat("-- DATA EXPORT%1:%2",space(9), dtoc(date()))
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      * --- Preparazione stringa elementi da esportare
      this.w_oMess=createobject("AH_Message")
      this.w_oPart = this.w_oMess.AddMsgPart("-- EXPORT DI%1:")
      this.w_oPart.AddParam(space(11))     
      if this.oParentObject.w_CLI="S"
        this.w_oPart = this.w_oMess.AddMsgPart("CLIENTI%1")
        this.w_oPart.AddParam(space(1))     
      endif
      if this.oParentObject.w_FOR="S"
        this.w_oPart = this.w_oMess.AddMsgPart("FORNITORI%1")
        this.w_oPart.AddParam(space(1))     
      endif
      if this.oParentObject.w_SOT="S"
        this.w_oPart = this.w_oMess.AddMsgPart("CONTI%1")
        this.w_oPart.AddParam(space(1))     
      endif
      if this.oParentObject.w_PNTIVA="S"
        this.w_oPart = this.w_oMess.AddMsgPart("PRIMANOTA IVA%1")
        this.w_oPart.AddParam(space(1))     
      endif
      if this.oParentObject.w_PNTNOIVA="S"
        this.w_oPart = this.w_oMess.AddMsgPart("PRIMANOTA NO IVA%1")
        this.w_oPart.AddParam(space(1))     
      endif
      this.w_STRINGA = this.w_oMess.ComposeMessage()
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      do case
        case this.oParentObject.w_CLI="S" and this.oParentObject.w_FOR="S"
          if this.oParentObject.w_TIPCLIFOR="T"
            this.w_STRINGA = ah_MsgFormat("-- CLIENTI / FORNITORI: TUTTI")
          else
            this.w_STRINGA = ah_MsgFormat("-- CLIENTI / FORNITORI: SOLO MOVIMENTATI")
          endif
          FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        case this.oParentObject.w_CLI="S" and this.oParentObject.w_FOR="N"
          if this.oParentObject.w_TIPCLIFOR="T"
            this.w_STRINGA = ah_MsgFormat("-- CLIENTI: TUTTI")
          else
            this.w_STRINGA = ah_MsgFormat("-- CLIENTI: SOLO MOVIMENTATI")
          endif
          FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        case this.oParentObject.w_CLI="N" and this.oParentObject.w_FOR="S"
          if this.oParentObject.w_TIPCLIFOR="T"
            this.w_STRINGA = ah_MsgFormat("-- FORNITORI: TUTTI")
          else
            this.w_STRINGA = ah_MsgFormat("-- FORNITORI: SOLO MOVIMENTATI")
          endif
          FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      endcase
      if this.oParentObject.w_PNTIVA="S" OR this.oParentObject.w_PNTNOIVA="S"
        this.w_STRINGA = ah_MsgFormat("-- EXPORT PRIMANOTA: DA DATA %1 A DATA %2",dtoc(this.oParentObject.w_DATINI),dtoc(this.oParentObject.w_DATFIN))
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = ah_MsgFormat("%1DA NUMERO %2 A NUMERO %3","                         ",STR(this.oParentObject.w_NUMINI,6,0),STR(this.oParentObject.w_NUMFIN,6,0))
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      endif
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      FWRITE(this.hLOG,REPL("-",80),80)
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      * --- Fine testata file di LOG
    endif
    if this.pEXEC="E"
      * --- Eseguo la procedura di export
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Chiudo il file di appoggio
      FCLOSE(this.hFile)
    else
      vq_exec("..\LEMC\EXE\QUERY\GSLM_FAV.VQR",this,"FATTVEND")
      do gslmcbfv with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT FATTVEND
      USE
    endif
    * --- Chiudo file di log
    FCLOSE(this.hLOG)
    * --- Esco se non ho trovato nulla da esportare
    if this.pEXEC="E"
      this.w_CONDIZ = (NOT this.w_ALMENOUNCLI) AND (NOT this.w_ALMENOUNFOR) AND (NOT this.w_ALMENOUNSOT) AND (NOT this.w_ALMENOUNFATTACQU)
      this.w_CONDIZ = (NOT this.w_ALMENOUNFATTVEND) AND (NOT this.w_ALMENOUNFATTCORR) AND (NOT this.w_ALMENOUNFATTVENT) AND this.w_CONDIZ
      this.w_CONDIZ = (NOT this.w_ALMENOUNCORRNORM) AND (NOT this.w_ALMENOUNCORRVENT) AND (NOT this.w_ALMENOUNMOCONTAB) AND this.w_CONDIZ
      if this.w_CONDIZ
        ah_ErrorMsg("Per l'intervallo selezionato non sono presenti movimenti",,"")
        return
      endif
      * --- Lancio la maschera di conferma.(Solo se la primanota era OK) Valorizza w_CONFERMA,w_FILETOSAVE e w_LOGTOSAVE
      if this.w_PNTNOTAOK AND NOT this.w_ERRORE
        * --- Lancio la maschera di conferma
        do GSLM_KCE with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        * --- Lancio la maschera di errore
        do GSLM_KER with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      do gslm_bvl with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- PROCEDURA PRINCIPALE DI EXPORT FILE
    * --- Incremento il numero di record inseriti (Il record di Testa)
    this.w_TRT = this.w_TRT+1
    * --- Record di Testa Generale (Tipo Record)
    FWRITE(this.hFile,"TINT",4)
    * --- - 94 07 01 � la data di spedizione (aa/mm/gg). 
    *     Nel frattempo � cambiato anche il secolo per cui se tu dovessi indicare la data di oggi dovresti compilare cos�:
    *     1121001 (1=secolo, 12=anno, 10=mese, 01=giorno)
    FWRITE(this.hFile,"1"+RIGHT(dtos(i_datsys),6),7)
    * --- - V = trasmissione Verso la contabilit� (impostate pure fisso V)
    FWRITE(this.hFile,"V",1)
    * --- - G = mittente. Questo campo � molto importante in quanto determina la provenienza del file e definisce come devono essere trattati i sottoconti clienti e fornitori presenti nel file. Se si indica G o C o I, significa che i sottoconti presenti nel file sono gi� quelli previsti nella contabilit� di arrivo o comunque utilizzabili per l'abbinamento. 
    *     E' preferibile NON INDICARE NULLA  e far determinare in automatico i sottoconti. In questo caso le entit� cli/for dei documenti trasferiti devono essere presenti nel file con un numero progressivo nel campo CodNumSogg (numero soggetto) che pu� non essere il codice della contabilit� che invece verr� determinato in automatico. (Magari su questa cosa possiamo scambiare due parole in pi�, in quanto  � una modalit� simile a quella prevista nel Lemco,  ma non uguale,  e non so se la utilizzate gi�).
    FWRITE(this.hFile," ",1)
    * ---  - C = destinatario (lasciare vuoto,  � un campo facoltativo senza particolari trattamenti )
    FWRITE(this.hFile,Space(15),15)
    FWRITE(this.hFile," ",1)
    FWRITE(this.hFile,"0100 ",4)
    * --- Psswd
    FWRITE(this.hFile,space(30),30)
    * --- Filler
    FWRITE(this.hFile,space(137),137)
    * --- Inserisco Record di tipo ditta
    *     ========================
    FWRITE(this.hFile,"IDIT",4)
    FWRITE(this.hFile,this.w_DITTA,6)
    * --- Inserisco il numero di spedizione
    FWRITE(this.hFile,this.w_LMPROFIL,2)
    FWRITE(this.hFile,space(188),188)
    * --- ======================================
    * --- Controllo se ho almeno un cliente da esportare
    this.w_ALMENOUNCLI = .F.
    if this.oParentObject.w_CLI="S"
      this.w_TIPOCLFO = "C"
      if this.oParentObject.w_TIPCLIFOR="M"
        vq_exec("..\LEMC\EXE\QUERY\GSLM_ACF.VQR",this,"ALMUNCLI")
        * --- Deve esistere almeno un cliente movimentato in primanota (tra le data comprese in datini e datfin) e avente ancodstu non vuoto
        this.w_TIPOCLFO = "F"
        this.w_TIPOCLFO1 = "C"
        * --- Devo prendere anche i clienti collegati ai fornitori intestatari delle fatture europee d'acquisto
        vq_exec("..\LEMC\EXE\QUERY\GSLM2ACF.VQR",this,"UNCLICOLL")
        SELECT * FROM ALMUNCLI UNION SELECT * FROM UNCLICOLL INTO CURSOR ALMUNCLI
      else
        * --- Deve esistere almeno un cliente avente codice studio non vuoto
        vq_exec("..\LEMC\EXE\QUERY\GSLM1ACF.VQR",this,"ALMUNCLI")
      endif
      if RECCOUNT("ALMUNCLI") > 0
        this.w_ALMENOUNCLI = .T.
      endif
    endif
    * --- Aggiorno la variabile di gruppi omogenei trasferiti
    if this.w_ALMENOUNCLI
      this.w_GOT = 0
    endif
    * --- Scrittura su log
    if this.oParentObject.w_CLI="S"
      this.w_STRINGA = ah_MsgFormat("-- INIZIO EXPORT CLIENTI --")
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    endif
    * --- Scrittura di clienti
    if this.w_ALMENOUNCLI
      * --- Testa cli/for
      * --- Incremento numero record inseriti (TESTA CLI/FOR)
      this.w_NUMCLI = 0
      * --- Inizio records di dettaglio
      FWRITE(this.hFile,"ICLF",4)
      this.w_FILLER = SPACE(196)
      FWRITE(this.hFile,this.w_FILLER,196)
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Scrittura su log
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      this.w_STRINGA = ah_MsgFormat("-- FINE EXPORT CLIENTI --")
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      * --- Fine riga Clienti\Fornitori
      *     ======================
      FWRITE(this.hFile,"FCLF",4)
      FWRITE(this.hFile,RIGHT("00000000"+ALLTRIM(STR(this.w_NUMCLI)),8),8)
    else
      if this.oParentObject.w_CLI="S"
        * --- Scrittura su log
        this.w_STRINGA = ah_MsgFormat("%1NESSUN CLIENTE DA ESPORTARE","          ")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = ah_MsgFormat("-- FINE EXPORT CLIENTI --")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      endif
    endif
    if USED("ALMUNCLI")
      SELECT ALMUNCLI
      USE
    endif
    * --- Scrittura su log
    * --- Riga conti\sottotconti
    if this.oParentObject.w_SOT="S"
      * --- Controllo se esiste almeno un conto da esportare
      this.w_ALMENOUNSOT = .F.
      vq_exec("..\LEMC\EXE\QUERY\GSLM_AUC.VQR",this,"ALMUNCON")
      if RECCOUNT("ALMUNCON") > 0
        this.w_ALMENOUNSOT = .T.
      endif
      if USED("ALMUNCON")
        SELECT ALMUNCON
        USE
      endif
    endif
    this.w_NUMCLI = this.w_NUMCLI+2
    this.w_FILLER = SPACE(188)
    FWRITE(this.hFile,this.w_FILLER,188)
    * --- =============================
    if USED("ALMUNFOR")
      SELECT ALMUNFOR
      USE
    endif
    * --- EXPORT SOTTOCONTI -NON PIU ESEGUITO- 
    * --- Eventuale export della primanota
    if this.oParentObject.w_PNTIVA="S" OR this.oParentObject.w_PNTNOIVA="S"
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Riga fine ditta
    *     ============================
    FWRITE(this.hFile,"FDIT",4)
    if this.w_ALMENOUNFATTVEND and this.w_ALMENOUNCLI
      FWRITE(this.hFile,"002",3)
    endif
    if !this.w_ALMENOUNFATTVEND or !this.w_ALMENOUNCLI
      FWRITE(this.hFile,"001",3)
    endif
    FWRITE(this.hFile,RIGHT("0000000000000"+ALLTRIM(STR(this.w_TRT+this.w_NUMCLI)),13),13)
    * --- Inserisco filler
    this.w_FILLER = SPACE(180)
    FWRITE(this.hFile,this.w_FILLER,180)
    * --- ===============================================================
    FWRITE(this.hFile,"FINT",4)
    this.w_FILLER = SPACE(196)
    FWRITE(this.hFile,this.w_FILLER,196)
    * --- Chiudo il file di appoggio e lo riapro per posizionarmi in testa
    FCLOSE(this.hFile)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT DEI CLIENTI
    SELECT ALMUNCLI
    GO TOP
    do while NOT EOF()
      this.w_OK = .T.
      * --- Se il tipo di Codifica � 'Anagrafica Unica' controllo che la lunghezza della Partita IVA sia 11: Se diversa passo al record successivo...
      if this.w_FLANUN="S" AND LEN(ALLTRIM(ALMUNCLI.ANPARIVA)) <> 11
        this.w_STRINGA = ah_MsgFormat("%1Export cliente %2 %3","          ",UPPER(ALMUNCLI.ANCODICE),UPPER(ALMUNCLI.ANDESCRI))
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = ah_MsgFormat("%1Errore: la partita IVA non � presente o la sua lunghezza non � corretta","              ")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_OK = .F.
      endif
      if this.w_OK
        * --- Messaggio a Video
        ah_Msg("Export cliente: %1",.T.,.F.,.F.,ALMUNCLI.ANCODICE)
        * --- Scrittura su LOG
        this.w_STRINGA = ah_MsgFormat("%1Export cliente %2 %3","          ",UPPER(ALMUNCLI.ANCODICE),UPPER(ALMUNCLI.ANDESCRI))
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        * --- Aggiorno il numero di record trasferiti
        FWRITE(this.hFile,"CLF1",4)
        this.w_NUMCLI = this.w_NUMCLI+1
        * --- CodNumSogg
        FWRITE(this.hFile,right("000000"+alltrim(ALMUNCLI.ANCODSTU),6),6)
        * --- CodCliForn
        FWRITE(this.hFile,"C",1)
        * --- Codice soggetto
        FWRITE(this.hFile,RIGHT(SPACE(8)+ALLTRIM(NVL(ALMUNCLI.ANCODSOG," ")),8),8)
        * --- Ragione sociale - Cognome e nome se persona fisica
        if ALMUNCLI.ANPERFIS="S" and ! empty(nvl(ALMUNCLI.ANCOGNOM," ")) and ! empty(nvl(ALMUNCLI.AN__NOME," "))
          * --- Cognome
          FWRITE(this.hFile,LEFT(ALLTRIM(ALMUNCLI.ANCOGNOM)+SPACE(25),25),25)
          * --- Nome
          FWRITE(this.hFile,LEFT(ALLTRIM(ALMUNCLI.AN__NOME)+SPACE(25),25),25)
        else
          if len(RTRIM(ALMUNCLI.ANDESCRI))<=25
            this.w_RS = NVL(SUBSTR(ALMUNCLI.ANDESCRI,1,25), "                         ")
            FWRITE(this.hFile,this.w_RS,25)
            FWRITE(this.hFile,SPACE(25),25)
          else
            * --- Cerco il primo spazio prima di 25
            this.w_INDICE = 25
            do while SUBSTR(RTRIM(ALMUNCLI.ANDESCRI),this.w_INDICE,1)<>" " AND this.w_INDICE>1
              this.w_INDICE = this.w_INDICE-1
            enddo
            * --- Se non � presente alcuno spazio, eseguo il troncamento alla 25� posizione
            if this.w_INDICE=1
              this.w_INDICE = 25
            endif
            * --- Spezzo a w_INDICE
            this.w_RS1 = SUBSTR(RTRIM(ALMUNCLI.ANDESCRI),1,this.w_INDICE)+SPACE(25)
            this.w_RS2 = SUBSTR(RTRIM(ALMUNCLI.ANDESCRI),this.w_INDICE+1,40)+SPACE(25)
            FWRITE(this.hFile,LEFT(this.w_RS1,25),25)
            FWRITE(this.hFile,LEFT(this.w_RS2,25),25)
          endif
        endif
        this.w_ISITALIAN = ALMUNCLI.AFFLINTR#"S" AND (ALMUNCLI.NACODISO="IT" OR EMPTY(NVL(ALMUNCLI.NACODISO," ")))
        this.w_NAZIONE = NVL(ALMUNCLI.ANNAZION," ")
        * --- Read from NAZIONI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.NAZIONI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NACODISO"+;
            " from "+i_cTable+" NAZIONI where ";
                +"NACODNAZ = "+cp_ToStrODBC(this.w_NAZIONE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NACODISO;
            from (i_cTable) where;
                NACODNAZ = this.w_NAZIONE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_NACODISO = NVL(cp_ToDate(_read_.NACODISO),cp_NullValue(_read_.NACODISO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Codice fiscale
        if ALMUNCLI.AFFLINTR#"S" and upper(this.w_NACODISO) # "SM"
          FWRITE(this.hFile,NVL(ALMUNCLI.ANCODFIS, "                "),16)
        else
          FWRITE(this.hFile,LEFT(ALLTRIM(NVL(ALMUNCLI.NACODISO," "))+ALLTRIM(NVL(ALMUNCLI.ANPARIVA," "))+SPACE(16),16),16)
        endif
        * --- Controllo codice fiscale
        this.w_COND1 = EMPTY(NVL(ALMUNCLI.ANPARIVA," "))
        this.w_COND2 = EMPTY(NVL(ALMUNCLI.ANCODFIS," "))
        if this.w_ISITALIAN AND (not(CHKCFP (ALMUNCLI.ANCODFIS,"CF","",2)) and (this.w_COND1 or (!this.w_COND1 and ! this.w_COND2))) or (this.w_COND1 and this.w_COND2)
          FWRITE(this.hFile,"E",1)
        else
          FWRITE(this.hFile," ",1)
        endif
        * --- Soggetto intracomunitario
        if ALMUNCLI.AFFLINTR="S"
          FWRITE(this.hFile,"S",1)
        else
          FWRITE(this.hFile," ",1)
        endif
        * --- Flag compilata denominazione
        * --- Diversificazione a seconda del tipo soggetto (entit� D11)
        if ALMUNCLI.ANPERFIS="S"
          FWRITE(this.hFile," ",1)
        else
          FWRITE(this.hFile,"S",1)
        endif
        * --- filler
        this.w_FILLER = SPACE(112)
        FWRITE(this.hFile,this.w_FILLER,112)
        * --- Seconda parte cli/for
        FWRITE(this.hFile,"CLF2",4)
        this.w_NUMCLI = this.w_NUMCLI+1
        * --- Indirizzo
        * --- Via e Numero Civico devono essere separati da virgola
        this.w_INDICE = 25
        this.w_INDIRIZZO = NVL(ALMUNCLI.ANINDIRI,SPACE(32))
        do while (SUBSTR(this.w_INDIRIZZO,this.w_INDICE,1)<>"," and this.w_INDICE>0)
          this.w_INDICE = this.w_INDICE-1
        enddo
        if this.w_INDICE=0
          * --- Virgola non trovata
          if LEN(RTRIM(this.w_INDIRIZZO))>32
            this.w_STRINGA = ah_MsgFormat("%1Attenzione! L'indirizzo del cliente %2 sar� tagliato al 32^ carattere","               ",ALMUNCLI.ANCODICE)
            FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
            FWRITE(this.hLOG,CHR(13)+CHR(10),2)
            this.w_WARNING = .T.
          endif
          FWRITE(this.hFile,this.w_INDIRIZZO,32)
          FWRITE(this.hFile,"     ",5)
        else
          * --- Virgola trovata
          this.w_IND = SUBSTR(this.w_INDIRIZZO,1,this.w_INDICE-1)+SPACE(32)
          this.w_NUM = SUBSTR(this.w_INDIRIZZO,this.w_INDICE+1,5)+SPACE(5)
          if LEN(RTRIM(this.w_IND))>32
            this.w_STRINGA = ah_MsgFormat("%1Attenzione! L'indirizzo del cliente %2 sar� tagliato al 32^ carattere","               ",ALMUNCLI.ANCODICE)
            FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
            FWRITE(this.hLOG,CHR(13)+CHR(10),2)
            this.w_WARNING = .T.
          endif
          if LEN(RTRIM(this.w_NUM))>5
            this.w_STRINGA = ah_MsgFormat("%1Attenzione! Il numero civico del cliente %2 sar� tagliato al 5^ carattere","               ",ALMUNCLI.ANCODICE)
            FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
            FWRITE(this.hLOG,CHR(13)+CHR(10),2)
            this.w_WARNING = .T.
          endif
          this.w_GOT = this.w_GOT+1
          FWRITE(this.hFile,this.w_IND,32)
          FWRITE(this.hFile,this.w_NUM,5)
        endif
        * --- Codice catastale del comune
        FWRITE(this.hFile,NVL(ALMUNCLI.ANCODCAT,SPACE(4)),4)
        * --- CAP
        FWRITE(this.hFile,NVL(ALMUNCLI.AN___CAP, "     "),5)
        * --- Localit� - Comune di residenza
        if LEN(RTRIM(NVL(ALMUNCLI.ANLOCALI,"   ")))>23
          this.w_STRINGA = ah_MsgFormat("%1Attenzione! La localit� del cliente %2 sar� tagliata al 23^ carattere","               ",ALMUNCLI.ANCODICE)
          FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        endif
        FWRITE(this.hFile,NVL(ALMUNCLI.ANLOCALI, "                       "),23)
        * --- Provincia di residenza
        FWRITE(this.hFile,NVL(ALMUNCLI.ANPROVIN, "  "),2)
        * --- Partita IVA
        if this.w_ISITALIAN
          FWRITE(this.hFile,NVL(ALMUNCLI.ANPARIVA, "           "),11)
        else
          FWRITE(this.hFile,SPACE(11),11)
        endif
        * --- Controllo correttezza partita IVA
        this.w_COND2 = EMPTY(NVL(ALMUNCLI.ANPARIVA," "))
        this.w_COND1 = EMPTY(NVL(ALMUNCLI.ANCODFIS," "))
        if this.w_ISITALIAN AND (not(CHKCFP (ALMUNCLI.ANPARIVA, "PI", "C", 2,ALMUNCLI.ANCODICE,ALMUNCLI.ANNAZION)) and (this.w_COND1 or (!this.w_COND1 and ! this.w_COND2))) or (this.w_COND1 and this.w_COND2)
          FWRITE(this.hFile,"E",1)
        else
          FWRITE(this.hFile," ",1)
        endif
        * --- Tipo Soggetto
        if NVL(ALMUNCLI.ANPERFIS," ")="S" 
          if not empty(nvl(ALMUNCLI.ANCODFIS,"  ")) 
            if not empty(nvl(ALMUNCLI.ANPARIVA," "))
              FWRITE(this.hFile,"D",1)
            else
              FWRITE(this.hFile,"P",1)
            endif
          else
            if not empty(nvl(ALMUNCLI.ANPARIVA," "))
              FWRITE(this.hFile,"D",1)
            else
              FWRITE(this.hFile,"P",1)
            endif
          endif
        else
          if not empty(nvl(ALMUNCLI.ANCODFIS,"  ")) 
            if not empty(nvl(ALMUNCLI.ANPARIVA,"  ")) 
              FWRITE(this.hFile,"S",1)
            else
              FWRITE(this.hFile,"P",1)
            endif
          else
            if not empty(nvl(ALMUNCLI.ANPARIVA,"  ")) 
              FWRITE(this.hFile,"S",1)
            else
              * --- Errore
              FWRITE(this.hFile,"S",1)
              if empty(nvl(ALMUNCLI.ANCODSOG," "))
                this.w_STRINGA = ah_MsgFormat("%1Attenzione! Il cliente %2 non pu� essere identificato all'interno dello studio","               ",ALMUNCLI.ANCODICE)
                FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
                FWRITE(this.hLOG,CHR(13)+CHR(10),2)
                this.w_WARNING = .T.
              endif
            endif
          endif
        endif
        * --- Assegno la nazione del cliente
        * --- Flag San Marino
        if UPPER(ALLTRIM(this.w_NACODISO)) = "SM"
          FWRITE(this.hFile,"S",1)
        else
          FWRITE(this.hFile," ",1)
        endif
        * --- Bolla Doganale
        FWRITE(this.hFile,NVL(ALMUNCLI.ANFLBODO," "),1)
        * --- Codice Movim. DR 770
        FWRITE(this.hFile," ",1)
        * --- Flag Art. 110 C.10
        FWRITE(this.hFile," ",1)
        * --- Flag cliente/fornitore fittizio
        FWRITE(this.hFile," ",1)
        if ALMUNCLI.AFFLINTR="S" OR UPPER(ALLTRIM(this.w_NACODISO)) = "SM"
          * --- Soggetto UE
          * --- Data di nascita
          this.w_DATANAS = IIF(EMPTY(DTOS(NVL(ALMUNCLI.ANDATNAS,CTOD("  -  -    ")))),REPL("0",8),RIGHT(REPL("0",8)+ALLTRIM(DTOS(ALMUNCLI.ANDATNAS)),8))
          FWRITE(this.hFile,this.w_DATANAS,8)
          * --- Comune di nascita
          FWRITE(this.hFile,RIGHT("    "+NVL(ALMUNCLI.CCCODICE,"    "),4),4)
          if ALMUNCLI.ANFLBLLS = "S"
            * --- In BlackList
            * --- Identificativo fiscale - Codice IVA
            FWRITE(this.hFile,LEFT(ALLTRIM(NVL(ALMUNCLI.NACODISO," "))+ALLTRIM(NVL(ALMUNCLI.ANPARIVA," "))+SPACE(25),25),25)
            * --- Identificativo fiscale - Codice Fiscale
            FWRITE(this.hFile,SPACE(25),25)
          else
            * --- Non in BlackList
            * --- Identificativo fiscale - Codice IVA
            FWRITE(this.hFile,SPACE(25),25)
            * --- Identificativo fiscale - Codice Fiscale
            FWRITE(this.hFile,SPACE(25),25)
          endif
          * --- Flag Black List
          FWRITE(this.hFile,ALMUNCLI.ANFLBLLS,1)
          * --- Nome frazione
          FWRITE(this.hFile,SPACE(25),25)
          * --- AIFT - gestioni 3000 euro 
          if g_AIFT ="S" AND NVL (ALMUNCLI.ANOPETRE ,"N") <>"N"
            FWRITE(this.hFile, alltrim( ALMUNCLI.ANOPETRE) ,1)
          else
            FWRITE(this.hFile, SPACE (1) , 1)
          endif
          this.w_FILLER = SPACE(18)
          * --- filler
          FWRITE(this.hFile,this.w_FILLER,18)
        else
          if this.w_ISITALIAN
            * --- Data nasc. + comune nasc. + Cod. Iva + Cod. fiscale
            this.w_FILLER = SPACE(62)
            * --- filler
            FWRITE(this.hFile,this.w_FILLER,62)
            * --- Flag Black List
            FWRITE(this.hFile,IIF(ALMUNCLI.ANFLBLLS="N", " ",ALMUNCLI.ANFLBLLS),1)
            * --- Nome frazione
            FWRITE(this.hFile,SPACE(25),25)
            * --- AIFT - gestioni 3000 euro 
            if g_AIFT ="S" AND NVL (ALMUNCLI.ANOPETRE ,"N") <>"N"
              FWRITE(this.hFile, alltrim( ALMUNCLI.ANOPETRE) ,1)
            else
              FWRITE(this.hFile, SPACE (1) , 1)
            endif
            this.w_FILLER = SPACE(18)
            * --- filler
            FWRITE(this.hFile,this.w_FILLER,18)
          else
            * --- Non soggetto
            * --- Data di nascita
            this.w_DATANAS = IIF(EMPTY(DTOS(NVL(ALMUNCLI.ANDATNAS,CTOD("  -  -    ")))),REPL("0",8),RIGHT(REPL("0",8)+ALLTRIM(DTOS(ALMUNCLI.ANDATNAS)),8))
            FWRITE(this.hFile,this.w_DATANAS,8)
            * --- Comune di nascita
            FWRITE(this.hFile,RIGHT("    "+NVL(ALMUNCLI.CCCODICE,"    "),4),4)
            * --- Identificativo fiscale - Codice IVA
            FWRITE(this.hFile,SPACE(25),25)
            if ALMUNCLI.ANFLBLLS = "S"
              * --- Identificativo fiscale - Codice Fiscale
              do case
                case ! empty(nvl(ALMUNCLI.ANCOFISC," "))
                  FWRITE(this.hFile,LEFT(ALLTRIM(NVL(ALMUNCLI.NACODISO," "))+ALLTRIM(NVL(ALMUNCLI.ANCOFISC," "))+SPACE(25),25),25)
                case ! empty(nvl(ALMUNCLI.ANCODFIS," "))
                  FWRITE(this.hFile,LEFT(ALLTRIM(NVL(ALMUNCLI.NACODISO," "))+ALLTRIM(NVL(ALMUNCLI.ANCODFIS," "))+SPACE(25),25),25)
                case ! empty(nvl(ALMUNCLI.ANPARIVA," "))
                  FWRITE(this.hFile,LEFT(ALLTRIM(NVL(ALMUNCLI.NACODISO," "))+ALLTRIM(NVL(ALMUNCLI.ANPARIVA," "))+SPACE(25),25),25)
                otherwise
                  FWRITE(this.hFile,SPACE(25),25)
              endcase
            else
              * --- Non in BlackList
              * --- Identificativo fiscale - Codice Fiscale
              FWRITE(this.hFile,SPACE(25),25)
            endif
            * --- Flag Black List
            FWRITE(this.hFile,IIF(ALMUNCLI.ANFLBLLS="N", " ",ALMUNCLI.ANFLBLLS),1)
            * --- Nome frazione
            FWRITE(this.hFile,SPACE(25),25)
            * --- AIFT - gestioni 3000 euro 
            if g_AIFT ="S" AND NVL (ALMUNCLI.ANOPETRE ,"N") <>"N"
              FWRITE(this.hFile, alltrim( ALMUNCLI.ANOPETRE) ,1)
            else
              FWRITE(this.hFile, SPACE (1) , 1)
            endif
            this.w_FILLER = SPACE(18)
            * --- filler
            FWRITE(this.hFile,this.w_FILLER,18)
          endif
        endif
      endif
      skip
    enddo
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT DEI FORNITORI
    SELECT ALMUNFOR
    GO TOP
    do while NOT EOF()
      this.w_OK = .T.
      * --- Se il tipo di Codifica � 'Anagrafica Unica' controllo che la lunghezza della Partita IVA sia 11: Se diversa passo al record successivo...
      if this.w_FLANUN="S" AND LEN(ALLTRIM(ALMUNFOR.ANPARIVA)) <> 11
        this.w_STRINGA = ah_MsgFormat("%1Export fornitore %2 %3","          ",UPPER(ALMUNFOR.ANCODICE),UPPER(ALMUNFOR.ANDESCRI))
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = ah_MsgFormat("%1Errore: la partita IVA non � presente o la sua lunghezza non � corretta","              ")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_OK = .F.
      endif
      if this.w_OK
        * --- Messaggio a video
        ah_Msg("Export fornitore: %1",.T.,.F.,.F.,ALMUNFOR.ANCODICE)
        * --- Scrittura su LOG
        this.w_STRINGA = ah_MsgFormat("%1Export fornitore %2 %3","          ",UPPER(ALMUNFOR.ANCODICE),UPPER(ALMUNFOR.ANDESCRI))
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        * --- Aggiorno il numero di record trasferiti
        this.w_TRT = this.w_TRT+2
        this.w_NUMFOR = this.w_NUMFOR+2
        * --- Prima parte cli/for
        FWRITE(this.hFile,"CLF1",4)
        * --- CodNumSogg
        FWRITE(this.hFile,right("000000"+alltrim(ALMUNFOR.ANCODSTU),6),6)
        * --- CodCliForn
        FWRITE(this.hFile,"F",1)
        * --- Codice soggetto
        FWRITE(this.hFile,RIGHT(SPACE(8)+ALLTRIM(NVL(ALMUNFOR.ANCODSOG," ")),8),8)
        * --- Ragione sociale
        if ALMUNFOR.ANPERFIS="S" and ! empty(nvl(ALMUNFOR.ANCOGNOM," ")) and ! empty(nvl(ALMUNFOR.AN__NOME," "))
          * --- Cognome
          FWRITE(this.hFile,LEFT(ALLTRIM(ALMUNFOR.ANCOGNOM)+SPACE(25),25),25)
          * --- Nome
          FWRITE(this.hFile,LEFT(ALLTRIM(ALMUNFOR.AN__NOME)+SPACE(25),25),25)
        else
          if len(RTRIM(ALMUNFOR.ANDESCRI))<=25
            this.w_RS = NVL(SUBSTR(ALMUNFOR.ANDESCRI,1,25), "                         ")
            FWRITE(this.hFile,this.w_RS,25)
            FWRITE(this.hFile,SPACE(25),25)
          else
            * --- Cerco il primo spazio prima di 25
            this.w_INDICE = 25
            do while SUBSTR(RTRIM(ALMUNFOR.ANDESCRI),this.w_INDICE,1)<>" " AND this.w_INDICE>1
              this.w_INDICE = this.w_INDICE-1
            enddo
            * --- Se non � presente alcuno spazio, eseguo il troncamento alla 25� posizione
            if this.w_INDICE=1
              this.w_INDICE = 25
            endif
            * --- Spezzo a w_INDICE
            this.w_RS1 = SUBSTR(RTRIM(ALMUNFOR.ANDESCRI),1,this.w_INDICE)+SPACE(25)
            this.w_RS2 = SUBSTR(RTRIM(ALMUNFOR.ANDESCRI),this.w_INDICE+1,40)+SPACE(25)
            FWRITE(this.hFile,LEFT(this.w_RS1,25),25)
            FWRITE(this.hFile,LEFT(this.w_RS2,25),25)
          endif
        endif
        this.w_ISITALIAN = ALMUNFOR.AFFLINTR#"S" AND (ALMUNFOR.NACODISO="IT" OR EMPTY(NVL(ALMUNFOR.NACODISO," ")))
        * --- Assegno la nazione del fornitore
        this.w_NAZIONE = NVL(ALMUNFOR.ANNAZION," ")
        * --- Read from NAZIONI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.NAZIONI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NACODISO"+;
            " from "+i_cTable+" NAZIONI where ";
                +"NACODNAZ = "+cp_ToStrODBC(this.w_NAZIONE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NACODISO;
            from (i_cTable) where;
                NACODNAZ = this.w_NAZIONE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_NACODISO = NVL(cp_ToDate(_read_.NACODISO),cp_NullValue(_read_.NACODISO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Codice fiscale
        if ALMUNFOR.AFFLINTR#"S" and upper(this.w_NACODISO) # "SM"
          FWRITE(this.hFile,NVL(ALMUNFOR.ANCODFIS, "                "),16)
        else
          FWRITE(this.hFile,LEFT(ALLTRIM(NVL(ALMUNFOR.NACODISO," "))+ALLTRIM(NVL(ALMUNFOR.ANPARIVA," "))+SPACE(16),16),16)
        endif
        * --- Controllo codice fiscale
        this.w_COND1 = EMPTY(NVL(ALMUNFOR.ANPARIVA," "))
        this.w_COND2 = EMPTY(NVL(ALMUNFOR.ANCODFIS," "))
        if (not(CHKCFP (ALMUNFOR.ANCODFIS,"CF","",2)) and (this.w_COND1 or (!this.w_COND1 and ! this.w_COND2))) or (this.w_COND1 and this.w_COND2)
          FWRITE(this.hFile,"E",1)
        else
          FWRITE(this.hFile," ",1)
        endif
        * --- Soggetto intracomunitario
        if ALMUNFOR.AFFLINTR="S"
          FWRITE(this.hFile,"S",1)
        else
          FWRITE(this.hFile," ",1)
        endif
        * --- Flag compilata denominazione
        * --- Diversificazione a seconda del tipo soggetto (entit� D11)
        if ALMUNFOR.ANPERFIS="S"
          FWRITE(this.hFile," ",1)
        else
          FWRITE(this.hFile,"S",1)
        endif
        * --- Filler
        this.w_FILLER = SPACE(112)
        FWRITE(this.hFile,this.w_FILLER,112)
        * --- Seconda parte cli/for
        FWRITE(this.hFile,"CLF2",4)
        this.w_NUMCLI = this.w_NUMCLI+1
        * --- Indirizzo
        * --- Via e Numero Civico devono essere separati da virgola
        this.w_INDIRIZZO = NVL(ALMUNFOR.ANINDIRI,SPACE(32))
        this.w_INDICE = 25
        do while (SUBSTR(this.w_INDIRIZZO,this.w_INDICE,1)<>"," and this.w_INDICE>0)
          this.w_INDICE = this.w_INDICE-1
        enddo
        if this.w_INDICE=0
          * --- Virgola non trovata
          if LEN(RTRIM(this.w_INDIRIZZO))>32
            this.w_STRINGA = ah_MsgFormat("%1Attenzione! L'indirizzo del fornitore %2 sar� tagliato al 32^ carattere","               ",ALMUNFOR.ANCODICE)
            FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
            FWRITE(this.hLOG,CHR(13)+CHR(10),2)
            this.w_WARNING = .T.
          endif
          FWRITE(this.hFile,NVL(this.w_INDIRIZZO, "                                "),32)
          FWRITE(this.hFile,"     ",5)
        else
          * --- Virgola trovata
          this.w_IND = SUBSTR(this.w_INDIRIZZO,1,this.w_INDICE-1)+SPACE(32)
          this.w_NUM = SUBSTR(this.w_INDIRIZZO,this.w_INDICE+1,5)+SPACE(5)
          if LEN(RTRIM(this.w_IND))>32
            this.w_STRINGA = ah_MsgFormat("%1Attenzione! L'indirizzo del fornitore %2 sar� tagliato al 32^ carattere","               ",ALMUNFOR.ANCODICE)
            FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
            FWRITE(this.hLOG,CHR(13)+CHR(10),2)
            this.w_WARNING = .T.
          endif
          if LEN(RTRIM(this.w_NUM))>5
            this.w_STRINGA = ah_MsgFormat("%1Attenzione! Il numero civico del fornitore %2 sar� tagliato al 5^ carattere","               ",ALMUNFOR.ANCODICE)
            FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
            FWRITE(this.hLOG,CHR(13)+CHR(10),2)
            this.w_WARNING = .T.
          endif
          FWRITE(this.hFile,this.w_IND,32)
          FWRITE(this.hFile,this.w_NUM,5)
        endif
        * --- Codice catastale del comune
        FWRITE(this.hFile,NVL(ALMUNFOR.ANCODCAT,SPACE(4)),4)
        * --- CAP
        FWRITE(this.hFile,NVL(ALMUNFOR.AN___CAP, "     "),5)
        * --- Localit� - Comune di Residenza
        if LEN(RTRIM(NVL(ALMUNFOR.ANLOCALI," ")))>23
          this.w_STRINGA = ah_MsgFormat("%1Attenzione! La localit� del fornitore %2 sar� tagliata al 23^ carattere","               ",ALMUNFOR.ANCODICE)
          FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        endif
        FWRITE(this.hFile,NVL(ALMUNFOR.ANLOCALI, "                       "),23)
        * --- Provincia di residenza
        FWRITE(this.hFile,NVL(ALMUNFOR.ANPROVIN, "  "),2)
        * --- Partita IVA
        if this.w_ISITALIAN
          FWRITE(this.hFile,NVL(ALMUNFOR.ANPARIVA, "           "),11)
        else
          FWRITE(this.hFile,SPACE(11),11)
        endif
        * --- Controllo correttezza partita IVA
        this.w_COND2 = EMPTY(NVL(ALMUNFOR.ANPARIVA," "))
        this.w_COND1 = EMPTY(NVL(ALMUNFOR.ANCODFIS," "))
        if (not(CHKCFP (ALMUNFOR.ANPARIVA, "PI", "C",2, ALMUNFOR.ANCODICE,ALMUNFOR.ANNAZION)) and (this.w_COND1 or (!this.w_COND1 and ! this.w_COND2))) or (this.w_COND1 and this.w_COND2)
          FWRITE(this.hFile,"E",1)
        else
          FWRITE(this.hFile," ",1)
        endif
        * --- Tipo Soggetto
        if NVL(ALMUNFOR.ANPERFIS," ")="S" 
          if not empty(nvl(ALMUNFOR.ANCODFIS,"  ")) 
            if not empty(nvl(ALMUNFOR.ANPARIVA," "))
              FWRITE(this.hFile,"D",1)
            else
              FWRITE(this.hFile,"P",1)
            endif
          else
            if not empty(nvl(ALMUNFOR.ANPARIVA," "))
              FWRITE(this.hFile,"D",1)
            else
              FWRITE(this.hFile,"P",1)
            endif
          endif
        else
          if not empty(nvl(ALMUNFOR.ANCODFIS,"  ")) 
            if not empty(nvl(ALMUNFOR.ANPARIVA,"  ")) 
              FWRITE(this.hFile,"S",1)
            else
              FWRITE(this.hFile,"P",1)
            endif
          else
            if not empty(nvl(ALMUNFOR.ANPARIVA,"  ")) 
              FWRITE(this.hFile,"S",1)
            else
              * --- Errore
              FWRITE(this.hFile,"S",1)
              if empty(nvl(ALMUNFOR.ANCODSOG," "))
                this.w_STRINGA = ah_MsgFormat("%1Attenzione! Il fornitore %2 non pu� essere identificato all'interno dello studio","               ",ALMUNFOR.ANCODICE)
                FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
                FWRITE(this.hLOG,CHR(13)+CHR(10),2)
                this.w_WARNING = .T.
              endif
            endif
          endif
        endif
        * --- Flag San Marino
        if UPPER(ALLTRIM(this.w_NACODISO)) = "SM"
          FWRITE(this.hFile,"S",1)
        else
          FWRITE(this.hFile," ",1)
        endif
        * --- Bolla Doganale
        FWRITE(this.hFile,NVL(ALMUNFOR.ANFLBODO," "),1)
        * --- Codice Movim. 770
        FWRITE(this.hFile," ",1)
        * --- Flag Art. 110 C.10
        FWRITE(this.hFile," ",1)
        * --- Flag cliente/fornitore fittizio
        FWRITE(this.hFile," ",1)
        if ALMUNFOR.AFFLINTR="S" OR UPPER(ALLTRIM(this.w_NACODISO)) = "SM"
          * --- Soggetto UE
          * --- Data di nascita
          this.w_DATANAS = IIF(EMPTY(DTOS(NVL(ALMUNFOR.ANDATNAS,CTOD("  -  -    ")))),REPL("0",8),RIGHT(REPL("0",8)+ALLTRIM(DTOS(ALMUNFOR.ANDATNAS)),8))
          FWRITE(this.hFile,this.w_DATANAS,8)
          * --- Comune di nascita
          FWRITE(this.hFile,RIGHT("    "+NVL(ALMUNFOR.CCCODICE,"    "),4),4)
          if ALMUNFOR.ANFLBLLS = "S"
            * --- In BlackList
            * --- Identificativo fiscale - Codice IVA
            FWRITE(this.hFile,LEFT(ALLTRIM(NVL(ALMUNFOR.NACODISO," "))+ALLTRIM(NVL(ALMUNFOR.ANPARIVA," "))+SPACE(25),25),25)
            * --- Identificativo fiscale - Codice Fiscale
            FWRITE(this.hFile,SPACE(25),25)
          else
            * --- Non in BlackList
            * --- Identificativo fiscale - Codice IVA
            FWRITE(this.hFile,SPACE(25),25)
            * --- Identificativo fiscale - Codice Fiscale
            FWRITE(this.hFile,SPACE(25),25)
          endif
          * --- Flag Black List
          FWRITE(this.hFile,IIF(ALMUNFOR.ANFLBLLS="N", " ",ALMUNFOR.ANFLBLLS),1)
          * --- Nome frazione
          FWRITE(this.hFile,SPACE(25),25)
          * --- AIFT - gestioni 3000 euro 
          if g_AIFT ="S" AND NVL (ALMUNFOR.ANOPETRE ,"N") <>"N"
            FWRITE(this.hFile, alltrim( ALMUNFOR.ANOPETRE) ,1)
          else
            FWRITE(this.hFile, SPACE (1) , 1)
          endif
          this.w_FILLER = SPACE(18)
          * --- filler
          FWRITE(this.hFile,this.w_FILLER,18)
        else
          if this.w_ISITALIAN
            * --- Data nasc. + comune nasc. + Cod. Iva + Cod. fiscale
            this.w_FILLER = SPACE(62)
            * --- filler
            FWRITE(this.hFile,this.w_FILLER,62)
            * --- Flag Black List
            FWRITE(this.hFile,IIF(ALMUNFOR.ANFLBLLS="N", " ",ALMUNFOR.ANFLBLLS),1)
            * --- Nome frazione
            FWRITE(this.hFile,SPACE(25),25)
            * --- AIFT - gestioni 3000 euro 
            if g_AIFT ="S" AND NVL (ALMUNFOR.ANOPETRE ,"N") <>"N"
              FWRITE(this.hFile, alltrim( ALMUNFOR.ANOPETRE) ,1)
            else
              FWRITE(this.hFile, SPACE (1) , 1)
            endif
            this.w_FILLER = SPACE(18)
            * --- filler
            FWRITE(this.hFile,this.w_FILLER,18)
          else
            * --- Non soggetto
            * --- Data di nascita
            this.w_DATANAS = IIF(EMPTY(DTOS(NVL(ALMUNFOR.ANDATNAS,CTOD("  -  -    ")))),REPL("0",8),RIGHT(REPL("0",8)+ALLTRIM(DTOS(ALMUNFOR.ANDATNAS)),8))
            FWRITE(this.hFile,this.w_DATANAS,8)
            * --- Comune di nascita
            FWRITE(this.hFile,RIGHT("    "+NVL(ALMUNFOR.CCCODICE,"    "),4),4)
            * --- Identificativo fiscale - Codice IVA
            FWRITE(this.hFile,SPACE(25),25)
            if ALMUNFOR.ANFLBLLS = "S"
              * --- Identificativo fiscale - Codice Fiscale
              do case
                case ! empty(nvl(ALMUNFOR.ANCOFISC," "))
                  FWRITE(this.hFile,LEFT(ALLTRIM(NVL(ALMUNFOR.NACODISO," "))+ALLTRIM(NVL(ALMUNFOR.ANCOFISC," "))+SPACE(25),25),25)
                case ! empty(nvl(ALMUNFOR.ANCODFIS," "))
                  FWRITE(this.hFile,LEFT(ALLTRIM(NVL(ALMUNFOR.NACODISO," "))+ALLTRIM(NVL(ALMUNFOR.ANCODFIS," "))+SPACE(25),25),25)
                case ! empty(nvl(ALMUNFOR.ANPARIVA," "))
                  FWRITE(this.hFile,LEFT(ALLTRIM(NVL(ALMUNFOR.NACODISO," "))+ALLTRIM(NVL(ALMUNFOR.ANPARIVA," "))+SPACE(25),25),25)
                otherwise
                  FWRITE(this.hFile,SPACE(25),25)
              endcase
            else
              * --- Non in BlackList
              * --- Identificativo fiscale - Codice Fiscale
              FWRITE(this.hFile,SPACE(25),25)
            endif
            * --- Flag Black List
            FWRITE(this.hFile,ALMUNFOR.ANFLBLLS,1)
            * --- Nome frazione
            FWRITE(this.hFile,SPACE(25),25)
            * --- AIFT - gestioni 3000 euro 
            if g_AIFT ="S" AND NVL (ALMUNFOR.ANOPETRE ,"N") <>"N"
              FWRITE(this.hFile, alltrim( ALMUNFOR.ANOPETRE) ,1)
            else
              FWRITE(this.hFile, SPACE (1) , 1)
            endif
            this.w_FILLER = SPACE(18)
            * --- filler
            FWRITE(this.hFile,this.w_FILLER,18)
          endif
        endif
      endif
      skip
    enddo
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT DEI CONTI - NON PREVISTO PER CONTB
    * --- EXPORT DEI CONTI
    SELECT ALMUNCON
    GO TOP
    do while NOT EOF()
      * --- Messaggio a Video
      ah_Msg("Export conto: %1",.T.,.F.,.F.,ALMUNCON.LMCODSOT)
      * --- Scrittura su LOG
      this.w_STRINGA = Ah_MsgFormat("%1Export conto %2 %3","          ",UPPER(ALMUNCON.LMCODSOT),UPPER(ALMUNCON.LMDESSOT))
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      * --- Incremento numero di record scritti
      this.w_TRT = this.w_TRT+1
      this.w_NUMSOT = this.w_NUMSOT+1
      * --- Inserisco il tipo record
      FWRITE(this.hFile,"CON1",4)
      * --- Scrittura del conto
      FWRITE(this.hFile,Right("00"+Nvl(STU_PIAC.LMCODCON,"   "),3),3)
      * --- Progressivo conto
      FWRITE(this.hFile,Right("00000000"+Nvl(STU_PIAC.LMPROGRE,Space(8)),8),8)
      * --- Scrittura della descrizione
      FWRITE(this.hFile,ALMUNCON.LMDESSOT,30)
      * --- STUMPIAC.LMMINCON
      FWRITE(this.hFile,Right("000000"+Nvl(STUMPIAC.LMMINCON,Space(6)),6),6)
      FWRITE(this.hFile,Right("000000"+Nvl(STUMPIAC.LMMAXCON,Space(6)),6),6)
      * --- A Patrimoniale  B Economico
      FWRITE(this.hFile,IIF(Nvl(STU_PIAC.LMTIPSOT," ") $ "C-R","B","A"),1)
      * --- C Costo R Ricavo A Altro
      FWRITE(this.hFile,IIF(Nvl(STU_PIAC.LMTIPSOT," ") $ "C-R",Nvl(STU_PIAC.LMTIPSOT," "),"A"),1)
      * --- Scrittura del tipo conto
      FWRITE(this.hFile,Nvl(ALMUNCON.LMTIPCON," "),1)
      * --- Conto d'ordine
      FWRITE(this.hFile,Nvl(ALMUNCON.LMTIPCON," "),1)
      * --- STUMPIAC.LMCONORD
      FWRITE(this.hFile,Nvl(ALMUNCON.LMCONORD,"N"),1)
      * --- Filler
      this.w_FILLER = SPACE(139)
      FWRITE(this.hFile,this.w_FILLER,139)
      skip
    enddo
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT DELLA PRIMANOTA
    this.w_ALMENOUNFATTACQU = .F.
    this.w_ALMENOUNFATTVEND = .F.
    this.w_ALMENOUNFATTCORR = .F.
    this.w_ALMENOUNFATTVENT = .F.
    this.w_ALMENOUNCORRNORM = .F.
    this.w_ALMENOUNCORRVENT = .F.
    this.w_ALMENOUNMOCONTAB = .F.
    this.w_TRT = 0
    * --- Delete from STU_PNTT
    i_nConn=i_TableProp[this.STU_PNTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- SCRITTURA DEL FILE DI LOG
    this.w_STRINGA = ah_MsgFormat("---------- INIZIO EXPORT PRIMANOTA ----------")
    FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    if this.oParentObject.w_PNTIVA="S"
      * --- FATTURE DI VENDITA e NOTE DI CREDITO
      * --- Controllo se esiste almeno una fattura di vendita nell'intervallo stabilito
      vq_exec("..\LEMC\EXE\QUERY\GSLM_FAV.VQR",this,"FATTVEND")
      if RECCOUNT("FATTVEND") > 0
        this.w_ALMENOUNFATTVEND = .T.
        FWRITE (this.hFile , "IFAT" , 4 )
        this.w_FILLER = SPACE(196)
        FWRITE (this.hFile , this.w_FILLER , 196 )
      endif
      * --- Scrittura sul log
      this.w_STRINGA = ah_MsgFormat("-- INIZIO EXPORT FATTURE DI VENDITA --")
      FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      if this.w_ALMENOUNFATTVEND
        * --- Testa
        * --- Incremento numero di record scritti
        this.w_FATTVEND = this.w_FATTVEND+1
        * --- Lancio il batch che butta giu le fatture di vendita e le Note di credito
        do GSLMLBFV with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Coda
        * --- Incremento numero di record scritti
        this.w_FATTVEND = this.w_FATTVEND+1
        FWRITE (this.hFile , "FFAT" , 4 )
        FWRITE(this.hFile,RIGHT("00000000"+ALLTRIM(STR(this.w_TRT)),8),8)
        this.w_TRT = this.w_TRT+2
        this.w_FILLER = SPACE(188)
        FWRITE(this.hFile,this.w_FILLER,188)
        * --- Aggiorno la variabile di gruppi omogeni trasferiti
        this.w_GOT = this.w_GOT+1
        * --- Scrittura sul log
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = ah_MsgFormat("-- FINE EXPORT FATTURE DI VENDITA --")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      else
        * --- Scrittura sul log
        this.w_STRINGA = ah_MsgFormat("%1NESSUNA FATTURE DI VENDITA DA ESPORTARE","          ")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        this.w_STRINGA = ah_MsgFormat("-- FINE EXPORT FATTURE DI VENDITA --")
        FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
        FWRITE(this.hLOG,CHR(13)+CHR(10),2)
      endif
      SELECT FATTVEND
      USE
    endif
    * --- SCRITTURA DEL FILE DI LOG
    this.w_STRINGA = ah_MsgFormat("---------- FINE EXPORT PRIMANOTA ----------")
    FWRITE(this.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    FWRITE(this.hLOG,CHR(13)+CHR(10),2)
    * --- Controllo se tutta la prima nota � andata a buon fine
    this.w_CONDIZ = this.w_ALMENOUNFATTVENT
    this.w_CONDIZ = this.w_ALMENOUNCORRNORM OR this.w_ALMENOUNCORRVENT OR this.w_ALMENOUNMOCONTAB OR this.w_CONDIZ
    if this.w_CONDIZ
      * --- Select from STU_PNTT
      i_nConn=i_TableProp[this.STU_PNTT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2],.t.,this.STU_PNTT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" STU_PNTT ";
            +" where LMNUMTRA=-1";
             ,"_Curs_STU_PNTT")
      else
        select * from (i_cTable);
         where LMNUMTRA=-1;
          into cursor _Curs_STU_PNTT
      endif
      if used('_Curs_STU_PNTT')
        select _Curs_STU_PNTT
        locate for 1=1
        do while not(eof())
        this.w_PNTNOTAOK = .F.
          select _Curs_STU_PNTT
          continue
        enddo
        use
      endif
    endif
  endproc


  proc Init(oParentObject,pEXEC)
    this.pEXEC=pEXEC
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='PNT_MAST'
    this.cWorkTables[4]='STU_PARA'
    this.cWorkTables[5]='STU_PIAC'
    this.cWorkTables[6]='STU_PNTT'
    this.cWorkTables[7]='AZIENDA'
    this.cWorkTables[8]='NAZIONI'
    return(this.OpenAllTables(8))

  proc CloseCursors()
    if used('_Curs_STU_PNTT')
      use in _Curs_STU_PNTT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEXEC"
endproc
