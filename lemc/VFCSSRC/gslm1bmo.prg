* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm1bmo                                                        *
*              Import movimenti contabili                                      *
*                                                                              *
*      Author: Zucchetti TM                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][116][VRS_430]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-09                                                      *
* Last revis.: 2010-10-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm1bmo",oParentObject)
return(i_retval)

define class tgslm1bmo as StdBatch
  * --- Local variables
  w_COMODO = space(10)
  w_MESS1 = space(120)
  w_TIPO = space(4)
  w_NUMPRG = 0
  w_DATREG = ctod("  /  /  ")
  w_FLPROV = space(1)
  w_APRCAU = space(3)
  w_DESRIG = space(50)
  w_IMPO = space(13)
  w_NUMIMPO = 0
  w_SEGNO = space(1)
  w_CCDAR = space(4)
  w_SOTDAR = space(6)
  w_CCAVE = space(4)
  w_SOTAVE = space(6)
  w_TIPCDAR = space(1)
  w_PIVADAR = space(16)
  w_TIPCAVE = space(1)
  w_PIVAAVE = space(16)
  w_FLAGDIV = space(1)
  w_CONTAB = space(1)
  w_ESCAPE = space(200)
  w_CLIAVE = space(15)
  w_CLIDAR = space(15)
  w_FORAVE = space(15)
  w_FORDAR = space(15)
  w_CONAVE = space(15)
  w_CONDAR = space(15)
  w_CCFLSALI = space(1)
  w_CCFLSALF = space(1)
  w_PCTIPSOTD = space(1)
  w_PCTIPSOTA = space(1)
  w_PCTIPSOT = space(1)
  w_ASSOCI = space(2)
  w_HOCCAU = space(5)
  w_CLIFORCON = space(1)
  w_FOUND = .f.
  w_CODVAL = space(3)
  w_CAOVAL = 0
  w_TIPSOT = space(1)
  w_NUMRIG = 0
  w_FLDAVE = space(1)
  w_COMPET = space(4)
  w_PERCORSO = space(10)
  w_PNSERIAL = space(10)
  w_PNNUMRER = space(10)
  w_PNCODUTE = 0
  w_PNCODESE = space(4)
  w_PNPRG = space(8)
  w_CHIUSURA = .f.
  w_CODAZI = space(5)
  w_DESCRI = space(50)
  w_RIGDES = .f.
  w_PNDATREG = ctod("  /  /  ")
  w_PNCODCAU = space(5)
  w_PNCOMPET = space(4)
  w_PNVALNAZ = space(3)
  w_PNCODVAL = space(3)
  w_PNCAOVAL = 0
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_PNTIPCON = space(1)
  w_PNCODCON = space(15)
  w_PNIMPDAR = 0
  w_PNIMPAVE = 0
  w_PNDESRIG = space(50)
  w_PNFLSALI = space(1)
  w_PNFLSALF = space(1)
  w_PNCAURIG = space(5)
  w_PNFLPART = space(1)
  w_PNFLZERO = space(1)
  w_PNFLSALD = space(1)
  w_PNFLABAN = space(1)
  w_PNTIPREG = space(1)
  w_PNTIPDOC = space(2)
  w_SLTIPCON = space(1)
  w_SLCODICE = space(15)
  w_SLCODESE = space(4)
  w_SLDARPER = 0
  w_SLAVEPER = 0
  w_SLDARINI = space(1)
  w_SLAVEINI = space(1)
  w_SLDARFIN = space(1)
  w_SLAVEFIN = space(1)
  w_LMCODCON = space(15)
  w_LMIMPDAR = 0
  w_LMIMPAVE = 0
  w_LMFLSALI = space(1)
  w_LMFLSALF = space(1)
  w_CONTSTU = space(6)
  w_TIPREG = space(1)
  w_TIPDOC = space(2)
  * --- WorkFile variables
  CAU_CONT_idx=0
  CONTI_idx=0
  MASTRI_idx=0
  PNT_DETT_idx=0
  PNT_MAST_idx=0
  SALDICON_idx=0
  STU_INTR_idx=0
  STU_PARA_idx=0
  STU_TPNT_idx=0
  STU_TRAS_idx=0
  ESERCIZI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LETTURA DEI MOVIMENTI CONTABILI
    this.w_NUMPRG = 0
    this.w_PNSERIAL = SPACE(10)
    this.w_PNNUMRER = SPACE(10)
    this.w_PNCODUTE = IIF(g_UNIUTE $ "UE", i_CODUTE, 0)
    i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
    this.w_CODAZI = i_CODAZI
    this.w_RIGDES = .F.
    * --- Variabili per la scrittura in primanota
    this.w_PNFLPART = "N"
    this.w_PNFLZERO = " "
    this.w_PNFLSALD = "+"
    this.w_PNFLABAN = " "
    * --- Variabili per la scrittura nei saldi contabili
    * --- Variabili per la scrittura nel temporaneo
    * --- Variabili per gestione IVA
    * --- Creazione e apertura file di log
    * --- Inizializzazione e preparazione percorso di default
    this.w_PERCORSO = SYS(5)+SYS(2003)+"\"+"CONTB"
    if !DIRECTORY(this.w_PERCORSO)
      * --- Crea la directory LEMCO all'interno della dir EXE
      w_CURRDIR=SYS(5)+SYS(2003)
      MKDIR "CONTB"
      set defa to &w_CURRDIR
    endif
    * --- Preparazione nome temporaneo di log
    this.oParentObject.w_FILELOG = this.w_PERCORSO+"\"+"LOG00000.TMP"
    * --- Creazione e apertura file di log
    hLOG=FCREATE(this.oParentObject.w_FILELOG,0)
    * --- Testata file di LOG
    FWRITE(hLOG,REPL("-",80),80)
    FWRITE(hLOG,CHR(13)+CHR(10),2)
    FWRITE(hLOG,CHR(13)+CHR(10),2)
    this.w_MESS1 = ah_MsgFormat("-- AZIENDA: %1",UPPER(g_RAGAZI))
    FWRITE(hLOG,this.w_MESS1,LEN(this.w_MESS1))
    FWRITE(hLOG,CHR(13)+CHR(10),2)
    this.w_MESS1 = ah_MsgFormat("-- DATA EXPORT: %1",dtoc(date()))
    FWRITE(hLOG,this.w_MESS1,LEN(this.w_MESS1))
    FWRITE(hLOG,CHR(13)+CHR(10),2)
    * --- Stringa di testo
    this.w_MESS1 = ah_MsgFormat("-- IMPORT DI MOVIMENTI CONTABILI")
    FWRITE(hLOG,this.w_MESS1,LEN(this.w_MESS1))
    FWRITE(hLOG,CHR(13)+CHR(10),2)
    FWRITE(hLOG,CHR(13)+CHR(10),2)
    FWRITE(hLOG,REPL("-",80),80)
    FWRITE(hLOG,CHR(13)+CHR(10),2)
    FWRITE(hLOG,CHR(13)+CHR(10),2)
    * --- Zap del temporaneo di primanota
    * --- Delete from STU_TPNT
    i_nConn=i_TableProp[this.STU_TPNT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_TPNT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Mi posiziono all'inizio dei record che identificano i movimenti contabili
    this.w_COMODO = FREAD(this.oParentObject.hFile,9)
    FSEEK(this.oParentObject.hFile,-9,1)
    do while ((this.w_COMODO<>"FMOCONTAB") .and. (.not. FEOF(this.oParentObject.hFile)))
      * --- Skippo un record
      FSEEK(this.oParentObject.hFile,this.oParentObject.LUNRECORD,1)
      * --- Rileggo il codice
      this.w_COMODO = FREAD(this.oParentObject.hFile,9)
      FSEEK(this.oParentObject.hFile,-9,1)
    enddo
    if this.w_COMODO<>"FMOCONTAB"
      this.oParentObject.w_STATOMOVCON = 0
      this.w_MESS1 = ah_MsgFormat("-- [Fmocontab] non trovato nel file di import")
      FWRITE(hLOG,this.w_MESS1,LEN(this.w_MESS1))
      FWRITE(hLOG,CHR(13)+CHR(10),2)
      FWRITE(hLOG,CHR(13)+CHR(10),2)
      * --- Chiusura file di log
      this.w_CHIUSURA = FCLOSE(hLOG)
      return
    endif
    * --- Skippo la testata
    FSEEK(this.oParentObject.hFile,this.oParentObject.LUNRECORD,1)
    * --- Inizio lettura dei movimenti contabili
    this.w_COMODO = FREAD(this.oParentObject.hFile,9)
    FSEEK(this.oParentObject.hFile,-9,1)
    * --- Ciclo sul file Acii per il recupero dei movimenti contabili
    do while this.w_COMODO<>"EMOCONTAB"
      this.w_NUMPRG = this.w_NUMPRG+1
      * --- Finestra di debug
      ah_Msg("Import movimento contabile %1",.T.,.F.,.F.,ALLTRIM(this.w_PNSERIAL))
      * --- Leggo il primo campo
      this.w_COMODO = FREAD(this.oParentObject.hFile,3)
      if not (this.w_COMODO="D50" or this.w_COMODO="D51")
        * --- Ritorno in quanto c'� un errore
        this.oParentObject.w_STATOMOVCON = 0
        this.w_MESS1 = ah_MsgFormat("[D50 - D51] non trovato nel file di import")
        FWRITE(hLOG,this.w_MESS1,LEN(this.w_MESS1))
        FWRITE(hLOG,CHR(13)+CHR(10),2)
        FWRITE(hLOG,CHR(13)+CHR(10),2)
        * --- Chiusura file di log
        this.w_CHIUSURA = FCLOSE(hLOG)
        return
      endif
      if this.w_COMODO="D51"
        if NOT this.w_RIGDES
          this.w_RIGDES = .T.
          * --- Se � la prima riga descrittiva
          * --- Descrizione Aggiuntiva
          this.w_DESRIG = FREAD(this.oParentObject.hFile,29)
          this.w_DESRIG = LEFT(this.w_DESRIG,21)
          this.w_NUMPRG = this.w_NUMPRG-1
          if this.w_NUMRIG=2
            * --- Recupero la descrizione di riga
            * --- Read from STU_TPNT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.STU_TPNT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.STU_TPNT_idx,2],.t.,this.STU_TPNT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "LMDESRIG"+;
                " from "+i_cTable+" STU_TPNT where ";
                    +"LMNUMPRG = "+cp_ToStrODBC(this.w_NUMPRG);
                    +" and LMNUMRIG = "+cp_ToStrODBC(this.w_NUMRIG);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                LMDESRIG;
                from (i_cTable) where;
                    LMNUMPRG = this.w_NUMPRG;
                    and LMNUMRIG = this.w_NUMRIG;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESCRI = NVL(cp_ToDate(_read_.LMDESRIG),cp_NullValue(_read_.LMDESRIG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_DESCRI = LEFT(this.w_DESCRI,29)
            this.w_DESCRI = this.w_DESCRI+this.w_DESRIG
            * --- Aggiorno la descrizione di riga
            * --- Write into STU_TPNT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.STU_TPNT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.STU_TPNT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_TPNT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"LMDESRIG ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'STU_TPNT','LMDESRIG');
                  +i_ccchkf ;
              +" where ";
                  +"LMNUMPRG = "+cp_ToStrODBC(this.w_NUMPRG);
                  +" and LMNUMRIG = "+cp_ToStrODBC(this.w_NUMRIG);
                     )
            else
              update (i_cTable) set;
                  LMDESRIG = this.w_DESCRI;
                  &i_ccchkf. ;
               where;
                  LMNUMPRG = this.w_NUMPRG;
                  and LMNUMRIG = this.w_NUMRIG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          this.w_NUMRIG = this.w_NUMRIG-1
          * --- Recupero la descrizione di riga
          * --- Read from STU_TPNT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.STU_TPNT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_TPNT_idx,2],.t.,this.STU_TPNT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LMDESRIG"+;
              " from "+i_cTable+" STU_TPNT where ";
                  +"LMNUMPRG = "+cp_ToStrODBC(this.w_NUMPRG);
                  +" and LMNUMRIG = "+cp_ToStrODBC(this.w_NUMRIG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LMDESRIG;
              from (i_cTable) where;
                  LMNUMPRG = this.w_NUMPRG;
                  and LMNUMRIG = this.w_NUMRIG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DESCRI = NVL(cp_ToDate(_read_.LMDESRIG),cp_NullValue(_read_.LMDESRIG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_DESCRI = LEFT(this.w_DESCRI,29)
          this.w_DESCRI = this.w_DESCRI+this.w_DESRIG
          * --- Aggiorno la descrizione di riga
          * --- Write into STU_TPNT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.STU_TPNT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_TPNT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_TPNT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LMDESRIG ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'STU_TPNT','LMDESRIG');
                +i_ccchkf ;
            +" where ";
                +"LMNUMPRG = "+cp_ToStrODBC(this.w_NUMPRG);
                +" and LMNUMRIG = "+cp_ToStrODBC(this.w_NUMRIG);
                   )
          else
            update (i_cTable) set;
                LMDESRIG = this.w_DESCRI;
                &i_ccchkf. ;
             where;
                LMNUMPRG = this.w_NUMPRG;
                and LMNUMRIG = this.w_NUMRIG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Se non � la prima riga descrittiva
          * --- Lettura per compatibilit� con il ramo then
          this.w_DESRIG = FREAD(this.oParentObject.hFile,29)
        endif
        * --- Salto al record successivo
        FSEEK(this.oParentObject.hFile,-32,1)
        FSEEK(this.oParentObject.hFile,this.oParentObject.LUNRECORD,1)
      else
        this.w_RIGDES = .F.
        * --- Recupero se la registrazione corrente � 1 -> 1 o 1 -> Molti
        FSEEK(this.oParentObject.hFile,58,1)
        this.w_SOTDAR = FREAD(this.oParentObject.hFile,6)
        FSEEK(this.oParentObject.hFile,-64,1)
        * --- Leggo Sottoconto Avere
        FSEEK(this.oParentObject.hFile,68,1)
        this.w_SOTAVE = FREAD(this.oParentObject.hFile,6)
        FSEEK(this.oParentObject.hFile,-74,1)
        if this.w_SOTDAR<>"000000" and this.w_SOTAVE<>"000000"
          this.w_TIPO = "1to1"
        else
          this.w_TIPO = "1toM"
        endif
        if this.w_TIPO = "1to1"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      * --- Leggo il prossimo codice
      this.w_COMODO = FREAD(this.oParentObject.hFile,9)
      FSEEK(this.oParentObject.hFile,-9,1)
    enddo
    * --- Testo errore
    if this.oParentObject.w_STATOMOVCON=0
      this.w_MESS1 = ah_MsgFormat("Import Movimenti Contabili terminato con errore")
      FWRITE(hLOG,this.w_MESS1,LEN(this.w_MESS1))
      FWRITE(hLOG,CHR(13)+CHR(10),2)
      FWRITE(hLOG,CHR(13)+CHR(10),2)
      * --- Chiusura file di log
      this.w_CHIUSURA = FCLOSE(hLOG)
      return
    endif
    * --- Passo i dati dal temporaneo alla primanota e aggiorno i saldi contabili
    * --- begin transaction
    cp_BeginTrs()
    * --- Try
    local bErr_03985E88
    bErr_03985E88=bTrsErr
    this.Try_03985E88()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_MESS1 = ah_MsgFormat("Import movimenti contabili terminato con errore durante la trascrizione nelle tabelle di Ad Hoc %1",i_ERRMSG)
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      * --- Operazione conclusa con fallimento
      this.oParentObject.w_STATOMOVCON = 0
    endif
    bTrsErr=bTrsErr or bErr_03985E88
    * --- End
    * --- Setto progressivo di primanota
    * --- Scrittura su file log
    FWRITE(hLOG,this.w_MESS1,LEN(this.w_MESS1))
    FWRITE(hLOG,CHR(13)+CHR(10),2)
    FWRITE(hLOG,CHR(13)+CHR(10),2)
    * --- Chiusura file di log
    this.w_CHIUSURA = FCLOSE(hLOG)
  endproc
  proc Try_03985E88()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Select from STU_TPNT
    i_nConn=i_TableProp[this.STU_TPNT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_TPNT_idx,2],.t.,this.STU_TPNT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" STU_TPNT ";
          +" order by LMNUMPRG";
           ,"_Curs_STU_TPNT")
    else
      select * from (i_cTable);
       order by LMNUMPRG;
        into cursor _Curs_STU_TPNT
    endif
    if used('_Curs_STU_TPNT')
      select _Curs_STU_TPNT
      locate for 1=1
      do while not(eof())
      if _Curs_STU_TPNT.LMNUMRIG=1
        * --- Recupero primo progressivo libero di primanota
        this.w_PNCODESE = str(year(_Curs_STU_TPNT.LMDATREG),4)
        this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(CP_TODATE(_Curs_STU_TPNT.LMDATREG)), SPACE(8))
        this.w_PNSERIAL = SPACE(10)
        this.w_PNNUMRER = 0
        cp_NextTableProg(this,i_Conn,"SEPNT","i_CODAZI,w_PNSERIAL")
        cp_NextTableProg(this,i_Conn,"PRPNT","i_CODAZI,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
        this.w_PNDATREG = CP_TODATE(_Curs_STU_TPNT.LMDATREG)
        if Not Empty(CHKCONS("P",this.w_PNDATREG,"B","N"))
          * --- Raise
          i_Error=SUBSTR(CHKCONS("P",this.w_PNDATREG,"B","N"),11)+"!"
          return
        endif
        this.w_PNCODCAU = _Curs_STU_TPNT.LMCODCAU
        this.w_PNCOMPET = _Curs_STU_TPNT.LMCOMPET
        this.w_PNCODVAL = _Curs_STU_TPNT.LMCODVAL
        this.w_PNCAOVAL = _Curs_STU_TPNT.LMCAOVAL
        this.w_PNTIPREG = _Curs_STU_TPNT.LMTIPREG
        this.w_PNTIPDOC = _Curs_STU_TPNT.LMTIPDOC
        this.w_FLPROV = "N"
        * --- Lettura Valuta Nazionale
        * --- Read from ESERCIZI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ESERCIZI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ESVALNAZ"+;
            " from "+i_cTable+" ESERCIZI where ";
                +"ESCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                +" and ESCODESE = "+cp_ToStrODBC(this.w_PNCOMPET);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ESVALNAZ;
            from (i_cTable) where;
                ESCODAZI = this.w_CODAZI;
                and ESCODESE = this.w_PNCOMPET;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PNVALNAZ = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Insert into PNT_MAST
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PNSERIAL"+",PNNUMRER"+",PNDATREG"+",PNCODCAU"+",PNCOMPET"+",PNCODVAL"+",PNCAOVAL"+",PNFLPROV"+",PNCOMIVA"+",PNVALNAZ"+",PNCODESE"+",PNTIPREG"+",PNTIPDOC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCOMPET');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODVAL),'PNT_MAST','PNCODVAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAOVAL),'PNT_MAST','PNCAOVAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_FLPROV),'PNT_MAST','PNFLPROV');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNCOMIVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'PNT_MAST','PNTIPREG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPDOC),'PNT_MAST','PNTIPDOC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'PNNUMRER',this.w_PNNUMRER,'PNDATREG',this.w_PNDATREG,'PNCODCAU',this.w_PNCODCAU,'PNCOMPET',this.w_PNCOMPET,'PNCODVAL',this.w_PNCODVAL,'PNCAOVAL',this.w_PNCAOVAL,'PNFLPROV',this.w_FLPROV,'PNCOMIVA',this.w_PNDATREG,'PNVALNAZ',this.w_PNVALNAZ,'PNCODESE',this.w_PNCODESE,'PNTIPREG',this.w_PNTIPREG)
          insert into (i_cTable) (PNSERIAL,PNNUMRER,PNDATREG,PNCODCAU,PNCOMPET,PNCODVAL,PNCAOVAL,PNFLPROV,PNCOMIVA,PNVALNAZ,PNCODESE,PNTIPREG,PNTIPDOC &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.w_PNNUMRER;
               ,this.w_PNDATREG;
               ,this.w_PNCODCAU;
               ,this.w_PNCOMPET;
               ,this.w_PNCODVAL;
               ,this.w_PNCAOVAL;
               ,this.w_FLPROV;
               ,this.w_PNDATREG;
               ,this.w_PNVALNAZ;
               ,this.w_PNCODESE;
               ,this.w_PNTIPREG;
               ,this.w_PNTIPDOC;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='Errore nella scrittura della Primanota (Master)'
          return
        endif
      endif
      this.w_CPROWNUM = _Curs_STU_TPNT.LMNUMRIG
      this.w_CPROWORD = 10*this.w_CPROWNUM
      this.w_PNTIPCON = _Curs_STU_TPNT.LMTIPCON
      this.w_PNCODCON = _Curs_STU_TPNT.LMCODCON
      this.w_PNIMPDAR = _Curs_STU_TPNT.LMIMPDAR
      this.w_PNIMPAVE = _Curs_STU_TPNT.LMIMPAVE
      this.w_PNDESRIG = _Curs_STU_TPNT.LMDESRIG
      this.w_PNFLSALI = _Curs_STU_TPNT.LMFLSALI
      this.w_PNFLSALF = _Curs_STU_TPNT.LMFLSALF
      this.w_PNCAURIG = _Curs_STU_TPNT.LMCODCAU
      * --- Insert into PNT_DETT
      i_nConn=i_TableProp[this.PNT_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNDESRIG"+",PNFLSALI"+",PNFLSALF"+",PNFLPART"+",PNFLZERO"+",PNCAURIG"+",PNFLSALD"+",PNFLABAN"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALI),'PNT_DETT','PNFLSALI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALF),'PNT_DETT','PNFLSALF');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPART),'PNT_DETT','PNFLPART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLZERO),'PNT_DETT','PNFLZERO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNCAURIG),'PNT_DETT','PNCAURIG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLSALD),'PNT_DETT','PNFLSALD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLABAN),'PNT_DETT','PNFLABAN');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',this.w_PNTIPCON,'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNDESRIG',this.w_PNDESRIG,'PNFLSALI',this.w_PNFLSALI,'PNFLSALF',this.w_PNFLSALF,'PNFLPART',this.w_PNFLPART,'PNFLZERO',this.w_PNFLZERO)
        insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNDESRIG,PNFLSALI,PNFLSALF,PNFLPART,PNFLZERO,PNCAURIG,PNFLSALD,PNFLABAN &i_ccchkf. );
           values (;
             this.w_PNSERIAL;
             ,this.w_CPROWNUM;
             ,this.w_CPROWORD;
             ,this.w_PNTIPCON;
             ,this.w_PNCODCON;
             ,this.w_PNIMPDAR;
             ,this.w_PNIMPAVE;
             ,this.w_PNDESRIG;
             ,this.w_PNFLSALI;
             ,this.w_PNFLSALF;
             ,this.w_PNFLPART;
             ,this.w_PNFLZERO;
             ,this.w_PNCAURIG;
             ,this.w_PNFLSALD;
             ,this.w_PNFLABAN;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore nella scrittura della Primanota (Detail)'
        return
      endif
      * --- Aggiorno i saldi
      this.w_SLTIPCON = _Curs_STU_TPNT.LMTIPCON
      this.w_SLCODICE = _Curs_STU_TPNT.LMCODCON
      this.w_SLCODESE = _Curs_STU_TPNT.LMCOMPET
      this.w_SLDARPER = _Curs_STU_TPNT.LMIMPDAR
      this.w_SLAVEPER = _Curs_STU_TPNT.LMIMPAVE
      this.w_SLDARINI = IIF(_Curs_STU_TPNT.LMFLSALI="+",_Curs_STU_TPNT.LMIMPDAR,0)
      this.w_SLAVEINI = IIF(_Curs_STU_TPNT.LMFLSALI="+",_Curs_STU_TPNT.LMIMPAVE,0)
      this.w_SLDARFIN = IIF(_Curs_STU_TPNT.LMFLSALF="+",_Curs_STU_TPNT.LMIMPAVE,0)
      this.w_SLAVEFIN = IIF(_Curs_STU_TPNT.LMFLSALF="+",_Curs_STU_TPNT.LMIMPDAR,0)
      this.w_LMFLSALI = _Curs_STU_TPNT.LMFLSALI
      this.w_LMFLSALF = _Curs_STU_TPNT.LMFLSALF
      * --- Riscrive dati Periodo Piano dei Conti
      * --- Try
      local bErr_039B95C8
      bErr_039B95C8=bTrsErr
      this.Try_039B95C8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into SALDICON
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICON_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
          i_cOp3=cp_SetTrsOp(this.w_LMFLSALI,'SLDARINI','this.w_SLDARINI',this.w_SLDARINI,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_LMFLSALI,'SLAVEINI','this.w_SLAVEINI',this.w_SLAVEINI,'update',i_nConn)
          i_cOp5=cp_SetTrsOp(this.w_LMFLSALF,'SLDARFIN','this.w_SLDARFIN',this.w_SLDARFIN,'update',i_nConn)
          i_cOp6=cp_SetTrsOp(this.w_LMFLSALF,'SLAVEFIN','this.w_SLAVEFIN',this.w_SLAVEFIN,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_SLDARPER);
          +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_SLAVEPER);
          +",SLDARINI ="+cp_NullLink(i_cOp3,'SALDICON','SLDARINI');
          +",SLAVEINI ="+cp_NullLink(i_cOp4,'SALDICON','SLAVEINI');
          +",SLDARFIN ="+cp_NullLink(i_cOp5,'SALDICON','SLDARFIN');
          +",SLAVEFIN ="+cp_NullLink(i_cOp6,'SALDICON','SLAVEFIN');
              +i_ccchkf ;
          +" where ";
              +"SLTIPCON = "+cp_ToStrODBC(this.w_SLTIPCON);
              +" and SLCODICE = "+cp_ToStrODBC(this.w_SLCODICE);
              +" and SLCODESE = "+cp_ToStrODBC(this.w_SLCODESE);
                 )
        else
          update (i_cTable) set;
              SLDARPER = SLDARPER + this.w_SLDARPER;
              ,SLAVEPER = SLAVEPER + this.w_SLAVEPER;
              ,SLDARINI = &i_cOp3.;
              ,SLAVEINI = &i_cOp4.;
              ,SLDARFIN = &i_cOp5.;
              ,SLAVEFIN = &i_cOp6.;
              &i_ccchkf. ;
           where;
              SLTIPCON = this.w_SLTIPCON;
              and SLCODICE = this.w_SLCODICE;
              and SLCODESE = this.w_SLCODESE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Errore nella prima scrittura dei saldi contabili '
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_039B95C8
      * --- End
        select _Curs_STU_TPNT
        continue
      enddo
      use
    endif
    this.w_MESS1 = ah_MsgFormat("Import movimenti contabili terminato con successo")
    * --- commit
    cp_EndTrs(.t.)
    * --- Operazione conclusa con successo
    this.oParentObject.w_STATOMOVCON = 1
    return
  proc Try_039B95C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SLTIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SLCODICE),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SLCODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SLDARPER),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SLAVEPER),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SLDARINI),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SLAVEINI),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SLDARFIN),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SLAVEFIN),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_SLTIPCON,'SLCODICE',this.w_SLCODICE,'SLCODESE',this.w_SLCODESE,'SLDARPER',this.w_SLDARPER,'SLAVEPER',this.w_SLAVEPER,'SLDARINI',this.w_SLDARINI,'SLAVEINI',this.w_SLAVEINI,'SLDARFIN',this.w_SLDARFIN,'SLAVEFIN',this.w_SLAVEFIN)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_SLTIPCON;
           ,this.w_SLCODICE;
           ,this.w_SLCODESE;
           ,this.w_SLDARPER;
           ,this.w_SLAVEPER;
           ,this.w_SLDARINI;
           ,this.w_SLAVEINI;
           ,this.w_SLDARFIN;
           ,this.w_SLAVEFIN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore nel primo inserimento dei saldi contabili '
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Registrazione 1 a 1
    this.w_CLIAVE = ""
    this.w_CLIDAR = ""
    this.w_FORAVE = ""
    this.w_FORDAR = ""
    this.w_CONAVE = ""
    this.w_CONDAR = ""
    this.w_SOTAVE = ""
    this.w_SOTDAR = ""
    * --- Lettura dei campi del file
    this.w_DATREG = FREAD(this.oParentObject.hFile,8)
    this.w_APRCAU = FREAD(this.oParentObject.hFile,3)
    this.w_DESRIG = FREAD(this.oParentObject.hFile,29)
    this.w_IMPO = FREAD(this.oParentObject.hFile,13)
    this.w_NUMIMPO = VAL(this.w_IMPO)
    this.w_SEGNO = FREAD(this.oParentObject.hFile,1)
    this.w_CCDAR = FREAD(this.oParentObject.hFile,4)
    this.w_SOTDAR = FREAD(this.oParentObject.hFile,6)
    this.w_CCAVE = FREAD(this.oParentObject.hFile,4)
    this.w_SOTAVE = FREAD(this.oParentObject.hFile,6)
    this.w_TIPCDAR = FREAD(this.oParentObject.hFile,1)
    this.w_PIVADAR = FREAD(this.oParentObject.hFile,16)
    this.w_TIPCAVE = FREAD(this.oParentObject.hFile,1)
    this.w_PIVAAVE = FREAD(this.oParentObject.hFile,16)
    this.w_FLAGDIV = FREAD(this.oParentObject.hFile,1)
    this.w_CONTAB = FREAD(this.oParentObject.hFile,1)
    this.w_ESCAPE = FREAD(this.oParentObject.hFile,87)
    * --- Variabili relative alla causale Ad Hoc
    this.w_CCFLSALI = ""
    this.w_CCFLSALF = ""
    * --- Variabili relative al piano dei conti di ad hoc
    this.w_PCTIPSOTD = ""
    this.w_PCTIPSOTA = ""
    * --- Trasformazione da Studio ad Ad Hoc Mediante Tabella trascodifica
    * --- Associazione definita
    * --- Read from STU_PARA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.STU_PARA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PARA_idx,2],.t.,this.STU_PARA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LMASSOCI"+;
        " from "+i_cTable+" STU_PARA where ";
            +"LMCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LMASSOCI;
        from (i_cTable) where;
            LMCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ASSOCI = NVL(cp_ToDate(_read_.LMASSOCI),cp_NullValue(_read_.LMASSOCI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_HOCCAU = SPACE(5)
    * --- Read from STU_TRAS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.STU_TRAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LMHOCCAU"+;
        " from "+i_cTable+" STU_TRAS where ";
            +"LMCODICE = "+cp_ToStrODBC(this.w_ASSOCI);
            +" and LMAPRCAU = "+cp_ToStrODBC(this.w_APRCAU);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LMHOCCAU;
        from (i_cTable) where;
            LMCODICE = this.w_ASSOCI;
            and LMAPRCAU = this.w_APRCAU;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_HOCCAU = NVL(cp_ToDate(_read_.LMHOCCAU),cp_NullValue(_read_.LMHOCCAU))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if NOT EMPTY(this.w_HOCCAU)
      * --- Lettura dati relativi alla causale
      * --- Read from CAU_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAU_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCFLSALI,CCFLSALF,CCTIPREG,CCTIPDOC"+;
          " from "+i_cTable+" CAU_CONT where ";
              +"CCCODICE = "+cp_ToStrODBC(this.w_HOCCAU);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCFLSALI,CCFLSALF,CCTIPREG,CCTIPDOC;
          from (i_cTable) where;
              CCCODICE = this.w_HOCCAU;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CCFLSALI = NVL(cp_ToDate(_read_.CCFLSALI),cp_NullValue(_read_.CCFLSALI))
        this.w_CCFLSALF = NVL(cp_ToDate(_read_.CCFLSALF),cp_NullValue(_read_.CCFLSALF))
        this.w_TIPREG = NVL(cp_ToDate(_read_.CCTIPREG),cp_NullValue(_read_.CCTIPREG))
        this.w_TIPDOC = NVL(cp_ToDate(_read_.CCTIPDOC),cp_NullValue(_read_.CCTIPDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_CCFLSALI = IIF(this.w_CCFLSALI="S","+"," ")
      this.w_CCFLSALF = IIF(this.w_CCFLSALF="S","+"," ")
    else
      this.oParentObject.w_STATOMOVCON = 0
      this.w_MESS1 = ah_MsgFormat("Causale %1 non associata nella tabella trascodifica",this.w_APRCAU)
      FWRITE(hLOG,this.w_MESS1,LEN(this.w_MESS1))
      FWRITE(hLOG,CHR(13)+CHR(10),2)
      FWRITE(hLOG,CHR(13)+CHR(10),2)
    endif
    * --- Trasformazione Sottoconti
    * --- Sottoconto dare
    * --- Verifico associazione del sottoconto studio nei CONTI
    if this.w_SOTDAR<>"000000"
      this.w_FOUND = .F.
      this.w_CONTSTU = CHKCONSTU(this.w_SOTDAR)
      if ! empty(this.w_CONTSTU)
        this.w_FOUND = .T.
        * --- Leggo la tipologia del sottoconto
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANTIPCON,ANCODICE"+;
            " from "+i_cTable+" CONTI where ";
                +"ANCODSTU = "+cp_ToStrODBC(this.w_CONTSTU);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANTIPCON,ANCODICE;
            from (i_cTable) where;
                ANCODSTU = this.w_CONTSTU;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PCTIPSOTD = NVL(cp_ToDate(_read_.ANTIPCON),cp_NullValue(_read_.ANTIPCON))
          this.w_CONDAR = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- sottoconto dare non riconosciuto
      if not this.w_FOUND
        this.oParentObject.w_STATOMOVCON = 0
        this.w_MESS1 = ah_MsgFormat("Sottoconto %1 non associato in Ad Hoc",this.w_SOTDAR)
        FWRITE(hLOG,this.w_MESS1,LEN(this.w_MESS1))
        FWRITE(hLOG,CHR(13)+CHR(10),2)
        FWRITE(hLOG,CHR(13)+CHR(10),2)
      endif
    endif
    if this.w_SOTAVE<>"000000"
      * --- Sottoconto avere
      this.w_FOUND = .F.
      this.w_CONTSTU = CHKCONSTU(this.w_SOTAVE)
      if NOT EMPTY(this.w_CONTSTU)
        this.w_FOUND = .T.
        * --- Leggo la tipologia del sottoconto
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANTIPCON,ANCODICE"+;
            " from "+i_cTable+" CONTI where ";
                +"ANCODSTU = "+cp_ToStrODBC(this.w_CONTSTU);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANTIPCON,ANCODICE;
            from (i_cTable) where;
                ANCODSTU = this.w_CONTSTU;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PCTIPSOTA = NVL(cp_ToDate(_read_.ANTIPCON),cp_NullValue(_read_.ANTIPCON))
          this.w_CONAVE = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- sottoconto avere non riconosciuto
      if not this.w_FOUND
        this.oParentObject.w_STATOMOVCON = 0
        this.w_MESS1 = ah_MsgFormat("Sottoconto %1 non associato in Ad Hoc",this.w_SOTAVE)
        FWRITE(hLOG,this.w_MESS1,LEN(this.w_MESS1))
        FWRITE(hLOG,CHR(13)+CHR(10),2)
        FWRITE(hLOG,CHR(13)+CHR(10),2)
      endif
    endif
    * --- Valuta in Euro
    this.w_CODVAL = g_PERVAL
    this.w_CAOVAL = GETCAM(g_PERVAL, i_DATSYS)
    * --- In caso di movimento in Euro l'importo viene considerato con 2 decimali
    this.w_NUMIMPO = this.w_NUMIMPO/100
    * --- Scrittura in Temporaneo Primanota
    this.w_COMPET = left(this.w_DATREG,4)
    this.w_DATREG = date(val (this.w_COMPET),val (substr(this.w_DATREG,5,2)) ,val (right(this.w_DATREG,2)) )
    * --- Riga in Dare
    this.w_NUMRIG = 1
    this.w_TIPSOT = IIF(not empty(this.w_CLIDAR),"C",IIF(not empty(this.w_FORDAR),"F","B"))
    this.w_LMIMPDAR = 0
    this.w_LMIMPAVE = 0
    this.w_LMFLSALI = IIF(this.w_CCFLSALI="S","+"," ")
    this.w_LMFLSALF = IIF(this.w_CCFLSALF="S","+"," ")
    * --- Insert into STU_TPNT
    i_nConn=i_TableProp[this.STU_TPNT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_TPNT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_TPNT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMNUMPRG"+",LMDATREG"+",LMCODCAU"+",LMCOMPET"+",LMCODVAL"+",LMCAOVAL"+",LMTIPCON"+",LMCODCON"+",LMIMPDAR"+",LMIMPAVE"+",LMDESRIG"+",LMFLSALI"+",LMFLSALF"+",LMNUMRIG"+",LMTIPREG"+",LMTIPDOC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_NUMPRG),'STU_TPNT','LMNUMPRG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATREG),'STU_TPNT','LMDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_HOCCAU),'STU_TPNT','LMCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMPET),'STU_TPNT','LMCOMPET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAL),'STU_TPNT','LMCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAOVAL),'STU_TPNT','LMCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PCTIPSOTD),'STU_TPNT','LMTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CONDAR),'STU_TPNT','LMCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMIMPO),'STU_TPNT','LMIMPDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMIMPAVE),'STU_TPNT','LMIMPAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESRIG),'STU_TPNT','LMDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMFLSALI),'STU_TPNT','LMFLSALI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMFLSALF),'STU_TPNT','LMFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIG),'STU_TPNT','LMNUMRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPREG),'STU_TPNT','LMTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPDOC),'STU_TPNT','LMTIPDOC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMNUMPRG',this.w_NUMPRG,'LMDATREG',this.w_DATREG,'LMCODCAU',this.w_HOCCAU,'LMCOMPET',this.w_COMPET,'LMCODVAL',this.w_CODVAL,'LMCAOVAL',this.w_CAOVAL,'LMTIPCON',this.w_PCTIPSOTD,'LMCODCON',this.w_CONDAR,'LMIMPDAR',this.w_NUMIMPO,'LMIMPAVE',this.w_LMIMPAVE,'LMDESRIG',this.w_DESRIG,'LMFLSALI',this.w_LMFLSALI)
      insert into (i_cTable) (LMNUMPRG,LMDATREG,LMCODCAU,LMCOMPET,LMCODVAL,LMCAOVAL,LMTIPCON,LMCODCON,LMIMPDAR,LMIMPAVE,LMDESRIG,LMFLSALI,LMFLSALF,LMNUMRIG,LMTIPREG,LMTIPDOC &i_ccchkf. );
         values (;
           this.w_NUMPRG;
           ,this.w_DATREG;
           ,this.w_HOCCAU;
           ,this.w_COMPET;
           ,this.w_CODVAL;
           ,this.w_CAOVAL;
           ,this.w_PCTIPSOTD;
           ,this.w_CONDAR;
           ,this.w_NUMIMPO;
           ,this.w_LMIMPAVE;
           ,this.w_DESRIG;
           ,this.w_LMFLSALI;
           ,this.w_LMFLSALF;
           ,this.w_NUMRIG;
           ,this.w_TIPREG;
           ,this.w_TIPDOC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Fallito inserimento riga dare nel temporaneo'
      return
    endif
    * --- Riga in Avere
    this.w_NUMRIG = 2
    * --- Insert into STU_TPNT
    i_nConn=i_TableProp[this.STU_TPNT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_TPNT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_TPNT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMNUMPRG"+",LMDATREG"+",LMCODCAU"+",LMCOMPET"+",LMCODVAL"+",LMCAOVAL"+",LMTIPCON"+",LMCODCON"+",LMIMPDAR"+",LMIMPAVE"+",LMDESRIG"+",LMFLSALI"+",LMFLSALF"+",LMNUMRIG"+",LMTIPREG"+",LMTIPDOC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_NUMPRG),'STU_TPNT','LMNUMPRG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DATREG),'STU_TPNT','LMDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_HOCCAU),'STU_TPNT','LMCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COMPET),'STU_TPNT','LMCOMPET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAL),'STU_TPNT','LMCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAOVAL),'STU_TPNT','LMCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PCTIPSOTA),'STU_TPNT','LMTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CONAVE),'STU_TPNT','LMCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMIMPDAR),'STU_TPNT','LMIMPDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMIMPO),'STU_TPNT','LMIMPAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESRIG),'STU_TPNT','LMDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMFLSALI),'STU_TPNT','LMFLSALI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMFLSALF),'STU_TPNT','LMFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIG),'STU_TPNT','LMNUMRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPREG),'STU_TPNT','LMTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPDOC),'STU_TPNT','LMTIPDOC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMNUMPRG',this.w_NUMPRG,'LMDATREG',this.w_DATREG,'LMCODCAU',this.w_HOCCAU,'LMCOMPET',this.w_COMPET,'LMCODVAL',this.w_CODVAL,'LMCAOVAL',this.w_CAOVAL,'LMTIPCON',this.w_PCTIPSOTA,'LMCODCON',this.w_CONAVE,'LMIMPDAR',this.w_LMIMPDAR,'LMIMPAVE',this.w_NUMIMPO,'LMDESRIG',this.w_DESRIG,'LMFLSALI',this.w_LMFLSALI)
      insert into (i_cTable) (LMNUMPRG,LMDATREG,LMCODCAU,LMCOMPET,LMCODVAL,LMCAOVAL,LMTIPCON,LMCODCON,LMIMPDAR,LMIMPAVE,LMDESRIG,LMFLSALI,LMFLSALF,LMNUMRIG,LMTIPREG,LMTIPDOC &i_ccchkf. );
         values (;
           this.w_NUMPRG;
           ,this.w_DATREG;
           ,this.w_HOCCAU;
           ,this.w_COMPET;
           ,this.w_CODVAL;
           ,this.w_CAOVAL;
           ,this.w_PCTIPSOTA;
           ,this.w_CONAVE;
           ,this.w_LMIMPDAR;
           ,this.w_NUMIMPO;
           ,this.w_DESRIG;
           ,this.w_LMFLSALI;
           ,this.w_LMFLSALF;
           ,this.w_NUMRIG;
           ,this.w_TIPREG;
           ,this.w_TIPDOC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Fallito inserimento riga dare nel temporaneo'
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Registrazione 1 a molti
    * --- Flag di fine registrazione
    this.w_FLAGDIV = " "
    * --- Numero riga registrazione
    this.w_NUMRIG = 1
    * --- Decremento di 3 il puntatore al file
    FSEEK(this.oParentObject.hFile,-3,1)
    do while this.w_FLAGDIV<>"X"
      this.w_CLIAVE = ""
      this.w_CLIDAR = ""
      this.w_FORAVE = ""
      this.w_FORDAR = ""
      this.w_CONAVE = ""
      this.w_CONDAR = ""
      this.w_SOTAVE = ""
      this.w_SOTDAR = ""
      * --- Lettura dei campi del file
      this.w_ESCAPE = FREAD(this.oParentObject.hFile,3)
      do case
        case this.w_ESCAPE="D51"
          if NOT this.w_RIGDES
            this.w_RIGDES = .T.
            * --- Se � la prima riga descrittiva
            * --- Descrizione Aggiuntiva
            this.w_DESRIG = FREAD(this.oParentObject.hFile,29)
            this.w_DESRIG = LEFT(this.w_DESRIG,21)
            this.w_NUMRIG = this.w_NUMRIG-1
            * --- Recupero la descrizione di riga
            * --- Read from STU_TPNT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.STU_TPNT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.STU_TPNT_idx,2],.t.,this.STU_TPNT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "LMDESRIG"+;
                " from "+i_cTable+" STU_TPNT where ";
                    +"LMNUMPRG = "+cp_ToStrODBC(this.w_NUMPRG);
                    +" and LMNUMRIG = "+cp_ToStrODBC(this.w_NUMRIG);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                LMDESRIG;
                from (i_cTable) where;
                    LMNUMPRG = this.w_NUMPRG;
                    and LMNUMRIG = this.w_NUMRIG;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESCRI = NVL(cp_ToDate(_read_.LMDESRIG),cp_NullValue(_read_.LMDESRIG))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_DESCRI = LEFT(this.w_DESCRI,29)
            this.w_DESCRI = this.w_DESCRI+this.w_DESRIG
            * --- Aggiorno la descrizione di riga
            * --- Write into STU_TPNT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.STU_TPNT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.STU_TPNT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_TPNT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"LMDESRIG ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'STU_TPNT','LMDESRIG');
                  +i_ccchkf ;
              +" where ";
                  +"LMNUMPRG = "+cp_ToStrODBC(this.w_NUMPRG);
                  +" and LMNUMRIG = "+cp_ToStrODBC(this.w_NUMRIG);
                     )
            else
              update (i_cTable) set;
                  LMDESRIG = this.w_DESCRI;
                  &i_ccchkf. ;
               where;
                  LMNUMPRG = this.w_NUMPRG;
                  and LMNUMRIG = this.w_NUMRIG;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            this.w_NUMRIG = this.w_NUMRIG+1
          else
            * --- Se non � la prima riga descrittiva
            * --- Lettura per compatibilit� con il ramo then
            this.w_DESRIG = FREAD(this.oParentObject.hFile,29)
          endif
          * --- Salto al record successivo
          FSEEK(this.oParentObject.hFile,-32,1)
          FSEEK(this.oParentObject.hFile,this.oParentObject.LUNRECORD,1)
        case this.w_ESCAPE="D50"
          this.w_RIGDES = .F.
          this.w_DATREG = FREAD(this.oParentObject.hFile,8)
          this.w_APRCAU = FREAD(this.oParentObject.hFile,3)
          this.w_DESRIG = FREAD(this.oParentObject.hFile,29)
          this.w_IMPO = FREAD(this.oParentObject.hFile,13)
          this.w_NUMIMPO = VAL(this.w_IMPO)
          this.w_SEGNO = FREAD(this.oParentObject.hFile,1)
          this.w_CCDAR = FREAD(this.oParentObject.hFile,4)
          this.w_SOTDAR = FREAD(this.oParentObject.hFile,6)
          this.w_CCAVE = FREAD(this.oParentObject.hFile,4)
          this.w_SOTAVE = FREAD(this.oParentObject.hFile,6)
          this.w_TIPCDAR = FREAD(this.oParentObject.hFile,1)
          this.w_PIVADAR = FREAD(this.oParentObject.hFile,16)
          this.w_TIPCAVE = FREAD(this.oParentObject.hFile,1)
          this.w_PIVAAVE = FREAD(this.oParentObject.hFile,16)
          this.w_FLAGDIV = FREAD(this.oParentObject.hFile,1)
          this.w_CONTAB = FREAD(this.oParentObject.hFile,1)
          this.w_ESCAPE = FREAD(this.oParentObject.hFile,87)
          * --- Recupero primo progressivo libero di primanota
          this.w_PNCODESE = left(this.w_DATREG,4)
          this.w_PNPRG = IIF(g_UNIUTE $ "GE", this.w_DATREG, SPACE(8))
          cp_NextTableProg(this,i_Conn,"PRPNT","w_CODAZI,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
          * --- Variabili relative alla causale Ad Hoc
          this.w_CCFLSALI = ""
          this.w_CCFLSALF = ""
          * --- Variabili relative al piano dei conti di ad hoc
          this.w_PCTIPSOT = ""
          * --- Trasformazione da Studio ad Ad Hoc Mediante Tabella trascodifica
          * --- Associazione definita
          * --- Read from STU_PARA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.STU_PARA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_PARA_idx,2],.t.,this.STU_PARA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LMASSOCI"+;
              " from "+i_cTable+" STU_PARA where ";
                  +"LMCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LMASSOCI;
              from (i_cTable) where;
                  LMCODAZI = this.w_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ASSOCI = NVL(cp_ToDate(_read_.LMASSOCI),cp_NullValue(_read_.LMASSOCI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_HOCCAU = SPACE(5)
          * --- Read from STU_TRAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.STU_TRAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LMHOCCAU"+;
              " from "+i_cTable+" STU_TRAS where ";
                  +"LMCODICE = "+cp_ToStrODBC(this.w_ASSOCI);
                  +" and LMAPRCAU = "+cp_ToStrODBC(this.w_APRCAU);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LMHOCCAU;
              from (i_cTable) where;
                  LMCODICE = this.w_ASSOCI;
                  and LMAPRCAU = this.w_APRCAU;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_HOCCAU = NVL(cp_ToDate(_read_.LMHOCCAU),cp_NullValue(_read_.LMHOCCAU))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT EMPTY(this.w_HOCCAU)
            * --- Lettura dati relativi alla causale
            * --- Read from CAU_CONT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAU_CONT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CCFLSALI,CCFLSALF,CCTIPREG,CCTIPDOC"+;
                " from "+i_cTable+" CAU_CONT where ";
                    +"CCCODICE = "+cp_ToStrODBC(this.w_HOCCAU);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CCFLSALI,CCFLSALF,CCTIPREG,CCTIPDOC;
                from (i_cTable) where;
                    CCCODICE = this.w_HOCCAU;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CCFLSALI = NVL(cp_ToDate(_read_.CCFLSALI),cp_NullValue(_read_.CCFLSALI))
              this.w_CCFLSALF = NVL(cp_ToDate(_read_.CCFLSALF),cp_NullValue(_read_.CCFLSALF))
              this.w_TIPREG = NVL(cp_ToDate(_read_.CCTIPREG),cp_NullValue(_read_.CCTIPREG))
              this.w_TIPDOC = NVL(cp_ToDate(_read_.CCTIPDOC),cp_NullValue(_read_.CCTIPDOC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_CCFLSALI = IIF(this.w_CCFLSALI="S","+"," ")
            this.w_CCFLSALF = IIF(this.w_CCFLSALF="S","+"," ")
          else
            this.oParentObject.w_STATOMOVCON = 0
            this.w_MESS1 = ah_MsgFormat("Causale %1 non associata nella tabella trascodifica",this.w_APRCAU)
            FWRITE(hLOG,this.w_MESS1,LEN(this.w_MESS1))
            FWRITE(hLOG,CHR(13)+CHR(10),2)
            FWRITE(hLOG,CHR(13)+CHR(10),2)
          endif
          * --- Trasformazione Sottoconti
          * --- Sottoconto dare
          if this.w_SOTDAR<>"000000"
            * --- Cerco se cliente
            this.w_FOUND = .F.
            this.w_CONTSTU = CHKCONSTU(this.w_SOTDAR)
            if NOT EMPTY(this.w_CONTSTU)
              this.w_FOUND = .T.
              * --- Leggo la tipologia del sottoconto
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANTIPCON,ANCODICE"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANCODSTU = "+cp_ToStrODBC(this.w_CONTSTU);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANTIPCON,ANCODICE;
                  from (i_cTable) where;
                      ANCODSTU = this.w_CONTSTU;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_PCTIPSOT = NVL(cp_ToDate(_read_.ANTIPCON),cp_NullValue(_read_.ANTIPCON))
                this.w_CONDAR = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            * --- sottoconto dare non riconosciuto
            if not this.w_FOUND
              this.oParentObject.w_STATOMOVCON = 0
              this.w_MESS1 = ah_MsgFormat("Sottoconto %1 non associato in Ad Hoc",this.w_SOTDAR)
              FWRITE(hLOG,this.w_MESS1,LEN(this.w_MESS1))
              FWRITE(hLOG,CHR(13)+CHR(10),2)
              FWRITE(hLOG,CHR(13)+CHR(10),2)
            endif
            * --- Flag di Dare
            this.w_FLDAVE = "D"
          endif
          * --- Sottoconto avere
          if this.w_SOTAVE<>"000000"
            * --- Cerco se cliente
            this.w_FOUND = .F.
            this.w_CONTSTU = CHKCONSTU(this.w_SOTAVE)
            if NOT EMPTY(this.w_CONTSTU)
              this.w_FOUND = .T.
              * --- Tipologia sottoconto
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANTIPCON,ANCODICE"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANCODSTU = "+cp_ToStrODBC(this.w_CONTSTU);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANTIPCON,ANCODICE;
                  from (i_cTable) where;
                      ANCODSTU = this.w_CONTSTU;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_PCTIPSOT = NVL(cp_ToDate(_read_.ANTIPCON),cp_NullValue(_read_.ANTIPCON))
                this.w_CONAVE = NVL(cp_ToDate(_read_.ANCODICE),cp_NullValue(_read_.ANCODICE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            * --- sottoconto avere non riconosciuto
            if not this.w_FOUND
              this.oParentObject.w_STATOMOVCON = 0
              this.w_MESS1 = ah_MsgFormat("Sottoconto %1 non associato in Ad Hoc",this.w_SOTAVE)
              FWRITE(hLOG,this.w_MESS1,LEN(this.w_MESS1))
              FWRITE(hLOG,CHR(13)+CHR(10),2)
              FWRITE(hLOG,CHR(13)+CHR(10),2)
            endif
            * --- Flag di Avere
            this.w_FLDAVE = "A"
          endif
          * --- Valuta in Euro
          this.w_CODVAL = g_PERVAL
          this.w_CAOVAL = GETCAM(g_PERVAL,i_DATSYS)
          * --- In caso di movimento in Euro l'importo viene considerato con 2 decimali
          this.w_NUMIMPO = this.w_NUMIMPO/100
          this.w_LMCODCON = IIF(this.w_FLDAVE="D",this.w_CONDAR,this.w_CONAVE)
          this.w_LMIMPDAR = IIF(this.w_FLDAVE="D",this.w_NUMIMPO,0)
          this.w_LMIMPAVE = IIF(this.w_FLDAVE="A",this.w_NUMIMPO,0)
          this.w_LMFLSALI = IIF(this.w_CCFLSALI="S","+"," ")
          this.w_LMFLSALF = IIF(this.w_CCFLSALF="S","+"," ")
          * --- Scrittura in Temporaneo Primanota
          this.w_COMPET = left(this.w_DATREG,4)
          this.w_DATREG = date(val (this.w_COMPET),val (substr(this.w_DATREG,5,2)) ,val (right(this.w_DATREG,2)) )
          * --- Insert into STU_TPNT
          i_nConn=i_TableProp[this.STU_TPNT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_TPNT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_TPNT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"LMNUMPRG"+",LMDATREG"+",LMCODCAU"+",LMCOMPET"+",LMCODVAL"+",LMCAOVAL"+",LMTIPCON"+",LMCODCON"+",LMIMPDAR"+",LMIMPAVE"+",LMDESRIG"+",LMFLSALI"+",LMFLSALF"+",LMNUMRIG"+",LMTIPREG"+",LMTIPDOC"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_NUMPRG),'STU_TPNT','LMNUMPRG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DATREG),'STU_TPNT','LMDATREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_HOCCAU),'STU_TPNT','LMCODCAU');
            +","+cp_NullLink(cp_ToStrODBC(this.w_COMPET),'STU_TPNT','LMCOMPET');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAL),'STU_TPNT','LMCODVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CAOVAL),'STU_TPNT','LMCAOVAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PCTIPSOT),'STU_TPNT','LMTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LMCODCON),'STU_TPNT','LMCODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LMIMPDAR),'STU_TPNT','LMIMPDAR');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LMIMPAVE),'STU_TPNT','LMIMPAVE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DESRIG),'STU_TPNT','LMDESRIG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LMFLSALI),'STU_TPNT','LMFLSALI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LMFLSALF),'STU_TPNT','LMFLSALF');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIG),'STU_TPNT','LMNUMRIG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TIPREG),'STU_TPNT','LMTIPREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TIPDOC),'STU_TPNT','LMTIPDOC');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'LMNUMPRG',this.w_NUMPRG,'LMDATREG',this.w_DATREG,'LMCODCAU',this.w_HOCCAU,'LMCOMPET',this.w_COMPET,'LMCODVAL',this.w_CODVAL,'LMCAOVAL',this.w_CAOVAL,'LMTIPCON',this.w_PCTIPSOT,'LMCODCON',this.w_LMCODCON,'LMIMPDAR',this.w_LMIMPDAR,'LMIMPAVE',this.w_LMIMPAVE,'LMDESRIG',this.w_DESRIG,'LMFLSALI',this.w_LMFLSALI)
            insert into (i_cTable) (LMNUMPRG,LMDATREG,LMCODCAU,LMCOMPET,LMCODVAL,LMCAOVAL,LMTIPCON,LMCODCON,LMIMPDAR,LMIMPAVE,LMDESRIG,LMFLSALI,LMFLSALF,LMNUMRIG,LMTIPREG,LMTIPDOC &i_ccchkf. );
               values (;
                 this.w_NUMPRG;
                 ,this.w_DATREG;
                 ,this.w_HOCCAU;
                 ,this.w_COMPET;
                 ,this.w_CODVAL;
                 ,this.w_CAOVAL;
                 ,this.w_PCTIPSOT;
                 ,this.w_LMCODCON;
                 ,this.w_LMIMPDAR;
                 ,this.w_LMIMPAVE;
                 ,this.w_DESRIG;
                 ,this.w_LMFLSALI;
                 ,this.w_LMFLSALF;
                 ,this.w_NUMRIG;
                 ,this.w_TIPREG;
                 ,this.w_TIPDOC;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error='Fallito inserimento riga dare nel temporaneo'
            return
          endif
          * --- Incremento numero di riga
          this.w_NUMRIG = this.w_NUMRIG+1
        otherwise
          this.w_FLAGDIV = "X"
          FSEEK(this.oParentObject.hFile,-3,1)
      endcase
    enddo
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,11)]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='MASTRI'
    this.cWorkTables[4]='PNT_DETT'
    this.cWorkTables[5]='PNT_MAST'
    this.cWorkTables[6]='SALDICON'
    this.cWorkTables[7]='STU_INTR'
    this.cWorkTables[8]='STU_PARA'
    this.cWorkTables[9]='STU_TPNT'
    this.cWorkTables[10]='STU_TRAS'
    this.cWorkTables[11]='ESERCIZI'
    return(this.OpenAllTables(11))

  proc CloseCursors()
    if used('_Curs_STU_TPNT')
      use in _Curs_STU_TPNT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
