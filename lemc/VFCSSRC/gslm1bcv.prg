* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm1bcv                                                        *
*              Export corrispettivi ventilati                                  *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][116][VRS_158]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-24                                                      *
* Last revis.: 2011-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm1bcv",oParentObject)
return(i_retval)

define class tgslm1bcv as StdBatch
  * --- Local variables
  w_STRINGA = space(120)
  w_ACTUALPOS = 0
  w_PNSERIAL = space(10)
  w_TOTDOC = 0
  w_NOTRAS = 0
  w_ANCODSTU = space(6)
  w_PNTIPCON = space(1)
  w_PNCODCON = space(15)
  w_NUMDETPERREG = 0
  w_CODATT = 0
  w_NUMREG = 0
  w_TIPOREG = space(1)
  w_PIUATTIV = space(1)
  w_CODSEZ = 0
  w_CODATTPR = space(5)
  w_FLAG3000EU = space(1)
  * --- WorkFile variables
  CONTI_idx=0
  STU_PNTT_idx=0
  ATTIDETT_idx=0
  ATTIMAST_idx=0
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT CORRISPETTIVI VENTILATI
    * --- Leggo se l'azienda esercita piu' Attivita'
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZATTIVI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZATTIVI;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PIUATTIV = NVL(cp_ToDate(_read_.AZATTIVI),cp_NullValue(_read_.AZATTIVI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT CORRVENT
    GO TOP
    * --- Ciclo sul cursore dei corrispettivi ventilati
    do while NOT EOF()
      * --- Messaggio a schermo
      ah_Msg("Export corrispettivi ventilati: reg. num. %1 - data %2",.t.,.f.,.f.,alltrim(STR(CORRVENT.PNNUMRER,6,0)),dtoc(CORRVENT.PNDATREG))
      * --- Scrittura su LOG
      this.w_STRINGA = space(10)+ah_MsgFormat("Export corrispettivi ventilati: reg. num. %1 - data %2",alltrim(STR(CORRVENT.PNNUMRER,6,0)),dtoc(CORRVENT.PNDATREG))
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      * --- Salvo la posizione attuale del record e il seriale della registrazione
      this.w_ACTUALPOS = RECNO()
      this.w_PNSERIAL = CORRVENT.PNSERIAL
      this.w_TOTDOC = 0
      * --- Conto il numero di dettagli per questa registrazione
      this.w_NUMDETPERREG = 0
      do while CORRVENT.PNSERIAL=this.w_PNSERIAL
        this.w_NUMDETPERREG = this.w_NUMDETPERREG+1
        skip 1
      enddo
      * --- Ripristino posizione
      go this.w_ACTUALPOS
      * --- Controllo se � una registrazione valida
      if (this.w_NUMDETPERREG%2<>0)
        * --- Marco questa registrazione come non valida
        go this.w_ACTUALPOS
        this.w_NOTRAS = -1
        * --- Insert into STU_PNTT
        i_nConn=i_TableProp[this.STU_PNTT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NOTRAS),'STU_PNTT','LMNUMTRA');
          +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.w_NOTRAS,'LMNUMTR2',0)
          insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.w_NOTRAS;
               ,0;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='GSLM_BCV: Scrittura con errore in STU_PNTT'
          return
        endif
        do while CORRVENT.PNSERIAL=this.w_PNSERIAL
          * --- Avanzo il puntatore
          skip 1
        enddo
      else
        do while CORRVENT.PNSERIAL=this.w_PNSERIAL
          * --- Registrazione valida
          * --- Incremento il numero di record trasferiti
          this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
          this.oParentObject.w_CORRVENT = this.oParentObject.w_CORRVENT+1
          * --- Tipo record
          FWRITE(this.oParentObject.hFile,"D40",3)
          * --- Estraggo il codice attivit� dal registro iva usato sulla prima riga del castelletto IVA della Primanota
          this.w_TIPOREG = NVL(CORRVENT.IVTIPREG," ")
          this.w_NUMREG = NVL(CORRVENT.IVNUMREG,0)
          * --- Estraggo il codice attivit� contenente il tipo e numero registro IVA presenti in Primanota
          * --- Read from ATTIDETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ATTIDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ATCODATT,ATCODSEZ"+;
              " from "+i_cTable+" ATTIDETT where ";
                  +"ATNUMREG = "+cp_ToStrODBC(this.w_NUMREG);
                  +" and ATTIPREG = "+cp_ToStrODBC(this.w_TIPOREG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ATCODATT,ATCODSEZ;
              from (i_cTable) where;
                  ATNUMREG = this.w_NUMREG;
                  and ATTIPREG = this.w_TIPOREG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODATTPR = NVL(cp_ToDate(_read_.ATCODATT),cp_NullValue(_read_.ATCODATT))
            this.w_CODSEZ = NVL(cp_ToDate(_read_.ATCODSEZ),cp_NullValue(_read_.ATCODSEZ))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Leggo il codice attivit� studio associato all'attivit� 
          * --- Read from ATTIMAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ATTIMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIMAST_idx,2],.t.,this.ATTIMAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ATATTIVA"+;
              " from "+i_cTable+" ATTIMAST where ";
                  +"ATCODATT = "+cp_ToStrODBC(this.w_CODATTPR);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ATATTIVA;
              from (i_cTable) where;
                  ATCODATT = this.w_CODATTPR;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODATT = NVL(cp_ToDate(_read_.ATATTIVA),cp_NullValue(_read_.ATATTIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_PIUATTIV # "S"
            * --- Se l'azienda non gestisce pi� attivit� assegno al codice attivit� '00'
            this.w_CODATT = 00
          endif
          * --- Codice attivit� IVA
          FWRITE (this.oParentObject.hFile ,right("00"+alltrim(str(this.w_CODATT)),2), 2)
          * --- Recupero e inserisco l' anno e il mese della registrazione
          FWRITE(this.oParentObject.hFile,LEFT(dtos(CORRVENT.PNDATREG),6),6)
          * --- Recupero e inserisco l' anno di competenza
          FWRITE(this.oParentObject.hFile,CORRVENT.PNCOMPET,4)
          * --- Recupero e inserisco il mese di competenza
          FWRITE(this.oParentObject.hFile,SUBSTR(dtos(CORRVENT.PNCOMIVA),5,2),2)
          * --- Sezione
          FWRITE(this.oParentObject.hFile,RIGHT("00"+ALLTRIM(STR(this.w_CODSEZ,2,0)),2),2)
          * --- Tipo Corrispettivo
          FWRITE(this.oParentObject.hFile,"SC",2)
          * --- Data registrazione
          FWRITE(this.oParentObject.hFile,dtos(CORRVENT.PNDATREG),8)
          * --- Scrittura del codice IVA
          FWRITE(this.oParentObject.hFile,"  ",2)
          * --- Importo Totale
          this.w_TOTDOC = CORRVENT.PNIMPDAR + CORRVENT.PNIMPAVE
          if g_PERVAL=this.oParentObject.w_VALEUR
            FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOC)),11,0))+RIGHT(STR(this.w_TOTDOC,12,2),2),11),11)
          else
            FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(this.w_TOTDOC),11,0)),11),11)
          endif
          * --- Segno
          FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOC>=0, "+", "-"),1)
          * --- Numero documentio dal al
          FWRITE(this.oParentObject.hFile,"000000000000",12)
          * --- Scrittura codice norma
          FWRITE(this.oParentObject.hFile,"  ",2)
          * --- Codice centro di costo
          FWRITE(this.oParentObject.hFile,"0000",4)
          * --- Contropartita
          if CORRVENT.ANTIPSOT="V"
            * --- Leggo il codice di contropartita
            this.w_PNTIPCON = CORRVENT.PNTIPCON
            this.w_PNCODCON = CORRVENT.PNCODCON
          else
            * --- Avanzo al record successivo
            skip 1
            * --- Leggo il codice di contropartita
            this.w_PNTIPCON = CORRVENT.PNTIPCON
            this.w_PNCODCON = CORRVENT.PNCODCON
            * --- Ripristino
            skip -1
          endif
          * --- Decodifico la contropartita
          this.w_ANCODSTU = SPACE(6)
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANCODSTU"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANCODSTU;
              from (i_cTable) where;
                  ANTIPCON = this.w_PNTIPCON;
                  and ANCODICE = this.w_PNCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          SELECT CORRVENT
          if EMPTY(this.w_ANCODSTU)
            this.w_ANCODSTU = "000000"
            this.w_STRINGA = ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON," "))
            FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
            FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
            this.oParentObject.w_ERRORE = .T.
          endif
          FWRITE(this.oParentObject.hFile,RIGHT("000000"+ALLTRIM(this.w_ANCODSTU),6),6)
          * --- Codice centro di costo
          FWRITE(this.oParentObject.hFile,"0000",4)
          * --- Codice Cassa
          if CORRVENT.ANTIPSOT<>"V"
            this.w_PNTIPCON = CORRVENT.PNTIPCON
            this.w_PNCODCON = CORRVENT.PNCODCON
          else
            * --- Avanzo al record successivo
            skip 1
            this.w_PNTIPCON = CORRVENT.PNTIPCON
            this.w_PNCODCON = CORRVENT.PNCODCON
            * --- Ripristino
            skip -1
          endif
          this.w_ANCODSTU = SPACE(6)
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANCODSTU"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANCODSTU;
              from (i_cTable) where;
                  ANTIPCON = this.w_PNTIPCON;
                  and ANCODICE = this.w_PNCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          SELECT CORRVENT
          if EMPTY(this.w_ANCODSTU)
            this.w_ANCODSTU = "000000"
            this.w_STRINGA = ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON," "))
            FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
            FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
            this.oParentObject.w_ERRORE = .T.
          endif
          * --- Scrivo su file
          FWRITE(this.oParentObject.hFile,RIGHT("000000"+ALLTRIM(this.w_ANCODSTU),6),6)
          * --- Codice CEE
          FWRITE(this.oParentObject.hFile,"  ",2)
          * --- Flag corrispettivo a calendario
          FWRITE(this.oParentObject.hFile," ",1)
          * --- Codice Cliente
          FWRITE(this.oParentObject.hFile, left (NVL (CORRVENT.ANCODSTU," ") +space (6) ,6) , 6)
          * --- Filler
          this.oParentObject.w_FILLER = SPACE(33)
          FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,33)
          * --- AIFT - gestioni 3000 euro 
          *     Se non c' � AIFT il campo � sbiancato
          *     Se non c' e intestatario si imposta CodTipoFatt3000Euro='E' anche se non c � AIFT
          *     Negli altri casi si imposta in base ai valori assunti dai campi OSFLGEXT e OSTIPOPE 
          *     - P = Contratto corrispettivi periodici 
          *     - F = Corrispettivo incluso forzatamente 
          *     - E = Corrispettivo escluso forzatamente 
          *     - Blk = Corrispettivo non classificato con alcuno dei valori precedenti 
          if g_AIFT ="S" AND NOT EMPTY ( NVL (CORRVENT.PNCODCLF ," ")) 
            this.w_FLAG3000EU = IIF ( NVL (CORRVENT.OSFLGEXT," ") = "I" , "F" , IIF ( NVL (CORRVENT.OSFLGEXT," ") = "F" , "E" , IIF ( NVL (CORRVENT.OSTIPOPE," ") = "P" , "P" , SPACE(1) ) ) )
            FWRITE(this.oParentObject.hFile, this.w_FLAG3000EU ,1)
          else
            if EMPTY ( NVL (CORRVENT.PNCODCLF ," ")) 
              FWRITE(this.oParentObject.hFile, "E" , 1)
            else
              FWRITE(this.oParentObject.hFile, "F" , 1)
            endif
          endif
          * --- Codice contratto
          if g_AIFT="S"
            FWRITE(this.oParentObject.hFile, left ( NVL(CORRVENT.OSRIFCON," ") + space(30),30) , 30)
          else
            this.oParentObject.w_FILLER = SPACE(30)
            FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,30)
          endif
          this.oParentObject.w_FILLER = SPACE(50)
          FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,50)
          * --- Avanzo di due record
          skip 2
        enddo
        * --- Insert into STU_PNTT
        i_nConn=i_TableProp[this.STU_PNTT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'STU_PNTT','LMNUMTRA');
          +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.oParentObject.w_PROFIL,'LMNUMTR2',0)
          insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.oParentObject.w_PROFIL;
               ,0;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='GSLM_BCV: Scrittura in STU_PNTT'
          return
        endif
        SELECT CORRVENT
      endif
    enddo
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='STU_PNTT'
    this.cWorkTables[3]='ATTIDETT'
    this.cWorkTables[4]='ATTIMAST'
    this.cWorkTables[5]='AZIENDA'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
