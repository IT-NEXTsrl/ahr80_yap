* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_bfr                                                        *
*              Import raccordo contb                                           *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-18                                                      *
* Last revis.: 2007-12-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm_bfr",oParentObject)
return(i_retval)

define class tgslm_bfr as StdBatch
  * --- Local variables
  w_MESS = space(254)
  w_MESS2 = space(254)
  w_HFILE = 0
  w_NUMBLOCCHI_OK = 0
  w_OK_BLOCCO = 0
  w_CLEN = 0
  w_PADRE = .NULL.
  w_STATO = space(5)
  w_NUMBLOCCHI = 0
  w_BLOCK = space(100)
  w_DITTA = space(6)
  w_OLDCONTO = space(6)
  w_NEWCONTO = space(6)
  w_RECBLANK = 0
  w_TIPCON = space(1)
  w_CODICE = space(15)
  w_TIPREC = space(1)
  w_TXTCOD = space(4)
  * --- WorkFile variables
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Converte i sottoconto studio di conti / clienti / fornitori utilizzando i due file
    *     di raccordo producibili da apri
    * --- Avviso che l'operazione non � ripetibile....
    this.w_MESS = "Attenzione, l'operazione non � ripetibile. E' quindi consigliabile creare una copia dei dati prima della sua esecuzione%0%1"
    * --- Sono costretto a spezzare il messaggio in due parti poich� la lunghezza complessiva eccede i 254 caratteri
    this.w_MESS2 = ah_MsgFormat("E' possibile fare una copia tramite l'utilit� Backup database nel men� servizi/manutenzione sistema%0Si desidera proseguire?")
    if Not ah_YesNo(this.w_MESS,,this.w_MESS2)
      i_retcode = 'stop'
      return
    endif
    this.oParentObject.w_PATH = Alltrim( this.oParentObject.w_PATH )
    if Not File( this.oParentObject.w_PATH )
      ah_ErrorMsg(MSG_FILE__NOT_FOUND_QM,,"")
      i_retcode = 'stop'
      return
    endif
    * --- Apro il file...
    this.w_HFILE = Fopen( this.oParentObject.w_PATH , 0 )
    if this.w_HFILE=-1
      ah_ErrorMsg("Impossibile aprire il file %1",,"", this.oParentObject.w_PATH)
      i_retcode = 'stop'
      return
    else
      * --- Leggo i vari blocchi fino alla fine del file di testo...
      this.w_STATO = "B1"
      this.w_PADRE = this.oParentObject
      this.w_NUMBLOCCHI = 0
      this.w_NUMBLOCCHI_OK = 0
      * --- Ho un file con blocchi composti da 51 caratteri se raccordo clienti o fornitori
      this.w_CLEN = 51
      this.oParentObject.w_Msg = ""
      * --- Metto in primo piano la pagina del log..
      this.w_PADRE.oPgFrm.ActivePage = 2
      AddMsgNL("Elaborazione iniziata alle: %1", this, Time()) 
 
      * --- Creo un cursore VFP per contenere l'elenco dei conti con sotto conto studio
      *     valorizzato, mano a mano che aggiorno cancello voci.
      *     
      *     Se al termine nel cursore ho ancora delle voci andr� sul database a svuotare
      *     l'informazione.
      vq_exec("..\LEMC\EXE\QUERY\GSLM_BFR.VQR",this,"__tmp__")
      nRes = WrCursor( "__tmp__" )
      AddMsgNL("Valutazione blocchi file di testo:",this, this.w_BLOCK, , , , , .t. )
      * --- Try
      local bErr_03A2B468
      bErr_03A2B468=bTrsErr
      this.Try_03A2B468()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        AddMsgNL("Elaborazione terminata alle: %1", this, Time())
        if i_ErrMsg<>"OUT"
          * --- Uscita imprevista...
          ah_ErrorMsg("Elaborazione annullata, errore: %1",,"", message())
        else
          ah_ErrorMsg("Elaborazione annullata, consultare il log per ulteriori informazioni.",,"")
        endif
      endif
      bTrsErr=bTrsErr or bErr_03A2B468
      * --- End
      * --- Chiudo il file..
      FClose( this.w_HFILE)
    endif
  endproc
  proc Try_03A2B468()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    do while Not FEOF( this.w_HFILE ) 
 
      this.w_DITTA = ""
      this.w_OK_BLOCCO = .0
      if this.oParentObject.w_TIPO="C"
        * --- Leggo un numero fisso di caratteri...
        this.w_OLDCONTO = ""
        this.w_NEWCONTO = ""
        this.w_BLOCK = FRead( this.w_HFILE , this.w_CLEN)
        this.w_NUMBLOCCHI = this.w_NUMBLOCCHI + 1
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        * --- Nel caso di raccordo per conti leggo l'intera riga..
        this.w_BLOCK = FGet( this.w_HFILE )
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Se blocco valido 
      if this.w_OK_BLOCCO=0
        * --- Modifico il codice del cliente fornitore conto
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Resoconto..
      if (this.oParentObject.w_ERRLOG="S" And this.w_OK_BLOCCO<0) or Empty( this.oParentObject.w_ERRLOG )
        AddMsgNL(this.w_BLOCK,this, , , , , , .t. )
      endif
      * --- Alcuni errori provocano l'uscita dall'elaborazione, si riconoscono per la Raise
      do case
        case this.w_OK_BLOCCO=0
          this.w_NUMBLOCCHI_OK = this.w_NUMBLOCCHI_OK + 1
          if Empty( this.oParentObject.w_ERRLOG )
            * --- Se attivo il check mostro ne log anche le righe con esito positivo...
            AddMsgNL("Esito positivo.",this, , , , , ,.t.)
          endif
        case this.w_OK_BLOCCO=-1
          AddMsgNL("Esito negativo (blocco non riconosciuto)." , this, , , , , ,.t. )
          * --- Raise
          i_Error="OUT"
          return
        case this.w_OK_BLOCCO=-2
          AddMsgNL("Esito negativo (tipo semplificato)." ,this, , , , , ,.t.)
        case this.w_OK_BLOCCO=-3
          AddMsgNL("Esito negativo (codice azienda studio differente)." , this, , , , , ,.t. )
        case this.w_OK_BLOCCO=-4
          AddMsgNL("Esito negativo (codice sottoconto %1 non trovato in ad hoc)." , this, this.w_OLDCONTO)
        case this.w_OK_BLOCCO=-5
          AddMsgNL("Esito negativo (file di formato non valido )." , this, , , , , ,.t. )
          * --- Raise
          i_Error="OUT"
          return
        case this.w_OK_BLOCCO=-6
          AddMsgNL("Esito negativo ( Codice piano dei conti %1 differente da quanto specificato in ad hoc %2)." , this, this.w_TXTCOD, this.oParentObject.w_CODPDC)
          * --- Raise
          i_Error="OUT"
          return
        case this.w_OK_BLOCCO=-7
          AddMsgNL("Esito negativo (file di formato non valido il dettaglio doveva iniziare per OLD )." , this, , , , , ,.t.)
          * --- Raise
          i_Error="OUT"
          return
        case this.w_OK_BLOCCO=-8
          AddMsgNL("Esito negativo (file di formato non valido il dettaglio doveva iniziare per NEW )." , this,,,,,,.t. )
          * --- Raise
          i_Error="OUT"
          return
        case this.w_OK_BLOCCO=-9
          AddMsgNL("Esito negativo (Cambio pagina non previsto )." , this,,,,,,.t. )
          * --- Raise
          i_Error="OUT"
          return
      endcase
    enddo
    * --- Passo alla pulizia del database...
     
 Select __tmp__ 
 Go Top
    this.w_RECBLANK = 0
    do while Not Eof( "__tmp__" ) 
      * --- Se il codice studio � valorizzato allora lo sbianco
      if Not Deleted()
        this.w_TIPCON = __tmp__.ANTIPCON
        this.w_CODICE = __tmp__.ANCODICE
        * --- Write into CONTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANCODSTU ="+cp_NullLink(cp_ToStrODBC(""),'CONTI','ANCODSTU');
              +i_ccchkf ;
          +" where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_CODICE);
                 )
        else
          update (i_cTable) set;
              ANCODSTU = "";
              &i_ccchkf. ;
           where;
              ANTIPCON = this.w_TIPCON;
              and ANCODICE = this.w_CODICE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_RECBLANK = this.w_RECBLANK + 1
      endif
       
 Select __tmp__ 
 Skip
    enddo
    * --- commit
    cp_EndTrs(.t.)
    AddMsgNL("Elaborazione terminata alle: %1", this, Time())
    AddMsgNL("Elaborazione terminata con successo per %1 conti su %2 globali.Sbiancati %3 codici.",this, Alltrim( Str( this.w_NUMBLOCCHI_OK ) ), Alltrim( Str( this.w_NUMBLOCCHI ) ), Alltrim( Str( this.w_RECBLANK ) ) )
    ah_ErrorMsg("Elaborazione terminata",,"")
    * --- Se il temporaneo � pieno allora stampo i codice sbiancati...
    if this.w_RECBLANK>0 And ah_YesNo("Si desidera stampare l'elenco dei conti/clienti/fornitori ai quali � stato sbiancato il codice sottoconto studio?")
       
 Select __tmp__
      CP_CHPRN("QUERY\GSVE_BCV.FRX")
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esamina il blocco di caratteri per clienti fornitori...
    * --- descrizione                           da               a
    *     TipoRecord                              1              3
    *     tipo contab.                             4               4
    *     codice ditta                              5              10
    *     old pianoconti                          11            14
    *     new pianoconti                        15            18
    *     old sottoconto                          19            24
    *     new sottoconto                        25            30
    *     flag stato x APRI                      31            31
    *     filler                                          31             51
    * --- Fille = rimpiemento
    *     Flag stato APRI da ignorare
    *     Old / new Pianoconti da ignorare
    *     Codice Ditta (corrisponde a LMAZISTU) si pu� verificare se differenti..
    *     Il progressivo ed il sottoconto non servono in quanto APRI non � possibile
    *     assegnare lo stesso sottoconto a due conti studio, anche se con progressivo diverso.
    * --- Verifico se inizia per CLF..
    if Left ( this.w_BLOCK , 3 ) ="CLF"
      this.w_TIPREC = Substr( this.w_BLOCK , 4, 1 )
      if this.w_TIPREC="G"
        * --- Leggo codice ditta...
        this.w_DITTA = Substr( this.w_BLOCK , 5, 6 )
        if this.oParentObject.w_CHKAZI="S" And this.w_DITTA <> this.oParentObject.w_AZISTU
          this.w_OK_BLOCCO = -3
        else
          * --- Vecchio e nuovo codice
          *     Il vecchio codice in ad hoc � rimasto di 5 caratteri mentre sul file di testo 
          *     � lungo 6, nel primo messo sempre uno 0 come prefisso.
          this.w_OLDCONTO = Substr( this.w_BLOCK , 19 , 6 )
          this.w_NEWCONTO = Substr( this.w_BLOCK , 25 , 6 )
          this.w_OK_BLOCCO = 0
        endif
      else
        * --- Escludiamo i blocchi di tipo "semplificata"
        this.w_OK_BLOCCO = -2
      endif
    else
      * --- Blocco non riconosciuto
      this.w_OK_BLOCCO = -1
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Il codice sottoconto � chiave al di la del progressivo e del codice piano dei conti
    *     in APRI.  Non � quindi possibile avere due sottoconti uguali appartenenti a due piani
    *     dei conti
    * --- Write into CONTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ANCODSTU ="+cp_NullLink(cp_ToStrODBC(this.w_NEWCONTO),'CONTI','ANCODSTU');
          +i_ccchkf ;
      +" where ";
          +"ANCODSTU = "+cp_ToStrODBC(this.w_OLDCONTO);
             )
    else
      update (i_cTable) set;
          ANCODSTU = this.w_NEWCONTO;
          &i_ccchkf. ;
       where;
          ANCODSTU = this.w_OLDCONTO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if i_ROWS=0
      * --- Codice Conto non trovato, potrebbe essere lungo 6 su ad hoc..
      if Len(Alltrim( this.w_OLDCONTO ))=5
        this.w_OLDCONTO = "0"+this.w_OLDCONTO
        * --- Write into CONTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANCODSTU ="+cp_NullLink(cp_ToStrODBC(this.w_NEWCONTO),'CONTI','ANCODSTU');
              +i_ccchkf ;
          +" where ";
              +"ANCODSTU = "+cp_ToStrODBC(this.w_OLDCONTO);
                 )
        else
          update (i_cTable) set;
              ANCODSTU = this.w_NEWCONTO;
              &i_ccchkf. ;
           where;
              ANCODSTU = this.w_OLDCONTO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if i_ROWS=0
          this.w_OK_BLOCCO = -4
        else
          * --- Pulisco il temporaneo dei non trovati...
          Delete from __tmp__ Where ANCODSTU = this.w_OLDCONTO
        endif
      else
        this.w_OK_BLOCCO = -4
      endif
    else
      * --- Pulisco il temporaneo dei non trovati...
      Delete from __tmp__ Where ANCODSTU = this.w_OLDCONTO
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Analisi file di testo da stampa per conti...
    * --- w_STATO
    *     B? = Righe intestazione pagina
    *     C? Righe titoli report
    *     D? Dati
    *     F ? sono arritvato alla fine tutte le righe sono ok
    do case
      case Left(this.w_STATO,1)="B"
        * --- Righe intestazione pagina...
        if Val ( Right( this.w_STATO ,1)) =6
          * --- Verifico se il blocco contiene un carattere che riconosco...
          if Lower( Left( this.w_BLOCK ,Len("piano dei conti da raccordare") ))= "piano dei conti da raccordare"
            this.w_STATO = "C1"
          else
            this.w_OK_BLOCCO = -5
          endif
        else
          * --- Se sono arrivato alla fine....
          if Upper( Left (this.w_BLOCK ,Len("CONTI CLIENTI/FORNITORI RACCORDATI") ) )= "CONTI CLIENTI/FORNITORI RACCORDATI"
            this.w_STATO = "F"
          else
            this.w_STATO = "B" + Str( Val ( Right( this.w_STATO ,1) )+1 ,1,0)
          endif
        endif
        * --- Uno significa che la riga � ignorata, viene solo messa nel log
        this.w_OK_BLOCCO = iif( this.w_OK_BLOCCO<0 , this.w_OK_BLOCCO , 1 )
      case Left(this.w_STATO,1)="C"
        * --- Parte di pagina relativa alle intestazione delle colonne ed al codice piano
        *     dei conti...
        do case
          case Val ( Right( this.w_STATO ,1)) =1
            * --- Verifico se il codice del piano dei conti � congruente...
            if this.oParentObject.w_CHKPDC="S"
              * --- Il blocco deve iniziare per 'Nuovo piano dei conti'
              if Lower( Left ( this.w_BLOCK ,Len("nuovo piano dei conti") ) )= "nuovo piano dei conti"
                * --- Leggo il codice...
                this.w_TXTCOD = Substr( this.w_BLOCK , 32 , 4 )
                if this.oParentObject.w_CODPDC=this.w_TXTCOD
                else
                  this.w_OK_BLOCCO = -6
                endif
              else
                this.w_OK_BLOCCO = -5
              endif
            endif
            this.w_STATO = "C" + Str( Val ( Right( this.w_STATO ,1) )+1 ,1,0)
          case Val ( Right( this.w_STATO ,1)) =3
            this.w_STATO = "D0"
          otherwise
            this.w_STATO = "C" + Str( Val ( Right( this.w_STATO ,1) )+1 ,1,0)
        endcase
        this.w_OK_BLOCCO = iif( this.w_OK_BLOCCO<0 , this.w_OK_BLOCCO , 1 )
      case Left(this.w_STATO,1)="D"
        * --- Se w_BLOCK � vuoto sono alla fine della pagina, testo anche la riga dopo,
        *     se composta di trattini, allora sono arrivato al cambio pagina..
        if Empty( this.w_BLOCK )
          * --- Il cambio pagina non pu� avvenire tra OLD e NEW
          if this.w_STATO="D1"
            this.w_OK_BLOCCO = -9
          else
            * --- Le prossime righe sono l'intestazione della pagina..
            this.w_STATO = "B1"
            this.w_OK_BLOCCO = 1
          endif
        else
          * --- D0 = la riga in esame deve iniziare per OLD
          *     D1 = la riga in esame deve iniziare per NEW
          if this.w_STATO="D0"
            if Upper( Left( this.w_BLOCK ,Len("OLD")) )= "OLD"
              * --- Passo di stato...
              this.w_STATO = "D1"
              * --- Leggo il vecchio sottoconto...
              this.w_OLDCONTO = Substr( this.w_BLOCK , 5, 5 )
              this.w_OK_BLOCCO = 1
            else
              this.w_OK_BLOCCO = -7
            endif
          else
            if Upper( Left ( this.w_BLOCK ,Len("NEW") ) )= "NEW"
              * --- Passo di stato...
              this.w_STATO = "D0"
              * --- Leggo il nuovo sottoconto...
              this.w_NEWCONTO = Substr( this.w_BLOCK , 5, 6 )
              * --- Ho sia OLD che NEW provo ad aggiornare...
              this.w_OK_BLOCCO = 0
              this.w_NUMBLOCCHI = this.w_NUMBLOCCHI + 1
            else
              this.w_OK_BLOCCO = -8
            endif
          endif
        endif
      case this.w_STATO="F"
        this.w_OK_BLOCCO = 1
    endcase
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
