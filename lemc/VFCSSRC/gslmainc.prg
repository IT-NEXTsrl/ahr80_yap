* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslmainc                                                        *
*              Export incassi/pagamenti                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-03-23                                                      *
* Last revis.: 2015-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslmainc",oParentObject)
return(i_retval)

define class tgslmainc as StdBatch
  * --- Local variables
  w_STRINGA = space(120)
  w_ACTUALPOS = 0
  w_PNSERIAL = space(10)
  w_TOTDOC = 0
  w_ANCODSTU = space(6)
  w_APRCAU = space(3)
  w_PNCODCAU = space(5)
  w_PNTIPCON = space(1)
  w_PNCODCON = space(15)
  w_NUMDETPERREG = 0
  w_DESCRIZIONE = space(29)
  w_CAUCAD = space(3)
  w_CAUDAC = space(3)
  w_PNCAURIG = space(5)
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_COD_DARE = space(1)
  w_COD_AVER = space(1)
  w_SERDACOR = .f.
  w_CODSOGG = space(8)
  w_CODPIVA = space(16)
  w_CODFISC = space(16)
  w_CODSOGGD = space(8)
  w_CODPIVAD = space(16)
  w_CODFISCD = space(16)
  w_CODSOGGA = space(8)
  w_CODPIVADA = space(16)
  w_CODFISCDA = space(16)
  w_CONTROPA = space(15)
  w_CAUDOC = space(5)
  w_CODIVA = space(5)
  w_PNSERORI = space(10)
  w_Valuta = space(3)
  w_Valore = 0
  w_Lunghezza = 0
  w_StrInt = space(1)
  w_StrDec = space(1)
  w_Decimali = 0
  w_CODPIVAA = space(12)
  w_CODFISCA = space(16)
  w_CODATTPR = space(5)
  w_NUMREG = 0
  w_CODATTPR = space(5)
  w_CODSEZ = 0
  w_TIPOREG = space(1)
  w_CODATT = 0
  * --- WorkFile variables
  STU_TRAS_idx=0
  CONTI_idx=0
  ATTIDETT_idx=0
  ATTIMAST_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT INCASSI E PAGAMENTI
    this.w_StrInt = space(1)
    this.w_StrDec = space(1)
    this.w_Valuta = g_PERVAL
    this.w_Decimali = GETVALUT( this.w_Valuta, "VADECTOT")
    * --- Movimenti di primanota standard
    this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
    this.oParentObject.w_INCASSI = this.oParentObject.w_INCASSI+1
    * --- Tipo record
    FWRITE(this.oParentObject.hFile,"D60",3)
    * --- Progressivo registrazione
    FWRITE(this.oParentObject.hFile,RIGHT(SPACE(15)+ALLTRIM(STR(INCASSI.PNNUMRER)),15),15)
    * --- Progressivo rata di origine
    FWRITE(this.oParentObject.hFile,RIGHT(SPACE(5)+ALLTRIM(STR(INCASSI.CPROWNUM)),5),5)
    * --- Tipo pagamento
    do case
      case INCASSI.PNTIPCON="C"
        FWRITE(this.oParentObject.hFile,"001",3)
      case INCASSI.PNTIPCON="F"
        FWRITE(this.oParentObject.hFile,"002",3)
      otherwise
        FWRITE(this.oParentObject.hFile,"   ",3)
    endcase
    * --- Data registrazione
    FWRITE(this.oParentObject.hFile,dtos(INCASSI.PNDATREG),8)
    * --- Recupero codice causale dello studio
    this.w_APRCAU = SPACE(10)
    this.w_PNCAURIG = NVL(INCASSI.PNCAURIG," ")
    * --- Read from STU_TRAS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.STU_TRAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "LMAPRCAU"+;
        " from "+i_cTable+" STU_TRAS where ";
            +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
            +" and LMHOCCAU = "+cp_ToStrODBC(this.w_PNCAURIG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        LMAPRCAU;
        from (i_cTable) where;
            LMCODICE = this.oParentObject.w_ASSOCI;
            and LMHOCCAU = this.w_PNCAURIG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_APRCAU = NVL(cp_ToDate(_read_.LMAPRCAU),cp_NullValue(_read_.LMAPRCAU))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT INCASSI
    if EMPTY(this.w_APRCAU)
      this.w_APRCAU = "0000000000"
      * --- Non ho trovato associato il codice causale
      this.w_STRINGA = ah_MsgFormat("%1Errore! Causale %2 non associata nello studio","               ",NVL(this.w_PNCAURIG, " "))
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      this.oParentObject.w_ERRORE = .T.
    endif
    FWRITE(this.oParentObject.hFile,left(alltrim(nvl(this.w_APRCAU," "))+repl(" ",10),10),10)
    * --- Recupero descrizione
    this.w_DESCRIZIONE = INCASSI.PNDESRIG
    if EMPTY(INCASSI.PNDESRIG)
      this.w_DESCRIZIONE = LOOKTAB("CAU_CONT","CCDESCRI","CCCODICE",INCASSI.PNCAURIG)
    endif
    * --- Descrizione
    FWRITE(this.oParentObject.hFile,left(alltrim(nvl(this.w_DESCRIZIONE," "))+repl(" ",30),30),30)
    this.oParentObject.w_FILLER = SPACE(11)
    FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,11)
    if INCASSI.PNIMPDAR<>0
      * --- Conto DARE
      this.w_PNTIPCON = NVL(INCASSI.PNTIPCON," ")
      this.w_COD_DARE = this.w_PNTIPCON
      this.w_PNCODCON = NVL(INCASSI.PNCODCON," ")
      this.w_ANCODSTU = SPACE(9)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU,ANCODSOG,ANPARIVA,ANCODFIS"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU,ANCODSOG,ANPARIVA,ANCODFIS;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCON;
              and ANCODICE = this.w_PNCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        this.w_CODSOGGD = NVL(cp_ToDate(_read_.ANCODSOG),cp_NullValue(_read_.ANCODSOG))
        this.w_CODPIVAD = NVL(cp_ToDate(_read_.ANPARIVA),cp_NullValue(_read_.ANPARIVA))
        this.w_CODFISCD = NVL(cp_ToDate(_read_.ANCODFIS),cp_NullValue(_read_.ANCODFIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT INCASSI
      if EMPTY(this.w_ANCODSTU)
        this.w_ANCODSTU = "000000000"
        do case
          case this.w_PNTIPCON="C"
            this.w_STRINGA = ah_MsgFormat("%1Errore! Cliente %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          case this.w_PNTIPCON="F"
            this.w_STRINGA = ah_Msgformat("%1Errore! Fornitore %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          otherwise
            this.w_STRINGA = ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        endcase
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      * --- Scrivo il Conto sul file
      FWRITE(this.oParentObject.hFile,left(alltrim(this.w_ANCODSTU)+repl("0",9),9),9)
    endif
    if INCASSI.PNIMPDAR=0
      * --- Conto AVERE
      this.w_PNTIPCON = NVL(INCASSI.PNTIPCON," ")
      this.w_COD_AVER = this.w_PNTIPCON
      this.w_PNCODCON = NVL(INCASSI.PNCODCON," ")
      this.w_ANCODSTU = SPACE(9)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU,ANCODSOG,ANPARIVA,ANCODFIS"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU,ANCODSOG,ANPARIVA,ANCODFIS;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCON;
              and ANCODICE = this.w_PNCODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        this.w_CODSOGGA = NVL(cp_ToDate(_read_.ANCODSOG),cp_NullValue(_read_.ANCODSOG))
        this.w_CODPIVAA = NVL(cp_ToDate(_read_.ANPARIVA),cp_NullValue(_read_.ANPARIVA))
        this.w_CODFISCA = NVL(cp_ToDate(_read_.ANCODFIS),cp_NullValue(_read_.ANCODFIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT INCASSI
      if EMPTY(this.w_ANCODSTU)
        this.w_ANCODSTU = "000000000"
        do case
          case this.w_PNTIPCON="C"
            this.w_STRINGA = ah_MsgFormat("%1Errore! Cliente %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          case this.w_PNTIPCON="F"
            this.w_STRINGA = ah_MsgFormat("%1Errore! Fornitore non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
          otherwise
            this.w_STRINGA = ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON, " "))
        endcase
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      * --- Scrivo il Conto sul file
      FWRITE(this.oParentObject.hFile,left(alltrim(this.w_ANCODSTU)+repl("0",9),9),9)
    endif
    * --- Codice tipo pagamento
    FWRITE(this.oParentObject.hFile," ",1)
    * --- Filler
    this.oParentObject.w_FILLER = SPACE(4)
    FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,4)
    * --- Importo
    this.w_Valore = nvl(INCASSI.PNIMPDAR,0)+nvl(INCASSI.PNIMPAVE,0)
    this.w_Lunghezza = 16
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    FWRITE(this.oParentObject.hFile,this.w_VALORE,16)
    * --- Estraggo il codice attivit� dal registro iva usato sulla prima riga del castelletto IVA della Primanota
    this.w_TIPOREG = NVL(INCASSI.IVTIPREG," ")
    this.w_NUMREG = NVL(INCASSI.IVNUMREG,0)
    * --- Estraggo il codice attivit� contenente il tipo e numero registro IVA presenti in Primanota
    * --- Read from ATTIDETT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ATTIDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ATCODATT,ATCODSEZ"+;
        " from "+i_cTable+" ATTIDETT where ";
            +"ATNUMREG = "+cp_ToStrODBC(this.w_NUMREG);
            +" and ATTIPREG = "+cp_ToStrODBC(this.w_TIPOREG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ATCODATT,ATCODSEZ;
        from (i_cTable) where;
            ATNUMREG = this.w_NUMREG;
            and ATTIPREG = this.w_TIPOREG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODATTPR = NVL(cp_ToDate(_read_.ATCODATT),cp_NullValue(_read_.ATCODATT))
      this.w_CODSEZ = NVL(cp_ToDate(_read_.ATCODSEZ),cp_NullValue(_read_.ATCODSEZ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Leggo il codice attivit� studio associato all'attivit� 
    * --- Read from ATTIMAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ATTIMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIMAST_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ATATTIVA"+;
        " from "+i_cTable+" ATTIMAST where ";
            +"ATCODATT = "+cp_ToStrODBC(this.w_CODATTPR);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ATATTIVA;
        from (i_cTable) where;
            ATCODATT = this.w_CODATTPR;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODATT = NVL(cp_ToDate(_read_.ATATTIVA),cp_NullValue(_read_.ATATTIVA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Codice attivit� IVA
    if this.w_CODATT=0
      this.w_STRINGA = Ah_MsgFormat("Errore! Inserire un codice attivit� valido")
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      this.oParentObject.w_ERRORE = .T.
    endif
    FWRITE (this.oParentObject.hFile ,right("00"+alltrim(str(this.w_CODATT)),2), 2)
    * --- Sezione
    FWRITE(this.oParentObject.hFile,RIGHT("00"+ALLTRIM(STR(this.w_CODSEZ,2,0)),2),2)
    * --- Filler
    this.oParentObject.w_FILLER = SPACE(181)
    FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,181)
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Trasformazione degli importi nel formato richiesto dal tracciato R.I.D.
    * --- UTILIZZO
    * --- Prima della chiamata a questa pagina del batch devono essere impostate le variabili:
    * --- w_Valore          = importo da convertire in stringa
    * --- w_Lunghezza  = lunghezza della stringa
    * --- Il risultato della conversione � disponibile nella variabile w_Valore.
    * --- CONVENZIONI
    * --- La virgola deve essere eliminata
    * --- Se la valuta � Euro le ultime due cifre rappresentano i decimali
    * --- Il valore deve essere allineato a destra
    * --- Non � richiesto il riempimento con 0 delle cifre non significative
    * --- Calcolo parte intera
    this.w_StrInt = alltrim( str( int(this.w_Valore), this.w_Lunghezza, 0 ) )
    * --- Calcolo parte decimale
    this.w_StrDec = ""
    if this.w_Decimali > 0
      this.w_StrDec = right( str( this.w_Valore, this.w_Lunghezza, this.w_Decimali ) , this.w_Decimali )
    endif
    * --- Risultato
    this.w_Valore = right( repl(" ",this.w_Lunghezza) + this.w_StrInt + this.w_StrDec , this.w_Lunghezza )
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='STU_TRAS'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='ATTIDETT'
    this.cWorkTables[4]='ATTIMAST'
    return(this.OpenAllTables(4))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
