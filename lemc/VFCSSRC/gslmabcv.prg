* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslmabcv                                                        *
*              Export corrispettivi ventilati                                  *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][116][VRS_158]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-24                                                      *
* Last revis.: 2018-04-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslmabcv",oParentObject)
return(i_retval)

define class tgslmabcv as StdBatch
  * --- Local variables
  w_STRINGA = space(120)
  w_ACTUALPOS = 0
  w_PNSERIAL = space(10)
  w_TOTDOC = 0
  w_NOTRAS = 0
  w_ANCODSTU = space(6)
  w_PNTIPCON = space(1)
  w_PNCODCON = space(15)
  w_CODATT = 0
  w_NUMREG = 0
  w_TIPOREG = space(1)
  w_PIUATTIV = space(1)
  w_CODSEZ = 0
  w_CODCAU = space(10)
  w_Valuta = space(3)
  w_Valore = 0
  w_Lunghezza = 0
  w_StrInt = space(1)
  w_StrDec = space(1)
  w_Decimali = 0
  w_APRCAU = space(3)
  w_APRNORM = space(2)
  w_IVASTU = space(2)
  w_IVACEE = space(2)
  w_PNCODCAU = space(5)
  w_IVCODIVA = space(5)
  w_CODATTPR = space(5)
  w_TIPCAS = space(1)
  w_CODCAS = space(15)
  * --- WorkFile variables
  CONTI_idx=0
  STU_PNTT_idx=0
  ATTIDETT_idx=0
  ATTIMAST_idx=0
  AZIENDA_idx=0
  STU_TRAS_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT CORRISPETTIVI VENTILATI
    this.w_StrInt = space(1)
    this.w_StrDec = space(1)
    * --- Leggo se l'azienda esercita piu' Attivita'
    this.w_Valuta = g_PERVAL
    this.w_Decimali = GETVALUT( this.w_Valuta, "VADECTOT")
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZATTIVI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZATTIVI;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PIUATTIV = NVL(cp_ToDate(_read_.AZATTIVI),cp_NullValue(_read_.AZATTIVI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT CORRVENT
    GO TOP
    * --- Ciclo sul cursore dei corrispettivi ventilati
    do while NOT EOF()
      * --- Messaggio a schermo
      ah_Msg("Export corrispettivi ventilati: reg. num. %1 - data %2",.t.,.f.,.f.,alltrim(STR(CORRVENT.PNNUMRER,6,0)),dtoc(CORRVENT.PNDATREG))
      * --- Scrittura su LOG
      this.w_STRINGA = space(10)+ah_MsgFormat("Export corrispettivi ventilati: reg. num. %1 - data %2",alltrim(STR(CORRVENT.PNNUMRER,6,0)),dtoc(CORRVENT.PNDATREG))
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      * --- Salvo la posizione attuale del record e il seriale della registrazione
      this.w_ACTUALPOS = RECNO()
      this.w_PNSERIAL = CORRVENT.PNSERIAL
      this.w_TOTDOC = 0
      * --- Ripristino posizione
      go this.w_ACTUALPOS
      * --- Controllo se � una registrazione valida
      do while CORRVENT.PNSERIAL=this.w_PNSERIAL
        * --- Registrazione valida
        * --- Incremento il numero di record trasferiti
        this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
        this.oParentObject.w_CORRVENT = this.oParentObject.w_CORRVENT+1
        * --- Tipo record
        FWRITE(this.oParentObject.hFile,"D40",3)
        * --- Estraggo il codice attivit� dal registro iva usato sulla prima riga del castelletto IVA della Primanota
        this.w_TIPOREG = NVL(CORRVENT.IVTIPREG," ")
        this.w_NUMREG = NVL(CORRVENT.IVNUMREG,0)
        * --- Estraggo il codice attivit� contenente il tipo e numero registro IVA presenti in Primanota
        * --- Read from ATTIDETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ATTIDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ATCODATT,ATCODSEZ"+;
            " from "+i_cTable+" ATTIDETT where ";
                +"ATNUMREG = "+cp_ToStrODBC(this.w_NUMREG);
                +" and ATTIPREG = "+cp_ToStrODBC(this.w_TIPOREG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ATCODATT,ATCODSEZ;
            from (i_cTable) where;
                ATNUMREG = this.w_NUMREG;
                and ATTIPREG = this.w_TIPOREG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODATTPR = NVL(cp_ToDate(_read_.ATCODATT),cp_NullValue(_read_.ATCODATT))
          this.w_CODSEZ = NVL(cp_ToDate(_read_.ATCODSEZ),cp_NullValue(_read_.ATCODSEZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Leggo il codice attivit� studio associato all'attivit� 
        * --- Read from ATTIMAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ATTIMAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIMAST_idx,2],.t.,this.ATTIMAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ATATTIVA"+;
            " from "+i_cTable+" ATTIMAST where ";
                +"ATCODATT = "+cp_ToStrODBC(this.w_CODATTPR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ATATTIVA;
            from (i_cTable) where;
                ATCODATT = this.w_CODATTPR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODATT = NVL(cp_ToDate(_read_.ATATTIVA),cp_NullValue(_read_.ATATTIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Codice attivit� IVA
        if this.w_CODATT=0
          this.w_STRINGA = Ah_MsgFormat("Errore! Inserire un codice attivit� valido")
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        * --- Recupero e scrittura del codice di Aliquota IVA
        this.w_APRCAU = SPACE(3)
        this.w_APRNORM = SPACE(2)
        this.w_IVASTU = SPACE(2)
        this.w_IVACEE = SPACE(2)
        this.w_PNCODCAU = CORRVENT.PNCODCAU
        * --- Read from STU_TRAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.STU_TRAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LMAPRNOR,LMAPRCAU,LMIVASTU,LMIVACEE"+;
            " from "+i_cTable+" STU_TRAS where ";
                +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
                +" and LMHOCCAU = "+cp_ToStrODBC(this.w_PNCODCAU);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LMAPRNOR,LMAPRCAU,LMIVASTU,LMIVACEE;
            from (i_cTable) where;
                LMCODICE = this.oParentObject.w_ASSOCI;
                and LMHOCCAU = this.w_PNCODCAU;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_APRNORM = NVL(cp_ToDate(_read_.LMAPRNOR),cp_NullValue(_read_.LMAPRNOR))
          this.w_APRCAU = NVL(cp_ToDate(_read_.LMAPRCAU),cp_NullValue(_read_.LMAPRCAU))
          this.w_IVASTU = NVL(cp_ToDate(_read_.LMIVASTU),cp_NullValue(_read_.LMIVASTU))
          this.w_IVACEE = NVL(cp_ToDate(_read_.LMIVACEE),cp_NullValue(_read_.LMIVACEE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        FWRITE (this.oParentObject.hFile ,right("00"+alltrim(str(this.w_CODATT)),2), 2)
        * --- Recupero e inserisco l' anno di competenza
        FWRITE(this.oParentObject.hFile,CORRVENT.PNCOMPET,4)
        * --- Recupero e inserisco il mese di competenza
        FWRITE(this.oParentObject.hFile,SUBSTR(dtos(CORRVENT.PNCOMIVA),5,2),2)
        * --- Giorno inizio competenza
        FWRITE(this.oParentObject.hFile,"01",2)
        * --- Sezione
        FWRITE(this.oParentObject.hFile,RIGHT("00"+ALLTRIM(STR(this.w_CODSEZ,2,0)),2),2)
        * --- Codice causale
        FWRITE (this.oParentObject.hFile , right(SPACE(10)+alltrim(this.w_APRCAU),10), 10)
        * --- Filler
        this.oParentObject.w_FILLER = " "
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,1)
        * --- Data registrazione
        FWRITE(this.oParentObject.hFile,dtos(CORRVENT.PNDATREG),8)
        * --- Filler
        this.oParentObject.w_FILLER = " "
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,1)
        * --- Scrittura del codice IVA
        FWRITE(this.oParentObject.hFile,"  ",2)
        this.oParentObject.w_FILLER = SPACE(4)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,4)
        * --- Importo Totale
        this.w_Valore = nvl(CORRVENT.PNIMPDAR,0) + nvl(CORRVENT.PNIMPAVE,0)
        this.w_Lunghezza = 16
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        FWRITE(this.oParentObject.hFile,this.w_VALORE,16)
        * --- Numero documentio dal al
        FWRITE(this.oParentObject.hFile,"00000000000000000000",20)
        * --- Scrittura codice norma
        FWRITE(this.oParentObject.hFile,"  ",2)
        * --- Codice centro di costo
        FWRITE(this.oParentObject.hFile,"   ",3)
        * --- Contropartita
        if CORRVENT.ANTIPSOT="V"
          * --- Leggo il codice di contropartita
          this.w_PNTIPCON = CORRVENT.PNTIPCON
          this.w_PNCODCON = CORRVENT.PNCODCON
        else
          * --- Avanzo al record successivo
          skip 1
          if NOT EOF()
            * --- Leggo il codice di contropartita
            this.w_PNTIPCON = CORRVENT.PNTIPCON
            this.w_PNCODCON = CORRVENT.PNCODCON
          endif
          * --- Ripristino
          skip -1
        endif
        * --- Decodifico la contropartita
        this.w_ANCODSTU = SPACE(9)
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODSTU"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODSTU;
            from (i_cTable) where;
                ANTIPCON = this.w_PNTIPCON;
                and ANCODICE = this.w_PNCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT CORRVENT
        if EMPTY(this.w_ANCODSTU)
          this.w_ANCODSTU = "000000000"
          this.w_STRINGA = ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.w_PNCODCON," "))
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        FWRITE(this.oParentObject.hFile,left(alltrim(this.w_ANCODSTU)+repl("0",9),9),9)
        * --- Codice Cassa
        if CORRVENT.PNTIPCLF="N"
          * --- Mappo il codice cassa e lo scrivo
          * --- Scrivo su file
          * --- Cerco le righe del dettaglio che contengono un conto di tipo cassa, banca, ...
          * --- Select from GSLMACOC_1
          do vq_exec with 'GSLMACOC_1',this,'_Curs_GSLMACOC_1','',.f.,.t.
          if used('_Curs_GSLMACOC_1')
            select _Curs_GSLMACOC_1
            locate for 1=1
            if not(eof())
            do while not(eof())
            this.w_ANCODSTU = "000000000"
            this.w_ANCODSTU = SPACE(9)
            this.w_TIPCAS = _Curs_GSLMACOC_1.PNTIPCON
            this.w_CODCAS = _Curs_GSLMACOC_1.PNCODCON
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANCODSTU"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCAS);
                    +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCAS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANCODSTU;
                from (i_cTable) where;
                    ANTIPCON = this.w_TIPCAS;
                    and ANCODICE = this.w_CODCAS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            SELECT CORRVENT
            if EMPTY(this.w_ANCODSTU)
              this.w_STRINGA = ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","              ",NVL(this.w_PNCODCON," "))
              FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
              FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
              this.oParentObject.w_ERRORE = .T.
            endif
            FWRITE(this.oParentObject.hFile,left(alltrim(this.w_ANCODSTU)+repl("0",9),9),9)
            SELECT _Curs_GSLMACOC_1
            exit
              select _Curs_GSLMACOC_1
              continue
            enddo
            else
              FWRITE(this.oParentObject.hFile, SPACE(9) ,9)
              select _Curs_GSLMACOC_1
            endif
            use
          endif
        else
          * --- Scrivo su file
          FWRITE(this.oParentObject.hFile, SPACE(9) ,9)
        endif
        SELECT CORRVENT
        * --- Codice CEE
        FWRITE(this.oParentObject.hFile,"  ",2)
        * --- Codice cliente
        this.oParentObject.w_FILLER = space(15)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,15)
        * --- Filler
        this.oParentObject.w_FILLER = space(53)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,53)
        * --- Codice sottoconto Cliente
        FWRITE(this.oParentObject.hFile, SPACE(9) ,9)
        * --- Progressivo registrazione
        FWRITE(this.oParentObject.hFile,RIGHT(repl("0",15)+ALLTRIM(CORRVENT.NUMERO),15),15)
        * --- Tipo corrispettivo
        FWRITE(this.oParentObject.hFile,"001",3)
        * --- Filler
        this.oParentObject.w_FILLER = SPACE(103)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,103)
        * --- Avanzo di due record
        if NOT EOF()
          skip 2
        endif
      enddo
      * --- Insert into STU_PNTT
      i_nConn=i_TableProp[this.STU_PNTT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'STU_PNTT','LMNUMTRA');
        +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.oParentObject.w_PROFIL,'LMNUMTR2',0)
        insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
           values (;
             this.w_PNSERIAL;
             ,this.oParentObject.w_PROFIL;
             ,0;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='GSLM_BCV: Scrittura in STU_PNTT'
        return
      endif
      SELECT CORRVENT
    enddo
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Trasformazione degli importi nel formato richiesto dal tracciato R.I.D.
    * --- UTILIZZO
    * --- Prima della chiamata a questa pagina del batch devono essere impostate le variabili:
    * --- w_Valore          = importo da convertire in stringa
    * --- w_Lunghezza  = lunghezza della stringa
    * --- Il risultato della conversione � disponibile nella variabile w_Valore.
    * --- CONVENZIONI
    * --- La virgola deve essere eliminata
    * --- Se la valuta � Euro le ultime due cifre rappresentano i decimali
    * --- Il valore deve essere allineato a destra
    * --- Non � richiesto il riempimento con 0 delle cifre non significative
    * --- Calcolo parte intera
    this.w_StrInt = alltrim( str( int(this.w_Valore), this.w_Lunghezza, 0 ) )
    * --- Calcolo parte decimale
    this.w_StrDec = ""
    if this.w_Decimali > 0
      this.w_StrDec = right( str( this.w_Valore, this.w_Lunghezza, this.w_Decimali ) , this.w_Decimali )
    endif
    * --- Risultato
    this.w_Valore = right( repl("0",this.w_Lunghezza) + this.w_StrInt + this.w_StrDec , this.w_Lunghezza )
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='STU_PNTT'
    this.cWorkTables[3]='ATTIDETT'
    this.cWorkTables[4]='ATTIMAST'
    this.cWorkTables[5]='AZIENDA'
    this.cWorkTables[6]='STU_TRAS'
    return(this.OpenAllTables(6))

  proc CloseCursors()
    if used('_Curs_GSLMACOC_1')
      use in _Curs_GSLMACOC_1
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
