* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_bic                                                        *
*              Importa codici studio                                           *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-03                                                      *
* Last revis.: 2000-04-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm_bic",oParentObject)
return(i_retval)

define class tgslm_bic as StdBatch
  * --- Local variables
  w_COD = space(2)
  w_DES = space(40)
  w_SIG = space(20)
  w_TIP = space(1)
  w_IVA = space(10)
  w_VEN = space(10)
  w_TVA = space(1)
  w_SE1 = space(10)
  w_INI = space(10)
  w_SE2 = space(10)
  w_FIN = space(10)
  w_ALI = space(1)
  w_COV = space(2)
  w_CODICE = space(10)
  w_ALL = space(1)
  w_CVA = space(2)
  w_MOD = space(10)
  w_AGR = space(10)
  w_DE1 = space(20)
  w_FOX1 = 0
  w_FOX2 = 0
  w_INIVAL = ctod("  /  /  ")
  w_FINVAL = ctod("  /  /  ")
  w_SECOLO = space(4)
  hFile = 0
  LUNRECORD = 0
  w_STATOINTRA = 0
  w_STATOIVA = 0
  w_STATONORMA = 0
  CF = space(10)
  PC = space(10)
  CC = space(10)
  MESS1 = space(10)
  MESS2 = space(10)
  MESS3 = space(10)
  w_STARTSEEK = 0
  * --- WorkFile variables
  STU_CIVA_idx=0
  STU_INTR_idx=0
  STU_NORM_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch di import codici norme,iva e intra
    this.LUNRECORD = 300
    * --- Variabili di stato
    this.w_STATOINTRA = 2
    this.w_STATOIVA = 2
    this.w_STATONORMA = 2
    * --- Lancio finestra di GetFile()
    if NOT EMPTY(this.oParentObject.w_FILENAME1)
      w_IMPFILE=this.oParentObject.w_FILENAME1
    else
      w_IMPFILE=GetFile("D","File di Import","Conferma",0,"Seleziona File di Import")
    endif
    if .not. empty(w_IMPFILE)
      * --- Apertura file di import
      this.hFile = FOPEN(w_IMPFILE,0)
      if (this.hFile<>-1)
        * --- Import codici INTRA
        if this.oParentObject.w_CODINT="S"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Import codici IVA
        if this.oParentObject.w_CODIVA="S"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Import codici NORMA
        if this.oParentObject.w_CODNOR="S"
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Chiusura del file
        FCLOSE(this.hFile)
        * --- Messaggio sullo stato dell'import
        if (this.w_STATOINTRA=1 .and. this.w_STATOIVA=1 .and. this.w_STATONORMA=1)
          ah_ErrorMsg("Import dei codici norme, IVA e INTRA eseguito con successo",,"")
        else
          * --- Preparazione del messaggio da visualizzare
          do case
            case this.w_STATOINTRA=0
              this.MESS1 = "Import codici intracomunitari= Fallito%0"
            case this.w_STATOINTRA=1
              this.MESS1 = "Import codici intracomunitari= Successo%0"
            otherwise
              this.MESS1 = "Import codici intracomunitari= Non eseguito%0"
          endcase
          do case
            case this.w_STATOIVA=0
              this.MESS2 = "Import codici IVA= Fallito%0"
            case this.w_STATOIVA=1
              this.MESS2 = "Import codici IVA= Successo%0"
            otherwise
              this.MESS2 = "Import codici IVA= Non eseguito%0"
          endcase
          do case
            case this.w_STATONORMA=0
              this.MESS3 = "Import codici norme=Fallito"
            case this.w_STATONORMA=1
              this.MESS3 = "Import codici norme=Successo"
            otherwise
              this.MESS3 = "Import codici norme=Non eseguito"
          endcase
          ah_ErrorMsg(this.MESS1+this.MESS2+this.MESS3,,"")
        endif
      else
        this.MESS1 = "Impossibile aprire il file di import"
        ah_ErrorMsg(this.MESS1,,"")
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- IMPORT CODICI INTRA
    * --- Posiziono il file all'inizio del recorde che identifica i codici INTRA
    this.w_STARTSEEK = 0
    this.w_CODICE = FREAD(this.hFile,3)
    FSEEK(this.hFile,-3,1)
    do while ((this.w_CODICE<>"CEE") .and. (.not. FEOF(this.hFile)))
      * --- Skippo un record
      FSEEK(this.hFile,this.LUNRECORD,this.w_STARTSEEK)
      * --- Rileggo il codice
      this.w_CODICE = FREAD(this.hFile,3)
      FSEEK(this.hFile,-3,1)
      this.w_STARTSEEK = 1
    enddo
    if this.w_CODICE<>"CEE"
      this.w_STATOINTRA=0
      return
    endif
    * --- Ciclo di lettura
    do while ((this.w_CODICE="CEE") .and. (.not. FEOF(this.hFile)))
      * --- Leggo il nuovo codice
      this.w_CODICE = FREAD(this.hFile,3)
      if this.w_CODICE<>"CEE"
        FSEEK(this.hFile,-3,1)
        exit
      endif
      * --- Leggo il codice CEE
      this.w_COD = FREAD(this.hFile,2)
      * --- Messaggio a Video
      ah_Msg("Import codice INTRA: %1",.T.,.F.,.F.,this.w_COD)
      * --- Skippo 6 caratteri per tracciato tipo CGTABSEC
      FREAD(this.hFile,6)
      * --- Leggo la descrizione
      this.w_DES = FREAD(this.hFile,30)
      * --- Leggo la descrizione sigla
      this.w_SIG = FREAD(this.hFile,10)
      * --- Leggo il tipo operazione
      this.w_TIP = FREAD(this.hFile,1)
      * --- Skippo il filler
      FREAD(this.hFile,248)
      * --- Inserisco in tabella
      * --- Scrivo il record
      * --- Try
      local bErr_037E6F20
      bErr_037E6F20=bTrsErr
      this.Try_037E6F20()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into STU_INTR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.STU_INTR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_INTR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_INTR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LMDESOPE ="+cp_NullLink(cp_ToStrODBC(this.w_DES),'STU_INTR','LMDESOPE');
          +",LMDESABB ="+cp_NullLink(cp_ToStrODBC(this.w_SIG),'STU_INTR','LMDESABB');
          +",LMTIPOPE ="+cp_NullLink(cp_ToStrODBC(this.w_TIP),'STU_INTR','LMTIPOPE');
              +i_ccchkf ;
          +" where ";
              +"LMCODOPE = "+cp_ToStrODBC(this.w_COD);
                 )
        else
          update (i_cTable) set;
              LMDESOPE = this.w_DES;
              ,LMDESABB = this.w_SIG;
              ,LMTIPOPE = this.w_TIP;
              &i_ccchkf. ;
           where;
              LMCODOPE = this.w_COD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Fallita la scrittura in Operazioni Intra Studio'
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_037E6F20
      * --- End
    enddo
    * --- Successo operazione
    this.w_STATOINTRA = 1
  endproc
  proc Try_037E6F20()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_INTR
    i_nConn=i_TableProp[this.STU_INTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_INTR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_INTR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMCODOPE"+",LMDESOPE"+",LMDESABB"+",LMTIPOPE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_COD),'STU_INTR','LMCODOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DES),'STU_INTR','LMDESOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SIG),'STU_INTR','LMDESABB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIP),'STU_INTR','LMTIPOPE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMCODOPE',this.w_COD,'LMDESOPE',this.w_DES,'LMDESABB',this.w_SIG,'LMTIPOPE',this.w_TIP)
      insert into (i_cTable) (LMCODOPE,LMDESOPE,LMDESABB,LMTIPOPE &i_ccchkf. );
         values (;
           this.w_COD;
           ,this.w_DES;
           ,this.w_SIG;
           ,this.w_TIP;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Fallito inserimento in Operazioni Intra Studio'
      return
    endif
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- IMPORT CODICI IVA
    * --- Posiziono il file all'inizio del recorde che identifica i codici INTRA
    this.w_STARTSEEK = 0
    this.w_CODICE = FREAD(this.hFile,3)
    FSEEK(this.hFile,-3,1)
    do while ((this.w_CODICE<>"IVA") .and. (.not. FEOF(this.hFile)))
      * --- Skippo un record
      FSEEK(this.hFile,this.LUNRECORD,this.w_STARTSEEK)
      * --- Rileggo il codice
      this.w_CODICE = FREAD(this.hFile,3)
      FSEEK(this.hFile,-3,1)
      this.w_STARTSEEK = 1
    enddo
    if this.w_CODICE<>"IVA"
      this.w_STATOIVA=0
      return
    endif
    * --- Ciclo di lettura
    do while ((this.w_CODICE="IVA") .and. (.not. FEOF(this.hFile)))
      * --- Leggo il nuovo codice
      this.w_CODICE = FREAD(this.hFile,3)
      if this.w_CODICE<>"IVA"
        FSEEK(this.hFile,-3,1)
        exit
      endif
      * --- Leggo il codice IVA
      this.w_COD = FREAD(this.hFile,2)
      * --- Messaggio a Video
      ah_Msg("Import codice IVA: %1",.T.,.F.,.F.,this.w_COD)
      * --- Skippo 4 caratteri
      FREAD(this.hFile,4)
      * --- Leggo la descrizione
      this.w_DES = FREAD(this.hFile,30)
      * --- Leggo la percentuale IVA
      this.w_IVA = FREAD(this.hFile,5)
      * --- Leggo la percentuale IVA in ventialzione
      this.w_VEN = FREAD(this.hFile,5)
      * --- Leggo il test validit�
      this.w_TVA = FREAD(this.hFile,1)
      * --- Leggo la data inizio attivita (secolo+anno+mese+giorno)
      this.w_SE1 = FREAD(this.hFile,1)
      this.w_INI = FREAD(this.hFile,6)
      * --- Leggo la data fine attivita (secolo+anno+mese+giorno)
      this.w_SE2 = FREAD(this.hFile,1)
      this.w_FIN = FREAD(this.hFile,6)
      * --- Leggo il flag tipo aliquota
      this.w_ALI = FREAD(this.hFile,1)
      * --- Leggo il codice ventilazioni
      this.w_COV = FREAD(this.hFile,2)
      * --- Filler
      FREAD(this.hFile,233)
      * --- Inserisco in tabella
      this.w_FOX1 = VAL(LEFT(this.w_IVA,3)+","+RIGHT(this.w_IVA,2))
      this.w_FOX2 = VAL(LEFT(this.w_VEN,3)+","+RIGHT(this.w_VEN,2))
      if val(this.w_SE1)=0
        this.w_SECOLO = "19"+left(this.w_INI,2)
      else
        this.w_SECOLO = "20"+left(this.w_INI,2)
      endif
      this.w_INIVAL = IIF(val(substr(this.w_INI,3,2))<>0 and val(substr(this.w_INI,5,2))<>0, CTOD(substr(this.w_INI,5,2)+"/"+substr(this.w_INI,3,2)+"/"+this.w_SECOLO) , CTOD("  "))
      if val(this.w_SE2)=0
        this.w_SECOLO = "19"+left(this.w_FIN,2)
      else
        this.w_SECOLO = "20"+left(this.w_FIN,2)
      endif
      this.w_FINVAL = IIF(val(substr(this.w_FIN,3,2))<>0 and val(substr(this.w_FIN,5,2))<>0, CTOD(substr(this.w_FIN,5,2)+"/"+substr(this.w_FIN,3,2)+"/"+this.w_SECOLO) , CTOD("  "))
      * --- Scrivo il record
      * --- Try
      local bErr_03692D60
      bErr_03692D60=bTrsErr
      this.Try_03692D60()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into STU_CIVA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.STU_CIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_CIVA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_CIVA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LMDESIVA ="+cp_NullLink(cp_ToStrODBC(this.w_DES),'STU_CIVA','LMDESIVA');
          +",LMPERIVA ="+cp_NullLink(cp_ToStrODBC(this.w_FOX1),'STU_CIVA','LMPERIVA');
          +",LMCODVEN ="+cp_NullLink(cp_ToStrODBC(this.w_COV),'STU_CIVA','LMCODVEN');
          +",LMPERVEN ="+cp_NullLink(cp_ToStrODBC(this.w_FOX2),'STU_CIVA','LMPERVEN');
          +",LMTIPREG ="+cp_NullLink(cp_ToStrODBC(this.w_TVA),'STU_CIVA','LMTIPREG');
          +",LMINIVAL ="+cp_NullLink(cp_ToStrODBC(this.w_INIVAL),'STU_CIVA','LMINIVAL');
          +",LMFINVAL ="+cp_NullLink(cp_ToStrODBC(this.w_FINVAL),'STU_CIVA','LMFINVAL');
          +",LMTIPCOD ="+cp_NullLink(cp_ToStrODBC(this.w_ALI),'STU_CIVA','LMTIPCOD');
              +i_ccchkf ;
          +" where ";
              +"LMCODIVA = "+cp_ToStrODBC(this.w_COD);
                 )
        else
          update (i_cTable) set;
              LMDESIVA = this.w_DES;
              ,LMPERIVA = this.w_FOX1;
              ,LMCODVEN = this.w_COV;
              ,LMPERVEN = this.w_FOX2;
              ,LMTIPREG = this.w_TVA;
              ,LMINIVAL = this.w_INIVAL;
              ,LMFINVAL = this.w_FINVAL;
              ,LMTIPCOD = this.w_ALI;
              &i_ccchkf. ;
           where;
              LMCODIVA = this.w_COD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Fallita la scrittura in Codici IVA Studio'
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_03692D60
      * --- End
    enddo
    * --- Inserisco in tabella il codice iva XX
    this.w_COD = ALLTRIM("XX")
    this.w_DES = Ah_MsgFormat("Codice IVA non definito")
    * --- Scrivo il record
    * --- Try
    local bErr_037F1450
    bErr_037F1450=bTrsErr
    this.Try_037F1450()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
      * --- Write into STU_CIVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.STU_CIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STU_CIVA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_CIVA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"LMDESIVA ="+cp_NullLink(cp_ToStrODBC(this.w_DES),'STU_CIVA','LMDESIVA');
            +i_ccchkf ;
        +" where ";
            +"LMCODIVA = "+cp_ToStrODBC(this.w_COD);
               )
      else
        update (i_cTable) set;
            LMDESIVA = this.w_DES;
            &i_ccchkf. ;
         where;
            LMCODIVA = this.w_COD;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error='Fallita la scrittura in Codici IVA Studio'
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_037F1450
    * --- End
    * --- Successo operazione
    this.w_STATOIVA = 1
  endproc
  proc Try_03692D60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_CIVA
    i_nConn=i_TableProp[this.STU_CIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_CIVA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_CIVA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMCODIVA"+",LMDESIVA"+",LMPERIVA"+",LMCODVEN"+",LMPERVEN"+",LMTIPREG"+",LMINIVAL"+",LMFINVAL"+",LMTIPCOD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_COD),'STU_CIVA','LMCODIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DES),'STU_CIVA','LMDESIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FOX1),'STU_CIVA','LMPERIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_COV),'STU_CIVA','LMCODVEN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FOX2),'STU_CIVA','LMPERVEN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TVA),'STU_CIVA','LMTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_INIVAL),'STU_CIVA','LMINIVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FINVAL),'STU_CIVA','LMFINVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ALI),'STU_CIVA','LMTIPCOD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMCODIVA',this.w_COD,'LMDESIVA',this.w_DES,'LMPERIVA',this.w_FOX1,'LMCODVEN',this.w_COV,'LMPERVEN',this.w_FOX2,'LMTIPREG',this.w_TVA,'LMINIVAL',this.w_INIVAL,'LMFINVAL',this.w_FINVAL,'LMTIPCOD',this.w_ALI)
      insert into (i_cTable) (LMCODIVA,LMDESIVA,LMPERIVA,LMCODVEN,LMPERVEN,LMTIPREG,LMINIVAL,LMFINVAL,LMTIPCOD &i_ccchkf. );
         values (;
           this.w_COD;
           ,this.w_DES;
           ,this.w_FOX1;
           ,this.w_COV;
           ,this.w_FOX2;
           ,this.w_TVA;
           ,this.w_INIVAL;
           ,this.w_FINVAL;
           ,this.w_ALI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Chiave gi� utilizzata'
      return
    endif
    return
  proc Try_037F1450()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_CIVA
    i_nConn=i_TableProp[this.STU_CIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_CIVA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_CIVA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMCODIVA"+",LMDESIVA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_COD),'STU_CIVA','LMCODIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DES),'STU_CIVA','LMDESIVA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMCODIVA',this.w_COD,'LMDESIVA',this.w_DES)
      insert into (i_cTable) (LMCODIVA,LMDESIVA &i_ccchkf. );
         values (;
           this.w_COD;
           ,this.w_DES;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Chiave gi� utilizzata'
      return
    endif
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- IMPORT CODICI NORME
    * --- Posiziono il file all'inizio del recorde che identifica i codici INTRA
    this.w_STARTSEEK = 0
    this.w_CODICE = FREAD(this.hFile,3)
    FSEEK(this.hFile,-3,1)
    do while ((this.w_CODICE<>"NOR") .and. (.not. FEOF(this.hFile)))
      * --- Skippo un record
      FSEEK(this.hFile,this.LUNRECORD,this.w_STARTSEEK)
      * --- Rileggo il codice
      this.w_CODICE = FREAD(this.hFile,3)
      FSEEK(this.hFile,-3,1)
      this.w_STARTSEEK = 1
    enddo
    if this.w_CODICE<>"NOR"
      this.w_STATONORMA=0
      return
    endif
    * --- Ciclo di lettura
    do while ((this.w_CODICE="NOR") .and. (.not. FEOF(this.hFile)))
      * --- Leggo il nuovo codice
      this.w_CODICE = FREAD(this.hFile,3)
      if this.w_CODICE<>"NOR"
        FSEEK(this.hFile,-3,1)
        exit
      endif
      * --- Leggo il codice NORMA
      this.w_COD = FREAD(this.hFile,2)
      * --- Messaggio a Video
      ah_Msg("Import codice norma: %1",.T.,.F.,.F.,this.w_COD)
      * --- Skippo 4 caratteri
      FREAD(this.hFile,4)
      * --- Leggo la descrizione
      this.w_DES = FREAD(this.hFile,30)
      * --- Leggo la percentuale indetraibilit�
      this.w_IVA = FREAD(this.hFile,5)
      * --- Leggo la percentuale ult. indetraibilita
      this.w_VEN = FREAD(this.hFile,5)
      * --- Leggo allegato
      this.w_ALL = FREAD(this.hFile,1)
      * --- Leggo il codice IVA
      this.w_CVA = FREAD(this.hFile,2)
      * --- Leggo il test validit�
      this.w_TVA = FREAD(this.hFile,1)
      * --- Leggo il modo registrazione
      this.w_MOD = FREAD(this.hFile,1)
      * --- Leggo la norma agricoltura
      this.w_AGR = FREAD(this.hFile,1)
      * --- Leggo l'ulteriore descrizione
      this.w_DE1 = FREAD(this.hFile,10)
      * --- Filler
      FREAD(this.hFile,235)
      * --- Inserisco in tabella
      this.w_FOX1 = VAL(LEFT(this.w_IVA,3)+","+RIGHT(this.w_IVA,2))
      this.w_FOX2 = VAL(LEFT(this.w_VEN,3)+","+RIGHT(this.w_VEN,2))
      * --- Scrivo il record
      * --- Try
      local bErr_03A212E8
      bErr_03A212E8=bTrsErr
      this.Try_03A212E8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        * --- Write into STU_NORM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.STU_NORM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_NORM_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_NORM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LMDESNOR ="+cp_NullLink(cp_ToStrODBC(this.w_DES),'STU_NORM','LMDESNOR');
          +",LMDEANOR ="+cp_NullLink(cp_ToStrODBC(this.w_DE1),'STU_NORM','LMDEANOR');
          +",LMINDETR ="+cp_NullLink(cp_ToStrODBC(this.w_FOX1),'STU_NORM','LMINDETR');
          +",LMULTDET ="+cp_NullLink(cp_ToStrODBC(this.w_FOX2),'STU_NORM','LMULTDET');
          +",LMCODIVA ="+cp_NullLink(cp_ToStrODBC(this.w_CVA),'STU_NORM','LMCODIVA');
          +",LMTIPOLO ="+cp_NullLink(cp_ToStrODBC(this.w_TVA),'STU_NORM','LMTIPOLO');
          +",LMALLEGA ="+cp_NullLink(cp_ToStrODBC(this.w_ALL),'STU_NORM','LMALLEGA');
              +i_ccchkf ;
          +" where ";
              +"LMCODNOR = "+cp_ToStrODBC(this.w_COD);
                 )
        else
          update (i_cTable) set;
              LMDESNOR = this.w_DES;
              ,LMDEANOR = this.w_DE1;
              ,LMINDETR = this.w_FOX1;
              ,LMULTDET = this.w_FOX2;
              ,LMCODIVA = this.w_CVA;
              ,LMTIPOLO = this.w_TVA;
              ,LMALLEGA = this.w_ALL;
              &i_ccchkf. ;
           where;
              LMCODNOR = this.w_COD;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error='Fallita la scrittura in Codici Norme Studio'
          return
        endif
      endif
      bTrsErr=bTrsErr or bErr_03A212E8
      * --- End
    enddo
    * --- Successo operazione
    this.w_STATONORMA = 1
  endproc
  proc Try_03A212E8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_NORM
    i_nConn=i_TableProp[this.STU_NORM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_NORM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_NORM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMCODNOR"+",LMDESNOR"+",LMDEANOR"+",LMINDETR"+",LMULTDET"+",LMCODIVA"+",LMTIPOLO"+",LMALLEGA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_COD),'STU_NORM','LMCODNOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DES),'STU_NORM','LMDESNOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DE1),'STU_NORM','LMDEANOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FOX1),'STU_NORM','LMINDETR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FOX2),'STU_NORM','LMULTDET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CVA),'STU_NORM','LMCODIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TVA),'STU_NORM','LMTIPOLO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ALL),'STU_NORM','LMALLEGA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMCODNOR',this.w_COD,'LMDESNOR',this.w_DES,'LMDEANOR',this.w_DE1,'LMINDETR',this.w_FOX1,'LMULTDET',this.w_FOX2,'LMCODIVA',this.w_CVA,'LMTIPOLO',this.w_TVA,'LMALLEGA',this.w_ALL)
      insert into (i_cTable) (LMCODNOR,LMDESNOR,LMDEANOR,LMINDETR,LMULTDET,LMCODIVA,LMTIPOLO,LMALLEGA &i_ccchkf. );
         values (;
           this.w_COD;
           ,this.w_DES;
           ,this.w_DE1;
           ,this.w_FOX1;
           ,this.w_FOX2;
           ,this.w_CVA;
           ,this.w_TVA;
           ,this.w_ALL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Chiave gi� utilizzata'
      return
    endif
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='STU_CIVA'
    this.cWorkTables[2]='STU_INTR'
    this.cWorkTables[3]='STU_NORM'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
