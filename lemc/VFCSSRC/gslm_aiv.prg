* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_aiv                                                        *
*              Codici IVA studio                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_33]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-17                                                      *
* Last revis.: 2008-09-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gslm_aiv
* Esco se il modulo � in semplificata
IF NOT LMAVANZATO()
  Ah_ErrorMsg("La funzione � attiva con il modulo in modalit� avanzata")
  return
ENDIF
* --- Fine Area Manuale
return(createobject("tgslm_aiv"))

* --- Class definition
define class tgslm_aiv as StdForm
  Top    = 54
  Left   = 72

  * --- Standard Properties
  Width  = 539
  Height = 186+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-09-23"
  HelpContextID=9026665
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  STU_CIVA_IDX = 0
  cFile = "STU_CIVA"
  cKeySelect = "LMCODIVA"
  cKeyWhere  = "LMCODIVA=this.w_LMCODIVA"
  cKeyWhereODBC = '"LMCODIVA="+cp_ToStrODBC(this.w_LMCODIVA)';

  cKeyWhereODBCqualified = '"STU_CIVA.LMCODIVA="+cp_ToStrODBC(this.w_LMCODIVA)';

  cPrg = "gslm_aiv"
  cComment = "Codici IVA studio"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LMCODIVA = space(2)
  w_LMDESIVA = space(40)
  w_LMPERIVA = 0
  w_LMCODVEN = space(2)
  w_LMPERVEN = 0
  w_LMINIVAL = ctod('  /  /  ')
  w_LMFINVAL = ctod('  /  /  ')
  w_LMTIPREG = space(1)
  w_LMTIPCOD = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'STU_CIVA','gslm_aiv')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslm_aivPag1","gslm_aiv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Codice iva studio")
      .Pages(1).HelpContextID = 137664696
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLMCODIVA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='STU_CIVA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.STU_CIVA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.STU_CIVA_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_LMCODIVA = NVL(LMCODIVA,space(2))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from STU_CIVA where LMCODIVA=KeySet.LMCODIVA
    *
    i_nConn = i_TableProp[this.STU_CIVA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STU_CIVA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('STU_CIVA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "STU_CIVA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' STU_CIVA '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LMCODIVA',this.w_LMCODIVA  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_LMCODIVA = NVL(LMCODIVA,space(2))
        .w_LMDESIVA = NVL(LMDESIVA,space(40))
        .w_LMPERIVA = NVL(LMPERIVA,0)
        .w_LMCODVEN = NVL(LMCODVEN,space(2))
        .w_LMPERVEN = NVL(LMPERVEN,0)
        .w_LMINIVAL = NVL(cp_ToDate(LMINIVAL),ctod("  /  /  "))
        .w_LMFINVAL = NVL(cp_ToDate(LMFINVAL),ctod("  /  /  "))
        .w_LMTIPREG = NVL(LMTIPREG,space(1))
        .w_LMTIPCOD = NVL(LMTIPCOD,space(1))
        cp_LoadRecExtFlds(this,'STU_CIVA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_LMCODIVA = space(2)
      .w_LMDESIVA = space(40)
      .w_LMPERIVA = 0
      .w_LMCODVEN = space(2)
      .w_LMPERVEN = 0
      .w_LMINIVAL = ctod("  /  /  ")
      .w_LMFINVAL = ctod("  /  /  ")
      .w_LMTIPREG = space(1)
      .w_LMTIPCOD = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,7,.f.)
        .w_LMTIPREG = 'X'
        .w_LMTIPCOD = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'STU_CIVA')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oLMCODIVA_1_1.enabled = i_bVal
      .Page1.oPag.oLMDESIVA_1_3.enabled = i_bVal
      .Page1.oPag.oLMPERIVA_1_4.enabled = i_bVal
      .Page1.oPag.oLMCODVEN_1_6.enabled = i_bVal
      .Page1.oPag.oLMPERVEN_1_8.enabled = i_bVal
      .Page1.oPag.oLMINIVAL_1_11.enabled = i_bVal
      .Page1.oPag.oLMFINVAL_1_13.enabled = i_bVal
      .Page1.oPag.oLMTIPREG_1_15.enabled = i_bVal
      .Page1.oPag.oLMTIPCOD_1_16.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oLMCODIVA_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oLMCODIVA_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'STU_CIVA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.STU_CIVA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCODIVA,"LMCODIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMDESIVA,"LMDESIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMPERIVA,"LMPERIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCODVEN,"LMCODVEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMPERVEN,"LMPERVEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMINIVAL,"LMINIVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMFINVAL,"LMFINVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMTIPREG,"LMTIPREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMTIPCOD,"LMTIPCOD",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.STU_CIVA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STU_CIVA_IDX,2])
    i_lTable = "STU_CIVA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.STU_CIVA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.STU_CIVA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_CIVA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.STU_CIVA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into STU_CIVA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'STU_CIVA')
        i_extval=cp_InsertValODBCExtFlds(this,'STU_CIVA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(LMCODIVA,LMDESIVA,LMPERIVA,LMCODVEN,LMPERVEN"+;
                  ",LMINIVAL,LMFINVAL,LMTIPREG,LMTIPCOD "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_LMCODIVA)+;
                  ","+cp_ToStrODBC(this.w_LMDESIVA)+;
                  ","+cp_ToStrODBC(this.w_LMPERIVA)+;
                  ","+cp_ToStrODBC(this.w_LMCODVEN)+;
                  ","+cp_ToStrODBC(this.w_LMPERVEN)+;
                  ","+cp_ToStrODBC(this.w_LMINIVAL)+;
                  ","+cp_ToStrODBC(this.w_LMFINVAL)+;
                  ","+cp_ToStrODBC(this.w_LMTIPREG)+;
                  ","+cp_ToStrODBC(this.w_LMTIPCOD)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'STU_CIVA')
        i_extval=cp_InsertValVFPExtFlds(this,'STU_CIVA')
        cp_CheckDeletedKey(i_cTable,0,'LMCODIVA',this.w_LMCODIVA)
        INSERT INTO (i_cTable);
              (LMCODIVA,LMDESIVA,LMPERIVA,LMCODVEN,LMPERVEN,LMINIVAL,LMFINVAL,LMTIPREG,LMTIPCOD  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_LMCODIVA;
                  ,this.w_LMDESIVA;
                  ,this.w_LMPERIVA;
                  ,this.w_LMCODVEN;
                  ,this.w_LMPERVEN;
                  ,this.w_LMINIVAL;
                  ,this.w_LMFINVAL;
                  ,this.w_LMTIPREG;
                  ,this.w_LMTIPCOD;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.STU_CIVA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_CIVA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.STU_CIVA_IDX,i_nConn)
      *
      * update STU_CIVA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'STU_CIVA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " LMDESIVA="+cp_ToStrODBC(this.w_LMDESIVA)+;
             ",LMPERIVA="+cp_ToStrODBC(this.w_LMPERIVA)+;
             ",LMCODVEN="+cp_ToStrODBC(this.w_LMCODVEN)+;
             ",LMPERVEN="+cp_ToStrODBC(this.w_LMPERVEN)+;
             ",LMINIVAL="+cp_ToStrODBC(this.w_LMINIVAL)+;
             ",LMFINVAL="+cp_ToStrODBC(this.w_LMFINVAL)+;
             ",LMTIPREG="+cp_ToStrODBC(this.w_LMTIPREG)+;
             ",LMTIPCOD="+cp_ToStrODBC(this.w_LMTIPCOD)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'STU_CIVA')
        i_cWhere = cp_PKFox(i_cTable  ,'LMCODIVA',this.w_LMCODIVA  )
        UPDATE (i_cTable) SET;
              LMDESIVA=this.w_LMDESIVA;
             ,LMPERIVA=this.w_LMPERIVA;
             ,LMCODVEN=this.w_LMCODVEN;
             ,LMPERVEN=this.w_LMPERVEN;
             ,LMINIVAL=this.w_LMINIVAL;
             ,LMFINVAL=this.w_LMFINVAL;
             ,LMTIPREG=this.w_LMTIPREG;
             ,LMTIPCOD=this.w_LMTIPCOD;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.STU_CIVA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_CIVA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.STU_CIVA_IDX,i_nConn)
      *
      * delete STU_CIVA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'LMCODIVA',this.w_LMCODIVA  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.STU_CIVA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STU_CIVA_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLMCODIVA_1_1.value==this.w_LMCODIVA)
      this.oPgFrm.Page1.oPag.oLMCODIVA_1_1.value=this.w_LMCODIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oLMDESIVA_1_3.value==this.w_LMDESIVA)
      this.oPgFrm.Page1.oPag.oLMDESIVA_1_3.value=this.w_LMDESIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oLMPERIVA_1_4.value==this.w_LMPERIVA)
      this.oPgFrm.Page1.oPag.oLMPERIVA_1_4.value=this.w_LMPERIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oLMCODVEN_1_6.value==this.w_LMCODVEN)
      this.oPgFrm.Page1.oPag.oLMCODVEN_1_6.value=this.w_LMCODVEN
    endif
    if not(this.oPgFrm.Page1.oPag.oLMPERVEN_1_8.value==this.w_LMPERVEN)
      this.oPgFrm.Page1.oPag.oLMPERVEN_1_8.value=this.w_LMPERVEN
    endif
    if not(this.oPgFrm.Page1.oPag.oLMINIVAL_1_11.value==this.w_LMINIVAL)
      this.oPgFrm.Page1.oPag.oLMINIVAL_1_11.value=this.w_LMINIVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oLMFINVAL_1_13.value==this.w_LMFINVAL)
      this.oPgFrm.Page1.oPag.oLMFINVAL_1_13.value=this.w_LMFINVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oLMTIPREG_1_15.RadioValue()==this.w_LMTIPREG)
      this.oPgFrm.Page1.oPag.oLMTIPREG_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLMTIPCOD_1_16.RadioValue()==this.w_LMTIPCOD)
      this.oPgFrm.Page1.oPag.oLMTIPCOD_1_16.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'STU_CIVA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_LMCODIVA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLMCODIVA_1_1.SetFocus()
            i_bnoObbl = !empty(.w_LMCODIVA)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgslm_aivPag1 as StdContainer
  Width  = 535
  height = 186
  stdWidth  = 535
  stdheight = 186
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLMCODIVA_1_1 as StdField with uid="TBYDFBWGAL",rtseq=1,rtrep=.f.,;
    cFormVar = "w_LMCODIVA", cQueryName = "LMCODIVA",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice IVA",;
    HelpContextID = 218744311,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=48, Left=150, Top=17, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  add object oLMDESIVA_1_3 as StdField with uid="ULHJJZCUDC",rtseq=2,rtrep=.f.,;
    cFormVar = "w_LMDESIVA", cQueryName = "LMDESIVA",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione IVA",;
    HelpContextID = 233821687,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=200, Top=17, InputMask=replicate('X',40)

  add object oLMPERIVA_1_4 as StdField with uid="YLOGNHGTEQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_LMPERIVA", cQueryName = "LMPERIVA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale IVA",;
    HelpContextID = 232822263,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=150, Top=46, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oLMCODVEN_1_6 as StdField with uid="ARZVGIWSWC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_LMCODVEN", cQueryName = "LMCODVEN",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice ventilazione",;
    HelpContextID = 168412676,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=150, Top=75, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2)

  add object oLMPERVEN_1_8 as StdField with uid="ASKCNGWDCX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_LMPERVEN", cQueryName = "LMPERVEN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale ventilazione",;
    HelpContextID = 182490628,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=150, Top=104, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oLMINIVAL_1_11 as StdField with uid="SVBSPLNQPD",rtseq=6,rtrep=.f.,;
    cFormVar = "w_LMINIVAL", cQueryName = "LMINIVAL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio valuta",;
    HelpContextID = 173614594,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=150, Top=133

  add object oLMFINVAL_1_13 as StdField with uid="DURQBKPCKG",rtseq=7,rtrep=.f.,;
    cFormVar = "w_LMFINVAL", cQueryName = "LMFINVAL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine valuta",;
    HelpContextID = 178517506,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=150, Top=162


  add object oLMTIPREG_1_15 as StdCombo with uid="ZBZWFIBNDU",rtseq=8,rtrep=.f.,left=414,top=75,width=114,height=21;
    , ToolTipText = "Tipo registro IVA";
    , HelpContextID = 113563133;
    , cFormVar="w_LMTIPREG",RowSource=""+"Acquisti,"+"Vendite,"+"Entrambi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oLMTIPREG_1_15.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'V',;
    iif(this.value =3,'X',;
    space(1)))))
  endfunc
  func oLMTIPREG_1_15.GetRadio()
    this.Parent.oContained.w_LMTIPREG = this.RadioValue()
    return .t.
  endfunc

  func oLMTIPREG_1_15.SetRadio()
    this.Parent.oContained.w_LMTIPREG=trim(this.Parent.oContained.w_LMTIPREG)
    this.value = ;
      iif(this.Parent.oContained.w_LMTIPREG=='A',1,;
      iif(this.Parent.oContained.w_LMTIPREG=='V',2,;
      iif(this.Parent.oContained.w_LMTIPREG=='X',3,;
      0)))
  endfunc


  add object oLMTIPCOD_1_16 as StdCombo with uid="GMMNJGTKLC",value=3,rtseq=9,rtrep=.f.,left=414,top=104,width=114,height=21;
    , ToolTipText = "Tipo aliquota IVA";
    , HelpContextID = 130340346;
    , cFormVar="w_LMTIPCOD",RowSource=""+"Agricola,"+"Non agricola,"+"Mista", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oLMTIPCOD_1_16.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'N',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oLMTIPCOD_1_16.GetRadio()
    this.Parent.oContained.w_LMTIPCOD = this.RadioValue()
    return .t.
  endfunc

  func oLMTIPCOD_1_16.SetRadio()
    this.Parent.oContained.w_LMTIPCOD=trim(this.Parent.oContained.w_LMTIPCOD)
    this.value = ;
      iif(this.Parent.oContained.w_LMTIPCOD=='A',1,;
      iif(this.Parent.oContained.w_LMTIPCOD=='N',2,;
      iif(this.Parent.oContained.w_LMTIPCOD=='',3,;
      0)))
  endfunc

  add object oStr_1_2 as StdString with uid="AXVGSWXDWC",Visible=.t., Left=4, Top=17,;
    Alignment=1, Width=141, Height=18,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="GTFTEGCBAM",Visible=.t., Left=4, Top=46,;
    Alignment=1, Width=141, Height=18,;
    Caption="Percentuale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="CPRRSHJFYA",Visible=.t., Left=4, Top=75,;
    Alignment=1, Width=141, Height=18,;
    Caption="Codice ventilazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="MIBNJIANJQ",Visible=.t., Left=4, Top=104,;
    Alignment=1, Width=141, Height=18,;
    Caption="Percentuale ventilazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="XEMJQUPUDR",Visible=.t., Left=245, Top=75,;
    Alignment=1, Width=165, Height=18,;
    Caption="Tipologia registro IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="VTSVOFWSRJ",Visible=.t., Left=4, Top=133,;
    Alignment=1, Width=141, Height=18,;
    Caption="Data di inizio valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="DTHCBRDOXF",Visible=.t., Left=4, Top=162,;
    Alignment=1, Width=141, Height=18,;
    Caption="Data di fine valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="ABJMCIPRPX",Visible=.t., Left=299, Top=104,;
    Alignment=1, Width=111, Height=18,;
    Caption="Tipo aliquota:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslm_aiv','STU_CIVA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LMCODIVA=STU_CIVA.LMCODIVA";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
