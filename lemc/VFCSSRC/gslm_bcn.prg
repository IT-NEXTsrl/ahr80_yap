* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_bcn                                                        *
*              Export corrispettivi normali                                    *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-24                                                      *
* Last revis.: 2000-04-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm_bcn",oParentObject)
return(i_retval)

define class tgslm_bcn as StdBatch
  * --- Local variables
  w_REGNOVALIDA = .f.
  w_STRINGA = space(120)
  w_ACTUALPOS = 0
  w_PNSERIAL = space(10)
  w_OLDPNSERIAL = space(10)
  w_TOTDOC = 0
  w_ANCODSTU = space(6)
  w_APRNORM = space(2)
  w_APRCAU = space(3)
  w_IVASTU = space(2)
  w_IVASTUNOR = space(2)
  w_IVACEE = space(2)
  w_ESCI = .f.
  w_OKTRAS = .f.
  w_NOTRAS = 0
  w_PNCODCAU = space(5)
  w_IVCODIVA = space(5)
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_PNTIPCON = space(1)
  w_PNCODCON = space(15)
  w_TIPCAS = space(1)
  w_CODCAS = space(15)
  w_TOTCAS = 0
  * --- WorkFile variables
  CONTI_idx=0
  PNT_DETT_idx=0
  STU_NORM_idx=0
  STU_PNTT_idx=0
  STU_TPNT_idx=0
  STU_TRAS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT CORRISPETTIVI NORMALI
    CREATE CURSOR MOVDACOR (PNSERIAL C(10), PNNUMRER N(6), ;
    PNDATREG D(8), PNCODCAU C(5), PNCOMPET C(4), PNTIPDOC C(2), ;
    PNNUMDOC N(6), PNALFDOC C(2), PNDATDOC D(8), PNNUMPRO N(6), ;
    PNALFPRO C(2), PNCODVAL C(3), PNVALNAZ C(3), PNTOTDOC N(18,4), ;
    PNTIPCLF C(1), PNCODCLF C(15), PNCOMIVA D(8), PNTIPREG C(1), ;
    PNNUMREG N(2), PNCAURIG C(5), PNDESRIG C(50), PNTIPCON C(1), ;
    PNIMPDAR N(18,4), PNCODCON C(15), PNIMPAVE N(18,4), ANTIPSOT C(1))
    this.w_OLDPNSERIAL = "  "
    SELECT CORRNORM
    GO TOP
    * --- Ciclo sul cursore dei corrispettivi normali
    do while NOT EOF()
      * --- Messaggio a schermo
      ah_Msg("Export corrispettivi con scorporo: reg num."+STR(CORRNORM.PNNUMRER,6,0)+"- Data"+dtoc(CORRNORM.PNDATREG),.T.)
      * --- Scrittura su Log
      this.w_STRINGA = "          Export Corrispettivi con Scorporo : Reg. Num. "+STR(CORRNORM.PNNUMRER,6,0)+" - Data "+dtoc(CORRNORM.PNDATREG)
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      * --- Salvo la posizione attuale del record e il seriale della registrazione
      this.w_ACTUALPOS = RECNO()
      this.w_PNSERIAL = CORRNORM.PNSERIAL
      this.w_TOTDOC = 0
      * --- Ciclo su questa registrazione
      this.w_ESCI = .F.
      this.w_OKTRAS = .T.
      do while CORRNORM.PNSERIAL=this.w_PNSERIAL AND NOT this.w_ESCI
        * --- Inizializzo la varibile di reg non valida
        this.w_REGNOVALIDA = .F.
        * --- Se � un rigo IVA lo butto giu
        * --- Incremento il numero di record trasferiti
        this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
        this.oParentObject.w_CORRNORM = this.oParentObject.w_CORRNORM+1
        FWRITE(this.oParentObject.hFile,"D40",3)
        * --- Recupero e inserisco l' anno e il mese della registrazione
        FWRITE(this.oParentObject.hFile,LEFT(dtos(CORRNORM.PNDATREG),6),6)
        * --- Recupero e inserisco l' anno di competenza
        FWRITE(this.oParentObject.hFile,CORRNORM.PNCOMPET,4)
        * --- Recupero e inserisco il mese di competenza
        FWRITE(this.oParentObject.hFile,SUBSTR(dtos(CORRNORM.PNCOMIVA),5,2),2)
        * --- Sezione
        FWRITE(this.oParentObject.hFile,RIGHT("00"+ALLTRIM(STR(CORRNORM.IVNUMREG-1,2,0)),2),2)
        * --- Tipo Corrispettivo: se c'� il codice cliente, ricevuta fiscale, altrimenti scontrino
        FWRITE(this.oParentObject.hFile,IIF(EMPTY(NVL(CORRNORM.PNCODCLF,"")), "SC", "RF"),2)
        * --- Data registrazione
        FWRITE(this.oParentObject.hFile,dtos(CORRNORM.PNDATREG),8)
        * --- Recupero e scrittura del codice di Aliquota IVA
        this.w_APRCAU = SPACE(3)
        this.w_APRNORM = SPACE(2)
        this.w_IVASTU = SPACE(2)
        this.w_IVACEE = SPACE(2)
        this.w_PNCODCAU = CORRNORM.PNCODCAU
        this.w_IVCODIVA = CORRNORM.IVCODIVA
        * --- Read from STU_TRAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.STU_TRAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LMAPRNOR,LMAPRCAU,LMIVASTU,LMIVACEE"+;
            " from "+i_cTable+" STU_TRAS where ";
                +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
                +" and LMHOCCAU = "+cp_ToStrODBC(this.w_PNCODCAU);
                +" and LMHOCIVA = "+cp_ToStrODBC(this.w_IVCODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LMAPRNOR,LMAPRCAU,LMIVASTU,LMIVACEE;
            from (i_cTable) where;
                LMCODICE = this.oParentObject.w_ASSOCI;
                and LMHOCCAU = this.w_PNCODCAU;
                and LMHOCIVA = this.w_IVCODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_APRNORM = NVL(cp_ToDate(_read_.LMAPRNOR),cp_NullValue(_read_.LMAPRNOR))
          this.w_APRCAU = NVL(cp_ToDate(_read_.LMAPRCAU),cp_NullValue(_read_.LMAPRCAU))
          this.w_IVASTU = NVL(cp_ToDate(_read_.LMIVASTU),cp_NullValue(_read_.LMIVASTU))
          this.w_IVACEE = NVL(cp_ToDate(_read_.LMIVACEE),cp_NullValue(_read_.LMIVACEE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT CORRNORM
        * --- Verifica se ho il codice IVA studio definito nella norma
        if NOT EMPTY(this.w_APRNORM)
          this.w_IVASTUNOR = SPACE(2)
          * --- Read from STU_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.STU_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_NORM_idx,2],.t.,this.STU_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LMCODIVA"+;
              " from "+i_cTable+" STU_NORM where ";
                  +"LMCODNOR = "+cp_ToStrODBC(this.w_APRNORM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LMCODIVA;
              from (i_cTable) where;
                  LMCODNOR = this.w_APRNORM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_IVASTUNOR = NVL(cp_ToDate(_read_.LMCODIVA),cp_NullValue(_read_.LMCODIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          SELECT CORRNORM
          if this.w_IVASTUNOR="XX"
            this.w_IVASTUNOR = this.w_IVASTU
          endif
        else
          this.w_IVASTUNOR = this.w_IVASTU
        endif
        * --- Controllo Codice IVA
        if (EMPTY(this.w_IVASTUNOR) or this.w_IVASTUNOR="XX")
          this.w_STRINGA = "               Errore! Codice IVA non definito"
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        * --- Scrittura del codice IVA
        FWRITE(this.oParentObject.hFile,(this.w_IVASTUNOR+"00"),4)
        * --- Importo totale
        this.w_TOTDOC = CORRNORM.IVIMPONI+CORRNORM.IVIMPIVA
        if g_PERVAL=this.oParentObject.w_VALEUR
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOC)),11,0))+RIGHT(STR(this.w_TOTDOC,12,2),2),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(this.w_TOTDOC),11,0)),11),11)
        endif
        * --- Segno
        FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOC>=0, "+", "-"),1)
        * --- Numero documento dal al 
        FWRITE(this.oParentObject.hFile,"0000000000",10)
        * --- Scrittura del codice norma
        FWRITE(this.oParentObject.hFile,IIF(EMPTY(this.w_APRNORM),"  ",this.w_APRNORM),2)
        * --- Codice centro di costo
        FWRITE(this.oParentObject.hFile,"00",2)
        * --- Contropartita (Sottoconto)
        this.w_ANCODSTU = SPACE(6)
        this.w_PNTIPCLF = CORRNORM.IVTIPCOP
        this.w_PNCODCLF = CORRNORM.IVCONTRO
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODSTU"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODSTU;
            from (i_cTable) where;
                ANTIPCON = this.w_PNTIPCLF;
                and ANCODICE = this.w_PNCODCLF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT CORRNORM
        if EMPTY(this.w_ANCODSTU)
          this.w_ANCODSTU = "000000"
          this.w_STRINGA = "               Errore! Contropartita "+NVL(this.w_PNCODCLF, " ")+" non ha un codice sottoconto nello studio associato"
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        else
          this.w_ANCODSTU = Right(this.w_ANCODSTU,5)
        endif
        FWRITE(this.oParentObject.hFile,this.w_ANCODSTU,5)
        * --- Codice Cassa
        if CORRNORM.PNTIPCLF="N"
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- Mappo il codice cliente e lo scrivo
          this.w_ANCODSTU = SPACE(6)
          this.w_PNTIPCLF = CORRNORM.PNTIPCLF
          this.w_PNCODCLF = CORRNORM.PNCODCLF
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANCODSTU"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANCODSTU;
              from (i_cTable) where;
                  ANTIPCON = this.w_PNTIPCLF;
                  and ANCODICE = this.w_PNCODCLF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          SELECT CORRNORM
          if EMPTY(this.w_ANCODSTU)
            this.w_ANCODSTU = "000000"
            this.w_STRINGA = "               Errore! Cliente "+NVL(this.w_PNCODCLF, " ")+" non ha un codice cliente nello studio associato"
            FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
            FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
            this.oParentObject.w_ERRORE = .T.
          else
            this.w_ANCODSTU = Right(this.w_ANCODSTU,5)
          endif
        endif
        * --- Scrivo su file
        FWRITE(this.oParentObject.hFile,this.w_ANCODSTU,5)
        * --- Codice CEE
        FWRITE(this.oParentObject.hFile,"  ",2)
        * --- Flag a calendario
        FWRITE(this.oParentObject.hFile," ",1)
        * --- Recupero valuta di conto
        FWRITE(this.oParentObject.hFile , IIF(g_PERVAL=this.oParentObject.w_VALEUR, "E", " ") , 1)
        * --- Filler
        this.oParentObject.w_FILLER = SPACE(129)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,129)
        if this.w_REGNOVALIDA
          go this.w_ACTUALPOS
          this.w_NOTRAS = -1
          * --- Insert into STU_PNTT
          i_nConn=i_TableProp[this.STU_PNTT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NOTRAS),'STU_PNTT','LMNUMTRA');
            +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.w_NOTRAS,'LMNUMTR2',0)
            insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
               values (;
                 this.w_PNSERIAL;
                 ,this.w_NOTRAS;
                 ,0;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error='GSLM_BCN: Scrittura con errore in STU_PNTT'
            return
          endif
          SELECT CORRNORM
          do while CORRNORM.PNSERIAL=this.w_PNSERIAL
            * --- Avanzo il puntatore
            skip 1
          enddo
          this.w_ESCI = .T.
          this.w_OKTRAS = .F.
        endif
        if not this.w_ESCI
          skip 1
        endif
      enddo
      if this.w_OKTRAS
        * --- Insert into STU_PNTT
        i_nConn=i_TableProp[this.STU_PNTT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'STU_PNTT','LMNUMTRA');
          +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.oParentObject.w_PROFIL,'LMNUMTR2',0)
          insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.oParentObject.w_PROFIL;
               ,0;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='GSLM_BCN: Scrittura in STU_PNTT'
          return
        endif
        SELECT CORRNORM
      endif
    enddo
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cerco le righe del dettaglio che contengono un conto di tipo cassa, banca, ...
    vq_exec("..\LEMC\EXE\QUERY\GSLM1COC.VQR",this,"CONTICAS")
    SELECT CONTICAS
    GO TOP
    this.w_ANCODSTU = "000000"
    if RECCOUNT("CONTICAS") > 0
      if RECCOUNT("CONTICAS") = 1
        this.w_ANCODSTU = SPACE(6)
        this.w_TIPCAS = "G"
        this.w_CODCAS = CONTICAS.PNCODCON
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODSTU"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCAS);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCAS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODSTU;
            from (i_cTable) where;
                ANTIPCON = this.w_TIPCAS;
                and ANCODICE = this.w_CODCAS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT CORRNORM
        if EMPTY(this.w_ANCODSTU)
          this.w_STRINGA = "               Errore! Conto "+NVL(this.w_PNCODCON, " ")+" non associato nello studio"
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        else
          this.w_ANCODSTU = Right(this.w_ANCODSTU,5)
        endif
      else
        this.w_TIPCAS = "G"
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODSTU"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCAS);
                +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CASFIT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODSTU;
            from (i_cTable) where;
                ANTIPCON = this.w_TIPCAS;
                and ANCODICE = this.oParentObject.w_CASFIT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if EMPTY(this.w_ANCODSTU)
          this.w_STRINGA = "               Errore! Conto "+NVL(this.oParentObject.w_CASFIT, " ")+" non associato nello studio"
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        else
          this.w_ANCODSTU = Right(this.w_ANCODSTU,5)
        endif
        if NOT EMPTY(this.oParentObject.w_CAUGIR)
          if this.w_PNSERIAL <> this.w_OLDPNSERIAL
            * --- Il calcolo viene fatto una volta sola per ogni registrazione di Primanota. Questo  per
            *     evitare la duplicazione dei records per ogni registrazione contabile.
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_OLDPNSERIAL = this.w_PNSERIAL
          endif
        else
          this.w_STRINGA = "               Errore! Causale Giroconto non definita nella Tabella Trasferimento Studio"
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
      endif
    endif
    SELECT CONTICAS
    USE
    SELECT CORRNORM
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CREAZIONE MOVIMENTI CONTABILI
    this.w_TOTCAS = 0
    SELECT CONTICAS
    do while NOT EOF()
      * --- Movimenti relativi ai conti di tipo cassa,  banca, ...
      INSERT INTO MOVDACOR (PNSERIAL, PNNUMRER, PNDATREG, PNCODCAU, ;
      PNCOMPET , PNTIPDOC, PNNUMDOC, PNALFDOC, PNDATDOC, PNNUMPRO, ;
      PNALFPRO, PNCODVAL, PNVALNAZ, PNTOTDOC, PNTIPCLF, PNCODCLF, ;
      PNCOMIVA, PNTIPREG, PNNUMREG, PNCAURIG, PNDESRIG, PNTIPCON, ;
      PNIMPDAR, PNCODCON, PNIMPAVE, ANTIPSOT) VALUES ;
      (CORRNORM.PNSERIAL, CORRNORM.PNNUMRER, CORRNORM.PNDATREG, this.oParentObject.w_CAUGIR, CORRNORM.PNCOMPET, "NO", ;
      NVL(CORRNORM.PNNUMDOC,0), NVL(CORRNORM.PNALFDOC," "), NVL(CORRNORM.PNDATDOC,CTOD("  -  -  ")), NVL(CORRNORM.PNNUMPRO,0), ;
      NVL(CORRNORM.PNALFPRO," "), NVL(CORRNORM.PNCODVAL," "), NVL(CORRNORM.PNVALNAZ," "), NVL(CORRNORM.PNTOTDOC,0), ;
      CORRNORM.PNTIPCLF, NVL(CORRNORM.PNCODCLF," "), NVL(CORRNORM.PNCOMIVA,CTOD("  -  -  ")), "N", 0, this.oParentObject.w_CAUGIR, ;
      " ", CONTICAS.PNTIPCON, CONTICAS.PNIMPDAR, CONTICAS.PNCODCON, CONTICAS.PNIMPAVE, CONTICAS.ANTIPSOT)
      this.w_TOTCAS = CONTICAS.PNIMPDAR-CONTICAS.PNIMPAVE+this.w_TOTCAS
      skip 1
    enddo
    * --- Movimento relativo al Sottoconto Studio Cassa Fittizia
    INSERT INTO MOVDACOR (PNSERIAL, PNNUMRER, PNDATREG, PNCODCAU, ;
    PNCOMPET , PNTIPDOC, PNNUMDOC, PNALFDOC, PNDATDOC, PNNUMPRO, ;
    PNALFPRO, PNCODVAL, PNVALNAZ, PNTOTDOC, PNTIPCLF, PNCODCLF, ;
    PNCOMIVA, PNTIPREG, PNNUMREG, PNCAURIG, PNDESRIG, PNTIPCON, ;
    PNIMPDAR, PNCODCON, PNIMPAVE, ANTIPSOT) VALUES ;
    (CORRNORM.PNSERIAL, CORRNORM.PNNUMRER, CORRNORM.PNDATREG, this.oParentObject.w_CAUGIR, CORRNORM.PNCOMPET, "NO", ;
    NVL(CORRNORM.PNNUMDOC,0), NVL(CORRNORM.PNALFDOC," "), NVL(CORRNORM.PNDATDOC,CTOD("  -  -  ")), NVL(CORRNORM.PNNUMPRO,0), ;
    NVL(CORRNORM.PNALFPRO," "), NVL(CORRNORM.PNCODVAL," "), NVL(CORRNORM.PNVALNAZ," "), NVL(CORRNORM.PNTOTDOC,0), ;
    CORRNORM.PNTIPCLF, NVL(CORRNORM.PNCODCLF," "), NVL(CORRNORM.PNCOMIVA,CTOD("  -  -  ")), "N", 0, this.oParentObject.w_CAUGIR, ;
    " ", "G", 0, this.oParentObject.w_CASFIT, this.w_TOTCAS, "X")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='PNT_DETT'
    this.cWorkTables[3]='STU_NORM'
    this.cWorkTables[4]='STU_PNTT'
    this.cWorkTables[5]='STU_TPNT'
    this.cWorkTables[6]='STU_TRAS'
    return(this.OpenAllTables(6))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
