* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_bcf                                                        *
*              Associazione codici cli/for                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-22                                                      *
* Last revis.: 2012-06-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPCON,pCODCON,w_CODSTU,pMASTRO,pCONSTU,pPROG
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm_bcf",oParentObject,m.pTIPCON,m.pCODCON,m.w_CODSTU,m.pMASTRO,m.pCONSTU,m.pPROG)
return(i_retval)

define class tgslm_bcf as StdBatch
  * --- Local variables
  pTIPCON = space(1)
  pCODCON = space(15)
  w_CODSTU = space(3)
  pMASTRO = space(15)
  pCONSTU = space(6)
  pPROG = space(6)
  w_RESULT = space(6)
  w_ORICON = space(6)
  w_MINCON = space(6)
  w_NELEM = space(5)
  w_INDICE = space(5)
  w_USCITA = .f.
  w_NOLOOP = .f.
  w_CODAZI = space(5)
  w_TRAEXP = space(1)
  * --- WorkFile variables
  STUMPIAC_idx=0
  CONTI_idx=0
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dato Tipo, Codice studio e progressivo studio di default determina
    *     il primo progressivo libero per  cliente e fornitore.
    *     Se risultato bianco allora impossibile determinare progressivo cliente /fornitore
    if type("pCONSTU")<>"C"
      * --- Calcolo il sottoconto da associare al Cliente/Fornitore
      this.w_RESULT = ""
      this.w_CODAZI = i_CODAZI
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZTRAEXP"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZTRAEXP;
          from (i_cTable) where;
              AZCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TRAEXP = NVL(cp_ToDate(_read_.AZTRAEXP),cp_NullValue(_read_.AZTRAEXP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if EMPTY(this.w_CODSTU) 
        if this.w_TRAEXP <> "N" and g_LEMC="S" and Not empty(this.pMASTRO)
          ah_ErrorMsg("Conto studio non specificato sul mastro selezionato",,"")
        endif
        * --- Se mancano i dati segnalo e ritorno ''
        i_retcode = 'stop'
        i_retval = ""
        return
      else
        * --- Dichiaro l'array che conterr� gli estremi degli intervalli di sottoconti validi per il conto specificato
        i=1
        * --- Leggo gli estremi per gli intervalli associati al conto Studio
        * --- Select from STUMPIAC
        i_nConn=i_TableProp[this.STUMPIAC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STUMPIAC_idx,2],.t.,this.STUMPIAC_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select LMMINCON,LMMAXCON,LMCODCON  from "+i_cTable+" STUMPIAC ";
              +" where LMCODCON = "+cp_ToStrODBC(this.w_CODSTU)+" and LMPROGRE="+cp_ToStrODBC(this.pPROG)+"";
               ,"_Curs_STUMPIAC")
        else
          select LMMINCON,LMMAXCON,LMCODCON from (i_cTable);
           where LMCODCON = this.w_CODSTU and LMPROGRE=this.pPROG;
            into cursor _Curs_STUMPIAC
        endif
        if used('_Curs_STUMPIAC')
          select _Curs_STUMPIAC
          locate for 1=1
          do while not(eof())
          * --- Definisco l'array all'interno del ciclo e prima di ogni inserimento per ovviare al problema
          *     di aggiungere elementi non utilizzati e quindi valorizzati a .f.
          Dimension A_EST(i)
          A_EST(i) = _Curs_STUMPIAC.LMMINCON
          i = i + 1
          Dimension A_EST(i)
          A_EST(i) = _Curs_STUMPIAC.LMMAXCON
          i = i + 1
            select _Curs_STUMPIAC
            continue
          enddo
          use
        endif
        if Type("A_EST")="U"
          i_retcode = 'stop'
          i_retval = this.w_RESULT
          return
        endif
        * --- Leggo il numero di elementi presenti nell'array degli estremi
        this.w_NELEM = ALEN(A_EST)
        * --- Ordine in modo ascendente gli elementi dell'array
        =ASORT(A_EST,1)
        * --- Assegno come estremo inferiore il minimo estremo degli intervalli trovati
        this.w_MINCON = A_EST(1)
        * --- Ricerco il massimo progressivo utilizzato - Calcolo del sottoconto Studio
        * --- Select from QUERY\GSAR3ACL
        do vq_exec with 'QUERY\GSAR3ACL',this,'_Curs_QUERY_GSAR3ACL','',.f.,.t.
        if used('_Curs_QUERY_GSAR3ACL')
          select _Curs_QUERY_GSAR3ACL
          locate for 1=1
          do while not(eof())
          if EMPTY(NVL(_Curs_QUERY_GSAR3ACL.ANCODSTU," ")) or val(NVL(_Curs_QUERY_GSAR3ACL.ANCODSTU," ")) < val(this.w_MINCON)
            * --- Se nessun progressivo presente...
            this.w_RESULT = Padl( Alltrim(this.w_MINCON) ,6 , "0" )
          else
            * --- Utilizzo il progressivo massimo trovato + 1
            this.w_RESULT = Padl( ALLTRIM(STR( VAL(NVL(_Curs_QUERY_GSAR3ACL.ANCODSTU,"0"))+1 )), 6 , "0" )
          endif
            select _Curs_QUERY_GSAR3ACL
            continue
          enddo
          use
        endif
        * --- Inizio elaborazione ricerca sottoconto disponibile
        this.w_INDICE = 1
        this.w_USCITA = .F.
        this.w_NOLOOP = .T.
        this.w_ORICON = ""
        * --- Ricerco l'intervallo contenente il sottoconto calcolato
        do while this.w_INDICE < this.w_NELEM and ! this.w_USCITA
          if BETWEEN(this.w_RESULT,A_EST(this.w_INDICE),A_EST(this.w_INDICE+1))
            * --- Il sottoconto calcolato appartine all'intervallo corrente, quindi esco dal ciclo
            this.w_USCITA = .T.
          else
            * --- Mi sposto all'intervallo successivo
            this.w_INDICE = this.w_INDICE + 2
          endif
        enddo
        * --- Verifico se ho raggiunto l'ultimo intervallo
        if this.w_INDICE > this.w_NELEM
          this.w_INDICE = 1
          this.w_RESULT = A_EST(this.w_INDICE)
        endif
        * --- Ciclo sugli intervalli alla ricerca del primo codice sottoconto disponibile 
        do while .T.
          * --- Verifico se ho gi� fatto un giro completo sugli intervalli
          if this.w_RESULT == this.w_ORICON
            * --- Restituisco il sottoconto vuoto, non ho trovato alcune codice disponibile
            this.w_RESULT = " "
            EXIT
          else
            * --- Verifico la disponibilit� del codice
            if ! CHK_STUDIO(this.pTIPCON,this.pCODCON,this.w_RESULT,.T.)
              if this.w_NOLOOP
                this.w_NOLOOP = .F.
                this.w_ORICON = this.w_RESULT
              endif
              if this.w_RESULT<>A_EST(this.w_INDICE+1)
                this.w_RESULT = RIGHT("000000"+ALLTRIM(STR(VAL(this.w_RESULT) + 1,6,0)),6)
              else
                if this.w_INDICE + 2 <= this.w_NELEM
                  this.w_INDICE = this.w_INDICE + 2
                  this.w_RESULT = A_EST(this.w_INDICE)
                else
                  this.w_INDICE = 1
                  this.w_RESULT = A_EST(this.w_INDICE)
                endif
              endif
            else
              EXIT
            endif
          endif
        enddo
        * --- Restituisco il primo sottoconto disponibile o la stringa vuota
        i_retcode = 'stop'
        i_retval = this.w_RESULT
        return
      endif
    else
      * --- Calcolo l'intervallo di appartenenza del sottoconto pCONSTU
      * --- Select from STUMPIAC
      i_nConn=i_TableProp[this.STUMPIAC_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STUMPIAC_idx,2],.t.,this.STUMPIAC_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select LMMINCON,LMMAXCON  from "+i_cTable+" STUMPIAC ";
            +" where LMCODCON="+cp_ToStrODBC(this.w_CODSTU)+" and LMPROGRE="+cp_ToStrODBC(this.pPROG)+"";
             ,"_Curs_STUMPIAC")
      else
        select LMMINCON,LMMAXCON from (i_cTable);
         where LMCODCON=this.w_CODSTU and LMPROGRE=this.pPROG;
          into cursor _Curs_STUMPIAC
      endif
      if used('_Curs_STUMPIAC')
        select _Curs_STUMPIAC
        locate for 1=1
        do while not(eof())
        * --- Inserisco comunque i valori massimo e minimo
        THIS.OPARENTOBJECT.W_MINCON=_Curs_STUMPIAC.LMMINCON
        THIS.OPARENTOBJECT.W_MAXCON=_Curs_STUMPIAC.LMMAXCON
        THIS.OPARENTOBJECT.SETCONTROLSVALUE()
        if BETWEEN(this.pCONSTU,_Curs_STUMPIAC.LMMINCON,_Curs_STUMPIAC.LMMAXCON)
          exit
        else
          THIS.OPARENTOBJECT.W_MINCON=""
          THIS.OPARENTOBJECT.W_MAXCON=""
        endif
          select _Curs_STUMPIAC
          continue
        enddo
        use
      endif
    endif
  endproc


  proc Init(oParentObject,pTIPCON,pCODCON,w_CODSTU,pMASTRO,pCONSTU,pPROG)
    this.pTIPCON=pTIPCON
    this.pCODCON=pCODCON
    this.w_CODSTU=w_CODSTU
    this.pMASTRO=pMASTRO
    this.pCONSTU=pCONSTU
    this.pPROG=pPROG
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='STUMPIAC'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='AZIENDA'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_STUMPIAC')
      use in _Curs_STUMPIAC
    endif
    if used('_Curs_QUERY_GSAR3ACL')
      use in _Curs_QUERY_GSAR3ACL
    endif
    if used('_Curs_STUMPIAC')
      use in _Curs_STUMPIAC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPCON,pCODCON,w_CODSTU,pMASTRO,pCONSTU,pPROG"
endproc
