* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_mhs                                                        *
*              Tabella trascodifica per studio                                 *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_47]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-21                                                      *
* Last revis.: 2018-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- gslm_mhs
* Esco se il modulo � in semplificata
IF NOT LMAVANZATO()
  do cplu_erm with "La funzione � attiva con il Modulo in modalit� Avanzata"
  return
ENDIF
* --- Fine Area Manuale
return(createobject("tgslm_mhs"))

* --- Class definition
define class tgslm_mhs as StdTrsForm
  Top    = 5
  Left   = 9

  * --- Standard Properties
  Width  = 726
  Height = 392+35
  bGlobalFont=.f.
  FontName      = "Arial"
  FontSize      = 8
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-05-31"
  HelpContextID=13220969
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  STU_TRAS_IDX = 0
  CAU_CONT_IDX = 0
  STU_NORM_IDX = 0
  STU_CAUS_IDX = 0
  STU_CIVA_IDX = 0
  STU_INTR_IDX = 0
  VOCIIVA_IDX = 0
  cFile = "STU_TRAS"
  cKeySelect = "LMCODICE"
  cKeyWhere  = "LMCODICE=this.w_LMCODICE"
  cKeyDetail  = "LMCODICE=this.w_LMCODICE"
  cKeyWhereODBC = '"LMCODICE="+cp_ToStrODBC(this.w_LMCODICE)';

  cKeyDetailWhereODBC = '"LMCODICE="+cp_ToStrODBC(this.w_LMCODICE)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"STU_TRAS.LMCODICE="+cp_ToStrODBC(this.w_LMCODICE)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'STU_TRAS.CPROWNUM '
  cPrg = "gslm_mhs"
  cComment = "Tabella trascodifica per studio"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 12
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LMDESCRI = space(20)
  w_LM__DATA = ctod('  /  /  ')
  w_LMHOCCAU = space(5)
  w_HOCCAUDES = space(35)
  w_LMHOCIVA = space(5)
  w_HOCIVADES = space(35)
  w_LMAPRNOR = space(2)
  w_APRNORDES = space(35)
  w_LMAPRCAU = space(10)
  w_APRCAUDES = space(35)
  w_LMIVASTU = space(2)
  w_DESIVA = space(35)
  w_LMIVACEE = space(2)
  w_DESCEE = space(35)
  w_OBTEST = ctod('  /  /  ')
  w_LMCODICE = space(2)
  w_LMDETIVA = space(3)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'STU_TRAS','gslm_mhs')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslm_mhsPag1","gslm_mhs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Trascodifica")
      .Pages(1).HelpContextID = 88566527
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLMCODICE_1_14
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='STU_NORM'
    this.cWorkTables[3]='STU_CAUS'
    this.cWorkTables[4]='STU_CIVA'
    this.cWorkTables[5]='STU_INTR'
    this.cWorkTables[6]='VOCIIVA'
    this.cWorkTables[7]='STU_TRAS'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.STU_TRAS_IDX,5],7]
    this.nPostItConn=i_TableProp[this.STU_TRAS_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_LMCODICE = NVL(LMCODICE,space(2))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_3_joined
    link_2_3_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    local link_2_9_joined
    link_2_9_joined=.f.
    local link_2_11_joined
    link_2_11_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from STU_TRAS where LMCODICE=KeySet.LMCODICE
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.STU_TRAS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STU_TRAS_IDX,2],this.bLoadRecFilter,this.STU_TRAS_IDX,"gslm_mhs")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('STU_TRAS')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "STU_TRAS.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' STU_TRAS '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_3_joined=this.AddJoinedLink_2_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_9_joined=this.AddJoinedLink_2_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_11_joined=this.AddJoinedLink_2_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LMCODICE',this.w_LMCODICE  )
      select * from (i_cTable) STU_TRAS where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_LMDESCRI = NVL(LMDESCRI,space(20))
        .w_LM__DATA = NVL(cp_ToDate(LM__DATA),ctod("  /  /  "))
        .w_OBTEST = i_DATSYS
        .w_LMCODICE = NVL(LMCODICE,space(2))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'STU_TRAS')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_HOCCAUDES = space(35)
          .w_HOCIVADES = space(35)
          .w_APRNORDES = space(35)
          .w_APRCAUDES = space(35)
          .w_DESIVA = space(35)
          .w_DESCEE = space(35)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_LMHOCCAU = NVL(LMHOCCAU,space(5))
          if link_2_1_joined
            this.w_LMHOCCAU = NVL(CCCODICE201,NVL(this.w_LMHOCCAU,space(5)))
            this.w_HOCCAUDES = NVL(CCDESCRI201,space(35))
          else
          .link_2_1('Load')
          endif
          .w_LMHOCIVA = NVL(LMHOCIVA,space(5))
          if link_2_3_joined
            this.w_LMHOCIVA = NVL(IVCODIVA203,NVL(this.w_LMHOCIVA,space(5)))
            this.w_HOCIVADES = NVL(IVDESIVA203,space(35))
          else
          .link_2_3('Load')
          endif
          .w_LMAPRNOR = NVL(LMAPRNOR,space(2))
          if link_2_5_joined
            this.w_LMAPRNOR = NVL(LMCODNOR205,NVL(this.w_LMAPRNOR,space(2)))
            this.w_APRNORDES = NVL(LMDESNOR205,space(35))
          else
          .link_2_5('Load')
          endif
          .w_LMAPRCAU = NVL(LMAPRCAU,space(10))
          if link_2_7_joined
            this.w_LMAPRCAU = NVL(LMCODCAU207,NVL(this.w_LMAPRCAU,space(10)))
            this.w_APRCAUDES = NVL(LMDESCAU207,space(35))
          else
          .link_2_7('Load')
          endif
          .w_LMIVASTU = NVL(LMIVASTU,space(2))
          if link_2_9_joined
            this.w_LMIVASTU = NVL(LMCODIVA209,NVL(this.w_LMIVASTU,space(2)))
            this.w_DESIVA = NVL(LMDESIVA209,space(35))
          else
          .link_2_9('Load')
          endif
          .w_LMIVACEE = NVL(LMIVACEE,space(2))
          if link_2_11_joined
            this.w_LMIVACEE = NVL(LMCODOPE211,NVL(this.w_LMIVACEE,space(2)))
            this.w_DESCEE = NVL(LMDESABB211,space(35))
          else
          .link_2_11('Load')
          endif
          .w_LMDETIVA = NVL(LMDETIVA,space(3))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_LMDESCRI=space(20)
      .w_LM__DATA=ctod("  /  /  ")
      .w_LMHOCCAU=space(5)
      .w_HOCCAUDES=space(35)
      .w_LMHOCIVA=space(5)
      .w_HOCIVADES=space(35)
      .w_LMAPRNOR=space(2)
      .w_APRNORDES=space(35)
      .w_LMAPRCAU=space(10)
      .w_APRCAUDES=space(35)
      .w_LMIVASTU=space(2)
      .w_DESIVA=space(35)
      .w_LMIVACEE=space(2)
      .w_DESCEE=space(35)
      .w_OBTEST=ctod("  /  /  ")
      .w_LMCODICE=space(2)
      .w_LMDETIVA=space(3)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_LM__DATA = i_DATSYS
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_LMHOCCAU))
         .link_2_1('Full')
        endif
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_LMHOCIVA))
         .link_2_3('Full')
        endif
        .DoRTCalc(6,7,.f.)
        if not(empty(.w_LMAPRNOR))
         .link_2_5('Full')
        endif
        .DoRTCalc(8,9,.f.)
        if not(empty(.w_LMAPRCAU))
         .link_2_7('Full')
        endif
        .DoRTCalc(10,11,.f.)
        if not(empty(.w_LMIVASTU))
         .link_2_9('Full')
        endif
        .DoRTCalc(12,13,.f.)
        if not(empty(.w_LMIVACEE))
         .link_2_11('Full')
        endif
        .DoRTCalc(14,14,.f.)
        .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(16,16,.f.)
        .w_LMDETIVA = '   '
      endif
    endwith
    cp_BlankRecExtFlds(this,'STU_TRAS')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oLMDESCRI_1_1.enabled = i_bVal
      .Page1.oPag.oLM__DATA_1_2.enabled = i_bVal
      .Page1.oPag.oLMCODICE_1_14.enabled = i_bVal
      .Page1.oPag.oLMDETIVA_2_13.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oLMCODICE_1_14.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oLMCODICE_1_14.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'STU_TRAS',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.STU_TRAS_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMDESCRI,"LMDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LM__DATA,"LM__DATA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCODICE,"LMCODICE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.STU_TRAS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STU_TRAS_IDX,2])
    i_lTable = "STU_TRAS"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.STU_TRAS_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      vx_exec("..\LEMC\EXE\QUERY\GSLM3STC.VQR,..\LEMC\EXE\QUERY\GSLM3STC.FRX",this)
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_LMHOCCAU C(5);
      ,t_HOCCAUDES C(35);
      ,t_LMHOCIVA C(5);
      ,t_HOCIVADES C(35);
      ,t_LMAPRNOR C(2);
      ,t_APRNORDES C(35);
      ,t_LMAPRCAU C(10);
      ,t_APRCAUDES C(35);
      ,t_LMIVASTU C(2);
      ,t_DESIVA C(35);
      ,t_LMIVACEE C(2);
      ,t_DESCEE C(35);
      ,t_LMDETIVA N(3);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgslm_mhsbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLMHOCCAU_2_1.controlsource=this.cTrsName+'.t_LMHOCCAU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oHOCCAUDES_2_2.controlsource=this.cTrsName+'.t_HOCCAUDES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLMHOCIVA_2_3.controlsource=this.cTrsName+'.t_LMHOCIVA'
    this.oPgFRm.Page1.oPag.oHOCIVADES_2_4.controlsource=this.cTrsName+'.t_HOCIVADES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLMAPRNOR_2_5.controlsource=this.cTrsName+'.t_LMAPRNOR'
    this.oPgFRm.Page1.oPag.oAPRNORDES_2_6.controlsource=this.cTrsName+'.t_APRNORDES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLMAPRCAU_2_7.controlsource=this.cTrsName+'.t_LMAPRCAU'
    this.oPgFRm.Page1.oPag.oAPRCAUDES_2_8.controlsource=this.cTrsName+'.t_APRCAUDES'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLMIVASTU_2_9.controlsource=this.cTrsName+'.t_LMIVASTU'
    this.oPgFRm.Page1.oPag.oDESIVA_2_10.controlsource=this.cTrsName+'.t_DESIVA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oLMIVACEE_2_11.controlsource=this.cTrsName+'.t_LMIVACEE'
    this.oPgFRm.Page1.oPag.oDESCEE_2_12.controlsource=this.cTrsName+'.t_DESCEE'
    this.oPgFRm.Page1.oPag.oLMDETIVA_2_13.controlsource=this.cTrsName+'.t_LMDETIVA'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(72)
    this.AddVLine(299)
    this.AddVLine(356)
    this.AddVLine(421)
    this.AddVLine(569)
    this.AddVLine(633)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMHOCCAU_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.STU_TRAS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STU_TRAS_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.STU_TRAS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_TRAS_IDX,2])
      *
      * insert into STU_TRAS
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'STU_TRAS')
        i_extval=cp_InsertValODBCExtFlds(this,'STU_TRAS')
        i_cFldBody=" "+;
                  "(LMDESCRI,LM__DATA,LMHOCCAU,LMHOCIVA,LMAPRNOR"+;
                  ",LMAPRCAU,LMIVASTU,LMIVACEE,LMCODICE,LMDETIVA,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_LMDESCRI)+","+cp_ToStrODBC(this.w_LM__DATA)+","+cp_ToStrODBCNull(this.w_LMHOCCAU)+","+cp_ToStrODBCNull(this.w_LMHOCIVA)+","+cp_ToStrODBCNull(this.w_LMAPRNOR)+;
             ","+cp_ToStrODBCNull(this.w_LMAPRCAU)+","+cp_ToStrODBCNull(this.w_LMIVASTU)+","+cp_ToStrODBCNull(this.w_LMIVACEE)+","+cp_ToStrODBC(this.w_LMCODICE)+","+cp_ToStrODBC(this.w_LMDETIVA)+;
             ","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'STU_TRAS')
        i_extval=cp_InsertValVFPExtFlds(this,'STU_TRAS')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'LMCODICE',this.w_LMCODICE)
        INSERT INTO (i_cTable) (;
                   LMDESCRI;
                  ,LM__DATA;
                  ,LMHOCCAU;
                  ,LMHOCIVA;
                  ,LMAPRNOR;
                  ,LMAPRCAU;
                  ,LMIVASTU;
                  ,LMIVACEE;
                  ,LMCODICE;
                  ,LMDETIVA;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_LMDESCRI;
                  ,this.w_LM__DATA;
                  ,this.w_LMHOCCAU;
                  ,this.w_LMHOCIVA;
                  ,this.w_LMAPRNOR;
                  ,this.w_LMAPRCAU;
                  ,this.w_LMIVASTU;
                  ,this.w_LMIVACEE;
                  ,this.w_LMCODICE;
                  ,this.w_LMDETIVA;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.STU_TRAS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_TRAS_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_LMHOCCAU<>space(3)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'STU_TRAS')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " LMDESCRI="+cp_ToStrODBC(this.w_LMDESCRI)+;
                 ",LM__DATA="+cp_ToStrODBC(this.w_LM__DATA)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'STU_TRAS')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  LMDESCRI=this.w_LMDESCRI;
                 ,LM__DATA=this.w_LM__DATA;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_LMHOCCAU<>space(3)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update STU_TRAS
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'STU_TRAS')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " LMDESCRI="+cp_ToStrODBC(this.w_LMDESCRI)+;
                     ",LM__DATA="+cp_ToStrODBC(this.w_LM__DATA)+;
                     ",LMHOCCAU="+cp_ToStrODBCNull(this.w_LMHOCCAU)+;
                     ",LMHOCIVA="+cp_ToStrODBCNull(this.w_LMHOCIVA)+;
                     ",LMAPRNOR="+cp_ToStrODBCNull(this.w_LMAPRNOR)+;
                     ",LMAPRCAU="+cp_ToStrODBCNull(this.w_LMAPRCAU)+;
                     ",LMIVASTU="+cp_ToStrODBCNull(this.w_LMIVASTU)+;
                     ",LMIVACEE="+cp_ToStrODBCNull(this.w_LMIVACEE)+;
                     ",LMDETIVA="+cp_ToStrODBC(this.w_LMDETIVA)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'STU_TRAS')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      LMDESCRI=this.w_LMDESCRI;
                     ,LM__DATA=this.w_LM__DATA;
                     ,LMHOCCAU=this.w_LMHOCCAU;
                     ,LMHOCIVA=this.w_LMHOCIVA;
                     ,LMAPRNOR=this.w_LMAPRNOR;
                     ,LMAPRCAU=this.w_LMAPRCAU;
                     ,LMIVASTU=this.w_LMIVASTU;
                     ,LMIVACEE=this.w_LMIVACEE;
                     ,LMDETIVA=this.w_LMDETIVA;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.STU_TRAS_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_TRAS_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_LMHOCCAU<>space(3)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete STU_TRAS
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_LMHOCCAU<>space(3)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.STU_TRAS_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STU_TRAS_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,14,.t.)
          .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(16,17,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LMHOCCAU
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMHOCCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_LMHOCCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_LMHOCCAU))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMHOCCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_LMHOCCAU)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_LMHOCCAU)+"%");

            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_LMHOCCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oLMHOCCAU_2_1'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMHOCCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_LMHOCCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_LMHOCCAU)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMHOCCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_HOCCAUDES = NVL(_Link_.CCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_LMHOCCAU = space(5)
      endif
      this.w_HOCCAUDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMHOCCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.CCCODICE as CCCODICE201"+ ",link_2_1.CCDESCRI as CCDESCRI201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on STU_TRAS.LMHOCCAU=link_2_1.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and STU_TRAS.LMHOCCAU=link_2_1.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LMHOCIVA
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMHOCIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_LMHOCIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_LMHOCIVA))
          select IVCODIVA,IVDESIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMHOCIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LMHOCIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oLMHOCIVA_2_3'),i_cWhere,'GSAR_AIV',"Codici IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMHOCIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_LMHOCIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_LMHOCIVA)
            select IVCODIVA,IVDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMHOCIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_HOCIVADES = NVL(_Link_.IVDESIVA,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_LMHOCIVA = space(5)
      endif
      this.w_HOCIVADES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMHOCIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_3.IVCODIVA as IVCODIVA203"+ ",link_2_3.IVDESIVA as IVDESIVA203"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_3 on STU_TRAS.LMHOCIVA=link_2_3.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_3"
          i_cKey=i_cKey+'+" and STU_TRAS.LMHOCIVA=link_2_3.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LMAPRNOR
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_NORM_IDX,3]
    i_lTable = "STU_NORM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2], .t., this.STU_NORM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMAPRNOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSLM_ACN',True,'STU_NORM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODNOR like "+cp_ToStrODBC(trim(this.w_LMAPRNOR)+"%");

          i_ret=cp_SQL(i_nConn,"select LMCODNOR,LMDESNOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODNOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODNOR',trim(this.w_LMAPRNOR))
          select LMCODNOR,LMDESNOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODNOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMAPRNOR)==trim(_Link_.LMCODNOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LMAPRNOR) and !this.bDontReportError
            deferred_cp_zoom('STU_NORM','*','LMCODNOR',cp_AbsName(oSource.parent,'oLMAPRNOR_2_5'),i_cWhere,'GSLM_ACN',"Norme studio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR,LMDESNOR";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',oSource.xKey(1))
            select LMCODNOR,LMDESNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMAPRNOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR,LMDESNOR";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(this.w_LMAPRNOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',this.w_LMAPRNOR)
            select LMCODNOR,LMDESNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMAPRNOR = NVL(_Link_.LMCODNOR,space(2))
      this.w_APRNORDES = NVL(_Link_.LMDESNOR,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_LMAPRNOR = space(2)
      endif
      this.w_APRNORDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])+'\'+cp_ToStr(_Link_.LMCODNOR,1)
      cp_ShowWarn(i_cKey,this.STU_NORM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMAPRNOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.STU_NORM_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.LMCODNOR as LMCODNOR205"+ ",link_2_5.LMDESNOR as LMDESNOR205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on STU_TRAS.LMAPRNOR=link_2_5.LMCODNOR"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and STU_TRAS.LMAPRNOR=link_2_5.LMCODNOR(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LMAPRCAU
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_CAUS_IDX,3]
    i_lTable = "STU_CAUS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_CAUS_IDX,2], .t., this.STU_CAUS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_CAUS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMAPRCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSLM_ACC',True,'STU_CAUS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODCAU like "+cp_ToStrODBC(trim(this.w_LMAPRCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select LMCODCAU,LMDESCAU";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODCAU","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODCAU',trim(this.w_LMAPRCAU))
          select LMCODCAU,LMDESCAU;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODCAU into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMAPRCAU)==trim(_Link_.LMCODCAU) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LMAPRCAU) and !this.bDontReportError
            deferred_cp_zoom('STU_CAUS','*','LMCODCAU',cp_AbsName(oSource.parent,'oLMAPRCAU_2_7'),i_cWhere,'GSLM_ACC',"Causale contabile studio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODCAU,LMDESCAU";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODCAU="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODCAU',oSource.xKey(1))
            select LMCODCAU,LMDESCAU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMAPRCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODCAU,LMDESCAU";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODCAU="+cp_ToStrODBC(this.w_LMAPRCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODCAU',this.w_LMAPRCAU)
            select LMCODCAU,LMDESCAU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMAPRCAU = NVL(_Link_.LMCODCAU,space(10))
      this.w_APRCAUDES = NVL(_Link_.LMDESCAU,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_LMAPRCAU = space(10)
      endif
      this.w_APRCAUDES = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_CAUS_IDX,2])+'\'+cp_ToStr(_Link_.LMCODCAU,1)
      cp_ShowWarn(i_cKey,this.STU_CAUS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMAPRCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.STU_CAUS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.STU_CAUS_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.LMCODCAU as LMCODCAU207"+ ",link_2_7.LMDESCAU as LMDESCAU207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on STU_TRAS.LMAPRCAU=link_2_7.LMCODCAU"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and STU_TRAS.LMAPRCAU=link_2_7.LMCODCAU(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LMIVASTU
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_CIVA_IDX,3]
    i_lTable = "STU_CIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_CIVA_IDX,2], .t., this.STU_CIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_CIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMIVASTU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSLM_AIV',True,'STU_CIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODIVA like "+cp_ToStrODBC(trim(this.w_LMIVASTU)+"%");

          i_ret=cp_SQL(i_nConn,"select LMCODIVA,LMDESIVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODIVA',trim(this.w_LMIVASTU))
          select LMCODIVA,LMDESIVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMIVASTU)==trim(_Link_.LMCODIVA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LMIVASTU) and !this.bDontReportError
            deferred_cp_zoom('STU_CIVA','*','LMCODIVA',cp_AbsName(oSource.parent,'oLMIVASTU_2_9'),i_cWhere,'GSLM_AIV',"Codici IVA studio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODIVA,LMDESIVA";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODIVA',oSource.xKey(1))
            select LMCODIVA,LMDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMIVASTU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODIVA,LMDESIVA";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODIVA="+cp_ToStrODBC(this.w_LMIVASTU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODIVA',this.w_LMIVASTU)
            select LMCODIVA,LMDESIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMIVASTU = NVL(_Link_.LMCODIVA,space(2))
      this.w_DESIVA = NVL(_Link_.LMDESIVA,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_LMIVASTU = space(2)
      endif
      this.w_DESIVA = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_CIVA_IDX,2])+'\'+cp_ToStr(_Link_.LMCODIVA,1)
      cp_ShowWarn(i_cKey,this.STU_CIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMIVASTU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.STU_CIVA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.STU_CIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_9.LMCODIVA as LMCODIVA209"+ ",link_2_9.LMDESIVA as LMDESIVA209"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_9 on STU_TRAS.LMIVASTU=link_2_9.LMCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_9"
          i_cKey=i_cKey+'+" and STU_TRAS.LMIVASTU=link_2_9.LMCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LMIVACEE
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_INTR_IDX,3]
    i_lTable = "STU_INTR"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_INTR_IDX,2], .t., this.STU_INTR_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_INTR_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMIVACEE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSLM_AIS',True,'STU_INTR')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODOPE like "+cp_ToStrODBC(trim(this.w_LMIVACEE)+"%");

          i_ret=cp_SQL(i_nConn,"select LMCODOPE,LMDESABB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODOPE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODOPE',trim(this.w_LMIVACEE))
          select LMCODOPE,LMDESABB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODOPE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMIVACEE)==trim(_Link_.LMCODOPE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LMIVACEE) and !this.bDontReportError
            deferred_cp_zoom('STU_INTR','*','LMCODOPE',cp_AbsName(oSource.parent,'oLMIVACEE_2_11'),i_cWhere,'GSLM_AIS',"Codice INTRA studio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODOPE,LMDESABB";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODOPE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODOPE',oSource.xKey(1))
            select LMCODOPE,LMDESABB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMIVACEE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODOPE,LMDESABB";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODOPE="+cp_ToStrODBC(this.w_LMIVACEE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODOPE',this.w_LMIVACEE)
            select LMCODOPE,LMDESABB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMIVACEE = NVL(_Link_.LMCODOPE,space(2))
      this.w_DESCEE = NVL(_Link_.LMDESABB,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_LMIVACEE = space(2)
      endif
      this.w_DESCEE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_INTR_IDX,2])+'\'+cp_ToStr(_Link_.LMCODOPE,1)
      cp_ShowWarn(i_cKey,this.STU_INTR_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMIVACEE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.STU_INTR_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.STU_INTR_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_11.LMCODOPE as LMCODOPE211"+ ",link_2_11.LMDESABB as LMDESABB211"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_11 on STU_TRAS.LMIVACEE=link_2_11.LMCODOPE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_11"
          i_cKey=i_cKey+'+" and STU_TRAS.LMIVACEE=link_2_11.LMCODOPE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oLMDESCRI_1_1.value==this.w_LMDESCRI)
      this.oPgFrm.Page1.oPag.oLMDESCRI_1_1.value=this.w_LMDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oLM__DATA_1_2.value==this.w_LM__DATA)
      this.oPgFrm.Page1.oPag.oLM__DATA_1_2.value=this.w_LM__DATA
    endif
    if not(this.oPgFrm.Page1.oPag.oHOCIVADES_2_4.value==this.w_HOCIVADES)
      this.oPgFrm.Page1.oPag.oHOCIVADES_2_4.value=this.w_HOCIVADES
      replace t_HOCIVADES with this.oPgFrm.Page1.oPag.oHOCIVADES_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oAPRNORDES_2_6.value==this.w_APRNORDES)
      this.oPgFrm.Page1.oPag.oAPRNORDES_2_6.value=this.w_APRNORDES
      replace t_APRNORDES with this.oPgFrm.Page1.oPag.oAPRNORDES_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oAPRCAUDES_2_8.value==this.w_APRCAUDES)
      this.oPgFrm.Page1.oPag.oAPRCAUDES_2_8.value=this.w_APRCAUDES
      replace t_APRCAUDES with this.oPgFrm.Page1.oPag.oAPRCAUDES_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESIVA_2_10.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oDESIVA_2_10.value=this.w_DESIVA
      replace t_DESIVA with this.oPgFrm.Page1.oPag.oDESIVA_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCEE_2_12.value==this.w_DESCEE)
      this.oPgFrm.Page1.oPag.oDESCEE_2_12.value=this.w_DESCEE
      replace t_DESCEE with this.oPgFrm.Page1.oPag.oDESCEE_2_12.value
    endif
    if not(this.oPgFrm.Page1.oPag.oLMCODICE_1_14.value==this.w_LMCODICE)
      this.oPgFrm.Page1.oPag.oLMCODICE_1_14.value=this.w_LMCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oLMDETIVA_2_13.RadioValue()==this.w_LMDETIVA)
      this.oPgFrm.Page1.oPag.oLMDETIVA_2_13.SetRadio()
      replace t_LMDETIVA with this.oPgFrm.Page1.oPag.oLMDETIVA_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMHOCCAU_2_1.value==this.w_LMHOCCAU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMHOCCAU_2_1.value=this.w_LMHOCCAU
      replace t_LMHOCCAU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMHOCCAU_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oHOCCAUDES_2_2.value==this.w_HOCCAUDES)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oHOCCAUDES_2_2.value=this.w_HOCCAUDES
      replace t_HOCCAUDES with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oHOCCAUDES_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMHOCIVA_2_3.value==this.w_LMHOCIVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMHOCIVA_2_3.value=this.w_LMHOCIVA
      replace t_LMHOCIVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMHOCIVA_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMAPRNOR_2_5.value==this.w_LMAPRNOR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMAPRNOR_2_5.value=this.w_LMAPRNOR
      replace t_LMAPRNOR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMAPRNOR_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMAPRCAU_2_7.value==this.w_LMAPRCAU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMAPRCAU_2_7.value=this.w_LMAPRCAU
      replace t_LMAPRCAU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMAPRCAU_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMIVASTU_2_9.value==this.w_LMIVASTU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMIVASTU_2_9.value=this.w_LMIVASTU
      replace t_LMIVASTU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMIVASTU_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMIVACEE_2_11.value==this.w_LMIVACEE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMIVACEE_2_11.value=this.w_LMIVACEE
      replace t_LMIVACEE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oLMIVACEE_2_11.value
    endif
    cp_SetControlsValueExtFlds(this,'STU_TRAS')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_LM__DATA))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oLM__DATA_1_2.SetFocus()
            i_bnoObbl = !empty(.w_LM__DATA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_LMCODICE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oLMCODICE_1_14.SetFocus()
            i_bnoObbl = !empty(.w_LMCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (t_LMHOCCAU<>space(3));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .w_LMHOCCAU<>space(3)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_LMHOCCAU<>space(3))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_LMHOCCAU=space(5)
      .w_HOCCAUDES=space(35)
      .w_LMHOCIVA=space(5)
      .w_HOCIVADES=space(35)
      .w_LMAPRNOR=space(2)
      .w_APRNORDES=space(35)
      .w_LMAPRCAU=space(10)
      .w_APRCAUDES=space(35)
      .w_LMIVASTU=space(2)
      .w_DESIVA=space(35)
      .w_LMIVACEE=space(2)
      .w_DESCEE=space(35)
      .w_LMDETIVA=space(3)
      .DoRTCalc(1,3,.f.)
      if not(empty(.w_LMHOCCAU))
        .link_2_1('Full')
      endif
      .DoRTCalc(4,5,.f.)
      if not(empty(.w_LMHOCIVA))
        .link_2_3('Full')
      endif
      .DoRTCalc(6,7,.f.)
      if not(empty(.w_LMAPRNOR))
        .link_2_5('Full')
      endif
      .DoRTCalc(8,9,.f.)
      if not(empty(.w_LMAPRCAU))
        .link_2_7('Full')
      endif
      .DoRTCalc(10,11,.f.)
      if not(empty(.w_LMIVASTU))
        .link_2_9('Full')
      endif
      .DoRTCalc(12,13,.f.)
      if not(empty(.w_LMIVACEE))
        .link_2_11('Full')
      endif
      .DoRTCalc(14,16,.f.)
        .w_LMDETIVA = '   '
    endwith
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_LMHOCCAU = t_LMHOCCAU
    this.w_HOCCAUDES = t_HOCCAUDES
    this.w_LMHOCIVA = t_LMHOCIVA
    this.w_HOCIVADES = t_HOCIVADES
    this.w_LMAPRNOR = t_LMAPRNOR
    this.w_APRNORDES = t_APRNORDES
    this.w_LMAPRCAU = t_LMAPRCAU
    this.w_APRCAUDES = t_APRCAUDES
    this.w_LMIVASTU = t_LMIVASTU
    this.w_DESIVA = t_DESIVA
    this.w_LMIVACEE = t_LMIVACEE
    this.w_DESCEE = t_DESCEE
    this.w_LMDETIVA = this.oPgFrm.Page1.oPag.oLMDETIVA_2_13.RadioValue(.t.)
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_LMHOCCAU with this.w_LMHOCCAU
    replace t_HOCCAUDES with this.w_HOCCAUDES
    replace t_LMHOCIVA with this.w_LMHOCIVA
    replace t_HOCIVADES with this.w_HOCIVADES
    replace t_LMAPRNOR with this.w_LMAPRNOR
    replace t_APRNORDES with this.w_APRNORDES
    replace t_LMAPRCAU with this.w_LMAPRCAU
    replace t_APRCAUDES with this.w_APRCAUDES
    replace t_LMIVASTU with this.w_LMIVASTU
    replace t_DESIVA with this.w_DESIVA
    replace t_LMIVACEE with this.w_LMIVACEE
    replace t_DESCEE with this.w_DESCEE
    replace t_LMDETIVA with this.oPgFrm.Page1.oPag.oLMDETIVA_2_13.ToRadio()
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgslm_mhsPag1 as StdContainer
  Width  = 722
  height = 392
  stdWidth  = 722
  stdheight = 392
  resizeXpos=245
  resizeYpos=193
  bGlobalFont=.f.
  FontName      = "Arial"
  FontSize      = 8
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLMDESCRI_1_1 as StdField with uid="WBGKIWXBSG",rtseq=1,rtrep=.f.,;
    cFormVar = "w_LMDESCRI", cQueryName = "LMDESCRI",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione transcodifica",;
    HelpContextID = 128964095,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=168, Left=158, Top=9, InputMask=replicate('X',20)

  add object oLM__DATA_1_2 as StdField with uid="NRQFTRPSST",rtseq=2,rtrep=.f.,;
    cFormVar = "w_LM__DATA", cQueryName = "LM__DATA",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data elaborazione transcodifica",;
    HelpContextID = 81495543,;
    FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=76, Left=432, Top=9, cSayPict='"@K"', cGetPict='"@K"'

  add object oLMCODICE_1_14 as StdField with uid="IUWWTJAGKY",rtseq=16,rtrep=.f.,;
    cFormVar = "w_LMCODICE", cQueryName = "LMCODICE",nZero=2,;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 214550011,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=36, Left=118, Top=9, InputMask=replicate('X',2)


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=6, top=57, width=707,height=19,;
    caption='oHeaderDetail',;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    nNumberColumn=7,Field1="LMHOCCAU",Label1="Causale",Field2="HOCCAUDES",Label2="Descrizione",Field3="LMHOCIVA",Label3="IVA",Field4="LMAPRNOR",Label4="Norma",Field5="LMAPRCAU",Label5="Causale",Field6="LMIVASTU",Label6="IVA",Field7="LMIVACEE",Label7="INTRA",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 34434426

  add object oStr_1_3 as StdString with uid="VAWRUGPSNT",Visible=.t., Left=3, Top=9,;
    Alignment=1, Width=111, Height=18,;
    Caption="Associazione:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_4 as StdString with uid="BCXWBJEQSD",Visible=.t., Left=335, Top=9,;
    Alignment=1, Width=94, Height=18,;
    Caption="Elaborata il:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="NLDRUCTXMN",Visible=.t., Left=5, Top=39,;
    Alignment=2, Width=352, Height=18,;
    Caption="Ad hoc"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (isalt())
    endwith
  endfunc

  add object oStr_1_7 as StdString with uid="DMVTXKJTBX",Visible=.t., Left=398, Top=39,;
    Alignment=2, Width=269, Height=18,;
    Caption="Studio"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_9 as StdString with uid="GMYDRSPXFY",Visible=.t., Left=38, Top=299,;
    Alignment=1, Width=87, Height=17,;
    Caption="IVA adhoc:"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_10 as StdString with uid="BPQPQLGXOF",Visible=.t., Left=418, Top=299,;
    Alignment=1, Width=70, Height=17,;
    Caption="Norma:"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="NMIYWQTYKG",Visible=.t., Left=418, Top=323,;
    Alignment=1, Width=70, Height=17,;
    Caption="Causale:"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_12 as StdString with uid="IERGEICKWB",Visible=.t., Left=418, Top=347,;
    Alignment=1, Width=70, Height=17,;
    Caption="IVA:"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="YNSIGBCBPH",Visible=.t., Left=418, Top=371,;
    Alignment=1, Width=70, Height=17,;
    Caption="INTRA:"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="PLQTGEGIDL",Visible=.t., Left=6, Top=39,;
    Alignment=2, Width=352, Height=18,;
    Caption="AlteregoTop"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (Not isalt())
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="RAUQYVLLGW",Visible=.t., Left=0, Top=325,;
    Alignment=1, Width=125, Height=17,;
    Caption="Detrazione IVA acquisti:"  ;
  ,  FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_5 as StdBox with uid="ASXAHSFSDT",left=6, top=37, width=707,height=21

  add object oBox_1_17 as StdBox with uid="VUAMMYDJDC",left=356, top=38, width=2,height=19

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-4,top=76,;
    width=704+Sysmetric(5),height=int(fontmetric(1,"Arial",8,"")*12*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-3,top=77,width=703+Sysmetric(5),height=int(fontmetric(1,"Arial",8,"")*12*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='CAU_CONT|VOCIIVA|STU_NORM|STU_CAUS|STU_CIVA|STU_INTR|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oHOCIVADES_2_4.Refresh()
      this.Parent.oAPRNORDES_2_6.Refresh()
      this.Parent.oAPRCAUDES_2_8.Refresh()
      this.Parent.oDESIVA_2_10.Refresh()
      this.Parent.oDESCEE_2_12.Refresh()
      this.Parent.oLMDETIVA_2_13.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='CAU_CONT'
        oDropInto=this.oBodyCol.oRow.oLMHOCCAU_2_1
      case cFile='VOCIIVA'
        oDropInto=this.oBodyCol.oRow.oLMHOCIVA_2_3
      case cFile='STU_NORM'
        oDropInto=this.oBodyCol.oRow.oLMAPRNOR_2_5
      case cFile='STU_CAUS'
        oDropInto=this.oBodyCol.oRow.oLMAPRCAU_2_7
      case cFile='STU_CIVA'
        oDropInto=this.oBodyCol.oRow.oLMIVASTU_2_9
      case cFile='STU_INTR'
        oDropInto=this.oBodyCol.oRow.oLMIVACEE_2_11
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oHOCIVADES_2_4 as StdTrsField with uid="MNBUEYOJPG",rtseq=6,rtrep=.t.,;
    cFormVar="w_HOCIVADES",value=space(35),enabled=.f.,;
    HelpContextID = 98815211,;
    cTotal="", bFixedPos=.t., cQueryName = "HOCIVADES",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=223, Left=129, Top=299, InputMask=replicate('X',35)

  add object oAPRNORDES_2_6 as StdTrsField with uid="GTQDSUOFWN",rtseq=8,rtrep=.t.,;
    cFormVar="w_APRNORDES",value=space(35),enabled=.f.,;
    HelpContextID = 108641659,;
    cTotal="", bFixedPos=.t., cQueryName = "APRNORDES",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=222, Left=490, Top=299, InputMask=replicate('X',35)

  add object oAPRCAUDES_2_8 as StdTrsField with uid="KRKMWVDZNU",rtseq=10,rtrep=.t.,;
    cFormVar="w_APRCAUDES",value=space(35),enabled=.f.,;
    HelpContextID = 143572347,;
    cTotal="", bFixedPos=.t., cQueryName = "APRCAUDES",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=222, Left=490, Top=323, InputMask=replicate('X',35)

  add object oDESIVA_2_10 as StdTrsField with uid="IMQWIAQKTA",rtseq=12,rtrep=.t.,;
    cFormVar="w_DESIVA",value=space(35),enabled=.f.,;
    HelpContextID = 169558730,;
    cTotal="", bFixedPos=.t., cQueryName = "DESIVA",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=222, Left=490, Top=347, InputMask=replicate('X',35)

  add object oDESCEE_2_12 as StdTrsField with uid="NFMSDQAPPS",rtseq=14,rtrep=.t.,;
    cFormVar="w_DESCEE",value=space(35),enabled=.f.,;
    HelpContextID = 120668874,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCEE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=222, Left=490, Top=371, InputMask=replicate('X',35)

  add object oLMDETIVA_2_13 as StdTrsCombo with uid="IFMHCIFTXJ",rtrep=.t.,;
    cFormVar="w_LMDETIVA", RowSource=""+"Nessuna valorizzazione,"+"Fattura da considerare in liquidazione IVA,"+"Fatture da considerare in DAI,"+"Fatture con iva indetraibile,"+"Fatture da considerare in DAI integrativa" , ;
    ToolTipText = "Causale che identifica la metodologia di detrazione dell'iva",;
    HelpContextID = 230675959,;
    Height=25, Width=249, Left=129, Top=323,;
    cTotal="", cQueryName = "LMDETIVA",;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.


  func oLMDETIVA_2_13.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..LMDETIVA,&i_cF..t_LMDETIVA),this.value)
    return(iif(xVal =1,'   ',;
    iif(xVal =2,'001',;
    iif(xVal =3,'002',;
    iif(xVal =4,'003',;
    iif(xVal =5,'004',;
    space(3)))))))
  endfunc
  func oLMDETIVA_2_13.GetRadio()
    this.Parent.oContained.w_LMDETIVA = this.RadioValue()
    return .t.
  endfunc

  func oLMDETIVA_2_13.ToRadio()
    this.Parent.oContained.w_LMDETIVA=trim(this.Parent.oContained.w_LMDETIVA)
    return(;
      iif(this.Parent.oContained.w_LMDETIVA=='',1,;
      iif(this.Parent.oContained.w_LMDETIVA=='001',2,;
      iif(this.Parent.oContained.w_LMDETIVA=='002',3,;
      iif(this.Parent.oContained.w_LMDETIVA=='003',4,;
      iif(this.Parent.oContained.w_LMDETIVA=='004',5,;
      0))))))
  endfunc

  func oLMDETIVA_2_13.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgslm_mhsBodyRow as CPBodyRowCnt
  Width=694
  Height=int(fontmetric(1,"Arial",8,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.f.
  FontName      = "Arial"
  FontSize      = 8
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oLMHOCCAU_2_1 as StdTrsField with uid="KPMIIOANHA",rtseq=3,rtrep=.t.,;
    cFormVar="w_LMHOCCAU",value=space(5),;
    ToolTipText = "Codice causale ad hoc",;
    HelpContextID = 155576821,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=16, Width=63, Left=-2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_LMHOCCAU"

  func oLMHOCCAU_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMHOCCAU_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oLMHOCCAU_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oLMHOCCAU_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oLMHOCCAU_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_LMHOCCAU
    i_obj.ecpSave()
  endproc

  add object oHOCCAUDES_2_2 as StdTrsField with uid="MYEHNEQGPW",rtseq=4,rtrep=.t.,;
    cFormVar="w_HOCCAUDES",value=space(35),enabled=.f.,;
    HelpContextID = 143510763,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=16, Width=222, Left=67, Top=0, InputMask=replicate('X',35)

  add object oLMHOCIVA_2_3 as StdTrsField with uid="WDSPKRVOCV",rtseq=5,rtrep=.t.,;
    cFormVar="w_LMHOCIVA",value=space(5),;
    ToolTipText = "Codice IVA ad hoc",;
    HelpContextID = 213521911,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=16, Width=54, Left=293, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_LMHOCIVA"

  func oLMHOCIVA_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMHOCIVA_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oLMHOCIVA_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oLMHOCIVA_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'',this.parent.oContained
  endproc
  proc oLMHOCIVA_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_LMHOCIVA
    i_obj.ecpSave()
  endproc

  add object oLMAPRNOR_2_5 as StdTrsField with uid="WAWTYFYYSN",rtseq=7,rtrep=.t.,;
    cFormVar="w_LMAPRNOR",value=space(2),nZero=2,;
    ToolTipText = "Codice norma studio",;
    HelpContextID = 223697400,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=16, Width=62, Left=350, Top=0, cSayPict=["!!"], cGetPict=["!!"], InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="STU_NORM", cZoomOnZoom="GSLM_ACN", oKey_1_1="LMCODNOR", oKey_1_2="this.w_LMAPRNOR"

  func oLMAPRNOR_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMAPRNOR_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oLMAPRNOR_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STU_NORM','*','LMCODNOR',cp_AbsName(this.parent,'oLMAPRNOR_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSLM_ACN',"Norme studio",'',this.parent.oContained
  endproc
  proc oLMAPRNOR_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSLM_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LMCODNOR=this.parent.oContained.w_LMAPRNOR
    i_obj.ecpSave()
  endproc

  add object oLMAPRCAU_2_7 as StdTrsField with uid="LWZDIXWTTT",rtseq=9,rtrep=.t.,;
    cFormVar="w_LMAPRCAU",value=space(10),;
    ToolTipText = "Codice causale studio",;
    HelpContextID = 139811317,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=16, Width=145, Left=415, Top=0, cSayPict=["!!!!!!!!!!"], cGetPict=["!!!!!!!!!!"], InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="STU_CAUS", cZoomOnZoom="GSLM_ACC", oKey_1_1="LMCODCAU", oKey_1_2="this.w_LMAPRCAU"

  func oLMAPRCAU_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMAPRCAU_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oLMAPRCAU_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STU_CAUS','*','LMCODCAU',cp_AbsName(this.parent,'oLMAPRCAU_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSLM_ACC',"Causale contabile studio",'',this.parent.oContained
  endproc
  proc oLMAPRCAU_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSLM_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LMCODCAU=this.parent.oContained.w_LMAPRCAU
    i_obj.ecpSave()
  endproc

  add object oLMIVASTU_2_9 as StdTrsField with uid="CLUAAGIQCV",rtseq=11,rtrep=.t.,;
    cFormVar="w_LMIVASTU",value=space(2),nZero=2,;
    ToolTipText = "Codice IVA studio",;
    HelpContextID = 111224331,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=16, Width=62, Left=562, Top=0, cSayPict=["!!"], cGetPict=["!!"], InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="STU_CIVA", cZoomOnZoom="GSLM_AIV", oKey_1_1="LMCODIVA", oKey_1_2="this.w_LMIVASTU"

  func oLMIVASTU_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMIVASTU_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oLMIVASTU_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STU_CIVA','*','LMCODIVA',cp_AbsName(this.parent,'oLMIVASTU_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSLM_AIV',"Codici IVA studio",'',this.parent.oContained
  endproc
  proc oLMIVASTU_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSLM_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LMCODIVA=this.parent.oContained.w_LMIVASTU
    i_obj.ecpSave()
  endproc

  add object oLMIVACEE_2_11 as StdTrsField with uid="KUFFDWXYUD",rtseq=13,rtrep=.t.,;
    cFormVar="w_LMIVACEE",value=space(2),nZero=2,;
    ToolTipText = "Codice INTRA studio",;
    HelpContextID = 111224315,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=16, Width=62, Left=627, Top=0, cSayPict=["!!"], cGetPict=["!!"], InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="STU_INTR", cZoomOnZoom="GSLM_AIS", oKey_1_1="LMCODOPE", oKey_1_2="this.w_LMIVACEE"

  func oLMIVACEE_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMIVACEE_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oLMIVACEE_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STU_INTR','*','LMCODOPE',cp_AbsName(this.parent,'oLMIVACEE_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSLM_AIS',"Codice INTRA studio",'',this.parent.oContained
  endproc
  proc oLMIVACEE_2_11.mZoomOnZoom
    local i_obj
    i_obj=GSLM_AIS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LMCODOPE=this.parent.oContained.w_LMIVACEE
    i_obj.ecpSave()
  endproc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oLMHOCCAU_2_1.When()
    return(.t.)
  proc oLMHOCCAU_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oLMHOCCAU_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=11
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslm_mhs','STU_TRAS','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LMCODICE=STU_TRAS.LMCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
