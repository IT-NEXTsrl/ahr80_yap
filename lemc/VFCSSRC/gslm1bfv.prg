* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm1bfv                                                        *
*              EXPORT FATTURE VENDITA                                          *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-24                                                      *
* Last revis.: 2018-05-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm1bfv",oParentObject)
return(i_retval)

define class tgslm1bfv as StdBatch
  * --- Local variables
  w_REGNOVALIDA = .f.
  w_STRINGA = space(120)
  w_ACTUALPOS = 0
  w_PNSERIAL = space(10)
  w_TOTDOC = 0
  w_TOTDOCINVAL = 0
  w_ANCODSTU = space(6)
  w_APRNORM = space(2)
  w_APRCAU = space(3)
  w_IVASTU = space(2)
  w_IVASTUNOR = space(2)
  w_IVACEE = space(2)
  w_ESCI = .f.
  w_OKTRAS = .f.
  w_NOTRAS = 0
  w_IVCODIVA = space(5)
  w_PNCODCAU = space(5)
  w_PNCOMIVA = ctod("  /  /  ")
  w_PNDATREG = ctod("  /  /  ")
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_NUMRIGHE = 0
  w_PROGRIGA = 0
  w_CODCLI = space(15)
  w_CODATT = 0
  w_TIPOREG = space(1)
  w_NUMREG = 0
  w_CODSOGG = space(8)
  w_CODPIVA = space(16)
  w_CODFISC = space(16)
  w_PIUATTIV = space(1)
  w_CODSEZ = 0
  w_MESEAT = 0
  w_FATANNPREC = space(1)
  w_ESIGDIF = space(1)
  w_PAGESIGDIF = space(1)
  w_PNSERORI = space(10)
  w_CODFATTSI = space(1)
  w_IVAIND = .f.
  w_PERCIND = 0
  w_GESTNORMA = .f.
  w_NORMAIND100 = space(2)
  w_NORMANOIVA = space(2)
  w_NORMADIFF = space(2)
  w_FATTDIFF = .f.
  w_MESECOMP = space(4)
  w_ROWNUM = 0
  w_IVACASSA = space(1)
  w_ANFLESIG = space(1)
  w_CODATTPR = space(5)
  w_SERIALE = space(10)
  w_VENACQ = space(10)
  w_FLAG3000EU = space(1)
  w_DATREGOR = ctod("  /  /  ")
  w_DATDOCOR = ctod("  /  /  ")
  w_DATREGIP = ctod("  /  /  ")
  w_DATDOCIP = ctod("  /  /  ")
  w_NORFATT = space(2)
  w_CODNORCAU = space(2)
  w_CODCAUOR = space(5)
  w_CODTRAC = space(10)
  w_SPLIT = space(1)
  * --- WorkFile variables
  CONTI_idx=0
  STU_NORM_idx=0
  STU_PNTT_idx=0
  STU_TRAS_idx=0
  ATTIDETT_idx=0
  ATTIMAST_idx=0
  AZIENDA_idx=0
  COD_NORM_idx=0
  VOCIIVA_idx=0
  PNT_IVA_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT VERSO LO STUDIO DELLE FATTURE DI VENDITA e NOTE DI CREDITO
    this.w_IVAIND = .F.
    * --- Leggo se l'azienda esercita piu' Attivita'
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZATTIVI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZATTIVI;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PIUATTIV = NVL(cp_ToDate(_read_.AZATTIVI),cp_NullValue(_read_.AZATTIVI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Leggo codice norma esigibilit� differita
    * --- Read from COD_NORM
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.COD_NORM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COD_NORM_idx,2],.t.,this.COD_NORM_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CNNORMA1,CNNORMA8,CNNORMA9,CNNORM10"+;
        " from "+i_cTable+" COD_NORM where ";
            +"CNCODTRA = "+cp_ToStrODBC("LEMCO");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CNNORMA1,CNNORMA8,CNNORMA9,CNNORM10;
        from (i_cTable) where;
            CNCODTRA = "LEMCO";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_NORFATT = NVL(cp_ToDate(_read_.CNNORMA1),cp_NullValue(_read_.CNNORMA1))
      this.w_NORMANOIVA = NVL(cp_ToDate(_read_.CNNORMA8),cp_NullValue(_read_.CNNORMA8))
      this.w_NORMAIND100 = NVL(cp_ToDate(_read_.CNNORMA9),cp_NullValue(_read_.CNNORMA9))
      this.w_NORMADIFF = NVL(cp_ToDate(_read_.CNNORM10),cp_NullValue(_read_.CNNORM10))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT FATTVEND
    GO TOP
    * --- Ciclo sul cursore della fatture di vendita
    do while NOT EOF()
      this.w_CODFATTSI = " "
      this.w_GESTNORMA = .F.
      this.w_FATTDIFF = .F.
      this.w_PROGRIGA = 0
      this.w_ROWNUM = 0
      * --- Messaggio a schermo
      this.w_REGNOVALIDA = .F.
      AH_MSG("Export fatture di vendita : reg. num. %1  - data %2",.t.,.f.,.f.,STR(FATTVEND.PNNUMRER,6,0),dtoc(FATTVEND.PNDATREG))
      * --- Scrittura su Log
      this.w_STRINGA = Ah_MsgFormat("%1Export fatture di vendita : reg. num. %2 - data %3","          ",STR(FATTVEND.PNNUMRER,6,0)+"/"+ALLTRIM(STR(FATTVEND.PNCODUTE,4,0)),dtoc(FATTVEND.PNDATREG))
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      * --- Salvo la posizione attuale del record e il seriale della registrazione
      this.w_ACTUALPOS = RECNO()
      this.w_PNSERIAL = FATTVEND.PNSERIAL
      this.w_TOTDOC = 0
      this.w_NUMRIGHE = 0
      * --- Calcolo l'importo totale del documento
      this.w_IVACASSA = " "
      do while FATTVEND.PNSERIAL=this.w_PNSERIAL
        if isalt()
          this.w_NUMRIGHE = iif(NOT EMPTY(FATTVEND.IVCONTRO),this.w_NUMRIGHE+1,this.w_NUMRIGHE)
        else
          this.w_NUMRIGHE = this.w_NUMRIGHE+1
        endif
        this.w_TOTDOC = FATTVEND.IVIMPONI+FATTVEND.IVIMPIVA+this.w_TOTDOC
        this.w_PNCODCAU = NVL(FATTVEND.PNCODCAU," ")
        this.w_IVCODIVA = NVL(FATTVEND.IVCODIVA," ")
        * --- Verifico se devo gestire l'iva per cassa
        if ! this.w_GESTNORMA
          * --- Read from STU_TRAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.STU_TRAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LMAPRNOR"+;
              " from "+i_cTable+" STU_TRAS where ";
                  +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
                  +" and LMHOCCAU = "+cp_ToStrODBC(this.w_PNCODCAU);
                  +" and LMHOCIVA = "+cp_ToStrODBC(this.w_IVCODIVA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LMAPRNOR;
              from (i_cTable) where;
                  LMCODICE = this.oParentObject.w_ASSOCI;
                  and LMHOCCAU = this.w_PNCODCAU;
                  and LMHOCIVA = this.w_IVCODIVA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODNORCAU = NVL(cp_ToDate(_read_.LMAPRNOR),cp_NullValue(_read_.LMAPRNOR))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if upper(this.w_NORFATT) == upper(this.w_CODNORCAU)
            this.w_GESTNORMA = .T.
          endif
        endif
        if this.oParentObject.w_LMCHKNO="S" AND NVL(FATTVEND.CCFLIVDF,"N") ="S"
          this.w_IVACASSA = "S"
        endif
        skip 1
      enddo
      * --- Ripristino posizione
      go this.w_ACTUALPOS
      * --- Record di testata
      this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
      this.oParentObject.w_FATTVEND = this.oParentObject.w_FATTVEND+1
      this.w_TOTDOCINVAL = FATTVEND.PNTOTDOC
      FWRITE (this.oParentObject.hFile , "D30" , 3 )
      * --- Estraggo il codice attivit� dal registro iva usato sulla prima riga del castelletto IVA della Primanota
      this.w_TIPOREG = NVL(FATTVEND.IVTIPREG," ")
      this.w_NUMREG = NVL(FATTVEND.IVNUMREG,0)
      * --- Estraggo il codice attivit� contenente il tipo e numero registro IVA presenti in Primanota
      * --- Read from ATTIDETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATCODATT,ATCODSEZ"+;
          " from "+i_cTable+" ATTIDETT where ";
              +"ATNUMREG = "+cp_ToStrODBC(this.w_NUMREG);
              +" and ATTIPREG = "+cp_ToStrODBC(this.w_TIPOREG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATCODATT,ATCODSEZ;
          from (i_cTable) where;
              ATNUMREG = this.w_NUMREG;
              and ATTIPREG = this.w_TIPOREG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODATTPR = NVL(cp_ToDate(_read_.ATCODATT),cp_NullValue(_read_.ATCODATT))
        this.w_CODSEZ = NVL(cp_ToDate(_read_.ATCODSEZ),cp_NullValue(_read_.ATCODSEZ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Leggo il codice attivit� studio associato all'attivit� 
      * --- Read from ATTIMAST
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ATTIMAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIMAST_idx,2],.t.,this.ATTIMAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ATATTIVA"+;
          " from "+i_cTable+" ATTIMAST where ";
              +"ATCODATT = "+cp_ToStrODBC(this.w_CODATTPR);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ATATTIVA;
          from (i_cTable) where;
              ATCODATT = this.w_CODATTPR;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODATT = NVL(cp_ToDate(_read_.ATATTIVA),cp_NullValue(_read_.ATATTIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_PIUATTIV # "S"
        * --- Se l'azienda non gestisce pi� attivit� assegno al codice attivit� '00'
        this.w_CODATT = 00
      endif
      * --- Codice attivit� IVA
      FWRITE (this.oParentObject.hFile ,right("00"+alltrim(str(this.w_CODATT)),2), 2)
      * --- Recupero e inserisco l' anno e il mese della registrazione
      FWRITE(this.oParentObject.hFile,LEFT(dtos(FATTVEND.PNDATREG),6),6)
      * --- Recupero e inserisco l' anno di competenza
      FWRITE(this.oParentObject.hFile,nvl(FATTVEND.PNCOMPET,"0000"),4)
      * --- Recupero e inserisco il mese di competenza
      this.w_MESECOMP = iif(empty(dtos(nvl(FATTVEND.PNCOMIVA,ctod("  -  -    ")))),"00000000",dtos(nvl(FATTVEND.PNCOMIVA,ctod("  -  -    "))))
      FWRITE(this.oParentObject.hFile,SUBSTR(this.w_MESECOMP,5,2),2)
      * --- Sezione
      FWRITE(this.oParentObject.hFile,RIGHT("00"+ALLTRIM(STR(this.w_CODSEZ,2,0)),2),2)
      * --- Tipo del documento - Fattura = 1 ; NC = 2
      FWRITE(this.oParentObject.hFile,IIF(FATTVEND.PNTIPDOC="NC" OR FATTVEND.PNTIPDOC="NE", "2", "1"),1)
      * --- Tipo fattura
      FWRITE(this.oParentObject.hFile,SPACE(2),2)
      * --- Differenzio la gestione del numero documento in base al tipo fattura e al castelletto IVA
      *     Castelletto IVA della registrazione di Primanota movimenta sia il registro vendite che il registro Acquisti
      if FATTVEND.PNTIPDOC $ "FE-NE-AU-NU" and Not Empty(Nvl(FATTVEND.IVSERIAL," "))
        * --- Numero Documento - Vendita INTRA = > Numero Protocollo
        FWRITE(this.oParentObject.hFile,RIGHT("0000000"+ALLTRIM(STR(FATTVEND.PNNUMPRO)),7),7)
        * --- Numero Bis Protocollo
        FWRITE(this.oParentObject.hFile,RIGHT(" "+ALLTRIM(FATTVEND.PNALFPRO),1),1)
        * --- Numero fattura fornitore = Numero Protocollo
        FWRITE(this.oParentObject.hFile,LEFT(ALLTRIM(STR(FATTVEND.PNNUMPRO))+" "+ALLTRIM(FATTVEND.PNALFPRO)+SPACE(10),10),10)
      else
        * --- Numero Documento - Vendita = Numero Documento
        FWRITE(this.oParentObject.hFile,RIGHT("0000000"+ALLTRIM(STR(FATTVEND.PNNUMDOC)),7),7)
        * --- Numero Bis
        FWRITE(this.oParentObject.hFile,RIGHT(" "+ALLTRIM(FATTVEND.PNALFDOC),1),1)
        * --- Numero fattura fornitore = Numero documento
        FWRITE(this.oParentObject.hFile,LEFT(ALLTRIM(STR(FATTVEND.PNNUMDOC))+" "+ALLTRIM(FATTVEND.PNALFDOC)+SPACE(10),10),10)
      endif
      * --- Date
      * --- Data registrazione
      FWRITE(this.oParentObject.hFile,dtos(FATTVEND.PNDATREG),8)
      * --- Data fattura
      FWRITE(this.oParentObject.hFile,NVL(dtos(FATTVEND.PNDATDOC),"        "),8)
      * --- Data scadenza, Riscontro, Rateo - Non Gestite
      FWRITE(this.oParentObject.hFile,repl("0",8),8)
      FWRITE(this.oParentObject.hFile,repl("0",8),8)
      FWRITE(this.oParentObject.hFile,repl("0",8),8)
      * --- Codice Pagamento e tipo pagamento - Non gestite
      FWRITE(this.oParentObject.hFile,"000",3)
      FWRITE(this.oParentObject.hFile," ",1)
      * --- Centro di costo Cliente/Fornitore
      FWRITE(this.oParentObject.hFile,"0000",4)
      * --- Codice cliente/fornitore
      this.w_ANCODSTU = SPACE(6)
      this.w_PNTIPCLF = NVL(FATTVEND.PNTIPCLF," ")
      this.w_PNCODCLF = NVL(FATTVEND.PNCODCLF," ")
      if this.w_PNTIPCLF="F"
        * --- Se l'intestatario � un fornitore, allora si tratta di un movimento derivante da una fattura ue d'acquisto
        * --- Il fornitore deve essere sostituito con il cliente collegato
        this.w_CODCLI = SPACE(15)
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCONRIF"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCONRIF;
            from (i_cTable) where;
                ANTIPCON = this.w_PNTIPCLF;
                and ANCODICE = this.w_PNCODCLF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODCLI = NVL(cp_ToDate(_read_.ANCONRIF),cp_NullValue(_read_.ANCONRIF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_PNTIPCLF = "C"
        this.w_PNCODCLF = this.w_CODCLI
      endif
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU,ANCODSOG,ANPARIVA,ANCODFIS,ANFLESIG"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU,ANCODSOG,ANPARIVA,ANCODFIS,ANFLESIG;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCLF;
              and ANCODICE = this.w_PNCODCLF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        this.w_CODSOGG = NVL(cp_ToDate(_read_.ANCODSOG),cp_NullValue(_read_.ANCODSOG))
        this.w_CODPIVA = NVL(cp_ToDate(_read_.ANPARIVA),cp_NullValue(_read_.ANPARIVA))
        this.w_CODFISC = NVL(cp_ToDate(_read_.ANCODFIS),cp_NullValue(_read_.ANCODFIS))
        this.w_ANFLESIG = NVL(cp_ToDate(_read_.ANFLESIG),cp_NullValue(_read_.ANFLESIG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_IVACASSA = IIF(NVL(this.w_ANFLESIG,"N")<>"N"," ",this.w_IVACASSA)
      SELECT FATTVEND
      if NOT EMPTY(this.w_ANCODSTU)
        * --- Codice Cli/For
        FWRITE(this.oParentObject.hFile,right("000000"+alltrim(this.w_ANCODSTU),6),6)
        * --- Tipo codifica P/F/S
        FWRITE(this.oParentObject.hFile," ",1)
        * --- Codice codifica
        FWRITE(this.oParentObject.hFile,replicate(" ",16),16)
      else
        this.w_STRINGA = Ah_MsgFormat("%1Errore! Cliente %2 non ha un codice cliente nello studio associato","               ",NVL(this.w_PNCODCLF, " "))
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      SELECT FATTVEND
      * --- Descrizione alternativa aggiuntiva
      FWRITE(this.oParentObject.hFile,NVL(FATTVEND.DESSUP,SPACE(29)),29)
      * --- Tipo descrizione
      FWRITE(this.oParentObject.hFile,IIF(NOT EMPTY(NVL(FATTVEND.DESSUP," ")),"A"," "),1)
      * --- Flag Partita - Non Gestito
      FWRITE(this.oParentObject.hFile," ",1)
      * --- Mese stampa su reg.IVA per autotrasportatori
      if FATTVEND.CCFLAUTR = "S"
        this.w_MESEAT = IIF(INT((MONTH(FATTVEND.PNDATDOC)+2)/3)+1>4,MOD(INT((MONTH(FATTVEND.PNDATDOC)+2)/3)+1,4),INT((MONTH(FATTVEND.PNDATDOC)+2)/3)+1)*3
        FWRITE(this.oParentObject.hFile , RIGHT("0"+LTRIM(STR(this.w_MESEAT,2,0)),2) , 2)
      else
        FWRITE(this.oParentObject.hFile , "00" , 2)
      endif
      FWRITE(this.oParentObject.hFile , "00000000000" , 11)
      * --- Segno importo atre ritenute
      FWRITE(this.oParentObject.hFile,"+",1)
      * --- Importo ritenuta di acconto - Non Gestito
      if isalt()
        FWRITE(this.oParentObject.hFile,right( "00000000000" + alltrim( str( int(ABS(FATTVEND.IMP_EUR)), 11, 0 ) ) + right( str( FATTVEND.IMP_EUR, 11, 2 ) , 2 ) , 11 ))
      else
        FWRITE(this.oParentObject.hFile, "00000000000", 11 )
      endif
      * --- Segno importo ritenuta di acconto
      FWRITE(this.oParentObject.hFile,"+",1)
      * --- Non Contabilizzare: a 'N' solo nel caso di Storno Iva Incasso Fattura Esigibilit� Differita
      * --- La query filtra le registrazioni in funzione del tipo documento, nel caso tipo documento='NO' deve essere  necessariamente un pagamento ad esigibilt� differita
      *     (CAU_CONT.CCFLPDIF='S')
      FWRITE(this.oParentObject.hFile,IIF(FATTVEND.PNTIPDOC="NO", "N", " "),1)
      * --- Leggo i dati relativi all'esigibilit� differita imputati nella causale
      this.w_ESIGDIF = NVL(FATTVEND.CCFLIVDF,"N")
      this.w_PAGESIGDIF = NVL(FATTVEND.CCFLPDIF,"N")
      this.w_SPLIT = IIF(Nvl(FATTVEND.CCSCIPAG,"N")="S","S","N")
      if this.w_ESIGDIF="N" and this.w_PAGESIGDIF="N"
        * --- Codice mittente
        FWRITE(this.oParentObject.hFile,"AHREV",5)
        * --- Chiave fattura mittente
        FWRITE(this.oParentObject.hFile,padl(this.w_PNSERIAL,10,"0"),10)
      else
        * --- Gestione del codice legame per le registrazione con IVA sospesa
        if this.w_ESIGDIF="S"
          * --- Registrazione fattura di acquisto (Origine)
          if this.oParentObject.w_LMCHKNO="S" AND this.w_IVACASSA="S"
            this.w_CODFATTSI = "S"
          endif
          if this.w_GESTNORMA
            this.w_CODFATTSI = "S"
            * --- Codice mittente
            FWRITE(this.oParentObject.hFile,"AHREV",5)
            * --- Chiave fattura mittente
            FWRITE(this.oParentObject.hFile,padl(this.w_PNSERIAL,10,"0"),10)
            * --- Verifico se registrazione inerente ad una fattura di vendita/acquisto differita
            this.w_PNCOMIVA = nvl(FATTVEND.PNCOMIVA,ctod("  -  -    "))
            this.w_PNDATREG = nvl(FATTVEND.PNDATREG,ctod("  -  -    "))
            if month(this.w_PNDATREG) = 1
              this.w_FATTDIFF = month(this.w_PNCOMIVA) = 12
            else
              this.w_FATTDIFF = month(this.w_PNCOMIVA) < month(this.w_PNDATREG)
            endif
          else
            * --- Soggetti pubblici
            * --- Codice mittente
            FWRITE(this.oParentObject.hFile,"AHREV",5)
            * --- Chiave fattura mittente
            FWRITE(this.oParentObject.hFile,padl(this.w_PNSERIAL,10,"0"),10)
          endif
        else
          * --- Registrazione di incasso/pagamento
          * --- Select from GSLM_NOR
          do vq_exec with 'GSLM_NOR',this,'_Curs_GSLM_NOR','',.f.,.t.
          if used('_Curs_GSLM_NOR')
            select _Curs_GSLM_NOR
            locate for 1=1
            do while not(eof())
            * --- Leggo la chiave della registrazione di origine
            this.w_PNSERORI = _Curs_GSLM_NOR.SERIALOR
            * --- Leggo il codice  causale della registrazione di origine
            this.w_CODCAUOR = _Curs_GSLM_NOR.PNCODCAU
            this.w_IVCODIVA = NVL(_Curs_GSLM_NOR.IVCODIVA," ")
            * --- Verifico se devo gestire l'iva per cassa
            if ! this.w_GESTNORMA
              * --- Read from STU_TRAS
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.STU_TRAS_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "LMAPRNOR"+;
                  " from "+i_cTable+" STU_TRAS where ";
                      +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
                      +" and LMHOCCAU = "+cp_ToStrODBC(this.w_CODCAUOR);
                      +" and LMHOCIVA = "+cp_ToStrODBC(this.w_IVCODIVA);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  LMAPRNOR;
                  from (i_cTable) where;
                      LMCODICE = this.oParentObject.w_ASSOCI;
                      and LMHOCCAU = this.w_CODCAUOR;
                      and LMHOCIVA = this.w_IVCODIVA;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CODNORCAU = NVL(cp_ToDate(_read_.LMAPRNOR),cp_NullValue(_read_.LMAPRNOR))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if upper(this.w_NORFATT) == upper(this.w_CODNORCAU)
                this.w_GESTNORMA = .T.
              endif
            endif
            if NVL(this.w_ANFLESIG,"N")="N" AND this.w_PAGESIGDIF="S" AND this.oParentObject.w_LMCHKNO="S"
              this.w_CODFATTSI = "C"
            endif
              select _Curs_GSLM_NOR
              continue
            enddo
            use
          endif
          if isalt()
            if this.w_GESTNORMA
              * --- Se la registrazione di incasso/pagamento fa riferimento ad una registrazione con IVA sospesa, valorizzo correttamente il flag 'codice fattura'
              this.w_CODFATTSI = " "
              * --- Codice mittente
              FWRITE(this.oParentObject.hFile,"AHREV",5)
              * --- Chiave fattura mittente
              this.w_SERIALE = IIF(NOT EMPTY(this.w_PNSERORI),this.w_PNSERORI,this.w_PNSERIAL)
              FWRITE(this.oParentObject.hFile,padl(this.w_SERIALE,10,"0"),10)
            else
              * --- Soggetti pubblici
              * --- Codice mittente
              FWRITE(this.oParentObject.hFile,"AHREV",5)
              * --- Chiave fattura mittente
              FWRITE(this.oParentObject.hFile,padl(this.w_PNSERIAL,10,"0"),10)
            endif
          else
            if this.w_CODFATTSI="C" AND this.oParentObject.w_LMCHKNO="S"
              * --- Codice mittente
              FWRITE(this.oParentObject.hFile,"AHREV",5)
              * --- Chiave fattura mittente
              FWRITE(this.oParentObject.hFile,padl(this.w_PNSERORI,10,"0"),10)
            else
              if this.w_GESTNORMA
                * --- Se la registrazione di incasso/pagamento fa riferimento ad una registrazione con IVA sospesa, valorizzo correttamente il flag 'codice fattura'
                this.w_CODFATTSI = "I"
                * --- Codice mittente
                FWRITE(this.oParentObject.hFile,"AHREV",5)
                * --- Chiave fattura mittente
                FWRITE(this.oParentObject.hFile,padl(this.w_PNSERORI,10,"0"),10)
              else
                * --- Soggetti pubblici
                * --- Codice mittente
                FWRITE(this.oParentObject.hFile,"AHREV",5)
                * --- Chiave fattura mittente
                FWRITE(this.oParentObject.hFile,padl(this.w_PNSERIAL,10,"0"),10)
              endif
            endif
          endif
        endif
      endif
      * --- Flag - Nota variazione relativa a fatture anni precedenti
      if (empty(nvl(FATTVEND.PN__ANNO,0)) OR empty(nvl(FATTVEND.PN__MESE,0))) OR (YEAR(FATTVEND.PNDATDOC) = FATTVEND.PN__ANNO and MONTH(FATTVEND.PNDATDOC) = FATTVEND.PN__MESE)
        this.w_FATANNPREC = " "
        this.oParentObject.w_FILLER = SPACE(6)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,6)
      else
        this.w_FATANNPREC = "S"
        * --- Anno fattura rif.
        FWRITE(this.oParentObject.hFile,ALLTRIM(STR(FATTVEND.PN__ANNO)),4)
        * --- Mese fattura rif.
        FWRITE(this.oParentObject.hFile,RIGHT("00"+ALLTRIM(STR(FATTVEND.PN__MESE)),2),2)
      endif
      FWRITE(this.oParentObject.hFile,this.w_FATANNPREC,1)
      * --- Verifico se la registrazione di primanota movienta sia registri di Vendita che di acquito
      this.w_VENACQ = ""
      * --- Read from PNT_IVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2],.t.,this.PNT_IVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVSERIAL"+;
          " from "+i_cTable+" PNT_IVA where ";
              +"IVSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
              +" and IVTIPREG = "+cp_ToStrODBC("V");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVSERIAL;
          from (i_cTable) where;
              IVSERIAL = this.w_PNSERIAL;
              and IVTIPREG = "V";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_VENACQ = NVL(cp_ToDate(_read_.IVSERIAL),cp_NullValue(_read_.IVSERIAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY (this.w_VENACQ)
        this.w_VENACQ = ""
        * --- Read from PNT_IVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PNT_IVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2],.t.,this.PNT_IVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVSERIAL"+;
            " from "+i_cTable+" PNT_IVA where ";
                +"IVSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                +" and IVTIPREG = "+cp_ToStrODBC("A");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVSERIAL;
            from (i_cTable) where;
                IVSERIAL = this.w_PNSERIAL;
                and IVTIPREG = "A";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_VENACQ = NVL(cp_ToDate(_read_.IVSERIAL),cp_NullValue(_read_.IVSERIAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.oParentObject.w_FILLER = IIF ( this.w_TIPOREG="V" AND NOT EMPTY (this.w_VENACQ),"VEN", IIF ( this.w_TIPOREG="A" AND NOT EMPTY (this.w_VENACQ) ,"ACQ",space(3))) +"N"
      FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,4)
      * --- Scrivo il codice fattura normale/Sospesa/Incasso
      FWRITE(this.oParentObject.hFile,this.w_CODFATTSI,1)
      * --- AIFT - gestioni 3000 euro 
      *     Se non c' � AIFT il campo � sbiancato
      *     Se non c' e intestatario si imposta CodTipoFatt3000Euro='E' anche se non c � AIFT
      *     Negli altri casi si imposta in base ai valori assunti dai campi OSFLGEXT e OSTIPOPE 
      *     - P = Contratto corrispettivi periodici 
      *     - F = Corrispettivo incluso forzatamente 
      *     - E = Corrispettivo escluso forzatamente 
      *     - Blk = Corrispettivo non classificato con alcuno dei valori precedenti 
      if g_AIFT ="S" AND NOT EMPTY ( NVL (FATTVEND.PNCODCLF ," ")) 
        this.w_FLAG3000EU = IIF ( NVL (FATTVEND.OSFLGEXT," ") = "I" , "F" , IIF ( NVL (FATTVEND.OSFLGEXT," ") = "F" , "E" , IIF ( NVL (FATTVEND.OSTIPOPE," ") = "P" , "P" , IIF ( NVL (FATTVEND.OSTIPOPE," ") = "C" , "C" , IIF ( NVL (FATTVEND.OSTIPFAT," ") = "A" , "A" , IIF ( NVL (FATTVEND.OSTIPFAT," ") = "S" , "S" , SPACE(1) ) ) ) ) ) )
        FWRITE(this.oParentObject.hFile, this.w_FLAG3000EU ,1)
      else
        if EMPTY ( NVL (FATTVEND.PNCODCLF ," ")) 
          FWRITE(this.oParentObject.hFile, "E" , 1)
        else
          FWRITE(this.oParentObject.hFile, SPACE (1) , 1)
        endif
      endif
      this.oParentObject.w_FILLER = SPACE(3)
      FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,3)
      * --- Ciclo su tutte le righe IVA
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT FATTVEND
    enddo
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Butto giu' tutte le righe IVA della fattura in questione
    this.w_ESCI = .F.
    this.w_OKTRAS = .T.
    this.w_PROGRIGA = 0
    * --- Posiziono il puntatore al record
    SELECT FATTVEND
    this.w_ACTUALPOS = RECNO()
    do while FATTVEND.PNSERIAL=this.w_PNSERIAL AND NOT this.w_ESCI
      this.w_PROGRIGA = this.w_PROGRIGA+1
      if NOT EMPTY(FATTVEND.IVCONTRO) AND NVL(FATTVEND.IVIMPONI,0)<>0
        * --- Incremento numero di record scritti
        this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
        this.oParentObject.w_FATTVEND = this.oParentObject.w_FATTVEND+1
        * --- Se sono in questo caso devo scrivere testata e dettaglio
        FWRITE (this.oParentObject.hFile , "D31" , 3 )
        * --- Recupero e scrittura del codice di Aliquota IVA
        this.w_APRCAU = SPACE(3)
        this.w_APRNORM = SPACE(2)
        this.w_IVASTU = SPACE(2)
        this.w_IVACEE = SPACE(2)
        this.w_PNCODCAU = NVL(FATTVEND.PNCODCAU, " ")
        this.w_IVCODIVA = NVL(FATTVEND.IVCODIVA," ")
        this.w_ROWNUM = NVL(FATTVEND.CPROWNUM, 0)
        * --- Leggo se il codice IVA in oggetto ha una percentuale di indetraibilit�
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIND"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_IVCODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIND;
            from (i_cTable) where;
                IVCODIVA = this.w_IVCODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PERCIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_PERCIND # 0 AND this.w_GESTNORMA
          * --- Sono in presenza di IVA detraibili, aggiorno l avariabile per l'inserimento di una nuova riga per la registrazione con IVA per cassa 
          this.w_IVAIND = .T.
          * --- Incremento il numero di righe associate alla registrazione di uno
          this.w_NUMRIGHE = this.w_NUMRIGHE+1
        else
          this.w_IVAIND = .F.
        endif
        * --- Read from STU_TRAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.STU_TRAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LMAPRNOR,LMAPRCAU,LMIVASTU,LMIVACEE"+;
            " from "+i_cTable+" STU_TRAS where ";
                +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
                +" and LMHOCCAU = "+cp_ToStrODBC(this.w_PNCODCAU);
                +" and LMHOCIVA = "+cp_ToStrODBC(this.w_IVCODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LMAPRNOR,LMAPRCAU,LMIVASTU,LMIVACEE;
            from (i_cTable) where;
                LMCODICE = this.oParentObject.w_ASSOCI;
                and LMHOCCAU = this.w_PNCODCAU;
                and LMHOCIVA = this.w_IVCODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_APRNORM = NVL(cp_ToDate(_read_.LMAPRNOR),cp_NullValue(_read_.LMAPRNOR))
          this.w_APRCAU = NVL(cp_ToDate(_read_.LMAPRCAU),cp_NullValue(_read_.LMAPRCAU))
          this.w_IVASTU = NVL(cp_ToDate(_read_.LMIVASTU),cp_NullValue(_read_.LMIVASTU))
          this.w_IVACEE = NVL(cp_ToDate(_read_.LMIVACEE),cp_NullValue(_read_.LMIVACEE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT FATTVEND
        if this.w_PAGESIGDIF="S"
          * --- Calcolo del codice norma per IVA ad esigibilit� differita
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Verifica se ho il codice IVA studio definito nella norma
        if NOT EMPTY(this.w_APRNORM)
          this.w_IVASTUNOR = SPACE(2)
          * --- Read from STU_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.STU_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_NORM_idx,2],.t.,this.STU_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LMCODIVA"+;
              " from "+i_cTable+" STU_NORM where ";
                  +"LMCODNOR = "+cp_ToStrODBC(this.w_APRNORM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LMCODIVA;
              from (i_cTable) where;
                  LMCODNOR = this.w_APRNORM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_IVASTUNOR = NVL(cp_ToDate(_read_.LMCODIVA),cp_NullValue(_read_.LMCODIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          SELECT FATTVEND
          if this.w_IVASTUNOR="XX"
            this.w_IVASTUNOR = this.w_IVASTU
          endif
        else
          this.w_IVASTUNOR = this.w_IVASTU
        endif
        * --- Controllo Codice IVA
        if (EMPTY(this.w_IVASTUNOR) or this.w_IVASTUNOR="XX")
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Codice IVA non definito","               ")
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        * --- Controllo Codice IVA
        if (FATTVEND.PNTIPDOC="FE" OR FATTVEND.PNTIPDOC="NE") AND EMPTY(this.w_IVACEE)
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Codice CEE non definito","               ")
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        * --- Scrittura del codice IVA
        FWRITE(this.oParentObject.hFile,right("00"+(this.w_IVASTUNOR),2),2)
        * --- Imponibile
        if g_PERVAL=this.oParentObject.w_VALEUR
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(FATTVEND.IVIMPONI)),11,0))+RIGHT(STR(FATTVEND.IVIMPONI,12,2),2),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(FATTVEND.IVIMPONI),11,0)),11),11)
        endif
        if INLIST(FATTVEND.PNTIPDOC,"NC","NE","NU")
          FWRITE(this.oParentObject.hFile,IIF(FATTVEND.IVIMPONI>=0, "-", "+"),1)
        else
          FWRITE(this.oParentObject.hFile,IIF(FATTVEND.IVIMPONI>=0, "+", "-"),1)
        endif
        * --- Imposta
        if g_PERVAL=this.oParentObject.w_VALEUR
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(FATTVEND.IVIMPIVA)),11,0))+RIGHT(STR(FATTVEND.IVIMPIVA,12,2),2),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(FATTVEND.IVIMPIVA),11,0)),11),11)
        endif
        if INLIST(FATTVEND.PNTIPDOC,"NC","NE","NU")
          FWRITE(this.oParentObject.hFile,IIF(FATTVEND.IVIMPIVA>=0, "-", "+"),1)
        else
          FWRITE(this.oParentObject.hFile,IIF(FATTVEND.IVIMPIVA>=0, "+", "-"),1)
        endif
        * --- Scrittura codice norma
        if this.w_FATTDIFF and this.w_APRNORM="IS"
          if ! empty(this.w_NORMADIFF)
            FWRITE(this.oParentObject.hFile,this.w_NORMADIFF,2)
          else
            FWRITE(this.oParentObject.hFile,"DS",2)
          endif
        else
          FWRITE(this.oParentObject.hFile,right(space(2)+IIF(EMPTY(this.w_APRNORM),"  ",this.w_APRNORM),2),2)
        endif
        * --- Centro di costo
        FWRITE(this.oParentObject.hFile,"0000",4)
        * --- Contropartita (Sottoconto)
        this.w_ANCODSTU = SPACE(6)
        this.w_PNTIPCLF = NVL(FATTVEND.IVTIPCOP," ")
        this.w_PNCODCLF = NVL(FATTVEND.IVCONTRO," ")
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODSTU"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODSTU;
            from (i_cTable) where;
                ANTIPCON = this.w_PNTIPCLF;
                and ANCODICE = this.w_PNCODCLF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT FATTVEND
        if EMPTY(this.w_ANCODSTU)
          this.w_ANCODSTU = "000000"
          this.w_STRINGA = Ah_MsgFormat("%1Errore! Contropartita %2 non ha un codice sottoconto nello studio associato","               ",NVL(this.w_PNCODCLF, " "))
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        FWRITE(this.oParentObject.hFile,right("000000"+alltrim(this.w_ANCODSTU),6),6)
        * --- Importo totale del documento - Lo inserisco solo nell'ultima riga IVA
        if this.w_PROGRIGA=this.w_NUMRIGHE
          * --- Importo totale
          if g_PERVAL=this.oParentObject.w_VALEUR
            FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOC)),11,0))+RIGHT(STR(this.w_TOTDOC,12,2),2),11),11)
          else
            FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(this.w_TOTDOC),11,0)),11),11)
          endif
          * --- Segno
          if INLIST(FATTVEND.PNTIPDOC,"NC","NE","NU")
            FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOC>=0, "-", "+"),1)
          else
            FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOC>=0, "+", "-"),1)
          endif
          if NOT EMPTY(this.w_TOTDOCINVAL) and g_PERVAL <> NVL(FATTVEND.PNCODVAL,"   ")
            * --- Importo in valuta
            FWRITE(this.oParentObject.hFile, ;
            RIGHT("0000000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOCINVAL)),13, 0))+RIGHT(STR(this.w_TOTDOCINVAL,15,2),2),13),13)
            * --- Segno
            if INLIST(FATTVEND.PNTIPDOC,"NC","NE","NU")
              FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOCINVAL>=0, "-", "+"),1)
            else
              FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOCINVAL>=0, "+", "-"),1)
            endif
          else
            FWRITE(this.oParentObject.hFile,repl("0",13),13)
            FWRITE(this.oParentObject.hFile," ",1)
          endif
        else
          * --- Lascio blank i campi di importo totale
          FWRITE(this.oParentObject.hFile,repl("0",11),11)
          FWRITE(this.oParentObject.hFile," ",1)
          FWRITE(this.oParentObject.hFile,repl("0",13),13)
          FWRITE(this.oParentObject.hFile," ",1)
        endif
        * --- Codice Valuta 
        if g_PERVAL <> NVL(FATTVEND.PNCODVAL,"   ")
          FWRITE(this.oParentObject.hFile,NVL(FATTVEND.PNCODVAL,"   "),3)
        else
          FWRITE(this.oParentObject.hFile,"   ",3)
        endif
        * --- Codice CEE
        FWRITE(this.oParentObject.hFile,IIF(FATTVEND.PNTIPDOC="FE" OR FATTVEND.PNTIPDOC="NE" AND NOT EMPTY(this.w_IVACEE), left(this.w_IVACEE+"  ",2), "  "),2)
        * --- Data uno + Data due - PARCE
        FWRITE(this.oParentObject.hFile,repl("0",16),16)
        * --- Flag prestazioni di servizi
        do case
          case FATTVEND.IVFLGSER = "S"
            FWRITE(this.oParentObject.hFile,FATTVEND.IVFLGSER,1)
          case FATTVEND.IVFLGSER = "B"
            FWRITE(this.oParentObject.hFile,"N",1)
          case FATTVEND.IVFLGSER = "E"
            FWRITE(this.oParentObject.hFile," ",1)
          otherwise
            FWRITE(this.oParentObject.hFile," ",1)
        endcase
        * --- Codice contratto
        if g_AIFT="S"
          FWRITE(this.oParentObject.hFile, left ( NVL(FATTVEND.OSRIFCON," ") + space(30),30) , 30)
        else
          this.oParentObject.w_FILLER = SPACE(30)
          FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,30)
        endif
        * --- Identificativo documento originario per note di retifica
        if NVL (FATTVEND.OSTIPFAT, " ") ="N"
          FWRITE(this.oParentObject.hFile,left ( NVL(FATTVEND.OSRIFFAT , " ") + space (13) ,13),13)
        else
          this.oParentObject.w_FILLER = SPACE(13)
          FWRITE(this.oParentObject.hFile , this.oParentObject.w_FILLER ,13)
        endif
        * --- Filler
        this.oParentObject.w_FILLER = SPACE(68)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,68)
      else
        if (NVL(FATTVEND.IVCODIVA,SPACE(5))<>SPACE(5) AND (NVL(FATTVEND.IVIMPONI,0)<>0 OR NVL(FATTVEND.IVIMPIVA,0)<>0 OR NVL(FATTVEND.IVCFLOMA," ")="S") ) AND EMPTY(FATTVEND.IVCONTRO)
          * --- Marco questa registrazione come non valida
          this.w_REGNOVALIDA = .T.
        endif
      endif
      if this.w_REGNOVALIDA
        go this.w_ACTUALPOS
        this.w_NOTRAS = -1
        * --- Nel temporaneo di prima nota potrebbe essere presente la registrazione derivata da una fattura UE
        * --- Try
        local bErr_05419A08
        bErr_05419A08=bTrsErr
        this.Try_05419A08()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          * --- Essendo w_NOTRAS=-1 sovrascrivo quello che era stato inserito dalla routine GSLM_BFA relativo alle fatture UE
          * --- Write into STU_PNTT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.STU_PNTT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_PNTT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LMNUMTRA ="+cp_NullLink(cp_ToStrODBC(this.w_NOTRAS),'STU_PNTT','LMNUMTRA');
            +",LMNUMTR2 ="+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
                +i_ccchkf ;
            +" where ";
                +"LMSERIAL = "+cp_ToStrODBC(this.w_PNSERIAL);
                   )
          else
            update (i_cTable) set;
                LMNUMTRA = this.w_NOTRAS;
                ,LMNUMTR2 = 0;
                &i_ccchkf. ;
             where;
                LMSERIAL = this.w_PNSERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_05419A08
        * --- End
        this.w_STRINGA = Ah_MsgFormat("%1Errore! Contropartita contabile non definita su riga castelletto IVA ","               ")
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        SELECT FATTVEND
        do while FATTVEND.PNSERIAL=this.w_PNSERIAL
          * --- Avanzo il puntatore
          skip 1
        enddo
        this.w_ESCI = .T.
        this.w_OKTRAS = .F.
      endif
      * --- Se sono in presenza di IVA detraibili procedo all'inserimento di una nuova riga, solo per registrazioni inerenti a IVA per cassa
      if this.w_IVAIND and this.w_GESTNORMA
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      * --- Avanzo il puntatore
      if .not. this.w_ESCI
        skip 1
      endif
    enddo
    if this.w_OKTRAS
      * --- Try
      local bErr_053FE018
      bErr_053FE018=bTrsErr
      this.Try_053FE018()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- Essendo w_PROFIL=numero del trasferimento
        * --- (uguale a quanto inserito dalla routine GSLM_BFA relativo alle fatture UE in caso di successo)
        * --- (in caso di insuccesso il valore inserito dalla routine GSLM_BFA � -1)
        * --- Non bisogna scrivere niente
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_053FE018
      * --- End
      SELECT FATTVEND
    endif
  endproc
  proc Try_05419A08()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_PNTT
    i_nConn=i_TableProp[this.STU_PNTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NOTRAS),'STU_PNTT','LMNUMTRA');
      +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.w_NOTRAS,'LMNUMTR2',0)
      insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_NOTRAS;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='GSLM_BFV: Scrittura con errore in STU_PNTT'
      return
    endif
    return
  proc Try_053FE018()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_PNTT
    i_nConn=i_TableProp[this.STU_PNTT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'STU_PNTT','LMNUMTRA');
      +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.oParentObject.w_PROFIL,'LMNUMTR2',0)
      insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.oParentObject.w_PROFIL;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='GSLM_BFV: Scrittura in STU_PNTT'
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo del codice norma
    * --- Dichiarazione variabili per calcolo della norma
    this.w_CODTRAC = "LEMCO"
    * --- Select from GSLM_NOR
    do vq_exec with 'GSLM_NOR',this,'_Curs_GSLM_NOR','',.f.,.t.
    if used('_Curs_GSLM_NOR')
      select _Curs_GSLM_NOR
      locate for 1=1
      do while not(eof())
      * --- Leggo le date di registrazione e documento
      this.w_DATREGOR = cp_todate(_Curs_GSLM_NOR.DATREGOR)
      this.w_DATDOCOR = cp_todate(_Curs_GSLM_NOR.DATDOCOR)
      this.w_DATREGIP = cp_todate(_Curs_GSLM_NOR.DATREGIP) 
      this.w_DATDOCIP = cp_todate(_Curs_GSLM_NOR.DATDOCIP)
      * --- Leggo il codice  causale della registrazione di origine
      this.w_CODCAUOR = _Curs_GSLM_NOR.PNCODCAU
      this.w_IVCODIVA = NVL(_Curs_GSLM_NOR.IVCODIVA," ")
      * --- Verifico se devo gestire l'iva per cassa
      * --- Read from STU_TRAS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.STU_TRAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "LMAPRNOR"+;
          " from "+i_cTable+" STU_TRAS where ";
              +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
              +" and LMHOCCAU = "+cp_ToStrODBC(this.w_CODCAUOR);
              +" and LMHOCIVA = "+cp_ToStrODBC(this.w_IVCODIVA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          LMAPRNOR;
          from (i_cTable) where;
              LMCODICE = this.oParentObject.w_ASSOCI;
              and LMHOCCAU = this.w_CODCAUOR;
              and LMHOCIVA = this.w_IVCODIVA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODNORCAU = NVL(cp_ToDate(_read_.LMAPRNOR),cp_NullValue(_read_.LMAPRNOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if upper(this.w_NORFATT) == upper(this.w_CODNORCAU)
        this.w_GESTNORMA = .T.
      else
        this.w_GESTNORMA = .F.
        if isalt()
          if inlist(this.w_CODFATTSI,"S"," ") 
            this.w_APRNORM = this.w_NORMANOIVA
          endif
        else
          if inlist(this.w_CODFATTSI,"S","I") 
            this.w_APRNORM = this.w_NORMANOIVA
          endif
        endif
      endif
      exit
        select _Curs_GSLM_NOR
        continue
      enddo
      use
    endif
    if this.w_GESTNORMA
      * --- Applico la norma in base alla modalit� di utilizzo che viene soddisfatta
      do case
        case year(this.w_DATREGOR) = year(this.w_DATREGIP) and abs((this.w_DATDOCOR - this.w_DATREGIP)) <= 365
          * --- Modalit� 2 nella tabella
          * --- Read from COD_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COD_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COD_NORM_idx,2],.t.,this.COD_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNNORMA2"+;
              " from "+i_cTable+" COD_NORM where ";
                  +"CNCODTRA = "+cp_ToStrODBC(this.w_CODTRAC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNNORMA2;
              from (i_cTable) where;
                  CNCODTRA = this.w_CODTRAC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APRNORM = NVL(cp_ToDate(_read_.CNNORMA2),cp_NullValue(_read_.CNNORMA2))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        case year(this.w_DATREGOR) < year(this.w_DATREGIP) and abs((this.w_DATDOCOR - this.w_DATREGIP)) <= 365
          * --- Modalit� 3 nella tabella
          * --- Read from COD_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COD_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COD_NORM_idx,2],.t.,this.COD_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNNORMA3"+;
              " from "+i_cTable+" COD_NORM where ";
                  +"CNCODTRA = "+cp_ToStrODBC(this.w_CODTRAC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNNORMA3;
              from (i_cTable) where;
                  CNCODTRA = this.w_CODTRAC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APRNORM = NVL(cp_ToDate(_read_.CNNORMA3),cp_NullValue(_read_.CNNORMA3))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        case year(this.w_DATREGOR) = year(this.w_DATREGIP) and abs((this.w_DATDOCOR - this.w_DATREGIP)) > 365
          * --- Modalit� 4 nella tabella
          * --- Read from COD_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COD_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COD_NORM_idx,2],.t.,this.COD_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNNORMA4"+;
              " from "+i_cTable+" COD_NORM where ";
                  +"CNCODTRA = "+cp_ToStrODBC(this.w_CODTRAC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNNORMA4;
              from (i_cTable) where;
                  CNCODTRA = this.w_CODTRAC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APRNORM = NVL(cp_ToDate(_read_.CNNORMA4),cp_NullValue(_read_.CNNORMA4))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        case year(this.w_DATREGOR) < year(this.w_DATREGIP) and abs((this.w_DATDOCOR - this.w_DATREGIP)) > 365
          * --- Modalit� 5 nella tabella
          * --- Read from COD_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.COD_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COD_NORM_idx,2],.t.,this.COD_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNNORMA5"+;
              " from "+i_cTable+" COD_NORM where ";
                  +"CNCODTRA = "+cp_ToStrODBC(this.w_CODTRAC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNNORMA5;
              from (i_cTable) where;
                  CNCODTRA = this.w_CODTRAC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_APRNORM = NVL(cp_ToDate(_read_.CNNORMA5),cp_NullValue(_read_.CNNORMA5))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
      endcase
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiungo la riga relativa a IVA indetraibile per codice iva sospesa e iva indetraibile 
    this.w_PROGRIGA = this.w_PROGRIGA+1
    if NOT EMPTY(fattvenU.IVCONTRO) AND NVL(FATTVEND.IVIMPONI,0)<>0
      * --- Incremento numero di record scritti
      this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
      this.oParentObject.w_FATTVEND = this.oParentObject.w_FATTVEND+1
      * --- Se sono in questo caso devo scrivere testata e dettaglio
      FWRITE (this.oParentObject.hFile , "D31" , 3 )
      * --- Scrittura del codice IVA
      FWRITE(this.oParentObject.hFile,right("00"+(this.w_IVASTUNOR),2),2)
      * --- Imponibile
      if g_PERVAL=this.oParentObject.w_VALEUR
        if ! this.w_IVAIND
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(fattvenU.IVIMPONI)),11,0))+RIGHT(STR(fattvenU.IVIMPONI,12,2),2),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS((fattvenU.IVIMPONI/100)*this.w_PERCIND)),11,0))+RIGHT(STR((fattvenU.IVIMPONI/100)*this.w_PERCIND,12,2),2),11),11)
        endif
      else
        if ! this.w_IVAIND
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(fattvenU.IVIMPONI),11,0)),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS((fattvenU.IVIMPONI/100)*this.w_PERCIND),11,0)),11),11)
        endif
      endif
      if INLIST(fattvenU.PNTIPDOC,"NC","NE","NU")
        FWRITE(this.oParentObject.hFile,IIF(fattvenU.IVIMPONI>=0, "-", "+"),1)
      else
        FWRITE(this.oParentObject.hFile,IIF(fattvenU.IVIMPONI>=0, "+", "-"),1)
      endif
      * --- Imposta
      if g_PERVAL=this.oParentObject.w_VALEUR
        if ! this.w_IVAIND
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(fattvenU.IVIMPIVA)),11,0))+RIGHT(STR(fattvenU.IVIMPIVA,12,2),2),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS((fattvenU.IVIMPIVA/100)*this.w_PERCIND)),11,0))+RIGHT(STR((fattvenU.IVIMPIVA/100)*this.w_PERCIND,12,2),2),11),11)
        endif
      else
        if ! this.w_IVAIND
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(fattvenU.IVIMPIVA),11,0)),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS((fattvenU.IVIMPIVA/100)*this.w_PERCIND),11,0)),11),11)
        endif
      endif
      if INLIST(fattvenU.PNTIPDOC,"NC","NE","NU")
        FWRITE(this.oParentObject.hFile,IIF(fattvenU.IVIMPIVA>=0, "-", "+"),1)
      else
        FWRITE(this.oParentObject.hFile,IIF(fattvenU.IVIMPIVA>=0, "+", "-"),1)
      endif
      * --- Scrittura del Codice Norma
      if this.w_PAGESIGDIF#"S"
        if EMPTY(this.w_NORMAIND100)
          FWRITE(this.oParentObject.hFile,"90",2)
        else
          FWRITE(this.oParentObject.hFile,this.w_NORMAIND100,2)
        endif
      else
        if EMPTY(this.w_NORMANOIVA)
          FWRITE(this.oParentObject.hFile,"IG",2)
        else
          FWRITE(this.oParentObject.hFile,this.w_NORMANOIVA,2)
        endif
      endif
      * --- Centro di costo
      FWRITE(this.oParentObject.hFile,"0000",4)
      * --- Contropartita (Sottoconto)
      this.w_ANCODSTU = SPACE(6)
      this.w_PNTIPCLF = NVL(fattvenU.IVTIPCOP," ")
      this.w_PNCODCLF = NVL(fattvenU.IVCONTRO," ")
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANCODSTU"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANCODSTU;
          from (i_cTable) where;
              ANTIPCON = this.w_PNTIPCLF;
              and ANCODICE = this.w_PNCODCLF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT fattvenU
      if EMPTY(this.w_ANCODSTU)
        this.w_ANCODSTU = "000000"
        this.w_STRINGA = Ah_msgFormat("%1Errore! Contropartita %2 non ha un codice sottoconto nello studio associato","               ",NVL(this.w_PNCODCLF, " "))
        FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
        FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
        this.oParentObject.w_ERRORE = .T.
      endif
      FWRITE(this.oParentObject.hFile,right("000000"+alltrim(this.w_ANCODSTU),6),6)
      * --- Importo totale del documento - Lo inserisco solo nell'ultima riga IVA
      if this.w_PROGRIGA=this.w_NUMRIGHE
        * --- Importo totale
        if g_PERVAL=this.oParentObject.w_VALEUR
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOC)),11,0))+RIGHT(STR(this.w_TOTDOC,12,2),2),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(this.w_TOTDOC),11,0)),11),11)
        endif
        * --- Segno
        if fattvenU.PNTIPDOC="NC" OR fattvenU.PNTIPDOC="NE" OR fattvenU.PNTIPDOC="NU"
          FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOC>=0, "-", "+"),1)
        else
          FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOC>=0, "+", "-"),1)
        endif
        if NOT EMPTY(this.w_TOTDOCINVAL) and g_PERVAL <> NVL(fattvenU.PNCODVAL,"   ")
          * --- Importo in valuta
          FWRITE(this.oParentObject.hFile,;
          RIGHT("0000000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOCINVAL)),11, 0))+RIGHT(STR(this.w_TOTDOCINVAL,14,2),2),13),13)
          * --- Segno
          if fattvenU.PNTIPDOC="NE" or fattvenU.PNTIPDOC="NC" or fattvenU.PNTIPDOC="NU"
            FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOCINVAL>=0, "-", "+"),1)
          else
            FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOCINVAL>=0, "+", "-"),1)
          endif
        else
          FWRITE(this.oParentObject.hFile,repl("0",13),13)
          FWRITE(this.oParentObject.hFile," ",1)
        endif
      else
        * --- Lascio blank i campi di importo totale
        FWRITE(this.oParentObject.hFile,repl("0",11),11)
        FWRITE(this.oParentObject.hFile," ",1)
        FWRITE(this.oParentObject.hFile,repl("0",13),13)
        FWRITE(this.oParentObject.hFile," ",1)
      endif
      * --- Codice Valuta
      if g_PERVAL <> NVL(fattvenU.PNCODVAL,"   ")
        FWRITE(this.oParentObject.hFile,NVL(fattvenU.PNCODVAL,"   "),3)
      else
        FWRITE(this.oParentObject.hFile,"   ",3)
      endif
      * --- Codice CEE
      FWRITE(this.oParentObject.hFile,IIF(fattvenU.PNTIPDOC="FE" OR fattvenU.PNTIPDOC="NE" AND NOT EMPTY(this.w_IVACEE), right("  "+this.w_IVACEE,2), "  "),2)
      * --- Data uno + Data due - PARCE
      FWRITE(this.oParentObject.hFile,repl("0",16),16)
      * --- Flag prestazioni di servizi
      do case
        case FATTVEND.IVFLGSER = "S"
          FWRITE(this.oParentObject.hFile,FATTVEND.IVFLGSER,1)
        case FATTVEND.IVFLGSER = "B"
          FWRITE(this.oParentObject.hFile,"N",1)
        case FATTVEND.IVFLGSER = "E"
          FWRITE(this.oParentObject.hFile," ",1)
        otherwise
          FWRITE(this.oParentObject.hFile," ",1)
      endcase
      * --- Codice contratto
      if g_AIFT="S"
        FWRITE(this.oParentObject.hFile, left ( NVL(FATTVEND.OSRIFCON," ") + space(30),30) , 30)
      else
        this.oParentObject.w_FILLER = SPACE(30)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,30)
      endif
      * --- Identificativo documento originario per note di retifica
      if NVL (FATTVEND.OSTIPFAT, " ") ="N" AND g_AIFT="S"
        FWRITE(this.oParentObject.hFile,left ( NVL(FATTVEND.OSRIFFAT," ") + space (13) ,13),13)
      else
        this.oParentObject.w_FILLER = SPACE(13)
        FWRITE(this.oParentObject.hFile , this.oParentObject.w_FILLER ,13)
      endif
      * --- Filler
      this.oParentObject.w_FILLER = SPACE(68)
      FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,68)
    else
      if NVL(fattvenU.IVCODIVA,SPACE(5))<>SPACE(5) AND (NVL(fattvenU.IVIMPONI,0)<>0 OR NVL(fattvenU.IVIMPIVA,0)<>0 OR (NVL(fattvenU.IVCFLOMA," ")="S")) AND EMPTY(FATTVEND.IVCONTRO)
        * --- Marco questa registrazione come non valida
        this.w_REGNOVALIDA = .T.
      endif
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Incremento numero di record scritti
    this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
    this.oParentObject.w_FATTVEND = this.oParentObject.w_FATTVEND+1
    * --- Se sono in questo caso devo scrivere testata e dettaglio
    FWRITE (this.oParentObject.hFile , "D33" , 3 )
    * --- Numero protocollo scanner
    FWRITE (this.oParentObject.hFile , Space(20) , 20 )
    * --- File allegato
    FWRITE (this.oParentObject.hFile , Space(30) , 30 )
    * --- Codice classificazione
    FWRITE (this.oParentObject.hFile , Space(2) , 2 )
    * --- Flag Split Payment
    FWRITE (this.oParentObject.hFile , this.w_SPLIT , 1 )
    * --- Filler
    FWRITE (this.oParentObject.hFile , " " , 1 )
    * --- Detrazione IVA
    FWRITE (this.oParentObject.hFile , "1" , 1 )
    * --- Filler 144
    FWRITE (this.oParentObject.hFile , Space(142) , 142 )
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='STU_NORM'
    this.cWorkTables[3]='STU_PNTT'
    this.cWorkTables[4]='STU_TRAS'
    this.cWorkTables[5]='ATTIDETT'
    this.cWorkTables[6]='ATTIMAST'
    this.cWorkTables[7]='AZIENDA'
    this.cWorkTables[8]='COD_NORM'
    this.cWorkTables[9]='VOCIIVA'
    this.cWorkTables[10]='PNT_IVA'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_GSLM_NOR')
      use in _Curs_GSLM_NOR
    endif
    if used('_Curs_GSLM_NOR')
      use in _Curs_GSLM_NOR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
