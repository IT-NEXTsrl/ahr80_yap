* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_bed                                                        *
*              Exp/imp tabella trascodifica DBF                                *
*                                                                              *
*      Author: TAM SOFTWARE                                                    *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_393]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-10-12                                                      *
* Last revis.: 2000-03-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm_bed",oParentObject)
return(i_retval)

define class tgslm_bed as StdBatch
  * --- Local variables
  Messaggio = space(254)
  w_SCELTA = 0
  response = space(1)
  w_ESITO = .f.
  w_nt = 0
  w_nConn = 0
  w_cTable = space(50)
  w_cTableName = space(50)
  * --- WorkFile variables
  STU_TRAS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Export/Import Tabella Trascodifica DBF
    * --- Variabili maschera
    * --- Tipo operazione da effettuare (I=Import; E=Export)
    * --- Nomi e path dei files DBF
    this.w_ESITO = .T.
    * --- Variabili per le connessioni al database
    * --- Numero area di lavoro tabella
    this.w_nt = 0
    * --- Numero connessione
    this.w_nConn = 0
    * --- Nome fisico tabella
    this.w_cTable = ""
    * --- Nome Logico Tabella
    this.w_cTableName = ""
    if this.oParentObject.w_RADSELIE = "E"
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    wait clear
    if this.w_ESITO
      if this.oParentObject.w_RADSELIE="I"
        this.Messaggio = "Procedura di importazione terminata con successo"
      else
        this.Messaggio = "Procedura di esportazione terminata con successo"
      endif
    else
      if this.oParentObject.w_RADSELIE="I"
        this.Messaggio = "Procedura di importazione terminata con fallimento"
      else
        this.Messaggio = "Procedura di esportazione terminata con fallimento"
      endif
    endif
    ah_ErrorMsg(this.Messaggio,"�","")
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Export Tabelle
    * --- Scrittura files dbf
    this.w_SCELTA = 1
    * --- Database STU_TRAS - Archivio di destinazione
    Filedbf = alltrim(this.oParentObject.w_DBF1)
    Ah_Msg ("Export archivio di destinazione",.T.)
    this.w_cTableName = "STU_TRAS"
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if used("CursDBF")
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Export Destinazioni (STU_TRAS.DBF)
      if this.response="S"
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Import Tabelle
    * --- Lettura files DBF
    * --- ARCHIVI DI DESTINAZIONE
    Filedbf = alltrim(this.oParentObject.w_DBF1)
    Ah_msg("Import archivi di destinazione",.T.)
    * --- Import Destinazioni in tabella STU_TRAS
    if file(Filedbf)
      use (Filedbf) alias CursDBF
      if reccount("CursDBF")<>0
        go top
        scan for 1=1
        * --- Try
        local bErr_0397BE38
        bErr_0397BE38=bTrsErr
        this.Try_0397BE38()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          this.oParentObject.w_MSG = " "
          ah_Msg("Impossibile inserire la trascodifica",.T.)
          AddMsg("Impossibile inserire la trascodifica:%1",this,cp_translateDBMessage(message(), .T.))
          this.w_ESITO = .F.
        endif
        bTrsErr=bTrsErr or bErr_0397BE38
        * --- End
        endscan
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    this.Pag5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc
  proc Try_0397BE38()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_TRAS
    i_nConn=i_TableProp[this.STU_TRAS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_TRAS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMCODICE"+",LMDESCRI"+",CPROWNUM"+",LMHOCCAU"+",LMHOCIVA"+",LMAPRNOR"+",LMAPRCAU"+",LM__DATA"+",LMIVASTU"+",LMIVACEE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(CursDBF.LMCODICE),'STU_TRAS','LMCODICE');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.LMDESCRI),'STU_TRAS','LMDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.CPROWNUM),'STU_TRAS','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.LMHOCCAU),'STU_TRAS','LMHOCCAU');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.LMHOCIVA),'STU_TRAS','LMHOCIVA');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.LMAPRNOR),'STU_TRAS','LMAPRNOR');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.LMAPRCAU),'STU_TRAS','LMAPRCAU');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.LM__DATA),'STU_TRAS','LM__DATA');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.LMIVASTU),'STU_TRAS','LMIVASTU');
      +","+cp_NullLink(cp_ToStrODBC(CursDBF.LMIVACEE),'STU_TRAS','LMIVACEE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMCODICE',CursDBF.LMCODICE,'LMDESCRI',CursDBF.LMDESCRI,'CPROWNUM',CursDBF.CPROWNUM,'LMHOCCAU',CursDBF.LMHOCCAU,'LMHOCIVA',CursDBF.LMHOCIVA,'LMAPRNOR',CursDBF.LMAPRNOR,'LMAPRCAU',CursDBF.LMAPRCAU,'LM__DATA',CursDBF.LM__DATA,'LMIVASTU',CursDBF.LMIVASTU,'LMIVACEE',CursDBF.LMIVACEE)
      insert into (i_cTable) (LMCODICE,LMDESCRI,CPROWNUM,LMHOCCAU,LMHOCIVA,LMAPRNOR,LMAPRCAU,LM__DATA,LMIVASTU,LMIVACEE &i_ccchkf. );
         values (;
           CursDBF.LMCODICE;
           ,CursDBF.LMDESCRI;
           ,CursDBF.CPROWNUM;
           ,CursDBF.LMHOCCAU;
           ,CursDBF.LMHOCIVA;
           ,CursDBF.LMAPRNOR;
           ,CursDBF.LMAPRCAU;
           ,CursDBF.LM__DATA;
           ,CursDBF.LMIVASTU;
           ,CursDBF.LMIVACEE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento in STU_TRAS'
      return
    endif
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica esistenza file da esportare
    this.response = "S"
    if file( Filedbf )
      if not ah_YesNo("E' gi� presente il file: %1 sovrascriverlo?","",FileDBF)
        this.response = "N"
      endif
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura cursore generico
    if used("Cursdbf")
      select Cursdbf
      use
    endif
  endproc


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura su file DBF
    if used("CursDBF")
      select CursDBF
      go top
      copy to (Filedbf) TYPE FOX2X
    endif
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea il cursore "CursDBF" dalla tabella (w_cTableName)
    *     
    *     Ritorno:
    *     - CursDBF
    *     - i_Rows con numero record tabella
    * --- --
    if used("CursDBF")
      select CursDBF
      use
    endif
    * --- Cerca area di lavoro tabella aperta in Work Table
    private cIDX
    cIDX = alltrim(this.w_cTableName)+"_idx"
    this.w_nT = this.&cIDX
    this.w_nConn = i_TableProp[ this.w_nT ,3]
    this.w_cTable = cp_SetAzi(i_TableProp[ this.w_nT ,2])
    if this.w_nConn<>0
      * --- SQL Server
      i_Rows = SqlExec( this.w_nConn,"select * from " + this.w_cTable,"CursDBF")
      i_Rows = iif(used("CursDBF"),reccount(),0)
    else
      * --- Fox Pro
      cTable = this.w_cTable
      select * from (cTable) into cursor CursDBF
      i_Rows = _TALLY
    endif
    if not used("CursDBF")
      Ah_ErrorMsg("Impossibile creare il cursore di esportazione per la tabella %1",48," ",this.w_cTableName)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='STU_TRAS'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
