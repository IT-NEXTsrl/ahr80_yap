* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_bex                                                        *
*              Export studio prog.iniziale                                     *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-23                                                      *
* Last revis.: 2015-05-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm_bex",oParentObject)
return(i_retval)

define class tgslm_bex as StdBatch
  * --- Local variables
  w_CURRDIR = space(50)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- BATCH CHE INIZIALIZZA LE SELEZIONI PER L'EXPORT VERSO LO STUDIO (LANCIATO DA GSLM_KEX)
    this.oParentObject.w_NOREGTOEXP = .F.
    this.oParentObject.w_DATINI = CTOD("  -  -  ")
    this.oParentObject.w_DATFIN = CTOD("  -  -  ")
    vq_exec("..\LEMC\EXE\QUERY\GSLM_EVI.VQR",this,"__TMP__")
    * --- Recupero la data prima registrazione da trasferire
    this.oParentObject.w_DATINI = CP_TODATE(__TMP__.DATINI)
    * --- Recupero la data dell'ultima registrazione da trasferire
    this.oParentObject.w_DATFIN = CP_TODATE(__TMP__.DATFIN)
    * --- Controllo se esistono registrazioni da trasferire
    if EMPTY(this.oParentObject.w_DATINI)
      this.oParentObject.w_NOREGTOEXP = .T.
    endif
    * --- Inizializzazione e preparazione percorso di default
    this.oParentObject.w_PERCORSO = SYS(5)+SYS(2003)+"\"+iCASE(g_TRAEXP $ "C-B","CONTB",g_TRAEXP="G","MOVAGO","COGEN")
    if !DIRECTORY(this.oParentObject.w_PERCORSO)
      * --- Crea la directory LEMCO all'interno della dir EXE
      this.w_CURRDIR = SYS(5)+SYS(2003)
      do case
        case g_TRAEXP ="G"
          MKDIR "MOVAGO"
        case g_TRAEXP $ "C-B"
          MKDIR "CONTB"
        otherwise
          MKDIR "COGEN"
      endcase
    endif
    * --- Inizializzazione variabili necessarie alla compilazione del file
    this.oParentObject.w_CLI = "S"
    this.oParentObject.w_FOR = "S"
    this.oParentObject.w_SOT = "N"
    this.oParentObject.w_PNTIVA = IIF(this.oParentObject.w_NOREGTOEXP,"N","S")
    this.oParentObject.w_PNTNOIVA = IIF(this.oParentObject.w_NOREGTOEXP,"N","S")
    this.oParentObject.w_TIPCLIFOR = IIF(this.oParentObject.w_NOREGTOEXP,"T","M")
    * --- Ritorna alla maschera di export dopo aver valorizzato w_DATINI, w_DATFIN, w_PERCORSO, w_CLI, w_FOR, w_SOT, w_PNT, w_TIPCLIFOR, w_NOREGTOEXP
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
