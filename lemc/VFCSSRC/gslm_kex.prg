* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_kex                                                        *
*              Export avanzato                                                 *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_149]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-23                                                      *
* Last revis.: 2015-04-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gslm_kex
* Esco se il modulo � in semplificata
IF NOT LMAVANZATO()
  do cplu_erm with "La funzione � attiva con il Modulo in modalit� Avanzata"
 return
ENDIF
* --- Fine Area Manuale
return(createobject("tgslm_kex",oParentObject))

* --- Class definition
define class tgslm_kex as StdForm
  Top    = 30
  Left   = 75

  * --- Standard Properties
  Width  = 478
  Height = 332
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-04-30"
  HelpContextID=65649769
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  cPrg = "gslm_kex"
  cComment = "Export avanzato"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_AZIENDA = space(5)
  w_AZINOM = space(40)
  w_NOREGTOEXP = .F.
  w_CLI = space(1)
  w_FOR = space(1)
  w_SOT = space(1)
  w_PNTIVA = space(1)
  w_PNTNOIVA = space(1)
  w_TIPCLIFOR = space(10)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_NUMINI = 0
  w_NUMFIN = 0
  w_PERCORSO = space(50)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslm_kexPag1","gslm_kex",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCLI_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA=space(5)
      .w_AZINOM=space(40)
      .w_NOREGTOEXP=.f.
      .w_CLI=space(1)
      .w_FOR=space(1)
      .w_SOT=space(1)
      .w_PNTIVA=space(1)
      .w_PNTNOIVA=space(1)
      .w_TIPCLIFOR=space(10)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_NUMINI=0
      .w_NUMFIN=0
      .w_PERCORSO=space(50)
        .w_AZIENDA = i_codazi
        .w_AZINOM = g_RAGAZI
          .DoRTCalc(3,11,.f.)
        .w_NUMINI = 1
        .w_NUMFIN = 999999
      .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_29.Calculate(ah_MsgFormat("Questo programma consente il trasferimento dei movimenti di primanota verso%0la procedura per commercialisti Zucchetti attraverso il protocollo %1",IIF(g_TRAEXP='C','LEMCO x CONTB.',IIF(g_TRAEXP='G', 'MOVAGO','LEMCO x COGEN.'))))
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
    endwith
    this.DoRTCalc(14,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate(ah_MsgFormat("Questo programma consente il trasferimento dei movimenti di primanota verso%0la procedura per commercialisti Zucchetti attraverso il protocollo %1",IIF(g_TRAEXP='C','LEMCO x CONTB.',IIF(g_TRAEXP='G', 'MOVAGO','LEMCO x COGEN.'))))
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_28.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_29.Calculate(ah_MsgFormat("Questo programma consente il trasferimento dei movimenti di primanota verso%0la procedura per commercialisti Zucchetti attraverso il protocollo %1",IIF(g_TRAEXP='C','LEMCO x CONTB.',IIF(g_TRAEXP='G', 'MOVAGO','LEMCO x COGEN.'))))
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSOT_1_6.enabled = this.oPgFrm.Page1.oPag.oSOT_1_6.mCond()
    this.oPgFrm.Page1.oPag.oPNTIVA_1_7.enabled = this.oPgFrm.Page1.oPag.oPNTIVA_1_7.mCond()
    this.oPgFrm.Page1.oPag.oPNTNOIVA_1_8.enabled = this.oPgFrm.Page1.oPag.oPNTNOIVA_1_8.mCond()
    this.oPgFrm.Page1.oPag.oTIPCLIFOR_1_9.enabled = this.oPgFrm.Page1.oPag.oTIPCLIFOR_1_9.mCond()
    this.oPgFrm.Page1.oPag.oDATINI_1_10.enabled = this.oPgFrm.Page1.oPag.oDATINI_1_10.mCond()
    this.oPgFrm.Page1.oPag.oDATFIN_1_11.enabled = this.oPgFrm.Page1.oPag.oDATFIN_1_11.mCond()
    this.oPgFrm.Page1.oPag.oNUMINI_1_12.enabled = this.oPgFrm.Page1.oPag.oNUMINI_1_12.mCond()
    this.oPgFrm.Page1.oPag.oNUMFIN_1_13.enabled = this.oPgFrm.Page1.oPag.oNUMFIN_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_28.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_29.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gslm_kex
    If cevent='Elabora'
       if g_TRAEXP='A'
         This.Notifyevent("Cogen")
       endif
       if g_TRAEXP='C'
         This.Notifyevent("Contb")
       endif
       if g_TRAEXP='G'
         This.Notifyevent("Movago")
       endif
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAZIENDA_1_1.value==this.w_AZIENDA)
      this.oPgFrm.Page1.oPag.oAZIENDA_1_1.value=this.w_AZIENDA
    endif
    if not(this.oPgFrm.Page1.oPag.oAZINOM_1_2.value==this.w_AZINOM)
      this.oPgFrm.Page1.oPag.oAZINOM_1_2.value=this.w_AZINOM
    endif
    if not(this.oPgFrm.Page1.oPag.oCLI_1_4.RadioValue()==this.w_CLI)
      this.oPgFrm.Page1.oPag.oCLI_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFOR_1_5.RadioValue()==this.w_FOR)
      this.oPgFrm.Page1.oPag.oFOR_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSOT_1_6.RadioValue()==this.w_SOT)
      this.oPgFrm.Page1.oPag.oSOT_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPNTIVA_1_7.RadioValue()==this.w_PNTIVA)
      this.oPgFrm.Page1.oPag.oPNTIVA_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPNTNOIVA_1_8.RadioValue()==this.w_PNTNOIVA)
      this.oPgFrm.Page1.oPag.oPNTNOIVA_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCLIFOR_1_9.RadioValue()==this.w_TIPCLIFOR)
      this.oPgFrm.Page1.oPag.oTIPCLIFOR_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_10.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_10.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_11.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_11.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINI_1_12.value==this.w_NUMINI)
      this.oPgFrm.Page1.oPag.oNUMINI_1_12.value=this.w_NUMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFIN_1_13.value==this.w_NUMFIN)
      this.oPgFrm.Page1.oPag.oNUMFIN_1_13.value=this.w_NUMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPERCORSO_1_14.value==this.w_PERCORSO)
      this.oPgFrm.Page1.oPag.oPERCORSO_1_14.value=this.w_PERCORSO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATINI))  and (.w_PNTIVA='S' OR .w_PNTNOIVA='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_10.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATFIN))  and (.w_PNTIVA='S' OR .w_PNTNOIVA='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_11.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NUMINI))  and (.w_PNTIVA='S' OR .w_PNTNOIVA='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMINI_1_12.SetFocus()
            i_bnoObbl = !empty(.w_NUMINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_NUMFIN))  and (.w_PNTIVA='S' OR .w_PNTNOIVA='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMFIN_1_13.SetFocus()
            i_bnoObbl = !empty(.w_NUMFIN)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgslm_kexPag1 as StdContainer
  Width  = 474
  height = 332
  stdWidth  = 474
  stdheight = 332
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAZIENDA_1_1 as StdField with uid="SGSNRRZHJZ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_AZIENDA", cQueryName = "AZIENDA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 88093190,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=136, Top=50, InputMask=replicate('X',5)

  add object oAZINOM_1_2 as StdField with uid="SGNIOVJVRB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_AZINOM", cQueryName = "AZINOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 240726534,;
   bGlobalFont=.t.,;
    Height=21, Width=278, Left=186, Top=50, InputMask=replicate('X',40)

  add object oCLI_1_4 as StdCheck with uid="JYLBZVCLJL",rtseq=4,rtrep=.f.,left=8, top=108, caption="Clienti",;
    HelpContextID = 65330138,;
    cFormVar="w_CLI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCLI_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCLI_1_4.GetRadio()
    this.Parent.oContained.w_CLI = this.RadioValue()
    return .t.
  endfunc

  func oCLI_1_4.SetRadio()
    this.Parent.oContained.w_CLI=trim(this.Parent.oContained.w_CLI)
    this.value = ;
      iif(this.Parent.oContained.w_CLI=='S',1,;
      0)
  endfunc

  add object oFOR_1_5 as StdCheck with uid="KZTWPQGJAE",rtseq=5,rtrep=.f.,left=8, top=125, caption="Fornitori",;
    HelpContextID = 65292458,;
    cFormVar="w_FOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFOR_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFOR_1_5.GetRadio()
    this.Parent.oContained.w_FOR = this.RadioValue()
    return .t.
  endfunc

  func oFOR_1_5.SetRadio()
    this.Parent.oContained.w_FOR=trim(this.Parent.oContained.w_FOR)
    this.value = ;
      iif(this.Parent.oContained.w_FOR=='S',1,;
      0)
  endfunc

  add object oSOT_1_6 as StdCheck with uid="FVECBANJFX",rtseq=6,rtrep=.f.,left=8, top=142, caption="Conti",;
    HelpContextID = 65284058,;
    cFormVar="w_SOT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSOT_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSOT_1_6.GetRadio()
    this.Parent.oContained.w_SOT = this.RadioValue()
    return .t.
  endfunc

  func oSOT_1_6.SetRadio()
    this.Parent.oContained.w_SOT=trim(this.Parent.oContained.w_SOT)
    this.value = ;
      iif(this.Parent.oContained.w_SOT=='S',1,;
      0)
  endfunc

  func oSOT_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_TRAEXP<>'C' AND g_TRAEXP<>'G')
    endwith
   endif
  endfunc

  add object oPNTIVA_1_7 as StdCheck with uid="WUBEJNQMUC",rtseq=7,rtrep=.f.,left=8, top=159, caption="Primanota IVA",;
    ToolTipText = "Se attivo vengono esportate le registrazioni contabili che movimentano l'IVA",;
    HelpContextID = 46454518,;
    cFormVar="w_PNTIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPNTIVA_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPNTIVA_1_7.GetRadio()
    this.Parent.oContained.w_PNTIVA = this.RadioValue()
    return .t.
  endfunc

  func oPNTIVA_1_7.SetRadio()
    this.Parent.oContained.w_PNTIVA=trim(this.Parent.oContained.w_PNTIVA)
    this.value = ;
      iif(this.Parent.oContained.w_PNTIVA=='S',1,;
      0)
  endfunc

  func oPNTIVA_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_NOREGTOEXP)
    endwith
   endif
  endfunc

  add object oPNTNOIVA_1_8 as StdCheck with uid="NHPLLYSJTN",rtseq=8,rtrep=.f.,left=8, top=176, caption="Primanota no IVA",;
    ToolTipText = "Se attivo vengono esportate le registrazioni contabili che non movimentano l'IVA",;
    HelpContextID = 94775497,;
    cFormVar="w_PNTNOIVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPNTNOIVA_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPNTNOIVA_1_8.GetRadio()
    this.Parent.oContained.w_PNTNOIVA = this.RadioValue()
    return .t.
  endfunc

  func oPNTNOIVA_1_8.SetRadio()
    this.Parent.oContained.w_PNTNOIVA=trim(this.Parent.oContained.w_PNTNOIVA)
    this.value = ;
      iif(this.Parent.oContained.w_PNTNOIVA=='S',1,;
      0)
  endfunc

  func oPNTNOIVA_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (not .w_NOREGTOEXP)
    endwith
   endif
  endfunc


  add object oTIPCLIFOR_1_9 as StdCombo with uid="HMPDZDBXFS",rtseq=9,rtrep=.f.,left=110,top=108,width=132,height=21;
    , ToolTipText = "Tipo clienti/fornitori che devono essere esportati";
    , HelpContextID = 98658395;
    , cFormVar="w_TIPCLIFOR",RowSource=""+"Tutti,"+"Solo movimentati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCLIFOR_1_9.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'M',;
    space(10))))
  endfunc
  func oTIPCLIFOR_1_9.GetRadio()
    this.Parent.oContained.w_TIPCLIFOR = this.RadioValue()
    return .t.
  endfunc

  func oTIPCLIFOR_1_9.SetRadio()
    this.Parent.oContained.w_TIPCLIFOR=trim(this.Parent.oContained.w_TIPCLIFOR)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCLIFOR=='T',1,;
      iif(this.Parent.oContained.w_TIPCLIFOR=='M',2,;
      0))
  endfunc

  func oTIPCLIFOR_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PNTIVA='S' OR .w_PNTNOIVA='S')
    endwith
   endif
  endfunc

  add object oDATINI_1_10 as StdField with uid="SOAVOGTALV",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 172280118,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=236, Top=142

  func oDATINI_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PNTIVA='S' OR .w_PNTNOIVA='S')
    endwith
   endif
  endfunc

  add object oDATFIN_1_11 as StdField with uid="LKWTRQQDJU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 250726710,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=392, Top=142

  func oDATFIN_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PNTIVA='S' OR .w_PNTNOIVA='S')
    endwith
   endif
  endfunc

  add object oNUMINI_1_12 as StdField with uid="ODBTPNEBWY",rtseq=12,rtrep=.f.,;
    cFormVar = "w_NUMINI", cQueryName = "NUMINI",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 172256726,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=236, Top=168, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMINI_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PNTIVA='S' OR .w_PNTNOIVA='S')
    endwith
   endif
  endfunc

  add object oNUMFIN_1_13 as StdField with uid="OQFHAHLDFI",rtseq=13,rtrep=.f.,;
    cFormVar = "w_NUMFIN", cQueryName = "NUMFIN",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 250703318,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=392, Top=168, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMFIN_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PNTIVA='S' OR .w_PNTNOIVA='S')
    endwith
   endif
  endfunc

  add object oPERCORSO_1_14 as StdField with uid="GGVZMRWYOR",rtseq=14,rtrep=.f.,;
    cFormVar = "w_PERCORSO", cQueryName = "PERCORSO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 55488069,;
   bGlobalFont=.t.,;
    Height=21, Width=337, Left=8, Top=260, InputMask=replicate('X',50)


  add object oBtn_1_15 as StdButton with uid="IFISWSZGLE",left=364, top=283, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per esportare";
    , HelpContextID = 65621018;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      this.parent.oContained.NotifyEvent("Elabora")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not (.w_CLI='N' and .w_FOR='N' and .w_SOT='N' and .w_PNTIVA='N' and .w_PNTNOIVA='N'))
      endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="RMAHAARJLI",left=419, top=283, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 58332346;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_27 as cp_runprogram with uid="ZSPGUGIMGW",left=199, top=354, width=156,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSLM_BIV",;
    cEvent = "w_PNTIVA Changed,w_PNTNOIVA Changed",;
    nPag=1;
    , HelpContextID = 112347878


  add object oObj_1_28 as cp_runprogram with uid="GFBSISZIYX",left=42, top=354, width=156,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSLM_BEX",;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 112347878


  add object oObj_1_29 as cp_calclbl with uid="FGUYHAFZYG",left=6, top=5, width=461,height=39,;
    caption='',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=1;
    , HelpContextID = 65649674


  add object oObj_1_31 as cp_runprogram with uid="FQWRCQHDCS",left=577, top=240, width=156,height=22,;
    caption='GSLM_BEA',;
   bGlobalFont=.t.,;
    prg="GSLM_BEA",;
    cEvent = "Cogen",;
    nPag=1;
    , HelpContextID = 195535961


  add object oObj_1_32 as cp_runprogram with uid="QAOSRCUXOQ",left=577, top=261, width=156,height=22,;
    caption='GSLM1BEA',;
   bGlobalFont=.t.,;
    prg="GSLM1BEA",;
    cEvent = "Contb",;
    nPag=1;
    , HelpContextID = 243770457


  add object oObj_1_33 as cp_runprogram with uid="JHWQQXVUFF",left=577, top=282, width=156,height=22,;
    caption='GSLMABEA',;
   bGlobalFont=.t.,;
    prg="GSLMABEA",;
    cEvent = "Movago",;
    nPag=1;
    , HelpContextID = 226993241

  add object oStr_1_17 as StdString with uid="QIIJNUYPIP",Visible=.t., Left=6, Top=50,;
    Alignment=1, Width=128, Height=15,;
    Caption="Azienda da trasferire:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="XQZDKMODZP",Visible=.t., Left=8, Top=81,;
    Alignment=0, Width=198, Height=15,;
    Caption="Parametri di export"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="HAJWYASHND",Visible=.t., Left=315, Top=142,;
    Alignment=1, Width=75, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="ZVRPMNSISS",Visible=.t., Left=151, Top=142,;
    Alignment=1, Width=83, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="OGZBYCPMMU",Visible=.t., Left=8, Top=213,;
    Alignment=0, Width=231, Height=15,;
    Caption="Cartella di export"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="XULLRNYWIG",Visible=.t., Left=9, Top=239,;
    Alignment=0, Width=387, Height=15,;
    Caption="Il file creato verr� memorizzato nella seguente cartella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="UDQMVKYABX",Visible=.t., Left=315, Top=168,;
    Alignment=1, Width=75, Height=15,;
    Caption="A numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="CVJGTWOFGK",Visible=.t., Left=151, Top=168,;
    Alignment=1, Width=83, Height=15,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oBox_1_25 as StdBox with uid="YVLCLZMKYK",left=8, top=232, width=455,height=2

  add object oBox_1_26 as StdBox with uid="ZGBQXDFARZ",left=8, top=99, width=455,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslm_kex','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
