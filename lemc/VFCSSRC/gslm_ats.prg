* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_ats                                                        *
*              Trasferimento studio                                            *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_84]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-05                                                      *
* Last revis.: 2018-08-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgslm_ats")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgslm_ats")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgslm_ats")
  return

* --- Class definition
define class tgslm_ats as StdPCForm
  Width  = 607
  Height = 343+35
  Top    = 14
  Left   = 13
  cComment = "Trasferimento studio"
  cPrg = "gslm_ats"
  HelpContextID=92912745
  add object cnt as tcgslm_ats
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgslm_ats as PCContext
  w_LMTIPCON = space(1)
  w_LMCODAZI = space(5)
  w_LMAZISTU = space(6)
  w_LMPROFIL = space(2)
  w_LMFLANUN = space(1)
  w_LMCODPDC = space(4)
  w_LMCHKNO = space(1)
  w_LMASSOCI = space(2)
  w_DESASS = space(20)
  w_ASSDAT = space(8)
  w_LMCAUGIR = space(5)
  w_CAUDES = space(20)
  w_LMCASFIT = space(15)
  w_LMCAUSED = space(5)
  w_LMCAUSSM = space(5)
  w_DESSOT = space(40)
  w_LMCAUDAC = space(10)
  w_CAUDES1 = space(20)
  w_LMCAUCAD = space(10)
  w_CAUDES2 = space(20)
  w_LMESCAU1 = space(5)
  w_DES1 = space(35)
  w_DES2 = space(35)
  w_DES3 = space(35)
  w_DES4 = space(35)
  w_DES5 = space(35)
  w_LMESCAU2 = space(5)
  w_LMESCAU3 = space(5)
  w_LMESCAU4 = space(5)
  w_LMESCAU5 = space(5)
  w_CAUSED = space(35)
  w_CAUSEDSM = space(35)
  w_LMAGOSOG = space(15)
  w_LMAGOPRO = space(1)
  w_LMAGOREG = space(5)
  w_LMAGONUM = space(10)
  w_LMAGOCLI = space(10)
  w_LMAGOFOR = space(10)
  w_LMAGOFLT = space(1)
  w_LMAGODAT = space(8)
  w_LMPROTVE = space(1)
  proc Save(oFrom)
    this.w_LMTIPCON = oFrom.w_LMTIPCON
    this.w_LMCODAZI = oFrom.w_LMCODAZI
    this.w_LMAZISTU = oFrom.w_LMAZISTU
    this.w_LMPROFIL = oFrom.w_LMPROFIL
    this.w_LMFLANUN = oFrom.w_LMFLANUN
    this.w_LMCODPDC = oFrom.w_LMCODPDC
    this.w_LMCHKNO = oFrom.w_LMCHKNO
    this.w_LMASSOCI = oFrom.w_LMASSOCI
    this.w_DESASS = oFrom.w_DESASS
    this.w_ASSDAT = oFrom.w_ASSDAT
    this.w_LMCAUGIR = oFrom.w_LMCAUGIR
    this.w_CAUDES = oFrom.w_CAUDES
    this.w_LMCASFIT = oFrom.w_LMCASFIT
    this.w_LMCAUSED = oFrom.w_LMCAUSED
    this.w_LMCAUSSM = oFrom.w_LMCAUSSM
    this.w_DESSOT = oFrom.w_DESSOT
    this.w_LMCAUDAC = oFrom.w_LMCAUDAC
    this.w_CAUDES1 = oFrom.w_CAUDES1
    this.w_LMCAUCAD = oFrom.w_LMCAUCAD
    this.w_CAUDES2 = oFrom.w_CAUDES2
    this.w_LMESCAU1 = oFrom.w_LMESCAU1
    this.w_DES1 = oFrom.w_DES1
    this.w_DES2 = oFrom.w_DES2
    this.w_DES3 = oFrom.w_DES3
    this.w_DES4 = oFrom.w_DES4
    this.w_DES5 = oFrom.w_DES5
    this.w_LMESCAU2 = oFrom.w_LMESCAU2
    this.w_LMESCAU3 = oFrom.w_LMESCAU3
    this.w_LMESCAU4 = oFrom.w_LMESCAU4
    this.w_LMESCAU5 = oFrom.w_LMESCAU5
    this.w_CAUSED = oFrom.w_CAUSED
    this.w_CAUSEDSM = oFrom.w_CAUSEDSM
    this.w_LMAGOSOG = oFrom.w_LMAGOSOG
    this.w_LMAGOPRO = oFrom.w_LMAGOPRO
    this.w_LMAGOREG = oFrom.w_LMAGOREG
    this.w_LMAGONUM = oFrom.w_LMAGONUM
    this.w_LMAGOCLI = oFrom.w_LMAGOCLI
    this.w_LMAGOFOR = oFrom.w_LMAGOFOR
    this.w_LMAGOFLT = oFrom.w_LMAGOFLT
    this.w_LMAGODAT = oFrom.w_LMAGODAT
    this.w_LMPROTVE = oFrom.w_LMPROTVE
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_LMTIPCON = this.w_LMTIPCON
    oTo.w_LMCODAZI = this.w_LMCODAZI
    oTo.w_LMAZISTU = this.w_LMAZISTU
    oTo.w_LMPROFIL = this.w_LMPROFIL
    oTo.w_LMFLANUN = this.w_LMFLANUN
    oTo.w_LMCODPDC = this.w_LMCODPDC
    oTo.w_LMCHKNO = this.w_LMCHKNO
    oTo.w_LMASSOCI = this.w_LMASSOCI
    oTo.w_DESASS = this.w_DESASS
    oTo.w_ASSDAT = this.w_ASSDAT
    oTo.w_LMCAUGIR = this.w_LMCAUGIR
    oTo.w_CAUDES = this.w_CAUDES
    oTo.w_LMCASFIT = this.w_LMCASFIT
    oTo.w_LMCAUSED = this.w_LMCAUSED
    oTo.w_LMCAUSSM = this.w_LMCAUSSM
    oTo.w_DESSOT = this.w_DESSOT
    oTo.w_LMCAUDAC = this.w_LMCAUDAC
    oTo.w_CAUDES1 = this.w_CAUDES1
    oTo.w_LMCAUCAD = this.w_LMCAUCAD
    oTo.w_CAUDES2 = this.w_CAUDES2
    oTo.w_LMESCAU1 = this.w_LMESCAU1
    oTo.w_DES1 = this.w_DES1
    oTo.w_DES2 = this.w_DES2
    oTo.w_DES3 = this.w_DES3
    oTo.w_DES4 = this.w_DES4
    oTo.w_DES5 = this.w_DES5
    oTo.w_LMESCAU2 = this.w_LMESCAU2
    oTo.w_LMESCAU3 = this.w_LMESCAU3
    oTo.w_LMESCAU4 = this.w_LMESCAU4
    oTo.w_LMESCAU5 = this.w_LMESCAU5
    oTo.w_CAUSED = this.w_CAUSED
    oTo.w_CAUSEDSM = this.w_CAUSEDSM
    oTo.w_LMAGOSOG = this.w_LMAGOSOG
    oTo.w_LMAGOPRO = this.w_LMAGOPRO
    oTo.w_LMAGOREG = this.w_LMAGOREG
    oTo.w_LMAGONUM = this.w_LMAGONUM
    oTo.w_LMAGOCLI = this.w_LMAGOCLI
    oTo.w_LMAGOFOR = this.w_LMAGOFOR
    oTo.w_LMAGOFLT = this.w_LMAGOFLT
    oTo.w_LMAGODAT = this.w_LMAGODAT
    oTo.w_LMPROTVE = this.w_LMPROTVE
    PCContext::Load(oTo)
enddefine

define class tcgslm_ats as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 607
  Height = 343+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-08-01"
  HelpContextID=92912745
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=41

  * --- Constant Properties
  STU_PARA_IDX = 0
  STU_TRAS_IDX = 0
  CAU_CONT_IDX = 0
  CONTI_IDX = 0
  STU_CAUS_IDX = 0
  cFile = "STU_PARA"
  cKeySelect = "LMCODAZI"
  cKeyWhere  = "LMCODAZI=this.w_LMCODAZI"
  cKeyWhereODBC = '"LMCODAZI="+cp_ToStrODBC(this.w_LMCODAZI)';

  cKeyWhereODBCqualified = '"STU_PARA.LMCODAZI="+cp_ToStrODBC(this.w_LMCODAZI)';

  cPrg = "gslm_ats"
  cComment = "Trasferimento studio"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_LMTIPCON = space(1)
  w_LMCODAZI = space(5)
  w_LMAZISTU = space(6)
  o_LMAZISTU = space(6)
  w_LMPROFIL = space(2)
  w_LMFLANUN = space(1)
  w_LMCODPDC = space(4)
  w_LMCHKNO = space(1)
  w_LMASSOCI = space(2)
  w_DESASS = space(20)
  w_ASSDAT = ctod('  /  /  ')
  w_LMCAUGIR = space(5)
  w_CAUDES = space(20)
  w_LMCASFIT = space(15)
  w_LMCAUSED = space(5)
  w_LMCAUSSM = space(5)
  w_DESSOT = space(40)
  w_LMCAUDAC = space(10)
  w_CAUDES1 = space(20)
  w_LMCAUCAD = space(10)
  w_CAUDES2 = space(20)
  w_LMESCAU1 = space(5)
  w_DES1 = space(35)
  w_DES2 = space(35)
  w_DES3 = space(35)
  w_DES4 = space(35)
  w_DES5 = space(35)
  w_LMESCAU2 = space(5)
  w_LMESCAU3 = space(5)
  w_LMESCAU4 = space(5)
  w_LMESCAU5 = space(5)
  w_CAUSED = space(35)
  w_CAUSEDSM = space(35)
  w_LMAGOSOG = space(15)
  w_LMAGOPRO = space(1)
  w_LMAGOREG = space(5)
  w_LMAGONUM = space(10)
  w_LMAGOCLI = space(10)
  w_LMAGOFOR = space(10)
  w_LMAGOFLT = space(1)
  w_LMAGODAT = ctod('  /  /  ')
  w_LMPROTVE = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslm_atsPag1","gslm_ats",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Impostazioni")
      .Pages(1).HelpContextID = 109906832
      .Pages(2).addobject("oPag","tgslm_atsPag2","gslm_ats",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Causali")
      .Pages(2).HelpContextID = 50267866
      .Pages(3).addobject("oPag","tgslm_atsPag3","gslm_ats",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Dati  Ago")
      .Pages(3).HelpContextID = 51974003
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLMAZISTU_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='STU_TRAS'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='STU_CAUS'
    this.cWorkTables[5]='STU_PARA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.STU_PARA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.STU_PARA_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgslm_ats'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_20_joined
    link_1_20_joined=.f.
    local link_1_23_joined
    link_1_23_joined=.f.
    local link_1_24_joined
    link_1_24_joined=.f.
    local link_1_26_joined
    link_1_26_joined=.f.
    local link_1_28_joined
    link_1_28_joined=.f.
    local link_2_8_joined
    link_2_8_joined=.f.
    local link_2_14_joined
    link_2_14_joined=.f.
    local link_2_15_joined
    link_2_15_joined=.f.
    local link_2_16_joined
    link_2_16_joined=.f.
    local link_2_17_joined
    link_2_17_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from STU_PARA where LMCODAZI=KeySet.LMCODAZI
    *
    i_nConn = i_TableProp[this.STU_PARA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STU_PARA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('STU_PARA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "STU_PARA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' STU_PARA '
      link_1_20_joined=this.AddJoinedLink_1_20(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_23_joined=this.AddJoinedLink_1_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_24_joined=this.AddJoinedLink_1_24(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_26_joined=this.AddJoinedLink_1_26(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_28_joined=this.AddJoinedLink_1_28(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_8_joined=this.AddJoinedLink_2_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_14_joined=this.AddJoinedLink_2_14(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_15_joined=this.AddJoinedLink_2_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_16_joined=this.AddJoinedLink_2_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_17_joined=this.AddJoinedLink_2_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LMCODAZI',this.w_LMCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESASS = space(20)
        .w_ASSDAT = ctod("  /  /  ")
        .w_CAUDES = space(20)
        .w_DESSOT = space(40)
        .w_CAUDES1 = space(20)
        .w_CAUDES2 = space(20)
        .w_DES1 = space(35)
        .w_DES2 = space(35)
        .w_DES3 = space(35)
        .w_DES4 = space(35)
        .w_DES5 = space(35)
        .w_CAUSED = space(35)
        .w_CAUSEDSM = space(35)
        .w_LMTIPCON = NVL(LMTIPCON,space(1))
        .w_LMCODAZI = NVL(LMCODAZI,space(5))
        .w_LMAZISTU = NVL(LMAZISTU,space(6))
        .w_LMPROFIL = NVL(LMPROFIL,space(2))
        .w_LMFLANUN = NVL(LMFLANUN,space(1))
        .w_LMCODPDC = NVL(LMCODPDC,space(4))
        .w_LMCHKNO = NVL(LMCHKNO,space(1))
        .w_LMASSOCI = NVL(LMASSOCI,space(2))
          .link_1_16('Load')
        .w_LMCAUGIR = NVL(LMCAUGIR,space(5))
          if link_1_20_joined
            this.w_LMCAUGIR = NVL(CCCODICE120,NVL(this.w_LMCAUGIR,space(5)))
            this.w_CAUDES = NVL(CCDESCRI120,space(20))
          else
          .link_1_20('Load')
          endif
        .w_LMCASFIT = NVL(LMCASFIT,space(15))
          .link_1_22('Load')
        .w_LMCAUSED = NVL(LMCAUSED,space(5))
          if link_1_23_joined
            this.w_LMCAUSED = NVL(CCCODICE123,NVL(this.w_LMCAUSED,space(5)))
            this.w_CAUSED = NVL(CCDESCRI123,space(35))
          else
          .link_1_23('Load')
          endif
        .w_LMCAUSSM = NVL(LMCAUSSM,space(5))
          if link_1_24_joined
            this.w_LMCAUSSM = NVL(CCCODICE124,NVL(this.w_LMCAUSSM,space(5)))
            this.w_CAUSEDSM = NVL(CCDESCRI124,space(35))
          else
          .link_1_24('Load')
          endif
        .w_LMCAUDAC = NVL(LMCAUDAC,space(10))
          if link_1_26_joined
            this.w_LMCAUDAC = NVL(LMCODCAU126,NVL(this.w_LMCAUDAC,space(10)))
            this.w_CAUDES1 = NVL(LMDESCAU126,space(20))
          else
          .link_1_26('Load')
          endif
        .w_LMCAUCAD = NVL(LMCAUCAD,space(10))
          if link_1_28_joined
            this.w_LMCAUCAD = NVL(LMCODCAU128,NVL(this.w_LMCAUCAD,space(10)))
            this.w_CAUDES2 = NVL(LMDESCAU128,space(20))
          else
          .link_1_28('Load')
          endif
        .w_LMESCAU1 = NVL(LMESCAU1,space(5))
          if link_2_8_joined
            this.w_LMESCAU1 = NVL(CCCODICE208,NVL(this.w_LMESCAU1,space(5)))
            this.w_DES1 = NVL(CCDESCRI208,space(35))
          else
          .link_2_8('Load')
          endif
        .w_LMESCAU2 = NVL(LMESCAU2,space(5))
          if link_2_14_joined
            this.w_LMESCAU2 = NVL(CCCODICE214,NVL(this.w_LMESCAU2,space(5)))
            this.w_DES2 = NVL(CCDESCRI214,space(35))
          else
          .link_2_14('Load')
          endif
        .w_LMESCAU3 = NVL(LMESCAU3,space(5))
          if link_2_15_joined
            this.w_LMESCAU3 = NVL(CCCODICE215,NVL(this.w_LMESCAU3,space(5)))
            this.w_DES3 = NVL(CCDESCRI215,space(35))
          else
          .link_2_15('Load')
          endif
        .w_LMESCAU4 = NVL(LMESCAU4,space(5))
          if link_2_16_joined
            this.w_LMESCAU4 = NVL(CCCODICE216,NVL(this.w_LMESCAU4,space(5)))
            this.w_DES4 = NVL(CCDESCRI216,space(35))
          else
          .link_2_16('Load')
          endif
        .w_LMESCAU5 = NVL(LMESCAU5,space(5))
          if link_2_17_joined
            this.w_LMESCAU5 = NVL(CCCODICE217,NVL(this.w_LMESCAU5,space(5)))
            this.w_DES5 = NVL(CCDESCRI217,space(35))
          else
          .link_2_17('Load')
          endif
        .w_LMAGOSOG = NVL(LMAGOSOG,space(15))
        .w_LMAGOPRO = NVL(LMAGOPRO,space(1))
        .w_LMAGOREG = NVL(LMAGOREG,space(5))
        .w_LMAGONUM = NVL(LMAGONUM,space(10))
        .w_LMAGOCLI = NVL(LMAGOCLI,space(10))
        .w_LMAGOFOR = NVL(LMAGOFOR,space(10))
        .w_LMAGOFLT = NVL(LMAGOFLT,space(1))
        .w_LMAGODAT = NVL(cp_ToDate(LMAGODAT),ctod("  /  /  "))
        .w_LMPROTVE = NVL(LMPROTVE,space(1))
        cp_LoadRecExtFlds(this,'STU_PARA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_LMTIPCON = space(1)
      .w_LMCODAZI = space(5)
      .w_LMAZISTU = space(6)
      .w_LMPROFIL = space(2)
      .w_LMFLANUN = space(1)
      .w_LMCODPDC = space(4)
      .w_LMCHKNO = space(1)
      .w_LMASSOCI = space(2)
      .w_DESASS = space(20)
      .w_ASSDAT = ctod("  /  /  ")
      .w_LMCAUGIR = space(5)
      .w_CAUDES = space(20)
      .w_LMCASFIT = space(15)
      .w_LMCAUSED = space(5)
      .w_LMCAUSSM = space(5)
      .w_DESSOT = space(40)
      .w_LMCAUDAC = space(10)
      .w_CAUDES1 = space(20)
      .w_LMCAUCAD = space(10)
      .w_CAUDES2 = space(20)
      .w_LMESCAU1 = space(5)
      .w_DES1 = space(35)
      .w_DES2 = space(35)
      .w_DES3 = space(35)
      .w_DES4 = space(35)
      .w_DES5 = space(35)
      .w_LMESCAU2 = space(5)
      .w_LMESCAU3 = space(5)
      .w_LMESCAU4 = space(5)
      .w_LMESCAU5 = space(5)
      .w_CAUSED = space(35)
      .w_CAUSEDSM = space(35)
      .w_LMAGOSOG = space(15)
      .w_LMAGOPRO = space(1)
      .w_LMAGOREG = space(5)
      .w_LMAGONUM = space(10)
      .w_LMAGOCLI = space(10)
      .w_LMAGOFOR = space(10)
      .w_LMAGOFLT = space(1)
      .w_LMAGODAT = ctod("  /  /  ")
      .w_LMPROTVE = space(1)
      if .cFunction<>"Filter"
        .w_LMTIPCON = 'G'
          .DoRTCalc(2,2,.f.)
        .w_LMAZISTU = IIF(g_TRAEXP='G','',.w_LMAZISTU)
          .DoRTCalc(4,4,.f.)
        .w_LMFLANUN = iif(g_TRAEXP $ ('C-B-G'),'',.w_LMFLANUN)
        .w_LMCODPDC = iif(g_TRAEXP='G','0001',.w_LMCODPDC)
        .w_LMCHKNO = 'N'
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_LMASSOCI))
          .link_1_16('Full')
          endif
        .DoRTCalc(9,11,.f.)
          if not(empty(.w_LMCAUGIR))
          .link_1_20('Full')
          endif
        .DoRTCalc(12,13,.f.)
          if not(empty(.w_LMCASFIT))
          .link_1_22('Full')
          endif
        .DoRTCalc(14,14,.f.)
          if not(empty(.w_LMCAUSED))
          .link_1_23('Full')
          endif
        .DoRTCalc(15,15,.f.)
          if not(empty(.w_LMCAUSSM))
          .link_1_24('Full')
          endif
        .DoRTCalc(16,17,.f.)
          if not(empty(.w_LMCAUDAC))
          .link_1_26('Full')
          endif
        .DoRTCalc(18,19,.f.)
          if not(empty(.w_LMCAUCAD))
          .link_1_28('Full')
          endif
        .DoRTCalc(20,21,.f.)
          if not(empty(.w_LMESCAU1))
          .link_2_8('Full')
          endif
        .DoRTCalc(22,27,.f.)
          if not(empty(.w_LMESCAU2))
          .link_2_14('Full')
          endif
        .DoRTCalc(28,28,.f.)
          if not(empty(.w_LMESCAU3))
          .link_2_15('Full')
          endif
        .DoRTCalc(29,29,.f.)
          if not(empty(.w_LMESCAU4))
          .link_2_16('Full')
          endif
        .DoRTCalc(30,30,.f.)
          if not(empty(.w_LMESCAU5))
          .link_2_17('Full')
          endif
          .DoRTCalc(31,35,.f.)
        .w_LMAGONUM = '1'
          .DoRTCalc(37,38,.f.)
        .w_LMAGOFLT = 'S'
        .w_LMAGODAT = I_DATSYS
      endif
    endwith
    cp_BlankRecExtFlds(this,'STU_PARA')
    this.DoRTCalc(41,41,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oLMAZISTU_1_3.enabled = i_bVal
      .Page1.oPag.oLMPROFIL_1_4.enabled = i_bVal
      .Page1.oPag.oLMFLANUN_1_5.enabled = i_bVal
      .Page1.oPag.oLMCODPDC_1_6.enabled = i_bVal
      .Page1.oPag.oLMCHKNO_1_7.enabled = i_bVal
      .Page1.oPag.oLMASSOCI_1_16.enabled = i_bVal
      .Page1.oPag.oLMCAUGIR_1_20.enabled = i_bVal
      .Page1.oPag.oLMCASFIT_1_22.enabled = i_bVal
      .Page1.oPag.oLMCAUSED_1_23.enabled = i_bVal
      .Page1.oPag.oLMCAUSSM_1_24.enabled = i_bVal
      .Page1.oPag.oLMCAUDAC_1_26.enabled = i_bVal
      .Page1.oPag.oLMCAUCAD_1_28.enabled = i_bVal
      .Page2.oPag.oLMESCAU1_2_8.enabled = i_bVal
      .Page2.oPag.oLMESCAU2_2_14.enabled = i_bVal
      .Page2.oPag.oLMESCAU3_2_15.enabled = i_bVal
      .Page2.oPag.oLMESCAU4_2_16.enabled = i_bVal
      .Page2.oPag.oLMESCAU5_2_17.enabled = i_bVal
      .Page3.oPag.oLMAGOSOG_3_1.enabled = i_bVal
      .Page3.oPag.oLMAGOPRO_3_3.enabled = i_bVal
      .Page3.oPag.oLMAGOREG_3_5.enabled = i_bVal
      .Page3.oPag.oLMAGONUM_3_7.enabled = i_bVal
      .Page3.oPag.oLMAGOCLI_3_9.enabled = i_bVal
      .Page3.oPag.oLMAGOFOR_3_10.enabled = i_bVal
      .Page3.oPag.oLMAGOFLT_3_13.enabled = i_bVal
      .Page3.oPag.oLMPROTVE_3_15.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oLMAZISTU_1_3.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'STU_PARA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.STU_PARA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMTIPCON,"LMTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCODAZI,"LMCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMAZISTU,"LMAZISTU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMPROFIL,"LMPROFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMFLANUN,"LMFLANUN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCODPDC,"LMCODPDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCHKNO,"LMCHKNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMASSOCI,"LMASSOCI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCAUGIR,"LMCAUGIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCASFIT,"LMCASFIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCAUSED,"LMCAUSED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCAUSSM,"LMCAUSSM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCAUDAC,"LMCAUDAC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMCAUCAD,"LMCAUCAD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMESCAU1,"LMESCAU1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMESCAU2,"LMESCAU2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMESCAU3,"LMESCAU3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMESCAU4,"LMESCAU4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMESCAU5,"LMESCAU5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMAGOSOG,"LMAGOSOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMAGOPRO,"LMAGOPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMAGOREG,"LMAGOREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMAGONUM,"LMAGONUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMAGOCLI,"LMAGOCLI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMAGOFOR,"LMAGOFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMAGOFLT,"LMAGOFLT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMAGODAT,"LMAGODAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LMPROTVE,"LMPROTVE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.STU_PARA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_PARA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.STU_PARA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into STU_PARA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'STU_PARA')
        i_extval=cp_InsertValODBCExtFlds(this,'STU_PARA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(LMTIPCON,LMCODAZI,LMAZISTU,LMPROFIL,LMFLANUN"+;
                  ",LMCODPDC,LMCHKNO,LMASSOCI,LMCAUGIR,LMCASFIT"+;
                  ",LMCAUSED,LMCAUSSM,LMCAUDAC,LMCAUCAD,LMESCAU1"+;
                  ",LMESCAU2,LMESCAU3,LMESCAU4,LMESCAU5,LMAGOSOG"+;
                  ",LMAGOPRO,LMAGOREG,LMAGONUM,LMAGOCLI,LMAGOFOR"+;
                  ",LMAGOFLT,LMAGODAT,LMPROTVE "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_LMTIPCON)+;
                  ","+cp_ToStrODBC(this.w_LMCODAZI)+;
                  ","+cp_ToStrODBC(this.w_LMAZISTU)+;
                  ","+cp_ToStrODBC(this.w_LMPROFIL)+;
                  ","+cp_ToStrODBC(this.w_LMFLANUN)+;
                  ","+cp_ToStrODBC(this.w_LMCODPDC)+;
                  ","+cp_ToStrODBC(this.w_LMCHKNO)+;
                  ","+cp_ToStrODBCNull(this.w_LMASSOCI)+;
                  ","+cp_ToStrODBCNull(this.w_LMCAUGIR)+;
                  ","+cp_ToStrODBCNull(this.w_LMCASFIT)+;
                  ","+cp_ToStrODBCNull(this.w_LMCAUSED)+;
                  ","+cp_ToStrODBCNull(this.w_LMCAUSSM)+;
                  ","+cp_ToStrODBCNull(this.w_LMCAUDAC)+;
                  ","+cp_ToStrODBCNull(this.w_LMCAUCAD)+;
                  ","+cp_ToStrODBCNull(this.w_LMESCAU1)+;
                  ","+cp_ToStrODBCNull(this.w_LMESCAU2)+;
                  ","+cp_ToStrODBCNull(this.w_LMESCAU3)+;
                  ","+cp_ToStrODBCNull(this.w_LMESCAU4)+;
                  ","+cp_ToStrODBCNull(this.w_LMESCAU5)+;
                  ","+cp_ToStrODBC(this.w_LMAGOSOG)+;
                  ","+cp_ToStrODBC(this.w_LMAGOPRO)+;
                  ","+cp_ToStrODBC(this.w_LMAGOREG)+;
                  ","+cp_ToStrODBC(this.w_LMAGONUM)+;
                  ","+cp_ToStrODBC(this.w_LMAGOCLI)+;
                  ","+cp_ToStrODBC(this.w_LMAGOFOR)+;
                  ","+cp_ToStrODBC(this.w_LMAGOFLT)+;
                  ","+cp_ToStrODBC(this.w_LMAGODAT)+;
                  ","+cp_ToStrODBC(this.w_LMPROTVE)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'STU_PARA')
        i_extval=cp_InsertValVFPExtFlds(this,'STU_PARA')
        cp_CheckDeletedKey(i_cTable,0,'LMCODAZI',this.w_LMCODAZI)
        INSERT INTO (i_cTable);
              (LMTIPCON,LMCODAZI,LMAZISTU,LMPROFIL,LMFLANUN,LMCODPDC,LMCHKNO,LMASSOCI,LMCAUGIR,LMCASFIT,LMCAUSED,LMCAUSSM,LMCAUDAC,LMCAUCAD,LMESCAU1,LMESCAU2,LMESCAU3,LMESCAU4,LMESCAU5,LMAGOSOG,LMAGOPRO,LMAGOREG,LMAGONUM,LMAGOCLI,LMAGOFOR,LMAGOFLT,LMAGODAT,LMPROTVE  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_LMTIPCON;
                  ,this.w_LMCODAZI;
                  ,this.w_LMAZISTU;
                  ,this.w_LMPROFIL;
                  ,this.w_LMFLANUN;
                  ,this.w_LMCODPDC;
                  ,this.w_LMCHKNO;
                  ,this.w_LMASSOCI;
                  ,this.w_LMCAUGIR;
                  ,this.w_LMCASFIT;
                  ,this.w_LMCAUSED;
                  ,this.w_LMCAUSSM;
                  ,this.w_LMCAUDAC;
                  ,this.w_LMCAUCAD;
                  ,this.w_LMESCAU1;
                  ,this.w_LMESCAU2;
                  ,this.w_LMESCAU3;
                  ,this.w_LMESCAU4;
                  ,this.w_LMESCAU5;
                  ,this.w_LMAGOSOG;
                  ,this.w_LMAGOPRO;
                  ,this.w_LMAGOREG;
                  ,this.w_LMAGONUM;
                  ,this.w_LMAGOCLI;
                  ,this.w_LMAGOFOR;
                  ,this.w_LMAGOFLT;
                  ,this.w_LMAGODAT;
                  ,this.w_LMPROTVE;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.STU_PARA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_PARA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.STU_PARA_IDX,i_nConn)
      *
      * update STU_PARA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'STU_PARA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " LMTIPCON="+cp_ToStrODBC(this.w_LMTIPCON)+;
             ",LMAZISTU="+cp_ToStrODBC(this.w_LMAZISTU)+;
             ",LMPROFIL="+cp_ToStrODBC(this.w_LMPROFIL)+;
             ",LMFLANUN="+cp_ToStrODBC(this.w_LMFLANUN)+;
             ",LMCODPDC="+cp_ToStrODBC(this.w_LMCODPDC)+;
             ",LMCHKNO="+cp_ToStrODBC(this.w_LMCHKNO)+;
             ",LMASSOCI="+cp_ToStrODBCNull(this.w_LMASSOCI)+;
             ",LMCAUGIR="+cp_ToStrODBCNull(this.w_LMCAUGIR)+;
             ",LMCASFIT="+cp_ToStrODBCNull(this.w_LMCASFIT)+;
             ",LMCAUSED="+cp_ToStrODBCNull(this.w_LMCAUSED)+;
             ",LMCAUSSM="+cp_ToStrODBCNull(this.w_LMCAUSSM)+;
             ",LMCAUDAC="+cp_ToStrODBCNull(this.w_LMCAUDAC)+;
             ",LMCAUCAD="+cp_ToStrODBCNull(this.w_LMCAUCAD)+;
             ",LMESCAU1="+cp_ToStrODBCNull(this.w_LMESCAU1)+;
             ",LMESCAU2="+cp_ToStrODBCNull(this.w_LMESCAU2)+;
             ",LMESCAU3="+cp_ToStrODBCNull(this.w_LMESCAU3)+;
             ",LMESCAU4="+cp_ToStrODBCNull(this.w_LMESCAU4)+;
             ",LMESCAU5="+cp_ToStrODBCNull(this.w_LMESCAU5)+;
             ",LMAGOSOG="+cp_ToStrODBC(this.w_LMAGOSOG)+;
             ",LMAGOPRO="+cp_ToStrODBC(this.w_LMAGOPRO)+;
             ",LMAGOREG="+cp_ToStrODBC(this.w_LMAGOREG)+;
             ",LMAGONUM="+cp_ToStrODBC(this.w_LMAGONUM)+;
             ",LMAGOCLI="+cp_ToStrODBC(this.w_LMAGOCLI)+;
             ",LMAGOFOR="+cp_ToStrODBC(this.w_LMAGOFOR)+;
             ",LMAGOFLT="+cp_ToStrODBC(this.w_LMAGOFLT)+;
             ",LMAGODAT="+cp_ToStrODBC(this.w_LMAGODAT)+;
             ",LMPROTVE="+cp_ToStrODBC(this.w_LMPROTVE)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'STU_PARA')
        i_cWhere = cp_PKFox(i_cTable  ,'LMCODAZI',this.w_LMCODAZI  )
        UPDATE (i_cTable) SET;
              LMTIPCON=this.w_LMTIPCON;
             ,LMAZISTU=this.w_LMAZISTU;
             ,LMPROFIL=this.w_LMPROFIL;
             ,LMFLANUN=this.w_LMFLANUN;
             ,LMCODPDC=this.w_LMCODPDC;
             ,LMCHKNO=this.w_LMCHKNO;
             ,LMASSOCI=this.w_LMASSOCI;
             ,LMCAUGIR=this.w_LMCAUGIR;
             ,LMCASFIT=this.w_LMCASFIT;
             ,LMCAUSED=this.w_LMCAUSED;
             ,LMCAUSSM=this.w_LMCAUSSM;
             ,LMCAUDAC=this.w_LMCAUDAC;
             ,LMCAUCAD=this.w_LMCAUCAD;
             ,LMESCAU1=this.w_LMESCAU1;
             ,LMESCAU2=this.w_LMESCAU2;
             ,LMESCAU3=this.w_LMESCAU3;
             ,LMESCAU4=this.w_LMESCAU4;
             ,LMESCAU5=this.w_LMESCAU5;
             ,LMAGOSOG=this.w_LMAGOSOG;
             ,LMAGOPRO=this.w_LMAGOPRO;
             ,LMAGOREG=this.w_LMAGOREG;
             ,LMAGONUM=this.w_LMAGONUM;
             ,LMAGOCLI=this.w_LMAGOCLI;
             ,LMAGOFOR=this.w_LMAGOFOR;
             ,LMAGOFLT=this.w_LMAGOFLT;
             ,LMAGODAT=this.w_LMAGODAT;
             ,LMPROTVE=this.w_LMPROTVE;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.STU_PARA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.STU_PARA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.STU_PARA_IDX,i_nConn)
      *
      * delete STU_PARA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'LMCODAZI',this.w_LMCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.STU_PARA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.STU_PARA_IDX,2])
    if i_bUpd
      with this
            .w_LMTIPCON = 'G'
        .DoRTCalc(2,2,.t.)
        if .o_LMAZISTU<>.w_LMAZISTU
            .w_LMAZISTU = IIF(g_TRAEXP='G','',.w_LMAZISTU)
        endif
        .DoRTCalc(4,5,.t.)
        if .o_LMAZISTU<>.w_LMAZISTU
            .w_LMCODPDC = iif(g_TRAEXP='G','0001',.w_LMCODPDC)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(7,41,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oLMAZISTU_1_3.enabled = this.oPgFrm.Page1.oPag.oLMAZISTU_1_3.mCond()
    this.oPgFrm.Page1.oPag.oLMPROFIL_1_4.enabled = this.oPgFrm.Page1.oPag.oLMPROFIL_1_4.mCond()
    this.oPgFrm.Page1.oPag.oLMFLANUN_1_5.enabled = this.oPgFrm.Page1.oPag.oLMFLANUN_1_5.mCond()
    this.oPgFrm.Page1.oPag.oLMCODPDC_1_6.enabled = this.oPgFrm.Page1.oPag.oLMCODPDC_1_6.mCond()
    this.oPgFrm.Page1.oPag.oLMASSOCI_1_16.enabled = this.oPgFrm.Page1.oPag.oLMASSOCI_1_16.mCond()
    this.oPgFrm.Page1.oPag.oLMCAUGIR_1_20.enabled = this.oPgFrm.Page1.oPag.oLMCAUGIR_1_20.mCond()
    this.oPgFrm.Page1.oPag.oLMCASFIT_1_22.enabled = this.oPgFrm.Page1.oPag.oLMCASFIT_1_22.mCond()
    this.oPgFrm.Page1.oPag.oLMCAUSED_1_23.enabled = this.oPgFrm.Page1.oPag.oLMCAUSED_1_23.mCond()
    this.oPgFrm.Page1.oPag.oLMCAUSSM_1_24.enabled = this.oPgFrm.Page1.oPag.oLMCAUSSM_1_24.mCond()
    this.oPgFrm.Page1.oPag.oLMCAUDAC_1_26.enabled = this.oPgFrm.Page1.oPag.oLMCAUDAC_1_26.mCond()
    this.oPgFrm.Page1.oPag.oLMCAUCAD_1_28.enabled = this.oPgFrm.Page1.oPag.oLMCAUCAD_1_28.mCond()
    this.oPgFrm.Page2.oPag.oLMESCAU1_2_8.enabled = this.oPgFrm.Page2.oPag.oLMESCAU1_2_8.mCond()
    this.oPgFrm.Page2.oPag.oLMESCAU2_2_14.enabled = this.oPgFrm.Page2.oPag.oLMESCAU2_2_14.mCond()
    this.oPgFrm.Page2.oPag.oLMESCAU3_2_15.enabled = this.oPgFrm.Page2.oPag.oLMESCAU3_2_15.mCond()
    this.oPgFrm.Page2.oPag.oLMESCAU4_2_16.enabled = this.oPgFrm.Page2.oPag.oLMESCAU4_2_16.mCond()
    this.oPgFrm.Page2.oPag.oLMESCAU5_2_17.enabled = this.oPgFrm.Page2.oPag.oLMESCAU5_2_17.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    oField= this.oPgFrm.Page1.oPag.oLMAZISTU_1_3
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page1.oPag.oLMCODPDC_1_6
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page3.oPag.oLMAGOSOG_3_1
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page3.oPag.oLMAGOPRO_3_3
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page3.oPag.oLMAGOREG_3_5
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page3.oPag.oLMAGONUM_3_7
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page3.oPag.oLMAGOCLI_3_9
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    oField= this.oPgFrm.Page3.oPag.oLMAGOFOR_3_10
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(3).enabled=not(g_TRAEXP<>'G')
    this.oPgFrm.Page1.oPag.oLMCASFIT_1_22.visible=!this.oPgFrm.Page1.oPag.oLMCASFIT_1_22.mHide()
    this.oPgFrm.Page1.oPag.oDESSOT_1_25.visible=!this.oPgFrm.Page1.oPag.oDESSOT_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LMASSOCI
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_TRAS_IDX,3]
    i_lTable = "STU_TRAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_TRAS_IDX,2], .t., this.STU_TRAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_TRAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMASSOCI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSLM_MHS',True,'STU_TRAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODICE like "+cp_ToStrODBC(trim(this.w_LMASSOCI)+"%");

          i_ret=cp_SQL(i_nConn,"select LMCODICE,LMDESCRI,LM__DATA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODICE',trim(this.w_LMASSOCI))
          select LMCODICE,LMDESCRI,LM__DATA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMASSOCI)==trim(_Link_.LMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LMASSOCI) and !this.bDontReportError
            deferred_cp_zoom('STU_TRAS','*','LMCODICE',cp_AbsName(oSource.parent,'oLMASSOCI_1_16'),i_cWhere,'GSLM_MHS',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODICE,LMDESCRI,LM__DATA";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODICE',oSource.xKey(1))
            select LMCODICE,LMDESCRI,LM__DATA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMASSOCI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODICE,LMDESCRI,LM__DATA";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODICE="+cp_ToStrODBC(this.w_LMASSOCI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODICE',this.w_LMASSOCI)
            select LMCODICE,LMDESCRI,LM__DATA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMASSOCI = NVL(_Link_.LMCODICE,space(2))
      this.w_DESASS = NVL(_Link_.LMDESCRI,space(20))
      this.w_ASSDAT = NVL(cp_ToDate(_Link_.LM__DATA),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_LMASSOCI = space(2)
      endif
      this.w_DESASS = space(20)
      this.w_ASSDAT = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_TRAS_IDX,2])+'\'+cp_ToStr(_Link_.LMCODICE,1)
      cp_ShowWarn(i_cKey,this.STU_TRAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMASSOCI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LMCAUGIR
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMCAUGIR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_LMCAUGIR)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_LMCAUGIR))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMCAUGIR)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LMCAUGIR) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oLMCAUGIR_1_20'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMCAUGIR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_LMCAUGIR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_LMCAUGIR)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMCAUGIR = NVL(_Link_.CCCODICE,space(5))
      this.w_CAUDES = NVL(_Link_.CCDESCRI,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LMCAUGIR = space(5)
      endif
      this.w_CAUDES = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMCAUGIR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_20(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_20.CCCODICE as CCCODICE120"+ ",link_1_20.CCDESCRI as CCDESCRI120"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_20 on STU_PARA.LMCAUGIR=link_1_20.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_20"
          i_cKey=i_cKey+'+" and STU_PARA.LMCAUGIR=link_1_20.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LMCASFIT
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMCASFIT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_api',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_LMCASFIT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_LMTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_LMTIPCON;
                     ,'ANCODICE',trim(this.w_LMCASFIT))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMCASFIT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LMCASFIT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oLMCASFIT_1_22'),i_cWhere,'gsar_api',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_LMTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_LMTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMCASFIT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_LMCASFIT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_LMTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_LMTIPCON;
                       ,'ANCODICE',this.w_LMCASFIT)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMCASFIT = NVL(_Link_.ANCODICE,space(15))
      this.w_DESSOT = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_LMCASFIT = space(15)
      endif
      this.w_DESSOT = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMCASFIT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LMCAUSED
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMCAUSED) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_LMCAUSED)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_LMCAUSED))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMCAUSED)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LMCAUSED) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oLMCAUSED_1_23'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMCAUSED)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_LMCAUSED);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_LMCAUSED)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMCAUSED = NVL(_Link_.CCCODICE,space(5))
      this.w_CAUSED = NVL(_Link_.CCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_LMCAUSED = space(5)
      endif
      this.w_CAUSED = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMCAUSED Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_23.CCCODICE as CCCODICE123"+ ",link_1_23.CCDESCRI as CCDESCRI123"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_23 on STU_PARA.LMCAUSED=link_1_23.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_23"
          i_cKey=i_cKey+'+" and STU_PARA.LMCAUSED=link_1_23.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LMCAUSSM
  func Link_1_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMCAUSSM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_LMCAUSSM)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_LMCAUSSM))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMCAUSSM)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LMCAUSSM) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oLMCAUSSM_1_24'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMCAUSSM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_LMCAUSSM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_LMCAUSSM)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMCAUSSM = NVL(_Link_.CCCODICE,space(5))
      this.w_CAUSEDSM = NVL(_Link_.CCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_LMCAUSSM = space(5)
      endif
      this.w_CAUSEDSM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMCAUSSM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_24(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_24.CCCODICE as CCCODICE124"+ ",link_1_24.CCDESCRI as CCDESCRI124"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_24 on STU_PARA.LMCAUSSM=link_1_24.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_24"
          i_cKey=i_cKey+'+" and STU_PARA.LMCAUSSM=link_1_24.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LMCAUDAC
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_CAUS_IDX,3]
    i_lTable = "STU_CAUS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_CAUS_IDX,2], .t., this.STU_CAUS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_CAUS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMCAUDAC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSLM_ACC',True,'STU_CAUS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODCAU like "+cp_ToStrODBC(trim(this.w_LMCAUDAC)+"%");

          i_ret=cp_SQL(i_nConn,"select LMCODCAU,LMDESCAU";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODCAU","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODCAU',trim(this.w_LMCAUDAC))
          select LMCODCAU,LMDESCAU;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODCAU into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMCAUDAC)==trim(_Link_.LMCODCAU) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LMCAUDAC) and !this.bDontReportError
            deferred_cp_zoom('STU_CAUS','*','LMCODCAU',cp_AbsName(oSource.parent,'oLMCAUDAC_1_26'),i_cWhere,'GSLM_ACC',"Causali contabili studio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODCAU,LMDESCAU";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODCAU="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODCAU',oSource.xKey(1))
            select LMCODCAU,LMDESCAU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMCAUDAC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODCAU,LMDESCAU";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODCAU="+cp_ToStrODBC(this.w_LMCAUDAC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODCAU',this.w_LMCAUDAC)
            select LMCODCAU,LMDESCAU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMCAUDAC = NVL(_Link_.LMCODCAU,space(10))
      this.w_CAUDES1 = NVL(_Link_.LMDESCAU,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LMCAUDAC = space(10)
      endif
      this.w_CAUDES1 = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_CAUS_IDX,2])+'\'+cp_ToStr(_Link_.LMCODCAU,1)
      cp_ShowWarn(i_cKey,this.STU_CAUS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMCAUDAC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_26(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.STU_CAUS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.STU_CAUS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_26.LMCODCAU as LMCODCAU126"+ ",link_1_26.LMDESCAU as LMDESCAU126"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_26 on STU_PARA.LMCAUDAC=link_1_26.LMCODCAU"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_26"
          i_cKey=i_cKey+'+" and STU_PARA.LMCAUDAC=link_1_26.LMCODCAU(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LMCAUCAD
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_CAUS_IDX,3]
    i_lTable = "STU_CAUS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_CAUS_IDX,2], .t., this.STU_CAUS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_CAUS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMCAUCAD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSLM_ACC',True,'STU_CAUS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODCAU like "+cp_ToStrODBC(trim(this.w_LMCAUCAD)+"%");

          i_ret=cp_SQL(i_nConn,"select LMCODCAU,LMDESCAU";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODCAU","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODCAU',trim(this.w_LMCAUCAD))
          select LMCODCAU,LMDESCAU;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODCAU into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMCAUCAD)==trim(_Link_.LMCODCAU) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LMCAUCAD) and !this.bDontReportError
            deferred_cp_zoom('STU_CAUS','*','LMCODCAU',cp_AbsName(oSource.parent,'oLMCAUCAD_1_28'),i_cWhere,'GSLM_ACC',"Causali contabili studio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODCAU,LMDESCAU";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODCAU="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODCAU',oSource.xKey(1))
            select LMCODCAU,LMDESCAU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMCAUCAD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODCAU,LMDESCAU";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODCAU="+cp_ToStrODBC(this.w_LMCAUCAD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODCAU',this.w_LMCAUCAD)
            select LMCODCAU,LMDESCAU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMCAUCAD = NVL(_Link_.LMCODCAU,space(10))
      this.w_CAUDES2 = NVL(_Link_.LMDESCAU,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LMCAUCAD = space(10)
      endif
      this.w_CAUDES2 = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_CAUS_IDX,2])+'\'+cp_ToStr(_Link_.LMCODCAU,1)
      cp_ShowWarn(i_cKey,this.STU_CAUS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMCAUCAD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_28(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.STU_CAUS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.STU_CAUS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_28.LMCODCAU as LMCODCAU128"+ ",link_1_28.LMDESCAU as LMDESCAU128"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_28 on STU_PARA.LMCAUCAD=link_1_28.LMCODCAU"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_28"
          i_cKey=i_cKey+'+" and STU_PARA.LMCAUCAD=link_1_28.LMCODCAU(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LMESCAU1
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMESCAU1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_LMESCAU1)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_LMESCAU1))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMESCAU1)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LMESCAU1) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oLMESCAU1_2_8'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMESCAU1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_LMESCAU1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_LMESCAU1)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMESCAU1 = NVL(_Link_.CCCODICE,space(5))
      this.w_DES1 = NVL(_Link_.CCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_LMESCAU1 = space(5)
      endif
      this.w_DES1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMESCAU1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_8.CCCODICE as CCCODICE208"+ ",link_2_8.CCDESCRI as CCDESCRI208"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_8 on STU_PARA.LMESCAU1=link_2_8.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_8"
          i_cKey=i_cKey+'+" and STU_PARA.LMESCAU1=link_2_8.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LMESCAU2
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMESCAU2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_LMESCAU2)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_LMESCAU2))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMESCAU2)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LMESCAU2) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oLMESCAU2_2_14'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMESCAU2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_LMESCAU2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_LMESCAU2)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMESCAU2 = NVL(_Link_.CCCODICE,space(5))
      this.w_DES2 = NVL(_Link_.CCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_LMESCAU2 = space(5)
      endif
      this.w_DES2 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMESCAU2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_14(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_14.CCCODICE as CCCODICE214"+ ",link_2_14.CCDESCRI as CCDESCRI214"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_14 on STU_PARA.LMESCAU2=link_2_14.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_14"
          i_cKey=i_cKey+'+" and STU_PARA.LMESCAU2=link_2_14.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LMESCAU3
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMESCAU3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_LMESCAU3)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_LMESCAU3))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMESCAU3)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LMESCAU3) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oLMESCAU3_2_15'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMESCAU3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_LMESCAU3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_LMESCAU3)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMESCAU3 = NVL(_Link_.CCCODICE,space(5))
      this.w_DES3 = NVL(_Link_.CCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_LMESCAU3 = space(5)
      endif
      this.w_DES3 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMESCAU3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_15.CCCODICE as CCCODICE215"+ ",link_2_15.CCDESCRI as CCDESCRI215"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_15 on STU_PARA.LMESCAU3=link_2_15.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_15"
          i_cKey=i_cKey+'+" and STU_PARA.LMESCAU3=link_2_15.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LMESCAU4
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMESCAU4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_LMESCAU4)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_LMESCAU4))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMESCAU4)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LMESCAU4) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oLMESCAU4_2_16'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMESCAU4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_LMESCAU4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_LMESCAU4)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMESCAU4 = NVL(_Link_.CCCODICE,space(5))
      this.w_DES4 = NVL(_Link_.CCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_LMESCAU4 = space(5)
      endif
      this.w_DES4 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMESCAU4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_16.CCCODICE as CCCODICE216"+ ",link_2_16.CCDESCRI as CCDESCRI216"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_16 on STU_PARA.LMESCAU4=link_2_16.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_16"
          i_cKey=i_cKey+'+" and STU_PARA.LMESCAU4=link_2_16.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=LMESCAU5
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LMESCAU5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_LMESCAU5)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_LMESCAU5))
          select CCCODICE,CCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_LMESCAU5)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_LMESCAU5) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oLMESCAU5_2_17'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LMESCAU5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_LMESCAU5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_LMESCAU5)
            select CCCODICE,CCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LMESCAU5 = NVL(_Link_.CCCODICE,space(5))
      this.w_DES5 = NVL(_Link_.CCDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_LMESCAU5 = space(5)
      endif
      this.w_DES5 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LMESCAU5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_17.CCCODICE as CCCODICE217"+ ",link_2_17.CCDESCRI as CCDESCRI217"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_17 on STU_PARA.LMESCAU5=link_2_17.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_17"
          i_cKey=i_cKey+'+" and STU_PARA.LMESCAU5=link_2_17.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLMAZISTU_1_3.value==this.w_LMAZISTU)
      this.oPgFrm.Page1.oPag.oLMAZISTU_1_3.value=this.w_LMAZISTU
    endif
    if not(this.oPgFrm.Page1.oPag.oLMPROFIL_1_4.value==this.w_LMPROFIL)
      this.oPgFrm.Page1.oPag.oLMPROFIL_1_4.value=this.w_LMPROFIL
    endif
    if not(this.oPgFrm.Page1.oPag.oLMFLANUN_1_5.RadioValue()==this.w_LMFLANUN)
      this.oPgFrm.Page1.oPag.oLMFLANUN_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLMCODPDC_1_6.value==this.w_LMCODPDC)
      this.oPgFrm.Page1.oPag.oLMCODPDC_1_6.value=this.w_LMCODPDC
    endif
    if not(this.oPgFrm.Page1.oPag.oLMCHKNO_1_7.RadioValue()==this.w_LMCHKNO)
      this.oPgFrm.Page1.oPag.oLMCHKNO_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLMASSOCI_1_16.value==this.w_LMASSOCI)
      this.oPgFrm.Page1.oPag.oLMASSOCI_1_16.value=this.w_LMASSOCI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESASS_1_17.value==this.w_DESASS)
      this.oPgFrm.Page1.oPag.oDESASS_1_17.value=this.w_DESASS
    endif
    if not(this.oPgFrm.Page1.oPag.oASSDAT_1_19.value==this.w_ASSDAT)
      this.oPgFrm.Page1.oPag.oASSDAT_1_19.value=this.w_ASSDAT
    endif
    if not(this.oPgFrm.Page1.oPag.oLMCAUGIR_1_20.value==this.w_LMCAUGIR)
      this.oPgFrm.Page1.oPag.oLMCAUGIR_1_20.value=this.w_LMCAUGIR
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUDES_1_21.value==this.w_CAUDES)
      this.oPgFrm.Page1.oPag.oCAUDES_1_21.value=this.w_CAUDES
    endif
    if not(this.oPgFrm.Page1.oPag.oLMCASFIT_1_22.value==this.w_LMCASFIT)
      this.oPgFrm.Page1.oPag.oLMCASFIT_1_22.value=this.w_LMCASFIT
    endif
    if not(this.oPgFrm.Page1.oPag.oLMCAUSED_1_23.value==this.w_LMCAUSED)
      this.oPgFrm.Page1.oPag.oLMCAUSED_1_23.value=this.w_LMCAUSED
    endif
    if not(this.oPgFrm.Page1.oPag.oLMCAUSSM_1_24.value==this.w_LMCAUSSM)
      this.oPgFrm.Page1.oPag.oLMCAUSSM_1_24.value=this.w_LMCAUSSM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSOT_1_25.value==this.w_DESSOT)
      this.oPgFrm.Page1.oPag.oDESSOT_1_25.value=this.w_DESSOT
    endif
    if not(this.oPgFrm.Page1.oPag.oLMCAUDAC_1_26.value==this.w_LMCAUDAC)
      this.oPgFrm.Page1.oPag.oLMCAUDAC_1_26.value=this.w_LMCAUDAC
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUDES1_1_27.value==this.w_CAUDES1)
      this.oPgFrm.Page1.oPag.oCAUDES1_1_27.value=this.w_CAUDES1
    endif
    if not(this.oPgFrm.Page1.oPag.oLMCAUCAD_1_28.value==this.w_LMCAUCAD)
      this.oPgFrm.Page1.oPag.oLMCAUCAD_1_28.value=this.w_LMCAUCAD
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUDES2_1_29.value==this.w_CAUDES2)
      this.oPgFrm.Page1.oPag.oCAUDES2_1_29.value=this.w_CAUDES2
    endif
    if not(this.oPgFrm.Page2.oPag.oLMESCAU1_2_8.value==this.w_LMESCAU1)
      this.oPgFrm.Page2.oPag.oLMESCAU1_2_8.value=this.w_LMESCAU1
    endif
    if not(this.oPgFrm.Page2.oPag.oDES1_2_9.value==this.w_DES1)
      this.oPgFrm.Page2.oPag.oDES1_2_9.value=this.w_DES1
    endif
    if not(this.oPgFrm.Page2.oPag.oDES2_2_10.value==this.w_DES2)
      this.oPgFrm.Page2.oPag.oDES2_2_10.value=this.w_DES2
    endif
    if not(this.oPgFrm.Page2.oPag.oDES3_2_11.value==this.w_DES3)
      this.oPgFrm.Page2.oPag.oDES3_2_11.value=this.w_DES3
    endif
    if not(this.oPgFrm.Page2.oPag.oDES4_2_12.value==this.w_DES4)
      this.oPgFrm.Page2.oPag.oDES4_2_12.value=this.w_DES4
    endif
    if not(this.oPgFrm.Page2.oPag.oDES5_2_13.value==this.w_DES5)
      this.oPgFrm.Page2.oPag.oDES5_2_13.value=this.w_DES5
    endif
    if not(this.oPgFrm.Page2.oPag.oLMESCAU2_2_14.value==this.w_LMESCAU2)
      this.oPgFrm.Page2.oPag.oLMESCAU2_2_14.value=this.w_LMESCAU2
    endif
    if not(this.oPgFrm.Page2.oPag.oLMESCAU3_2_15.value==this.w_LMESCAU3)
      this.oPgFrm.Page2.oPag.oLMESCAU3_2_15.value=this.w_LMESCAU3
    endif
    if not(this.oPgFrm.Page2.oPag.oLMESCAU4_2_16.value==this.w_LMESCAU4)
      this.oPgFrm.Page2.oPag.oLMESCAU4_2_16.value=this.w_LMESCAU4
    endif
    if not(this.oPgFrm.Page2.oPag.oLMESCAU5_2_17.value==this.w_LMESCAU5)
      this.oPgFrm.Page2.oPag.oLMESCAU5_2_17.value=this.w_LMESCAU5
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUSED_1_37.value==this.w_CAUSED)
      this.oPgFrm.Page1.oPag.oCAUSED_1_37.value=this.w_CAUSED
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUSEDSM_1_39.value==this.w_CAUSEDSM)
      this.oPgFrm.Page1.oPag.oCAUSEDSM_1_39.value=this.w_CAUSEDSM
    endif
    if not(this.oPgFrm.Page3.oPag.oLMAGOSOG_3_1.value==this.w_LMAGOSOG)
      this.oPgFrm.Page3.oPag.oLMAGOSOG_3_1.value=this.w_LMAGOSOG
    endif
    if not(this.oPgFrm.Page3.oPag.oLMAGOPRO_3_3.value==this.w_LMAGOPRO)
      this.oPgFrm.Page3.oPag.oLMAGOPRO_3_3.value=this.w_LMAGOPRO
    endif
    if not(this.oPgFrm.Page3.oPag.oLMAGOREG_3_5.value==this.w_LMAGOREG)
      this.oPgFrm.Page3.oPag.oLMAGOREG_3_5.value=this.w_LMAGOREG
    endif
    if not(this.oPgFrm.Page3.oPag.oLMAGONUM_3_7.value==this.w_LMAGONUM)
      this.oPgFrm.Page3.oPag.oLMAGONUM_3_7.value=this.w_LMAGONUM
    endif
    if not(this.oPgFrm.Page3.oPag.oLMAGOCLI_3_9.value==this.w_LMAGOCLI)
      this.oPgFrm.Page3.oPag.oLMAGOCLI_3_9.value=this.w_LMAGOCLI
    endif
    if not(this.oPgFrm.Page3.oPag.oLMAGOFOR_3_10.value==this.w_LMAGOFOR)
      this.oPgFrm.Page3.oPag.oLMAGOFOR_3_10.value=this.w_LMAGOFOR
    endif
    if not(this.oPgFrm.Page3.oPag.oLMAGOFLT_3_13.RadioValue()==this.w_LMAGOFLT)
      this.oPgFrm.Page3.oPag.oLMAGOFLT_3_13.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oLMPROTVE_3_15.RadioValue()==this.w_LMPROTVE)
      this.oPgFrm.Page3.oPag.oLMPROTVE_3_15.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'STU_PARA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_LMAZISTU) and (g_TRAEXP<>'G'))  and (g_TRAEXP<>'G')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLMAZISTU_1_3.SetFocus()
            i_bnoObbl = !empty(.w_LMAZISTU) or !(g_TRAEXP<>'G')
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_LMCODPDC) and (g_TRAEXP<>'G'))  and (g_TRAEXP<>'G')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oLMCODPDC_1_6.SetFocus()
            i_bnoObbl = !empty(.w_LMCODPDC) or !(g_TRAEXP<>'G')
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_LMAGOSOG) and (g_TRAEXP='G'))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oLMAGOSOG_3_1.SetFocus()
            i_bnoObbl = !empty(.w_LMAGOSOG) or !(g_TRAEXP='G')
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_LMAGOPRO) and (g_TRAEXP='G'))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oLMAGOPRO_3_3.SetFocus()
            i_bnoObbl = !empty(.w_LMAGOPRO) or !(g_TRAEXP='G')
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_LMAGOREG) and (g_TRAEXP='G'))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oLMAGOREG_3_5.SetFocus()
            i_bnoObbl = !empty(.w_LMAGOREG) or !(g_TRAEXP='G')
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_LMAGONUM) and (g_TRAEXP='G'))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oLMAGONUM_3_7.SetFocus()
            i_bnoObbl = !empty(.w_LMAGONUM) or !(g_TRAEXP='G')
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_LMAGOCLI) and (g_TRAEXP='G'))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oLMAGOCLI_3_9.SetFocus()
            i_bnoObbl = !empty(.w_LMAGOCLI) or !(g_TRAEXP='G')
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_LMAGOFOR) and (g_TRAEXP='G'))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oLMAGOFOR_3_10.SetFocus()
            i_bnoObbl = !empty(.w_LMAGOFOR) or !(g_TRAEXP='G')
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LMAZISTU = this.w_LMAZISTU
    return

enddefine

* --- Define pages as container
define class tgslm_atsPag1 as StdContainer
  Width  = 603
  height = 343
  stdWidth  = 603
  stdheight = 343
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLMAZISTU_1_3 as StdField with uid="UGHPOIOUKZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_LMAZISTU", cQueryName = "LMAZISTU",nZero=6,;
    bObbl = .t. , nPag = 1, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Codice azienda nello studio",;
    HelpContextID = 40150539,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=251, Top=38, cSayPict='"999999"', cGetPict='"999999"', InputMask=replicate('X',6)

  func oLMAZISTU_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_TRAEXP<>'G')
    endwith
   endif
  endfunc

  func oLMAZISTU_1_3.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=g_TRAEXP<>'G'
    endwith
    return i_bres
  endfunc

  add object oLMPROFIL_1_4 as StdField with uid="DIPKWVZYDH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_LMPROFIL", cQueryName = "LMPROFIL",nZero=2,;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 172124670,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=543, Top=38, cSayPict='"99"', cGetPict='"99"', InputMask=replicate('X',2)

  func oLMPROFIL_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_TRAEXP<>'G')
    endwith
   endif
  endfunc

  add object oLMFLANUN_1_5 as StdCheck with uid="NSSJISPJVD",rtseq=5,rtrep=.f.,left=251, top=64, caption="Anagrafica unica",;
    ToolTipText = "Se attivo importa/esporta i clienti/fornitori componendo il codice con la partita IVA",;
    HelpContextID = 215414276,;
    cFormVar="w_LMFLANUN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLMFLANUN_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    ''))
  endfunc
  func oLMFLANUN_1_5.GetRadio()
    this.Parent.oContained.w_LMFLANUN = this.RadioValue()
    return .t.
  endfunc

  func oLMFLANUN_1_5.SetRadio()
    this.Parent.oContained.w_LMFLANUN=trim(this.Parent.oContained.w_LMFLANUN)
    this.value = ;
      iif(this.Parent.oContained.w_LMFLANUN=='S',1,;
      0)
  endfunc

  func oLMFLANUN_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_TRAEXP $ ('N-S-A'))
    endwith
   endif
  endfunc

  add object oLMCODPDC_1_6 as StdField with uid="QSTSWBORQA",rtseq=6,rtrep=.f.,;
    cFormVar = "w_LMCODPDC", cQueryName = "LMCODPDC",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Rappresenta univocamente il codice del piano dei conti di apri usato per l'azienda",;
    HelpContextID = 252298745,;
   bGlobalFont=.t.,;
    Height=21, Width=45, Left=543, Top=64, InputMask=replicate('X',4)

  func oLMCODPDC_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_TRAEXP<>'G')
    endwith
   endif
  endfunc

  func oLMCODPDC_1_6.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=g_TRAEXP<>'G'
    endwith
    return i_bres
  endfunc

  add object oLMCHKNO_1_7 as StdCheck with uid="YARZKHAINX",rtseq=7,rtrep=.f.,left=251, top=87, caption="Gestione nuovo regime iva per cassa",;
    ToolTipText = "Nel file di export considera le fatture ad esigibilitÓ differita senza norme associate secondo il nuovo regime di iva per cassa (art.32 bis del D.L.83/2012)",;
    HelpContextID = 42809930,;
    cFormVar="w_LMCHKNO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLMCHKNO_1_7.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oLMCHKNO_1_7.GetRadio()
    this.Parent.oContained.w_LMCHKNO = this.RadioValue()
    return .t.
  endfunc

  func oLMCHKNO_1_7.SetRadio()
    this.Parent.oContained.w_LMCHKNO=trim(this.Parent.oContained.w_LMCHKNO)
    this.value = ;
      iif(this.Parent.oContained.w_LMCHKNO=='S',1,;
      0)
  endfunc

  add object oLMASSOCI_1_16 as StdField with uid="PIPVTPWVKH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_LMASSOCI", cQueryName = "LMASSOCI",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Trascodifica utilizzata per il trasferimento allo studio",;
    HelpContextID = 251504127,;
   bGlobalFont=.t.,;
    Height=21, Width=68, Left=251, Top=135, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="STU_TRAS", cZoomOnZoom="GSLM_MHS", oKey_1_1="LMCODICE", oKey_1_2="this.w_LMASSOCI"

  func oLMASSOCI_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LMAVANZATO())
    endwith
   endif
  endfunc

  func oLMASSOCI_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMASSOCI_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLMASSOCI_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STU_TRAS','*','LMCODICE',cp_AbsName(this.parent,'oLMASSOCI_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSLM_MHS',"",'',this.parent.oContained
  endproc
  proc oLMASSOCI_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSLM_MHS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LMCODICE=this.parent.oContained.w_LMASSOCI
     i_obj.ecpSave()
  endproc

  add object oDESASS_1_17 as StdField with uid="WMPOLEEOTE",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESASS", cQueryName = "DESASS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 49069366,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=326, Top=135, InputMask=replicate('X',20)

  add object oASSDAT_1_19 as StdField with uid="IIPPCFEQTZ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ASSDAT", cQueryName = "ASSDAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 47172358,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=522, Top=135

  add object oLMCAUGIR_1_20 as StdField with uid="MLNCFSFBBV",rtseq=11,rtrep=.f.,;
    cFormVar = "w_LMCAUGIR", cQueryName = "LMCAUGIR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice causale di giroconto per generazione automatica movimenti",;
    HelpContextID = 150223352,;
   bGlobalFont=.t.,;
    Height=21, Width=68, Left=251, Top=161, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_LMCAUGIR"

  func oLMCAUGIR_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LMAVANZATO())
    endwith
   endif
  endfunc

  func oLMCAUGIR_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMCAUGIR_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLMCAUGIR_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oLMCAUGIR_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oLMCAUGIR_1_20.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_LMCAUGIR
     i_obj.ecpSave()
  endproc

  add object oCAUDES_1_21 as StdField with uid="MQPQZSCKTA",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CAUDES", cQueryName = "CAUDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 34593062,;
   bGlobalFont=.t.,;
    Height=21, Width=160, Left=326, Top=161, InputMask=replicate('X',20)

  add object oLMCASFIT_1_22 as StdField with uid="SQWUELCFGL",rtseq=13,rtrep=.f.,;
    cFormVar = "w_LMCASFIT", cQueryName = "LMCASFIT",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Conto cassa fittizia",;
    HelpContextID = 169097718,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=251, Top=190, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="gsar_api", oKey_1_1="ANTIPCON", oKey_1_2="this.w_LMTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_LMCASFIT"

  func oLMCASFIT_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LMAVANZATO())
    endwith
   endif
  endfunc

  func oLMCASFIT_1_22.mHide()
    with this.Parent.oContained
      return (g_TRAEXP='G')
    endwith
  endfunc

  func oLMCASFIT_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMCASFIT_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLMCASFIT_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_LMTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_LMTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oLMCASFIT_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_api',"Conti",'',this.parent.oContained
  endproc
  proc oLMCASFIT_1_22.mZoomOnZoom
    local i_obj
    i_obj=gsar_api()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_LMTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_LMCASFIT
     i_obj.ecpSave()
  endproc

  add object oLMCAUSED_1_23 as StdField with uid="BQDJYBISEV",rtseq=14,rtrep=.f.,;
    cFormVar = "w_LMCAUSED", cQueryName = "LMCAUSED",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale di riga da utilizzare nel  piano generazione movimenti esigibilitÓ differita per la generazione automatica dello storno delle fatture di acquisto e vendita con IVA sospesa",;
    HelpContextID = 51103226,;
   bGlobalFont=.t.,;
    Height=21, Width=68, Left=251, Top=218, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_LMCAUSED"

  func oLMCAUSED_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LMAVANZATO())
    endwith
   endif
  endfunc

  func oLMCAUSED_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMCAUSED_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLMCAUSED_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oLMCAUSED_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oLMCAUSED_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_LMCAUSED
     i_obj.ecpSave()
  endproc

  add object oLMCAUSSM_1_24 as StdField with uid="ACFCFJMJQO",rtseq=15,rtrep=.f.,;
    cFormVar = "w_LMCAUSSM", cQueryName = "LMCAUSSM",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale delle operazioni di acquisto con San Marino",;
    HelpContextID = 51103235,;
   bGlobalFont=.t.,;
    Height=21, Width=68, Left=251, Top=243, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_LMCAUSSM"

  func oLMCAUSSM_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LMAVANZATO())
    endwith
   endif
  endfunc

  func oLMCAUSSM_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMCAUSSM_1_24.ecpDrop(oSource)
    this.Parent.oContained.link_1_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLMCAUSSM_1_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oLMCAUSSM_1_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oLMCAUSSM_1_24.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_LMCAUSSM
     i_obj.ecpSave()
  endproc

  add object oDESSOT_1_25 as StdField with uid="MNLFVKPQZR",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESSOT", cQueryName = "DESSOT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 62831926,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=382, Top=190, InputMask=replicate('X',40)

  func oDESSOT_1_25.mHide()
    with this.Parent.oContained
      return (g_TRAEXP='G')
    endwith
  endfunc

  add object oLMCAUDAC_1_26 as StdField with uid="BUNPHXEPOX",rtseq=17,rtrep=.f.,;
    cFormVar = "w_LMCAUDAC", cQueryName = "LMCAUDAC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice causale studio diversi a conto",;
    HelpContextID = 67880441,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=251, Top=295, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="STU_CAUS", cZoomOnZoom="GSLM_ACC", oKey_1_1="LMCODCAU", oKey_1_2="this.w_LMCAUDAC"

  func oLMCAUDAC_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LMAVANZATO())
    endwith
   endif
  endfunc

  func oLMCAUDAC_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMCAUDAC_1_26.ecpDrop(oSource)
    this.Parent.oContained.link_1_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLMCAUDAC_1_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STU_CAUS','*','LMCODCAU',cp_AbsName(this.parent,'oLMCAUDAC_1_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSLM_ACC',"Causali contabili studio",'',this.parent.oContained
  endproc
  proc oLMCAUDAC_1_26.mZoomOnZoom
    local i_obj
    i_obj=GSLM_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LMCODCAU=this.parent.oContained.w_LMCAUDAC
     i_obj.ecpSave()
  endproc

  add object oCAUDES1_1_27 as StdField with uid="SYJZANLAML",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CAUDES1", cQueryName = "CAUDES1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 34593062,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=337, Top=295, InputMask=replicate('X',20)

  add object oLMCAUCAD_1_28 as StdField with uid="XARWYRCTIO",rtseq=19,rtrep=.f.,;
    cFormVar = "w_LMCAUCAD", cQueryName = "LMCAUCAD",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice causale studio conto a diversi",;
    HelpContextID = 51103226,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=251, Top=318, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="STU_CAUS", cZoomOnZoom="GSLM_ACC", oKey_1_1="LMCODCAU", oKey_1_2="this.w_LMCAUCAD"

  func oLMCAUCAD_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LMAVANZATO())
    endwith
   endif
  endfunc

  func oLMCAUCAD_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMCAUCAD_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLMCAUCAD_1_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STU_CAUS','*','LMCODCAU',cp_AbsName(this.parent,'oLMCAUCAD_1_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSLM_ACC',"Causali contabili studio",'',this.parent.oContained
  endproc
  proc oLMCAUCAD_1_28.mZoomOnZoom
    local i_obj
    i_obj=GSLM_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LMCODCAU=this.parent.oContained.w_LMCAUCAD
     i_obj.ecpSave()
  endproc

  add object oCAUDES2_1_29 as StdField with uid="PXNNQIYSBH",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CAUDES2", cQueryName = "CAUDES2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 34593062,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=337, Top=318, InputMask=replicate('X',20)

  add object oCAUSED_1_37 as StdField with uid="NPBXBGRMXO",rtseq=31,rtrep=.f.,;
    cFormVar = "w_CAUSED", cQueryName = "CAUSED",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 216082138,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=326, Top=218, InputMask=replicate('X',35)

  add object oCAUSEDSM_1_39 as StdField with uid="WSSLUTHJMZ",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CAUSEDSM", cQueryName = "CAUSEDSM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 52353395,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=326, Top=243, InputMask=replicate('X',35)

  add object oStr_1_8 as StdString with uid="KVNKNGZATF",Visible=.t., Left=4, Top=113,;
    Alignment=0, Width=333, Height=18,;
    Caption="Impostazioni avanzate per il trasferimento studio/Ago"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_10 as StdString with uid="PGQKEKLWSF",Visible=.t., Left=98, Top=135,;
    Alignment=1, Width=148, Height=18,;
    Caption="Trascodifica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="MXYHGJCTBV",Visible=.t., Left=98, Top=161,;
    Alignment=1, Width=148, Height=18,;
    Caption="Causale giroconto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="QSNIFNOCQC",Visible=.t., Left=98, Top=295,;
    Alignment=1, Width=148, Height=18,;
    Caption="Causale diversi a conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="TZWNQVNTOR",Visible=.t., Left=98, Top=318,;
    Alignment=1, Width=148, Height=18,;
    Caption="Causale conto a diversi:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="BXCDXEWRZU",Visible=.t., Left=4, Top=268,;
    Alignment=0, Width=364, Height=18,;
    Caption="Causali dello studio necessarie per generazione movimenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="LXJBSEDUQQ",Visible=.t., Left=489, Top=135,;
    Alignment=1, Width=32, Height=18,;
    Caption="del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="QQABMLVDWG",Visible=.t., Left=98, Top=190,;
    Alignment=1, Width=148, Height=18,;
    Caption="Cassa fittizia:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (g_TRAEXP='G')
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="OPCMRJHBEV",Visible=.t., Left=4, Top=10,;
    Alignment=0, Width=311, Height=18,;
    Caption="Impostazioni base per il trasferimento studio"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_32 as StdString with uid="XKGKLFDPSG",Visible=.t., Left=88, Top=38,;
    Alignment=1, Width=158, Height=18,;
    Caption="Codice azienda studio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="KDKWVSLVFT",Visible=.t., Left=367, Top=38,;
    Alignment=1, Width=171, Height=18,;
    Caption="Progressivo spedizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="WPXEAOCIJF",Visible=.t., Left=414, Top=64,;
    Alignment=1, Width=124, Height=18,;
    Caption="Cod. piano dei conti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="NJTHZJMBVD",Visible=.t., Left=4, Top=219,;
    Alignment=1, Width=242, Height=18,;
    Caption="Causale di riga storno fatt. con IVA sospesa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="XQFJWWAIXV",Visible=.t., Left=-2, Top=243,;
    Alignment=1, Width=248, Height=18,;
    Caption="Causale operazioni acquisti con San Marino:"  ;
  , bGlobalFont=.t.

  add object oBox_1_9 as StdBox with uid="BMFDSNPMZF",left=2, top=131, width=600,height=2

  add object oBox_1_15 as StdBox with uid="URJZYTMPUG",left=2, top=286, width=601,height=2

  add object oBox_1_34 as StdBox with uid="DZDQVKJTKA",left=2, top=28, width=601,height=2
enddefine
define class tgslm_atsPag2 as StdContainer
  Width  = 603
  height = 343
  stdWidth  = 603
  stdheight = 343
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLMESCAU1_2_8 as StdField with uid="YAPQSGKXYP",rtseq=21,rtrep=.f.,;
    cFormVar = "w_LMESCAU1", cQueryName = "LMESCAU1",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale 1 da escludere nel trasferimento",;
    HelpContextID = 268297703,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=157, Top=39, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_LMESCAU1"

  func oLMESCAU1_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LMAVANZATO())
    endwith
   endif
  endfunc

  func oLMESCAU1_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMESCAU1_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLMESCAU1_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oLMESCAU1_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oLMESCAU1_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_LMESCAU1
     i_obj.ecpSave()
  endproc

  add object oDES1_2_9 as StdField with uid="AMMWQHURPH",rtseq=22,rtrep=.f.,;
    cFormVar = "w_DES1", cQueryName = "DES1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 89342666,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=223, Top=39, InputMask=replicate('X',35)

  add object oDES2_2_10 as StdField with uid="IETMRUKISK",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DES2", cQueryName = "DES2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 89277130,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=223, Top=66, InputMask=replicate('X',35)

  add object oDES3_2_11 as StdField with uid="QFFTTLNHEX",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DES3", cQueryName = "DES3",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 89211594,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=223, Top=93, InputMask=replicate('X',35)

  add object oDES4_2_12 as StdField with uid="XBWAWYUXSS",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DES4", cQueryName = "DES4",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 89146058,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=223, Top=120, InputMask=replicate('X',35)

  add object oDES5_2_13 as StdField with uid="PTFDXVFBTG",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DES5", cQueryName = "DES5",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 89080522,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=223, Top=147, InputMask=replicate('X',35)

  add object oLMESCAU2_2_14 as StdField with uid="CDOIJNVZPT",rtseq=27,rtrep=.f.,;
    cFormVar = "w_LMESCAU2", cQueryName = "LMESCAU2",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale 2 da escludere nel trasferimento",;
    HelpContextID = 268297704,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=157, Top=66, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_LMESCAU2"

  func oLMESCAU2_2_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LMAVANZATO())
    endwith
   endif
  endfunc

  func oLMESCAU2_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMESCAU2_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLMESCAU2_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oLMESCAU2_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oLMESCAU2_2_14.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_LMESCAU2
     i_obj.ecpSave()
  endproc

  add object oLMESCAU3_2_15 as StdField with uid="XVKOGEGEDZ",rtseq=28,rtrep=.f.,;
    cFormVar = "w_LMESCAU3", cQueryName = "LMESCAU3",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale 3 da escludere nel trasferimento",;
    HelpContextID = 268297705,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=157, Top=93, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_LMESCAU3"

  func oLMESCAU3_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LMAVANZATO())
    endwith
   endif
  endfunc

  func oLMESCAU3_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMESCAU3_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLMESCAU3_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oLMESCAU3_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oLMESCAU3_2_15.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_LMESCAU3
     i_obj.ecpSave()
  endproc

  add object oLMESCAU4_2_16 as StdField with uid="ZMXVXVHCOO",rtseq=29,rtrep=.f.,;
    cFormVar = "w_LMESCAU4", cQueryName = "LMESCAU4",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale 4 da escludere nel trasferimento",;
    HelpContextID = 268297706,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=157, Top=120, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_LMESCAU4"

  func oLMESCAU4_2_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LMAVANZATO())
    endwith
   endif
  endfunc

  func oLMESCAU4_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMESCAU4_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLMESCAU4_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oLMESCAU4_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oLMESCAU4_2_16.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_LMESCAU4
     i_obj.ecpSave()
  endproc

  add object oLMESCAU5_2_17 as StdField with uid="MWRKZOREOR",rtseq=30,rtrep=.f.,;
    cFormVar = "w_LMESCAU5", cQueryName = "LMESCAU5",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale 5 da escludere nel trasferimento",;
    HelpContextID = 268297707,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=157, Top=147, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_LMESCAU5"

  func oLMESCAU5_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LMAVANZATO())
    endwith
   endif
  endfunc

  func oLMESCAU5_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oLMESCAU5_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oLMESCAU5_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oLMESCAU5_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oLMESCAU5_2_17.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_LMESCAU5
     i_obj.ecpSave()
  endproc

  add object oStr_2_1 as StdString with uid="SETXKHWAMN",Visible=.t., Left=4, Top=39,;
    Alignment=1, Width=148, Height=18,;
    Caption="1^ Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_2 as StdString with uid="JCOFIKOIQB",Visible=.t., Left=4, Top=66,;
    Alignment=1, Width=148, Height=18,;
    Caption="2^ Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_3 as StdString with uid="NKFRHHLLBQ",Visible=.t., Left=4, Top=93,;
    Alignment=1, Width=148, Height=18,;
    Caption="3^ Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_4 as StdString with uid="KALEMXRUHB",Visible=.t., Left=4, Top=120,;
    Alignment=1, Width=148, Height=18,;
    Caption="4^ Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="MZKHTJWXCD",Visible=.t., Left=4, Top=147,;
    Alignment=1, Width=148, Height=18,;
    Caption="5^ Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="IWWTUXOMYV",Visible=.t., Left=4, Top=11,;
    Alignment=0, Width=317, Height=18,;
    Caption="Elenco causali escluse dal trasferimento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_2_7 as StdBox with uid="HSHFMEAEWO",left=1, top=30, width=520,height=2
enddefine
define class tgslm_atsPag3 as StdContainer
  Width  = 603
  height = 343
  stdWidth  = 603
  stdheight = 343
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLMAGOSOG_3_1 as StdField with uid="UEVMMXMHUO",rtseq=33,rtrep=.f.,;
    cFormVar = "w_LMAGOSOG", cQueryName = "LMAGOSOG",;
    bObbl = .t. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice soggetto Ago (Ditta)",;
    HelpContextID = 223238659,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=175, Top=18, InputMask=replicate('X',15)

  func oLMAGOSOG_3_1.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=g_TRAEXP='G'
    endwith
    return i_bres
  endfunc

  add object oLMAGOPRO_3_3 as StdField with uid="VMRIKGQAGU",rtseq=34,rtrep=.f.,;
    cFormVar = "w_LMAGOPRO", cQueryName = "LMAGOPRO",;
    bObbl = .t. , nPag = 3, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Progressivo scheda: 1 ordinaria 2 semplificata",;
    HelpContextID = 263300613,;
   bGlobalFont=.t.,;
    Height=21, Width=38, Left=175, Top=52, InputMask=replicate('X',1)

  func oLMAGOPRO_3_3.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=g_TRAEXP='G'
    endwith
    return i_bres
  endfunc

  add object oLMAGOREG_3_5 as StdField with uid="AONUYWBFCP",rtseq=35,rtrep=.f.,;
    cFormVar = "w_LMAGOREG", cQueryName = "LMAGOREG",;
    bObbl = .t. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Applicazione registrata assegnata ad ogni singolo soggetto, utilizzata per la trascodifica dei clienti/fornitori",;
    HelpContextID = 28419581,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=175, Top=86, InputMask=replicate('X',5)

  func oLMAGOREG_3_5.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=g_TRAEXP='G'
    endwith
    return i_bres
  endfunc

  add object oLMAGONUM_3_7 as StdField with uid="OXAWPFKIHP",rtseq=36,rtrep=.f.,;
    cFormVar = "w_LMAGONUM", cQueryName = "LMAGONUM",;
    bObbl = .t. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Numero importazione Ago",;
    HelpContextID = 229746179,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=175, Top=117, InputMask=replicate('X',10)

  func oLMAGONUM_3_7.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=g_TRAEXP='G'
    endwith
    return i_bres
  endfunc

  add object oLMAGOCLI_3_9 as StdField with uid="YXOAJMRWJE",rtseq=37,rtrep=.f.,;
    cFormVar = "w_LMAGOCLI", cQueryName = "LMAGOCLI",;
    bObbl = .t. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice mastro cliente",;
    HelpContextID = 223238657,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=175, Top=151, InputMask=replicate('X',10)

  func oLMAGOCLI_3_9.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=g_TRAEXP='G'
    endwith
    return i_bres
  endfunc

  add object oLMAGOFOR_3_10 as StdField with uid="TPLNKDWDJL",rtseq=38,rtrep=.f.,;
    cFormVar = "w_LMAGOFOR", cQueryName = "LMAGOFOR",;
    bObbl = .t. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice mastro fornitore",;
    HelpContextID = 172907000,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=175, Top=185, InputMask=replicate('X',10)

  func oLMAGOFOR_3_10.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=g_TRAEXP='G'
    endwith
    return i_bres
  endfunc

  add object oLMAGOFLT_3_13 as StdCheck with uid="XTUFZXQXFH",rtseq=39,rtrep=.f.,left=175, top=211, caption="Prefisso tipo conto",;
    ToolTipText = "Se attivo antepone il tipo conto al codice cliente/fornitore",;
    HelpContextID = 172906998,;
    cFormVar="w_LMAGOFLT", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oLMAGOFLT_3_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oLMAGOFLT_3_13.GetRadio()
    this.Parent.oContained.w_LMAGOFLT = this.RadioValue()
    return .t.
  endfunc

  func oLMAGOFLT_3_13.SetRadio()
    this.Parent.oContained.w_LMAGOFLT=trim(this.Parent.oContained.w_LMAGOFLT)
    this.value = ;
      iif(this.Parent.oContained.w_LMAGOFLT=='S',1,;
      0)
  endfunc

  add object oLMPROTVE_3_15 as StdCheck with uid="STFJCJXEBI",rtseq=41,rtrep=.f.,left=175, top=235, caption="Forza protocollo sulle vendite",;
    ToolTipText = "Se attivo scrive nel campo del protocollo il numero documento ",;
    HelpContextID = 62756347,;
    cFormVar="w_LMPROTVE", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oLMPROTVE_3_15.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oLMPROTVE_3_15.GetRadio()
    this.Parent.oContained.w_LMPROTVE = this.RadioValue()
    return .t.
  endfunc

  func oLMPROTVE_3_15.SetRadio()
    this.Parent.oContained.w_LMPROTVE=trim(this.Parent.oContained.w_LMPROTVE)
    this.value = ;
      iif(this.Parent.oContained.w_LMPROTVE=='S',1,;
      0)
  endfunc

  add object oStr_3_2 as StdString with uid="HXAONAAHHA",Visible=.t., Left=58, Top=18,;
    Alignment=1, Width=116, Height=18,;
    Caption="Codice soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_3_4 as StdString with uid="YVWTXYXRXZ",Visible=.t., Left=61, Top=52,;
    Alignment=1, Width=113, Height=18,;
    Caption="Progressivo scheda:"  ;
  , bGlobalFont=.t.

  add object oStr_3_6 as StdString with uid="VAQNFFJQVI",Visible=.t., Left=47, Top=86,;
    Alignment=1, Width=127, Height=18,;
    Caption="Applicazione registrata:"  ;
  , bGlobalFont=.t.

  add object oStr_3_8 as StdString with uid="PAZBSQKGTD",Visible=.t., Left=29, Top=117,;
    Alignment=1, Width=145, Height=18,;
    Caption="Numero importazione Ago:"  ;
  , bGlobalFont=.t.

  add object oStr_3_11 as StdString with uid="BVWLRBUDCS",Visible=.t., Left=51, Top=151,;
    Alignment=1, Width=123, Height=18,;
    Caption="Codice mastro cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_3_12 as StdString with uid="IMXDBENCGK",Visible=.t., Left=42, Top=185,;
    Alignment=1, Width=132, Height=18,;
    Caption="Codice mastro fornitore:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslm_ats','STU_PARA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LMCODAZI=STU_PARA.LMCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
