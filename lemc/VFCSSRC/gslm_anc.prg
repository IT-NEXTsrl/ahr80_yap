* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_anc                                                        *
*              Tabella codici norma gestione iva per cassa                     *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-07-08                                                      *
* Last revis.: 2009-07-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgslm_anc")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgslm_anc")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgslm_anc")
  return

* --- Class definition
define class tgslm_anc as StdPCForm
  Width  = 776
  Height = 474
  Top    = 15
  Left   = 14
  cComment = "Tabella codici norma gestione iva per cassa"
  cPrg = "gslm_anc"
  HelpContextID=193576041
  add object cnt as tcgslm_anc
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgslm_anc as PCContext
  w_CNCODTRA = space(10)
  w_CNNORMA1 = space(2)
  w_CNNORMA2 = space(2)
  w_CNNORMA3 = space(2)
  w_CNNORMA4 = space(2)
  w_CNNORMA5 = space(2)
  w_CNNORMA6 = space(2)
  w_CNNORMA7 = space(2)
  w_CNNORMA8 = space(2)
  w_CNNORMA9 = space(2)
  w_CNNORM10 = space(2)
  proc Save(oFrom)
    this.w_CNCODTRA = oFrom.w_CNCODTRA
    this.w_CNNORMA1 = oFrom.w_CNNORMA1
    this.w_CNNORMA2 = oFrom.w_CNNORMA2
    this.w_CNNORMA3 = oFrom.w_CNNORMA3
    this.w_CNNORMA4 = oFrom.w_CNNORMA4
    this.w_CNNORMA5 = oFrom.w_CNNORMA5
    this.w_CNNORMA6 = oFrom.w_CNNORMA6
    this.w_CNNORMA7 = oFrom.w_CNNORMA7
    this.w_CNNORMA8 = oFrom.w_CNNORMA8
    this.w_CNNORMA9 = oFrom.w_CNNORMA9
    this.w_CNNORM10 = oFrom.w_CNNORM10
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_CNCODTRA = this.w_CNCODTRA
    oTo.w_CNNORMA1 = this.w_CNNORMA1
    oTo.w_CNNORMA2 = this.w_CNNORMA2
    oTo.w_CNNORMA3 = this.w_CNNORMA3
    oTo.w_CNNORMA4 = this.w_CNNORMA4
    oTo.w_CNNORMA5 = this.w_CNNORMA5
    oTo.w_CNNORMA6 = this.w_CNNORMA6
    oTo.w_CNNORMA7 = this.w_CNNORMA7
    oTo.w_CNNORMA8 = this.w_CNNORMA8
    oTo.w_CNNORMA9 = this.w_CNNORMA9
    oTo.w_CNNORM10 = this.w_CNNORM10
    PCContext::Load(oTo)
enddefine

define class tcgslm_anc as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 776
  Height = 474
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-07-15"
  HelpContextID=193576041
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  COD_NORM_IDX = 0
  STU_NORM_IDX = 0
  cFile = "COD_NORM"
  cKeySelect = "CNCODTRA"
  cKeyWhere  = "CNCODTRA=this.w_CNCODTRA"
  cKeyWhereODBC = '"CNCODTRA="+cp_ToStrODBC(this.w_CNCODTRA)';

  cKeyWhereODBCqualified = '"COD_NORM.CNCODTRA="+cp_ToStrODBC(this.w_CNCODTRA)';

  cPrg = "gslm_anc"
  cComment = "Tabella codici norma gestione iva per cassa"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CNCODTRA = space(10)
  w_CNNORMA1 = space(2)
  w_CNNORMA2 = space(2)
  w_CNNORMA3 = space(2)
  w_CNNORMA4 = space(2)
  w_CNNORMA5 = space(2)
  w_CNNORMA6 = space(2)
  w_CNNORMA7 = space(2)
  w_CNNORMA8 = space(2)
  w_CNNORMA9 = space(2)
  w_CNNORM10 = space(2)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslm_ancPag1","gslm_anc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Codici Norma")
      .Pages(1).HelpContextID = 75382884
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCNNORMA1_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='STU_NORM'
    this.cWorkTables[2]='COD_NORM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.COD_NORM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.COD_NORM_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgslm_anc'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from COD_NORM where CNCODTRA=KeySet.CNCODTRA
    *
    i_nConn = i_TableProp[this.COD_NORM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COD_NORM_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('COD_NORM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "COD_NORM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' COD_NORM '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CNCODTRA',this.w_CNCODTRA  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CNCODTRA = NVL(CNCODTRA,space(10))
        .w_CNNORMA1 = NVL(CNNORMA1,space(2))
          * evitabile
          *.link_1_2('Load')
        .w_CNNORMA2 = NVL(CNNORMA2,space(2))
          * evitabile
          *.link_1_4('Load')
        .w_CNNORMA3 = NVL(CNNORMA3,space(2))
          * evitabile
          *.link_1_6('Load')
        .w_CNNORMA4 = NVL(CNNORMA4,space(2))
          * evitabile
          *.link_1_8('Load')
        .w_CNNORMA5 = NVL(CNNORMA5,space(2))
          * evitabile
          *.link_1_10('Load')
        .w_CNNORMA6 = NVL(CNNORMA6,space(2))
          * evitabile
          *.link_1_12('Load')
        .w_CNNORMA7 = NVL(CNNORMA7,space(2))
          * evitabile
          *.link_1_14('Load')
        .w_CNNORMA8 = NVL(CNNORMA8,space(2))
          * evitabile
          *.link_1_16('Load')
        .w_CNNORMA9 = NVL(CNNORMA9,space(2))
          * evitabile
          *.link_1_17('Load')
        .w_CNNORM10 = NVL(CNNORM10,space(2))
          * evitabile
          *.link_1_19('Load')
        cp_LoadRecExtFlds(this,'COD_NORM')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_CNCODTRA = space(10)
      .w_CNNORMA1 = space(2)
      .w_CNNORMA2 = space(2)
      .w_CNNORMA3 = space(2)
      .w_CNNORMA4 = space(2)
      .w_CNNORMA5 = space(2)
      .w_CNNORMA6 = space(2)
      .w_CNNORMA7 = space(2)
      .w_CNNORMA8 = space(2)
      .w_CNNORMA9 = space(2)
      .w_CNNORM10 = space(2)
      if .cFunction<>"Filter"
        .w_CNCODTRA = 'LEMCO'
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_CNNORMA1))
          .link_1_2('Full')
          endif
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_CNNORMA2))
          .link_1_4('Full')
          endif
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_CNNORMA3))
          .link_1_6('Full')
          endif
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_CNNORMA4))
          .link_1_8('Full')
          endif
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_CNNORMA5))
          .link_1_10('Full')
          endif
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_CNNORMA6))
          .link_1_12('Full')
          endif
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_CNNORMA7))
          .link_1_14('Full')
          endif
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_CNNORMA8))
          .link_1_16('Full')
          endif
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_CNNORMA9))
          .link_1_17('Full')
          endif
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_CNNORM10))
          .link_1_19('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'COD_NORM')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCNNORMA1_1_2.enabled = i_bVal
      .Page1.oPag.oCNNORMA2_1_4.enabled = i_bVal
      .Page1.oPag.oCNNORMA3_1_6.enabled = i_bVal
      .Page1.oPag.oCNNORMA4_1_8.enabled = i_bVal
      .Page1.oPag.oCNNORMA5_1_10.enabled = i_bVal
      .Page1.oPag.oCNNORMA6_1_12.enabled = i_bVal
      .Page1.oPag.oCNNORMA7_1_14.enabled = i_bVal
      .Page1.oPag.oCNNORMA8_1_16.enabled = i_bVal
      .Page1.oPag.oCNNORMA9_1_17.enabled = i_bVal
      .Page1.oPag.oCNNORM10_1_19.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'COD_NORM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.COD_NORM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNCODTRA,"CNCODTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNNORMA1,"CNNORMA1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNNORMA2,"CNNORMA2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNNORMA3,"CNNORMA3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNNORMA4,"CNNORMA4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNNORMA5,"CNNORMA5",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNNORMA6,"CNNORMA6",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNNORMA7,"CNNORMA7",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNNORMA8,"CNNORMA8",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNNORMA9,"CNNORMA9",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CNNORM10,"CNNORM10",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.COD_NORM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COD_NORM_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.COD_NORM_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into COD_NORM
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'COD_NORM')
        i_extval=cp_InsertValODBCExtFlds(this,'COD_NORM')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CNCODTRA,CNNORMA1,CNNORMA2,CNNORMA3,CNNORMA4"+;
                  ",CNNORMA5,CNNORMA6,CNNORMA7,CNNORMA8,CNNORMA9"+;
                  ",CNNORM10 "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CNCODTRA)+;
                  ","+cp_ToStrODBCNull(this.w_CNNORMA1)+;
                  ","+cp_ToStrODBCNull(this.w_CNNORMA2)+;
                  ","+cp_ToStrODBCNull(this.w_CNNORMA3)+;
                  ","+cp_ToStrODBCNull(this.w_CNNORMA4)+;
                  ","+cp_ToStrODBCNull(this.w_CNNORMA5)+;
                  ","+cp_ToStrODBCNull(this.w_CNNORMA6)+;
                  ","+cp_ToStrODBCNull(this.w_CNNORMA7)+;
                  ","+cp_ToStrODBCNull(this.w_CNNORMA8)+;
                  ","+cp_ToStrODBCNull(this.w_CNNORMA9)+;
                  ","+cp_ToStrODBCNull(this.w_CNNORM10)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'COD_NORM')
        i_extval=cp_InsertValVFPExtFlds(this,'COD_NORM')
        cp_CheckDeletedKey(i_cTable,0,'CNCODTRA',this.w_CNCODTRA)
        INSERT INTO (i_cTable);
              (CNCODTRA,CNNORMA1,CNNORMA2,CNNORMA3,CNNORMA4,CNNORMA5,CNNORMA6,CNNORMA7,CNNORMA8,CNNORMA9,CNNORM10  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CNCODTRA;
                  ,this.w_CNNORMA1;
                  ,this.w_CNNORMA2;
                  ,this.w_CNNORMA3;
                  ,this.w_CNNORMA4;
                  ,this.w_CNNORMA5;
                  ,this.w_CNNORMA6;
                  ,this.w_CNNORMA7;
                  ,this.w_CNNORMA8;
                  ,this.w_CNNORMA9;
                  ,this.w_CNNORM10;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.COD_NORM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COD_NORM_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.COD_NORM_IDX,i_nConn)
      *
      * update COD_NORM
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'COD_NORM')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CNNORMA1="+cp_ToStrODBCNull(this.w_CNNORMA1)+;
             ",CNNORMA2="+cp_ToStrODBCNull(this.w_CNNORMA2)+;
             ",CNNORMA3="+cp_ToStrODBCNull(this.w_CNNORMA3)+;
             ",CNNORMA4="+cp_ToStrODBCNull(this.w_CNNORMA4)+;
             ",CNNORMA5="+cp_ToStrODBCNull(this.w_CNNORMA5)+;
             ",CNNORMA6="+cp_ToStrODBCNull(this.w_CNNORMA6)+;
             ",CNNORMA7="+cp_ToStrODBCNull(this.w_CNNORMA7)+;
             ",CNNORMA8="+cp_ToStrODBCNull(this.w_CNNORMA8)+;
             ",CNNORMA9="+cp_ToStrODBCNull(this.w_CNNORMA9)+;
             ",CNNORM10="+cp_ToStrODBCNull(this.w_CNNORM10)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'COD_NORM')
        i_cWhere = cp_PKFox(i_cTable  ,'CNCODTRA',this.w_CNCODTRA  )
        UPDATE (i_cTable) SET;
              CNNORMA1=this.w_CNNORMA1;
             ,CNNORMA2=this.w_CNNORMA2;
             ,CNNORMA3=this.w_CNNORMA3;
             ,CNNORMA4=this.w_CNNORMA4;
             ,CNNORMA5=this.w_CNNORMA5;
             ,CNNORMA6=this.w_CNNORMA6;
             ,CNNORMA7=this.w_CNNORMA7;
             ,CNNORMA8=this.w_CNNORMA8;
             ,CNNORMA9=this.w_CNNORMA9;
             ,CNNORM10=this.w_CNNORM10;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.COD_NORM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.COD_NORM_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.COD_NORM_IDX,i_nConn)
      *
      * delete COD_NORM
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CNCODTRA',this.w_CNCODTRA  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.COD_NORM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.COD_NORM_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CNNORMA1
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_NORM_IDX,3]
    i_lTable = "STU_NORM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2], .t., this.STU_NORM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNNORMA1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSLM_ACN',True,'STU_NORM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODNOR like "+cp_ToStrODBC(trim(this.w_CNNORMA1)+"%");

          i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODNOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODNOR',trim(this.w_CNNORMA1))
          select LMCODNOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODNOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CNNORMA1)==trim(_Link_.LMCODNOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CNNORMA1) and !this.bDontReportError
            deferred_cp_zoom('STU_NORM','*','LMCODNOR',cp_AbsName(oSource.parent,'oCNNORMA1_1_2'),i_cWhere,'GSLM_ACN',"Elenco codici norma studio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',oSource.xKey(1))
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNNORMA1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(this.w_CNNORMA1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',this.w_CNNORMA1)
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNNORMA1 = NVL(_Link_.LMCODNOR,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CNNORMA1 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])+'\'+cp_ToStr(_Link_.LMCODNOR,1)
      cp_ShowWarn(i_cKey,this.STU_NORM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNNORMA1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CNNORMA2
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_NORM_IDX,3]
    i_lTable = "STU_NORM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2], .t., this.STU_NORM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNNORMA2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSLM_ACN',True,'STU_NORM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODNOR like "+cp_ToStrODBC(trim(this.w_CNNORMA2)+"%");

          i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODNOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODNOR',trim(this.w_CNNORMA2))
          select LMCODNOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODNOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CNNORMA2)==trim(_Link_.LMCODNOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CNNORMA2) and !this.bDontReportError
            deferred_cp_zoom('STU_NORM','*','LMCODNOR',cp_AbsName(oSource.parent,'oCNNORMA2_1_4'),i_cWhere,'GSLM_ACN',"Elenco codici norma studio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',oSource.xKey(1))
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNNORMA2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(this.w_CNNORMA2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',this.w_CNNORMA2)
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNNORMA2 = NVL(_Link_.LMCODNOR,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CNNORMA2 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])+'\'+cp_ToStr(_Link_.LMCODNOR,1)
      cp_ShowWarn(i_cKey,this.STU_NORM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNNORMA2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CNNORMA3
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_NORM_IDX,3]
    i_lTable = "STU_NORM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2], .t., this.STU_NORM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNNORMA3) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSLM_ACN',True,'STU_NORM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODNOR like "+cp_ToStrODBC(trim(this.w_CNNORMA3)+"%");

          i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODNOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODNOR',trim(this.w_CNNORMA3))
          select LMCODNOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODNOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CNNORMA3)==trim(_Link_.LMCODNOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CNNORMA3) and !this.bDontReportError
            deferred_cp_zoom('STU_NORM','*','LMCODNOR',cp_AbsName(oSource.parent,'oCNNORMA3_1_6'),i_cWhere,'GSLM_ACN',"Elenco codici norma studio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',oSource.xKey(1))
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNNORMA3)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(this.w_CNNORMA3);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',this.w_CNNORMA3)
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNNORMA3 = NVL(_Link_.LMCODNOR,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CNNORMA3 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])+'\'+cp_ToStr(_Link_.LMCODNOR,1)
      cp_ShowWarn(i_cKey,this.STU_NORM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNNORMA3 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CNNORMA4
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_NORM_IDX,3]
    i_lTable = "STU_NORM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2], .t., this.STU_NORM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNNORMA4) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSLM_ACN',True,'STU_NORM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODNOR like "+cp_ToStrODBC(trim(this.w_CNNORMA4)+"%");

          i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODNOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODNOR',trim(this.w_CNNORMA4))
          select LMCODNOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODNOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CNNORMA4)==trim(_Link_.LMCODNOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CNNORMA4) and !this.bDontReportError
            deferred_cp_zoom('STU_NORM','*','LMCODNOR',cp_AbsName(oSource.parent,'oCNNORMA4_1_8'),i_cWhere,'GSLM_ACN',"Elenco codici norma studio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',oSource.xKey(1))
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNNORMA4)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(this.w_CNNORMA4);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',this.w_CNNORMA4)
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNNORMA4 = NVL(_Link_.LMCODNOR,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CNNORMA4 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])+'\'+cp_ToStr(_Link_.LMCODNOR,1)
      cp_ShowWarn(i_cKey,this.STU_NORM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNNORMA4 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CNNORMA5
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_NORM_IDX,3]
    i_lTable = "STU_NORM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2], .t., this.STU_NORM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNNORMA5) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSLM_ACN',True,'STU_NORM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODNOR like "+cp_ToStrODBC(trim(this.w_CNNORMA5)+"%");

          i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODNOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODNOR',trim(this.w_CNNORMA5))
          select LMCODNOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODNOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CNNORMA5)==trim(_Link_.LMCODNOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CNNORMA5) and !this.bDontReportError
            deferred_cp_zoom('STU_NORM','*','LMCODNOR',cp_AbsName(oSource.parent,'oCNNORMA5_1_10'),i_cWhere,'GSLM_ACN',"Elenco codici norma studio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',oSource.xKey(1))
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNNORMA5)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(this.w_CNNORMA5);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',this.w_CNNORMA5)
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNNORMA5 = NVL(_Link_.LMCODNOR,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CNNORMA5 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])+'\'+cp_ToStr(_Link_.LMCODNOR,1)
      cp_ShowWarn(i_cKey,this.STU_NORM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNNORMA5 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CNNORMA6
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_NORM_IDX,3]
    i_lTable = "STU_NORM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2], .t., this.STU_NORM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNNORMA6) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSLM_ACN',True,'STU_NORM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODNOR like "+cp_ToStrODBC(trim(this.w_CNNORMA6)+"%");

          i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODNOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODNOR',trim(this.w_CNNORMA6))
          select LMCODNOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODNOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CNNORMA6)==trim(_Link_.LMCODNOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CNNORMA6) and !this.bDontReportError
            deferred_cp_zoom('STU_NORM','*','LMCODNOR',cp_AbsName(oSource.parent,'oCNNORMA6_1_12'),i_cWhere,'GSLM_ACN',"Elenco codici norma studio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',oSource.xKey(1))
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNNORMA6)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(this.w_CNNORMA6);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',this.w_CNNORMA6)
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNNORMA6 = NVL(_Link_.LMCODNOR,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CNNORMA6 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])+'\'+cp_ToStr(_Link_.LMCODNOR,1)
      cp_ShowWarn(i_cKey,this.STU_NORM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNNORMA6 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CNNORMA7
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_NORM_IDX,3]
    i_lTable = "STU_NORM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2], .t., this.STU_NORM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNNORMA7) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSLM_ACN',True,'STU_NORM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODNOR like "+cp_ToStrODBC(trim(this.w_CNNORMA7)+"%");

          i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODNOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODNOR',trim(this.w_CNNORMA7))
          select LMCODNOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODNOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CNNORMA7)==trim(_Link_.LMCODNOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CNNORMA7) and !this.bDontReportError
            deferred_cp_zoom('STU_NORM','*','LMCODNOR',cp_AbsName(oSource.parent,'oCNNORMA7_1_14'),i_cWhere,'GSLM_ACN',"Elenco codici norma studio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',oSource.xKey(1))
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNNORMA7)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(this.w_CNNORMA7);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',this.w_CNNORMA7)
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNNORMA7 = NVL(_Link_.LMCODNOR,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CNNORMA7 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])+'\'+cp_ToStr(_Link_.LMCODNOR,1)
      cp_ShowWarn(i_cKey,this.STU_NORM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNNORMA7 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CNNORMA8
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_NORM_IDX,3]
    i_lTable = "STU_NORM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2], .t., this.STU_NORM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNNORMA8) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSLM_ACN',True,'STU_NORM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODNOR like "+cp_ToStrODBC(trim(this.w_CNNORMA8)+"%");

          i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODNOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODNOR',trim(this.w_CNNORMA8))
          select LMCODNOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODNOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CNNORMA8)==trim(_Link_.LMCODNOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CNNORMA8) and !this.bDontReportError
            deferred_cp_zoom('STU_NORM','*','LMCODNOR',cp_AbsName(oSource.parent,'oCNNORMA8_1_16'),i_cWhere,'GSLM_ACN',"Elenco codici norma studio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',oSource.xKey(1))
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNNORMA8)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(this.w_CNNORMA8);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',this.w_CNNORMA8)
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNNORMA8 = NVL(_Link_.LMCODNOR,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CNNORMA8 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])+'\'+cp_ToStr(_Link_.LMCODNOR,1)
      cp_ShowWarn(i_cKey,this.STU_NORM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNNORMA8 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CNNORMA9
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_NORM_IDX,3]
    i_lTable = "STU_NORM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2], .t., this.STU_NORM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNNORMA9) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSLM_ACN',True,'STU_NORM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODNOR like "+cp_ToStrODBC(trim(this.w_CNNORMA9)+"%");

          i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODNOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODNOR',trim(this.w_CNNORMA9))
          select LMCODNOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODNOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CNNORMA9)==trim(_Link_.LMCODNOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CNNORMA9) and !this.bDontReportError
            deferred_cp_zoom('STU_NORM','*','LMCODNOR',cp_AbsName(oSource.parent,'oCNNORMA9_1_17'),i_cWhere,'GSLM_ACN',"Elenco codici norma studio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',oSource.xKey(1))
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNNORMA9)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(this.w_CNNORMA9);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',this.w_CNNORMA9)
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNNORMA9 = NVL(_Link_.LMCODNOR,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CNNORMA9 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])+'\'+cp_ToStr(_Link_.LMCODNOR,1)
      cp_ShowWarn(i_cKey,this.STU_NORM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNNORMA9 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CNNORM10
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_NORM_IDX,3]
    i_lTable = "STU_NORM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2], .t., this.STU_NORM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CNNORM10) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSLM_ACN',True,'STU_NORM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LMCODNOR like "+cp_ToStrODBC(trim(this.w_CNNORM10)+"%");

          i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LMCODNOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LMCODNOR',trim(this.w_CNNORM10))
          select LMCODNOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LMCODNOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CNNORM10)==trim(_Link_.LMCODNOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CNNORM10) and !this.bDontReportError
            deferred_cp_zoom('STU_NORM','*','LMCODNOR',cp_AbsName(oSource.parent,'oCNNORM10_1_19'),i_cWhere,'GSLM_ACN',"Elenco codici norma studio",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                     +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',oSource.xKey(1))
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CNNORM10)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODNOR";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODNOR="+cp_ToStrODBC(this.w_CNNORM10);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODNOR',this.w_CNNORM10)
            select LMCODNOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CNNORM10 = NVL(_Link_.LMCODNOR,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CNNORM10 = space(2)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_NORM_IDX,2])+'\'+cp_ToStr(_Link_.LMCODNOR,1)
      cp_ShowWarn(i_cKey,this.STU_NORM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CNNORM10 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCNNORMA1_1_2.value==this.w_CNNORMA1)
      this.oPgFrm.Page1.oPag.oCNNORMA1_1_2.value=this.w_CNNORMA1
    endif
    if not(this.oPgFrm.Page1.oPag.oCNNORMA2_1_4.value==this.w_CNNORMA2)
      this.oPgFrm.Page1.oPag.oCNNORMA2_1_4.value=this.w_CNNORMA2
    endif
    if not(this.oPgFrm.Page1.oPag.oCNNORMA3_1_6.value==this.w_CNNORMA3)
      this.oPgFrm.Page1.oPag.oCNNORMA3_1_6.value=this.w_CNNORMA3
    endif
    if not(this.oPgFrm.Page1.oPag.oCNNORMA4_1_8.value==this.w_CNNORMA4)
      this.oPgFrm.Page1.oPag.oCNNORMA4_1_8.value=this.w_CNNORMA4
    endif
    if not(this.oPgFrm.Page1.oPag.oCNNORMA5_1_10.value==this.w_CNNORMA5)
      this.oPgFrm.Page1.oPag.oCNNORMA5_1_10.value=this.w_CNNORMA5
    endif
    if not(this.oPgFrm.Page1.oPag.oCNNORMA6_1_12.value==this.w_CNNORMA6)
      this.oPgFrm.Page1.oPag.oCNNORMA6_1_12.value=this.w_CNNORMA6
    endif
    if not(this.oPgFrm.Page1.oPag.oCNNORMA7_1_14.value==this.w_CNNORMA7)
      this.oPgFrm.Page1.oPag.oCNNORMA7_1_14.value=this.w_CNNORMA7
    endif
    if not(this.oPgFrm.Page1.oPag.oCNNORMA8_1_16.value==this.w_CNNORMA8)
      this.oPgFrm.Page1.oPag.oCNNORMA8_1_16.value=this.w_CNNORMA8
    endif
    if not(this.oPgFrm.Page1.oPag.oCNNORMA9_1_17.value==this.w_CNNORMA9)
      this.oPgFrm.Page1.oPag.oCNNORMA9_1_17.value=this.w_CNNORMA9
    endif
    if not(this.oPgFrm.Page1.oPag.oCNNORM10_1_19.value==this.w_CNNORM10)
      this.oPgFrm.Page1.oPag.oCNNORM10_1_19.value=this.w_CNNORM10
    endif
    cp_SetControlsValueExtFlds(this,'COD_NORM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgslm_ancPag1 as StdContainer
  Width  = 772
  height = 474
  stdWidth  = 772
  stdheight = 474
  resizeXpos=429
  resizeYpos=468
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCNNORMA1_1_2 as StdField with uid="GOTIYOAPWS",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CNNORMA1", cQueryName = "CNNORMA1",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice norma uno",;
    HelpContextID = 116029015,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=10, Top=10, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="STU_NORM", cZoomOnZoom="GSLM_ACN", oKey_1_1="LMCODNOR", oKey_1_2="this.w_CNNORMA1"

  func oCNNORMA1_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCNNORMA1_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCNNORMA1_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STU_NORM','*','LMCODNOR',cp_AbsName(this.parent,'oCNNORMA1_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSLM_ACN',"Elenco codici norma studio",'',this.parent.oContained
  endproc
  proc oCNNORMA1_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSLM_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LMCODNOR=this.parent.oContained.w_CNNORMA1
     i_obj.ecpSave()
  endproc

  add object oCNNORMA2_1_4 as StdField with uid="LOHLVOBVNP",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CNNORMA2", cQueryName = "CNNORMA2",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice norma due",;
    HelpContextID = 116029016,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=10, Top=50, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="STU_NORM", cZoomOnZoom="GSLM_ACN", oKey_1_1="LMCODNOR", oKey_1_2="this.w_CNNORMA2"

  func oCNNORMA2_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCNNORMA2_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCNNORMA2_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STU_NORM','*','LMCODNOR',cp_AbsName(this.parent,'oCNNORMA2_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSLM_ACN',"Elenco codici norma studio",'',this.parent.oContained
  endproc
  proc oCNNORMA2_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSLM_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LMCODNOR=this.parent.oContained.w_CNNORMA2
     i_obj.ecpSave()
  endproc

  add object oCNNORMA3_1_6 as StdField with uid="GQGSCKDIBJ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CNNORMA3", cQueryName = "CNNORMA3",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice norma tre",;
    HelpContextID = 116029017,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=10, Top=110, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="STU_NORM", cZoomOnZoom="GSLM_ACN", oKey_1_1="LMCODNOR", oKey_1_2="this.w_CNNORMA3"

  func oCNNORMA3_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCNNORMA3_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCNNORMA3_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STU_NORM','*','LMCODNOR',cp_AbsName(this.parent,'oCNNORMA3_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSLM_ACN',"Elenco codici norma studio",'',this.parent.oContained
  endproc
  proc oCNNORMA3_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSLM_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LMCODNOR=this.parent.oContained.w_CNNORMA3
     i_obj.ecpSave()
  endproc

  add object oCNNORMA4_1_8 as StdField with uid="MVRKLBWWCC",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CNNORMA4", cQueryName = "CNNORMA4",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice norma quattro",;
    HelpContextID = 116029018,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=10, Top=164, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="STU_NORM", cZoomOnZoom="GSLM_ACN", oKey_1_1="LMCODNOR", oKey_1_2="this.w_CNNORMA4"

  func oCNNORMA4_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCNNORMA4_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCNNORMA4_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STU_NORM','*','LMCODNOR',cp_AbsName(this.parent,'oCNNORMA4_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSLM_ACN',"Elenco codici norma studio",'',this.parent.oContained
  endproc
  proc oCNNORMA4_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSLM_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LMCODNOR=this.parent.oContained.w_CNNORMA4
     i_obj.ecpSave()
  endproc

  add object oCNNORMA5_1_10 as StdField with uid="CXBFVYMQWS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CNNORMA5", cQueryName = "CNNORMA5",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice norma cinque",;
    HelpContextID = 116029019,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=10, Top=222, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="STU_NORM", cZoomOnZoom="GSLM_ACN", oKey_1_1="LMCODNOR", oKey_1_2="this.w_CNNORMA5"

  func oCNNORMA5_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCNNORMA5_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCNNORMA5_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STU_NORM','*','LMCODNOR',cp_AbsName(this.parent,'oCNNORMA5_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSLM_ACN',"Elenco codici norma studio",'',this.parent.oContained
  endproc
  proc oCNNORMA5_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSLM_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LMCODNOR=this.parent.oContained.w_CNNORMA5
     i_obj.ecpSave()
  endproc

  add object oCNNORMA6_1_12 as StdField with uid="KQZEIOLETT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CNNORMA6", cQueryName = "CNNORMA6",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice norma sei",;
    HelpContextID = 116029020,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=10, Top=270, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="STU_NORM", cZoomOnZoom="GSLM_ACN", oKey_1_1="LMCODNOR", oKey_1_2="this.w_CNNORMA6"

  func oCNNORMA6_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCNNORMA6_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCNNORMA6_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STU_NORM','*','LMCODNOR',cp_AbsName(this.parent,'oCNNORMA6_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSLM_ACN',"Elenco codici norma studio",'',this.parent.oContained
  endproc
  proc oCNNORMA6_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSLM_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LMCODNOR=this.parent.oContained.w_CNNORMA6
     i_obj.ecpSave()
  endproc

  add object oCNNORMA7_1_14 as StdField with uid="HINHLCZAYX",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CNNORMA7", cQueryName = "CNNORMA7",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice norma sette",;
    HelpContextID = 116029021,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=10, Top=312, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="STU_NORM", cZoomOnZoom="GSLM_ACN", oKey_1_1="LMCODNOR", oKey_1_2="this.w_CNNORMA7"

  func oCNNORMA7_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCNNORMA7_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCNNORMA7_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STU_NORM','*','LMCODNOR',cp_AbsName(this.parent,'oCNNORMA7_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSLM_ACN',"Elenco codici norma studio",'',this.parent.oContained
  endproc
  proc oCNNORMA7_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSLM_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LMCODNOR=this.parent.oContained.w_CNNORMA7
     i_obj.ecpSave()
  endproc

  add object oCNNORMA8_1_16 as StdField with uid="NNNXPFKQVN",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CNNORMA8", cQueryName = "CNNORMA8",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice norma otto",;
    HelpContextID = 116029022,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=10, Top=356, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="STU_NORM", cZoomOnZoom="GSLM_ACN", oKey_1_1="LMCODNOR", oKey_1_2="this.w_CNNORMA8"

  func oCNNORMA8_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCNNORMA8_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCNNORMA8_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STU_NORM','*','LMCODNOR',cp_AbsName(this.parent,'oCNNORMA8_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSLM_ACN',"Elenco codici norma studio",'',this.parent.oContained
  endproc
  proc oCNNORMA8_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSLM_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LMCODNOR=this.parent.oContained.w_CNNORMA8
     i_obj.ecpSave()
  endproc

  add object oCNNORMA9_1_17 as StdField with uid="JXDLCALIPQ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CNNORMA9", cQueryName = "CNNORMA9",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice norma nove",;
    HelpContextID = 116029023,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=10, Top=403, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="STU_NORM", cZoomOnZoom="GSLM_ACN", oKey_1_1="LMCODNOR", oKey_1_2="this.w_CNNORMA9"

  func oCNNORMA9_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCNNORMA9_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCNNORMA9_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STU_NORM','*','LMCODNOR',cp_AbsName(this.parent,'oCNNORMA9_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSLM_ACN',"Elenco codici norma studio",'',this.parent.oContained
  endproc
  proc oCNNORMA9_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSLM_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LMCODNOR=this.parent.oContained.w_CNNORMA9
     i_obj.ecpSave()
  endproc

  add object oCNNORM10_1_19 as StdField with uid="OWYLLXBXFG",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CNNORM10", cQueryName = "CNNORM10",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Codice norma dieci",;
    HelpContextID = 116029014,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=10, Top=440, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="STU_NORM", cZoomOnZoom="GSLM_ACN", oKey_1_1="LMCODNOR", oKey_1_2="this.w_CNNORM10"

  func oCNNORM10_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oCNNORM10_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCNNORM10_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'STU_NORM','*','LMCODNOR',cp_AbsName(this.parent,'oCNNORM10_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSLM_ACN',"Elenco codici norma studio",'',this.parent.oContained
  endproc
  proc oCNNORM10_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSLM_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LMCODNOR=this.parent.oContained.w_CNNORM10
     i_obj.ecpSave()
  endproc

  add object oStr_1_3 as StdString with uid="DZMWQMQEQT",Visible=.t., Left=57, Top=13,;
    Alignment=0, Width=204, Height=17,;
    Caption="IVA sospesa ex Art.7 D.L. 185/08 "  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="USRLNKWIWK",Visible=.t., Left=57, Top=55,;
    Alignment=0, Width=204, Height=17,;
    Caption="In/pg ft.ex A7 D.L. 185 anno doc."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_7 as StdString with uid="UZDEBMAZJP",Visible=.t., Left=57, Top=111,;
    Alignment=0, Width=204, Height=17,;
    Caption="In/pg ft.ex A7 D.L. 185 anno prec."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_9 as StdString with uid="KNSWCJWNRA",Visible=.t., Left=57, Top=169,;
    Alignment=0, Width=204, Height=17,;
    Caption="IVA esigibile decorso anno"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_11 as StdString with uid="PTMPRIITGC",Visible=.t., Left=57, Top=227,;
    Alignment=0, Width=204, Height=17,;
    Caption="IVA esig.dopo 1 anno ft.a.prec"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_13 as StdString with uid="ZHUVWIPBQT",Visible=.t., Left=57, Top=275,;
    Alignment=0, Width=204, Height=17,;
    Caption="In/pg ex A7 D.L. 185 anno pr.conc "  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="ZXWOWMDENN",Visible=.t., Left=57, Top=317,;
    Alignment=0, Width=204, Height=17,;
    Caption="In/pg exA7 D.L. 185 a.prec.p.conc"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="UAGHWZLRYD",Visible=.t., Left=57, Top=361,;
    Alignment=0, Width=204, Height=17,;
    Caption="Op. non considerare ai fini IVA"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_20 as StdString with uid="ZYAIUFMMSD",Visible=.t., Left=267, Top=43,;
    Alignment=0, Width=502, Height=18,;
    Caption="Da utilizzare per registrare gli incassi/pagamenti di fatture con IVA sospesa se l'anno di"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="EZOMFFMBHU",Visible=.t., Left=267, Top=99,;
    Alignment=0, Width=502, Height=18,;
    Caption="Da utilizzare per registrare gli incassi/pagamenti di fatture con IVA sospesa se l'anno di"  ;
  , bGlobalFont=.t.

  add object oStr_1_22 as StdString with uid="ZIZYMMTZWO",Visible=.t., Left=267, Top=153,;
    Alignment=0, Width=502, Height=18,;
    Caption="Da utilizzare per registrare gli incassi/pagamenti di fatture con IVA sospesa se l'anno di"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="MLOKVRFHRE",Visible=.t., Left=267, Top=212,;
    Alignment=0, Width=502, Height=18,;
    Caption="Da utilizzare per registrare gli incassi/pagamenti di fatture con IVA sospesa se l'anno di"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="AFGLWBQJEY",Visible=.t., Left=267, Top=268,;
    Alignment=0, Width=502, Height=18,;
    Caption="Da utilizzare per registrare gli incassi di fatture con IVA sospesa di soggetti che sono in "  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="QRZHSYWPAO",Visible=.t., Left=267, Top=309,;
    Alignment=0, Width=502, Height=18,;
    Caption="Da utilizzare per registrare gli incassi di fatture con IVA sospesa di soggetti che sono in "  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="LGEXEUUWPU",Visible=.t., Left=267, Top=356,;
    Alignment=0, Width=502, Height=18,;
    Caption="Da utilizzare per registrare nei documenti di incasso/pagamento tutte le righe che nella fattura originaria"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="YSQPCYIZXO",Visible=.t., Left=267, Top=12,;
    Alignment=0, Width=502, Height=18,;
    Caption="Da utilizzare per registrare le fatture di acquisto e di vendita con IVA sospesa."  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="KPPWWBAJNS",Visible=.t., Left=267, Top=242,;
    Alignment=0, Width=502, Height=18,;
    Caption="la data documento della fattura originaria e la data di registrazione dell'incasso/pagamento � trascorso pi� di un anno."  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="QFTNCYJTAX",Visible=.t., Left=267, Top=184,;
    Alignment=0, Width=502, Height=18,;
    Caption="la data documento della fattura originaria e la data di registrazione dell'incasso/pagamento � trascorso pi� di un anno."  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="SOXADUXSIU",Visible=.t., Left=268, Top=128,;
    Alignment=0, Width=502, Height=18,;
    Caption="la data documento della fattura originaria e la data di registrazione dell'incasso/pagamento � trascorso meno di un anno."  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="UNCYHAFNYA",Visible=.t., Left=267, Top=71,;
    Alignment=0, Width=502, Height=18,;
    Caption="la data documento della fattura originaria e la data di registrazione dell'incasso/pagamento � trascorso meno di un anno."  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="APJVSPCUMC",Visible=.t., Left=267, Top=57,;
    Alignment=0, Width=502, Height=18,;
    Caption="registrazione della fattura originaria � uguale all'anno di registrazione dell'incasso/pagamento e fra "  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="TTKIWLITNM",Visible=.t., Left=267, Top=113,;
    Alignment=0, Width=502, Height=18,;
    Caption="registrazione della fattura originaria � precedente all'anno di registrazione dell'incasso/pagamento e fra"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="XLRCAARAVW",Visible=.t., Left=267, Top=168,;
    Alignment=0, Width=502, Height=18,;
    Caption="registrazione della fattura originaria � uguale all'anno di registrazione dell'incasso/pagamento e fra "  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="EEUEPUZACV",Visible=.t., Left=267, Top=227,;
    Alignment=0, Width=502, Height=18,;
    Caption="registrazione della fattura originaria � precedente all'anno di registrazione dell'incasso/pagamento e fra"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="SUJSZAMMLB",Visible=.t., Left=267, Top=281,;
    Alignment=0, Width=502, Height=18,;
    Caption="procedura concorsuale, se l'anno di registrazione della fattura originaria � uguale all'anno di registrazione dell'incasso."  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="FEFRDTKNDF",Visible=.t., Left=267, Top=325,;
    Alignment=0, Width=502, Height=18,;
    Caption="procedura concorsuale, se l'anno di registrazione della fattura originaria � precedente all'anno di registrazione dell'incasso."  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="QWZMOFCLTV",Visible=.t., Left=267, Top=371,;
    Alignment=0, Width=502, Height=18,;
    Caption="presentavano un codice norma diverso da IS."  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="SNOTFKMUNM",Visible=.t., Left=57, Top=400,;
    Alignment=0, Width=204, Height=17,;
    Caption="Op. con iva parz. detraibile e sosp."  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_49 as StdString with uid="UVMRNAFVUA",Visible=.t., Left=267, Top=436,;
    Alignment=0, Width=502, Height=18,;
    Caption="Da utilizzare per registrare le fatture di acquisto e di vendita differite ai sensi dell'art. 21 c.4"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="CTVNHPVAWA",Visible=.t., Left=267, Top=452,;
    Alignment=0, Width=502, Height=18,;
    Caption="del DPR 633/72 con IVA sospesa"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="HFALHQBKCY",Visible=.t., Left=57, Top=445,;
    Alignment=0, Width=204, Height=17,;
    Caption="IVA sospesa ex Art.7 D.L. 185/08 "  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_53 as StdString with uid="YLFUKYGQRW",Visible=.t., Left=267, Top=398,;
    Alignment=0, Width=502, Height=18,;
    Caption="Da utilizzare per registrare nelle fatture di acquisto con iva parzialmente detraibile e sospesa ai sensi dell'art.7"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="HOONRBJWIL",Visible=.t., Left=267, Top=413,;
    Alignment=0, Width=502, Height=18,;
    Caption="D.L.185/08, la parte di imponibile e imposta totalmente indetraibile (codice norma con % indetraibilit� pari a 100)"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="AHRYTWSZJV",Visible=.t., Left=57, Top=415,;
    Alignment=0, Width=204, Height=17,;
    Caption="art.7 D.L.185/08"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_38 as StdBox with uid="JEHSZQOVLK",left=10, top=38, width=754,height=2

  add object oBox_1_39 as StdBox with uid="KWIDNZMNEM",left=10, top=93, width=754,height=2

  add object oBox_1_40 as StdBox with uid="IGTSNIOEQM",left=10, top=148, width=754,height=2

  add object oBox_1_41 as StdBox with uid="UYBODSAVST",left=10, top=205, width=754,height=2

  add object oBox_1_42 as StdBox with uid="ACWLXAJRXB",left=10, top=263, width=754,height=2

  add object oBox_1_43 as StdBox with uid="CPETYZNKYM",left=10, top=302, width=754,height=2

  add object oBox_1_44 as StdBox with uid="AHLBALPPDJ",left=10, top=347, width=754,height=2

  add object oBox_1_45 as StdBox with uid="LXDHYYLCUH",left=249, top=9, width=1,height=463

  add object oBox_1_47 as StdBox with uid="THOFLYVIRW",left=10, top=392, width=754,height=2

  add object oBox_1_51 as StdBox with uid="EHSHSQJDVP",left=10, top=432, width=754,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslm_anc','COD_NORM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CNCODTRA=COD_NORM.CNCODTRA";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
