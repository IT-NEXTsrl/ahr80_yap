* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm1bcn                                                        *
*              Export corrispettivi normali                                    *
*                                                                              *
*      Author: Zucchetti TAM                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][116][VRS_212]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-24                                                      *
* Last revis.: 2011-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm1bcn",oParentObject)
return(i_retval)

define class tgslm1bcn as StdBatch
  * --- Local variables
  w_REGNOVALIDA = .f.
  w_STRINGA = space(120)
  w_ACTUALPOS = 0
  w_PNSERIAL = space(10)
  w_OLDPNSERIAL = space(10)
  w_TOTDOC = 0
  w_ANCODSTU = space(6)
  w_APRNORM = space(2)
  w_APRCAU = space(3)
  w_IVASTU = space(2)
  w_IVASTUNOR = space(2)
  w_IVACEE = space(2)
  w_ESCI = .f.
  w_OKTRAS = .f.
  w_NOTRAS = 0
  w_PNCODCAU = space(5)
  w_IVCODIVA = space(5)
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_PNTIPCON = space(1)
  w_PNCODCON = space(15)
  w_TIPCAS = space(1)
  w_CODCAS = space(15)
  w_TOTCAS = 0
  w_CODATT = 0
  w_NUMREG = 0
  w_TIPOREG = space(1)
  w_PIUATTIV = space(1)
  w_CODSEZ = 0
  w_CODATTPR = space(5)
  w_FLAG3000EU = space(1)
  * --- WorkFile variables
  CONTI_idx=0
  PNT_DETT_idx=0
  STU_NORM_idx=0
  STU_PNTT_idx=0
  STU_TPNT_idx=0
  STU_TRAS_idx=0
  ATTIDETT_idx=0
  ATTIMAST_idx=0
  AZIENDA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- EXPORT CORRISPETTIVI NORMALI
    CREATE CURSOR MOVDACOR (PNSERIAL C(10), PNNUMRER N(6), ;
    PNDATREG D(8), PNCODCAU C(5), PNCOMPET C(4), PNTIPDOC C(2), ;
    PNNUMDOC N(6), PNALFDOC C(2), PNDATDOC D(8), PNNUMPRO N(6), ;
    PNALFPRO C(2), PNCODVAL C(3), PNVALNAZ C(3), PNTOTDOC N(18,4), ;
    PNTIPCLF C(1), PNCODCLF C(15), PNCOMIVA D(8), PNTIPREG C(1), ;
    PNNUMREG N(2), PNCAURIG C(5), PNDESRIG C(50), PNTIPCON C(1), ;
    PNIMPDAR N(18,4), PNCODCON C(15), PNIMPAVE N(18,4), ANTIPSOT C(1), CCFLPDIF C(1))
    this.w_OLDPNSERIAL = "  "
    * --- Leggo se l'azienda esercita piu' Attivita'
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZATTIVI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZATTIVI;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PIUATTIV = NVL(cp_ToDate(_read_.AZATTIVI),cp_NullValue(_read_.AZATTIVI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    SELECT CORRNORM
    GO TOP
    * --- Ciclo sul cursore dei corrispettivi normali
    do while NOT EOF()
      * --- Messaggio a schermo
      ah_Msg("Export corrispettivi con scorporo: reg. num. %1 - data %2",.t.,.f.,.f.,alltrim(STR(CORRNORM.PNNUMRER,6,0)),dtoc(CORRNORM.PNDATREG))
      * --- Scrittura su Log
      this.w_STRINGA = space(10)+ah_MsgFormat("Export corrispettivi con scorporo: reg. num. %1 - data %2",alltrim(STR(CORRNORM.PNNUMRER,6,0)),dtoc(CORRNORM.PNDATREG))
      FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
      FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
      * --- Salvo la posizione attuale del record e il seriale della registrazione
      this.w_ACTUALPOS = RECNO()
      this.w_PNSERIAL = CORRNORM.PNSERIAL
      this.w_TOTDOC = 0
      * --- Ciclo su questa registrazione
      this.w_ESCI = .F.
      this.w_OKTRAS = .T.
      do while CORRNORM.PNSERIAL=this.w_PNSERIAL AND NOT this.w_ESCI
        * --- Inizializzo la varibile di reg non valida
        this.w_REGNOVALIDA = .F.
        * --- Se � un rigo IVA lo butto giu
        * --- Incremento il numero di record trasferiti
        this.oParentObject.w_TRT = this.oParentObject.w_TRT+1
        this.oParentObject.w_CORRNORM = this.oParentObject.w_CORRNORM+1
        FWRITE(this.oParentObject.hFile,"D40",3)
        * --- Estraggo il codice attivit� dal registro iva usato sulla prima riga del castelletto IVA della Primanota
        this.w_TIPOREG = NVL(CORRNORM.IVTIPREG," ")
        this.w_NUMREG = NVL(CORRNORM.IVNUMREG,0)
        * --- Estraggo il codice attivit� contenente il tipo e numero registro IVA presenti in Primanota
        * --- Read from ATTIDETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ATTIDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ATCODATT,ATCODSEZ"+;
            " from "+i_cTable+" ATTIDETT where ";
                +"ATNUMREG = "+cp_ToStrODBC(this.w_NUMREG);
                +" and ATTIPREG = "+cp_ToStrODBC(this.w_TIPOREG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ATCODATT,ATCODSEZ;
            from (i_cTable) where;
                ATNUMREG = this.w_NUMREG;
                and ATTIPREG = this.w_TIPOREG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODATTPR = NVL(cp_ToDate(_read_.ATCODATT),cp_NullValue(_read_.ATCODATT))
          this.w_CODSEZ = NVL(cp_ToDate(_read_.ATCODSEZ),cp_NullValue(_read_.ATCODSEZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Leggo il codice attivit� studio associato all'attivit� 
        * --- Read from ATTIMAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ATTIMAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIMAST_idx,2],.t.,this.ATTIMAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ATATTIVA"+;
            " from "+i_cTable+" ATTIMAST where ";
                +"ATCODATT = "+cp_ToStrODBC(this.w_CODATTPR);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ATATTIVA;
            from (i_cTable) where;
                ATCODATT = this.w_CODATTPR;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODATT = NVL(cp_ToDate(_read_.ATATTIVA),cp_NullValue(_read_.ATATTIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_PIUATTIV # "S"
          * --- Se l'azienda non gestisce pi� attivit� assegno al codice attivit� '00'
          this.w_CODATT = 00
        endif
        * --- Codice attivit� IVA
        FWRITE (this.oParentObject.hFile ,right("00"+alltrim(str(this.w_CODATT)),2), 2)
        * --- Recupero e inserisco l' anno e il mese della registrazione
        FWRITE(this.oParentObject.hFile,LEFT(dtos(CORRNORM.PNDATREG),6),6)
        * --- Recupero e inserisco l' anno di competenza
        FWRITE(this.oParentObject.hFile,CORRNORM.PNCOMPET,4)
        * --- Recupero e inserisco il mese di competenza
        FWRITE(this.oParentObject.hFile,SUBSTR(dtos(CORRNORM.PNCOMIVA),5,2),2)
        * --- Sezione
        FWRITE(this.oParentObject.hFile,RIGHT("00"+ALLTRIM(STR(this.w_CODSEZ,2,0)),2),2)
        * --- Tipo Corrispettivo: se c'� il codice cliente, ricevuta fiscale, altrimenti scontrino
        FWRITE(this.oParentObject.hFile,IIF(EMPTY(NVL(CORRNORM.PNCODCLF,"")), "SC", "RF"),2)
        * --- Data registrazione
        FWRITE(this.oParentObject.hFile,dtos(CORRNORM.PNDATREG),8)
        * --- Recupero e scrittura del codice di Aliquota IVA
        this.w_APRCAU = SPACE(3)
        this.w_APRNORM = SPACE(2)
        this.w_IVASTU = SPACE(2)
        this.w_IVACEE = SPACE(2)
        this.w_PNCODCAU = CORRNORM.PNCODCAU
        this.w_IVCODIVA = CORRNORM.IVCODIVA
        * --- Read from STU_TRAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.STU_TRAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_TRAS_idx,2],.t.,this.STU_TRAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LMAPRNOR,LMAPRCAU,LMIVASTU,LMIVACEE"+;
            " from "+i_cTable+" STU_TRAS where ";
                +"LMCODICE = "+cp_ToStrODBC(this.oParentObject.w_ASSOCI);
                +" and LMHOCCAU = "+cp_ToStrODBC(this.w_PNCODCAU);
                +" and LMHOCIVA = "+cp_ToStrODBC(this.w_IVCODIVA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LMAPRNOR,LMAPRCAU,LMIVASTU,LMIVACEE;
            from (i_cTable) where;
                LMCODICE = this.oParentObject.w_ASSOCI;
                and LMHOCCAU = this.w_PNCODCAU;
                and LMHOCIVA = this.w_IVCODIVA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_APRNORM = NVL(cp_ToDate(_read_.LMAPRNOR),cp_NullValue(_read_.LMAPRNOR))
          this.w_APRCAU = NVL(cp_ToDate(_read_.LMAPRCAU),cp_NullValue(_read_.LMAPRCAU))
          this.w_IVASTU = NVL(cp_ToDate(_read_.LMIVASTU),cp_NullValue(_read_.LMIVASTU))
          this.w_IVACEE = NVL(cp_ToDate(_read_.LMIVACEE),cp_NullValue(_read_.LMIVACEE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT CORRNORM
        * --- Verifica se ho il codice IVA studio definito nella norma
        if NOT EMPTY(this.w_APRNORM)
          this.w_IVASTUNOR = SPACE(2)
          * --- Read from STU_NORM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.STU_NORM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_NORM_idx,2],.t.,this.STU_NORM_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LMCODIVA"+;
              " from "+i_cTable+" STU_NORM where ";
                  +"LMCODNOR = "+cp_ToStrODBC(this.w_APRNORM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LMCODIVA;
              from (i_cTable) where;
                  LMCODNOR = this.w_APRNORM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_IVASTUNOR = NVL(cp_ToDate(_read_.LMCODIVA),cp_NullValue(_read_.LMCODIVA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          SELECT CORRNORM
          if this.w_IVASTUNOR="XX"
            this.w_IVASTUNOR = this.w_IVASTU
          endif
        else
          this.w_IVASTUNOR = this.w_IVASTU
        endif
        * --- Controllo Codice IVA
        if (EMPTY(this.w_IVASTUNOR) or this.w_IVASTUNOR="XX")
          this.w_STRINGA = ah_MsgFormat("%1Errore! Codice IVA non definito","               ")
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        * --- Scrittura del codice IVA
        FWRITE(this.oParentObject.hFile,(this.w_IVASTUNOR),2)
        * --- Importo totale
        this.w_TOTDOC = CORRNORM.IVIMPONI+CORRNORM.IVIMPIVA
        if g_PERVAL=this.oParentObject.w_VALEUR
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(INT(ABS(this.w_TOTDOC)),11,0))+RIGHT(STR(this.w_TOTDOC,12,2),2),11),11)
        else
          FWRITE(this.oParentObject.hFile,RIGHT("00000000000"+ALLTRIM(STR(ABS(this.w_TOTDOC),11,0)),11),11)
        endif
        * --- Segno
        FWRITE(this.oParentObject.hFile,IIF(this.w_TOTDOC>=0, "+", "-"),1)
        * --- Numero documento dal al 
        FWRITE(this.oParentObject.hFile,"000000000000",12)
        * --- Scrittura del codice norma
        FWRITE(this.oParentObject.hFile,IIF(EMPTY(this.w_APRNORM),"  ",this.w_APRNORM),2)
        * --- Codice centro di costo
        FWRITE(this.oParentObject.hFile,"0000",4)
        * --- Contropartita (Sottoconto)
        this.w_ANCODSTU = SPACE(6)
        this.w_PNTIPCLF = CORRNORM.IVTIPCOP
        this.w_PNCODCLF = CORRNORM.IVCONTRO
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODSTU"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_PNTIPCLF);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_PNCODCLF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODSTU;
            from (i_cTable) where;
                ANTIPCON = this.w_PNTIPCLF;
                and ANCODICE = this.w_PNCODCLF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT CORRNORM
        if EMPTY(this.w_ANCODSTU)
          this.w_ANCODSTU = "000000"
          this.w_STRINGA = ah_MsgFormat("%1Errore! Contropartita %2 non ha un codice sottoconto nello studio associato","               ",NVL(this.w_PNCODCLF," "))
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        FWRITE(this.oParentObject.hFile,right("000000"+alltrim(this.w_ANCODSTU),6),6)
        * --- Codice centro di costo
        FWRITE(this.oParentObject.hFile,"0000",4)
        * --- Codice Cassa
        if CORRNORM.PNTIPCLF="N"
          * --- Mappo il codice cassa e lo scrivo
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Scrivo su file
          FWRITE(this.oParentObject.hFile,RIGHT("000000"+alltrim(this.w_ANCODSTU),6),6)
        else
          * --- Scrivo su file
          FWRITE(this.oParentObject.hFile, "000000" ,6)
        endif
        * --- Codice CEE
        FWRITE(this.oParentObject.hFile,"  ",2)
        * --- Flag a calendario
        FWRITE(this.oParentObject.hFile," ",1)
        * --- Codice Cliente
        FWRITE(this.oParentObject.hFile, left (NVL (CORRNORM.ANCODSTU," ") +space (6) ,6) , 6)
        * --- Filler
        this.oParentObject.w_FILLER = SPACE(33)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,33)
        * --- AIFT - gestioni 3000 euro 
        *     Se non c' � AIFT il campo � sbiancato
        *     Se non c' e intestatario si imposta CodTipoFatt3000Euro='E' anche se non c � AIFT
        *     Negli altri casi si imposta in base ai valori assunti dai campi OSFLGEXT e OSTIPOPE 
        *     - P = Contratto corrispettivi periodici 
        *     - F = Corrispettivo incluso forzatamente 
        *     - E = Corrispettivo escluso forzatamente 
        *     - Blk = Corrispettivo non classificato con alcuno dei valori precedenti 
        if g_AIFT ="S" AND NOT EMPTY ( NVL (CORRNORM.PNCODCLF ," ")) 
          this.w_FLAG3000EU = IIF ( NVL (CORRNORM.OSFLGEXT," ") = "I" , "F" , IIF ( NVL (CORRNORM.OSFLGEXT," ") = "F" , "E" , IIF ( NVL (CORRNORM.OSTIPOPE," ") = "P" , "P" , SPACE(1) ) ) )
          FWRITE(this.oParentObject.hFile, this.w_FLAG3000EU ,1)
        else
          if EMPTY ( NVL (CORRNORM.PNCODCLF ," ")) 
            FWRITE(this.oParentObject.hFile, "E" , 1)
          else
            FWRITE(this.oParentObject.hFile, "F" , 1)
          endif
        endif
        * --- Codice contratto
        if g_AIFT="S"
          FWRITE(this.oParentObject.hFile, left ( NVL(CORRNORM.OSRIFCON," ") + space(30),30) , 30)
        else
          this.oParentObject.w_FILLER = SPACE(30)
          FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,30)
        endif
        this.oParentObject.w_FILLER = SPACE(50)
        FWRITE(this.oParentObject.hFile,this.oParentObject.w_FILLER,50)
        if this.w_REGNOVALIDA
          go this.w_ACTUALPOS
          this.w_NOTRAS = -1
          * --- Insert into STU_PNTT
          i_nConn=i_TableProp[this.STU_PNTT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NOTRAS),'STU_PNTT','LMNUMTRA');
            +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.w_NOTRAS,'LMNUMTR2',0)
            insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
               values (;
                 this.w_PNSERIAL;
                 ,this.w_NOTRAS;
                 ,0;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error='GSLM_BCN: Scrittura con errore in STU_PNTT'
            return
          endif
          SELECT CORRNORM
          do while CORRNORM.PNSERIAL=this.w_PNSERIAL
            * --- Avanzo il puntatore
            skip 1
          enddo
          this.w_ESCI = .T.
          this.w_OKTRAS = .F.
        endif
        if not this.w_ESCI
          skip 1
        endif
      enddo
      if this.w_OKTRAS
        * --- Insert into STU_PNTT
        i_nConn=i_TableProp[this.STU_PNTT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STU_PNTT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PNTT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LMSERIAL"+",LMNUMTRA"+",LMNUMTR2"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'STU_PNTT','LMSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PROFIL),'STU_PNTT','LMNUMTRA');
          +","+cp_NullLink(cp_ToStrODBC(0),'STU_PNTT','LMNUMTR2');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LMSERIAL',this.w_PNSERIAL,'LMNUMTRA',this.oParentObject.w_PROFIL,'LMNUMTR2',0)
          insert into (i_cTable) (LMSERIAL,LMNUMTRA,LMNUMTR2 &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.oParentObject.w_PROFIL;
               ,0;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error='GSLM_BCN: Scrittura in STU_PNTT'
          return
        endif
        SELECT CORRNORM
      endif
    enddo
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cerco le righe del dettaglio che contengono un conto di tipo cassa, banca, ...
    vq_exec("..\LEMC\EXE\QUERY\GSLM1COC.VQR",this,"CONTICAS")
    SELECT CONTICAS
    GO TOP
    this.w_ANCODSTU = "000000"
    if RECCOUNT("CONTICAS") > 0
      if RECCOUNT("CONTICAS") = 1
        this.w_ANCODSTU = SPACE(6)
        this.w_TIPCAS = "G"
        this.w_CODCAS = CONTICAS.PNCODCON
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODSTU"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCAS);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCAS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODSTU;
            from (i_cTable) where;
                ANTIPCON = this.w_TIPCAS;
                and ANCODICE = this.w_CODCAS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT CORRNORM
        if EMPTY(this.w_ANCODSTU)
          this.w_STRINGA = ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","              ",NVL(this.w_PNCODCON," "))
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
      else
        this.w_TIPCAS = "G"
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANCODSTU"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCAS);
                +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CASFIT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANCODSTU;
            from (i_cTable) where;
                ANTIPCON = this.w_TIPCAS;
                and ANCODICE = this.oParentObject.w_CASFIT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ANCODSTU = NVL(cp_ToDate(_read_.ANCODSTU),cp_NullValue(_read_.ANCODSTU))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if EMPTY(this.w_ANCODSTU)
          this.w_STRINGA = ah_MsgFormat("%1Errore! Conto %2 non associato nello studio","               ",NVL(this.oParentObject.w_CASFIT," "))
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
        if NOT EMPTY(this.oParentObject.w_CAUGIR)
          if this.w_OLDPNSERIAL <> this.w_PNSERIAL
            * --- Il calcolo viene fatto una volta sola per ogni registrazione di Primanota. Questo  per
            *     evitare la duplicazione dei records per ogni registrazione contabile.
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_OLDPNSERIAL = this.w_PNSERIAL
          endif
        else
          this.w_STRINGA = ah_MsgFormat("%1Errore! Causale giroconto non definita nella tabella trasferimento studio","               ")
          FWRITE(this.oParentObject.hLOG,this.w_STRINGA,LEN(this.w_STRINGA))
          FWRITE(this.oParentObject.hLOG,CHR(13)+CHR(10),2)
          this.oParentObject.w_ERRORE = .T.
        endif
      endif
    endif
    SELECT CONTICAS
    USE
    SELECT CORRNORM
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CREAZIONE MOVIMENTI CONTABILI
    this.w_TOTCAS = 0
    SELECT CONTICAS
    do while NOT EOF()
      * --- Movimenti relativi ai conti di tipo cassa,  banca, ...
      INSERT INTO MOVDACOR (PNSERIAL, PNNUMRER, PNDATREG, PNCODCAU, ;
      PNCOMPET , PNTIPDOC, PNNUMDOC, PNALFDOC, PNDATDOC, PNNUMPRO, ;
      PNALFPRO, PNCODVAL, PNVALNAZ, PNTOTDOC, PNTIPCLF, PNCODCLF, ;
      PNCOMIVA, PNTIPREG, PNNUMREG, PNCAURIG, PNDESRIG, PNTIPCON, ;
      PNIMPDAR, PNCODCON, PNIMPAVE, ANTIPSOT, CCFLPDIF) VALUES ;
      (CORRNORM.PNSERIAL, CORRNORM.PNNUMRER, CORRNORM.PNDATREG, this.oParentObject.w_CAUGIR, CORRNORM.PNCOMPET, "NO", ;
      NVL(CORRNORM.PNNUMDOC,0), NVL(CORRNORM.PNALFDOC," "), NVL(CORRNORM.PNDATDOC,CTOD("  -  -  ")), NVL(CORRNORM.PNNUMPRO,0), ;
      NVL(CORRNORM.PNALFPRO," "), NVL(CORRNORM.PNCODVAL," "), NVL(CORRNORM.PNVALNAZ," "), NVL(CORRNORM.PNTOTDOC,0), ;
      CORRNORM.PNTIPCLF, NVL(CORRNORM.PNCODCLF," "), NVL(CORRNORM.PNCOMIVA,CTOD("  -  -  ")), "N", 0, this.oParentObject.w_CAUGIR, ;
      " ", CONTICAS.PNTIPCON, CONTICAS.PNIMPDAR, CONTICAS.PNCODCON, CONTICAS.PNIMPAVE, CONTICAS.ANTIPSOT, CORRNORM.CCFLPDIF)
      this.w_TOTCAS = CONTICAS.PNIMPDAR-CONTICAS.PNIMPAVE+this.w_TOTCAS
      skip 1
    enddo
    * --- Movimento relativo al Sottoconto Studio Cassa Fittizia
    INSERT INTO MOVDACOR (PNSERIAL, PNNUMRER, PNDATREG, PNCODCAU, ;
    PNCOMPET , PNTIPDOC, PNNUMDOC, PNALFDOC, PNDATDOC, PNNUMPRO, ;
    PNALFPRO, PNCODVAL, PNVALNAZ, PNTOTDOC, PNTIPCLF, PNCODCLF, ;
    PNCOMIVA, PNTIPREG, PNNUMREG, PNCAURIG, PNDESRIG, PNTIPCON, ;
    PNIMPDAR, PNCODCON, PNIMPAVE, ANTIPSOT, CCFLPDIF) VALUES ;
    (CORRNORM.PNSERIAL, CORRNORM.PNNUMRER, CORRNORM.PNDATREG, this.oParentObject.w_CAUGIR, CORRNORM.PNCOMPET, "NO", ;
    NVL(CORRNORM.PNNUMDOC,0), NVL(CORRNORM.PNALFDOC," "), NVL(CORRNORM.PNDATDOC,CTOD("  -  -  ")), NVL(CORRNORM.PNNUMPRO,0), ;
    NVL(CORRNORM.PNALFPRO," "), NVL(CORRNORM.PNCODVAL," "), NVL(CORRNORM.PNVALNAZ," "), NVL(CORRNORM.PNTOTDOC,0), ;
    CORRNORM.PNTIPCLF, NVL(CORRNORM.PNCODCLF," "), NVL(CORRNORM.PNCOMIVA,CTOD("  -  -  ")), "N", 0, this.oParentObject.w_CAUGIR, ;
    " ", "G", 0, this.oParentObject.w_CASFIT, this.w_TOTCAS, "X", CORRNORM.CCFLPDIF)
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='PNT_DETT'
    this.cWorkTables[3]='STU_NORM'
    this.cWorkTables[4]='STU_PNTT'
    this.cWorkTables[5]='STU_TPNT'
    this.cWorkTables[6]='STU_TRAS'
    this.cWorkTables[7]='ATTIDETT'
    this.cWorkTables[8]='ATTIMAST'
    this.cWorkTables[9]='AZIENDA'
    return(this.OpenAllTables(9))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
