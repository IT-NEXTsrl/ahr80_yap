* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm_kim                                                        *
*              Import da studio                                                *
*                                                                              *
*      Author: Zucchetti SPA                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161][VRS_96]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-21                                                      *
* Last revis.: 2018-05-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gslm_kim
* Esco se il modulo non � attivo
IF LMDISACTIVATE()
  do cplu_erm with "La funzione � attiva con il Modulo in modalit� Semplificata o Avanzata"
  return
ENDIF

* --- Fine Area Manuale
return(createobject("tgslm_kim",oParentObject))

* --- Class definition
define class tgslm_kim as StdForm
  Top    = 53
  Left   = 84

  * --- Standard Properties
  Width  = 456
  Height = 319+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-05-29"
  HelpContextID=1459095
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=23

  * --- Constant Properties
  _IDX = 0
  LINGUE_IDX = 0
  MASTRI_IDX = 0
  CACOCLFO_IDX = 0
  STU_PARA_IDX = 0
  cPrg = "gslm_kim"
  cComment = "Import da studio"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PIACON = space(1)
  o_PIACON = space(1)
  w_AZIENDA = space(5)
  w_CLIFOR = space(1)
  o_CLIFOR = space(1)
  w_CAUCON = space(1)
  o_CAUCON = space(1)
  w_MOVCON = space(1)
  o_MOVCON = space(1)
  w_CODPIA = space(4)
  w_IMPCLIFOR = space(1)
  w_IMPPIACON = space(1)
  w_IMPCAUCON = space(1)
  w_IMPMOVCON = space(1)
  w_FILENAME = space(255)
  o_FILENAME = space(255)
  w_AGGDATI = space(1)
  w_CLICATCON = space(5)
  w_CLICODLIN = space(3)
  w_CLICONSUP = space(15)
  w_FORCATCON = space(5)
  w_FORCODLIN = space(3)
  w_FORCONSUP = space(15)
  w_CLITIPMAS = space(1)
  w_CLINUMLIV = 0
  w_FORTIPMAS = space(1)
  w_FORNUMLIV = 0
  w_FILENAME1 = space(255)
  w_BTNREP = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgslm_kimPag1","gslm_kim",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni import")
      .Pages(2).addobject("oPag","tgslm_kimPag2","gslm_kim",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Campi obbligatori")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPIACON_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_BTNREP = this.oPgFrm.Pages(1).oPag.BTNREP
    DoDefault()
    proc Destroy()
      this.w_BTNREP = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='LINGUE'
    this.cWorkTables[2]='MASTRI'
    this.cWorkTables[3]='CACOCLFO'
    this.cWorkTables[4]='STU_PARA'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PIACON=space(1)
      .w_AZIENDA=space(5)
      .w_CLIFOR=space(1)
      .w_CAUCON=space(1)
      .w_MOVCON=space(1)
      .w_CODPIA=space(4)
      .w_IMPCLIFOR=space(1)
      .w_IMPPIACON=space(1)
      .w_IMPCAUCON=space(1)
      .w_IMPMOVCON=space(1)
      .w_FILENAME=space(255)
      .w_AGGDATI=space(1)
      .w_CLICATCON=space(5)
      .w_CLICODLIN=space(3)
      .w_CLICONSUP=space(15)
      .w_FORCATCON=space(5)
      .w_FORCODLIN=space(3)
      .w_FORCONSUP=space(15)
      .w_CLITIPMAS=space(1)
      .w_CLINUMLIV=0
      .w_FORTIPMAS=space(1)
      .w_FORNUMLIV=0
      .w_FILENAME1=space(255)
        .w_PIACON = 'N'
        .w_AZIENDA = i_codazi
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_AZIENDA))
          .link_1_2('Full')
        endif
        .w_CLIFOR = 'N'
        .w_CAUCON = 'N'
        .w_MOVCON = 'N'
          .DoRTCalc(6,6,.f.)
        .w_IMPCLIFOR = .w_CLIFOR
        .w_IMPPIACON = .w_PIACON
        .w_IMPCAUCON = .w_CAUCON
        .w_IMPMOVCON = .w_MOVCON
          .DoRTCalc(11,11,.f.)
        .w_AGGDATI = 'N'
        .w_CLICATCON = SPACE(5)
        .DoRTCalc(13,13,.f.)
        if not(empty(.w_CLICATCON))
          .link_2_1('Full')
        endif
        .w_CLICODLIN = SPACE(3)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CLICODLIN))
          .link_2_2('Full')
        endif
        .w_CLICONSUP = SPACE(15)
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CLICONSUP))
          .link_2_3('Full')
        endif
        .w_FORCATCON = SPACE(5)
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_FORCATCON))
          .link_2_8('Full')
        endif
        .w_FORCODLIN = SPACE(3)
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_FORCODLIN))
          .link_2_9('Full')
        endif
        .w_FORCONSUP = SPACE(15)
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_FORCONSUP))
          .link_2_10('Full')
        endif
          .DoRTCalc(19,22,.f.)
        .w_FILENAME1 = .w_FILENAME
      .oPgFrm.Page1.oPag.BTNREP.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_20.Calculate(ah_MsgFormat("Questa procedura permette di importare dati provenienti dal programma per%0commercialisti Zucchetti."))
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate(ah_MsgFormat("Nota per import pdc: viene creato o modificato il pdc il cui codice � identico a%0quello riportato in 'cod. pdc'."))
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate(ah_MsgFormat("E' obbligatoria la presenza del codice relativo al piano%0dei conti usato, � necessario selezionare il tipo di dati che si desidera importare."))
      .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        .DoRTCalc(3,6,.t.)
        if .o_CLIFOR<>.w_CLIFOR
            .w_IMPCLIFOR = .w_CLIFOR
        endif
        if .o_PIACON<>.w_PIACON
            .w_IMPPIACON = .w_PIACON
        endif
        if .o_CAUCON<>.w_CAUCON
            .w_IMPCAUCON = .w_CAUCON
        endif
        if .o_MOVCON<>.w_MOVCON
            .w_IMPMOVCON = .w_MOVCON
        endif
        .DoRTCalc(11,22,.t.)
        if .o_FILENAME<>.w_FILENAME
            .w_FILENAME1 = .w_FILENAME
        endif
        .oPgFrm.Page1.oPag.BTNREP.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(ah_MsgFormat("Questa procedura permette di importare dati provenienti dal programma per%0commercialisti Zucchetti."))
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(ah_MsgFormat("Nota per import pdc: viene creato o modificato il pdc il cui codice � identico a%0quello riportato in 'cod. pdc'."))
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(ah_MsgFormat("E' obbligatoria la presenza del codice relativo al piano%0dei conti usato, � necessario selezionare il tipo di dati che si desidera importare."))
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.BTNREP.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_20.Calculate(ah_MsgFormat("Questa procedura permette di importare dati provenienti dal programma per%0commercialisti Zucchetti."))
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate(ah_MsgFormat("Nota per import pdc: viene creato o modificato il pdc il cui codice � identico a%0quello riportato in 'cod. pdc'."))
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate(ah_MsgFormat("E' obbligatoria la presenza del codice relativo al piano%0dei conti usato, � necessario selezionare il tipo di dati che si desidera importare."))
        .oPgFrm.Page1.oPag.oObj_1_23.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_25.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCAUCON_1_4.enabled = this.oPgFrm.Page1.oPag.oCAUCON_1_4.mCond()
    this.oPgFrm.Page1.oPag.oMOVCON_1_5.enabled = this.oPgFrm.Page1.oPag.oMOVCON_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODPIA_1_7.visible=!this.oPgFrm.Page1.oPag.oCODPIA_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.BTNREP.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_20.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_23.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_25.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- gslm_kim
    If cevent='Elabora'
       if g_TRAEXP='A'
         This.Notifyevent("Cogen")
       endif
       if g_TRAEXP='C'
         This.Notifyevent("Contb")
       endif
       if g_TRAEXP='G'
         This.Notifyevent("Movago")
       endif
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.STU_PARA_IDX,3]
    i_lTable = "STU_PARA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.STU_PARA_IDX,2], .t., this.STU_PARA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.STU_PARA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LMCODAZI,LMCODPDC";
                   +" from "+i_cTable+" "+i_lTable+" where LMCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LMCODAZI',this.w_AZIENDA)
            select LMCODAZI,LMCODPDC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.LMCODAZI,space(5))
      this.w_CODPIA = NVL(_Link_.LMCODPDC,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(5)
      endif
      this.w_CODPIA = space(4)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.STU_PARA_IDX,2])+'\'+cp_ToStr(_Link_.LMCODAZI,1)
      cp_ShowWarn(i_cKey,this.STU_PARA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLICATCON
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLICATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_CLICATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_CLICATCON))
          select C2CODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLICATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLICATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oCLICATCON_2_1'),i_cWhere,'GSAR_AC2',"Categorie contabili cli/for",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLICATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_CLICATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_CLICATCON)
            select C2CODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLICATCON = NVL(_Link_.C2CODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CLICATCON = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLICATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLICODLIN
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLICODLIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALG',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_CLICODLIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_CLICODLIN))
          select LUCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLICODLIN)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLICODLIN) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'oCLICODLIN_2_2'),i_cWhere,'GSAR_ALG',"Lingue",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLICODLIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_CLICODLIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_CLICODLIN)
            select LUCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLICODLIN = NVL(_Link_.LUCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_CLICODLIN = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLICODLIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLICONSUP
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLICONSUP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_CLICONSUP)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCTIPMAS,MCNUMLIV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_CLICONSUP))
          select MCCODICE,MCTIPMAS,MCNUMLIV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLICONSUP)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLICONSUP) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oCLICONSUP_2_3'),i_cWhere,'GSAR_AMC',"Mastri contabili (generici)",'GSAR_APC.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCTIPMAS,MCNUMLIV";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCTIPMAS,MCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLICONSUP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCTIPMAS,MCNUMLIV";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_CLICONSUP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_CLICONSUP)
            select MCCODICE,MCTIPMAS,MCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLICONSUP = NVL(_Link_.MCCODICE,space(15))
      this.w_CLITIPMAS = NVL(_Link_.MCTIPMAS,space(1))
      this.w_CLINUMLIV = NVL(_Link_.MCNUMLIV,0)
    else
      if i_cCtrl<>'Load'
        this.w_CLICONSUP = space(15)
      endif
      this.w_CLITIPMAS = space(1)
      this.w_CLINUMLIV = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CLITIPMAS='C' AND .w_CLINUMLIV=1
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il mastro di raggruppamento deve essere di tipo cliente e di livello 1")
        endif
        this.w_CLICONSUP = space(15)
        this.w_CLITIPMAS = space(1)
        this.w_CLINUMLIV = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLICONSUP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORCATCON
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORCATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_FORCATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_FORCATCON))
          select C2CODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORCATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FORCATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oFORCATCON_2_8'),i_cWhere,'GSAR_AC2',"Categorie contabili cli/for",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORCATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_FORCATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_FORCATCON)
            select C2CODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORCATCON = NVL(_Link_.C2CODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_FORCATCON = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORCATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORCODLIN
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORCODLIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALG',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_FORCODLIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_FORCODLIN))
          select LUCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORCODLIN)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FORCODLIN) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'oFORCODLIN_2_9'),i_cWhere,'GSAR_ALG',"Lingue",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORCODLIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_FORCODLIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_FORCODLIN)
            select LUCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORCODLIN = NVL(_Link_.LUCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_FORCODLIN = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORCODLIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FORCONSUP
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FORCONSUP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_FORCONSUP)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCTIPMAS,MCNUMLIV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_FORCONSUP))
          select MCCODICE,MCTIPMAS,MCNUMLIV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FORCONSUP)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_FORCONSUP) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oFORCONSUP_2_10'),i_cWhere,'GSAR_AMC',"Mastri contabili (generici)",'GSAR_APF.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCTIPMAS,MCNUMLIV";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCTIPMAS,MCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FORCONSUP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCTIPMAS,MCNUMLIV";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_FORCONSUP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_FORCONSUP)
            select MCCODICE,MCTIPMAS,MCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FORCONSUP = NVL(_Link_.MCCODICE,space(15))
      this.w_FORTIPMAS = NVL(_Link_.MCTIPMAS,space(1))
      this.w_FORNUMLIV = NVL(_Link_.MCNUMLIV,0)
    else
      if i_cCtrl<>'Load'
        this.w_FORCONSUP = space(15)
      endif
      this.w_FORTIPMAS = space(1)
      this.w_FORNUMLIV = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FORTIPMAS='F' AND .w_FORNUMLIV=1
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il mastro di raggruppamento deve essere di tipo fornitore e di livello 1")
        endif
        this.w_FORCONSUP = space(15)
        this.w_FORTIPMAS = space(1)
        this.w_FORNUMLIV = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FORCONSUP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPIACON_1_1.RadioValue()==this.w_PIACON)
      this.oPgFrm.Page1.oPag.oPIACON_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLIFOR_1_3.RadioValue()==this.w_CLIFOR)
      this.oPgFrm.Page1.oPag.oCLIFOR_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUCON_1_4.RadioValue()==this.w_CAUCON)
      this.oPgFrm.Page1.oPag.oCAUCON_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMOVCON_1_5.RadioValue()==this.w_MOVCON)
      this.oPgFrm.Page1.oPag.oMOVCON_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPIA_1_7.value==this.w_CODPIA)
      this.oPgFrm.Page1.oPag.oCODPIA_1_7.value=this.w_CODPIA
    endif
    if not(this.oPgFrm.Page1.oPag.oAGGDATI_1_13.RadioValue()==this.w_AGGDATI)
      this.oPgFrm.Page1.oPag.oAGGDATI_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCLICATCON_2_1.value==this.w_CLICATCON)
      this.oPgFrm.Page2.oPag.oCLICATCON_2_1.value=this.w_CLICATCON
    endif
    if not(this.oPgFrm.Page2.oPag.oCLICODLIN_2_2.value==this.w_CLICODLIN)
      this.oPgFrm.Page2.oPag.oCLICODLIN_2_2.value=this.w_CLICODLIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCLICONSUP_2_3.value==this.w_CLICONSUP)
      this.oPgFrm.Page2.oPag.oCLICONSUP_2_3.value=this.w_CLICONSUP
    endif
    if not(this.oPgFrm.Page2.oPag.oFORCATCON_2_8.value==this.w_FORCATCON)
      this.oPgFrm.Page2.oPag.oFORCATCON_2_8.value=this.w_FORCATCON
    endif
    if not(this.oPgFrm.Page2.oPag.oFORCODLIN_2_9.value==this.w_FORCODLIN)
      this.oPgFrm.Page2.oPag.oFORCODLIN_2_9.value=this.w_FORCODLIN
    endif
    if not(this.oPgFrm.Page2.oPag.oFORCONSUP_2_10.value==this.w_FORCONSUP)
      this.oPgFrm.Page2.oPag.oFORCONSUP_2_10.value=this.w_FORCONSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oFILENAME1_1_14.value==this.w_FILENAME1)
      this.oPgFrm.Page1.oPag.oFILENAME1_1_14.value=this.w_FILENAME1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(! empty(.w_CODPIA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPIACON_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Opzione non disponibile con codice piano dei conti vuoto")
          case   not(.w_CLITIPMAS='C' AND .w_CLINUMLIV=1)  and not(empty(.w_CLICONSUP))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCLICONSUP_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il mastro di raggruppamento deve essere di tipo cliente e di livello 1")
          case   not(.w_FORTIPMAS='F' AND .w_FORNUMLIV=1)  and not(empty(.w_FORCONSUP))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFORCONSUP_2_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il mastro di raggruppamento deve essere di tipo fornitore e di livello 1")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PIACON = this.w_PIACON
    this.o_CLIFOR = this.w_CLIFOR
    this.o_CAUCON = this.w_CAUCON
    this.o_MOVCON = this.w_MOVCON
    this.o_FILENAME = this.w_FILENAME
    return

enddefine

* --- Define pages as container
define class tgslm_kimPag1 as StdContainer
  Width  = 452
  height = 319
  stdWidth  = 452
  stdheight = 319
  resizeXpos=208
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPIACON_1_1 as StdCheck with uid="ZZUNDZJFMU",rtseq=1,rtrep=.f.,left=11, top=143, caption="Piano dei conti",;
    ToolTipText = "Se attivo importa il piano dei conti dello studio in ad hoc",;
    HelpContextID = 213016074,;
    cFormVar="w_PIACON", bObbl = .f. , nPag = 1;
    ,sErrorMsg = "Opzione non disponibile con codice piano dei conti vuoto";
   , bGlobalFont=.t.


  func oPIACON_1_1.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPIACON_1_1.GetRadio()
    this.Parent.oContained.w_PIACON = this.RadioValue()
    return .t.
  endfunc

  func oPIACON_1_1.SetRadio()
    this.Parent.oContained.w_PIACON=trim(this.Parent.oContained.w_PIACON)
    this.value = ;
      iif(this.Parent.oContained.w_PIACON=='S',1,;
      0)
  endfunc

  func oPIACON_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (! empty(.w_CODPIA))
    endwith
    return bRes
  endfunc

  add object oCLIFOR_1_3 as StdCheck with uid="WCSHXVILID",rtseq=3,rtrep=.f.,left=11, top=166, caption="Clienti/fornitori",;
    ToolTipText = "Se attivo importa i clienti e i fornitori presenti nello studio",;
    HelpContextID = 145677274,;
    cFormVar="w_CLIFOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCLIFOR_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCLIFOR_1_3.GetRadio()
    this.Parent.oContained.w_CLIFOR = this.RadioValue()
    return .t.
  endfunc

  func oCLIFOR_1_3.SetRadio()
    this.Parent.oContained.w_CLIFOR=trim(this.Parent.oContained.w_CLIFOR)
    this.value = ;
      iif(this.Parent.oContained.w_CLIFOR=='S',1,;
      0)
  endfunc

  add object oCAUCON_1_4 as StdCheck with uid="JAEZFTMKHS",rtseq=4,rtrep=.f.,left=11, top=189, caption="Causali contabili",;
    ToolTipText = "Se attivo importa le causali contabili dello studio",;
    HelpContextID = 212936410,;
    cFormVar="w_CAUCON", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCAUCON_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oCAUCON_1_4.GetRadio()
    this.Parent.oContained.w_CAUCON = this.RadioValue()
    return .t.
  endfunc

  func oCAUCON_1_4.SetRadio()
    this.Parent.oContained.w_CAUCON=trim(this.Parent.oContained.w_CAUCON)
    this.value = ;
      iif(this.Parent.oContained.w_CAUCON=='S',1,;
      0)
  endfunc

  func oCAUCON_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LMAVANZATO())
    endwith
   endif
  endfunc

  add object oMOVCON_1_5 as StdCheck with uid="HIJLFCMIWV",rtseq=5,rtrep=.f.,left=11, top=212, caption="Movimenti contabili",;
    ToolTipText = "Se attivo importa i movimenti contabili dallo studio",;
    HelpContextID = 212928570,;
    cFormVar="w_MOVCON", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oMOVCON_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oMOVCON_1_5.GetRadio()
    this.Parent.oContained.w_MOVCON = this.RadioValue()
    return .t.
  endfunc

  func oMOVCON_1_5.SetRadio()
    this.Parent.oContained.w_MOVCON=trim(this.Parent.oContained.w_MOVCON)
    this.value = ;
      iif(this.Parent.oContained.w_MOVCON=='S',1,;
      0)
  endfunc

  func oMOVCON_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (LMAVANZATO())
    endwith
   endif
  endfunc


  add object oBtn_1_6 as StdButton with uid="UAJUDWBCRV",left=352, top=274, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 1487846;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      this.parent.oContained.NotifyEvent("Elabora")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((! empty(.w_CODPIA) or .w_PIACON<>'S') and (.w_CLIFOR='S' or .w_PIACON='S' or .w_CAUCON='S' or .w_MOVCON='S'))
      endwith
    endif
  endfunc

  add object oCODPIA_1_7 as StdField with uid="PSXAUKZOOS",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODPIA", cQueryName = "CODPIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 168110298,;
   bGlobalFont=.t.,;
    Height=21, Width=45, Left=310, Top=149, InputMask=replicate('X',4)

  func oCODPIA_1_7.mHide()
    with this.Parent.oContained
      return (g_TRAEXP='G')
    endwith
  endfunc

  add object oAGGDATI_1_13 as StdCheck with uid="TYWUVDGHIA",rtseq=12,rtrep=.f.,left=299, top=195, caption="Aggiorna dati",;
    ToolTipText = "Se attivo vengono aggiornati i dati gi� presenti",;
    HelpContextID = 141491974,;
    cFormVar="w_AGGDATI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAGGDATI_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAGGDATI_1_13.GetRadio()
    this.Parent.oContained.w_AGGDATI = this.RadioValue()
    return .t.
  endfunc

  func oAGGDATI_1_13.SetRadio()
    this.Parent.oContained.w_AGGDATI=trim(this.Parent.oContained.w_AGGDATI)
    this.value = ;
      iif(this.Parent.oContained.w_AGGDATI=='S',1,;
      0)
  endfunc

  add object oFILENAME1_1_14 as StdField with uid="LEMEBGUMIJ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_FILENAME1", cQueryName = "FILENAME1",;
    bObbl = .f. , nPag = 1, value=space(255), bMultilanguage =  .f.,;
    ToolTipText = "File di import",;
    HelpContextID = 104879275,;
   bGlobalFont=.t.,;
    Height=21, Width=379, Left=47, Top=244, InputMask=replicate('X',255)


  add object BTNREP as cp_askfile with uid="ISHGMEOMJK",left=431, top=246, width=19,height=18,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_FILENAME",;
    nPag=1;
    , ToolTipText = "Premere per selezionare il file di import";
    , HelpContextID = 1660118


  add object oBtn_1_17 as StdButton with uid="BZSNAJZXHL",left=402, top=274, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 8776518;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_20 as cp_calclbl with uid="PNADPZEBES",left=3, top=7, width=446,height=37,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 88978714


  add object oObj_1_21 as cp_calclbl with uid="CZSQYLRWPY",left=3, top=85, width=446,height=51,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 88978714


  add object oObj_1_22 as cp_calclbl with uid="FNBUUSSHBM",left=3, top=45, width=446,height=39,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",;
    nPag=1;
    , HelpContextID = 88978714


  add object oObj_1_23 as cp_runprogram with uid="FQWRCQHDCS",left=545, top=208, width=156,height=22,;
    caption='GSLM_BIM',;
   bGlobalFont=.t.,;
    prg="GSLM_BIM",;
    cEvent = "Cogen",;
    nPag=1;
    , HelpContextID = 140008371


  add object oObj_1_24 as cp_runprogram with uid="QAOSRCUXOQ",left=545, top=229, width=156,height=22,;
    caption='GSLM1BIM',;
   bGlobalFont=.t.,;
    prg="GSLM1BIM",;
    cEvent = "Contb",;
    nPag=1;
    , HelpContextID = 91773875


  add object oObj_1_25 as cp_runprogram with uid="JHWQQXVUFF",left=545, top=250, width=156,height=22,;
    caption='GSLMABIM',;
   bGlobalFont=.t.,;
    prg="GSLMABIM",;
    cEvent = "Movago",;
    nPag=1;
    , HelpContextID = 108551091

  add object oStr_1_16 as StdString with uid="NELXOIUDDN",Visible=.t., Left=210, Top=149,;
    Alignment=1, Width=95, Height=18,;
    Caption="Cod. pdc:"  ;
  , bGlobalFont=.t.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (g_TRAEXP='G')
    endwith
  endfunc

  add object oStr_1_19 as StdString with uid="VRSELSQYKM",Visible=.t., Left=5, Top=244,;
    Alignment=1, Width=41, Height=18,;
    Caption="Path:"  ;
  , bGlobalFont=.t.

  add object oBox_1_18 as StdBox with uid="JYYYGWMIVC",left=2, top=138, width=449,height=2
enddefine
define class tgslm_kimPag2 as StdContainer
  Width  = 452
  height = 319
  stdWidth  = 452
  stdheight = 319
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCLICATCON_2_1 as StdField with uid="KGKUKLRXVG",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CLICATCON", cQueryName = "CLICATCON",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 126998187,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=203, Top=39, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_CLICATCON"

  func oCLICATCON_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLICATCON_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLICATCON_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oCLICATCON_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili cli/for",'',this.parent.oContained
  endproc
  proc oCLICATCON_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_CLICATCON
     i_obj.ecpSave()
  endproc

  add object oCLICODLIN_2_2 as StdField with uid="XWKGJIILNH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CLICODLIN", cQueryName = "CLICODLIN",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 156117327,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=203, Top=64, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", cZoomOnZoom="GSAR_ALG", oKey_1_1="LUCODICE", oKey_1_2="this.w_CLICODLIN"

  func oCLICODLIN_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLICODLIN_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLICODLIN_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'oCLICODLIN_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALG',"Lingue",'',this.parent.oContained
  endproc
  proc oCLICODLIN_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LUCODICE=this.parent.oContained.w_CLICODLIN
     i_obj.ecpSave()
  endproc

  add object oCLICONSUP_2_3 as StdField with uid="BZOPQWWGSN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CLICONSUP", cQueryName = "CLICONSUP",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il mastro di raggruppamento deve essere di tipo cliente e di livello 1",;
    HelpContextID = 212981381,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=203, Top=89, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_CLICONSUP"

  func oCLICONSUP_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLICONSUP_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLICONSUP_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oCLICONSUP_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri contabili (generici)",'GSAR_APC.MASTRI_VZM',this.parent.oContained
  endproc
  proc oCLICONSUP_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_CLICONSUP
     i_obj.ecpSave()
  endproc

  add object oFORCATCON_2_8 as StdField with uid="PPFTQPGSKF",rtseq=16,rtrep=.f.,;
    cFormVar = "w_FORCATCON", cQueryName = "FORCATCON",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 126960507,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=203, Top=151, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_FORCATCON"

  func oFORCATCON_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORCATCON_2_8.ecpDrop(oSource)
    this.Parent.oContained.link_2_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORCATCON_2_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oFORCATCON_2_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili cli/for",'',this.parent.oContained
  endproc
  proc oFORCATCON_2_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_FORCATCON
     i_obj.ecpSave()
  endproc

  add object oFORCODLIN_2_9 as StdField with uid="YWDLQPNQVU",rtseq=17,rtrep=.f.,;
    cFormVar = "w_FORCODLIN", cQueryName = "FORCODLIN",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 156155007,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=203, Top=176, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", cZoomOnZoom="GSAR_ALG", oKey_1_1="LUCODICE", oKey_1_2="this.w_FORCODLIN"

  func oFORCODLIN_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORCODLIN_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORCODLIN_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'oFORCODLIN_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALG',"Lingue",'',this.parent.oContained
  endproc
  proc oFORCODLIN_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LUCODICE=this.parent.oContained.w_FORCODLIN
     i_obj.ecpSave()
  endproc

  add object oFORCONSUP_2_10 as StdField with uid="OWKPFUHIFG",rtseq=18,rtrep=.f.,;
    cFormVar = "w_FORCONSUP", cQueryName = "FORCONSUP",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il mastro di raggruppamento deve essere di tipo fornitore e di livello 1",;
    HelpContextID = 212943701,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=203, Top=201, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_FORCONSUP"

  func oFORCONSUP_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oFORCONSUP_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFORCONSUP_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oFORCONSUP_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri contabili (generici)",'GSAR_APF.MASTRI_VZM',this.parent.oContained
  endproc
  proc oFORCONSUP_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_FORCONSUP
     i_obj.ecpSave()
  endproc

  add object oStr_2_4 as StdString with uid="MPBVBFNUGR",Visible=.t., Left=13, Top=39,;
    Alignment=1, Width=185, Height=18,;
    Caption="Categoria contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="HLDHODCFAR",Visible=.t., Left=13, Top=64,;
    Alignment=1, Width=185, Height=18,;
    Caption="Codice lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="CLQHUXYAYG",Visible=.t., Left=13, Top=89,;
    Alignment=1, Width=185, Height=18,;
    Caption="Mastro di raggruppamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="ICNIWQRZTM",Visible=.t., Left=5, Top=12,;
    Alignment=0, Width=215, Height=18,;
    Caption="Campi obbligatori per i clienti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_11 as StdString with uid="YZUYBMIDIV",Visible=.t., Left=13, Top=151,;
    Alignment=1, Width=185, Height=18,;
    Caption="Categoria contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="DBJDMVZIYY",Visible=.t., Left=13, Top=176,;
    Alignment=1, Width=185, Height=18,;
    Caption="Codice lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="JQNOBIKZOS",Visible=.t., Left=13, Top=201,;
    Alignment=1, Width=185, Height=18,;
    Caption="Mastro di raggruppamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_14 as StdString with uid="JXORUGBCZX",Visible=.t., Left=5, Top=124,;
    Alignment=0, Width=234, Height=18,;
    Caption="Campi obbligatori per i fornitori"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_2_15 as StdBox with uid="PPRQQQXYRS",left=4, top=31, width=445,height=2

  add object oBox_2_16 as StdBox with uid="NOLYGTIMFS",left=4, top=143, width=445,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gslm_kim','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
