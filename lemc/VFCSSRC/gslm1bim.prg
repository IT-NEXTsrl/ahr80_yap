* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gslm1bim                                                        *
*              Import da studio                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][116][VRS_526]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-21                                                      *
* Last revis.: 2000-06-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gslm1bim
public w_IMPCLIFOR,w_IMPPIACON,w_IMPCAUCON,w_GO
* - Init delle varabili - *
w_IMPCLIFOR = .F.
w_IMPPIACON = .F.
w_IMPCAUCON = .F.
w_GO=.F.
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgslm1bim",oParentObject)
return(i_retval)

define class tgslm1bim as StdBatch
  * --- Local variables
  w_CODAZI = space(5)
  hFile = 0
  ch = space(1)
  LUNRECORD = 0
  w_STATOCLIFOR = 0
  w_STATOPIACON = 0
  w_STATOCAUCON = 0
  w_STATOSOTCON = 0
  w_STATOMOVCON = 0
  w_MESS1 = space(200)
  w_MESS2 = space(200)
  w_MESS3 = space(200)
  w_MESS4 = space(200)
  w_STR1 = space(200)
  w_STR2 = space(200)
  w_STR3 = space(200)
  w_STR4 = space(200)
  w_FILELOG = space(50)
  w_oMess = .NULL.
  w_COMODO = space(10)
  w_IDENT = space(1)
  w_DITTA = space(6)
  w_RAGSOC1 = space(25)
  w_RAGSOC2 = space(25)
  w_CODFISOK = space(1)
  w_FIELDSK = space(200)
  w_VIA = space(32)
  w_NUMCIV = space(5)
  w_PARIVAOK = space(1)
  w_ALLEGATO = space(1)
  w_BOLLA = space(1)
  w_CODSOTLUNG = space(2)
  w_CODSOT = 0
  w_TIPCON = space(1)
  w_ANTIPCON = space(1)
  w_ANCODICE = space(15)
  w_ANDESCRI = space(40)
  w_ANDESCR2 = space(40)
  w_ANINDIRI = space(35)
  w_AN___CAP = space(8)
  w_ANLOCALI = space(30)
  w_ANPROVIN = space(2)
  w_ANPERFIS = space(1)
  w_ANCODFIS = space(16)
  w_ANPARIVA = space(12)
  w_ANCATCON = space(5)
  w_ANCODLIN = space(3)
  w_ANCONSUP = space(15)
  w_ANFLINTR = space(1)
  w_ANCODSTU = space(6)
  w_ANFLESIG = space(1)
  w_ANPARTSN = space(1)
  w_AN_SESSO = space(1)
  w_ANTIPFAT = space(1)
  w_ANBOLFAT = space(1)
  w_ANPREBOL = space(1)
  w_ANSCORPO = space(1)
  w_ANFLGAVV = space(1)
  w_ANCONCON = space(1)
  w_ANFLCONA = space(1)
  w_ANRITENU = space(1)
  w_ANTIPCLF = space(1)
  w_ANGESCON = space(1)
  w_ANCODSOG = space(8)
  w_ANCODCAT = space(4)
  w_ANCOGNOM = space(20)
  w_AN__NOME = space(20)
  w_RAGSOC = space(50)
  w_LMCODCON = space(3)
  w_LMDESCON = space(30)
  w_LMPROCON = space(8)
  w_LMMINCON = space(5)
  w_LMMAXCON = space(5)
  w_LMTIPCON = space(1)
  w_LMCONCLI = space(1)
  w_LMCONORD = space(1)
  w_LMCODSOT = space(5)
  w_LMDESSOT = space(30)
  w_LMDABSOT = space(10)
  w_LMTIPSOT = space(1)
  w_LMCAQSOT = space(1)
  w_LMCARSOT = space(1)
  w_LMPEISOT = 0
  w_LMCODCAU = space(3)
  w_LMDESCAU = space(29)
  w_LMINFDAR = space(6)
  w_LMSUPDAR = space(6)
  w_LMINFAVE = space(6)
  w_LMSUPAVE = space(6)
  w_TPEISOT = space(5)
  * --- WorkFile variables
  CAU_CONT_idx=0
  CONTI_idx=0
  MASTRI_idx=0
  STUMPIAC_idx=0
  STU_CAUS_idx=0
  STU_PARA_idx=0
  STU_PIAC_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definizione variabili per vedere cosa importare
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_CODAZI = i_CODAZI
    * --- Handle del file di Import
    * --- Dimensione del record
    this.LUNRECORD = 200
    * --- Stato do import
    * --- Lancio finestra di GetFile()
    if NOT EMPTY(this.oParentObject.w_FILENAME1)
      w_IMPFILE=this.oParentObject.w_FILENAME1
    else
      w_IMPFILE=GetFile("D","File di Import","Conferma",0,"Seleziona File di Import")
    endif
    if .not. empty(w_IMPFILE)
      * --- Apertura file di import
      this.hFile = FOPEN(w_IMPFILE,0)
      if (this.hFile<>-1)
        * --- Lettura del Piano dei Conti
        this.w_STATOPIACON = 2
        this.w_STATOSOTCON = 2
        if this.oParentObject.w_IMPPIACON="S"
          this.Pag3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Lettura dei clienti e fornitori
        this.w_STATOCLIFOR = 2
        if this.oParentObject.w_IMPCLIFOR="S"
          FSEEK(this.hFile,0,0)
          vq_exec("..\LEMC\EXE\QUERY\GSLM_CON.VQR",this,"CURCONTI")
          this.Pag5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Lettura delle Causali contabili
        this.w_STATOCAUCON = 2
        if this.oParentObject.w_IMPCAUCON="S"
          FSEEK(this.hFile,0,0)
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Lettura Movimenti contabili
        this.w_STATOMOVCON = 2
        if this.oParentObject.w_IMPMOVCON="S"
          FSEEK(this.hFile,0,0)
          do GSLM1BMO with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_STATOMOVCON<>1
            * --- Lancio la finestra di errore
            this.w_FILELOG = SYS(5)+SYS(2003)+"\"+iif(g_TRAEXP $ "C-B","CONTB","COGEN")+"\LOG00000.TMP"
            do gslm_kir with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        * --- Chiusura del File
        FCLOSE(this.hFile)
        * --- Messaggio sullo stato dell'import
        if (this.w_STATOCLIFOR=1 .and. this.w_STATOPIACON=1 .and. this.w_STATOCAUCON=1 .and. this.w_STATOMOVCON=1)
          this.w_MESS1 = "Import da studio eseguito con successo"
          ah_ErrorMsg(this.w_MESS1,,"")
        else
          this.w_oMess=createobject("AH_Message")
          * --- Preparazione del messaggio da visualizzare
          do case
            case this.w_STATOPIACON=0
              this.w_oMess.AddMsgPartNL("Import piano dei conti fallito")     
            case this.w_STATOPIACON=1
              this.w_oMess.AddMsgPartNL("Import piano dei conti eseguito con successo")     
            otherwise
              this.w_oMess.AddMsgPartNL("Import piano dei conti non eseguito")     
          endcase
          do case
            case this.w_STATOCLIFOR=0
              this.w_oMess.AddMsgPartNL("Import cli/for fallito")     
            case this.w_STATOCLIFOR=1
              this.w_oMess.AddMsgPartNL("Import cli/for eseguito con successo")     
            otherwise
              this.w_oMess.AddMsgPartNL("Import cli/for non eseguito")     
          endcase
          do case
            case this.w_STATOCAUCON=0
              this.w_oMess.AddMsgPartNL("Import causali contabili fallito")     
            case this.w_STATOCAUCON=1
              this.w_oMess.AddMsgPartNL("Import causali contabili eseguito con successo")     
            otherwise
              this.w_oMess.AddMsgPartNL("Import causali contabili non eseguito")     
          endcase
          do case
            case this.w_STATOMOVCON=0
              this.w_oMess.AddMsgPartNL("Import movimenti contabili fallito")     
            case this.w_STATOMOVCON=1
              this.w_oMess.AddMsgPartNL("Import movimenti contabili eseguito con successo")     
            otherwise
              this.w_oMess.AddMsgPartNL("Import movimenti contabili non eseguito")     
          endcase
          this.w_oMess.ah_ErrorMsg()     
        endif
      else
        this.w_MESS1 = "Impossibile aprire il file di import"
        ah_ErrorMsg(this.w_MESS1,"","")
      endif
    endif
    this.Pag7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Campi della tabella CONTI
    this.w_AN_SESSO = "M"
    this.w_ANSCORPO = "N"
    this.w_ANFLGAVV = "N"
    this.w_ANCONCON = IIF(g_ISONAZ="ITA","E","1")
    this.w_ANCATCON = SPACE(5)
    this.w_ANCODLIN = SPACE(3)
    this.w_ANCONSUP = SPACE(15)
    this.w_ANFLESIG = "N"
    this.w_ANPARTSN = g_PERPAR
    this.w_ANCOGNOM = space(20)
    this.w_AN__NOME = space(20)
    this.w_ANDESCR2 = space(40)
    * --- Campi della tabella PIANO DEI CONTI
    * --- MASTER
    this.w_LMCODCON = SPACE(3)
    this.w_LMDESCON = SPACE(30)
    this.w_LMMINCON = SPACE(5)
    this.w_LMMAXCON = SPACE(5)
    this.w_LMTIPCON = SPACE(1)
    this.w_LMCONCLI = SPACE(1)
    this.w_LMCONORD = SPACE(1)
    * --- DETAIL
    this.w_LMCODSOT = SPACE(5)
    this.w_LMDESSOT = SPACE(30)
    this.w_LMTIPSOT = SPACE(1)
    this.w_LMCAQSOT = SPACE(1)
    this.w_LMCARSOT = SPACE(1)
    this.w_LMPEISOT = 0
    * --- Campi della tabella CAUSALI CONTABILI
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LETTURA DEL PIANO DEI CONTI
    * --- Mi posiziono all'inizio dei record che identificano i conti
    this.w_COMODO = FREAD(this.hFile,9)
    FSEEK(this.hFile,-9,1)
    do while ((this.w_COMODO<>"FPIACONTI") .and. (.not. FEOF(this.hFile)))
      * --- Skippo un record
      FSEEK(this.hFile,this.LUNRECORD,1)
      * --- Rileggo il codice
      this.w_COMODO = FREAD(this.hFile,9)
      FSEEK(this.hFile,-9,1)
    enddo
    if this.w_COMODO<>"FPIACONTI"
      this.w_STATOPIACON = 0
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      return
    endif
    * --- Skippo la testata
    FSEEK(this.hFile,this.LUNRECORD,1)
    * --- Inizio lettura dei conti dello studio
    this.w_COMODO = FREAD(this.hFile,9)
    FSEEK(this.hFile,-9,1)
    do while this.w_COMODO<>"EPIACONTI"
      * --- Leggo il primo campo
      this.w_COMODO = FREAD(this.hFile,3)
      if this.w_COMODO<>"D19"
        * --- Ritorno in quanto c'� un errore
        this.w_STATOPIACON = 0
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        return
      endif
      * --- Leggo il codice del conto
      this.w_LMCODCON = FREAD(this.hFile,3)
      * --- Messaggio a Video
      this.w_STR1 = "Import mastro %1"
      ah_Msg(this.w_STR1,.T.,.F.,.F.,this.w_LMCODCON)
      * --- Leggo il progressivo del conto
      this.w_LMPROCON = FREAD(this.hFile,8)
      * --- Leggo la descrizione del conto
      this.w_LMDESCON = FREAD(this.hFile,30)
      * --- Leggo il sottoconto limite inferiore
      this.w_LMMINCON = FREAD(this.hFile,6)
      * --- Leggo il sottoconto limite superiore
      this.w_LMMAXCON = FREAD(this.hFile,6)
      * --- Leggo il tipo conto
      this.w_LMTIPCON = FREAD(this.hFile,1)
      * --- Leggo il conto d'ordine
      this.w_LMCONORD = FREAD(this.hFile,1)
      * --- Leggo il conto cli\for\altro
      this.w_LMCONCLI = FREAD(this.hFile,1)
      this.w_LMCONCLI = IIF(EMPTY(this.w_LMCONCLI), "G", this.w_LMCONCLI)
      * --- Leggo il campo lasciato a spazio
      this.w_FIELDSK = FREAD(this.hFile,141)
      * --- SCRITTURA TABELLA PIANO DEI CONTI (MASTER)
      * --- Try
      local bErr_03916338
      bErr_03916338=bTrsErr
      this.Try_03916338()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        if this.oParentObject.w_AGGDATI="S"
          * --- Try
          local bErr_0391AE38
          bErr_0391AE38=bTrsErr
          this.Try_0391AE38()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_0391AE38
          * --- End
        endif
      endif
      bTrsErr=bTrsErr or bErr_03916338
      * --- End
      * --- Leggo il prossimo codice
      this.w_COMODO = FREAD(this.hFile,9)
      FSEEK(this.hFile,-9,1)
    enddo
    * --- Skippo la coda dei conti
    FSEEK(this.hFile,this.LUNRECORD,1)
    * --- Lettura dei sottoconti
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Stato dell'operazione
    this.w_STATOPIACON=this.w_STATOSOTCON
  endproc
  proc Try_03916338()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STUMPIAC
    i_nConn=i_TableProp[this.STUMPIAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STUMPIAC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STUMPIAC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMCODCON"+",LMPROGRE"+",LMDESCON"+",LMMINCON"+",LMMAXCON"+",LMTIPCON"+",LMCONCLI"+",LMCONORD"+",LMCODPIA"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_LMCODCON),'STUMPIAC','LMCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMPROCON),'STUMPIAC','LMPROGRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMDESCON),'STUMPIAC','LMDESCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMMINCON),'STUMPIAC','LMMINCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMMAXCON),'STUMPIAC','LMMAXCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMTIPCON),'STUMPIAC','LMTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMCONCLI),'STUMPIAC','LMCONCLI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMCONORD),'STUMPIAC','LMCONORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODPIA),'STUMPIAC','LMCODPIA');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMCODCON',this.w_LMCODCON,'LMPROGRE',this.w_LMPROCON,'LMDESCON',this.w_LMDESCON,'LMMINCON',this.w_LMMINCON,'LMMAXCON',this.w_LMMAXCON,'LMTIPCON',this.w_LMTIPCON,'LMCONCLI',this.w_LMCONCLI,'LMCONORD',this.w_LMCONORD,'LMCODPIA',this.oParentObject.w_CODPIA)
      insert into (i_cTable) (LMCODCON,LMPROGRE,LMDESCON,LMMINCON,LMMAXCON,LMTIPCON,LMCONCLI,LMCONORD,LMCODPIA &i_ccchkf. );
         values (;
           this.w_LMCODCON;
           ,this.w_LMPROCON;
           ,this.w_LMDESCON;
           ,this.w_LMMINCON;
           ,this.w_LMMAXCON;
           ,this.w_LMTIPCON;
           ,this.w_LMCONCLI;
           ,this.w_LMCONORD;
           ,this.oParentObject.w_CODPIA;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Fallito inserimento nella tabella Piano dei Conti (Master)'
      return
    endif
    return
  proc Try_0391AE38()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into STUMPIAC
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.STUMPIAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STUMPIAC_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.STUMPIAC_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"LMDESCON ="+cp_NullLink(cp_ToStrODBC(this.w_LMDESCON),'STUMPIAC','LMDESCON');
      +",LMMINCON ="+cp_NullLink(cp_ToStrODBC(this.w_LMMINCON),'STUMPIAC','LMMINCON');
      +",LMMAXCON ="+cp_NullLink(cp_ToStrODBC(this.w_LMMAXCON),'STUMPIAC','LMMAXCON');
      +",LMTIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_LMTIPCON),'STUMPIAC','LMTIPCON');
      +",LMCONCLI ="+cp_NullLink(cp_ToStrODBC(this.w_LMCONCLI),'STUMPIAC','LMCONCLI');
      +",LMCONORD ="+cp_NullLink(cp_ToStrODBC(this.w_LMCONORD),'STUMPIAC','LMCONORD');
      +",LMCODPIA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODPIA),'STUMPIAC','LMCODPIA');
          +i_ccchkf ;
      +" where ";
          +"LMCODCON = "+cp_ToStrODBC(this.w_LMCODCON);
          +" and LMPROGRE = "+cp_ToStrODBC(this.w_LMPROCON);
             )
    else
      update (i_cTable) set;
          LMDESCON = this.w_LMDESCON;
          ,LMMINCON = this.w_LMMINCON;
          ,LMMAXCON = this.w_LMMAXCON;
          ,LMTIPCON = this.w_LMTIPCON;
          ,LMCONCLI = this.w_LMCONCLI;
          ,LMCONORD = this.w_LMCONORD;
          ,LMCODPIA = this.oParentObject.w_CODPIA;
          &i_ccchkf. ;
       where;
          LMCODCON = this.w_LMCODCON;
          and LMPROGRE = this.w_LMPROCON;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Fallita la scrittura nella Tabella Piano dei Conti (Master)'
      return
    endif
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LETTURA DEI SOTTOCONTI
    vq_exec("..\LEMC\EXE\QUERY\GSLM_ISC.VQR",this,"CONTISTUDIO")
    * --- Mi posiziono all'inizio dei record che identificano i sottoconti
    this.w_COMODO = FREAD(this.hFile,9)
    FSEEK(this.hFile,-9,1)
    do while ((this.w_COMODO<>"FDESCONTI") .and. (.not. FEOF(this.hFile)))
      * --- Skippo un record
      FSEEK(this.hFile,this.LUNRECORD,1)
      * --- Rileggo il codice
      this.w_COMODO = FREAD(this.hFile,9)
      FSEEK(this.hFile,-9,1)
    enddo
    if this.w_COMODO<>"FDESCONTI"
      this.w_STATOSOTCON = 0
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      return
    endif
    * --- Skippo la testata
    FSEEK(this.hFile,this.LUNRECORD,1)
    * --- Inizio lettura dei conti dello studio
    this.w_COMODO = FREAD(this.hFile,9)
    FSEEK(this.hFile,-9,1)
    do while this.w_COMODO<>"EDESCONTI"
      * --- Leggo il primo campo
      this.w_COMODO = FREAD(this.hFile,3)
      if this.w_COMODO<>"D20"
        * --- Ritorno in quanto c'� un errore
        this.w_STATOSOTCON = 0
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        return
      endif
      * --- Leggo il codice sottoconto
      this.w_LMCODSOT = FREAD(this.hFile,6)
      * --- Messaggio a Video
      this.w_STR1 = "Import conto %1"
      ah_Msg(this.w_STR1,.T.,.F.,.F.,this.w_LMCODSOT)
      * --- Leggo la descrizione del sottoconto
      this.w_LMDESSOT = FREAD(this.hFile,30)
      * --- Leggo la descrizione abbreviata del sottoconto
      this.w_LMDABSOT = FREAD(this.hFile,10)
      * --- Leggo il tipo sottoconto
      this.w_LMTIPSOT = FREAD(this.hFile,1)
      * --- Leggo check acquisti per ventilazione
      this.w_LMCAQSOT = FREAD(this.hFile,1)
      * --- Leggo check acquisti destinati alla rivendita
      this.w_LMCARSOT = FREAD(this.hFile,1)
      * --- Leggo percentuale indeducibilit�
      this.w_TPEISOT = FREAD(this.hFile,5)
      if Not Empty( this.w_TPEISOT )
        * --- 5 caratteri i primi 3 parte intera, gli ultimi due parte decimale
        this.w_LMPEISOT = Val( Left( this.w_TPEISOT, 3) ) + ( Val( Right( this.w_TPEISOT, 2) )/100 )
      else
        this.w_LMPEISOT = 0
      endif
      * --- Leggo il campo lasciato a spazio
      this.w_FIELDSK = FREAD(this.hFile,143)
      * --- SCRITTURA TABELLA PIANO DEI CONTI (DETAIL)
      SELECT CONTISTUDIO
      GO TOP
      * --- Ricerca del Conto con l'intervallo dei Sottoconti congruente con il Sottoconto che voglio inserire
      LOCATE FOR (VAL(CONTISTUDIO.LMMINCON)<=VAL(this.w_LMCODSOT) AND VAL(CONTISTUDIO.LMMAXCON)>=VAL(this.w_LMCODSOT) AND CONTISTUDIO.LMCODPIA=this.oParentObject.w_CODPIA)
      if FOUND()
        this.w_LMCODCON = CONTISTUDIO.LMCODCON
        this.w_LMPROCON = CONTISTUDIO.LMPROGRE
        * --- Try
        local bErr_03964CC8
        bErr_03964CC8=bTrsErr
        this.Try_03964CC8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          if this.oParentObject.w_AGGDATI="S"
            * --- Try
            local bErr_03966DF8
            bErr_03966DF8=bTrsErr
            this.Try_03966DF8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_03966DF8
            * --- End
          endif
        endif
        bTrsErr=bTrsErr or bErr_03964CC8
        * --- End
      else
        * --- Ritorno in quanto c'� un errore
        this.w_STATOSOTCON = 0
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        return
      endif
      * --- Leggo il prossimo codice
      this.w_COMODO = FREAD(this.hFile,9)
      FSEEK(this.hFile,-9,1)
    enddo
    * --- Import Sottoconti corretto
    this.w_STATOSOTCON=1
  endproc
  proc Try_03964CC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_PIAC
    i_nConn=i_TableProp[this.STU_PIAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PIAC_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_PIAC_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMCODCON"+",LMPROGRE"+",LMCODSOT"+",LMDESSOT"+",LMTIPSOT"+",LMDESABB"+",LMACQVEN"+",LMACQRIV"+",LMPERIND"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_LMCODCON),'STU_PIAC','LMCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMPROCON),'STU_PIAC','LMPROGRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMCODSOT),'STU_PIAC','LMCODSOT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMDESSOT),'STU_PIAC','LMDESSOT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMTIPSOT),'STU_PIAC','LMTIPSOT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMDABSOT),'STU_PIAC','LMDESABB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMCAQSOT),'STU_PIAC','LMACQVEN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMCARSOT),'STU_PIAC','LMACQRIV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMPEISOT),'STU_PIAC','LMPERIND');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMCODCON',this.w_LMCODCON,'LMPROGRE',this.w_LMPROCON,'LMCODSOT',this.w_LMCODSOT,'LMDESSOT',this.w_LMDESSOT,'LMTIPSOT',this.w_LMTIPSOT,'LMDESABB',this.w_LMDABSOT,'LMACQVEN',this.w_LMCAQSOT,'LMACQRIV',this.w_LMCARSOT,'LMPERIND',this.w_LMPEISOT)
      insert into (i_cTable) (LMCODCON,LMPROGRE,LMCODSOT,LMDESSOT,LMTIPSOT,LMDESABB,LMACQVEN,LMACQRIV,LMPERIND &i_ccchkf. );
         values (;
           this.w_LMCODCON;
           ,this.w_LMPROCON;
           ,this.w_LMCODSOT;
           ,this.w_LMDESSOT;
           ,this.w_LMTIPSOT;
           ,this.w_LMDABSOT;
           ,this.w_LMCAQSOT;
           ,this.w_LMCARSOT;
           ,this.w_LMPEISOT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Fallito inserimento nella tabella Piano dei Conti (Detail)'
      return
    endif
    return
  proc Try_03966DF8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into STU_PIAC
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.STU_PIAC_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_PIAC_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_PIAC_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"LMDESSOT ="+cp_NullLink(cp_ToStrODBC(this.w_LMDESSOT),'STU_PIAC','LMDESSOT');
      +",LMTIPSOT ="+cp_NullLink(cp_ToStrODBC(this.w_LMTIPSOT),'STU_PIAC','LMTIPSOT');
      +",LMDESABB ="+cp_NullLink(cp_ToStrODBC(this.w_LMDABSOT),'STU_PIAC','LMDESABB');
      +",LMACQVEN ="+cp_NullLink(cp_ToStrODBC(w_LMACQSOT),'STU_PIAC','LMACQVEN');
      +",LMACQRIV ="+cp_NullLink(cp_ToStrODBC(this.w_LMCARSOT),'STU_PIAC','LMACQRIV');
      +",LMPERIND ="+cp_NullLink(cp_ToStrODBC(this.w_LMPEISOT),'STU_PIAC','LMPERIND');
          +i_ccchkf ;
      +" where ";
          +"LMCODCON = "+cp_ToStrODBC(this.w_LMCODCON);
          +" and LMPROGRE = "+cp_ToStrODBC(this.w_LMPROCON);
          +" and LMCODSOT = "+cp_ToStrODBC(this.w_LMCODSOT);
             )
    else
      update (i_cTable) set;
          LMDESSOT = this.w_LMDESSOT;
          ,LMTIPSOT = this.w_LMTIPSOT;
          ,LMDESABB = this.w_LMDABSOT;
          ,LMACQVEN = w_LMACQSOT;
          ,LMACQRIV = this.w_LMCARSOT;
          ,LMPERIND = this.w_LMPEISOT;
          &i_ccchkf. ;
       where;
          LMCODCON = this.w_LMCODCON;
          and LMPROGRE = this.w_LMPROCON;
          and LMCODSOT = this.w_LMCODSOT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Fallita la scrittura nella Tabella Piano dei Conti (Detail)'
      return
    endif
    return


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LETTURA DI CLIENTI E FORNITORI
    * --- Mi posiziono all'inizio dei record che identificano i clienti
    this.w_COMODO = FREAD(this.hFile,9)
    FSEEK(this.hFile,-9,1)
    do while ((this.w_COMODO<>"FCLIEFORN") .and. (.not. FEOF(this.hFile)))
      * --- Skippo un record
      FSEEK(this.hFile,this.LUNRECORD,1)
      * --- Rileggo il codice
      this.w_COMODO = FREAD(this.hFile,9)
      FSEEK(this.hFile,-9,1)
    enddo
    if this.w_COMODO<>"FCLIEFORN"
      this.w_STATOCLIFOR = 0
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      return
    endif
    * --- Skippo la testata
    FSEEK(this.hFile,this.LUNRECORD,1)
    * --- Inizio lettura dei clienti/fornitori
    this.w_COMODO = FREAD(this.hFile,9)
    FSEEK(this.hFile,-9,1)
    do while this.w_COMODO<>"ECLIEFORN"
      this.w_ANTIPCON = SPACE(1)
      this.w_ANCODICE = SPACE(15)
      this.w_ANDESCRI = SPACE(40)
      this.w_ANCODSOG = SPACE(8)
      this.w_ANINDIRI = SPACE(35)
      this.w_AN___CAP = SPACE(8)
      this.w_ANLOCALI = SPACE(30)
      this.w_ANPROVIN = SPACE(2)
      this.w_ANPERFIS = SPACE(1)
      this.w_ANCODFIS = SPACE(16)
      this.w_ANPARIVA = SPACE(12)
      this.w_ANFLINTR = SPACE(1)
      this.w_ANCODSTU = SPACE(6)
      this.w_ANCODCAT = SPACE(4)
      * --- Leggo il primo campo
      this.w_COMODO = FREAD(this.hFile,3)
      if this.w_COMODO<>"D10"
        * --- Ritorno in quanto c'� un errore
        this.w_STATOCLIFOR = 0
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        return
      endif
      * --- Leggo se cliente o fornitore
      this.w_ANTIPCON = FREAD(this.hFile,1)
      if this.w_ANTIPCON="C"
        this.w_ANCATCON = this.oParentObject.w_CLICATCON
        this.w_ANCODLIN = this.oParentObject.w_CLICODLIN
        this.w_ANCONSUP = this.oParentObject.w_CLICONSUP
        this.w_ANTIPFAT = "R"
        this.w_ANBOLFAT = "N"
        this.w_ANPREBOL = "N"
        this.w_ANFLCONA = "U"
        this.w_ANRITENU = " "
        this.w_ANTIPCLF = " "
        this.w_ANGESCON = "N"
      else
        this.w_ANCATCON = this.oParentObject.w_FORCATCON
        this.w_ANCODLIN = this.oParentObject.w_FORCODLIN
        this.w_ANCONSUP = this.oParentObject.w_FORCONSUP
        this.w_ANTIPFAT = " "
        this.w_ANBOLFAT = " "
        this.w_ANPREBOL = " "
        this.w_ANFLCONA = " "
        this.w_ANRITENU = "N"
        this.w_ANTIPCLF = "G"
        this.w_ANGESCON = " "
      endif
      * --- Leggo codice  CLI/FOR
      this.w_ANCODSTU = FREAD(this.hFile,6)
      * --- Messaggio a Video
      if this.w_ANTIPCON="C"
        this.w_STR1 = "Import cliente %1"
      else
        this.w_STR1 = "Import fornitore %1"
      endif
      ah_Msg(this.w_STR1,.T.,.F.,.F.,this.w_ANCODSTU)
      * --- Leggo il codice soggetto
      this.w_ANCODSOG = FREAD(this.hFile,8)
      * --- Leggo la ragione sociale
      this.w_RAGSOC1 = FREAD(this.hFile,25)
      this.w_RAGSOC2 = FREAD(this.hFile,25)
      * --- Leggo il codice fiscale
      this.w_ANCODFIS = FREAD(this.hFile,16)
      * --- Leggo flag codice fiscale errato
      this.w_CODFISOK = FREAD(this.hFile,1)
      * --- Leggo il tipo di soggetto (intracomunitario o no)
      this.w_ANFLINTR = FREAD(this.hFile,1)
      * --- Skippo flag compilata denominazione
      FSEEK(this.hFile,1,1)
      * --- Leggo il campo lasciato a spazio
      this.w_FIELDSK = FREAD(this.hFile,113)
      * --- Leggo il codice della seconda parte
      this.w_COMODO = FREAD(this.hFile,3)
      if this.w_COMODO<>"D11"
        * --- Ritorno in quanto c'� un errore
        this.w_STATOCLIFOR = 0
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        return
      endif
      * --- Leggo la via
      this.w_VIA = FREAD(this.hFile,32)
      * --- Leggo il numero civico
      this.w_NUMCIV = FREAD(this.hFile,5)
      this.w_ANINDIRI = ALLTRIM(ALLTRIM(LEFT(this.w_VIA,29))+iif(! EMPTY(ALLTRIM(this.w_NUMCIV)),","+ALLTRIM(this.w_NUMCIV),space(5)))
      * --- Leggo il codice comune
      this.w_ANCODCAT = FREAD(this.hFile,4)
      * --- Leggo il codice postale
      this.w_AN___CAP = FREAD(this.hFile,5)
      * --- Leggo il comune di residenza
      this.w_ANLOCALI = FREAD(this.hFile,23)
      * --- Leggo la provincia
      this.w_ANPROVIN = FREAD(this.hFile,2)
      * --- Leggo la partita iva
      this.w_ANPARIVA = FREAD(this.hFile,11)
      this.w_PARIVAOK = FREAD(this.hFile,1)
      * --- Persona o societ�
      this.w_ANPERFIS = FREAD(this.hFile,1)
      this.w_ANPERFIS = IIF(this.w_ANPERFIS="D","S","N")
      * --- Gestisco i campi Cognome e Nome della persona fisica presenti in anagrafica Conti
      if this.w_ANPERFIS = "S"
        this.w_ANCOGNOM = this.w_RAGSOC1
        this.w_AN__NOME = this.w_RAGSOC2
        this.w_ANDESCRI = LEFT(this.w_RAGSOC1+this.w_RAGSOC2,40)
      else
        this.w_RAGSOC = this.w_RAGSOC1+this.w_RAGSOC2
        this.w_ANDESCRI = LEFT(this.w_RAGSOC,40)
        this.w_ANDESCR2 = IIF(SUBSTR(this.w_RAGSOC,40,1)=" "," "+SUBSTR(this.w_RAGSOC,41),SUBSTR(this.w_RAGSOC,41))
        this.w_ANCOGNOM = space(20)
        this.w_AN__NOME = space(20)
      endif
      * --- Skippo flag San Marino
      FSEEK(this.hFile,1,1)
      * --- Bolla doganale
      this.w_BOLLA = FREAD(this.hFile,1)
      * --- Skippo flag codice Mov. DR 770 1/2/3/4/5 e flag black list
      FSEEK(this.hFile,2,1)
      * --- Leggo il campo lasciato a spazio
      this.w_FIELDSK = FREAD(this.hFile,109)
      * --- Mantengo separate le due ricerche per contemplare tutte le combinazione codice conto/codice soggetto
      if ! EMPTY(this.w_ANCODSOG)
        SELECT CURCONTI
        GO TOP
        LOCATE FOR CURCONTI.ANCODSOG=this.w_ANCODSOG AND CURCONTI.ANTIPCON=this.w_ANTIPCON
        if FOUND()
          this.w_ANCODICE = CURCONTI.ANCODICE
        else
          this.w_ANCODICE = this.w_ANCODSTU
        endif
      else
        SELECT CURCONTI
        GO TOP
        LOCATE FOR CURCONTI.ANCODSTU=this.w_ANCODSTU
        if FOUND()
          this.w_ANCODICE = CURCONTI.ANCODICE
        else
          this.w_ANCODICE = this.w_ANCODSTU
        endif
      endif
      * --- SCRITTURA TABELLA CONTI
      * --- Try
      local bErr_02844260
      bErr_02844260=bTrsErr
      this.Try_02844260()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        if this.oParentObject.w_AGGDATI="S"
          * --- Try
          local bErr_028472F0
          bErr_028472F0=bTrsErr
          this.Try_028472F0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_028472F0
          * --- End
        endif
      endif
      bTrsErr=bTrsErr or bErr_02844260
      * --- End
      * --- Leggo il prossimo codice
      this.w_COMODO = FREAD(this.hFile,9)
      FSEEK(this.hFile,-9,1)
    enddo
    * --- Successo dell'operazione
    this.w_STATOCLIFOR=1
  endproc
  proc Try_02844260()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CONTI
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ANTIPCON"+",ANCODICE"+",ANDESCRI"+",ANINDIRI"+",AN___CAP"+",ANLOCALI"+",ANPROVIN"+",ANPERFIS"+",ANCODFIS"+",ANPARIVA"+",ANCATCON"+",ANCODLIN"+",ANCONSUP"+",AFFLINTR"+",ANFLESIG"+",ANPARTSN"+",ANCODSTU"+",AN_SESSO"+",ANTIPFAT"+",ANBOLFAT"+",ANPREBOL"+",ANSCORPO"+",ANFLGAVV"+",ANCONCON"+",ANFLCONA"+",ANRITENU"+",ANTIPCLF"+",ANGESCON"+",ANCODSOG"+",ANCODCAT"+",ANCOGNOM"+",AN__NOME"+",ANDESCR2"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_ANTIPCON),'CONTI','ANTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODICE),'CONTI','ANCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANDESCRI),'CONTI','ANDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANINDIRI),'CONTI','ANINDIRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AN___CAP),'CONTI','AN___CAP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANLOCALI),'CONTI','ANLOCALI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANPROVIN),'CONTI','ANPROVIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANPERFIS),'CONTI','ANPERFIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODFIS),'CONTI','ANCODFIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANPARIVA),'CONTI','ANPARIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANCATCON),'CONTI','ANCATCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODLIN),'CONTI','ANCODLIN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANCONSUP),'CONTI','ANCONSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANFLINTR),'CONTI','AFFLINTR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANFLESIG),'CONTI','ANFLESIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANPARTSN),'CONTI','ANPARTSN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODSTU),'CONTI','ANCODSTU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AN_SESSO),'CONTI','AN_SESSO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANTIPFAT),'CONTI','ANTIPFAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANBOLFAT),'CONTI','ANBOLFAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANPREBOL),'CONTI','ANPREBOL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANSCORPO),'CONTI','ANSCORPO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANFLGAVV),'CONTI','ANFLGAVV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANCONCON),'CONTI','ANCONCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANFLCONA),'CONTI','ANFLCONA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANRITENU),'CONTI','ANRITENU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANTIPCLF),'CONTI','ANTIPCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANGESCON),'CONTI','ANGESCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODSOG),'CONTI','ANCODSOG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODCAT),'CONTI','ANCODCAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANCOGNOM),'CONTI','ANCOGNOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AN__NOME),'CONTI','AN__NOME');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ANDESCR2),'CONTI','ANDESCR2');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ANTIPCON',this.w_ANTIPCON,'ANCODICE',this.w_ANCODICE,'ANDESCRI',this.w_ANDESCRI,'ANINDIRI',this.w_ANINDIRI,'AN___CAP',this.w_AN___CAP,'ANLOCALI',this.w_ANLOCALI,'ANPROVIN',this.w_ANPROVIN,'ANPERFIS',this.w_ANPERFIS,'ANCODFIS',this.w_ANCODFIS,'ANPARIVA',this.w_ANPARIVA,'ANCATCON',this.w_ANCATCON,'ANCODLIN',this.w_ANCODLIN)
      insert into (i_cTable) (ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN,ANPERFIS,ANCODFIS,ANPARIVA,ANCATCON,ANCODLIN,ANCONSUP,AFFLINTR,ANFLESIG,ANPARTSN,ANCODSTU,AN_SESSO,ANTIPFAT,ANBOLFAT,ANPREBOL,ANSCORPO,ANFLGAVV,ANCONCON,ANFLCONA,ANRITENU,ANTIPCLF,ANGESCON,ANCODSOG,ANCODCAT,ANCOGNOM,AN__NOME,ANDESCR2 &i_ccchkf. );
         values (;
           this.w_ANTIPCON;
           ,this.w_ANCODICE;
           ,this.w_ANDESCRI;
           ,this.w_ANINDIRI;
           ,this.w_AN___CAP;
           ,this.w_ANLOCALI;
           ,this.w_ANPROVIN;
           ,this.w_ANPERFIS;
           ,this.w_ANCODFIS;
           ,this.w_ANPARIVA;
           ,this.w_ANCATCON;
           ,this.w_ANCODLIN;
           ,this.w_ANCONSUP;
           ,this.w_ANFLINTR;
           ,this.w_ANFLESIG;
           ,this.w_ANPARTSN;
           ,this.w_ANCODSTU;
           ,this.w_AN_SESSO;
           ,this.w_ANTIPFAT;
           ,this.w_ANBOLFAT;
           ,this.w_ANPREBOL;
           ,this.w_ANSCORPO;
           ,this.w_ANFLGAVV;
           ,this.w_ANCONCON;
           ,this.w_ANFLCONA;
           ,this.w_ANRITENU;
           ,this.w_ANTIPCLF;
           ,this.w_ANGESCON;
           ,this.w_ANCODSOG;
           ,this.w_ANCODCAT;
           ,this.w_ANCOGNOM;
           ,this.w_AN__NOME;
           ,this.w_ANDESCR2;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Fallito inserimento nella tabella Conti'
      return
    endif
    return
  proc Try_028472F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into CONTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ANDESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_ANDESCRI),'CONTI','ANDESCRI');
      +",ANINDIRI ="+cp_NullLink(cp_ToStrODBC(this.w_ANINDIRI),'CONTI','ANINDIRI');
      +",AN___CAP ="+cp_NullLink(cp_ToStrODBC(this.w_AN___CAP),'CONTI','AN___CAP');
      +",ANLOCALI ="+cp_NullLink(cp_ToStrODBC(this.w_ANLOCALI),'CONTI','ANLOCALI');
      +",ANPROVIN ="+cp_NullLink(cp_ToStrODBC(this.w_ANPROVIN),'CONTI','ANPROVIN');
      +",ANPERFIS ="+cp_NullLink(cp_ToStrODBC(this.w_ANPERFIS),'CONTI','ANPERFIS');
      +",ANCODFIS ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODFIS),'CONTI','ANCODFIS');
      +",ANPARIVA ="+cp_NullLink(cp_ToStrODBC(this.w_ANPARIVA),'CONTI','ANPARIVA');
      +",ANCATCON ="+cp_NullLink(cp_ToStrODBC(this.w_ANCATCON),'CONTI','ANCATCON');
      +",ANCODLIN ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODLIN),'CONTI','ANCODLIN');
      +",ANCONSUP ="+cp_NullLink(cp_ToStrODBC(this.w_ANCONSUP),'CONTI','ANCONSUP');
      +",AFFLINTR ="+cp_NullLink(cp_ToStrODBC(this.w_ANFLINTR),'CONTI','AFFLINTR');
      +",ANFLESIG ="+cp_NullLink(cp_ToStrODBC(this.w_ANFLESIG),'CONTI','ANFLESIG');
      +",ANCODSTU ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODSTU),'CONTI','ANCODSTU');
      +",ANPARTSN ="+cp_NullLink(cp_ToStrODBC(this.w_ANPARTSN),'CONTI','ANPARTSN');
      +",AN_SESSO ="+cp_NullLink(cp_ToStrODBC(this.w_AN_SESSO),'CONTI','AN_SESSO');
      +",ANTIPFAT ="+cp_NullLink(cp_ToStrODBC(this.w_ANTIPFAT),'CONTI','ANTIPFAT');
      +",ANBOLFAT ="+cp_NullLink(cp_ToStrODBC(this.w_ANBOLFAT),'CONTI','ANBOLFAT');
      +",ANPREBOL ="+cp_NullLink(cp_ToStrODBC(this.w_ANPREBOL),'CONTI','ANPREBOL');
      +",ANSCORPO ="+cp_NullLink(cp_ToStrODBC(this.w_ANSCORPO),'CONTI','ANSCORPO');
      +",ANFLGAVV ="+cp_NullLink(cp_ToStrODBC(this.w_ANFLGAVV),'CONTI','ANFLGAVV');
      +",ANCONCON ="+cp_NullLink(cp_ToStrODBC(this.w_ANCONCON),'CONTI','ANCONCON');
      +",ANFLCONA ="+cp_NullLink(cp_ToStrODBC(this.w_ANFLCONA),'CONTI','ANFLCONA');
      +",ANRITENU ="+cp_NullLink(cp_ToStrODBC(this.w_ANRITENU),'CONTI','ANRITENU');
      +",ANTIPCLF ="+cp_NullLink(cp_ToStrODBC(this.w_ANTIPCLF),'CONTI','ANTIPCLF');
      +",ANGESCON ="+cp_NullLink(cp_ToStrODBC(this.w_ANGESCON),'CONTI','ANGESCON');
      +",ANCODSOG ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODSOG),'CONTI','ANCODSOG');
      +",ANCODCAT ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODCAT),'CONTI','ANCODCAT');
      +",ANCOGNOM ="+cp_NullLink(cp_ToStrODBC(this.w_ANCOGNOM),'CONTI','ANCOGNOM');
      +",AN__NOME ="+cp_NullLink(cp_ToStrODBC(this.w_AN__NOME),'CONTI','AN__NOME');
      +",ANDESCR2 ="+cp_NullLink(cp_ToStrODBC(this.w_ANDESCR2),'CONTI','ANDESCR2');
          +i_ccchkf ;
      +" where ";
          +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
          +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
             )
    else
      update (i_cTable) set;
          ANDESCRI = this.w_ANDESCRI;
          ,ANINDIRI = this.w_ANINDIRI;
          ,AN___CAP = this.w_AN___CAP;
          ,ANLOCALI = this.w_ANLOCALI;
          ,ANPROVIN = this.w_ANPROVIN;
          ,ANPERFIS = this.w_ANPERFIS;
          ,ANCODFIS = this.w_ANCODFIS;
          ,ANPARIVA = this.w_ANPARIVA;
          ,ANCATCON = this.w_ANCATCON;
          ,ANCODLIN = this.w_ANCODLIN;
          ,ANCONSUP = this.w_ANCONSUP;
          ,AFFLINTR = this.w_ANFLINTR;
          ,ANFLESIG = this.w_ANFLESIG;
          ,ANCODSTU = this.w_ANCODSTU;
          ,ANPARTSN = this.w_ANPARTSN;
          ,AN_SESSO = this.w_AN_SESSO;
          ,ANTIPFAT = this.w_ANTIPFAT;
          ,ANBOLFAT = this.w_ANBOLFAT;
          ,ANPREBOL = this.w_ANPREBOL;
          ,ANSCORPO = this.w_ANSCORPO;
          ,ANFLGAVV = this.w_ANFLGAVV;
          ,ANCONCON = this.w_ANCONCON;
          ,ANFLCONA = this.w_ANFLCONA;
          ,ANRITENU = this.w_ANRITENU;
          ,ANTIPCLF = this.w_ANTIPCLF;
          ,ANGESCON = this.w_ANGESCON;
          ,ANCODSOG = this.w_ANCODSOG;
          ,ANCODCAT = this.w_ANCODCAT;
          ,ANCOGNOM = this.w_ANCOGNOM;
          ,AN__NOME = this.w_AN__NOME;
          ,ANDESCR2 = this.w_ANDESCR2;
          &i_ccchkf. ;
       where;
          ANTIPCON = this.w_ANTIPCON;
          and ANCODICE = this.w_ANCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Fallita la scrittura nella Tabella Contii'
      return
    endif
    return


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LETTURA CAUSALI CONTABILI
    * --- Mi posiziono all'inizio dei record che identificano le causali
    this.w_COMODO = FREAD(this.hFile,9)
    FSEEK(this.hFile,-9,1)
    do while ((this.w_COMODO<>"FCAUSMOVM") .and. (.not. FEOF(this.hFile)))
      * --- Skippo un record
      FSEEK(this.hFile,this.LUNRECORD,1)
      * --- Rileggo il codice
      this.w_COMODO = FREAD(this.hFile,9)
      FSEEK(this.hFile,-9,1)
    enddo
    if this.w_COMODO<>"FCAUSMOVM"
      this.w_STATOCAUCON = 0
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      return
    endif
    * --- Skippo la testata
    FSEEK(this.hFile,this.LUNRECORD,1)
    * --- Seleziono il file della causali contabili dello studio
    this.w_COMODO = FREAD(this.hFile,9)
    FSEEK(this.hFile,-9,1)
    do while this.w_COMODO<>"ECAUSMOVM"
      this.w_LMCODCAU = SPACE(3)
      this.w_LMDESCAU = SPACE(29)
      this.w_LMINFDAR = SPACE(6)
      this.w_LMSUPDAR = SPACE(6)
      this.w_LMINFAVE = SPACE(6)
      this.w_LMSUPAVE = SPACE(6)
      * --- Leggo il primo campo
      this.w_COMODO = FREAD(this.hFile,3)
      if this.w_COMODO<>"D21"
        * --- Ritorno in quanto c'� un errore
        this.w_STATOCAUCON = 0
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        return
      endif
      * --- Leggo il codice causale
      this.w_LMCODCAU = FREAD(this.hFile,3)
      * --- Messaggio a Video
      this.w_STR1 = "Import causale contabile %1"
      ah_Msg(this.w_STR1,.T.,.F.,.F.,this.w_LMCODCAU)
      * --- Leggo la descrizione della causale
      this.w_LMDESCAU = FREAD(this.hFile,29)
      * --- Leggo il sottoconto limite inferiore dare
      this.w_LMINFDAR = FREAD(this.hFile,6)
      * --- Leggo il sottoconto limite superiore dare
      this.w_LMSUPDAR = FREAD(this.hFile,6)
      * --- Leggo il sottoconto limite inferiore avere
      this.w_LMINFAVE = FREAD(this.hFile,6)
      * --- Leggo il sottoconto limite superiore avere
      this.w_LMSUPAVE = FREAD(this.hFile,6)
      * --- Leggo il campo lasciato a spazio
      this.w_FIELDSK = FREAD(this.hFile,141)
      * --- SCRITTURA TABELLA CAUSALI CONTABILI
      * --- Try
      local bErr_036E7548
      bErr_036E7548=bTrsErr
      this.Try_036E7548()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
        if this.oParentObject.w_AGGDATI="S"
          * --- Try
          local bErr_0369CF48
          bErr_0369CF48=bTrsErr
          this.Try_0369CF48()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_0369CF48
          * --- End
        endif
      endif
      bTrsErr=bTrsErr or bErr_036E7548
      * --- End
      * --- Leggo il prossimo codice
      this.w_COMODO = FREAD(this.hFile,9)
      FSEEK(this.hFile,-9,1)
    enddo
    * --- Stato dell'operazione
    this.w_STATOCAUCON=1
  endproc
  proc Try_036E7548()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into STU_CAUS
    i_nConn=i_TableProp[this.STU_CAUS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_CAUS_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.STU_CAUS_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LMCODCAU"+",LMDESCAU"+",LMINFDAR"+",LMSUPDAR"+",LMINFAVE"+",LMSUPAVE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_LMCODCAU),'STU_CAUS','LMCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMDESCAU),'STU_CAUS','LMDESCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMINFDAR),'STU_CAUS','LMINFDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMSUPDAR),'STU_CAUS','LMSUPDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMINFAVE),'STU_CAUS','LMINFAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LMSUPAVE),'STU_CAUS','LMSUPAVE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LMCODCAU',this.w_LMCODCAU,'LMDESCAU',this.w_LMDESCAU,'LMINFDAR',this.w_LMINFDAR,'LMSUPDAR',this.w_LMSUPDAR,'LMINFAVE',this.w_LMINFAVE,'LMSUPAVE',this.w_LMSUPAVE)
      insert into (i_cTable) (LMCODCAU,LMDESCAU,LMINFDAR,LMSUPDAR,LMINFAVE,LMSUPAVE &i_ccchkf. );
         values (;
           this.w_LMCODCAU;
           ,this.w_LMDESCAU;
           ,this.w_LMINFDAR;
           ,this.w_LMSUPDAR;
           ,this.w_LMINFAVE;
           ,this.w_LMSUPAVE;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Fallito inserimento nella tabella Causali Contabili'
      return
    endif
    return
  proc Try_0369CF48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into STU_CAUS
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.STU_CAUS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.STU_CAUS_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.STU_CAUS_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"LMDESCAU ="+cp_NullLink(cp_ToStrODBC(this.w_LMDESCAU),'STU_CAUS','LMDESCAU');
      +",LMINFDAR ="+cp_NullLink(cp_ToStrODBC(this.w_LMINFDAR),'STU_CAUS','LMINFDAR');
      +",LMSUPDAR ="+cp_NullLink(cp_ToStrODBC(this.w_LMSUPDAR),'STU_CAUS','LMSUPDAR');
      +",LMINFAVE ="+cp_NullLink(cp_ToStrODBC(this.w_LMINFAVE),'STU_CAUS','LMINFAVE');
      +",LMSUPAVE ="+cp_NullLink(cp_ToStrODBC(this.w_LMSUPAVE),'STU_CAUS','LMSUPAVE');
          +i_ccchkf ;
      +" where ";
          +"LMCODCAU = "+cp_ToStrODBC(this.w_LMCODCAU);
             )
    else
      update (i_cTable) set;
          LMDESCAU = this.w_LMDESCAU;
          ,LMINFDAR = this.w_LMINFDAR;
          ,LMSUPDAR = this.w_LMSUPDAR;
          ,LMINFAVE = this.w_LMINFAVE;
          ,LMSUPAVE = this.w_LMSUPAVE;
          &i_ccchkf. ;
       where;
          LMCODCAU = this.w_LMCODCAU;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error='Fallita la scrittura nella Tabella Causali Contabili'
      return
    endif
    return


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura Cursori
    if USED("CONTISTUDIO")
      SELECT CONTISTUDIO
      USE
    endif
    if USED("CURCONTI")
      SELECT CURCONTI
      USE
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='MASTRI'
    this.cWorkTables[4]='STUMPIAC'
    this.cWorkTables[5]='STU_CAUS'
    this.cWorkTables[6]='STU_PARA'
    this.cWorkTables[7]='STU_PIAC'
    return(this.OpenAllTables(7))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
