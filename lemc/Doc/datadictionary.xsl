<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:output method="html" version="4.0" encoding="ISO-8859-1"/>

<xsl:param name="default_extension" select="'.xml'"/>  <!-- usato nei link del tag Plan -->

<xsl:include href="common.xsl"/>


<xsl:template match="/">
  <xsl:apply-templates select="Plan"/>
</xsl:template>


<!-- Nodo principale -->

<xsl:template match="Plan">
  <HTML>
    <HEAD>
      <STYLE>
        BODY {font-family:Times New Roman; color:black; background:white; margin:20px}
        TABLE {font-family:Times New Roman; color:black; background:white}
        A:hover {color:red}
      </STYLE>
    </HEAD>
    <BODY>
      <P ALIGN="CENTER"><FONT COLOR="#000080" SIZE="6"><B>Data dictionary</B></FONT></P>
      <P ALIGN="CENTER">
        <TABLE BORDER="1" CELLPADDING="4" CELLSPACING="0">
          <TR BGCOLOR="#6495ED">
            <TD><FONT COLOR="WHITE"><B>Field</B></FONT></TD>
            <TD><FONT COLOR="WHITE"><B>Description</B></FONT></TD>
            <TD><FONT COLOR="WHITE"><B>Type</B></FONT></TD>
            <TD><FONT COLOR="WHITE"><B>Len</B></FONT></TD>
            <TD><FONT COLOR="WHITE"><B>Dec</B></FONT></TD>
            <TD><FONT COLOR="WHITE"><B>Key</B></FONT></TD>
            <xsl:if test="PlanItem/ItemField/FieldNotnull">
              <TD><FONT COLOR="WHITE"><B>Not&#160;null</B></FONT></TD>
            </xsl:if>
            <TD><FONT COLOR="WHITE"><B>Repeated</B></FONT></TD>
            <TD><FONT COLOR="WHITE"><B>Table</B></FONT></TD>
            <TD><FONT COLOR="WHITE"><B>Phisical name</B></FONT></TD>
          </TR>
          <xsl:for-each select="PlanItem/ItemField">
            <xsl:sort select="FieldName"/>
            <xsl:sort select="@TablePhName"/>
            <TR BGCOLOR="#F4FBFF">
              <TD>
                <A HREF="{/Plan/PlanName}_Tables{$default_extension}#{@TableName}_{FieldName}">
                  <xsl:value-of select="FieldName"/>
                </A>
              </TD>
              <TD><xsl:apply-templates select="FieldComment"/></TD>
              <TD><xsl:apply-templates select="FieldType"/></TD>
              <TD><xsl:apply-templates select="FieldLen"/></TD>
              <TD><xsl:apply-templates select="FieldDec"/></TD>
              <TD><xsl:apply-templates select="FieldKey"/></TD>
              <xsl:if test="FieldNotnull">
                <TD ALIGN="CENTER"><xsl:apply-templates select="FieldNotnull"/></TD>
              </xsl:if>
              <TD ALIGN="CENTER"><xsl:apply-templates select="FieldRepeated"/></TD>
              <TD>
                <A HREF="{/Plan/PlanName}_Tables{$default_extension}#Table_{@TableName}">
                  <xsl:value-of select="@TableName"/>
                </A>
              </TD>
              <TD><xsl:value-of select="@TablePhName"/></TD>
            </TR>
          </xsl:for-each>
        </TABLE>
      </P>
      <DIV ALIGN="CENTER">
        <A HREF="{PlanName}_Main{$default_extension}">Project overview</A>
      </DIV>
    </BODY>
  </HTML>
</xsl:template>


<!-- Gestione checkbox TRUE/FALSE -->

<xsl:template match="FieldNotnull | FieldRepeated">
  <xsl:call-template name="LogicalCheckBox"/>
</xsl:template>


<!-- Gestione celle vuote -->

<xsl:template match="FieldComment | FieldType | FieldLen | FieldDec | FieldKey">
  <xsl:call-template name="ValueOrNotAvailable"/>
</xsl:template>


</xsl:stylesheet>
