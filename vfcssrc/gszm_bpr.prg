* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bpr                                                        *
*              Menu contestuale pratiche                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-02-19                                                      *
* Last revis.: 2011-05-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFUNZ
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bpr",oParentObject,m.pFUNZ)
return(i_retval)

define class tgszm_bpr as StdBatch
  * --- Local variables
  pFUNZ = space(1)
  w_OBJECT = .NULL.
  w_OBJ = .NULL.
  w_CNCODCAN = space(15)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                                         Routine per avvio funzionalit� legate alle Pratiche, da men� contestuale (tasto destro)
    * --- Assegno alla variabile il valore relativo alla chiave primaria 1 (CNCODCAN) delle Pratiche
    this.w_CNCODCAN = g_oMenu.getbyindexkeyvalue(1)
    do case
      case this.pFUNZ="S" OR this.pFUNZ="N"
        * --- CREAZIONE PRATICA SIMILE/COLLEGATA
        * --- Istanzio la maschera
        this.w_OBJECT = GSAL_KDP()
        * --- Controllo se ha passato il test di accesso
        if !(this.w_OBJECT.bSec1)
          Ah_ErrorMsg("Impossibile eseguire la creazione pratica!",48,"")
          i_retcode = 'stop'
          return
        endif
        if this.pFUNZ="N" 
          * --- Pratica simile
          this.w_OBJECT.w_FlColleg = "N"
          this.w_OBJECT.w_CodOrigNoColl = this.w_CNCODCAN
          this.w_OBJ = this.w_OBJECT.GetCtrl("w_CodOrigNoColl")
        else
          * --- Pratica collegata
          this.w_OBJECT.w_FlColleg = "S"
          this.w_OBJECT.w_CodOrigColl = this.w_CNCODCAN
          this.w_OBJ = this.w_OBJECT.GetCtrl("w_CodOrigColl")
        endif
        this.w_OBJ.Check()     
        * --- Aggiornamento variabili in maschera
        this.w_OBJECT.mcalc(.t.)     
        if p_PRNUME $ "SI"
          * --- Posiziono il cursore sulla classificazione della pratica di destinazione
          this.w_OBJ = this.w_OBJECT.GetCtrl("w_CODCLA")
        else
          * --- Posiziono il cursore sulla pratica di destinazione
          this.w_OBJ = this.w_OBJECT.GetCtrl("w_CodDest")
        endif
        this.w_OBJ.Setfocus()     
    endcase
  endproc


  proc Init(oParentObject,pFUNZ)
    this.pFUNZ=pFUNZ
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFUNZ"
endproc
