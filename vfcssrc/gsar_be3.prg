* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_be3                                                        *
*              CONTA I CONTRIBUTI GESTITI A PESO                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-03                                                      *
* Last revis.: 2015-03-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPAR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_be3",oParentObject,m.pPAR)
return(i_retval)

define class tgsar_be3 as StdBatch
  * --- Local variables
  pPAR = space(3)
  w_GSAR_MAR = .NULL.
  w_CURSORE = space(10)
  w_NUMERODIRECORD = 0
  w_ARTICOLO = space(20)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    L_NOLDAREA = SELECT()
    this.w_CURSORE = SYS( 2015 )
    do case
      case this.pPAR = "ART"
        this.w_GSAR_MAR = this.oParentObject.GSAR_MAR
        this.w_GSAR_MAR.LinkPCClick(.T.)     
        * --- Nuovo Oggetto
        this.w_GSAR_MAR = this.oParentObject.GSAR_MAR
        this.w_GSAR_MAR.ecpsave()     
        this.w_GSAR_MAR.CNT.EXEC_SELECT( this.w_CURSORE , "t_MCTIPCAT AS MCTIPCAT , t_TPAPPPES AS TPAPPPES" , "t_TPAPPPES = 'S' AND NOT DELETED()" )
      case this.pPAR = "KEY"
        this.w_ARTICOLO = this.oParentObject.w_CACODART
        VQ_EXEC( "QUERY\GSAR_BE3.VQR" , THIS , this.w_CURSORE )
    endcase
    this.w_NUMERODIRECORD = RECCOUNT( this.w_CURSORE )
    if USED( this.w_CURSORE )
      SELECT( this.w_CURSORE )
      USE
    endif
    SELECT( L_NOLDAREA )
    i_retcode = 'stop'
    i_retval = this.w_NUMERODIRECORD
    return
  endproc


  proc Init(oParentObject,pPAR)
    this.pPAR=pPAR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPAR"
endproc
