* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bdc                                                        *
*              Registra distinta tesoreria                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_22]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-01                                                      *
* Last revis.: 2000-05-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_DISERIAL,w_BOTTONE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bdc",oParentObject,m.w_DISERIAL,m.w_BOTTONE)
return(i_retval)

define class tgste_bdc as StdBatch
  * --- Local variables
  w_DISERIAL = space(10)
  w_BOTTONE = .NULL.
  w_APPO = space(1)
  w_NUMDIS = space(10)
  w_TIPSCA = space(1)
  w_CODCAU = space(5)
  w_SCAINI = ctod("  /  /  ")
  w_SCAFIN = ctod("  /  /  ")
  w_FLBANC1 = space(1)
  w_CODABI1 = space(5)
  w_FLCABI1 = space(1)
  w_TIPDIS = space(2)
  w_BANRIF = space(15)
  w_FLRD1 = space(2)
  w_FLRI1 = space(2)
  w_FLMA1 = space(2)
  w_FLRB1 = space(2)
  w_FLBO1 = space(2)
  w_FLCA1 = space(2)
  w_TIPO = space(1)
  w_CODVAL = space(3)
  w_FLIBAN1 = space(1)
  w_PADRE = .NULL.
  w_MESS_ERR = space(250)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Conferma Distinta Tesoreria (da GSTE_ADI)
    * --- Questo batch viene richiamato dal bottone di conferma presente sull'anagrafica delle
    * --- distinte di tesoreria.
    * --- Memorizza la distinta che l'operatore sta' caricando e rientra in stato di variazione.
    * --- Parametri
    * --- Serial della distinta
    * --- Nome del bottone che ha lanciato questo batch
    this.w_NUMDIS = this.oParentObject.w_DINUMDIS
    this.w_TIPSCA = this.oParentObject.w_DITIPSCA
    this.w_CODCAU = this.oParentObject.w_DICODCAU
    this.w_SCAINI = this.oParentObject.w_DISCAINI
    this.w_SCAFIN = this.oParentObject.w_DISCAFIN
    this.w_FLBANC1 = this.oParentObject.w_FLBANC
    this.w_CODABI1 = this.oParentObject.w_CODABI
    this.w_FLCABI1 = this.oParentObject.w_FLCABI
    this.w_TIPDIS = this.oParentObject.w_DITIPDIS
    this.w_BANRIF = this.oParentObject.w_DIBANRIF
    this.w_FLRD1 = this.oParentObject.w_FLRD
    this.w_FLRI1 = this.oParentObject.w_FLRI
    this.w_FLMA1 = this.oParentObject.w_FLMA
    this.w_FLRB1 = this.oParentObject.w_FLRB
    this.w_FLBO1 = this.oParentObject.w_FLBO
    this.w_FLCA1 = this.oParentObject.w_FLCA
    this.w_TIPO = this.oParentObject.w_TIPCON
    this.w_CODVAL = this.oParentObject.w_DICODVAL
    this.w_FLIBAN1 = this.oParentObject.w_FLIBAN
    if this.oParentObject.w_FLORI
      do case
        case EMPTY(this.oParentObject.w_DICAURIF)
          ah_ErrorMsg("Inserire la causale distinta",,"")
          i_retcode = 'stop'
          return
        case EMPTY(this.oParentObject.w_DIBANRIF)
          ah_ErrorMsg("Inserire la banca di presentazione",,"")
          i_retcode = 'stop'
          return
        case EMPTY(this.oParentObject.w_DICODVAL)
          ah_ErrorMsg("Inserire la valuta della distinta",,"")
          i_retcode = 'stop'
          return
        case EMPTY(this.oParentObject.w_DICAOVAL)
          ah_ErrorMsg("Inserire il cambio di contabilizzazione delle scadenze",,"")
          i_retcode = 'stop'
          return
      endcase
      * --- Se in modifica impedisco l'abbina se esistono effetti negativi 
      *     (caso possibile se l'utente elimina delle scadenze raggruppate con note di credito)
      this.w_PADRE = This.oParentObject
      if this.w_PADRE.cFunction="Edit"
        this.w_MESS_ERR = GSTE_BCR(this.w_PADRE,"C",this.w_PADRE.GSTE_MPD.cTrsName,this.oParentObject.w_FLBANC,this.oParentObject.w_DITIPDIS,this.oParentObject.w_DITIPSCA)
        if not Empty( this.w_MESS_ERR )
          ah_ErrorMsg("%1",,"", this.w_MESS_ERR)
          i_retcode = 'stop'
          return
        endif
      endif
      * --- Variabili
      this.oParentObject.w_DINUMDIS = this.w_DISERIAL
      this.oParentObject.w_FASE = 1
      this.w_APPO = this.oParentObject.w_FLCABI
      * --- Memorizzazione Distinta
      this.oParentObject.w_FLORI = .F.
      this.oParentObject.w_FASE1 = 1
      this.oParentObject.w_FLCABI = this.w_APPO
      do GSTE_KAI with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  proc Init(oParentObject,w_DISERIAL,w_BOTTONE)
    this.w_DISERIAL=w_DISERIAL
    this.w_BOTTONE=w_BOTTONE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_DISERIAL,w_BOTTONE"
endproc
