* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsarabia                                                        *
*              INTERROGAZIONE AGENDA                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-04-11                                                      *
* Last revis.: 2008-04-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_FLTCODRI,w_FLTGRUAP
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsarabia",oParentObject,m.w_FLTCODRI,m.w_FLTGRUAP)
return(i_retval)

define class tgsarabia as StdBatch
  * --- Local variables
  w_FLTCODRI = space(5)
  w_FLTGRUAP = space(5)
  * --- WorkFile variables
  gsarabia_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Create temporary table gsarabia
    i_nIdx=cp_AddTableDef('gsarabia') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('gsarabia',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.gsarabia_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
  endproc


  proc Init(oParentObject,w_FLTCODRI,w_FLTGRUAP)
    this.w_FLTCODRI=w_FLTCODRI
    this.w_FLTGRUAP=w_FLTGRUAP
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='*gsarabia'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_FLTCODRI,w_FLTGRUAP"
endproc
