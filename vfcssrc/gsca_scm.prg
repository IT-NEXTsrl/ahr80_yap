* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_scm                                                        *
*              Stampa parametri di ripartizione per centri di costo            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_10]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-11                                                      *
* Last revis.: 2007-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsca_scm",oParentObject))

* --- Class definition
define class tgsca_scm as StdForm
  Top    = 12
  Left   = 61

  * --- Standard Properties
  Width  = 531
  Height = 210
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-17"
  HelpContextID=177568407
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  CAL_RIPA_IDX = 0
  OUT_PUTS_IDX = 0
  CENCOST_IDX = 0
  cPrg = "gsca_scm"
  cComment = "Stampa parametri di ripartizione per centri di costo"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PCODINIZ = space(15)
  w_FEDESINI = space(40)
  w_PCODFINA = space(15)
  w_FEDESFIN = space(40)
  w_PDATINIZ = space(5)
  w_PEDESINI = space(40)
  w_PDATFIN = space(5)
  w_PEDESFIN = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATINI1 = ctod('  /  /  ')
  w_DATINI2 = ctod('  /  /  ')
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsca_scmPag1","gsca_scm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPCODINIZ_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='CAL_RIPA'
    this.cWorkTables[2]='OUT_PUTS'
    this.cWorkTables[3]='CENCOST'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsca_scm
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PCODINIZ=space(15)
      .w_FEDESINI=space(40)
      .w_PCODFINA=space(15)
      .w_FEDESFIN=space(40)
      .w_PDATINIZ=space(5)
      .w_PEDESINI=space(40)
      .w_PDATFIN=space(5)
      .w_PEDESFIN=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATINI1=ctod("  /  /  ")
      .w_DATINI2=ctod("  /  /  ")
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PCODINIZ))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_PCODFINA))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_PDATINIZ))
          .link_1_5('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .DoRTCalc(6,7,.f.)
        if not(empty(.w_PDATFIN))
          .link_1_11('Full')
        endif
          .DoRTCalc(8,8,.f.)
        .w_OBTEST = i_INIDAT
    endwith
    this.DoRTCalc(10,11,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PCODINIZ
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCODINIZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_PCODINIZ)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_PCODINIZ))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCODINIZ)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_PCODINIZ)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_PCODINIZ)+"%");

            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PCODINIZ) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oPCODINIZ_1_1'),i_cWhere,'GSCA_ACC',"Centri costo/ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCODINIZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_PCODINIZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_PCODINIZ)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCODINIZ = NVL(_Link_.CC_CONTO,space(15))
      this.w_FEDESINI = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PCODINIZ = space(15)
      endif
      this.w_FEDESINI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_PCODFINA)) OR  (.w_PCODINIZ<=.w_PCODFINA)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_PCODINIZ = space(15)
        this.w_FEDESINI = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCODINIZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PCODFINA
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CENCOST_IDX,3]
    i_lTable = "CENCOST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2], .t., this.CENCOST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PCODFINA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACC',True,'CENCOST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CC_CONTO like "+cp_ToStrODBC(trim(this.w_PCODFINA)+"%");

          i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CC_CONTO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CC_CONTO',trim(this.w_PCODFINA))
          select CC_CONTO,CCDESPIA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CC_CONTO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PCODFINA)==trim(_Link_.CC_CONTO) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStrODBC(trim(this.w_PCODFINA)+"%");

            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESPIA like "+cp_ToStr(trim(this.w_PCODFINA)+"%");

            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PCODFINA) and !this.bDontReportError
            deferred_cp_zoom('CENCOST','*','CC_CONTO',cp_AbsName(oSource.parent,'oPCODFINA_1_3'),i_cWhere,'GSCA_ACC',"Centri costo\ricavo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                     +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',oSource.xKey(1))
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PCODFINA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CC_CONTO,CCDESPIA";
                   +" from "+i_cTable+" "+i_lTable+" where CC_CONTO="+cp_ToStrODBC(this.w_PCODFINA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CC_CONTO',this.w_PCODFINA)
            select CC_CONTO,CCDESPIA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PCODFINA = NVL(_Link_.CC_CONTO,space(15))
      this.w_FEDESFIN = NVL(_Link_.CCDESPIA,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PCODFINA = space(15)
      endif
      this.w_FEDESFIN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_PCODFINA>=.w_PCODINIZ) or (empty(.w_PCODINIZ))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_PCODFINA = space(15)
        this.w_FEDESFIN = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CENCOST_IDX,2])+'\'+cp_ToStr(_Link_.CC_CONTO,1)
      cp_ShowWarn(i_cKey,this.CENCOST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PCODFINA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDATINIZ
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAL_RIPA_IDX,3]
    i_lTable = "CAL_RIPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAL_RIPA_IDX,2], .t., this.CAL_RIPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAL_RIPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDATINIZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACA',True,'CAL_RIPA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CAPERIOD like "+cp_ToStrODBC(trim(this.w_PDATINIZ)+"%");

          i_ret=cp_SQL(i_nConn,"select CAPERIOD,CADESCRI,CADATINI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CAPERIOD","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CAPERIOD',trim(this.w_PDATINIZ))
          select CAPERIOD,CADESCRI,CADATINI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CAPERIOD into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDATINIZ)==trim(_Link_.CAPERIOD) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDATINIZ) and !this.bDontReportError
            deferred_cp_zoom('CAL_RIPA','*','CAPERIOD',cp_AbsName(oSource.parent,'oPDATINIZ_1_5'),i_cWhere,'GSCA_ACA',"Calendario di ripartizione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CAPERIOD,CADESCRI,CADATINI";
                     +" from "+i_cTable+" "+i_lTable+" where CAPERIOD="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CAPERIOD',oSource.xKey(1))
            select CAPERIOD,CADESCRI,CADATINI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDATINIZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CAPERIOD,CADESCRI,CADATINI";
                   +" from "+i_cTable+" "+i_lTable+" where CAPERIOD="+cp_ToStrODBC(this.w_PDATINIZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CAPERIOD',this.w_PDATINIZ)
            select CAPERIOD,CADESCRI,CADATINI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDATINIZ = NVL(_Link_.CAPERIOD,space(5))
      this.w_PEDESINI = NVL(_Link_.CADESCRI,space(40))
      this.w_DATINI1 = NVL(cp_ToDate(_Link_.CADATINI),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PDATINIZ = space(5)
      endif
      this.w_PEDESINI = space(40)
      this.w_DATINI1 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_PDATFIN)) OR  (.w_DATINI1<=.w_DATINI2)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
        endif
        this.w_PDATINIZ = space(5)
        this.w_PEDESINI = space(40)
        this.w_DATINI1 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAL_RIPA_IDX,2])+'\'+cp_ToStr(_Link_.CAPERIOD,1)
      cp_ShowWarn(i_cKey,this.CAL_RIPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDATINIZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PDATFIN
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAL_RIPA_IDX,3]
    i_lTable = "CAL_RIPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAL_RIPA_IDX,2], .t., this.CAL_RIPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAL_RIPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PDATFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACA',True,'CAL_RIPA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CAPERIOD like "+cp_ToStrODBC(trim(this.w_PDATFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select CAPERIOD,CADESCRI,CADATINI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CAPERIOD","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CAPERIOD',trim(this.w_PDATFIN))
          select CAPERIOD,CADESCRI,CADATINI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CAPERIOD into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PDATFIN)==trim(_Link_.CAPERIOD) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PDATFIN) and !this.bDontReportError
            deferred_cp_zoom('CAL_RIPA','*','CAPERIOD',cp_AbsName(oSource.parent,'oPDATFIN_1_11'),i_cWhere,'GSCA_ACA',"Calendario di ripartizione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CAPERIOD,CADESCRI,CADATINI";
                     +" from "+i_cTable+" "+i_lTable+" where CAPERIOD="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CAPERIOD',oSource.xKey(1))
            select CAPERIOD,CADESCRI,CADATINI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PDATFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CAPERIOD,CADESCRI,CADATINI";
                   +" from "+i_cTable+" "+i_lTable+" where CAPERIOD="+cp_ToStrODBC(this.w_PDATFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CAPERIOD',this.w_PDATFIN)
            select CAPERIOD,CADESCRI,CADATINI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PDATFIN = NVL(_Link_.CAPERIOD,space(5))
      this.w_PEDESFIN = NVL(_Link_.CADESCRI,space(40))
      this.w_DATINI2 = NVL(cp_ToDate(_Link_.CADATINI),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PDATFIN = space(5)
      endif
      this.w_PEDESFIN = space(40)
      this.w_DATINI2 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_DATINI2>=.w_DATINI1) or (empty(.w_PDATINIZ))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande della codice finale")
        endif
        this.w_PDATFIN = space(5)
        this.w_PEDESFIN = space(40)
        this.w_DATINI2 = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAL_RIPA_IDX,2])+'\'+cp_ToStr(_Link_.CAPERIOD,1)
      cp_ShowWarn(i_cKey,this.CAL_RIPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PDATFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPCODINIZ_1_1.value==this.w_PCODINIZ)
      this.oPgFrm.Page1.oPag.oPCODINIZ_1_1.value=this.w_PCODINIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oFEDESINI_1_2.value==this.w_FEDESINI)
      this.oPgFrm.Page1.oPag.oFEDESINI_1_2.value=this.w_FEDESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPCODFINA_1_3.value==this.w_PCODFINA)
      this.oPgFrm.Page1.oPag.oPCODFINA_1_3.value=this.w_PCODFINA
    endif
    if not(this.oPgFrm.Page1.oPag.oFEDESFIN_1_4.value==this.w_FEDESFIN)
      this.oPgFrm.Page1.oPag.oFEDESFIN_1_4.value=this.w_FEDESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPDATINIZ_1_5.value==this.w_PDATINIZ)
      this.oPgFrm.Page1.oPag.oPDATINIZ_1_5.value=this.w_PDATINIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oPEDESINI_1_6.value==this.w_PEDESINI)
      this.oPgFrm.Page1.oPag.oPEDESINI_1_6.value=this.w_PEDESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPDATFIN_1_11.value==this.w_PDATFIN)
      this.oPgFrm.Page1.oPag.oPDATFIN_1_11.value=this.w_PDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPEDESFIN_1_12.value==this.w_PEDESFIN)
      this.oPgFrm.Page1.oPag.oPEDESFIN_1_12.value=this.w_PEDESFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((empty(.w_PCODFINA)) OR  (.w_PCODINIZ<=.w_PCODFINA))  and not(empty(.w_PCODINIZ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPCODINIZ_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
          case   not((.w_PCODFINA>=.w_PCODINIZ) or (empty(.w_PCODINIZ)))  and not(empty(.w_PCODFINA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPCODFINA_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
          case   not((empty(.w_PDATFIN)) OR  (.w_DATINI1<=.w_DATINI2))  and not(empty(.w_PDATINIZ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDATINIZ_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale")
          case   not((.w_DATINI2>=.w_DATINI1) or (empty(.w_PDATINIZ)))  and not(empty(.w_PDATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDATFIN_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande della codice finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsca_scmPag1 as StdContainer
  Width  = 527
  height = 210
  stdWidth  = 527
  stdheight = 210
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPCODINIZ_1_1 as StdField with uid="BYCOLGNQST",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PCODINIZ", cQueryName = "PCODINIZ",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Centro di costo/ricavo di inizio selezione",;
    HelpContextID = 225358672,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=94, Top=13, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_PCODINIZ"

  func oPCODINIZ_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCODINIZ_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPCODINIZ_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oPCODINIZ_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri costo/ricavo",'',this.parent.oContained
  endproc
  proc oPCODINIZ_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_PCODINIZ
     i_obj.ecpSave()
  endproc

  add object oFEDESINI_1_2 as StdField with uid="XKCARPQZJC",rtseq=2,rtrep=.f.,;
    cFormVar = "w_FEDESINI", cQueryName = "FEDESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 151979167,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=236, Top=13, InputMask=replicate('X',40)

  add object oPCODFINA_1_3 as StdField with uid="ONPZLJCDGG",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PCODFINA", cQueryName = "PCODFINA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Centro di costo/ricavo di fine selezione",;
    HelpContextID = 138326839,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=94, Top=39, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CENCOST", cZoomOnZoom="GSCA_ACC", oKey_1_1="CC_CONTO", oKey_1_2="this.w_PCODFINA"

  func oPCODFINA_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPCODFINA_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPCODFINA_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CENCOST','*','CC_CONTO',cp_AbsName(this.parent,'oPCODFINA_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACC',"Centri costo\ricavo",'',this.parent.oContained
  endproc
  proc oPCODFINA_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CC_CONTO=this.parent.oContained.w_PCODFINA
     i_obj.ecpSave()
  endproc

  add object oFEDESFIN_1_4 as StdField with uid="KSGEGVECOF",rtseq=4,rtrep=.f.,;
    cFormVar = "w_FEDESFIN", cQueryName = "FEDESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 101647524,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=236, Top=39, InputMask=replicate('X',40)

  add object oPDATINIZ_1_5 as StdField with uid="YFFOXSRPIP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PDATINIZ", cQueryName = "PDATINIZ",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale",;
    ToolTipText = "Periodo di ripartizione di inizio selezione",;
    HelpContextID = 226350160,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=94, Top=65, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAL_RIPA", cZoomOnZoom="GSCA_ACA", oKey_1_1="CAPERIOD", oKey_1_2="this.w_PDATINIZ"

  func oPDATINIZ_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDATINIZ_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDATINIZ_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAL_RIPA','*','CAPERIOD',cp_AbsName(this.parent,'oPDATINIZ_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACA',"Calendario di ripartizione",'',this.parent.oContained
  endproc
  proc oPDATINIZ_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CAPERIOD=this.parent.oContained.w_PDATINIZ
     i_obj.ecpSave()
  endproc

  add object oPEDESINI_1_6 as StdField with uid="KVYOXESMSH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PEDESINI", cQueryName = "PEDESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 151979327,;
   bGlobalFont=.t.,;
    Height=21, Width=345, Left=173, Top=65, InputMask=replicate('X',40)


  add object oObj_1_7 as cp_outputCombo with uid="ZLERXIVPWT",left=94, top=127, width=424,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 181304858


  add object oBtn_1_8 as StdButton with uid="SRUBPURMSK",left=416, top=158, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 217512922;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_9 as StdButton with uid="WYNIOGCQBX",left=470, top=158, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 184885830;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oPDATFIN_1_11 as StdField with uid="JFYRKBXUKT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PDATFIN", cQueryName = "PDATFIN",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande della codice finale",;
    ToolTipText = "Periodo di ripartizione di fine selezione",;
    HelpContextID = 139318262,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=94, Top=91, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAL_RIPA", cZoomOnZoom="GSCA_ACA", oKey_1_1="CAPERIOD", oKey_1_2="this.w_PDATFIN"

  func oPDATFIN_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oPDATFIN_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPDATFIN_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAL_RIPA','*','CAPERIOD',cp_AbsName(this.parent,'oPDATFIN_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACA',"Calendario di ripartizione",'',this.parent.oContained
  endproc
  proc oPDATFIN_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CAPERIOD=this.parent.oContained.w_PDATFIN
     i_obj.ecpSave()
  endproc

  add object oPEDESFIN_1_12 as StdField with uid="BNNDQHNZYC",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PEDESFIN", cQueryName = "PEDESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 101647684,;
   bGlobalFont=.t.,;
    Height=21, Width=345, Left=173, Top=91, InputMask=replicate('X',40)

  add object oStr_1_10 as StdString with uid="YYJUMHRPDV",Visible=.t., Left=3, Top=127,;
    Alignment=1, Width=90, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="MOKUKDIDZW",Visible=.t., Left=3, Top=13,;
    Alignment=1, Width=90, Height=15,;
    Caption="Da C. C/R:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="OUMRNJGNKH",Visible=.t., Left=3, Top=39,;
    Alignment=1, Width=90, Height=15,;
    Caption="A C. C/R:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="JMBPOUDWZY",Visible=.t., Left=3, Top=65,;
    Alignment=1, Width=90, Height=15,;
    Caption="Da periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="OGOWCMOQYQ",Visible=.t., Left=3, Top=91,;
    Alignment=1, Width=90, Height=15,;
    Caption="A periodo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsca_scm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
