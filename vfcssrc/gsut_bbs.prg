* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bbs                                                        *
*              Archiviazione da scanner                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-20                                                      *
* Last revis.: 2011-12-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,p_SingMult
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bbs",oParentObject,m.p_SingMult)
return(i_retval)

define class tgsut_bbs as StdBatch
  * --- Local variables
  p_SingMult = space(1)
  w_LcFile = space(10)
  w_ImgHandle = 0
  w_Res = 0
  w_Abort = .f.
  w_RESULT = space(200)
  w_CALLER = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Catturo immagine da Scanner: lancio programma di acquisizione immagini dello scanner
    * --- Inizializzazioni
    this.w_CALLER = iif(type("this.oParentObject")="O",this.oParentObject,_screen)
    this.w_RESULT = ""
     
 DECLARE INTEGER TWAIN_LoadSourceManager IN Eztwain3.DLL 
 DECLARE INTEGER TWAIN_SelectImageSource IN Eztwain3.DLL INTEGER hWnd 
 DECLARE INTEGER TWAIN_GetSourceList IN Eztwain3.DLL 
 DECLARE INTEGER TWAIN_GetNextSourceName IN Eztwain3.DLL STRING @cSourceName 
 DECLARE INTEGER TWAIN_OpenSource IN Eztwain3.DLL STRING @ pStr 
 DECLARE INTEGER TWAIN_AcquireNative IN Eztwain3.DLL INTEGER nAppWind, INTEGER nPixelTypes 
 DECLARE INTEGER TWAIN_WriteNativeToFilename IN Eztwain3.DLL INTEGER nDIB, STRING cFilename 
 DECLARE INTEGER TWAIN_FreeNative IN Eztwain3.DLL INTEGER nDIB 
 DECLARE INTEGER TWAIN_SetMultiTransfer IN Eztwain3.DLL INTEGER nFlag 
 DECLARE LONG TWAIN_BeginMultipageFile IN Eztwain3.DLL STRING sFile 
 DECLARE LONG TWAIN_DibWritePage IN Eztwain3.DLL LONG hdib 
 DECLARE LONG TWAIN_EndMultipageFile IN Eztwain3.DLL 
 DECLARE LONG TWAIN_CloseSource IN Eztwain3.DLL 
 DECLARE LONG PDF_SetPDFACompliance IN Eztwain3.dll LONG nLevel
    * --- File sul quale memorizzero l'immagine acquisita
    if this.p_SingMult = 1
      this.w_LcFile = tempadhoc()+"\"+SYS(2015)+".BMP"
    else
      this.w_LcFile = tempadhoc()+"\"+SYS(2015)+".PDF"
      if VARTYPE(g_CreateStandardPDF)<>"U" AND g_CreateStandardPDF
        PDF_SetPDFACompliance(0)
      else
        PDF_SetPDFACompliance(1)
      endif
    endif
    * --- Seleziono la periferica di acquisizione immagine
    if Type("g_SCANNER")<>"U" And Not(Empty(g_SCANNER)) And TWAIN_OpenSource(Alltrim(g_SCANNER)) = 1
      this.w_Res = 1
    else
      this.w_Res = TWAIN_SelectImageSource(this.w_CALLER.hWnd)
    endif
    * --- Se = 1 ho selezionato il dispositivo
    if this.w_Res <>0
      * --- Acquisisco l'immagine
      if this.p_SingMult = 1
        this.w_ImgHandle = TWAIN_AcquireNative(this.w_CALLER.hWnd,0)
        * --- Salvo l'immagine con il nome del file sopra creato
        this.w_Res = TWAIN_WriteNativeToFilename(this.w_ImgHandle,this.w_LcFile)
        * --- Rilascio il file 
        TWAIN_FreeNative(this.w_ImgHandle)
      else
        this.w_Abort = .t.
        TWAIN_SetMultiTransfer(1)
        this.w_Res = TWAIN_BeginMultipageFile(this.w_LcFile)
        do while this.w_Res = 0
          this.w_ImgHandle = TWAIN_AcquireNative(0,0)
          if this.w_ImgHandle = 0
            Exit
          endif
          this.w_Abort = .f.
          this.w_Res = TWAIN_DibWritePage(this.w_ImgHandle)
        enddo
        TWAIN_SetMultiTransfer(0)
        this.w_Res = min(this.w_Res, TWAIN_EndMultipageFile())
        TWAIN_CloseSource()
      endif
      if this.w_Res <> 0 Or this.w_Abort
        * --- File non acquisito o non accedibile
        if File(alltrim(this.w_LcFile))
          Erase(this.w_LcFile)
        endif
        i_retcode = 'stop'
        return
      endif
      this.w_RESULT = Alltrim(this.w_LcFile)
    else
      i_retcode = 'stop'
      return
    endif
    * --- Risultato
    i_retcode = 'stop'
    i_retval = this.w_RESULT
    return
  endproc


  proc Init(oParentObject,p_SingMult)
    this.p_SingMult=p_SingMult
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="p_SingMult"
endproc
