* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bka                                                        *
*              Valorizzazione attributo                                        *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-03-23                                                      *
* Last revis.: 2010-03-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bka",oParentObject)
return(i_retval)

define class tgsut_bka as StdBatch
  * --- Local variables
  w_VALORE = space(150)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzo l'attributo con i valori presenti nei campi chiave
    for i = 1 to this.oParentObject.numchiavi
    j=alltrim(str(i))
    this.w_VALORE = this.w_VALORE + alltrim(this.oParentObject.w_CHIAVE&j)
    next
    this.oParentObject.oParentObject.set("w_IDVALATT",alltrim(this.w_VALORE),.f.,.t.)
    this.oParentObject.oParentObject.Notifyevent("w_IDVALATT Changed")
    * --- Chiudo la maschera dello zoom
    this.oParentObject.ecpQuit()
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
