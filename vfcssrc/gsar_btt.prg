* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_btt                                                        *
*              Legge codice contatto                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-04-22                                                      *
* Last revis.: 2008-11-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_btt",oParentObject,m.pOper)
return(i_retval)

define class tgsar_btt as StdBatch
  * --- Local variables
  pOper = space(10)
  w_ObjMCN = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * ---                             Trova il codice del contatto pi� elevato
    do case
      case this.pOper=="LETTURA"
        if IsAlt()
          * --- Contatti
          this.w_ObjMCN = this.oParentObject
          this.w_ObjMCN.MarKPos()     
          SELECT t_NCCODCON FROM (this.w_ObjMCN.cTrsName) INTO CURSOR APPO ORDER BY t_NCCODCON DESC
          SELECT APPO
          GO TOP
          * --- Se non � un numero
          if VAL(t_NCCODCON) = 0
            DO WHILE NOT EOF()
            * --- Se � un numero
            if VAL(t_NCCODCON) # 0
              EXIT
            endif
            SKIP
            ENDDO
          endif
          this.oParentObject.w_PROGR = VAL(t_NCCODCON)
          if USED("APPO")
            SELECT APPO
            USE
          endif
          this.w_ObjMCN.RePos()     
        endif
      case this.pOper=="RESETTA"
        if IsAlt()
          this.w_ObjMCN = this.oParentObject.GSAR_MCN
          if VARTYPE(this.w_ObjMCN.w_PROGR)#"U"
            this.w_ObjMCN.w_PROGR = 1
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
