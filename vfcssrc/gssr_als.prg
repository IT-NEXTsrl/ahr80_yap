* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gssr_als                                                        *
*              Log elaborazioni storico                                        *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_16]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-02-15                                                      *
* Last revis.: 2006-11-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgssr_als"))

* --- Class definition
define class tgssr_als as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 556
  Height = 289+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2006-11-29"
  HelpContextID=227108201
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  LOG_STOR_IDX = 0
  ESERCIZI_IDX = 0
  cpusers_IDX = 0
  cFile = "LOG_STOR"
  cKeySelect = "LSSERIAL"
  cKeyWhere  = "LSSERIAL=this.w_LSSERIAL"
  cKeyWhereODBC = '"LSSERIAL="+cp_ToStrODBC(this.w_LSSERIAL)';

  cKeyWhereODBCqualified = '"LOG_STOR.LSSERIAL="+cp_ToStrODBC(this.w_LSSERIAL)';

  cPrg = "gssr_als"
  cComment = "Log elaborazioni storico"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CODAZI = space(4)
  w_LSSERIAL = space(10)
  w_LG__TIPO = space(1)
  w_LG___OPE = space(1)
  w_LSCODESE = space(4)
  w_LG__DATA = ctod('  /  /  ')
  w_LGCODUTE = 0
  w_LG__NOTE = space(0)
  w_DESUTE = space(20)
  w_LG_ESITO = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_LSSERIAL = this.W_LSSERIAL
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'LOG_STOR','gssr_als')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgssr_alsPag1","gssr_als",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Log elaborazioni storico")
      .Pages(1).HelpContextID = 194297312
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oLSSERIAL_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='cpusers'
    this.cWorkTables[3]='LOG_STOR'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.LOG_STOR_IDX,5],7]
    this.nPostItConn=i_TableProp[this.LOG_STOR_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_LSSERIAL = NVL(LSSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from LOG_STOR where LSSERIAL=KeySet.LSSERIAL
    *
    i_nConn = i_TableProp[this.LOG_STOR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOG_STOR_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('LOG_STOR')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "LOG_STOR.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' LOG_STOR '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'LSSERIAL',this.w_LSSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_DESUTE = space(20)
        .w_LSSERIAL = NVL(LSSERIAL,space(10))
        .op_LSSERIAL = .w_LSSERIAL
        .w_LG__TIPO = NVL(LG__TIPO,space(1))
        .w_LG___OPE = NVL(LG___OPE,space(1))
        .w_LSCODESE = NVL(LSCODESE,space(4))
          * evitabile
          *.link_1_5('Load')
        .w_LG__DATA = NVL(cp_ToDate(LG__DATA),ctod("  /  /  "))
        .w_LGCODUTE = NVL(LGCODUTE,0)
          .link_1_7('Load')
        .w_LG__NOTE = NVL(LG__NOTE,space(0))
        .w_LG_ESITO = NVL(LG_ESITO,space(1))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'LOG_STOR')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI = space(4)
      .w_LSSERIAL = space(10)
      .w_LG__TIPO = space(1)
      .w_LG___OPE = space(1)
      .w_LSCODESE = space(4)
      .w_LG__DATA = ctod("  /  /  ")
      .w_LGCODUTE = 0
      .w_LG__NOTE = space(0)
      .w_DESUTE = space(20)
      .w_LG_ESITO = space(1)
      if .cFunction<>"Filter"
        .w_CODAZI = i_CODAZI
        .DoRTCalc(2,5,.f.)
          if not(empty(.w_LSCODESE))
          .link_1_5('Full')
          endif
        .DoRTCalc(6,7,.f.)
          if not(empty(.w_LGCODUTE))
          .link_1_7('Full')
          endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'LOG_STOR')
    this.DoRTCalc(8,10,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.LOG_STOR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOG_STOR_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"STLOG","i_codazi,w_LSSERIAL")
      .op_codazi = .w_codazi
      .op_LSSERIAL = .w_LSSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oLSSERIAL_1_2.enabled = i_bVal
      .Page1.oPag.oLG__NOTE_1_14.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oLSSERIAL_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oLSSERIAL_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'LOG_STOR',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.LOG_STOR_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LSSERIAL,"LSSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LG__TIPO,"LG__TIPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LG___OPE,"LG___OPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LSCODESE,"LSCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LG__DATA,"LG__DATA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LGCODUTE,"LGCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LG__NOTE,"LG__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_LG_ESITO,"LG_ESITO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.LOG_STOR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOG_STOR_IDX,2])
    i_lTable = "LOG_STOR"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.LOG_STOR_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      vx_exec("QUERY\GSSR_ALS.VQR",this)
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.LOG_STOR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LOG_STOR_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.LOG_STOR_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"STLOG","i_codazi,w_LSSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into LOG_STOR
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'LOG_STOR')
        i_extval=cp_InsertValODBCExtFlds(this,'LOG_STOR')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(LSSERIAL,LG__TIPO,LG___OPE,LSCODESE,LG__DATA"+;
                  ",LGCODUTE,LG__NOTE,LG_ESITO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_LSSERIAL)+;
                  ","+cp_ToStrODBC(this.w_LG__TIPO)+;
                  ","+cp_ToStrODBC(this.w_LG___OPE)+;
                  ","+cp_ToStrODBCNull(this.w_LSCODESE)+;
                  ","+cp_ToStrODBC(this.w_LG__DATA)+;
                  ","+cp_ToStrODBCNull(this.w_LGCODUTE)+;
                  ","+cp_ToStrODBC(this.w_LG__NOTE)+;
                  ","+cp_ToStrODBC(this.w_LG_ESITO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'LOG_STOR')
        i_extval=cp_InsertValVFPExtFlds(this,'LOG_STOR')
        cp_CheckDeletedKey(i_cTable,0,'LSSERIAL',this.w_LSSERIAL)
        INSERT INTO (i_cTable);
              (LSSERIAL,LG__TIPO,LG___OPE,LSCODESE,LG__DATA,LGCODUTE,LG__NOTE,LG_ESITO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_LSSERIAL;
                  ,this.w_LG__TIPO;
                  ,this.w_LG___OPE;
                  ,this.w_LSCODESE;
                  ,this.w_LG__DATA;
                  ,this.w_LGCODUTE;
                  ,this.w_LG__NOTE;
                  ,this.w_LG_ESITO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.LOG_STOR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LOG_STOR_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.LOG_STOR_IDX,i_nConn)
      *
      * update LOG_STOR
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'LOG_STOR')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " LG__TIPO="+cp_ToStrODBC(this.w_LG__TIPO)+;
             ",LG___OPE="+cp_ToStrODBC(this.w_LG___OPE)+;
             ",LSCODESE="+cp_ToStrODBCNull(this.w_LSCODESE)+;
             ",LG__DATA="+cp_ToStrODBC(this.w_LG__DATA)+;
             ",LGCODUTE="+cp_ToStrODBCNull(this.w_LGCODUTE)+;
             ",LG__NOTE="+cp_ToStrODBC(this.w_LG__NOTE)+;
             ",LG_ESITO="+cp_ToStrODBC(this.w_LG_ESITO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'LOG_STOR')
        i_cWhere = cp_PKFox(i_cTable  ,'LSSERIAL',this.w_LSSERIAL  )
        UPDATE (i_cTable) SET;
              LG__TIPO=this.w_LG__TIPO;
             ,LG___OPE=this.w_LG___OPE;
             ,LSCODESE=this.w_LSCODESE;
             ,LG__DATA=this.w_LG__DATA;
             ,LGCODUTE=this.w_LGCODUTE;
             ,LG__NOTE=this.w_LG__NOTE;
             ,LG_ESITO=this.w_LG_ESITO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.LOG_STOR_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.LOG_STOR_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.LOG_STOR_IDX,i_nConn)
      *
      * delete LOG_STOR
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'LSSERIAL',this.w_LSSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.LOG_STOR_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.LOG_STOR_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
          .link_1_5('Full')
        .DoRTCalc(6,6,.t.)
          .link_1_7('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"STLOG","i_codazi,w_LSSERIAL")
          .op_LSSERIAL = .w_LSSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(8,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=LSCODESE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LSCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LSCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_LSCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_LSCODESE)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LSCODESE = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_LSCODESE = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LSCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=LGCODUTE
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_LGCODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_LGCODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_LGCODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_LGCODUTE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_LGCODUTE = NVL(_Link_.code,0)
      this.w_DESUTE = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_LGCODUTE = 0
      endif
      this.w_DESUTE = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_LGCODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oLSSERIAL_1_2.value==this.w_LSSERIAL)
      this.oPgFrm.Page1.oPag.oLSSERIAL_1_2.value=this.w_LSSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oLG__TIPO_1_3.RadioValue()==this.w_LG__TIPO)
      this.oPgFrm.Page1.oPag.oLG__TIPO_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLG___OPE_1_4.RadioValue()==this.w_LG___OPE)
      this.oPgFrm.Page1.oPag.oLG___OPE_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLSCODESE_1_5.value==this.w_LSCODESE)
      this.oPgFrm.Page1.oPag.oLSCODESE_1_5.value=this.w_LSCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oLG__DATA_1_6.value==this.w_LG__DATA)
      this.oPgFrm.Page1.oPag.oLG__DATA_1_6.value=this.w_LG__DATA
    endif
    if not(this.oPgFrm.Page1.oPag.oLGCODUTE_1_7.value==this.w_LGCODUTE)
      this.oPgFrm.Page1.oPag.oLGCODUTE_1_7.value=this.w_LGCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oLG__NOTE_1_14.value==this.w_LG__NOTE)
      this.oPgFrm.Page1.oPag.oLG__NOTE_1_14.value=this.w_LG__NOTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_16.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_16.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oLG_ESITO_1_17.RadioValue()==this.w_LG_ESITO)
      this.oPgFrm.Page1.oPag.oLG_ESITO_1_17.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'LOG_STOR')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgssr_alsPag1 as StdContainer
  Width  = 552
  height = 289
  stdWidth  = 552
  stdheight = 289
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oLSSERIAL_1_2 as StdField with uid="VTMZDXSQQZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_LSSERIAL", cQueryName = "LSSERIAL",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Seriale",;
    HelpContextID = 14754562,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=64, Top=19, InputMask=replicate('X',10)


  add object oLG__TIPO_1_3 as StdCombo with uid="QUSJUGFAEO",rtseq=3,rtrep=.f.,left=199,top=19,width=121,height=21, enabled=.f.;
    , HelpContextID = 249833723;
    , cFormVar="w_LG__TIPO",RowSource=""+"Contabile,"+"Logistica,"+"Produzione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oLG__TIPO_1_3.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'L',;
    iif(this.value =3,'P',;
    space(1)))))
  endfunc
  func oLG__TIPO_1_3.GetRadio()
    this.Parent.oContained.w_LG__TIPO = this.RadioValue()
    return .t.
  endfunc

  func oLG__TIPO_1_3.SetRadio()
    this.Parent.oContained.w_LG__TIPO=trim(this.Parent.oContained.w_LG__TIPO)
    this.value = ;
      iif(this.Parent.oContained.w_LG__TIPO=='C',1,;
      iif(this.Parent.oContained.w_LG__TIPO=='L',2,;
      iif(this.Parent.oContained.w_LG__TIPO=='P',3,;
      0)))
  endfunc


  add object oLG___OPE_1_4 as StdCombo with uid="WGKQARIQDZ",rtseq=4,rtrep=.f.,left=411,top=19,width=112,height=21, enabled=.f.;
    , HelpContextID = 137636101;
    , cFormVar="w_LG___OPE",RowSource=""+"Storico,"+"Analisi", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oLG___OPE_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'A',;
    space(1))))
  endfunc
  func oLG___OPE_1_4.GetRadio()
    this.Parent.oContained.w_LG___OPE = this.RadioValue()
    return .t.
  endfunc

  func oLG___OPE_1_4.SetRadio()
    this.Parent.oContained.w_LG___OPE=trim(this.Parent.oContained.w_LG___OPE)
    this.value = ;
      iif(this.Parent.oContained.w_LG___OPE=='S',1,;
      iif(this.Parent.oContained.w_LG___OPE=='A',2,;
      0))
  endfunc

  add object oLSCODESE_1_5 as StdField with uid="PNFMYBVIUX",rtseq=5,rtrep=.f.,;
    cFormVar = "w_LSCODESE", cQueryName = "LSCODESE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    HelpContextID = 201990907,;
   bGlobalFont=.t.,;
    Height=21, Width=50, Left=64, Top=49, InputMask=replicate('X',4), cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_LSCODESE"

  func oLSCODESE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oLG__DATA_1_6 as StdField with uid="VTUBVENIIT",rtseq=6,rtrep=.f.,;
    cFormVar = "w_LG__DATA", cQueryName = "LG__DATA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 136042231,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=194, Top=49

  add object oLGCODUTE_1_7 as StdField with uid="KKNGIARICJ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_LGCODUTE", cQueryName = "LGCODUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 201987835,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=348, Top=49, cSayPict='"9999"', cGetPict='"9999"', cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_LGCODUTE"

  func oLGCODUTE_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oLG__NOTE_1_14 as StdMemo with uid="KSLSCOKVTW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_LG__NOTE", cQueryName = "LG__NOTE",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 112973563,;
   bGlobalFont=.t.,;
    Height=166, Width=539, Left=8, Top=96, tabstop = .f., readonly = .t.

  add object oDESUTE_1_16 as StdField with uid="DLVHQWOXAE",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    HelpContextID = 49212362,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=394, Top=49, InputMask=replicate('X',20)


  add object oLG_ESITO_1_17 as StdCombo with uid="LEWGDSMFJG",rtseq=10,rtrep=.f.,left=404,top=265,width=112,height=21, enabled=.f.;
    , ToolTipText = "Esito operazione";
    , HelpContextID = 15849221;
    , cFormVar="w_LG_ESITO",RowSource=""+"Positivo,"+"Negativo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oLG_ESITO_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oLG_ESITO_1_17.GetRadio()
    this.Parent.oContained.w_LG_ESITO = this.RadioValue()
    return .t.
  endfunc

  func oLG_ESITO_1_17.SetRadio()
    this.Parent.oContained.w_LG_ESITO=trim(this.Parent.oContained.w_LG_ESITO)
    this.value = ;
      iif(this.Parent.oContained.w_LG_ESITO=='S',1,;
      iif(this.Parent.oContained.w_LG_ESITO=='N',2,;
      0))
  endfunc

  add object oStr_1_8 as StdString with uid="JWUNDKWUUQ",Visible=.t., Left=8, Top=19,;
    Alignment=1, Width=51, Height=18,;
    Caption="Seriale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="PACYOIBHJP",Visible=.t., Left=5, Top=49,;
    Alignment=1, Width=54, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="JHIBFAZLHY",Visible=.t., Left=154, Top=19,;
    Alignment=1, Width=39, Height=18,;
    Caption="Area:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="DIGRVJQDXX",Visible=.t., Left=327, Top=19,;
    Alignment=1, Width=76, Height=18,;
    Caption="Operazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="BIDOOKUNVS",Visible=.t., Left=288, Top=49,;
    Alignment=1, Width=53, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="YFQNXPNACI",Visible=.t., Left=119, Top=49,;
    Alignment=1, Width=69, Height=18,;
    Caption="Data elab.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="QCWDOELKOR",Visible=.t., Left=8, Top=76,;
    Alignment=0, Width=145, Height=18,;
    Caption="Messaggi elaborazione"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="XLHFZELRCM",Visible=.t., Left=320, Top=265,;
    Alignment=1, Width=76, Height=18,;
    Caption="Esito:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gssr_als','LOG_STOR','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".LSSERIAL=LOG_STOR.LSSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
