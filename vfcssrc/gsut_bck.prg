* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bck                                                        *
*              Visualizza/carica output utente                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-12                                                      *
* Last revis.: 2010-10-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bck",oParentObject,m.pAzione)
return(i_retval)

define class tgsut_bck as StdBatch
  * --- Local variables
  pAzione = space(2)
  NC = space(10)
  w_POS = 0
  w_ERRORE = space(200)
  w_ERR_MSG = space(200)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- parametro
    * --- Nome cursore collegato allo zoom
    Create cursor OUTPUTINF (OUNOMPRG char(30) , OUDESPRG char (35) , OUROWNUM NUMERIC(3,0), OUDESCRI char(100) ,; 
 OUCODUTE numeric (4,0), OUCODAZI char (5) , OUNOMQUE char (254), OUNOMREP char (254),OUPREDEF char (1) ,OUSTESTO char (1),; 
 OUFLGQUE char (1) ,OUNOMBAT char (254) ,OUCARMEM numeric (3,0) ,OUTIMSTA char (14),OUISOLAN char(50) , APPLICAZIONE cahr(3),MOTORE char(3) ,LOCALIZZAZIONE char (3))
    * --- Test parametro
    do case
      case this.pAzione = "CARICA"
        this.w_ERR_MSG = OutputdaInf (this.oParentObject.w_PATH,.F. )
        if ! EMPTY (this.w_ERR_MSG)
          ah_errormsg( this.w_ERR_MSG)
        else
          ah_errormsg( "Elaborazione terminata con successo")
        endif
      case this.pAzione = "LEGGI"
        this.w_ERR_MSG = LEGGIINF (this.oParentObject.w_PATH, "OUTPUTINF" )
        if ! EMPTY (this.w_ERR_MSG)
          ah_errormsg( this.w_ERR_MSG)
        endif
        UPDATE OUTPUTINF SET APPLICAZIONE="Tutti" where EMPTY (NVL(APPLICAZIONE,""))
        UPDATE OUTPUTINF SET MOTORE="Tutti" where EMPTY (NVL(MOTORE,""))
        CURTOTAB ( "OUTPUTINF" ,"RIPATMP1")
        this.oParentObject.notifyevent ("aggiorna")
      case this.pAzione = "PATH"
        this.oParentObject.w_PATH = GETFILE()
      case this.pAzione = "END"
        * --- Drop temporary table RIPATMP1
        i_nIdx=cp_GetTableDefIdx('RIPATMP1')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('RIPATMP1')
        endif
      case this.pAzione = "INIT"
        * --- Crea tabella temporanea per lo zoom di visualizzazione
        CURTOTAB ( "OUTPUTINF" ,"RIPATMP1")
    endcase
    if USED ("OUTPUTINF")
      SELECT OUTPUTINF 
 USE
    endif
  endproc


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
