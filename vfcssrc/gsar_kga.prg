* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kga                                                        *
*              Gestione utenti e gruppi struttura aziendale                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-09-02                                                      *
* Last revis.: 2011-05-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kga",oParentObject))

* --- Class definition
define class tgsar_kga as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 785
  Height = 560
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-06"
  HelpContextID=236357783
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsar_kga"
  cComment = "Gestione utenti e gruppi struttura aziendale"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_NRGRUSEL = 0
  o_NRGRUSEL = 0
  w_NRPERSEL = 0
  w_FLRESP = .F.
  w_FLGRUPRE = .F.
  o_FLGRUPRE = .F.
  w_FLGFORCE = .F.
  w_PERSONE = .NULL.
  w_GRUPPI = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kgaPag1","gsar_kga",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFLRESP_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_PERSONE = this.oPgFrm.Pages(1).oPag.PERSONE
    this.w_GRUPPI = this.oPgFrm.Pages(1).oPag.GRUPPI
    DoDefault()
    proc Destroy()
      this.w_PERSONE = .NULL.
      this.w_GRUPPI = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NRGRUSEL=0
      .w_NRPERSEL=0
      .w_FLRESP=.f.
      .w_FLGRUPRE=.f.
      .w_FLGFORCE=.f.
      .oPgFrm.Page1.oPag.PERSONE.Calculate()
      .oPgFrm.Page1.oPag.GRUPPI.Calculate()
        .w_NRGRUSEL = 0
        .w_NRPERSEL = 0
        .w_FLRESP = .F.
        .w_FLGRUPRE = .F.
        .w_FLGFORCE = IIF(.w_FLGRUPRE, .w_FLGFORCE, .F.)
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.PERSONE.Calculate()
        .oPgFrm.Page1.oPag.GRUPPI.Calculate()
        .DoRTCalc(1,4,.t.)
        if .o_FLGRUPRE<>.w_FLGRUPRE.or. .o_NRGRUSEL<>.w_NRGRUSEL
            .w_FLGFORCE = IIF(.w_FLGRUPRE, .w_FLGFORCE, .F.)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.PERSONE.Calculate()
        .oPgFrm.Page1.oPag.GRUPPI.Calculate()
    endwith
  return

  proc Calculate_KCPMJHNGXY()
    with this
          * --- VERIFICHE SELEZIONE/DESELEZIONE GRUPPI - GSAR_BKG(CUGRU)
          gsar_bkg(this;
              ,'CUGRU';
             )
    endwith
  endproc
  proc Calculate_PZIOFLNEVY()
    with this
          * --- VERIFICHE SELEZIONE/DESELEZIONE PERSONE - GSAR_BKG(CUPER)
          gsar_bkg(this;
              ,'CUPER';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFLRESP_1_5.enabled = this.oPgFrm.Page1.oPag.oFLRESP_1_5.mCond()
    this.oPgFrm.Page1.oPag.oFLGRUPRE_1_6.enabled = this.oPgFrm.Page1.oPag.oFLGRUPRE_1_6.mCond()
    this.oPgFrm.Page1.oPag.oFLGFORCE_1_7.enabled = this.oPgFrm.Page1.oPag.oFLGFORCE_1_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.PERSONE.Event(cEvent)
      .oPgFrm.Page1.oPag.GRUPPI.Event(cEvent)
        if lower(cEvent)==lower("w_GRUPPI row checked") or lower(cEvent)==lower("w_GRUPPI row unchecked") or lower(cEvent)==lower("ModZoomGruppi")
          .Calculate_KCPMJHNGXY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_PERSONE row checked") or lower(cEvent)==lower("w_PERSONE row unchecked") or lower(cEvent)==lower("ModZoomPersone")
          .Calculate_PZIOFLNEVY()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oFLRESP_1_5.RadioValue()==this.w_FLRESP)
      this.oPgFrm.Page1.oPag.oFLRESP_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGRUPRE_1_6.RadioValue()==this.w_FLGRUPRE)
      this.oPgFrm.Page1.oPag.oFLGRUPRE_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGFORCE_1_7.RadioValue()==this.w_FLGFORCE)
      this.oPgFrm.Page1.oPag.oFLGFORCE_1_7.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_NRGRUSEL = this.w_NRGRUSEL
    this.o_FLGRUPRE = this.w_FLGRUPRE
    return

enddefine

* --- Define pages as container
define class tgsar_kgaPag1 as StdContainer
  Width  = 781
  height = 560
  stdWidth  = 781
  stdheight = 560
  resizeXpos=353
  resizeYpos=256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object PERSONE as cp_szoombox with uid="NQTXYAXDUI",left=12, top=19, width=375,height=463,;
    caption='PERSONE',;
   bGlobalFont=.t.,;
    cZoomFile="GSAR_KGA",bOptions=.f.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="GSAR_BDZ",cTable="DIPENDEN",;
    nPag=1;
    , HelpContextID = 22999798


  add object GRUPPI as cp_szoombox with uid="XWFZWAYHJV",left=392, top=19, width=375,height=463,;
    caption='GRUPPI',;
   bGlobalFont=.t.,;
    cZoomFile="GSARGADP",bOptions=.f.,bAdvOptions=.t.,bReadOnly=.t.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="GSAR_BDZ",cTable="DIPENDEN",;
    nPag=1;
    , HelpContextID = 60018842

  add object oFLRESP_1_5 as StdCheck with uid="CJTAFPDSWE",rtseq=3,rtrep=.f.,left=10, top=485, caption="Responsabili",;
    HelpContextID = 208602794,;
    cFormVar="w_FLRESP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLRESP_1_5.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oFLRESP_1_5.GetRadio()
    this.Parent.oContained.w_FLRESP = this.RadioValue()
    return .t.
  endfunc

  func oFLRESP_1_5.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLRESP==.T.,1,;
      0)
  endfunc

  func oFLRESP_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NRPERSEL>0)
    endwith
   endif
  endfunc

  add object oFLGRUPRE_1_6 as StdCheck with uid="LUZEVZROOW",rtseq=4,rtrep=.f.,left=392, top=485, caption="Gruppo predefinito",;
    ToolTipText = "Se attivo il gruppo selezionato verr� impostato come predefinito per le persone selezionate",;
    HelpContextID = 205698661,;
    cFormVar="w_FLGRUPRE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLGRUPRE_1_6.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oFLGRUPRE_1_6.GetRadio()
    this.Parent.oContained.w_FLGRUPRE = this.RadioValue()
    return .t.
  endfunc

  func oFLGRUPRE_1_6.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLGRUPRE==.T.,1,;
      0)
  endfunc

  func oFLGRUPRE_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NRGRUSEL=1)
    endwith
   endif
  endfunc

  add object oFLGFORCE_1_7 as StdCheck with uid="TREJIHFEYG",rtseq=5,rtrep=.f.,left=576, top=485, caption="Forza gruppo predefinito",;
    ToolTipText = "Se attivo forza il gruppo come predefinito anche alle persone con gruppo predefinito gi� assegnato",;
    HelpContextID = 89213339,;
    cFormVar="w_FLGFORCE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLGFORCE_1_7.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oFLGFORCE_1_7.GetRadio()
    this.Parent.oContained.w_FLGFORCE = this.RadioValue()
    return .t.
  endfunc

  func oFLGFORCE_1_7.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLGFORCE==.T.,1,;
      0)
  endfunc

  func oFLGFORCE_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_NRGRUSEL=1 AND .w_FLGRUPRE)
    endwith
   endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="ZMIZZNTUDJ",left=671, top=508, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 245358058;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      with this.Parent.oContained
        GSAR_BKG(this.Parent.oContained,"AGGIO")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_9 as StdButton with uid="JWIBHYOYEX",left=723, top=508, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 243675206;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="XOMFIZBJJN",left=10, top=508, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutte le persone";
    , HelpContextID = 209567210;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSAR_BKG(this.Parent.oContained,"SELPE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="TRGQAKKHMN",left=58, top=508, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutte le persone";
    , HelpContextID = 209567210;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        GSAR_BKG(this.Parent.oContained,"DESPE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="WVNDCXPDGA",left=106, top=508, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per invertite la selezione delle persone";
    , HelpContextID = 209567210;
    , caption='\<Inv. Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      with this.Parent.oContained
        GSAR_BKG(this.Parent.oContained,"INVPE")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_17 as StdButton with uid="ZAZNELCYSU",left=397, top=508, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutti i gruppi";
    , HelpContextID = 209567210;
    , caption='Se\<leziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSAR_BKG(this.Parent.oContained,"SELGR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="LWZEEUNMUE",left=445, top=508, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutti i gruppi";
    , HelpContextID = 209567210;
    , caption='Desele\<z.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        GSAR_BKG(this.Parent.oContained,"DESGR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_19 as StdButton with uid="FDEEXBIFWW",left=493, top=508, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per invertite la selezione dei gruppi";
    , HelpContextID = 209567210;
    , caption='I\<nv. Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        GSAR_BKG(this.Parent.oContained,"INVGR")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_10 as StdString with uid="OAPNWTHIEA",Visible=.t., Left=12, Top=4,;
    Alignment=0, Width=210, Height=18,;
    Caption="Persone / Risorse"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="FRERIIZCQC",Visible=.t., Left=397, Top=4,;
    Alignment=0, Width=210, Height=18,;
    Caption="Gruppi"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kga','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
