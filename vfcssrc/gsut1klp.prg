* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut1klp                                                        *
*              Leggi risposta PEC                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-11-19                                                      *
* Last revis.: 2014-10-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut1klp",oParentObject))

* --- Class definition
define class tgsut1klp as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 459
  Height = 288+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-07"
  HelpContextID=48807063
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  UTEMAILS_IDX = 0
  AUT_LPE_IDX = 0
  CONTROPA_IDX = 0
  cpusers_IDX = 0
  cPrg = "gsut1klp"
  cComment = "Leggi risposta PEC"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODUTE = 0
  w_READAZI = space(10)
  w_LOG = space(254)
  w_CODUID = space(100)
  w_DESUTE = space(40)
  w_UTDATINV = ctot('')
  w_UTFILMIT = space(254)
  w_UTFILOGG = space(100)
  w_ZOOMUID = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut1klpPag1","gsut1klp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Leggi")
      .Pages(2).addobject("oPag","tgsut1klpPag2","gsut1klp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Lista UID")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODUTE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMUID = this.oPgFrm.Pages(2).oPag.ZOOMUID
    DoDefault()
    proc Destroy()
      this.w_ZOOMUID = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='UTEMAILS'
    this.cWorkTables[2]='AUT_LPE'
    this.cWorkTables[3]='CONTROPA'
    this.cWorkTables[4]='cpusers'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do SaveLeggi with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODUTE=0
      .w_READAZI=space(10)
      .w_LOG=space(254)
      .w_CODUID=space(100)
      .w_DESUTE=space(40)
      .w_UTDATINV=ctot("")
      .w_UTFILMIT=space(254)
      .w_UTFILOGG=space(100)
        .w_CODUTE = i_codute
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODUTE))
          .link_1_1('Full')
        endif
        .w_READAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_READAZI))
          .link_1_3('Full')
        endif
      .oPgFrm.Page2.oPag.ZOOMUID.Calculate()
          .DoRTCalc(3,3,.f.)
        .w_CODUID = .w_ZOOMUID.Getvar('AUCODUID')
    endwith
    this.DoRTCalc(5,8,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_READAZI = i_CODAZI
          .link_1_3('Full')
        .oPgFrm.Page2.oPag.ZOOMUID.Calculate()
        .DoRTCalc(3,3,.t.)
            .w_CODUID = .w_ZOOMUID.Getvar('AUCODUID')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.ZOOMUID.Calculate()
    endwith
  return

  proc Calculate_DVGCPGZYMB()
    with this
          * --- Apre archivio
          .a = opengest('A','GSUT_AUI','AUCODUID',.w_CODUID)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.ZOOMUID.Event(cEvent)
        if lower(cEvent)==lower("w_ZOOMUID selected")
          .Calculate_DVGCPGZYMB()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODUTE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " CODE="+cp_ToStrODBC(this.w_CODUTE);

          i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CODE',this.w_CODUTE)
          select CODE,NAME;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODUTE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','CODE',cp_AbsName(oSource.parent,'oCODUTE_1_1'),i_cWhere,'',"Elenco utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                     +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',oSource.xKey(1))
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_CODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_CODUTE)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE = NVL(_Link_.CODE,0)
      this.w_DESUTE = NVL(_Link_.NAME,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE = 0
      endif
      this.w_DESUTE = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=READAZI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_READAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_READAZI)
            select COCODAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READAZI = NVL(_Link_.COCODAZI,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_READAZI = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODUTE_1_1.value==this.w_CODUTE)
      this.oPgFrm.Page1.oPag.oCODUTE_1_1.value=this.w_CODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUTE_1_6.value==this.w_DESUTE)
      this.oPgFrm.Page1.oPag.oDESUTE_1_6.value=this.w_DESUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oUTDATINV_1_7.value==this.w_UTDATINV)
      this.oPgFrm.Page1.oPag.oUTDATINV_1_7.value=this.w_UTDATINV
    endif
    if not(this.oPgFrm.Page1.oPag.oUTFILMIT_1_8.value==this.w_UTFILMIT)
      this.oPgFrm.Page1.oPag.oUTFILMIT_1_8.value=this.w_UTFILMIT
    endif
    if not(this.oPgFrm.Page1.oPag.oUTFILOGG_1_9.value==this.w_UTFILOGG)
      this.oPgFrm.Page1.oPag.oUTFILOGG_1_9.value=this.w_UTFILOGG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut1klpPag1 as StdContainer
  Width  = 456
  height = 288
  stdWidth  = 456
  stdheight = 288
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODUTE_1_1 as StdField with uid="RNLKBMRAQT",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODUTE", cQueryName = "CODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente",;
    HelpContextID = 41791450,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=91, Top=25, bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="CODE", oKey_1_2="this.w_CODUTE"

  func oCODUTE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUTE_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUTE_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','CODE',cp_AbsName(this.parent,'oCODUTE_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco utenti",'',this.parent.oContained
  endproc


  add object oBtn_1_2 as StdButton with uid="KBNKTNIFDT",left=406, top=239, width=48,height=45,;
    CpPicture="bmp\fxmail.ico", caption="", nPag=1;
    , HelpContextID = 166106806;
    , caption='L\<eggi';
    , FontName = "Arial Narrow", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_2.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESUTE_1_6 as StdField with uid="IOFBDZFXOA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DESUTE", cQueryName = "DESUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 41732554,;
   bGlobalFont=.t.,;
    Height=21, Width=246, Left=169, Top=25, InputMask=replicate('X',40)

  add object oUTDATINV_1_7 as StdField with uid="UJXTNUZLXB",rtseq=6,rtrep=.f.,;
    cFormVar = "w_UTDATINV", cQueryName = "UTDATINV",;
    bObbl = .f. , nPag = 1, value=ctot(""), bMultilanguage =  .f.,;
    ToolTipText = "FIltro data invio",;
    HelpContextID = 244427108,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=100, Top=125

  add object oUTFILMIT_1_8 as StdField with uid="POWQXFIFLW",rtseq=7,rtrep=.f.,;
    cFormVar = "w_UTFILMIT", cQueryName = "UTFILMIT",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Mittente",;
    HelpContextID = 83261082,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=100, Top=156, InputMask=replicate('X',254)

  add object oUTFILOGG_1_9 as StdField with uid="HVPVGUUGNN",rtseq=8,rtrep=.f.,;
    cFormVar = "w_UTFILOGG", cQueryName = "UTFILOGG",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Oggetto risposta",;
    HelpContextID = 116815501,;
   bGlobalFont=.t.,;
    Height=21, Width=356, Left=100, Top=188, InputMask=replicate('X',100)

  add object oStr_1_5 as StdString with uid="ZCTEIFVLNY",Visible=.t., Left=49, Top=29,;
    Alignment=1, Width=42, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="SCQIXLCIMN",Visible=.t., Left=7, Top=128,;
    Alignment=1, Width=93, Height=18,;
    Caption="Data invio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="BKLUCXOVWZ",Visible=.t., Left=51, Top=159,;
    Alignment=1, Width=49, Height=18,;
    Caption="Mittente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="LRWNUOIPHQ",Visible=.t., Left=-3, Top=190,;
    Alignment=1, Width=103, Height=18,;
    Caption="Oggetto risposta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="GXOJKIOJVO",Visible=.t., Left=13, Top=96,;
    Alignment=0, Width=25, Height=18,;
    Caption="Filtri"  ;
  , bGlobalFont=.t.

  add object oBox_1_13 as StdBox with uid="DEYKFDGVEA",left=12, top=115, width=442,height=2
enddefine
define class tgsut1klpPag2 as StdContainer
  Width  = 456
  height = 288
  stdWidth  = 456
  stdheight = 288
  resizeXpos=368
  resizeYpos=175
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZOOMUID as cp_szoombox with uid="GTNAFFHESE",left=3, top=19, width=440,height=217,;
    caption='Object',;
   bGlobalFont=.t.,;
    bQueryOnDblClick=.t.,bQueryOnLoad=.t.,bReadOnly=.f.,cMenuFile="",bNoZoomGridShape=.f.,cZoomOnZoom="",cTable="ANA_CUID",cZoomFile="GSUT_KLP",bOptions=.t.,bAdvOptions=.t.,;
    nPag=2;
    , HelpContextID = 41630746


  add object oBtn_2_3 as StdButton with uid="JQFKLBDUOT",left=395, top=242, width=48,height=45,;
    CpPicture="bmp\fxmail.ico", caption="", nPag=2;
    , HelpContextID = 166106806;
    , caption='L\<eggi';
    , FontName = "Arial Narrow", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_3.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut1klp','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut1klp
Proc SaveLeggi(obj)
   Do Gsut_blp with obj,obj.w_CODUTE,.t.,obj.w_UTDATINV,obj.w_UTFILMIT,obj.w_UTFILOGG
EndProc
* --- Fine Area Manuale
