* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bin                                                        *
*              Congruenza mov. INTRA                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_56]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-13                                                      *
* Last revis.: 2018-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bin",oParentObject,m.pOper)
return(i_retval)

define class tgsar_bin as StdBatch
  * --- Local variables
  pOper = space(1)
  w_OK = .f.
  w_OK1 = .f.
  w_MESS = space(10)
  w_OREC = 0
  w_NREC = 0
  w_TIPOPE = space(10)
  w_CODISO = space(3)
  w_SERIALE = space(10)
  w_PADRE = .NULL.
  w_INTRA = .NULL.
  w_NUMREG = 0
  w_DATREG = ctod("  /  /  ")
  w_oMess = .NULL.
  w_oPart = .NULL.
  * --- WorkFile variables
  DOC_MAST_idx=0
  NAZIONI_idx=0
  ELEIDETT_idx=0
  ELEIMAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo Congruenza Movimenti Intra (da GSAR_MIT)
    * --- I = CheckForm ; D = Delete Start
    this.oParentObject.w_RESCHK = 0
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    do case
      case this.pOper="I"
        this.w_TIPOPE = this.oParentObject.cFunction
        this.w_PADRE = this.oParentObject
        this.w_MESS = " "
        this.w_OK = .T.
        this.w_OK1 = .T.
        * --- Read from NAZIONI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.NAZIONI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NACODISO"+;
            " from "+i_cTable+" NAZIONI where ";
                +"NACODNAZ = "+cp_ToStrODBC(this.oParentObject.w_NAZION);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NACODISO;
            from (i_cTable) where;
                NACODNAZ = this.oParentObject.w_NAZION;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODISO = NVL(cp_ToDate(_read_.NACODISO),cp_NullValue(_read_.NACODISO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        do case
          case EMPTY(this.oParentObject.w_PIVA)
            if this.oParentObject.w_PITIPCON="C"
              this.w_oMess.AddMsgPartNL("Partita IVA cliente non definita")     
            else
              this.w_oMess.AddMsgPartNL("Partita IVA fornitore non definita")     
            endif
            this.w_OK = .F.
          case this.oParentObject.w_PITIPCON = "C" .and. this.oParentObject.w_PITIPMOV $ "AC-RA"
            this.w_oMess.AddMsgPartNL("Movimento/rettifica di acquisto riferito ad un cliente")     
            this.w_OK = .F.
          case this.oParentObject.w_PITIPCON = "F" .and. this.oParentObject.w_PITIPMOV $ "CE-RC"
            this.w_oMess.AddMsgPartNL("Movimento/rettifica di vendita riferito ad un fornitore")     
            this.w_OK = .F.
          case this.oParentObject.w_PITIPMOV $"CS-AS" And (this.oParentObject.w_PINUMDOC=0 Or Empty(this.oParentObject.w_PIDATDOC))
            * --- Nel caso di cessione/acquisti di servizi verifico che siano presenti
            *     sia il numero documento che la data documento
            this.w_oMess.AddMsgPartNL("Numero/data documento non valorizzati")     
            this.w_OK = .F.
        endcase
        * --- Controllo Congruenza Righe
        if this.w_OK
          this.w_PADRE.MarkPos()     
          this.w_PADRE.FirstRow()     
          do while Not this.w_PADRE.Eof_Trs()
            this.w_PADRE.SetRow()     
            if this.w_PADRE.FullRow()
              this.w_oPart = this.w_oMess.AddMsgPartNL("Verificare riga: %1")
              this.w_oPart.AddParam(ALLTRIM(STR(this.oParentObject.w_CPROWORD)))     
              if this.oParentObject.w_PITIPMOV $"RS-RX"
                if EMPTY(this.oParentObject.w_PISEZDOG) OR EMPTY(this.oParentObject.w_PIANNREG) OR EMPTY(this.oParentObject.w_PIPRORET) OR EMPTY(this.oParentObject.w_PIPROGSE)
                  if EMPTY(this.oParentObject.w_PISEZDOG)
                    this.w_oMess.AddMsgPartNL("Sezione doganale non valorizzata")     
                    this.w_OK = .F.
                  endif
                  if EMPTY(this.oParentObject.w_PIANNREG)
                    this.w_oMess.AddMsgPartNL("Anno di registrazione non valorizzato")     
                    this.w_OK = .F.
                  endif
                  if EMPTY(this.oParentObject.w_PIPRORET)
                    this.w_oMess.AddMsgPartNL("Protocollo della dichiarazione non valorizzato")     
                    this.w_OK = .F.
                  endif
                  if EMPTY(this.oParentObject.w_PIPROGSE)
                    this.w_oMess.AddMsgPartNL("Progressivo della sezione 3 non valorizzato")     
                    this.w_OK = .F.
                  endif
                endif
              endif
              if this.w_OK AND this.oParentObject.w_PITIPMOV $ "RC-RA" AND EMPTY(this.oParentObject.w_PITIPRET)
                this.w_oMess.AddMsgPartNL("Segno della rettifica non definito")     
                this.w_OK = .F.
              endif
              * --- Periodo Mensile e Obbligatorieta' dati Statistici
              if this.w_OK AND this.oParentObject.w_TIPPER="M" AND this.w_CODISO <>"SM" AND ((this.oParentObject.w_PITIPMOV $ "CE-RC" AND this.oParentObject.w_DTOBBL $ "CE") OR (this.oParentObject.w_PITIPMOV $ "AC-RA" AND this.oParentObject.w_DTOBBL $ "AE"))
                if this.oParentObject.w_PITIPMOV $ "CE-AC" OR (VAL(this.oParentObject.w_PI__ANNO)=VAL(this.oParentObject.w_PIANNRET) AND this.oParentObject.w_PIPERRET=MONTH(this.oParentObject.w_PIDATCOM))
                  * --- Se cessione o Acquisto o Rettifica dello stesso periodo
                  do case
                    case g_ISONAZ="ITA" AND NVL(this.oParentObject.w_PICONDCO,"E") = "N" or empty( NVL(this.oParentObject.w_PICONDCO,"E"))
                      this.w_oMess.AddMsgPartNL("Codice condizione di consegna non definito")     
                      this.w_OK1 = .F.
                    case EMPTY(NVL(this.oParentObject.w_PIMODTRA,""))
                      this.w_oMess.AddMsgPartNL("Codice modalit� di trasporto non definito")     
                      this.w_OK1 = .F.
                    case NVL(this.oParentObject.w_PIVALSTA, 0)=0
                      this.w_oMess.AddMsgPartNL("Valore statistico uguale a zero (solo warning)")     
                      this.w_OK1 = .F.
                  endcase
                endif
              endif
            endif
            * --- Aggiorno il numero di record
            this.w_NREC = this.w_NREC + 1
            if Not this.w_OK
              EXIT
            else
              this.w_PADRE.NextRow()     
            endif
          enddo
          this.w_PADRE.Repos()     
        endif
        if this.w_OK AND this.w_NREC=0
          this.w_oMess.AddMsgPartNL("Inserire almeno una riga")     
          this.w_OK = .F.
        endif
        this.bUpdateParentObject=.F.
        if this.w_OK = .F. OR this.w_OK1=.F.
          if this.w_OK=.F.
            this.oParentObject.w_RESCHK = -1
            this.w_oMess.Ah_ErrorMsg()     
          else
            this.w_oMess.AddMsgPartNL("Confermi ugualmente?")     
            if ! this.w_oMess.Ah_YesNo()
              this.oParentObject.w_RESCHK = -1
            endif
          endif
        endif
      case this.pOper="D" AND NOT EMPTY(this.oParentObject.w_PIRIFDOC)
        * --- Prima di effettuare la cancellazione del movimento INTRA verifico se esistono altri movimenti
        *     originati dallo stesso documento. Allo stesso documento massimo possono
        *     essere associati solo due movimenti INTRA
        * --- Select from ELEIMAST
        i_nConn=i_TableProp[this.ELEIMAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ELEIMAST_idx,2],.t.,this.ELEIMAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ELEIMAST ";
              +" where PIRIFDOC="+cp_ToStrODBC(this.oParentObject.w_PIRIFDOC)+" AND PISERIAL<>"+cp_ToStrODBC(this.oParentObject.w_PISERIAL)+"";
              +" order by PIDATCOM";
               ,"_Curs_ELEIMAST")
        else
          select * from (i_cTable);
           where PIRIFDOC=this.oParentObject.w_PIRIFDOC AND PISERIAL<>this.oParentObject.w_PISERIAL;
           order by PIDATCOM;
            into cursor _Curs_ELEIMAST
        endif
        if used('_Curs_ELEIMAST')
          select _Curs_ELEIMAST
          locate for 1=1
          do while not(eof())
          this.w_SERIALE = _Curs_ELEIMAST.PISERIAL
            select _Curs_ELEIMAST
            continue
          enddo
          use
        endif
        * --- Cancella il movimento INTRA associato al documento ed inoltre elimina
        *     il flag di generazione INTRA sui documenti
        * --- Try
        local bErr_052A5BC0
        bErr_052A5BC0=bTrsErr
        this.Try_052A5BC0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_052A5BC0
        * --- End
      case this.pOper="V"
        * --- Visualizzazione movimento Intra rettificato
        this.w_INTRA = GSAR_MIT()
        if !(this.w_INTRA.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_INTRA.EcpFilter()     
        this.w_INTRA.w_PITIPMOV = IIF(this.oParentObject.w_PITIPMOV="RX","AS","CS")
        this.w_INTRA.w_PIANNREG = this.oParentObject.w_PIANNREG
        this.w_INTRA.w_PIPROGSE = this.oParentObject.w_PIPROGSE
        this.w_INTRA.w_PINUMDOC = this.oParentObject.w_PINUMFAT
        this.w_INTRA.w_PIALFDOC = this.oParentObject.w_PIALFFAT
        this.w_INTRA.w_PIDATDOC = this.oParentObject.w_PIDATFAT
        this.w_INTRA.EcpSave()     
      case this.pOper="C"
        * --- Nel caso il movimento INTRA sia cancellabile avverto l'utente se esiste
        *     un documento associato
        if Not Empty(this.oParentObject.w_PIRIFDOC) And Upper(this.oParentObject.w_HASEVCOP)="ECPDELETE"
          * --- Select from ELEIMAST
          i_nConn=i_TableProp[this.ELEIMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ELEIMAST_idx,2],.t.,this.ELEIMAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" ELEIMAST ";
                +" where PIRIFDOC="+cp_ToStrODBC(this.oParentObject.w_PIRIFDOC)+" AND PISERIAL<>"+cp_ToStrODBC(this.oParentObject.w_PISERIAL)+"";
                +" order by PIDATCOM";
                 ,"_Curs_ELEIMAST")
          else
            select * from (i_cTable);
             where PIRIFDOC=this.oParentObject.w_PIRIFDOC AND PISERIAL<>this.oParentObject.w_PISERIAL;
             order by PIDATCOM;
              into cursor _Curs_ELEIMAST
          endif
          if used('_Curs_ELEIMAST')
            select _Curs_ELEIMAST
            locate for 1=1
            do while not(eof())
            this.w_NUMREG = ALLTRIM(STR(_Curs_ELEIMAST.PINUMREG))
            this.w_DATREG = DTOC(_Curs_ELEIMAST.PIDATREG)
            this.w_MESS = "Attenzione verr� eliminato anche il movimento INTRA %1 del %2 associato"
            Ah_ErrorMsg(this.w_MESS,48,"",this.w_NUMREG,this.w_DATREG)
              select _Curs_ELEIMAST
              continue
            enddo
            use
          endif
        endif
    endcase
  endproc
  proc Try_052A5BC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from ELEIDETT
    i_nConn=i_TableProp[this.ELEIDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELEIDETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PISERIAL = "+cp_ToStrODBC(this.w_SERIALE);
             )
    else
      delete from (i_cTable) where;
            PISERIAL = this.w_SERIALE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from ELEIMAST
    i_nConn=i_TableProp[this.ELEIMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELEIMAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PISERIAL = "+cp_ToStrODBC(this.w_SERIALE);
             )
    else
      delete from (i_cTable) where;
            PISERIAL = this.w_SERIALE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVFLINTR ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLINTR');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_PIRIFDOC);
             )
    else
      update (i_cTable) set;
          MVFLINTR = " ";
          &i_ccchkf. ;
       where;
          MVSERIAL = this.oParentObject.w_PIRIFDOC;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='NAZIONI'
    this.cWorkTables[3]='ELEIDETT'
    this.cWorkTables[4]='ELEIMAST'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_ELEIMAST')
      use in _Curs_ELEIMAST
    endif
    if used('_Curs_ELEIMAST')
      use in _Curs_ELEIMAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
