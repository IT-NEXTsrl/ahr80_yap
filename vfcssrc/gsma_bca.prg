* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bca                                                        *
*              Legge chiavi articolo                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_12]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-16                                                      *
* Last revis.: 2000-02-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bca",oParentObject,m.pOper)
return(i_retval)

define class tgsma_bca as StdBatch
  * --- Local variables
  pOper = space(5)
  w_ZOOM = space(10)
  w_CODKEY = space(20)
  w_MESS = space(10)
  w_OK = .f.
  * --- WorkFile variables
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lettura Chiavi dell' Articolo (Lanciato da GSMA_KCA)
    this.w_ZOOM = this.oParentObject.w_CalcZoom
    do case
      case this.pOper="CALC"
        * --- Lancia lo Zoom
        This.OparentObject.NotifyEvent("Legge")
        * --- Setta Proprieta' Campi del Cursore
        FOR I=1 TO This.w_Zoom.grd.ColumnCount
        NC = ALLTRIM(STR(I))
        if NOT "XCHK" $ UPPER(This.w_Zoom.grd.Column&NC..ControlSource)
          This.w_Zoom.grd.Column&NC..Enabled=.f.
        endif
        ENDFOR
      case this.pOper="SELE"
        * --- Seleziona/Deseleziona Tutto
        NC = this.w_ZOOM.cCursor
        UPDATE &NC SET XCHK = IIF(this.oParentObject.w_SELEZI="S", 1, 0)
      case this.pOper="AGGIO"
        * --- Elimina i Codici Selezionati
        NC = this.w_ZOOM.cCursor
        SELECT &NC
        GO TOP
        * --- deve esistere almeno un Record non Selezionato
        COUNT FOR XCHK=0 TO w_CONTA
        if w_CONTA=0
          ah_ErrorMsg("Deve esistere almeno un codice di ricerca")
          i_retcode = 'stop'
          return
        endif
        this.w_OK = .F.
        this.w_MESS = ah_msgformat("Transazione abbandonata")
        if ah_YesNo("ATTENZIONE:%0I codici selezionati saranno definitivamente eliminati. Confermi?")
          * --- Try
          local bErr_03D7B860
          bErr_03D7B860=bTrsErr
          this.Try_03D7B860()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_ErrorMsg("Errore durante eliminazione; operazione annullata")
          endif
          bTrsErr=bTrsErr or bErr_03D7B860
          * --- End
        endif
    endcase
  endproc
  proc Try_03D7B860()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT &NC
    GO TOP
    * --- Cicla sui record Selezionati
    SCAN FOR XCHK=1
    this.w_CODKEY = CACODICE
    if this.oParentObject.w_TIPCON $ "RMIDA" AND (this.w_CODKEY= this.oParentObject.w_ARCODART)
      ah_ErrorMsg("Codice di tipo interno%0Impossibile eliminare")
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
    ah_Msg("Elimino codice: %1", .T.,.F.,.F.,CACODICE)
    * --- Delete from KEY_ARTI
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"CACODICE = "+cp_ToStrODBC(this.w_CODKEY);
             )
    else
      delete from (i_cTable) where;
            CACODICE = this.w_CODKEY;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    this.w_OK = .T.
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    if this.w_OK=.T.
      * --- Lancia lo Zoom
      This.OparentObject.NotifyEvent("Legge")
      ah_ErrorMsg("Eliminazione completata")
    else
      ah_ErrorMsg("Non ci sono codici da eliminare")
    endif
    return


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='KEY_ARTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
