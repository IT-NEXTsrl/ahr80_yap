* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bnd                                                        *
*              Modifica causale documento                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_67]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-11-15                                                      *
* Last revis.: 2004-11-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pExec
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bnd",oParentObject,m.pExec)
return(i_retval)

define class tgsve_bnd as StdBatch
  * --- Local variables
  pExec = space(1)
  w_PADRE = .NULL.
  w_EVCOP = space(50)
  w_EVENT = .f.
  w_OK = .f.
  w_SERIAL = space(10)
  w_FLRAGG = space(1)
  w_FLINTE = space(1)
  w_ALFEST = space(2)
  w_CLADOC = space(2)
  w_CAUCON = space(5)
  w_NAZION = space(3)
  w_CODBAN = space(10)
  w_CODBA2 = space(15)
  w_CONCON = space(1)
  w_CODDES = space(5)
  w_CODVET = space(5)
  w_CODSPE = space(3)
  w_CODPOR = space(1)
  w_DTOBS1 = ctod("  /  /  ")
  w_ANCODAGE = space(5)
  w_CODAGE = space(5)
  w_CODAG2 = space(5)
  w_AGDTOBSO = ctod("  /  /  ")
  w_MVCODAGE = space(5)
  w_ANNDOC = space(4)
  w_CODESE = space(4)
  w_GSVE_MDV = .NULL.
  w_OKTRS = space(1)
  * --- WorkFile variables
  DOC_MAST_idx=0
  TIP_DOCU_idx=0
  DOC_DETT_idx=0
  CONTI_idx=0
  AGENTI_idx=0
  DES_DIVE_idx=0
  TMPVEND2_idx=0
  TMPVEND3_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da Bottone 'Modifica' in testata Corrispettivi (GSVE_MDV)
    *     Nel caso in cui non ci sono blocchi apre la maschera GSVE_KND
    *     Lanciato anche dalla stessa maschera GSVE_KND per generare il nuovo documento
    *     
    *     pExec: 'D': da bottone Modifica in GSVE_MDV
    *                 'K': da maschera GSVE_KND
    this.w_PADRE = this.oParentObject
    do case
      case this.pExec="D"
        this.w_OK = .F.
        * --- Memorizzo le propriet� del Padre ( documenti )
        this.w_EVCOP = this.w_PADRE.w_HASEVCOP
        this.w_EVENT = this.w_PADRE.w_HASEVENT
        * --- Simulo un F3 sui documenti in modo che il GSVE_BNR esegua i controlli del caso
        this.w_PADRE.w_HASEVCOP = "ECPEDIT"
        this.w_PADRE.NotifyEvent("HasEvent")     
        this.w_OK = this.w_PADRE.w_HASEVENT
        * --- Risetto quindi le precedenti propriet� prima di aprire la mschera poich� se poi 
        *     premo Ok sulla machera i documenti vengono chiusi nell'altro ramo del CASE (pExec='K')
        this.w_PADRE.w_HASEVCOP = this.w_EVCOP
        this.w_PADRE.w_HASEVENT = this.w_EVENT
        * --- Se non ho errori lancio la maschera di Modifica Causale GSVE_KND
        if this.w_OK
          this.w_PADRE.NotifyEvent("ModCausale")     
        endif
      case this.pExec="K"
        * --- Try
        local bErr_0403D170
        bErr_0403D170=bTrsErr
        this.Try_0403D170()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          ah_ErrorMsg("Impossibile creare nuovo documento%0%1",,"", Message() )
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_0403D170
        * --- End
        * --- Cancellazione Tabelle Temporanee
        * --- Drop temporary table TMPVEND2
        i_nIdx=cp_GetTableDefIdx('TMPVEND2')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPVEND2')
        endif
        * --- Drop temporary table TMPVEND3
        i_nIdx=cp_GetTableDefIdx('TMPVEND3')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPVEND3')
        endif
    endcase
  endproc
  proc Try_0403D170()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_SERIAL = this.oParentObject.w_MVSERIAL
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TFFLRAGG,TDFLINTE,TDSERPRO,TDCATDOC,TDCAUCON"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_PSTIPDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TFFLRAGG,TDFLINTE,TDSERPRO,TDCATDOC,TDCAUCON;
        from (i_cTable) where;
            TDTIPDOC = this.oParentObject.w_PSTIPDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLRAGG = NVL(cp_ToDate(_read_.TFFLRAGG),cp_NullValue(_read_.TFFLRAGG))
      this.w_FLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
      this.w_ALFEST = NVL(cp_ToDate(_read_.TDSERPRO),cp_NullValue(_read_.TDSERPRO))
      this.w_CLADOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
      this.w_CAUCON = NVL(cp_ToDate(_read_.TDCAUCON),cp_NullValue(_read_.TDCAUCON))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_ANCODAGE = Space(5)
    if Not Empty(this.oParentObject.w_CODCON)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANNAZION,ANCODBA2,ANCODBAN,ANCONCON,ANCODAG1"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODCON);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANNAZION,ANCODBA2,ANCODBAN,ANCONCON,ANCODAG1;
          from (i_cTable) where;
              ANTIPCON = this.oParentObject.w_MVTIPCON;
              and ANCODICE = this.oParentObject.w_CODCON;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_NAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
        this.w_CODBA2 = NVL(cp_ToDate(_read_.ANCODBA2),cp_NullValue(_read_.ANCODBA2))
        this.w_CODBAN = NVL(cp_ToDate(_read_.ANCODBAN),cp_NullValue(_read_.ANCODBAN))
        this.w_CONCON = NVL(cp_ToDate(_read_.ANCONCON),cp_NullValue(_read_.ANCONCON))
        this.w_ANCODAGE = NVL(cp_ToDate(_read_.ANCODAG1),cp_NullValue(_read_.ANCODAG1))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Cambio Cli/For rilegge i dati accompagnatori
      * --- Propone dati Accompagnatori (Riferimento: 'CO' = Consegna)
      * --- Read from DES_DIVE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DES_DIVE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DDCODDES,DDCODVET,DDCODSPE,DDCODPOR,DDDTOBSO,DDCODAGE"+;
          " from "+i_cTable+" DES_DIVE where ";
              +"DDTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
              +" and DDCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODCON);
              +" and DDTIPRIF = "+cp_ToStrODBC("CO");
              +" and DDPREDEF = "+cp_ToStrODBC("S");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DDCODDES,DDCODVET,DDCODSPE,DDCODPOR,DDDTOBSO,DDCODAGE;
          from (i_cTable) where;
              DDTIPCON = this.oParentObject.w_MVTIPCON;
              and DDCODICE = this.oParentObject.w_CODCON;
              and DDTIPRIF = "CO";
              and DDPREDEF = "S";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODDES = NVL(cp_ToDate(_read_.DDCODDES),cp_NullValue(_read_.DDCODDES))
        this.w_CODVET = NVL(cp_ToDate(_read_.DDCODVET),cp_NullValue(_read_.DDCODVET))
        this.w_CODSPE = NVL(cp_ToDate(_read_.DDCODSPE),cp_NullValue(_read_.DDCODSPE))
        this.w_CODPOR = NVL(cp_ToDate(_read_.DDCODPOR),cp_NullValue(_read_.DDCODPOR))
        this.w_DTOBS1 = NVL(cp_ToDate(_read_.DDDTOBSO),cp_NullValue(_read_.DDDTOBSO))
        this.w_CODAGE = NVL(cp_ToDate(_read_.DDCODAGE),cp_NullValue(_read_.DDCODAGE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if i_Rows=0 OR (this.w_DTOBS1<=this.oParentObject.w_PSDATDOC AND NOT EMPTY(this.w_DTOBS1)) Or Empty(this.oParentObject.w_CODCON)
      * --- Se non trovato dato valido o non presente Intestatario Azzera tutto
      this.w_CODDES = SPACE(5)
      this.w_CODVET = SPACE(5)
      this.w_CODSPE = SPACE(3)
      this.w_CODPOR = " "
      this.w_CODAGE = this.w_ANCODAGE
    endif
    this.w_MVCODAGE = this.w_CODAGE
    * --- Leggo data obsolescenza e capo area
    if NOT EMPTY(this.w_MVCODAGE)
      * --- Read from AGENTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AGENTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AGDTOBSO,AGCZOAGE"+;
          " from "+i_cTable+" AGENTI where ";
              +"AGCODAGE = "+cp_ToStrODBC(this.w_MVCODAGE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AGDTOBSO,AGCZOAGE;
          from (i_cTable) where;
              AGCODAGE = this.w_MVCODAGE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_AGDTOBSO = NVL(cp_ToDate(_read_.AGDTOBSO),cp_NullValue(_read_.AGDTOBSO))
        this.w_CODAG2 = NVL(cp_ToDate(_read_.AGCZOAGE),cp_NullValue(_read_.AGCZOAGE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- se obsoleto non lo riporto
    if this.w_AGDTOBSO<this.oParentObject.w_PSDATDOC AND NOT EMPTY(this.w_AGDTOBSO)
      this.w_MVCODAGE = space(5)
      this.w_CODAG2 = Space(5)
    endif
    * --- begin transaction
    cp_BeginTrs()
    this.w_ANNDOC = STR(YEAR(this.oParentObject.w_PSDATDOC), 4, 0)
    this.w_CODESE = CALCESER(this.oParentObject.w_PSDATDOC, "    ")
    * --- Prima di eseguire le Modifiche al documento, memorizzo tutte le informazioni 
    *     che andr� a modificare in due tabelle temporanee (Master e detail)
    *     Nel caso di errore andr� quindi a rispristinare la situazione precedente
    *     (Transazione Manuale)
    * --- Create temporary table TMPVEND2
    i_nIdx=cp_AddTableDef('TMPVEND2') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsve2bnd',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND2_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Create temporary table TMPVEND3
    i_nIdx=cp_AddTableDef('TMPVEND3') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('query\gsve3bnd',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND3_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- ATTENZIONE:
    *     il numero documento inserito � quello proposto sulla maschera.
    *     Il numero corretto viene ricalcolato nella Replace Init della Gestione GSVE_MDV (Documenti Vendita)
    *     in modo che se i controlli finali non permettano l'inserimento del nuovo documento, il progressivo non venga 
    *     aggiornato.
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVTIPDOC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSTIPDOC),'DOC_MAST','MVTIPDOC');
      +",MVNUMDOC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNUMDOC),'DOC_MAST','MVNUMDOC');
      +",MVALFDOC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSALFDOC),'DOC_MAST','MVALFDOC');
      +",MVTIPCON ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTIPCON),'DOC_MAST','MVTIPCON');
      +",MVCODCON ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODCON),'DOC_MAST','MVCODCON');
      +",MVDATDOC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSDATDOC),'DOC_MAST','MVDATDOC');
      +",MVDATREG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSDATDOC),'DOC_MAST','MVDATREG');
      +",MVTFRAGG ="+cp_NullLink(cp_ToStrODBC(this.w_FLRAGG),'DOC_MAST','MVTFRAGG');
      +",MVFLINTE ="+cp_NullLink(cp_ToStrODBC(this.w_FLINTE),'DOC_MAST','MVFLINTE');
      +",MVALFEST ="+cp_NullLink(cp_ToStrODBC(this.w_ALFEST),'DOC_MAST','MVALFEST');
      +",MVCLADOC ="+cp_NullLink(cp_ToStrODBC(this.w_CLADOC),'DOC_MAST','MVCLADOC');
      +",MVPRD ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVPRD),'DOC_MAST','MVPRD');
      +",MVNUMREG ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNUMDOC),'DOC_MAST','MVNUMREG');
      +",MVCAUCON ="+cp_NullLink(cp_ToStrODBC(this.w_CAUCON),'DOC_MAST','MVCAUCON');
      +",MVCODBAN ="+cp_NullLink(cp_ToStrODBC(this.w_CODBAN),'DOC_MAST','MVCODBAN');
      +",MVCODBA2 ="+cp_NullLink(cp_ToStrODBC(this.w_CODBA2),'DOC_MAST','MVCODBA2');
      +",MVCONCON ="+cp_NullLink(cp_ToStrODBC(this.w_CONCON),'DOC_MAST','MVCONCON');
      +",MVCODAGE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODAGE),'DOC_MAST','MVCODAGE');
      +",MVCODPOR ="+cp_NullLink(cp_ToStrODBC(this.w_CODPOR),'DOC_MAST','MVCODPOR');
      +",MVCODVET ="+cp_NullLink(cp_ToStrODBC(this.w_CODVET),'DOC_MAST','MVCODVET');
      +",MVCODDES ="+cp_NullLink(cp_ToStrODBC(this.w_CODDES),'DOC_MAST','MVCODDES');
      +",MVCODSPE ="+cp_NullLink(cp_ToStrODBC(this.w_CODSPE),'DOC_MAST','MVCODSPE');
      +",MVCODAG2 ="+cp_NullLink(cp_ToStrODBC(this.w_CODAG2),'DOC_MAST','MVCODAG2');
      +",MVDATCIV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_PSDATDOC),'DOC_MAST','MVDATCIV');
      +",MVCODESE ="+cp_NullLink(cp_ToStrODBC(this.w_CODESE),'DOC_MAST','MVCODESE');
      +",MVANNDOC ="+cp_NullLink(cp_ToStrODBC(this.w_ANNDOC),'DOC_MAST','MVANNDOC');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
             )
    else
      update (i_cTable) set;
          MVTIPDOC = this.oParentObject.w_PSTIPDOC;
          ,MVNUMDOC = this.oParentObject.w_MVNUMDOC;
          ,MVALFDOC = this.oParentObject.w_PSALFDOC;
          ,MVTIPCON = this.oParentObject.w_MVTIPCON;
          ,MVCODCON = this.oParentObject.w_CODCON;
          ,MVDATDOC = this.oParentObject.w_PSDATDOC;
          ,MVDATREG = this.oParentObject.w_PSDATDOC;
          ,MVTFRAGG = this.w_FLRAGG;
          ,MVFLINTE = this.w_FLINTE;
          ,MVALFEST = this.w_ALFEST;
          ,MVCLADOC = this.w_CLADOC;
          ,MVPRD = this.oParentObject.w_MVPRD;
          ,MVNUMREG = this.oParentObject.w_MVNUMDOC;
          ,MVCAUCON = this.w_CAUCON;
          ,MVCODBAN = this.w_CODBAN;
          ,MVCODBA2 = this.w_CODBA2;
          ,MVCONCON = this.w_CONCON;
          ,MVCODAGE = this.w_MVCODAGE;
          ,MVCODPOR = this.w_CODPOR;
          ,MVCODVET = this.w_CODVET;
          ,MVCODDES = this.w_CODDES;
          ,MVCODSPE = this.w_CODSPE;
          ,MVCODAG2 = this.w_CODAG2;
          ,MVDATCIV = this.oParentObject.w_PSDATDOC;
          ,MVCODESE = this.w_CODESE;
          ,MVANNDOC = this.w_ANNDOC;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.oParentObject.w_MVSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVFLRAGG ="+cp_NullLink(cp_ToStrODBC(this.w_FLRAGG),'DOC_DETT','MVFLRAGG');
      +",MVNAZPRO ="+cp_NullLink(cp_ToStrODBC(this.w_NAZION),'DOC_DETT','MVNAZPRO');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
             )
    else
      update (i_cTable) set;
          MVFLRAGG = this.w_FLRAGG;
          ,MVNAZPRO = this.w_NAZION;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.oParentObject.w_MVSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Chiudo la maschera di modifica causale e i documenti
    this.oParentObject.w_GEST.ecpQuit()     
    this.w_PADRE.ecpQuit()     
    * --- Riapro il nuovo documento in Interroga
    *     Il GSAR_BZM ritorna l'oggetto Gestione aperto
    this.w_GSVE_MDV = gsar_bzm(this,this.w_SERIAL,-20)
    this.w_GSVE_MDV.ecpEdit()     
    * --- Impostando OFLPROV a 'S' viene aggiornato anche il rischio che altrimenti in 
    *     modfica non verrebbe aggiornato. 
    this.w_GSVE_MDV.w_OFLPROV = "S"
    * --- La propriet� w_GSVE_MDV.w_Mod_Cau � inizializzata nell'area Declare Variables in GSVE_MDV
    *     Utilizzata invece nella Replace Init per far si che il progressivo Numero Documento venga ricalcolato e aggiornato sotto transazione
    this.w_GSVE_MDV.w_Mod_Cau = .T.
    this.w_GSVE_MDV.ecpSave()     
    * --- La propriet� w_GSVE_MDV.w_Loc_Error � valorizzata nell'area Replace End e Check Form della maschera GSVE_MDV
    *     Se c'� un errore al salvataggio (ecpSave qui sopra) viene valorizzata con 'E' altrimenti rimane ' '
    * --- Se c'� stato un errore di transazione, setto la propriet� a 'S' in modo che la ecpQuit successiva
    *     non faccia la domanda 'Abbandoni le Modifiche?'. La procedura ecpQuit � stata per questo ridefinita 
    *     sulla gestione dei documenti di vendita
    this.w_GSVE_MDV.w_Loc_Error = IIF(this.w_GSVE_MDV.w_Loc_Error = "E","S","N")
    this.w_OKTRS = this.w_GSVE_MDV.w_Loc_Error 
    if this.w_OKTRS = "S"
      * --- Se c'� errore di transazione chiudo il documento, rieseguo le modifiche al contrario
      *     (ripristino situazione iniziale) e riapro il documento corrispettivo di origine
      this.w_GSVE_MDV.ecpQuit()     
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND2_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVTIPDOC = _t2.MVTIPDOC";
            +",MVNUMDOC = _t2.MVNUMDOC";
            +",MVALFDOC = _t2.MVALFDOC";
            +",MVTIPCON = _t2.MVTIPCON";
            +",MVCODCON = _t2.MVCODCON";
            +",MVDATDOC = _t2.MVDATDOC";
            +",MVDATREG = _t2.MVDATREG";
            +",MVTFRAGG = _t2.MVTFRAGG";
            +",MVFLINTE = _t2.MVFLINTE";
            +",MVALFEST = _t2.MVALFEST";
            +",MVCLADOC = _t2.MVCLADOC";
            +",MVPRD = _t2.MVPRD";
            +",MVNUMREG = _t2.MVNUMDOC";
            +",MVCAUCON = _t2.MVCAUCON";
            +",MVCODBAN = _t2.MVCODBAN";
            +",MVCODBA2 = _t2.MVCODBA2";
            +",MVCONCON = _t2.MVCONCON";
            +",MVCODAGE = _t2.MVCODAGE";
            +",MVCODPOR = _t2.MVCODPOR";
            +",MVCODVET = _t2.MVCODVET";
            +",MVCODDES = _t2.MVCODDES";
            +",MVCODSPE = _t2.MVCODSPE";
            +",MVCODAG2 = _t2.MVCODAG2";
            +",MVDATCIV = _t2.MVDATCIV";
            +",MVCODESE = _t2.MVCODESE";
            +",MVANNDOC = _t2.MVANNDOC";
            +i_ccchkf;
            +" from "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST, "+i_cQueryTable+" _t2 set ";
            +"DOC_MAST.MVTIPDOC = _t2.MVTIPDOC";
            +",DOC_MAST.MVNUMDOC = _t2.MVNUMDOC";
            +",DOC_MAST.MVALFDOC = _t2.MVALFDOC";
            +",DOC_MAST.MVTIPCON = _t2.MVTIPCON";
            +",DOC_MAST.MVCODCON = _t2.MVCODCON";
            +",DOC_MAST.MVDATDOC = _t2.MVDATDOC";
            +",DOC_MAST.MVDATREG = _t2.MVDATREG";
            +",DOC_MAST.MVTFRAGG = _t2.MVTFRAGG";
            +",DOC_MAST.MVFLINTE = _t2.MVFLINTE";
            +",DOC_MAST.MVALFEST = _t2.MVALFEST";
            +",DOC_MAST.MVCLADOC = _t2.MVCLADOC";
            +",DOC_MAST.MVPRD = _t2.MVPRD";
            +",DOC_MAST.MVNUMREG = _t2.MVNUMDOC";
            +",DOC_MAST.MVCAUCON = _t2.MVCAUCON";
            +",DOC_MAST.MVCODBAN = _t2.MVCODBAN";
            +",DOC_MAST.MVCODBA2 = _t2.MVCODBA2";
            +",DOC_MAST.MVCONCON = _t2.MVCONCON";
            +",DOC_MAST.MVCODAGE = _t2.MVCODAGE";
            +",DOC_MAST.MVCODPOR = _t2.MVCODPOR";
            +",DOC_MAST.MVCODVET = _t2.MVCODVET";
            +",DOC_MAST.MVCODDES = _t2.MVCODDES";
            +",DOC_MAST.MVCODSPE = _t2.MVCODSPE";
            +",DOC_MAST.MVCODAG2 = _t2.MVCODAG2";
            +",DOC_MAST.MVDATCIV = _t2.MVDATCIV";
            +",DOC_MAST.MVCODESE = _t2.MVCODESE";
            +",DOC_MAST.MVANNDOC = _t2.MVANNDOC";
            +Iif(Empty(i_ccchkf),"",",DOC_MAST.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="PostgreSQL"
          i_cWhere="DOC_MAST.MVSERIAL = _t2.MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_MAST set ";
            +"MVTIPDOC = _t2.MVTIPDOC";
            +",MVNUMDOC = _t2.MVNUMDOC";
            +",MVALFDOC = _t2.MVALFDOC";
            +",MVTIPCON = _t2.MVTIPCON";
            +",MVCODCON = _t2.MVCODCON";
            +",MVDATDOC = _t2.MVDATDOC";
            +",MVDATREG = _t2.MVDATREG";
            +",MVTFRAGG = _t2.MVTFRAGG";
            +",MVFLINTE = _t2.MVFLINTE";
            +",MVALFEST = _t2.MVALFEST";
            +",MVCLADOC = _t2.MVCLADOC";
            +",MVPRD = _t2.MVPRD";
            +",MVNUMREG = _t2.MVNUMDOC";
            +",MVCAUCON = _t2.MVCAUCON";
            +",MVCODBAN = _t2.MVCODBAN";
            +",MVCODBA2 = _t2.MVCODBA2";
            +",MVCONCON = _t2.MVCONCON";
            +",MVCODAGE = _t2.MVCODAGE";
            +",MVCODPOR = _t2.MVCODPOR";
            +",MVCODVET = _t2.MVCODVET";
            +",MVCODDES = _t2.MVCODDES";
            +",MVCODSPE = _t2.MVCODSPE";
            +",MVCODAG2 = _t2.MVCODAG2";
            +",MVDATCIV = _t2.MVDATCIV";
            +",MVCODESE = _t2.MVCODESE";
            +",MVANNDOC = _t2.MVANNDOC";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVTIPDOC = (select MVTIPDOC from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVNUMDOC = (select MVNUMDOC from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVALFDOC = (select MVALFDOC from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVTIPCON = (select MVTIPCON from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVCODCON = (select MVCODCON from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVDATDOC = (select MVDATDOC from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVDATREG = (select MVDATREG from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVTFRAGG = (select MVTFRAGG from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVFLINTE = (select MVFLINTE from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVALFEST = (select MVALFEST from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVCLADOC = (select MVCLADOC from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVPRD = (select MVPRD from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVNUMREG = (select MVNUMDOC from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVCAUCON = (select MVCAUCON from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVCODBAN = (select MVCODBAN from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVCODBA2 = (select MVCODBA2 from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVCONCON = (select MVCONCON from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVCODAGE = (select MVCODAGE from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVCODPOR = (select MVCODPOR from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVCODVET = (select MVCODVET from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVCODDES = (select MVCODDES from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVCODSPE = (select MVCODSPE from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVCODAG2 = (select MVCODAG2 from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVDATCIV = (select MVDATCIV from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVCODESE = (select MVCODESE from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVANNDOC = (select MVANNDOC from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_SetAzi(i_TableProp[this.TMPVEND3_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVFLRAGG = _t2.MVFLRAGG";
            +",MVNAZPRO = _t2.MVNAZPRO";
            +i_ccchkf;
            +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
            +"DOC_DETT.MVFLRAGG = _t2.MVFLRAGG";
            +",DOC_DETT.MVNAZPRO = _t2.MVNAZPRO";
            +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="PostgreSQL"
          i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
            +"MVFLRAGG = _t2.MVFLRAGG";
            +",MVNAZPRO = _t2.MVNAZPRO";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVFLRAGG = (select MVFLRAGG from "+i_cQueryTable+" where "+i_cWhere+")";
            +",MVNAZPRO = (select MVNAZPRO from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Riapro il documento originale in Interroga
      gsar_bzm(this,this.w_SERIAL,-20)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    return


  proc Init(oParentObject,pExec)
    this.pExec=pExec
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,8)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='TIP_DOCU'
    this.cWorkTables[3]='DOC_DETT'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='AGENTI'
    this.cWorkTables[6]='DES_DIVE'
    this.cWorkTables[7]='*TMPVEND2'
    this.cWorkTables[8]='*TMPVEND3'
    return(this.OpenAllTables(8))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pExec"
endproc
