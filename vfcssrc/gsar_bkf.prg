* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bkf                                                        *
*              Aggiornamento massivo intestari                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-10-31                                                      *
* Last revis.: 2018-06-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bkf",oParentObject,m.pParame)
return(i_retval)

define class tgsar_bkf as StdBatch
  * --- Local variables
  pParame = space(1)
  w_ZOOM = .NULL.
  w_ZOOMC = .NULL.
  w_ZOOMF = .NULL.
  w_ZOOMV = .NULL.
  w_ZOOMA = .NULL.
  w_ZOOMCO = .NULL.
  w_ZOOMR = .NULL.
  w_ZOOMPC = .NULL.
  w_ZOOMPF = .NULL.
  w_NFLTATTR = 0
  w_oERRORLOG = .NULL.
  w_MSGAVV = 0
  w_ANCODICE = space(15)
  w_ANDESCRI = space(40)
  w_ANCONSUP = space(15)
  w_ANCATCON = space(5)
  w_ANCODIVA = space(5)
  w_ANTIPOPE = space(10)
  w_ANPARTSN = space(1)
  w_ANCODIRP = space(5)
  w_ANCAURIT = space(1)
  w_ANFLRITE = space(1)
  w_NOAGGIORNAMENTO = .f.
  w_MESS_ELA = space(10)
  w_ANFLFIDO = space(1)
  w_ANCATCOM = space(5)
  w_ANCATSCM = space(5)
  w_ANCODZON = space(5)
  w_ANGRUPRO = space(5)
  w_ANCODAG1 = space(5)
  w_ANMAGTER = space(5)
  w_ANCODPAG = space(5)
  w_ANCODBA2 = space(15)
  w_ANGESCON = space(1)
  w_ANFLGCON = space(1)
  w_ANPAGPAR = space(5)
  w_ANDATMOR = ctod("  /  /  ")
  w_ANFLESIM = space(1)
  w_ANSAGINT = 0
  w_ANSPRINT = space(1)
  w_ANRITENU = space(1)
  w_ANCODTR2 = space(5)
  w_ANPEINPS = 0
  w_ANRIINPS = 0
  w_ANCOINPS = 0
  w_ANCASPRO = 0
  w_ANCODATT = 0
  w_ANCODASS = space(3)
  w_ANCOIMPS = space(20)
  w_AggDettaglio = .f.
  w_ANCODEST = space(7)
  w_ANCODCLA = space(5)
  * --- WorkFile variables
  CONTI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorno massivo intestatari
    if this.pPARAME $ "ZSDIA"
      this.w_ZOOMC = This.oParentObject.w_ZoomAggC
      this.w_ZOOMF = This.oParentObject.w_ZoomAggF
      this.w_ZOOMV = This.oParentObject.w_ZoomAggV
      this.w_ZOOMA = This.oParentObject.w_ZoomAggA
      this.w_ZOOMCO = This.oParentObject.w_ZoomAggCo
      this.w_ZOOMR = This.oParentObject.w_ZoomAggR
      this.w_ZOOMPC = This.oParentObject.w_ZoomAggPc
      this.w_ZOOMPF = This.oParentObject.w_ZoomAggPf
      do case
        case this.oParentObject.w_Sezione="C" And this.oParentObject.w_Tipcon="C"
          this.w_ZOOM = This.oParentObject.w_ZoomAggC
        case this.oParentObject.w_Sezione="C" And this.oParentObject.w_Tipcon="F"
          this.w_ZOOM = This.oParentObject.w_ZoomAggF
        case this.oParentObject.w_Sezione="V"
          this.w_ZOOM = This.oParentObject.w_ZoomAggV
        case this.oParentObject.w_Sezione="R"
          this.w_ZOOM = This.oParentObject.w_ZoomAggR
        case this.oParentObject.w_Sezione="Z"
          this.w_ZOOM = This.oParentObject.w_ZoomAggCo
        case this.oParentObject.w_Sezione="A"
          this.w_ZOOM = This.oParentObject.w_ZoomAggA
        case this.oParentObject.w_Sezione="P" And this.oParentObject.w_Tipcon="C"
          this.w_ZOOM = This.oParentObject.w_ZoomAggPc
        case this.oParentObject.w_Sezione="P" And this.oParentObject.w_Tipcon="F"
          this.w_ZOOM = This.oParentObject.w_ZoomAggPf
      endcase
    endif
    do case
      case this.pParame="Z"
        This.oParentObject.w_ZoomAggC.Visible=.f.
        This.oParentObject.w_ZoomAggF.Visible=.f.
        This.oParentObject.w_ZoomAggV.Visible=.f.
        This.oParentObject.w_ZoomAggA.Visible=.f.
        This.oParentObject.w_ZoomAggCo.Visible=.f.
        This.oParentObject.w_ZoomAggR.Visible=.f.
        This.oParentObject.w_ZoomAggPc.Visible=.f.
        This.oParentObject.w_ZoomAggPf.Visible=.f.
        do case
          case this.oParentObject.w_Sezione="C" And this.oParentObject.w_Tipcon="C"
            This.oParentObject.w_ZoomAggC.Visible=.t.
          case this.oParentObject.w_Sezione="C" And this.oParentObject.w_Tipcon="F"
            This.oParentObject.w_ZoomAggF.Visible=.t.
          case this.oParentObject.w_Sezione="V"
            This.oParentObject.w_ZoomAggV.Visible=.t.
          case this.oParentObject.w_Sezione="R"
            This.oParentObject.w_ZoomAggR.Visible=.t.
          case this.oParentObject.w_Sezione="Z"
            This.oParentObject.w_ZoomAggCo.Visible=.t.
          case this.oParentObject.w_Sezione="A"
            This.oParentObject.w_ZoomAggA.Visible=.t.
          case this.oParentObject.w_Sezione="P" And this.oParentObject.w_Tipcon="C"
            This.oParentObject.w_ZoomAggPc.Visible=.t.
          case this.oParentObject.w_Sezione="P" And this.oParentObject.w_Tipcon="F"
            This.oParentObject.w_ZoomAggPf.Visible=.t.
        endcase
      case this.pParame="S" or this.pParame="D" or this.pParame="I"
        Select (this.w_ZOOM.cCursor)
        this.w_NFLTATTR = RECNO()
        UPDATE (this.w_ZOOM.cCursor) SET XCHK=ICASE(this.pParame=="S",1,this.pParame=="D",0,IIF(XCHK=1,0,1))
        Select (this.w_ZOOM.cCursor)
        if this.w_NFLTATTR<RECCOUNT()
          GO this.w_NFLTATTR
        endif
      case this.pParame="A"
        SELECT(this.w_ZOOM.cCursor)
        COUNT FOR XCHK=1 TO w_CONTA
        * --- Verifico se l'utente ha selezionato almeno un record da aggiornare
        if w_CONTA = 0
          ah_errormsg("Selezionare almeno un conto da aggiornare",48)
          i_retcode = 'stop'
          return
        endif
        this.w_AggDettaglio = iif(this.oParentObject.w_AggMasCon="S" Or this.oParentObject.w_AggCatCon="S" Or this.oParentObject.w_AggAncodiva="S" Or this.oParentObject.w_AggAntipope="S" Or this.oParentObject.w_AggAnpartsn="S" Or this.oParentObject.w_AggAnflrite="S" Or this.oParentObject.w_Aggancodirp="S" Or this.oParentObject.w_Aggancaurit="S" Or this.oParentObject.w_AggAnflfido="S",.t.,.f.)
        this.w_AggDettaglio = iif(this.oParentObject.w_AggAnCatCom="S" Or this.oParentObject.w_AggAnCatScm="S" Or this.oParentObject.w_AggAnCodZon="S" Or this.oParentObject.w_AggAnMagTer="S" Or this.oParentObject.w_AggAnGruPro="S" Or this.oParentObject.w_AggAnCodAg1="S",.t.,this.w_AggDettaglio)
        this.w_AggDettaglio = iif(this.oParentObject.w_AggAnCodPag="S" Or this.oParentObject.w_AggAnCodBa2="S" Or this.oParentObject.w_AggAnFleSim<>"X" Or this.oParentObject.w_AggAnSagInt="S",.t.,this.w_AggDettaglio)
        this.w_AggDettaglio = iif(this.oParentObject.w_Aggangescon<>"X" Or this.oParentObject.w_AggAnFlgCon<>"X" Or this.oParentObject.w_AggAnPagPar="S" Or this.oParentObject.w_AggAnDatMor="S" Or this.oParentObject.w_AggAnFleSim<>"X" Or this.oParentObject.w_AggAnSagInt="S" Or this.oParentObject.w_AggAnSprInt<>"X",.t.,this.w_AggDettaglio)
        this.w_AggDettaglio = iif(this.oParentObject.w_Agganritenu<>"X" Or this.oParentObject.w_AggAnCodIrpF="S" Or this.oParentObject.w_AggAnCodTr2="S" Or this.oParentObject.w_AggAnCauRitF="S" Or this.oParentObject.w_AggAnPeInps="S" Or this.oParentObject.w_AggAnRiInps="S" Or this.oParentObject.w_AggAnCoInps="S" Or this.oParentObject.w_AggAnCasPro="S" Or this.oParentObject.w_AggAnCodAtt="S" Or this.oParentObject.w_AggAnCodAss="S" Or this.oParentObject.w_AggAnCoiMps="S",.t.,this.w_AggDettaglio)
        this.w_AggDettaglio = iif(this.oParentObject.w_AGGANCODEST="S" OR this.oParentObject.w_AGGANCODCLA="S",.t.,this.w_AggDettaglio)
        if ! this.w_AggDettaglio
          ah_errormsg("Selezionare almeno un campo da aggiornare",48)
          i_retcode = 'stop'
          return
        endif
        if this.oParentObject.checkform()
          * --- Crea la classe delle Messaggistiche di Errore
          this.w_oERRORLOG=createobject("AH_ErrorLog")
          this.w_MSGAVV = 0
          Select (this.w_ZOOM.cCursor) 
 Go Top 
 Scan for XCHK = 1
          this.w_ANCODICE = Ancodice
          this.w_ANDESCRI = Alltrim(Andescri)
          this.w_NOAGGIORNAMENTO = .F.
          do case
            case this.oParentObject.w_Sezione="C"
              this.w_ANCONSUP = iif(this.oParentObject.w_AggMasCon="S",this.oParentObject.w_NewMasCon,Nvl(Anconsup,Space(15)))
              this.w_ANCATCON = iif(this.oParentObject.w_AggCatCon="S",this.oParentObject.w_NewCatCon,Nvl(Ancatcon,Space(5)))
              this.w_ANCODIVA = iif(this.oParentObject.w_AggAncodiva="S",this.oParentObject.w_NewAncodiva,Nvl(Ancodiva,Space(5)))
              this.w_ANTIPOPE = iif(this.oParentObject.w_AggAntipope="S",this.oParentObject.w_NewAntipope,Nvl(Antipope,Space(10)))
              this.w_ANPARTSN = iif(this.oParentObject.w_AggAnpartsn="S",this.oParentObject.w_AggAnpartsn,Nvl(Anpartsn,"N"))
              if this.oParentObject.w_TIPCON="C"
                if this.oParentObject.w_AGGANFLRITE="N"
                  this.w_ANFLRITE = "N"
                  this.w_ANCODIRP = Space(5)
                  this.w_ANCAURIT = Space(1)
                else
                  this.w_ANFLRITE = iif(this.oParentObject.w_AggAnflrite="S","S",Nvl(Anflrite,"N"))
                  * --- Se ho attivato il check di aggiorna codice tributo o causale prestazione
                  *     ma il cliente non ha il flag di ritenute attivato non deve effettuare nessun
                  *     aggiornamento 
                  if (this.oParentObject.w_AGGANCODIRP="S" OR this.oParentObject.w_AGGANCAURIT="S") And this.w_ANFLRITE<>"S"
                    this.w_oERRORLOG.AddMsgLog("Verificare cliente: %1 %2. Gestione ritenute disattiva",ALLTRIM(this.w_ANCODICE),this.w_ANDESCRI)     
                    this.w_NOAGGIORNAMENTO = .T.
                    this.w_MSGAVV = this.w_MSGAVV+1
                  else
                    this.w_ANCODIRP = iif(this.oParentObject.w_AGGANCODIRP="S",this.oParentObject.w_NEWANCODIRP,NVL(ANCODIRP,SPACE(5)))
                    this.w_ANCAURIT = iif(this.oParentObject.w_AGGANCAURIT="S",this.oParentObject.w_NEWANCAURIT,NVL(ANCAURIT,SPACE(1)))
                  endif
                endif
                this.w_ANFLFIDO = iif(this.oParentObject.w_AggAnflfido="S",this.oParentObject.w_AggAnflfido,Nvl(Anflfido," "))
                if ! this.w_NOAGGIORNAMENTO
                  * --- Write into CONTI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.CONTI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"ANCONSUP ="+cp_NullLink(cp_ToStrODBC(this.w_ANCONSUP),'CONTI','ANCONSUP');
                    +",ANCATCON ="+cp_NullLink(cp_ToStrODBC(this.w_ANCATCON),'CONTI','ANCATCON');
                    +",ANCODIVA ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODIVA),'CONTI','ANCODIVA');
                    +",ANTIPOPE ="+cp_NullLink(cp_ToStrODBC(this.w_ANTIPOPE),'CONTI','ANTIPOPE');
                    +",ANPARTSN ="+cp_NullLink(cp_ToStrODBC(this.w_ANPARTSN),'CONTI','ANPARTSN');
                    +",ANFLRITE ="+cp_NullLink(cp_ToStrODBC(this.w_ANFLRITE),'CONTI','ANFLRITE');
                    +",ANCODIRP ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODIRP),'CONTI','ANCODIRP');
                    +",ANCAURIT ="+cp_NullLink(cp_ToStrODBC(this.w_ANCAURIT),'CONTI','ANCAURIT');
                    +",ANFLFIDO ="+cp_NullLink(cp_ToStrODBC(this.w_ANFLFIDO),'CONTI','ANFLFIDO');
                        +i_ccchkf ;
                    +" where ";
                        +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPCON );
                        +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                           )
                  else
                    update (i_cTable) set;
                        ANCONSUP = this.w_ANCONSUP;
                        ,ANCATCON = this.w_ANCATCON;
                        ,ANCODIVA = this.w_ANCODIVA;
                        ,ANTIPOPE = this.w_ANTIPOPE;
                        ,ANPARTSN = this.w_ANPARTSN;
                        ,ANFLRITE = this.w_ANFLRITE;
                        ,ANCODIRP = this.w_ANCODIRP;
                        ,ANCAURIT = this.w_ANCAURIT;
                        ,ANFLFIDO = this.w_ANFLFIDO;
                        &i_ccchkf. ;
                     where;
                        ANTIPCON = this.oParentObject.w_TIPCON ;
                        and ANCODICE = this.w_ANCODICE;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              else
                * --- Write into CONTI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.CONTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"ANCONSUP ="+cp_NullLink(cp_ToStrODBC(this.w_ANCONSUP),'CONTI','ANCONSUP');
                  +",ANCATCON ="+cp_NullLink(cp_ToStrODBC(this.w_ANCATCON),'CONTI','ANCATCON');
                  +",ANCODIVA ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODIVA),'CONTI','ANCODIVA');
                  +",ANTIPOPE ="+cp_NullLink(cp_ToStrODBC(this.w_ANTIPOPE),'CONTI','ANTIPOPE');
                  +",ANPARTSN ="+cp_NullLink(cp_ToStrODBC(this.w_ANPARTSN),'CONTI','ANPARTSN');
                      +i_ccchkf ;
                  +" where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPCON );
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                         )
                else
                  update (i_cTable) set;
                      ANCONSUP = this.w_ANCONSUP;
                      ,ANCATCON = this.w_ANCATCON;
                      ,ANCODIVA = this.w_ANCODIVA;
                      ,ANTIPOPE = this.w_ANTIPOPE;
                      ,ANPARTSN = this.w_ANPARTSN;
                      &i_ccchkf. ;
                   where;
                      ANTIPCON = this.oParentObject.w_TIPCON ;
                      and ANCODICE = this.w_ANCODICE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            case this.oParentObject.w_Sezione $ "VA"
              this.w_ANCATCOM = iif(this.oParentObject.w_AggAnCatCom="S",this.oParentObject.w_NewAnCatCom,Nvl(Ancatcom,Space(5)))
              this.w_ANCATSCM = iif(this.oParentObject.w_AggAnCatScm="S",this.oParentObject.w_NewAnCatScm,Nvl(Ancatscm,Space(5)))
              this.w_ANCODZON = iif(this.oParentObject.w_AggAnCodZon="S",this.oParentObject.w_NewAnCodZon,Nvl(Ancodzon,Space(5)))
              this.w_ANMAGTER = iif(this.oParentObject.w_AggAnMagTer="S",this.oParentObject.w_NewAnMagTer,Nvl(Anmagter,Space(5)))
              this.w_ANCODEST = iif(this.oParentObject.w_AGGANCODEST="S",this.oParentObject.w_NEWCODEST,Nvl(ANCODEST,Space(7)))
              this.w_ANCODCLA = iif(this.oParentObject.w_AGGANCODCLA="S",this.oParentObject.w_NEWCODCLA,Nvl(ANCODCLA,Space(5)))
              if this.oParentObject.w_Sezione="V"
                this.w_ANGRUPRO = iif(this.oParentObject.w_AggAnGruPro="S",this.oParentObject.w_NewAnGruPro,Nvl(Angrupro,Space(5)))
                this.w_ANCODAG1 = iif(this.oParentObject.w_AggAnCodAg1="S",this.oParentObject.w_NewAnCodAg1,Nvl(Ancodag1,Space(5)))
                * --- Write into CONTI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.CONTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"ANCATCOM ="+cp_NullLink(cp_ToStrODBC(this.w_ANCATCOM),'CONTI','ANCATCOM');
                  +",ANCATSCM ="+cp_NullLink(cp_ToStrODBC(this.w_ANCATSCM),'CONTI','ANCATSCM');
                  +",ANCODZON ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODZON),'CONTI','ANCODZON');
                  +",ANMAGTER ="+cp_NullLink(cp_ToStrODBC(this.w_ANMAGTER),'CONTI','ANMAGTER');
                  +",ANGRUPRO ="+cp_NullLink(cp_ToStrODBC(this.w_ANGRUPRO),'CONTI','ANGRUPRO');
                  +",ANCODAG1 ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODAG1),'CONTI','ANCODAG1');
                  +",ANCODEST ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODEST),'CONTI','ANCODEST');
                  +",ANCODCLA ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODCLA),'CONTI','ANCODCLA');
                      +i_ccchkf ;
                  +" where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPCON );
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                         )
                else
                  update (i_cTable) set;
                      ANCATCOM = this.w_ANCATCOM;
                      ,ANCATSCM = this.w_ANCATSCM;
                      ,ANCODZON = this.w_ANCODZON;
                      ,ANMAGTER = this.w_ANMAGTER;
                      ,ANGRUPRO = this.w_ANGRUPRO;
                      ,ANCODAG1 = this.w_ANCODAG1;
                      ,ANCODEST = this.w_ANCODEST;
                      ,ANCODCLA = this.w_ANCODCLA;
                      &i_ccchkf. ;
                   where;
                      ANTIPCON = this.oParentObject.w_TIPCON ;
                      and ANCODICE = this.w_ANCODICE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              else
                * --- Write into CONTI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.CONTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"ANCATCOM ="+cp_NullLink(cp_ToStrODBC(this.w_ANCATCOM),'CONTI','ANCATCOM');
                  +",ANCATSCM ="+cp_NullLink(cp_ToStrODBC(this.w_ANCATSCM),'CONTI','ANCATSCM');
                  +",ANCODZON ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODZON),'CONTI','ANCODZON');
                  +",ANMAGTER ="+cp_NullLink(cp_ToStrODBC(this.w_ANMAGTER),'CONTI','ANMAGTER');
                  +",ANCODEST ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODEST),'CONTI','ANCODEST');
                  +",ANCODCLA ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODCLA),'CONTI','ANCODCLA');
                      +i_ccchkf ;
                  +" where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPCON );
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                         )
                else
                  update (i_cTable) set;
                      ANCATCOM = this.w_ANCATCOM;
                      ,ANCATSCM = this.w_ANCATSCM;
                      ,ANCODZON = this.w_ANCODZON;
                      ,ANMAGTER = this.w_ANMAGTER;
                      ,ANCODEST = this.w_ANCODEST;
                      ,ANCODCLA = this.w_ANCODCLA;
                      &i_ccchkf. ;
                   where;
                      ANTIPCON = this.oParentObject.w_TIPCON ;
                      and ANCODICE = this.w_ANCODICE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            case this.oParentObject.w_Sezione="P"
              this.w_ANCODPAG = iif(this.oParentObject.w_AggAnCodPag="S",this.oParentObject.w_NewAnCodPag,Nvl(Ancodpag,Space(5)))
              this.w_ANCODBA2 = iif(this.oParentObject.w_AggAnCodBa2="S",this.oParentObject.w_NewAnCodBa2,Nvl(Ancodba2,Space(15)))
              if this.oParentObject.w_TIPCON="F"
                if this.oParentObject.w_AGGANFLESIM="S"
                  this.w_ANFLESIM = "S"
                  this.w_ANSAGINT = 0
                else
                  this.w_ANFLESIM = iif(this.oParentObject.w_AggAnFleSim="X",Nvl(Anflesim," "),this.oParentObject.w_AggAnFleSim)
                  this.w_ANSAGINT = iif(this.oParentObject.w_AggAnSagInt="S",this.oParentObject.w_NewAnSagInt,Nvl(Ansagint,0))
                  this.w_ANSPRINT = iif(this.oParentObject.w_AggAnSprInt="X",Nvl(Ansprint," "),this.oParentObject.w_AggAnSprInt)
                  * --- Se ho attivato il check "Aggiorna interessi di mora" ma il cliente ha il flag
                  *     "Escludi applicazione interessi di mora"
                  if (this.oParentObject.w_AGGANSAGINT="S" And this.w_ANFLESIM="S")
                    this.w_oERRORLOG.AddMsgLog("Verificare fornitore: %1 %2. Escludi applicazione interessi di mora attivato",ALLTRIM(this.w_ANCODICE),this.w_ANDESCRI)     
                    this.w_NOAGGIORNAMENTO = .T.
                    this.w_MSGAVV = this.w_MSGAVV+1
                  endif
                  if this.w_ANSPRINT="S" And this.w_ANSAGINT=0
                    this.w_oERRORLOG.AddMsgLog("Verificare cliente: %1 %2.Interessi di mora non valorizzati",ALLTRIM(this.w_ANCODICE),this.w_ANDESCRI)     
                    this.w_NOAGGIORNAMENTO = .T.
                    this.w_MSGAVV = this.w_MSGAVV+1
                  endif
                endif
                if ! this.w_NOAGGIORNAMENTO
                  * --- Write into CONTI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.CONTI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"ANCODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODPAG),'CONTI','ANCODPAG');
                    +",ANCODBA2 ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODBA2),'CONTI','ANCODBA2');
                    +",ANFLESIM ="+cp_NullLink(cp_ToStrODBC(this.w_ANFLESIM),'CONTI','ANFLESIM');
                    +",ANSAGINT ="+cp_NullLink(cp_ToStrODBC(this.w_ANSAGINT),'CONTI','ANSAGINT');
                    +",ANSPRINT ="+cp_NullLink(cp_ToStrODBC(this.w_ANSPRINT),'CONTI','ANSPRINT');
                        +i_ccchkf ;
                    +" where ";
                        +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPCON );
                        +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                           )
                  else
                    update (i_cTable) set;
                        ANCODPAG = this.w_ANCODPAG;
                        ,ANCODBA2 = this.w_ANCODBA2;
                        ,ANFLESIM = this.w_ANFLESIM;
                        ,ANSAGINT = this.w_ANSAGINT;
                        ,ANSPRINT = this.w_ANSPRINT;
                        &i_ccchkf. ;
                     where;
                        ANTIPCON = this.oParentObject.w_TIPCON ;
                        and ANCODICE = this.w_ANCODICE;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              else
                * --- Write into CONTI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.CONTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"ANCODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODPAG),'CONTI','ANCODPAG');
                  +",ANCODBA2 ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODBA2),'CONTI','ANCODBA2');
                      +i_ccchkf ;
                  +" where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPCON );
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                         )
                else
                  update (i_cTable) set;
                      ANCODPAG = this.w_ANCODPAG;
                      ,ANCODBA2 = this.w_ANCODBA2;
                      &i_ccchkf. ;
                   where;
                      ANTIPCON = this.oParentObject.w_TIPCON ;
                      and ANCODICE = this.w_ANCODICE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            case this.oParentObject.w_Sezione="Z"
              do case
                case this.oParentObject.w_AGGANGESCON="N"
                  this.w_ANGESCON = "N"
                  this.w_ANFLGCON = " "
                  this.w_ANPAGPAR = Space(5)
                  this.w_ANDATMOR = Cp_CharToDate("  -  -    ")
                case this.oParentObject.w_AGGANGESCON="M"
                  this.w_ANGESCON = "M"
                  this.w_ANFLGCON = " "
                  this.w_ANPAGPAR = iif(this.oParentObject.w_AggAnPagPar="S",this.oParentObject.w_NewAnPagPar,Nvl(Anpagfor,Space(5)))
                  this.w_ANDATMOR = iif(this.oParentObject.w_AggAnDatMor="S",this.oParentObject.w_NewAnDatMor,Nvl(Andatmor,Cp_CharToDate("  -  -    ")))
                  if this.oParentObject.w_AggAndatMor="S" And Empty(this.w_Anpagpar)
                    this.w_oERRORLOG.AddMsgLog("Verificare cliente: %1 %2. Pagamento moratoria non attivato",ALLTRIM(this.w_ANCODICE),this.w_ANDESCRI)     
                    this.w_NOAGGIORNAMENTO = .T.
                    this.w_MSGAVV = this.w_MSGAVV+1
                  endif
                case this.oParentObject.w_AGGANGESCON="B"
                  this.w_ANGESCON = "B"
                  this.w_ANFLGCON = iif(this.oParentObject.w_AggAnFlgCon="X",Nvl(Anflgcon," "),this.oParentObject.w_AggAnFlgCon)
                  this.w_ANPAGPAR = Space(5)
                  this.w_ANDATMOR = Cp_CharToDate("  -  -    ")
                case this.oParentObject.w_AGGANGESCON="X"
                  this.w_ANGESCON = Nvl(Gescon,"N")
                  this.w_ANFLGCON = iif(this.oParentObject.w_AggAnFlgCon="X",Nvl(Anflgcon," "),this.oParentObject.w_AggAnFlgCon)
                  this.w_ANPAGPAR = iif(this.oParentObject.w_AggAnPagPar="S",this.oParentObject.w_NewAnPagPar,Nvl(Anpagfor,Space(5)))
                  this.w_ANDATMOR = iif(this.oParentObject.w_AggAnDatMor="S",this.oParentObject.w_NewAnDatMor,Nvl(Andatmor,Cp_CharToDate("  -  -    ")))
                  do case
                    case this.w_Angescon="N"
                      if this.w_Anflgcon="S" Or Not Empty(this.w_Anpagpar) Or Not Empty(this.w_Andatmor)
                        this.w_oERRORLOG.AddMsgLog("Verificare cliente: %1 %2. Gestione contenzioso impostata a nessun blocco",ALLTRIM(this.w_ANCODICE),this.w_ANDESCRI)     
                        this.w_NOAGGIORNAMENTO = .T.
                        this.w_MSGAVV = this.w_MSGAVV+1
                      endif
                    case this.w_Angescon="M"
                      if this.w_Anflgcon="S"
                        this.w_oERRORLOG.AddMsgLog("Verificare cliente: %1 %2. Gestione contenzioso impostata a pagamento moratoria",ALLTRIM(this.w_ANCODICE),this.w_ANDESCRI)     
                        this.w_NOAGGIORNAMENTO = .T.
                        this.w_MSGAVV = this.w_MSGAVV+1
                      endif
                    case this.w_Angescon="B"
                      if Not Empty(this.w_Anpagpar) Or Not Empty(this.w_Andatmor)
                        this.w_oERRORLOG.AddMsgLog("Verificare cliente: %1 %2. Gestione contenzioso impostata a blocco attivabile",ALLTRIM(this.w_ANCODICE),this.w_ANDESCRI)     
                        this.w_NOAGGIORNAMENTO = .T.
                        this.w_MSGAVV = this.w_MSGAVV+1
                      endif
                  endcase
                  if Not Empty(this.w_Andatmor) And Empty(this.w_Anpagpar) And ! this.w_NoAggiornamento
                    this.w_oERRORLOG.AddMsgLog("Verificare cliente: %1 %2. Pagamento moratoria non attivato",ALLTRIM(this.w_ANCODICE),this.w_ANDESCRI)     
                    this.w_NOAGGIORNAMENTO = .T.
                    this.w_MSGAVV = this.w_MSGAVV+1
                  endif
              endcase
              if this.oParentObject.w_AGGANFLESIM="S"
                this.w_ANFLESIM = "S"
                this.w_ANSAGINT = 0
                this.w_ANSPRINT = "N"
              else
                this.w_ANFLESIM = iif(this.oParentObject.w_AggAnFleSim="X",Nvl(Anflesim," "),this.oParentObject.w_AggAnFleSim)
                this.w_ANSAGINT = iif(this.oParentObject.w_AggAnSagInt="S",this.oParentObject.w_NewAnSagInt,Nvl(Ansagint,0))
                this.w_ANSPRINT = iif(this.oParentObject.w_AggAnSprInt="X",Nvl(Ansprint," "),this.oParentObject.w_AggAnSprInt)
                * --- Se ho attivato il check "Aggiorna interessi di mora" ma il cliente ha il flag
                *     "Escludi applicazione interessi di mora"
                if (this.oParentObject.w_AGGANSAGINT="S" And this.w_ANFLESIM="S") Or (this.w_ANSPRINT="S" And this.w_ANFLESIM="S" )
                  this.w_oERRORLOG.AddMsgLog("Verificare cliente: %1 %2. Escludi applicazione interessi di mora attivato",ALLTRIM(this.w_ANCODICE),this.w_ANDESCRI)     
                  this.w_NOAGGIORNAMENTO = .T.
                  this.w_MSGAVV = this.w_MSGAVV+1
                endif
                if this.w_ANSPRINT="S" And this.w_ANSAGINT=0
                  this.w_oERRORLOG.AddMsgLog("Verificare cliente: %1 %2.Interessi di mora non valorizzati",ALLTRIM(this.w_ANCODICE),this.w_ANDESCRI)     
                  this.w_NOAGGIORNAMENTO = .T.
                  this.w_MSGAVV = this.w_MSGAVV+1
                endif
              endif
              if ! this.w_NOAGGIORNAMENTO
                * --- Write into CONTI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.CONTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"ANGESCON ="+cp_NullLink(cp_ToStrODBC(this.w_ANGESCON),'CONTI','ANGESCON');
                  +",ANFLGCON ="+cp_NullLink(cp_ToStrODBC(this.w_ANFLGCON),'CONTI','ANFLGCON');
                  +",ANPAGPAR ="+cp_NullLink(cp_ToStrODBC(this.w_ANPAGPAR),'CONTI','ANPAGPAR');
                  +",ANDATMOR ="+cp_NullLink(cp_ToStrODBC(this.w_ANDATMOR),'CONTI','ANDATMOR');
                  +",ANFLESIM ="+cp_NullLink(cp_ToStrODBC(this.w_ANFLESIM),'CONTI','ANFLESIM');
                  +",ANSAGINT ="+cp_NullLink(cp_ToStrODBC(this.w_ANSAGINT),'CONTI','ANSAGINT');
                  +",ANSPRINT ="+cp_NullLink(cp_ToStrODBC(this.w_ANSPRINT),'CONTI','ANSPRINT');
                      +i_ccchkf ;
                  +" where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPCON );
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                         )
                else
                  update (i_cTable) set;
                      ANGESCON = this.w_ANGESCON;
                      ,ANFLGCON = this.w_ANFLGCON;
                      ,ANPAGPAR = this.w_ANPAGPAR;
                      ,ANDATMOR = this.w_ANDATMOR;
                      ,ANFLESIM = this.w_ANFLESIM;
                      ,ANSAGINT = this.w_ANSAGINT;
                      ,ANSPRINT = this.w_ANSPRINT;
                      &i_ccchkf. ;
                   where;
                      ANTIPCON = this.oParentObject.w_TIPCON ;
                      and ANCODICE = this.w_ANCODICE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            case this.oParentObject.w_Sezione="R"
              this.w_ANRITENU = Nvl(Ritenu,"N")
              this.w_ANCODIRP = iif(this.oParentObject.w_AggAnCodIrpF="S",this.oParentObject.w_NewAnCodIrpF,Nvl(Ancodirp,Space(5)))
              this.w_ANCODTR2 = iif(this.oParentObject.w_AggAnCodTr2="S",this.oParentObject.w_NewAnCodTr2,Nvl(Ancodtr2,Space(5)))
              this.w_ANCAURIT = iif(this.oParentObject.w_AggAnCauRitF="S",this.oParentObject.w_NewAnCauRitF,Nvl(Ancaurit,Space(1)))
              this.w_ANPEINPS = iif(this.oParentObject.w_AggAnPeInps="S",this.oParentObject.w_NewAnPeInps,Nvl(Anpeinps,0))
              this.w_ANRIINPS = iif(this.oParentObject.w_AggAnRiInps="S",this.oParentObject.w_NewAnRiInps,Nvl(Anriinps,0))
              this.w_ANCOINPS = iif(this.oParentObject.w_AggAnCoInps="S",this.oParentObject.w_NewAnCoInps,Nvl(Ancoinps,0))
              this.w_ANCASPRO = iif(this.oParentObject.w_AggAnCasPro="S",this.oParentObject.w_NewAnCasPro,Nvl(Ancaspro,0))
              this.w_ANCODATT = iif(this.oParentObject.w_AggAnCodAtt="S",this.oParentObject.w_NewAnCodAtt,Nvl(Ancodatt,0))
              this.w_ANCODASS = iif(this.oParentObject.w_AggAnCodAss="S",this.oParentObject.w_NewAnCodAss,Nvl(Ancodass,Space(3)))
              this.w_ANCOIMPS = iif(this.oParentObject.w_AggAnCoiMps="S",this.oParentObject.w_NewAnCoiMps,Nvl(Ancoimps,Space(20)))
              do case
                case this.oParentObject.w_AGGANRITENU="N"
                  this.w_ANRITENU = "N"
                  this.w_ANCODIRP = Space(5)
                  this.w_ANCODTR2 = Space(5)
                  this.w_ANCAURIT = Space(1)
                  this.w_ANPEINPS = 0
                  this.w_ANRIINPS = 0
                  this.w_ANCOINPS = 0
                  this.w_ANCASPRO = 0
                  this.w_ANCODATT = 0
                  this.w_ANCODASS = Space(3)
                  this.w_ANCOIMPS = Space(20)
                case this.oParentObject.w_AGGANRITENU="S"
                  this.w_ANRITENU = "S"
                  this.w_ANCODTR2 = Space(5)
                  this.w_ANPEINPS = 0
                  this.w_ANRIINPS = 0
                  this.w_ANCOINPS = 0
                  this.w_ANCODATT = 0
                  this.w_ANCODASS = Space(3)
                  this.w_ANCOIMPS = Space(20)
                case this.oParentObject.w_AGGANRITENU="C"
                  this.w_ANRITENU = "C"
                  if Empty(this.w_Ancodtr2)
                    this.w_oERRORLOG.AddMsgLog("Verificare fornitore: %1 %2. Contributo previdenziale non specificato",ALLTRIM(this.w_ANCODICE),this.w_ANDESCRI)     
                    this.w_NOAGGIORNAMENTO = .T.
                    this.w_MSGAVV = this.w_MSGAVV+1
                  endif
                case this.oParentObject.w_AGGANRITENU="X"
                  do case
                    case this.w_ANRITENU="N"
                      if Not Empty(this.w_Ancodirp) Or Not Empty(this.w_Ancodtr2) Or Not Empty(this.w_Ancaurit) Or this.w_Anpeinps<>0 Or this.w_Anriinps<>0 Or this.w_Ancoinps<>0 Or this.w_Ancaspro<>0 Or this.w_Ancodatt<>0 Or Not Empty(this.w_Ancodass) Or Not Empty(this.w_Ancoimps)
                        this.w_oERRORLOG.AddMsgLog("Verificare fornitore: %1 %2. Non soggetto a ritenute",ALLTRIM(this.w_ANCODICE),this.w_ANDESCRI)     
                        this.w_NOAGGIORNAMENTO = .T.
                        this.w_MSGAVV = this.w_MSGAVV+1
                      endif
                    case this.w_ANRITENU="S"
                      if Not Empty(this.w_Ancodtr2) Or this.w_Anpeinps<>0 Or this.w_Anriinps<>0 Or this.w_Ancoinps<>0 Or this.w_Ancodatt<>0 Or Not Empty(this.w_Ancodass) Or Not Empty(this.w_Ancoimps)
                        this.w_oERRORLOG.AddMsgLog("Verificare fornitore: %1 %2. Soggetto a ritenute solo I.R.PE.F.",ALLTRIM(this.w_ANCODICE),this.w_ANDESCRI)     
                        this.w_NOAGGIORNAMENTO = .T.
                        this.w_MSGAVV = this.w_MSGAVV+1
                      endif
                  endcase
              endcase
              if ! this.w_NOAGGIORNAMENTO
                * --- Write into CONTI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.CONTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"ANRITENU ="+cp_NullLink(cp_ToStrODBC(this.w_ANRITENU),'CONTI','ANRITENU');
                  +",ANCODIRP ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODIRP),'CONTI','ANCODIRP');
                  +",ANCODTR2 ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODTR2),'CONTI','ANCODTR2');
                  +",ANCAURIT ="+cp_NullLink(cp_ToStrODBC(this.w_ANCAURIT),'CONTI','ANCAURIT');
                  +",ANPEINPS ="+cp_NullLink(cp_ToStrODBC(this.w_ANPEINPS),'CONTI','ANPEINPS');
                  +",ANRIINPS ="+cp_NullLink(cp_ToStrODBC(this.w_ANRIINPS),'CONTI','ANRIINPS');
                  +",ANCOINPS ="+cp_NullLink(cp_ToStrODBC(this.w_ANCOINPS),'CONTI','ANCOINPS');
                  +",ANCASPRO ="+cp_NullLink(cp_ToStrODBC(this.w_ANCASPRO),'CONTI','ANCASPRO');
                  +",ANCODATT ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODATT),'CONTI','ANCODATT');
                  +",ANCODASS ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODASS),'CONTI','ANCODASS');
                  +",ANCOIMPS ="+cp_NullLink(cp_ToStrODBC(this.w_ANCOIMPS),'CONTI','ANCOIMPS');
                      +i_ccchkf ;
                  +" where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_TIPCON );
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_ANCODICE);
                         )
                else
                  update (i_cTable) set;
                      ANRITENU = this.w_ANRITENU;
                      ,ANCODIRP = this.w_ANCODIRP;
                      ,ANCODTR2 = this.w_ANCODTR2;
                      ,ANCAURIT = this.w_ANCAURIT;
                      ,ANPEINPS = this.w_ANPEINPS;
                      ,ANRIINPS = this.w_ANRIINPS;
                      ,ANCOINPS = this.w_ANCOINPS;
                      ,ANCASPRO = this.w_ANCASPRO;
                      ,ANCODATT = this.w_ANCODATT;
                      ,ANCODASS = this.w_ANCODASS;
                      ,ANCOIMPS = this.w_ANCOIMPS;
                      &i_ccchkf. ;
                   where;
                      ANTIPCON = this.oParentObject.w_TIPCON ;
                      and ANCODICE = this.w_ANCODICE;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
          endcase
          Endscan
          if this.w_MSGAVV>0
            if this.oParentObject.w_TIPCON="C"
              this.w_MESS_ELA = "Elaborazione completata.%0Non sono stati aggiornati  %1 clienti"
            else
              this.w_MESS_ELA = "Elaborazione completata.%0Non sono stati aggiornati  %1 fornitori"
            endif
            AH_ERRORMSG(this.w_MESS_ELA,64,,ALLTRIM(STR(this.w_MSGAVV)))
            * --- Lancia report
            this.w_oERRORLOG.PrintLog(this.oParentObject,"Segnalazioni in fase di generazione")     
          else
            ah_errormsg("Elaborazione terminata con successo")
          endif
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          This.oParentObject.NotifyEvent("Interroga")
          this.w_ZOOM.SetFocus()     
        endif
      case this.pParame="P"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.oParentObject.w_AGGMASCON = "N"
    this.oParentObject.w_AGGCATCON = "N"
    this.oParentObject.w_AGGANCODIVA = "N"
    this.oParentObject.w_AGGANTIPOPE = "N"
    this.oParentObject.w_AGGANPARTSN = "X"
    this.oParentObject.w_AGGANFLRITE = "X"
    this.oParentObject.w_AGGANCODIRP = "N"
    this.oParentObject.w_AGGANCAURIT = "N"
    this.oParentObject.w_AGGANCATCOM = "N"
    this.oParentObject.w_AGGANCATSCM = "N"
    this.oParentObject.w_AGGANCODZON = "N"
    this.oParentObject.w_AGGANGRUPRO = "N"
    this.oParentObject.w_AGGANCODAG1 = "N"
    this.oParentObject.w_AGGANMAGTER = "N"
    this.oParentObject.w_AGGANFLFIDO = "X"
    this.oParentObject.w_AGGANCODPAG = "N"
    this.oParentObject.w_AGGANCODBA2 = "N"
    this.oParentObject.w_AGGANGESCON = "X"
    this.oParentObject.w_AGGANFLGCON = "X"
    this.oParentObject.w_AGGANPAGPAR = "N"
    this.oParentObject.w_AGGANDATMOR = "N"
    this.oParentObject.w_AGGANFLESIM = "X"
    this.oParentObject.w_AGGANSAGINT = "N"
    this.oParentObject.w_AGGANSPRINT = "X"
    this.oParentObject.w_AGGANRITENU = "X"
    this.oParentObject.w_AGGANCODIRPF = "N"
    this.oParentObject.w_AGGANCAURITF = "N"
    this.oParentObject.w_AGGANCASPRO = "N"
    this.oParentObject.w_AGGANCODTR2 = "N"
    this.oParentObject.w_AGGANPEINPS = "N"
    this.oParentObject.w_AGGANRIINPS = "N"
    this.oParentObject.w_AGGANCOINPS = "N"
    this.oParentObject.w_AGGANCODATT = "N"
    this.oParentObject.w_AGGANCODASS = "N"
    this.oParentObject.w_AGGANCOIMPS = "N"
    this.oParentObject.w_AGGANCODEST = "N"
    this.oParentObject.w_AGGANCODCLA = "N"
  endproc


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
