* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mug                                                        *
*              Uffici di consultazione per utente                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-12-29                                                      *
* Last revis.: 2014-10-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mug")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mug")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mug")
  return

* --- Class definition
define class tgsar_mug as StdPCForm
  Width  = 615
  Height = 200
  Top    = 42
  Left   = 50
  cComment = "Uffici di consultazione per utente"
  cPrg = "gsar_mug"
  HelpContextID=63534953
  add object cnt as tcgsar_mug
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mug as PCContext
  w_CPROWORD = 0
  w_UFCODUFF = space(15)
  w_UFLOCALI = space(50)
  w_UF__NOME = space(80)
  w_UFCODPER = space(5)
  w_UFPRORIF = space(2)
  w_UFCODPDA = space(5)
  w_FiltDistretto                = space(4)
  w_TIPUFF = space(1)
  w_DesUff = space(150)
  w_FiltLocalita = space(10)
  w_UFULTIMP = space(14)
  proc Save(i_oFrom)
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_UFCODUFF = i_oFrom.w_UFCODUFF
    this.w_UFLOCALI = i_oFrom.w_UFLOCALI
    this.w_UF__NOME = i_oFrom.w_UF__NOME
    this.w_UFCODPER = i_oFrom.w_UFCODPER
    this.w_UFPRORIF = i_oFrom.w_UFPRORIF
    this.w_UFCODPDA = i_oFrom.w_UFCODPDA
    this.w_FiltDistretto                = i_oFrom.w_FiltDistretto               
    this.w_TIPUFF = i_oFrom.w_TIPUFF
    this.w_DesUff = i_oFrom.w_DesUff
    this.w_FiltLocalita = i_oFrom.w_FiltLocalita
    this.w_UFULTIMP = i_oFrom.w_UFULTIMP
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_UFCODUFF = this.w_UFCODUFF
    i_oTo.w_UFLOCALI = this.w_UFLOCALI
    i_oTo.w_UF__NOME = this.w_UF__NOME
    i_oTo.w_UFCODPER = this.w_UFCODPER
    i_oTo.w_UFPRORIF = this.w_UFPRORIF
    i_oTo.w_UFCODPDA = this.w_UFCODPDA
    i_oTo.w_FiltDistretto                = this.w_FiltDistretto               
    i_oTo.w_TIPUFF = this.w_TIPUFF
    i_oTo.w_DesUff = this.w_DesUff
    i_oTo.w_FiltLocalita = this.w_FiltLocalita
    i_oTo.w_UFULTIMP = this.w_UFULTIMP
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mug as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 615
  Height = 200
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-02"
  HelpContextID=63534953
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  UTE_PDAD_IDX = 0
  UFF_GIUD_IDX = 0
  cFile = "UTE_PDAD"
  cKeySelect = "UFCODPER,UFCODPDA"
  cKeyWhere  = "UFCODPER=this.w_UFCODPER and UFCODPDA=this.w_UFCODPDA"
  cKeyDetail  = "UFCODUFF=this.w_UFCODUFF and UFCODPER=this.w_UFCODPER and UFCODPDA=this.w_UFCODPDA"
  cKeyWhereODBC = '"UFCODPER="+cp_ToStrODBC(this.w_UFCODPER)';
      +'+" and UFCODPDA="+cp_ToStrODBC(this.w_UFCODPDA)';

  cKeyDetailWhereODBC = '"UFCODUFF="+cp_ToStrODBC(this.w_UFCODUFF)';
      +'+" and UFCODPER="+cp_ToStrODBC(this.w_UFCODPER)';
      +'+" and UFCODPDA="+cp_ToStrODBC(this.w_UFCODPDA)';

  cKeyWhereODBCqualified = '"UTE_PDAD.UFCODPER="+cp_ToStrODBC(this.w_UFCODPER)';
      +'+" and UTE_PDAD.UFCODPDA="+cp_ToStrODBC(this.w_UFCODPDA)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'UTE_PDAD.CPROWORD '
  cPrg = "gsar_mug"
  cComment = "Uffici di consultazione per utente"
  i_nRowNum = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CPROWORD = 0
  w_UFCODUFF = space(15)
  w_UFLOCALI = space(50)
  w_UF__NOME = space(80)
  w_UFCODPER = space(5)
  w_UFPRORIF = space(2)
  w_UFCODPDA = space(5)
  w_FiltDistretto                = space(4)
  w_TIPUFF = space(1)
  w_DesUff = space(150)
  w_FiltLocalita = space(10)
  w_UFULTIMP = ctot('')
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mugPag1","gsar_mug",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='UFF_GIUD'
    this.cWorkTables[2]='UTE_PDAD'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.UTE_PDAD_IDX,5],7]
    this.nPostItConn=i_TableProp[this.UTE_PDAD_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_mug'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from UTE_PDAD where UFCODUFF=KeySet.UFCODUFF
    *                            and UFCODPER=KeySet.UFCODPER
    *                            and UFCODPDA=KeySet.UFCODPDA
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.UTE_PDAD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UTE_PDAD_IDX,2],this.bLoadRecFilter,this.UTE_PDAD_IDX,"gsar_mug")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('UTE_PDAD')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "UTE_PDAD.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' UTE_PDAD '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'UFCODPER',this.w_UFCODPER  ,'UFCODPDA',this.w_UFCODPDA  )
      select * from (i_cTable) UTE_PDAD where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_FiltDistretto                = space(4)
        .w_TIPUFF = space(1)
        .w_FiltLocalita = space(10)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_UFCODPER = NVL(UFCODPER,space(5))
        .w_UFCODPDA = NVL(UFCODPDA,space(5))
        cp_LoadRecExtFlds(this,'UTE_PDAD')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_UFLOCALI = space(50)
          .w_UF__NOME = space(80)
          .w_UFPRORIF = space(2)
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_UFCODUFF = NVL(UFCODUFF,space(15))
          if link_2_2_joined
            this.w_UFCODUFF = NVL(UFCODICE202,NVL(this.w_UFCODUFF,space(15)))
            this.w_UF__NOME = NVL(UF__NOME202,space(80))
            this.w_UFLOCALI = NVL(UFLOCALI202,space(50))
            this.w_UFPRORIF = NVL(UFPRORIF202,space(2))
          else
          .link_2_2('Load')
          endif
        .w_DesUff = ALLTRIM(.w_UF__NOME)+IIF(!EMPTY(.w_UFLOCALI),' ( '+ALLTRIM(.w_UFLOCALI)+' )','')
          .w_UFULTIMP = NVL(UFULTIMP,ctot(""))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace UFCODUFF with .w_UFCODUFF
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_CPROWORD=10
      .w_UFCODUFF=space(15)
      .w_UFLOCALI=space(50)
      .w_UF__NOME=space(80)
      .w_UFCODPER=space(5)
      .w_UFPRORIF=space(2)
      .w_UFCODPDA=space(5)
      .w_FiltDistretto               =space(4)
      .w_TIPUFF=space(1)
      .w_DesUff=space(150)
      .w_FiltLocalita=space(10)
      .w_UFULTIMP=ctot("")
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_UFCODUFF))
         .link_2_2('Full')
        endif
        .DoRTCalc(3,9,.f.)
        .w_DesUff = ALLTRIM(.w_UF__NOME)+IIF(!EMPTY(.w_UFLOCALI),' ( '+ALLTRIM(.w_UFLOCALI)+' )','')
      endif
    endwith
    cp_BlankRecExtFlds(this,'UTE_PDAD')
    this.DoRTCalc(11,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'UTE_PDAD',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.UTE_PDAD_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UFCODPER,"UFCODPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UFCODPDA,"UFCODPDA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(5);
      ,t_UFCODUFF C(15);
      ,t_DesUff C(150);
      ,t_UFULTIMP T(14);
      ,UFCODUFF C(15);
      ,t_UFLOCALI C(50);
      ,t_UF__NOME C(80);
      ,t_UFPRORIF C(2);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mugbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oUFCODUFF_2_2.controlsource=this.cTrsName+'.t_UFCODUFF'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDesUff_2_6.controlsource=this.cTrsName+'.t_DesUff'
    this.oPgFRm.Page1.oPag.oUFULTIMP_2_7.controlsource=this.cTrsName+'.t_UFULTIMP'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(60)
    this.AddVLine(152)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.UTE_PDAD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UTE_PDAD_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.UTE_PDAD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UTE_PDAD_IDX,2])
      *
      * insert into UTE_PDAD
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'UTE_PDAD')
        i_extval=cp_InsertValODBCExtFlds(this,'UTE_PDAD')
        i_cFldBody=" "+;
                  "(CPROWORD,UFCODUFF,UFCODPER,UFCODPDA,UFULTIMP,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_UFCODUFF)+","+cp_ToStrODBC(this.w_UFCODPER)+","+cp_ToStrODBC(this.w_UFCODPDA)+","+cp_ToStrODBC(this.w_UFULTIMP)+;
             ","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'UTE_PDAD')
        i_extval=cp_InsertValVFPExtFlds(this,'UTE_PDAD')
        cp_CheckDeletedKey(i_cTable,0,'UFCODUFF',this.w_UFCODUFF,'UFCODPER',this.w_UFCODPER,'UFCODPDA',this.w_UFCODPDA)
        INSERT INTO (i_cTable) (;
                   CPROWORD;
                  ,UFCODUFF;
                  ,UFCODPER;
                  ,UFCODPDA;
                  ,UFULTIMP;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CPROWORD;
                  ,this.w_UFCODUFF;
                  ,this.w_UFCODPER;
                  ,this.w_UFCODPDA;
                  ,this.w_UFULTIMP;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.UTE_PDAD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UTE_PDAD_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_UFCODUFF))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'UTE_PDAD')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and UFCODUFF="+cp_ToStrODBC(&i_TN.->UFCODUFF)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'UTE_PDAD')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and UFCODUFF=&i_TN.->UFCODUFF;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_UFCODUFF))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and UFCODUFF="+cp_ToStrODBC(&i_TN.->UFCODUFF)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and UFCODUFF=&i_TN.->UFCODUFF;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace UFCODUFF with this.w_UFCODUFF
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update UTE_PDAD
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'UTE_PDAD')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",UFULTIMP="+cp_ToStrODBC(this.w_UFULTIMP)+;
                     ",UFCODUFF="+cp_ToStrODBC(this.w_UFCODUFF)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and UFCODUFF="+cp_ToStrODBC(UFCODUFF)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'UTE_PDAD')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,UFULTIMP=this.w_UFULTIMP;
                     ,UFCODUFF=this.w_UFCODUFF;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and UFCODUFF=&i_TN.->UFCODUFF;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.UTE_PDAD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.UTE_PDAD_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_UFCODUFF))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete UTE_PDAD
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and UFCODUFF="+cp_ToStrODBC(&i_TN.->UFCODUFF)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and UFCODUFF=&i_TN.->UFCODUFF;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_UFCODUFF))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.UTE_PDAD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.UTE_PDAD_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(1,9,.t.)
          .w_DesUff = ALLTRIM(.w_UF__NOME)+IIF(!EMPTY(.w_UFLOCALI),' ( '+ALLTRIM(.w_UFLOCALI)+' )','')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_UFLOCALI with this.w_UFLOCALI
      replace t_UF__NOME with this.w_UF__NOME
      replace t_UFPRORIF with this.w_UFPRORIF
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=UFCODUFF
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UFF_GIUD_IDX,3]
    i_lTable = "UFF_GIUD"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UFF_GIUD_IDX,2], .t., this.UFF_GIUD_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UFF_GIUD_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UFCODUFF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UFF_GIUD')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UFCODICE like "+cp_ToStrODBC(trim(this.w_UFCODUFF)+"%");

          i_ret=cp_SQL(i_nConn,"select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UFCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UFCODICE',trim(this.w_UFCODUFF))
          select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UFCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_UFCODUFF)==trim(_Link_.UFCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" UF__NOME like "+cp_ToStrODBC(trim(this.w_UFCODUFF)+"%");

            i_ret=cp_SQL(i_nConn,"select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" UF__NOME like "+cp_ToStr(trim(this.w_UFCODUFF)+"%");

            select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" UFLOCALI like "+cp_ToStrODBC(trim(this.w_UFCODUFF)+"%");

            i_ret=cp_SQL(i_nConn,"select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" UFLOCALI like "+cp_ToStr(trim(this.w_UFCODUFF)+"%");

            select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_UFCODUFF) and !this.bDontReportError
            deferred_cp_zoom('UFF_GIUD','*','UFCODICE',cp_AbsName(oSource.parent,'oUFCODUFF_2_2'),i_cWhere,'',"Codici uffici giudiziari",'GSAL_KWS.UFF_GIUD_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF";
                     +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',oSource.xKey(1))
            select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UFCODUFF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF";
                   +" from "+i_cTable+" "+i_lTable+" where UFCODICE="+cp_ToStrODBC(this.w_UFCODUFF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UFCODICE',this.w_UFCODUFF)
            select UFCODICE,UF__NOME,UFLOCALI,UFPRORIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UFCODUFF = NVL(_Link_.UFCODICE,space(15))
      this.w_UF__NOME = NVL(_Link_.UF__NOME,space(80))
      this.w_UFLOCALI = NVL(_Link_.UFLOCALI,space(50))
      this.w_UFPRORIF = NVL(_Link_.UFPRORIF,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_UFCODUFF = space(15)
      endif
      this.w_UF__NOME = space(80)
      this.w_UFLOCALI = space(50)
      this.w_UFPRORIF = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_UFCODUFF) OR !EMPTY(.w_UFPRORIF)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Selezionare un ufficio che abbia un gestore associato")
        endif
        this.w_UFCODUFF = space(15)
        this.w_UF__NOME = space(80)
        this.w_UFLOCALI = space(50)
        this.w_UFPRORIF = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UFF_GIUD_IDX,2])+'\'+cp_ToStr(_Link_.UFCODICE,1)
      cp_ShowWarn(i_cKey,this.UFF_GIUD_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UFCODUFF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UFF_GIUD_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UFF_GIUD_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.UFCODICE as UFCODICE202"+ ",link_2_2.UF__NOME as UF__NOME202"+ ",link_2_2.UFLOCALI as UFLOCALI202"+ ",link_2_2.UFPRORIF as UFPRORIF202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on UTE_PDAD.UFCODUFF=link_2_2.UFCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and UTE_PDAD.UFCODUFF=link_2_2.UFCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oUFULTIMP_2_7.value==this.w_UFULTIMP)
      this.oPgFrm.Page1.oPag.oUFULTIMP_2_7.value=this.w_UFULTIMP
      replace t_UFULTIMP with this.oPgFrm.Page1.oPag.oUFULTIMP_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUFCODUFF_2_2.value==this.w_UFCODUFF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUFCODUFF_2_2.value=this.w_UFCODUFF
      replace t_UFCODUFF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUFCODUFF_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDesUff_2_6.value==this.w_DesUff)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDesUff_2_6.value=this.w_DesUff
      replace t_DesUff with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDesUff_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'UTE_PDAD')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(EMPTY(.w_UFCODUFF) OR !EMPTY(.w_UFPRORIF)) and not(empty(.w_UFCODUFF)) and (not(Empty(.w_UFCODUFF)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oUFCODUFF_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Selezionare un ufficio che abbia un gestore associato")
      endcase
      if not(Empty(.w_UFCODUFF))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_UFCODUFF)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(99999,cp_maxroword()+10)
      .w_UFCODUFF=space(15)
      .w_UFLOCALI=space(50)
      .w_UF__NOME=space(80)
      .w_UFPRORIF=space(2)
      .w_DesUff=space(150)
      .w_UFULTIMP=ctot("")
      .DoRTCalc(1,2,.f.)
      if not(empty(.w_UFCODUFF))
        .link_2_2('Full')
      endif
      .DoRTCalc(3,9,.f.)
        .w_DesUff = ALLTRIM(.w_UF__NOME)+IIF(!EMPTY(.w_UFLOCALI),' ( '+ALLTRIM(.w_UFLOCALI)+' )','')
    endwith
    this.DoRTCalc(11,12,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_UFCODUFF = t_UFCODUFF
    this.w_UFLOCALI = t_UFLOCALI
    this.w_UF__NOME = t_UF__NOME
    this.w_UFPRORIF = t_UFPRORIF
    this.w_DesUff = t_DesUff
    this.w_UFULTIMP = t_UFULTIMP
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_UFCODUFF with this.w_UFCODUFF
    replace t_UFLOCALI with this.w_UFLOCALI
    replace t_UF__NOME with this.w_UF__NOME
    replace t_UFPRORIF with this.w_UFPRORIF
    replace t_DesUff with this.w_DesUff
    replace t_UFULTIMP with this.w_UFULTIMP
    if i_srv='A'
      replace UFCODUFF with this.w_UFCODUFF
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mugPag1 as StdContainer
  Width  = 611
  height = 200
  stdWidth  = 611
  stdheight = 200
  resizeXpos=411
  resizeYpos=130
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=11, top=27, width=477,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="CPROWORD",Label1="Riga",Field2="UFCODUFF",Label2="Ufficio",Field3="DesUff",Label3="Descrizione",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 252555898

  add object oStr_1_6 as StdString with uid="NLEMPALPYW",Visible=.t., Left=11, Top=5,;
    Alignment=0, Width=176, Height=18,;
    Caption="Uffici Giudiziari di consultazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="JCDHWRVIUW",Visible=.t., Left=11, Top=172,;
    Alignment=0, Width=166, Height=18,;
    Caption="Data/ora ultima consultazione:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=1,top=49,;
    width=581+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=2,top=50,width=580+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='UFF_GIUD|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oUFULTIMP_2_7.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='UFF_GIUD'
        oDropInto=this.oBodyCol.oRow.oUFCODUFF_2_2
    endcase
    return(oDropInto)
  EndFunc


  add object oUFULTIMP_2_7 as StdTrsField with uid="CXPFGHLJFC",rtseq=12,rtrep=.t.,;
    cFormVar="w_UFULTIMP",value=ctot(""),enabled=.f.,;
    HelpContextID = 180888726,;
    cTotal="", bFixedPos=.t., cQueryName = "UFULTIMP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=183, Top=172

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mugBodyRow as CPBodyRowCnt
  Width=571
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="TZOHEYCQXW",rtseq=1,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 16448874,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=-2, Top=0, cSayPict=["99999"], cGetPict=["99999"]

  add object oUFCODUFF_2_2 as StdTrsField with uid="AKTWYJIOEU",rtseq=2,rtrep=.t.,;
    cFormVar="w_UFCODUFF",value=space(15),isprimarykey=.t.,;
    ToolTipText = "Codice ufficio",;
    HelpContextID = 171309940,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Selezionare un ufficio che abbia un gestore associato",;
   bGlobalFont=.t.,;
    Height=17, Width=90, Left=48, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="UFF_GIUD", oKey_1_1="UFCODICE", oKey_1_2="this.w_UFCODUFF"

  func oUFCODUFF_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oUFCODUFF_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oUFCODUFF_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oUFCODUFF_2_2.readonly and this.parent.oUFCODUFF_2_2.isprimarykey)
    do cp_zoom with 'UFF_GIUD','*','UFCODICE',cp_AbsName(this.parent,'oUFCODUFF_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Codici uffici giudiziari",'GSAL_KWS.UFF_GIUD_VZM',this.parent.oContained
   endif
  endproc

  add object oDesUff_2_6 as StdTrsField with uid="PKOLWCHMBE",rtseq=10,rtrep=.t.,;
    cFormVar="w_DesUff",value=space(150),enabled=.f.,;
    HelpContextID = 150151734,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=426, Left=140, Top=0, InputMask=replicate('X',150)
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mug','UTE_PDAD','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".UFCODPER=UTE_PDAD.UFCODPER";
  +" and "+i_cAliasName2+".UFCODPDA=UTE_PDAD.UFCODPDA";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
