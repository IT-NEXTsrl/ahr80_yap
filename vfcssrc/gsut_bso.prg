* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bso                                                        *
*              Send to                                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-06-10                                                      *
* Last revis.: 2018-07-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER,w_FILELISTNAME
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bso",oParentObject,m.pOPER,m.w_FILELISTNAME)
return(i_retval)

define class tgsut_bso as StdBatch
  * --- Local variables
  pOPER = space(2)
  w_FILELISTNAME = space(254)
  w_COPIADESCR = space(1)
  w_AE_TESTO = space(0)
  w_AECLALCP = space(1)
  w_AECODPRA = space(15)
  w_CNCODCAN = space(15)
  w_AEOGGETT = space(220)
  w_AEORIFIL = space(100)
  w_AEPATHFI = space(250)
  w_AETIALCP = space(1)
  w_CDMODALL = space(1)
  w_CLASALL = space(5)
  w_CNDESCAN = space(100)
  w_ORIGFILE = space(100)
  w_PRPATH = space(254)
  w_TIPOALL = space(5)
  w_IDSERIAL = space(20)
  w_LICLADOC = space(15)
  w_CDRIFTAB = space(20)
  w_SENDTO = .f.
  w_NUMFILE = 0
  w_FILELIST = space(0)
  w_COUNT = 0
  w_MSK = .NULL.
  w_MODALLEG = space(1)
  w_TIPOARCH = space(1)
  w_CLAALLE = space(5)
  w_TIPPATH = space(1)
  w_TIPOALLE = space(5)
  w_TIPCLA = space(1)
  w_AR_TABLE = space(20)
  w_SETALL = space(1)
  w_PATHSTD = space(254)
  w_CDALIASF = space(15)
  w_CDFLGDES = space(1)
  w_TESTOK = .f.
  w_FLOFFICE = .f.
  w_CODPRE = space(20)
  w_RAGPRE = space(10)
  w_estensione = space(10)
  w_Crea_Ind_Mod = space(1)
  w_QUERYMOD = space(254)
  w_PARENTCLASS = space(10)
  w_ISALT = .f.
  w_IDTIPBST = space(1)
  w_DATAARCH = ctod("  /  /  ")
  w_ORAARCH = space(8)
  w_PADATARC = space(1)
  w_DefaultOgge = space(220)
  w_DefaultPath = space(250)
  w_DESART = space(40)
  w_NOMSK = .f.
  w_EVSERIAL = space(10)
  w_EVOGGETT = space(100)
  w_DE__TIPO = space(1)
  w_TECLADOC = space(15)
  w_EV__ANNO = space(4)
  w_EVNOMINA = space(15)
  w_NOCLASS = .f.
  w_CLASSE = space(10)
  w_CONFMASK = .f.
  w_OPERAZIONE = space(1)
  w_ARCHIVIO = space(20)
  w_CDPUBWEB = space(1)
  w_DESCRI = space(40)
  w_CDCLAINF = space(5)
  w_PATIPARC = space(1)
  w_PATIPSEN = space(1)
  w_PERSONA = space(5)
  w_DPSERPRE = space(41)
  w_MODPERS = space(1)
  w_ChrSep = space(3)
  w_StrDate = space(15)
  w_CNPATHFA = space(254)
  w_INSPATIP = space(1)
  w_INSPACLA = space(1)
  w_PUBWEB = space(1)
  w_INARCHIVIO = space(1)
  w_NEWFILENAME = space(254)
  w_estensione = space(10)
  w_FILENAME = space(60)
  w_TIPOPERAZ = space(1)
  w_PATH_KRF = space(254)
  w_OK = .f.
  w_OLDORIGFILE = space(254)
  w_OLDCDMODALL = space(1)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_IDSERIAL = space(15)
  w_PATALL = space(254)
  w_NOMFIL = space(220)
  w_LORIFIL = space(254)
  w_AGG_ORI = space(100)
  w_AGG_FIL = space(100)
  w_MOCODICE = space(10)
  w_SPAZI = space(10)
  * --- WorkFile variables
  PROMCLAS_idx=0
  CAN_TIER_idx=0
  PROMINDI_idx=0
  PAR_ALTE_idx=0
  DIPENDEN_idx=0
  TIP_ALLE_idx=0
  CLA_ALLE_idx=0
  PRODINDI_idx=0
  PROMODEL_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Send To...
    * --- Variabili di GSUT_KSO
    if this.pOper = "IN" and g_DOCM <>"S"
      i_retcode = 'stop'
      return
    endif
    this.w_PARENTCLASS = iif(Type("This.oParentObject") ="O", Lower(This.oParentObject.class), "xxxxxxxxxx")
    this.w_ISALT = isalt()
    this.w_TESTOK = .F.
    if this.w_ISALT
      this.w_AR_TABLE = "CAN_TIER"
    endif
    this.w_NOMSK = .t.
    this.w_DE__TIPO = " "
    if this.w_PARENTCLASS="tgsfa_bev"
      this.w_AR_TABLE = "ANEVENTI"
      this.w_EVSERIAL = This.oparentobject.w_EVSERIAL
      this.w_EVOGGETT = This.oparentobject.w_EVOGGETT
      this.w_NOMSK = .f.
      this.w_DE__TIPO = This.oParentObject.w_DE__TIPO
      this.w_TECLADOC = this.oParentObject.w_TECLADOC
      this.w_EV__ANNO = this.oParentObject.w_EV__ANNO
      this.w_EVNOMINA = this.oParentObject.w_EVNOMINA
    endif
    this.w_CDRIFTAB = this.w_AR_TABLE
    this.w_SETALL = "N"
    this.w_Crea_Ind_Mod = "I"
    * --- Legge, se c'�, la classe di archiviazione standard
    if this.w_ISALT
      * --- Read from PROMCLAS
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PROMCLAS_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDTIPALL,CDPATSTD,CDALIASF,CDFLGDES"+;
          " from "+i_cTable+" PROMCLAS where ";
              +"CDRIFTAB = "+cp_ToStrODBC(this.w_AR_TABLE);
              +" and CDRIFDEF = "+cp_ToStrODBC("S");
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDTIPALL,CDPATSTD,CDALIASF,CDFLGDES;
          from (i_cTable) where;
              CDRIFTAB = this.w_AR_TABLE;
              and CDRIFDEF = "S";
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_LICLADOC = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
        this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
        this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
        this.w_CLAALLE = NVL(cp_ToDate(_read_.CDCLAALL),cp_NullValue(_read_.CDCLAALL))
        this.w_TIPPATH = NVL(cp_ToDate(_read_.CDCOMTIP),cp_NullValue(_read_.CDCOMTIP))
        this.w_TIPCLA = NVL(cp_ToDate(_read_.CDCOMCLA),cp_NullValue(_read_.CDCOMCLA))
        this.w_TIPOALLE = NVL(cp_ToDate(_read_.CDTIPALL),cp_NullValue(_read_.CDTIPALL))
        this.w_PATHSTD = NVL(cp_ToDate(_read_.CDPATSTD),cp_NullValue(_read_.CDPATSTD))
        this.w_CDALIASF = NVL(cp_ToDate(_read_.CDALIASF),cp_NullValue(_read_.CDALIASF))
        this.w_CDFLGDES = NVL(cp_ToDate(_read_.CDFLGDES),cp_NullValue(_read_.CDFLGDES))
        this.w_PATIPARC = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if empty(this.w_LICLADOC)
        * --- Verifica se c'� almeno una classe
        * --- Read from PROMCLAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PROMCLAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDTIPALL,CDALIASF,CDFLGDES"+;
            " from "+i_cTable+" PROMCLAS where ";
                +"CDRIFTAB = "+cp_ToStrODBC(this.w_AR_TABLE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDTIPALL,CDALIASF,CDFLGDES;
            from (i_cTable) where;
                CDRIFTAB = this.w_AR_TABLE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LICLADOC = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
          this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
          this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
          this.w_CLAALLE = NVL(cp_ToDate(_read_.CDCLAALL),cp_NullValue(_read_.CDCLAALL))
          this.w_TIPPATH = NVL(cp_ToDate(_read_.CDCOMTIP),cp_NullValue(_read_.CDCOMTIP))
          this.w_TIPCLA = NVL(cp_ToDate(_read_.CDCOMCLA),cp_NullValue(_read_.CDCOMCLA))
          this.w_TIPOALLE = NVL(cp_ToDate(_read_.CDTIPALL),cp_NullValue(_read_.CDTIPALL))
          this.w_CDALIASF = NVL(cp_ToDate(_read_.CDALIASF),cp_NullValue(_read_.CDALIASF))
          this.w_CDFLGDES = NVL(cp_ToDate(_read_.CDFLGDES),cp_NullValue(_read_.CDFLGDES))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_NOCLASS = i_rows=0
      endif
    else
      if this.w_PARENTCLASS="tgsfa_bev"
        * --- Read from PROMCLAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PROMCLAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDTIPALL,CDPATSTD,CDALIASF,CDFLGDES,CDRIFTAB"+;
            " from "+i_cTable+" PROMCLAS where ";
                +"CDCODCLA = "+cp_ToStrODBC(this.w_TECLADOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CDCODCLA,CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDTIPALL,CDPATSTD,CDALIASF,CDFLGDES,CDRIFTAB;
            from (i_cTable) where;
                CDCODCLA = this.w_TECLADOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LICLADOC = NVL(cp_ToDate(_read_.CDCODCLA),cp_NullValue(_read_.CDCODCLA))
          this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
          this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
          this.w_CLAALLE = NVL(cp_ToDate(_read_.CDCLAALL),cp_NullValue(_read_.CDCLAALL))
          this.w_TIPPATH = NVL(cp_ToDate(_read_.CDCOMTIP),cp_NullValue(_read_.CDCOMTIP))
          this.w_TIPCLA = NVL(cp_ToDate(_read_.CDCOMCLA),cp_NullValue(_read_.CDCOMCLA))
          this.w_TIPOALLE = NVL(cp_ToDate(_read_.CDTIPALL),cp_NullValue(_read_.CDTIPALL))
          this.w_PATHSTD = NVL(cp_ToDate(_read_.CDPATSTD),cp_NullValue(_read_.CDPATSTD))
          this.w_CDALIASF = NVL(cp_ToDate(_read_.CDALIASF),cp_NullValue(_read_.CDALIASF))
          this.w_CDFLGDES = NVL(cp_ToDate(_read_.CDFLGDES),cp_NullValue(_read_.CDFLGDES))
          this.w_AR_TABLE = NVL(cp_ToDate(_read_.CDRIFTAB),cp_NullValue(_read_.CDRIFTAB))
          this.w_PATIPARC = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        if Empty(this.w_LICLADOC)
          this.w_OPERAZIONE = "C"
          this.w_CONFMASK = .F.
          do GSUT_KKW with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_LICLADOC = this.w_CLASSE
        endif
        if Not Empty(this.w_LICLADOC)
          * --- Read from PROMCLAS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PROMCLAS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDTIPALL,CDALIASF,CDFLGDES"+;
              " from "+i_cTable+" PROMCLAS where ";
                  +"CDCODCLA = "+cp_ToStrODBC(this.w_LICLADOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CDMODALL,CDTIPARC,CDCLAALL,CDCOMTIP,CDCOMCLA,CDTIPALL,CDALIASF,CDFLGDES;
              from (i_cTable) where;
                  CDCODCLA = this.w_LICLADOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MODALLEG = NVL(cp_ToDate(_read_.CDMODALL),cp_NullValue(_read_.CDMODALL))
            this.w_TIPOARCH = NVL(cp_ToDate(_read_.CDTIPARC),cp_NullValue(_read_.CDTIPARC))
            this.w_CLAALLE = NVL(cp_ToDate(_read_.CDCLAALL),cp_NullValue(_read_.CDCLAALL))
            this.w_TIPPATH = NVL(cp_ToDate(_read_.CDCOMTIP),cp_NullValue(_read_.CDCOMTIP))
            this.w_TIPCLA = NVL(cp_ToDate(_read_.CDCOMCLA),cp_NullValue(_read_.CDCOMCLA))
            this.w_TIPOALLE = NVL(cp_ToDate(_read_.CDTIPALL),cp_NullValue(_read_.CDTIPALL))
            this.w_CDALIASF = NVL(cp_ToDate(_read_.CDALIASF),cp_NullValue(_read_.CDALIASF))
            this.w_CDFLGDES = NVL(cp_ToDate(_read_.CDFLGDES),cp_NullValue(_read_.CDFLGDES))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          i_rows=0
        endif
      endif
      this.w_NOCLASS = i_rows=0
    endif
    if this.w_NOCLASS
      Ah_ErrorMsg("Classe documentale per l'invia a non definita")
      i_retcode = 'stop'
      return
    endif
    * --- Verifica se l'utente pu� archiviare ....
    if SUBSTR( CHKPECLA( this.w_LICLADOC, i_CODUTE, this.w_AR_TABLE), 2, 1 ) <> "S"
      Ah_ErrorMsg("Non si possiedono i diritti per archiviare documenti con la classe documentale %1", "!", "", ALLTRIM(this.w_LICLADOC))
      i_retcode = 'stop'
      return
    endif
    this.w_DefaultOgge = ""
    * --- ( Type('This.oParentObject') ='O' and ( Left(Lower(This.oParentObject.class),5)='tgsfa' OR Left(Lower(This.oParentObject.class),5)='tgsal' ) ) OR (IsAlt() and pOper = 'GB' and Type('noParentObject') ='O' and Lower(noParentObject.class) = 'tgsal_bxl')
    if Left(this.w_PARENTCLASS,5)="tgsfa" OR Left(this.w_PARENTCLASS,5)="tgsal" OR (this.w_IsAlt and this.pOper = "GB" and Type("noParentObject") ="O" and Lower(noParentObject.class) = "tgsal_bxl")
      do case
        case this.w_IsAlt and this.pOper = "GB" and Type("noParentObject") ="O" and Lower(noParentObject.class) = "tgsal_bxl"
          this.w_AECODPRA = noParentObject.w_AECODPRA
        case this.w_IsAlt AND this.w_PARENTCLASS= "tgsal_bci"
          this.w_AECODPRA = oParentObject.w_AECODPRA
        otherwise
          this.w_AECODPRA = This.oParentObject.w_CODPRA
      endcase
      * --- Read from CAN_TIER
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CNPATHFA"+;
          " from "+i_cTable+" CAN_TIER where ";
              +"CNCODCAN = "+cp_ToStrODBC(this.w_AECODPRA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CNPATHFA;
          from (i_cTable) where;
              CNCODCAN = this.w_AECODPRA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PRPATH = NVL(cp_ToDate(_read_.CNPATHFA),cp_NullValue(_read_.CNPATHFA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from TIP_ALLE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.TIP_ALLE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_ALLE_idx,2],.t.,this.TIP_ALLE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TAPATTIP,TACOPTIP"+;
          " from "+i_cTable+" TIP_ALLE where ";
              +"TACODICE = "+cp_ToStrODBC(this.w_TIPOALL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TAPATTIP,TACOPTIP;
          from (i_cTable) where;
              TACODICE = this.w_TIPOALL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_TAPATTIP = NVL(cp_ToDate(_read_.TAPATTIP),cp_NullValue(_read_.TAPATTIP))
        this.w_AETIALCP = NVL(cp_ToDate(_read_.TACOPTIP),cp_NullValue(_read_.TACOPTIP))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from CLA_ALLE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CLA_ALLE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CLA_ALLE_idx,2],.t.,this.CLA_ALLE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "TAPATCLA,TACOPCLA"+;
          " from "+i_cTable+" CLA_ALLE where ";
              +"TACODICE = "+cp_ToStrODBC(this.w_CLAALLE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          TAPATCLA,TACOPCLA;
          from (i_cTable) where;
              TACODICE = this.w_CLAALLE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_TAPATCLA = NVL(cp_ToDate(_read_.TAPATCLA),cp_NullValue(_read_.TAPATCLA))
        this.w_AECLALCP = NVL(cp_ToDate(_read_.TACOPCLA),cp_NullValue(_read_.TACOPCLA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_AEPATHFI = UPPER(ADDBS(IIF(!Empty(this.w_PRPATH), this.w_PRPATH, this.w_PATHSTD))+ADDBS(IIF(this.w_AETIALCP="S", IIF(Not Empty(w_TAPATTIP), w_TAPATTIP, ADDBS(this.w_TIPOALL)),""))+ADDBS(IIF(this.w_AECLALCP="S", IIF(Not empty(w_TAPATCLA), w_TAPATCLA, this.w_CLASALL),"")))
      do case
        case Left(this.w_PARENTCLASS,5)="tgsal"
          * --- Oggetto da GSAL_
          this.w_DefaultOgge = This.oParentObject.w_AEOGGETT
        case Type("noParentObject") ="O" and Lower(noParentObject.class) = "tgsal_bxl"
          * --- Oggetto da GSAL_BXL
          this.w_DefaultOgge = noParentObject.w_AEOGGETT
      endcase
    endif
    if !this.w_ISALT and Not EMpty(this.w_PATHSTD)
      this.w_AEPATHFI = UPPER(ADDBS(this.w_PATHSTD))
    endif
    * --- Leggo parametri alterego
    this.w_PERSONA = readdipend( i_CodUte, "C")
    if Not Empty(this.w_PERSONA)
      * --- Read from DIPENDEN
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DIPENDEN_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DPMODALL,DPSERPRE"+;
          " from "+i_cTable+" DIPENDEN where ";
              +"DPCODICE = "+cp_ToStrODBC(this.w_PERSONA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DPMODALL,DPSERPRE;
          from (i_cTable) where;
              DPCODICE = this.w_PERSONA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MODPERS = NVL(cp_ToDate(_read_.DPMODALL),cp_NullValue(_read_.DPMODALL))
        this.w_DPSERPRE = NVL(cp_ToDate(_read_.DPSERPRE),cp_NullValue(_read_.DPSERPRE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    if this.w_ISALT
      * --- Read from PAR_ALTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_ALTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_ALTE_idx,2],.t.,this.PAR_ALTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PATIPARC,PATIPSEN,PADATARC"+;
          " from "+i_cTable+" PAR_ALTE where ";
              +"PACODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PATIPARC,PATIPSEN,PADATARC;
          from (i_cTable) where;
              PACODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PATIPARC = NVL(cp_ToDate(_read_.PATIPARC),cp_NullValue(_read_.PATIPARC))
        this.w_PATIPSEN = NVL(cp_ToDate(_read_.PATIPSEN),cp_NullValue(_read_.PATIPSEN))
        this.w_PADATARC = NVL(cp_ToDate(_read_.PADATARC),cp_NullValue(_read_.PADATARC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.pOper = "IN"
        this.w_CODPRE = this.w_DPSERPRE
      endif
    endif
    this.w_COUNT = 1
    this.w_FLOFFICE = .F.
    if this.pOper = "GB"
      Dimension aFileList(1) 
 aFileList(1) = alltrim(this.w_FILELISTNAME)
      this.w_NUMFILE = 1
      this.w_SENDTO = .F.
      this.w_CDMODALL = "S"
    else
      this.w_FILELIST = FILETOSTR(this.w_FILELISTNAME)
      this.w_NUMFILE = ALINES(aFileList, this.w_FILELIST, 4)
      if this.w_NUMFILE>1 AND aFileList(1)=="***FROMOFFICE***"
        this.w_FLOFFICE = .T.
        this.w_COUNT = this.w_COUNT + 1
        this.w_CDMODALL = "S"
      else
        this.w_CDMODALL = Icase(!this.w_ISALT,this.w_PATIPARC,this.w_MODPERS = "N" or Empty(this.w_PERSONA),this.w_PATIPSEN,this.w_MODPERS)
      endif
      this.w_SENDTO = .T.
    endif
    this.w_DefaultPath = this.w_AEPATHFI
    do while this.w_COUNT <= this.w_NUMFILE
      this.w_AEOGGETT = this.w_DefaultOgge
      * --- Ripristiniamo il path, che potrebbe essere stato modificato inserendo tipo e classe in un documento precedente
      this.w_AEPATHFI = this.w_DefaultPath
      if this.w_SETALL<>"S"
        this.w_IDSERIAL = SPACE(20)
        cp_AskProg(This, i_nConn, cp_SetAzi("xxxPROMINDI"), "IDSERIAL")
        this.w_ORIGFILE = aFileList[this.w_COUNT]
        this.w_DATAARCH = i_DATSYS
        this.w_ORAARCH = ""
        * --- Se proveniamo da Outlook, nella stessa riga abbiamo il file separato tramite i caratteri '*!*' dalla data, ora di invio e oggetto della mail
        this.w_ChrSep = "*|*"
        if AT(this.w_ChrSep, this.w_ORIGFILE)>0
          if AT(this.w_ChrSep, this.w_ORIGFILE,2)>0
            * --- Ci sono le due occorrenze che ci aspettiamo per dividere i campi necessari
            this.w_StrDate = SUBSTR(this.w_ORIGFILE,AT(this.w_ChrSep, this.w_ORIGFILE,1)+LEN(this.w_ChrSep),AT(this.w_ChrSep, this.w_ORIGFILE,2)-AT(this.w_ChrSep, this.w_ORIGFILE,1)-LEN(this.w_ChrSep))
            if this.w_PADATARC="M"
              * --- Data/ora di invio della mail
              do case
                case AT("_", this.w_StrDate,1)>0
                  * --- Data in formato GGMMAAAA_HHMMSS
                  this.w_ORAARCH = SUBSTR(this.w_StrDate,AT("_", this.w_StrDate)+1)
                  this.w_ORAARCH = LEFT(this.w_ORAARCH,2)+":"+SUBSTR(this.w_ORAARCH,3,2)
                  this.w_StrDate = SUBSTR(this.w_StrDate,1, AT("_", this.w_StrDate)-1)
                  * --- Formato GGMMAA oppure GGMMAAAA
                  this.w_DATAARCH = cp_CharToDate(LEFT(this.w_StrDate,2)+"-"+SUBSTR(this.w_StrDate,3,2)+"-"+SUBSTR(this.w_StrDate,5))
                  * --- Analizziamo la data ottenuta. Se non � compresa tra i_IniDat e i_FinDat � segno che il formato data � differente, pertanto proviamo ad ottenere la data invertendo anno e giorno
                  if this.w_DATAARCH<i_IniDat OR this.w_DATAARCH>i_FinDat
                    * --- Proviamo con il formato data AAAAMMGG oppure AAMMGG
                    this.w_DATAARCH = cp_CharToDate(RIGHT(this.w_StrDate,2)+"-"+LEFT(RIGHT(this.w_StrDate,4),2)+"-"+LEFT(this.w_StrDate,IIF(LEN(this.w_StrDate)=6,2,4)))
                  endif
                case AT(" ", this.w_StrDate,1)>0
                  * --- Data in formato GG/MM/AAAA HH:MM:SS
                  this.w_ORAARCH = LEFT(SUBSTR(this.w_StrDate,AT(" ", this.w_StrDate)+1), 5)
                  this.w_StrDate = SUBSTR(this.w_StrDate,1, AT(" ", this.w_StrDate)-1)
                  * --- Formato GG/MM/AA oppure GG/MM/AAAA
                  this.w_DATAARCH = cp_CharToDate(LEFT(this.w_StrDate,2)+"-"+SUBSTR(this.w_StrDate,4,2)+"-"+SUBSTR(this.w_StrDate,7))
                  * --- Analizziamo la data ottenuta. Se non � compresa tra i_IniDat e i_FinDat � segno che il formato data � differente, pertanto proviamo ad ottenere la data invertendo anno e giorno
                  if this.w_DATAARCH<i_IniDat OR this.w_DATAARCH>i_FinDat
                    * --- Proviamo con il formato data AAAA/MM/GG oppure AA/MM/GG
                    this.w_DATAARCH = cp_CharToDate(RIGHT(this.w_StrDate,2)+"-"+LEFT(RIGHT(this.w_StrDate,5),2)+"-"+LEFT(this.w_StrDate,IIF(LEN(this.w_StrDate)=8,2,4)))
                  endif
              endcase
              if this.w_DATAARCH<i_IniDat OR this.w_DATAARCH>i_FinDat
                * --- La data analizzata non fa parte del range consentito - azzeriamo qualunque valore
                this.w_DATAARCH = i_DatSys
                this.w_ORAARCH = ""
              endif
            endif
            this.w_AEOGGETT = SUBSTR(this.w_ORIGFILE,AT(this.w_ChrSep, this.w_ORIGFILE,2)+LEN(this.w_ChrSep))
          endif
          this.w_ORIGFILE = SUBSTR(this.w_ORIGFILE,1,AT(this.w_ChrSep, this.w_ORIGFILE)-1)
        endif
        this.w_AEORIFIL = JUSTFNAME(this.w_ORIGFILE)
        if IsAlt() AND EMPTY(this.w_AEOGGETT) AND !EMPTY(this.pOper) AND this.pOper <> "GB"
          this.w_AEOGGETT = JUSTSTEM(this.w_ORIGFILE)
        endif
        this.w_DESART = IIF(!EMPTY(NVL(this.w_RAGPRE,"")), SPACE(40),IIF(this.w_COPIADESCR<>"S",this.w_DESCRI,LEFT(this.w_AEOGGETT,40)))
        this.w_estensione = JUSTEXT(this.w_AEORIFIL)
        this.w_TIPOALL = this.w_TIPOALLE
        this.w_CLASALL = this.w_CLAALLE
        if this.w_NOMSK
          this.w_TESTOK = .F.
          this.w_MSK = GSUT_KSO(This)
        else
          this.w_TESTOK = .t.
        endif
      endif
      if this.w_TESTOK
        if this.w_Crea_Ind_Mod = "I"
          * --- Crea indice
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_ISALT and g_AGEN="S" and (Not Empty(this.w_CODPRE) or Not Empty(this.w_RAGPRE))
            GSAG_BPI(this,this.w_AECODPRA,this.w_CODPRE,this.w_RAGPRE,this.w_DESART)
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        else
          * --- Crea modello
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      endif
      this.w_COUNT = this.w_COUNT + 1
    enddo
    this.w_MSK = .NULL.
    if this.pOper = "GB"
      * --- Non devo chiudere l'oggetto chiamante
      this.bUpdateParentObject = .F.
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Archivia
    private L_SerIndex
    L_SerIndex=""
    this.w_CNCODCAN = this.w_AECODPRA
    if this.w_PARENTCLASS="tgsfa_bev"
      this.w_AEPATHFI = ADDBS((ADDBS(this.w_AEPATHFI) + iif(! empty(this.w_AEPATHFI),alltrim(i_codazi),"")))
    endif
    if this.w_SETALL="S" And this.w_COUNT>1
      this.w_ORIGFILE = aFileList[this.w_COUNT]
      this.w_AEORIFIL = JUSTFNAME(this.w_ORIGFILE)
      this.w_NEWFILENAME = ADDBS(this.w_AEPATHFI)+JUSTFNAME(this.w_ORIGFILE)
    else
      * --- w_CNPATHFA utilizzato in GSUT_BBA
      * --- Read from CAN_TIER
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CNPATHFA"+;
          " from "+i_cTable+" CAN_TIER where ";
              +"CNCODCAN = "+cp_ToStrODBC(this.w_AECODPRA);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CNPATHFA;
          from (i_cTable) where;
              CNCODCAN = this.w_AECODPRA;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CNPATHFA = NVL(cp_ToDate(_read_.CNPATHFA),cp_NullValue(_read_.CNPATHFA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- w_CDMODALL determina il tipo di modalit� di archiviazione, condizione all'interno di GSUT_BBA
      this.w_INSPATIP = this.w_AETIALCP
      this.w_INSPACLA = this.w_AECLALCP
      this.w_NEWFILENAME = ADDBS(this.w_AEPATHFI)+ALLTRIM(this.w_AEORIFIL)
    endif
    this.w_OK = .t.
    if (this.w_PATIPARC = "L" AND this.w_CDMODALL <> "L" and this.w_ISALT) OR (this.w_PATIPARC = "F" AND this.w_CDMODALL <> "L" and ! this.w_ISALT)
      * --- Creo la copia del file
      this.w_estensione = JUSTEXT(this.w_AEORIFIL)
      this.w_TIPOPERAZ = "N"
      do while cp_FileExist(this.w_NEWFILENAME) And this.w_TIPOPERAZ="N"
        * --- Richiamo la maschera per modificare il nome o sovrascrittura
        this.w_PATH_KRF = ADDBS(this.w_AEPATHFI)
        do GSUT_KRF with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_TIPOPERAZ = "N"
          this.w_NEWFILENAME = ADDBS(this.w_AEPATHFI)+ALLTRIM(this.w_FILENAME)
        else
          this.w_FILENAME = JUSTFNAME(this.w_NEWFILENAME)
        endif
        this.w_OK = Not Empty(this.w_FILENAME)
      enddo
      if this.w_OK
        MAKEDIR(ADDBS(this.w_AEPATHFI))
        if Not Empty(this.w_NEWFILENAME) and this.w_ORIGFILE <> this.w_NEWFILENAME
          COPY FILE (this.w_ORIGFILE) TO (this.w_NEWFILENAME)
        endif
        if !this.w_ISALT
          this.w_AEORIFIL = JUSTFNAME(this.w_NEWFILENAME)
        endif
        this.w_OLDORIGFILE = this.w_ORIGFILE
        this.w_ORIGFILE = this.w_NEWFILENAME
        this.w_OLDCDMODALL = this.w_CDMODALL
        this.w_CDMODALL = "L"
      endif
    else
      if this.w_PARENTCLASS="tgsfa_bev"
        this.w_OLDORIGFILE = this.w_ORIGFILE
        ADDPROPERTY(this, "cCursor", this.oParentObject.pEventsCur)
      endif
    endif
    if this.w_OK
      this.w_INARCHIVIO = "S"
      this.w_PATHSTD = iif((this.w_ISALT and this.w_AR_TABLE="CAN_TIER") or !this.w_ISALT, " ", ADDBS(Fullpath(this.w_AEPATHFI))+ Alltrim(this.w_AEORIFIL))
      Private L_ArrParam
      dimension L_ArrParam(30)
      L_ArrParam(1)="AR"
      L_ArrParam(2)=this.w_ORIGFILE
      L_ArrParam(3)=this.w_LICLADOC
      L_ArrParam(4)=i_CODUTE
      L_ArrParam(5)=icase(this.w_PARENTCLASS="tgsfa_bev",this.oParentObject.pEventsCur,(this.w_ISALT and this.w_AR_TABLE="CAN_TIER") or !this.w_NOMSK ,this,this.w_MSK)
      L_ArrParam(6)=this.w_AR_TABLE
      L_ArrParam(7)=this.w_AECODPRA
      L_ArrParam(8)=ALLTRIM(this.w_AEOGGETT)
      L_ArrParam(9)=this.w_AE_TESTO
      L_ArrParam(10)=.T.
      L_ArrParam(11)=.T.
      L_ArrParam(12)=" "
      L_ArrParam(13)=" "
      L_ArrParam(14)=this
      L_ArrParam(15)=this.w_TIPOALL
      L_ArrParam(16)=this.w_CLASALL
      L_ArrParam(17)=" "
      L_ArrParam(18)=" "
      L_ArrParam(19)="C"
      L_ArrParam(21)=.F.
      L_ArrParam(22)=this.w_PATHSTD
      L_ArrParam(25)=this.w_IDTIPBST
      L_ArrParam(26)=this.w_DATAARCH
      L_ArrParam(27)=this.w_ORAARCH
      GSUT_BBA(this,@L_ArrParam, @L_SerIndex)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if !EMPTY(L_SerIndex) AND this.w_DE__TIPO="M" AND LOWER(JUSTFNAME(JUSTPATH(this.w_OLDORIGFILE)))==LOWER(JUSTSTEM(this.w_ORIGFILE)) AND LOWER(JUSTEXT(this.w_ORIGFILE))="html"
        this.w_CPROWORD = 10
        this.w_CPROWNUM = 1
        this.w_IDSERIAL = LEFT(ALLTRIM(L_SerIndex), AT("*",ALLTRIM(L_SerIndex))-1)
        * --- Select from PRODINDI
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2],.t.,this.PRODINDI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select IDSERIAL, MAX(CPROWNUM) As CPROWNUM, MAX(CPROWORD) As CPROWORD  from "+i_cTable+" PRODINDI ";
              +" where IDSERIAL = "+cp_ToStrODBC(this.w_IDSERIAL)+"";
              +" group by IDSERIAL";
               ,"_Curs_PRODINDI")
        else
          select IDSERIAL, MAX(CPROWNUM) As CPROWNUM, MAX(CPROWORD) As CPROWORD from (i_cTable);
           where IDSERIAL = this.w_IDSERIAL;
           group by IDSERIAL;
            into cursor _Curs_PRODINDI
        endif
        if used('_Curs_PRODINDI')
          select _Curs_PRODINDI
          locate for 1=1
          do while not(eof())
          this.w_CPROWNUM = NVL(_Curs_PRODINDI.CPROWNUM, 0) + 1
          this.w_CPROWORD = NVL(_Curs_PRODINDI.CPROWORD, 0) + 10
            select _Curs_PRODINDI
            continue
          enddo
          use
        endif
        * --- Insert into PRODINDI
        i_nConn=i_TableProp[this.PRODINDI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRODINDI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRODINDI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"IDSERIAL"+",CPROWNUM"+",CPROWORD"+",IDCODATT"+",IDDESATT"+",IDTIPATT"+",IDVALATT"+",IDCHKOBB"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_IDSERIAL),'PRODINDI','IDSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PRODINDI','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PRODINDI','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC("ZABODYMAIL"),'PRODINDI','IDCODATT');
          +","+cp_NullLink(cp_ToStrODBC("Identificativo corpo email"),'PRODINDI','IDDESATT');
          +","+cp_NullLink(cp_ToStrODBC("C"),'PRODINDI','IDTIPATT');
          +","+cp_NullLink(cp_ToStrODBC("S"),'PRODINDI','IDVALATT');
          +","+cp_NullLink(cp_ToStrODBC("N"),'PRODINDI','IDCHKOBB');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'IDSERIAL',this.w_IDSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'IDCODATT',"ZABODYMAIL",'IDDESATT',"Identificativo corpo email",'IDTIPATT',"C",'IDVALATT',"S",'IDCHKOBB',"N")
          insert into (i_cTable) (IDSERIAL,CPROWNUM,CPROWORD,IDCODATT,IDDESATT,IDTIPATT,IDVALATT,IDCHKOBB &i_ccchkf. );
             values (;
               this.w_IDSERIAL;
               ,this.w_CPROWNUM;
               ,this.w_CPROWORD;
               ,"ZABODYMAIL";
               ,"Identificativo corpo email";
               ,"C";
               ,"S";
               ,"N";
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      if !Empty(this.w_OLDCDMODALL)
        * --- Ripristino vecchio valore
        this.w_CDMODALL = this.w_OLDCDMODALL
        this.w_ORIGFILE = this.w_OLDORIGFILE
        this.w_OLDCDMODALL = ""
        this.w_OLDORIGFILE = ""
      endif
      * --- Dopo la copia cancello il file
      L_SerIndex=LEFT(L_SerIndex, AT("*", L_SerIndex)-1)
      if Not empty(L_SerIndex)
        if this.w_PATIPARC = "F" or this.w_ISALT
          * --- Read from PROMINDI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PROMINDI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2],.t.,this.PROMINDI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IDPATALL,IDNOMFIL,IDORIGIN,IDORIFIL"+;
              " from "+i_cTable+" PROMINDI where ";
                  +"IDSERIAL = "+cp_ToStrODBC(L_SerIndex);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IDPATALL,IDNOMFIL,IDORIGIN,IDORIFIL;
              from (i_cTable) where;
                  IDSERIAL = L_SerIndex;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PATALL = NVL(cp_ToDate(_read_.IDPATALL),cp_NullValue(_read_.IDPATALL))
            this.w_NOMFIL = NVL(cp_ToDate(_read_.IDNOMFIL),cp_NullValue(_read_.IDNOMFIL))
            this.w_AGG_ORI = NVL(cp_ToDate(_read_.IDORIGIN),cp_NullValue(_read_.IDORIGIN))
            this.w_LORIFIL = NVL(cp_ToDate(_read_.IDORIFIL),cp_NullValue(_read_.IDORIFIL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if Empty(this.w_LORIFIL)
            * --- A seconda che l'indice appena fatto si copia o collegamento il file generato � memorizzato in campi diversi
            this.w_LORIFIL = Alltrim(this.w_PATALL)+Alltrim(this.w_NOMFIL)
          endif
          if (this.w_CDMODALL="S" and cp_FILEEXIST(Alltrim(this.w_LORIFIL)))
            * --- Completo l'operazione di sposta eliminando file dalla cartella di origine
            DELETE FILE (this.w_ORIGFILE)
          endif
          if this.w_PATIPARC = "F" AND this.w_CDMODALL<>"L"
            * --- Elimino file creato attraverso l'indice perch� ho gi� creato il file corretto
            * --- or (w_ISALT and  w_AR_TABLE<>'CAN_TIER')
            if !this.w_ISALT 
              DELETE FILE (Alltrim(this.w_PATALL)+Alltrim(this.w_NOMFIL))
            endif
            this.w_AGG_ORI = iif(this.w_ISALT,this.w_AEORIFIL,this.w_AGG_ORI)
            this.w_AGG_FIL = iif(!this.w_ISALT,this.w_AEORIFIL,this.w_NOMFIL)
            * --- Aggiorno nome file originario
            * --- Write into PROMINDI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PROMINDI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PROMINDI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PROMINDI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"IDORIGIN ="+cp_NullLink(cp_ToStrODBC(this.w_AGG_ORI),'PROMINDI','IDORIGIN');
              +",IDNOMFIL ="+cp_NullLink(cp_ToStrODBC(this.w_AGG_FIL),'PROMINDI','IDNOMFIL');
                  +i_ccchkf ;
              +" where ";
                  +"IDSERIAL = "+cp_ToStrODBC(L_SerIndex);
                     )
            else
              update (i_cTable) set;
                  IDORIGIN = this.w_AGG_ORI;
                  ,IDNOMFIL = this.w_AGG_FIL;
                  &i_ccchkf. ;
               where;
                  IDSERIAL = L_SerIndex;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      endif
    else
      Ah_ErrorMsg("Operazione annullata",48,"")
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione modello
    this.w_MOCODICE = space(10)
    this.w_SPAZI = space(10)
    this.w_OK = .T.
    * --- Try
    local bErr_04EAC8C0
    bErr_04EAC8C0=bTrsErr
    this.Try_04EAC8C0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_OK = .F.
      Ah_ErrorMsg("Creazione modello non eseguita")
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_04EAC8C0
    * --- End
    * --- Se modalit� di archiviazione non � Collegamento
    if this.w_OK AND this.w_CDMODALL<>"L"
      * --- La var. w_AEPATHFI contiene il path del documento di destinazione
      if MAKEDIR(FULLPATH(ADDBS(JUSTPATH(this.w_AEPATHFI))))
        * --- Se esiste gi� il documento di destinazione
        if cp_FILEEXIST(this.w_AEPATHFI)
          this.w_OK = AH_YESNO("File %1 gi� esistente.%0 Si vuole sovrascriverlo?", "" , alltrim(this.w_AEPATHFI))
        endif
        if this.w_OK
          * --- Esegue copia del file
          COPY FILE (this.w_ORIGFILE) TO (this.w_AEPATHFI)
        endif
        * --- Se spostamento
        if this.w_CDMODALL = "S" AND cp_FILEEXIST(this.w_ORIGFILE) AND cp_FILEEXIST(this.w_AEPATHFI)
          * --- Cancella documento iniziale
          DELETE FILE (this.w_ORIGFILE)
        endif
      else
        Ah_ErrorMsg("Impossibile creare la cartella di archiviazione %1", 64, "" , alltrim(FULLPATH(ADDBS(JUSTPATH(this.w_AEPATHFI)))) )
      endif
    endif
  endproc
  proc Try_04EAC8C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.PROMODEL_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEMDO", "i_codazi,w_MOCODICE")
    * --- Insert into PROMODEL
    i_nConn=i_TableProp[this.PROMODEL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PROMODEL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PROMODEL_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MOCODICE"+",MODESCRI"+",MOCLASSE"+",MOPATMOD"+",MOQUERY"+",MO__NOTE"+",MOPREFER"+",MOCODPRE"+",MORAGPRE"+",MOTIPPRA"+",MOMATPRA"+",MO__ENTE"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MOCODICE),'PROMODEL','MOCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AEOGGETT),'PROMODEL','MODESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LICLADOC),'PROMODEL','MOCLASSE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AEPATHFI),'PROMODEL','MOPATMOD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_QUERYMOD),'PROMODEL','MOQUERY');
      +","+cp_NullLink(cp_ToStrODBC(this.w_AE_TESTO),'PROMODEL','MO__NOTE');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PROMODEL','MOPREFER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODPRE),'PROMODEL','MOCODPRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RAGPRE),'PROMODEL','MORAGPRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SPAZI),'PROMODEL','MOTIPPRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SPAZI),'PROMODEL','MOMATPRA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SPAZI),'PROMODEL','MO__ENTE');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MOCODICE',this.w_MOCODICE,'MODESCRI',this.w_AEOGGETT,'MOCLASSE',this.w_LICLADOC,'MOPATMOD',this.w_AEPATHFI,'MOQUERY',this.w_QUERYMOD,'MO__NOTE',this.w_AE_TESTO,'MOPREFER'," ",'MOCODPRE',this.w_CODPRE,'MORAGPRE',this.w_RAGPRE,'MOTIPPRA',this.w_SPAZI,'MOMATPRA',this.w_SPAZI,'MO__ENTE',this.w_SPAZI)
      insert into (i_cTable) (MOCODICE,MODESCRI,MOCLASSE,MOPATMOD,MOQUERY,MO__NOTE,MOPREFER,MOCODPRE,MORAGPRE,MOTIPPRA,MOMATPRA,MO__ENTE &i_ccchkf. );
         values (;
           this.w_MOCODICE;
           ,this.w_AEOGGETT;
           ,this.w_LICLADOC;
           ,this.w_AEPATHFI;
           ,this.w_QUERYMOD;
           ,this.w_AE_TESTO;
           ," ";
           ,this.w_CODPRE;
           ,this.w_RAGPRE;
           ,this.w_SPAZI;
           ,this.w_SPAZI;
           ,this.w_SPAZI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOPER,w_FILELISTNAME)
    this.pOPER=pOPER
    this.w_FILELISTNAME=w_FILELISTNAME
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='PROMCLAS'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='PROMINDI'
    this.cWorkTables[4]='PAR_ALTE'
    this.cWorkTables[5]='DIPENDEN'
    this.cWorkTables[6]='TIP_ALLE'
    this.cWorkTables[7]='CLA_ALLE'
    this.cWorkTables[8]='PRODINDI'
    this.cWorkTables[9]='PROMODEL'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_PRODINDI')
      use in _Curs_PRODINDI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER,w_FILELISTNAME"
endproc
