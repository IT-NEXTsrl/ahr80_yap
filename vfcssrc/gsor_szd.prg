* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_szd                                                        *
*              Disponibilit� nel tempo                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [30][VRS_136]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-24                                                      *
* Last revis.: 2015-06-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsor_szd",oParentObject))

* --- Class definition
define class tgsor_szd as StdForm
  Top    = 79
  Left   = 13

  * --- Standard Properties
  Width  = 750
  Height = 440
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-06-18"
  HelpContextID=241789289
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  MAGAZZIN_IDX = 0
  GRUMMAG_IDX = 0
  cPrg = "gsor_szd"
  cComment = "Disponibilit� nel tempo"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPOELA = space(10)
  w_CODART = space(20)
  w_CODMAG = space(5)
  w_GRUMAG = space(5)
  w_SELMOV = space(1)
  w_SELPER = 0
  w_MDDAT = ctod('  /  /  ')
  w_MADAT = ctod('  /  /  ')
  w_DISPONIB = 0
  w_DATFIN = ctod('  /  /  ')
  o_DATFIN = ctod('  /  /  ')
  w_DATAULTINV = ctod('  /  /  ')
  w_SLQTAPER = 0
  w_SERIALE = space(10)
  w_NUMRIF = 0
  w_DATOBSO = ctod('  /  /  ')
  w_TIPO = space(2)
  w_OBTEST = ctod('  /  /  ')
  w_SLQTRPER = 0
  w_RISERV = 0
  w_SERIAL1 = space(15)
  w_DESART = space(40)
  w_ZoomDis1 = .NULL.
  w_ZoomDis = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsor_szdPag1","gsor_szd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODART_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomDis1 = this.oPgFrm.Pages(1).oPag.ZoomDis1
    this.w_ZoomDis = this.oPgFrm.Pages(1).oPag.ZoomDis
    DoDefault()
    proc Destroy()
      this.w_ZoomDis1 = .NULL.
      this.w_ZoomDis = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='GRUMMAG'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOELA=space(10)
      .w_CODART=space(20)
      .w_CODMAG=space(5)
      .w_GRUMAG=space(5)
      .w_SELMOV=space(1)
      .w_SELPER=0
      .w_MDDAT=ctod("  /  /  ")
      .w_MADAT=ctod("  /  /  ")
      .w_DISPONIB=0
      .w_DATFIN=ctod("  /  /  ")
      .w_DATAULTINV=ctod("  /  /  ")
      .w_SLQTAPER=0
      .w_SERIALE=space(10)
      .w_NUMRIF=0
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPO=space(2)
      .w_OBTEST=ctod("  /  /  ")
      .w_SLQTRPER=0
      .w_RISERV=0
      .w_SERIAL1=space(15)
      .w_DESART=space(40)
      .w_CODART=oParentObject.w_CODART
      .w_CODMAG=oParentObject.w_CODMAG
      .w_SLQTAPER=oParentObject.w_SLQTAPER
      .w_SLQTRPER=oParentObject.w_SLQTRPER
        .w_TIPOELA = iif( type('this.oParentObject .w_TIPO')='C', this.oParentObject .w_TIPO, 'M')
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODART))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODMAG))
          .link_1_3('Full')
        endif
        .w_GRUMAG = oparentobject .w_CODMAG
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_GRUMAG))
          .link_1_4('Full')
        endif
        .w_SELMOV = 'T'
        .w_SELPER = 99
        .w_MDDAT = g_iniese
        .w_MADAT = g_finese
          .DoRTCalc(9,9,.f.)
        .w_DATFIN = i_datsys
      .oPgFrm.Page1.oPag.ZoomDis1.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
      .oPgFrm.Page1.oPag.ZoomDis.Calculate()
          .DoRTCalc(11,12,.f.)
        .w_SERIALE = IIF(.w_SELPER=99,.w_ZoomDis1.getVar('SERIAL'),.w_ZoomDis.getVar('SERIAL'))
        .w_NUMRIF = iif(.w_SELPER=99,.w_ZoomDis1.getVar('NUMRIF'),.w_ZoomDis.getVar('NUMRIF'))
      .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
          .DoRTCalc(15,16,.f.)
        .w_OBTEST = i_DATSYS
      .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
          .DoRTCalc(18,19,.f.)
        .w_SERIAL1 = IIF(.w_SELPER=99,.w_ZoomDis1.getVar('SERIAL1'),.w_ZoomDis.getVar('SERIAL1'))
    endwith
    this.DoRTCalc(21,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CODART=.w_CODART
      .oParentObject.w_CODMAG=.w_CODMAG
      .oParentObject.w_SLQTAPER=.w_SLQTAPER
      .oParentObject.w_SLQTRPER=.w_SLQTRPER
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomDis1.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.ZoomDis.Calculate()
        .DoRTCalc(1,12,.t.)
            .w_SERIALE = IIF(.w_SELPER=99,.w_ZoomDis1.getVar('SERIAL'),.w_ZoomDis.getVar('SERIAL'))
            .w_NUMRIF = iif(.w_SELPER=99,.w_ZoomDis1.getVar('NUMRIF'),.w_ZoomDis.getVar('NUMRIF'))
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .DoRTCalc(15,16,.t.)
        if .o_DATFIN<>.w_DATFIN
            .w_OBTEST = i_DATSYS
        endif
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .DoRTCalc(18,19,.t.)
            .w_SERIAL1 = IIF(.w_SELPER=99,.w_ZoomDis1.getVar('SERIAL1'),.w_ZoomDis.getVar('SERIAL1'))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomDis1.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_14.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.ZoomDis.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCODMAG_1_3.visible=!this.oPgFrm.Page1.oPag.oCODMAG_1_3.mHide()
    this.oPgFrm.Page1.oPag.oGRUMAG_1_4.visible=!this.oPgFrm.Page1.oPag.oGRUMAG_1_4.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_21.visible=!this.oPgFrm.Page1.oPag.oBtn_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_42.visible=!this.oPgFrm.Page1.oPag.oStr_1_42.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomDis1.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_14.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomDis.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODART
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART)+"%");

            select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_2'),i_cWhere,'GSMA_BZA',"Elenco articoli",'GSOR0ADI.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_TIPO = NVL(_Link_.ARTIPART,space(2))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_TIPO = space(2)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPO $ 'PF-SE-MP-PH-MC-MA-IM-FS')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice non di tipo articolo oppure obsoleto o inesistente")
        endif
        this.w_CODART = space(20)
        this.w_DESART = space(40)
        this.w_TIPO = space(2)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_3'),i_cWhere,'GSAR_AMA',"Elenco magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente oppure obsoleto")
        endif
        this.w_CODMAG = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUMAG
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMMAG_IDX,3]
    i_lTable = "GRUMMAG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMMAG_IDX,2], .t., this.GRUMMAG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMMAG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gsar_Mgz',True,'GRUMMAG')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GRCODICE like "+cp_ToStrODBC(trim(this.w_GRUMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select GRCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GRCODICE',trim(this.w_GRUMAG))
          select GRCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUMAG)==trim(_Link_.GRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUMAG) and !this.bDontReportError
            deferred_cp_zoom('GRUMMAG','*','GRCODICE',cp_AbsName(oSource.parent,'oGRUMAG_1_4'),i_cWhere,'Gsar_Mgz',"Gruppi di magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where GRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',oSource.xKey(1))
            select GRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where GRCODICE="+cp_ToStrODBC(this.w_GRUMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',this.w_GRUMAG)
            select GRCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUMAG = NVL(_Link_.GRCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_GRUMAG = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Gruppo magazzini inesistente oppure obsoleto")
        endif
        this.w_GRUMAG = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMMAG_IDX,2])+'\'+cp_ToStr(_Link_.GRCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMMAG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODART_1_2.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_2.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_3.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_3.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oGRUMAG_1_4.value==this.w_GRUMAG)
      this.oPgFrm.Page1.oPag.oGRUMAG_1_4.value=this.w_GRUMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oSELMOV_1_5.RadioValue()==this.w_SELMOV)
      this.oPgFrm.Page1.oPag.oSELMOV_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELPER_1_6.RadioValue()==this.w_SELPER)
      this.oPgFrm.Page1.oPag.oSELPER_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMDDAT_1_7.value==this.w_MDDAT)
      this.oPgFrm.Page1.oPag.oMDDAT_1_7.value=this.w_MDDAT
    endif
    if not(this.oPgFrm.Page1.oPag.oMADAT_1_8.value==this.w_MADAT)
      this.oPgFrm.Page1.oPag.oMADAT_1_8.value=this.w_MADAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDISPONIB_1_9.value==this.w_DISPONIB)
      this.oPgFrm.Page1.oPag.oDISPONIB_1_9.value=this.w_DISPONIB
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_10.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_10.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oRISERV_1_38.value==this.w_RISERV)
      this.oPgFrm.Page1.oPag.oRISERV_1_38.value=this.w_RISERV
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_41.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_41.value=this.w_DESART
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPO $ 'PF-SE-MP-PH-MC-MA-IM-FS'))  and not(empty(.w_CODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice non di tipo articolo oppure obsoleto o inesistente")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_TIPOELA='G')  and not(empty(.w_CODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(.w_TIPOELA<>'G')  and not(empty(.w_GRUMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGRUMAG_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Gruppo magazzini inesistente oppure obsoleto")
          case   not(.w_MDDAT<=.w_MADAT)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMDDAT_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
          case   not(.w_MDDAT<=.w_MADAT)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMADAT_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La prima data � maggiore della seconda")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATFIN = this.w_DATFIN
    return

enddefine

* --- Define pages as container
define class tgsor_szdPag1 as StdContainer
  Width  = 746
  height = 440
  stdWidth  = 746
  stdheight = 440
  resizeXpos=280
  resizeYpos=336
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODART_1_2 as StdField with uid="ULKJAIVOUB",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice non di tipo articolo oppure obsoleto o inesistente",;
    ToolTipText = "Codice dell'articolo da considerare",;
    HelpContextID = 184298022,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=117, Top=9, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco articoli",'GSOR0ADI.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODART_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODART
     i_obj.ecpSave()
  endproc

  add object oCODMAG_1_3 as StdField with uid="JHAHLNWBOU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente oppure obsoleto",;
    ToolTipText = "Codice del magazzino da considerare",;
    HelpContextID = 217590310,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=602, Top=9, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_3.mHide()
    with this.Parent.oContained
      return (.w_TIPOELA='G')
    endwith
  endfunc

  func oCODMAG_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Elenco magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oGRUMAG_1_4 as StdField with uid="RKLIVQPTMW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_GRUMAG", cQueryName = "GRUMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Gruppo magazzini inesistente oppure obsoleto",;
    ToolTipText = "Codice del gruppo di magazzini da considerare",;
    HelpContextID = 217660774,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=602, Top=9, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMMAG", cZoomOnZoom="Gsar_Mgz", oKey_1_1="GRCODICE", oKey_1_2="this.w_GRUMAG"

  func oGRUMAG_1_4.mHide()
    with this.Parent.oContained
      return (.w_TIPOELA<>'G')
    endwith
  endfunc

  func oGRUMAG_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUMAG_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUMAG_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMMAG','*','GRCODICE',cp_AbsName(this.parent,'oGRUMAG_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'Gsar_Mgz',"Gruppi di magazzino",'',this.parent.oContained
  endproc
  proc oGRUMAG_1_4.mZoomOnZoom
    local i_obj
    i_obj=Gsar_Mgz()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GRCODICE=this.parent.oContained.w_GRUMAG
     i_obj.ecpSave()
  endproc


  add object oSELMOV_1_5 as StdCombo with uid="ZGGWDZIRGJ",rtseq=5,rtrep=.f.,left=117,top=34,width=86,height=21;
    , ToolTipText = "Movimenti da visualizzare";
    , HelpContextID = 215523622;
    , cFormVar="w_SELMOV",RowSource=""+"Solo futuri,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELMOV_1_5.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oSELMOV_1_5.GetRadio()
    this.Parent.oContained.w_SELMOV = this.RadioValue()
    return .t.
  endfunc

  func oSELMOV_1_5.SetRadio()
    this.Parent.oContained.w_SELMOV=trim(this.Parent.oContained.w_SELMOV)
    this.value = ;
      iif(this.Parent.oContained.w_SELMOV=='F',1,;
      iif(this.Parent.oContained.w_SELMOV=='T',2,;
      0))
  endfunc


  add object oSELPER_1_6 as StdCombo with uid="UDOGIJLNVT",rtseq=6,rtrep=.f.,left=318,top=34,width=128,height=21;
    , ToolTipText = "Periodo di raggruppamento";
    , HelpContextID = 138125606;
    , cFormVar="w_SELPER",RowSource=""+"Dettaglio movimenti,"+"Giornaliero,"+"Settimanale,"+"Mensile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELPER_1_6.RadioValue()
    return(iif(this.value =1,99,;
    iif(this.value =2,1,;
    iif(this.value =3,7,;
    iif(this.value =4,30,;
    0)))))
  endfunc
  func oSELPER_1_6.GetRadio()
    this.Parent.oContained.w_SELPER = this.RadioValue()
    return .t.
  endfunc

  func oSELPER_1_6.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_SELPER==99,1,;
      iif(this.Parent.oContained.w_SELPER==1,2,;
      iif(this.Parent.oContained.w_SELPER==7,3,;
      iif(this.Parent.oContained.w_SELPER==30,4,;
      0))))
  endfunc

  add object oMDDAT_1_7 as StdField with uid="NKWMZQSIJM",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MDDAT", cQueryName = "MDDAT",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data dalla quale devono essere considerati i movimenti",;
    HelpContextID = 149151802,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=117, Top=62

  func oMDDAT_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MDDAT<=.w_MADAT)
    endwith
    return bRes
  endfunc

  add object oMADAT_1_8 as StdField with uid="PQWIOMBYEW",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MADAT", cQueryName = "MADAT",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La prima data � maggiore della seconda",;
    ToolTipText = "Data fino alla quale devono essere considerati i movimenti",;
    HelpContextID = 149152570,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=318, Top=62

  func oMADAT_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MDDAT<=.w_MADAT)
    endwith
    return bRes
  endfunc

  add object oDISPONIB_1_9 as StdField with uid="RGKLLTRMST",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DISPONIB", cQueryName = "DISPONIB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Esistenza + tot. ordinato - tot. impegnato e riservato scaduti",;
    HelpContextID = 186903432,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=117, Top=87, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oDATFIN_1_10 as StdField with uid="DSZWTPMYOP",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data precedente al primo periodo non scaduto",;
    HelpContextID = 74587190,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=454, Top=87


  add object oBtn_1_11 as StdButton with uid="PSCSMLTUFF",left=692, top=9, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 203568406;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        do gsor_bzd with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_CODART) AND (NOT EMPTY(.w_CODMAG) or NOT EMPTY(.w_GRUMAG)))
      endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="UITHPJUCGP",left=692, top=387, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 234471866;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomDis1 as cp_zoombox with uid="EIZVMBNKEC",left=-2, top=116, width=744,height=266,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="SALDIART",cZoomFile="GSMA1SZD",bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bQueryOnDblClick=.f.,cMenuFile="",bRetriveAllRows=.f.,bAdvOptions=.t.,cZoomOnZoom="GSZM_BCC",bNoZoomGridShape=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 204643814


  add object oObj_1_14 as cp_runprogram with uid="GJDWVUNJBM",left=132, top=527, width=128,height=28,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='gsor_bzd',;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 204643814


  add object oObj_1_17 as cp_runprogram with uid="DUYGLOFWKY",left=132, top=558, width=112,height=30,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='GSMA_BUD',;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 204643814


  add object oBtn_1_21 as StdButton with uid="JCDJQLRSVE",left=7, top=387, width=48,height=45,;
    CpPicture="BMP\Origine.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento/movimento di magazzino associato";
    , HelpContextID = 158032410;
    , caption='Or\<igine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        gsar_bzm(this.Parent.oContained,.w_SERIALE,.w_NUMRIF,.w_SERIAL1)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_SELPER<>99)
     endwith
    endif
  endfunc


  add object ZoomDis as cp_zoombox with uid="GOWBNNDKLW",left=-2, top=116, width=744,height=266,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="SALDIART",cZoomFile="GSMA_SZD",bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bQueryOnDblClick=.f.,cMenuFile="",bRetriveAllRows=.f.,bAdvOptions=.t.,cZoomOnZoom="GSMA_ASA",bNoZoomGridShape=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 204643814


  add object oObj_1_30 as cp_runprogram with uid="IKFWOGVCGM",left=1, top=527, width=128,height=28,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='gsor_bes',;
    cEvent = "w_CODART Changed",;
    nPag=1;
    , HelpContextID = 204643814


  add object oObj_1_31 as cp_runprogram with uid="SIBRBXFBTB",left=1, top=558, width=128,height=28,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg='gsor_bes',;
    cEvent = "w_CODMAG Changed",;
    nPag=1;
    , HelpContextID = 204643814


  add object oObj_1_37 as cp_runprogram with uid="EUXBICYPOP",left=280, top=536, width=128,height=28,;
    caption='GSAR_BZM',;
   bGlobalFont=.t.,;
    prg="gsar_bzm(w_SERIALE,w_NUMRIF,w_SERIAL1)",;
    cEvent = "w_zoomdis1 selected",;
    nPag=1;
    , HelpContextID = 102957389

  add object oRISERV_1_38 as StdField with uid="WAFRIUWXMX",rtseq=19,rtrep=.f.,;
    cFormVar = "w_RISERV", cQueryName = "RISERV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Riservato",;
    HelpContextID = 218174742,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=318, Top=87, cSayPict="v_PQ(14)", cGetPict="v_GQ(14)"

  add object oDESART_1_41 as StdField with uid="OVNGHYNQRO",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 184356918,;
   bGlobalFont=.t.,;
    Height=21, Width=234, Left=260, Top=9, InputMask=replicate('X',40)

  add object oStr_1_15 as StdString with uid="XSDXQCQQBQ",Visible=.t., Left=428, Top=87,;
    Alignment=1, Width=25, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="CLLQYMXIVP",Visible=.t., Left=257, Top=34,;
    Alignment=1, Width=56, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="FLMYECYEKF",Visible=.t., Left=0, Top=87,;
    Alignment=1, Width=116, Height=15,;
    Caption="Disponibilit� teorica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="OARSLTAYUC",Visible=.t., Left=61, Top=9,;
    Alignment=1, Width=55, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="LKKUYIIXNC",Visible=.t., Left=49, Top=34,;
    Alignment=1, Width=67, Height=15,;
    Caption="Movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="ETIAMZMEFJ",Visible=.t., Left=59, Top=62,;
    Alignment=1, Width=57, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="KIQXRXBLEU",Visible=.t., Left=259, Top=62,;
    Alignment=1, Width=48, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="BJDNUHUUIU",Visible=.t., Left=533, Top=9,;
    Alignment=1, Width=67, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (.w_TIPOELA='G')
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="UVTEWGDNBH",Visible=.t., Left=12, Top=-52,;
    Alignment=0, Width=305, Height=18,;
    Caption="Esistono due oggetti zoom sovrapposti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_39 as StdString with uid="MKMFEGMHNQ",Visible=.t., Left=237, Top=87,;
    Alignment=1, Width=76, Height=15,;
    Caption="di cui Ris.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="YZJTNEGHZF",Visible=.t., Left=492, Top=9,;
    Alignment=1, Width=108, Height=15,;
    Caption="Gr. di magazzino:"  ;
  , bGlobalFont=.t.

  func oStr_1_42.mHide()
    with this.Parent.oContained
      return (.w_TIPOELA<>'G')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsor_szd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
