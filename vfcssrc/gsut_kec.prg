* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kec                                                        *
*              Cruscotto elaborazioni KPI                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-07-09                                                      *
* Last revis.: 2015-11-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kec",oParentObject))

* --- Class definition
define class tgsut_kec as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 711
  Height = 399+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-26"
  HelpContextID=65618793
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=21

  * --- Constant Properties
  _IDX = 0
  ELABKPIM_IDX = 0
  cpusers_IDX = 0
  cpgroups_IDX = 0
  cPrg = "gsut_kec"
  cComment = "Cruscotto elaborazioni KPI"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODINI = space(10)
  w_CODFIN = space(10)
  w_DESINI = space(100)
  w_DESFIN = space(100)
  w_QUERY = space(250)
  w_TIPRES = space(1)
  w_TIPSIN = space(3)
  w_BENCHM = space(3)
  w_CODICE = space(10)
  w_CHKATT = space(1)
  w_RESCUR = space(10)
  w_RCODKPI = space(10)
  w_RDATELA = ctot('')
  w_RLOGMSG = space(0)
  w_RRESSIN = 0
  w_ATTIVAZ = space(1)
  w_RNOROWS = space(0)
  w_TIPOUG = space(1)
  o_TIPOUG = space(1)
  w_CODUTE = 0
  w_CODGRP = 0
  w_DESCRI = space(20)
  w_ZoomKPI = .NULL.
  w_ZoomRes = .NULL.
  w_LblAtt = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kecPag1","gsut_kec",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Cruscotto elaborazioni")
      .Pages(2).addobject("oPag","tgsut_kecPag2","gsut_kec",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Risultati")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODINI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomKPI = this.oPgFrm.Pages(1).oPag.ZoomKPI
    this.w_ZoomRes = this.oPgFrm.Pages(2).oPag.ZoomRes
    this.w_LblAtt = this.oPgFrm.Pages(1).oPag.LblAtt
    DoDefault()
    proc Destroy()
      this.w_ZoomKPI = .NULL.
      this.w_ZoomRes = .NULL.
      this.w_LblAtt = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='ELABKPIM'
    this.cWorkTables[2]='cpusers'
    this.cWorkTables[3]='cpgroups'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODINI=space(10)
      .w_CODFIN=space(10)
      .w_DESINI=space(100)
      .w_DESFIN=space(100)
      .w_QUERY=space(250)
      .w_TIPRES=space(1)
      .w_TIPSIN=space(3)
      .w_BENCHM=space(3)
      .w_CODICE=space(10)
      .w_CHKATT=space(1)
      .w_RESCUR=space(10)
      .w_RCODKPI=space(10)
      .w_RDATELA=ctot("")
      .w_RLOGMSG=space(0)
      .w_RRESSIN=0
      .w_ATTIVAZ=space(1)
      .w_RNOROWS=space(0)
      .w_TIPOUG=space(1)
      .w_CODUTE=0
      .w_CODGRP=0
      .w_DESCRI=space(20)
      .oPgFrm.Page1.oPag.ZoomKPI.Calculate()
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODINI))
          .link_1_2('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_CODFIN))
          .link_1_3('Full')
        endif
          .DoRTCalc(3,4,.f.)
        .w_QUERY = IIF(Empty(.w_QUERY),'',Sys(2014,.w_QUERY))
      .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .w_TIPRES = ' '
        .w_TIPSIN = Space(3)
        .w_BENCHM = Space(3)
        .w_CODICE = .w_ZoomKPI.GetVar('EKCODICE')
        .w_CHKATT = ' '
        .w_RESCUR = Sys(2015)
          .DoRTCalc(12,15,.f.)
        .w_ATTIVAZ = .w_ZoomKPI.GetVar('EKCHKATT')
      .oPgFrm.Page2.oPag.ZoomRes.Calculate(.w_RESCUR)
        .w_RNOROWS = 'Nessun record estratto'
      .oPgFrm.Page1.oPag.LblAtt.Calculate('Disabilitata',0,Rgb(255,255,0))
        .w_TIPOUG = 'U'
        .w_CODUTE = IIF(.w_TIPOUG='U',.w_CODUTE, 0)
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_CODUTE))
          .link_1_38('Full')
        endif
        .w_CODGRP = IIF(.w_TIPOUG='G',.w_CODGRP, 0)
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_CODGRP))
          .link_1_39('Full')
        endif
    endwith
    this.DoRTCalc(21,21,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomKPI.Calculate()
        .DoRTCalc(1,4,.t.)
            .w_QUERY = IIF(Empty(.w_QUERY),'',Sys(2014,.w_QUERY))
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .DoRTCalc(6,8,.t.)
            .w_CODICE = .w_ZoomKPI.GetVar('EKCODICE')
        .DoRTCalc(10,15,.t.)
            .w_ATTIVAZ = .w_ZoomKPI.GetVar('EKCHKATT')
        .oPgFrm.Page2.oPag.ZoomRes.Calculate(.w_RESCUR)
        .oPgFrm.Page1.oPag.LblAtt.Calculate('Disabilitata',0,Rgb(255,255,0))
        .DoRTCalc(17,18,.t.)
        if .o_TIPOUG<>.w_TIPOUG
            .w_CODUTE = IIF(.w_TIPOUG='U',.w_CODUTE, 0)
          .link_1_38('Full')
        endif
        if .o_TIPOUG<>.w_TIPOUG
            .w_CODGRP = IIF(.w_TIPOUG='G',.w_CODGRP, 0)
          .link_1_39('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(21,21,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomKPI.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_10.Calculate()
        .oPgFrm.Page2.oPag.ZoomRes.Calculate(.w_RESCUR)
        .oPgFrm.Page1.oPag.LblAtt.Calculate('Disabilitata',0,Rgb(255,255,0))
    endwith
  return

  proc Calculate_MMKNYWGLEJ()
    with this
          * --- Carica risultati all'attivazione della pagina
          gsut_bec(this;
              ,'RESU';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page2.oPag.oRNOROWS_2_11.visible=!this.oPgFrm.Page2.oPag.oRNOROWS_2_11.mHide()
    this.oPgFrm.Page1.oPag.oCODUTE_1_38.visible=!this.oPgFrm.Page1.oPag.oCODUTE_1_38.mHide()
    this.oPgFrm.Page1.oPag.oCODGRP_1_39.visible=!this.oPgFrm.Page1.oPag.oCODGRP_1_39.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomKPI.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_10.Event(cEvent)
      .oPgFrm.Page2.oPag.ZoomRes.Event(cEvent)
        if lower(cEvent)==lower("ActivatePage 2")
          .Calculate_MMKNYWGLEJ()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.LblAtt.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODINI
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ELABKPIM_IDX,3]
    i_lTable = "ELABKPIM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2], .t., this.ELABKPIM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ELABKPIM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EKCODICE like "+cp_ToStrODBC(trim(this.w_CODINI)+"%");

          i_ret=cp_SQL(i_nConn,"select EKCODICE,EKDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by EKCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'EKCODICE',trim(this.w_CODINI))
          select EKCODICE,EKDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by EKCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODINI)==trim(_Link_.EKCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODINI) and !this.bDontReportError
            deferred_cp_zoom('ELABKPIM','*','EKCODICE',cp_AbsName(oSource.parent,'oCODINI_1_2'),i_cWhere,'',"Elaborazioni KPI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EKCODICE,EKDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where EKCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EKCODICE',oSource.xKey(1))
            select EKCODICE,EKDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EKCODICE,EKDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where EKCODICE="+cp_ToStrODBC(this.w_CODINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EKCODICE',this.w_CODINI)
            select EKCODICE,EKDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODINI = NVL(_Link_.EKCODICE,space(10))
      this.w_DESINI = NVL(_Link_.EKDESCRI,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_CODINI = space(10)
      endif
      this.w_DESINI = space(100)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Empty(.w_CODFIN) OR (Upper(.w_CODINI)<=Upper(.w_CODFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice elaborazione iniziale successivo a quello finale o inesistente")
        endif
        this.w_CODINI = space(10)
        this.w_DESINI = space(100)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2])+'\'+cp_ToStr(_Link_.EKCODICE,1)
      cp_ShowWarn(i_cKey,this.ELABKPIM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFIN
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ELABKPIM_IDX,3]
    i_lTable = "ELABKPIM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2], .t., this.ELABKPIM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ELABKPIM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" EKCODICE like "+cp_ToStrODBC(trim(this.w_CODFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select EKCODICE,EKDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by EKCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'EKCODICE',trim(this.w_CODFIN))
          select EKCODICE,EKDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by EKCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFIN)==trim(_Link_.EKCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFIN) and !this.bDontReportError
            deferred_cp_zoom('ELABKPIM','*','EKCODICE',cp_AbsName(oSource.parent,'oCODFIN_1_3'),i_cWhere,'',"Elaborazioni KPI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EKCODICE,EKDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where EKCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EKCODICE',oSource.xKey(1))
            select EKCODICE,EKDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select EKCODICE,EKDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where EKCODICE="+cp_ToStrODBC(this.w_CODFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'EKCODICE',this.w_CODFIN)
            select EKCODICE,EKDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFIN = NVL(_Link_.EKCODICE,space(10))
      this.w_DESFIN = NVL(_Link_.EKDESCRI,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_CODFIN = space(10)
      endif
      this.w_DESFIN = space(100)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=!Empty(.w_CODINI) AND (Upper(.w_CODINI)<=Upper(.w_CODFIN))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o codice elaborazione iniziale vuoto o successivo a quello finale")
        endif
        this.w_CODFIN = space(10)
        this.w_DESFIN = space(100)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ELABKPIM_IDX,2])+'\'+cp_ToStr(_Link_.EKCODICE,1)
      cp_ShowWarn(i_cKey,this.ELABKPIM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODUTE
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpusers_IDX,3]
    i_lTable = "cpusers"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2], .t., this.cpusers_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODUTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpusers')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODUTE);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODUTE)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODUTE) and !this.bDontReportError
            deferred_cp_zoom('cpusers','*','code',cp_AbsName(oSource.parent,'oCODUTE_1_38'),i_cWhere,'',"Elenco utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODUTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODUTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODUTE)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODUTE = NVL(_Link_.code,0)
      this.w_DESCRI = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODUTE = 0
      endif
      this.w_DESCRI = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpusers_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpusers_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODUTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRP
  func Link_1_39(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.cpgroups_IDX,3]
    i_lTable = "cpgroups"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2], .t., this.cpgroups_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'cpgroups')
        if i_nConn<>0
          i_cWhere = " code="+cp_ToStrODBC(this.w_CODGRP);

          i_ret=cp_SQL(i_nConn,"select code,name";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'code',this.w_CODGRP)
          select code,name;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_CODGRP) and !this.bDontReportError
            deferred_cp_zoom('cpgroups','*','code',cp_AbsName(oSource.parent,'oCODGRP_1_39'),i_cWhere,'',"Elenco gruppi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                     +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',oSource.xKey(1))
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select code,name";
                   +" from "+i_cTable+" "+i_lTable+" where code="+cp_ToStrODBC(this.w_CODGRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'code',this.w_CODGRP)
            select code,name;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRP = NVL(_Link_.code,0)
      this.w_DESCRI = NVL(_Link_.name,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRP = 0
      endif
      this.w_DESCRI = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.cpgroups_IDX,2])+'\'+cp_ToStr(_Link_.code,1)
      cp_ShowWarn(i_cKey,this.cpgroups_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODINI_1_2.value==this.w_CODINI)
      this.oPgFrm.Page1.oPag.oCODINI_1_2.value=this.w_CODINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFIN_1_3.value==this.w_CODFIN)
      this.oPgFrm.Page1.oPag.oCODFIN_1_3.value=this.w_CODFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_6.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_6.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_7.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_7.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oQUERY_1_8.value==this.w_QUERY)
      this.oPgFrm.Page1.oPag.oQUERY_1_8.value=this.w_QUERY
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPRES_1_11.RadioValue()==this.w_TIPRES)
      this.oPgFrm.Page1.oPag.oTIPRES_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPSIN_1_13.RadioValue()==this.w_TIPSIN)
      this.oPgFrm.Page1.oPag.oTIPSIN_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBENCHM_1_15.RadioValue()==this.w_BENCHM)
      this.oPgFrm.Page1.oPag.oBENCHM_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCHKATT_1_19.RadioValue()==this.w_CHKATT)
      this.oPgFrm.Page1.oPag.oCHKATT_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oRCODKPI_2_4.value==this.w_RCODKPI)
      this.oPgFrm.Page2.oPag.oRCODKPI_2_4.value=this.w_RCODKPI
    endif
    if not(this.oPgFrm.Page2.oPag.oRDATELA_2_5.value==this.w_RDATELA)
      this.oPgFrm.Page2.oPag.oRDATELA_2_5.value=this.w_RDATELA
    endif
    if not(this.oPgFrm.Page2.oPag.oRLOGMSG_2_6.value==this.w_RLOGMSG)
      this.oPgFrm.Page2.oPag.oRLOGMSG_2_6.value=this.w_RLOGMSG
    endif
    if not(this.oPgFrm.Page2.oPag.oRRESSIN_2_8.value==this.w_RRESSIN)
      this.oPgFrm.Page2.oPag.oRRESSIN_2_8.value=this.w_RRESSIN
    endif
    if not(this.oPgFrm.Page2.oPag.oRNOROWS_2_11.value==this.w_RNOROWS)
      this.oPgFrm.Page2.oPag.oRNOROWS_2_11.value=this.w_RNOROWS
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOUG_1_37.RadioValue()==this.w_TIPOUG)
      this.oPgFrm.Page1.oPag.oTIPOUG_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODUTE_1_38.value==this.w_CODUTE)
      this.oPgFrm.Page1.oPag.oCODUTE_1_38.value=this.w_CODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRP_1_39.value==this.w_CODGRP)
      this.oPgFrm.Page1.oPag.oCODGRP_1_39.value=this.w_CODGRP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_40.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_40.value=this.w_DESCRI
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(Empty(.w_CODFIN) OR (Upper(.w_CODINI)<=Upper(.w_CODFIN)))  and not(empty(.w_CODINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODINI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice elaborazione iniziale successivo a quello finale o inesistente")
          case   not(!Empty(.w_CODINI) AND (Upper(.w_CODINI)<=Upper(.w_CODFIN)))  and not(empty(.w_CODFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o codice elaborazione iniziale vuoto o successivo a quello finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOUG = this.w_TIPOUG
    return

enddefine

* --- Define pages as container
define class tgsut_kecPag1 as StdContainer
  Width  = 707
  height = 401
  stdWidth  = 707
  stdheight = 401
  resizeXpos=652
  resizeYpos=224
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object ZoomKPI as cp_szoombox with uid="DXMSEDJBAP",left=-4, top=154, width=714,height=193,;
    caption='Zoom KPI',;
   bGlobalFont=.t.,;
    bReadOnly=.t.,cMenuFile="",cZoomFile="GSUT_KEC",bOptions=.f.,bAdvOptions=.f.,bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,cZoomOnZoom="",cTable="ELABKPIM",bQueryOnLoad=.t.,;
    cEvent = "Init,AggiornaCruscotto",;
    nPag=1;
    , HelpContextID = 108322337

  add object oCODINI_1_2 as StdField with uid="TVWCQGVOVC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODINI", cQueryName = "CODINI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Codice elaborazione iniziale successivo a quello finale o inesistente",;
    ToolTipText = "Codice elaborazione",;
    HelpContextID = 96186330,;
   bGlobalFont=.t.,;
    Height=21, Width=98, Left=91, Top=11, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="ELABKPIM", oKey_1_1="EKCODICE", oKey_1_2="this.w_CODINI"

  func oCODINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODINI_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODINI_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ELABKPIM','*','EKCODICE',cp_AbsName(this.parent,'oCODINI_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elaborazioni KPI",'',this.parent.oContained
  endproc

  add object oCODFIN_1_3 as StdField with uid="LEUKIMHSEQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_CODFIN", cQueryName = "CODFIN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o codice elaborazione iniziale vuoto o successivo a quello finale",;
    ToolTipText = "Codice elaborazione",;
    HelpContextID = 17739738,;
   bGlobalFont=.t.,;
    Height=21, Width=98, Left=91, Top=33, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="ELABKPIM", oKey_1_1="EKCODICE", oKey_1_2="this.w_CODFIN"

  func oCODFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFIN_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFIN_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ELABKPIM','*','EKCODICE',cp_AbsName(this.parent,'oCODFIN_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elaborazioni KPI",'',this.parent.oContained
  endproc

  add object oDESINI_1_6 as StdField with uid="BRPXHMNRTY",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione elaborazione",;
    HelpContextID = 96127434,;
   bGlobalFont=.t.,;
    Height=21, Width=511, Left=192, Top=11, InputMask=replicate('X',100)

  add object oDESFIN_1_7 as StdField with uid="TMJMOKKGJY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione elaborazione",;
    HelpContextID = 17680842,;
   bGlobalFont=.t.,;
    Height=21, Width=511, Left=192, Top=33, InputMask=replicate('X',100)

  add object oQUERY_1_8 as StdField with uid="OFXJARVWIN",rtseq=5,rtrep=.f.,;
    cFormVar = "w_QUERY", cQueryName = "QUERY",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    ToolTipText = "Query di estrazione dati",;
    HelpContextID = 235051258,;
   bGlobalFont=.t.,;
    Height=21, Width=587, Left=91, Top=56, InputMask=replicate('X',250)


  add object oObj_1_10 as cp_askfile with uid="CSSUALBELH",left=681, top=55, width=22,height=23,;
    caption='...',;
   bGlobalFont=.t.,;
    var="w_QUERY",;
    nPag=1;
    , HelpContextID = 65417770


  add object oTIPRES_1_11 as StdCombo with uid="HJTACMGCIR",value=1,rtseq=6,rtrep=.f.,left=91,top=103,width=140,height=21;
    , ToolTipText = "Tipologia del risultato dell'elaborazione";
    , HelpContextID = 205649098;
    , cFormVar="w_TIPRES",RowSource=""+"Tutti,"+"Conteggio,"+"Monetario,"+"Percentuale,"+"Testo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPRES_1_11.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'C',;
    iif(this.value =3,'M',;
    iif(this.value =4,'P',;
    iif(this.value =5,'T',;
    space(1)))))))
  endfunc
  func oTIPRES_1_11.GetRadio()
    this.Parent.oContained.w_TIPRES = this.RadioValue()
    return .t.
  endfunc

  func oTIPRES_1_11.SetRadio()
    this.Parent.oContained.w_TIPRES=trim(this.Parent.oContained.w_TIPRES)
    this.value = ;
      iif(this.Parent.oContained.w_TIPRES=='',1,;
      iif(this.Parent.oContained.w_TIPRES=='C',2,;
      iif(this.Parent.oContained.w_TIPRES=='M',3,;
      iif(this.Parent.oContained.w_TIPRES=='P',4,;
      iif(this.Parent.oContained.w_TIPRES=='T',5,;
      0)))))
  endfunc


  add object oTIPSIN_1_13 as StdCombo with uid="JMVAYIZVHK",value=1,rtseq=7,rtrep=.f.,left=340,top=103,width=140,height=21;
    , ToolTipText = "Tipologia del risultato sintetico";
    , HelpContextID = 16839882;
    , cFormVar="w_TIPSIN",RowSource=""+"Tutti,"+"Nessun risultato,"+"Primo,"+"Massimo,"+"Minimo,"+"Media,"+"Mediana,"+"Conteggio,"+"Somma", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPSIN_1_13.RadioValue()
    return(iif(this.value =1,'   ',;
    iif(this.value =2,'NUL',;
    iif(this.value =3,'FST',;
    iif(this.value =4,'MAX',;
    iif(this.value =5,'MIN',;
    iif(this.value =6,'AVG',;
    iif(this.value =7,'MED',;
    iif(this.value =8,'CNT',;
    iif(this.value =9,'SUM',;
    space(3)))))))))))
  endfunc
  func oTIPSIN_1_13.GetRadio()
    this.Parent.oContained.w_TIPSIN = this.RadioValue()
    return .t.
  endfunc

  func oTIPSIN_1_13.SetRadio()
    this.Parent.oContained.w_TIPSIN=trim(this.Parent.oContained.w_TIPSIN)
    this.value = ;
      iif(this.Parent.oContained.w_TIPSIN=='',1,;
      iif(this.Parent.oContained.w_TIPSIN=='NUL',2,;
      iif(this.Parent.oContained.w_TIPSIN=='FST',3,;
      iif(this.Parent.oContained.w_TIPSIN=='MAX',4,;
      iif(this.Parent.oContained.w_TIPSIN=='MIN',5,;
      iif(this.Parent.oContained.w_TIPSIN=='AVG',6,;
      iif(this.Parent.oContained.w_TIPSIN=='MED',7,;
      iif(this.Parent.oContained.w_TIPSIN=='CNT',8,;
      iif(this.Parent.oContained.w_TIPSIN=='SUM',9,;
      0)))))))))
  endfunc


  add object oBENCHM_1_15 as StdCombo with uid="DGRBKAYLJD",value=1,rtseq=8,rtrep=.f.,left=340,top=127,width=140,height=21;
    , ToolTipText = "Tipologia del benchmark calcolato";
    , HelpContextID = 35723754;
    , cFormVar="w_BENCHM",RowSource=""+"Tutti,"+"Nessun benchmark,"+"Massimo,"+"Minimo,"+"Media,"+"Mediana,"+"Conteggio,"+"Somma,"+"Valore fisso,"+"Link al risultato sintetico,"+"Link al set di risultati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oBENCHM_1_15.RadioValue()
    return(iif(this.value =1,'   ',;
    iif(this.value =2,'NUL',;
    iif(this.value =3,'MAX',;
    iif(this.value =4,'MIN',;
    iif(this.value =5,'AVG',;
    iif(this.value =6,'MED',;
    iif(this.value =7,'CNT',;
    iif(this.value =8,'SUM',;
    iif(this.value =9,'FIX',;
    iif(this.value =10,'LSI',;
    iif(this.value =11,'LRE',;
    space(3)))))))))))))
  endfunc
  func oBENCHM_1_15.GetRadio()
    this.Parent.oContained.w_BENCHM = this.RadioValue()
    return .t.
  endfunc

  func oBENCHM_1_15.SetRadio()
    this.Parent.oContained.w_BENCHM=trim(this.Parent.oContained.w_BENCHM)
    this.value = ;
      iif(this.Parent.oContained.w_BENCHM=='',1,;
      iif(this.Parent.oContained.w_BENCHM=='NUL',2,;
      iif(this.Parent.oContained.w_BENCHM=='MAX',3,;
      iif(this.Parent.oContained.w_BENCHM=='MIN',4,;
      iif(this.Parent.oContained.w_BENCHM=='AVG',5,;
      iif(this.Parent.oContained.w_BENCHM=='MED',6,;
      iif(this.Parent.oContained.w_BENCHM=='CNT',7,;
      iif(this.Parent.oContained.w_BENCHM=='SUM',8,;
      iif(this.Parent.oContained.w_BENCHM=='FIX',9,;
      iif(this.Parent.oContained.w_BENCHM=='LSI',10,;
      iif(this.Parent.oContained.w_BENCHM=='LRE',11,;
      0)))))))))))
  endfunc


  add object oBtn_1_17 as StdButton with uid="AKWJQTHXDN",left=655, top=103, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , HelpContextID = 111303446;
    , Caption='Ricerca';
  , bGlobalFont=.t.

    proc oBtn_1_17.Click()
      this.parent.oContained.NotifyEvent("AggiornaCruscotto")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oCHKATT_1_19 as StdCombo with uid="CMMZMEUDAR",value=1,rtseq=10,rtrep=.f.,left=91,top=127,width=140,height=21;
    , ToolTipText = "Filtro sul check di abilitazione";
    , HelpContextID = 174278362;
    , cFormVar="w_CHKATT",RowSource=""+"Tutte,"+"Solo abilitate,"+"Solo disabilitate", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCHKATT_1_19.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oCHKATT_1_19.GetRadio()
    this.Parent.oContained.w_CHKATT = this.RadioValue()
    return .t.
  endfunc

  func oCHKATT_1_19.SetRadio()
    this.Parent.oContained.w_CHKATT=trim(this.Parent.oContained.w_CHKATT)
    this.value = ;
      iif(this.Parent.oContained.w_CHKATT=='',1,;
      iif(this.Parent.oContained.w_CHKATT=='S',2,;
      iif(this.Parent.oContained.w_CHKATT=='N',3,;
      0)))
  endfunc


  add object oBtn_1_21 as StdButton with uid="TXIYPCXWQI",left=143, top=353, width=48,height=45,;
    CpPicture="exe\bmp\apertura.ico", caption="", nPag=1;
    , ToolTipText = "Apre l'elaborazione corrente";
    , HelpContextID = 58240762;
    , Caption='Apri';
  , bGlobalFont=.t.

    proc oBtn_1_21.Click()
      with this.Parent.oContained
        opengest("A","GSUT_MEK","EKCODICE",.w_CODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!Empty(Nvl(.w_CODICE,'')))
      endwith
    endif
  endfunc


  add object oBtn_1_22 as StdButton with uid="KSJAHKXAEV",left=93, top=353, width=48,height=45,;
    CpPicture="addtabl.bmp", caption="", nPag=1;
    , ToolTipText = "Consente l'inserimento di una nuova elaborazione";
    , HelpContextID = 209443114;
    , Caption='Nuovo';
  , bGlobalFont=.t.

    proc oBtn_1_22.Click()
      with this.Parent.oContained
        opengest("L","GSUT_MEK")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_23 as StdButton with uid="FNFRBQUSQG",left=193, top=353, width=48,height=45,;
    CpPicture="BMP\Modifica.BMP", caption="", nPag=1;
    , ToolTipText = "Apre la gestione elaborazioni e consente la modifica di quella corrente";
    , HelpContextID = 199651623;
    , Caption='Modifica';
  , bGlobalFont=.t.

    proc oBtn_1_23.Click()
      with this.Parent.oContained
        opengest("M","GSUT_MEK","EKCODICE",.w_CODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!Empty(Nvl(.w_CODICE,'')))
      endwith
    endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="RHQQFEAHMR",left=350, top=353, width=48,height=45,;
    CpPicture="trfstop.bmp", caption="", nPag=1;
    , ToolTipText = "Disabilita le elaborazioni selezionate";
    , HelpContextID = 138452804;
    , Caption='Disattiva';
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      with this.Parent.oContained
        GSUT_BEC(this.Parent.oContained,"DEAC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_25 as StdButton with uid="OBBAYCKNBP",left=400, top=353, width=48,height=45,;
    CpPicture="BMP/play.bmp", caption="", nPag=1;
    , ToolTipText = "Esegue le elaborazioni selezionate";
    , HelpContextID = 215254086;
    , Caption='Esegui';
  , bGlobalFont=.t.

    proc oBtn_1_25.Click()
      with this.Parent.oContained
        GSUT_BEC(this.Parent.oContained,"EXEC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_26 as StdButton with uid="FSGTYHUHLC",left=500, top=353, width=48,height=45,;
    CpPicture="BMP/Book.ico", caption="", nPag=1;
    , ToolTipText = "Visualizza i risultati dell'elaborazione corrente";
    , HelpContextID = 122905114;
    , Caption='Risultati';
  , bGlobalFont=.t.

    proc oBtn_1_26.Click()
      with this.Parent.oContained
        GSUT_BEC(this.Parent.oContained,"RESU")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!Empty(Nvl(.w_CODICE,'')))
      endwith
    endif
  endfunc


  add object oBtn_1_27 as StdButton with uid="TFLYPBYDPY",left=655, top=353, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 58301370;
    , Caption='Esci';
  , bGlobalFont=.t.

    proc oBtn_1_27.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_29 as StdButton with uid="LQBZFJRBGS",left=300, top=353, width=48,height=45,;
    CpPicture="trfgo.bmp", caption="", nPag=1;
    , ToolTipText = "Abilita le elaborazioni selezionate";
    , HelpContextID = 82277638;
    , Caption='Attiva';
  , bGlobalFont=.t.

    proc oBtn_1_29.Click()
      with this.Parent.oContained
        GSUT_BEC(this.Parent.oContained,"ACTV")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_31 as StdButton with uid="AZVFOCNGGA",left=243, top=353, width=48,height=45,;
    CpPicture="canc.bmp", caption="", nPag=1;
    , ToolTipText = "Cancella le elaborazioni selezionate";
    , HelpContextID = 19856761;
    , Caption='Cancella';
  , bGlobalFont=.t.

    proc oBtn_1_31.Click()
      with this.Parent.oContained
        GSUT_BEC(this.Parent.oContained,"DEL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_32 as StdButton with uid="TBFDLZEKKP",left=550, top=353, width=48,height=45,;
    CpPicture="canc.bmp", caption="", nPag=1;
    , ToolTipText = "Esegue la pulizia delle tabelle dei risultati e dei log";
    , HelpContextID = 220657398;
    , Caption='Pulizia';
  , bGlobalFont=.t.

    proc oBtn_1_32.Click()
      with this.Parent.oContained
        GSUT_BEC(this.Parent.oContained,"CLRL")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_33 as StdButton with uid="UPXAPJHUZX",left=450, top=353, width=48,height=45,;
    CpPicture="BMP/play.bmp", caption="", nPag=1;
    , ToolTipText = "Esegue le elaborazioni selezionate in background";
    , HelpContextID = 83410283;
    , Caption='Background';
  , bGlobalFont=.t.

    proc oBtn_1_33.Click()
      with this.Parent.oContained
        GSUT_BEC(this.Parent.oContained,"BGEX")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Vartype(i_DDEchannel)='N' And i_DDEchannel<>-1)
      endwith
    endif
  endfunc


  add object oBtn_1_35 as StdButton with uid="SZLHIQZGVN",left=600, top=353, width=48,height=45,;
    CpPicture="bmp\timer.bmp", caption="", nPag=1;
    , ToolTipText = "Apre le impostazioni del timer di esecuzione delle KPI";
    , HelpContextID = 207422666;
    , Caption='Timer';
  , bGlobalFont=.t.

    proc oBtn_1_35.Click()
      do GSUT_KTK with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object LblAtt as cp_calclbl with uid="QROJMHXGBK",left=8, top=353, width=62,height=17,;
    caption='LblAtt',;
   bGlobalFont=.t.,;
    fontUnderline=.f.,bGlobalFont=.f.,caption="Disabilitata",alignment=0,fontname="Arial",fontsize=8,fontBold=.f.,fontItalic=.t.,;
    nPag=1;
    , HelpContextID = 127853494


  add object oTIPOUG_1_37 as StdCombo with uid="CSNCYEFXDU",rtseq=18,rtrep=.f.,left=91,top=80,width=98,height=21;
    , ToolTipText = "La configurazione pu� essere valida per un utente, un gruppo o per l'installazione";
    , HelpContextID = 121959626;
    , cFormVar="w_TIPOUG",RowSource=""+"Utente,"+"Gruppo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOUG_1_37.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'G',;
    space(1))))
  endfunc
  func oTIPOUG_1_37.GetRadio()
    this.Parent.oContained.w_TIPOUG = this.RadioValue()
    return .t.
  endfunc

  func oTIPOUG_1_37.SetRadio()
    this.Parent.oContained.w_TIPOUG=trim(this.Parent.oContained.w_TIPOUG)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOUG=='U',1,;
      iif(this.Parent.oContained.w_TIPOUG=='G',2,;
      0))
  endfunc

  add object oCODUTE_1_38 as StdField with uid="XLWHUGPTFM",rtseq=19,rtrep=.f.,;
    cFormVar = "w_CODUTE", cQueryName = "CODUTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Filtra i gadget personalizzati per questo utente",;
    HelpContextID = 156217306,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=191, Top=80, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="cpusers", oKey_1_1="code", oKey_1_2="this.w_CODUTE"

  func oCODUTE_1_38.mHide()
    with this.Parent.oContained
      return (.w_TIPOUG<>'U')
    endwith
  endfunc

  func oCODUTE_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODUTE_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODUTE_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpusers','*','code',cp_AbsName(this.parent,'oCODUTE_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco utenti",'',this.parent.oContained
  endproc

  add object oCODGRP_1_39 as StdField with uid="MJFVYHVXYO",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CODGRP", cQueryName = "CODGRP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Filtra i gadget personalizzati per gli utenti appartenenti a questo gruppo",;
    HelpContextID = 243118042,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=191, Top=80, cSayPict='"9999"', cGetPict='"9999"', bHasZoom = .t. , cLinkFile="cpgroups", oKey_1_1="code", oKey_1_2="this.w_CODGRP"

  func oCODGRP_1_39.mHide()
    with this.Parent.oContained
      return (.w_TIPOUG<>'G')
    endwith
  endfunc

  func oCODGRP_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_39('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRP_1_39.ecpDrop(oSource)
    this.Parent.oContained.link_1_39('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRP_1_39.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'cpgroups','*','code',cp_AbsName(this.parent,'oCODGRP_1_39'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco gruppi",'',this.parent.oContained
  endproc

  add object oDESCRI_1_40 as StdField with uid="QVFKKLYMAG",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente o gruppo",;
    HelpContextID = 92326346,;
   bGlobalFont=.t.,;
    Height=21, Width=435, Left=268, Top=80, InputMask=replicate('X',20)

  add object oStr_1_4 as StdString with uid="GQOTJAGUHO",Visible=.t., Left=26, Top=15,;
    Alignment=1, Width=63, Height=18,;
    Caption="Da codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="YKHVGNJLIN",Visible=.t., Left=36, Top=37,;
    Alignment=1, Width=53, Height=18,;
    Caption="A codice:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="ECRPPUIHYJ",Visible=.t., Left=32, Top=60,;
    Alignment=1, Width=57, Height=18,;
    Caption="Query:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="VQOXCKJULX",Visible=.t., Left=8, Top=107,;
    Alignment=1, Width=81, Height=18,;
    Caption="Tipo risultato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="VFWETUXOLU",Visible=.t., Left=235, Top=107,;
    Alignment=1, Width=103, Height=18,;
    Caption="Risultato sintetico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="QAMZJSZAGT",Visible=.t., Left=237, Top=130,;
    Alignment=1, Width=101, Height=18,;
    Caption="Tipo benchmark:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="FBJHWJIWWL",Visible=.t., Left=21, Top=130,;
    Alignment=1, Width=68, Height=18,;
    Caption="Abilitazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="XMACPDKSNN",Visible=.t., Left=5, Top=83,;
    Alignment=1, Width=86, Height=18,;
    Caption="Filtra per:"  ;
  , bGlobalFont=.t.

  add object oBox_1_28 as StdBox with uid="KDIDXRMAUI",left=90, top=350, width=205,height=51

  add object oBox_1_30 as StdBox with uid="GLFFRWBTFU",left=296, top=350, width=356,height=51
enddefine
define class tgsut_kecPag2 as StdContainer
  Width  = 707
  height = 401
  stdWidth  = 707
  stdheight = 401
  resizeXpos=587
  resizeYpos=103
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRCODKPI_2_4 as StdField with uid="DLBTDTYHPR",rtseq=12,rtrep=.f.,;
    cFormVar = "w_RCODKPI", cQueryName = "RCODKPI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 250612458,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=53, Top=7, InputMask=replicate('X',10)

  add object oRDATELA_2_5 as StdField with uid="GIFBYHKDUU",rtseq=13,rtrep=.f.,;
    cFormVar = "w_RDATELA", cQueryName = "RDATELA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=ctot(""), bMultilanguage =  .f.,;
    HelpContextID = 54585834,;
   bGlobalFont=.t.,;
    Height=21, Width=130, Left=164, Top=7

  add object oRLOGMSG_2_6 as StdMemo with uid="VQXOBTNXCD",rtseq=14,rtrep=.f.,;
    cFormVar = "w_RLOGMSG", cQueryName = "RLOGMSG",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 70450710,;
   bGlobalFont=.t.,;
    Height=137, Width=619, Left=7, Top=244, ReadOnly=.t.

  add object oRRESSIN_2_8 as StdField with uid="ISAKIDBTJY",rtseq=15,rtrep=.f.,;
    cFormVar = "w_RRESSIN", cQueryName = "RRESSIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 90282986,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=398, Top=7, cSayPict="v_PU(38+VVU)", cGetPict="v_GU(38+VVU)"


  add object ZoomRes as cp_CBrowse with uid="GZAXSSKPYM",left=7, top=36, width=619,height=204,;
    caption='ZoomRes',;
   bGlobalFont=.t.,;
    cEvent = "MostraRisultati",;
    nPag=2;
    , HelpContextID = 111878550

  add object oRNOROWS_2_11 as StdMemo with uid="JJHQFXCEPL",rtseq=17,rtrep=.f.,;
    cFormVar = "w_RNOROWS", cQueryName = "RNOROWS",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 140378134,;
   bGlobalFont=.t.,;
    Height=203, Width=619, Left=7, Top=36, ReadOnly=.t.

  func oRNOROWS_2_11.mHide()
    with this.Parent.oContained
      return (Empty(.w_RCODKPI) Or (!Empty(.w_RESCUR) And Used(.w_RESCUR) And Reccount(.w_RESCUR)>0))
    endwith
  endfunc

  add object oStr_2_2 as StdString with uid="UOZDVTGGXL",Visible=.t., Left=28, Top=11,;
    Alignment=1, Width=24, Height=18,;
    Caption="KPI:"  ;
  , bGlobalFont=.t.

  add object oStr_2_3 as StdString with uid="XAQXCQRITN",Visible=.t., Left=139, Top=11,;
    Alignment=1, Width=24, Height=18,;
    Caption="del:"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="BPWWQBBGCB",Visible=.t., Left=293, Top=11,;
    Alignment=1, Width=104, Height=18,;
    Caption="Risultato sintetico:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kec','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
