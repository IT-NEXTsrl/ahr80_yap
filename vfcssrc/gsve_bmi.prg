* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bmi                                                        *
*              Ricalcolo incassi                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_9]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-02                                                      *
* Last revis.: 2001-11-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bmi",oParentObject)
return(i_retval)

define class tgsve_bmi as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricalcolo incasso corrispettivi
    this.oParentObject.w_TOTIMP = this.oParentObject.w_TOTIMP - this.oParentObject.w_DARIMP
    this.oParentObject.w_TOTSAL = this.oParentObject.w_TOTSAL - this.oParentObject.w_DARSAL
    this.oParentObject.w_DARIMP = IIF(this.oParentObject.w_INCODVAL=this.oParentObject.w_INVALNAZ, this.oParentObject.w_INTOTIMP, cp_ROUND(VAL2MON(this.oParentObject.w_INTOTIMP, this.oParentObject.w_INCAOVAL, g_CAOVAL, this.oParentObject.w_INDATAPE, this.oParentObject.w_INVALNAZ), g_PERPVL))
    this.oParentObject.w_DARSAL = IIF(this.oParentObject.w_INCODVAL=this.oParentObject.w_INVALNAZ, this.oParentObject.w_INIMPSAL, cp_ROUND(VAL2MON(this.oParentObject.w_INIMPSAL, this.oParentObject.w_INCAOVAL, g_CAOVAL, this.oParentObject.w_INDATAPE, this.oParentObject.w_INVALNAZ), g_PERPVL))
    this.oParentObject.w_TOTIMP = this.oParentObject.w_TOTIMP + this.oParentObject.w_DARIMP
    this.oParentObject.w_TOTSAL = this.oParentObject.w_TOTSAL + this.oParentObject.w_DARSAL
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
