* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bp4                                                        *
*              Carica automatismi IVA 2                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_14]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-10                                                      *
* Last revis.: 2013-03-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bp4",oParentObject)
return(i_retval)

define class tgscg_bp4 as StdBatch
  * --- Local variables
  w_TIPREG = space(1)
  w_NUMREG = 0
  w_PADRE = .NULL.
  * --- WorkFile variables
  VOCIIVA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica temporaneo Registrazioni IVA Primanota per Eventuli Differenze di Conversione (da GSCG_MIV)
    this.w_PADRE = this.oParentObject
    this.w_TIPREG = this.oParentObject.oParentObject.w_PNTIPREG
    this.w_NUMREG = this.oParentObject.oParentObject.w_PNNUMREG
    this.w_PADRE.MarkPos()     
    SELECT (this.oParentObject.cTrsName)
    GO BOTTOM
    if NOT EMPTY(NVL(t_IVCODIVA," ")) AND (t_IVIMPONI<>0 OR t_IVIMPIVA<>0)
      * --- Se l'ultima riga e' congruente aggiunge una nuova
      this.oParentObject.InitRow()
    endif
    this.oParentObject.w_IVCODIVA = g_COIDIF
    this.oParentObject.w_CODIVA = g_COIDIF
    this.oParentObject.w_IVTIPCON = "G"
    this.oParentObject.w_IVCODCON = SPACE(15)
    this.oParentObject.w_IVCODCOI = SPACE(15)
    this.oParentObject.w_IVIMPONI = - this.oParentObject.w_DIFDOC
    this.oParentObject.w_IMPONI = this.oParentObject.w_IVIMPONI
    this.oParentObject.w_TOTIMP = this.oParentObject.w_TOTIMP + this.oParentObject.w_IVIMPONI
    this.oParentObject.w_IVIMPIVA = 0
    this.oParentObject.w_IVTIPREG = this.w_TIPREG
    this.oParentObject.w_CTIPREG = this.w_TIPREG
    this.oParentObject.w_IVNUMREG = this.w_NUMREG
    this.oParentObject.w_IVFLOMAG = "X"
    this.oParentObject.w_IVCFLOMA = "X"
    * --- Read from VOCIIVA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VOCIIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO"+;
        " from "+i_cTable+" VOCIIVA where ";
            +"IVCODIVA = "+cp_ToStrODBC(this.oParentObject.w_IVCODIVA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IVDESIVA,IVPERIVA,IVPERIND,IVDTOBSO;
        from (i_cTable) where;
            IVCODIVA = this.oParentObject.w_IVCODIVA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_DESIVA = NVL(cp_ToDate(_read_.IVDESIVA),cp_NullValue(_read_.IVDESIVA))
      this.oParentObject.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
      this.oParentObject.w_IVPERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
      this.oParentObject.w_IVOBSO = NVL(cp_ToDate(_read_.IVDTOBSO),cp_NullValue(_read_.IVDTOBSO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Carica il Temporaneo dei Dati e skippa al record successivo
    this.oParentObject.TrsFromWork()
    this.w_PADRE.RePos(.T.)     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VOCIIVA'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
