* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gssr_bad                                                        *
*              Analisi documenti pre storicizzazione                           *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][146]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-02-04                                                      *
* Last revis.: 2007-12-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgssr_bad",oParentObject,m.pParam)
return(i_retval)

define class tgssr_bad as StdBatch
  * --- Local variables
  pParam = space(10)
  w_OGG = .NULL.
  w_FLINTE = space(1)
  w_TIPDOC = space(5)
  w_LSSERIAL = space(10)
  w_SERRIF = space(10)
  w_SERPAD = space(10)
  w_ROWNUM = 0
  w_NUMRIF = 0
  w_ANSERIAL = space(10)
  w_FIRSTLAP = .f.
  w_LOOP = .f.
  w_RECPOS = 0
  w_Mess = space(10)
  w_TmpC = space(10)
  w_POSIZIONE = 0
  w_RecElab = 0
  w_Messaggio = space(10)
  w_CONTA = 0
  w_PARMSG1 = space(1)
  w_MSGold = space(0)
  * --- WorkFile variables
  SDANALISI_idx=0
  LOG_STOR_idx=0
  SDMOVMAG_idx=0
  TIP_DOCU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Analisi Documenti pre-storicizzazione
    * --- Inserisco i documenti aperti
    do case
      case this.pParam="ESEGUI"
        * --- Ricordo l'ultima elaborazione dell'analisi per l'esercizio
        this.w_PARMSG1 = space(8)+"-"+space(1)
        * --- Select from LOG_STOR
        i_nConn=i_TableProp[this.LOG_STOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LOG_STOR_idx,2],.t.,this.LOG_STOR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select LGCODUTE,LG__DATA  from "+i_cTable+" LOG_STOR ";
              +" where LSCODESE="+cp_ToStrODBC(this.oParentObject.w_CODESE)+" And LG___OPE='A'   And LG__TIPO='L' And LG_esito='S'";
              +" order by LSSERIAL Desc";
               ,"_Curs_LOG_STOR")
        else
          select LGCODUTE,LG__DATA from (i_cTable);
           where LSCODESE=this.oParentObject.w_CODESE And LG___OPE="A"   And LG__TIPO="L" And LG_esito="S";
           order by LSSERIAL Desc;
            into cursor _Curs_LOG_STOR
        endif
        if used('_Curs_LOG_STOR')
          select _Curs_LOG_STOR
          locate for 1=1
          do while not(eof())
          this.w_MESS = "L'ultima analisi � stata lanciata il %1 dall'utente %2"
          if !ah_yesno("Attenzione:%0L'ultima analisi � stata lanciata il %1 dall'utente %2%0%0Confermi comunque l'elaborazione?","",dtoc(Nvl(_Curs_LOG_STOR.LG__DATA,cp_CharToDate("  /  /    "))),Alltrim(str(Nvl(_Curs_LOG_STOR.LGCODUTE,0))))
            i_retcode = 'stop'
            return
          endif
          Exit
            select _Curs_LOG_STOR
            continue
          enddo
          use
        endif
        * --- Verifico eventuali analisi relative alla Produzione
        if g_DIBA="S"
          * --- Select from LOG_STOR
          i_nConn=i_TableProp[this.LOG_STOR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LOG_STOR_idx,2],.t.,this.LOG_STOR_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select LGCODUTE,LG__DATA  from "+i_cTable+" LOG_STOR ";
                +" where LSCODESE="+cp_ToStrODBC(this.oParentObject.w_CODESE)+" And LG___OPE='A'   And LG__TIPO='P' And LG_esito='S' And LGPRODOK='S'";
                +" order by LSSERIAL Desc";
                 ,"_Curs_LOG_STOR")
          else
            select LGCODUTE,LG__DATA from (i_cTable);
             where LSCODESE=this.oParentObject.w_CODESE And LG___OPE="A"   And LG__TIPO="P" And LG_esito="S" And LGPRODOK="S";
             order by LSSERIAL Desc;
              into cursor _Curs_LOG_STOR
          endif
          if used('_Curs_LOG_STOR')
            select _Curs_LOG_STOR
            locate for 1=1
            do while not(eof())
            this.w_MESS = "Esiste una precedente analisi produzione, lanciata il %1 dall'utente %2 i cui risultati verranno annullati proseguendo"
            if !ah_yesno("Attenzione:%0Esiste una precedente analisi produzione, lanciata il %1 dall'utente %2 i cui  risultati verranno annullati proseguendo%0%0Confermi comunque l'elaborazione?%0","",dtoc(Nvl(_Curs_LOG_STOR.LG__DATA,cp_CharToDate("  /  /    "))),Alltrim(str(Nvl(_Curs_LOG_STOR.LGCODUTE,0))))
              i_retcode = 'stop'
              return
            endif
            Exit
              select _Curs_LOG_STOR
              continue
            enddo
            use
          endif
        endif
        * --- Richiesta conferma
        this.w_Messaggio = "Attenzione:%0L'elaborazione dell'analisi pu� richiedere molto tempo in relazione alla dimensione degli archivi%0%0Confermi l'elaborazione?"
        if NOT AH_YESNO(this.w_Messaggio)
          i_retcode = 'stop'
          return
        endif
        * --- Svuoto il testo per segnalare l'operato della pocedura
        this.oParentObject.w_MSG = ""
        * --- Try
        local bErr_04FF4510
        bErr_04FF4510=bTrsErr
        this.Try_04FF4510()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          if Empty ( this.oParentObject.w_TRANSAZIONE )
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
          endif
          AH_ERRORMSG("Analisi terminata con errore %1",48,"",message())
          * --- Registro errore nel log
          * --- begin transaction
          cp_BeginTrs()
          i_Conn=i_TableProp[this.LOG_STOR_IDX, 3]
          cp_NextTableProg(this, i_Conn, "STLOG", "i_codazi,w_LSSERIAL")
          AddMsg("%0Errore: %1" ,this,Message())
          * --- Insert into LOG_STOR
          i_nConn=i_TableProp[this.LOG_STOR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LOG_STOR_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOG_STOR_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"LSSERIAL"+",LSCODESE"+",LG__TIPO"+",LG___OPE"+",LGCODUTE"+",LG__DATA"+",LG__NOTE"+",LGPRODOK"+",LGTODOOK"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_LSSERIAL),'LOG_STOR','LSSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODESE),'LOG_STOR','LSCODESE');
            +","+cp_NullLink(cp_ToStrODBC("L"),'LOG_STOR','LG__TIPO');
            +","+cp_NullLink(cp_ToStrODBC("A"),'LOG_STOR','LG___OPE');
            +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'LOG_STOR','LGCODUTE');
            +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'LOG_STOR','LG__DATA');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MSG),'LOG_STOR','LG__NOTE');
            +","+cp_NullLink(cp_ToStrODBC(" "),'LOG_STOR','LGPRODOK');
            +","+cp_NullLink(cp_ToStrODBC("N"),'LOG_STOR','LGTODOOK');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'LSSERIAL',this.w_LSSERIAL,'LSCODESE',this.oParentObject.w_CODESE,'LG__TIPO',"L",'LG___OPE',"A",'LGCODUTE',i_CODUTE,'LG__DATA',i_DATSYS,'LG__NOTE',this.oParentObject.w_MSG,'LGPRODOK'," ",'LGTODOOK',"N")
            insert into (i_cTable) (LSSERIAL,LSCODESE,LG__TIPO,LG___OPE,LGCODUTE,LG__DATA,LG__NOTE,LGPRODOK,LGTODOOK &i_ccchkf. );
               values (;
                 this.w_LSSERIAL;
                 ,this.oParentObject.w_CODESE;
                 ,"L";
                 ,"A";
                 ,i_CODUTE;
                 ,i_DATSYS;
                 ,this.oParentObject.w_MSG;
                 ," ";
                 ,"N";
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error='Errore inserimento Log'
            return
          endif
          * --- commit
          cp_EndTrs(.t.)
        endif
        bTrsErr=bTrsErr or bErr_04FF4510
        * --- End
      case this.pParam="SELEZIONA"
        * --- Seleziona Deseleziona Tutti
        *     Se il documento � fuori dal'esercizio lo marco sempre
        Update ( this.oParentObject.w_ZOOMDOC.cCursor) Set Xchk= this.oParentObject.w_SELALL Where (ANIMPAPE<>"I" And ANORDAPE<>"O" And ANDOCAPE<>"C" And ANDOCCOM<>"S" And ANDOCPRO<>"S") Or ANDOCAPE="F"
      case this.pParam="MARCA"
        * --- Try
        local bErr_0501CB10
        bErr_0501CB10=bTrsErr
        this.Try_0501CB10()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          AH_ERRORMSG("Aggiornamento terminato con errore %1",48,"",message())
        endif
        bTrsErr=bTrsErr or bErr_0501CB10
        * --- End
        * --- Rimuovo il cursore
        Use in Temp
      case this.pParam="CHECK"
        * --- Se sto visualizzando le scelte gia passate a storico non permetto il check
        Select ( this.oParentObject.w_ZOOMDOC.cCursor )
        this.w_RECPOS = Recno()
        if this.oParentObject.w_STO_OK<>"T"
          * --- Documento che movimenta l'ordinato / impegnato
          if (ANIMPAPE = "I" Or ANORDAPE="O") And ANDOCAPE<>"F"
            if ANORDAPE="O"
              AH_ERRORMSG("Il documento selezionato movimenta l'ordinato, prima di storicizzare occorre evaderlo",48)
            else
              AH_ERRORMSG("Il documento selezionato movimenta l'impegnato, prima di storicizzare occorre evaderlo",48)
            endif
             
 AA=ALLTRIM(STR( this.oParentObject.w_ZoomDoc.grd.ColumnCount )) 
 this.oParentObject.w_ZoomDoc.grd.Column&AA..chk.Value = 0
            i_retcode = 'stop'
            return
          endif
          if ANDOCAPE="C"
            if g_COGE="S"
              ah_ErrorMsg("Il documento selezionato � da contabilizzare, impossibile storicizzarlo senza contabilizzarlo","!","")
            else
              ah_ErrorMsg("Il documento selezionato � da confermare, impossibile storicizzarlo senza confermarlo","!","")
            endif
             
 AA=ALLTRIM(STR( this.oParentObject.w_ZoomDoc.grd.ColumnCount )) 
 this.oParentObject.w_ZoomDoc.grd.Column&AA..chk.Value = 0
            i_retcode = 'stop'
            return
          endif
          if ANDOCPRO="S"
            AH_ERRORMSG("Il documento selezionato � provvisiorio, impossibile storicizzare senza conferma",48)
             
 AA=ALLTRIM(STR( this.oParentObject.w_ZoomDoc.grd.ColumnCount )) 
 this.oParentObject.w_ZoomDoc.grd.Column&AA..chk.Value = 0
            i_retcode = 'stop'
            return
          endif
          if ANDOCCOM="S"
            AH_ERRORMSG("Il documento selezionato utilizza una commessa aperta, non pu� essere storicizzato",48)
             
 AA=ALLTRIM(STR( this.oParentObject.w_ZoomDoc.grd.ColumnCount )) 
 this.oParentObject.w_ZoomDoc.grd.Column&AA..chk.Value = 0
            i_retcode = 'stop'
            return
          endif
          if ANDOCAPE="F"
            AH_ERRORMSG("Il documento selezionato � fuori dall'esercizio da storicizzare, non verr� storicizzato",48)
          endif
          if ANSERIAL=ANSERPAD
            * --- Alla selezione di una riga in automatico seleziono anche tutte le righe generate da essa
            this.w_SERPAD = ANSERPAD
            Update ( this.oParentObject.w_ZOOMDOC.cCursor) Set Xchk= 1 Where ANDOCAPE<>"F" ; 
 And ANSERPAD=this.w_SERPAD And ANIMPAPE<>"I" And ANORDAPE<>"O" and ANDOCAPE<>"C"
          endif
        else
          AH_ERRORMSG("Il documento selezionato � gi� passato a storico",48)
           
 AA=ALLTRIM(STR( this.oParentObject.w_ZoomDoc.grd.ColumnCount )) 
 this.oParentObject.w_ZoomDoc.grd.Column&AA..chk.Value = 0
          i_retcode = 'stop'
          return
        endif
        Go (this.w_RECPOS)
      case this.pParam="Tracciabilita"
        * --- apro la maschera della tracciabilita e riempio i campi
        if g_APPLICATION = "ADHOC REVOLUTION"
          this.w_OGG = GSOR_STD()
        else
          this.w_OGG = GSVE_STD()
        endif
        this.w_OGG.w_TIPCAU = Left( this.oParentObject.w_PARAME ,1 )
        this.w_TIPDOC = Nvl( this.oParentObject.w_ZoomDoc.getVar("MVTIPDOC"), "" )
        this.w_OGG.w_CAUDOC = this.w_TIPDOC
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDFLINTE"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.w_TIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDFLINTE;
            from (i_cTable) where;
                TDTIPDOC = this.w_TIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_OGG.w_TIPOCF = this.w_FLINTE
        this.w_OGG.w_CODCLIFOR = Nvl( this.oParentObject.w_ZoomDoc.getVar("MVCODCON"), "" )
        this.w_OGG.w_NUMDOC = Nvl( this.oParentObject.w_ZoomDoc.getVar("MVNUMDOC"), 0 )
        this.w_OGG.w_ALFDOC = Nvl( this.oParentObject.w_ZoomDoc.getVar("MVALFDOC"), "" )
        this.w_OGG.w_DATDOC = Nvl( this.oParentObject.w_ZoomDoc.getVar("MVDATDOC"), cp_CharToDate("  -  -    ") )
        this.w_OGG.w_MVSERIAL = this.oParentObject.w_SERIALE
        this.w_OGG.SetControlsValue()     
        * --- Lancio il batch per il calcolo tracciabilita
        if g_APPLICATION = "ADHOC REVOLUTION"
          do GSOR_BTD with this.w_OGG , "R" 
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          do GSVE_BTD with this.w_OGG , "R" 
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pParam="UNCHECK"
        Select ( this.oParentObject.w_ZOOMDOC.cCursor )
        if ANSERIAL=ANSERPAD
          this.w_RECPOS = Recno()
          * --- Alla selezione di una riga in automatico seleziono anche tutte le righe generate da essa
          this.w_SERPAD = ANSERPAD
          Update ( this.oParentObject.w_ZOOMDOC.cCursor) Set Xchk= 0 Where ANDOCAPE<>"F" ; 
 And ANSERPAD=this.w_SERPAD And ANIMPAPE<>"I" And ANORDAPE<>"O" and ANDOCAPE<>"C" 
          Go (this.w_RECPOS)
        endif
      case this.pParam="VEDI"
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc
  proc Try_04FF4510()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    AddMsgNL("Elaborazione iniziata alle %1",this,Time())
    * --- Elenco documenti non storicizzati esercizi precedenti.
    *     La procedura consulta l'analisi di questi esercizi.
    * --- Select from GSSR4BAD
    do vq_exec with 'GSSR4BAD',this,'_Curs_GSSR4BAD','',.f.,.t.
    if used('_Curs_GSSR4BAD')
      select _Curs_GSSR4BAD
      locate for 1=1
      do while not(eof())
      if Nvl( _Curs_GSSR4BAD.TIPO ,"" )="D"
        AddMsgNL("Documenti non storicizzati nell'esercizio %1: %2",this,Nvl( _Curs_GSSR4BAD.CODESE , " "),alltrim(Str( Nvl(_Curs_GSSR4BAD.CONTA,0))))
      endif
      if Nvl( _Curs_GSSR4BAD.TIPO ,"" )="M"
        AddMsgnl("Mov. magazzino non storicizzati nell'esercizio %1: %2",this,Nvl( _Curs_GSSR4BAD.CODESE , " " ),alltrim(Str( Nvl(_Curs_GSSR4BAD.CONTA,0))))
      endif
        select _Curs_GSSR4BAD
        continue
      enddo
      use
    endif
    if Empty ( this.oParentObject.w_TRANSAZIONE )
      addmsgNL("Elaborazione fuori transazione")
    else
      AddmsgNL("Elaborazione sotto transazione")
      * --- begin transaction
      cp_BeginTrs()
    endif
    AddMsgNL("Svuoto temporaneo documenti analizzati precedenti elaborazioni esercizio %1",this,this.oParentObject.w_CODESE)
    * --- Non elimino le righe gi� storicizzate per tenere traccia delle scelte degli utenti
    * --- Delete from SDANALISI
    i_nConn=i_TableProp[this.SDANALISI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"ANCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
            +" and ANSTO_OK <> "+cp_ToStrODBC("T");
             )
    else
      delete from (i_cTable) where;
            ANCODESE = this.oParentObject.w_CODESE;
            and ANSTO_OK <> "T";

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error='Errore delete su temporaneo analisi'
      return
    endif
    * --- Try
    local bErr_05013D50
    bErr_05013D50=bTrsErr
    this.Try_05013D50()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if i_Rows=-1
        AddMsg("Errore inserzione in analisi dei documenti aperti: %1",this,Message())
        this.w_Messaggio = AH_MSGFORMAT("Errore")
        * --- Raise
        i_Error=this.w_Messaggio
        return
      else
        * --- Nel caso non ho  nessun documento aperto
        * --- accept error
        bTrsErr=.f.
      endif
    endif
    bTrsErr=bTrsErr or bErr_05013D50
    * --- End
    this.w_LOOP = .T.
    this.w_RecElab = 0
    AddmsgNL("Ricerca antenati documenti...",this)
    do while this.w_LOOP
      this.w_LOOP = .F.
      this.w_FIRSTLAP = .T.
      * --- Recupero tutti i documenti ancora da analizzare
      * --- Select from SDANALISI
      i_nConn=i_TableProp[this.SDANALISI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2],.t.,this.SDANALISI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" SDANALISI ";
            +" where ANCODESE= "+cp_ToStrODBC(this.oParentObject.w_CODESE)+" And AN__FLAG<>'S'";
             ,"_Curs_SDANALISI")
      else
        select * from (i_cTable);
         where ANCODESE= this.oParentObject.w_CODESE And AN__FLAG<>"S";
          into cursor _Curs_SDANALISI
      endif
      if used('_Curs_SDANALISI')
        select _Curs_SDANALISI
        locate for 1=1
        do while not(eof())
        if this.w_FIRSTLAP
          * --- Metto 'S' (AN__FLAG, documento analizzato) in tutte le righe recuperate dalla Select (la visual query � analoga alla
          *     frase nella select).Non ho utilizzato un temporaneo perch� comunque devo recuperare il dato sul Client
          * --- Write into SDANALISI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SDANALISI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="ANSERIAL"
            do vq_exec with 'SDANALISI',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SDANALISI_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="SDANALISI.ANSERIAL = _t2.ANSERIAL";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"AN__FLAG       ="+cp_NullLink(cp_ToStrODBC("S"),'SDANALISI','AN__FLAG      ');
                +i_ccchkf;
                +" from "+i_cTable+" SDANALISI, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="SDANALISI.ANSERIAL = _t2.ANSERIAL";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SDANALISI, "+i_cQueryTable+" _t2 set ";
            +"SDANALISI.AN__FLAG       ="+cp_NullLink(cp_ToStrODBC("S"),'SDANALISI','AN__FLAG      ');
                +Iif(Empty(i_ccchkf),"",",SDANALISI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="SDANALISI.ANSERIAL = t2.ANSERIAL";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SDANALISI set (";
                +"AN__FLAG      ";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +cp_NullLink(cp_ToStrODBC("S"),'SDANALISI','AN__FLAG      ')+"";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="SDANALISI.ANSERIAL = _t2.ANSERIAL";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SDANALISI set ";
            +"AN__FLAG       ="+cp_NullLink(cp_ToStrODBC("S"),'SDANALISI','AN__FLAG      ');
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".ANSERIAL = "+i_cQueryTable+".ANSERIAL";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"AN__FLAG       ="+cp_NullLink(cp_ToStrODBC("S"),'SDANALISI','AN__FLAG      ');
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore nella scrittura del flag analizzato nel temporaneo analisi'
            return
          endif
          this.w_FIRSTLAP = .F.
        endif
        this.w_SERRIF = Nvl( _Curs_SDANALISI.ANSERRIF , Space(10) )
        if Not Empty( this.w_SERRIF )
          this.w_RecElab = this.w_RecElab + 1
          this.w_Mess = "Elabora documento %1 esaminati %2"
          this.w_SERPAD = Nvl( _Curs_SDANALISI.ANSERPAD , Space(10) )
          this.w_ROWNUM = Nvl( _Curs_SDANALISI.ANROWNUM , 0)
          this.w_NUMRIF = Nvl( _Curs_SDANALISI.ANNUMRIF , 0)
          * --- Verifico la presenza nel Log di analisi del documento con Serial
          *     w_SERRIF. Se � presente significa che l'ho gia elaborato non eseguo
          *     la query ma creo un entrata nel Log per motivi di visualizzazione
          *     - Leggo un campo a caso per verifiare l'esistenza - 
          * --- Read from SDANALISI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.SDANALISI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2],.t.,this.SDANALISI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANSERPAD"+;
              " from "+i_cTable+" SDANALISI where ";
                  +"ANSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANSERPAD;
              from (i_cTable) where;
                  ANSERIAL = this.w_SERRIF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TmpC = NVL(cp_ToDate(_read_.ANSERPAD),cp_NullValue(_read_.ANSERPAD))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows=0
            * --- Try
            local bErr_0530BFC8
            bErr_0530BFC8=bTrsErr
            this.Try_0530BFC8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- Nel caso non ho  nessun documento aperto
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_0530BFC8
            * --- End
          else
            * --- Creo un riferimento con flag analizzato al documento di origine
            * --- Insert into SDANALISI
            i_nConn=i_TableProp[this.SDANALISI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SDANALISI_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"ANSERIAL"+",ANSERPAD"+",ANROWNUM"+",ANNUMRIF"+",AN__FLAG"+",ANCODESE"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_SERRIF),'SDANALISI','ANSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_SERPAD),'SDANALISI','ANSERPAD');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'SDANALISI','ANROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIF),'SDANALISI','ANNUMRIF');
              +","+cp_NullLink(cp_ToStrODBC("S"),'SDANALISI','AN__FLAG      ');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODESE),'SDANALISI','ANCODESE');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'ANSERIAL',this.w_SERRIF,'ANSERPAD',this.w_SERPAD,'ANROWNUM',this.w_ROWNUM,'ANNUMRIF',this.w_NUMRIF,'AN__FLAG      ',"S",'ANCODESE',this.oParentObject.w_CODESE)
              insert into (i_cTable) (ANSERIAL,ANSERPAD,ANROWNUM,ANNUMRIF,AN__FLAG      ,ANCODESE &i_ccchkf. );
                 values (;
                   this.w_SERRIF;
                   ,this.w_SERPAD;
                   ,this.w_ROWNUM;
                   ,this.w_NUMRIF;
                   ,"S";
                   ,this.oParentObject.w_CODESE;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error='Errore inserimento riga riferimento in analisi'
              return
            endif
          endif
          if this.w_RecElab=1
            this.w_POSIZIONE = LEN(this.oParentObject.w_MSG) 
            AddMsgNL(this.w_MESS,This.oparentobject, ALLTRIM(this.w_SERRIF), Alltrim(Str(this.w_RecElab)) )
          else
            this.oParentObject.w_MSG = Left( this.oParentObject.w_MSG , this.w_POSIZIONE) + ah_msgformat(this.w_MESS,ALLTRIM(this.w_SERRIF),Alltrim(Str( this.w_RecElab )))
            AddMsgNL("",this)
          endif
        endif
        this.w_LOOP = .T.
          select _Curs_SDANALISI
          continue
        enddo
        use
      endif
    enddo
    * --- Verifico la presenza o meno di record all'interno dell'archivio di analisi.
    *     Questo per poter verificare prima della storicizzazione se � stata lanciata 
    *     l'analisi
    this.w_CONTA = 0
    * --- Select from SDANALISI
    i_nConn=i_TableProp[this.SDANALISI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2],.t.,this.SDANALISI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" SDANALISI ";
          +" where ANCODESE= "+cp_ToStrODBC(this.oParentObject.w_CODESE)+"";
           ,"_Curs_SDANALISI")
    else
      select Count(*) As Conta from (i_cTable);
       where ANCODESE= this.oParentObject.w_CODESE;
        into cursor _Curs_SDANALISI
    endif
    if used('_Curs_SDANALISI')
      select _Curs_SDANALISI
      locate for 1=1
      do while not(eof())
      this.w_CONTA = Nvl( _Curs_SDANALISI.CONTA , 0 )
        select _Curs_SDANALISI
        continue
      enddo
      use
    endif
    if this.w_CONTA=0
      * --- Creo una riga fittizia se non ho documenti scorretti contenente come Serial -1
      AddMsgNL("Inserisco riga fittizia per esercizio %1",this,this.oParentObject.w_CODESE)
      * --- Insert into SDANALISI
      i_nConn=i_TableProp[this.SDANALISI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SDANALISI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"ANSERIAL"+",ANCODESE"+",AN__FLAG"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC("-1"),'SDANALISI','ANSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODESE),'SDANALISI','ANCODESE');
        +","+cp_NullLink(cp_ToStrODBC("S"),'SDANALISI','AN__FLAG');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'ANSERIAL',"-1",'ANCODESE',this.oParentObject.w_CODESE,'AN__FLAG',"S")
        insert into (i_cTable) (ANSERIAL,ANCODESE,AN__FLAG &i_ccchkf. );
           values (;
             "-1";
             ,this.oParentObject.w_CODESE;
             ,"S";
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore creazione riga fittizia'
        return
      endif
    else
      * --- Setto Flag Impegnato, Ordinato o da Contabilizzare nel caso un documento
      *     sia antenato (Bianco) oltre che non storicizzabile.
      *     Esempio Ordine parzialmente evaso da DDT aperto (evadibile). Senza
      *     questa Update sarebbe in analisi 2 volte una come ordine aperto e una 
      *     come antenato del DDT, quest'ultima occorenza risulterebbe selezionabile
      *     (Bianca) e quindi darei la possibilit� di storicizzare un ordine non storicizzabile
      AddmsgNL("Marco le righe chiuse appartenenti a documenti non storicizzabili")
      * --- Write into SDANALISI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SDANALISI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="ANSERIAL"
        do vq_exec with 'GSSR2BAD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SDANALISI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="SDANALISI.ANSERIAL = _t2.ANSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ANDOCAPE = _t2.ANDOCAPE";
            +",ANIMPAPE = _t2.ANIMPAPE";
            +",ANORDAPE = _t2.ANORDAPE";
            +i_ccchkf;
            +" from "+i_cTable+" SDANALISI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="SDANALISI.ANSERIAL = _t2.ANSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SDANALISI, "+i_cQueryTable+" _t2 set ";
            +"SDANALISI.ANDOCAPE = _t2.ANDOCAPE";
            +",SDANALISI.ANIMPAPE = _t2.ANIMPAPE";
            +",SDANALISI.ANORDAPE = _t2.ANORDAPE";
            +Iif(Empty(i_ccchkf),"",",SDANALISI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="SDANALISI.ANSERIAL = t2.ANSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SDANALISI set (";
            +"ANDOCAPE,";
            +"ANIMPAPE,";
            +"ANORDAPE";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.ANDOCAPE,";
            +"t2.ANIMPAPE,";
            +"t2.ANORDAPE";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="SDANALISI.ANSERIAL = _t2.ANSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SDANALISI set ";
            +"ANDOCAPE = _t2.ANDOCAPE";
            +",ANIMPAPE = _t2.ANIMPAPE";
            +",ANORDAPE = _t2.ANORDAPE";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".ANSERIAL = "+i_cQueryTable+".ANSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ANDOCAPE = (select ANDOCAPE from "+i_cQueryTable+" where "+i_cWhere+")";
            +",ANIMPAPE = (select ANIMPAPE from "+i_cQueryTable+" where "+i_cWhere+")";
            +",ANORDAPE = (select ANORDAPE from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Marco i documenti gia presenti
    AddmsgNL("Marco documenti provvisori")
    * --- Write into SDANALISI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SDANALISI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="ANSERIAL"
      do vq_exec with 'GSSR10BAD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SDANALISI_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="SDANALISI.ANSERIAL = _t2.ANSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ANDOCPRO ="+cp_NullLink(cp_ToStrODBC("S"),'SDANALISI','ANDOCPRO');
          +i_ccchkf;
          +" from "+i_cTable+" SDANALISI, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="SDANALISI.ANSERIAL = _t2.ANSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SDANALISI, "+i_cQueryTable+" _t2 set ";
      +"SDANALISI.ANDOCPRO ="+cp_NullLink(cp_ToStrODBC("S"),'SDANALISI','ANDOCPRO');
          +Iif(Empty(i_ccchkf),"",",SDANALISI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="SDANALISI.ANSERIAL = t2.ANSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SDANALISI set (";
          +"ANDOCPRO";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +cp_NullLink(cp_ToStrODBC("S"),'SDANALISI','ANDOCPRO')+"";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="SDANALISI.ANSERIAL = _t2.ANSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SDANALISI set ";
      +"ANDOCPRO ="+cp_NullLink(cp_ToStrODBC("S"),'SDANALISI','ANDOCPRO');
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".ANSERIAL = "+i_cQueryTable+".ANSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ANDOCPRO ="+cp_NullLink(cp_ToStrODBC("S"),'SDANALISI','ANDOCPRO');
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Aggiungo i provvisori non trovati in analisi
    AddmsgNL("Aggiungo documenti provvisori%0")
    * --- Insert into SDANALISI
    i_nConn=i_TableProp[this.SDANALISI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSR11BAD",this.SDANALISI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    AddMsgNL("Documenti provvisori aggiunti: %1",this,Alltrim(Str( i_Rows )))
    * --- Inserisco i documenti provvisori all'interno dell'analisi
    if g_COMM="S"
      * --- Svuoto il contenuto archivio contenente movimenti scartati precedente
      *     analisi
      AddMsgNL("Prod. su commessa, svuoto temporaneo mov. di magazzino: %1",this,alltrim(Str( i_Rows )))
      * --- Delete from SDMOVMAG
      i_nConn=i_TableProp[this.SDMOVMAG_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SDMOVMAG_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"MMTIPESC = "+cp_ToStrODBC("C");
              +" and MMCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
               )
      else
        delete from (i_cTable) where;
              MMTIPESC = "C";
              and MMCODESE = this.oParentObject.w_CODESE;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Marco i documenti gia presenti
      AddmsgNL("Marco documenti con commessa")
      * --- Write into SDANALISI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SDANALISI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="ANSERIAL"
        do vq_exec with 'GSSR6BAD',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SDANALISI_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="SDANALISI.ANSERIAL = _t2.ANSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ANDOCCOM ="+cp_NullLink(cp_ToStrODBC("S"),'SDANALISI','ANDOCCOM');
            +i_ccchkf;
            +" from "+i_cTable+" SDANALISI, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="SDANALISI.ANSERIAL = _t2.ANSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SDANALISI, "+i_cQueryTable+" _t2 set ";
        +"SDANALISI.ANDOCCOM ="+cp_NullLink(cp_ToStrODBC("S"),'SDANALISI','ANDOCCOM');
            +Iif(Empty(i_ccchkf),"",",SDANALISI.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="SDANALISI.ANSERIAL = t2.ANSERIAL";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SDANALISI set (";
            +"ANDOCCOM";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +cp_NullLink(cp_ToStrODBC("S"),'SDANALISI','ANDOCCOM')+"";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="SDANALISI.ANSERIAL = _t2.ANSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SDANALISI set ";
        +"ANDOCCOM ="+cp_NullLink(cp_ToStrODBC("S"),'SDANALISI','ANDOCCOM');
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".ANSERIAL = "+i_cQueryTable+".ANSERIAL";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ANDOCCOM ="+cp_NullLink(cp_ToStrODBC("S"),'SDANALISI','ANDOCCOM');
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiungo i documenti non trovati nell'analisi
      AddmsgNL("Aggiungo documenti con commessa")
      * --- Insert into SDANALISI
      i_nConn=i_TableProp[this.SDANALISI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSR8BAD",this.SDANALISI_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      AddmsgNL("Aggiungo mov. magazzino con commessa")
      * --- Insert into SDMOVMAG
      i_nConn=i_TableProp[this.SDMOVMAG_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SDMOVMAG_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSR9BAD",this.SDMOVMAG_idx)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    if g_DIBA="S"
      AddMsg("%0Annullo analisi produzione")
      * --- Imposto il semaforo per impedire lo storico della produzione
      * --- Write into LOG_STOR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.LOG_STOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LOG_STOR_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.LOG_STOR_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"LGPRODOK ="+cp_NullLink(cp_ToStrODBC("N"),'LOG_STOR','LGPRODOK');
            +i_ccchkf ;
        +" where ";
            +"LSCODESE = "+cp_ToStrODBC(this.oParentObject.w_CODESE);
            +" and LG__TIPO = "+cp_ToStrODBC("P");
            +" and LG_ESITO = "+cp_ToStrODBC("S");
            +" and LGPRODOK = "+cp_ToStrODBC("S");
               )
      else
        update (i_cTable) set;
            LGPRODOK = "N";
            &i_ccchkf. ;
         where;
            LSCODESE = this.oParentObject.w_CODESE;
            and LG__TIPO = "P";
            and LG_ESITO = "S";
            and LGPRODOK = "S";

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    AddMsg("%0Elaborazione terminata alle: %1",this,Time())
    * --- Creo il log dell'esecuzione dell'analisi
    AddMsg("%0Inserisco l'elaborazione nel log")
    this.w_LSSERIAL = Space(10)
    if this.oParentObject.w_TRANSAZIONE<>"S"
      * --- begin transaction
      cp_BeginTrs()
    endif
    i_Conn=i_TableProp[this.LOG_STOR_IDX, 3]
    cp_NextTableProg(this, i_Conn, "STLOG", "i_codazi,w_LSSERIAL")
    * --- Insert into LOG_STOR
    i_nConn=i_TableProp[this.LOG_STOR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOG_STOR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOG_STOR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LSSERIAL"+",LSCODESE"+",LG__TIPO"+",LG___OPE"+",LGCODUTE"+",LG__DATA"+",LG__NOTE"+",LG_ESITO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_LSSERIAL),'LOG_STOR','LSSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CODESE),'LOG_STOR','LSCODESE');
      +","+cp_NullLink(cp_ToStrODBC("L"),'LOG_STOR','LG__TIPO');
      +","+cp_NullLink(cp_ToStrODBC("A"),'LOG_STOR','LG___OPE');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'LOG_STOR','LGCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(i_DATSYS),'LOG_STOR','LG__DATA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MSG),'LOG_STOR','LG__NOTE');
      +","+cp_NullLink(cp_ToStrODBC("S"),'LOG_STOR','LG_ESITO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LSSERIAL',this.w_LSSERIAL,'LSCODESE',this.oParentObject.w_CODESE,'LG__TIPO',"L",'LG___OPE',"A",'LGCODUTE',i_CODUTE,'LG__DATA',i_DATSYS,'LG__NOTE',this.oParentObject.w_MSG,'LG_ESITO',"S")
      insert into (i_cTable) (LSSERIAL,LSCODESE,LG__TIPO,LG___OPE,LGCODUTE,LG__DATA,LG__NOTE,LG_ESITO &i_ccchkf. );
         values (;
           this.w_LSSERIAL;
           ,this.oParentObject.w_CODESE;
           ,"L";
           ,"A";
           ,i_CODUTE;
           ,i_DATSYS;
           ,this.oParentObject.w_MSG;
           ,"S";
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento Log'
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_0501CB10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.oParentObject.w_MSG = ""
    this.oParentobject.oPgFrm.ActivePage=3
    AddmsgNL("Marca i documenti selezionati per essere storicizzati")
    AddMsgNL("Elaborazione iniziata alle %1",this,Time())
    Select Distinct ANSERIAL from ( this.oParentObject.w_ZOOMDOC.cCursor ) where Xchk=1 into cursor Temp NoFilter 
 Select Temp 
 Go Top
    this.w_RecElab = 0
    * --- begin transaction
    cp_BeginTrs()
    do while Not Eof( "Temp" )
      this.w_ANSERIAL = Temp.ANSERIAL
      * --- Write into SDANALISI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SDANALISI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SDANALISI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ANSTO_OK ="+cp_NullLink(cp_ToStrODBC("S"),'SDANALISI','ANSTO_OK');
            +i_ccchkf ;
        +" where ";
            +"ANSERIAL = "+cp_ToStrODBC(this.w_ANSERIAL);
               )
      else
        update (i_cTable) set;
            ANSTO_OK = "S";
            &i_ccchkf. ;
         where;
            ANSERIAL = this.w_ANSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_RecElab = this.w_RecElab + i_Rows
      this.w_Mess = "Marco documento %1 totale record marcati %2"
      if this.w_RecElab=1
        this.w_POSIZIONE = LEN(this.oParentObject.w_MSG)
        AddMsgNL(this.w_MESS, this.oparentobject, ALLTRIM(this.w_ANSERIAL), Alltrim(Str(this.w_RecElab)) )
      else
        this.oParentObject.w_MSG = Left( this.oParentObject.w_MSG , this.w_POSIZIONE) + ah_msgformat(this.w_MESS,ALLTRIM(this.w_ANSERIAL),Alltrim(Str( this.w_RecElab )))
        AddMsgNL("",this.oparentobject)
      endif
      if Not Eof( "Temp" )
        Select Temp 
 Skip
      endif
    enddo
    if g_DIBA="S"
      * --- Se attivo il Modulo Produzione sbianca flag dell'analisi
      * --- Select from LOG_STOR
      i_nConn=i_TableProp[this.LOG_STOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LOG_STOR_idx,2],.t.,this.LOG_STOR_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select LSSERIAL  from "+i_cTable+" LOG_STOR ";
            +" where LSCODESE="+cp_ToStrODBC(this.oParentObject.w_CODESE)+" And LG___OPE='A'   And LG__TIPO='L'";
            +" order by LSSERIAL Desc";
             ,"_Curs_LOG_STOR")
      else
        select LSSERIAL from (i_cTable);
         where LSCODESE=this.oParentObject.w_CODESE And LG___OPE="A"   And LG__TIPO="L";
         order by LSSERIAL Desc;
          into cursor _Curs_LOG_STOR
      endif
      if used('_Curs_LOG_STOR')
        select _Curs_LOG_STOR
        locate for 1=1
        do while not(eof())
        * --- Write into LOG_STOR
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.LOG_STOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LOG_STOR_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.LOG_STOR_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"LGPRODOK ="+cp_NullLink(cp_ToStrODBC("A"),'LOG_STOR','LGPRODOK');
              +i_ccchkf ;
          +" where ";
              +"LSSERIAL > "+cp_ToStrODBC(_Curs_LOG_STOR.LSSERIAL);
              +" and LG__TIPO = "+cp_ToStrODBC("P");
              +" and LGPRODOK = "+cp_ToStrODBC("S");
                 )
        else
          update (i_cTable) set;
              LGPRODOK = "A";
              &i_ccchkf. ;
           where;
              LSSERIAL > _Curs_LOG_STOR.LSSERIAL;
              and LG__TIPO = "P";
              and LGPRODOK = "S";

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        AddMsg("%0Annullo analisi della produzione",this)
        exit
          select _Curs_LOG_STOR
          continue
        enddo
        use
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    AddMsg("%0Elaborazione terminata alle: %1",this,Time())
    AH_ERRORMSG("Aggiornamento terminato con successo",48)
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return
  proc Try_05013D50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    AddmsgNL("Inserisco nel temporaneo i documenti aperti e/o da contabilizzare")
    * --- Insert into SDANALISI
    i_nConn=i_TableProp[this.SDANALISI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSR_AND",this.SDANALISI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento documenti aperti'
      return
    endif
    AddMsgNL("Righe di documenti da contabilizzare: %1",this,Alltrim(Str( i_Rows )))
    * --- Insert into SDANALISI
    i_nConn=i_TableProp[this.SDANALISI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSR1AND",this.SDANALISI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento documenti aperti'
      return
    endif
    AddMsgNL("Righe di documenti aperti che movimentano impegnato o ordinato: %1",this,Alltrim(Str( i_Rows )))
    * --- Insert into SDANALISI
    i_nConn=i_TableProp[this.SDANALISI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSR2AND",this.SDANALISI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento documenti aperti'
      return
    endif
    AddMsgnl("Righe di documenti importati da documenti nell'esercizio: %1",this,Alltrim(Str( i_Rows )))
    * --- Insert into SDANALISI
    i_nConn=i_TableProp[this.SDANALISI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSSR3AND",this.SDANALISI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserimento documenti aperti'
      return
    endif
    AddMsgNL("Righe di documenti aperti nell'esercizio: %1",this,Alltrim(Str( i_Rows )))
    return
  proc Try_0530BFC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SDANALISI
    i_nConn=i_TableProp[this.SDANALISI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SDANALISI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"gssr_bad",this.SDANALISI_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore inserzione temporaneo analisi'
      return
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Visualizza le scelte passate a storico o gi� analizzate
    *     Cambia la query dello zoom (ottimizzazione per l'apertura della maschera)
    this.oParentObject.w_ZOOMDOC.cCpQueryName = "QUERY\GSSR3KAD"
    This.oParentObject.NotifyEvent("Esegui")
  endproc


  proc Init(oParentObject,pParam)
    this.pParam=pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='SDANALISI'
    this.cWorkTables[2]='LOG_STOR'
    this.cWorkTables[3]='SDMOVMAG'
    this.cWorkTables[4]='TIP_DOCU'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_LOG_STOR')
      use in _Curs_LOG_STOR
    endif
    if used('_Curs_LOG_STOR')
      use in _Curs_LOG_STOR
    endif
    if used('_Curs_GSSR4BAD')
      use in _Curs_GSSR4BAD
    endif
    if used('_Curs_SDANALISI')
      use in _Curs_SDANALISI
    endif
    if used('_Curs_SDANALISI')
      use in _Curs_SDANALISI
    endif
    if used('_Curs_LOG_STOR')
      use in _Curs_LOG_STOR
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParam"
endproc
