* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_bgk                                                        *
*              Controllo parametri                                             *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-16                                                      *
* Last revis.: 2005-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscp_bgk",oParentObject)
return(i_retval)

define class tgscp_bgk as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if EMPTY(this.oParentObject.w_MVTIPDOC2) OR EMPTY(this.oParentObject.w_MVTIPDOC4) OR EMPTY(this.oParentObject.w_MVTIPDOC6) OR EMPTY(this.oParentObject.w_MVTIPDOC8) OR EMPTY(this.oParentObject.w_CODMAG2) OR EMPTY(this.oParentObject.w_CODMAG4) OR EMPTY(this.oParentObject.w_CODMAG6) OR EMPTY(this.oParentObject.w_CODMAG6)
      ah_errormsg("Parametri di generazione non impostati. Impossibile proseguire")
      this.oParentobject.ecpQuit
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
