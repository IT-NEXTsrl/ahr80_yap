* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_bpd                                                        *
*              Manutenzione PDA                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_114]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-09-20                                                      *
* Last revis.: 2017-06-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Azione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsac_bpd",oParentObject,m.Azione)
return(i_retval)

define class tgsac_bpd as StdBatch
  * --- Local variables
  k = 0
  Azione = space(10)
  PunPAD = .NULL.
  GRID = .NULL.
  cCursor = space(10)
  ELEN = .NULL.
  NumPeriodi = 0
  PrimaColPer = 0
  NumMaxPeriodi = 0
  DatEla = ctod("  /  /  ")
  TmpD = ctod("  /  /  ")
  f_TIPGES = space(1)
  gDI = ctod("  /  /  ")
  TmpN = 0
  sDI = ctod("  /  /  ")
  i = 0
  mDI = ctod("  /  /  ")
  TmpC = space(100)
  tDI = ctod("  /  /  ")
  TmpC1 = space(10)
  gDF = ctod("  /  /  ")
  y = 0
  sDF = ctod("  /  /  ")
  tCURPER = space(3)
  mDF = ctod("  /  /  ")
  tPABCUM = 0
  tDF = ctod("  /  /  ")
  w_GestionePDA = .f.
  DETT = .NULL.
  cCursDett = space(10)
  w_LockColPDA = 0
  w_LockColDETT = 0
  w_c = 0
  w_MESS = space(50)
  TmpC2 = space(100)
  TmpD1 = ctod("  /  /  ")
  w_NewCursMast = space(10)
  cCursMast = space(10)
  z = 0
  w_KEYSAL = space(40)
  w_ONHAND = 0
  cNRecSel = 0
  w_NewCursDett = space(10)
  w_EVAMAG = ctod("  /  /  ")
  tDESART = space(40)
  tCODICE = space(20)
  tCODMAG = space(5)
  tCAUIMP = space(5)
  tINIPER = ctod("  /  /  ")
  tDESCOD = space(40)
  tCODART = space(20)
  tMAGDEF = space(5)
  tOFLORD = space(1)
  tPRILAV = ctod("  /  /  ")
  tDESVAR = space(40)
  tCODVAR = space(20)
  tCAUORD = space(5)
  tOFLIMP = space(1)
  tDFINE = ctod("  /  /  ")
  tUNMIS1 = space(3)
  tOPERAT = space(1)
  tMOLTIP = 0
  tMPDATINI = ctod("  /  /  ")
  tLOTPRO = 0
  tUNMIS2 = space(3)
  tOPERA3 = space(1)
  tMOLTI3 = 0
  tMPDATFIN = ctod("  /  /  ")
  tLOTECO = 0
  tUNMIS3 = space(3)
  tTIPVAR = space(5)
  tMPLEATIM = 0
  tOGGPDA = space(1)
  tFAMPRO = space(5)
  tGIOAPP = 0
  tCODVAP = space(20)
  tQTAMIN = 0
  tDESMAG = space(40)
  tLOTRIO = 0
  tDATEVA = ctod("  /  /  ")
  tCODCON = space(15)
  tTIPVAR = space(5)
  GestPDA = .NULL.
  w_RCODART = space(20)
  * --- WorkFile variables
  KEY_ARTI_idx=0
  PDA_TPER_idx=0
  PAR_PROD_idx=0
  ART_ICOL_idx=0
  PAR_RIOR_idx=0
  MAGAZZIN_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Richiamata da GSAC_KPD
    * --- INTESTAZIONI = Prepara intestazioni dei due zoom della prima pagina (w_ZoomPDA, w_DettPDA)
    * --- MENU = Prepara menu per manutenzione PDA
    * --- CARICA = Prepara menu per manutenzione PDA
    * --- se .T. � visibile solo lo zoom w_ZoomPDA, altrimenti anche w_DettPDA
    * --- Altezza dello zoom w_ZoomPDA
    * --- Data Elaborazione PDA
    * --- Ora Elaborazione PDA
    * --- Dimensione colonna in visualizzazione Zoom
    * --- Dimensione Font per visualizzazione Zoom
    * --- Numero Periodi dell'orizzonte temporale elaborato
    * --- Massimo Numero Periodi dell'orizzonte temporale elaborato (Default=54)
    * --- Codice selezionato per visualizzazione dettaglio del periodo selezionato da zoom PDA (per zoom <PeriodoPDA>)
    * --- Codice selezionato per visualizzazione dettaglio del periodo selezionato da zoom PDA (per zoom <PeriodoPDA>)
    * --- Periodo ASSOLUTO selezionato da <ZoomPDA> (per zomm <PeriodoPDA>)
    * --- Periodo RELATIVO selezionato da <ZoomPDA> (per zomm <PeriodoPDA>)
    * --- Descrizione Tipo Periodo selezionato
    * --- Data INIZIO periodo selezionato
    * --- Data FINE periodo selezionato
    * --- PDA Selezionata da zoom del dettaglio periodo (<PeriodoPDA>) (Seriale)
    * --- PDA Selezionata da zoom del dettaglio periodo (<PeriodoPDA>)
    * --- Colonna attiva in w_ZOOMPDA
    * --- FILTRI
    * --- Variabili locali
    Private Scelta
    Scelta = rTrim(this.Azione)
    this.PunPAD = this.oParentObject
    this.GRID = this.PunPAD.w_ZoomPDA.GRD
    this.cCursor = this.PunPad.w_ZoomPDA.cCursor
    this.ELEN = this.PunPAD.w_PeriodoPDA.GRD
    this.NumPeriodi = this.oParentObject.w_NMAXPE
    this.PrimaColPer = 3
    * --- Prima colonna dei periodi
    this.NumMaxPeriodi = this.PrimaColPer + this.NumPeriodi
    * --- Controlla se sulla maschera c'� lo zoom del dettaglio (w_DETTPDA)
    if type("this.PUNPAD.w_DettPDA") = "U"
      this.w_GestionePDA = .F.
    else
      * --- zoom dettaglio
      this.w_GestionePDA = .T.
      this.DETT = this.PunPAD.w_DettPDA.GRD
      this.cCursDETT = this.PunPad.w_DettPDA.cCursor
    endif
    do case
      case this.Azione = "XSCROLL"
        this.w_LockColPDA = this.GRID.LockColumns
        this.w_LockColDETT = this.DETT.LockColumns
        if this.PunPAD.w_DettPDA.Visible
          * --- Attiva sempre il pannello di destra per lo scroll
          this.PunPAD.LockScreen = .T.
          this.w_c = 1
          do while this.w_c <= this.w_LockColPDA
            this.GRID.Columns( this.w_c ).Visible = .f.
            this.w_c = this.w_c + 1
          enddo
          this.w_c = 1
          do while this.w_c <= this.w_LockColDETT
            this.DETT.Columns( this.w_c ).Visible = .f.
            this.w_c = this.w_c + 1
          enddo
          if this.GRID.LeftColumn + 2 <> this.DETT.LeftColumn + 2
            * --- Allinea
            this.TmpN = this.DETT.LeftColumn
            do while this.GRID.LeftColumn > this.DETT.LeftColumn
              * --- Scrolla il dettaglio a destra di una posizione
              this.DETT.DoScroll(5)     
              if this.DETT.LeftColumn=this.TmpN
                * --- Lo scroll a destra sul dettaglio non puo' avvenire ...
                this.GRID.DoScroll(4)     
                exit
              else
                this.TmpN = this.DETT.LeftColumn
              endif
            enddo
            do while this.GRID.LeftColumn < this.DETT.LeftColumn
              * --- Scrolla il dettaglio a destra di una posizione
              this.DETT.DoScroll(4)     
            enddo
          endif
          this.w_c = 1
          do while this.w_c <= this.w_LockColPDA
            this.GRID.Columns( this.w_c ).Visible = .t.
            this.w_c = this.w_c + 1
          enddo
          this.GRID.Resize()     
          this.w_c = 1
          do while this.w_c <= this.w_LockColDETT
            this.DETT.Columns( this.w_c ).Visible = .t.
            this.w_c = this.w_c + 1
          enddo
          this.DETT.Resize()     
          this.PunPAD.LockScreen = .F.
        endif
      case this.Azione = "INTESTAZIONI"
        * --- All'evento INIT - Prepara colonne Zoom
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Nasconde Zoom Dettaglio
        if this.w_GestionePDA
          this.PunPAD.w_DettPDA.visible = .F.
        endif
      case this.Azione = "Interroga"
        * --- Bottone Aggiorna - Carica cursore zoom
        if this.oParentObject.oPgFrm.ActivePage <> 1
          this.oParentObject.oPgFrm.ActivePage = 1
        endif
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Azione = "HIDEDETT"
        * --- zoom dettaglio
        this.DETT = this.PunPAD.w_DettPDA.GRD
        this.cCursDETT = this.PunPad.w_DettPDA.cCursor
        this.oParentObject.w_ZoomEspanso = .t.
        this.PunPAD.w_ZoomPDA.cHeight = max(this.PunPAD.w_DettPDA.TOP + this.PunPAD.w_DettPDA.Height + this.PunPAD.w_ZoomPDA.Top - 5 , 0)
        this.PunPAD.w_DettPDA.visible = .F.
        this.PunPAD.w_DettPDA.zOrder(1)     
        this.PunPAD.Resize()     
        this.PunPAD.w_ZoomPDA.Resize()     
      case this.Azione="SHOWDETT"
        if this.Azione=="SHOWDETTC" and this.oParentObject.w_ZoomEspanso
          i_retcode = 'stop'
          return
        endif
        this.PunPAD.LockScreen = .T.
        if this.oParentObject.w_ZoomEspanso
          this.oParentObject.w_ZoomEspanso = .f.
          this.PunPAD.w_DettPDA.visible = .T.
          this.PunPAD.Resize()     
          this.PunPAD.w_ZoomPDA.cHeight = max(this.PunPAD.w_DettPDA.TOP - this.PunPAD.w_ZoomPDA.Top +5, 0)
          this.PunPAD.w_ZoomPDA.Resize()     
          this.PunPAD.w_ZoomPDA.GRD.Resize()     
        endif
        * --- Carica Cursore dettaglio
        this.Page_4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.PunPAD.LockScreen = .F.
      case this.Azione = "SELEZIONE"
        * --- All'evento 'w_zoompda selected' oppure 'ActivatePage 2' - Se click su prima colonna, non fa niente
        if this.oParentObject.w_COLSEL > 2
          * --- Valorizza correttamente le variabili sulla maschera
          this.oParentObject.w_SELART = this.PUNPAD.w_ZoomPDA.GetVar("CODART")
          * --- Legge dati da tabella dei periodi
          this.oParentObject.w_PSELA = right("000"+alltrim(str(this.oParentObject.w_ColSel-this.PrimaColPer,3,0)),3)
          * --- Read from PDA_TPER
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PDA_TPER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PDA_TPER_idx,2],.t.,this.PDA_TPER_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TPPERREL,TPDATINI,TPDATFIN"+;
              " from "+i_cTable+" PDA_TPER where ";
                  +"TPPERASS = "+cp_ToStrODBC(this.oParentObject.w_PSELA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TPPERREL,TPDATINI,TPDATFIN;
              from (i_cTable) where;
                  TPPERASS = this.oParentObject.w_PSELA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_PSELR = NVL(cp_ToDate(_read_.TPPERREL),cp_NullValue(_read_.TPPERREL))
            this.oParentObject.w_DIP = NVL(cp_ToDate(_read_.TPDATINI),cp_NullValue(_read_.TPDATINI))
            this.oParentObject.w_DFP = NVL(cp_ToDate(_read_.TPDATFIN),cp_NullValue(_read_.TPDATFIN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Assegna Descrzione del tipo periodo
          this.oParentObject.w_TIPOPERIODO = ah_Msgformat("Giornaliero")
          do case
            case this.oParentObject.w_PSELR="SCAD"
              this.oParentObject.w_TIPOPERIODO = ah_Msgformat("Scaduto")
            case left(this.oParentObject.w_PSELR,1)="S" and alltrim(this.oParentObject.w_PSELR)#"SCAD"
              this.oParentObject.w_TIPOPERIODO = ah_Msgformat("Settimanale")
            case left(this.oParentObject.w_PSELR,1)="M"
              this.oParentObject.w_TIPOPERIODO = ah_Msgformat("Mensile")
            case left(this.oParentObject.w_PSELR,1)="T"
              this.oParentObject.w_TIPOPERIODO = ah_Msgformat("Trimestrale")
          endcase
          * --- Visualizza Zoom Elendo PDA periodo
          this.PunPAD.NotifyEvent("ListaPDA")     
          * --- Attiva la pagina 2 automaticamente
          this.oParentObject.oPgFrm.ActivePage = 2
        endif
      case this.Azione="VISUALIZZA_PDA" or this.Azione="ELIMINA_PDA" or this.Azione="CARICA_PDA" or this.Azione="MODIFICA_PDA"
        this.Page_5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.Azione = "MENU_LISTAPDA"
        * --- Definisce SHORTCUT
        store space(10) to Scelta
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if not empty( Scelta )
          this.Page_5()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.Azione = "VERIF_TEMP"
        this.Page_7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- SISTEMA  INTESTAZIONI ZOOM (DA LANCIARE SOLO ALLA INIT DELLA MASCHERA)
    this.w_MESS = "Impossibile accedere alla manutenzione PDA%0Archivio periodi vuoto%0� necessario lanciare la generazione PDA"
    if Type("PeriodiPDA(1)") $ "L"
      dimension PeriodiPDA(100)
      vq_exec("query\GSAC_BPD", this, "_PeriodiPDA_")
      if RECCOUNT("_PeriodiPDA_")=0
        ah_ErrorMsg(this.w_MESS)
        this.oParentObject.windowtype=0
        use in _PeriodiPDA_
        i_retcode = 'stop'
        return
      endif
      Cur=WrCursor("_PeriodiPDA_")
      select * from _PeriodiPDA_ into array PeriodiPDA
      use in _PeriodiPDA_
    endif
    * --- Sistema intestazioni, font e larghezza celle
    this.TmpC = "iif(thisform.w_zoomPDA.grd.activerow=recno(thisform.w_zoomPDA.grd."
    this.TmpC = this.TmpC+"recordsource),rgb(255,255,0),rgb(255,255,255))"
    this.GRID.fontsize = this.oParentObject.w_DimFont
    this.GRID.Column1.DynamicBackColor = this.TmpC
    this.GRID.Column1.width = 145
    this.GRID.Column58.width = 0
    this.GRID.Column58.visible = .f.
    * --- Zoom dettaglio ...
    if this.w_GestionePDA
      this.TmpC = "iif(thisform.w_DettPDA.grd.activerow=recno(thisform.w_DettPDA.grd."
      this.TmpC = this.TmpC+"recordsource),rgb(255,255,0),rgb(255,255,255))"
      this.DETT.fontsize = this.oParentObject.w_DimFont
      this.DETT.Column1.DynamicBackColor = this.TmpC
      this.DETT.Column1.movable = .f.
      this.DETT.column1.hdr.forecolor = RGB(128,0,0)
      this.DETT.column1.hdr.fontbold = .T.
      this.DETT.Column1.width = 145
      this.DETT.Column2.movable = .f.
      this.DETT.rowheight = 15
    endif
    this.i = this.PrimaColPer
    do while this.i <= this.NumMaxPeriodi
      j= alltrim(str(this.i,3,0))
      * --- Determina indice vettore
      this.TmpC = right("000"+alltrim(str(this.i-this.PrimaColPer,2,0)),3)
      this.TmpN = int( Val(this.TmpC) + 1 )
      this.TmpC1 = nvl( PeriodiPDA( this.TmpN , 2 ) , "XXXX")
      * --- Periodo relativo
      this.TmpD = cp_ToDate( PeriodiPDA( this.TmpN , 3 ) )
      * --- Data inizio periodo
      this.TmpD1 = cp_ToDate( PeriodiPDA( this.TmpN , 5 ) )
      * --- Primo giorno lavorativo
      if this.i-this.PrimaColPer<=this.oParentObject.w_NumPer
        this.GRID.Column&j..DynamicBackColor = "mod(COL_"+this.TmpC+",16777216)"
        this.GRID.Column&j..DynamicForeColor = "int(COL_"+this.TmpC+"/16777216)"
        this.GRID.column&j..hdr.forecolor = iif(empty(this.TmpD1), this.oParentObject.w_PPCOLFES, RGB(0,0,0))
        this.GRID.Column&j..width = this.oParentObject.w_LargCol
        this.GRID.Column&j..hdr.caption = iif(this.i=this.PrimaColPer,ah_Msgformat("Scaduto"),this.TmpC1+iif(left(this.TmpC1,1)="G"," ("+left(dtoc(this.TmpD),5)+")","-"+str(year(this.TmpD),4,0)))
        if this.w_GestionePDA
          this.DETT.Column&j..width = this.oParentObject.w_LargCol
          this.DETT.Column&j..hdr.caption = iif(this.i=this.PrimaColPer,ah_Msgformat("Scaduto"),this.TmpC1+iif(left(this.TmpC1,1)="G"," ("+left(dtoc(this.TmpD),5)+")","-"+str(year(this.TmpD),4,0)))
          this.DETT.column&j..hdr.forecolor = iif(empty(this.TmpD1), this.oParentObject.w_PPCOLFES, RGB(0,0,0))
        endif
      else
        this.GRID.Column&j..visible = .f.
        this.GRID.Column&j..width = 1
        if this.w_GestionePDA
          this.DETT.Column&j..visible = .f.
          this.DETT.Column&j..width = 1
        endif
      endif
      this.GRID.Column&j..movable = .f.
      if this.w_GestionePDA
        this.DETT.Column&j..movable = .f.
      endif
      this.i = this.i + 1
    enddo
    * --- Memorizza Altezza iniziale zoom
    this.oParentObject.w_Zoom1Height = this.GRID.height
    * --- Prepara zoom dettaglio gia' splittato
    if this.w_GestionePDA
      this.DETT.ScrollBars = 0
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.k = rgb(255,255,255)
    * --- Colore BIANCO
    * --- Aggiorna cursore zoom per PDA
    this.w_NewCursMast = this.cCursor
    this.cCursMast = SYS(2015)
    SELECT * FROM (this.w_NewCursMast) INTO CURSOR (this.cCursMast) WHERE 1=0 READWRITE
    if Type("PeriodiPDA(1)") $ "L"
      dimension PeriodiPDA(100)
      vq_exec("query\GSAC_BPD", this, "_PeriodiPDA_")
      Cur=WrCursor("_PeriodiPDA_")
      select * from _PeriodiPDA_ into array PeriodiPDA
      use in _PeriodiPDA_
    endif
    * --- Crea array dei colori
    dimension Colore(8)
    Colore(1) = this.k
    * --- Colore BIANCO: cella vuota
    Colore(2) = this.oParentObject.w_PPCOLSUG
    * --- Colore SUGGERITO
    Colore(3) = this.oParentObject.w_PPCOLCON
    * --- Colore CONFERMATO
    Colore(4) = this.oParentObject.w_PPCOLMIS
    * --- Colore MISTO
    Colore(5) = this.oParentObject.w_PPCOLPIA
    * --- Colore DA ORDINARE
    Colore(6) = this.oParentObject.w_PPCOLMIS
    * --- Colore MISTO
    Colore(7) = this.oParentObject.w_PPCOLMIS
    * --- Colore MISTO
    Colore(8) = this.oParentObject.w_PPCOLMIS
    * --- Colore MISTO
    * --- Esegue query per creazione struttura cursore (vuoto) .....
    this.f_TIPGES = iif(this.oParentObject.w_fTIPGES="X",space(1),this.oParentObject.w_fTIPGES)
    ah_Msg("Preparazione dati PDA in corso...",.T.)
    vq_exec("query\GSAC2BPD", this, "_PDA_")
    if used("_PDA_")
      * --- Mette sfondo bianco
      for this.i=0 to this.oParentObject.w_NumPer
      j = right("000"+alltrim(str(this.i,3,0)),3)
      m.COL_&j = 0
      next
      * --- Riempie cursore zoom
      Select _PDA_
      SCAN
      * --- Legge e scrive su cursore
      SCATTER MEMVAR
      * --- Mette sfondo bianco su tutte le variabili
      for this.i=0 to this.oParentObject.w_NumPer
      j = right("000"+alltrim(str(this.i,3,0)),3)
      if Type("m.TOT_&j")="U" or m.TOT_&j =0
        m.COL_&j = this.k
      else
        m.COL_&j = Colore(m.COL_&j +m.COL1_&j +m.COL2_&j +1)
      endif
      next
      * --- --
      m.ARCODART = m.CODART
      * --- --
      Select (this.cCursMast)
      append BLANK
      GATHER MEMVAR
      ENDSCAN
      * --- Chiude cursore query
      Use in _PDA_
      * --- Refresh ...
      Select (this.cCursMast)
      go top
      * --- Rinfresca grid
      wait CLEAR
      this.GRID.RecordSource = this.cCursMast
      this.PunPAD.w_ZoomPDA.cCursor = this.cCursMast
      this.GRID.Refresh()     
    endif
    wait CLEAR
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna cursore zoom dettaglio
    this.w_NewCursDett = this.cCursDETT
    this.cCursDETT = SYS(2015)
    SELECT * FROM (this.w_NewCursDett) INTO CURSOR (this.cCursDETT) WHERE 1=0 READWRITE
    select (this.cCursDett)
    this.oParentObject.w_SELART = this.PUNPAD.w_ZoomPDA.GetVar("CODART")
    this.w_KEYSAL = this.oParentObject.w_SELART
    this.z = 7
    * --- Numero di righe del dettaglio
    * --- Sistema Intestazione:
    this.DETT.column1.hdr.caption = iif(empty(this.oParentObject.w_SELART)," ",ah_Msgformat("Dett.: %1",this.oParentObject.w_SELART) )
    if empty(this.oParentObject.w_SELART)
      i_retcode = 'stop'
      return
    endif
    * --- Legge Dato Giacenza Fisica ...
    this.w_ONHAND = 0
    vq_exec("query\GSAC3BPD", this, "_TempGiac_")
    * --- Memorizza Giacenza
    this.w_ONHAND = _TempGiac_.QTASAL
    * --- Chiude Cursore
    use in _TempGiac_
    ah_Msg("Lettura dati per visualizzazione dettaglio in corso...",.T.)
    * --- Crea Cursori
    * --- Cursore delle PDA
    vq_exec("query\GSAC5BPD", this, "_TempPDA_")
    * --- Cursore con gli ORDINI CLIENTE e FORNITORE da documenti
    * --- Per i movimenti di magazzino, la data di prevista evasione corrisponde alla data di elaborazione
    *     delle PDA
    * --- Read from PDA_TPER
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PDA_TPER_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PDA_TPER_idx,2],.t.,this.PDA_TPER_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TPDATINI"+;
        " from "+i_cTable+" PDA_TPER where ";
            +"TPPERASS = "+cp_ToStrODBC("001");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TPDATINI;
        from (i_cTable) where;
            TPPERASS = "001";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_EVAMAG = NVL(cp_ToDate(_read_.TPDATINI),cp_NullValue(_read_.TPDATINI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_EVAMAG = CP_TODATE( this.w_EVAMAG )
    * --- La variabile w_EVAMAG � utilizzata come filtro per fare la join tra MVM_MAST e
    *     e PDA_TPER nelle query GSAC6MBPD e GSAC7MBPD
    vq_exec("query\GSAC6BPD", this, "_TempDocu_")
    if g_PROD="S"
      * --- Cursore con gli ORDINI CLIENTE e FORNITORE da Materiali ODL
      vq_exec("..\COLA\EXE\query\GSCO9BPD", this, "_TempODL_")
    else
      * --- Quey "Fittizia"
      vq_exec("query\GSAC9BPD", this, "_TempODL_")
    endif
    * --- Unisce Cursori
    select PERASS,UNIMIS, ORDCLI AS FBFABLOR, ORDFOR, ORDFOR*0 AS QTASUGGE, ;
    ORDFOR*0 AS QTADAORD, ORDFOR*0 AS QTATOTAL from _TempDocu_ ;
    union ALL ;
    select PERASS,UNIMIS,QTASUGGE*0 AS FBFABLOR, QTASUGGE*0 AS ORDFOR, QTASUGGE, ;
    QTADAORD, QTATOTAL from _TempPDA_ ;
    union ALL ;
    select PERASS,UNIMIS, ORDCLI AS FBFABLOR, ORDFOR, ORDFOR*0 AS QTASUGGE, ;
    ORDFOR*0 AS QTADAORD, ORDFOR*0 AS QTATOTAL from _TempODL_ ;
    order by 1 into cursor _Temp_ nofilter
    select PERASS, max(UNIMIS), sum(FBFABLOR), sum(ORDFOR), sum(QTASUGGE), ;
    sum(QTADAORD), sum(QTATOTAL) from _Temp_ ;
    group by PERASS order by PERASS into array Base
    this.cNRecSel = _TALLY
    * --- Chiude Cursori
    if used("_TempDocu_")
      USE IN _TempDocu_
    endif
    if used("_TempPDA_")
      USE IN _TempPDA_
    endif
    if used("_TempODL_")
      USE IN _TempODL_
    endif
    if used("_TempImpe_")
      USE IN _TempImpe_
    endif
    if used("_TempOrdi_")
      USE IN _TempOrdi_
    endif
    if used("_Temp_")
      USE IN _Temp_
    endif
    if this.cNRecSel > 0
      * --- Array per PAB
      dimension Dettaglio(this.z,this.oParentObject.w_NumPer+this.PrimaColPer+1)
      for this.i=1 to this.z
      for this.k=this.PrimaColPer to this.oParentObject.w_NumPer+this.PrimaColPer+1
      Dettaglio(this.i,this.k) = 0
      next
      next
      * --- Prepara array ...
      dimension Descrizioni(this.z)
      Descrizioni(1)=ah_Msgformat("Giacenza fisica")
      Descrizioni(2)=ah_Msgformat("Fabbisogno lordo")
      Descrizioni(3)=ah_Msgformat("Ordini fornitore")
      Descrizioni(4)=ah_Msgformat("PDA suggerite")
      Descrizioni(5)=ah_Msgformat("PDA confermate")
      Descrizioni(6)=ah_Msgformat("PDA TOTALI")
      Descrizioni(7)=ah_Msgformat("PAB (Disponibilit� nel tempo)")
      * --- Cicla sulle righe
      this.TmpN = alen(Base,1)
      this.i = 2
      * --- Cerca unit� di misura
      do while empty(Base(1,2)) and this.i<=this.TmpN
        Base(1,2)=Base(this.i,2)
        this.i = this.i + 1
      enddo
      for this.i=1 to this.z-1
      * --- Descrizione e UM
      Dettaglio(this.i,1) = Descrizioni(this.i)
      Dettaglio(this.i,2) = Base(1,2)
      if this.i=1
        * --- Caso particolare, la giacenza
        Dettaglio(this.i,this.PrimaColPer) = this.w_ONHAND
      else
        * --- Cicla su tutti i periodi presenti, per ogni singola voce
        for this.k=1 to this.TmpN
        * --- Legge il periodo assoluto al quale il dato fa riferimento ...
        this.tCURPER = Base(this.k,1)
        * --- Determina indice della colonna sul quale deve essere posizionato il dato ...
        this.y = int(val(this.tCurPer)) + this.PrimaColPer
        * --- Aggiorna vettore
        Dettaglio(this.i,this.y) = Base(this.k,this.i+1)
        next
      endif
      next
      * --- Determina PAB
      Dettaglio(this.z,1) = Descrizioni(this.z)
      Dettaglio(this.z,3) = Base(1,2)
      this.y = this.PrimaColPer
      Dettaglio(this.z,this.PrimaColPer) = this.w_ONHAND + Dettaglio(6,this.y) + Dettaglio(3,this.y) - Dettaglio(2,this.y)
      for this.y=this.PrimaColPer+1 to this.PrimaColPer+this.oParentObject.w_NumPer
      Dettaglio(this.z,this.y) = Dettaglio(this.z,this.y-1) + Dettaglio(6,this.y) + Dettaglio(3,this.y) - Dettaglio(2,this.y)
      next
      * --- Riempie cursore zoom ...
      select (this.cCursDett)
      append from array Dettaglio
      go top
      * --- Rilascia vettori ...
      release Dettaglio,Base,Descrizioni
    endif
    * --- Rinfresca grid
    wait CLEAR
    this.PUNPAD.w_DettPDA.GRD.RecordSource = this.cCursDett
    this.PUNPAD.w_DettPDA.cCursor = this.cCursDett
    this.PUNPAD.w_DettPDA.Refresh()
    USE IN SELECT(this.w_NewCursDett)
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carica gestione PDA
    * --- Variabili locali
    * --- Legge PDA selezionato
    this.oParentObject.w_PDASEL = this.PUNPAD.w_PeriodoPDA.GetVar("PDSERIAL")
    this.oParentObject.w_PDANUM = this.PUNPAD.w_PeriodoPDA.GetVar("PDROWNUM")
    * --- Definisce e chiama gestione
    this.GestPDA = GSAC_APD()
    * --- Controllo se ha passato il test di accesso
    if !(this.GestPDA.bSec1)
      i_retcode = 'stop'
      return
    endif
    do case
      case Scelta = "CARICA_PDA"
        * --- Lancia Caricamento ...
        this.GestPDA.ECPLoad()     
        * --- Default
        this.tCODART = this.PunPAD.w_sCODART
        this.tCODVAR = this.PunPAD.w_sCODVAR
        this.tCODVAP = iif(empty(this.tCODVAR),"#",this.tCODVAR)
        this.tCODMAG = g_MAGAZI
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARDESART"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.tCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARUNMIS1,AROPERAT,ARMOLTIP,ARUNMIS2,ARDESART;
            from (i_cTable) where;
                ARCODART = this.tCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.tUNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          this.tOPERAT = NVL(cp_ToDate(_read_.AROPERAT),cp_NullValue(_read_.AROPERAT))
          this.tMOLTIP = NVL(cp_ToDate(_read_.ARMOLTIP),cp_NullValue(_read_.ARMOLTIP))
          this.tUNMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
          this.tDESART = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from PAR_RIOR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2],.t.,this.PAR_RIOR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PRGIOAPP,PRQTAMIN,PRLOTRIO,PRCODFOR"+;
            " from "+i_cTable+" PAR_RIOR where ";
                +"PRCODART = "+cp_ToStrODBC(this.tCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PRGIOAPP,PRQTAMIN,PRLOTRIO,PRCODFOR;
            from (i_cTable) where;
                PRCODART = this.tCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.tGIOAPP = NVL(cp_ToDate(_read_.PRGIOAPP),cp_NullValue(_read_.PRGIOAPP))
          this.tQTAMIN = NVL(cp_ToDate(_read_.PRQTAMIN),cp_NullValue(_read_.PRQTAMIN))
          this.tLOTRIO = NVL(cp_ToDate(_read_.PRLOTRIO),cp_NullValue(_read_.PRLOTRIO))
          this.tCODCON = NVL(cp_ToDate(_read_.PRCODFOR),cp_NullValue(_read_.PRCODFOR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Cerca il codice di ricerca
        if not empty(this.tCODCON)
          * --- Cerco il codice del fornitore
          * --- Read from KEY_ARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CAUNIMIS,CAOPERAT,CAMOLTIP,CACODICE"+;
              " from "+i_cTable+" KEY_ARTI where ";
                  +"CACODART = "+cp_ToStrODBC(this.tCODART);
                  +" and CATIPCON = "+cp_ToStrODBC("F");
                  +" and CACODCON = "+cp_ToStrODBC(this.tCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CAUNIMIS,CAOPERAT,CAMOLTIP,CACODICE;
              from (i_cTable) where;
                  CACODART = this.tCODART;
                  and CATIPCON = "F";
                  and CACODCON = this.tCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.tUNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
            this.tOPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
            this.tMOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
            this.tCODICE = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.GestPDA.w_COCONA = this.tCODCON
        endif
        if empty(this.tCODICE)
          * --- Cerco il codice principale
          * --- Read from KEY_ARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CAUNIMIS,CAOPERAT,CAMOLTIP,CACODICE"+;
              " from "+i_cTable+" KEY_ARTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.tCODART);
                  +" and CACODART = "+cp_ToStrODBC(this.tCODART);
                  +" and CATIPCON = "+cp_ToStrODBC("R");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CAUNIMIS,CAOPERAT,CAMOLTIP,CACODICE;
              from (i_cTable) where;
                  CACODICE = this.tCODART;
                  and CACODART = this.tCODART;
                  and CATIPCON = "R";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.tUNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
            this.tOPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
            this.tMOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
            this.tCODICE = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if empty(this.tCODICE)
          * --- Read from KEY_ARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CAUNIMIS,CAOPERAT,CAMOLTIP,CACODICE"+;
              " from "+i_cTable+" KEY_ARTI where ";
                  +"CACODART = "+cp_ToStrODBC(this.tCODART);
                  +" and CATIPCON = "+cp_ToStrODBC("R");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CAUNIMIS,CAOPERAT,CAMOLTIP,CACODICE;
              from (i_cTable) where;
                  CACODART = this.tCODART;
                  and CATIPCON = "R";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.tUNMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
            this.tOPERA3 = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
            this.tMOLTI3 = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
            this.tCODICE = NVL(cp_ToDate(_read_.CACODICE),cp_NullValue(_read_.CACODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Read from MAGAZZIN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MGDESMAG"+;
            " from "+i_cTable+" MAGAZZIN where ";
                +"MGCODMAG = "+cp_ToStrODBC(this.tCODMAG);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MGDESMAG;
            from (i_cTable) where;
                MGCODMAG = this.tCODMAG;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.tDESMAG = NVL(cp_ToDate(_read_.MGDESMAG),cp_NullValue(_read_.MGDESMAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.GestPDA.w_PDSTATUS = "D"
        this.GestPDA.w_PDCODART = this.tCODART
        this.GestPDA.w_DESART = this.tDESART
        this.GestPDA.w_CODART = this.tCODART
        this.GestPDA.w_UNMIS1 = this.tUNMIS1
        this.GestPDA.w_UNMIS2 = this.tUNMIS2
        this.GestPDA.w_UNMIS3 = this.tUNMIS3
        this.GestPDA.w_OPERAT = this.tOPERAT
        this.GestPDA.w_OPERA3 = this.tOPERA3
        this.GestPDA.w_MOLTIP = this.tMOLTIP
        this.GestPDA.w_MOLTI3 = this.tMOLTI3
        this.GestPDA.w_GIOAPP = this.tGIOAPP
        this.GestPDA.w_QTAMIN = this.tQTAMIN
        this.GestPDA.w_LOTRIO = this.tLOTRIO
        this.GestPDA.w_PDTIPCON = "F"
        this.GestPDA.w_CODCON = this.tCODCON
        this.GestPDA.w_PDUMORDI = IIF(NOT EMPTY(this.tUNMIS3) AND this.tMOLTI3<>0, this.tUNMIS3, this.tUNMIS1)
        this.GestPDA.w_PDUNIMIS = this.tUNMIS1
        this.GestPDA.w_PDCODMAG = this.tCODMAG
        this.tDATEVA = iif(this.PunPAD.w_DIP - this.tGIOAPP < i_DATSYS, cp_CharToDate("  -  -  "), this.PunPAD.w_DIP - this.tGIOAPP)
        this.GestPDA.w_PDDATORD = this.tDATEVA
        this.GestPDA.w_PDDATEVA = iif(empty(this.tDATEVA), this.tDATEVA, this.tDATEVA + this.tGIOAPP)
        this.GestPDA.w_PDROWNUM = 1
        this.GestPDA.w_PDCODICE = this.tCODICE
        this.GestPDA.w_PDPERASS = this.PunPAD.w_PSELA
        this.GestPDA.w_DESMAG = this.tDESMAG
        this.GestPDA.mCalc(.T.)     
      case Scelta="VISUALIZZA_PDA" or Scelta="MODIFICA_PDA" or Scelta="ELIMINA_PDA"
        * --- Carica record e lascia in interrograzione ...
        this.GestPDA.w_PDSERIAL = this.oParentObject.w_PDASEL
        this.GestPDA.w_PDROWNUM = this.oParentObject.w_PDANUM
        this.TMPc = "PDSERIAL="+cp_ToStrODBC(this.oParentObject.w_PDASEL)+" and PDROWNUM="+cp_ToStrODBC(this.oParentObject.w_PDANUM)
        this.GestPDA.QueryKeySet(this.TmpC,"")     
        this.GestPDA.LoadRec()     
        do case
          case Scelta = "MODIFICA_PDA"
            * --- Modifica record
            this.GestPDA.ECPEdit()     
          case Scelta = "ELIMINA_PDA"
            * --- Cancella record ...
            this.GestPDA.ECPDelete()     
        endcase
    endcase
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Definisce Menu
    DEFINE POPUP MENUPDA from MRow()+1,MCol()+1 shortcut margin
    DEFINE BAR 1 OF MENUPDA prompt ah_Msgformat("Visualizza")
    DEFINE BAR 2 OF MENUPDA prompt ah_Msgformat("Modifica")
    DEFINE BAR 3 OF MENUPDA prompt ah_Msgformat("Carica")
    DEFINE BAR 4 OF MENUPDA prompt "\-"
    DEFINE BAR 5 OF MENUPDA prompt ah_Msgformat("Elimina")
    ON SELE BAR 1 OF MENUPDA Scelta="VISUALIZZA_PDA"
    ON SELE BAR 2 OF MENUPDA Scelta="MODIFICA_PDA"
    ON SELE BAR 3 OF MENUPDA Scelta="CARICA_PDA"
    ON SELE BAR 5 OF MENUPDA Scelta="ELIMINA_PDA"
    ACTI POPUP MENUPDA
    DEACTIVATE POPUP MENUPDA
    RELEASE POPUPS MENUPDA EXTENDED
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica Temporale
    * --- PDA in ritardo
    vq_exec("query\GSAC8BPD", this, "_Ritardo_")
    scan
    this.w_RCODART = CODART
    PeriodoAss = PERASS
    Select (this.cCursor)
    Locate for CODART=this.w_RCODART
    if found()
      replace COL_&PeriodoAss with this.oParentObject.w_PPCOLMCO
    endif
    Endscan
    if used("_Ritardo_")
      use in _Ritardo_
    endif
    Select (this.cCursor)
    go top
    this.GRID.Refresh()     
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,Azione)
    this.Azione=Azione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,6)]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='PDA_TPER'
    this.cWorkTables[3]='PAR_PROD'
    this.cWorkTables[4]='ART_ICOL'
    this.cWorkTables[5]='PAR_RIOR'
    this.cWorkTables[6]='MAGAZZIN'
    return(this.OpenAllTables(6))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Azione"
endproc
