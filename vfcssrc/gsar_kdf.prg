* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kdf                                                        *
*              Duplicazione festivitÓ                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-07                                                      *
* Last revis.: 2011-05-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kdf",oParentObject))

* --- Class definition
define class tgsar_kdf as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 575
  Height = 148
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-05-03"
  HelpContextID=82409321
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  FES_MAST_IDX = 0
  cPrg = "gsar_kdf"
  cComment = "Duplicazione festivitÓ"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_COD_ORIG = space(3)
  w_FEDESCRI = space(35)
  w_COD_DEST = space(3)
  w_DSDESCRI = space(35)
  w_ANNO = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kdfPag1","gsar_kdf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCOD_ORIG_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='FES_MAST'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_COD_ORIG=space(3)
      .w_FEDESCRI=space(35)
      .w_COD_DEST=space(3)
      .w_DSDESCRI=space(35)
      .w_ANNO=0
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_COD_ORIG))
          .link_1_1('Full')
        endif
          .DoRTCalc(2,4,.f.)
        .w_ANNO = year(i_DATSYS)+1
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,5,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COD_ORIG
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FES_MAST_IDX,3]
    i_lTable = "FES_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FES_MAST_IDX,2], .t., this.FES_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FES_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COD_ORIG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'FES_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FECODICE like "+cp_ToStrODBC(trim(this.w_COD_ORIG)+"%");

          i_ret=cp_SQL(i_nConn,"select FECODICE,FEDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FECODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FECODICE',trim(this.w_COD_ORIG))
          select FECODICE,FEDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FECODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COD_ORIG)==trim(_Link_.FECODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COD_ORIG) and !this.bDontReportError
            deferred_cp_zoom('FES_MAST','*','FECODICE',cp_AbsName(oSource.parent,'oCOD_ORIG_1_1'),i_cWhere,'',"FestivitÓ",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FECODICE,FEDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FECODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FECODICE',oSource.xKey(1))
            select FECODICE,FEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COD_ORIG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FECODICE,FEDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FECODICE="+cp_ToStrODBC(this.w_COD_ORIG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FECODICE',this.w_COD_ORIG)
            select FECODICE,FEDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COD_ORIG = NVL(_Link_.FECODICE,space(3))
      this.w_FEDESCRI = NVL(_Link_.FEDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_COD_ORIG = space(3)
      endif
      this.w_FEDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FES_MAST_IDX,2])+'\'+cp_ToStr(_Link_.FECODICE,1)
      cp_ShowWarn(i_cKey,this.FES_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COD_ORIG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOD_ORIG_1_1.value==this.w_COD_ORIG)
      this.oPgFrm.Page1.oPag.oCOD_ORIG_1_1.value=this.w_COD_ORIG
    endif
    if not(this.oPgFrm.Page1.oPag.oFEDESCRI_1_2.value==this.w_FEDESCRI)
      this.oPgFrm.Page1.oPag.oFEDESCRI_1_2.value=this.w_FEDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCOD_DEST_1_4.value==this.w_COD_DEST)
      this.oPgFrm.Page1.oPag.oCOD_DEST_1_4.value=this.w_COD_DEST
    endif
    if not(this.oPgFrm.Page1.oPag.oDSDESCRI_1_5.value==this.w_DSDESCRI)
      this.oPgFrm.Page1.oPag.oDSDESCRI_1_5.value=this.w_DSDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_8.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_8.value=this.w_ANNO
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_ANNO>1990)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_kdfPag1 as StdContainer
  Width  = 571
  height = 148
  stdWidth  = 571
  stdheight = 148
  resizeXpos=520
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOD_ORIG_1_1 as StdField with uid="NARJTRFDHP",rtseq=1,rtrep=.f.,;
    cFormVar = "w_COD_ORIG", cQueryName = "COD_ORIG",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 227926931,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=243, Top=10, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="FES_MAST", oKey_1_1="FECODICE", oKey_1_2="this.w_COD_ORIG"

  func oCOD_ORIG_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOD_ORIG_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOD_ORIG_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FES_MAST','*','FECODICE',cp_AbsName(this.parent,'oCOD_ORIG_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"FestivitÓ",'',this.parent.oContained
  endproc

  add object oFEDESCRI_1_2 as StdField with uid="SODKAWLBFO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_FEDESCRI", cQueryName = "FEDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 59773599,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=302, Top=10, InputMask=replicate('X',35)

  add object oCOD_DEST_1_4 as StdField with uid="UTJROVLCSO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_COD_DEST", cQueryName = "COD_DEST",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice nuovo calendario festivitÓ da creare",;
    HelpContextID = 79305850,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=243, Top=37, InputMask=replicate('X',3)

  add object oDSDESCRI_1_5 as StdField with uid="FKSLDZPSNC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DSDESCRI", cQueryName = "DSDESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 59777151,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=302, Top=37, InputMask=replicate('X',35)

  add object oANNO_1_8 as StdField with uid="FFZDYIEWMA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Anno solare per il quale si vuole generare il canlendario",;
    HelpContextID = 76891386,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=243, Top=64, cSayPict='"@K 9999"', cGetPict='"@K 9999"'

  func oANNO_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ANNO>1990)
    endwith
    return bRes
  endfunc


  add object oBtn_1_9 as StdButton with uid="ZMIZZNTUDJ",left=460, top=91, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 241181206;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      with this.Parent.oContained
        do GSAR_BDU with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_9.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_COD_ORIG) AND !EMPTY(.w_COD_DEST))
      endwith
    endif
  endfunc


  add object oBtn_1_10 as StdButton with uid="JWIBHYOYEX",left=512, top=91, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 75091898;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_10.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_3 as StdString with uid="HJVDYZEKJR",Visible=.t., Left=8, Top=12,;
    Alignment=1, Width=231, Height=18,;
    Caption="Codice festivitÓ d'origine:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="VJXTYQQYHS",Visible=.t., Left=8, Top=38,;
    Alignment=1, Width=231, Height=18,;
    Caption="Codice festivitÓ da creare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="UUINRNBKEV",Visible=.t., Left=8, Top=64,;
    Alignment=1, Width=231, Height=18,;
    Caption="Anno solare festivitÓ da creare:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kdf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
