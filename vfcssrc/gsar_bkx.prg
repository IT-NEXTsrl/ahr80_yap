* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bkx                                                        *
*              Controlli documenti confermati                                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_161]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-08-09                                                      *
* Last revis.: 2006-03-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOpe
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bkx",oParentObject,m.pTipOpe)
return(i_retval)

define class tgsar_bkx as StdBatch
  * --- Local variables
  pTipOpe = space(1)
  w_OK = .f.
  w_MESS = space(100)
  w_CFUNC = space(10)
  w_PROG = .NULL.
  w_TIPDOC = space(2)
  w_FLVEAC = space(1)
  * --- WorkFile variables
  ATTIDETT_idx=0
  AZIENDA_idx=0
  CON_INDI_idx=0
  DIS_TINT_idx=0
  DOC_MAST_idx=0
  INC_CORR_idx=0
  PAR_TITE_idx=0
  PNT_MAST_idx=0
  PRI_MAST_idx=0
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Finali documenti Confermati (da GSVE_ADC) (in A.M. Check Form e Evento: Delete Init)
    if this.oParentObject.w_Origine = "D"
      * --- Solo se da Documenti Confermati
      if this.pTipOpe="O"
        * --- Legge Documento di Origine
        gsar_bzm(this,this.oParentObject.w_SCRIFDCO,-20)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        this.w_OK = .T.
        this.w_CFUNC = this.oParentObject.cFunction
        * --- Controlla se il Documento e' stato Generato 
        if this.w_CFUNC<>"Load" AND NOT EMPTY(NVL(this.oParentObject.w_SCRIFDCO,""))
          this.w_OK = ah_YesNo("Registrazione generata da conferma documenti%0Proseguo ugualmente?")
          this.w_MESS = "Operazione abbandonata"
          * --- Se Cancellazione, Elimina il Riferimento all' Eventuale Distinta o Documento
          if this.w_OK AND this.w_CFUNC="Query"
            * --- Try
            local bErr_039B7B78
            bErr_039B7B78=bTrsErr
            this.Try_039B7B78()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_039B7B78
            * --- End
          endif
          if NOT this.w_OK 
            * --- Abbandona la Transazione
            if this.w_CFUNC="Query"
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg=ah_MsgFormat(this.w_MESS)
            else
              ah_ErrorMsg(this.w_MESS,,"")
              this.oParentObject.w_RESCHK = -1
            endif
          endif
        endif
      endif
    endif
  endproc
  proc Try_039B7B78()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    ah_Msg("Annullo conferma documento collegato...",.T.)
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVRIFDCO ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_MAST','MVRIFDCO');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SCRIFDCO);
             )
    else
      update (i_cTable) set;
          MVRIFDCO = SPACE(10);
          &i_ccchkf. ;
       where;
          MVSERIAL = this.oParentObject.w_SCRIFDCO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    WAIT CLEAR
    return


  proc Init(oParentObject,pTipOpe)
    this.pTipOpe=pTipOpe
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='ATTIDETT'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='CON_INDI'
    this.cWorkTables[4]='DIS_TINT'
    this.cWorkTables[5]='DOC_MAST'
    this.cWorkTables[6]='INC_CORR'
    this.cWorkTables[7]='PAR_TITE'
    this.cWorkTables[8]='PNT_MAST'
    this.cWorkTables[9]='PRI_MAST'
    this.cWorkTables[10]='VALUTE'
    return(this.OpenAllTables(10))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOpe"
endproc
