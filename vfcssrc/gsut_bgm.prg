* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bgm                                                        *
*              Visualizza mappe                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-10-01                                                      *
* Last revis.: 2011-06-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pImpVis,pARCHIVIO,pCODCON,pSEDE,pINDIRIZZO,pCAP,pLOCALITA,pDESNAZ,pTOINDIRIZZO,pTOCAP,pTOLOCALITA,pTODESNAZ
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bgm",oParentObject,m.pImpVis,m.pARCHIVIO,m.pCODCON,m.pSEDE,m.pINDIRIZZO,m.pCAP,m.pLOCALITA,m.pDESNAZ,m.pTOINDIRIZZO,m.pTOCAP,m.pTOLOCALITA,m.pTODESNAZ)
return(i_retval)

define class tgsut_bgm as StdBatch
  * --- Local variables
  w_FLVEAC = space(1)
  w_DPCOGNOM = space(50)
  w_DPNOME = space(50)
  pImpVis = space(1)
  pARCHIVIO = space(1)
  pCODCON = space(15)
  pSEDE = space(5)
  pINDIRIZZO = space(254)
  pCAP = space(9)
  pLOCALITA = space(254)
  pDESNAZ = space(254)
  pTOINDIRIZZO = space(254)
  pTOCAP = space(9)
  pTOLOCALITA = space(254)
  pTODESNAZ = space(254)
  w_cLink = space(255)
  w_cParms = space(10)
  w_ABSPATH = space(255)
  w_TmpPat = space(10)
  w_SHELLEXEC = 0
  w_ANNAZION = space(50)
  w_CODAZI = space(5)
  w_TOOLTIPGESTIONE = space(254)
  w_GESTIONECHIAMANTE = space(10)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_SEDEINTESTATARIO = space(5)
  w_SEDEAZIENDA = space(5)
  w_INDIRIZZOAZIENDA = space(254)
  w_CAPAZIENDA = space(9)
  w_LOCALITAAZIENDA = space(254)
  w_DESNAZAZIENDA = space(254)
  w_INDIRIZZOGESTIONE = space(254)
  w_CAPGESTIONE = space(9)
  w_LOCALITAGESTIONE = space(254)
  w_DESNAZGESTIONE = space(254)
  w_TOOLTIPAZIENDA = space(254)
  w_TOOLTIP = space(254)
  w_TOTOOLTIP = space(254)
  w_ADDR = space(10)
  w_ADDR2 = space(10)
  * --- WorkFile variables
  CONTI_idx=0
  NAZIONI_idx=0
  AZIENDA_idx=0
  DES_DIVE_idx=0
  OFF_NOMI_idx=0
  DOC_MAST_idx=0
  SEDIAZIE_idx=0
  MVM_MAST_idx=0
  AGENTI_idx=0
  VETTORI_idx=0
  PNT_MAST_idx=0
  MAGAZZIN_idx=0
  ANAGRAFE_idx=0
  BAN_CHE_idx=0
  DIPENDEN_idx=0
  CAN_TIER_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- --
    * --- Le impostazioni di visualizzazione possono essere
    *     pImpVis = 'M': mappa
    *     pImpVis = 'E': earth con zoom
    *     pImpVis = 'S': satellite con zoom
    *     pImpVis = 'P': percorso
    * --- --
    this.w_CODAZI = i_CODAZI
    * --- Gestione chiamante
    this.w_GESTIONECHIAMANTE = "ALTRO"
    * --- Assegna i valori provenienti dai parametri
    this.w_TIPCON = IIF( EMPTY( this.pARCHIVIO ), " ", this.pARCHIVIO )
    this.w_CODCON = IIF( EMPTY( this.pCODCON ), SPACE( 15 ), this.pCODCON )
    this.w_SEDEINTESTATARIO = IIF( EMPTY( this.pSEDE ), SPACE( 5 ), this.pSEDE )
    this.Pag2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.pImpVis = "P" OR this.pARCHIVIO $ "DATIAZIENDA-DOCUMENTISENZAINTESTATARIO-MOVIMENTISENZAINTESTATARIO-PRIMANOTASENZAINTESTATARIO-SEDIAZIENDA"
      * --- pIMPVIS = "P" indica che si � scelto di visualizzare un percorso
      * --- w_SEDEAZIENDA � letto dalla testata del documento. Se � presente viene utilizzato
      *     come codice della sede.
      if EMPTY( this.w_SEDEAZIENDA )
        * --- Read from AZIENDA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AZINDAZI,AZLOCAZI,AZCAPAZI,AZCODNAZ"+;
            " from "+i_cTable+" AZIENDA where ";
                +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AZINDAZI,AZLOCAZI,AZCAPAZI,AZCODNAZ;
            from (i_cTable) where;
                AZCODAZI = this.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INDIRIZZOAZIENDA = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
          this.w_LOCALITAAZIENDA = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
          this.w_CAPAZIENDA = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
          this.w_ANNAZION = NVL(cp_ToDate(_read_.AZCODNAZ),cp_NullValue(_read_.AZCODNAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Cerca nelle sedi dell'azienda il flag predefinito. Se lo trova utilizza di dati della sede,
        *     altrimenti rimangono validi quelli dell'azienda
        * --- Select from SEDIAZIE
        i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2],.t.,this.SEDIAZIE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" SEDIAZIE ";
              +" where SECODAZI = "+cp_ToStrODBC(this.w_CODAZI)+" AND SEPREMAP = 'S'";
               ,"_Curs_SEDIAZIE")
        else
          select * from (i_cTable);
           where SECODAZI = this.w_CODAZI AND SEPREMAP = "S";
            into cursor _Curs_SEDIAZIE
        endif
        if used('_Curs_SEDIAZIE')
          select _Curs_SEDIAZIE
          locate for 1=1
          do while not(eof())
          this.w_INDIRIZZOAZIENDA = _Curs_SEDIAZIE.SEINDIRI
          this.w_CAPAZIENDA = _Curs_SEDIAZIE.SE___CAP
          this.w_LOCALITAAZIENDA = _Curs_SEDIAZIE.SELOCALI
          this.w_TOOLTIPAZIENDA = _Curs_SEDIAZIE.SENOMSED
            select _Curs_SEDIAZIE
            continue
          enddo
          use
        endif
      else
        * --- Read from SEDIAZIE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2],.t.,this.SEDIAZIE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SEINDIRI,SE___CAP,SELOCALI"+;
            " from "+i_cTable+" SEDIAZIE where ";
                +"SECODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                +" and SECODDES = "+cp_ToStrODBC(this.w_SEDEAZIENDA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SEINDIRI,SE___CAP,SELOCALI;
            from (i_cTable) where;
                SECODAZI = this.w_CODAZI;
                and SECODDES = this.w_SEDEAZIENDA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INDIRIZZOAZIENDA = NVL(cp_ToDate(_read_.SEINDIRI),cp_NullValue(_read_.SEINDIRI))
          this.w_CAPAZIENDA = NVL(cp_ToDate(_read_.SE___CAP),cp_NullValue(_read_.SE___CAP))
          this.w_LOCALITAAZIENDA = NVL(cp_ToDate(_read_.SELOCALI),cp_NullValue(_read_.SELOCALI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Legge descrizione nazione
      * --- Read from NAZIONI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.NAZIONI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "NADESNAZ"+;
          " from "+i_cTable+" NAZIONI where ";
              +"NACODNAZ = "+cp_ToStrODBC(this.w_ANNAZION);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          NADESNAZ;
          from (i_cTable) where;
              NACODNAZ = this.w_ANNAZION;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DESNAZAZIENDA = NVL(cp_ToDate(_read_.NADESNAZ),cp_NullValue(_read_.NADESNAZ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Lancia eseguibile associato al file
    * --- Se l'indirizzo � vuoto e passo la chiave cerco i dati nell'anagrafica
    if EMPTY(this.pINDIRIZZO) AND NOT EMPTY(this.pCODCON)
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if EMPTY(this.w_INDIRIZZOGESTIONE)
      CP_ERRORMSG("Indirizzo non trovato",64)
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.pTOINDIRIZZO)
      if this.pImpVis="P"
        * --- Se si � scelto di visualizzare un percorso, nel caso di cliente, nominativo o documento di vendita 
        *     l'indirizzo di partenza � quello dei dati azienda e il dato della gestione � quello di arrivo
        if this.w_GESTIONECHIAMANTE $ "CLIENTI-NOMINATIVI-DOCUMENTIACLIENTE-MOVIMENTIACLIENTE"
          * --- Indirizzo di partenza: dati azienda
          this.pINDIRIZZO = this.w_INDIRIZZOAZIENDA
          this.pCAP = this.w_CAPAZIENDA
          this.pLOCALITA = this.w_LOCALITAAZIENDA
          this.pDESNAZ = this.w_DESNAZAZIENDA
          * --- Indirizzo di destinazione: dati della gestione
          this.pTOINDIRIZZO = this.w_INDIRIZZOGESTIONE
          this.pTOCAP = this.w_CAPGESTIONE
          this.pTOLOCALITA = this.w_LOCALITAGESTIONE
          this.pTODESNAZ = this.w_DESNAZGESTIONE
        else
          * --- Indirizzo di partenza: dati della gestione
          this.pINDIRIZZO = this.w_INDIRIZZOGESTIONE
          this.pCAP = this.w_CAPGESTIONE
          this.pLOCALITA = this.w_LOCALITAGESTIONE
          this.pDESNAZ = this.w_DESNAZGESTIONE
          * --- Indirizzo di destinazione: dati azienda
          this.pTOINDIRIZZO = this.w_INDIRIZZOAZIENDA
          this.pTOCAP = this.w_CAPAZIENDA
          this.pTOLOCALITA = this.w_LOCALITAAZIENDA
          this.pTODESNAZ = this.w_DESNAZAZIENDA
        endif
      else
        * --- Indirizzo: dati della gestione
        this.pINDIRIZZO = this.w_INDIRIZZOGESTIONE
        this.pCAP = this.w_CAPGESTIONE
        this.pLOCALITA = this.w_LOCALITAGESTIONE
        this.pDESNAZ = this.w_DESNAZGESTIONE
      endif
    endif
    * --- Calcola gli indirizzi
    if NOT EMPTY( this.pINDIRIZZO )
      this.w_ADDR = ALLTRIM( this.pINDIRIZZO )
    endif
    if NOT EMPTY(this.pCAP)
      if NOT EMPTY( this.w_ADDR )
        this.w_ADDR = this.w_ADDR + "+"
      endif
      this.w_ADDR = this.w_ADDR + ALLTRIM( this.pCAP )
    endif
    if NOT EMPTY(this.pLOCALITA)
      if NOT EMPTY( this.w_ADDR )
        this.w_ADDR = this.w_ADDR + "+"
      endif
      this.w_ADDR = this.w_ADDR + ALLTRIM( this.pLOCALITA )
    endif
    if NOT EMPTY(this.pDESNAZ)
      if NOT EMPTY( this.w_ADDR )
        this.w_ADDR = this.w_ADDR + "+"
      endif
      this.w_ADDR = this.w_ADDR + ALLTRIM( this.pDESNAZ )
    endif
    if NOT EMPTY( this.w_TOOLTIP )
      if NOT EMPTY( this.w_ADDR )
        this.w_ADDR = this.w_ADDR + "+"
      endif
      this.w_ADDR = this.w_ADDR + "(" + ALLTRIM( this.w_TOOLTIP ) + ")"
    endif
    if NOT EMPTY( this.pTOINDIRIZZO )
      this.w_ADDR2 = ALLTRIM( this.pTOINDIRIZZO )
    endif
    if NOT EMPTY(this.pTOCAP)
      if NOT EMPTY( this.w_ADDR2 )
        this.w_ADDR2 = this.w_ADDR2 + "+"
      endif
      this.w_ADDR2 = this.w_ADDR2 + ALLTRIM( this.pTOCAP )
    endif
    if NOT EMPTY( this.pTOLOCALITA )
      if NOT EMPTY( this.w_ADDR2 )
        this.w_ADDR2 = this.w_ADDR2 + "+"
      endif
      this.w_ADDR2 = this.w_ADDR2 + ALLTRIM( this.pTOLOCALITA )
    endif
    if NOT EMPTY( this.pTODESNAZ )
      if NOT EMPTY( this.w_ADDR2 )
        this.w_ADDR2 = this.w_ADDR2 + "+"
      endif
      this.w_ADDR2 = this.w_ADDR2 + ALLTRIM( this.pTODESNAZ )
    endif
    if NOT EMPTY( this.w_TOTOOLTIP )
      if NOT EMPTY( this.w_ADDR2 )
        this.w_ADDR2 = this.w_ADDR2 + "+"
      endif
      this.w_ADDR2 = this.w_ADDR2 + "(" + ALLTRIM( this.w_TOTOOLTIP ) + ")"
    endif
    * --- Converto eventuale percorso relativo in assoluto
    this.w_TmpPat = "http://maps.google.com/maps?"
    if this.pImpVis="P"
      this.w_TmpPat = this.w_TmpPat + "saddr=" + this.w_ADDR + "&daddr=" + this.w_ADDR2
    else
      this.w_TmpPat = this.w_TmpPat + "q=" + this.w_ADDR
    endif
    this.w_TmpPat = CHRTRAN(ALLTRIM(this.w_TmpPat), "++", "+")
    do case
      case this.pImpVis="E"
        * --- earth
        this.w_TmpPat = this.w_TmpPat + "&t=e&z=20"
      case this.pImpVis="K"
        * --- satellite
        this.w_TmpPat = this.w_TmpPat + "&t=k&z=20"
    endcase
    this.w_cLink = iif(RIGHT(this.w_TmpPat,1)=",", LEFT(this.w_TmpPat, LEN(this.w_TmpPat)-1), this.w_TmpPat)
    this.w_cParms = space(1)
    declare integer ShellExecute in shell32.dll integer nHwnd, string cOperation, string cFileName, string cParms, string cDir, integer nShowCmd
     declare integer FindWindow in win32api string cNull, string cWinName 
    w_hWnd = FindWindow(0,_screen.caption)
    this.w_SHELLEXEC = ShellExecute(w_hWnd,"open",alltrim(this.w_cLink),alltrim(this.w_cParms),sys(5)+curdir(),1)
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pARCHIVIO="DOCUMENTI"
        * --- Lanciato dai documenti
        this.w_FLVEAC = "V"
        * --- Con questa read assegna i valori a w_TIPCON, w_CODCON e w_SEDEINTESTATARIO
        if g_APPLICATION="ADHOC REVOLUTION"
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVFLVEAC,MVTIPCON,MVCODCON,MVCODDES,MVCODSED"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.pCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVFLVEAC,MVTIPCON,MVCODCON,MVCODDES,MVCODSED;
              from (i_cTable) where;
                  MVSERIAL = this.pCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
            this.w_TIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
            this.w_CODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
            this.w_SEDEINTESTATARIO = NVL(cp_ToDate(_read_.MVCODDES),cp_NullValue(_read_.MVCODDES))
            this.w_SEDEAZIENDA = NVL(cp_ToDate(_read_.MVCODSED),cp_NullValue(_read_.MVCODSED))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVFLVEAC,MVTIPCON,MVCODCON,MVCODDES,MVSEDDES"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.pCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVFLVEAC,MVTIPCON,MVCODCON,MVCODDES,MVSEDDES;
              from (i_cTable) where;
                  MVSERIAL = this.pCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLVEAC = NVL(cp_ToDate(_read_.MVFLVEAC),cp_NullValue(_read_.MVFLVEAC))
            this.w_TIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
            this.w_CODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
            this.w_SEDEINTESTATARIO = NVL(cp_ToDate(_read_.MVCODDES),cp_NullValue(_read_.MVCODDES))
            this.w_SEDEAZIENDA = NVL(cp_ToDate(_read_.MVSEDDES),cp_NullValue(_read_.MVSEDDES))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        this.w_TIPCON = NVL( this.w_TIPCON, " " )
        this.w_CODCON = NVL( this.w_CODCON, SPACE( 15 ) )
        this.w_SEDEAZIENDA = NVL( this.w_SEDEAZIENDA, SPACE( 5 ) )
        if EMPTY( this.w_CODCON )
          this.w_GESTIONECHIAMANTE = "DOCUMENTISENZAINTESTATARIO"
          this.w_INDIRIZZOGESTIONE = this.w_INDIRIZZOAZIENDA
          this.w_CAPGESTIONE = this.w_CAPAZIENDA
          this.w_LOCALITAGESTIONE = this.w_LOCALITAAZIENDA
          this.w_DESNAZGESTIONE = this.w_DESNAZAZIENDA
        else
          if this.w_FLVEAC = "V"
            this.w_GESTIONECHIAMANTE = "DOCUMENTIACLIENTE"
            * --- Se valorizzata leggo indirizzo sede
            if NOT EMPTY(this.w_SEDEINTESTATARIO)
              * --- Read from DES_DIVE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DES_DIVE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DDLOCALI,DD___CAP,DDINDIRI,DDCODNAZ,DDNOMDES"+;
                  " from "+i_cTable+" DES_DIVE where ";
                      +"DDTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                      +" and DDCODICE = "+cp_ToStrODBC(this.w_CODCON);
                      +" and DDCODDES = "+cp_ToStrODBC(this.w_SEDEINTESTATARIO);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DDLOCALI,DD___CAP,DDINDIRI,DDCODNAZ,DDNOMDES;
                  from (i_cTable) where;
                      DDTIPCON = this.w_TIPCON;
                      and DDCODICE = this.w_CODCON;
                      and DDCODDES = this.w_SEDEINTESTATARIO;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.DDLOCALI),cp_NullValue(_read_.DDLOCALI))
                this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.DD___CAP),cp_NullValue(_read_.DD___CAP))
                this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.DDINDIRI),cp_NullValue(_read_.DDINDIRI))
                this.w_ANNAZION = NVL(cp_ToDate(_read_.DDCODNAZ),cp_NullValue(_read_.DDCODNAZ))
                this.w_TOOLTIPGESTIONE = NVL(cp_ToDate(_read_.DDNOMDES),cp_NullValue(_read_.DDNOMDES))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            else
              * --- Leggo indirizzo cliente o fornitore
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANLOCALI,AN___CAP,ANINDIRI,ANNAZION,ANDESCRI"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANLOCALI,AN___CAP,ANINDIRI,ANNAZION,ANDESCRI;
                  from (i_cTable) where;
                      ANTIPCON = this.w_TIPCON;
                      and ANCODICE = this.w_CODCON;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
                this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
                this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
                this.w_ANNAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
                this.w_TOOLTIPGESTIONE = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            * --- Leggo descrizione nazione
            * --- Read from NAZIONI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.NAZIONI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "NADESNAZ"+;
                " from "+i_cTable+" NAZIONI where ";
                    +"NACODNAZ = "+cp_ToStrODBC(this.w_ANNAZION);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                NADESNAZ;
                from (i_cTable) where;
                    NACODNAZ = this.w_ANNAZION;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESNAZGESTIONE = NVL(cp_ToDate(_read_.NADESNAZ),cp_NullValue(_read_.NADESNAZ))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            this.w_GESTIONECHIAMANTE = "DOCUMENTIDAFORNITORE"
            * --- Se valorizzata leggo indirizzo sede
            if NOT EMPTY(this.w_SEDEINTESTATARIO)
              * --- Read from DES_DIVE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DES_DIVE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DDLOCALI,DD___CAP,DDINDIRI,DDCODNAZ"+;
                  " from "+i_cTable+" DES_DIVE where ";
                      +"DDTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                      +" and DDCODICE = "+cp_ToStrODBC(this.w_CODCON);
                      +" and DDCODDES = "+cp_ToStrODBC(this.w_SEDEINTESTATARIO);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DDLOCALI,DD___CAP,DDINDIRI,DDCODNAZ;
                  from (i_cTable) where;
                      DDTIPCON = this.w_TIPCON;
                      and DDCODICE = this.w_CODCON;
                      and DDCODDES = this.w_SEDEINTESTATARIO;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.DDLOCALI),cp_NullValue(_read_.DDLOCALI))
                this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.DD___CAP),cp_NullValue(_read_.DD___CAP))
                this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.DDINDIRI),cp_NullValue(_read_.DDINDIRI))
                this.w_ANNAZION = NVL(cp_ToDate(_read_.DDCODNAZ),cp_NullValue(_read_.DDCODNAZ))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            else
              * --- Leggo indirizzo cliente o fornitore
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANLOCALI,AN___CAP,ANINDIRI,ANNAZION"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANLOCALI,AN___CAP,ANINDIRI,ANNAZION;
                  from (i_cTable) where;
                      ANTIPCON = this.w_TIPCON;
                      and ANCODICE = this.w_CODCON;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
                this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
                this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
                this.w_ANNAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            * --- Leggo descrizione nazione
            * --- Read from NAZIONI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.NAZIONI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "NADESNAZ"+;
                " from "+i_cTable+" NAZIONI where ";
                    +"NACODNAZ = "+cp_ToStrODBC(this.w_ANNAZION);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                NADESNAZ;
                from (i_cTable) where;
                    NACODNAZ = this.w_ANNAZION;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESNAZGESTIONE = NVL(cp_ToDate(_read_.NADESNAZ),cp_NullValue(_read_.NADESNAZ))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        endif
      case this.pARCHIVIO="MOVIMENTIMAGAZZINO"
        * --- Lanciato dai movimenti di magazzino
        * --- Con questa read assegna i valori a w_TIPCON e w_CODCON
        * --- Read from MVM_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MVM_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MVM_MAST_idx,2],.t.,this.MVM_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MMTIPCON,MMCODCON"+;
            " from "+i_cTable+" MVM_MAST where ";
                +"MMSERIAL = "+cp_ToStrODBC(this.pCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MMTIPCON,MMCODCON;
            from (i_cTable) where;
                MMSERIAL = this.pCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPCON = NVL(cp_ToDate(_read_.MMTIPCON),cp_NullValue(_read_.MMTIPCON))
          this.w_CODCON = NVL(cp_ToDate(_read_.MMCODCON),cp_NullValue(_read_.MMCODCON))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_TIPCON = NVL( this.w_TIPCON, " " )
        this.w_CODCON = NVL( this.w_CODCON, SPACE( 15 ) )
        this.w_SEDEINTESTATARIO = SPACE( 5 )
        if EMPTY( this.w_CODCON )
          this.w_GESTIONECHIAMANTE = "MOVIMENTISENZAINTESTATARIO"
          this.w_INDIRIZZOGESTIONE = this.w_INDIRIZZOAZIENDA
          this.w_CAPGESTIONE = this.w_CAPAZIENDA
          this.w_LOCALITAGESTIONE = this.w_LOCALITAAZIENDA
          this.w_DESNAZGESTIONE = this.w_DESNAZAZIENDA
        else
          if this.w_TIPCON = "C"
            this.w_GESTIONECHIAMANTE = "MOVIMENTIACLIENTE"
            * --- Se valorizzata leggo indirizzo sede
            if NOT EMPTY(this.w_SEDEINTESTATARIO)
              * --- Read from DES_DIVE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DES_DIVE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DDLOCALI,DD___CAP,DDINDIRI,DDCODNAZ"+;
                  " from "+i_cTable+" DES_DIVE where ";
                      +"DDTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                      +" and DDCODICE = "+cp_ToStrODBC(this.w_CODCON);
                      +" and DDCODDES = "+cp_ToStrODBC(this.w_SEDEINTESTATARIO);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DDLOCALI,DD___CAP,DDINDIRI,DDCODNAZ;
                  from (i_cTable) where;
                      DDTIPCON = this.w_TIPCON;
                      and DDCODICE = this.w_CODCON;
                      and DDCODDES = this.w_SEDEINTESTATARIO;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.DDLOCALI),cp_NullValue(_read_.DDLOCALI))
                this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.DD___CAP),cp_NullValue(_read_.DD___CAP))
                this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.DDINDIRI),cp_NullValue(_read_.DDINDIRI))
                this.w_ANNAZION = NVL(cp_ToDate(_read_.DDCODNAZ),cp_NullValue(_read_.DDCODNAZ))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            else
              * --- Leggo indirizzo cliente o fornitore
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANLOCALI,AN___CAP,ANINDIRI,ANNAZION"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANLOCALI,AN___CAP,ANINDIRI,ANNAZION;
                  from (i_cTable) where;
                      ANTIPCON = this.w_TIPCON;
                      and ANCODICE = this.w_CODCON;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
                this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
                this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
                this.w_ANNAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            * --- Leggo descrizione nazione
            * --- Read from NAZIONI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.NAZIONI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "NADESNAZ"+;
                " from "+i_cTable+" NAZIONI where ";
                    +"NACODNAZ = "+cp_ToStrODBC(this.w_ANNAZION);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                NADESNAZ;
                from (i_cTable) where;
                    NACODNAZ = this.w_ANNAZION;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESNAZGESTIONE = NVL(cp_ToDate(_read_.NADESNAZ),cp_NullValue(_read_.NADESNAZ))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            this.w_GESTIONECHIAMANTE = "MOVIMENTIDAFORNITORE"
            * --- Se valorizzata leggo indirizzo sede
            if NOT EMPTY(this.w_SEDEINTESTATARIO)
              * --- Read from DES_DIVE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DES_DIVE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DDLOCALI,DD___CAP,DDINDIRI,DDCODNAZ"+;
                  " from "+i_cTable+" DES_DIVE where ";
                      +"DDTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                      +" and DDCODICE = "+cp_ToStrODBC(this.w_CODCON);
                      +" and DDCODDES = "+cp_ToStrODBC(this.w_SEDEINTESTATARIO);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DDLOCALI,DD___CAP,DDINDIRI,DDCODNAZ;
                  from (i_cTable) where;
                      DDTIPCON = this.w_TIPCON;
                      and DDCODICE = this.w_CODCON;
                      and DDCODDES = this.w_SEDEINTESTATARIO;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.DDLOCALI),cp_NullValue(_read_.DDLOCALI))
                this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.DD___CAP),cp_NullValue(_read_.DD___CAP))
                this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.DDINDIRI),cp_NullValue(_read_.DDINDIRI))
                this.w_ANNAZION = NVL(cp_ToDate(_read_.DDCODNAZ),cp_NullValue(_read_.DDCODNAZ))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            else
              * --- Leggo indirizzo cliente o fornitore
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANLOCALI,AN___CAP,ANINDIRI,ANNAZION"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANLOCALI,AN___CAP,ANINDIRI,ANNAZION;
                  from (i_cTable) where;
                      ANTIPCON = this.w_TIPCON;
                      and ANCODICE = this.w_CODCON;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
                this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
                this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
                this.w_ANNAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            * --- Leggo descrizione nazione
            * --- Read from NAZIONI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.NAZIONI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "NADESNAZ"+;
                " from "+i_cTable+" NAZIONI where ";
                    +"NACODNAZ = "+cp_ToStrODBC(this.w_ANNAZION);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                NADESNAZ;
                from (i_cTable) where;
                    NACODNAZ = this.w_ANNAZION;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESNAZGESTIONE = NVL(cp_ToDate(_read_.NADESNAZ),cp_NullValue(_read_.NADESNAZ))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        endif
      case this.pARCHIVIO="OFFERTE"
        * --- Lanciato da un'offerta
        this.w_GESTIONECHIAMANTE = "OFFERTE"
        * --- La lettura del nominativo viene fatta in un altro batch per non aprire la tabella OFF_ERTE
        GSOF_BG2(this, this.pCODCON )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Read from OFF_NOMI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NOINDIRI,NOLOCALI,NO___CAP,NONAZION"+;
            " from "+i_cTable+" OFF_NOMI where ";
                +"NOCODICE = "+cp_ToStrODBC(this.w_CODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NOINDIRI,NOLOCALI,NO___CAP,NONAZION;
            from (i_cTable) where;
                NOCODICE = this.w_CODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.NOINDIRI),cp_NullValue(_read_.NOINDIRI))
          this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.NOLOCALI),cp_NullValue(_read_.NOLOCALI))
          this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.NO___CAP),cp_NullValue(_read_.NO___CAP))
          this.w_ANNAZION = NVL(cp_ToDate(_read_.NONAZION),cp_NullValue(_read_.NONAZION))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Leggo descrizione nazione
        * --- Read from NAZIONI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.NAZIONI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NADESNAZ"+;
            " from "+i_cTable+" NAZIONI where ";
                +"NACODNAZ = "+cp_ToStrODBC(this.w_ANNAZION);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NADESNAZ;
            from (i_cTable) where;
                NACODNAZ = this.w_ANNAZION;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESNAZGESTIONE = NVL(cp_ToDate(_read_.NADESNAZ),cp_NullValue(_read_.NADESNAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      case this.pARCHIVIO="DATIAZIENDA"
        * --- Lanciato dalla gestione dei dati azienda
        this.w_GESTIONECHIAMANTE = "DATIAZIENDA"
        this.w_INDIRIZZOGESTIONE = this.w_INDIRIZZOAZIENDA
        this.w_CAPGESTIONE = this.w_CAPAZIENDA
        this.w_LOCALITAGESTIONE = this.w_LOCALITAAZIENDA
        this.w_DESNAZGESTIONE = this.w_DESNAZAZIENDA
      case this.pARCHIVIO="NOMINATIVI"
        * --- Lanciato dai nominativi
        this.w_GESTIONECHIAMANTE = "NOMINATIVI"
        * --- Read from OFF_NOMI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NOINDIRI,NOLOCALI,NO___CAP,NONAZION"+;
            " from "+i_cTable+" OFF_NOMI where ";
                +"NOCODICE = "+cp_ToStrODBC(this.w_CODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NOINDIRI,NOLOCALI,NO___CAP,NONAZION;
            from (i_cTable) where;
                NOCODICE = this.w_CODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.NOINDIRI),cp_NullValue(_read_.NOINDIRI))
          this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.NOLOCALI),cp_NullValue(_read_.NOLOCALI))
          this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.NO___CAP),cp_NullValue(_read_.NO___CAP))
          this.w_ANNAZION = NVL(cp_ToDate(_read_.NONAZION),cp_NullValue(_read_.NONAZION))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Leggo descrizione nazione
        * --- Read from NAZIONI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.NAZIONI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NADESNAZ"+;
            " from "+i_cTable+" NAZIONI where ";
                +"NACODNAZ = "+cp_ToStrODBC(this.w_ANNAZION);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NADESNAZ;
            from (i_cTable) where;
                NACODNAZ = this.w_ANNAZION;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESNAZGESTIONE = NVL(cp_ToDate(_read_.NADESNAZ),cp_NullValue(_read_.NADESNAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      case this.pARCHIVIO="CLIENTI"
        * --- Lanciato dai clienti
        this.w_GESTIONECHIAMANTE = "CLIENTI"
        * --- Se valorizzata leggo indirizzo sede
        if NOT EMPTY(this.w_SEDEINTESTATARIO)
          * --- Read from DES_DIVE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DES_DIVE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DDLOCALI,DD___CAP,DDINDIRI,DDCODNAZ"+;
              " from "+i_cTable+" DES_DIVE where ";
                  +"DDTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                  +" and DDCODICE = "+cp_ToStrODBC(this.w_CODCON);
                  +" and DDCODDES = "+cp_ToStrODBC(this.w_SEDEINTESTATARIO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DDLOCALI,DD___CAP,DDINDIRI,DDCODNAZ;
              from (i_cTable) where;
                  DDTIPCON = this.w_TIPCON;
                  and DDCODICE = this.w_CODCON;
                  and DDCODDES = this.w_SEDEINTESTATARIO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.DDLOCALI),cp_NullValue(_read_.DDLOCALI))
            this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.DD___CAP),cp_NullValue(_read_.DD___CAP))
            this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.DDINDIRI),cp_NullValue(_read_.DDINDIRI))
            this.w_ANNAZION = NVL(cp_ToDate(_read_.DDCODNAZ),cp_NullValue(_read_.DDCODNAZ))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Leggo indirizzo cliente o fornitore
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANLOCALI,AN___CAP,ANINDIRI,ANNAZION"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANLOCALI,AN___CAP,ANINDIRI,ANNAZION;
              from (i_cTable) where;
                  ANTIPCON = this.w_TIPCON;
                  and ANCODICE = this.w_CODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
            this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
            this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
            this.w_ANNAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Leggo descrizione nazione
        * --- Read from NAZIONI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.NAZIONI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NADESNAZ"+;
            " from "+i_cTable+" NAZIONI where ";
                +"NACODNAZ = "+cp_ToStrODBC(this.w_ANNAZION);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NADESNAZ;
            from (i_cTable) where;
                NACODNAZ = this.w_ANNAZION;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESNAZGESTIONE = NVL(cp_ToDate(_read_.NADESNAZ),cp_NullValue(_read_.NADESNAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      case this.pARCHIVIO="FORNITORI"
        * --- Lanciato dai fornitori
        this.w_GESTIONECHIAMANTE = "FORNITORI"
        * --- Se valorizzata leggo indirizzo sede
        if NOT EMPTY(this.w_SEDEINTESTATARIO)
          * --- Read from DES_DIVE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DES_DIVE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DDLOCALI,DD___CAP,DDINDIRI,DDCODNAZ"+;
              " from "+i_cTable+" DES_DIVE where ";
                  +"DDTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                  +" and DDCODICE = "+cp_ToStrODBC(this.w_CODCON);
                  +" and DDCODDES = "+cp_ToStrODBC(this.w_SEDEINTESTATARIO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DDLOCALI,DD___CAP,DDINDIRI,DDCODNAZ;
              from (i_cTable) where;
                  DDTIPCON = this.w_TIPCON;
                  and DDCODICE = this.w_CODCON;
                  and DDCODDES = this.w_SEDEINTESTATARIO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.DDLOCALI),cp_NullValue(_read_.DDLOCALI))
            this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.DD___CAP),cp_NullValue(_read_.DD___CAP))
            this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.DDINDIRI),cp_NullValue(_read_.DDINDIRI))
            this.w_ANNAZION = NVL(cp_ToDate(_read_.DDCODNAZ),cp_NullValue(_read_.DDCODNAZ))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Leggo indirizzo cliente o fornitore
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANLOCALI,AN___CAP,ANINDIRI,ANNAZION"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANLOCALI,AN___CAP,ANINDIRI,ANNAZION;
              from (i_cTable) where;
                  ANTIPCON = this.w_TIPCON;
                  and ANCODICE = this.w_CODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
            this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
            this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
            this.w_ANNAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Leggo descrizione nazione
        * --- Read from NAZIONI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.NAZIONI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NADESNAZ"+;
            " from "+i_cTable+" NAZIONI where ";
                +"NACODNAZ = "+cp_ToStrODBC(this.w_ANNAZION);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NADESNAZ;
            from (i_cTable) where;
                NACODNAZ = this.w_ANNAZION;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESNAZGESTIONE = NVL(cp_ToDate(_read_.NADESNAZ),cp_NullValue(_read_.NADESNAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      case this.pARCHIVIO="AGENTI"
        * --- Lanciato dalla gestione degli agenti
        this.w_GESTIONECHIAMANTE = "AGENTI"
        * --- Read from AGENTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AGENTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AGINDAGE,AGCITAGE,AGCAPAGE"+;
            " from "+i_cTable+" AGENTI where ";
                +"AGCODAGE = "+cp_ToStrODBC(this.w_CODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AGINDAGE,AGCITAGE,AGCAPAGE;
            from (i_cTable) where;
                AGCODAGE = this.w_CODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.AGINDAGE),cp_NullValue(_read_.AGINDAGE))
          this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.AGCITAGE),cp_NullValue(_read_.AGCITAGE))
          this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.AGCAPAGE),cp_NullValue(_read_.AGCAPAGE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DESNAZGESTIONE = ""
      case this.pARCHIVIO="VETTORI"
        * --- Lanciato dalla gestione dei vettori
        this.w_GESTIONECHIAMANTE = "VETTORI"
        * --- Read from VETTORI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VETTORI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VETTORI_idx,2],.t.,this.VETTORI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VTINDVET,VTLOCVET,VTCAPVET"+;
            " from "+i_cTable+" VETTORI where ";
                +"VTCODVET = "+cp_ToStrODBC(this.w_CODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VTINDVET,VTLOCVET,VTCAPVET;
            from (i_cTable) where;
                VTCODVET = this.w_CODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.VTINDVET),cp_NullValue(_read_.VTINDVET))
          this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.VTLOCVET),cp_NullValue(_read_.VTLOCVET))
          this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.VTCAPVET),cp_NullValue(_read_.VTCAPVET))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DESNAZGESTIONE = ""
      case this.pARCHIVIO="PRIMANOTA"
        * --- Lanciato dalla primanota
        this.w_GESTIONECHIAMANTE = "PRIMANOTA"
        * --- Read from PNT_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PNTIPCLF,PNCODCLF"+;
            " from "+i_cTable+" PNT_MAST where ";
                +"PNSERIAL = "+cp_ToStrODBC(this.pCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PNTIPCLF,PNCODCLF;
            from (i_cTable) where;
                PNSERIAL = this.pCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_TIPCON = NVL(cp_ToDate(_read_.PNTIPCLF),cp_NullValue(_read_.PNTIPCLF))
          this.w_CODCON = NVL(cp_ToDate(_read_.PNCODCLF),cp_NullValue(_read_.PNCODCLF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_TIPCON = NVL( this.w_TIPCON, " " )
        this.w_CODCON = NVL( this.w_CODCON, SPACE( 15 ) )
        this.w_SEDEINTESTATARIO = SPACE( 5 )
        if EMPTY( this.w_CODCON )
          this.w_GESTIONECHIAMANTE = "PRIMANOTASENZAINTESTATARIO"
          this.w_INDIRIZZOGESTIONE = this.w_INDIRIZZOAZIENDA
          if EMPTY(this.w_INDIRIZZOGESTIONE)
            CP_ERRORMSG("Indirizzo non trovato",64)
            i_retcode = 'stop'
            return
          endif
          this.w_CAPGESTIONE = this.w_CAPAZIENDA
          this.w_LOCALITAGESTIONE = this.w_LOCALITAAZIENDA
          this.w_DESNAZGESTIONE = this.w_DESNAZAZIENDA
        else
          if this.w_TIPCON = "C"
            this.w_GESTIONECHIAMANTE = "PRIMANOTAACLIENTE"
            * --- Se valorizzata leggo indirizzo sede
            if NOT EMPTY(this.w_SEDEINTESTATARIO)
              * --- Read from DES_DIVE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DES_DIVE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DDLOCALI,DD___CAP,DDINDIRI,DDCODNAZ"+;
                  " from "+i_cTable+" DES_DIVE where ";
                      +"DDTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                      +" and DDCODICE = "+cp_ToStrODBC(this.w_CODCON);
                      +" and DDCODDES = "+cp_ToStrODBC(this.w_SEDEINTESTATARIO);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DDLOCALI,DD___CAP,DDINDIRI,DDCODNAZ;
                  from (i_cTable) where;
                      DDTIPCON = this.w_TIPCON;
                      and DDCODICE = this.w_CODCON;
                      and DDCODDES = this.w_SEDEINTESTATARIO;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.DDLOCALI),cp_NullValue(_read_.DDLOCALI))
                this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.DD___CAP),cp_NullValue(_read_.DD___CAP))
                this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.DDINDIRI),cp_NullValue(_read_.DDINDIRI))
                this.w_ANNAZION = NVL(cp_ToDate(_read_.DDCODNAZ),cp_NullValue(_read_.DDCODNAZ))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            else
              * --- Leggo indirizzo cliente o fornitore
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANLOCALI,AN___CAP,ANINDIRI,ANNAZION"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANLOCALI,AN___CAP,ANINDIRI,ANNAZION;
                  from (i_cTable) where;
                      ANTIPCON = this.w_TIPCON;
                      and ANCODICE = this.w_CODCON;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
                this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
                this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
                this.w_ANNAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            * --- Leggo descrizione nazione
            * --- Read from NAZIONI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.NAZIONI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "NADESNAZ"+;
                " from "+i_cTable+" NAZIONI where ";
                    +"NACODNAZ = "+cp_ToStrODBC(this.w_ANNAZION);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                NADESNAZ;
                from (i_cTable) where;
                    NACODNAZ = this.w_ANNAZION;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESNAZGESTIONE = NVL(cp_ToDate(_read_.NADESNAZ),cp_NullValue(_read_.NADESNAZ))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            this.w_GESTIONECHIAMANTE = "PRIMANOTAAFORNITORE"
            * --- Se valorizzata leggo indirizzo sede
            if NOT EMPTY(this.w_SEDEINTESTATARIO)
              * --- Read from DES_DIVE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.DES_DIVE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "DDLOCALI,DD___CAP,DDINDIRI,DDCODNAZ"+;
                  " from "+i_cTable+" DES_DIVE where ";
                      +"DDTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                      +" and DDCODICE = "+cp_ToStrODBC(this.w_CODCON);
                      +" and DDCODDES = "+cp_ToStrODBC(this.w_SEDEINTESTATARIO);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  DDLOCALI,DD___CAP,DDINDIRI,DDCODNAZ;
                  from (i_cTable) where;
                      DDTIPCON = this.w_TIPCON;
                      and DDCODICE = this.w_CODCON;
                      and DDCODDES = this.w_SEDEINTESTATARIO;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.DDLOCALI),cp_NullValue(_read_.DDLOCALI))
                this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.DD___CAP),cp_NullValue(_read_.DD___CAP))
                this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.DDINDIRI),cp_NullValue(_read_.DDINDIRI))
                this.w_ANNAZION = NVL(cp_ToDate(_read_.DDCODNAZ),cp_NullValue(_read_.DDCODNAZ))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            else
              * --- Leggo indirizzo cliente o fornitore
              * --- Read from CONTI
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CONTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "ANLOCALI,AN___CAP,ANINDIRI,ANNAZION"+;
                  " from "+i_cTable+" CONTI where ";
                      +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                      +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  ANLOCALI,AN___CAP,ANINDIRI,ANNAZION;
                  from (i_cTable) where;
                      ANTIPCON = this.w_TIPCON;
                      and ANCODICE = this.w_CODCON;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
                this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
                this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
                this.w_ANNAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            * --- Leggo descrizione nazione
            * --- Read from NAZIONI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.NAZIONI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "NADESNAZ"+;
                " from "+i_cTable+" NAZIONI where ";
                    +"NACODNAZ = "+cp_ToStrODBC(this.w_ANNAZION);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                NADESNAZ;
                from (i_cTable) where;
                    NACODNAZ = this.w_ANNAZION;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESNAZGESTIONE = NVL(cp_ToDate(_read_.NADESNAZ),cp_NullValue(_read_.NADESNAZ))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        endif
      case this.pARCHIVIO="MAGAZZINO"
        * --- Lanciato dalla gestione del magazzino
        this.w_GESTIONECHIAMANTE = "MAGAZZINO"
        * --- Read from MAGAZZIN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2],.t.,this.MAGAZZIN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MGINDMAG,MGCITMAG,MGCAPMAG"+;
            " from "+i_cTable+" MAGAZZIN where ";
                +"MGCODMAG = "+cp_ToStrODBC(this.w_CODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MGINDMAG,MGCITMAG,MGCAPMAG;
            from (i_cTable) where;
                MGCODMAG = this.w_CODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.MGINDMAG),cp_NullValue(_read_.MGINDMAG))
          this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.MGCITMAG),cp_NullValue(_read_.MGCITMAG))
          this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.MGCAPMAG),cp_NullValue(_read_.MGCAPMAG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DESNAZGESTIONE = ""
      case this.pARCHIVIO="ANAGRAFE"
        * --- Lanciato dalla gestione del magazzino
        this.w_GESTIONECHIAMANTE = "ANAGRAFE"
        * --- Read from ANAGRAFE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ANAGRAFE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ANAGRAFE_idx,2],.t.,this.ANAGRAFE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANINDIRI,ANLOCALI,AN___CAP"+;
            " from "+i_cTable+" ANAGRAFE where ";
                +"ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANINDIRI,ANLOCALI,AN___CAP;
            from (i_cTable) where;
                ANCODICE = this.w_CODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
          this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
          this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DESNAZGESTIONE = ""
      case this.pARCHIVIO="BANCHE"
        * --- Lanciato dall'anagrafica delle banche
        this.w_GESTIONECHIAMANTE = "BANCHE"
        * --- Read from BAN_CHE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.BAN_CHE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.BAN_CHE_idx,2],.t.,this.BAN_CHE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "BAINDIRI,BALOCALI,BA___CAP"+;
            " from "+i_cTable+" BAN_CHE where ";
                +"BACODBAN = "+cp_ToStrODBC(this.w_CODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            BAINDIRI,BALOCALI,BA___CAP;
            from (i_cTable) where;
                BACODBAN = this.w_CODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.BAINDIRI),cp_NullValue(_read_.BAINDIRI))
          this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.BALOCALI),cp_NullValue(_read_.BALOCALI))
          this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.BA___CAP),cp_NullValue(_read_.BA___CAP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DESNAZGESTIONE = ""
      case this.pARCHIVIO="PERSONE"
        * --- Lanciato dall'anagrafica delle persone
        this.w_GESTIONECHIAMANTE = "PERSONE"
        * --- Read from DIPENDEN
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIPENDEN_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIPENDEN_idx,2],.t.,this.DIPENDEN_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DPRESVIA,DPRESLOC,DPRESCAP,DPCOGNOM,DPNOME,DPNAZION"+;
            " from "+i_cTable+" DIPENDEN where ";
                +"DPCODICE = "+cp_ToStrODBC(this.w_CODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DPRESVIA,DPRESLOC,DPRESCAP,DPCOGNOM,DPNOME,DPNAZION;
            from (i_cTable) where;
                DPCODICE = this.w_CODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.DPRESVIA),cp_NullValue(_read_.DPRESVIA))
          this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.DPRESLOC),cp_NullValue(_read_.DPRESLOC))
          this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.DPRESCAP),cp_NullValue(_read_.DPRESCAP))
          this.w_DPCOGNOM = NVL(cp_ToDate(_read_.DPCOGNOM),cp_NullValue(_read_.DPCOGNOM))
          this.w_DPNOME = NVL(cp_ToDate(_read_.DPNOME),cp_NullValue(_read_.DPNOME))
          this.w_ANNAZION = NVL(cp_ToDate(_read_.DPNAZION),cp_NullValue(_read_.DPNAZION))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Leggo descrizione nazione
        * --- Read from NAZIONI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.NAZIONI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NADESNAZ"+;
            " from "+i_cTable+" NAZIONI where ";
                +"NACODNAZ = "+cp_ToStrODBC(this.w_ANNAZION);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NADESNAZ;
            from (i_cTable) where;
                NACODNAZ = this.w_ANNAZION;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESNAZGESTIONE = NVL(cp_ToDate(_read_.NADESNAZ),cp_NullValue(_read_.NADESNAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      case this.pARCHIVIO="COMMESSE"
        * --- Lanciato dall'anagrafica delle commesse
        this.w_GESTIONECHIAMANTE = "COMMESSE"
        * --- Read from CAN_TIER
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAN_TIER_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CNLOCALI,CNINDIRI,CN___CAP"+;
            " from "+i_cTable+" CAN_TIER where ";
                +"CNCODCAN = "+cp_ToStrODBC(this.w_CODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CNLOCALI,CNINDIRI,CN___CAP;
            from (i_cTable) where;
                CNCODCAN = this.w_CODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.CNLOCALI),cp_NullValue(_read_.CNLOCALI))
          this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.CNINDIRI),cp_NullValue(_read_.CNINDIRI))
          this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.CN___CAP),cp_NullValue(_read_.CN___CAP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DESNAZGESTIONE = ""
      case this.pARCHIVIO="SEDIAZIENDA"
        * --- Lanciato dall'anagrafica delle sediazienda
        this.w_GESTIONECHIAMANTE = "SEDIAZIENDA"
        * --- Read from SEDIAZIE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.SEDIAZIE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SEDIAZIE_idx,2],.t.,this.SEDIAZIE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SELOCALI,SEINDIRI,SE___CAP"+;
            " from "+i_cTable+" SEDIAZIE where ";
                +"SECODAZI = "+cp_ToStrODBC(this.w_CODCON);
                +" and SECODDES = "+cp_ToStrODBC(this.pSEDE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SELOCALI,SEINDIRI,SE___CAP;
            from (i_cTable) where;
                SECODAZI = this.w_CODCON;
                and SECODDES = this.pSEDE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.SELOCALI),cp_NullValue(_read_.SELOCALI))
          this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.SEINDIRI),cp_NullValue(_read_.SEINDIRI))
          this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.SE___CAP),cp_NullValue(_read_.SE___CAP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DESNAZGESTIONE = ""
      otherwise
        * --- Se valorizzata leggo indirizzo sede
        if NOT EMPTY(this.w_SEDEINTESTATARIO)
          * --- Read from DES_DIVE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DES_DIVE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DDLOCALI,DD___CAP,DDINDIRI,DDCODNAZ"+;
              " from "+i_cTable+" DES_DIVE where ";
                  +"DDTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                  +" and DDCODICE = "+cp_ToStrODBC(this.w_CODCON);
                  +" and DDCODDES = "+cp_ToStrODBC(this.w_SEDEINTESTATARIO);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DDLOCALI,DD___CAP,DDINDIRI,DDCODNAZ;
              from (i_cTable) where;
                  DDTIPCON = this.w_TIPCON;
                  and DDCODICE = this.w_CODCON;
                  and DDCODDES = this.w_SEDEINTESTATARIO;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.DDLOCALI),cp_NullValue(_read_.DDLOCALI))
            this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.DD___CAP),cp_NullValue(_read_.DD___CAP))
            this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.DDINDIRI),cp_NullValue(_read_.DDINDIRI))
            this.w_ANNAZION = NVL(cp_ToDate(_read_.DDCODNAZ),cp_NullValue(_read_.DDCODNAZ))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Leggo indirizzo cliente o fornitore
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANLOCALI,AN___CAP,ANINDIRI,ANNAZION"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANLOCALI,AN___CAP,ANINDIRI,ANNAZION;
              from (i_cTable) where;
                  ANTIPCON = this.w_TIPCON;
                  and ANCODICE = this.w_CODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LOCALITAGESTIONE = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
            this.w_CAPGESTIONE = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
            this.w_INDIRIZZOGESTIONE = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
            this.w_ANNAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Leggo descrizione nazione
        * --- Read from NAZIONI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.NAZIONI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NAZIONI_idx,2],.t.,this.NAZIONI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NADESNAZ"+;
            " from "+i_cTable+" NAZIONI where ";
                +"NACODNAZ = "+cp_ToStrODBC(this.w_ANNAZION);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NADESNAZ;
            from (i_cTable) where;
                NACODNAZ = this.w_ANNAZION;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESNAZGESTIONE = NVL(cp_ToDate(_read_.NADESNAZ),cp_NullValue(_read_.NADESNAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
    endcase
  endproc


  proc Init(oParentObject,pImpVis,pARCHIVIO,pCODCON,pSEDE,pINDIRIZZO,pCAP,pLOCALITA,pDESNAZ,pTOINDIRIZZO,pTOCAP,pTOLOCALITA,pTODESNAZ)
    this.pImpVis=pImpVis
    this.pARCHIVIO=pARCHIVIO
    this.pCODCON=pCODCON
    this.pSEDE=pSEDE
    this.pINDIRIZZO=pINDIRIZZO
    this.pCAP=pCAP
    this.pLOCALITA=pLOCALITA
    this.pDESNAZ=pDESNAZ
    this.pTOINDIRIZZO=pTOINDIRIZZO
    this.pTOCAP=pTOCAP
    this.pTOLOCALITA=pTOLOCALITA
    this.pTODESNAZ=pTODESNAZ
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,16)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='NAZIONI'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='DES_DIVE'
    this.cWorkTables[5]='OFF_NOMI'
    this.cWorkTables[6]='DOC_MAST'
    this.cWorkTables[7]='SEDIAZIE'
    this.cWorkTables[8]='MVM_MAST'
    this.cWorkTables[9]='AGENTI'
    this.cWorkTables[10]='VETTORI'
    this.cWorkTables[11]='PNT_MAST'
    this.cWorkTables[12]='MAGAZZIN'
    this.cWorkTables[13]='ANAGRAFE'
    this.cWorkTables[14]='BAN_CHE'
    this.cWorkTables[15]='DIPENDEN'
    this.cWorkTables[16]='CAN_TIER'
    return(this.OpenAllTables(16))

  proc CloseCursors()
    if used('_Curs_SEDIAZIE')
      use in _Curs_SEDIAZIE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pImpVis,pARCHIVIO,pCODCON,pSEDE,pINDIRIZZO,pCAP,pLOCALITA,pDESNAZ,pTOINDIRIZZO,pTOCAP,pTOLOCALITA,pTODESNAZ"
endproc
