* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_atr                                                        *
*              Tipologie righe documenti                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_4]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-11                                                      *
* Last revis.: 2018-07-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_atr"))

* --- Class definition
define class tgsar_atr as StdForm
  Top    = 48
  Left   = 75

  * --- Standard Properties
  Width  = 346
  Height = 64+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-26"
  HelpContextID=92895081
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  CLA_RIGD_IDX = 0
  cFile = "CLA_RIGD"
  cKeySelect = "TRCODCLA"
  cKeyWhere  = "TRCODCLA=this.w_TRCODCLA"
  cKeyWhereODBC = '"TRCODCLA="+cp_ToStrODBC(this.w_TRCODCLA)';

  cKeyWhereODBCqualified = '"CLA_RIGD.TRCODCLA="+cp_ToStrODBC(this.w_TRCODCLA)';

  cPrg = "gsar_atr"
  cComment = "Tipologie righe documenti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TRCODCLA = space(3)
  w_TRDESCLA = space(30)
  w_TRFLAGPA = space(1)
  w_HASEVENT = .F.
  * --- Area Manuale = Declare Variables
  * --- gsar_atr
  * --- Impedisco la cancellazione se
  * --- Tipologia riga utilizzata in
  * --- articoli (ART_ICOL), causali documento (TIP_DOCU)
  * --- e documenti (DOC_DETT)
  Func ah_HasCPEvents(i_cOp)
  if Upper(This.cFunction)="QUERY" And (Upper(i_cop)='ECPDELETE')
        if this.nLoadTime=0
          this.LoadRecWarn()  && rilettura se sono passati piu' di 60 secondi dall' ultima lettura
        ENDIF
    this.NotifyEvent('HasEvent')
    return(this.w_HASEVENT)
  Else
   Return(.t.)
  Endif
  Endfunc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CLA_RIGD','gsar_atr')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_atrPag1","gsar_atr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Tipologia")
      .Pages(1).HelpContextID = 257098321
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTRCODCLA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CLA_RIGD'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CLA_RIGD_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CLA_RIGD_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_TRCODCLA = NVL(TRCODCLA,space(3))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CLA_RIGD where TRCODCLA=KeySet.TRCODCLA
    *
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CLA_RIGD')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CLA_RIGD.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CLA_RIGD '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TRCODCLA',this.w_TRCODCLA  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_HASEVENT = .f.
        .w_TRCODCLA = NVL(TRCODCLA,space(3))
        .w_TRDESCLA = NVL(TRDESCLA,space(30))
        .w_TRFLAGPA = NVL(TRFLAGPA,space(1))
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        cp_LoadRecExtFlds(this,'CLA_RIGD')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TRCODCLA = space(3)
      .w_TRDESCLA = space(30)
      .w_TRFLAGPA = space(1)
      .w_HASEVENT = .f.
      if .cFunction<>"Filter"
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CLA_RIGD')
    this.DoRTCalc(1,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oTRCODCLA_1_1.enabled = i_bVal
      .Page1.oPag.oTRDESCLA_1_2.enabled = i_bVal
      .Page1.oPag.oTRFLAGPA_1_3.enabled = i_bVal
      .Page1.oPag.oObj_1_7.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oTRCODCLA_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oTRCODCLA_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'CLA_RIGD',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODCLA,"TRCODCLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRDESCLA,"TRDESCLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRFLAGPA,"TRFLAGPA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    i_lTable = "CLA_RIGD"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CLA_RIGD_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CLA_RIGD_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CLA_RIGD
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CLA_RIGD')
        i_extval=cp_InsertValODBCExtFlds(this,'CLA_RIGD')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(TRCODCLA,TRDESCLA,TRFLAGPA "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_TRCODCLA)+;
                  ","+cp_ToStrODBC(this.w_TRDESCLA)+;
                  ","+cp_ToStrODBC(this.w_TRFLAGPA)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CLA_RIGD')
        i_extval=cp_InsertValVFPExtFlds(this,'CLA_RIGD')
        cp_CheckDeletedKey(i_cTable,0,'TRCODCLA',this.w_TRCODCLA)
        INSERT INTO (i_cTable);
              (TRCODCLA,TRDESCLA,TRFLAGPA  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_TRCODCLA;
                  ,this.w_TRDESCLA;
                  ,this.w_TRFLAGPA;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CLA_RIGD_IDX,i_nConn)
      *
      * update CLA_RIGD
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CLA_RIGD')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " TRDESCLA="+cp_ToStrODBC(this.w_TRDESCLA)+;
             ",TRFLAGPA="+cp_ToStrODBC(this.w_TRFLAGPA)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CLA_RIGD')
        i_cWhere = cp_PKFox(i_cTable  ,'TRCODCLA',this.w_TRCODCLA  )
        UPDATE (i_cTable) SET;
              TRDESCLA=this.w_TRDESCLA;
             ,TRFLAGPA=this.w_TRFLAGPA;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CLA_RIGD_IDX,i_nConn)
      *
      * delete CLA_RIGD
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'TRCODCLA',this.w_TRCODCLA  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CLA_RIGD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RIGD_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTRCODCLA_1_1.value==this.w_TRCODCLA)
      this.oPgFrm.Page1.oPag.oTRCODCLA_1_1.value=this.w_TRCODCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oTRDESCLA_1_2.value==this.w_TRDESCLA)
      this.oPgFrm.Page1.oPag.oTRDESCLA_1_2.value=this.w_TRDESCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oTRFLAGPA_1_3.RadioValue()==this.w_TRFLAGPA)
      this.oPgFrm.Page1.oPag.oTRFLAGPA_1_3.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'CLA_RIGD')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_TRCODCLA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oTRCODCLA_1_1.SetFocus()
            i_bnoObbl = !empty(.w_TRCODCLA)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_atrPag1 as StdContainer
  Width  = 342
  height = 64
  stdWidth  = 342
  stdheight = 64
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTRCODCLA_1_1 as StdField with uid="XQTUJKXZJV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_TRCODCLA", cQueryName = "TRCODCLA",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della tipologia del documento",;
    HelpContextID = 234221449,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=81, Top=10, InputMask=replicate('X',3)

  add object oTRDESCLA_1_2 as StdField with uid="QOHJGBBJYV",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TRDESCLA", cQueryName = "TRDESCLA",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 219144073,;
   bGlobalFont=.t.,;
    Height=21, Width=248, Left=81, Top=39, InputMask=replicate('X',30)

  add object oTRFLAGPA_1_3 as StdCheck with uid="DCRRIQBXGX",rtseq=3,rtrep=.f.,left=179, top=7, caption="Fatturazione elettronica",;
    ToolTipText = "Se attivo identifica le righe descrittive importabili in fatel",;
    HelpContextID = 170442633,;
    cFormVar="w_TRFLAGPA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTRFLAGPA_1_3.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oTRFLAGPA_1_3.GetRadio()
    this.Parent.oContained.w_TRFLAGPA = this.RadioValue()
    return .t.
  endfunc

  func oTRFLAGPA_1_3.SetRadio()
    this.Parent.oContained.w_TRFLAGPA=trim(this.Parent.oContained.w_TRFLAGPA)
    this.value = ;
      iif(this.Parent.oContained.w_TRFLAGPA=='S',1,;
      0)
  endfunc


  add object oObj_1_7 as cp_runprogram with uid="HWYPAZCGWO",left=0, top=79, width=159,height=25,;
    caption='GSAR_BTR',;
   bGlobalFont=.t.,;
    prg="GSAR_BTR",;
    cEvent = "HasEvent",;
    nPag=1;
    , ToolTipText = "Check per impedire la cancellazione se riga utilizzata";
    , HelpContextID = 45936824

  add object oStr_1_4 as StdString with uid="PMRIZTZDJD",Visible=.t., Left=0, Top=10,;
    Alignment=1, Width=80, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_5 as StdString with uid="WDVJUWBRFC",Visible=.t., Left=0, Top=39,;
    Alignment=1, Width=80, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_atr','CLA_RIGD','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TRCODCLA=CLA_RIGD.TRCODCLA";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
