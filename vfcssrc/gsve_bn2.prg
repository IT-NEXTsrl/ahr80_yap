* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bn2                                                        *
*              Ritorna progressivo nota provv                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_2]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2000-03-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bn2",oParentObject)
return(i_retval)

define class tgsve_bn2 as StdBatch
  * --- Local variables
  * --- WorkFile variables
  PRO_LIQU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Propone il Primo Progressivo Disponibile (da GSVE_SNP)
    * --- Leggo il progressivo solo se stampa Nota Provvigioni
    if this.oParentObject.w_stampa=" "
      this.oParentObject.w_NUMERO = 0
      this.oParentObject.w_ANNDOC = YEAR(this.oParentObject.w_DATNOTA)
      i_Conn=i_TableProp[this.PRO_LIQU_IDX, 3]
      cp_AskTableProg(this.oParentObject, i_Conn, "PRLIN", "i_codazi,w_ANNDOC,w_ALFDOC,w_NUMERO")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PRO_LIQU'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
