* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bre                                                        *
*              Visualizza file di log                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_18]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-05-22                                                      *
* Last revis.: 2009-02-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bre",oParentObject)
return(i_retval)

define class tgsve_bre as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSVE_KLG per visualizzare il file di log
    CREATE CURSOR RESOCON ( RESOCON M(10), MESBLOK M(10))
    INSERT INTO RESOCON (RESOCON, MESBLOK) VALUES ( this.oParentObject.w_RESOCON1, this.oParentObject.w_MESBLOK )
    SELECT * FROM RESOCON INTO CURSOR __TMP__
    SELECT RESOCON
    USE
    CP_CHPRN("QUERY\GSVE_BRE.FRX", " ", this)
    USE IN SELECT("__TMP__")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
