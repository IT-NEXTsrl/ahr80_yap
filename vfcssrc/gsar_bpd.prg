* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bpd                                                        *
*              Gestione progressivi documenti                                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_45]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-02                                                      *
* Last revis.: 2008-04-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOpar,pCiclo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bpd",oParentObject,m.pOpar,m.pCiclo)
return(i_retval)

define class tgsar_bpd as StdBatch
  * --- Local variables
  pOpar = space(1)
  pCiclo = space(1)
  w_CODKEY = space(50)
  w_CODANN = 0
  w_CODANN1 = space(4)
  w_ANNOINI = 0
  w_ANNOFIN = 0
  w_TIPDOC = space(2)
  w_SERDOC = space(10)
  w_NUMDOC = 0
  w_ZOOM = space(10)
  w_CODAZI = space(5)
  w_OK = 0
  w_Messaggio = space(10)
  w_CHIAVE = space(20)
  w_LCLADOC = space(2)
  w_TREBARRA = 0
  * --- WorkFile variables
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestisce i progressivi dei documenti dalla maschera GSAR_KPD
    * --- pCiclo =
    *     'V' numero documento
    *     'A' numero protocollo
    if this.pCiclo = "V"
      this.w_CHIAVE = "PROG\PRDOC"
      this.w_LCLADOC = this.oParentObject.w_CLADOC
    else
      this.w_CHIAVE = "PROG\PRPRO"
      this.w_LCLADOC = this.oParentObject.w_CLADOC1
    endif
    * --- Preparo l'intervallo su cui filtrare
    this.w_ANNOINI = this.oParentObject.w_ANNO
    this.w_ANNOFIN = this.oParentObject.w_ANNO
    if this.oParentObject.w_ANNO = 0 AND NOT EMPTY(this.oParentObject.w_ESERC)
      this.w_ANNOINI = YEAR(this.oParentObject.w_DATAINI)
      this.w_ANNOFIN = YEAR(this.oParentObject.w_DATAFIN)
    endif
    this.w_CODAZI = i_CODAZI
    this.w_ZOOM = this.oParentObject.w_ZoomPDoc
    ND = this.oParentObject.w_ZoomPDoc.cCursor
    This.OparentObject.NotifyEvent("Ricerca")
    select (ND)
    if  this.pOpar="A"
      * --- Inserisce i  valori selezionati nello Zoom
      i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
      this.w_OK = sqlexec(i_Conn,"select * from cpwarn where warncode="+ cp_ToStrODBC(this.w_codazi),"tmp_warn")
      if this.w_OK<0
        ah_ErrorMsg("Errore di connessione",,"")
        i_retcode = 'stop'
        return
      else
        select tmp_warn
        go top
        scan
        this.w_CODKEY = tablecode
        * --- Ricerco occorrenza terza barra per identificare  il primo campo dopo l'azienda
        this.w_TREBARRA = AT("\", this.w_CODKEY ,3) +2
        this.w_CODANN1 = substr(tablecode,this.w_TREBARRA,4)
        this.w_CODANN = int(val( this.w_CODANN1 ))
        this.w_TREBARRA = AT("\", this.w_CODKEY ,4) +2
        this.w_TIPDOC = substr(tablecode, this.w_TREBARRA ,2)
        this.w_NUMDOC = autonum
        this.w_TREBARRA = AT("\", this.w_CODKEY ,5) + 2
        this.w_SERDOC = substr(tablecode, this.w_TREBARRA ,10)
        * --- Filtra su tabella, anno e tipo e esegue l'inserimento nello Zoom
        if UPPER(LEFT(this.w_CODKEY,10))=this.w_CHIAVE AND this.w_TIPDOC=(this.w_LCLADOC) AND ((this.w_CODANN>=this.w_ANNOINI AND this.w_CODANN<=this.w_ANNOFIN) OR (this.oParentObject.w_ANNO=0 AND EMPTY(this.oParentObject.w_ESERC))) 
          INSERT INTO (ND) (CODANN, SERDOC, NUMDOC, CODKEY) VALUES (this.w_CODANN1,this.w_SERDOC,this.w_NUMDOC,this.w_CODKEY)
          select tmp_warn
        endif
        endscan
        * --- chiude il cursore
        if used ("tmp_warn")
          select tmp_warn
          use
        endif
        select (ND)
        go top
        this.w_ZOOM = this.oParentObject.w_ZoomPDoc.refresh()
      endif
    endif
    if this.pOpar="U"
      select (ND)
      go top
      this.w_ZOOM = this.oParentObject.w_ZoomPDoc.refresh()
      * --- Controlla se la tabella � vuota
      if this.oParentObject.w_CODKEY1=" "
        ah_ErrorMsg("Operazione d'aggiornamento impossibile",,"")
        i_retcode = 'stop'
        return
      endif
      if this.oParentObject.w_NUMDOC1=this.oParentObject.w_NUMDOC2
        ah_ErrorMsg("La numerazione non � stata cambiata. Effettuare prima il cambiamento",,"")
      else
        * --- Aggiorna lo Zoom 
        * --- Controlla congruenza dati
        if this.oParentObject.w_NUMDOC1 <0 
          ah_ErrorMsg("Valore non ammesso",,"")
          this.oParentObject.w_NUMDOC1 = this.oParentObject.w_NUMDOC2
        else
          * --- Richiesta conferma
          this.w_Messaggio = "Attenzione: confermi il cambiamento della numerazione progressiva?"
          if ah_YesNo(this.w_Messaggio)
            * --- Esegue l'aggiornamento nella tabella cpwarn
            i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
            this.w_OK = sqlexec(i_Conn,"UPDATE CPWARN SET AUTONUM="+cp_ToStrODBC(this.oParentObject.w_NUMDOC1)+" WHERE TABLECODE="+cp_ToStrODBC(this.oParentObject.w_CODKEY1))
            if this.w_OK<0
              ah_ErrorMsg("Errore di connessione","stop","")
              i_retcode = 'stop'
              return
            endif
          else
            this.oParentObject.w_NUMDOC1 = this.oParentObject.w_NUMDOC2
          endif
        endif
      endif
      * --- Rilegge i valori cambiati nello Zoom
      i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
      this.w_OK = sqlexec(i_Conn,"select * from cpwarn where warncode="+ cp_ToStrODBC(this.w_codazi),"tmp_warn")
      if this.w_OK<0
        ah_ErrorMsg("Errore di connessione","stop","")
        i_retcode = 'stop'
        return
      else
        select tmp_warn
        go top
        scan
        this.w_CODKEY = tablecode
        this.w_TREBARRA = AT("\", this.w_CODKEY ,3) +2
        this.w_CODANN1 = substr(tablecode,this.w_TREBARRA,4)
        this.w_CODANN = int(val( this.w_CODANN1 ))
        this.w_TREBARRA = AT("\", this.w_CODKEY ,4) +2
        this.w_TIPDOC = substr(tablecode, this.w_TREBARRA ,2)
        this.w_NUMDOC = autonum
        this.w_TREBARRA = AT("\", this.w_CODKEY ,5) + 2
        this.w_SERDOC = substr(tablecode, this.w_TREBARRA ,10)
        * --- Filtra su tabella, anno e serie e riesegue l'inserimento nello Zoom
        if UPPER(LEFT(this.w_CODKEY,10))=this.w_CHIAVE AND this.w_TIPDOC=(this.w_LCLADOC) AND ((this.w_CODANN>=this.w_ANNOINI AND this.w_CODANN<=this.w_ANNOFIN) OR (this.oParentObject.w_ANNO=0 AND EMPTY(this.oParentObject.w_ESERC))) 
          INSERT INTO (ND) (CODANN, SERDOC, NUMDOC, CODKEY) VALUES (this.w_CODANN1,this.w_SERDOC,this.w_NUMDOC,this.w_CODKEY)
          select tmp_warn
        endif
        endscan
        * --- chiude il cursore
        if used ("tmp_warn")
          select tmp_warn
          use
        endif
        select (ND)
        go top
        this.w_ZOOM = this.oParentObject.w_ZoomPDoc.refresh()
      endif
    endif
  endproc


  proc Init(oParentObject,pOpar,pCiclo)
    this.pOpar=pOpar
    this.pCiclo=pCiclo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOpar,pCiclo"
endproc
