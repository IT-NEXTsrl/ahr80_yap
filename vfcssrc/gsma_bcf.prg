* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bcf                                                        *
*              Ultimi articoli vend/acq per C/F                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_68]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-24                                                      *
* Last revis.: 2013-07-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bcf",oParentObject,m.pOPER)
return(i_retval)

define class tgsma_bcf as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_FLVEAC = space(1)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_CODART = space(20)
  w_FLULPV = space(1)
  w_FLULCA = space(1)
  w_ZOOM = space(10)
  w_OLDCON = space(15)
  w_DESCON = space(40)
  w_DATREG = ctod("  /  /  ")
  w_DATDOC = ctod("  /  /  ")
  w_CODVAL = space(3)
  w_VALULT = 0
  w_UNIMIS = space(3)
  w_QTAMOV = 0
  w_SERIAL = space(10)
  w_CPROWNUM = 0
  w_NUMRIF = 0
  w_PREZZO = 0
  w_LORDO = space(1)
  w_SCONT1 = 0
  w_SCONT2 = 0
  w_SCONT3 = 0
  w_SCONT4 = 0
  w_FLOMAG = space(20)
  w_PREZZOSM = 0
  w_CODICE = space(20)
  w_VALUNI = 0
  w_DECUNI = 0
  w_VALUN1 = 0
  w_QTAUM1 = 0
  w_UNMIS1 = space(3)
  w_VALFISC = 0
  * --- WorkFile variables
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elabora la situazione degli Ultimi Articoli Venduti ordinati per Cliente o Acquistati ordinati per Fornitore (da GSMA_KUC, GSMA_KUF)
    * --- Al termine della Elaborazione Popola il Cursore dello Zoom della Maschera chiamante
    * --- Parametro Chiamato da: C = Venduto a Cliente ; F = Acquistato da Fornitore
    * --- Parametri in Funzione del Cli/For
    if this.pOPER="C"
      this.w_FLVEAC = "V"
      this.w_TIPCON = "C"
      this.w_CODART = this.oParentObject.w_ARCODART
    else
      this.w_FLVEAC = "A"
      this.w_TIPCON = "F"
      this.w_CODART = this.oParentObject.w_ARCODART
    endif
    this.w_ZOOM = this.oParentObject.w_ZoomVEAC
    ND = this.oParentObject.w_ZoomVEAC.cCursor
    This.OparentObject.NotifyEvent("Ricerca")
    Ah_Msg("Lettura documenti...")
    vq_exec("query\GSMA_KUC",this, "TOTDOC")
    if USED("TOTDOC")
      this.w_OLDCON = "@@@###XXX"
      SELECT TOTDOC
      GO TOP
      SCAN FOR NOT EMPTY(NVL(MVCODCON,""))
      if MVCODCON<>this.w_OLDCON
        * --- Nuovo Cliente/Fornitore
        this.w_OLDCON = MVCODCON
        this.w_DESCON = ANDESCRI
        this.w_DATREG = CP_TODATE(MVDATREG)
        this.w_DATDOC = CP_TODATE(MVDATDOC)
        this.w_CODVAL = MVCODVAL
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECUNI"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_CODVAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECUNI;
            from (i_cTable) where;
                VACODVAL = this.w_CODVAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_PREZZO = MVPREZZO
        this.w_LORDO = LORDO
        this.w_UNIMIS = MVUNIMIS
        this.w_UNMIS1 = ARUNMIS1
        this.w_QTAMOV = MVQTAMOV
        this.w_QTAUM1 = MVQTAUM1
        this.w_FLOMAG = MVFLOMAG
        this.w_CODICE = IIF(this.w_FLOMAG="X"," ",ah_msgformat("OMAGGIO") )
        this.w_SCONT1 = MVSCONT1
        this.w_SCONT2 = MVSCONT2
        this.w_SCONT3 = MVSCONT3
        this.w_SCONT4 = MVSCONT4
        this.w_VALUNI = cp_ROUND((this.w_PREZZO * (1+this.w_SCONT1/100)*(1+this.w_SCONT2/100)*(1+this.w_SCONT3/100)*(1+this.w_SCONT4/100)),this.w_DECUNI)
        this.w_VALUN1 = IIF(this.w_QTAMOV=this.w_QTAUM1 OR this.w_QTAUM1=0, this.w_VALUNI, cp_ROUND((this.w_VALUNI * this.w_QTAMOV) / this.w_QTAUM1, this.w_DECUNI))
        this.w_SERIAL = MVSERIAL
        this.w_CPROWNUM = CPROWNUM
        this.w_NUMRIF = MVNUMRIF
        this.w_VALULT = MVVALULT
        this.w_VALFISC = cp_ROUND(VALFISC,this.w_DECUNI)
        INSERT INTO (ND) ;
        (MVCODCON, ANDESCRI, MVDATREG, MVCODVAL, MVPREZZO, LORDO, MVUNIMIS, MVQTAMOV, OMAGGIO, ;
        MVSCONT1, MVSCONT2, MVSCONT3, MVSCONT4, MVPREZZOSM, ARUNMIS1, MVQTAUM1, MVPREZZOU1, ;
        MVSERIAL, MVNUMRIF, CPROWNUM, MVDATDOC,VALFISC) VALUES ;
        (this.w_OLDCON, this.w_DESCON, this.w_DATREG, this.w_CODVAL, this.w_PREZZO, this.w_LORDO, this.w_UNIMIS, this.w_QTAMOV, this.w_CODICE, ;
        this.w_SCONT1, this.w_SCONT2, this.w_SCONT3 , this.w_SCONT4, this.w_VALUNI, this.w_UNMIS1, this.w_QTAUM1, this.w_VALUN1, ;
        this.w_SERIAL, this.w_NUMRIF, this.w_CPROWNUM, this.w_DATDOC,this.w_VALFISC)
      endif
      ENDSCAN
      * --- chiude il cursore
      if USED("TOTDOC")
        SELECT TOTDOC
        USE
      endif
      select (ND)
      go top
      this.w_ZOOM = this.oParentObject.w_ZoomVEAC.refresh()
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VALUTE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
