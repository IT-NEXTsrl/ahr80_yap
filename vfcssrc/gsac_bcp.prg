* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_bcp                                                        *
*              Caricamento giacenze Pegging commessa                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-07-12                                                      *
* Last revis.: 2005-07-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsac_bcp",oParentObject,m.pAzione)
return(i_retval)

define class tgsac_bcp as StdBatch
  * --- Local variables
  pAzione = space(2)
  Padre = .NULL.
  NC = space(10)
  w_nRecSel = 0
  w_PROGR = 0
  w_PESERIAL = space(10)
  w_SERODL = space(15)
  w_OK = .f.
  w_CODCOM = space(15)
  TmpC = space(100)
  * --- WorkFile variables
  PEG_SELI_idx=0
  CAN_TIER_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Procedura di inserimento delle giacenze libere di magazzino nel Pegging Commessa (da GSMR_KCP)
    this.Padre = this.oParentObject
    this.NC = this.Padre.w_ZoomSel.cCursor
    do case
      case this.pAzione = "ABB"
        * --- Controlla selezioni
        select count(*) as Numero from (this.NC) where xChk=1 into cursor __temp__
        go top
        this.w_nRecSel = NVL(__temp__.Numero,0)
        use in __Temp__
        if this.w_nRecSel=0
          ah_ErrorMsg("Non sono stati selezionati elementi da inserire",48)
        else
          this.w_OK = .T.
          select(this.NC) 
 locate for xchk=1 and empty (nvl(CODCOM," "))
          if found()
            this.TmpC = "Per una o pi� righe selezionate non � stato inserito il codice Commessa.%0Si desidera comunque proseguire con l'elaborazione?"
            if ah_YesNo(this.TmpC)
              this.w_OK = .T.
            else
              this.w_OK = .F.
              ah_ErrorMsg("Operazione sospesa dall'utente",48)
            endif
          endif
          select(this.NC) 
 go top 
 scan for xchk=1
          if this.w_OK
            * --- Verifica validit� valori inseriti
            * --- Verifica Quantit�
            if QTAABB<=0
              this.w_OK = .F.
              ah_ErrorMsg("La quantit� abbinata deve essere maggiore di 0",48)
            endif
            if QTAABB>QTAPER
              this.w_OK = .F.
              ah_ErrorMsg("La quantit� abbinata non pu� essere superiore a quella abbinabile",48)
            endif
            * --- Verifica Commessa 
            *     Devo fare il controllo perch� non c'� link di integrit� referenziale tra CAN_TIER e PEG_SELI
            if ! empty (CODCOM)
              * --- Read from CAN_TIER
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.CAN_TIER_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "CNCODCAN"+;
                  " from "+i_cTable+" CAN_TIER where ";
                      +"CNCODCAN = "+cp_ToStrODBC(CODCOM);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  CNCODCAN;
                  from (i_cTable) where;
                      CNCODCAN = CODCOM;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_CODCOM = NVL(cp_ToDate(_read_.CNCODCAN),cp_NullValue(_read_.CNCODCAN))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if i_rows=0
                this.w_OK = .F.
                ah_ErrorMsg("Il codice commessa inserito non � valido",48)
              endif
            endif
          endif
          if this.w_OK
            * --- I valori inseriti sono corretti, posso procedere all'inserzione
            * --- Try
            local bErr_0386F048
            bErr_0386F048=bTrsErr
            this.Try_0386F048()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- rollback
              bTrsErr=.t.
              cp_EndTrs(.t.)
              ah_ErrorMsg("Impossibile aggiornare la tabella del Pegging di Secondo Livello.",48)
              this.w_OK = .F.
            endif
            bTrsErr=bTrsErr or bErr_0386F048
            * --- End
          else
            exit
          endif
          endscan
          if this.w_OK
            ah_ErrorMsg("Aggiornamento completato con successo.",64)
          endif
        endif
        * --- Riesegue l'interrogazione
        this.Padre.NotifyEvent("Interroga")     
      case this.pAzione = "SS"
        if used(this.NC)
          if this.oParentObject.w_SELEZI="S"
            * --- Seleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=1
          else
            * --- Deseleziona tutte le righe dello zoom
            UPDATE (this.NC) SET xChk=0
          endif
        endif
      case this.pAzione = "INTERROGA"
        this.Padre.NotifyEvent("Interroga")     
        * --- Attiva la pagina 2
        this.oParentObject.oPgFrm.ActivePage = 2
      case this.pAzione = "AGG"
        * --- Aggiorno il Cursore dello Zoom
        update (this.NC) set CODCOM=this.oParentObject.w_CODCAN where xchk=1
    endcase
  endproc
  proc Try_0386F048()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Calcolo il seriale di PEG_SELI
    * --- Select from GSDBMBPG
    do vq_exec with 'GSDBMBPG',this,'_Curs_GSDBMBPG','',.f.,.t.
    if used('_Curs_GSDBMBPG')
      select _Curs_GSDBMBPG
      locate for 1=1
      do while not(eof())
      this.w_PESERIAL = _Curs_GSDBMBPG.PESERIAL
        select _Curs_GSDBMBPG
        continue
      enddo
      use
    endif
    this.w_PROGR = VAL(nvl(this.w_PESERIAL,"0"))
    select(this.NC)
    this.w_PROGR = max(this.w_PROGR + 1,1)
    this.w_PESERIAL = right("0000000000"+alltrim(STR(this.w_PROGR,10,0)),10)
    this.w_SERODL = " Magazz.: "+CODMAG
    * --- Inserisco la riga
    * --- Insert into PEG_SELI
    i_nConn=i_TableProp[this.PEG_SELI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PEG_SELI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PEG_SELI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PESERIAL"+",PESERODL"+",PETIPRIF"+",PERIGORD"+",PEQTAABB"+",PEODLORI"+",PECODCOM"+",PECODRIC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PESERIAL),'PEG_SELI','PESERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SERODL),'PEG_SELI','PESERODL');
      +","+cp_NullLink(cp_ToStrODBC("M"),'PEG_SELI','PETIPRIF');
      +","+cp_NullLink(cp_ToStrODBC(0),'PEG_SELI','PERIGORD');
      +","+cp_NullLink(cp_ToStrODBC(QTAABB),'PEG_SELI','PEQTAABB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_SERODL),'PEG_SELI','PEODLORI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCOM),'PEG_SELI','PECODCOM');
      +","+cp_NullLink(cp_ToStrODBC(CODRIC),'PEG_SELI','PECODRIC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PESERIAL',this.w_PESERIAL,'PESERODL',this.w_SERODL,'PETIPRIF',"M",'PERIGORD',0,'PEQTAABB',QTAABB,'PEODLORI',this.w_SERODL,'PECODCOM',this.w_CODCOM,'PECODRIC',CODRIC)
      insert into (i_cTable) (PESERIAL,PESERODL,PETIPRIF,PERIGORD,PEQTAABB,PEODLORI,PECODCOM,PECODRIC &i_ccchkf. );
         values (;
           this.w_PESERIAL;
           ,this.w_SERODL;
           ,"M";
           ,0;
           ,QTAABB;
           ,this.w_SERODL;
           ,this.w_CODCOM;
           ,CODRIC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pAzione)
    this.pAzione=pAzione
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='PEG_SELI'
    this.cWorkTables[2]='CAN_TIER'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_GSDBMBPG')
      use in _Curs_GSDBMBPG
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione"
endproc
