* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bap                                                        *
*              Controlli import pos                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-09-29                                                      *
* Last revis.: 2009-04-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pAzione,pArrKey,pParent,pTrsOk
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bap",oParentObject,m.pAzione,m.pArrKey,m.pParent,m.pTrsOk)
return(i_retval)

define class tgsar_bap as StdBatch
  * --- Local variables
  pAzione = space(1)
  pArrKey = space(10)
  pParent = .NULL.
  pTrsOk = .f.
  w_Azione = space(1)
  w_Serial = space(10)
  w_Padre = .NULL.
  w_MySelf = .NULL.
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_Loop = 0
  w_Param = space(1)
  w_MDSERIAL = space(10)
  w_Loop = 0
  w_CFUNC = space(10)
  w_DaGest = .f.
  w_TRIG = 0
  w_OK = .f.
  w_Param = space(1)
  w_CAURES = space(5)
  w_CARKIT = space(5)
  w_SCAKIT = space(5)
  w_MDCODNEG = space(5)
  w_CODAZI = space(5)
  w_MDTIPCHI = space(2)
  w_SERDOC = space(10)
  w_MDRIFCOR = space(10)
  w_MDRIFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_TIPDOC = space(5)
  w_FLPROV = space(1)
  w_FLCONT = space(1)
  w_MDCODESE = space(4)
  w_MDCODMAG = space(5)
  w_MDNUMRIF = 0
  w_TESLOT = .f.
  w_MDDATREG = ctod("  /  /  ")
  w_FLDEL = .f.
  w_MDTIPRIG = space(1)
  w_MDCODICE = space(20)
  w_MDCODART = space(20)
  w_MDTIPVEN = space(5)
  w_MDQTAMOV = 0
  w_MDPREZZO = 0
  w_BNOSCHED = .f.
  w_DaGest = .f.
  w_READHARD = space(5)
  w_PASQTA = space(1)
  w_RIGAPIENA = .f.
  w_MESS = space(200)
  w_DMAGCAR = space(5)
  w_DMAGSCA = space(5)
  * --- WorkFile variables
  ADDE_SPE_idx=0
  ATTIVITA_idx=0
  CAM_AGAZ_idx=0
  CAU_CONT_idx=0
  CONTI_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  ESERCIZI_idx=0
  INCDCORR_idx=0
  TIP_DOCU_idx=0
  RAG_FATT_idx=0
  DOC_DETT_idx=0
  PAR_VDET_idx=0
  COR_RISP_idx=0
  DIS_HARD_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo al salvataggio della vendita pos.
    *     Il batch pu� essere lanciato non solo dal pos ma anche da 
    *     batch di generazione vendita pos..
    *     
    *     ATTENZIONE: Nelle manual block sono gestite una serie di metodi per gestire il transitorio
    *     nel caso di lancio direttamente dalla gestione o del cursore nel caso il batch venga lanciato 
    *     da una generazione documenti
    * --- pAzione: 'I' Inserimento, 'D' Cancellazione, 'U' Modifica
    * --- Array delle chiavi del documento
    * --- oParentObject nel caso di lancio da gestione. Altrimenti .Null.
    * --- pTrsOk Passata per riferimento: indica se la transazione deve fallire
    *     .T. Tutto ok-> la transazione va a buon fine
    *     .F. Errore-> la transazione viene abbandonata
    * --- istanzio oggetto per mess. incrementali
    * --- Per gestione messaggistica di ritorno.
    *     Non posso visualizzare messaggi di avviso
    * --- Per gestione
    this.w_OK = .T.
    this.w_oMESS=createobject("ah_message")
    this.w_Azione = this.pAzione
    this.w_Loop = 1
    if Type("g_SCHEDULER")<>"C" Or cp_GetGlobalVar("g_SCHEDULER")="N"
      * --- Se schedulatore non � gia attivo, per impedire la visualizzazione di messaggi
      *     simulo una sua attivazione...
      * --- Lo schedulatore � attivo
      cp_SetGlobalVar("g_SCHEDULER","S")
      * --- Ripulisco la variabile contenente il log
      cp_SetGlobalVar("g_MSG","")
      this.w_BNOSCHED = .T.
    endif
    this.w_CODAZI = i_CODAZI
    * --- Recupero il seriale
    *     Devo utilizzare la macro perch� l'array passato per riferimento da sincronizza
    *     visto che questo � un batch viene passato da pArrKey a this.pArrKey perdendo il giusto puntamento
    *     pArrKey della classe invece � ancora valorizzato
     
 Local AMacro 
 AMacro = "pArrKey"
    do while this.w_Loop <= Alen(&AMacro,1)
      if Upper(Alltrim(&AMacro(this.w_Loop, 1) ) ) = "MDSERIAL"
        this.w_Serial = Alltrim( &AMacro(this.w_Loop, 2) )
        Exit
      endif
      this.w_Loop = this.w_Loop + 1
    enddo
    this.w_Loop = 1
    if Type("this.pParent")="O"
      * --- Se il batch � lanciato dalla gestione pParent contiene l'oggetto gestione (Es.: GSVE_MDV)
      *     Se lanciato da batch di generazione pParent deve essere Null.
      if Upper(this.pParent.Class)="TGSPS_MVD"
        * --- Se lanciato da gestione assegno l'oggetto
        this.w_Padre = this.pParent
      else
        * --- Prevengo il caso in cui da batch venga passato un oggetto
        *     Comunque sia nel caso debba essere passato per qualche motivo 
        *     pParent resta sempre valorizzato correttamente
        this.w_Padre = .Null.
      endif
    endif
    * --- Oggetto MySelf per gestire funzioni nell'area manuale
    this.w_MySelf = This
    this.w_MDNUMRIF = -30
    if Type("this.w_Padre")="O"
      * --- Lanciato da gestione
      this.w_CFUNC = this.w_PADRE.cFunction
      this.w_DaGest = .T.
      this.w_MDSERIAL = this.w_Padre.w_MDSERIAL
      this.w_CAURES = this.w_Padre.w_CAURES
      this.w_CARKIT = this.w_Padre.w_CARKIT
      this.w_SCAKIT = this.w_Padre.w_SCAKIT
      this.w_MDCODNEG = this.w_Padre.w_MDCODNEG
      this.w_MDTIPCHI = this.w_Padre.w_MDTIPCHI
      this.w_MDRIFCOR = this.w_Padre.w_MDRIFCOR
      this.w_MDRIFDOC = this.w_Padre.w_MDRIFDOC
      this.w_MDCODESE = this.w_Padre.w_MDCODESE
      this.w_MDCODMAG = this.w_Padre.w_MDCODMAG
      this.w_MDDATREG = this.w_Padre.w_MDDATREG
      this.w_MDTIPVEN = this.w_Padre.w_MDTIPVEN
    else
      * --- Lanciato da batch
      this.w_CFUNC = IIF(this.pAzione="I","Load",IIF(this.pAzione="D","Query","Edit"))
      this.w_MDSERIAL = this.w_Serial
      * --- Read from COR_RISP
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COR_RISP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2],.t.,this.COR_RISP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MDCODNEG"+;
          " from "+i_cTable+" COR_RISP where ";
              +"MDSERIAL = "+cp_ToStrODBC(this.w_MDSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MDCODNEG;
          from (i_cTable) where;
              MDSERIAL = this.w_MDSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MDCODNEG = NVL(cp_ToDate(_read_.MDCODNEG),cp_NullValue(_read_.MDCODNEG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from PAR_VDET
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PAR_VDET_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "PACAURES,PACARKIT,PASCAKIT"+;
          " from "+i_cTable+" PAR_VDET where ";
              +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
              +" and PACODNEG = "+cp_ToStrODBC(this.w_MDCODNEG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          PACAURES,PACARKIT,PASCAKIT;
          from (i_cTable) where;
              PACODAZI = this.w_CODAZI;
              and PACODNEG = this.w_MDCODNEG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CAURES = NVL(cp_ToDate(_read_.PACAURES),cp_NullValue(_read_.PACAURES))
        this.w_CARKIT = NVL(cp_ToDate(_read_.PACARKIT),cp_NullValue(_read_.PACARKIT))
        this.w_SCAKIT = NVL(cp_ToDate(_read_.PASCAKIT),cp_NullValue(_read_.PASCAKIT))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows=0
        this.w_MESS = ah_Msgformat("Non esistono i parametri per negozio %1", Alltrim(this.w_MDCODNEG))
        this.w_OK = .F.
      endif
      * --- Read from COR_RISP
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.COR_RISP_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COR_RISP_idx,2],.t.,this.COR_RISP_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MDCODESE,MDCODMAG,MDDATREG,MDRIFCOR,MDRIFDOC,MDTIPCHI,MDTIPVEN"+;
          " from "+i_cTable+" COR_RISP where ";
              +"MDSERIAL = "+cp_ToStrODBC(this.w_MDSERIAL);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MDCODESE,MDCODMAG,MDDATREG,MDRIFCOR,MDRIFDOC,MDTIPCHI,MDTIPVEN;
          from (i_cTable) where;
              MDSERIAL = this.w_MDSERIAL;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_MDCODESE = NVL(cp_ToDate(_read_.MDCODESE),cp_NullValue(_read_.MDCODESE))
        this.w_MDCODMAG = NVL(cp_ToDate(_read_.MDCODMAG),cp_NullValue(_read_.MDCODMAG))
        this.w_MDDATREG = NVL(cp_ToDate(_read_.MDDATREG),cp_NullValue(_read_.MDDATREG))
        this.w_MDRIFCOR = NVL(cp_ToDate(_read_.MDRIFCOR),cp_NullValue(_read_.MDRIFCOR))
        this.w_MDRIFDOC = NVL(cp_ToDate(_read_.MDRIFDOC),cp_NullValue(_read_.MDRIFDOC))
        this.w_MDTIPCHI = NVL(cp_ToDate(_read_.MDTIPCHI),cp_NullValue(_read_.MDTIPCHI))
        this.w_MDTIPVEN = NVL(cp_ToDate(_read_.MDTIPVEN),cp_NullValue(_read_.MDTIPVEN))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Non lanciato da gestione
      this.w_DaGest = .F.
    endif
    if this.w_OK
      this.w_READHARD = IIF( TYPE("g_CODCAS")="C",g_CODCAS, SPACE(5))
      if Not Empty(this.w_READHARD)
        * --- Read from DIS_HARD
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIS_HARD_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIS_HARD_idx,2],.t.,this.DIS_HARD_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DHPASQTA"+;
            " from "+i_cTable+" DIS_HARD where ";
                +"DHCODICE = "+cp_ToStrODBC(this.w_READHARD);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DHPASQTA;
            from (i_cTable) where;
                DHCODICE = this.w_READHARD;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PASQTA = NVL(cp_ToDate(_read_.DHPASQTA),cp_NullValue(_read_.DHPASQTA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      this.w_TRIG = 0
      this.w_Param = IIF(this.w_DaGest,"G","D")
    endif
    if this.w_OK And this.w_CFUNC<>"Query"
      if EMPTY(this.w_CAURES) OR EMPTY(this.w_CARKIT) OR EMPTY(this.w_SCAKIT)
        this.w_MESS = ah_Msgformat("Causali magazzino per resi/kit non definite nei parametri P.O.S.%0Impossibile confermare la vendita")
        this.w_OK = .F.
      endif
    endif
    if this.w_OK
      * --- Se Variazione o Cancellazione e la Vendita ha generato un documento
      * --- Nel caso di emissione scontrino con corrispettivo generato, per eliminare lo scontrino devo prima eliminare il Corr generato.
      *     Nel caso di Ricevuta fiscale Immediata invece, la vendita Negozio comanda: 
      *     Non � possibile eliminare il documento. Cancellando la vendita negozio vengono cancellati entrambi
      * --- Se sono chiusure che generano documenti immediati eseguo i controlli e cancello anche il documento generato
      if Not (this.w_MDTIPCHI$"ES-BV-NS") And (Empty(this.w_MDRIFCOR) Or this.w_MDTIPCHI="RF" )
        if NOT EMPTY(this.w_MDRIFDOC) Or (this.w_MDTIPCHI="RF" And Not Empty(this.w_MDRIFCOR))
          this.w_DATDOC = cp_CharToDate("  -  -  ")
          this.w_NUMDOC = 0
          this.w_ALFDOC = Space(10)
          this.w_TIPDOC = SPACE(5)
          this.w_FLPROV = " "
          this.w_FLCONT = " "
          if this.w_MDTIPCHI="RF"
            * --- Se ricevuta fiscale....
            this.w_SERDOC = this.w_MDRIFCOR
          else
            * --- Se documento 
            this.w_SERDOC = this.w_MDRIFDOC
          endif
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVDATDOC,MVNUMDOC,MVALFDOC,MVTIPDOC,MVGENPRO,MVFLCONT"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVDATDOC,MVNUMDOC,MVALFDOC,MVTIPDOC,MVGENPRO,MVFLCONT;
              from (i_cTable) where;
                  MVSERIAL = this.w_SERDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
            this.w_NUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
            this.w_ALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
            this.w_TIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
            this.w_FLPROV = NVL(cp_ToDate(_read_.MVGENPRO),cp_NullValue(_read_.MVGENPRO))
            this.w_FLCONT = NVL(cp_ToDate(_read_.MVFLCONT),cp_NullValue(_read_.MVFLCONT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Solo se il documento esiste....
          *     Potrebbe non esistere nel database di sincronizzazione
          if i_Rows<>0
            * --- Controllo se i dettagli del documento sono stampati nel giornale magazzino
            if ! CHKGIOM(this.w_MDCODESE,this.w_MDCODMAG,this.w_DATDOC,"S")
              this.w_MESS = ah_Msgformat("Documento stampato su g.magazzino")
              this.w_OK = .F.
            endif
            if this.w_OK
              * --- Vendita Associata  Ric.Fiscale/Fattura/DDT (documenti generati Immediatamente)
              do case
                case this.w_FLCONT="S"
                  this.w_MESS = ah_Msgformat("Vendita associata ad un documento (%1 n.%2 del %3) contabilizzato",this.w_TIPDOC , ALLTRIM(STR(this.w_NUMDOC,15) + IIF(Not Empty(this.w_ALFDOC),Alltrim(this.w_ALFDOC),"") ), DTOC(this.w_DATDOC) )
                  this.w_OK = .F.
                case this.w_FLPROV="S" AND g_PERAGE="S"
                  this.w_MESS = ah_Msgformat("Vendita associata ad un documento (%1 n.%2 del %3) con provvigioni generate",this.w_TIPDOC , ALLTRIM(STR(this.w_NUMDOC,15) + IIF(Not Empty(this.w_ALFDOC),Alltrim(this.w_ALFDOC),"") ) , DTOC(this.w_DATDOC) )
                  this.w_OK = .F.
              endcase
            endif
            if this.w_OK
              * --- Select from DOC_DETT
              i_nConn=i_TableProp[this.DOC_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_DETT ";
                    +" where MVSERIAL="+cp_ToStrODBC(this.w_SERDOC)+"";
                     ,"_Curs_DOC_DETT")
              else
                select * from (i_cTable);
                 where MVSERIAL=this.w_SERDOC;
                  into cursor _Curs_DOC_DETT
              endif
              if used('_Curs_DOC_DETT')
                select _Curs_DOC_DETT
                locate for 1=1
                do while not(eof())
                if NVL(_Curs_DOC_DETT.MVQTAEV1, 0)<>0 OR NVL(_Curs_DOC_DETT.MVIMPEVA, 0)<>0
                  if this.w_CFUNC="Query"
                    this.w_MESS = ah_Msgformat("Vendita associata ad un documento (%1 n.%2 del %3) evaso da altri documenti%0Impossibile eliminare",this.w_TIPDOC , ALLTRIM(STR(this.w_NUMDOC,15) + IIF(Not Empty(this.w_ALFDOC),Alltrim(this.w_ALFDOC),"") ) , DTOC(this.w_DATDOC) )
                  else
                    this.w_MESS = ah_Msgformat("Vendita associata ad un documento (%1 n.%2 del %3) evaso da altri documenti%0Impossibile variare",this.w_TIPDOC , ALLTRIM(STR(this.w_NUMDOC,15) + IIF(Not Empty(this.w_ALFDOC),Alltrim(this.w_ALFDOC),"") ), DTOC(this.w_DATDOC) )
                  endif
                  this.w_OK = .F.
                endif
                  select _Curs_DOC_DETT
                  continue
                enddo
                use
              endif
            endif
          endif
        endif
      else
        * --- Se la vendita � di tipo Emissione scontrino e il corrispettivo � stato generato dalla Generazione Massiva
        *     blocco la cancellazione perch� deve essere prima eliminato il corrispettivo generato.
        *     Se la vendita invece ha generato immediatamente un corrispettivo � la vendita negozio a comandare.
        *     La cancellazione della vendita negozio cancella anche il corrispettivo
        if Not empty(this.w_MDRIFCOR) And this.w_MDTIPCHI = "ES"
          this.w_DATDOC = cp_CharToDate("  -  -  ")
          this.w_NUMDOC = 0
          this.w_ALFDOC = Space(10)
          this.w_TIPDOC = SPACE(5)
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVDATDOC,MVNUMDOC,MVALFDOC,MVTIPDOC"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_MDRIFCOR);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVDATDOC,MVNUMDOC,MVALFDOC,MVTIPDOC;
              from (i_cTable) where;
                  MVSERIAL = this.w_MDRIFCOR;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
            this.w_NUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
            this.w_ALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
            this.w_TIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_Rows<>0
            * --- Vendita Corrispettivi (documenti generati in differita)
            *     Se sul database il documento non c'� non do avvisi e considero la cosa
            *     corretta
            if this.w_CFUNC="Query"
              this.w_MESS = ah_Msgformat("Vendita associata ad un documento di corrispettivi (%1 n.%2 del %3)%0Per eliminare occorre prima cancellare il documento generato", this.w_TIPDOC , ALLTRIM(STR(this.w_NUMDOC,15) + IIF(Not Empty(this.w_ALFDOC),Alltrim(this.w_ALFDOC),"") ) ,DTOC(this.w_DATDOC) ) 
            else
              this.w_MESS = ah_Msgformat("Vendita associata ad un documento di corrispettivi (%1 n.%2 del %3)%0Per variare occorre prima cancellare il documento generato", this.w_TIPDOC , ALLTRIM(STR(this.w_NUMDOC,15) + IIF(Not Empty(this.w_ALFDOC),Alltrim(this.w_ALFDOC),"") ), DTOC(this.w_DATDOC) ) 
            endif
            this.w_OK = .F.
          endif
        endif
      endif
    endif
    if this.w_OK And this.w_CFUNC<>"Query"
      this.Pag3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Scorro il dettaglio documento...
    if this.w_OK
      if this.w_DaGest
        * --- Se lanciato da gestione lavoro sul TrsName...
        this.w_PADRE.MarkPos()     
      else
        * --- ...altrimenti costruisco un cursore con tutti i campi del dettaglio 
        *     e rinomino le colonne con t_+ nome campo
        vq_exec("query\CHKPOS.VQR",this,"GENEAPP")
      endif
      this.w_MySelf.FirstRow()     
      this.w_FLDEL = .F.
      this.w_TRIG = 0
      * --- Test se cancellata
      this.w_FLDEL = this.w_CFUNC="Query"
      this.w_MDTIPRIG = this.w_MySelf.Get("w_MDTIPRIG")
      this.w_MDCODICE = this.w_MySelf.Get("w_MDCODICE")
      this.w_MDCODART = this.w_MySelf.Get("w_MDCODART")
      this.w_RIGAPIENA = NOT EMPTY(this.w_MDCODART)
      if Not this.w_RIGAPIENA
        this.w_MESS = ah_Msgformat("Impossibile salvare: documento senza dettaglio")
        this.w_OK = .F.
      endif
      if this.w_OK
        * --- Primo giro sulle righe non cancellate...
        do while Not this.w_MySelf.Eof_Trs()
          this.w_MySelf.SetRow()     
          if this.w_RIGAPIENA
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Se tutto ok passo alla prossima riga altrimenti esco...
          if this.w_OK
            this.w_MySelf.NextRow()     
          else
            Exit
          endif
        enddo
      endif
      if this.w_DaGest And .F.
        * --- Secondo giro, se tutto Ok sulle righe cancellate...
        *     Solo se lanciato da gestione
        *     Inserire controlli per righe cancellate se necessario
        if this.w_OK
          this.w_PADRE.FirstRowDel()     
          * --- Test Se riga Eliminata
          this.w_FLDEL = .T.
          do while Not this.w_PADRE.Eof_Trs()
            this.w_PADRE.SetRow()     
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Se tutto ok passo alla prossima riga altrimenti esco...
            if this.w_OK
              this.w_PADRE.NextRowDel()     
            else
              if not Empty( this.w_MESS )
                ah_ErrorMsg("%1",,"", this.w_MESS)
              endif
              Exit
            endif
          enddo
        endif
        if this.w_OK And w_MVFLPROV<>"S" And g_PERDIS="S" 
          * --- Lancio il check disponibilit� articoli
          this.w_OK = GSAR_BDA( This , "S", this.w_PADRE )
        endif
      endif
      if this.w_DaGest
        * --- Se lanciato da gestione mi riposiziono sul TrsName...
        this.w_PADRE.RePos(.F.)     
      else
        if Used("GENEAPP")
           
 Select GENEAPP 
 Use
        endif
      endif
    endif
    if Not Empty(this.w_MESS)
      * --- Aggiungo gli estremi del documento
      this.w_oPART = this.w_oMess.AddMsgPartNL("Vendita %1 del %2 Progressivo %3")
      this.w_oPART.AddParam(Alltrim(this.w_MDTIPVEN))     
      this.w_oPART.AddParam(Alltrim(DTOC(this.w_MDDATREG)))     
      this.w_oPART.AddParam(this.w_Serial)     
      this.w_oPART = this.w_oMess.AddMsgPartNL("%1")
      this.w_oPART.AddParam(this.w_MESS)     
      this.w_MESS = this.w_oMess.ComposeMessage()
    endif
    * --- Ripasso per riferimento il buon fine della transazione
    *     devo usare la macro poich� essendo un batch il painter mette il this. davanti alla
    *     propriet� non andando a modificare la vera variabile passata per riferimento ma solo
    *     la propriet� this.pTrsOk di pag1
     
 L_Macro = "pTrsOk = this.w_OK" 
 &L_Macro
    if this.w_BNOSCHED
      * --- Disattivo lo schedulatore se non presente..
      cp_SetGlobalVar("g_SCHEDULER","N")
    endif
    i_retcode = 'stop'
    i_retval = this.w_MESS
    return
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ciclo sui dettaglio documenti...
    * --- La riga � valida...
    if this.w_RIGAPIENA
      if this.w_PASQTA = "S" And (this.w_MDQTAMOV <> cp_Round(this.w_MDQTAMOV, 2) Or this.w_MDPREZZO <> cp_Round(this.w_MDPREZZO, 2)) And this.w_MDTIPRIG <>"D" And this.w_MDTIPCHI = "ES"
        this.w_MESS = ah_Msgformat("Impossibile utilizzare prezzo o quantit� con pi� di due decimali se attivo invio quantit� sul dispositivo cassa installato")
        this.w_OK = .F.
      endif
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifico la presenza delle matricole!
    if this.w_CFUNC<>"Query" And g_MATR="S" And this.w_MDDATREG>=nvl(g_DATMAT,cp_CharToDate("  /  /    "))
      if this.w_OK
        * --- Verifico congruenza numero matricole con qt� di riga (SE NON IN FASE DI CORR. ERORRI LOTTI / UBICAZIONI)
        * --- Select from GSPS_BMT
        do vq_exec with 'GSPS_BMT',this,'_Curs_GSPS_BMT','',.f.,.t.
        if used('_Curs_GSPS_BMT')
          select _Curs_GSPS_BMT
          locate for 1=1
          do while not(eof())
          this.w_OK = .F.
          if NOT EMPTY(NVL(_Curs_GSPS_BMT.DOMAGSCA," "))
            * --- Se si tratta di un trasferimento determino il magazzino di carico\scarico corretto del documento
            this.w_DMAGCAR = Nvl(_Curs_GSPS_BMT.DOMAGCAR,Space(5) )
            this.w_DMAGSCA = Nvl(_Curs_GSPS_BMT.DOMAGSCA,Space(5) )
          else
            this.w_DMAGCAR = IIF(_Curs_GSPS_BMT.MVFLCASC="+" , NVL(_Curs_GSPS_BMT.DOMAGCAR,Space(5)),NVL(_Curs_GSPS_BMT.DOMAGSCA,Space(5)))
            this.w_DMAGSCA = IIF(_Curs_GSPS_BMT.MVFLCASC="-" , NVL(_Curs_GSPS_BMT.DOMAGSCA,Space(5)),NVL(_Curs_GSPS_BMT.DOMAGCAR,Space(5)))
          endif
          do case
            case Nvl(qtaum1,0)<>Nvl(qtamat,0)
              * --- Quantit� frazionabili e qta riga diversa da qta matricole: poich� se una vendita pos genera un documento,
              *     le matricole vengono spostate sul documento, in fase di sincronizzazione non devo effettuare questi controlli
              *     poich� le matricole sul pos non sono pi� presenti.
              this.w_OK = .T.
            case ALLTRIM(Nvl(Mvcodice,Space(20)))<>ALLTRIM(Nvl(Mtkeysal,Space(20)))
              this.w_MESS = ah_Msgformat("Riga %1 con articolo incongruente. Articolo documento: %2, articolo matricole: %3", Alltrim( Str( _Curs_GSPS_BMT.ROWORD ) ), Alltrim( _Curs_GSPS_BMT.MVCODICE ), Alltrim( _Curs_GSPS_BMT.MTKEYSAL ) ) 
            case Nvl(Mvcodlot,Space(20)) <> Nvl(Mtcodlot,Space(20)) AND g_PERLOT="S"
              this.w_MESS = ah_Msgformat("Riga %1 con lotto incongruente. Lotto documento: %2, lotto matricole: %3", Alltrim( Str( _Curs_GSPS_BMT.ROWORD ) ), Alltrim( NVL(_Curs_GSPS_BMT.MVCODLOT,Space(20)) ), Alltrim( NVL(_Curs_GSPS_BMT.MTCODLOT,Space(20)) ) )
            case ALLTRIM(Nvl(Mtmagpri,Space(5)))<>Alltrim(this.w_DMAGCAR)
              this.w_MESS = ah_Msgformat("Riga %1 con magazzino di carico incongruente. Magazzino documento: %2, magazzino matricole: %3", Alltrim( Str( _Curs_GSPS_BMT.ROWORD ) ) , Alltrim(this.w_DMAGCAR), Alltrim( NVL(_Curs_GSPS_BMT.MTMAGPRI,Space(5))) )
            case Nvl(Mvcodubi,Space(20))<>Nvl(Mtcodubi,Space(20)) AND g_PERUBI="S"
              this.w_MESS = ah_Msgformat("Riga %1 con ubicazione incongruente. Ubicazione documento: %2, ubicazione matricole: %3", Alltrim( Str( _Curs_GSPS_BMT.ROWORD ) ), Alltrim( NVL(_Curs_GSPS_BMT.MVCODUBI,Space(20))), Alltrim( NVL(_Curs_GSPS_BMT.MTCODUBI,Space(20)) ) )
            case ALLTRIM(Nvl(Mtmagsca,Space(5)))<>Alltrim(this.w_DMAGSCA) and Not Empty(Nvl(Mtmagsca,Space(5)))
              this.w_MESS = ah_Msgformat("Riga %1 con magazzino di scarico incongruente. Magazzino documento: %2, magazzino matricole: %3", Alltrim( Str( _Curs_GSPS_BMT.ROWORD ) ), Alltrim(this.w_DMAGSCA), Alltrim( NVL(_Curs_GSPS_BMT.MTMAGSCA,Space(5))) )
          endcase
          Exit
            select _Curs_GSPS_BMT
            continue
          enddo
          use
        endif
      endif
    endif
  endproc


  proc Init(oParentObject,pAzione,pArrKey,pParent,pTrsOk)
    this.pAzione=pAzione
    this.pArrKey=pArrKey
    this.pParent=pParent
    this.pTrsOk=pTrsOk
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,15)]
    this.cWorkTables[1]='ADDE_SPE'
    this.cWorkTables[2]='ATTIVITA'
    this.cWorkTables[3]='CAM_AGAZ'
    this.cWorkTables[4]='CAU_CONT'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='DOC_MAST'
    this.cWorkTables[7]='DOC_RATE'
    this.cWorkTables[8]='ESERCIZI'
    this.cWorkTables[9]='INCDCORR'
    this.cWorkTables[10]='TIP_DOCU'
    this.cWorkTables[11]='RAG_FATT'
    this.cWorkTables[12]='DOC_DETT'
    this.cWorkTables[13]='PAR_VDET'
    this.cWorkTables[14]='COR_RISP'
    this.cWorkTables[15]='DIS_HARD'
    return(this.OpenAllTables(15))

  proc CloseCursors()
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_GSPS_BMT')
      use in _Curs_GSPS_BMT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- gsar_bap
  * Il batch gestisce sia il transitorio dei documenti
  * che un transitorio costruito da batch per la generazione
  * documenti (Es. GSVE_BFD)
  * Definiti quindi metodi propri di questo batch che in base
  * al parametro lanciano metodi sul transitorio della gestione
  * o su GENEAPP (cursore creato dalle generazioni)
  
  Procedure Exec_Select(cTmp,cFields,cWhere,cOrder,cGroupBy,cHaving)
  LOCAL cName_1,cName_2,cCmdSql,cOlderr,cMsgErr
   If This.w_PARAM='D'
  	* Assegnamenti..
  	cOlderr=ON('error')
  	cMsgErr=''
  	On error cMsgErr=Message(1)+': '+MESSAGE()
  	
  	cTmp=   IIF( EMPTY(cTmp)   , '__Tmp__', cTmp )
  	cFields=IIF( EMPTY(cFields), '*'      , cFields )
  	cWhere= IIF( EMPTY(cWhere) , '1=1'    , cWhere )	
  	cOrder= IIF( EMPTY(cOrder) , '1'      , cOrder )	
  	
  	cOrder=IIF( EMPTY(cGroupBy),'',' GROUP BY '+(cGroupBy)+IIF( EMPTY(cHaving),'',' HAVING '+(cHaving) ) )+' Order By '+cOrder
  	
  	cName_1='GENEAPP'
  	
  	cCmdSql='Select '+cFields+' From '+cName_1			
  	cCmdSql=cCmdSql+' Where '
  	
  	cCmdSql=cCmdSql+cWhere+' &cOrder Into Cursor '+cTmp+' Nofilter '
  	* Eseguo la frase..
  	&cCmdSql
  
  	* Ripristino la vecchia gestione errori
  	ON ERROR &cOldErr
  
  	* Se Qualcosa va storto lo segnalo ed esco...
  	  IF NOT EMPTY(cMsgErr)
        ah_ErrorMsg(cMsgErr,'stop','')
        * memorizzo nella clipboard la frase generata in caso di errore
        _ClipText=cCmdSql
      ELSE
  		  * mi posiziono sul temporaneo al primo record..
  		  SELECT (cTmp)
  		  GO Top
      ENDIF  
   Else
    This.w_PADRE.Exec_Select(cTmp,cFields,cWhere,cOrder,cGroupBy,cHaving)
   Endif
  Endproc
  
  PROCEDURE SetRow(id_row) 
  LOCAL cOldArea
   if This.w_PARAM='D'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Se paramento valorizzato mi posiziono sulla riga passata 
  	* altrimenti sulla riga attuale...
  	IF TYPE( 'id_row' )='N'
  		SELECT( 'GENEAPP' )
  		goto (id_row) 	
  	EndIf
  		
  	* ripristino la vecchia area
  	SELECT(coldArea)	  
   else
  	 this.w_PADRE.SetRow( id_Row )
   endif
  
  Endproc
  
  Procedure FirstRow()
   LOCAL cOldArea
   If This.w_PARAM='D'	
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
    Select GENEAPP
    Go Top
  	* ripristino la vecchia area
  	SELECT(coldArea)  
   else
    this.w_PADRE.FirstRow()
   endif
  EndProc
  
  Function Eof_Trs() 
   if This.w_PARAM='D'
    RETURN (EOF('GENEAPP'))
   else
  	 RETURN (this.w_PADRE.Eof_Trs())
   endif
  ENDFUNC
  
  
  FUNCTION GET(Item)
  LOCAL cOldArea,cFldName
   if This.w_PARAM='D'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Nel caso di GENEAPP i campi del cursore non hanno la t_ davanti
  	* quindi prendo il nome della colonne che ha lo stesso nome della variabile senza w_ davanti
  	IF lower(LEFT(Item,2))='w_' 
  		cFldName=SUBSTR(Item,3,LEN(Item))
  	ELSE
  		cFldName=Item
  	ENDIF
  
  	SELECT( 'GENEAPP' )
  	* Leggo il campo...
  	Result=eval(cFldName)
  	
  	* ripristino la vecchia area
  	SELECT(coldArea)	
  	
  	* Restituisco il valore recuperato..
  	Return(Result)
     
   else
    Return( This.w_PADRE.Get(Item) )
   Endif
  EndFunc
  
  Procedure NextRow()
  LOCAL cOldArea
   If This.w_PARAM='D'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
   
    Select 'GENEAPP'
    Skip
    
  	* ripristino la vecchia area
  	SELECT(coldArea)  
    
   Else
    This.w_PADRE.NextRow()
   Endif
  	
  EndProc
  
  Function Search(Criterio,StartFrom)
  LOCAL cOldArea,cName_1, cCursName,nResult,cCond
   If This.w_PARAM='D'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	cName_1='GENEAPP'
  	cCursName=SYS(2015)	
  	* Se ricerca a partire da un certo record aggiungo la condizione..
  	IF TYPE('StartFrom')='N' AND StartFrom<>0
  	cCond=' RECNO(cName_1)>'+ALLTRIM(STR(StartFrom))
  	ELSE
  	cCond=' 1=1 '
  	Endif
  		SELECT MIN(RECNO()) As Riga FROM (cName_1) WHERE  &cCond AND  		&Criterio INTO CURSOR &cCursName
  	* leggo il risultato
  	SELECT(cCursName)
  	GO Top
  	nResult=NVL(&cCursName..Riga,-1) 
  	nResult=IIF(nResult=0,-1, nResult)
  	* rimuovo il cursore di appoggio
      if used(cCursName)
        select (cCursName)
        use
      ENDIF    
  	* ripristino la vecchia area
  	SELECT(coldArea)
  	* restituisco il risultato
  	RETURN nResult
    
   Else
    Return(This.w_PADRE.Search(Criterio,StartFrom))
   Endif
  
  EndFunc
  
  
  Procedure SET(cItem,vValue,bNoUpd)
  LOCAL cOldArea,cFldName,bResult
   If This.w_PARAM='D'
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Nel caso di GENEAPP i campi non hanno t_ davanti 
  	* quindi cerco semplicemente il campo con il nome della variabile senza w_
  	IF lower(LEFT(cItem,2))='w_' 
  		cFldName=SUBSTR(cItem,3,LEN(cItem))
  	ELSE
  		cFldName=cItem
  	ENDIF
  
  	* Aggiorno il transitorio normale...
  	SELECT( 'GENEAPP' )
  		
  	* Se cambio allora svolgo la Replace..
  	IF &cFldName<>vValue
  		Replace &cFldName WITH vValue
  	endif			
  	* ripristino la vecchia area
  	SELECT(coldArea) 
   Else
    This.w_PADRE.Set(cItem,vValue,bNoUpd)
   Endif
  EndProc
  
  FUNCTION GetType(cFieldsName)
   If This.w_PARAM='D'
   	LOCAL cOldArea,cFldName
  	* Memorizzo l'area corrente per ripristino
  	cOldarea=SELECT()
  	* Se le prima due lettere sono w_ le sostituisco con t_
  	* altrimenti lascio quanto trovo...
  	IF lower(LEFT(cFieldsName,2))='w_' 
  		cFldName='t_'+SUBSTR(cFieldsName,3,LEN(cFieldsName))
  	ELSE
  		cFldName=cFieldsName
  	ENDIF	
  	
  		SELECT( 'GENEAPP'  )
  			
  	* Leggo il campo...
  	Result=Type(cFldName)
  				
  	* ripristino la vecchia area
  	SELECT(coldArea)		
  	Return( Result )
   ELSE
   Return(This.w_PADRE.GetType( cFieldsName ))
   endif
  EndFunc
  
  Procedure Done()
   *Azzerro la variabile di comodo, se non lo facessi rimarebbe la classe
   *del batch appesa in memoria, impedendo tra l'altro la compilazione
   *all'interno di ad hoc
   this.w_MYSELF=.Null.
   DoDefault()
  EndpRoc
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pAzione,pArrKey,pParent,pTrsOk"
endproc
