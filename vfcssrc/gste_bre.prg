* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bre                                                        *
*              Emissione bonifici esteri- scrittura                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_327]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1999-12-21                                                      *
* Last revis.: 2016-03-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bre",oParentObject)
return(i_retval)

define class tgste_bre as StdBatch
  * --- Local variables
  w_TIPODIS = space(2)
  w_DATSUP = ctod("  /  /  ")
  w_SERIALE = space(10)
  w_DatPre = ctod("  /  /  ")
  w_NomeFile = space(200)
  w_Separa = space(2)
  w_Decimali = 0
  w_Handle = 0
  w_Record = space(120)
  w_AziAtt = space(1)
  w_AziSia = space(5)
  w_BanAbi = space(5)
  w_BanCab = space(5)
  w_ConCor = space(12)
  w_DatCre = space(6)
  w_DatSca = space(6)
  w_DatVal = space(6)
  w_NomSup = space(20)
  w_CodDiv = space(1)
  w_Totale = 0
  w_NumDis = space(7)
  w_Valore = 0
  w_Lunghezza = 0
  w_StrInt = space(1)
  w_StrDec = space(1)
  w_RifDoc = space(1)
  w_TotDoc = space(1)
  w_MESS = space(200)
  w_OK = .f.
  w_OK1 = .f.
  w_IBANBEN = space(34)
  w_BICBEN = space(11)
  w_IBANORD = space(34)
  w_BICORD = space(11)
  w_INFALTER1 = space(35)
  w_INFALTER2 = space(35)
  w_INFALTER3 = space(35)
  w_IDENTIFI = space(34)
  w_DATAESEC = space(8)
  w_DATAVALU = ctod("  /  /  ")
  w_BANBENE = space(10)
  w_CODPAESE = space(2)
  w_NAZBANCA = space(3)
  w_DIVIMP = space(3)
  w_DIVACC = space(3)
  w_MODPAG = space(2)
  w_IMPORTO = 0
  w_TIPOCONTO = space(1)
  w_CODCONTO = space(15)
  w_MODADD = space(2)
  w_RAGSOC = space(40)
  w_INDIRIZ = space(35)
  w_LOCALITA = space(30)
  w_CONTESO = space(15)
  w_DIVBONIF = space(3)
  w_VALIMP = space(18)
  w_DIVACRED = space(3)
  w_RELDIVIS = space(3)
  w_AVVISO = space(2)
  w_CONTATTO = space(2)
  w_UFFREF = space(17)
  w_NOMINAT = space(35)
  w_Datval1 = space(8)
  w_DatEse = space(8)
  w_DatCre1 = space(6)
  w_DITIPDES = space(3)
  w_oERRORLOG = .NULL.
  w_COPAEBEN = space(3)
  w_TIPCON = space(1)
  w_CODCON = space(1)
  w_DICODVAL = space(3)
  w_PRIMO = .f.
  w_DICONCOR = space(25)
  w_ROWORD = 0
  w_ROWNUM = 0
  w_SERIAL = space(10)
  w_VECCHIOTIPO = .f.
  w_SepRec = space(2)
  w_DESCEST = space(1)
  w_DatPre = ctod("  /  /  ")
  w_NomeFile = space(40)
  w_NomeFile5 = space(40)
  w_NomeFile4 = space(40)
  w_NomeFile7 = space(40)
  w_FLIBAN = space(1)
  w_FLQUALI = space(1)
  w_Separa = space(2)
  w_Decimali = 0
  w_Handle = 0
  w_Record = space(120)
  w_AziAtt = space(1)
  w_AziSia = space(5)
  w_BanAbi = space(5)
  w_BanCab = space(5)
  w_ConCor = space(12)
  w_CliAbi = space(5)
  w_CliCab = space(5)
  w_CliFis = space(16)
  w_CliCod = space(16)
  w_CLIConCor = space(12)
  w_DatCre = space(6)
  w_DatSca = space(6)
  w_NomSup = space(20)
  w_CodDiv = space(1)
  w_Totale = 0
  w_NumDis = space(7)
  w_NumRec = space(7)
  w_Valore = 0
  w_Lunghezza = 0
  w_StrInt = space(1)
  w_StrDec = space(1)
  w_RifDoc = space(1)
  w_TotDoc = space(1)
  w_CodFis = space(16)
  w_ParIva = space(12)
  w_MESS = space(200)
  w_OK = .f.
  w_CIN = space(1)
  w_CABCLI = space(5)
  w_OK1 = .f.
  w_CLIFIS = space(16)
  w_COFAZI = space(16)
  w_OK3 = .f.
  w_OK4 = .f.
  w_PIVAZI = space(11)
  w_LSERIAL = space(10)
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_SEGNO = space(1)
  w_NUMPAR = space(31)
  w_LDATSCA = ctod("  /  /  ")
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_RIF = space(40)
  w_BANCA = space(35)
  w_BANCA1 = space(35)
  w_CINABI = space(1)
  w_BBAN = space(30)
  w_BA__IBAN = space(35)
  w_ChekIban = space(2)
  w_AN__IBAN = space(35)
  w_Chek = space(2)
  w_ROWORD = 0
  w_ROWNUM = 0
  w_TIPDIS = space(1)
  w_PTNUMEFF = 0
  w_Cabonurg = space(1)
  w_BANAPP = space(10)
  w_BLOCCO = .f.
  w_CODCUC = space(8)
  w_ROWEFFE = 0
  w_PTLOCALI = space(254)
  w_DIDATDIS = ctod("  /  /  ")
  w_PTTOTIMP = 0
  w_CODISO = space(3)
  w_FLBEST = space(1)
  w_BODATSCA = ctod("  /  /  ")
  w_DECIMALI = space(2)
  w_TIPDOCU1 = space(3)
  w_TIPDOCU2 = space(3)
  w_TIPDOCU3 = space(3)
  w_TIPDOCU4 = space(3)
  w_CODOCU1 = space(17)
  w_CODOCU2 = space(17)
  w_CODOCU3 = space(17)
  w_CODOCU4 = space(17)
  w_DATDOCU1 = ctod("  /  /  ")
  w_DATDOCU2 = ctod("  /  /  ")
  w_DATDOCU3 = ctod("  /  /  ")
  w_DATDOCU4 = ctod("  /  /  ")
  w_DATA1 = space(8)
  w_DATA2 = space(8)
  w_DATA3 = space(8)
  w_DATA4 = space(8)
  w_NOCVS = space(1)
  w_CAUBON = space(5)
  w_PTCODVAL = space(5)
  w_PTBANAPP = space(5)
  w_PTNUMCOR = space(5)
  w_STFILXSD = space(254)
  w_MSGVAL = space(0)
  w_FILEXML = space(100)
  w_FLCAU = space(1)
  w_CASTRUTT = space(10)
  w_NOCLOSE = .f.
  * --- WorkFile variables
  ATTIMAST_idx=0
  AZIENDA_idx=0
  CONTI_idx=0
  DIS_TINT_idx=0
  DIS_BOES_idx=0
  BAN_CHE_idx=0
  COC_MAST_idx=0
  DIS_BECV_idx=0
  VALUTE_idx=0
  BAN_CONTI_idx=0
  PAR_TITE_idx=0
  PAR_EFFE_idx=0
  VASTRUTT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione file BONIFICI ESTERI secondo tracciato CBI
    * --- Variabili del batch precedente
    this.w_TIPODIS = this.oParentObject.oParentObject.w_TIPDIS
    this.w_SERIALE = this.oParentObject.w_Numdis
    this.w_DatPre = this.oParentObject.oParentObject.w_DatPre
    this.w_NomeFile = this.oParentObject.oParentObject.w_NomeFile
    * --- Variabili locali
    this.w_Separa = iif(this.w_SEPREC="S", chr(13)+chr(10), "")
    this.w_Decimali = GETVALUT( this.oParentObject.w_Valuta, "VADECTOT" )
    this.w_Handle = 0
    this.w_Record = space(120)
    this.w_AziAtt = space(35)
    this.w_AziSia = this.oParentObject.w_CodSia
    this.w_BanAbi = space(5)
    this.w_BanCab = space(5)
    this.w_ConCor = space(12)
    this.w_DatCre = space(6)
    this.w_DatSca = space(6)
    this.w_DatVal = space(6)
    this.w_NomSup = space(20)
    this.w_CodDiv = space(1)
    this.w_Totale = 0
    this.w_NumDis = "0000000"
    this.w_Valore = 0
    this.w_Lunghezza = 0
    this.w_StrInt = space(1)
    this.w_StrDec = space(1)
    this.w_RifDoc = space(1)
    this.w_TotDoc = space(1)
    this.w_OK = .F.
    this.w_OK1 = .F.
    * --- Inserimento nuove variabili
    this.w_Datval1 = space(8)
    this.w_DatEse = space(8)
    this.w_DatCre1 = space(6)
    this.w_DITIPDES = space(3)
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    this.w_VECCHIOTIPO = .F.
    * --- Variabili della maschera
    this.w_SepRec = this.oParentObject.oParentObject.w_SepRec
    this.w_DatPre = this.oParentObject.oParentObject.w_DatPre
    this.w_FLIBAN = this.oParentObject.oParentObject.w_FLIBAN
    this.w_FLQUALI = this.oParentObject.oParentObject.w_FLQUALI
    this.w_NomeFile = this.oParentObject.oParentObject.w_NomeFile
    this.w_NomeFile5 = this.oParentObject.oParentObject.w_NomeFile5
    this.w_NomeFile4 = this.oParentObject.oParentObject.w_NomeFile4
    this.w_NomeFile7 = this.oParentObject.oParentObject.w_NomeFile7
    * --- Variabili del batch precedente
    * --- Variabili locali
    this.w_Separa = iif(this.w_SEPREC="S", chr(13)+chr(10), "")
    this.w_Decimali = GETVALUT(this.oParentObject.w_Valuta, "VADECTOT")
    this.w_Handle = 0
    this.w_Record = space(120)
    this.w_AziAtt = space(35)
    this.w_AziSia = this.oParentObject.w_CodSia
    this.w_BanAbi = space(5)
    this.w_BanCab = space(5)
    this.w_ConCor = space(12)
    this.w_CliAbi = space(5)
    this.w_CliCab = space(5)
    this.w_CliFis = space(16)
    this.w_CliCod = space(16)
    this.w_CLIConCor = space(12)
    this.w_DatCre = space(6)
    this.w_DatSca = space(6)
    this.w_NomSup = space(20)
    this.w_CodDiv = space(1)
    this.w_Totale = 0
    this.w_NumDis = "0000000"
    this.w_NumRec = "0000000"
    this.w_Valore = 0
    this.w_Lunghezza = 0
    this.w_StrInt = space(1)
    this.w_StrDec = space(1)
    this.w_RifDoc = space(1)
    this.w_TotDoc = space(1)
    this.w_CodFis = space(16)
    this.w_ParIva = space(12)
    this.w_OK = .F.
    this.w_CIN = SPACE(1)
    this.w_CABCLI = SPACE(5)
    this.w_OK3 = .F.
    this.w_OK1 = .F.
    this.w_OK4 = .F.
    this.w_COFAZI = space(16)
    this.w_PIVAZI = "00000000000"
    this.w_CINABI = SPACE(1)
    this.w_BBAN = SPACE(30)
    this.w_BA__IBAN = SPACE(35)
    this.w_ChekIban = space(2)
    this.w_AN__IBAN = SPACE(35)
    this.w_Chek = space(2)
    this.oParentObject.w_GENERA = .F.
    this.w_BANCA = this.oParentObject.w_BADESC
    this.w_BANCA1 = STRTRAN(this.w_BANCA, "'", " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1, '"', " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"?" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"!" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"/" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"\" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"*" , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,"," , " ")
    this.w_BANCA1 = STRTRAN(this.w_BANCA1,":" , " ")
    this.w_TIPDIS = this.oParentObject.oParentObject.w_TIPDIS
    this.w_CASTRUTT = this.oParentObject.w_CASTRUTT
    this.w_Cabonurg = iif(This.oParentObject.oParentObject.w_Cabonurg="S","U"," ")
    * --- Controllo che il tipo CVS non sia 'REG'  (poiche non piu gestito )
    if this.w_TIPDIS="BE"
      * --- Select from DIS_BECV
      i_nConn=i_TableProp[this.DIS_BECV_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIS_BECV_idx,2],.t.,this.DIS_BECV_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select COUNT (*) as CONTA  from "+i_cTable+" DIS_BECV ";
            +" where DINUMDIS="+cp_ToStrODBC(this.w_SERIALE)+" AND DITIPCVS='REG'";
             ,"_Curs_DIS_BECV")
      else
        select COUNT (*) as CONTA from (i_cTable);
         where DINUMDIS=this.w_SERIALE AND DITIPCVS="REG";
          into cursor _Curs_DIS_BECV
      endif
      if used('_Curs_DIS_BECV')
        select _Curs_DIS_BECV
        locate for 1=1
        do while not(eof())
        if CONTA>0
          this.w_VECCHIOTIPO = .T.
        endif
          select _Curs_DIS_BECV
          continue
        enddo
        use
      endif
      if this.w_VECCHIOTIPO
        * --- Messaggistica di Errore e blocco la generazione.
        ah_ErrorMsg("Tipo CVS= 'Regol. di CVS gi� emessa' non ammesso!%0L' operazione � stata annullata","!","")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Creazione file Bonifici
    ah_Msg("Creazione file bonifici...",.T.)
    do case
      case this.oParentObject.w_TIPOBAN="C" AND EMPTY(ALLTRIM(this.oParentObject.w_PATHFILE2))
        if !DIRECTORY(ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.oParentObject.w_COBANC))
          MKDIR(ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.oParentObject.w_COBANC))
        endif
        this.oParentObject.w_PATHFILE = ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.oParentObject.w_COBANC)+ ALLTRIM(IIF(EMPTY(this.oParentObject.w_COBANC), " ", "\"))
      case this.oParentObject.w_TIPOBAN="D" AND EMPTY(ALLTRIM(this.oParentObject.w_PATHFILE2))
        if !DIRECTORY(ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.w_BANCA1))
          MKDIR(ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.w_BANCA1))
        endif
        this.oParentObject.w_PATHFILE = ALLTRIM(this.oParentObject.w_PATHFILE) + ALLTRIM(this.w_BANCA1)+ ALLTRIM(IIF(EMPTY(this.w_BANCA1), " ", "\"))
    endcase
    if this.oParentObject.w_TIPOPAG="S" AND EMPTY(ALLTRIM(this.oParentObject.w_PATHFILE2))
      if !DIRECTORY(ALLTRIM(this.oParentObject.w_PATHFILE) +IIF(this.w_TIPDIS<>"BE","\SCE","\BONI"))
        MKDIR(ALLTRIM(this.oParentObject.w_PATHFILE) +IIF(this.w_TIPDIS<>"BE","\SCE","\BONI"))
      endif
      this.oParentObject.w_PATHFILE = Addbs(ALLTRIM(this.oParentObject.w_PATHFILE) + IIF(this.w_TIPDIS<>"BE","SCE\","BONI\"))
    endif
    if this.w_TIPDIS="BE"
      if EMPTY(ALLTRIM(this.oParentObject.w_PATHFILE2))
        this.w_Handle =fcreate(ALLTRIM(this.oParentObject.w_PATHFILE) + this.oParentObject.w_PATHFILE1, 0)
      else
        this.w_Handle = fcreate(ALLTRIM(this.oParentObject.w_PATHFILE) + this.oParentObject.w_PATHFILE2 , 0)
      endif
      if this.w_Handle = -1
        ah_ErrorMsg("Non � possibile creare il file dei bonifici",,"")
        i_retcode = 'stop'
        return
      endif
    endif
    * --- Verifico la valuta della distinta.Se questa non � Lire oppure Euro non 
    *     effettuo nessuna generazione dei file BONIFICO.
    if this.oParentObject.w_Valuta<>G_PERVAL and this.w_TIPDIS="BE"
      * --- Messaggistica di Errore e blocco la generazione.
      ah_ErrorMsg("Generazione non consentita per bonifici con%0valuta diversa da Lire oppure Euro","!","")
      i_retcode = 'stop'
      return
    endif
    * --- Legge Anagrafica Azienda
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZCOFAZI,AZPIVAZI"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZCOFAZI,AZPIVAZI;
        from (i_cTable) where;
            AZCODAZI = i_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
      this.w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Lettura attivita' principale azienda
    * --- Read from ATTIMAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ATTIMAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIMAST_idx,2],.t.,this.ATTIMAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ATDESATT"+;
        " from "+i_cTable+" ATTIMAST where ";
            +"ATCODATT = "+cp_ToStrODBC(g_CATAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ATDESATT;
        from (i_cTable) where;
            ATCODATT = g_CATAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_AziAtt = NVL(cp_ToDate(_read_.ATDESATT),cp_NullValue(_read_.ATDESATT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Lettura degli effetti presenti sul temporaneo e scrittura del file ascii
    select __tmp__
    go top
    * --- Record - PE (record di testa)
    * --- Codice assegnato dalla Sia all'Azienda Mittente
    this.w_AziSia = left( this.w_AziSia + space(5) , 5)
    * --- Controllo su codice assegnato dalla SIa all'Azienda Mittente (creditrice)
    if empty(NVL(this.w_AziSia,"")) and this.w_TIPDIS="BE"
      this.w_oERRORLOG.AddMsgLog("Codice Sia per azienda mittente non inserito")     
    endif
    this.w_CODCUC = __TMP__.BACODCUC
    if empty(NVL(this.w_CODCUC,"")) AND this.w_TIPDIS="SE"
      this.w_oERRORLOG.AddMsgLog("Codice CUC assegnato all'azienda mittente non inserito")     
      this.w_BLOCCO = .t.
    endif
    * --- Dati banca ricevente
    this.w_CINABI = alltrim(nvl(__tmp__.bacinabi, " "))
    this.w_BanAbi = right("00000" + alltrim(NVL(__tmp__.bacodabi,SPACE(5))), 5)
    this.w_BanCab = right("00000" + alltrim(NVL(__tmp__.bacodcab,SPACE(5))), 5)
    this.w_ConCor = Right("000000000000" + alltrim(NVL(UPPER( __tmp__.baconcor),space(12))) , 12)
    this.w_BA__IBAN = alltrim(nvl(__tmp__.ba__iban, space(35)))
    * --- Dati banca domiciliataria
    this.w_CliAbi = right("00000" + alltrim(NVL(__tmp__.clcodabi,space(5))), 5)
    * --- Controllo su numero del conto corrente 
    * --- Controllo Formale su Numero Conto Corrente
    if EMPTY(alltrim(this.w_ConCor))
      if this.w_OK=.F.
        this.w_oERRORLOG.AddMsgLog("Numero del conto corrente della filiale della banca cui devono essere inviate le disposizioni di pagamento (ns. banca) non inserito")     
        this.w_OK = .T.
      endif
    endif
    * --- Controllo su codice banca a cui devono essere inviate le disposizioni
    if this.w_BanAbi="00000"
      this.w_oERRORLOG.AddMsgLog("Codice ABI della banca a cui devono essere inviate le disposizioni di pagamento (ns. banca) non inserito")     
    endif
    * --- Data creazione file
    this.w_DatCre = dtoc(CP_TODATE(this.w_DatPre))
    this.w_DatCre = left(this.w_DatCre,2) + substr(this.w_DatCre,4,2) + right(this.w_DatCre,2)
    * --- Nome supporto
    * --- w_FILDIS - Nome FISICO del file generato, deve essere un valore univoco
    * --- w_NomSup - Nome del supporto di origine scritto nel file (Record EF) deve essere SEMPRE il nome del file generato dalla distinta EFFETTI di origine
    * --- Campo di libera composizione. Deve comunque essere univoco: data,ora,minuti,secondi,SIA
    this.w_NomSup = time()
    this.w_NomSup = substr(this.w_NomSup,1,2) + substr(this.w_NomSup,4,2) + substr(this.w_NomSup,7,2)
    this.w_NomSup = left( this.w_DatCre + this.w_NomSup + this.w_AziSia +space(20), 20)
    this.oParentObject.w_FILDIS = this.w_NomSup
    * --- Codice divisa (valuta)
    this.w_CodDiv = iif( this.oParentObject.w_Valuta=g_PERVAL , "E" , "I")
    * --- Composizione record e scrittura
    * --- Composizione record di testa e scrittura
    this.w_Record = space(1)+"PE"
    this.w_Record = this.w_Record + this.w_AziSia + this.w_BanAbi + this.w_DatCre + this.w_NomSup
    this.w_Record = this.w_Record + space(6) + space(75)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Ciclo sugli effetti da presentare
    ah_Msg("Scrittura file bonifici esteri...",.T.)
    this.w_ROWEFFE = 0
     
 select __tmp__ 
 go top 
 scan
    * --- Non scrivo il messaggio progressivo
    *     Totale Importi 
    this.w_PTLOCALI = iif(NVL(__tmp__.DDTIPRIF," ")="PA", alltrim(Nvl(__tmp__.DDPROVIN," ")) +" "+ alltrim(Nvl(__tmp__.DD___CAP," ")) +" "+ alltrim(Nvl(__tmp__.DDLOCALI," "))+" "+alltrim(Nvl(__tmp__.DDINDIRI," ")), Alltrim(Nvl(__tmp__.ANPROVIN," ")) +" "+ alltrim(Nvl(__tmp__.AN___CAP," ")) + " " + alltrim(Nvl(__tmp__.ANLOCALI," "))+" "+alltrim(NVL(__tmp__.ANINDIRI," "))) 
    this.w_SERIAL = __tmp__.PTSERIAL
    this.w_ROWORD = __tmp__.PTROWORD
    this.w_ROWEFFE = this.w_ROWEFFE + 1
    this.w_ROWNUM = __tmp__.CPROWNUM
    this.w_LDATSCA = __tmp__.PTDATSCA
    this.w_TOTALE = this.w_TOTALE + NVL(__tmp__.PTTOTIMP,0)
    * --- Inizializzo l'importo che viene inserito nel record P1
    this.w_IMPORTO = NVL(__tmp__.PTTOTIMP,0)
    * --- Record - (H1)
    *     Header messaggio
    this.w_NumDis = right( "0000000" + alltrim( str(val(this.w_NumDis)+1) ) , 7)
    * --- Leggo l'identificativo univoco e gli altri dati richiesti per la generazione del bonifico estero
    this.w_DIDATDIS = __TMP__.DIDATDIS
    this.w_PTTOTIMP = NVL(__tmp__.PTTOTIMP,0)
    * --- Read from DIS_BOES
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DIS_BOES_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_BOES_idx,2],.t.,this.DIS_BOES_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DIORDINE,DIFUNUFF,DINOMINA,DICONTAT,DITIPAVV,DIALTER1,DIALTER2,DIALTER3,DIDATESE,DITIPOPA,DIBANBEN,DICONCOR,DICOMMIS,DITIPDES"+;
        " from "+i_cTable+" DIS_BOES where ";
            +"DINUMDIS = "+cp_ToStrODBC(this.w_SERIAL);
            +" and DIROWORD = "+cp_ToStrODBC(this.w_ROWORD);
            +" and DIROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DIORDINE,DIFUNUFF,DINOMINA,DICONTAT,DITIPAVV,DIALTER1,DIALTER2,DIALTER3,DIDATESE,DITIPOPA,DIBANBEN,DICONCOR,DICOMMIS,DITIPDES;
        from (i_cTable) where;
            DINUMDIS = this.w_SERIAL;
            and DIROWORD = this.w_ROWORD;
            and DIROWNUM = this.w_ROWNUM;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_IDENTIFI = NVL(cp_ToDate(_read_.DIORDINE),cp_NullValue(_read_.DIORDINE))
      this.w_UFFREF = NVL(cp_ToDate(_read_.DIFUNUFF),cp_NullValue(_read_.DIFUNUFF))
      this.w_NOMINAT = NVL(cp_ToDate(_read_.DINOMINA),cp_NullValue(_read_.DINOMINA))
      this.w_CONTATTO = NVL(cp_ToDate(_read_.DICONTAT),cp_NullValue(_read_.DICONTAT))
      this.w_AVVISO = NVL(cp_ToDate(_read_.DITIPAVV),cp_NullValue(_read_.DITIPAVV))
      this.w_INFALTER1 = NVL(cp_ToDate(_read_.DIALTER1),cp_NullValue(_read_.DIALTER1))
      this.w_INFALTER2 = NVL(cp_ToDate(_read_.DIALTER2),cp_NullValue(_read_.DIALTER2))
      this.w_INFALTER3 = NVL(cp_ToDate(_read_.DIALTER3),cp_NullValue(_read_.DIALTER3))
      this.w_DATAESEC = NVL(cp_ToDate(_read_.DIDATESE),cp_NullValue(_read_.DIDATESE))
      this.w_MODPAG = NVL(cp_ToDate(_read_.DITIPOPA),cp_NullValue(_read_.DITIPOPA))
      this.w_BANBENE = NVL(cp_ToDate(_read_.DIBANBEN),cp_NullValue(_read_.DIBANBEN))
      this.w_DICONCOR = NVL(cp_ToDate(_read_.DICONCOR),cp_NullValue(_read_.DICONCOR))
      this.w_MODADD = NVL(cp_ToDate(_read_.DICOMMIS),cp_NullValue(_read_.DICOMMIS))
      this.w_DITIPDES = NVL(cp_ToDate(_read_.DITIPDES),cp_NullValue(_read_.DITIPDES))
      this.w_DATESE = NVL(cp_ToDate(_read_.DIDATESE),cp_NullValue(_read_.DIDATESE))
      use
      if i_Rows=0
        this.w_IDENTIFI = RIGHT(REPLICATE("0",34)+ALLTRIM(STR(NVL(__TMP__.PTNUMEFF,0))),34)
        this.w_UFFREF = SPACE(17)
        this.w_NOMINAT = SPACE(35)
        this.w_CONTATTO = "BC"
        this.w_AVVISO = "AP"
        this.w_INFALTER1 = SPACE(35)
        this.w_INFALTER2 = SPACE(35)
        this.w_INFALTER3 = SPACE(35)
        this.w_DATAESEC = this.w_DIDATDIS
        this.w_MODPAG = "BB"
        this.w_BANBENE = __tmp__.banapp
        this.w_DICONCOR = SPACE(25)
        this.w_MODADD = "15"
        this.w_DITIPDES = "203"
      endif
    else
      * --- Error: sql sentence error.
      i_Error = 'DIS_BOES'
      return
    endif
    select (i_nOldArea)
    * --- l'DENTIFICATIVO NON � DATO DAL CAMPO NELLA TABELLA MA DAL NUMERO EFFETTO
    this.w_IDENTIFI = RIGHT(REPLICATE("0",34)+ALLTRIM(STR(NVL(__TMP__.PTNUMEFF,0))),34)
    * --- Leggo la data valuta e di esecuzione  modalit� pagamento/addebito pi� tutte le varibili
    *     che servono per compilare gli altri records
    * --- Read from DIS_TINT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DIS_TINT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_TINT_idx,2],.t.,this.DIS_TINT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DIDATVAL,DICODVAL,DIBANRIF"+;
        " from "+i_cTable+" DIS_TINT where ";
            +"DINUMDIS = "+cp_ToStrODBC(this.w_SERIALE);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DIDATVAL,DICODVAL,DIBANRIF;
        from (i_cTable) where;
            DINUMDIS = this.w_SERIALE;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATAVALU = NVL(cp_ToDate(_read_.DIDATVAL),cp_NullValue(_read_.DIDATVAL))
      this.w_DIVIMP = NVL(cp_ToDate(_read_.DICODVAL),cp_NullValue(_read_.DICODVAL))
      this.w_CONTESO = NVL(cp_ToDate(_read_.DIBANRIF),cp_NullValue(_read_.DIBANRIF))
      this.w_DICODVAL = NVL(cp_ToDate(_read_.DICODVAL),cp_NullValue(_read_.DICODVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = 'DIS_TINT'
      return
    endif
    select (i_nOldArea)
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VACODVAL,VACODISO"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_DICODVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VACODVAL,VACODISO;
        from (i_cTable) where;
            VACODVAL = this.w_DICODVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DIVBONIF = NVL(cp_ToDate(_read_.VACODVAL),cp_NullValue(_read_.VACODVAL))
      this.w_CODISO = NVL(cp_ToDate(_read_.VACODISO),cp_NullValue(_read_.VACODISO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = 'VALUTE'
      return
    endif
    select (i_nOldArea)
    this.w_DIVBONIF = IIF(EMPTY(NVL(this.w_DIVBONIF,"   ")),this.w_DICODVAL,this.w_DIVBONIF)
    this.w_MODPAG = IIF(this.w_MODPAG="BB", "  ", this.w_MODPAG)
    * --- Leggo il paese di appartenenza della banca del beneficiario
    * --- Read from BAN_CHE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.BAN_CHE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BAN_CHE_idx,2],.t.,this.BAN_CHE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "BAPASEUR,BAFLBEST"+;
        " from "+i_cTable+" BAN_CHE where ";
            +"BACODBAN = "+cp_ToStrODBC(__tmp__.banapp);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        BAPASEUR,BAFLBEST;
        from (i_cTable) where;
            BACODBAN = __tmp__.banapp;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODPAESE = NVL(cp_ToDate(_read_.BAPASEUR),cp_NullValue(_read_.BAPASEUR))
      this.w_FLBEST = NVL(cp_ToDate(_read_.BAFLBEST),cp_NullValue(_read_.BAFLBEST))
      use
    else
      * --- Error: sql sentence error.
      i_Error = 'BAN_CHE'
      return
    endif
    select (i_nOldArea)
    * --- Calcolo del valore che devomo avere le posizioni 107-109 
    this.w_NAZBANCA = IIF(NVL(this.w_FLBEST," ")="S", "IN", "DO")
    * --- Leggo il paese di appartenenza del beneficiario
    this.w_TIPCON = NVL(__TMP__.PNTIPCON," ")
    this.w_CODCON = NVL(__TMP__.CODCLIEN," ")
    this.w_PTNUMEFF = NVL(__TMP__.PTNUMEFF,0)
    VQ_EXEC("..\QUERY\GSTE_BRE.VQR",this,"PAESECLI")
    * --- Data creazione file
    this.w_DatCre1 = dtoc(CP_TODATE(this.w_DatPre))
    this.w_DatCre1 = right(this.w_DatCre1,4)+ substr(this.w_DatCre1,4,2) + left(this.w_DatCre1,2) 
    * --- Data di esecuzione
    this.w_BODATSCA = this.w_DatEse
    this.w_DatEse = dtoc(cp_todate(this.w_DATAESEC))
    this.w_DatEse = right(this.w_DatEse,4)+ substr(this.w_DatEse,4,2) + left(this.w_DatEse,2) 
    * --- Data valuta
    this.w_Datval1 = dtoc(CP_TODATE(this.w_DATAVALU))
    this.w_Datval1 = right(this.w_DatVal1,4)+ substr(this.w_DatVal1,4,2) + left(this.w_DatVal1,2) 
    * --- Composizione record e scrittura
    *     Controllo il tipo data Esecuzione
    if this.w_DITIPDES="203" or this.w_DITIPDES="228"
      this.w_Record = " H1" + this.w_NumDis + "PAYORD"+"D  "+"93A"+"UN"+"450"+this.w_IDENTIFI+space(1)
      this.w_Record = this.w_Record + "9 "+"137"+this.w_DatCre1+"102"+this.w_DITIPDES+this.w_DatEse+"102"+"   "
      this.w_Record = this.w_Record + space(8) +"   "+this.w_NAZBANCA + space(1)+ "1  "+space(3)+"00000"
    else
      this.w_Record = " H1" + this.w_NumDis + "PAYORD"+"D  "+"93A"+"UN"+"450"+this.w_IDENTIFI+space(1)
      this.w_Record = this.w_Record + "9 "+"137"+this.w_DatCre1+"102"+"   "+space(8)+"   "+this.w_DITIPDES
      this.w_Record = this.w_Record + this.w_DatEse +"102"+this.w_NAZBANCA + space(1)+ "1  "+space(3)+"00000"
    endif
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record - (P0)
    *     Modalit� di pagamento
    * --- Composizione record e scrittura
    this.w_Record = " P0" + this.w_NumDis +this.w_MODPAG+space(1)+this.w_MODADD+space(1)+space(29)+space(75)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record - (P1)
    * --- Importi
    this.w_RELDIVIS = "9  "
    this.w_Record = " P1" + this.w_NumDis + this.w_RELDIVIS+IIF(this.w_IMPORTO=0,"000000000000000,00",right(repl("0",18)+alltrim(str(this.w_IMPORTO,18,2)),18))+this.w_CODISO+space(6)+"            "+space(6)
    this.w_Record = this.w_Record+space(12)+space(11)+space(39)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if EMPTY(this.w_CODISO)
      this.w_oERRORLOG.AddMsgLog("Codice ISO della valuta mancante")     
    endif
    * --- Record - (PH)
    *     Banche e C/C interessati espressi in forma codificata
    * --- Leggo il codice IBAN e BIC del beneficiario
    * --- Read from BAN_CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.BAN_CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.BAN_CONTI_idx,2],.t.,this.BAN_CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CCCOIBAN,CCCODBIC"+;
        " from "+i_cTable+" BAN_CONTI where ";
            +"CCTIPCON = "+cp_ToStrODBC(__tmp__.pntipcon);
            +" and CCCODCON = "+cp_ToStrODBC(__tmp__.codclien);
            +" and CCCODBAN = "+cp_ToStrODBC(__tmp__.banapp);
            +" and CCCONCOR = "+cp_ToStrODBC(__tmp__.annumcor);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CCCOIBAN,CCCODBIC;
        from (i_cTable) where;
            CCTIPCON = __tmp__.pntipcon;
            and CCCODCON = __tmp__.codclien;
            and CCCODBAN = __tmp__.banapp;
            and CCCONCOR = __tmp__.annumcor;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_IBANBEN = NVL(cp_ToDate(_read_.CCCOIBAN),cp_NullValue(_read_.CCCOIBAN))
      this.w_BICBEN = NVL(cp_ToDate(_read_.CCCODBIC),cp_NullValue(_read_.CCCODBIC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = 'BAN_CONTI'
      return
    endif
    select (i_nOldArea)
    this.w_IBANBEN = LEFT(NVL(this.w_IBANBEN,SPACE(34))+SPACE(34),34)
    this.w_BICBEN = LEFT(NVL(this.w_BICBEN,SPACE(11))+SPACE(11),11)
    * --- Leggo il codice IBAN e BIC associato al conto di tesoreria
    * --- Read from COC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.COC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "BA__IBAN,BACODBIC"+;
        " from "+i_cTable+" COC_MAST where ";
            +"BACODBAN = "+cp_ToStrODBC(this.w_CONTESO);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        BA__IBAN,BACODBIC;
        from (i_cTable) where;
            BACODBAN = this.w_CONTESO;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_IBANORD = NVL(cp_ToDate(_read_.BA__IBAN),cp_NullValue(_read_.BA__IBAN))
      this.w_BICORD = NVL(cp_ToDate(_read_.BACODBIC),cp_NullValue(_read_.BACODBIC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = 'COC_MAST'
      return
    endif
    select (i_nOldArea)
    this.w_IBANORD = LEFT(NVL(this.w_IBANORD,SPACE(34))+SPACE(34),34)
    this.w_BICORD = LEFT(NVL(this.w_BICORD,SPACE(11))+SPACE(11),11)
    this.w_Record = " PH"+this.w_NUMDIS+this.w_IBANORD+this.w_BICORD+space(5)+this.w_IBANBEN+this.w_BICBEN+space(15)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record - (P5)
    *     Beneficiario.
    * --- Leggo i dati relativi al beneficiario
    * --- Read from CONTI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CONTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ANDESCRI,ANINDIRI,ANLOCALI"+;
        " from "+i_cTable+" CONTI where ";
            +"ANTIPCON = "+cp_ToStrODBC(__tmp__.pntipcon);
            +" and ANCODICE = "+cp_ToStrODBC(__tmp__.codclien);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ANDESCRI,ANINDIRI,ANLOCALI;
        from (i_cTable) where;
            ANTIPCON = __tmp__.pntipcon;
            and ANCODICE = __tmp__.codclien;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_RAGSOC = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
      this.w_INDIRIZ = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
      this.w_LOCALITA = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = 'CONTI'
      return
    endif
    select (i_nOldArea)
    this.w_RAGSOC = LEFT(NVL(this.w_RAGSOC,SPACE(40))+SPACE(35),35)
    this.w_INDIRIZ = LEFT(NVL(this.w_INDIRIZ,SPACE(35))+SPACE(35),35)
    this.w_LOCALITA = LEFT(NVL(this.w_LOCALITA,SPACE(32))+SPACE(32),32)
    * --- Composizione record e scrittura
    this.w_Record = " P5"+ this.w_NumDis + "BE "+this.w_RAGSOC+this.w_INDIRIZ+this.w_LOCALITA+space(5)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record - (P6) Istruzioni speciali di pagamento
    this.w_UFFREF = NVL(this.w_UFFREF,SPACE(17))
    this.w_NOMINAT = NVL(this.w_NOMINAT,SPACE(35))
    this.w_Record = " P6"+ this.w_NumDis + "BF "+ "1  "+this.w_AVVISO+space(1)+this.W_CONTATTO+space(1)+this.w_UFFREF+this.w_NOMINAT+space(46)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Record - (P7)
    *     Riferimenti bonifico (in testo libero) per il beneficiario
    this.w_INFALTER1 = nvl(this.w_INFALTER1,SPACE(35))
    this.w_INFALTER2 = nvl(this.w_INFALTER2,SPACE(35))
    this.w_INFALTER3 = nvl(this.w_INFALTER3,SPACE(35))
    this.w_Record = " P7" + this.w_NumDis + "PMD"+this.w_INFALTER1+this.w_INFALTER2+this.w_INFALTER3+SPACE(2)
    if NOT EMPTY(this.w_INFALTER1) OR NOT EMPTY(this.w_INFALTER2) OR NOT EMPTY(this.w_INFALTER3)
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Record - (P8)
    *     Riferimenti bonifico (strutturato) per il beneficiario
    * --- Leggo le informazioni relative ai documenti
    * --- Read from DIS_BOES
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DIS_BOES_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIS_BOES_idx,2],.t.,this.DIS_BOES_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "DITIPDO1,DITIPDO2,DITIPDO3,DITIPDO4,DICODDO1,DICODDO2,DICODDO3,DICODDO4,DIDATDO1,DIDATDO2,DIDATDO3,DIDATDO4"+;
        " from "+i_cTable+" DIS_BOES where ";
            +"DINUMDIS = "+cp_ToStrODBC(this.w_SERIAL);
            +" and DIROWORD = "+cp_ToStrODBC(this.w_ROWORD);
            +" and DIROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        DITIPDO1,DITIPDO2,DITIPDO3,DITIPDO4,DICODDO1,DICODDO2,DICODDO3,DICODDO4,DIDATDO1,DIDATDO2,DIDATDO3,DIDATDO4;
        from (i_cTable) where;
            DINUMDIS = this.w_SERIAL;
            and DIROWORD = this.w_ROWORD;
            and DIROWNUM = this.w_ROWNUM;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TIPDOCU1 = NVL(cp_ToDate(_read_.DITIPDO1),cp_NullValue(_read_.DITIPDO1))
      this.w_TIPDOCU2 = NVL(cp_ToDate(_read_.DITIPDO2),cp_NullValue(_read_.DITIPDO2))
      this.w_TIPDOCU3 = NVL(cp_ToDate(_read_.DITIPDO3),cp_NullValue(_read_.DITIPDO3))
      this.w_TIPDOCU4 = NVL(cp_ToDate(_read_.DITIPDO4),cp_NullValue(_read_.DITIPDO4))
      this.w_CODOCU1 = NVL(cp_ToDate(_read_.DICODDO1),cp_NullValue(_read_.DICODDO1))
      this.w_CODOCU2 = NVL(cp_ToDate(_read_.DICODDO2),cp_NullValue(_read_.DICODDO2))
      this.w_CODOCU3 = NVL(cp_ToDate(_read_.DICODDO3),cp_NullValue(_read_.DICODDO3))
      this.w_CODOCU4 = NVL(cp_ToDate(_read_.DICODDO4),cp_NullValue(_read_.DICODDO4))
      this.w_DATDOCU1 = NVL(cp_ToDate(_read_.DIDATDO1),cp_NullValue(_read_.DIDATDO1))
      this.w_DATDOCU2 = NVL(cp_ToDate(_read_.DIDATDO2),cp_NullValue(_read_.DIDATDO2))
      this.w_DATDOCU3 = NVL(cp_ToDate(_read_.DIDATDO3),cp_NullValue(_read_.DIDATDO3))
      this.w_DATDOCU4 = NVL(cp_ToDate(_read_.DIDATDO4),cp_NullValue(_read_.DIDATDO4))
      use
      if i_Rows=0
        this.w_TIPDOCU1 = SPACE(3)
        this.w_TIPDOCU2 = SPACE(3)
        this.w_TIPDOCU3 = SPACE(3)
        this.w_TIPDOCU4 = SPACE(3)
        this.w_CODOCU1 = SPACE(17)
        this.w_CODOCU2 = SPACE(17)
        this.w_CODOCU3 = SPACE(17)
        this.w_CODOCU4 = SPACE(17)
      endif
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TIPDOCU1 = iif(this.w_TIPDOCU1="384","315",nvl(this.w_TIPDOCU1,SPACE(3)))
    this.w_TIPDOCU2 = iif(this.w_TIPDOCU2="384","315",nvl(this.w_TIPDOCU2,SPACE(3)))
    this.w_TIPDOCU3 = iif(this.w_TIPDOCU3="384","315",nvl(this.w_TIPDOCU3,SPACE(3)))
    this.w_TIPDOCU4 = iif(this.w_TIPDOCU4="384","315",nvl(this.w_TIPDOCU4,SPACE(3)))
    this.w_CODOCU1 = nvl(this.w_CODOCU1,SPACE(17))
    this.w_CODOCU2 = nvl(this.w_CODOCU2,SPACE(17))
    this.w_CODOCU3 = nvl(this.w_CODOCU3,SPACE(17))
    this.w_CODOCU4 = nvl(this.w_CODOCU4,SPACE(17))
    if ! EMPTY(this.w_TIPDOCU1)
      this.w_DATA1 = dtoc(CP_TODATE(this.w_DATDOCU1))
      this.w_DATA1 = right(this.w_DATA1,4)+ substr(this.w_DATA1,4,2) + left(this.w_DATA1,2) 
    endif
    if ! EMPTY(this.w_TIPDOCU2)
      this.w_DATA2 = dtoc(CP_TODATE(this.w_DATDOCU2))
      this.w_DATA2 = right(this.w_DATA2,4)+ substr(this.w_DATA2,4,2) + left(this.w_DATA2,2) 
    endif
    if ! EMPTY(this.w_TIPDOCU3)
      this.w_DATA3 = dtoc(CP_TODATE(this.w_DATDOCU3))
      this.w_DATA3 = right(this.w_DATA3,4)+ substr(this.w_DATA3,4,2) + left(this.w_DATA3,2) 
    endif
    if ! EMPTY(this.w_TIPDOCU4)
      this.w_DATA4 = dtoc(CP_TODATE(this.w_DATDOCU4))
      this.w_DATA4 = right(this.w_DATA4,4)+ substr(this.w_DATA4,4,2) + left(this.w_DATA4,2) 
    endif
    this.w_Record = " P8" + this.w_NumDis + this.w_TIPDOCU1+this.w_CODOCU1+"171"+this.w_DATA1+"102"+space(16)
    this.w_Record = this.w_RECORD+this.w_TIPDOCU2+this.w_CODOCU2+IIF(NOT EMPTY(this.w_TIPDOCU2),"171","")+this.w_DATA2+IIF(NOT EMPTY(this.w_TIPDOCU2),"102","")+SPACE(26)
    if NOT EMPTY(this.w_TIPDOCU1) OR NOT EMPTY(this.w_TIPDOCU2)
      * --- Scrivo il primo record
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if NOT EMPTY(this.w_TIPDOCU3) OR NOT EMPTY(this.w_TIPDOCU4)
      this.w_Record = " P8" + this.w_NumDis + this.w_TIPDOCU3+this.w_CODOCU3+"171"+this.w_DATA3+"102"+space(16)
      this.w_Record = this.w_RECORD+this.w_TIPDOCU4+this.w_CODOCU4+IIF(NOT EMPTY(this.w_TIPDOCU4),"171","")+this.w_DATA4+IIF(NOT EMPTY(this.w_TIPDOCU4),"102","")+SPACE(26)
      * --- Scrivo il secondo record
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    this.w_NOCVS = __TMP__.CA_NOCVS
    if this.w_NOCVS<>"S"
      * --- Record - (P9) Dettagli CVS
      this.w_PRIMO = .t.
      this.w_ROWORD = __TMP__.PTROWORD
      this.w_ROWNUM = __TMP__.CPROWNUM
      * --- Select from DIS_BECV
      i_nConn=i_TableProp[this.DIS_BECV_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIS_BECV_idx,2],.t.,this.DIS_BECV_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" DIS_BECV ";
            +" where DINUMDIS="+cp_ToStrODBC(this.w_SERIALE)+" AND DIROWNUM="+cp_ToStrODBC(this.w_ROWNUM)+" AND DIROWORD="+cp_ToStrODBC(this.w_ROWORD)+"";
            +" order by CPROWNUM";
             ,"_Curs_DIS_BECV")
      else
        select * from (i_cTable);
         where DINUMDIS=this.w_SERIALE AND DIROWNUM=this.w_ROWNUM AND DIROWORD=this.w_ROWORD;
         order by CPROWNUM;
          into cursor _Curs_DIS_BECV
      endif
      if used('_Curs_DIS_BECV')
        select _Curs_DIS_BECV
        locate for 1=1
        do while not(eof())
        this.w_Record = " P9"+ this.w_NumDis + "ZZ "+ "RA "+NVL(_Curs_DIS_BECV.DITIPCVS,"REG")
        this.w_DATA4 = SPACE(8)
        this.w_Record = this.w_Record+this.w_DATA4
        if EMPTY(NVL(_Curs_DIS_BECV.DIDATSDO,cp_CharToDate("  -  -    ")))
          this.w_DATA4 = SPACE(8)
        else
          this.w_DATA4 = dtoc(CP_TODATE(_Curs_DIS_BECV.DIDATSDO))
          this.w_DATA4 = right(this.w_DATA4,4)+ substr(this.w_DATA4,4,2) + left(this.w_DATA4,2) 
        endif
        this.w_Record = this.w_Record+this.w_DATA4+IIF(NVL(_Curs_DIS_BECV.DITARDOG,0)=0,space(7),right(repl("0",7)+alltrim(str(INT(_Curs_DIS_BECV.DITARDOG))),7))
        this.w_Record = this.w_Record+space(12)+space(5)+left(nvl(_Curs_DIS_BECV.DICAUUIC,"")+space(5),5)
        this.w_Record = this.w_Record+IIF(NVL(_Curs_DIS_BECV.DIIMPORT,0)=0,"000000000000000,00",right(repl("0",18)+alltrim(str(_Curs_DIS_BECV.DIIMPORT,18,2)),18))
        this.w_Record = this.w_Record+ space(20)
        if this.w_PRIMO
          if USED("PAESECLI")
            Select PAESECLI 
 Go Top
            if EMPTY(NVL(COPAEBEN,"ITL")) and this.w_TIPDIS="BE"
              this.w_oERRORLOG.AddMsgLog("Codifica UIC della nazione mancante. Verr� codificato con ITL")     
            endif
            this.w_COPAEBEN = IIF(EMPTY(NVL(COPAEBEN,"ITL")),"ITL",NVL(COPAEBEN,"ITL"))
            Select PAESECLI 
 Use
          else
            this.w_COPAEBEN = "ITL"
          endif
          this.w_Record = this.w_Record+this.w_COPAEBEN+space(15)
          this.w_PRIMO = .f.
        else
          this.w_Record = this.w_Record+"   "+space(15)
        endif
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- DA METTRE INSERT SE FACCIO XML
          select _Curs_DIS_BECV
          continue
        enddo
        use
      endif
      if this.w_TIPDIS = "SE"
        do case
          case Not Empty(NVL(__TMP__.ANFLRAGG,"")) AND __TMP__.NUMGRUP>1
            this.w_SEGNO = IIF(NVL(__TMP__.PT_SEGNO, " ")="D","A","D")
            this.w_NUMPAR = NVL(__TMP__.PTNUMPAR, SPACE(31))
            this.w_RifDoc = ah_Msgformat("DOC:") +" "
            VQ_EXEC("QUERY\GSTE4BRF.VQR",this,"temp1")
            SELECT TEMP1
            GO TOP
            SCAN
            this.w_NUMDOC = NVL(PNNUMDOC, 0)
            this.w_ALFDOC = NVL(PNALFDOC,SPACE(10))
            this.w_DATDOC = CP_TODATE(PNDATDOC)
            this.w_SEGNO = IIF(NVL(PT_SEGNO," ")="A","D","A")
            this.w_RifDoc = alltrim(this.w_RifDoc) + alltrim(str(NVL(temp1.pnnumdoc, 0),15,0))
            this.w_RifDoc = alltrim(this.w_RifDoc)+ iif( empty(NVL(temp1.pnalfdoc,space(10))) , "", "/" + Alltrim( NVL(temp1.pnalfdoc,space(10)) ) )
            this.w_RifDoc = left(alltrim(this.w_RifDoc) + "-" + dtoc(CP_TODATE(temp1.pndatdoc)), 140)
            ENDSCAN
            if USED("TEMP1")
              SELECT TEMP1
              USE
            endif
          case NVL(__TMP__.PTFLRAGG, "")="S" 
            if NVL(__TMP__.PNNUMDOC, 0)>0
              this.w_RifDoc = ah_Msgformat("DOC:") +" "
              this.w_RifDoc = alltrim(this.w_RifDoc) + alltrim(str(NVL(__tmp__.PNNUMDOC, 0),15,0))
              this.w_RifDoc = alltrim(this.w_RifDoc)+ iif( empty(NVL(__tmp__.PNALFDOC,space(10))) , "", "/" + Alltrim( NVL(__tmp__.PNALFDOC,space(10)) ) )
              this.w_RifDoc = left(alltrim(this.w_RifDoc) + "-" + dtoc(CP_TODATE(__tmp__.PNDATDOC)), 70)
            else
              this.w_SEGNO = IIF(NVL(__TMP__.PT_SEGNO, " ")="D","A","D")
              this.w_CODCON = NVL(__TMP__.CODCLIEN, SPACE(15))
              this.w_TIPCON = NVL(__TMP__.PNTIPCON, " ")
              this.w_NUMPAR = NVL(__TMP__.PTNUMPAR, SPACE(31))
              this.w_LDATSCA = CP_TODATE(__TMP__.PTDATSCA)
              * --- Select from PAR_TITE
              i_nConn=i_TableProp[this.PAR_TITE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select PTSERIAL  from "+i_cTable+" PAR_TITE ";
                    +" where PTNUMPAR="+cp_ToStrODBC(this.w_NUMPAR)+" AND PTDATSCA="+cp_ToStrODBC(this.w_LDATSCA)+" AND PTTIPCON="+cp_ToStrODBC(this.w_TIPCON)+" AND PTCODCON="+cp_ToStrODBC(this.w_CODCON)+" AND PT_SEGNO="+cp_ToStrODBC(this.w_SEGNO)+" AND PTROWORD =-1 AND PTFLRAGG='S'";
                    +" order by PTFLRAGG";
                     ,"_Curs_PAR_TITE")
              else
                select PTSERIAL from (i_cTable);
                 where PTNUMPAR=this.w_NUMPAR AND PTDATSCA=this.w_LDATSCA AND PTTIPCON=this.w_TIPCON AND PTCODCON=this.w_CODCON AND PT_SEGNO=this.w_SEGNO AND PTROWORD =-1 AND PTFLRAGG="S";
                 order by PTFLRAGG;
                  into cursor _Curs_PAR_TITE
              endif
              if used('_Curs_PAR_TITE')
                select _Curs_PAR_TITE
                locate for 1=1
                do while not(eof())
                this.w_LSERIAL = _Curs_PAR_TITE.PTSERIAL
                  select _Curs_PAR_TITE
                  continue
                enddo
                use
              endif
              this.w_RifDoc = ah_Msgformat("DOC:") +" "
              VQ_EXEC("QUERY\GSTE2BRF.VQR",this,"temp")
              SELECT TEMP
              GO TOP
              SCAN
              this.w_SEGNO = IIF(NVL(PT_SEGNO," ")="A","D","A")
              this.w_RifDoc = alltrim(this.w_RifDoc) + alltrim(str(NVL(temp.ptnumdoc, 0),15,0))
              this.w_RifDoc = alltrim(this.w_RifDoc)+ iif( empty(NVL(temp.ptalfdoc,space(10))) , "", "/" + Alltrim( NVL(temp.ptalfdoc,space(10)) ) )
              this.w_RifDoc = left(alltrim(this.w_RifDoc) + "-" + dtoc(CP_TODATE(temp.ptdatdoc)), 140)
              ENDSCAN
              if USED("TEMP")
                SELECT TEMP
                USE
              endif
            endif
          otherwise
            this.w_CAUBON = NVL(__TMP__.CACAUBON,SPACE(5))
            if EMPTY(NVL(__TMP__.PTDESRIG, SPACE(59)))
              if ALLTRIM(this.w_CAUBON)$"48000-27000"
                this.w_RifDoc = left(ah_Msgformat("Rif. fattura n. %1 del %2", alltrim(str(NVL(__tmp__.pnnumdoc,0),15,0)) + iif( empty(NVL(__tmp__.pnalfdoc,space(10))) , "", "/" +Alltrim( NVL(__tmp__.pnalfdoc,space(10))) ), dtoc(CP_TODATE(__tmp__.pndatdoc)) + space(140)), 140) 
              else
                this.w_RifDoc = left(ah_Msgformat("PER LA FATTURA N. %1 DEL %2", alltrim(str(NVL(__tmp__.pnnumdoc,0),15,0)) + iif( empty(NVL(__tmp__.pnalfdoc,space(10))) , "", "/" +Alltrim( NVL(__tmp__.pnalfdoc,space(10))) ), dtoc(CP_TODATE(__tmp__.pndatdoc)) + space(140)), 140) 
              endif
            else
              this.w_RifDoc = LEFT(__TMP__.PTDESRIG+SPACE(140),140)
            endif
        endcase
      endif
      if this.w_PRIMO and this.w_TIPDIS="BE"
        this.w_oERRORLOG.AddMsgLog("Il prospetto CVS non risulta valorizzato: controllare la distinta'")     
      endif
    endif
    this.w_PTCODVAL = __TMP__.PTCODVAL
    this.w_PTNUMCOR = __TMP__.PTNUMCOR
    this.w_PTBANAPP = __TMP__.BANAPP
    if this.w_TIPDIS = "SE"
      * --- Try
      local bErr_0401A570
      bErr_0401A570=bTrsErr
      this.Try_0401A570()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_0401A570
      * --- End
    endif
    EndScan
    this.w_Record = space(1)+"EF"
    this.w_Record = this.w_Record + this.w_AziSia + this.w_BanAbi + this.w_DatCre + this.w_NomSup + space(6) 
    this.w_NumRec = right( "0000000" + alltrim( str(val(this.w_NumRec)+1) ) , 7)
    this.w_Record = this.w_Record + this.w_NumDis + space(12) + IIF(NVL(this.w_Totale,0)=0,"000000000000000,00",right(repl("0",18)+alltrim(str(this.w_Totale,18,2)),18)) + this.w_NumRec + space(31)
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    NAMEFILE=ALLTRIM(this.oParentObject.w_PATHFILE) + IIF(EMPTY(ALLTRIM(this.w_NOMEFILE5)), ALLTRIM(this.w_NOMEFILE4), ALLTRIM(this.w_NOMEFILE7))
    * --- Try
    local bErr_04026E10
    bErr_04026E10=bTrsErr
    this.Try_04026E10()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_04026E10
    * --- End
    if this.w_TIPDIS="SE"
      this.w_NOCLOSE = .F.
    else
      this.w_NOCLOSE = .not. fclose(this.w_Handle)
    endif
    if this.w_NOCLOSE OR this.w_oErrorLog.IsFullLog()
      if this.w_TIPDIS="SE"
        this.w_oERRORLOG.PrintLog(this,"Errori riscontrati",.T., "File Sepa Credit Transfer generato con errori%0Stampo situazione messaggi di errore?")     
      else
        this.w_oERRORLOG.PrintLog(this,"Errori riscontrati",.T., "File bonifici generato con errori%0Stampo situazione messaggi di errore?")     
      endif
      if NOT ah_YesNo("Mantengo comunque il file generato?")
        DELETE FILE (NAMEFILE)
        this.oParentObject.w_GENERA = .T.
        this.oParentObject.w_FILE=" "
      endif
    else
      if this.w_TIPDIS="SE"
        ah_ErrorMsg("Generazione file Sepa Credit Transfer terminata","","")
      else
        ah_ErrorMsg("Generazione file bonifici terminata","","")
      endif
    endif
  endproc
  proc Try_0401A570()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PAR_EFFE
    i_nConn=i_TableProp[this.PAR_EFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_EFFE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_EFFE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PTTOTIMP"+",PTCODVAL"+",PTBANAPP"+",PTNUMEFF"+",PTDATPRE"+",PTRIFDOC"+",PTFLIBAN"+",PTNUMCOR"+",PTIBADEB"+",PTBONURG"+",PTIBANSE"+",PTMODADD"+",PTMESGID"+",PTLOCALI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'PAR_EFFE','PTSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(-2),'PAR_EFFE','PTROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_ROWEFFE),'PAR_EFFE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_BODATSCA),'PAR_EFFE','PTDATSCA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'PAR_EFFE','PTTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'PAR_EFFE','PTCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_EFFE','PTTOTIMP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODVAL),'PAR_EFFE','PTCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANAPP),'PAR_EFFE','PTBANAPP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMEFF),'PAR_EFFE','PTNUMEFF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DatPre),'PAR_EFFE','PTDATPRE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_RifDoc),'PAR_EFFE','PTRIFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_FLIBAN),'PAR_EFFE','PTFLIBAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMCOR),'PAR_EFFE','PTNUMCOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IBANBEN),'PAR_EFFE','PTIBADEB');
      +","+cp_NullLink(cp_ToStrODBC(this.w_Cabonurg),'PAR_EFFE','PTBONURG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_IBANORD),'PAR_EFFE','PTIBANSE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MODADD),'PAR_EFFE','PTMODADD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_FILDIS),'PAR_EFFE','PTMESGID');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PTLOCALI),'PAR_EFFE','PTLOCALI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_SERIAL,'PTROWORD',-2,'CPROWNUM',this.w_ROWEFFE,'PTDATSCA',this.w_BODATSCA,'PTTIPCON',this.w_TIPCON,'PTCODCON',this.w_CODCON,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.w_PTCODVAL,'PTBANAPP',this.w_PTBANAPP,'PTNUMEFF',this.w_PTNUMEFF,'PTDATPRE',this.w_DatPre,'PTRIFDOC',this.w_RifDoc)
      insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTDATSCA,PTTIPCON,PTCODCON,PTTOTIMP,PTCODVAL,PTBANAPP,PTNUMEFF,PTDATPRE,PTRIFDOC,PTFLIBAN,PTNUMCOR,PTIBADEB,PTBONURG,PTIBANSE,PTMODADD,PTMESGID,PTLOCALI &i_ccchkf. );
         values (;
           this.w_SERIAL;
           ,-2;
           ,this.w_ROWEFFE;
           ,this.w_BODATSCA;
           ,this.w_TIPCON;
           ,this.w_CODCON;
           ,this.w_PTTOTIMP;
           ,this.w_PTCODVAL;
           ,this.w_PTBANAPP;
           ,this.w_PTNUMEFF;
           ,this.w_DatPre;
           ,this.w_RifDoc;
           ,this.w_FLIBAN;
           ,this.w_PTNUMCOR;
           ,this.w_IBANBEN;
           ,this.w_Cabonurg;
           ,this.w_IBANORD;
           ,this.w_MODADD;
           ,this.oParentObject.w_FILDIS;
           ,this.w_PTLOCALI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_04026E10()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Generazione XML
    if this.w_TIPDIS = "SE" and !this.w_BLOCCO
      this.w_FILEXML = FORCEEXT(ADDBS(JUSTPATH(Alltrim(NAMEFILE)))+Juststem(Alltrim(NAMEFILE)),"xml")
      if Not Empty(this.w_CASTRUTT)
        GSTE_BGF(this,this.w_SERIAL,this.w_CASTRUTT,this.w_FILEXML)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if ! cp_fileexist(this.w_FILEXML)
          this.w_oERRORLOG.AddMsgLog("Generazione file Xml non avvenuta")     
        else
          * --- Read from VASTRUTT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VASTRUTT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VASTRUTT_idx,2],.t.,this.VASTRUTT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "STFILXSD"+;
              " from "+i_cTable+" VASTRUTT where ";
                  +"STCODICE = "+cp_ToStrODBC(this.w_CASTRUTT);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              STFILXSD;
              from (i_cTable) where;
                  STCODICE = this.w_CASTRUTT;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_STFILXSD = NVL(cp_ToDate(_read_.STFILXSD),cp_NullValue(_read_.STFILXSD))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_MSGVAL = " "
          this.w_MSGVAL = ValidateXml(this.w_FILEXML,Fullpath(this.w_STFILXSD))
          if Not Empty(this.w_MSGVAL)
            this.w_oERRORLOG.AddMsgLog("Errore di validazione XML: %1",Alltrim(this.w_MSGVAL))     
          endif
        endif
      else
        this.w_oERRORLOG.AddMsgLog("Codice Strutta CBI/SEPA non presente nella causale distinta associata")     
      endif
    endif
    * --- begin transaction
    cp_BeginTrs()
    if this.w_TIPDIS = "SE"
      * --- Delete from PAR_EFFE
      i_nConn=i_TableProp[this.PAR_EFFE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_EFFE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        delete from (i_cTable) where;
              PTSERIAL = this.w_SERIAL;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura record su file ascii
    *     Scrittura record su File BONI
    if this.w_TIPDIS="BE"
      w_t = fwrite(this.w_Handle, this.w_Record+this.w_Separa)
      this.w_NumRec = right( "0000000" + alltrim( str(val(this.w_NumRec)+1) ) , 7)
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Trasformazione degli importi nel formato richiesto dal tracciato Bonifici
    *     UTILIZZO
    *     Prima della chiamata a questa pagina del batch devono essere impostate le variabili:
    *     w_Valore          = importo da convertire in stringa
    *     w_Lunghezza  = lunghezza della stringa
    *     Il risultato della conversione � disponibile nella variabile w_Valore.
    *     CONVENZIONI
    *     La virgola deve essere eliminata
    *     Se la valuta � Euro le ultime due cifre rappresentano i decimali
    *     Il valore deve essere allineato a destra
    *     E' richiesto il riempimento con 0 delle cifre non significative
    *     Calcolo parte intera
    this.w_StrInt = alltrim( str( int(this.w_Valore), this.w_Lunghezza, 0 ) )
    * --- Calcolo parte decimale
    this.w_StrDec = ""
    if this.w_Decimali > 0
      this.w_StrDec = right( str( this.w_Valore, this.w_Lunghezza, this.w_Decimali ) , this.w_Decimali )
    endif
    * --- Risultato
    this.w_Valore = right( repl("0",this.w_Lunghezza) + this.w_StrInt + this.w_StrDec , this.w_Lunghezza )
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,13)]
    this.cWorkTables[1]='ATTIMAST'
    this.cWorkTables[2]='AZIENDA'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='DIS_TINT'
    this.cWorkTables[5]='DIS_BOES'
    this.cWorkTables[6]='BAN_CHE'
    this.cWorkTables[7]='COC_MAST'
    this.cWorkTables[8]='DIS_BECV'
    this.cWorkTables[9]='VALUTE'
    this.cWorkTables[10]='BAN_CONTI'
    this.cWorkTables[11]='PAR_TITE'
    this.cWorkTables[12]='PAR_EFFE'
    this.cWorkTables[13]='VASTRUTT'
    return(this.OpenAllTables(13))

  proc CloseCursors()
    if used('_Curs_DIS_BECV')
      use in _Curs_DIS_BECV
    endif
    if used('_Curs_DIS_BECV')
      use in _Curs_DIS_BECV
    endif
    if used('_Curs_PAR_TITE')
      use in _Curs_PAR_TITE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
