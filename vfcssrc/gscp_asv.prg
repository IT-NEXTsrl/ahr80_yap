* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_asv                                                        *
*              Pubblicazione servizi spese                                     *
*                                                                              *
*      Author: Gianluca Bellani                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-10-21                                                      *
* Last revis.: 2016-05-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscp_asv")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscp_asv")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscp_asv")
  return

* --- Class definition
define class tgscp_asv as StdPCForm
  Width  = 631
  Height = 118
  Top    = 3
  Left   = 10
  cComment = "Pubblicazione servizi spese"
  cPrg = "gscp_asv"
  HelpContextID=109679977
  add object cnt as tcgscp_asv
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscp_asv as PCContext
  w_SVCODAZI = space(5)
  w_SVSERTRA = space(20)
  w_SVSERINC = space(20)
  w_SVSERBOL = space(20)
  w_SVSERIMB = space(20)
  w_SVDESTRA = space(50)
  w_SVDESINC = space(50)
  w_SVDESBOL = space(50)
  w_SVDESIMB = space(50)
  w_TIPARTTRA = space(2)
  w_PUBWEBTRA = space(1)
  w_TIPARTINC = space(2)
  w_PUBWEBINC = space(1)
  w_TIPARTBOL = space(2)
  w_PUBWEBBOL = space(1)
  w_TIPARTIMB = space(2)
  w_PUBWEBIMB = space(1)
  proc Save(oFrom)
    this.w_SVCODAZI = oFrom.w_SVCODAZI
    this.w_SVSERTRA = oFrom.w_SVSERTRA
    this.w_SVSERINC = oFrom.w_SVSERINC
    this.w_SVSERBOL = oFrom.w_SVSERBOL
    this.w_SVSERIMB = oFrom.w_SVSERIMB
    this.w_SVDESTRA = oFrom.w_SVDESTRA
    this.w_SVDESINC = oFrom.w_SVDESINC
    this.w_SVDESBOL = oFrom.w_SVDESBOL
    this.w_SVDESIMB = oFrom.w_SVDESIMB
    this.w_TIPARTTRA = oFrom.w_TIPARTTRA
    this.w_PUBWEBTRA = oFrom.w_PUBWEBTRA
    this.w_TIPARTINC = oFrom.w_TIPARTINC
    this.w_PUBWEBINC = oFrom.w_PUBWEBINC
    this.w_TIPARTBOL = oFrom.w_TIPARTBOL
    this.w_PUBWEBBOL = oFrom.w_PUBWEBBOL
    this.w_TIPARTIMB = oFrom.w_TIPARTIMB
    this.w_PUBWEBIMB = oFrom.w_PUBWEBIMB
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_SVCODAZI = this.w_SVCODAZI
    oTo.w_SVSERTRA = this.w_SVSERTRA
    oTo.w_SVSERINC = this.w_SVSERINC
    oTo.w_SVSERBOL = this.w_SVSERBOL
    oTo.w_SVSERIMB = this.w_SVSERIMB
    oTo.w_SVDESTRA = this.w_SVDESTRA
    oTo.w_SVDESINC = this.w_SVDESINC
    oTo.w_SVDESBOL = this.w_SVDESBOL
    oTo.w_SVDESIMB = this.w_SVDESIMB
    oTo.w_TIPARTTRA = this.w_TIPARTTRA
    oTo.w_PUBWEBTRA = this.w_PUBWEBTRA
    oTo.w_TIPARTINC = this.w_TIPARTINC
    oTo.w_PUBWEBINC = this.w_PUBWEBINC
    oTo.w_TIPARTBOL = this.w_TIPARTBOL
    oTo.w_PUBWEBBOL = this.w_PUBWEBBOL
    oTo.w_TIPARTIMB = this.w_TIPARTIMB
    oTo.w_PUBWEBIMB = this.w_PUBWEBIMB
    PCContext::Load(oTo)
enddefine

define class tcgscp_asv as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 631
  Height = 118
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-05-20"
  HelpContextID=109679977
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=17

  * --- Constant Properties
  SERVAL_IDX = 0
  ART_ICOL_IDX = 0
  cFile = "SERVAL"
  cKeySelect = "SVCODAZI"
  cKeyWhere  = "SVCODAZI=this.w_SVCODAZI"
  cKeyWhereODBC = '"SVCODAZI="+cp_ToStrODBC(this.w_SVCODAZI)';

  cKeyWhereODBCqualified = '"SERVAL.SVCODAZI="+cp_ToStrODBC(this.w_SVCODAZI)';

  cPrg = "gscp_asv"
  cComment = "Pubblicazione servizi spese"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_SVCODAZI = space(5)
  w_SVSERTRA = space(20)
  o_SVSERTRA = space(20)
  w_SVSERINC = space(20)
  o_SVSERINC = space(20)
  w_SVSERBOL = space(20)
  o_SVSERBOL = space(20)
  w_SVSERIMB = space(20)
  o_SVSERIMB = space(20)
  w_SVDESTRA = space(50)
  w_SVDESINC = space(50)
  w_SVDESBOL = space(50)
  w_SVDESIMB = space(50)
  w_TIPARTTRA = space(2)
  w_PUBWEBTRA = space(1)
  w_TIPARTINC = space(2)
  w_PUBWEBINC = space(1)
  w_TIPARTBOL = space(2)
  w_PUBWEBBOL = space(1)
  w_TIPARTIMB = space(2)
  w_PUBWEBIMB = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscp_asvPag1","gscp_asv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Servizi a valore")
      .Pages(1).HelpContextID = 257131589
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSVSERTRA_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='SERVAL'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SERVAL_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SERVAL_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscp_asv'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_2_joined
    link_1_2_joined=.f.
    local link_1_4_joined
    link_1_4_joined=.f.
    local link_1_5_joined
    link_1_5_joined=.f.
    local link_1_6_joined
    link_1_6_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from SERVAL where SVCODAZI=KeySet.SVCODAZI
    *
    i_nConn = i_TableProp[this.SERVAL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SERVAL_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SERVAL')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SERVAL.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SERVAL '
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_6_joined=this.AddJoinedLink_1_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SVCODAZI',this.w_SVCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TIPARTTRA = space(2)
        .w_TIPARTINC = space(2)
        .w_TIPARTBOL = space(2)
        .w_TIPARTIMB = space(2)
        .w_SVCODAZI = NVL(SVCODAZI,space(5))
        .w_SVSERTRA = NVL(SVSERTRA,space(20))
          if link_1_2_joined
            this.w_SVSERTRA = NVL(ARCODART102,NVL(this.w_SVSERTRA,space(20)))
            this.w_SVDESTRA = NVL(ARDESART102,space(50))
            this.w_TIPARTTRA = NVL(ARTIPART102,space(2))
            this.w_PUBWEBTRA = NVL(ARPUBWEB102,space(1))
          else
          .link_1_2('Load')
          endif
        .w_SVSERINC = NVL(SVSERINC,space(20))
          if link_1_4_joined
            this.w_SVSERINC = NVL(ARCODART104,NVL(this.w_SVSERINC,space(20)))
            this.w_SVDESINC = NVL(ARDESART104,space(50))
            this.w_TIPARTINC = NVL(ARTIPART104,space(2))
            this.w_PUBWEBINC = NVL(ARPUBWEB104,space(1))
          else
          .link_1_4('Load')
          endif
        .w_SVSERBOL = NVL(SVSERBOL,space(20))
          if link_1_5_joined
            this.w_SVSERBOL = NVL(ARCODART105,NVL(this.w_SVSERBOL,space(20)))
            this.w_SVDESBOL = NVL(ARDESART105,space(50))
            this.w_TIPARTBOL = NVL(ARTIPART105,space(2))
            this.w_PUBWEBBOL = NVL(ARPUBWEB105,space(1))
          else
          .link_1_5('Load')
          endif
        .w_SVSERIMB = NVL(SVSERIMB,space(20))
          if link_1_6_joined
            this.w_SVSERIMB = NVL(ARCODART106,NVL(this.w_SVSERIMB,space(20)))
            this.w_SVDESIMB = NVL(ARDESART106,space(50))
            this.w_TIPARTIMB = NVL(ARTIPART106,space(2))
            this.w_PUBWEBIMB = NVL(ARPUBWEB106,space(1))
          else
          .link_1_6('Load')
          endif
        .w_SVDESTRA = NVL(SVDESTRA,space(50))
        .w_SVDESINC = NVL(SVDESINC,space(50))
        .w_SVDESBOL = NVL(SVDESBOL,space(50))
        .w_SVDESIMB = NVL(SVDESIMB,space(50))
        .w_PUBWEBTRA = 'N'
        .w_PUBWEBINC = 'N'
        .w_PUBWEBBOL = 'N'
        .w_PUBWEBIMB = 'N'
        cp_LoadRecExtFlds(this,'SERVAL')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_SVCODAZI = space(5)
      .w_SVSERTRA = space(20)
      .w_SVSERINC = space(20)
      .w_SVSERBOL = space(20)
      .w_SVSERIMB = space(20)
      .w_SVDESTRA = space(50)
      .w_SVDESINC = space(50)
      .w_SVDESBOL = space(50)
      .w_SVDESIMB = space(50)
      .w_TIPARTTRA = space(2)
      .w_PUBWEBTRA = space(1)
      .w_TIPARTINC = space(2)
      .w_PUBWEBINC = space(1)
      .w_TIPARTBOL = space(2)
      .w_PUBWEBBOL = space(1)
      .w_TIPARTIMB = space(2)
      .w_PUBWEBIMB = space(1)
      if .cFunction<>"Filter"
        .w_SVCODAZI = i_CODAZI
        .DoRTCalc(2,2,.f.)
          if not(empty(.w_SVSERTRA))
          .link_1_2('Full')
          endif
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_SVSERINC))
          .link_1_4('Full')
          endif
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_SVSERBOL))
          .link_1_5('Full')
          endif
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_SVSERIMB))
          .link_1_6('Full')
          endif
          .DoRTCalc(6,10,.f.)
        .w_PUBWEBTRA = 'N'
          .DoRTCalc(12,12,.f.)
        .w_PUBWEBINC = 'N'
          .DoRTCalc(14,14,.f.)
        .w_PUBWEBBOL = 'N'
          .DoRTCalc(16,16,.f.)
        .w_PUBWEBIMB = 'N'
      endif
    endwith
    cp_BlankRecExtFlds(this,'SERVAL')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oSVSERTRA_1_2.enabled = i_bVal
      .Page1.oPag.oSVSERINC_1_4.enabled = i_bVal
      .Page1.oPag.oSVSERBOL_1_5.enabled = i_bVal
      .Page1.oPag.oSVSERIMB_1_6.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'SERVAL',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SERVAL_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SVCODAZI,"SVCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SVSERTRA,"SVSERTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SVSERINC,"SVSERINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SVSERBOL,"SVSERBOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SVSERIMB,"SVSERIMB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SVDESTRA,"SVDESTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SVDESINC,"SVDESINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SVDESBOL,"SVDESBOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SVDESIMB,"SVDESIMB",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SERVAL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SERVAL_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.SERVAL_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SERVAL
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SERVAL')
        i_extval=cp_InsertValODBCExtFlds(this,'SERVAL')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SVCODAZI,SVSERTRA,SVSERINC,SVSERBOL,SVSERIMB"+;
                  ",SVDESTRA,SVDESINC,SVDESBOL,SVDESIMB "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_SVCODAZI)+;
                  ","+cp_ToStrODBCNull(this.w_SVSERTRA)+;
                  ","+cp_ToStrODBCNull(this.w_SVSERINC)+;
                  ","+cp_ToStrODBCNull(this.w_SVSERBOL)+;
                  ","+cp_ToStrODBCNull(this.w_SVSERIMB)+;
                  ","+cp_ToStrODBC(this.w_SVDESTRA)+;
                  ","+cp_ToStrODBC(this.w_SVDESINC)+;
                  ","+cp_ToStrODBC(this.w_SVDESBOL)+;
                  ","+cp_ToStrODBC(this.w_SVDESIMB)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SERVAL')
        i_extval=cp_InsertValVFPExtFlds(this,'SERVAL')
        cp_CheckDeletedKey(i_cTable,0,'SVCODAZI',this.w_SVCODAZI)
        INSERT INTO (i_cTable);
              (SVCODAZI,SVSERTRA,SVSERINC,SVSERBOL,SVSERIMB,SVDESTRA,SVDESINC,SVDESBOL,SVDESIMB  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SVCODAZI;
                  ,this.w_SVSERTRA;
                  ,this.w_SVSERINC;
                  ,this.w_SVSERBOL;
                  ,this.w_SVSERIMB;
                  ,this.w_SVDESTRA;
                  ,this.w_SVDESINC;
                  ,this.w_SVDESBOL;
                  ,this.w_SVDESIMB;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.SERVAL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SERVAL_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.SERVAL_IDX,i_nConn)
      *
      * update SERVAL
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'SERVAL')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SVSERTRA="+cp_ToStrODBCNull(this.w_SVSERTRA)+;
             ",SVSERINC="+cp_ToStrODBCNull(this.w_SVSERINC)+;
             ",SVSERBOL="+cp_ToStrODBCNull(this.w_SVSERBOL)+;
             ",SVSERIMB="+cp_ToStrODBCNull(this.w_SVSERIMB)+;
             ",SVDESTRA="+cp_ToStrODBC(this.w_SVDESTRA)+;
             ",SVDESINC="+cp_ToStrODBC(this.w_SVDESINC)+;
             ",SVDESBOL="+cp_ToStrODBC(this.w_SVDESBOL)+;
             ",SVDESIMB="+cp_ToStrODBC(this.w_SVDESIMB)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'SERVAL')
        i_cWhere = cp_PKFox(i_cTable  ,'SVCODAZI',this.w_SVCODAZI  )
        UPDATE (i_cTable) SET;
              SVSERTRA=this.w_SVSERTRA;
             ,SVSERINC=this.w_SVSERINC;
             ,SVSERBOL=this.w_SVSERBOL;
             ,SVSERIMB=this.w_SVSERIMB;
             ,SVDESTRA=this.w_SVDESTRA;
             ,SVDESINC=this.w_SVDESINC;
             ,SVDESBOL=this.w_SVDESBOL;
             ,SVDESIMB=this.w_SVDESIMB;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Restore Transaction
  proc mRestoreTrs(i_bCanSkip)
    local i_cWhere,i_cF,i_nConn,i_cTable
    local i_cOp1

    i_cF=this.cCursor
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..PUBWEBTRA,space(1))==this.w_PUBWEBTRA;
              and NVL(&i_cF..SVSERTRA,space(20))==this.w_SVSERTRA;

      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..SVSERTRA,space(20))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE ARCODART="+cp_ToStrODBC(NVL(&i_cF..SVSERTRA,space(20)));
             )
      endif
    else
      i_cWhere = cp_PKFox(i_cTable;
                 ,'ARCODART',&i_cF..SVSERTRA)
      UPDATE (i_cTable) SET ;
  CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..PUBWEBINC,space(1))==this.w_PUBWEBINC;
              and NVL(&i_cF..SVSERINC,space(20))==this.w_SVSERINC;

      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..SVSERINC,space(20))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE ARCODART="+cp_ToStrODBC(NVL(&i_cF..SVSERINC,space(20)));
             )
      endif
    else
      i_cWhere = cp_PKFox(i_cTable;
                 ,'ARCODART',&i_cF..SVSERINC)
      UPDATE (i_cTable) SET ;
  CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..PUBWEBBOL,space(1))==this.w_PUBWEBBOL;
              and NVL(&i_cF..SVSERBOL,space(20))==this.w_SVSERBOL;

      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..SVSERBOL,space(20))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE ARCODART="+cp_ToStrODBC(NVL(&i_cF..SVSERBOL,space(20)));
             )
      endif
    else
      i_cWhere = cp_PKFox(i_cTable;
                 ,'ARCODART',&i_cF..SVSERBOL)
      UPDATE (i_cTable) SET ;
  CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..PUBWEBIMB,space(1))==this.w_PUBWEBIMB;
              and NVL(&i_cF..SVSERIMB,space(20))==this.w_SVSERIMB;

      Local i_cValueForTrsact
      i_cValueForTrsact=NVL(&i_cF..SVSERIMB,space(20))
      if !i_bSkip and !Empty(i_cValueForTrsact)
        =cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET ";
  +"CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE ARCODART="+cp_ToStrODBC(NVL(&i_cF..SVSERIMB,space(20)));
             )
      endif
    else
      i_cWhere = cp_PKFox(i_cTable;
                 ,'ARCODART',&i_cF..SVSERIMB)
      UPDATE (i_cTable) SET ;
  CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Update Transaction
  proc mUpdateTrs(i_bCanSkip)
    local i_cWhere,i_cF,i_nModRow,i_nConn,i_cTable
    local i_cOp1

    i_cF=this.cCursor
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..PUBWEBTRA,space(1))==this.w_PUBWEBTRA;
              and NVL(&i_cF..SVSERTRA,space(20))==this.w_SVSERTRA;

      if !i_bSkip and !Empty(this.w_SVSERTRA)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" ARPUBWEB="+cp_ToStrODBC(this.w_PUBWEBTRA)  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE ARCODART="+cp_ToStrODBC(this.w_SVSERTRA);
           )
      endif
    else
      i_cWhere = cp_PKFox(i_cTable;
                 ,'ARCODART',this.w_SVSERTRA)
      UPDATE (i_cTable) SET;
           ARPUBWEB=this.w_PUBWEBTRA  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..PUBWEBINC,space(1))==this.w_PUBWEBINC;
              and NVL(&i_cF..SVSERINC,space(20))==this.w_SVSERINC;

      if !i_bSkip and !Empty(this.w_SVSERINC)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" ARPUBWEB="+cp_ToStrODBC(this.w_PUBWEBINC)  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE ARCODART="+cp_ToStrODBC(this.w_SVSERINC);
           )
      endif
    else
      i_cWhere = cp_PKFox(i_cTable;
                 ,'ARCODART',this.w_SVSERINC)
      UPDATE (i_cTable) SET;
           ARPUBWEB=this.w_PUBWEBINC  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..PUBWEBBOL,space(1))==this.w_PUBWEBBOL;
              and NVL(&i_cF..SVSERBOL,space(20))==this.w_SVSERBOL;

      if !i_bSkip and !Empty(this.w_SVSERBOL)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" ARPUBWEB="+cp_ToStrODBC(this.w_PUBWEBBOL)  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE ARCODART="+cp_ToStrODBC(this.w_SVSERBOL);
           )
      endif
    else
      i_cWhere = cp_PKFox(i_cTable;
                 ,'ARCODART',this.w_SVSERBOL)
      UPDATE (i_cTable) SET;
           ARPUBWEB=this.w_PUBWEBBOL  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    if i_nConn<>0
      local i_bSkip
      i_bSkip=i_bCanSkip ;
              and NVL(&i_cF..PUBWEBIMB,space(1))==this.w_PUBWEBIMB;
              and NVL(&i_cF..SVSERIMB,space(20))==this.w_SVSERIMB;

      if !i_bSkip and !Empty(this.w_SVSERIMB)
        i_nModRow=cp_TrsSQL(i_nConn,"UPDATE "+i_cTable+" SET"+;
         +" ARPUBWEB="+cp_ToStrODBC(this.w_PUBWEBIMB)  +",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+" WHERE ARCODART="+cp_ToStrODBC(this.w_SVSERIMB);
           )
      endif
    else
      i_cWhere = cp_PKFox(i_cTable;
                 ,'ARCODART',this.w_SVSERIMB)
      UPDATE (i_cTable) SET;
           ARPUBWEB=this.w_PUBWEBIMB  ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere
    endif
  endproc

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SERVAL_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SERVAL_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.SERVAL_IDX,i_nConn)
      *
      * delete SERVAL
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SVCODAZI',this.w_SVCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SERVAL_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SERVAL_IDX,2])
    if i_bUpd
      with this
        if .o_SVSERTRA<>.w_SVSERTRA.or. .o_SVSERIMB<>.w_SVSERIMB.or. .o_SVSERINC<>.w_SVSERINC.or. .o_SVSERBOL<>.w_SVSERBOL
          .Calculate_TOHVIXJCFK()
        endif
        .DoRTCalc(1,10,.t.)
            .w_PUBWEBTRA = 'N'
        .DoRTCalc(12,12,.t.)
            .w_PUBWEBINC = 'N'
        .DoRTCalc(14,14,.t.)
            .w_PUBWEBBOL = 'N'
        .DoRTCalc(16,16,.t.)
            .w_PUBWEBIMB = 'N'
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_TOHVIXJCFK()
    with this
          * --- Controllo univocit� servizi selezionati
          Check_Univoc(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SVSERTRA
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SVSERTRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAS',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_SVSERTRA)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARPUBWEB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_SVSERTRA))
          select ARCODART,ARDESART,ARTIPART,ARPUBWEB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SVSERTRA)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SVSERTRA) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oSVSERTRA_1_2'),i_cWhere,'GSMA_AAS',"Servizi",'GSCP_ASV.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARPUBWEB";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARPUBWEB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SVSERTRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARPUBWEB";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_SVSERTRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_SVSERTRA)
            select ARCODART,ARDESART,ARTIPART,ARPUBWEB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SVSERTRA = NVL(_Link_.ARCODART,space(20))
      this.w_SVDESTRA = NVL(_Link_.ARDESART,space(50))
      this.w_TIPARTTRA = NVL(_Link_.ARTIPART,space(2))
      this.w_PUBWEBTRA = NVL(_Link_.ARPUBWEB,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SVSERTRA = space(20)
      endif
      this.w_SVDESTRA = space(50)
      this.w_TIPARTTRA = space(2)
      this.w_PUBWEBTRA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_SVSERTRA) OR .w_TIPARTTRA='FO'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Servizio non valido")
        endif
        this.w_SVSERTRA = space(20)
        this.w_SVDESTRA = space(50)
        this.w_TIPARTTRA = space(2)
        this.w_PUBWEBTRA = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SVSERTRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.ARCODART as ARCODART102"+ ",link_1_2.ARDESART as ARDESART102"+ ",link_1_2.ARTIPART as ARTIPART102"+ ",link_1_2.ARPUBWEB as ARPUBWEB102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on SERVAL.SVSERTRA=link_1_2.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and SERVAL.SVSERTRA=link_1_2.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SVSERINC
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SVSERINC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAS',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_SVSERINC)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARPUBWEB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_SVSERINC))
          select ARCODART,ARDESART,ARTIPART,ARPUBWEB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SVSERINC)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SVSERINC) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oSVSERINC_1_4'),i_cWhere,'GSMA_AAS',"Servizi",'GSCP_ASV.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARPUBWEB";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARPUBWEB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SVSERINC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARPUBWEB";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_SVSERINC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_SVSERINC)
            select ARCODART,ARDESART,ARTIPART,ARPUBWEB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SVSERINC = NVL(_Link_.ARCODART,space(20))
      this.w_SVDESINC = NVL(_Link_.ARDESART,space(50))
      this.w_TIPARTINC = NVL(_Link_.ARTIPART,space(2))
      this.w_PUBWEBINC = NVL(_Link_.ARPUBWEB,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SVSERINC = space(20)
      endif
      this.w_SVDESINC = space(50)
      this.w_TIPARTINC = space(2)
      this.w_PUBWEBINC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_SVSERINC) OR .w_TIPARTINC='FO'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Servizio non valido")
        endif
        this.w_SVSERINC = space(20)
        this.w_SVDESINC = space(50)
        this.w_TIPARTINC = space(2)
        this.w_PUBWEBINC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SVSERINC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.ARCODART as ARCODART104"+ ",link_1_4.ARDESART as ARDESART104"+ ",link_1_4.ARTIPART as ARTIPART104"+ ",link_1_4.ARPUBWEB as ARPUBWEB104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on SERVAL.SVSERINC=link_1_4.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and SERVAL.SVSERINC=link_1_4.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SVSERBOL
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SVSERBOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAS',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_SVSERBOL)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARPUBWEB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_SVSERBOL))
          select ARCODART,ARDESART,ARTIPART,ARPUBWEB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SVSERBOL)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SVSERBOL) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oSVSERBOL_1_5'),i_cWhere,'GSMA_AAS',"Servizi",'GSCP_ASV.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARPUBWEB";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARPUBWEB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SVSERBOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARPUBWEB";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_SVSERBOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_SVSERBOL)
            select ARCODART,ARDESART,ARTIPART,ARPUBWEB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SVSERBOL = NVL(_Link_.ARCODART,space(20))
      this.w_SVDESBOL = NVL(_Link_.ARDESART,space(50))
      this.w_TIPARTBOL = NVL(_Link_.ARTIPART,space(2))
      this.w_PUBWEBBOL = NVL(_Link_.ARPUBWEB,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SVSERBOL = space(20)
      endif
      this.w_SVDESBOL = space(50)
      this.w_TIPARTBOL = space(2)
      this.w_PUBWEBBOL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_SVSERBOL) OR .w_TIPARTBOL='FO'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Servizio non valido")
        endif
        this.w_SVSERBOL = space(20)
        this.w_SVDESBOL = space(50)
        this.w_TIPARTBOL = space(2)
        this.w_PUBWEBBOL = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SVSERBOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.ARCODART as ARCODART105"+ ",link_1_5.ARDESART as ARDESART105"+ ",link_1_5.ARTIPART as ARTIPART105"+ ",link_1_5.ARPUBWEB as ARPUBWEB105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on SERVAL.SVSERBOL=link_1_5.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and SERVAL.SVSERBOL=link_1_5.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SVSERIMB
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SVSERIMB) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAS',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_SVSERIMB)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARPUBWEB";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_SVSERIMB))
          select ARCODART,ARDESART,ARTIPART,ARPUBWEB;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SVSERIMB)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SVSERIMB) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oSVSERIMB_1_6'),i_cWhere,'GSMA_AAS',"Servizi",'GSCP_ASV.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARPUBWEB";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARPUBWEB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SVSERIMB)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARPUBWEB";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_SVSERIMB);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_SVSERIMB)
            select ARCODART,ARDESART,ARTIPART,ARPUBWEB;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SVSERIMB = NVL(_Link_.ARCODART,space(20))
      this.w_SVDESIMB = NVL(_Link_.ARDESART,space(50))
      this.w_TIPARTIMB = NVL(_Link_.ARTIPART,space(2))
      this.w_PUBWEBIMB = NVL(_Link_.ARPUBWEB,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SVSERIMB = space(20)
      endif
      this.w_SVDESIMB = space(50)
      this.w_TIPARTIMB = space(2)
      this.w_PUBWEBIMB = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_SVSERIMB) OR .w_TIPARTIMB='FO'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Servizio non valido")
        endif
        this.w_SVSERIMB = space(20)
        this.w_SVDESIMB = space(50)
        this.w_TIPARTIMB = space(2)
        this.w_PUBWEBIMB = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SVSERIMB Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_6.ARCODART as ARCODART106"+ ",link_1_6.ARDESART as ARDESART106"+ ",link_1_6.ARTIPART as ARTIPART106"+ ",link_1_6.ARPUBWEB as ARPUBWEB106"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_6 on SERVAL.SVSERIMB=link_1_6.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_6"
          i_cKey=i_cKey+'+" and SERVAL.SVSERIMB=link_1_6.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSVSERTRA_1_2.value==this.w_SVSERTRA)
      this.oPgFrm.Page1.oPag.oSVSERTRA_1_2.value=this.w_SVSERTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oSVSERINC_1_4.value==this.w_SVSERINC)
      this.oPgFrm.Page1.oPag.oSVSERINC_1_4.value=this.w_SVSERINC
    endif
    if not(this.oPgFrm.Page1.oPag.oSVSERBOL_1_5.value==this.w_SVSERBOL)
      this.oPgFrm.Page1.oPag.oSVSERBOL_1_5.value=this.w_SVSERBOL
    endif
    if not(this.oPgFrm.Page1.oPag.oSVSERIMB_1_6.value==this.w_SVSERIMB)
      this.oPgFrm.Page1.oPag.oSVSERIMB_1_6.value=this.w_SVSERIMB
    endif
    if not(this.oPgFrm.Page1.oPag.oSVDESTRA_1_7.value==this.w_SVDESTRA)
      this.oPgFrm.Page1.oPag.oSVDESTRA_1_7.value=this.w_SVDESTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oSVDESINC_1_8.value==this.w_SVDESINC)
      this.oPgFrm.Page1.oPag.oSVDESINC_1_8.value=this.w_SVDESINC
    endif
    if not(this.oPgFrm.Page1.oPag.oSVDESBOL_1_9.value==this.w_SVDESBOL)
      this.oPgFrm.Page1.oPag.oSVDESBOL_1_9.value=this.w_SVDESBOL
    endif
    if not(this.oPgFrm.Page1.oPag.oSVDESIMB_1_10.value==this.w_SVDESIMB)
      this.oPgFrm.Page1.oPag.oSVDESIMB_1_10.value=this.w_SVDESIMB
    endif
    cp_SetControlsValueExtFlds(this,'SERVAL')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_SVSERTRA) OR .w_TIPARTTRA='FO')  and not(empty(.w_SVSERTRA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSVSERTRA_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Servizio non valido")
          case   not(EMPTY(.w_SVSERINC) OR .w_TIPARTINC='FO')  and not(empty(.w_SVSERINC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSVSERINC_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Servizio non valido")
          case   not(EMPTY(.w_SVSERBOL) OR .w_TIPARTBOL='FO')  and not(empty(.w_SVSERBOL))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSVSERBOL_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Servizio non valido")
          case   not(EMPTY(.w_SVSERIMB) OR .w_TIPARTIMB='FO')  and not(empty(.w_SVSERIMB))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSVSERIMB_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Servizio non valido")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SVSERTRA = this.w_SVSERTRA
    this.o_SVSERINC = this.w_SVSERINC
    this.o_SVSERBOL = this.w_SVSERBOL
    this.o_SVSERIMB = this.w_SVSERIMB
    return

enddefine

* --- Define pages as container
define class tgscp_asvPag1 as StdContainer
  Width  = 627
  height = 118
  stdWidth  = 627
  stdheight = 118
  resizeXpos=451
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSVSERTRA_1_2 as StdField with uid="NUCDNNILRN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SVSERTRA", cQueryName = "SVSERTRA",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Servizio non valido",;
    HelpContextID = 48297575,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=144, Top=4, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAS", oKey_1_1="ARCODART", oKey_1_2="this.w_SVSERTRA"

  func oSVSERTRA_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oSVSERTRA_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSVSERTRA_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oSVSERTRA_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAS',"Servizi",'GSCP_ASV.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oSVSERTRA_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_SVSERTRA
     i_obj.ecpSave()
  endproc

  add object oSVSERINC_1_4 as StdField with uid="KZLKBXTAYU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SVSERINC", cQueryName = "SVSERINC",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Servizio non valido",;
    HelpContextID = 132183657,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=144, Top=32, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAS", oKey_1_1="ARCODART", oKey_1_2="this.w_SVSERINC"

  func oSVSERINC_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSVSERINC_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSVSERINC_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oSVSERINC_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAS',"Servizi",'GSCP_ASV.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oSVSERINC_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_SVSERINC
     i_obj.ecpSave()
  endproc

  add object oSVSERBOL_1_5 as StdField with uid="BGVJOTQBTT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SVSERBOL", cQueryName = "SVSERBOL",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Servizio non valido",;
    HelpContextID = 14743154,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=144, Top=60, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAS", oKey_1_1="ARCODART", oKey_1_2="this.w_SVSERBOL"

  func oSVSERBOL_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oSVSERBOL_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSVSERBOL_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oSVSERBOL_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAS',"Servizi",'GSCP_ASV.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oSVSERBOL_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_SVSERBOL
     i_obj.ecpSave()
  endproc

  add object oSVSERIMB_1_6 as StdField with uid="INTJFYJQID",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SVSERIMB", cQueryName = "SVSERIMB",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Servizio non valido",;
    HelpContextID = 136251800,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=144, Top=88, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAS", oKey_1_1="ARCODART", oKey_1_2="this.w_SVSERIMB"

  func oSVSERIMB_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oSVSERIMB_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSVSERIMB_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oSVSERIMB_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAS',"Servizi",'GSCP_ASV.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oSVSERIMB_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_SVSERIMB
     i_obj.ecpSave()
  endproc

  add object oSVDESTRA_1_7 as StdField with uid="WYHOUMCCST",rtseq=6,rtrep=.f.,;
    cFormVar = "w_SVDESTRA", cQueryName = "SVDESTRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 49284711,;
   bGlobalFont=.t.,;
    Height=21, Width=320, Left=303, Top=4, InputMask=replicate('X',50)

  add object oSVDESINC_1_8 as StdField with uid="LGMEVKZSTI",rtseq=7,rtrep=.f.,;
    cFormVar = "w_SVDESINC", cQueryName = "SVDESINC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 133170793,;
   bGlobalFont=.t.,;
    Height=21, Width=320, Left=303, Top=32, InputMask=replicate('X',50)

  add object oSVDESBOL_1_9 as StdField with uid="XXAYLUPMUS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SVDESBOL", cQueryName = "SVDESBOL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 15730290,;
   bGlobalFont=.t.,;
    Height=21, Width=320, Left=303, Top=60, InputMask=replicate('X',50)

  add object oSVDESIMB_1_10 as StdField with uid="CPAPKUHVDQ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_SVDESIMB", cQueryName = "SVDESIMB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 135264664,;
   bGlobalFont=.t.,;
    Height=21, Width=320, Left=303, Top=88, InputMask=replicate('X',50)

  add object oStr_1_3 as StdString with uid="TMYNCVKUYH",Visible=.t., Left=10, Top=7,;
    Alignment=1, Width=132, Height=18,;
    Caption="Spese di trasporto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="HINXANCFHU",Visible=.t., Left=10, Top=34,;
    Alignment=1, Width=132, Height=18,;
    Caption="Spese di incasso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="FFZPGXXBVB",Visible=.t., Left=10, Top=61,;
    Alignment=1, Width=132, Height=18,;
    Caption="Spese bolli:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="UYZSTURDJH",Visible=.t., Left=10, Top=88,;
    Alignment=1, Width=132, Height=18,;
    Caption="Spese di imballo:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscp_asv','SERVAL','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SVCODAZI=SERVAL.SVCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscp_asv
Function Check_Univoc
  Parameters padre
  if !empty(padre.w_SVSERINC) and INLIST(padre.w_SVSERINC,padre.w_SVSERTRA,padre.w_SVSERBOL,padre.w_SVSERIMB)
     Ah_ErrorMSg('Servizio per spese di incasso gi� usato')
     padre.w_SVSERINC=' '
     padre.w_SVDESINC=' '
  endif   
  if !empty(padre.w_SVSERBOL) and INLIST(padre.w_SVSERBOL,padre.w_SVSERINC,padre.w_SVSERTRA,padre.w_SVSERIMB)
      Ah_ErrorMSg('Servizio per spese bolli gi� usato')
      padre.w_SVSERBOL=' '
      padre.w_SVDESBOL=''
  endif  
  if !empty(padre.w_SVSERIMB) and INLIST(padre.w_SVSERIMB,padre.w_SVSERTRA,padre.w_SVSERBOL,padre.w_SVSERINC)
     Ah_ErrorMSg('Servizio per spese imballi gi� usato')  
     padre.w_SVSERIMB=' '
      padre.w_SVDESIMB=''
  endif
  if !empty(padre.w_SVSERTRA) and INLIST(padre.w_SVSERTRA,padre.w_SVSERBOL,padre.w_SVSERINC,padre.w_SVSERIMB)
     Ah_ErrorMSg('Servizio per spese di trasporto gi� usato')
     padre.w_SVSERTRA=' '
     padre.w_SVDESTRA=' '
  endif  
endfunc  
* --- Fine Area Manuale
