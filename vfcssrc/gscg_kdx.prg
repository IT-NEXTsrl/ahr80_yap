* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kdx                                                        *
*              Aggiornamento dati integrativa                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2017-07-28                                                      *
* Last revis.: 2017-11-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kdx",oParentObject))

* --- Class definition
define class tgscg_kdx as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 627
  Height = 315+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-11-16"
  HelpContextID=82453865
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_kdx"
  cComment = "Aggiornamento dati integrativa"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_AGG_INTEGRA = space(1)
  w_DFIMPDIC = 0
  o_DFIMPDIC = 0
  w_DFPROTEC = space(17)
  o_DFPROTEC = space(17)
  w_DFPRODOC = 0
  w_DFNOTOPE = space(100)
  w_DFCODUTE = 0
  w_DFDATOPE = ctod('  /  /  ')
  w_IMPDIC = 0
  w_PROTEC = space(17)
  w_PRODOC = 0
  w_SERIALE_DICH = space(15)
  w_DFIMPUTI = 0
  w_MSG_IMPORTO = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kdxPag1","gscg_kdx",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati integrativa")
      .Pages(2).addobject("oPag","tgscg_kdxPag2","gscg_kdx",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettaglio dati integrativa")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDFIMPDIC_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AGG_INTEGRA=space(1)
      .w_DFIMPDIC=0
      .w_DFPROTEC=space(17)
      .w_DFPRODOC=0
      .w_DFNOTOPE=space(100)
      .w_DFCODUTE=0
      .w_DFDATOPE=ctod("  /  /  ")
      .w_IMPDIC=0
      .w_PROTEC=space(17)
      .w_PRODOC=0
      .w_SERIALE_DICH=space(15)
      .w_DFIMPUTI=0
      .w_MSG_IMPORTO=space(10)
      .w_AGG_INTEGRA=oParentObject.w_AGG_INTEGRA
      .w_DFIMPDIC=oParentObject.w_DFIMPDIC
      .w_DFPROTEC=oParentObject.w_DFPROTEC
      .w_DFPRODOC=oParentObject.w_DFPRODOC
      .w_DFNOTOPE=oParentObject.w_DFNOTOPE
      .w_DFCODUTE=oParentObject.w_DFCODUTE
      .w_DFDATOPE=oParentObject.w_DFDATOPE
      .w_IMPDIC=oParentObject.w_IMPDIC
      .w_PROTEC=oParentObject.w_PROTEC
      .w_PRODOC=oParentObject.w_PRODOC
      .w_SERIALE_DICH=oParentObject.w_SERIALE_DICH
      .w_DFIMPUTI=oParentObject.w_DFIMPUTI
        .w_AGG_INTEGRA = .T.
          .DoRTCalc(2,5,.f.)
        .w_DFCODUTE = i_Codute
        .w_DFDATOPE = i_Datsys
      .oPgFrm.Page2.oPag.oObj_2_1.Calculate()
    endwith
    this.DoRTCalc(8,13,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_AGG_INTEGRA=.w_AGG_INTEGRA
      .oParentObject.w_DFIMPDIC=.w_DFIMPDIC
      .oParentObject.w_DFPROTEC=.w_DFPROTEC
      .oParentObject.w_DFPRODOC=.w_DFPRODOC
      .oParentObject.w_DFNOTOPE=.w_DFNOTOPE
      .oParentObject.w_DFCODUTE=.w_DFCODUTE
      .oParentObject.w_DFDATOPE=.w_DFDATOPE
      .oParentObject.w_IMPDIC=.w_IMPDIC
      .oParentObject.w_PROTEC=.w_PROTEC
      .oParentObject.w_PRODOC=.w_PRODOC
      .oParentObject.w_SERIALE_DICH=.w_SERIALE_DICH
      .oParentObject.w_DFIMPUTI=.w_DFIMPUTI
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
        if .o_DFPROTEC<>.w_DFPROTEC
            .w_DFPRODOC = 0
        endif
        .oPgFrm.Page2.oPag.oObj_2_1.Calculate()
        if .o_DFIMPDIC<>.w_DFIMPDIC
          .Calculate_BBBLQPLFWI()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(5,13,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.oObj_2_1.Calculate()
    endwith
  return

  proc Calculate_BBBLQPLFWI()
    with this
          * --- Controllo importo definito
      if .w_DFIMPDIC<.w_DFIMPUTI AND .w_DFIMPDIC<>0
          .w_MSG_IMPORTO = Ah_ErrorMsg("L'importo definito indicato risulta inferiore all'importo utilizzato")
      endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page2.oPag.oObj_2_1.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDFIMPDIC_1_2.value==this.w_DFIMPDIC)
      this.oPgFrm.Page1.oPag.oDFIMPDIC_1_2.value=this.w_DFIMPDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDFPROTEC_1_3.value==this.w_DFPROTEC)
      this.oPgFrm.Page1.oPag.oDFPROTEC_1_3.value=this.w_DFPROTEC
    endif
    if not(this.oPgFrm.Page1.oPag.oDFPRODOC_1_4.value==this.w_DFPRODOC)
      this.oPgFrm.Page1.oPag.oDFPRODOC_1_4.value=this.w_DFPRODOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDFNOTOPE_1_6.value==this.w_DFNOTOPE)
      this.oPgFrm.Page1.oPag.oDFNOTOPE_1_6.value=this.w_DFNOTOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oDFCODUTE_1_10.value==this.w_DFCODUTE)
      this.oPgFrm.Page1.oPag.oDFCODUTE_1_10.value=this.w_DFCODUTE
    endif
    if not(this.oPgFrm.Page1.oPag.oDFDATOPE_1_12.value==this.w_DFDATOPE)
      this.oPgFrm.Page1.oPag.oDFDATOPE_1_12.value=this.w_DFDATOPE
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPDIC_1_15.value==this.w_IMPDIC)
      this.oPgFrm.Page1.oPag.oIMPDIC_1_15.value=this.w_IMPDIC
    endif
    if not(this.oPgFrm.Page1.oPag.oPROTEC_1_16.value==this.w_PROTEC)
      this.oPgFrm.Page1.oPag.oPROTEC_1_16.value=this.w_PROTEC
    endif
    if not(this.oPgFrm.Page1.oPag.oPRODOC_1_17.value==this.w_PRODOC)
      this.oPgFrm.Page1.oPag.oPRODOC_1_17.value=this.w_PRODOC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_DFPROTEC)) or not(VAL(.w_DFPROTEC)<>0))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDFPROTEC_1_3.SetFocus()
            i_bnoObbl = !empty(.w_DFPROTEC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DFPRODOC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDFPRODOC_1_4.SetFocus()
            i_bnoObbl = !empty(.w_DFPRODOC)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DFIMPDIC = this.w_DFIMPDIC
    this.o_DFPROTEC = this.w_DFPROTEC
    return

enddefine

* --- Define pages as container
define class tgscg_kdxPag1 as StdContainer
  Width  = 623
  height = 315
  stdWidth  = 623
  stdheight = 315
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDFIMPDIC_1_2 as StdField with uid="WWKYXKBVLK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DFIMPDIC", cQueryName = "DFIMPDIC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo che verr� indicato come importo definito sulla dichiarazione di intento",;
    HelpContextID = 194529927,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=189, Top=114, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oDFPROTEC_1_3 as StdField with uid="FFRMDHLQBD",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DFPROTEC", cQueryName = "DFPROTEC",;
    bObbl = .t. , nPag = 1, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Protocollo telematico (prima parte)",;
    HelpContextID = 195222151,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=189, Top=151, cSayPict='"99999999999999999"', cGetPict='"99999999999999999"', InputMask=replicate('X',17), Alignment=1

  func oDFPROTEC_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (VAL(.w_DFPROTEC)<>0)
    endwith
    return bRes
  endfunc

  add object oDFPRODOC_1_4 as StdField with uid="YKHSPCCSQH",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DFPRODOC", cQueryName = "DFPRODOC",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Protocollo telematico (Parte seconda)",;
    HelpContextID = 73213305,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=347, Top=151, cSayPict='"999999"', cGetPict='"999999"'

  add object oDFNOTOPE_1_6 as StdField with uid="IEWFYEEDNQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DFNOTOPE", cQueryName = "DFNOTOPE",;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 262800763,;
   bGlobalFont=.t.,;
    Height=21, Width=412, Left=189, Top=225, InputMask=replicate('X',100)


  add object oBtn_1_7 as StdButton with uid="APIWEFNJEL",left=503, top=259, width=48,height=45,;
    CpPicture="bmp\OK.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte effettuate";
    , HelpContextID = 64171655;
    , tabstop=.f., caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_DFPROTEC) And Not Empty(.w_DFPRODOC) AND .w_DFIMPDIC>=0)
      endwith
    endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="ZJIUAZYUNY",left=553, top=259, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 75136442;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDFCODUTE_1_10 as StdField with uid="RNLLIRMOOG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DFCODUTE", cQueryName = "DFCODUTE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 190229125,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=189, Top=189

  add object oDFDATOPE_1_12 as StdField with uid="JNHJHJINUB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DFDATOPE", cQueryName = "DFDATOPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 261842299,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=405, Top=189

  add object oIMPDIC_1_15 as StdField with uid="OVPBCJFEOP",rtseq=8,rtrep=.f.,;
    cFormVar = "w_IMPDIC", cQueryName = "IMPDIC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo",;
    HelpContextID = 49228934,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=185, Top=15, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  add object oPROTEC_1_16 as StdField with uid="XFZXBWGJXS",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PROTEC", cQueryName = "PROTEC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(17), bMultilanguage =  .f.,;
    ToolTipText = "Protocollo telematico (prima parte)",;
    HelpContextID = 46080502,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=185, Top=49, cSayPict='"99999999999999999"', cGetPict='"99999999999999999"', InputMask=replicate('X',17), Alignment=1

  add object oPRODOC_1_17 as StdField with uid="BVQNORKVAD",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PRODOC", cQueryName = "PRODOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Protocollo telematico (Parte seconda)",;
    HelpContextID = 55517686,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=347, Top=49, cSayPict='"999999"', cGetPict='"999999"'

  add object oStr_1_5 as StdString with uid="ZYAPVNNFAO",Visible=.t., Left=38, Top=116,;
    Alignment=1, Width=147, Height=18,;
    Caption="Nuovo importo definito:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="NRUYZDMPKQ",Visible=.t., Left=90, Top=191,;
    Alignment=1, Width=95, Height=18,;
    Caption="Codice utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="KTBKEYGDYU",Visible=.t., Left=277, Top=191,;
    Alignment=1, Width=124, Height=18,;
    Caption="Data inserimento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="VEERJQXXGY",Visible=.t., Left=142, Top=228,;
    Alignment=1, Width=43, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="LQUMAFZHHC",Visible=.t., Left=69, Top=17,;
    Alignment=1, Width=112, Height=18,;
    Caption="Importo definito:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="BMLKAAVFQY",Visible=.t., Left=29, Top=52,;
    Alignment=1, Width=152, Height=18,;
    Caption="Protocollo telematico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="RKSOLNRTSH",Visible=.t., Left=334, Top=50,;
    Alignment=0, Width=12, Height=18,;
    Caption="-"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="BHSRSQFABJ",Visible=.t., Left=29, Top=154,;
    Alignment=1, Width=152, Height=18,;
    Caption="Protocollo telematico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="DLBIXRCSWG",Visible=.t., Left=334, Top=152,;
    Alignment=0, Width=12, Height=18,;
    Caption="-"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="BLBXERKYSY",Visible=.t., Left=12, Top=85,;
    Alignment=0, Width=81, Height=18,;
    Caption="Dati integrativa"  ;
  , bGlobalFont=.t.

  add object oBox_1_22 as StdBox with uid="YABJBHUCLY",left=3, top=104, width=615,height=2
enddefine
define class tgscg_kdxPag2 as StdContainer
  Width  = 623
  height = 315
  stdWidth  = 623
  stdheight = 315
  resizeXpos=301
  resizeYpos=159
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_2_1 as cp_zoombox with uid="YIFJFCYVAL",left=4, top=5, width=614,height=310,;
    caption='DettDatInt',;
   bGlobalFont=.t.,;
    cZoomFile="GSCG_KDX",bOptions=.t.,bAdvOptions=.t.,bQueryOnLoad=.t.,bReadOnly=.t.,bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,cZoomOnZoom="",cMenuFile="",cTable="DETTINTG",bRetriveAllRows=.f.,;
    cEvent = "Blank",;
    nPag=2;
    , HelpContextID = 254673057
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kdx','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
