* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_spc                                                        *
*              Stampa bilancio di verifica                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-05-03                                                      *
* Last revis.: 2013-07-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_spc",oParentObject))

* --- Class definition
define class tgscg_spc as StdForm
  Top    = 12
  Left   = 12

  * --- Standard Properties
  Width  = 788
  Height = 330
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-07-09"
  HelpContextID=141174121
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=54

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  BUSIUNIT_IDX = 0
  AZIENDA_IDX = 0
  SB_MAST_IDX = 0
  cPrg = "gscg_spc"
  cComment = "Stampa bilancio di verifica"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_codaz1 = space(5)
  w_MASABI = space(1)
  w_AZESSTCO = space(4)
  w_FINSTO = ctod('  /  /  ')
  w_UTENTE = 0
  w_gestbu = space(1)
  w_ese = space(4)
  o_ese = space(4)
  w_ESSALDI = space(1)
  o_ESSALDI = space(1)
  w_INFRAAN = space(1)
  w_totdat = space(1)
  o_totdat = space(1)
  w_VALAPP = space(3)
  o_VALAPP = space(3)
  w_data1 = ctod('  /  /  ')
  o_data1 = ctod('  /  /  ')
  w_data2 = ctod('  /  /  ')
  w_DATINIESE = ctod('  /  /  ')
  w_DATFINESE = ctod('  /  /  ')
  w_monete = space(1)
  o_monete = space(1)
  w_ESPREC = space(1)
  o_ESPREC = space(1)
  w_DIVISA = space(3)
  o_DIVISA = space(3)
  w_DATEMU = ctod('  /  /  ')
  w_CAMBGIOR = 0
  w_SIMVAL = space(5)
  w_CODBUN = space(3)
  w_SUPERBU = space(15)
  w_SBDESCRI = space(35)
  w_attivita = space(1)
  w_passivi = space(1)
  w_Costi = space(1)
  w_Ricavi = space(1)
  w_Ordine = space(1)
  w_transi = space(1)
  w_TUTTI = space(1)
  w_LIVELLI = space(1)
  o_LIVELLI = space(1)
  w_TUTTI_Z = space(1)
  o_TUTTI_Z = space(1)
  w_PERIODO = space(1)
  o_PERIODO = space(1)
  w_SEZVAR = space(1)
  o_SEZVAR = space(1)
  w_TIPSTA = space(1)
  o_TIPSTA = space(1)
  w_CONPRO = space(1)
  w_SEZVAR1 = space(1)
  o_SEZVAR1 = space(1)
  w_PROVVI = space(1)
  o_PROVVI = space(1)
  w_decimi = 0
  w_descri = space(30)
  w_BUDESCRI = space(35)
  w_EXTRAEUR = 0
  o_EXTRAEUR = 0
  w_CAMVAL = 0
  o_CAMVAL = 0
  w_MODSTA = space(1)
  o_MODSTA = space(1)
  w_DECNAZ = 0
  w_CAONAZ = 0
  w_esepre = space(4)
  w_PRVALNAZ = space(3)
  w_PRCAOVAL = 0
  w_CALCSTR = space(1)
  w_TIPORD = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_EXCEL = space(1)
  o_EXCEL = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_spcPag1","gscg_spc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oese_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='BUSIUNIT'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='SB_MAST'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCG_BPC with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gscg_spc
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_codaz1=space(5)
      .w_MASABI=space(1)
      .w_AZESSTCO=space(4)
      .w_FINSTO=ctod("  /  /  ")
      .w_UTENTE=0
      .w_gestbu=space(1)
      .w_ese=space(4)
      .w_ESSALDI=space(1)
      .w_INFRAAN=space(1)
      .w_totdat=space(1)
      .w_VALAPP=space(3)
      .w_data1=ctod("  /  /  ")
      .w_data2=ctod("  /  /  ")
      .w_DATINIESE=ctod("  /  /  ")
      .w_DATFINESE=ctod("  /  /  ")
      .w_monete=space(1)
      .w_ESPREC=space(1)
      .w_DIVISA=space(3)
      .w_DATEMU=ctod("  /  /  ")
      .w_CAMBGIOR=0
      .w_SIMVAL=space(5)
      .w_CODBUN=space(3)
      .w_SUPERBU=space(15)
      .w_SBDESCRI=space(35)
      .w_attivita=space(1)
      .w_passivi=space(1)
      .w_Costi=space(1)
      .w_Ricavi=space(1)
      .w_Ordine=space(1)
      .w_transi=space(1)
      .w_TUTTI=space(1)
      .w_LIVELLI=space(1)
      .w_TUTTI_Z=space(1)
      .w_PERIODO=space(1)
      .w_SEZVAR=space(1)
      .w_TIPSTA=space(1)
      .w_CONPRO=space(1)
      .w_SEZVAR1=space(1)
      .w_PROVVI=space(1)
      .w_decimi=0
      .w_descri=space(30)
      .w_BUDESCRI=space(35)
      .w_EXTRAEUR=0
      .w_CAMVAL=0
      .w_MODSTA=space(1)
      .w_DECNAZ=0
      .w_CAONAZ=0
      .w_esepre=space(4)
      .w_PRVALNAZ=space(3)
      .w_PRCAOVAL=0
      .w_CALCSTR=space(1)
      .w_TIPORD=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_EXCEL=space(1)
        .w_codaz1 = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_codaz1))
          .link_1_1('Full')
        endif
        .w_MASABI = IIF(g_APPLICATION="ADHOC REVOLUTION" OR g_APPLICATION="ad hoc ENTERPRISE",' ',LOOKTAB("AZIENDA","AZMASABI","AZCODAZI",.w_CODAZ1))
        .w_AZESSTCO = IIF(g_APPLICATION="ADHOC REVOLUTION",'    ',NVL(LOOKTAB("AZIENDA","AZESSTCO","AZCODAZI",.w_CODAZ1),'    '))
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_AZESSTCO))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_UTENTE = g_Codute
          .DoRTCalc(6,6,.f.)
        .w_ese = g_codese
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_ese))
          .link_1_7('Full')
        endif
        .w_ESSALDI = IIF(.w_MASABI='S',' ','S')
          .DoRTCalc(9,9,.f.)
        .w_totdat = IIF(.w_ESSALDI='S','T',IIF(EMPTY(NVL(.w_totdat,' ')),'T',.w_totdat))
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_VALAPP))
          .link_1_11('Full')
        endif
        .w_data1 = iif(.w_totdat='T',cp_CharToDate('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATINIESE))
        .w_data2 = iif(.w_totdat='T',cp_CharToDate('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATFINESE))
          .DoRTCalc(14,15,.f.)
        .w_monete = 'c'
        .w_ESPREC = IIF(.w_TOTDAT='T','S',IIF(.w_DATA1=.w_DATINIESE,'S',' '))
        .w_DIVISA = IIF(.w_monete='c',.w_VALAPP,.w_DIVISA)
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_DIVISA))
          .link_1_18('Full')
        endif
          .DoRTCalc(19,19,.f.)
        .w_CAMBGIOR = GETCAM(.w_DIVISA, i_datsys, 7)
        .w_SIMVAL = .w_SIMVAL
        .w_CODBUN = IIF(.w_ESSALDI='S' OR g_APPLICATION <> "ad hoc ENTERPRISE",'   ',IIF(EMPTY(NVL(.w_CODBUN,'   ')),'   ',.w_CODBUN))
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_CODBUN))
          .link_1_22('Full')
        endif
        .w_SUPERBU = IIF(.w_ESSALDI='S' OR g_APPLICATION <> "ad hoc ENTERPRISE",'',IIF(NOT EMPTY(.w_CODBUN),'',IIF(EMPTY(NVL(.w_SUPERBU,'')),'',.w_SUPERBU)))
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_SUPERBU))
          .link_1_23('Full')
        endif
          .DoRTCalc(24,24,.f.)
        .w_attivita = 'A'
        .w_passivi = 'P'
        .w_Costi = 'C'
        .w_Ricavi = 'R'
          .DoRTCalc(29,30,.f.)
        .w_TUTTI = IIF(.w_TUTTI_Z='S','S',.w_TUTTI)
        .w_LIVELLI = IIF(.w_PERIODO='S',' ',.w_LIVELLI)
        .w_TUTTI_Z = IIF(.w_PERIODO='S',' ',.w_TUTTI_Z)
          .DoRTCalc(34,34,.f.)
        .w_SEZVAR = IIF(.w_SEZVAR1='S',' ',.w_SEZVAR)
        .w_TIPSTA = 'T'
        .w_CONPRO = IIF(NOT(.w_PERIODO<>'S' AND .w_TIPSTA='T' AND .w_MODSTA='U' AND .w_LIVELLI<>'S' AND .w_EXCEL<>'S' AND EMPTY(NVL(.w_PROVVI,' ')) AND .w_ESSALDI<>'S'),' ',.w_CONPRO)
        .w_SEZVAR1 = IIF(.w_SEZVAR='S','N',.w_SEZVAR1)
        .w_PROVVI = IIF(.w_ESSALDI='S','N',IIF(.w_TOTDAT='T','N',.w_PROVVI))
          .DoRTCalc(40,43,.f.)
        .w_CAMVAL = iif(.w_EXTRAEUR=0,GETCAM(.w_DIVISA, .w_DATFINESE, -1), .w_EXTRAEUR)
        .w_MODSTA = 'U'
      .oPgFrm.Page1.oPag.oObj_1_59.Calculate()
          .DoRTCalc(46,47,.f.)
        .w_esepre = CALCESPR(i_CODAZI,.w_DATINIESE,.T.)
        .DoRTCalc(48,48,.f.)
        if not(empty(.w_esepre))
          .link_1_62('Full')
        endif
        .DoRTCalc(49,49,.f.)
        if not(empty(.w_PRVALNAZ))
          .link_1_63('Full')
        endif
          .DoRTCalc(50,50,.f.)
        .w_CALCSTR = 'S'
        .w_TIPORD = 'C'
        .w_OBTEST = i_DATSYS
        .w_EXCEL = IIF(.w_LIVELLI='S' OR .w_MODSTA='C',' ',.w_EXCEL)
      .oPgFrm.Page1.oPag.oObj_1_72.Calculate(AH_Msgformat(IIF(g_OFFICE='M','Stampa su Excel', 'Stampa su Calc')))
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_67.enabled = this.oPgFrm.Page1.oPag.oBtn_1_67.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_68.enabled = this.oPgFrm.Page1.oPag.oBtn_1_68.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
          .link_1_3('Full')
        .DoRTCalc(4,9,.t.)
        if .o_ESSALDI<>.w_ESSALDI
            .w_totdat = IIF(.w_ESSALDI='S','T',IIF(EMPTY(NVL(.w_totdat,' ')),'T',.w_totdat))
        endif
          .link_1_11('Full')
        if .o_ESE<>.w_ESE.or. .o_TOTDAT<>.w_TOTDAT
            .w_data1 = iif(.w_totdat='T',cp_CharToDate('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATINIESE))
        endif
        if .o_ESE<>.w_ESE.or. .o_TOTDAT<>.w_TOTDAT
            .w_data2 = iif(.w_totdat='T',cp_CharToDate('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATFINESE))
        endif
        .DoRTCalc(14,16,.t.)
        if .o_data1<>.w_data1.or. .o_totdat<>.w_totdat.or. .o_PROVVI<>.w_PROVVI
            .w_ESPREC = IIF(.w_TOTDAT='T','S',IIF(.w_DATA1=.w_DATINIESE,'S',' '))
        endif
        if .o_monete<>.w_monete.or. .o_VALAPP<>.w_VALAPP
            .w_DIVISA = IIF(.w_monete='c',.w_VALAPP,.w_DIVISA)
          .link_1_18('Full')
        endif
        .DoRTCalc(19,19,.t.)
        if .o_DIVISA<>.w_DIVISA
            .w_CAMBGIOR = GETCAM(.w_DIVISA, i_datsys, 7)
        endif
            .w_SIMVAL = .w_SIMVAL
        if .o_ESSALDI<>.w_ESSALDI
            .w_CODBUN = IIF(.w_ESSALDI='S' OR g_APPLICATION <> "ad hoc ENTERPRISE",'   ',IIF(EMPTY(NVL(.w_CODBUN,'   ')),'   ',.w_CODBUN))
          .link_1_22('Full')
        endif
        if .o_totdat<>.w_totdat.or. .o_ESPREC<>.w_ESPREC.or. .o_data1<>.w_data1.or. .o_ESSALDI<>.w_ESSALDI
            .w_SUPERBU = IIF(.w_ESSALDI='S' OR g_APPLICATION <> "ad hoc ENTERPRISE",'',IIF(NOT EMPTY(.w_CODBUN),'',IIF(EMPTY(NVL(.w_SUPERBU,'')),'',.w_SUPERBU)))
          .link_1_23('Full')
        endif
        .DoRTCalc(24,30,.t.)
        if .o_TUTTI_Z<>.w_TUTTI_Z
            .w_TUTTI = IIF(.w_TUTTI_Z='S','S',.w_TUTTI)
        endif
        if .o_PERIODO<>.w_PERIODO
            .w_LIVELLI = IIF(.w_PERIODO='S',' ',.w_LIVELLI)
        endif
        if .o_PERIODO<>.w_PERIODO
            .w_TUTTI_Z = IIF(.w_PERIODO='S',' ',.w_TUTTI_Z)
        endif
        .DoRTCalc(34,34,.t.)
        if .o_SEZVAR1<>.w_SEZVAR1
            .w_SEZVAR = IIF(.w_SEZVAR1='S',' ',.w_SEZVAR)
        endif
        .DoRTCalc(36,36,.t.)
        if .o_LIVELLI<>.w_LIVELLI.or. .o_MODSTA<>.w_MODSTA.or. .o_TIPSTA<>.w_TIPSTA.or. .o_PERIODO<>.w_PERIODO.or. .o_EXCEL<>.w_EXCEL.or. .o_PROVVI<>.w_PROVVI.or. .o_ESSALDI<>.w_ESSALDI
            .w_CONPRO = IIF(NOT(.w_PERIODO<>'S' AND .w_TIPSTA='T' AND .w_MODSTA='U' AND .w_LIVELLI<>'S' AND .w_EXCEL<>'S' AND EMPTY(NVL(.w_PROVVI,' ')) AND .w_ESSALDI<>'S'),' ',.w_CONPRO)
        endif
        if .o_SEZVAR<>.w_SEZVAR
            .w_SEZVAR1 = IIF(.w_SEZVAR='S','N',.w_SEZVAR1)
        endif
        if .o_TOTDAT<>.w_TOTDAT.or. .o_ESSALDI<>.w_ESSALDI
            .w_PROVVI = IIF(.w_ESSALDI='S','N',IIF(.w_TOTDAT='T','N',.w_PROVVI))
        endif
        .DoRTCalc(40,43,.t.)
        if .o_EXTRAEUR<>.w_EXTRAEUR.or. .o_DIVISA<>.w_DIVISA
            .w_CAMVAL = iif(.w_EXTRAEUR=0,GETCAM(.w_DIVISA, .w_DATFINESE, -1), .w_EXTRAEUR)
        endif
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate()
        .DoRTCalc(45,47,.t.)
        if .o_ese<>.w_ese
            .w_esepre = CALCESPR(i_CODAZI,.w_DATINIESE,.T.)
          .link_1_62('Full')
        endif
          .link_1_63('Full')
        .DoRTCalc(50,53,.t.)
        if .o_LIVELLI<>.w_LIVELLI.or. .o_MODSTA<>.w_MODSTA.or. .o_TIPSTA<>.w_TIPSTA
            .w_EXCEL = IIF(.w_LIVELLI='S' OR .w_MODSTA='C',' ',.w_EXCEL)
        endif
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate(AH_Msgformat(IIF(g_OFFICE='M','Stampa su Excel', 'Stampa su Calc')))
        if .o_CAMVAL<>.w_CAMVAL
          .Calculate_XTABNXKVLP()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_59.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_72.Calculate(AH_Msgformat(IIF(g_OFFICE='M','Stampa su Excel', 'Stampa su Calc')))
    endwith
  return

  proc Calculate_XTABNXKVLP()
    with this
          * --- Inserimento cambio giornaliero
     if not empty(.w_DIVISA) and .w_DIVISA<>g_perval and .w_CAMBGIOR<>.w_CAMVAL
          GSAR_BIC(this;
              ,.w_DIVISA;
              ,.w_CAMVAL;
              ,i_datsys;
             )
     endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.ototdat_1_10.enabled = this.oPgFrm.Page1.oPag.ototdat_1_10.mCond()
    this.oPgFrm.Page1.oPag.odata1_1_12.enabled = this.oPgFrm.Page1.oPag.odata1_1_12.mCond()
    this.oPgFrm.Page1.oPag.odata2_1_13.enabled = this.oPgFrm.Page1.oPag.odata2_1_13.mCond()
    this.oPgFrm.Page1.oPag.oESPREC_1_17.enabled = this.oPgFrm.Page1.oPag.oESPREC_1_17.mCond()
    this.oPgFrm.Page1.oPag.oDIVISA_1_18.enabled = this.oPgFrm.Page1.oPag.oDIVISA_1_18.mCond()
    this.oPgFrm.Page1.oPag.oCODBUN_1_22.enabled = this.oPgFrm.Page1.oPag.oCODBUN_1_22.mCond()
    this.oPgFrm.Page1.oPag.oSUPERBU_1_23.enabled = this.oPgFrm.Page1.oPag.oSUPERBU_1_23.mCond()
    this.oPgFrm.Page1.oPag.oattivita_1_25.enabled = this.oPgFrm.Page1.oPag.oattivita_1_25.mCond()
    this.oPgFrm.Page1.oPag.opassivi_1_26.enabled = this.oPgFrm.Page1.oPag.opassivi_1_26.mCond()
    this.oPgFrm.Page1.oPag.oCosti_1_27.enabled = this.oPgFrm.Page1.oPag.oCosti_1_27.mCond()
    this.oPgFrm.Page1.oPag.oRicavi_1_28.enabled = this.oPgFrm.Page1.oPag.oRicavi_1_28.mCond()
    this.oPgFrm.Page1.oPag.oOrdine_1_29.enabled = this.oPgFrm.Page1.oPag.oOrdine_1_29.mCond()
    this.oPgFrm.Page1.oPag.otransi_1_30.enabled = this.oPgFrm.Page1.oPag.otransi_1_30.mCond()
    this.oPgFrm.Page1.oPag.oTUTTI_1_31.enabled = this.oPgFrm.Page1.oPag.oTUTTI_1_31.mCond()
    this.oPgFrm.Page1.oPag.oLIVELLI_1_32.enabled = this.oPgFrm.Page1.oPag.oLIVELLI_1_32.mCond()
    this.oPgFrm.Page1.oPag.oPERIODO_1_34.enabled = this.oPgFrm.Page1.oPag.oPERIODO_1_34.mCond()
    this.oPgFrm.Page1.oPag.oSEZVAR_1_35.enabled = this.oPgFrm.Page1.oPag.oSEZVAR_1_35.mCond()
    this.oPgFrm.Page1.oPag.oTIPSTA_1_36.enabled = this.oPgFrm.Page1.oPag.oTIPSTA_1_36.mCond()
    this.oPgFrm.Page1.oPag.oCONPRO_1_37.enabled = this.oPgFrm.Page1.oPag.oCONPRO_1_37.mCond()
    this.oPgFrm.Page1.oPag.oSEZVAR1_1_38.enabled = this.oPgFrm.Page1.oPag.oSEZVAR1_1_38.mCond()
    this.oPgFrm.Page1.oPag.oPROVVI_1_39.enabled = this.oPgFrm.Page1.oPag.oPROVVI_1_39.mCond()
    this.oPgFrm.Page1.oPag.oCAMVAL_1_56.enabled = this.oPgFrm.Page1.oPag.oCAMVAL_1_56.mCond()
    this.oPgFrm.Page1.oPag.oEXCEL_1_71.enabled = this.oPgFrm.Page1.oPag.oEXCEL_1_71.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_67.enabled = this.oPgFrm.Page1.oPag.oBtn_1_67.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oESSALDI_1_8.visible=!this.oPgFrm.Page1.oPag.oESSALDI_1_8.mHide()
    this.oPgFrm.Page1.oPag.oINFRAAN_1_9.visible=!this.oPgFrm.Page1.oPag.oINFRAAN_1_9.mHide()
    this.oPgFrm.Page1.oPag.oESPREC_1_17.visible=!this.oPgFrm.Page1.oPag.oESPREC_1_17.mHide()
    this.oPgFrm.Page1.oPag.oCODBUN_1_22.visible=!this.oPgFrm.Page1.oPag.oCODBUN_1_22.mHide()
    this.oPgFrm.Page1.oPag.oSUPERBU_1_23.visible=!this.oPgFrm.Page1.oPag.oSUPERBU_1_23.mHide()
    this.oPgFrm.Page1.oPag.oSBDESCRI_1_24.visible=!this.oPgFrm.Page1.oPag.oSBDESCRI_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.oPgFrm.Page1.oPag.oBUDESCRI_1_51.visible=!this.oPgFrm.Page1.oPag.oBUDESCRI_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_52.visible=!this.oPgFrm.Page1.oPag.oStr_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_69.visible=!this.oPgFrm.Page1.oPag.oStr_1_69.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_59.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_72.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=codaz1
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_codaz1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_codaz1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLBUNI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_codaz1)
            select AZCODAZI,AZFLBUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_codaz1 = NVL(_Link_.AZCODAZI,space(5))
      this.w_gestbu = NVL(_Link_.AZFLBUNI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_codaz1 = space(5)
      endif
      this.w_gestbu = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_codaz1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZESSTCO
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZESSTCO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZESSTCO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_AZESSTCO);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZ1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZ1;
                       ,'ESCODESE',this.w_AZESSTCO)
            select ESCODAZI,ESCODESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZESSTCO = NVL(_Link_.ESCODESE,space(4))
      this.w_FINSTO = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AZESSTCO = space(4)
      endif
      this.w_FINSTO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZESSTCO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ese
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ese) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ese)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_codaz1;
                     ,'ESCODESE',trim(this.w_ese))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ese)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ese) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oese_1_7'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_codaz1<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Esercizio inesistente o storicizzato")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ese)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ese);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_codaz1;
                       ,'ESCODESE',this.w_ese)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ese = NVL(_Link_.ESCODESE,space(4))
      this.w_DATINIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_DATFINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_VALAPP = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ese = space(4)
      endif
      this.w_DATINIESE = ctod("  /  /  ")
      this.w_DATFINESE = ctod("  /  /  ")
      this.w_VALAPP = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATINIESE>.w_FINSTO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Esercizio inesistente o storicizzato")
        endif
        this.w_ese = space(4)
        this.w_DATINIESE = ctod("  /  /  ")
        this.w_DATFINESE = ctod("  /  /  ")
        this.w_VALAPP = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ese Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALAPP
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALAPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALAPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECTOT,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALAPP)
            select VACODVAL,VADECTOT,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALAPP = NVL(_Link_.VACODVAL,space(3))
      this.w_DECNAZ = NVL(_Link_.VADECTOT,0)
      this.w_CAONAZ = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALAPP = space(3)
      endif
      this.w_DECNAZ = 0
      this.w_CAONAZ = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALAPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIVISA
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIVISA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_DIVISA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADATEUR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_DIVISA))
          select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADATEUR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIVISA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_DIVISA)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADATEUR";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_DIVISA)+"%");

            select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADATEUR;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DIVISA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oDIVISA_1_18'),i_cWhere,'GSAR_AVL',"Divise",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADATEUR";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADATEUR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIVISA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADATEUR";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_DIVISA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_DIVISA)
            select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADATEUR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIVISA = NVL(_Link_.VACODVAL,space(3))
      this.w_descri = NVL(_Link_.VADESVAL,space(30))
      this.w_decimi = NVL(_Link_.VADECTOT,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_EXTRAEUR = NVL(_Link_.VACAOVAL,0)
      this.w_DATEMU = NVL(cp_ToDate(_Link_.VADATEUR),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DIVISA = space(3)
      endif
      this.w_descri = space(30)
      this.w_decimi = 0
      this.w_SIMVAL = space(5)
      this.w_EXTRAEUR = 0
      this.w_DATEMU = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIVISA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODBUN
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODBUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KBU',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_CODBUN)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_codaz1);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_codaz1;
                     ,'BUCODICE',trim(this.w_CODBUN))
          select BUCODAZI,BUCODICE,BUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODBUN)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODBUN) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oCODBUN_1_22'),i_cWhere,'GSAR_KBU',"Business Unit",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_codaz1<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_codaz1);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODBUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_CODBUN);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_codaz1;
                       ,'BUCODICE',this.w_CODBUN)
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODBUN = NVL(_Link_.BUCODICE,space(3))
      this.w_BUDESCRI = NVL(_Link_.BUDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODBUN = space(3)
      endif
      this.w_BUDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODBUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SUPERBU
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SB_MAST_IDX,3]
    i_lTable = "SB_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2], .t., this.SB_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SUPERBU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MSB',True,'SB_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SBCODICE like "+cp_ToStrODBC(trim(this.w_SUPERBU)+"%");

          i_ret=cp_SQL(i_nConn,"select SBCODICE,SBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SBCODICE',trim(this.w_SUPERBU))
          select SBCODICE,SBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SUPERBU)==trim(_Link_.SBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SUPERBU) and !this.bDontReportError
            deferred_cp_zoom('SB_MAST','*','SBCODICE',cp_AbsName(oSource.parent,'oSUPERBU_1_23'),i_cWhere,'GSAR_MSB',"Gruppi Business Unit",'Gruppobunit.SB_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SBCODICE,SBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SBCODICE',oSource.xKey(1))
            select SBCODICE,SBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SUPERBU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SBCODICE,SBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SBCODICE="+cp_ToStrODBC(this.w_SUPERBU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SBCODICE',this.w_SUPERBU)
            select SBCODICE,SBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SUPERBU = NVL(_Link_.SBCODICE,space(15))
      this.w_SBDESCRI = NVL(_Link_.SBDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SUPERBU = space(15)
      endif
      this.w_SBDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2])+'\'+cp_ToStr(_Link_.SBCODICE,1)
      cp_ShowWarn(i_cKey,this.SB_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SUPERBU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=esepre
  func Link_1_62(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_esepre) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_esepre)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_esepre);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_codaz1;
                       ,'ESCODESE',this.w_esepre)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_esepre = NVL(_Link_.ESCODESE,space(4))
      this.w_PRVALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_esepre = space(4)
      endif
      this.w_PRVALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_esepre Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRVALNAZ
  func Link_1_63(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRVALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRVALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PRVALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PRVALNAZ)
            select VACODVAL,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRVALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_PRCAOVAL = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_PRVALNAZ = space(3)
      endif
      this.w_PRCAOVAL = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRVALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oese_1_7.value==this.w_ese)
      this.oPgFrm.Page1.oPag.oese_1_7.value=this.w_ese
    endif
    if not(this.oPgFrm.Page1.oPag.oESSALDI_1_8.RadioValue()==this.w_ESSALDI)
      this.oPgFrm.Page1.oPag.oESSALDI_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINFRAAN_1_9.RadioValue()==this.w_INFRAAN)
      this.oPgFrm.Page1.oPag.oINFRAAN_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.ototdat_1_10.RadioValue()==this.w_totdat)
      this.oPgFrm.Page1.oPag.ototdat_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.odata1_1_12.value==this.w_data1)
      this.oPgFrm.Page1.oPag.odata1_1_12.value=this.w_data1
    endif
    if not(this.oPgFrm.Page1.oPag.odata2_1_13.value==this.w_data2)
      this.oPgFrm.Page1.oPag.odata2_1_13.value=this.w_data2
    endif
    if not(this.oPgFrm.Page1.oPag.omonete_1_16.RadioValue()==this.w_monete)
      this.oPgFrm.Page1.oPag.omonete_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESPREC_1_17.RadioValue()==this.w_ESPREC)
      this.oPgFrm.Page1.oPag.oESPREC_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIVISA_1_18.value==this.w_DIVISA)
      this.oPgFrm.Page1.oPag.oDIVISA_1_18.value=this.w_DIVISA
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_21.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_21.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCODBUN_1_22.value==this.w_CODBUN)
      this.oPgFrm.Page1.oPag.oCODBUN_1_22.value=this.w_CODBUN
    endif
    if not(this.oPgFrm.Page1.oPag.oSUPERBU_1_23.value==this.w_SUPERBU)
      this.oPgFrm.Page1.oPag.oSUPERBU_1_23.value=this.w_SUPERBU
    endif
    if not(this.oPgFrm.Page1.oPag.oSBDESCRI_1_24.value==this.w_SBDESCRI)
      this.oPgFrm.Page1.oPag.oSBDESCRI_1_24.value=this.w_SBDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oattivita_1_25.RadioValue()==this.w_attivita)
      this.oPgFrm.Page1.oPag.oattivita_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.opassivi_1_26.RadioValue()==this.w_passivi)
      this.oPgFrm.Page1.oPag.opassivi_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCosti_1_27.RadioValue()==this.w_Costi)
      this.oPgFrm.Page1.oPag.oCosti_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oRicavi_1_28.RadioValue()==this.w_Ricavi)
      this.oPgFrm.Page1.oPag.oRicavi_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oOrdine_1_29.RadioValue()==this.w_Ordine)
      this.oPgFrm.Page1.oPag.oOrdine_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.otransi_1_30.RadioValue()==this.w_transi)
      this.oPgFrm.Page1.oPag.otransi_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTUTTI_1_31.RadioValue()==this.w_TUTTI)
      this.oPgFrm.Page1.oPag.oTUTTI_1_31.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oLIVELLI_1_32.RadioValue()==this.w_LIVELLI)
      this.oPgFrm.Page1.oPag.oLIVELLI_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTUTTI_Z_1_33.RadioValue()==this.w_TUTTI_Z)
      this.oPgFrm.Page1.oPag.oTUTTI_Z_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIODO_1_34.RadioValue()==this.w_PERIODO)
      this.oPgFrm.Page1.oPag.oPERIODO_1_34.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSEZVAR_1_35.RadioValue()==this.w_SEZVAR)
      this.oPgFrm.Page1.oPag.oSEZVAR_1_35.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPSTA_1_36.RadioValue()==this.w_TIPSTA)
      this.oPgFrm.Page1.oPag.oTIPSTA_1_36.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCONPRO_1_37.RadioValue()==this.w_CONPRO)
      this.oPgFrm.Page1.oPag.oCONPRO_1_37.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSEZVAR1_1_38.RadioValue()==this.w_SEZVAR1)
      this.oPgFrm.Page1.oPag.oSEZVAR1_1_38.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVVI_1_39.RadioValue()==this.w_PROVVI)
      this.oPgFrm.Page1.oPag.oPROVVI_1_39.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBUDESCRI_1_51.value==this.w_BUDESCRI)
      this.oPgFrm.Page1.oPag.oBUDESCRI_1_51.value=this.w_BUDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMVAL_1_56.value==this.w_CAMVAL)
      this.oPgFrm.Page1.oPag.oCAMVAL_1_56.value=this.w_CAMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMODSTA_1_57.RadioValue()==this.w_MODSTA)
      this.oPgFrm.Page1.oPag.oMODSTA_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEXCEL_1_71.RadioValue()==this.w_EXCEL)
      this.oPgFrm.Page1.oPag.oEXCEL_1_71.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ese)) or not(.w_DATINIESE>.w_FINSTO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oese_1_7.SetFocus()
            i_bnoObbl = !empty(.w_ese)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Esercizio inesistente o storicizzato")
          case   not(empty(.w_data2) or .w_data2>=.w_data1)  and (.w_totdat='D')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odata1_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo date non valido")
          case   not(empty(.w_data1) or .w_data2>=.w_data1)  and (.w_totdat='D')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odata2_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di date non valido")
          case   (empty(.w_DIVISA))  and (.w_monete='a')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIVISA_1_18.SetFocus()
            i_bnoObbl = !empty(.w_DIVISA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAMVAL))  and (i_datsys<.w_DATEMU or .w_EXTRAEUR=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMVAL_1_56.SetFocus()
            i_bnoObbl = !empty(.w_CAMVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_esepre))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oesepre_1_62.SetFocus()
            i_bnoObbl = !empty(.w_esepre)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ese = this.w_ese
    this.o_ESSALDI = this.w_ESSALDI
    this.o_totdat = this.w_totdat
    this.o_VALAPP = this.w_VALAPP
    this.o_data1 = this.w_data1
    this.o_monete = this.w_monete
    this.o_ESPREC = this.w_ESPREC
    this.o_DIVISA = this.w_DIVISA
    this.o_LIVELLI = this.w_LIVELLI
    this.o_TUTTI_Z = this.w_TUTTI_Z
    this.o_PERIODO = this.w_PERIODO
    this.o_SEZVAR = this.w_SEZVAR
    this.o_TIPSTA = this.w_TIPSTA
    this.o_SEZVAR1 = this.w_SEZVAR1
    this.o_PROVVI = this.w_PROVVI
    this.o_EXTRAEUR = this.w_EXTRAEUR
    this.o_CAMVAL = this.w_CAMVAL
    this.o_MODSTA = this.w_MODSTA
    this.o_EXCEL = this.w_EXCEL
    return

enddefine

* --- Define pages as container
define class tgscg_spcPag1 as StdContainer
  Width  = 784
  height = 330
  stdWidth  = 784
  stdheight = 330
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oese_1_7 as StdField with uid="XWXKESWBWM",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ese", cQueryName = "ese",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Esercizio inesistente o storicizzato",;
    ToolTipText = "Esercizio di competenza dei movimenti da stampare",;
    HelpContextID = 140729274,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=149, Top=11, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_codaz1", oKey_2_1="ESCODESE", oKey_2_2="this.w_ese"

  func oese_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oese_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oese_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_codaz1)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_codaz1)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oese_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oESSALDI_1_8 as StdCheck with uid="LHILIIGAEZ",rtseq=8,rtrep=.f.,left=266, top=11, caption="Lettura saldi eser. attuale",;
    ToolTipText = "I dati estratti verranno presi dall'archivio saldi",;
    HelpContextID = 258186682,;
    cFormVar="w_ESSALDI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESSALDI_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    ''))
  endfunc
  func oESSALDI_1_8.GetRadio()
    this.Parent.oContained.w_ESSALDI = this.RadioValue()
    return .t.
  endfunc

  func oESSALDI_1_8.SetRadio()
    this.Parent.oContained.w_ESSALDI=trim(this.Parent.oContained.w_ESSALDI)
    this.value = ;
      iif(this.Parent.oContained.w_ESSALDI=='S',1,;
      0)
  endfunc

  func oESSALDI_1_8.mHide()
    with this.Parent.oContained
      return (.w_MASABI='S')
    endwith
  endfunc

  add object oINFRAAN_1_9 as StdCheck with uid="RRZRWNHGRB",rtseq=9,rtrep=.f.,left=491, top=11, caption="Escludi movimenti da chiusura infrannuale",;
    ToolTipText = "Se attivo, non stampa le registrazioni da chiusure infrannuali",;
    HelpContextID = 50557562,;
    cFormVar="w_INFRAAN", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oINFRAAN_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oINFRAAN_1_9.GetRadio()
    this.Parent.oContained.w_INFRAAN = this.RadioValue()
    return .t.
  endfunc

  func oINFRAAN_1_9.SetRadio()
    this.Parent.oContained.w_INFRAAN=trim(this.Parent.oContained.w_INFRAAN)
    this.value = ;
      iif(this.Parent.oContained.w_INFRAAN=='S',1,;
      0)
  endfunc

  func oINFRAAN_1_9.mHide()
    with this.Parent.oContained
      return (Not islron())
    endwith
  endfunc


  add object ototdat_1_10 as StdCombo with uid="BRHXJMVGFR",rtseq=10,rtrep=.f.,left=149,top=39,width=127,height=21;
    , ToolTipText = "Tipo bilancio selezionato";
    , HelpContextID = 34705718;
    , cFormVar="w_totdat",RowSource=""+"Totale esercizio,"+"Da data a data", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func ototdat_1_10.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func ototdat_1_10.GetRadio()
    this.Parent.oContained.w_totdat = this.RadioValue()
    return .t.
  endfunc

  func ototdat_1_10.SetRadio()
    this.Parent.oContained.w_totdat=trim(this.Parent.oContained.w_totdat)
    this.value = ;
      iif(this.Parent.oContained.w_totdat=='T',1,;
      iif(this.Parent.oContained.w_totdat=='D',2,;
      0))
  endfunc

  func ototdat_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ESSALDI<>'S')
    endwith
   endif
  endfunc

  add object odata1_1_12 as StdField with uid="YUXQUOHLGC",rtseq=12,rtrep=.f.,;
    cFormVar = "w_data1", cQueryName = "data1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo date non valido",;
    ToolTipText = "Data di registrazione di inizio stampa",;
    HelpContextID = 82935242,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=341, Top=39

  func odata1_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_totdat='D')
    endwith
   endif
  endfunc

  func odata1_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data2) or .w_data2>=.w_data1)
    endwith
    return bRes
  endfunc

  add object odata2_1_13 as StdField with uid="QIUSUKKEHT",rtseq=13,rtrep=.f.,;
    cFormVar = "w_data2", cQueryName = "data2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di date non valido",;
    ToolTipText = "Data di registrazione di fine stampa",;
    HelpContextID = 81886666,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=450, Top=39

  func odata2_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_totdat='D')
    endwith
   endif
  endfunc

  func odata2_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data1) or .w_data2>=.w_data1)
    endwith
    return bRes
  endfunc


  add object omonete_1_16 as StdCombo with uid="VCJGADBTTY",rtseq=16,rtrep=.f.,left=149,top=70,width=127,height=21;
    , ToolTipText = "Stampa in valuta di conto o in altra valuta";
    , HelpContextID = 71446726;
    , cFormVar="w_monete",RowSource=""+"Valuta di conto,"+"Altra valuta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func omonete_1_16.RadioValue()
    return(iif(this.value =1,'c',;
    iif(this.value =2,'a',;
    space(1))))
  endfunc
  func omonete_1_16.GetRadio()
    this.Parent.oContained.w_monete = this.RadioValue()
    return .t.
  endfunc

  func omonete_1_16.SetRadio()
    this.Parent.oContained.w_monete=trim(this.Parent.oContained.w_monete)
    this.value = ;
      iif(this.Parent.oContained.w_monete=='c',1,;
      iif(this.Parent.oContained.w_monete=='a',2,;
      0))
  endfunc

  add object oESPREC_1_17 as StdCheck with uid="HPOFQDIQQX",rtseq=17,rtrep=.f.,left=542, top=39, caption="Saldi es. precedente",;
    ToolTipText = "Considera i saldi dell'esercizio precedente",;
    HelpContextID = 12766650,;
    cFormVar="w_ESPREC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESPREC_1_17.RadioValue()
    return(iif(this.value =1,'S',;
    ''))
  endfunc
  func oESPREC_1_17.GetRadio()
    this.Parent.oContained.w_ESPREC = this.RadioValue()
    return .t.
  endfunc

  func oESPREC_1_17.SetRadio()
    this.Parent.oContained.w_ESPREC=trim(this.Parent.oContained.w_ESPREC)
    this.value = ;
      iif(this.Parent.oContained.w_ESPREC=='S',1,;
      0)
  endfunc

  func oESPREC_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TOTDAT='D' AND .w_DATA1=.w_DATINIESE)
    endwith
   endif
  endfunc

  func oESPREC_1_17.mHide()
    with this.Parent.oContained
      return (.w_PROVVI='S')
    endwith
  endfunc

  add object oDIVISA_1_18 as StdField with uid="ZARHUESMJK",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DIVISA", cQueryName = "DIVISA",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta selezionata",;
    HelpContextID = 32208842,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=341, Top=70, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_DIVISA"

  func oDIVISA_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_monete='a')
    endwith
   endif
  endfunc

  func oDIVISA_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIVISA_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDIVISA_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oDIVISA_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Divise",'',this.parent.oContained
  endproc
  proc oDIVISA_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_DIVISA
     i_obj.ecpSave()
  endproc

  add object oSIMVAL_1_21 as StdField with uid="TWCPYBXLID",rtseq=21,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Simbolo della valuta",;
    HelpContextID = 134153946,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=396, Top=70, InputMask=replicate('X',5)

  add object oCODBUN_1_22 as StdField with uid="WMUDNLGAAK",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CODBUN", cQueryName = "CODBUN",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Business Unit selezionata",;
    HelpContextID = 80974298,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=149, Top=99, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", cZoomOnZoom="GSAR_KBU", oKey_1_1="BUCODAZI", oKey_1_2="this.w_codaz1", oKey_2_1="BUCODICE", oKey_2_2="this.w_CODBUN"

  func oCODBUN_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" AND ((.w_gestbu $ 'ET') and EMPTY(.w_SUPERBU) and .w_ESSALDI<>'S'))
    endwith
   endif
  endfunc

  func oCODBUN_1_22.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  func oCODBUN_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODBUN_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODBUN_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_codaz1)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_codaz1)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oCODBUN_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KBU',"Business Unit",'',this.parent.oContained
  endproc
  proc oCODBUN_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KBU()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.BUCODAZI=w_codaz1
     i_obj.w_BUCODICE=this.parent.oContained.w_CODBUN
     i_obj.ecpSave()
  endproc

  add object oSUPERBU_1_23 as StdField with uid="NPXRMQESWI",rtseq=23,rtrep=.f.,;
    cFormVar = "w_SUPERBU", cQueryName = "SUPERBU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo Business Unit selezionato",;
    HelpContextID = 251671846,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=149, Top=127, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="SB_MAST", cZoomOnZoom="GSAR_MSB", oKey_1_1="SBCODICE", oKey_1_2="this.w_SUPERBU"

  func oSUPERBU_1_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" AND ((.w_gestbu $ 'ET') AND EMPTY(.w_CODBUN) and .w_ESSALDI<>'S'))
    endwith
   endif
  endfunc

  func oSUPERBU_1_23.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  func oSUPERBU_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oSUPERBU_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSUPERBU_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SB_MAST','*','SBCODICE',cp_AbsName(this.parent,'oSUPERBU_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MSB',"Gruppi Business Unit",'Gruppobunit.SB_MAST_VZM',this.parent.oContained
  endproc
  proc oSUPERBU_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MSB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SBCODICE=this.parent.oContained.w_SUPERBU
     i_obj.ecpSave()
  endproc

  add object oSBDESCRI_1_24 as StdField with uid="LMYTZKVNIL",rtseq=24,rtrep=.f.,;
    cFormVar = "w_SBDESCRI", cQueryName = "SBDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 1008239,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=286, Top=127, InputMask=replicate('X',35)

  func oSBDESCRI_1_24.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oattivita_1_25 as StdCheck with uid="SWLVJZATFA",rtseq=25,rtrep=.f.,left=149, top=183, caption="Attivit�",;
    ToolTipText = "Stampa i conti con sezione bilancio = attivit�",;
    HelpContextID = 140940647,;
    cFormVar="w_attivita", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oattivita_1_25.RadioValue()
    return(iif(this.value =1,'A',;
    ' '))
  endfunc
  func oattivita_1_25.GetRadio()
    this.Parent.oContained.w_attivita = this.RadioValue()
    return .t.
  endfunc

  func oattivita_1_25.SetRadio()
    this.Parent.oContained.w_attivita=trim(this.Parent.oContained.w_attivita)
    this.value = ;
      iif(this.Parent.oContained.w_attivita=='A',1,;
      0)
  endfunc

  func oattivita_1_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODSTA<>"C")
    endwith
   endif
  endfunc

  add object opassivi_1_26 as StdCheck with uid="DEIRKTKHBF",rtseq=26,rtrep=.f.,left=149, top=201, caption="Passivit�",;
    ToolTipText = "Stampa i conti con sezione bilancio = passivit�",;
    HelpContextID = 190811402,;
    cFormVar="w_passivi", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func opassivi_1_26.RadioValue()
    return(iif(this.value =1,'P',;
    ' '))
  endfunc
  func opassivi_1_26.GetRadio()
    this.Parent.oContained.w_passivi = this.RadioValue()
    return .t.
  endfunc

  func opassivi_1_26.SetRadio()
    this.Parent.oContained.w_passivi=trim(this.Parent.oContained.w_passivi)
    this.value = ;
      iif(this.Parent.oContained.w_passivi=='P',1,;
      0)
  endfunc

  func opassivi_1_26.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODSTA<>"C")
    endwith
   endif
  endfunc

  add object oCosti_1_27 as StdCheck with uid="AVULITYUGV",rtseq=27,rtrep=.f.,left=149, top=219, caption="Costi",;
    ToolTipText = "Stampa i conti con sezione bilancio = costi",;
    HelpContextID = 22970842,;
    cFormVar="w_Costi", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCosti_1_27.RadioValue()
    return(iif(this.value =1,'C',;
    ' '))
  endfunc
  func oCosti_1_27.GetRadio()
    this.Parent.oContained.w_Costi = this.RadioValue()
    return .t.
  endfunc

  func oCosti_1_27.SetRadio()
    this.Parent.oContained.w_Costi=trim(this.Parent.oContained.w_Costi)
    this.value = ;
      iif(this.Parent.oContained.w_Costi=='C',1,;
      0)
  endfunc

  func oCosti_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODSTA<>"C")
    endwith
   endif
  endfunc

  add object oRicavi_1_28 as StdCheck with uid="XEXRUXMRPY",rtseq=28,rtrep=.f.,left=149, top=237, caption="Ricavi",;
    ToolTipText = "Stampa i conti con sezione bilancio = ricavi",;
    HelpContextID = 140343574,;
    cFormVar="w_Ricavi", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oRicavi_1_28.RadioValue()
    return(iif(this.value =1,'R',;
    ' '))
  endfunc
  func oRicavi_1_28.GetRadio()
    this.Parent.oContained.w_Ricavi = this.RadioValue()
    return .t.
  endfunc

  func oRicavi_1_28.SetRadio()
    this.Parent.oContained.w_Ricavi=trim(this.Parent.oContained.w_Ricavi)
    this.value = ;
      iif(this.Parent.oContained.w_Ricavi=='R',1,;
      0)
  endfunc

  func oRicavi_1_28.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODSTA<>"C")
    endwith
   endif
  endfunc

  add object oOrdine_1_29 as StdCheck with uid="PEPIYQYFSY",rtseq=29,rtrep=.f.,left=149, top=255, caption="Ordine",;
    ToolTipText = "Stampa i conti con sezione bilancio = ordine",;
    HelpContextID = 65376742,;
    cFormVar="w_Ordine", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oOrdine_1_29.RadioValue()
    return(iif(this.value =1,'O',;
    ' '))
  endfunc
  func oOrdine_1_29.GetRadio()
    this.Parent.oContained.w_Ordine = this.RadioValue()
    return .t.
  endfunc

  func oOrdine_1_29.SetRadio()
    this.Parent.oContained.w_Ordine=trim(this.Parent.oContained.w_Ordine)
    this.value = ;
      iif(this.Parent.oContained.w_Ordine=='O',1,;
      0)
  endfunc

  func oOrdine_1_29.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODSTA<>"C")
    endwith
   endif
  endfunc

  add object otransi_1_30 as StdCheck with uid="NQLVAEFAIL",rtseq=30,rtrep=.f.,left=149, top=273, caption="Transitori",;
    ToolTipText = "Stampa i conti con sezione bilancio = transitori",;
    HelpContextID = 138044470,;
    cFormVar="w_transi", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func otransi_1_30.RadioValue()
    return(iif(this.value =1,'T',;
    ' '))
  endfunc
  func otransi_1_30.GetRadio()
    this.Parent.oContained.w_transi = this.RadioValue()
    return .t.
  endfunc

  func otransi_1_30.SetRadio()
    this.Parent.oContained.w_transi=trim(this.Parent.oContained.w_transi)
    this.value = ;
      iif(this.Parent.oContained.w_transi=='T',1,;
      0)
  endfunc

  func otransi_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODSTA<>"C")
    endwith
   endif
  endfunc

  add object oTUTTI_1_31 as StdCheck with uid="TDJCVSTXKQ",rtseq=31,rtrep=.f.,left=330, top=177, caption="Escludi conti non movimentati",;
    ToolTipText = "Stampa solo i conti movimentati nel periodo",;
    HelpContextID = 58755786,;
    cFormVar="w_TUTTI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTUTTI_1_31.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTUTTI_1_31.GetRadio()
    this.Parent.oContained.w_TUTTI = this.RadioValue()
    return .t.
  endfunc

  func oTUTTI_1_31.SetRadio()
    this.Parent.oContained.w_TUTTI=trim(this.Parent.oContained.w_TUTTI)
    this.value = ;
      iif(this.Parent.oContained.w_TUTTI=='S',1,;
      0)
  endfunc

  func oTUTTI_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TUTTI_Z<>'S')
    endwith
   endif
  endfunc

  add object oLIVELLI_1_32 as StdCheck with uid="WTIQEERGWW",rtseq=32,rtrep=.f.,left=616, top=183, caption="Stampa mastri",;
    ToolTipText = "Se attivo stampa tutti i mastri di qualsiasi livello",;
    HelpContextID = 123696970,;
    cFormVar="w_LIVELLI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oLIVELLI_1_32.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oLIVELLI_1_32.GetRadio()
    this.Parent.oContained.w_LIVELLI = this.RadioValue()
    return .t.
  endfunc

  func oLIVELLI_1_32.SetRadio()
    this.Parent.oContained.w_LIVELLI=trim(this.Parent.oContained.w_LIVELLI)
    this.value = ;
      iif(this.Parent.oContained.w_LIVELLI=='S',1,;
      0)
  endfunc

  func oLIVELLI_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT (.w_Tipsta<>'T' or .w_Modsta<>'U' or .w_Periodo='S'))
    endwith
   endif
  endfunc

  add object oTUTTI_Z_1_33 as StdCheck with uid="FSKLWQCHUI",rtseq=33,rtrep=.f.,left=330, top=195, caption="Escludi importi a zero",;
    ToolTipText = "Stampa solo i conti con saldo diverso da 0",;
    HelpContextID = 75533002,;
    cFormVar="w_TUTTI_Z", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTUTTI_Z_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTUTTI_Z_1_33.GetRadio()
    this.Parent.oContained.w_TUTTI_Z = this.RadioValue()
    return .t.
  endfunc

  func oTUTTI_Z_1_33.SetRadio()
    this.Parent.oContained.w_TUTTI_Z=trim(this.Parent.oContained.w_TUTTI_Z)
    this.value = ;
      iif(this.Parent.oContained.w_TUTTI_Z=='S',1,;
      0)
  endfunc

  add object oPERIODO_1_34 as StdCheck with uid="LJCGIGNJZS",rtseq=34,rtrep=.f.,left=330, top=213, caption="Saldi periodo",;
    ToolTipText = "Bilancio con sezioni dare / avere",;
    HelpContextID = 254524170,;
    cFormVar="w_PERIODO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPERIODO_1_34.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPERIODO_1_34.GetRadio()
    this.Parent.oContained.w_PERIODO = this.RadioValue()
    return .t.
  endfunc

  func oPERIODO_1_34.SetRadio()
    this.Parent.oContained.w_PERIODO=trim(this.Parent.oContained.w_PERIODO)
    this.value = ;
      iif(this.Parent.oContained.w_PERIODO=='S',1,;
      0)
  endfunc

  func oPERIODO_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODSTA<>'C')
    endwith
   endif
  endfunc

  add object oSEZVAR_1_35 as StdCheck with uid="GYGHBKDFQU",rtseq=35,rtrep=.f.,left=616, top=219, caption="Sezioni variabili",;
    ToolTipText = "Bilancio a sezioni variabili",;
    HelpContextID = 33438426,;
    cFormVar="w_SEZVAR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSEZVAR_1_35.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oSEZVAR_1_35.GetRadio()
    this.Parent.oContained.w_SEZVAR = this.RadioValue()
    return .t.
  endfunc

  func oSEZVAR_1_35.SetRadio()
    this.Parent.oContained.w_SEZVAR=trim(this.Parent.oContained.w_SEZVAR)
    this.value = ;
      iif(this.Parent.oContained.w_SEZVAR=='S',1,;
      0)
  endfunc

  func oSEZVAR_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZVAR1<>'S')
    endwith
   endif
  endfunc


  add object oTIPSTA_1_36 as StdCombo with uid="ZKUHNJBKUP",rtseq=36,rtrep=.f.,left=418,top=259,width=127,height=21;
    , ToolTipText = "Selezioni di stampa";
    , HelpContextID = 30529226;
    , cFormVar="w_TIPSTA",RowSource=""+"Completo,"+"Solo mastri", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPSTA_1_36.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'M',;
    space(1))))
  endfunc
  func oTIPSTA_1_36.GetRadio()
    this.Parent.oContained.w_TIPSTA = this.RadioValue()
    return .t.
  endfunc

  func oTIPSTA_1_36.SetRadio()
    this.Parent.oContained.w_TIPSTA=trim(this.Parent.oContained.w_TIPSTA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPSTA=='T',1,;
      iif(this.Parent.oContained.w_TIPSTA=='M',2,;
      0))
  endfunc

  func oTIPSTA_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_MODSTA<>"C")
    endwith
   endif
  endfunc

  add object oCONPRO_1_37 as StdCheck with uid="QQUAROPFQM",rtseq=37,rtrep=.f.,left=616, top=237, caption="Confermati/provv.",;
    ToolTipText = "Stampa separatamente provvisori da confermati",;
    HelpContextID = 66384346,;
    cFormVar="w_CONPRO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCONPRO_1_37.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oCONPRO_1_37.GetRadio()
    this.Parent.oContained.w_CONPRO = this.RadioValue()
    return .t.
  endfunc

  func oCONPRO_1_37.SetRadio()
    this.Parent.oContained.w_CONPRO=trim(this.Parent.oContained.w_CONPRO)
    this.value = ;
      iif(this.Parent.oContained.w_CONPRO=='S',1,;
      0)
  endfunc

  func oCONPRO_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PERIODO<>'S' AND .w_TIPSTA='T' AND .w_MODSTA='U' AND .w_LIVELLI<>'S' AND .w_EXCEL<>'S' AND EMPTY(NVL(.w_PROVVI,' ')) AND .w_ESSALDI<>'S')
    endwith
   endif
  endfunc

  add object oSEZVAR1_1_38 as StdCheck with uid="XTTVMLVLKD",rtseq=38,rtrep=.f.,left=616, top=255, caption="Sez. var. generici",;
    ToolTipText = "Bilancio a sezioni variabili solo per conti generici",;
    HelpContextID = 234997030,;
    cFormVar="w_SEZVAR1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oSEZVAR1_1_38.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oSEZVAR1_1_38.GetRadio()
    this.Parent.oContained.w_SEZVAR1 = this.RadioValue()
    return .t.
  endfunc

  func oSEZVAR1_1_38.SetRadio()
    this.Parent.oContained.w_SEZVAR1=trim(this.Parent.oContained.w_SEZVAR1)
    this.value = ;
      iif(this.Parent.oContained.w_SEZVAR1=='S',1,;
      0)
  endfunc

  func oSEZVAR1_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_SEZVAR<>'S')
    endwith
   endif
  endfunc


  add object oPROVVI_1_39 as StdCombo with uid="VZFATUEWXD",value=3,rtseq=39,rtrep=.f.,left=149,top=155,width=136,height=21;
    , ToolTipText = "Selezione dello stato dei movimenti da stampare";
    , HelpContextID = 162455050;
    , cFormVar="w_PROVVI",RowSource=""+"Confermati,"+"Provvisori,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROVVI_1_39.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oPROVVI_1_39.GetRadio()
    this.Parent.oContained.w_PROVVI = this.RadioValue()
    return .t.
  endfunc

  func oPROVVI_1_39.SetRadio()
    this.Parent.oContained.w_PROVVI=trim(this.Parent.oContained.w_PROVVI)
    this.value = ;
      iif(this.Parent.oContained.w_PROVVI=='N',1,;
      iif(this.Parent.oContained.w_PROVVI=='S',2,;
      iif(this.Parent.oContained.w_PROVVI=='',3,;
      0)))
  endfunc

  func oPROVVI_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ESSALDI<>'S')
    endwith
   endif
  endfunc

  add object oBUDESCRI_1_51 as StdField with uid="UKZVKYVIJC",rtseq=42,rtrep=.f.,;
    cFormVar = "w_BUDESCRI", cQueryName = "BUDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 1012831,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=200, Top=99, InputMask=replicate('X',35)

  func oBUDESCRI_1_51.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oCAMVAL_1_56 as StdField with uid="SQCMWJRTOT",rtseq=44,rtrep=.f.,;
    cFormVar = "w_CAMVAL", cQueryName = "CAMVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio in uscita nei confronti della moneta di conto",;
    HelpContextID = 134156250,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=547, Top=70, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oCAMVAL_1_56.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (i_datsys<.w_DATEMU or .w_EXTRAEUR=0)
    endwith
   endif
  endfunc


  add object oMODSTA_1_57 as StdCombo with uid="HXEQAESRKC",rtseq=45,rtrep=.f.,left=418,top=287,width=155,height=21;
    , ToolTipText = "Selezioni di stampa";
    , HelpContextID = 30576954;
    , cFormVar="w_MODSTA",RowSource=""+"Sezioni unite,"+"Sezioni contrapposte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMODSTA_1_57.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oMODSTA_1_57.GetRadio()
    this.Parent.oContained.w_MODSTA = this.RadioValue()
    return .t.
  endfunc

  func oMODSTA_1_57.SetRadio()
    this.Parent.oContained.w_MODSTA=trim(this.Parent.oContained.w_MODSTA)
    this.value = ;
      iif(this.Parent.oContained.w_MODSTA=='U',1,;
      iif(this.Parent.oContained.w_MODSTA=='C',2,;
      0))
  endfunc


  add object oObj_1_59 as cp_runprogram with uid="QRKOEPAQQJ",left=7, top=334, width=252,height=26,;
    caption='Changeval',;
   bGlobalFont=.t.,;
    prg="CambiaV",;
    cEvent = "w_MODSTA Changed",;
    nPag=1;
    , HelpContextID = 58351175


  add object oBtn_1_67 as StdButton with uid="ZMIZZNTUDJ",left=673, top=281, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare";
    , HelpContextID = 615462;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_67.Click()
      with this.Parent.oContained
        do GSCG_BPC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_67.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((.w_monete='c' or ((not empty(.w_DIVISA)) and (not empty(.w_CAMVAL)))))
      endwith
    endif
  endfunc


  add object oBtn_1_68 as StdButton with uid="JWIBHYOYEX",left=727, top=281, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 133856698;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_68.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oEXCEL_1_71 as StdCheck with uid="YJQCVXLQLM",rtseq=54,rtrep=.f.,left=616, top=201, caption="Stampa su Excel",;
    ToolTipText = "Invia i dati su foglio Excel",;
    HelpContextID = 56662202,;
    cFormVar="w_EXCEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEXCEL_1_71.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oEXCEL_1_71.GetRadio()
    this.Parent.oContained.w_EXCEL = this.RadioValue()
    return .t.
  endfunc

  func oEXCEL_1_71.SetRadio()
    this.Parent.oContained.w_EXCEL=trim(this.Parent.oContained.w_EXCEL)
    this.value = ;
      iif(this.Parent.oContained.w_EXCEL=='S',1,;
      0)
  endfunc

  func oEXCEL_1_71.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((.w_MODSTA='U' AND .w_LIVELLI<>'S') OR .w_TIPSTA='M')
    endwith
   endif
  endfunc


  add object oObj_1_72 as cp_setobjprop with uid="LYWRJLJMCW",left=896, top=160, width=38,height=38,;
    caption='Object',;
   bGlobalFont=.t.,;
    cObj="w_EXCEL",cProp="CAPTION",;
    nPag=1;
    , HelpContextID = 36823526

  add object oStr_1_40 as StdString with uid="MNVXYSSEMS",Visible=.t., Left=7, Top=183,;
    Alignment=1, Width=136, Height=15,;
    Caption="Sezione bilancio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="VTWINQAPCZ",Visible=.t., Left=424, Top=39,;
    Alignment=1, Width=22, Height=15,;
    Caption="al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="AKCVVOGMSL",Visible=.t., Left=7, Top=70,;
    Alignment=1, Width=136, Height=15,;
    Caption="Valuta di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="KMJTOWRTFW",Visible=.t., Left=283, Top=70,;
    Alignment=1, Width=54, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="HSXYJERYAJ",Visible=.t., Left=55, Top=39,;
    Alignment=1, Width=88, Height=15,;
    Caption="Tipo bilancio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="EWRJYHQLIT",Visible=.t., Left=7, Top=11,;
    Alignment=1, Width=136, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="NBOSTBYBDU",Visible=.t., Left=7, Top=127,;
    Alignment=1, Width=136, Height=15,;
    Caption="Gruppo B.Unit:"  ;
  , bGlobalFont=.t.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="LMYPIRMDKQ",Visible=.t., Left=282, Top=39,;
    Alignment=1, Width=55, Height=18,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="WRQNIZIRVJ",Visible=.t., Left=462, Top=70,;
    Alignment=1, Width=81, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="WVPPOUXRHH",Visible=.t., Left=7, Top=99,;
    Alignment=1, Width=136, Height=15,;
    Caption="B. Unit:"  ;
  , bGlobalFont=.t.

  func oStr_1_52.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="FNFOJBXIII",Visible=.t., Left=7, Top=155,;
    Alignment=1, Width=136, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="XTZRVTUIZE",Visible=.t., Left=294, Top=259,;
    Alignment=1, Width=122, Height=15,;
    Caption="Selezione stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_58 as StdString with uid="RITZPMSAXD",Visible=.t., Left=291, Top=287,;
    Alignment=1, Width=124, Height=15,;
    Caption="Modalit� di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="TRQCFYUXYJ",Visible=.t., Left=8, Top=365,;
    Alignment=0, Width=415, Height=19,;
    Caption="Attenzione aggiungere eventuali variabili anche a gscg_scf"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_69.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_spc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_spc
PROC CAMBIAV (Parent)
with Parent
if .w_MODSTA="C"
   .w_attivita='A'
   .w_passivi='P'
   .w_costi="C"
   .w_ricavi="R"
   .w_SEZVAR="S"
   .w_PERIODO="N"
   .w_ORDINE="O"
   .w_TRANSI="T"
   .w_TUTTI="S"
   .w_TIPSTA="T"
endif
endwith

ENDPROC
* --- Fine Area Manuale
