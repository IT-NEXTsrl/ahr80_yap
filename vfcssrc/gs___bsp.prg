* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gs___bsp                                                        *
*              Crea driver stampa PDF                                          *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_15]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-20                                                      *
* Last revis.: 2016-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgs___bsp",oParentObject)
return(i_retval)

define class tgs___bsp as StdBatch
  * --- Local variables
  w_MESS = space(200)
  w_POS = 0
  w_oMess = .NULL.
  w_oPart = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea la stampante per creazione file PDF
    * --- Verifico che ad hoc sia stato lanciato come amministratore
    if not cp_IsAdminWin()
      if ah_YesNo(MSG_RESTART_AS_ADMIN_QP)
        AdHocRunAs()
      endif
      i_retcode = 'stop'
      return
    endif
    * --- Inizializzo w_POS a 0 in modo che se non vi sono stampanti installate la creo
    this.w_POS = 0
    DECLARE LONG PDFDriverInit in cdintf.dll STRING @ szPrinter
    APRINTERS(El_Printer)
    * --- Se non vi sono stampanti El_Printer non viene creato
    if Type("El_Printer") = "C"
      * --- Verifico se la stampante � gia presente
      this.w_POS = ASCAN( El_Printer , "PDF ADHOC" ) 
    endif
    if this.w_POS=0
      PDF = PDFDriverInit( "PDF ADHOC" )
      if PDF=0
        * --- Oggetto per messaggi incrementali
        this.w_oMess=createobject("Ah_Message")
        this.w_oPART = this.w_oMESS.addmsgpartNL("Errore installazione driver di stampa PDF %1")
        this.w_oPART.addParam(Message())     
        this.w_oMess.AddMsgPart("Verificare i diritti dell'utente su questa macchina e%0consultare le note di installazione per verificare l'eventuale presenza di file%0relativi al driver PDF, con versioni errate")     
        this.w_oMess.Ah_ErrorMsg()     
      else
        ah_ErrorMsg("Stampante PDF installata con successo","!","")
      endif
    else
      ah_ErrorMsg("Stampante PDF precedentemente installata",,"")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
