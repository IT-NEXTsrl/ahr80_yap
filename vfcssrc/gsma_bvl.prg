* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bvl                                                        *
*              Variazione listino prezzi                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_419]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-05                                                      *
* Last revis.: 2010-08-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bvl",oParentObject)
return(i_retval)

define class tgsma_bvl as StdBatch
  * --- Local variables
  w_CODART = space(20)
  w_PERIVA = 0
  w_PREZZOIN = 0
  w_ROWNUM = 0
  w_RICALCO = .f.
  w_PREZZOBA = 0
  w_CALCOLA = 0
  w_SCORPO = 0
  w_PREZZOFI = 0
  w_NUMROW = 0
  w_PERCRIC = 0
  w_MESS = space(10)
  w_PREZSCOR = 0
  w_VALORE = 0
  w_OK = .f.
  w_PREZZOFI = 0
  w_AGGIORNO = .f.
  w_TROV = .f.
  w_SCORPORO = .f.
  w_QUANTI = 0
  w_VALCAMBI = .f.
  w_PREZZO = 0
  w_FLSCON = space(1)
  w_LISCONT1 = 0
  w_LISCONT2 = 0
  w_LISCONT3 = 0
  w_LISCONT4 = 0
  w_VALUCA = 0
  w_VALUPV = 0
  w_PERCENT = 0
  w_CODRIC = space(5)
  w_OLDART = space(20)
  w_CODLIS = space(4)
  w_LCODLIS = space(4)
  * --- WorkFile variables
  LIS_SCAG_idx=0
  LIS_TINI_idx=0
  RIC_PREZ_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dalla maschera Variazione Listino Prezzi (GSMA_KVL)
    * --- Inizializzazione variabili globali.
    * --- Inizializzazione variabili Locali
    this.w_LISCONT1 = this.oParentObject.w_SCONT1
    this.w_LISCONT2 = this.oParentObject.w_SCONT2
    this.w_LISCONT3 = this.oParentObject.w_SCONT3
    this.w_LISCONT4 = this.oParentObject.w_SCONT4
    * --- Controllo campi obbligatori
    do case
      case EMPTY(this.oParentObject.w_LISTINO)
        ah_ErrorMsg("Listino da aggiornare non selezionato")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_DATLISIN)
        ah_ErrorMsg("La data di inizio validit� del listino non � stata selezionata")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_DATLISFI)
        ah_ErrorMsg("La data di fine validit� del listino non � stata selezionata")
        i_retcode = 'stop'
        return
      case this.oParentObject.w_CRITER<>4 AND this.oParentObject.w_CRITER<>5 AND this.oParentObject.w_CRITER<>9 AND this.oParentObject.w_CRITER<>10 AND EMPTY(this.oParentObject.w_INNUMINV)
        ah_ErrorMsg("L'inventario di riferimento non � stato selezionato")
        i_retcode = 'stop'
        return
    endcase
    do case
      case this.oParentObject.w_CRITER=4
        * --- Selezionato l'aggiornamento da un Listino di Riferimento
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_CRITER=5
        * --- Selezionato l'aggiornamento dai Costi Standard
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_CRITER=9
        * --- Selezionato l'aggiornamento dall'Ultimo Costo dei Saldi
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.oParentObject.w_CRITER=10
        * --- Selezionato l'aggiornamento dall'Ultimo Costo dei Saldi
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      otherwise
        * --- Selezionato l'aggiornamento da un Inventario
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
    endcase
    USE IN SELECT("LISTDATA")
    USE IN SELECT("LISTRIFE")
    USE IN SELECT("LISTINO")
    USE IN SELECT("INVENTAR")
    USE IN SELECT("SCAGLION")
    USE IN SELECT("ULTCOST")
    USE IN SELECT("ULTPREV")
    USE IN SELECT("LISTIN1")
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rivalorizza da un Listino di Riferimento
    * --- Eseguo la query che legge i dati in base al listino di riferimento selezionato
    if this.oParentObject.w_RICALC="R"
      vq_exec("query\GSMA6BVL.VQR",this,"LISTDATA")
    else
      vq_exec("query\GSMA1BVL.VQR",this,"LISTDATA")
    endif
    * --- Per la Stessa Data di Riferimento possono esserci piu' Listini Validi
    * --- Per ciascun Articolo prendo solo il Primo record (gia' ordinato sulla data di Attivazione piu' prossima)
    this.w_OLDART = REPLICATE("X", 20)
    CREATE CURSOR LISTRIFE (CODART C(20), CODLIS C(4), NUMROW N(4,0), PERIVA N(5,2), PERCEN N(6,2))
    SELECT LISTDATA
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODART, ""))
    if this.w_OLDART<>NVL(CODART, SPACE(20)) AND NVL(CODLIS, SPACE(4))=this.oParentObject.w_LISTRIFE
      this.w_CODART = NVL(CODART,SPACE(20))
      this.w_CODLIS = this.oParentObject.w_LISTINO
      this.w_NUMROW = NVL(NUMROW, 0)
      this.w_PERIVA = NVL(PERIVA, 0)
      this.w_PERCENT = NVL(PERCEN, 0)
      INSERT INTO LISTRIFE (CODART, CODLIS, NUMROW, PERIVA, PERCEN) ;
      VALUES (this.w_CODART, this.w_CODLIS, this.w_NUMROW, this.w_PERIVA, this.w_PERCENT)
      this.w_OLDART = this.w_CODART
    endif
    SELECT LISTDATA
    ENDSCAN
    SELECT LISTDATA
    USE
    * --- Eseguo la query che legge tutti gli Articoli con Listino e Date uguali a quelle da Aggiornare
    * --- Serve per discriminare se Esiste gia' il Listino da Aggiornare o deve essere caricato Ex Novo
    vq_exec("query\GSMA2BVL.VQR",this,"LISTINO")
    * --- Controllo se il cursore creato ha almeno un record.
    if RECCOUNT("LISTRIFE")>0
      * --- Aggiorno i prezzi dei listini
      this.w_SCORPORO = .F.
      this.w_RICALCO = .F.
      * --- Try
      local bErr_03A87130
      bErr_03A87130=bTrsErr
      this.Try_03A87130()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("Errore durante l'inserimento")
      endif
      bTrsErr=bTrsErr or bErr_03A87130
      * --- End
    else
      ah_ErrorMsg("Per le selezioni impostate non esistono listini-prezzi da aggiornare")
    endif
  endproc
  proc Try_03A87130()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT LISTRIFE
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODART,""))
    this.w_CODART = CODART
    this.w_NUMROW = NUMROW
    this.w_PERIVA = PERIVA
    this.w_PERCENT = PERCEN
    * --- vado ad aggiornare gli Articoli del listino prezzi di arrivo
    this.w_MESS = ALLTR(this.w_CODART)
    ah_Msg("Aggiorna listini articolo: %1",.T.,.F.,.F., this.w_MESS)
    * --- Cerca i Listini che gia' Esistono
    SELECT LISTINO
    GO TOP
    LOCATE FOR CODART=this.w_CODART
    this.w_OK = FOUND()
    if this.oParentObject.w_CREALIS="S" AND this.w_OK=.F.
      * --- Se non trovato Listino e Flag Solo Esistenti Attivo passa al record Successivo...
      SELECT LISTRIFE
      LOOP
    endif
    * --- Leggo gli Scaglioni del Listino di Riferimento
    vq_exec("query\GSMA3BVL.VQR",this,"SCAGLION")
    SELECT LISTINO
    if this.w_OK=.T.
      * --- Il Listino Origine Esiste (Varia)
      this.w_ROWNUM = NUMORI
      * --- Azzero gli Scaglioni precedenti (inseriti piu' Avanti da quelli del Listino di riferimento)
      * --- Delete from LIS_SCAG
      i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"LICODART = "+cp_ToStrODBC(this.w_CODART);
              +" and LIROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
               )
      else
        delete from (i_cTable) where;
              LICODART = this.w_CODART;
              and LIROWNUM = this.w_ROWNUM;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    else
      * --- Il Listino Origine non Esiste
      * --- Cerco l' Ultima CPROWNUM di ogni listino all' interno di ogni articolo
      vq_exec("query\GSMA0BVL.VQR",this,"CPROW")
      SELECT CPROWNUM FROM CPROW INTO ARRAY L_ROWNUM
      * --- Assegno la Prima CPROWNUM Disponibile
      this.w_ROWNUM = IIF( _TALLY<>0, L_ROWNUM+1, 1)
      SELECT CPROW
      USE
      * --- Inserisco il Nuovo Listino
      * --- Insert into LIS_TINI
      i_nConn=i_TableProp[this.LIS_TINI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_TINI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"LICODART"+",CPROWNUM"+",LICODLIS"+",LIDATATT"+",LIDATDIS"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_TINI','LICODART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'LIS_TINI','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LISTINO),'LIS_TINI','LICODLIS');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATLISIN),'LIS_TINI','LIDATATT');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATLISFI),'LIS_TINI','LIDATDIS');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'CPROWNUM',this.w_ROWNUM,'LICODLIS',this.oParentObject.w_LISTINO,'LIDATATT',this.oParentObject.w_DATLISIN,'LIDATDIS',this.oParentObject.w_DATLISFI)
        insert into (i_cTable) (LICODART,CPROWNUM,LICODLIS,LIDATATT,LIDATDIS &i_ccchkf. );
           values (;
             this.w_CODART;
             ,this.w_ROWNUM;
             ,this.oParentObject.w_LISTINO;
             ,this.oParentObject.w_DATLISIN;
             ,this.oParentObject.w_DATLISFI;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='INSERT LISTINI'
        return
      endif
      if this.oParentObject.w_RICALC="R"
        * --- Leggo la percentuale di ricarico
        * --- Read from RIC_PREZ
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.RIC_PREZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.RIC_PREZ_idx,2],.t.,this.RIC_PREZ_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LIPERRIC"+;
            " from "+i_cTable+" RIC_PREZ where ";
                +"LICODART = "+cp_ToStrODBC(this.w_CODART);
                +" and LICODLIS = "+cp_ToStrODBC(this.oParentObject.w_LISTINO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LIPERRIC;
            from (i_cTable) where;
                LICODART = this.w_CODART;
                and LICODLIS = this.oParentObject.w_LISTINO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PERCENT = NVL(cp_ToDate(_read_.LIPERRIC),cp_NullValue(_read_.LIPERRIC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
    endif
    * --- Leggo gli Scaglioni del Listino di Riferimento
    SELECT SCAGLION
    GO TOP
    SCAN FOR NVL(LIPREZZO,0)<>0
    this.w_QUANTI = LIQUANTI
    this.w_PREZZO = LIPREZZO
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Scrivo i Nuovi Scaglioni
    if this.oParentObject.w_AGSCO<>"F" 
      this.w_LISCONT1 = NVL(SCAGLION.LISCONT1,0)
      this.w_LISCONT2 = NVL(SCAGLION.LISCONT2,0)
      this.w_LISCONT3 = NVL(SCAGLION.LISCONT3, 0)
      this.w_LISCONT4 = NVL(SCAGLION.LISCONT4, 0)
    endif
    if this.w_PREZZOFI<>0 
      * --- Insert into LIS_SCAG
      i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+",LIPREZZO"+",LISCONT1"+",LISCONT2"+",LISCONT3"+",LISCONT4"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_SCAG','LICODART');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'LIS_SCAG','LIROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_QUANTI),'LIS_SCAG','LIQUANTI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
        +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'LIROWNUM',this.w_ROWNUM,'LIQUANTI',this.w_QUANTI,'LIPREZZO',this.w_PREZZOFI,'LISCONT1',this.w_LISCONT1,'LISCONT2',this.w_LISCONT2,'LISCONT3',this.w_LISCONT3,'LISCONT4',this.w_LISCONT4)
        insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI,LIPREZZO,LISCONT1,LISCONT2,LISCONT3,LISCONT4 &i_ccchkf. );
           values (;
             this.w_CODART;
             ,this.w_ROWNUM;
             ,this.w_QUANTI;
             ,this.w_PREZZOFI;
             ,this.w_LISCONT1;
             ,this.w_LISCONT2;
             ,this.w_LISCONT3;
             ,this.w_LISCONT4;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error='Errore Inserimento Scaglioni Listini'
        return
      endif
    endif
    SELECT SCAGLION
    ENDSCAN
    SELECT LISTRIFE
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Elaborazione terminata")
    return


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo il prezzo del listino tramite l'inventario di riferimento selezionato sulla maschera
    * --- Controllo se ho selezionato il check 'Solo Esistenti'
    * --- Siccome l'inventario tiene conto di prezzi sempre al netto assegno
    * --- alla variabile w_LORNET1 il valore 'N' (Netto).
    * --- L'inventario ha come valuta quella dell'esercizio in cui � stato effettuato.
    this.oParentObject.w_LORNET1 = "N"
    this.oParentObject.w_VALRIF = this.oParentObject.w_VALNAZ
    this.oParentObject.w_CAMBIO = this.oParentObject.w_CAMBIO2
    * --- Eseguo la query che legge i dati in base all'nventario di riferimento selezionato.
    * --- A seconda del criterio selezionato il valore che assume il campo prezzo pu� essere
    * --- riferito all'ultimo Costo , Costo Medio Ponderato Annuo, Costo Medio Ponderato Periodo.
    if this.oParentObject.w_RICALC<>"R"
      vq_exec("query\GSMA4BVL.VQR",this,"INVENTAR")
    else
      vq_exec("query\GSMA11BV.VQR",this,"INVENTAR")
    endif
    * --- Eseguo la query che legge tutti gli Articoli con Listino e Date uguali a quelle da Aggiornare
    if this.oParentObject.w_RICALC<>"R"
      vq_exec("query\GSMA_BVL.VQR",this,"LISTINO")
    else
      vq_exec("query\GSMA_BV3.VQR",this,"LISTINO")
    endif
    * --- Controllo se il cursore creato ha almeno un record.
    if RECCOUNT("LISTINO")>0 OR (RECCOUNT("INVENTAR")>0 AND this.oParentObject.w_CREALIS<>"S")
      * --- Aggiorno i prezzi dei listini
      this.w_SCORPORO = .F.
      this.w_RICALCO = .F.
      * --- Try
      local bErr_03AC24D8
      bErr_03AC24D8=bTrsErr
      this.Try_03AC24D8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("Errore durante l'inserimento")
      endif
      bTrsErr=bTrsErr or bErr_03AC24D8
      * --- End
    else
      ah_ErrorMsg("Per le selezioni impostate non esistono listini-prezzi da aggiornare")
    endif
  endproc
  proc Try_03AC24D8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT LISTINO
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODART, ""))
    this.w_CODART = CODART
    this.w_ROWNUM = NVL(NUMORI, 0)
    this.w_PERIVA = NVL(PERIVA, 0)
    this.w_PERCENT = NVL(PERCEN, 0)
    this.w_LCODLIS = Nvl(LICODLIS,SPACE(4))
    * --- vado ad aggiornare gli Articoli del listino prezzi di arrivo
    this.w_MESS = ALLTR(this.w_CODART)
    ah_Msg("Aggiorna listini articolo: %1",.T.,.F.,.F., this.w_MESS)
    * --- Cerca i dati sull Inventario
    SELECT INVENTAR
    GO TOP
    LOCATE FOR CODART=this.w_CODART
    if NOT FOUND()
      * --- Se non trovato Articolo in Inventario Cicla al Record Successivo...
      SELECT LISTINO
      LOOP
    else
      * --- Articolo presente sull'Inventario , Aggiorna
      do case
        case this.oParentObject.w_CRITER=1
          this.w_PREZZO = COSULT
        case this.oParentObject.w_CRITER=2
          this.w_PREZZO = COSMPA
        case this.oParentObject.w_CRITER=3
          this.w_PREZZO = COSMPP
        case this.oParentObject.w_CRITER=6
          this.w_PREZZO = COSLCO
        case this.oParentObject.w_CRITER=7
          this.w_PREZZO = COSLSC
        case this.oParentObject.w_CRITER=8
          this.w_PREZZO = COSFCO
      endcase
      * --- Prezzo Base quello letto dall'Inventario
      this.w_PREZZOIN = this.w_PREZZO
      this.w_PREZZOBA = -9876
      * --- Leggo gli Scaglioni del Listino di Origine
      this.w_TROV = .F.
      * --- Select from GSMA9BVL
      do vq_exec with 'GSMA9BVL',this,'_Curs_GSMA9BVL','',.f.,.t.
      if used('_Curs_GSMA9BVL')
        select _Curs_GSMA9BVL
        locate for 1=1
        do while not(eof())
        this.w_TROV = .T.
        this.w_PREZZOFI = 0
        this.w_QUANTI = _Curs_GSMA9BVL.LIQUANTI
        * --- Prezzo Base, Primo importo degli Scaglioni
        if this.w_PREZZOBA=-9876
          this.w_PREZZOBA = _Curs_GSMA9BVL.LIPREZZO
          this.w_PREZZO = this.w_PREZZOIN
        else
          * --- Gli Altri scaglioni sono calcolati in proporzione
          this.w_PREZZO = (this.w_PREZZOIN * _Curs_GSMA9BVL.LIPREZZO) / this.w_PREZZOBA
        endif
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_PREZZOFI<>0
          * --- Write into LIS_SCAG
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.LIS_SCAG_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LIPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
            +",LISCONT1 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
            +",LISCONT2 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
            +",LISCONT3 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
            +",LISCONT4 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
                +i_ccchkf ;
            +" where ";
                +"LICODART = "+cp_ToStrODBC(this.w_CODART);
                +" and LIROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                +" and LIQUANTI = "+cp_ToStrODBC(this.w_QUANTI);
                   )
          else
            update (i_cTable) set;
                LIPREZZO = this.w_PREZZOFI;
                ,LISCONT1 = this.w_LISCONT1;
                ,LISCONT2 = this.w_LISCONT2;
                ,LISCONT3 = this.w_LISCONT3;
                ,LISCONT4 = this.w_LISCONT4;
                &i_ccchkf. ;
             where;
                LICODART = this.w_CODART;
                and LIROWNUM = this.w_ROWNUM;
                and LIQUANTI = this.w_QUANTI;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
          select _Curs_GSMA9BVL
          continue
        enddo
        use
      endif
      if this.w_TROV=.F.
        * --- Il Listino esiste ma non sono presenti scaglioni di origine ne inserisce uno come nuovo
        SELECT LISTINO
        * --- w_PREZZO gia' calcolato sopra
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Scrivo i Nuovi Scaglioni
        if this.w_PREZZOFI<>0
          this.w_QUANTI = 0
          * --- Insert into LIS_SCAG
          i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+",LIPREZZO"+",LISCONT1"+",LISCONT2"+",LISCONT3"+",LISCONT4"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_SCAG','LICODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'LIS_SCAG','LIROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_QUANTI),'LIS_SCAG','LIQUANTI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'LIROWNUM',this.w_ROWNUM,'LIQUANTI',this.w_QUANTI,'LIPREZZO',this.w_PREZZOFI,'LISCONT1',this.w_LISCONT1,'LISCONT2',this.w_LISCONT2,'LISCONT3',this.w_LISCONT3,'LISCONT4',this.w_LISCONT4)
            insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI,LIPREZZO,LISCONT1,LISCONT2,LISCONT3,LISCONT4 &i_ccchkf. );
               values (;
                 this.w_CODART;
                 ,this.w_ROWNUM;
                 ,this.w_QUANTI;
                 ,this.w_PREZZOFI;
                 ,this.w_LISCONT1;
                 ,this.w_LISCONT2;
                 ,this.w_LISCONT3;
                 ,this.w_LISCONT4;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      endif
      * --- Se Crea nuovi Listini Elimino il record elaborato dal file INVENTAR
      if this.oParentObject.w_CREALIS<>"S"
        * --- Quelli che rimarranno al termine andranno ad aggiornare in Insert i Listini
        SELECT INVENTAR
        DELETE
      endif
    endif
    SELECT LISTINO
    ENDSCAN
    * --- Se Crea nuovi Listini Inserisco dai record non cancellati in INVENTAR
    if this.oParentObject.w_CREALIS<>"S"
      SELECT INVENTAR
      if RECCOUNT()>0
        SCAN FOR NOT DELETED() AND NOT EMPTY(NVL(CODART, ""))
        * --- Inserisco il Nuovo Listino
        this.w_CODART = CODART
        this.w_PERIVA = NVL(PERIVA, 0)
        this.w_PERCENT = NVL(PERCEN, 0)
        do case
          case this.oParentObject.w_CRITER=1
            this.w_PREZZO = COSULT
          case this.oParentObject.w_CRITER=2
            this.w_PREZZO = COSMPA
          case this.oParentObject.w_CRITER=3
            this.w_PREZZO = COSMPP
          case this.oParentObject.w_CRITER=6
            this.w_PREZZO = COSLCO
          case this.oParentObject.w_CRITER=7
            this.w_PREZZO = COSLSC
          case this.oParentObject.w_CRITER=8
            this.w_PREZZO = COSFCO
        endcase
        this.w_PREZZOIN = this.w_PREZZO
        * --- Cerco l' Ultima CPROWNUM di ogni listino all' interno di ogni articolo
        vq_exec("query\GSMA0BVL.VQR",this,"CPROW")
        SELECT CPROWNUM FROM CPROW INTO ARRAY L_ROWNUM
        * --- Assegno la Prima CPROWNUM Disponibile
        this.w_ROWNUM = IIF( _TALLY<>0, L_ROWNUM+1, 1)
        SELECT CPROW
        USE
        SELECT INVENTAR
        this.w_PREZZOFI = 0
        this.w_MESS = ALLTR(this.w_CODART)
        ah_Msg("Inserisce listini articolo: %1",.T.,.F.,.F., this.w_MESS)
        * --- Insert into LIS_TINI
        i_nConn=i_TableProp[this.LIS_TINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_TINI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LICODART"+",CPROWNUM"+",LICODLIS"+",LIDATATT"+",LIDATDIS"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_TINI','LICODART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'LIS_TINI','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LISTINO),'LIS_TINI','LICODLIS');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATLISIN),'LIS_TINI','LIDATATT');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATLISFI),'LIS_TINI','LIDATDIS');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'CPROWNUM',this.w_ROWNUM,'LICODLIS',this.oParentObject.w_LISTINO,'LIDATATT',this.oParentObject.w_DATLISIN,'LIDATDIS',this.oParentObject.w_DATLISFI)
          insert into (i_cTable) (LICODART,CPROWNUM,LICODLIS,LIDATATT,LIDATDIS &i_ccchkf. );
             values (;
               this.w_CODART;
               ,this.w_ROWNUM;
               ,this.oParentObject.w_LISTINO;
               ,this.oParentObject.w_DATLISIN;
               ,this.oParentObject.w_DATLISFI;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_QUANTI = 0
        this.w_PREZZO = this.w_PREZZOIN
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Scrivo i Nuovi Scaglioni
        if this.w_PREZZOFI<>0
          * --- Insert into LIS_SCAG
          i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+",LIPREZZO"+",LISCONT1"+",LISCONT2"+",LISCONT3"+",LISCONT4"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_SCAG','LICODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'LIS_SCAG','LIROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_QUANTI),'LIS_SCAG','LIQUANTI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'LIROWNUM',this.w_ROWNUM,'LIQUANTI',this.w_QUANTI,'LIPREZZO',this.w_PREZZOFI,'LISCONT1',this.w_LISCONT1,'LISCONT2',this.w_LISCONT2,'LISCONT3',this.w_LISCONT3,'LISCONT4',this.w_LISCONT4)
            insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI,LIPREZZO,LISCONT1,LISCONT2,LISCONT3,LISCONT4 &i_ccchkf. );
               values (;
                 this.w_CODART;
                 ,this.w_ROWNUM;
                 ,this.w_QUANTI;
                 ,this.w_PREZZOFI;
                 ,this.w_LISCONT1;
                 ,this.w_LISCONT2;
                 ,this.w_LISCONT3;
                 ,this.w_LISCONT4;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        SELECT INVENTAR
        ENDSCAN
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Elaborazione terminata")
    return


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- calcola Prezzo Finale
    * --- Azzero le variabili utilizzate per i calcoli
    this.w_SCORPO = 0
    this.w_CALCOLA = 0
    this.w_PREZSCOR = 0
    this.w_PREZZOFI = 0
    * --- Controllo se i due listini sono uno netto e l'altro lordo oppure viceversa.
    if this.oParentObject.w_LORNET<>this.oParentObject.w_LORNET1
      * --- Variabile che mi dice che � stato effettuato uno scorporo.
      this.w_SCORPORO = .T.
      this.w_SCORPO = this.w_PERIVA+100
      * --- Controllo se il listino di riferimento � lordo oppure netto
      if this.oParentObject.w_LORNET1="L"
        * --- Il listino di riferimento � lordo mentre il listino di arrivo � netto
        this.w_PREZSCOR = (this.w_PREZZO*100)/this.w_SCORPO
      else
        * --- Il listino di riferimento � netto ed il listino di arrivo � lordo
        this.w_PREZSCOR = (this.w_PREZZO*this.w_SCORPO)/100
      endif
    else
      * --- Nessuno scorporo effettuato.
      this.w_SCORPORO = .F.
    endif
    * --- Controllo se i due listini hanno la stassa valuta
    if this.oParentObject.w_VALUTA=this.oParentObject.w_VALRIF
      * --- Non devo operare nessun cambio
      this.w_VALCAMBI = .F.
    else
      * --- Variabile che mi dice che sono presenti listini con valute diverse.
      this.w_VALCAMBI = .T.
      * --- Controllo se � stato effettuato uno scorporo.
      if this.w_SCORPORO=.T.
        this.w_PREZZOFI = (this.w_PREZSCOR/this.oParentObject.w_CAMBIO)*this.oParentObject.w_CAMBIO1
      else
        this.w_PREZZOFI = (this.w_PREZZO/this.oParentObject.w_CAMBIO)*this.oParentObject.w_CAMBIO1
      endif
    endif
    * --- Controllo che tipo di ricalcolo � stato utilizzato.
    if this.oParentObject.w_RICALC="R" OR this.oParentObject.w_RICALC="S"
      this.w_PERCRIC = this.w_PERCENT
    else
      this.w_PERCRIC = this.oParentObject.w_RICPE1
    endif
    if this.w_PERCRIC=0
      * --- Non ho inserito nessuna percentuale di ricalcolo
      this.w_RICALCO = .F.
    else
      this.w_RICALCO = .T.
      * --- Controllo se i due listini hanno la stassa valuta
      if this.w_VALCAMBI=.T.
        * --- Calcolo il prezzo in base alla percentuale di ricalcolo (Scorporo)
        this.w_CALCOLA = (this.w_PREZZOFI*this.w_PERCRIC)/100
        this.w_PREZZOFI = (this.w_PREZZOFI+this.w_CALCOLA)
      else
        * --- Controllo se � stato effettuato uno scorporo.
        if this.w_SCORPORO=.T.
          this.w_CALCOLA = (this.w_PREZSCOR*this.w_PERCRIC)/100
          this.w_PREZZOFI = (this.w_PREZSCOR+this.w_CALCOLA)
        else
          this.w_CALCOLA = (this.w_PREZZO*this.w_PERCRIC)/100
          this.w_PREZZOFI = (this.w_PREZZO+this.w_CALCOLA)
        endif
      endif
    endif
    * --- Controllo se � stata inserita la percentuale di ricalcolo.
    if this.w_RICALCO=.T.
      * --- Inserisco il parametro del ricalcolo in valore inserito
      this.w_PREZZOFI = this.w_PREZZOFI+this.oParentObject.w_RICVA1
    else
      * --- Controllo se i due listini hanno la stassa valuta
      if this.w_VALCAMBI=.T.
        this.w_PREZZOFI = this.w_PREZZOFI+this.oParentObject.w_RICVA1
      else
        * --- Controllo se � stato effettuato uno scorporo.
        if this.w_SCORPORO=.T.
          this.w_PREZZOFI = this.w_PREZSCOR+this.oParentObject.w_RICVA1
        else
          this.w_PREZZOFI = this.w_PREZZO+this.oParentObject.w_RICVA1
        endif
      endif
    endif
    * --- Controllo se sono stati inseriti degli arrotondamenti
    if this.oParentObject.w_ARROT1<>0
      * --- Controllo se il prezzo finale � minore del primo arrotondamento
      if this.w_PREZZOFI<=this.oParentObject.w_VALORIN
        * --- Applico l'arrotondamento selezionato.
        this.w_PREZZOFI = cp_ROUND((this.w_PREZZOFI/this.oParentObject.w_ARROT1),0)*this.oParentObject.w_ARROT1
      else
        * --- Controllo se il prezzo finale � minore del secondo arrotondamento
        if this.w_PREZZOFI<=this.oParentObject.w_VALOR2IN
          if this.oParentObject.w_ARROT2<>0
            * --- Applico l'arrotondamento selezionato.
            this.w_PREZZOFI = cp_ROUND((this.w_PREZZOFI/this.oParentObject.w_ARROT2),0)*this.oParentObject.w_ARROT2
          endif
        else
          * --- Controllo se il prezzo finale � minore del terzo arrotondamento
          if this.w_PREZZOFI<=this.oParentObject.w_VALOR3IN
            if this.oParentObject.w_ARROT3<>0
              * --- Applico l'arrotondamento selezionato.
              this.w_PREZZOFI = cp_ROUND((this.w_PREZZOFI/this.oParentObject.w_ARROT3),0)*this.oParentObject.w_ARROT3
            endif
          else
            * --- Controllo se il prezzo finale � minore del quarto arrotondamento
            if this.oParentObject.w_ARROT4<>0
              * --- Applico l'ultimo arrotondamento selezionato.
              this.w_PREZZOFI = cp_ROUND((this.w_PREZZOFI/this.oParentObject.w_ARROT4),0)*this.oParentObject.w_ARROT4
            else
              * --- Il prezzo � maggiore dei vari valori degli arrotondamenti quindi non applico nessun arrotondamento.
              this.w_PREZZOFI = cp_ROUND((this.w_PREZZOFI),this.oParentObject.w_DECTOT)
            endif
          endif
        endif
      endif
    else
      * --- Non ho applicato nessun arrotondamento.Il prezzo finale viene calcolato in base alla valuta del listino finale
      this.w_PREZZOFI = cp_ROUND((this.w_PREZZOFI),this.oParentObject.w_DECTOT)
    endif
  endproc


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo il prezzo del listino leggendo i Costi Standard presenti nei Dati Articoli / Magazzino (Globali)
    * --- Controllo se ho selezionato il check 'Solo Esistenti'
    * --- Variabile w_LORNET1 assegnata a 'N' (Netto).
    * --- L'inventario ha come valuta quella dell'esercizio in cui � stato effettuato.
    this.oParentObject.w_LORNET1 = "N"
    * --- Valua e Cambio dei Costi Standard riferiti alla Valuta di Conto
    this.oParentObject.w_VALRIF = g_PERVAL
    this.oParentObject.w_CAMBIO = g_CAOVAL
    * --- Eseguo la query che legge i Costi Standard significativi presenti nelle Anagrafiche Articoli
    if this.oParentObject.w_RICALC<>"R"
      vq_exec("query\GSMA5BVL.VQR",this,"INVENTAR")
    else
      vq_exec("query\GSMA12BV.VQR",this,"INVENTAR")
    endif
    * --- Eseguo la query che legge tutti gli Articoli con Listino e Date uguali a quelle da Aggiornare
    if this.oParentObject.w_RICALC<>"R"
      vq_exec("query\GSMA_BV6.VQR",this,"LISTINO")
    else
      vq_exec("query\GSMA_BV5.VQR",this,"LISTINO")
    endif
    * --- Controllo se il cursore creato ha almeno un record.
    if RECCOUNT("LISTINO")>0 OR (RECCOUNT("INVENTAR")>0 AND this.oParentObject.w_CREALIS<>"S")
      * --- Aggiorno i prezzi dei listini
      this.w_SCORPORO = .F.
      this.w_RICALCO = .F.
      * --- Try
      local bErr_03A891A0
      bErr_03A891A0=bTrsErr
      this.Try_03A891A0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("Errore durante l'inserimento")
      endif
      bTrsErr=bTrsErr or bErr_03A891A0
      * --- End
    else
      ah_ErrorMsg("Per le selezioni impostate non esistono listini-prezzi da aggiornare")
    endif
  endproc
  proc Try_03A891A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT LISTINO
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODART, ""))
    this.w_CODART = CODART
    this.w_PERIVA = NVL(PERIVA, 0)
    this.w_PERCENT = NVL(PERCEN, 0)
    * --- vado ad aggiornare gli Articoli del listino prezzi di arrivo
    this.w_MESS = ALLTR(this.w_CODART)
    ah_Msg("Aggiorna listini articolo: %1",.T.,.F.,.F., this.w_MESS)
    * --- Cerca i dati sull Inventario
    SELECT INVENTAR
    GO TOP
    LOCATE FOR CODART=this.w_CODART
    if NOT FOUND()
      * --- Se non trovato Articolo in Dati Articoli/Magazzino, Cicla al Record Successivo...
      SELECT LISTINO
      LOOP
    else
      * --- Read from LIS_TINI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.LIS_TINI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2],.t.,this.LIS_TINI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "LICODLIS,CPROWNUM"+;
          " from "+i_cTable+" LIS_TINI where ";
              +"LICODART = "+cp_ToStrODBC(this.w_CODART);
              +" and LICODLIS = "+cp_ToStrODBC(this.oParentObject.w_LISTINO);
              +" and LIDATATT = "+cp_ToStrODBC(this.oParentObject.w_DATLISIN);
              +" and LIDATDIS = "+cp_ToStrODBC(this.oParentObject.w_DATLISFI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          LICODLIS,CPROWNUM;
          from (i_cTable) where;
              LICODART = this.w_CODART;
              and LICODLIS = this.oParentObject.w_LISTINO;
              and LIDATATT = this.oParentObject.w_DATLISIN;
              and LIDATDIS = this.oParentObject.w_DATLISFI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_LCODLIS = NVL(cp_ToDate(_read_.LICODLIS),cp_NullValue(_read_.LICODLIS))
        this.w_ROWNUM = NVL(cp_ToDate(_read_.CPROWNUM),cp_NullValue(_read_.CPROWNUM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if I_ROWS>0
        * --- Articolo presente sui Dati Articoli/Magazzino, Aggiorna
        this.w_PREZZO = PREZZO
        * --- Prezzo Base quello letto dal Costo Standard
        this.w_PREZZOIN = this.w_PREZZO
        this.w_PREZZOBA = -9876
        * --- Leggo gli Scaglioni del Listino di Origine
        this.w_TROV = .F.
        * --- Select from GSMA9BVL
        do vq_exec with 'GSMA9BVL',this,'_Curs_GSMA9BVL','',.f.,.t.
        if used('_Curs_GSMA9BVL')
          select _Curs_GSMA9BVL
          locate for 1=1
          do while not(eof())
          this.w_TROV = .T.
          this.w_PREZZOFI = 0
          this.w_QUANTI = _Curs_GSMA9BVL.LIQUANTI
          this.w_ROWNUM = _Curs_GSMA9BVL.LIROWNUM
          * --- Prezzo Base, Primo importo degli Scaglioni
          if this.w_PREZZOBA=-9876
            this.w_PREZZOBA = _Curs_GSMA9BVL.LIPREZZO
            this.w_PREZZO = this.w_PREZZOIN
          else
            * --- Gli Altri scaglioni sono calcolati in proporzione
            this.w_PREZZO = (this.w_PREZZOIN * _Curs_GSMA9BVL.LIPREZZO) / this.w_PREZZOBA
          endif
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_PREZZOFI<>0
            * --- Write into LIS_SCAG
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.LIS_SCAG_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"LIPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
              +",LISCONT1 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
              +",LISCONT2 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
              +",LISCONT3 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
              +",LISCONT4 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
                  +i_ccchkf ;
              +" where ";
                  +"LICODART = "+cp_ToStrODBC(this.w_CODART);
                  +" and LIROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                  +" and LIQUANTI = "+cp_ToStrODBC(this.w_QUANTI);
                     )
            else
              update (i_cTable) set;
                  LIPREZZO = this.w_PREZZOFI;
                  ,LISCONT1 = this.w_LISCONT1;
                  ,LISCONT2 = this.w_LISCONT2;
                  ,LISCONT3 = this.w_LISCONT3;
                  ,LISCONT4 = this.w_LISCONT4;
                  &i_ccchkf. ;
               where;
                  LICODART = this.w_CODART;
                  and LIROWNUM = this.w_ROWNUM;
                  and LIQUANTI = this.w_QUANTI;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
            select _Curs_GSMA9BVL
            continue
          enddo
          use
        endif
        if this.w_TROV=.F.
          * --- Il Listino esiste ma non sono presenti scaglioni di origine ne inserisce uno come nuovo
          SELECT LISTINO
          * --- w_PREZZO gia' calcolato sopra
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Scrivo i Nuovi Scaglioni
          if this.w_PREZZOFI<>0
            this.w_QUANTI = 0
            * --- Insert into LIS_SCAG
            i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+",LIPREZZO"+",LISCONT1"+",LISCONT2"+",LISCONT3"+",LISCONT4"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_SCAG','LICODART');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'LIS_SCAG','LIROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_QUANTI),'LIS_SCAG','LIQUANTI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'LIROWNUM',this.w_ROWNUM,'LIQUANTI',this.w_QUANTI,'LIPREZZO',this.w_PREZZOFI,'LISCONT1',this.w_LISCONT1,'LISCONT2',this.w_LISCONT2,'LISCONT3',this.w_LISCONT3,'LISCONT4',this.w_LISCONT4)
              insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI,LIPREZZO,LISCONT1,LISCONT2,LISCONT3,LISCONT4 &i_ccchkf. );
                 values (;
                   this.w_CODART;
                   ,this.w_ROWNUM;
                   ,this.w_QUANTI;
                   ,this.w_PREZZOFI;
                   ,this.w_LISCONT1;
                   ,this.w_LISCONT2;
                   ,this.w_LISCONT3;
                   ,this.w_LISCONT4;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
        * --- Se Crea nuovi Listini Elimino il record elaborato dal file INVENTAR
        if this.oParentObject.w_CREALIS<>"S"
          * --- Quelli che rimarranno al termine andranno ad aggiornare in Insert i Listini
          SELECT INVENTAR
          DELETE
        endif
      endif
    endif
    SELECT LISTINO
    ENDSCAN
    * --- Se Crea nuovi Listini Inserisco dai record non cancellati in INVENTAR
    if this.oParentObject.w_CREALIS<>"S"
      SELECT INVENTAR
      if RECCOUNT()>0
        SCAN FOR NOT DELETED() AND NOT EMPTY(NVL(CODART, ""))
        * --- Inserisco il Nuovo Listino
        this.w_CODART = CODART
        this.w_PERIVA = NVL(PERIVA, 0)
        this.w_PERCENT = NVL(PERCEN, 0)
        this.w_PREZZOIN = PREZZO
        * --- Cerco l' Ultima CPROWNUM di ogni listino all' interno di ogni articolo
        vq_exec("query\GSMA0BVL.VQR",this,"CPROW")
        SELECT CPROWNUM FROM CPROW INTO ARRAY L_ROWNUM
        * --- Assegno la Prima CPROWNUM Disponibile
        this.w_ROWNUM = IIF( _TALLY<>0, L_ROWNUM+1, 1)
        SELECT CPROW
        USE
        SELECT INVENTAR
        this.w_PREZZOFI = 0
        this.w_MESS = ALLTR(this.w_CODART)
        ah_Msg("Inserisce listini articolo: %1",.T.,.F.,.F., this.w_MESS)
        * --- Insert into LIS_TINI
        i_nConn=i_TableProp[this.LIS_TINI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_TINI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"LICODART"+",CPROWNUM"+",LICODLIS"+",LIDATATT"+",LIDATDIS"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_TINI','LICODART');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'LIS_TINI','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LISTINO),'LIS_TINI','LICODLIS');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATLISIN),'LIS_TINI','LIDATATT');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATLISFI),'LIS_TINI','LIDATDIS');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'CPROWNUM',this.w_ROWNUM,'LICODLIS',this.oParentObject.w_LISTINO,'LIDATATT',this.oParentObject.w_DATLISIN,'LIDATDIS',this.oParentObject.w_DATLISFI)
          insert into (i_cTable) (LICODART,CPROWNUM,LICODLIS,LIDATATT,LIDATDIS &i_ccchkf. );
             values (;
               this.w_CODART;
               ,this.w_ROWNUM;
               ,this.oParentObject.w_LISTINO;
               ,this.oParentObject.w_DATLISIN;
               ,this.oParentObject.w_DATLISFI;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_QUANTI = 0
        this.w_PREZZO = this.w_PREZZOIN
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Scrivo i Nuovi Scaglioni
        if this.w_PREZZOFI<>0
          * --- Insert into LIS_SCAG
          i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+",LIPREZZO"+",LISCONT1"+",LISCONT2"+",LISCONT3"+",LISCONT4"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_SCAG','LICODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'LIS_SCAG','LIROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_QUANTI),'LIS_SCAG','LIQUANTI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'LIROWNUM',this.w_ROWNUM,'LIQUANTI',this.w_QUANTI,'LIPREZZO',this.w_PREZZOFI,'LISCONT1',this.w_LISCONT1,'LISCONT2',this.w_LISCONT2,'LISCONT3',this.w_LISCONT3,'LISCONT4',this.w_LISCONT4)
            insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI,LIPREZZO,LISCONT1,LISCONT2,LISCONT3,LISCONT4 &i_ccchkf. );
               values (;
                 this.w_CODART;
                 ,this.w_ROWNUM;
                 ,this.w_QUANTI;
                 ,this.w_PREZZOFI;
                 ,this.w_LISCONT1;
                 ,this.w_LISCONT2;
                 ,this.w_LISCONT3;
                 ,this.w_LISCONT4;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        SELECT INVENTAR
        ENDSCAN
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Elaborazione terminata")
    return


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo il prezzo del listino leggendo i Costi Standard presenti nei Dati Articoli / Magazzino (Globali)
    * --- Controllo se ho selezionato il check 'Solo Esistenti'
    * --- Variabile w_LORNET1 assegnata a 'N' (Netto).
    * --- L'inventario ha come valuta quella dell'esercizio in cui � stato effettuato.
    this.oParentObject.w_LORNET1 = "N"
    * --- Valua e Cambio dei Costi Standard riferiti alla Valuta di Conto
    this.oParentObject.w_VALRIF = g_PERVAL
    this.oParentObject.w_CAMBIO = g_CAOVAL
    * --- Eseguo la query che legge i Costi Standard significativi presenti nelle Anagrafiche Articoli
    if this.oParentObject.w_RICALC<>"R"
      vq_exec("query\GSMA13BV.VQR",this,"ULTCOST")
    else
      vq_exec("query\GSMA7BVL.VQR",this,"ULTCOST")
    endif
    * --- Eseguo la query che legge tutti gli Articoli con Listino e Date uguali a quelle da Aggiornare
    if this.oParentObject.w_RICALC<>"R"
      vq_exec("query\GSMA_BVL.VQR",this,"LISTINO")
    else
      vq_exec("query\GSMA_BV3.VQR",this,"LISTINO")
    endif
    * --- Controllo se il cursore creato ha almeno un record.
    if RECCOUNT("LISTINO")>0 OR (RECCOUNT("ULTCOST")>0 AND this.oParentObject.w_CREALIS<>"S")
      * --- Aggiorno i prezzi dei listini
      this.w_SCORPORO = .F.
      this.w_RICALCO = .F.
      * --- Try
      local bErr_03A84D90
      bErr_03A84D90=bTrsErr
      this.Try_03A84D90()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("Errore durante l'inserimento")
      endif
      bTrsErr=bTrsErr or bErr_03A84D90
      * --- End
    else
      ah_ErrorMsg("Per le selezioni impostate non esistono listini-prezzi da aggiornare")
    endif
  endproc
  proc Try_03A84D90()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT LISTINO
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODART, ""))
    this.w_CODART = CODART
    this.w_ROWNUM = NVL(NUMORI, 0)
    this.w_LCODLIS = Nvl(LICODLIS,SPACE(4))
    this.w_PERIVA = NVL(PERIVA, 0)
    this.w_PERCENT = NVL(PERCEN, 0)
    * --- vado ad aggiornare gli Articoli del listino prezzi di arrivo
    this.w_MESS = ALLTR(this.w_CODART)
    ah_Msg("Aggiorna listini articolo: %1",.T.,.F.,.F., this.w_MESS)
    * --- Cerca i dati sull Inventario
    SELECT ULTCOST
    GO TOP
    LOCATE FOR this.w_CODART= ULTCOST.CODART AND (this.oParentObject.w_CODMAG=ULTCOST.CODMAG OR EMPTY(this.oParentObject.w_CODMAG))
    if NOT FOUND()
      * --- Se non trovato Articolo in Dati Articoli/Magazzino, Cicla al Record Successivo...
      SELECT LISTINO
      LOOP
    else
      * --- Articolo presente sui Dati Articoli/Magazzino, Aggiorna
      this.w_PREZZO = VALUCA
      * --- Prezzo Base quello letto dal Costo Standard
      this.w_PREZZOIN = this.w_PREZZO
      this.w_PREZZOBA = -9876
      * --- Leggo gli Scaglioni del Listino di Origine
      this.w_TROV = .F.
      * --- Select from GSMA9BVL
      do vq_exec with 'GSMA9BVL',this,'_Curs_GSMA9BVL','',.f.,.t.
      if used('_Curs_GSMA9BVL')
        select _Curs_GSMA9BVL
        locate for 1=1
        do while not(eof())
        this.w_TROV = .T.
        this.w_PREZZOFI = 0
        this.w_QUANTI = _Curs_GSMA9BVL.LIQUANTI
        * --- Prezzo Base, Primo importo degli Scaglioni
        if this.w_PREZZOBA=-9876
          this.w_PREZZOBA = _Curs_GSMA9BVL.LIPREZZO
          this.w_PREZZO = this.w_PREZZOIN
        else
          * --- Gli Altri scaglioni sono calcolati in proporzione
          this.w_PREZZO = (this.w_PREZZOIN * _Curs_GSMA9BVL.LIPREZZO) / this.w_PREZZOBA
        endif
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_PREZZOFI<>0
          * --- Write into LIS_SCAG
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.LIS_SCAG_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LIPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
            +",LISCONT1 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
            +",LISCONT2 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
            +",LISCONT3 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
            +",LISCONT4 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
                +i_ccchkf ;
            +" where ";
                +"LICODART = "+cp_ToStrODBC(this.w_CODART);
                +" and LIROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                +" and LIQUANTI = "+cp_ToStrODBC(this.w_QUANTI);
                   )
          else
            update (i_cTable) set;
                LIPREZZO = this.w_PREZZOFI;
                ,LISCONT1 = this.w_LISCONT1;
                ,LISCONT2 = this.w_LISCONT2;
                ,LISCONT3 = this.w_LISCONT3;
                ,LISCONT4 = this.w_LISCONT4;
                &i_ccchkf. ;
             where;
                LICODART = this.w_CODART;
                and LIROWNUM = this.w_ROWNUM;
                and LIQUANTI = this.w_QUANTI;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
          select _Curs_GSMA9BVL
          continue
        enddo
        use
      endif
      if this.w_TROV=.F. 
        * --- Il Listino esiste ma non sono presenti scaglioni di origine ne inserisce uno come nuovo
        SELECT LISTINO
        * --- w_PREZZO gia' calcolato sopra
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Scrivo i Nuovi Scaglioni
        if this.w_PREZZOFI<>0
          this.w_QUANTI = 0
          * --- Insert into LIS_SCAG
          i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+",LIPREZZO"+",LISCONT1"+",LISCONT2"+",LISCONT3"+",LISCONT4"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_SCAG','LICODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'LIS_SCAG','LIROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_QUANTI),'LIS_SCAG','LIQUANTI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'LIROWNUM',this.w_ROWNUM,'LIQUANTI',this.w_QUANTI,'LIPREZZO',this.w_PREZZOFI,'LISCONT1',this.w_LISCONT1,'LISCONT2',this.w_LISCONT2,'LISCONT3',this.w_LISCONT3,'LISCONT4',this.w_LISCONT4)
            insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI,LIPREZZO,LISCONT1,LISCONT2,LISCONT3,LISCONT4 &i_ccchkf. );
               values (;
                 this.w_CODART;
                 ,this.w_ROWNUM;
                 ,this.w_QUANTI;
                 ,this.w_PREZZOFI;
                 ,this.w_LISCONT1;
                 ,this.w_LISCONT2;
                 ,this.w_LISCONT3;
                 ,this.w_LISCONT4;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      endif
    endif
    SELECT LISTINO
    ENDSCAN
    * --- Se Crea nuovi Listini Inserisco dai record non cancellati in INVENTAR
    if this.oParentObject.w_RICALC="R"
      vq_exec("query\GSMA_BV1.VQR",this,"LISTIN1")
    else
      vq_exec("query\GSMA_BV2.VQR",this,"LISTIN1")
    endif
    if this.oParentObject.w_CREALIS<>"S" 
      SELECT * FROM LISTIN1 WHERE LISTIN1.CODART NOT IN (SELECT LISTINO.CODART FROM LISTINO) INTO CURSOR LISTIN1
      if RECCOUNT("LISTIN1")>0
        SELECT LISTIN1
        GO TOP
        SCAN FOR NOT DELETED() AND NOT EMPTY(NVL(CODART, ""))
        * --- Inserisco il Nuovo Listino
        this.w_CODART = CODART
        this.w_ROWNUM = NVL(NUMORI, 1)
        this.w_PERIVA = NVL(PERIVA, 0)
        this.w_PERCENT = NVL(PERCEN, 0)
        SELECT ULTCOST
        GO TOP
        LOCATE FOR this.w_CODART= ULTCOST.CODART AND (this.oParentObject.w_CODMAG=ULTCOST.CODMAG OR EMPTY(this.oParentObject.w_CODMAG))
        if FOUND()
          this.w_PREZZO = NVL(VALUCA, 0)
          * --- Cerco l' Ultima CPROWNUM di ogni listino all' interno di ogni articolo
          vq_exec("query\GSMA0BVL.VQR",this,"CPROW")
          if RECCOUNT()<>0
            SELECT CPROWNUM FROM CPROW INTO ARRAY L_ROWNUM
            * --- Assegno la Prima CPROWNUM Disponibile
            this.w_ROWNUM = IIF( _TALLY<>0, L_ROWNUM+1, 1)
          endif
          SELECT CPROW
          USE
          SELECT LISTIN1
          this.w_PREZZOFI = 0
          this.w_MESS = ALLTR(this.w_CODART)
          ah_Msg("Inserisce listini articolo: %1",.T., .F.,.F., this.w_MESS)
          * --- Insert into LIS_TINI
          i_nConn=i_TableProp[this.LIS_TINI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_TINI_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"LICODART"+",CPROWNUM"+",LICODLIS"+",LIDATATT"+",LIDATDIS"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_TINI','LICODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'LIS_TINI','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LISTINO),'LIS_TINI','LICODLIS');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATLISIN),'LIS_TINI','LIDATATT');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATLISFI),'LIS_TINI','LIDATDIS');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'CPROWNUM',this.w_ROWNUM,'LICODLIS',this.oParentObject.w_LISTINO,'LIDATATT',this.oParentObject.w_DATLISIN,'LIDATDIS',this.oParentObject.w_DATLISFI)
            insert into (i_cTable) (LICODART,CPROWNUM,LICODLIS,LIDATATT,LIDATDIS &i_ccchkf. );
               values (;
                 this.w_CODART;
                 ,this.w_ROWNUM;
                 ,this.oParentObject.w_LISTINO;
                 ,this.oParentObject.w_DATLISIN;
                 ,this.oParentObject.w_DATLISFI;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_QUANTI = 0
          this.w_PERCENT = NVL(PERCEN, 0)
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Scrivo i Nuovi Scaglioni
          if this.w_PREZZOFI<>0
            * --- Insert into LIS_SCAG
            i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+",LIPREZZO"+",LISCONT1"+",LISCONT2"+",LISCONT3"+",LISCONT4"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_SCAG','LICODART');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'LIS_SCAG','LIROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_QUANTI),'LIS_SCAG','LIQUANTI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'LIROWNUM',this.w_ROWNUM,'LIQUANTI',this.w_QUANTI,'LIPREZZO',this.w_PREZZOFI,'LISCONT1',this.w_LISCONT1,'LISCONT2',this.w_LISCONT2,'LISCONT3',this.w_LISCONT3,'LISCONT4',this.w_LISCONT4)
              insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI,LIPREZZO,LISCONT1,LISCONT2,LISCONT3,LISCONT4 &i_ccchkf. );
                 values (;
                   this.w_CODART;
                   ,this.w_ROWNUM;
                   ,this.w_QUANTI;
                   ,this.w_PREZZOFI;
                   ,this.w_LISCONT1;
                   ,this.w_LISCONT2;
                   ,this.w_LISCONT3;
                   ,this.w_LISCONT4;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
        SELECT LISTIN1
        ENDSCAN
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Elaborazione terminata")
    return


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo il prezzo del listino leggendo i Costi Standard presenti nei Dati Articoli / Magazzino (Globali)
    * --- Controllo se ho selezionato il check 'Solo Esistenti'
    * --- Variabile w_LORNET1 assegnata a 'N' (Netto).
    * --- L'inventario ha come valuta quella dell'esercizio in cui � stato effettuato.
    this.oParentObject.w_LORNET1 = "N"
    * --- Valua e Cambio dei Costi Standard riferiti alla Valuta di Conto
    this.oParentObject.w_VALRIF = g_PERVAL
    this.oParentObject.w_CAMBIO = g_CAOVAL
    * --- Eseguo la query che legge i Costi Standard significativi presenti nelle Anagrafiche Articoli
    if this.oParentObject.w_RICALC<>"R"
      vq_exec("query\GSMA10BV.VQR",this,"ULTPREV")
    else
      vq_exec("query\GSMA8BVL.VQR",this,"ULTPREV")
    endif
    * --- Eseguo la query che legge tutti gli Articoli con Listino e Date uguali a quelle da Aggiornare
    if this.oParentObject.w_RICALC<>"R"
      vq_exec("query\GSMA_BVL.VQR",this,"LISTINO")
    else
      vq_exec("query\GSMA_BV3.VQR",this,"LISTINO")
    endif
    * --- Controllo se il cursore creato ha almeno un record.
    if RECCOUNT("LISTINO")>0 OR (RECCOUNT("ULTPREV")>0 AND this.oParentObject.w_CREALIS<>"S")
      * --- Aggiorno i prezzi dei listini
      this.w_SCORPORO = .F.
      this.w_RICALCO = .F.
      * --- Try
      local bErr_03D593C0
      bErr_03D593C0=bTrsErr
      this.Try_03D593C0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
        ah_ErrorMsg("Errore durante l'inserimento")
      endif
      bTrsErr=bTrsErr or bErr_03D593C0
      * --- End
    else
      ah_ErrorMsg("Per le selezioni impostate non esistono listini-prezzi da aggiornare")
    endif
  endproc
  proc Try_03D593C0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    SELECT LISTINO
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODART, ""))
    this.w_CODART = CODART
    this.w_ROWNUM = NVL(NUMORI, 0)
    this.w_PERIVA = NVL(PERIVA, 0)
    this.w_PERCENT = NVL(PERCEN, 0)
    this.w_LCODLIS = Nvl(LICODLIS,SPACE(4))
    * --- vado ad aggiornare gli Articoli del listino prezzi di arrivo
    this.w_MESS = ALLTR(this.w_CODART)
    ah_Msg("Aggiorna listini articolo: %1",.T.,.F.,.F., this.w_MESS)
    * --- Cerca i dati sull Inventario
    SELECT ULTPREV
    GO TOP
    LOCATE FOR this.w_CODART= ULTPREV.CODART AND (this.oParentObject.w_CODMAG=ULTPREV.CODMAG OR EMPTY(this.oParentObject.w_CODMAG))
    if NOT FOUND()
      * --- Se non trovato Articolo in Dati Articoli/Magazzino, Cicla al Record Successivo...
      SELECT LISTINO
      LOOP
    else
      * --- Articolo presente sui Dati Articoli/Magazzino, Aggiorna
      this.w_PREZZO = NVL(VALUPV, 0)
      * --- Prezzo Base quello letto dal Costo Standard
      this.w_PREZZOIN = this.w_PREZZO
      this.w_PREZZOBA = -9876
      * --- Leggo gli Scaglioni del Listino di Origine
      this.w_TROV = .F.
      * --- Select from GSMA9BVL
      do vq_exec with 'GSMA9BVL',this,'_Curs_GSMA9BVL','',.f.,.t.
      if used('_Curs_GSMA9BVL')
        select _Curs_GSMA9BVL
        locate for 1=1
        do while not(eof())
        this.w_TROV = .T.
        this.w_PREZZOFI = 0
        this.w_QUANTI = _Curs_GSMA9BVL.LIQUANTI
        * --- Prezzo Base, Primo importo degli Scaglioni
        if this.w_PREZZOBA=-9876
          this.w_PREZZOBA = _Curs_GSMA9BVL.LIPREZZO
          this.w_PREZZO = this.w_PREZZOIN
        else
          * --- Gli Altri scaglioni sono calcolati in proporzione
          this.w_PREZZO = (this.w_PREZZOIN * _Curs_GSMA9BVL.LIPREZZO) / this.w_PREZZOBA
        endif
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.w_PREZZOFI<>0
          * --- Write into LIS_SCAG
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.LIS_SCAG_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"LIPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
            +",LISCONT1 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
            +",LISCONT2 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
            +",LISCONT3 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
            +",LISCONT4 ="+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
                +i_ccchkf ;
            +" where ";
                +"LICODART = "+cp_ToStrODBC(this.w_CODART);
                +" and LIROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                +" and LIQUANTI = "+cp_ToStrODBC(this.w_QUANTI);
                   )
          else
            update (i_cTable) set;
                LIPREZZO = this.w_PREZZOFI;
                ,LISCONT1 = this.w_LISCONT1;
                ,LISCONT2 = this.w_LISCONT2;
                ,LISCONT3 = this.w_LISCONT3;
                ,LISCONT4 = this.w_LISCONT4;
                &i_ccchkf. ;
             where;
                LICODART = this.w_CODART;
                and LIROWNUM = this.w_ROWNUM;
                and LIQUANTI = this.w_QUANTI;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
          select _Curs_GSMA9BVL
          continue
        enddo
        use
      endif
      if this.w_TROV=.F. 
        * --- Il Listino esiste ma non sono presenti scaglioni di origine ne inserisce uno come nuovo
        SELECT LISTINO
        * --- w_PREZZO gia' calcolato sopra
        this.Pag4()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Scrivo i Nuovi Scaglioni
        if this.w_PREZZOFI<>0
          this.w_QUANTI = 0
          * --- Insert into LIS_SCAG
          i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+",LIPREZZO"+",LISCONT1"+",LISCONT2"+",LISCONT3"+",LISCONT4"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_SCAG','LICODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'LIS_SCAG','LIROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_QUANTI),'LIS_SCAG','LIQUANTI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
            +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'LIROWNUM',this.w_ROWNUM,'LIQUANTI',this.w_QUANTI,'LIPREZZO',this.w_PREZZOFI,'LISCONT1',this.w_LISCONT1,'LISCONT2',this.w_LISCONT2,'LISCONT3',this.w_LISCONT3,'LISCONT4',this.w_LISCONT4)
            insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI,LIPREZZO,LISCONT1,LISCONT2,LISCONT3,LISCONT4 &i_ccchkf. );
               values (;
                 this.w_CODART;
                 ,this.w_ROWNUM;
                 ,this.w_QUANTI;
                 ,this.w_PREZZOFI;
                 ,this.w_LISCONT1;
                 ,this.w_LISCONT2;
                 ,this.w_LISCONT3;
                 ,this.w_LISCONT4;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      endif
    endif
    SELECT LISTINO
    ENDSCAN
    * --- Se Crea nuovi Listini Inserisco dai record non cancellati in INVENTAR
    if this.oParentObject.w_RICALC="R"
      vq_exec("query\GSMA_BV1.VQR",this,"LISTIN1")
    else
      vq_exec("query\GSMA_BV2.VQR",this,"LISTIN1")
    endif
    if this.oParentObject.w_CREALIS<>"S" 
      SELECT * FROM LISTIN1 WHERE LISTIN1.CODART NOT IN (SELECT LISTINO.CODART FROM LISTINO) INTO CURSOR LISTIN1
      if RECCOUNT("LISTIN1")>0
        SELECT LISTIN1
        GO TOP
        SCAN FOR NOT DELETED() AND NOT EMPTY(NVL(CODART, ""))
        * --- Inserisco il Nuovo Listino
        this.w_CODART = CODART
        this.w_ROWNUM = NVL(NUMORI, 1)
        this.w_PERIVA = NVL(PERIVA, 0)
        this.w_PERCENT = NVL(PERCEN, 0)
        SELECT ULTPREV
        GO TOP
        LOCATE FOR this.w_CODART= ULTPREV.CODART AND (this.oParentObject.w_CODMAG=ULTPREV.CODMAG OR EMPTY(this.oParentObject.w_CODMAG))
        if FOUND()
          this.w_PREZZO = NVL(VALUPV, 0)
          * --- Cerco l' Ultima CPROWNUM di ogni listino all' interno di ogni articolo
          vq_exec("query\GSMA0BVL.VQR",this,"CPROW")
          if RECCOUNT()<>0
            SELECT CPROWNUM FROM CPROW INTO ARRAY L_ROWNUM
            * --- Assegno la Prima CPROWNUM Disponibile
            this.w_ROWNUM = IIF( _TALLY<>0, L_ROWNUM+1, 1)
          endif
          SELECT CPROW
          USE
          SELECT LISTIN1
          this.w_PREZZOFI = 0
          this.w_MESS = ALLTR(this.w_CODART)
          ah_Msg("Inserisce listini articolo: %1",.T.,.F.,.F.,.F.,this.w_MESS)
          * --- Insert into LIS_TINI
          i_nConn=i_TableProp[this.LIS_TINI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LIS_TINI_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_TINI_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"LICODART"+",CPROWNUM"+",LICODLIS"+",LIDATATT"+",LIDATDIS"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_TINI','LICODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'LIS_TINI','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_LISTINO),'LIS_TINI','LICODLIS');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATLISIN),'LIS_TINI','LIDATATT');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATLISFI),'LIS_TINI','LIDATDIS');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'CPROWNUM',this.w_ROWNUM,'LICODLIS',this.oParentObject.w_LISTINO,'LIDATATT',this.oParentObject.w_DATLISIN,'LIDATDIS',this.oParentObject.w_DATLISFI)
            insert into (i_cTable) (LICODART,CPROWNUM,LICODLIS,LIDATATT,LIDATDIS &i_ccchkf. );
               values (;
                 this.w_CODART;
                 ,this.w_ROWNUM;
                 ,this.oParentObject.w_LISTINO;
                 ,this.oParentObject.w_DATLISIN;
                 ,this.oParentObject.w_DATLISFI;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_QUANTI = 0
          this.w_PERCENT = NVL(PERCEN, 0)
          this.Pag4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Scrivo i Nuovi Scaglioni
          if this.w_PREZZOFI<>0
            * --- Insert into LIS_SCAG
            i_nConn=i_TableProp[this.LIS_SCAG_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LIS_SCAG_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LIS_SCAG_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"LICODART"+",LIROWNUM"+",LIQUANTI"+",LIPREZZO"+",LISCONT1"+",LISCONT2"+",LISCONT3"+",LISCONT4"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_CODART),'LIS_SCAG','LICODART');
              +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'LIS_SCAG','LIROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_QUANTI),'LIS_SCAG','LIQUANTI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PREZZOFI),'LIS_SCAG','LIPREZZO');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT1),'LIS_SCAG','LISCONT1');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT2),'LIS_SCAG','LISCONT2');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT3),'LIS_SCAG','LISCONT3');
              +","+cp_NullLink(cp_ToStrODBC(this.w_LISCONT4),'LIS_SCAG','LISCONT4');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'LICODART',this.w_CODART,'LIROWNUM',this.w_ROWNUM,'LIQUANTI',this.w_QUANTI,'LIPREZZO',this.w_PREZZOFI,'LISCONT1',this.w_LISCONT1,'LISCONT2',this.w_LISCONT2,'LISCONT3',this.w_LISCONT3,'LISCONT4',this.w_LISCONT4)
              insert into (i_cTable) (LICODART,LIROWNUM,LIQUANTI,LIPREZZO,LISCONT1,LISCONT2,LISCONT3,LISCONT4 &i_ccchkf. );
                 values (;
                   this.w_CODART;
                   ,this.w_ROWNUM;
                   ,this.w_QUANTI;
                   ,this.w_PREZZOFI;
                   ,this.w_LISCONT1;
                   ,this.w_LISCONT2;
                   ,this.w_LISCONT3;
                   ,this.w_LISCONT4;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        endif
        SELECT LISTIN1
        ENDSCAN
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Elaborazione terminata")
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='LIS_SCAG'
    this.cWorkTables[2]='LIS_TINI'
    this.cWorkTables[3]='RIC_PREZ'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_GSMA9BVL')
      use in _Curs_GSMA9BVL
    endif
    if used('_Curs_GSMA9BVL')
      use in _Curs_GSMA9BVL
    endif
    if used('_Curs_GSMA9BVL')
      use in _Curs_GSMA9BVL
    endif
    if used('_Curs_GSMA9BVL')
      use in _Curs_GSMA9BVL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
