* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bmf                                                        *
*              Routine per l'aggiornamento dei campi relativi ai filtri per la funzionalitą 'Ricostruzione saldi di magazzino'*
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-12-18                                                      *
* Last revis.: 2009-04-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bmf",oParentObject)
return(i_retval)

define class tgsma_bmf as StdBatch
  * --- Local variables
  * --- WorkFile variables
  FILDTREG_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine per l'aggiornamento dei campi relativi ai filtri per la funzionalitą 'Ricostruzione saldi di magazzino'
    if this.oParentObject.w_GGINTERV#0 or this.oParentObject.w_NUMDOC#0
      * --- Aggiorno i valori di filtro
      * --- Write into FILDTREG
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.FILDTREG_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.FILDTREG_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.FILDTREG_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FLNOFLSM ="+cp_NullLink(cp_ToStrODBC("S"),'FILDTREG','FLNOFLSM');
        +",FLGIOINT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_GGINTERV),'FILDTREG','FLGIOINT');
        +",FLNUMDOC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NUMDOC),'FILDTREG','FLNUMDOC');
            +i_ccchkf ;
        +" where ";
            +"FLCODAZI = "+cp_ToStrODBC(i_CODAZI);
               )
      else
        update (i_cTable) set;
            FLNOFLSM = "S";
            ,FLGIOINT = this.oParentObject.w_GGINTERV;
            ,FLNUMDOC = this.oParentObject.w_NUMDOC;
            &i_ccchkf. ;
         where;
            FLCODAZI = i_CODAZI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Aggiorno le variabili pubbliche
      g_DTNUMGG = this.oParentObject.w_GGINTERV
      g_DTNUMDOC = this.oParentObject.w_NUMDOC
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='FILDTREG'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
