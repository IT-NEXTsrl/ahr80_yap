* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bim                                                        *
*              Importa matricole                                               *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_9]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-04-11                                                      *
* Last revis.: 2012-02-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_GEST,w_SERRIF,w_ROWRIF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bim",oParentObject,m.w_GEST,m.w_SERRIF,m.w_ROWRIF)
return(i_retval)

define class tgsar_bim as StdBatch
  * --- Local variables
  w_GEST = .NULL.
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_GSVE_MMT = .NULL.
  w_PADRE = .NULL.
  w_TMPCARI = space(1)
  w_FUNCTION = space(20)
  w_MTCARI = space(1)
  w_MVSERRIF = space(10)
  w_MVROWRIF = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Importazione Matricole da GSVE_BI3 e GSPS_BI3 (Documenti e POS)
    this.w_PADRE = this.w_GEST
    this.w_MTCARI = this.w_PADRE.w_MTCARI
    this.w_MVSERRIF = this.w_SERRIF
    this.w_MVROWRIF = this.w_ROWRIF
    * --- Imposto MTCARI (inserimento rapido matricole) = ' ' in modo che se fosse invece attivato nella 
    *     causale di magazzino, in fase di importazione documenti non venga aperta la maschera di inserimento.
    *     Mi segno anche il vecchio valore per poi reimpostarlo.
    this.w_TMPCARI = this.w_MTCARI
    this.w_PADRE.w_MTCARI = " "
    this.w_PADRE.GSVE_MMT.LinkPCClick(.T.)     
    this.w_PADRE.GSVE_MMT.Hide()     
    this.w_GSVE_MMT = this.w_PADRE.GSVE_MMT.Cnt
    * --- Select from GSVE_BI3
    do vq_exec with 'GSVE_BI3',this,'_Curs_GSVE_BI3','',.f.,.t.
    if used('_Curs_GSVE_BI3')
      select _Curs_GSVE_BI3
      locate for 1=1
      do while not(eof())
      this.w_GSVE_MMT.AddRow()     
      this.w_GSVE_MMT.w_MTMAGSCA = _Curs_GSVE_BI3.MTMAGSCA
      this.w_GSVE_MMT.w_MTMAGCAR = _Curs_GSVE_BI3.MTMAGCAR
      this.w_GSVE_MMT.InitSon()     
      this.w_GSVE_MMT.w_MTSERRIF = _Curs_GSVE_BI3.MTSERIAL 
      this.w_GSVE_MMT.w_MTROWRIF = _Curs_GSVE_BI3.MTROWNUM
      this.w_GSVE_MMT.w_MTRIFNUM = _Curs_GSVE_BI3.MTNUMRIF
      this.w_GSVE_MMT.w_MTRIFSTO = _Curs_GSVE_BI3.MTNUMRIF
      this.w_GSVE_MMT.w_MT__FLAG = "+"
      this.w_GSVE_MMT.w_MT_SALDO = 0
      this.w_GSVE_MMT.w_MTCODMAT = _Curs_GSVE_BI3.MTCODMAT
      this.w_GSVE_MMT.w_MTCODLOT = Nvl( _Curs_GSVE_BI3.MTCODLOT , Space(20) )
      this.w_GSVE_MMT.w_MTCODUBI = Nvl( _Curs_GSVE_BI3.MTCODUBI , Space(20) )
      this.w_GSVE_MMT.w_RIGA = IIF( Empty( this.w_GSVE_MMT.w_MTCODMAT ) , 0 ,1 )
      this.w_GSVE_MMT.w_TOTALE = this.w_GSVE_MMT.w_TOTALE + this.w_GSVE_MMT.w_RIGA
      this.w_GSVE_MMT.TrsFromWork()     
        select _Curs_GSVE_BI3
        continue
      enddo
      use
    endif
    this.w_PADRE.set("w_TOTMAT", this.w_GSVE_MMT.w_TOTALE ,.T.)     
    this.w_GSVE_MMT.bUpdated = .t.
    this.w_FUNCTION = this.w_PADRE.GSVE_MMT.cFunction
    * --- Per evitare l'esecuzione della CheckForm
    this.w_PADRE.GSVE_MMT.cFunction = "Query"
    this.w_PADRE.GSVE_MMT.EcpSave()     
    this.w_PADRE.GSVE_MMT.cFunction = this.w_FUNCTION
    * --- Reimposto MTCARI come era in origine
    this.w_PADRE.w_MTCARI = this.w_TMPCARI
    this.w_PADRE.w_SHOWMMT = "S"
  endproc


  proc Init(oParentObject,w_GEST,w_SERRIF,w_ROWRIF)
    this.w_GEST=w_GEST
    this.w_SERRIF=w_SERRIF
    this.w_ROWRIF=w_ROWRIF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_GSVE_BI3')
      use in _Curs_GSVE_BI3
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_GEST,w_SERRIF,w_ROWRIF"
endproc
