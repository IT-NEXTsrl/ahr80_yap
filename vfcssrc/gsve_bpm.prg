* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bpm                                                        *
*              Verifica provvigioni da maturare                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_47]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-12-07                                                      *
* Last revis.: 2013-09-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bpm",oParentObject)
return(i_retval)

define class tgsve_bpm as StdBatch
  * --- Local variables
  w_NUMREG = 0
  w_DATREG = ctod("  /  /  ")
  w_CODCON = space(15)
  w_DESCON = space(40)
  w_CODAGE = space(5)
  w_DESAGE = space(35)
  w_DATSCA = ctod("  /  /  ")
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_DATDOC = ctod("  /  /  ")
  w_RIFDOC = space(2)
  w_RIFCON = space(10)
  w_MESS = space(10)
  w_MES_REG = space(10)
  w_MES_CLI = space(10)
  w_MES_AGE = space(10)
  w_oERRORLOG = .NULL.
  w_INIT = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo Provvigioni da Maturare (da GSVE_SMP)
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    * --- Carica Le Provvigioni da Maturare (alla Data Incasso)
    ah_Msg("Ricerca provvigioni da maturare...")
    vq_exec("QUERY\GSVE_BPM.VQR",this,"MATUGENE")
    if USED("MATUGENE")
      SELECT MATUGENE
      if RECCOUNT()>0
        ah_Msg("Ricerca documenti contabilizzati...")
        vq_exec("QUERY\GSVE1BPM.VQR",this,"PARTITE")
        * --- Cerca le Scadenze non ancora Saldate (da non considerare)
        ah_Msg("Ricerca documenti incassati...")
        vq_exec("QUERY\GSVE2BPM.VQR",this,"NOSALD")
        SELECT ;
        DTOS(CP_TODATE(PTDATSCA))+LEFT(ALLTRIM(PTNUMPAR)+SPACE(31),31)+PTTIPCON+ ;
        LEFT(ALLTRIM(PTCODCON)+SPACE(15),15)+PTCODVAL AS CHIAVE, SALDO AS SALDO ;
        FROM NOSALD INTO CURSOR NOSALD1 ORDER BY CHIAVE
        SELECT NOSALD
        USE
        * --- Inserisce il Flag nelle Saldate (quelle non comprese nelle Scadenze non Saldate)
        SELECT PARTITE
        GO TOP
        UPDATE PARTITE SET PTFLSALD="S" ;
        WHERE DTOS(CP_TODATE(PTDATSCA))+LEFT(ALLTRIM(PTNUMPAR)+SPACE(31),31)+PTTIPCON+ ;
        LEFT(ALLTRIM(PTCODCON)+SPACE(15),15)+PTCODVAL ;
         NOT IN (SELECT CHIAVE FROM NOSALD1)
        SELECT NOSALD1
        USE
        SELECT PARTITE
        GO TOP
        SELECT MATUGENE
        GO TOP
        SCAN FOR NOT EMPTY(NVL(MPSERIAL,"")) AND NVL(CPROWNUM,0)>0
        this.w_NUMREG = NVL(MPNUMREG, 0)
        this.w_DATREG = CP_TODATE(MPDATREG)
        this.w_CODCON = NVL(MPCODCON,"")
        this.w_DESCON = NVL(MPDESCON,"")
        this.w_CODAGE = NVL(MPCODAGE,"")
        this.w_DESAGE = NVL(MPDESAGE,"")
        this.w_DATSCA = CP_TODATE(MPDATSCA)
        this.w_NUMDOC = NVL(MPNUMDOC, 0)
        this.w_ALFDOC = NVL(MPALFDOC, Space(10))
        this.w_DATDOC = CP_TODATE(MPDATDOC)
        this.w_RIFCON = NVL(MVRIFCON, SPACE(10))
        this.w_MES_REG = "Verificare reg.n.:%1 del %2"
        this.w_MES_CLI = "Cliente:"+CHR(9)+"%1   -   %2"
        this.w_MES_AGE = "Agente:"+CHR(9)+"%1   -   %2"
        this.w_MESS = " "
        ah_Msg("Verifica provvigione del: %1",.T.,.F.,.F., DTOC(this.w_DATSCA) )
        do case
          case EMPTY(this.w_DATSCA)
            this.w_MESS = ah_Msgformat("Data scadenza inesistente")
          case EMPTY(this.w_RIFCON)
            this.w_MESS = ah_Msgformat("Documento n.%1 del %2 non contabilizzato", ALLTRIM(STR(this.w_NUMDOC,15))+IIF(EMPTY(this.w_ALFDOC),"", "/"+Alltrim(this.w_ALFDOC)), DTOC(this.w_DATDOC) )
          otherwise
            SELECT PARTITE
            GO TOP
            LOCATE FOR NVL(PTSERIAL," ")=this.w_RIFCON AND CP_TODATE(PTDATSCA)=this.w_DATSCA
            if FOUND()
              if NVL(PTFLSALD, " ")<>"S"
                this.w_MESS = ah_Msgformat("Scadenza: %1 del %2 del %3 non ancora incassata", DTOC(this.w_DATSCA), ALLTRIM(STR(this.w_NUMDOC,15))+IIF(EMPTY(this.w_ALFDOC),"", "/"+Alltrim(this.w_ALFDOC)), DTOC(this.w_DATDOC) )
              endif
            else
              this.w_oERRORLOG.AddMsgLog(left(this.w_MES_REG,120) , ALLTRIM(STR(this.w_NUMREG)), DTOC(CP_TODATE(this.w_DATREG)))     
              this.w_oERRORLOG.AddMsgLog(left(this.w_MES_CLI,120), ALLTRIM(this.w_CODCON), left(this.w_DESCON,30))     
              this.w_oERRORLOG.AddMsgLog(left(this.w_MES_AGE,120), ALLTRIM(this.w_CODAGE), left(this.w_DESAGE,30))     
              this.w_MESS = ah_Msgformat("Scadenza: %1 del %2 del %3 non associata ad alcuna partita", DTOC(this.w_DATSCA), ALLTRIM(STR(this.w_NUMDOC,15))+IIF(EMPTY(this.w_ALFDOC),"", "/"+Alltrim(this.w_ALFDOC)), DTOC(this.w_DATDOC) )
              this.w_oERRORLOG.AddMsgLogNoTranslate(left(SPACE(20)+this.w_MESS,120))     
              * --- Propongo le Partite del Documento Contabilizzato
              SELECT PARTITE
              GO TOP
              this.w_INIT = .T.
              SCAN FOR NVL(PTSERIAL," ")=this.w_RIFCON
              if this.w_INIT=.T.
                this.w_oERRORLOG.AddMsgLogPartNoTrans(space(20), "Elenco scadenze del documento associabili:")     
                this.w_INIT = .F.
              endif
              this.w_MESS = SPACE(20) + DTOC(CP_TODATE(PTDATSCA))
              this.w_oERRORLOG.AddMsgLogNoTranslate(left(SPACE(20)+this.w_MESS,120))     
              ENDSCAN 
              this.w_MESS = ""
            endif
        endcase
        if NOT EMPTY(this.w_MESS)
          this.w_oERRORLOG.AddMsgLog(left(this.w_MES_REG,250) , ALLTRIM(STR(this.w_NUMREG)), DTOC(CP_TODATE(this.w_DATREG)), CHR(10)+CHR(13), ALLTRIM(this.w_CODCON), ALLTRIM(this.w_CODAGE))     
          this.w_oERRORLOG.AddMsgLog(left(this.w_MES_CLI,120), ALLTRIM(this.w_CODCON), left(this.w_DESCON,30))     
          this.w_oERRORLOG.AddMsgLog(left(this.w_MES_AGE,120), ALLTRIM(this.w_CODAGE), left(this.w_DESAGE,30))     
          this.w_oERRORLOG.AddMsgLogNoTranslate(left(SPACE(20)+this.w_MESS,120))     
        endif
        this.w_oERRORLOG.AddMsgLogNoTranslate(CHR(10)+CHR(13))     
        SELECT MATUGENE
        ENDSCAN
        * --- LOG Errori
        if this.w_oERRORLOG.IsFullLog()
          this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati", .F.)     
        else
          ah_ErrorMsg("Per l'intervallo selezionato non esistono segnalazioni da fare")
        endif
      else
        ah_ErrorMsg("Per l'intervallo selezionato non esistono provvigioni da maturare")
      endif
      * --- Al termine Azzera il Cursore
      SELECT MATUGENE
      USE
      if USED("PARTITE")
        SELECT PARTITE
        USE
      endif
    else
      this.w_APPO = "Per l'intervallo selezionato non esistono provvigioni da maturare"
      ah_ErrorMsg(this.w_APPO)
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
