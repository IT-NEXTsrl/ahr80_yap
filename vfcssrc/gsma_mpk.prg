* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_mpk                                                        *
*              Manutenzione Packing List                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_193]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-16                                                      *
* Last revis.: 2011-03-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_mpk"))

* --- Class definition
define class tgsma_mpk as StdTrsForm
  Top    = 6
  Left   = 16

  * --- Standard Properties
  Width  = 699
  Height = 428+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-03-01"
  HelpContextID=120947863
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=43

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  PAC_MAST_IDX = 0
  PAC_DETT_IDX = 0
  TIP_DOCU_IDX = 0
  CONTI_IDX = 0
  PAC_CONF_IDX = 0
  TIP_COLL_IDX = 0
  UNIMIS_IDX = 0
  DES_DIVE_IDX = 0
  PORTI_IDX = 0
  MODASPED_IDX = 0
  VETTORI_IDX = 0
  cFile = "PAC_MAST"
  cFileDetail = "PAC_DETT"
  cKeySelect = "PLSERIAL"
  cKeyWhere  = "PLSERIAL=this.w_PLSERIAL"
  cKeyDetail  = "PLSERIAL=this.w_PLSERIAL and CPROWNUM=this.w_CPROWNUM"
  cKeyWhereODBC = '"PLSERIAL="+cp_ToStrODBC(this.w_PLSERIAL)';

  cKeyDetailWhereODBC = '"PLSERIAL="+cp_ToStrODBC(this.w_PLSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"PAC_DETT.PLSERIAL="+cp_ToStrODBC(this.w_PLSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PAC_DETT.CPROWORD '
  cPrg = "gsma_mpk"
  cComment = "Manutenzione Packing List"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 5
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PLRIFDOC = space(10)
  w_PLNUMDOC = 0
  w_PLSERIAL = space(10)
  w_PLALFDOC = space(10)
  w_PLDATDOC = ctod('  /  /  ')
  o_PLDATDOC = ctod('  /  /  ')
  w_PLTIPDOC = space(5)
  w_CPROWORD = 0
  w_PLTIPCOL = space(5)
  w_PLDESCOL = space(35)
  w_PLVOLCOL = 0
  w_PLUMVOLU = space(3)
  w_PLPESLOR = 0
  w_PL__TARA = 0
  w_DESCAU = space(35)
  w_PLTIPCON = space(1)
  o_PLTIPCON = space(1)
  w_PLCODCON = space(15)
  o_PLCODCON = space(15)
  w_PLSTATUS = space(1)
  w_DESCON = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_PLTIPORN = space(1)
  w_TIPRIF = space(2)
  w_PLCODDES = space(5)
  w_PLCODORN = space(15)
  w_PLCODPOR = space(1)
  w_PLCODSPE = space(3)
  w_PLCODVET = space(5)
  w_PL__NOTE = space(0)
  w_DESPOR = space(30)
  w_DESSPE = space(35)
  w_NOMDES = space(36)
  w_INDDES = space(36)
  w_CAPDES = space(8)
  w_LOCDES = space(28)
  w_PRODES = space(2)
  w_DESORN = space(36)
  w_INDORN = space(36)
  w_CAPORN = space(8)
  w_LOCORN = space(28)
  w_DESVET = space(35)
  w_CPROWNUM = 0
  w_PROORN = space(2)
  w_DDTIPCON = space(1)
  w_DDCODCON = space(15)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_PLSERIAL = this.W_PLSERIAL

  * --- Children pointers
  GSMA_MCK = .NULL.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PAC_MAST','gsma_mpk')
    stdPageFrame::Init()
    *set procedure to GSMA_MCK additive
    with this
      .Pages(1).addobject("oPag","tgsma_mpkPag1","gsma_mpk",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Composizione")
      .Pages(1).HelpContextID = 34151984
      .Pages(2).addobject("oPag","tgsma_mpkPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati spedizione")
      .Pages(2).HelpContextID = 34027995
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSMA_MCK
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[11]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='PAC_CONF'
    this.cWorkTables[4]='TIP_COLL'
    this.cWorkTables[5]='UNIMIS'
    this.cWorkTables[6]='DES_DIVE'
    this.cWorkTables[7]='PORTI'
    this.cWorkTables[8]='MODASPED'
    this.cWorkTables[9]='VETTORI'
    this.cWorkTables[10]='PAC_MAST'
    this.cWorkTables[11]='PAC_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(11))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAC_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAC_MAST_IDX,3]
  return

  function CreateChildren()
    this.GSMA_MCK = CREATEOBJECT('stdDynamicChild',this,'GSMA_MCK',this.oPgFrm.Page1.oPag.oLinkPC_2_3)
    this.GSMA_MCK.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSMA_MCK)
      this.GSMA_MCK.DestroyChildrenChain()
      this.GSMA_MCK=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_2_3')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSMA_MCK.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSMA_MCK.IsAChildUpdated(.t.)
    endif	
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSMA_MCK.NewDocument()
    return

  procedure ChildrenChangeRow()
    local i_cOldSel,i_cRow
    i_cOldSel=select()
    select (this.cTrsName)
    i_cRow=str(recno(),7,0)
    with this
      .GSMA_MCK.ChangeRow(this.cRowID+i_cRow,1;
             ,.w_PLSERIAL,"PCSERIAL";
             ,.w_CPROWNUM,"PCNUMCOL";
             )
    endwith
    select (i_cOldSel)
    return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_PLSERIAL = NVL(PLSERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_7_joined
    link_1_7_joined=.f.
    local link_1_12_joined
    link_1_12_joined=.f.
    local link_4_4_joined
    link_4_4_joined=.f.
    local link_4_5_joined
    link_4_5_joined=.f.
    local link_4_6_joined
    link_4_6_joined=.f.
    local link_4_7_joined
    link_4_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from PAC_MAST where PLSERIAL=KeySet.PLSERIAL
    *
    i_nConn = i_TableProp[this.PAC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAC_MAST_IDX,2],this.bLoadRecFilter,this.PAC_MAST_IDX,"gsma_mpk")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAC_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAC_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"PAC_DETT.","PAC_MAST.")
      i_cTable = i_cTable+' PAC_MAST '
      link_1_7_joined=this.AddJoinedLink_1_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_12_joined=this.AddJoinedLink_1_12(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_4_joined=this.AddJoinedLink_4_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_5_joined=this.AddJoinedLink_4_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_6_joined=this.AddJoinedLink_4_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_7_joined=this.AddJoinedLink_4_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PLSERIAL',this.w_PLSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_DESCAU = space(35)
        .w_DESCON = space(40)
        .w_TIPRIF = 'CO'
        .w_DESPOR = space(30)
        .w_DESSPE = space(35)
        .w_NOMDES = space(36)
        .w_INDDES = space(36)
        .w_CAPDES = space(8)
        .w_LOCDES = space(28)
        .w_PRODES = space(2)
        .w_DESORN = space(36)
        .w_INDORN = space(36)
        .w_CAPORN = space(8)
        .w_LOCORN = space(28)
        .w_DESVET = space(35)
        .w_PROORN = space(2)
        .w_PLRIFDOC = NVL(PLRIFDOC,space(10))
        .w_PLNUMDOC = NVL(PLNUMDOC,0)
        .w_PLSERIAL = NVL(PLSERIAL,space(10))
        .op_PLSERIAL = .w_PLSERIAL
        .w_PLALFDOC = NVL(PLALFDOC,space(10))
        .w_PLDATDOC = NVL(cp_ToDate(PLDATDOC),ctod("  /  /  "))
        .w_PLTIPDOC = NVL(PLTIPDOC,space(5))
          if link_1_7_joined
            this.w_PLTIPDOC = NVL(TDTIPDOC107,NVL(this.w_PLTIPDOC,space(5)))
            this.w_DESCAU = NVL(TDDESDOC107,space(35))
          else
          .link_1_7('Load')
          endif
        .w_PLTIPCON = NVL(PLTIPCON,space(1))
        .w_PLCODCON = NVL(PLCODCON,space(15))
          if link_1_12_joined
            this.w_PLCODCON = NVL(ANCODICE112,NVL(this.w_PLCODCON,space(15)))
            this.w_DESCON = NVL(ANDESCRI112,space(40))
          else
          .link_1_12('Load')
          endif
        .w_PLSTATUS = NVL(PLSTATUS,space(1))
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .w_PLTIPORN = NVL(PLTIPORN,space(1))
        .w_PLCODDES = NVL(PLCODDES,space(5))
          .link_4_3('Load')
        .w_PLCODORN = NVL(PLCODORN,space(15))
          if link_4_4_joined
            this.w_PLCODORN = NVL(ANCODICE404,NVL(this.w_PLCODORN,space(15)))
            this.w_DESORN = NVL(ANDESCRI404,space(36))
            this.w_INDORN = NVL(ANINDIRI404,space(36))
            this.w_CAPORN = NVL(AN___CAP404,space(8))
            this.w_LOCORN = NVL(ANLOCALI404,space(28))
            this.w_PROORN = NVL(ANPROVIN404,space(2))
          else
          .link_4_4('Load')
          endif
        .w_PLCODPOR = NVL(PLCODPOR,space(1))
          if link_4_5_joined
            this.w_PLCODPOR = NVL(POCODPOR405,NVL(this.w_PLCODPOR,space(1)))
            this.w_DESPOR = NVL(PODESPOR405,space(30))
          else
          .link_4_5('Load')
          endif
        .w_PLCODSPE = NVL(PLCODSPE,space(3))
          if link_4_6_joined
            this.w_PLCODSPE = NVL(SPCODSPE406,NVL(this.w_PLCODSPE,space(3)))
            this.w_DESSPE = NVL(SPDESSPE406,space(35))
          else
          .link_4_6('Load')
          endif
        .w_PLCODVET = NVL(PLCODVET,space(5))
          if link_4_7_joined
            this.w_PLCODVET = NVL(VTCODVET407,NVL(this.w_PLCODVET,space(5)))
            this.w_DESVET = NVL(VTDESVET407,space(35))
          else
          .link_4_7('Load')
          endif
        .w_PL__NOTE = NVL(PL__NOTE,space(0))
        .oPgFrm.Page2.oPag.oObj_4_27.Calculate()
        .w_DDTIPCON = .w_PLTIPCON
        .w_DDCODCON = .w_PLCODCON
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'PAC_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from PAC_DETT where PLSERIAL=KeySet.PLSERIAL
      *                            and CPROWNUM=KeySet.CPROWNUM
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.PAC_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAC_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('PAC_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "PAC_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" PAC_DETT"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'PLSERIAL',this.w_PLSERIAL  )
        select * from (i_cTable) PAC_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_PLTIPCOL = NVL(PLTIPCOL,space(5))
          * evitabile
          *.link_2_2('Load')
          .w_PLDESCOL = NVL(PLDESCOL,space(35))
          .w_PLVOLCOL = NVL(PLVOLCOL,0)
          .w_PLUMVOLU = NVL(PLUMVOLU,space(3))
          * evitabile
          *.link_2_6('Load')
          .w_PLPESLOR = NVL(PLPESLOR,0)
          .w_PL__TARA = NVL(PL__TARA,0)
        .oPgFrm.Page1.oPag.oObj_2_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_11.Calculate(ALLTRIM(STR(.w_CPROWORD)))
        .w_OBTEST = IIF(EMPTY(.w_PLDATDOC), i_DATSYS, .w_PLDATDOC)
          .w_CPROWNUM = NVL(CPROWNUM,0)
          select (this.cTrsName)
          append blank
          replace PLSERIAL with .w_PLSERIAL
          replace CPROWNUM with .w_CPROWNUM
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page2.oPag.oObj_4_27.Calculate()
        .w_DDTIPCON = .w_PLTIPCON
        .w_DDCODCON = .w_PLCODCON
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_PLRIFDOC=space(10)
      .w_PLNUMDOC=0
      .w_PLSERIAL=space(10)
      .w_PLALFDOC=space(10)
      .w_PLDATDOC=ctod("  /  /  ")
      .w_PLTIPDOC=space(5)
      .w_CPROWORD=10
      .w_PLTIPCOL=space(5)
      .w_PLDESCOL=space(35)
      .w_PLVOLCOL=0
      .w_PLUMVOLU=space(3)
      .w_PLPESLOR=0
      .w_PL__TARA=0
      .w_DESCAU=space(35)
      .w_PLTIPCON=space(1)
      .w_PLCODCON=space(15)
      .w_PLSTATUS=space(1)
      .w_DESCON=space(40)
      .w_OBTEST=ctod("  /  /  ")
      .w_PLTIPORN=space(1)
      .w_TIPRIF=space(2)
      .w_PLCODDES=space(5)
      .w_PLCODORN=space(15)
      .w_PLCODPOR=space(1)
      .w_PLCODSPE=space(3)
      .w_PLCODVET=space(5)
      .w_PL__NOTE=space(0)
      .w_DESPOR=space(30)
      .w_DESSPE=space(35)
      .w_NOMDES=space(36)
      .w_INDDES=space(36)
      .w_CAPDES=space(8)
      .w_LOCDES=space(28)
      .w_PRODES=space(2)
      .w_DESORN=space(36)
      .w_INDORN=space(36)
      .w_CAPORN=space(8)
      .w_LOCORN=space(28)
      .w_DESVET=space(35)
      .w_CPROWNUM=0
      .w_PROORN=space(2)
      .w_DDTIPCON=space(1)
      .w_DDCODCON=space(15)
      if .cFunction<>"Filter"
        .DoRTCalc(1,6,.f.)
        if not(empty(.w_PLTIPDOC))
         .link_1_7('Full')
        endif
        .DoRTCalc(7,8,.f.)
        if not(empty(.w_PLTIPCOL))
         .link_2_2('Full')
        endif
        .DoRTCalc(9,11,.f.)
        if not(empty(.w_PLUMVOLU))
         .link_2_6('Full')
        endif
        .DoRTCalc(12,16,.f.)
        if not(empty(.w_PLCODCON))
         .link_1_12('Full')
        endif
        .w_PLSTATUS = 'N'
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_11.Calculate(ALLTRIM(STR(.w_CPROWORD)))
        .DoRTCalc(18,18,.f.)
        .w_OBTEST = IIF(EMPTY(.w_PLDATDOC), i_DATSYS, .w_PLDATDOC)
        .w_PLTIPORN = iif(.w_PLTIPCON='C','C',IIF(EMPTY(NVL(.w_PLTIPCON,' ')),SPACE(1),'F'))
        .w_TIPRIF = 'CO'
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_PLCODDES))
         .link_4_3('Full')
        endif
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_PLCODORN))
         .link_4_4('Full')
        endif
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_PLCODPOR))
         .link_4_5('Full')
        endif
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_PLCODSPE))
         .link_4_6('Full')
        endif
        .DoRTCalc(26,26,.f.)
        if not(empty(.w_PLCODVET))
         .link_4_7('Full')
        endif
        .oPgFrm.Page2.oPag.oObj_4_27.Calculate()
        .DoRTCalc(27,41,.f.)
        .w_DDTIPCON = .w_PLTIPCON
        .w_DDCODCON = .w_PLCODCON
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAC_MAST')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPLNUMDOC_1_2.enabled = i_bVal
      .Page1.oPag.oPLALFDOC_1_5.enabled = i_bVal
      .Page1.oPag.oPLDATDOC_1_6.enabled = i_bVal
      .Page1.oPag.oPLTIPDOC_1_7.enabled = i_bVal
      .Page1.oPag.oPLTIPCON_1_11.enabled = i_bVal
      .Page1.oPag.oPLCODCON_1_12.enabled = i_bVal
      .Page1.oPag.oPLSTATUS_1_14.enabled = i_bVal
      .Page2.oPag.oPLCODDES_4_3.enabled = i_bVal
      .Page2.oPag.oPLCODORN_4_4.enabled = i_bVal
      .Page2.oPag.oPLCODPOR_4_5.enabled = i_bVal
      .Page2.oPag.oPLCODSPE_4_6.enabled = i_bVal
      .Page2.oPag.oPLCODVET_4_7.enabled = i_bVal
      .Page2.oPag.oPL__NOTE_4_8.enabled = i_bVal
      .Page1.oPag.oObj_1_17.enabled = i_bVal
      .Page1.oPag.oObj_2_9.enabled = i_bVal
      .Page2.oPag.oObj_4_27.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    this.GSMA_MCK.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'PAC_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAC_MAST_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEPACK","i_codazi,w_PLSERIAL")
      .op_codazi = .w_codazi
      .op_PLSERIAL = .w_PLSERIAL
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *  this.GSMA_MCK.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAC_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLRIFDOC,"PLRIFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLNUMDOC,"PLNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLSERIAL,"PLSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLALFDOC,"PLALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLDATDOC,"PLDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLTIPDOC,"PLTIPDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLTIPCON,"PLTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLCODCON,"PLCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLSTATUS,"PLSTATUS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLTIPORN,"PLTIPORN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLCODDES,"PLCODDES",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLCODORN,"PLCODORN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLCODPOR,"PLCODPOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLCODSPE,"PLCODSPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PLCODVET,"PLCODVET",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PL__NOTE,"PL__NOTE",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PAC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAC_MAST_IDX,2])
    i_lTable = "PAC_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PAC_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        GSMA_BPK(this,"Z")
      endwith
    endif
    return

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(6);
      ,t_PLTIPCOL C(5);
      ,t_PLDESCOL C(35);
      ,t_PLVOLCOL N(10,2);
      ,t_PLUMVOLU C(3);
      ,t_PLPESLOR N(9,3);
      ,t_PL__TARA N(9,3);
      ,PLSERIAL C(10);
      ,CPROWNUM N(10);
      ,t_OBTEST D(8);
      ,t_CPROWNUM N(5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsma_mpkbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPLTIPCOL_2_2.controlsource=this.cTrsName+'.t_PLTIPCOL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPLDESCOL_2_4.controlsource=this.cTrsName+'.t_PLDESCOL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPLVOLCOL_2_5.controlsource=this.cTrsName+'.t_PLVOLCOL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPLUMVOLU_2_6.controlsource=this.cTrsName+'.t_PLUMVOLU'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPLPESLOR_2_7.controlsource=this.cTrsName+'.t_PLPESLOR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPL__TARA_2_8.controlsource=this.cTrsName+'.t_PL__TARA'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(60)
    this.AddVLine(121)
    this.AddVLine(376)
    this.AddVLine(465)
    this.AddVLine(507)
    this.AddVLine(592)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAC_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAC_MAST_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SEPACK","i_codazi,w_PLSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PAC_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAC_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'PAC_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(PLRIFDOC,PLNUMDOC,PLSERIAL,PLALFDOC,PLDATDOC"+;
                  ",PLTIPDOC,PLTIPCON,PLCODCON,PLSTATUS,PLTIPORN"+;
                  ",PLCODDES,PLCODORN,PLCODPOR,PLCODSPE,PLCODVET"+;
                  ",PL__NOTE"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_PLRIFDOC)+;
                    ","+cp_ToStrODBC(this.w_PLNUMDOC)+;
                    ","+cp_ToStrODBC(this.w_PLSERIAL)+;
                    ","+cp_ToStrODBC(this.w_PLALFDOC)+;
                    ","+cp_ToStrODBC(this.w_PLDATDOC)+;
                    ","+cp_ToStrODBCNull(this.w_PLTIPDOC)+;
                    ","+cp_ToStrODBC(this.w_PLTIPCON)+;
                    ","+cp_ToStrODBCNull(this.w_PLCODCON)+;
                    ","+cp_ToStrODBC(this.w_PLSTATUS)+;
                    ","+cp_ToStrODBC(this.w_PLTIPORN)+;
                    ","+cp_ToStrODBCNull(this.w_PLCODDES)+;
                    ","+cp_ToStrODBCNull(this.w_PLCODORN)+;
                    ","+cp_ToStrODBCNull(this.w_PLCODPOR)+;
                    ","+cp_ToStrODBCNull(this.w_PLCODSPE)+;
                    ","+cp_ToStrODBCNull(this.w_PLCODVET)+;
                    ","+cp_ToStrODBC(this.w_PL__NOTE)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAC_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'PAC_MAST')
        cp_CheckDeletedKey(i_cTable,0,'PLSERIAL',this.w_PLSERIAL)
        INSERT INTO (i_cTable);
              (PLRIFDOC,PLNUMDOC,PLSERIAL,PLALFDOC,PLDATDOC,PLTIPDOC,PLTIPCON,PLCODCON,PLSTATUS,PLTIPORN,PLCODDES,PLCODORN,PLCODPOR,PLCODSPE,PLCODVET,PL__NOTE &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_PLRIFDOC;
                  ,this.w_PLNUMDOC;
                  ,this.w_PLSERIAL;
                  ,this.w_PLALFDOC;
                  ,this.w_PLDATDOC;
                  ,this.w_PLTIPDOC;
                  ,this.w_PLTIPCON;
                  ,this.w_PLCODCON;
                  ,this.w_PLSTATUS;
                  ,this.w_PLTIPORN;
                  ,this.w_PLCODDES;
                  ,this.w_PLCODORN;
                  ,this.w_PLCODPOR;
                  ,this.w_PLCODSPE;
                  ,this.w_PLCODVET;
                  ,this.w_PL__NOTE;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAC_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAC_DETT_IDX,2])
      *
      * insert into PAC_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(PLSERIAL,CPROWORD,PLTIPCOL,PLDESCOL,PLVOLCOL"+;
                  ",PLUMVOLU,PLPESLOR,PL__TARA,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PLSERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBCNull(this.w_PLTIPCOL)+","+cp_ToStrODBC(this.w_PLDESCOL)+","+cp_ToStrODBC(this.w_PLVOLCOL)+;
             ","+cp_ToStrODBCNull(this.w_PLUMVOLU)+","+cp_ToStrODBC(this.w_PLPESLOR)+","+cp_ToStrODBC(this.w_PL__TARA)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'PLSERIAL',this.w_PLSERIAL,'CPROWNUM',this.w_CPROWNUM)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_PLSERIAL,this.w_CPROWORD,this.w_PLTIPCOL,this.w_PLDESCOL,this.w_PLVOLCOL"+;
                ",this.w_PLUMVOLU,this.w_PLPESLOR,this.w_PL__TARA,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.PAC_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAC_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update PAC_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'PAC_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " PLRIFDOC="+cp_ToStrODBC(this.w_PLRIFDOC)+;
             ",PLNUMDOC="+cp_ToStrODBC(this.w_PLNUMDOC)+;
             ",PLALFDOC="+cp_ToStrODBC(this.w_PLALFDOC)+;
             ",PLDATDOC="+cp_ToStrODBC(this.w_PLDATDOC)+;
             ",PLTIPDOC="+cp_ToStrODBCNull(this.w_PLTIPDOC)+;
             ",PLTIPCON="+cp_ToStrODBC(this.w_PLTIPCON)+;
             ",PLCODCON="+cp_ToStrODBCNull(this.w_PLCODCON)+;
             ",PLSTATUS="+cp_ToStrODBC(this.w_PLSTATUS)+;
             ",PLTIPORN="+cp_ToStrODBC(this.w_PLTIPORN)+;
             ",PLCODDES="+cp_ToStrODBCNull(this.w_PLCODDES)+;
             ",PLCODORN="+cp_ToStrODBCNull(this.w_PLCODORN)+;
             ",PLCODPOR="+cp_ToStrODBCNull(this.w_PLCODPOR)+;
             ",PLCODSPE="+cp_ToStrODBCNull(this.w_PLCODSPE)+;
             ",PLCODVET="+cp_ToStrODBCNull(this.w_PLCODVET)+;
             ",PL__NOTE="+cp_ToStrODBC(this.w_PL__NOTE)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'PAC_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'PLSERIAL',this.w_PLSERIAL  )
          UPDATE (i_cTable) SET;
              PLRIFDOC=this.w_PLRIFDOC;
             ,PLNUMDOC=this.w_PLNUMDOC;
             ,PLALFDOC=this.w_PLALFDOC;
             ,PLDATDOC=this.w_PLDATDOC;
             ,PLTIPDOC=this.w_PLTIPDOC;
             ,PLTIPCON=this.w_PLTIPCON;
             ,PLCODCON=this.w_PLCODCON;
             ,PLSTATUS=this.w_PLSTATUS;
             ,PLTIPORN=this.w_PLTIPORN;
             ,PLCODDES=this.w_PLCODDES;
             ,PLCODORN=this.w_PLCODORN;
             ,PLCODPOR=this.w_PLCODPOR;
             ,PLCODSPE=this.w_PLCODSPE;
             ,PLCODVET=this.w_PLCODVET;
             ,PL__NOTE=this.w_PL__NOTE;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
          if this.w_PLSERIAL<>PLSERIAL
            i_bUpdAll = .t.
          endif
        endif
        scan for (t_CPROWORD<>0 AND NOT EMPTY(t_PLTIPCOL)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.PAC_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.PAC_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Deleting row children
              this.GSMA_MCK.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
                     ,this.w_PLSERIAL,"PCSERIAL";
                     ,this.w_CPROWNUM,"PCNUMCOL";
                     )
              this.GSMA_MCK.mDelete()
              *
              * delete from PAC_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PAC_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",PLTIPCOL="+cp_ToStrODBCNull(this.w_PLTIPCOL)+;
                     ",PLDESCOL="+cp_ToStrODBC(this.w_PLDESCOL)+;
                     ",PLVOLCOL="+cp_ToStrODBC(this.w_PLVOLCOL)+;
                     ",PLUMVOLU="+cp_ToStrODBCNull(this.w_PLUMVOLU)+;
                     ",PLPESLOR="+cp_ToStrODBC(this.w_PLPESLOR)+;
                     ",PL__TARA="+cp_ToStrODBC(this.w_PL__TARA)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,PLTIPCOL=this.w_PLTIPCOL;
                     ,PLDESCOL=this.w_PLDESCOL;
                     ,PLVOLCOL=this.w_PLVOLCOL;
                     ,PLUMVOLU=this.w_PLUMVOLU;
                     ,PLPESLOR=this.w_PLPESLOR;
                     ,PL__TARA=this.w_PL__TARA;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
      * --- Ask children belonging to rows to save themselves
      select (this.cTrsName)
      i_TN = this.cTrsName
      scan for (t_CPROWORD<>0 AND NOT EMPTY(t_PLTIPCOL))
        * --- > Optimize children saving
        i_nRec = recno()
        this.WorkFromTrs()
        this.GSMA_MCK.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
             ,this.w_PLSERIAL,"PCSERIAL";
             ,this.w_CPROWNUM,"PCNUMCOL";
             )
        this.GSMA_MCK.mReplace()
        this.GSMA_MCK.bSaveContext=.f.
      endscan
     this.GSMA_MCK.bSaveContext=.t.
      * --- Make the last record the actual position (mcalc problem with double transitory)2
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_CPROWORD<>0 AND NOT EMPTY(t_PLTIPCOL)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_nRec = recno()
        * --- GSMA_MCK : Deleting
        this.GSMA_MCK.bSaveContext=.f.
        this.GSMA_MCK.ChangeRow(this.cRowID+str(i_nRec,7,0),0;
               ,this.w_PLSERIAL,"PCSERIAL";
               ,this.w_CPROWNUM,"PCNUMCOL";
               )
        this.GSMA_MCK.bSaveContext=.t.
        this.GSMA_MCK.mDelete()
        if bTrsErr
          i_nModRow = -1
          exit
        endif
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.PAC_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.PAC_DETT_IDX,2])
        *
        * delete PAC_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.PAC_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.PAC_MAST_IDX,2])
        *
        * delete PAC_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_CPROWORD<>0 AND NOT EMPTY(t_PLTIPCOL)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAC_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAC_MAST_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_11.Calculate(ALLTRIM(STR(.w_CPROWORD)))
        .DoRTCalc(1,18,.t.)
        if .o_PLDATDOC<>.w_PLDATDOC
          .w_OBTEST = IIF(EMPTY(.w_PLDATDOC), i_DATSYS, .w_PLDATDOC)
        endif
        if .o_PLTIPCON<>.w_PLTIPCON
          .w_PLTIPORN = iif(.w_PLTIPCON='C','C',IIF(EMPTY(NVL(.w_PLTIPCON,' ')),SPACE(1),'F'))
        endif
        .oPgFrm.Page2.oPag.oObj_4_27.Calculate()
        .DoRTCalc(21,41,.t.)
        if .o_PLTIPCON<>.w_PLTIPCON
          .w_DDTIPCON = .w_PLTIPCON
        endif
        if .o_PLCODCON<>.w_PLCODCON
          .w_DDCODCON = .w_PLCODCON
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEPACK","i_codazi,w_PLSERIAL")
          .op_PLSERIAL = .w_PLSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_OBTEST with this.w_OBTEST
      replace t_CPROWNUM with this.w_CPROWNUM
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_17.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_11.Calculate(ALLTRIM(STR(.w_CPROWORD)))
        .oPgFrm.Page2.oPag.oObj_4_27.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_2_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_11.Calculate(ALLTRIM(STR(.w_CPROWORD)))
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPLNUMDOC_1_2.enabled = this.oPgFrm.Page1.oPag.oPLNUMDOC_1_2.mCond()
    this.oPgFrm.Page1.oPag.oPLALFDOC_1_5.enabled = this.oPgFrm.Page1.oPag.oPLALFDOC_1_5.mCond()
    this.oPgFrm.Page1.oPag.oPLDATDOC_1_6.enabled = this.oPgFrm.Page1.oPag.oPLDATDOC_1_6.mCond()
    this.oPgFrm.Page1.oPag.oPLTIPDOC_1_7.enabled = this.oPgFrm.Page1.oPag.oPLTIPDOC_1_7.mCond()
    this.oPgFrm.Page1.oPag.oPLTIPCON_1_11.enabled = this.oPgFrm.Page1.oPag.oPLTIPCON_1_11.mCond()
    this.oPgFrm.Page1.oPag.oPLCODCON_1_12.enabled = this.oPgFrm.Page1.oPag.oPLCODCON_1_12.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.GSMA_MCK.enabled = this.oPgFrm.Page1.oPag.oLinkPC_2_3.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_17.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_9.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_2_11.Event(cEvent)
      .oPgFrm.Page2.oPag.oObj_4_27.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PLTIPDOC
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PLTIPDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_ATD',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_PLTIPDOC)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_PLTIPDOC))
          select TDTIPDOC,TDDESDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PLTIPDOC)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PLTIPDOC) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oPLTIPDOC_1_7'),i_cWhere,'GSVE_ATD',"Tipi documenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PLTIPDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_PLTIPDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_PLTIPDOC)
            select TDTIPDOC,TDDESDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PLTIPDOC = NVL(_Link_.TDTIPDOC,space(5))
      this.w_DESCAU = NVL(_Link_.TDDESDOC,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PLTIPDOC = space(5)
      endif
      this.w_DESCAU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PLTIPDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_DOCU_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_7.TDTIPDOC as TDTIPDOC107"+ ",link_1_7.TDDESDOC as TDDESDOC107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_7 on PAC_MAST.PLTIPDOC=link_1_7.TDTIPDOC"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_7"
          i_cKey=i_cKey+'+" and PAC_MAST.PLTIPDOC=link_1_7.TDTIPDOC(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PLTIPCOL
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_COLL_IDX,3]
    i_lTable = "TIP_COLL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2], .t., this.TIP_COLL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PLTIPCOL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MTO',True,'TIP_COLL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_PLTIPCOL)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI,TCVOLUME,TCUMVOLU,TC__TARA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_PLTIPCOL))
          select TCCODICE,TCDESCRI,TCVOLUME,TCUMVOLU,TC__TARA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PLTIPCOL)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PLTIPCOL) and !this.bDontReportError
            deferred_cp_zoom('TIP_COLL','*','TCCODICE',cp_AbsName(oSource.parent,'oPLTIPCOL_2_2'),i_cWhere,'GSAR_MTO',"Tipologie colli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI,TCVOLUME,TCUMVOLU,TC__TARA";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI,TCVOLUME,TCUMVOLU,TC__TARA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PLTIPCOL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI,TCVOLUME,TCUMVOLU,TC__TARA";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_PLTIPCOL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_PLTIPCOL)
            select TCCODICE,TCDESCRI,TCVOLUME,TCUMVOLU,TC__TARA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PLTIPCOL = NVL(_Link_.TCCODICE,space(5))
      this.w_PLDESCOL = NVL(_Link_.TCDESCRI,space(35))
      this.w_PLVOLCOL = NVL(_Link_.TCVOLUME,0)
      this.w_PLUMVOLU = NVL(_Link_.TCUMVOLU,space(3))
      this.w_PL__TARA = NVL(_Link_.TC__TARA,0)
    else
      if i_cCtrl<>'Load'
        this.w_PLTIPCOL = space(5)
      endif
      this.w_PLDESCOL = space(35)
      this.w_PLVOLCOL = 0
      this.w_PLUMVOLU = space(3)
      this.w_PL__TARA = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_COLL_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_COLL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PLTIPCOL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PLUMVOLU
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PLUMVOLU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_PLUMVOLU)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_PLUMVOLU))
          select UMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PLUMVOLU)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PLUMVOLU) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oPLUMVOLU_2_6'),i_cWhere,'GSAR_AUM',"Unit� di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PLUMVOLU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_PLUMVOLU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_PLUMVOLU)
            select UMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PLUMVOLU = NVL(_Link_.UMCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_PLUMVOLU = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PLUMVOLU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PLCODCON
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PLCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PLCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PLTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_PLTIPCON;
                     ,'ANCODICE',trim(this.w_PLCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PLCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PLCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PLTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PLCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_PLTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PLCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPLCODCON_1_12'),i_cWhere,'GSAR_BZC',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PLTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_PLTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PLCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PLCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PLTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_PLTIPCON;
                       ,'ANCODICE',this.w_PLCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PLCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PLCODCON = space(15)
      endif
      this.w_DESCON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PLCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_12(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_12.ANCODICE as ANCODICE112"+ ",link_1_12.ANDESCRI as ANDESCRI112"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_12 on PAC_MAST.PLCODCON=link_1_12.ANCODICE"+" and PAC_MAST.PLTIPCON=link_1_12.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_12"
          i_cKey=i_cKey+'+" and PAC_MAST.PLCODCON=link_1_12.ANCODICE(+)"'+'+" and PAC_MAST.PLTIPCON=link_1_12.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PLCODDES
  func Link_4_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PLCODDES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_PLCODDES)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_PLTIPCON);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_PLCODCON);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDTIPRIF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_PLTIPCON;
                     ,'DDCODICE',this.w_PLCODCON;
                     ,'DDCODDES',trim(this.w_PLCODDES))
          select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDTIPRIF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PLCODDES)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PLCODDES) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oPLCODDES_4_3'),i_cWhere,'',"Destinazioni",'GSVE_BZD.DES_DIVE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PLTIPCON<>oSource.xKey(1);
           .or. this.w_PLCODCON<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDTIPRIF";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice destinazione inesistente o di tipo diverso da consegna")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDTIPRIF";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_PLTIPCON);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_PLCODCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PLCODDES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDTIPRIF";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_PLCODDES);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_PLTIPCON);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_PLCODCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_PLTIPCON;
                       ,'DDCODICE',this.w_PLCODCON;
                       ,'DDCODDES',this.w_PLCODDES)
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDTIPRIF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PLCODDES = NVL(_Link_.DDCODDES,space(5))
      this.w_NOMDES = NVL(_Link_.DDNOMDES,space(36))
      this.w_INDDES = NVL(_Link_.DDINDIRI,space(36))
      this.w_CAPDES = NVL(_Link_.DD___CAP,space(8))
      this.w_LOCDES = NVL(_Link_.DDLOCALI,space(28))
      this.w_PRODES = NVL(_Link_.DDPROVIN,space(2))
      this.w_TIPRIF = NVL(_Link_.DDTIPRIF,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PLCODDES = space(5)
      endif
      this.w_NOMDES = space(36)
      this.w_INDDES = space(36)
      this.w_CAPDES = space(8)
      this.w_LOCDES = space(28)
      this.w_PRODES = space(2)
      this.w_TIPRIF = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=Empty( .w_PLCODDES ) Or .w_TIPRIF='CO'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice destinazione inesistente o di tipo diverso da consegna")
        endif
        this.w_PLCODDES = space(5)
        this.w_NOMDES = space(36)
        this.w_INDDES = space(36)
        this.w_CAPDES = space(8)
        this.w_LOCDES = space(28)
        this.w_PRODES = space(2)
        this.w_TIPRIF = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PLCODDES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PLCODORN
  func Link_4_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PLCODORN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PLCODORN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PLTIPORN);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_PLTIPORN;
                     ,'ANCODICE',trim(this.w_PLCODORN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PLCODORN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_PLCODORN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PLTIPORN);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_PLCODORN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_PLTIPORN);

            select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PLCODORN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPLCODORN_4_4'),i_cWhere,'GSAR_BZC',"Anagrafica clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PLTIPORN<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_PLTIPORN);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PLCODORN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PLCODORN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PLTIPORN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_PLTIPORN;
                       ,'ANCODICE',this.w_PLCODORN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANINDIRI,AN___CAP,ANLOCALI,ANPROVIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PLCODORN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESORN = NVL(_Link_.ANDESCRI,space(36))
      this.w_INDORN = NVL(_Link_.ANINDIRI,space(36))
      this.w_CAPORN = NVL(_Link_.AN___CAP,space(8))
      this.w_LOCORN = NVL(_Link_.ANLOCALI,space(28))
      this.w_PROORN = NVL(_Link_.ANPROVIN,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PLCODORN = space(15)
      endif
      this.w_DESORN = space(36)
      this.w_INDORN = space(36)
      this.w_CAPORN = space(8)
      this.w_LOCORN = space(28)
      this.w_PROORN = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PLCODORN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_4.ANCODICE as ANCODICE404"+ ",link_4_4.ANDESCRI as ANDESCRI404"+ ",link_4_4.ANINDIRI as ANINDIRI404"+ ",link_4_4.AN___CAP as AN___CAP404"+ ",link_4_4.ANLOCALI as ANLOCALI404"+ ",link_4_4.ANPROVIN as ANPROVIN404"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_4 on PAC_MAST.PLCODORN=link_4_4.ANCODICE"+" and PAC_MAST.PLTIPORN=link_4_4.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_4"
          i_cKey=i_cKey+'+" and PAC_MAST.PLCODORN=link_4_4.ANCODICE(+)"'+'+" and PAC_MAST.PLTIPORN=link_4_4.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PLCODPOR
  func Link_4_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PORTI_IDX,3]
    i_lTable = "PORTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2], .t., this.PORTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PLCODPOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APO',True,'PORTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" POCODPOR like "+cp_ToStrODBC(trim(this.w_PLCODPOR)+"%");

          i_ret=cp_SQL(i_nConn,"select POCODPOR,PODESPOR"+cp_TransInsFldName("PODESPOR")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by POCODPOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'POCODPOR',trim(this.w_PLCODPOR))
          select POCODPOR,PODESPOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by POCODPOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PLCODPOR)==trim(_Link_.POCODPOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PLCODPOR) and !this.bDontReportError
            deferred_cp_zoom('PORTI','*','POCODPOR',cp_AbsName(oSource.parent,'oPLCODPOR_4_5'),i_cWhere,'GSAR_APO',"Porti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select POCODPOR,PODESPOR"+cp_TransInsFldName("PODESPOR")+"";
                     +" from "+i_cTable+" "+i_lTable+" where POCODPOR="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'POCODPOR',oSource.xKey(1))
            select POCODPOR,PODESPOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PLCODPOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select POCODPOR,PODESPOR"+cp_TransInsFldName("PODESPOR")+"";
                   +" from "+i_cTable+" "+i_lTable+" where POCODPOR="+cp_ToStrODBC(this.w_PLCODPOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'POCODPOR',this.w_PLCODPOR)
            select POCODPOR,PODESPOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PLCODPOR = NVL(_Link_.POCODPOR,space(1))
      this.w_DESPOR = NVL(cp_TransLoadField('_Link_.PODESPOR'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_PLCODPOR = space(1)
      endif
      this.w_DESPOR = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])+'\'+cp_ToStr(_Link_.POCODPOR,1)
      cp_ShowWarn(i_cKey,this.PORTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PLCODPOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PORTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PORTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_5.POCODPOR as POCODPOR405"+ ","+cp_TransLinkFldName('link_4_5.PODESPOR')+" as PODESPOR405"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_5 on PAC_MAST.PLCODPOR=link_4_5.POCODPOR"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_5"
          i_cKey=i_cKey+'+" and PAC_MAST.PLCODPOR=link_4_5.POCODPOR(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PLCODSPE
  func Link_4_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODASPED_IDX,3]
    i_lTable = "MODASPED"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2], .t., this.MODASPED_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PLCODSPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASP',True,'MODASPED')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SPCODSPE like "+cp_ToStrODBC(trim(this.w_PLCODSPE)+"%");

          i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SPCODSPE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SPCODSPE',trim(this.w_PLCODSPE))
          select SPCODSPE,SPDESSPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SPCODSPE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PLCODSPE)==trim(_Link_.SPCODSPE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PLCODSPE) and !this.bDontReportError
            deferred_cp_zoom('MODASPED','*','SPCODSPE',cp_AbsName(oSource.parent,'oPLCODSPE_4_6'),i_cWhere,'GSAR_ASP',"Spedizioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                     +" from "+i_cTable+" "+i_lTable+" where SPCODSPE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODSPE',oSource.xKey(1))
            select SPCODSPE,SPDESSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PLCODSPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                   +" from "+i_cTable+" "+i_lTable+" where SPCODSPE="+cp_ToStrODBC(this.w_PLCODSPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODSPE',this.w_PLCODSPE)
            select SPCODSPE,SPDESSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PLCODSPE = NVL(_Link_.SPCODSPE,space(3))
      this.w_DESSPE = NVL(cp_TransLoadField('_Link_.SPDESSPE'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PLCODSPE = space(3)
      endif
      this.w_DESSPE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])+'\'+cp_ToStr(_Link_.SPCODSPE,1)
      cp_ShowWarn(i_cKey,this.MODASPED_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PLCODSPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MODASPED_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_6.SPCODSPE as SPCODSPE406"+ ","+cp_TransLinkFldName('link_4_6.SPDESSPE')+" as SPDESSPE406"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_6 on PAC_MAST.PLCODSPE=link_4_6.SPCODSPE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_6"
          i_cKey=i_cKey+'+" and PAC_MAST.PLCODSPE=link_4_6.SPCODSPE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PLCODVET
  func Link_4_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VETTORI_IDX,3]
    i_lTable = "VETTORI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2], .t., this.VETTORI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PLCODVET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVT',True,'VETTORI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VTCODVET like "+cp_ToStrODBC(trim(this.w_PLCODVET)+"%");

          i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VTCODVET","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VTCODVET',trim(this.w_PLCODVET))
          select VTCODVET,VTDESVET;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VTCODVET into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PLCODVET)==trim(_Link_.VTCODVET) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VTDESVET like "+cp_ToStrODBC(trim(this.w_PLCODVET)+"%");

            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VTDESVET like "+cp_ToStr(trim(this.w_PLCODVET)+"%");

            select VTCODVET,VTDESVET;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PLCODVET) and !this.bDontReportError
            deferred_cp_zoom('VETTORI','*','VTCODVET',cp_AbsName(oSource.parent,'oPLCODVET_4_7'),i_cWhere,'GSAR_AVT',"Vettori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                     +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',oSource.xKey(1))
            select VTCODVET,VTDESVET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PLCODVET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                   +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(this.w_PLCODVET);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',this.w_PLCODVET)
            select VTCODVET,VTDESVET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PLCODVET = NVL(_Link_.VTCODVET,space(5))
      this.w_DESVET = NVL(_Link_.VTDESVET,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PLCODVET = space(5)
      endif
      this.w_DESVET = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])+'\'+cp_ToStr(_Link_.VTCODVET,1)
      cp_ShowWarn(i_cKey,this.VETTORI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PLCODVET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VETTORI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_7.VTCODVET as VTCODVET407"+ ",link_4_7.VTDESVET as VTDESVET407"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_7 on PAC_MAST.PLCODVET=link_4_7.VTCODVET"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_7"
          i_cKey=i_cKey+'+" and PAC_MAST.PLCODVET=link_4_7.VTCODVET(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPLNUMDOC_1_2.value==this.w_PLNUMDOC)
      this.oPgFrm.Page1.oPag.oPLNUMDOC_1_2.value=this.w_PLNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPLALFDOC_1_5.value==this.w_PLALFDOC)
      this.oPgFrm.Page1.oPag.oPLALFDOC_1_5.value=this.w_PLALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPLDATDOC_1_6.value==this.w_PLDATDOC)
      this.oPgFrm.Page1.oPag.oPLDATDOC_1_6.value=this.w_PLDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oPLTIPDOC_1_7.value==this.w_PLTIPDOC)
      this.oPgFrm.Page1.oPag.oPLTIPDOC_1_7.value=this.w_PLTIPDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_10.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_10.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oPLTIPCON_1_11.RadioValue()==this.w_PLTIPCON)
      this.oPgFrm.Page1.oPag.oPLTIPCON_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPLCODCON_1_12.value==this.w_PLCODCON)
      this.oPgFrm.Page1.oPag.oPLCODCON_1_12.value=this.w_PLCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oPLSTATUS_1_14.RadioValue()==this.w_PLSTATUS)
      this.oPgFrm.Page1.oPag.oPLSTATUS_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_16.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_16.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oPLCODDES_4_3.value==this.w_PLCODDES)
      this.oPgFrm.Page2.oPag.oPLCODDES_4_3.value=this.w_PLCODDES
    endif
    if not(this.oPgFrm.Page2.oPag.oPLCODORN_4_4.value==this.w_PLCODORN)
      this.oPgFrm.Page2.oPag.oPLCODORN_4_4.value=this.w_PLCODORN
    endif
    if not(this.oPgFrm.Page2.oPag.oPLCODPOR_4_5.value==this.w_PLCODPOR)
      this.oPgFrm.Page2.oPag.oPLCODPOR_4_5.value=this.w_PLCODPOR
    endif
    if not(this.oPgFrm.Page2.oPag.oPLCODSPE_4_6.value==this.w_PLCODSPE)
      this.oPgFrm.Page2.oPag.oPLCODSPE_4_6.value=this.w_PLCODSPE
    endif
    if not(this.oPgFrm.Page2.oPag.oPLCODVET_4_7.value==this.w_PLCODVET)
      this.oPgFrm.Page2.oPag.oPLCODVET_4_7.value=this.w_PLCODVET
    endif
    if not(this.oPgFrm.Page2.oPag.oPL__NOTE_4_8.value==this.w_PL__NOTE)
      this.oPgFrm.Page2.oPag.oPL__NOTE_4_8.value=this.w_PL__NOTE
    endif
    if not(this.oPgFrm.Page2.oPag.oDESPOR_4_10.value==this.w_DESPOR)
      this.oPgFrm.Page2.oPag.oDESPOR_4_10.value=this.w_DESPOR
    endif
    if not(this.oPgFrm.Page2.oPag.oDESSPE_4_11.value==this.w_DESSPE)
      this.oPgFrm.Page2.oPag.oDESSPE_4_11.value=this.w_DESSPE
    endif
    if not(this.oPgFrm.Page2.oPag.oNOMDES_4_14.value==this.w_NOMDES)
      this.oPgFrm.Page2.oPag.oNOMDES_4_14.value=this.w_NOMDES
    endif
    if not(this.oPgFrm.Page2.oPag.oINDDES_4_15.value==this.w_INDDES)
      this.oPgFrm.Page2.oPag.oINDDES_4_15.value=this.w_INDDES
    endif
    if not(this.oPgFrm.Page2.oPag.oCAPDES_4_16.value==this.w_CAPDES)
      this.oPgFrm.Page2.oPag.oCAPDES_4_16.value=this.w_CAPDES
    endif
    if not(this.oPgFrm.Page2.oPag.oLOCDES_4_17.value==this.w_LOCDES)
      this.oPgFrm.Page2.oPag.oLOCDES_4_17.value=this.w_LOCDES
    endif
    if not(this.oPgFrm.Page2.oPag.oPRODES_4_18.value==this.w_PRODES)
      this.oPgFrm.Page2.oPag.oPRODES_4_18.value=this.w_PRODES
    endif
    if not(this.oPgFrm.Page2.oPag.oDESORN_4_19.value==this.w_DESORN)
      this.oPgFrm.Page2.oPag.oDESORN_4_19.value=this.w_DESORN
    endif
    if not(this.oPgFrm.Page2.oPag.oINDORN_4_20.value==this.w_INDORN)
      this.oPgFrm.Page2.oPag.oINDORN_4_20.value=this.w_INDORN
    endif
    if not(this.oPgFrm.Page2.oPag.oCAPORN_4_21.value==this.w_CAPORN)
      this.oPgFrm.Page2.oPag.oCAPORN_4_21.value=this.w_CAPORN
    endif
    if not(this.oPgFrm.Page2.oPag.oLOCORN_4_22.value==this.w_LOCORN)
      this.oPgFrm.Page2.oPag.oLOCORN_4_22.value=this.w_LOCORN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVET_4_26.value==this.w_DESVET)
      this.oPgFrm.Page2.oPag.oDESVET_4_26.value=this.w_DESVET
    endif
    if not(this.oPgFrm.Page2.oPag.oPROORN_4_28.value==this.w_PROORN)
      this.oPgFrm.Page2.oPag.oPROORN_4_28.value=this.w_PROORN
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLTIPCOL_2_2.value==this.w_PLTIPCOL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLTIPCOL_2_2.value=this.w_PLTIPCOL
      replace t_PLTIPCOL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLTIPCOL_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLDESCOL_2_4.value==this.w_PLDESCOL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLDESCOL_2_4.value=this.w_PLDESCOL
      replace t_PLDESCOL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLDESCOL_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLVOLCOL_2_5.value==this.w_PLVOLCOL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLVOLCOL_2_5.value=this.w_PLVOLCOL
      replace t_PLVOLCOL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLVOLCOL_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLUMVOLU_2_6.value==this.w_PLUMVOLU)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLUMVOLU_2_6.value=this.w_PLUMVOLU
      replace t_PLUMVOLU with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLUMVOLU_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLPESLOR_2_7.value==this.w_PLPESLOR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLPESLOR_2_7.value=this.w_PLPESLOR
      replace t_PLPESLOR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPLPESLOR_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPL__TARA_2_8.value==this.w_PL__TARA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPL__TARA_2_8.value=this.w_PL__TARA
      replace t_PL__TARA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPL__TARA_2_8.value
    endif
    cp_SetControlsValueExtFlds(this,'PAC_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(Empty( .w_PLCODDES ) Or .w_TIPRIF='CO')  and not(empty(.w_PLCODDES))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oPLCODDES_4_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice destinazione inesistente o di tipo diverso da consegna")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsma_mpk
      if i_bRes = .t. and this.w_PLSTATUS='N'
         * --- Verifica lo Status
         this.NotifyEvent('ControlliFinali')
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
      endcase
      i_bRes = i_bRes .and. .GSMA_MCK.CheckForm()
      if .w_CPROWORD<>0 AND NOT EMPTY(.w_PLTIPCOL)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PLDATDOC = this.w_PLDATDOC
    this.o_PLTIPCON = this.w_PLTIPCON
    this.o_PLCODCON = this.w_PLCODCON
    * --- GSMA_MCK : Depends On
    this.GSMA_MCK.SaveDependsOn()
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_CPROWORD<>0 AND NOT EMPTY(t_PLTIPCOL))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(999999,cp_maxroword()+10)
      .w_PLTIPCOL=space(5)
      .w_PLDESCOL=space(35)
      .w_PLVOLCOL=0
      .w_PLUMVOLU=space(3)
      .w_PLPESLOR=0
      .w_PL__TARA=0
      .w_OBTEST=ctod("  /  /  ")
      .DoRTCalc(1,8,.f.)
      if not(empty(.w_PLTIPCOL))
        .link_2_2('Full')
      endif
      .DoRTCalc(9,11,.f.)
      if not(empty(.w_PLUMVOLU))
        .link_2_6('Full')
      endif
        .oPgFrm.Page1.oPag.oObj_2_9.Calculate()
        .oPgFrm.Page1.oPag.oObj_2_11.Calculate(ALLTRIM(STR(.w_CPROWORD)))
      .DoRTCalc(12,18,.f.)
        .w_OBTEST = IIF(EMPTY(.w_PLDATDOC), i_DATSYS, .w_PLDATDOC)
    endwith
    this.DoRTCalc(20,43,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_PLTIPCOL = t_PLTIPCOL
    this.w_PLDESCOL = t_PLDESCOL
    this.w_PLVOLCOL = t_PLVOLCOL
    this.w_PLUMVOLU = t_PLUMVOLU
    this.w_PLPESLOR = t_PLPESLOR
    this.w_PL__TARA = t_PL__TARA
    this.w_OBTEST = t_OBTEST
    this.w_CPROWNUM = t_CPROWNUM
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_PLTIPCOL with this.w_PLTIPCOL
    replace t_PLDESCOL with this.w_PLDESCOL
    replace t_PLVOLCOL with this.w_PLVOLCOL
    replace t_PLUMVOLU with this.w_PLUMVOLU
    replace t_PLPESLOR with this.w_PLPESLOR
    replace t_PL__TARA with this.w_PL__TARA
    replace t_OBTEST with this.w_OBTEST
    replace t_CPROWNUM with this.w_CPROWNUM
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsma_mpkPag1 as StdContainer
  Width  = 695
  height = 428
  stdWidth  = 695
  stdheight = 428
  resizeXpos=343
  resizeYpos=178
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPLNUMDOC_1_2 as StdField with uid="ECVQZJNGPE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PLNUMDOC", cQueryName = "PLNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento",;
    HelpContextID = 6272569,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=114, Top=14, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oPLNUMDOC_1_2.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
  endfunc

  add object oPLALFDOC_1_5 as StdField with uid="NGLREKHEEV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PLALFDOC", cQueryName = "PLALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Seriale documento",;
    HelpContextID = 266724921,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=244, Top=14, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oPLALFDOC_1_5.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
  endfunc

  add object oPLDATDOC_1_6 as StdField with uid="JQZNHXWNEN",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PLDATDOC", cQueryName = "PLDATDOC",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento",;
    HelpContextID = 12260921,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=370, Top=14

  func oPLDATDOC_1_6.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
  endfunc

  add object oPLTIPDOC_1_7 as StdField with uid="BUPICQCLMP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PLTIPDOC", cQueryName = "PLTIPDOC",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale documento di riferimento",;
    HelpContextID = 8656441,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=115, Top=42, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", cZoomOnZoom="GSVE_ATD", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_PLTIPDOC"

  func oPLTIPDOC_1_7.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
  endfunc

  func oPLTIPDOC_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPLTIPDOC_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPLTIPDOC_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oPLTIPDOC_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSVE_ATD',"Tipi documenti",'',this.parent.oContained
  endproc
  proc oPLTIPDOC_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSVE_ATD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TDTIPDOC=this.parent.oContained.w_PLTIPDOC
    i_obj.ecpSave()
  endproc

  add object oDESCAU_1_10 as StdField with uid="MTLUJDKXQE",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 259129802,;
   bGlobalFont=.t.,;
    Height=21, Width=267, Left=179, Top=42, InputMask=replicate('X',35)


  add object oPLTIPCON_1_11 as StdCombo with uid="YEQJLZREGE",rtseq=15,rtrep=.f.,left=114,top=72,width=112,height=21;
    , ToolTipText = "Cliente o fornitore";
    , HelpContextID = 260314692;
    , cFormVar="w_PLTIPCON",RowSource=""+"Cliente,"+"Fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPLTIPCON_1_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PLTIPCON,&i_cF..t_PLTIPCON),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'F',;
    space(1))))
  endfunc
  func oPLTIPCON_1_11.GetRadio()
    this.Parent.oContained.w_PLTIPCON = this.RadioValue()
    return .t.
  endfunc

  func oPLTIPCON_1_11.ToRadio()
    this.Parent.oContained.w_PLTIPCON=trim(this.Parent.oContained.w_PLTIPCON)
    return(;
      iif(this.Parent.oContained.w_PLTIPCON=='C',1,;
      iif(this.Parent.oContained.w_PLTIPCON=='F',2,;
      0)))
  endfunc

  func oPLTIPCON_1_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPLTIPCON_1_11.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
  endfunc

  func oPLTIPCON_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_PLCODCON)
        bRes2=.link_1_12('Full')
      endif
      if .not. empty(.w_PLCODDES)
        bRes2=.link_4_3('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oPLCODCON_1_12 as StdField with uid="CZAKIKOTUU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_PLCODCON", cQueryName = "PLCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Cliente/Fornitore",;
    HelpContextID = 248055364,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=231, Top=72, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_PLTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PLCODCON"

  func oPLCODCON_1_12.mCond()
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
  endfunc

  func oPLCODCON_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
      if .not. empty(.w_PLCODDES)
        bRes2=.link_4_3('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oPLCODCON_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPLCODCON_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_PLTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_PLTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPLCODCON_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori",'',this.parent.oContained
  endproc
  proc oPLCODCON_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_PLTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_PLCODCON
    i_obj.ecpSave()
  endproc


  add object oPLSTATUS_1_14 as StdCombo with uid="PWFTPDHLGQ",rtseq=17,rtrep=.f.,left=551,top=14,width=128,height=21;
    , tabstop = .f.;
    , ToolTipText = "Stato della Packing List";
    , HelpContextID = 6355383;
    , cFormVar="w_PLSTATUS",RowSource=""+"Confermata,"+"Da confermare", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPLSTATUS_1_14.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PLSTATUS,&i_cF..t_PLSTATUS),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'N',;
    space(1))))
  endfunc
  func oPLSTATUS_1_14.GetRadio()
    this.Parent.oContained.w_PLSTATUS = this.RadioValue()
    return .t.
  endfunc

  func oPLSTATUS_1_14.ToRadio()
    this.Parent.oContained.w_PLSTATUS=trim(this.Parent.oContained.w_PLSTATUS)
    return(;
      iif(this.Parent.oContained.w_PLSTATUS=='S',1,;
      iif(this.Parent.oContained.w_PLSTATUS=='N',2,;
      0)))
  endfunc

  func oPLSTATUS_1_14.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDESCON_1_16 as StdField with uid="ZUXCRCXFLX",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 174980662,;
   bGlobalFont=.t.,;
    Height=21, Width=328, Left=352, Top=72, InputMask=replicate('X',40)


  add object oObj_1_17 as cp_runprogram with uid="RYTYZVOSKP",left=218, top=448, width=195,height=19,;
    caption='GSMA_BPK(C)',;
   bGlobalFont=.t.,;
    prg="GSMA_BPK('C')",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 258900529


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=10, top=121, width=679,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="CPROWORD",Label1="Num.",Field2="PLTIPCOL",Label2="Collo",Field3="PLDESCOL",Label3="Descrizione",Field4="PLVOLCOL",Label4="Volume",Field5="PLUMVOLU",Label5="U.M.",Field6="PLPESLOR",Label6="P. lordo (kg)",Field7="PL__TARA",Label7="Tara (kg)",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 99832198

  add object oStr_1_3 as StdString with uid="BAKVOCCGAQ",Visible=.t., Left=60, Top=16,;
    Alignment=1, Width=54, Height=18,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="JUOFEYZCAT",Visible=.t., Left=330, Top=16,;
    Alignment=1, Width=40, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="SFFFNOZDPJ",Visible=.t., Left=38, Top=44,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="QDKULJEGIG",Visible=.t., Left=5, Top=72,;
    Alignment=1, Width=109, Height=18,;
    Caption="Cliente/fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="JVNGKTCNMI",Visible=.t., Left=502, Top=14,;
    Alignment=1, Width=44, Height=18,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="NZNHVIEMYB",Visible=.t., Left=8, Top=102,;
    Alignment=0, Width=76, Height=18,;
    Caption="Elenco colli"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_19 as StdString with uid="DCNNRTDINB",Visible=.t., Left=232, Top=16,;
    Alignment=2, Width=11, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsma_mck",lower(this.oContained.GSMA_MCK.class))=0
        this.oContained.GSMA_MCK.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=0,top=140,;
    width=675+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=1,top=141,width=674+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*5*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TIP_COLL|UNIMIS|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oContained.ChildrenChangeRow()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TIP_COLL'
        oDropInto=this.oBodyCol.oRow.oPLTIPCOL_2_2
      case cFile='UNIMIS'
        oDropInto=this.oBodyCol.oRow.oPLUMVOLU_2_6
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oLinkPC_2_3 as stdDynamicChildContainer with uid="HSVTGQGOWQ",bOnScreen=.t.,width=698,height=175,;
   left=-2, top=256;


  func oLinkPC_2_3.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_PLTIPCOL))
    endwith
  endfunc

  add object oObj_2_9 as cp_runprogram with uid="BTMGMIUFFF",width=218,height=19,;
   left=-4, top=448,;
    caption='GSMA_BP2',;
   bGlobalFont=.t.,;
    prg="GSMA_BP2",;
    cEvent = "w_PLTIPCOL Changed",;
    nPag=2;
    , HelpContextID = 258714776

  add object oObj_2_11 as cp_calclbl with uid="UHYPVCCROL",width=53,height=15,;
   left=135, top=241,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",fontbold=.t.,alignment=0,;
    nPag=2;
    , ToolTipText = "Numero del collo di cui appare la composizione";
    , HelpContextID = 237925402

  add object oStr_2_10 as StdString with uid="WYMXUAKAUF",Visible=.t., Left=6, Top=241,;
    Alignment=0, Width=137, Height=15,;
    Caption="Composizione collo n."  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

  define class tgsma_mpkPag2 as StdContainer
    Width  = 695
    height = 428
    stdWidth  = 695
    stdheight = 428
  resizeXpos=355
  resizeYpos=313
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPLCODDES_4_3 as StdField with uid="LBRZDSMGCL",rtseq=22,rtrep=.f.,;
    cFormVar = "w_PLCODDES", cQueryName = "PLCODDES",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice destinazione inesistente o di tipo diverso da consegna",;
    ToolTipText = "Codice destinazione",;
    HelpContextID = 3602871,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=71, Top=39, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", oKey_1_1="DDTIPCON", oKey_1_2="this.w_PLTIPCON", oKey_2_1="DDCODICE", oKey_2_2="this.w_PLCODCON", oKey_3_1="DDCODDES", oKey_3_2="this.w_PLCODDES"

  proc oPLCODDES_4_3.mAfter
    with this.Parent.oContained
      .w_PLCODDES=chkdesdiv(.w_PLCODDES, .w_PLTIPCON, .w_PLCODCON)
    endwith
  endproc

  func oPLCODDES_4_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oPLCODDES_4_3.ecpDrop(oSource)
    this.Parent.oContained.link_4_3('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPLCODDES_4_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_PLTIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_PLCODCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_PLTIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_PLCODCON)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oPLCODDES_4_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Destinazioni",'GSVE_BZD.DES_DIVE_VZM',this.parent.oContained
  endproc

  add object oPLCODORN_4_4 as StdField with uid="SWTJDCNNTN",rtseq=23,rtrep=.f.,;
    cFormVar = "w_PLCODORN", cQueryName = "PLCODORN",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Cliente per conto del quale � spedita la merce",;
    HelpContextID = 87488956,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=409, Top=39, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_PLTIPORN", oKey_2_1="ANCODICE", oKey_2_2="this.w_PLCODORN"

  func oPLCODORN_4_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPLCODORN_4_4.ecpDrop(oSource)
    this.Parent.oContained.link_4_4('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPLCODORN_4_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_PLTIPORN)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_PLTIPORN)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oPLCODORN_4_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Anagrafica clienti/fornitori",'',this.parent.oContained
  endproc
  proc oPLCODORN_4_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_PLTIPORN
     i_obj.w_ANCODICE=this.parent.oContained.w_PLCODORN
    i_obj.ecpSave()
  endproc

  add object oPLCODPOR_4_5 as StdField with uid="WLRAWNKWOA",rtseq=24,rtrep=.f.,;
    cFormVar = "w_PLCODPOR", cQueryName = "PLCODPOR",;
    bObbl = .f. , nPag = 4, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Tipo porto",;
    HelpContextID = 197723720,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=71, Top=152, InputMask=replicate('X',1), bHasZoom = .t. , cLinkFile="PORTI", cZoomOnZoom="GSAR_APO", oKey_1_1="POCODPOR", oKey_1_2="this.w_PLCODPOR"

  func oPLCODPOR_4_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPLCODPOR_4_5.ecpDrop(oSource)
    this.Parent.oContained.link_4_5('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPLCODPOR_4_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PORTI','*','POCODPOR',cp_AbsName(this.parent,'oPLCODPOR_4_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APO',"Porti",'',this.parent.oContained
  endproc
  proc oPLCODPOR_4_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_POCODPOR=this.parent.oContained.w_PLCODPOR
    i_obj.ecpSave()
  endproc

  add object oPLCODSPE_4_6 as StdField with uid="VNHOFRBQDX",rtseq=25,rtrep=.f.,;
    cFormVar = "w_PLCODSPE", cQueryName = "PLCODSPE",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice del mezzo di spedizione",;
    HelpContextID = 248055355,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=71, Top=177, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODASPED", cZoomOnZoom="GSAR_ASP", oKey_1_1="SPCODSPE", oKey_1_2="this.w_PLCODSPE"

  func oPLCODSPE_4_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPLCODSPE_4_6.ecpDrop(oSource)
    this.Parent.oContained.link_4_6('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPLCODSPE_4_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODASPED','*','SPCODSPE',cp_AbsName(this.parent,'oPLCODSPE_4_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASP',"Spedizioni",'',this.parent.oContained
  endproc
  proc oPLCODSPE_4_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SPCODSPE=this.parent.oContained.w_PLCODSPE
    i_obj.ecpSave()
  endproc

  add object oPLCODVET_4_7 as StdField with uid="OQBCNUOYDK",rtseq=26,rtrep=.f.,;
    cFormVar = "w_PLCODVET", cQueryName = "PLCODVET",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del vettore incaricato del trasporto",;
    HelpContextID = 238483894,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=71, Top=202, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VETTORI", cZoomOnZoom="GSAR_AVT", oKey_1_1="VTCODVET", oKey_1_2="this.w_PLCODVET"

  func oPLCODVET_4_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oPLCODVET_4_7.ecpDrop(oSource)
    this.Parent.oContained.link_4_7('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPLCODVET_4_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VETTORI','*','VTCODVET',cp_AbsName(this.parent,'oPLCODVET_4_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVT',"Vettori",'',this.parent.oContained
  endproc
  proc oPLCODVET_4_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VTCODVET=this.parent.oContained.w_PLCODVET
    i_obj.ecpSave()
  endproc

  add object oPL__NOTE_4_8 as StdMemo with uid="CQFKUYRKXT",rtseq=27,rtrep=.f.,;
    cFormVar = "w_PL__NOTE", cQueryName = "PL__NOTE",;
    bObbl = .f. , nPag = 4, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note legate alla Packing List",;
    HelpContextID = 75839941,;
   bGlobalFont=.t.,;
    Height=115, Width=577, Left=71, Top=252

  add object oDESPOR_4_10 as StdField with uid="QKFHYSRINV",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESPOR", cQueryName = "DESPOR",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 242941494,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=129, Top=152, InputMask=replicate('X',30)

  add object oDESSPE_4_11 as StdField with uid="OSDMWCFQJR",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESSPE", cQueryName = "DESSPE",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 26082870,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=129, Top=177, InputMask=replicate('X',35)

  add object oNOMDES_4_14 as StdField with uid="BYMTMVIUYD",rtseq=30,rtrep=.f.,;
    cFormVar = "w_NOMDES", cQueryName = "NOMDES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(36), bMultilanguage =  .f.,;
    HelpContextID = 248424662,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=232, Left=71, Top=63, InputMask=replicate('X',36)

  add object oINDDES_4_15 as StdField with uid="RYFGSDORJE",rtseq=31,rtrep=.f.,;
    cFormVar = "w_INDDES", cQueryName = "INDDES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(36), bMultilanguage =  .f.,;
    HelpContextID = 248387462,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=232, Left=71, Top=83, InputMask=replicate('X',36)

  add object oCAPDES_4_16 as StdField with uid="KAABDQISLE",rtseq=32,rtrep=.f.,;
    cFormVar = "w_CAPDES", cQueryName = "CAPDES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(8), bMultilanguage =  .f.,;
    HelpContextID = 248433190,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=60, Left=71, Top=103, InputMask=replicate('X',8)

  add object oLOCDES_4_17 as StdField with uid="XDUOMGMTZY",rtseq=33,rtrep=.f.,;
    cFormVar = "w_LOCDES", cQueryName = "LOCDES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(28), bMultilanguage =  .f.,;
    HelpContextID = 248383670,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=168, Left=136, Top=104, InputMask=replicate('X',28)

  add object oPRODES_4_18 as StdField with uid="JYPBGZVJUQ",rtseq=34,rtrep=.f.,;
    cFormVar = "w_PRODES", cQueryName = "PRODES",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 248433654,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=26, Left=307, Top=104, InputMask=replicate('X',2)

  add object oDESORN_4_19 as StdField with uid="UKQHPFTRVU",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DESORN", cQueryName = "DESORN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(36), bMultilanguage =  .f.,;
    HelpContextID = 178912822,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=233, Left=409, Top=63, InputMask=replicate('X',36)

  add object oINDORN_4_20 as StdField with uid="DLJIPWTHJP",rtseq=36,rtrep=.f.,;
    cFormVar = "w_INDORN", cQueryName = "INDORN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(36), bMultilanguage =  .f.,;
    HelpContextID = 178853766,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=233, Left=409, Top=83, InputMask=replicate('X',36)

  add object oCAPORN_4_21 as StdField with uid="LEBJVSEUAL",rtseq=37,rtrep=.f.,;
    cFormVar = "w_CAPORN", cQueryName = "CAPORN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(8), bMultilanguage =  .f.,;
    HelpContextID = 178899494,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=60, Left=409, Top=103, InputMask=replicate('X',8)

  add object oLOCORN_4_22 as StdField with uid="TKPNHAPDTO",rtseq=38,rtrep=.f.,;
    cFormVar = "w_LOCORN", cQueryName = "LOCORN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(28), bMultilanguage =  .f.,;
    HelpContextID = 178849974,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=19, Width=168, Left=474, Top=104, InputMask=replicate('X',28)

  add object oDESVET_4_26 as StdField with uid="QSQLRZYDUK",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESVET", cQueryName = "DESVET",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 266403382,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=129, Top=202, InputMask=replicate('X',35)


  add object oObj_4_27 as cp_runprogram with uid="JDCUSYGWHM",left=7, top=433, width=244,height=19,;
    caption='GSVE_BCO(P)',;
   bGlobalFont=.t.,;
    prg="GSVE_BCO('P')",;
    cEvent = "w_PLCODDES Changed",;
    nPag=4;
    , HelpContextID = 9232587

  add object oPROORN_4_28 as StdField with uid="FMJSFWLWKI",rtseq=41,rtrep=.f.,;
    cFormVar = "w_PROORN", cQueryName = "PROORN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(2), bMultilanguage =  .f.,;
    HelpContextID = 178899958,;
    FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=20, Width=24, Left=646, Top=104, InputMask=replicate('X',2)

  add object oStr_4_9 as StdString with uid="UECQCBZQHK",Visible=.t., Left=31, Top=252,;
    Alignment=1, Width=38, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_4_12 as StdString with uid="LUXZUAAEGI",Visible=.t., Left=3, Top=177,;
    Alignment=1, Width=66, Height=15,;
    Caption="Spedizione:"  ;
  , bGlobalFont=.t.

  add object oStr_4_13 as StdString with uid="GYKRALJYHO",Visible=.t., Left=2, Top=152,;
    Alignment=1, Width=67, Height=15,;
    Caption="Porto:"  ;
  , bGlobalFont=.t.

  add object oStr_4_23 as StdString with uid="JJYXOGAMPS",Visible=.t., Left=406, Top=20,;
    Alignment=0, Width=191, Height=18,;
    Caption="Per conto di"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_24 as StdString with uid="RTTBFBZHJV",Visible=.t., Left=71, Top=20,;
    Alignment=0, Width=113, Height=18,;
    Caption="Destinazione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_25 as StdString with uid="PGJLAUXFDM",Visible=.t., Left=3, Top=204,;
    Alignment=1, Width=66, Height=18,;
    Caption="Vettore:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgsma_mpkBodyRow as CPBodyRowCnt
  Width=665
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="SMFWPYLMSW",rtseq=7,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    ToolTipText = "Progressivo del collo",;
    HelpContextID = 67503766,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=49, Left=-2, Top=0, cSayPict=["999999"], cGetPict=["999999"]

  add object oPLTIPCOL_2_2 as StdTrsField with uid="WBYPROVXJU",rtseq=8,rtrep=.t.,;
    cFormVar="w_PLTIPCOL",value=space(5),;
    ToolTipText = "Tipologia collo",;
    HelpContextID = 260314690,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=59, Left=49, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_COLL", cZoomOnZoom="GSAR_MTO", oKey_1_1="TCCODICE", oKey_1_2="this.w_PLTIPCOL"

  func oPLTIPCOL_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPLTIPCOL_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPLTIPCOL_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_COLL','*','TCCODICE',cp_AbsName(this.parent,'oPLTIPCOL_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MTO',"Tipologie colli",'',this.parent.oContained
  endproc
  proc oPLTIPCOL_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MTO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_PLTIPCOL
    i_obj.ecpSave()
  endproc

  add object oPLDESCOL_2_4 as StdTrsField with uid="KPXKUVQXJO",rtseq=9,rtrep=.t.,;
    cFormVar="w_PLDESCOL",value=space(35),enabled=.f.,;
    HelpContextID = 263132738,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    FontName="Arial", FontSize=9, FontBold=.f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=17, Width=251, Left=111, Top=0, InputMask=replicate('X',35)

  add object oPLVOLCOL_2_5 as StdTrsField with uid="PTTVHYJEBI",rtseq=10,rtrep=.t.,;
    cFormVar="w_PLVOLCOL",value=0,;
    ToolTipText = "Volume del collo",;
    HelpContextID = 256521794,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=83, Left=367, Top=0, cSayPict=[v_PQ(10)], cGetPict=[v_GQ(10)]

  add object oPLUMVOLU_2_6 as StdTrsField with uid="PJVSHUMGPB",rtseq=11,rtrep=.t.,;
    cFormVar="w_PLUMVOLU",value=space(3),;
    ToolTipText = "Unit� di misura del volume",;
    HelpContextID = 199763531,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=34, Left=457, Top=0, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_PLUMVOLU"

  func oPLUMVOLU_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oPLUMVOLU_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPLUMVOLU_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oPLUMVOLU_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unit� di misura",'',this.parent.oContained
  endproc
  proc oPLUMVOLU_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_PLUMVOLU
    i_obj.ecpSave()
  endproc

  add object oPLPESLOR_2_7 as StdTrsField with uid="CKJQIAXZCB",rtseq=12,rtrep=.t.,;
    cFormVar="w_PLPESLOR",value=0,;
    ToolTipText = "Peso lordo in kg.",;
    HelpContextID = 145741384,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=500, Top=0, cSayPict=["999999.999"], cGetPict=["999999.999"]

  add object oPL__TARA_2_8 as StdTrsField with uid="QVDLIZYPTQ",rtseq=13,rtrep=.t.,;
    cFormVar="w_PL__TARA",value=0,;
    ToolTipText = "Tara in kg",;
    HelpContextID = 35994057,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=584, Top=0, cSayPict=["999999.999"], cGetPict=["999999.999"]
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=4
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_mpk','PAC_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PLSERIAL=PAC_MAST.PLSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
