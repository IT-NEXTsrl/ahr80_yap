* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bge                                                        *
*              Gestione ev.righe descrittive                                   *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_7]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-02-18                                                      *
* Last revis.: 2005-02-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo,pSerial,pDSERRIF,pDROWRIF,pDNUMRIF,pDATREG
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bge",oParentObject,m.pTipo,m.pSerial,m.pDSERRIF,m.pDROWRIF,m.pDNUMRIF,m.pDATREG)
return(i_retval)

define class tgsve_bge as StdBatch
  * --- Local variables
  pTipo = space(10)
  pSerial = space(10)
  pDSERRIF = space(10)
  pDROWRIF = 0
  pDNUMRIF = 0
  pDATREG = ctod("  /  /  ")
  * --- WorkFile variables
  DET_DIFF_idx=0
  DOC_DETT_idx=0
  EVA_DESC_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione righe descrittive evase nel caso di fatturazione per ordine
    *     Utilizzo:
    *     pTipo='CHECK'
    *     Verifica se la fattura in fase di cancellazione � l'ultima del piano di fatturazione
    *     pTipo='NEW'
    *     Verifica se ha abbinato un elenco di righe descrittive
    *     pTipo='RIAPRE'
    *     Riapre nel DDT le righe descrittive
    *     pSerial
    *     Seriale piano di fatturazione
    *     Ritorna .T. o .F.
    * --- Parametri aggiuntivi  (chiave) per chiudere le righe descrittive
    do case
      case this.pTipo="CHECK"
        * --- Ho eliminato tutte le fatture del piano ?
        * --- Read from DET_DIFF
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DET_DIFF_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DET_DIFF_idx,2],.t.,this.DET_DIFF_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" DET_DIFF where ";
                +"DPSERIAL = "+cp_ToStrODBC(this.pSerial);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                DPSERIAL = this.pSerial;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        i_retcode = 'stop'
        i_retval = i_Rows=0
        return
      case this.pTipo="RIAPRE"
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_SetAzi(i_TableProp[this.EVA_DESC_idx,2])
          i_cTempTable=cp_GetTempTableName(i_nConn)
          i_aIndex[1]='MVSERIAL,CPROWNUM,MVNUMRIF'
          cp_CreateTempTable(i_nConn,i_cTempTable,"EDSERDDT As MVSERIAL,EDROWDDT As CPROWNUM ,EDRIFDDT As MVNUMRIF "," from "+i_cQueryTable+" where EDSERPIA ="+cp_ToStrODBC(this.pSerial)+"",.f.,@i_aIndex)
          i_cQueryTable=i_cTempTable
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                  +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
          +",MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'DOC_DETT','MVDATGEN');
              +i_ccchkf;
              +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                  +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
          +"DOC_DETT.MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
          +",DOC_DETT.MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'DOC_DETT','MVDATGEN');
              +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="PostgreSQL"
            i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                  +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                  +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
          +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
          +",MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'DOC_DETT','MVDATGEN');
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                  +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
          +",MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'DOC_DETT','MVDATGEN');
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Elimino i riferimenti...
        * --- Delete from EVA_DESC
        i_nConn=i_TableProp[this.EVA_DESC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.EVA_DESC_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"EDSERPIA = "+cp_ToStrODBC(this.pSerial);
                 )
        else
          delete from (i_cTable) where;
                EDSERPIA = this.pSerial;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        i_retcode = 'stop'
        i_retval = .T.
        return
      case this.pTipo="NEW"
        * --- Read from EVA_DESC
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.EVA_DESC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.EVA_DESC_idx,2],.t.,this.EVA_DESC_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" EVA_DESC where ";
                +"EDSERPIA = "+cp_ToStrODBC(this.pSerial);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                EDSERPIA = this.pSerial;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        i_retcode = 'stop'
        i_retval = i_Rows<>0
        return
      case this.pTipo="CHIUDE"
        * --- Marca come evasa la riga descrittiva e la aggiunge all'elenco...
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVDATGEN ="+cp_NullLink(cp_ToStrODBC(this.pDATREG),'DOC_DETT','MVDATGEN');
          +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_DETT','MVFLEVAS');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.pDSERRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.pDROWRIF);
              +" and MVNUMRIF = "+cp_ToStrODBC(this.pDNUMRIF);
                 )
        else
          update (i_cTable) set;
              MVDATGEN = this.pDATREG;
              ,MVFLEVAS = "S";
              &i_ccchkf. ;
           where;
              MVSERIAL = this.pDSERRIF;
              and CPROWNUM = this.pDROWRIF;
              and MVNUMRIF = this.pDNUMRIF;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Memorizzo l'operazione...
        * --- Insert into EVA_DESC
        i_nConn=i_TableProp[this.EVA_DESC_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.EVA_DESC_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.EVA_DESC_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"EDSERPIA"+",EDSERDDT"+",EDROWDDT"+",EDRIFDDT"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.pSerial),'EVA_DESC','EDSERPIA');
          +","+cp_NullLink(cp_ToStrODBC(this.pDSERRIF),'EVA_DESC','EDSERDDT');
          +","+cp_NullLink(cp_ToStrODBC(this.pDROWRIF),'EVA_DESC','EDROWDDT');
          +","+cp_NullLink(cp_ToStrODBC(this.pDNUMRIF),'EVA_DESC','EDRIFDDT');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'EDSERPIA',this.pSerial,'EDSERDDT',this.pDSERRIF,'EDROWDDT',this.pDROWRIF,'EDRIFDDT',this.pDNUMRIF)
          insert into (i_cTable) (EDSERPIA,EDSERDDT,EDROWDDT,EDRIFDDT &i_ccchkf. );
             values (;
               this.pSerial;
               ,this.pDSERRIF;
               ,this.pDROWRIF;
               ,this.pDNUMRIF;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        i_retcode = 'stop'
        i_retval = .T.
        return
    endcase
  endproc


  proc Init(oParentObject,pTipo,pSerial,pDSERRIF,pDROWRIF,pDNUMRIF,pDATREG)
    this.pTipo=pTipo
    this.pSerial=pSerial
    this.pDSERRIF=pDSERRIF
    this.pDROWRIF=pDROWRIF
    this.pDNUMRIF=pDNUMRIF
    this.pDATREG=pDATREG
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='DET_DIFF'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='EVA_DESC'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo,pSerial,pDSERRIF,pDROWRIF,pDNUMRIF,pDATREG"
endproc
