* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_bmx                                                        *
*              Movim. di analitica da schede                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_165]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-21                                                      *
* Last revis.: 2013-12-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pSERIAL,pROWNUM,pIMPDAR,pIMPAVE,pTIPMOV,pDATREG
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca_bmx",oParentObject,m.pSERIAL,m.pROWNUM,m.pIMPDAR,m.pIMPAVE,m.pTIPMOV,m.pDATREG)
return(i_retval)

define class tgsca_bmx as StdBatch
  * --- Local variables
  pSERIAL = space(10)
  pROWNUM = 0
  pIMPDAR = 0
  pIMPAVE = 0
  pTIPMOV = space(10)
  pDATREG = .f.
  w_MVCODCOM = space(15)
  w_MVCODCEN = space(15)
  w_MVVOCCEN = space(15)
  w_DESCOM = space(30)
  w_DATOBSO = ctod("  /  /  ")
  w_VALORE = 0
  w_COMPDA = ctod("  /  /  ")
  w_DESCOS = space(40)
  w_OBTEST = ctod("  /  /  ")
  w_CODUTE = 0
  w_COMPA = ctod("  /  /  ")
  w_DESVOC = space(40)
  w_SEGNO = space(1)
  w_NUMREG = 0
  MovAna = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Manutenzione Schede Analitica da Schede Movimenti (da GSCA_SZM)
    if Not Empty(CHKCONS("PC",this.pDATREG,"B","S"))
      * --- Esegue controllo data consolidamento
      i_retcode = 'stop'
      return
    endif
    if g_COGE="S" AND LEFT(this.pTIPMOV,5)="(P.N."
      * --- Questo oggetto sar� definito come Gestione Movimenti di Analitica
      * --- Istanzio l'oggetto come Gestione Movimenti di Analitica
      this.MovAna = GSCA_MMX()
      * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
      if !(this.MovAna.bSec1)
        i_retcode = 'stop'
        return
      endif
      this.MovAna.i_Batch = .t.
      this.MovAna.ecpFilter()     
      * --- inizializzo la chiave della Prima Nota
      this.MovAna.w_MRSERIAL = this.pSERIAL
      this.MovAna.w_MRROWORD = this.pROWNUM
      this.MovAna.w_IMPDAR = this.pIMPDAR
      this.MovAna.w_IMPAVE = this.pIMPAVE
      this.MovAna.ecpSave()     
      this.MovAna.ecpEdit()     
      if EMPTY(this.MOVANA.w_MRSERIAL)
        this.MovAna.ecpLoad()     
        this.MovAna.w_MRSERIAL = this.pSERIAL
        this.MovAna.w_MRROWORD = this.pROWNUM
        this.Movana.notifyevent("Rilegge")
      endif
      this.MovAna.i_Batch = .f.
    else
      ah_ErrorMsg("Selezionare un movimento da primanota",,"")
    endif
  endproc


  proc Init(oParentObject,pSERIAL,pROWNUM,pIMPDAR,pIMPAVE,pTIPMOV,pDATREG)
    this.pSERIAL=pSERIAL
    this.pROWNUM=pROWNUM
    this.pIMPDAR=pIMPDAR
    this.pIMPAVE=pIMPAVE
    this.pTIPMOV=pTIPMOV
    this.pDATREG=pDATREG
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pSERIAL,pROWNUM,pIMPDAR,pIMPAVE,pTIPMOV,pDATREG"
endproc
