* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bm2                                                        *
*              Documenti, cambia dati testata                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_373]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-09                                                      *
* Last revis.: 2017-01-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bm2",oParentObject)
return(i_retval)

define class tgsve_bm2 as StdBatch
  * --- Local variables
  w_SCOLIS = space(1)
  w_MESS = space(10)
  w_RECO = 0
  w_AGG = 0
  w_AGGRIG = .f.
  w_DATA = ctod("  /  /  ")
  w_MAGA = space(5)
  w_NUREC = 0
  w_CFUNC = space(5)
  w_AGGSCO = space(1)
  w_AGGSAL = space(10)
  w_AGGSAL1 = space(10)
  w_NO_DEPON = .f.
  w_OK = .f.
  w_PADRE = .NULL.
  w_OCODICE = space(20)
  w_NUREC1 = 0
  w_REC1 = 0
  w_oMess = .NULL.
  w_CALPRZ = 0
  * --- WorkFile variables
  TIP_DOCU_idx=0
  TAB_SCON_idx=0
  LISTINI_idx=0
  MAGAZZIN_idx=0
  SALDIART_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna le Righe Documento al Variare del Listino, Magazzino, Commessa, ecc. (da GSVE_MDV, GSOR_MDV, GSAC_MDV)
    this.w_PADRE = This.OparentObject
    if this.oParentObject.w_GESMAT="S" AND this.oParentObject.w_TOTMAT=this.oParentObject.w_MVQTAUM1 AND this.oParentObject.w_TOTMAT>0
      this.w_PADRE.GSVE_MMT.LinkPCClick(.T.)     
      this.w_PADRE.GSVE_MMT.EcpSave()     
    endif
    this.w_CFUNC = this.w_PADRE.cFunction
    this.w_REC1 = this.w_PADRE.NumRow()
    this.w_RECO = RECCOUNT(this.w_PADRE.CtrsName)
    this.w_NUREC = this.w_PADRE.RowIndex()
    if this.w_REC1>0
      this.w_PADRE.MarkPos()     
      this.w_OCODICE = this.oParentObject.o_MVCODICE
      this.w_NO_DEPON = .F.
      this.w_AGG = 0
      this.w_AGGSCO = " "
      * --- Oggetto per messaggi incrementali
      this.w_oMess=createobject("Ah_Message")
      do case
        case ((this.oParentObject.w_MVTCOLIS<>this.oParentObject.o_MVTCOLIS AND NOT EMPTY(this.oParentObject.w_MVTCOLIS)) or ( this.oParentObject.o_PRZVAC<>this.oParentObject.w_PRZVAC and this.oParentObject.w_PRZVAC="S" )) AND !IsAlt()
          if this.oParentObject.w_NOPRSC <> "S"
            if this.oParentObject.w_MVTCOLIS<>this.oParentObject.o_MVTCOLIS
              this.w_oMess.AddMsgPartNL("Aggiorno i prezzi sulle righe in base al listino impostato?%0Nel caso il listino non sia gestito a sconti,%0questi verranno ricalcolati da tabella sconti/maggiorazioni")     
            else
              this.w_oMess.AddMsgPartNL("Aggiorno i prezzi sulle righe in base all'ultimo prezzo di vendita?")     
            endif
            * --- Se aggiorno i listini non eseguo la Save Dependson alla fine
            this.w_AGG = 1
          else
            * --- Se la causale socumento non gestisce prezzi/sconti non si propone la domanda
            this.w_AGG = 0
          endif
        case this.oParentObject.w_MVDATEVA<>this.oParentObject.o_MVDATEVA AND NOT EMPTY(this.oParentObject.w_MVDATEVA) 
          this.w_AGG = 2
          if this.oParentObject.w_TDORDAPE <> "S" 
            this.w_oMess.AddMsgPartNL("Aggiorno le date di evasione presenti sulle righe?")     
          else
            this.w_oMess.AddMsgPartNL("Aggiorno le date di prossima fatturazione presenti sulle righe?")     
          endif
          this.w_DATA = this.oParentObject.w_MVDATEVA
          * --- Aggiorna w_ODTE
          this.oParentObject.w_ODTE = IIF(EMPTY(this.oParentObject.w_ODTE), this.oParentObject.w_MVDATDOC, this.oParentObject.w_MVDATEVA)
        case this.oParentObject.w_MVCODMAG<>this.oParentObject.o_MVCODMAG 
          this.w_AGG = 3
          this.w_oMess.AddMsgPartNL("Aggiorno i magazzini presenti sulle righe?")     
          this.w_NO_DEPON = .T.
          this.w_MAGA = this.oParentObject.w_MVCODMAG
        case this.oParentObject.w_MVCODMAT<>this.oParentObject.o_MVCODMAT 
          this.w_AGG = 4
          this.w_oMess.AddMsgPartNL("Aggiorno i magazzini collegati presenti sulle righe?")     
          this.w_NO_DEPON = .T.
          this.w_MAGA = this.oParentObject.w_MVCODMAT
        case this.oParentObject.w_MVCODVAL<>this.oParentObject.o_MVCODVAL 
          this.w_AGG = 5
          this.w_NO_DEPON = .T.
          * --- Reimposta i Decimali
          =DEFPIC(this.oParentObject.w_DECTOT)
          =DEFPIU(this.oParentObject.w_DECUNI)
        case ((this.oParentObject.w_MVTCOLIS<>this.oParentObject.o_MVTCOLIS And EMPTY(this.oParentObject.w_MVTCOLIS)) or ( this.oParentObject.o_PRZVAC<>this.oParentObject.w_PRZVAC and this.oParentObject.w_PRZVAC<>"S" )) AND !IsAlt()
          * --- Read from LISTINI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.LISTINI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LSFLSCON"+;
              " from "+i_cTable+" LISTINI where ";
                  +"LSCODLIS = "+cp_ToStrODBC(this.oParentObject.o_MVTCOLIS);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LSFLSCON;
              from (i_cTable) where;
                  LSCODLIS = this.oParentObject.o_MVTCOLIS;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SCOLIS = NVL(cp_ToDate(_read_.LSFLSCON),cp_NullValue(_read_.LSFLSCON))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.oParentObject.w_NOPRSC <> "S"
            if this.oParentObject.w_MVTCOLIS<>this.oParentObject.o_MVTCOLIS
              this.w_oMess.AddMsgPartNL("Aggiorno gli sconti secondo la tabella sconti/maggiorazioni?")     
              this.w_oMess.AddMsgPartNL("Se gli sconti precedenti sono calcolati da contratto,%0o le nuove impostazioni non trovano sconti validi,%0verranno mantenuti quelli precedentemente calcolati")     
              this.w_OK = this.w_SCOLIS<>"S"
            else
              this.w_oMess.AddMsgPartNL("Aggiorno i prezzi sulle righe?")     
            endif
            this.w_AGG = 6
          else
            this.w_AGG = 0
          endif
      endcase
      this.w_PADRE.MarkPos()     
      this.w_PADRE.FirstRow()     
      if this.w_PADRE.FullRow()
        this.w_NUREC1 = this.w_PADRE.RowIndex()
      endif
      this.w_PADRE.RePos(this.w_NO_DEPON)     
      if ((this.w_AGG=3 OR this.w_AGG=4) AND ((this.w_CFUNC="Load" AND this.w_NUREC=this.w_RECO) OR EMPTY(this.w_MAGA))) Or ( this.w_AGG=2 AND this.w_CFUNC="Load" AND ( this.w_NUREC<>this.w_NUREC1 OR this.w_NUREC=this.w_RECO))
        * --- I Dati di riga 3,4 si variano solo in Modifica oppure se non sono all'ultima riga
        *     I Dati di riga 2 si variano solo in Modifica oppure se sono sulla prima riga
        this.w_AGG = 0
      endif
      if this.w_AGG>0
        * --- Nel caso di cambio valuta non do nessun messaggio in quanto cmq
        *     vado a ricalcolare MVIMPNAZ.
        this.w_OK = (this.w_AGG=5) Or this.w_oMess.ah_YesNo()
        if this.w_OK AND this.w_AGG=1
          this.w_AGGSCO = "S"
        endif
        if this.w_AGG=5
          this.oParentObject.w_MVTCOLIS = SPACE(5)
          this.oParentObject.w_MVTCONTR = SPACE(15)
          if g_GESCON="S" AND this.oParentObject.w_MVTIPCON $ "CF" AND NOT EMPTY(this.oParentObject.w_XCONORN)
            * --- Legge contratto associato alla nuova Valuta
            * --- Select from GSVE_BES
            do vq_exec with 'GSVE_BES',this,'_Curs_GSVE_BES','',.f.,.t.
            if used('_Curs_GSVE_BES')
              select _Curs_GSVE_BES
              locate for 1=1
              do while not(eof())
              if NOT EMPTY(NVL(CONUMERO," "))
                if (this.oParentObject.w_MVFLSCOR<>"S" AND NVL(_Curs_GSVE_BES.COIVALIS," ")<>"L") OR (this.oParentObject.w_MVFLSCOR="S" AND NVL(_Curs_GSVE_BES.COIVALIS," ")="L") 
                  * --- Ordinato per data piu' recente (prende l'ultimo)
                  this.oParentObject.w_MVTCONTR = _Curs_GSVE_BES.CONUMERO
                endif
              endif
                select _Curs_GSVE_BES
                continue
              enddo
              use
            endif
          endif
        endif
        if this.w_OK OR this.w_AGG=5 OR this.w_AGG=6
          * --- Se Tipo 5 Aggiorna comunque gli Importi Fiscali
          this.w_PADRE.mCalc(.T.)     
          this.w_AGGRIG = this.w_AGG=1
          this.w_PADRE.FirstRow()     
          do while Not this.w_PADRE.Eof_Trs()
            this.w_PADRE.SetRow()     
            if this.w_PADRE.FullRow()
              this.w_PADRE.SaveDependsOn()     
              do case
                case this.w_AGG=1
                  this.oParentObject.w_MVCODLIS = this.oParentObject.w_MVTCOLIS
                  if this.w_AGGRIG
                    this.oParentObject.w_LIPREZZO = 0
                    this.w_PADRE.NotifyEvent("Ricalcola")     
                  endif
                  this.oParentObject.o_MVCODLIS = this.oParentObject.w_MVTCOLIS
                case this.w_AGG=2 AND Not this.oParentObject.w_TESCOMP
                  this.oParentObject.w_MVDATEVA = this.w_DATA
                case this.w_AGG=3 AND Not this.oParentObject.w_TESCOMP
                  this.w_AGGSAL = ALLTRIM(this.oParentObject.w_FLCASC+this.oParentObject.w_FLRISE+this.oParentObject.w_FLORDI+this.oParentObject.w_FLIMPE)
                  this.oParentObject.w_MVCODMAG = IIF(this.oParentObject.w_MVTIPRIG="R" AND NOT EMPTY(this.w_AGGSAL) , this.w_MAGA, SPACE(5))
                  if Not Empty(this.oParentObject.w_MVCODMAG)
                    * --- Read from MAGAZZIN
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "MGDESMAG,MGDTOBSO,MGFLUBIC,MGPROMAG"+;
                        " from "+i_cTable+" MAGAZZIN where ";
                            +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVCODMAG);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        MGDESMAG,MGDTOBSO,MGFLUBIC,MGPROMAG;
                        from (i_cTable) where;
                            MGCODMAG = this.oParentObject.w_MVCODMAG;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.oParentObject.w_DESAPP = NVL(cp_ToDate(_read_.MGDESMAG),cp_NullValue(_read_.MGDESMAG))
                      this.oParentObject.w_DTOBS2 = NVL(cp_ToDate(_read_.MGDTOBSO),cp_NullValue(_read_.MGDTOBSO))
                      this.oParentObject.w_FLUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
                      this.oParentObject.w_MPROORD = NVL(cp_ToDate(_read_.MGPROMAG),cp_NullValue(_read_.MGPROMAG))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    this.oParentObject.w_MVPROORD = IIF(NOT EMPTY(this.oParentObject.w_MPROORD),this.oParentObject.w_MPROORD,this.oParentObject.w_OPROORD)
                    this.oParentObject.w_MVCODUBI = SPACE(20)
                  endif
                  if Not Empty(this.oParentObject.w_MVCODMAG) AND Not Empty(this.oParentObject.w_MVKEYSAL)
                    * --- Read from SALDIART
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.SALDIART_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "SLQTRPER,SLQTAPER,SLDATUCA,SLDATUPV,SLVALUCA,SLCODVAA,SLCODVAV"+;
                        " from "+i_cTable+" SALDIART where ";
                            +"SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVKEYSAL);
                            +" and SLCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVCODMAG);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        SLQTRPER,SLQTAPER,SLDATUCA,SLDATUPV,SLVALUCA,SLCODVAA,SLCODVAV;
                        from (i_cTable) where;
                            SLCODICE = this.oParentObject.w_MVKEYSAL;
                            and SLCODMAG = this.oParentObject.w_MVCODMAG;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.oParentObject.w_QTRPER = NVL(cp_ToDate(_read_.SLQTRPER),cp_NullValue(_read_.SLQTRPER))
                      this.oParentObject.w_QTAPER = NVL(cp_ToDate(_read_.SLQTAPER),cp_NullValue(_read_.SLQTAPER))
                      this.oParentObject.w_ULTCAR = NVL(cp_ToDate(_read_.SLDATUCA),cp_NullValue(_read_.SLDATUCA))
                      this.oParentObject.w_ULTSCA = NVL(cp_ToDate(_read_.SLDATUPV),cp_NullValue(_read_.SLDATUPV))
                      this.oParentObject.w_VALUCA = NVL(cp_ToDate(_read_.SLVALUCA),cp_NullValue(_read_.SLVALUCA))
                      this.oParentObject.w_CODVAA = NVL(cp_ToDate(_read_.SLCODVAA),cp_NullValue(_read_.SLCODVAA))
                      this.oParentObject.w_CODVAV = NVL(cp_ToDate(_read_.SLCODVAV),cp_NullValue(_read_.SLCODVAV))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                  endif
                  this.oParentObject.w_MVFLLOTT = IIF((g_PERLOT="S" AND this.oParentObject.w_FLLOTT$ "SC") OR (g_PERUBI="S" AND this.oParentObject.w_FLUBIC="S"), LEFT(ALLTRIM(this.oParentObject.w_MVFLCASC)+IIF(this.oParentObject.w_MVFLRISE="+", "-", IIF(this.oParentObject.w_MVFLRISE="-", "+", " ")), 1), " ")
                  this.oParentObject.w_MVLOTMAG = iif( Empty( this.oParentObject.w_MVCODLOT ) And Empty( this.oParentObject.w_MVCODUBI ) , SPACE(5) , this.oParentObject.w_MVCODMAG )
                  if this.oParentObject.w_GESMAT="S" AND this.oParentObject.w_TOTMAT=this.oParentObject.w_MVQTAUM1 AND this.oParentObject.w_TOTMAT>0
                    this.w_PADRE.GSVE_MMT.LinkPCClick(.T.)     
                  endif
                case this.w_AGG=4 AND Not this.oParentObject.w_TESCOMP
                  this.w_AGGSAL1 = ALLTRIM(this.oParentObject.w_F2CASC+this.oParentObject.w_F2RISE+this.oParentObject.w_F2ORDI+this.oParentObject.w_F2IMPE)
                  this.oParentObject.w_MVCODMAT = IIF(this.oParentObject.w_MVTIPRIG="R" AND NOT EMPTY(this.w_AGGSAL1) , this.w_MAGA, SPACE(5))
                  this.oParentObject.w_MVLOTMAT = iif( Empty( this.oParentObject.w_MVCODLOT ) And Empty( this.oParentObject.w_MVCODUBI ) , SPACE(5) , this.oParentObject.w_MVCODMAT )
                  if Not Empty(this.oParentObject.w_MVCODMAT)
                    * --- Read from MAGAZZIN
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "MGDESMAG,MGDTOBSO,MGFLUBIC"+;
                        " from "+i_cTable+" MAGAZZIN where ";
                            +"MGCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVCODMAT);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        MGDESMAG,MGDTOBSO,MGFLUBIC;
                        from (i_cTable) where;
                            MGCODMAG = this.oParentObject.w_MVCODMAT;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.oParentObject.w_DESAPP = NVL(cp_ToDate(_read_.MGDESMAG),cp_NullValue(_read_.MGDESMAG))
                      this.oParentObject.w_DTOBS2 = NVL(cp_ToDate(_read_.MGDTOBSO),cp_NullValue(_read_.MGDTOBSO))
                      this.oParentObject.w_F2UBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    this.oParentObject.w_MVCODUB2 = SPACE(20)
                  endif
                  if Not Empty(this.oParentObject.w_MVCODMAT) AND Not Empty(this.oParentObject.w_MVKEYSAL)
                    * --- Read from SALDIART
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.SALDIART_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "SLQTRPER,SLQTAPER"+;
                        " from "+i_cTable+" SALDIART where ";
                            +"SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVKEYSAL);
                            +" and SLCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVCODMAT);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        SLQTRPER,SLQTAPER;
                        from (i_cTable) where;
                            SLCODICE = this.oParentObject.w_MVKEYSAL;
                            and SLCODMAG = this.oParentObject.w_MVCODMAT;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.oParentObject.w_Q2RPER = NVL(cp_ToDate(_read_.SLQTRPER),cp_NullValue(_read_.SLQTRPER))
                      this.oParentObject.w_Q2APER = NVL(cp_ToDate(_read_.SLQTAPER),cp_NullValue(_read_.SLQTAPER))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                  endif
                  this.oParentObject.w_MVF2LOTT = IIF((g_PERLOT="S" AND this.oParentObject.w_FLLOTT$ "SC") OR (g_PERUBI="S" AND this.oParentObject.w_F2UBIC="S"), LEFT(ALLTRIM(this.oParentObject.w_MVF2CASC)+IIF(this.oParentObject.w_MVF2RISE="+", "-", IIF(this.oParentObject.w_MVF2RISE="-", "+", " ")), 1), " ")
                  if this.oParentObject.w_GESMAT="S" AND this.oParentObject.w_TOTMAT=this.oParentObject.w_MVQTAUM1 AND this.oParentObject.w_TOTMAT>0
                    this.w_PADRE.GSVE_MMT.LinkPCClick(.T.)     
                  endif
                case this.w_AGG=5
                  this.oParentObject.w_MVIMPNAZ = CAIMPNAZ(this.oParentObject.w_MVFLVEAC, this.oParentObject.w_MVVALMAG, this.oParentObject.w_MVCAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_MVDATDOC, this.oParentObject.w_MVVALNAZ, this.oParentObject.w_MVCODVAL, this.oParentObject.w_MVCODIVE, this.oParentObject.w_PERIVE, this.oParentObject.w_INDIVE, NVL(this.oParentObject.w_PERIVA,0), NVL(this.oParentObject.w_INDIVA,0) )
                  this.oParentObject.w_MVVALULT = IIF(this.oParentObject.w_MVQTAUM1=0, 0, cp_ROUND(this.oParentObject.w_MVIMPNAZ/this.oParentObject.w_MVQTAUM1, this.oParentObject.w_DECTOU))
                  this.oParentObject.w_VISNAZ = cp_ROUND(this.oParentObject.w_MVIMPNAZ, this.oParentObject.w_DECTOP)
                  this.oParentObject.w_MVCODLIS = this.oParentObject.w_MVTCOLIS
                  this.oParentObject.w_MVCONTRA = this.oParentObject.w_MVTCONTR
                  * --- Ricalcola le Spese di Incasso
                  this.w_PADRE.NotifyEvent("CalcolaSpeseIncasso")     
                  if this.w_OK
                    this.oParentObject.w_LIPREZZO = 0
                    this.w_PADRE.NotifyEvent("Ricalcola")     
                  endif
                case this.w_AGG=6
                  if this.oParentObject.w_MVTCOLIS<>this.oParentObject.o_MVTCOLIS
                    this.oParentObject.w_MVCODLIS = this.oParentObject.w_MVTCOLIS
                    if this.w_OK And this.oParentObject.w_LISCON<>1
                      DECLARE ARRCALC (16,1)
                      DIMENSION pArrUm[9]
                      pArrUm [1] = " " 
 pArrUm [2] = " " 
 pArrUm [3] = 0 
 pArrUm [4] = " " 
 pArrUm [5] = 0 
 pArrUm [6] = " " 
 pArrUm [7] = 0 
 pArrUm [8] = " " 
 pArrUm[9] =0
                      * --- Azzero l'Array che verr� riempito dalla Funzione
                      ARRCALC(1)=0
                      * --- Rileggo solo gli sconti da tabella sconti maggiorazioni
                      this.w_CALPRZ = CalPrzli( REPL("X",16) , " " , " " , this.oParentObject.w_MVCODART , " " , this.oParentObject.w_MVQTAUM1 , this.oParentObject.w_MVCODVAL , this.oParentObject.w_MVCAOVAL , this.oParentObject.w_MVDATREG , this.oParentObject.w_CATSCC , "XXXXXX" , " ", " ", " ", " ", " ", 0,"T", @ARRCALC, " ", " " ,"N",@pArrUm)
                      * --- Ricalcolo gli sconti solo se gli ho trovati altrimenti li sbiancherebbe
                      if Not Empty(ARRCALC(1))
                        this.oParentObject.w_MVSCONT1 = ARRCALC(1)
                        this.oParentObject.w_MVSCONT2 = ARRCALC(2)
                        this.oParentObject.w_MVSCONT3 = ARRCALC(3)
                        this.oParentObject.w_MVSCONT4 = ARRCALC(4)
                        this.w_NO_DEPON = .T.
                      endif
                    endif
                    this.oParentObject.w_LIPREZZO = this.oParentObject.w_MVPREZZO
                    this.oParentObject.o_MVCODLIS = this.oParentObject.w_MVTCOLIS
                  else
                    if this.w_OK
                      this.oParentObject.w_LIPREZZO = 0
                      this.w_PADRE.NotifyEvent("Ricalcola")     
                    endif
                  endif
              endcase
              * --- Salvo la riga sui temporanei..
              this.w_PADRE.SaveRow()     
              * --- Marco la riga come modificata..
              this.w_PADRE.SetupdateRow()     
            endif
            * --- Passo alla prossima riga non cancellata..
            this.w_PADRE.NextRow()     
          enddo
        endif
      endif
      this.w_PADRE.RePos(this.w_NO_DEPON)     
      this.oParentObject.o_MVCODICE = this.w_OCODICE
      if this.w_AGG=2 Or this.w_AGG=3 Or this.w_AGG=4
        * --- Per non ricalcolare il prezzo sulla riga.
        this.oParentObject.o_LIPREZZO = this.oParentObject.w_LIPREZZO
        this.oParentObject.o_MVCODLIS = this.oParentObject.w_MVCODLIS
      endif
    endif
    if this.oParentObject.w_MVCODVAL<>this.oParentObject.o_MVCODVAL
      * --- Evita la doppia rilettura del Cambio
      this.bUpdateParentObject=.f.
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='TIP_DOCU'
    this.cWorkTables[2]='TAB_SCON'
    this.cWorkTables[3]='LISTINI'
    this.cWorkTables[4]='MAGAZZIN'
    this.cWorkTables[5]='SALDIART'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_GSVE_BES')
      use in _Curs_GSVE_BES
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
