* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_szm                                                        *
*              Visualizza schede contabili                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_156]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-26                                                      *
* Last revis.: 2012-11-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gscg_szm
* --- Inizializza Picture
VVP=20*g_PERPVL

* --- Fine Area Manuale
return(createobject("tgscg_szm",oParentObject))

* --- Class definition
define class tgscg_szm as StdForm
  Top    = 2
  Left   = 8

  * --- Standard Properties
  Width  = 766
  Height = 496
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-06"
  HelpContextID=26598039
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=59

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  ESERCIZI_IDX = 0
  cPrg = "gscg_szm"
  cComment = "Visualizza schede contabili"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PNOTA = .F.
  w_CODAZI = space(5)
  w_PTIPCON = space(1)
  w_PCODCON = space(15)
  w_PCODESE = space(4)
  w_PDATFIN = ctod('  /  /  ')
  w_FLSTCONT = space(10)
  w_TIPCON = space(1)
  w_CODICE = space(15)
  w_CODESE = space(4)
  o_CODESE = space(4)
  w_INIESE = ctod('  /  /  ')
  w_FINESE2 = ctod('  /  /  ')
  w_FINESE = ctod('  /  /  ')
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_FLAPER = space(1)
  o_FLAPER = space(1)
  w_FLPROV = space(1)
  w_FLAPER = space(1)
  w_INIAVE = 0
  w_PNDESSUP = space(45)
  w_TOTDAR = 0
  w_TOTAVE = 0
  w_INIDAR = 0
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_SERIALE = space(35)
  w_NUMROW = 0
  w_FINAVE = 0
  w_FINDAR = 0
  w_DESCRI = space(40)
  w_ALFA = space(10)
  w_NUMERO = space(15)
  w_DATA = ctod('  /  /  ')
  w_SERIALP = space(10)
  w_TIPVAL = space(1)
  w_CODINI = space(15)
  w_CODFIN = space(15)
  w_CODCAU = space(5)
  w_SEZBIL = space(1)
  w_PERCOM = space(1)
  w_CODVAL = space(3)
  w_CAOVAL = 0
  w_SIMVAL = space(6)
  w_FLSPAG = space(1)
  w_FLCONT = space(1)
  w_FLDESC = space(1)
  w_DECTOT = 0
  w_SALDOD = 0
  w_SALDOA = 0
  w_CONSU = space(15)
  w_CONSU1 = space(15)
  w_CONSU2 = space(15)
  w_CATCON = space(5)
  w_CAMREG = space(1)
  w_FLCONT = space(1)
  w_DETSCA = space(1)
  w_DATSAL = space(1)
  w_RAGSOCINI = space(40)
  w_RAGSOCFIN = space(40)
  w_ZoomScad = .NULL.
  w_COLORE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_szmPag1","gscg_szm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPCON_1_8
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomScad = this.oPgFrm.Pages(1).oPag.ZoomScad
    this.w_COLORE = this.oPgFrm.Pages(1).oPag.COLORE
    DoDefault()
    proc Destroy()
      this.w_ZoomScad = .NULL.
      this.w_COLORE = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='ESERCIZI'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PNOTA=.f.
      .w_CODAZI=space(5)
      .w_PTIPCON=space(1)
      .w_PCODCON=space(15)
      .w_PCODESE=space(4)
      .w_PDATFIN=ctod("  /  /  ")
      .w_FLSTCONT=space(10)
      .w_TIPCON=space(1)
      .w_CODICE=space(15)
      .w_CODESE=space(4)
      .w_INIESE=ctod("  /  /  ")
      .w_FINESE2=ctod("  /  /  ")
      .w_FINESE=ctod("  /  /  ")
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_FLAPER=space(1)
      .w_FLPROV=space(1)
      .w_FLAPER=space(1)
      .w_INIAVE=0
      .w_PNDESSUP=space(45)
      .w_TOTDAR=0
      .w_TOTAVE=0
      .w_INIDAR=0
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_SERIALE=space(35)
      .w_NUMROW=0
      .w_FINAVE=0
      .w_FINDAR=0
      .w_DESCRI=space(40)
      .w_ALFA=space(10)
      .w_NUMERO=space(15)
      .w_DATA=ctod("  /  /  ")
      .w_SERIALP=space(10)
      .w_TIPVAL=space(1)
      .w_CODINI=space(15)
      .w_CODFIN=space(15)
      .w_CODCAU=space(5)
      .w_SEZBIL=space(1)
      .w_PERCOM=space(1)
      .w_CODVAL=space(3)
      .w_CAOVAL=0
      .w_SIMVAL=space(6)
      .w_FLSPAG=space(1)
      .w_FLCONT=space(1)
      .w_FLDESC=space(1)
      .w_DECTOT=0
      .w_SALDOD=0
      .w_SALDOA=0
      .w_CONSU=space(15)
      .w_CONSU1=space(15)
      .w_CONSU2=space(15)
      .w_CATCON=space(5)
      .w_CAMREG=space(1)
      .w_FLCONT=space(1)
      .w_DETSCA=space(1)
      .w_DATSAL=space(1)
      .w_RAGSOCINI=space(40)
      .w_RAGSOCFIN=space(40)
        .w_PNOTA = iif( type('this.oParentObject .w_PNOTA')='L', this.oParentObject .w_PNOTA, .f.)
        .w_CODAZI = i_CODAZI
        .w_PTIPCON = iif( type('this.oParentObject .w_PTIPCON')='C', this.oParentObject .w_PTIPCON, 'G')
        .w_PCODCON = iif( type('this.oParentObject .w_PCODCON')='C', this.oParentObject .w_PCODCON, .w_PCODCON)
        .w_PCODESE = iif( type('this.oParentObject .w_PCODESE')='C', this.oParentObject .w_PCODESE, .w_PCODESE)
        .w_PDATFIN = iif( type('this.oParentObject .w_PDATFIN')='D', this.oParentObject .w_PDATFIN, i_DATSYS)
        .w_FLSTCONT = 'N'
        .w_TIPCON = .w_PTIPCON
        .w_CODICE = .w_PCODCON
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_CODICE))
          .link_1_9('Full')
        endif
        .w_CODESE = .w_PCODESE
        .DoRTCalc(10,10,.f.)
        if not(empty(.w_CODESE))
          .link_1_10('Full')
        endif
          .DoRTCalc(11,12,.f.)
        .w_FINESE = cp_CharToDate((STR(DAY(.w_FINESE2))) + '-' + STR(MONTH(.w_FINESE2)) + '-' + STR(YEAR(.w_FINESE2)+1))
        .w_DATINI = IIF(EMPTY(.w_CODESE), g_INIESE, .w_INIESE)
        .w_DATFIN = IIF(EMPTY(.w_CODESE), g_FINESE, .w_FINESE)
        .w_FLAPER = 'N'
        .w_FLPROV = 'N'
        .w_FLAPER = 'N'
          .DoRTCalc(19,19,.f.)
        .w_PNDESSUP = IIF (EMPTY(.w_ZoomScad.getVar('DESRIG')),.w_ZoomScad.getVar('DESSUP'), .w_ZoomScad.getVar('DESRIG'))
      .oPgFrm.Page1.oPag.oObj_1_32.Calculate(Ah_msgformat('Saldo al %1:',dtoc(.w_DATFIN)))
      .oPgFrm.Page1.oPag.oObj_1_33.Calculate(IIF(.w_FLAPER='S','',Ah_msgformat('Saldo al %1:',dtoc(.w_DATINI-1) )))
      .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
      .oPgFrm.Page1.oPag.ZoomScad.Calculate()
          .DoRTCalc(21,23,.f.)
        .w_OBTEST = i_INIDAT
          .DoRTCalc(25,25,.f.)
        .w_SERIALE = .w_ZoomScad.getVar('PNSERIAL')
        .w_NUMROW = .w_ZoomScad.getVar('CPROWNUM')
          .DoRTCalc(28,30,.f.)
        .w_ALFA = .w_ZoomScad.getVar('ALFDOC')
        .w_NUMERO = .w_ZoomScad.getVar('NUMDOC')
        .w_DATA = .w_ZoomScad.getVar('DATDOC')
        .w_SERIALP = .w_ZoomScad.getVar('PNSERIAL')
        .w_TIPVAL = 'C'
        .w_CODINI = .w_CODICE
        .w_CODFIN = .w_CODICE
        .w_CODCAU = SPACE(5)
        .w_SEZBIL = 'S'
        .w_PERCOM = ' '
        .w_CODVAL = g_PERVAL
        .w_CAOVAL = GETCAM(.w_CODVAL, i_DATSYS, 7)
        .w_SIMVAL = g_VALSIM
        .w_FLSPAG = ' '
        .w_FLCONT = ' '
        .w_FLDESC = ' '
        .w_DECTOT = g_PERPVL
      .oPgFrm.Page1.oPag.COLORE.Calculate('     ', RGB(0,0,0), RGB(196,255,255))
      .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
        .w_SALDOD = .w_TOTDAR-.w_TOTAVE
        .w_SALDOA = ABS(.w_TOTDAR-.w_TOTAVE)
          .DoRTCalc(50,53,.f.)
        .w_CAMREG = 'N'
    endwith
    this.DoRTCalc(55,59,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_52.enabled = this.oPgFrm.Page1.oPag.oBtn_1_52.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,12,.t.)
            .w_FINESE = cp_CharToDate((STR(DAY(.w_FINESE2))) + '-' + STR(MONTH(.w_FINESE2)) + '-' + STR(YEAR(.w_FINESE2)+1))
        if .o_CODESE<>.w_CODESE
            .w_DATINI = IIF(EMPTY(.w_CODESE), g_INIESE, .w_INIESE)
        endif
        if .o_CODESE<>.w_CODESE
            .w_DATFIN = IIF(EMPTY(.w_CODESE), g_FINESE, .w_FINESE)
        endif
        if .o_CODESE<>.w_CODESE
            .w_FLAPER = 'N'
        endif
        .DoRTCalc(17,17,.t.)
        if .o_CODESE<>.w_CODESE
            .w_FLAPER = 'N'
        endif
        .DoRTCalc(19,19,.t.)
            .w_PNDESSUP = IIF (EMPTY(.w_ZoomScad.getVar('DESRIG')),.w_ZoomScad.getVar('DESSUP'), .w_ZoomScad.getVar('DESRIG'))
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate(Ah_msgformat('Saldo al %1:',dtoc(.w_DATFIN)))
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(IIF(.w_FLAPER='S','',Ah_msgformat('Saldo al %1:',dtoc(.w_DATINI-1) )))
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.ZoomScad.Calculate()
        .DoRTCalc(21,25,.t.)
            .w_SERIALE = .w_ZoomScad.getVar('PNSERIAL')
            .w_NUMROW = .w_ZoomScad.getVar('CPROWNUM')
        .DoRTCalc(28,30,.t.)
            .w_ALFA = .w_ZoomScad.getVar('ALFDOC')
            .w_NUMERO = .w_ZoomScad.getVar('NUMDOC')
            .w_DATA = .w_ZoomScad.getVar('DATDOC')
            .w_SERIALP = .w_ZoomScad.getVar('PNSERIAL')
            .w_TIPVAL = 'C'
            .w_CODINI = .w_CODICE
            .w_CODFIN = .w_CODICE
        .DoRTCalc(38,38,.t.)
            .w_SEZBIL = 'S'
            .w_PERCOM = ' '
            .w_CODVAL = g_PERVAL
            .w_CAOVAL = GETCAM(.w_CODVAL, i_DATSYS, 7)
            .w_SIMVAL = g_VALSIM
            .w_FLSPAG = ' '
            .w_FLCONT = ' '
            .w_FLDESC = ' '
            .w_DECTOT = g_PERPVL
        .oPgFrm.Page1.oPag.COLORE.Calculate('     ', RGB(0,0,0), RGB(196,255,255))
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
            .w_SALDOD = .w_TOTDAR-.w_TOTAVE
            .w_SALDOA = ABS(.w_TOTDAR-.w_TOTAVE)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(50,59,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_32.Calculate(Ah_msgformat('Saldo al %1:',dtoc(.w_DATFIN)))
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(IIF(.w_FLAPER='S','',Ah_msgformat('Saldo al %1:',dtoc(.w_DATINI-1) )))
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.ZoomScad.Calculate()
        .oPgFrm.Page1.oPag.COLORE.Calculate('     ', RGB(0,0,0), RGB(196,255,255))
        .oPgFrm.Page1.oPag.oObj_1_67.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_47.enabled = this.oPgFrm.Page1.oPag.oBtn_1_47.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_52.enabled = this.oPgFrm.Page1.oPag.oBtn_1_52.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLAPER_1_16.visible=!this.oPgFrm.Page1.oPag.oFLAPER_1_16.mHide()
    this.oPgFrm.Page1.oPag.oFLAPER_1_19.visible=!this.oPgFrm.Page1.oPag.oFLAPER_1_19.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_22.visible=!this.oPgFrm.Page1.oPag.oBtn_1_22.mHide()
    this.oPgFrm.Page1.oPag.oINIAVE_1_25.visible=!this.oPgFrm.Page1.oPag.oINIAVE_1_25.mHide()
    this.oPgFrm.Page1.oPag.oINIDAR_1_31.visible=!this.oPgFrm.Page1.oPag.oINIDAR_1_31.mHide()
    this.oPgFrm.Page1.oPag.oFINAVE_1_43.visible=!this.oPgFrm.Page1.oPag.oFINAVE_1_43.mHide()
    this.oPgFrm.Page1.oPag.oFINDAR_1_44.visible=!this.oPgFrm.Page1.oPag.oFINDAR_1_44.mHide()
    this.oPgFrm.Page1.oPag.oSALDOD_1_68.visible=!this.oPgFrm.Page1.oPag.oSALDOD_1_68.mHide()
    this.oPgFrm.Page1.oPag.oSALDOA_1_69.visible=!this.oPgFrm.Page1.oPag.oSALDOA_1_69.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_32.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.ZoomScad.Event(cEvent)
      .oPgFrm.Page1.oPag.COLORE.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_67.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODICE
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODICE))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODICE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODICE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODICE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODICE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODICE_1_9'),i_cWhere,'GSAR_BZC',"Elenco",'CONTIZOOM.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODICE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODICE)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODICE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODICE = space(15)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODESE
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_10'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_INIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_FINESE2 = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
      this.w_INIESE = ctod("  /  /  ")
      this.w_FINESE2 = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_8.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_9.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_9.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_10.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_10.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_14.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_14.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_15.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_15.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAPER_1_16.RadioValue()==this.w_FLAPER)
      this.oPgFrm.Page1.oPag.oFLAPER_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPROV_1_17.RadioValue()==this.w_FLPROV)
      this.oPgFrm.Page1.oPag.oFLPROV_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAPER_1_19.RadioValue()==this.w_FLAPER)
      this.oPgFrm.Page1.oPag.oFLAPER_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oINIAVE_1_25.value==this.w_INIAVE)
      this.oPgFrm.Page1.oPag.oINIAVE_1_25.value=this.w_INIAVE
    endif
    if not(this.oPgFrm.Page1.oPag.oPNDESSUP_1_26.value==this.w_PNDESSUP)
      this.oPgFrm.Page1.oPag.oPNDESSUP_1_26.value=this.w_PNDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTDAR_1_28.value==this.w_TOTDAR)
      this.oPgFrm.Page1.oPag.oTOTDAR_1_28.value=this.w_TOTDAR
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTAVE_1_29.value==this.w_TOTAVE)
      this.oPgFrm.Page1.oPag.oTOTAVE_1_29.value=this.w_TOTAVE
    endif
    if not(this.oPgFrm.Page1.oPag.oINIDAR_1_31.value==this.w_INIDAR)
      this.oPgFrm.Page1.oPag.oINIDAR_1_31.value=this.w_INIDAR
    endif
    if not(this.oPgFrm.Page1.oPag.oFINAVE_1_43.value==this.w_FINAVE)
      this.oPgFrm.Page1.oPag.oFINAVE_1_43.value=this.w_FINAVE
    endif
    if not(this.oPgFrm.Page1.oPag.oFINDAR_1_44.value==this.w_FINDAR)
      this.oPgFrm.Page1.oPag.oFINDAR_1_44.value=this.w_FINDAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_46.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_46.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSALDOD_1_68.value==this.w_SALDOD)
      this.oPgFrm.Page1.oPag.oSALDOD_1_68.value=this.w_SALDOD
    endif
    if not(this.oPgFrm.Page1.oPag.oSALDOA_1_69.value==this.w_SALDOA)
      this.oPgFrm.Page1.oPag.oSALDOA_1_69.value=this.w_SALDOA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_DATINI)) or not((.w_DATINI<=.w_DATFIN OR (EMPTY(.w_DATFIN))) AND .w_DATINI>=.w_INIESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_14.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � superiore alla finale o precedente all'inizio dell'esercizio")
          case   ((empty(.w_DATFIN)) or not(.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATINI)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_15.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CODESE = this.w_CODESE
    this.o_FLAPER = this.w_FLAPER
    return

enddefine

* --- Define pages as container
define class tgscg_szmPag1 as StdContainer
  Width  = 762
  height = 496
  stdWidth  = 762
  stdheight = 496
  resizeXpos=407
  resizeYpos=316
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPCON_1_8 as StdCombo with uid="TKQYZYBPVV",rtseq=8,rtrep=.f.,left=78,top=8,width=80,height=21;
    , ToolTipText = "Tipo di conto selezionato";
    , HelpContextID = 187815626;
    , cFormVar="w_TIPCON",RowSource=""+"Cliente,"+"Fornitore,"+"Conto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_8.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'G',;
    ' '))))
  endfunc
  func oTIPCON_1_8.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_8.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      iif(this.Parent.oContained.w_TIPCON=='G',3,;
      0)))
  endfunc

  func oTIPCON_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODICE)
        bRes2=.link_1_9('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODICE_1_9 as StdField with uid="ZZFIMKSUCD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Conto selezionato",;
    HelpContextID = 82612698,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=170, Top=8, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODICE"

  func oCODICE_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODICE_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODICE_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODICE_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco",'CONTIZOOM.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODICE_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODICE
     i_obj.ecpSave()
  endproc

  add object oCODESE_1_10 as StdField with uid="ZMIJKNZDWM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza dei movimenti da stampare",;
    HelpContextID = 66097626,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=78, Top=37, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oDATINI_1_14 as StdField with uid="VPJLNVRTYN",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � superiore alla finale o precedente all'inizio dell'esercizio",;
    ToolTipText = "Data di registrazione di inizio visualizzazione",;
    HelpContextID = 3907530,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=170, Top=37

  func oDATINI_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATINI<=.w_DATFIN OR (EMPTY(.w_DATFIN))) AND .w_DATINI>=.w_INIESE)
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_15 as StdField with uid="MCOUDCLKFB",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data di registrazione di fine visualizzazione",;
    HelpContextID = 193896394,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=272, Top=37

  func oDATFIN_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN OR EMPTY(.w_DATINI))
    endwith
    return bRes
  endfunc

  add object oFLAPER_1_16 as StdCheck with uid="FARHXMVDPY",rtseq=16,rtrep=.f.,left=6, top=407, caption="Considera solo movimenti dell'esercizio",;
    ToolTipText = "Se attivo considera solo movimenti del periodo comprese le aperure e chiusure",;
    HelpContextID = 130401450,;
    cFormVar="w_FLAPER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLAPER_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLAPER_1_16.GetRadio()
    this.Parent.oContained.w_FLAPER = this.RadioValue()
    return .t.
  endfunc

  func oFLAPER_1_16.SetRadio()
    this.Parent.oContained.w_FLAPER=trim(this.Parent.oContained.w_FLAPER)
    this.value = ;
      iif(this.Parent.oContained.w_FLAPER=='S',1,;
      0)
  endfunc

  func oFLAPER_1_16.mHide()
    with this.Parent.oContained
      return (Empty(.w_CODESE))
    endwith
  endfunc


  add object oFLPROV_1_17 as StdCombo with uid="VZJIVUBLRK",value=3,rtseq=17,rtrep=.f.,left=506,top=37,width=87,height=21;
    , ToolTipText = "Stato dei movimenti da visualizzare";
    , HelpContextID = 52614314;
    , cFormVar="w_FLPROV",RowSource=""+"Confermati,"+"Provvisori,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLPROV_1_17.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'',;
    ''))))
  endfunc
  func oFLPROV_1_17.GetRadio()
    this.Parent.oContained.w_FLPROV = this.RadioValue()
    return .t.
  endfunc

  func oFLPROV_1_17.SetRadio()
    this.Parent.oContained.w_FLPROV=trim(this.Parent.oContained.w_FLPROV)
    this.value = ;
      iif(this.Parent.oContained.w_FLPROV=='N',1,;
      iif(this.Parent.oContained.w_FLPROV=='S',2,;
      iif(this.Parent.oContained.w_FLPROV=='',3,;
      0)))
  endfunc


  add object oBtn_1_18 as StdButton with uid="ISKAWSSZTY",left=656, top=8, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 64915178;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        GSCG_BSV(this.Parent.oContained,"Esegui", "V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DATINI) AND NOT EMPTY(.w_DATFIN))
      endwith
    endif
  endfunc

  add object oFLAPER_1_19 as StdCheck with uid="SFDRKNVLEM",rtseq=18,rtrep=.f.,left=6, top=63, caption="Nei totali dare/avere considera anche mov. apertura/chiusura",;
    ToolTipText = "se attivato nei totali dare/avere verranno conteggiati anche i movimenti di apertura/chiusura (che quindi non verranno visualizzati in azzurro)",;
    HelpContextID = 130401450,;
    cFormVar="w_FLAPER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLAPER_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLAPER_1_19.GetRadio()
    this.Parent.oContained.w_FLAPER = this.RadioValue()
    return .t.
  endfunc

  func oFLAPER_1_19.SetRadio()
    this.Parent.oContained.w_FLAPER=trim(this.Parent.oContained.w_FLAPER)
    this.value = ;
      iif(this.Parent.oContained.w_FLAPER=='S',1,;
      0)
  endfunc

  func oFLAPER_1_19.mHide()
    with this.Parent.oContained
      return (Empty(.w_CODESE))
    endwith
  endfunc


  add object oBtn_1_20 as StdButton with uid="KURSIHYUAD",left=8, top=447, width=48,height=45,;
    CpPicture="BMP\SOLDIT.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare le partite associate alla riga di primanota";
    , HelpContextID = 211902710;
    , Caption='Par\<tite';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      do GSCG_SZ2 with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(NVL(.w_SERIALE,' ')) AND NOT EMPTY(.w_NUMROW))
      endwith
    endif
  endfunc


  add object oBtn_1_21 as StdButton with uid="CLVUFEEUEM",left=58, top=447, width=48,height=45,;
    CpPicture="BMP\CONTROPA.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza le contropartite del movimento selezionato";
    , HelpContextID = 130994604;
    , Caption='\<Controp.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      do GSCG_SZ1 with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_22 as StdButton with uid="OUEOXFACNW",left=108, top=447, width=48,height=45,;
    CpPicture="bmp\TESO.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza la registrazione di prima nota";
    , HelpContextID = 95818250;
    , Caption='\<Primanota';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_SERIALE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_COGE='S' AND NOT EMPTY(NVL(.w_SERIALE,' ')))
      endwith
    endif
  endfunc

  func oBtn_1_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_COGE<>'S')
     endwith
    endif
  endfunc


  add object oBtn_1_23 as StdButton with uid="ZVARLVJFTI",left=706, top=447, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 33915462;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oINIAVE_1_25 as StdField with uid="HATTWDFXYI",rtseq=19,rtrep=.f.,;
    cFormVar = "w_INIAVE", cQueryName = "INIAVE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo precedente alla data di inizio periodo",;
    HelpContextID = 63193722,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=626, Top=77, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oINIAVE_1_25.mHide()
    with this.Parent.oContained
      return (.w_INIAVE=0 or .w_FLAPER='S')
    endwith
  endfunc

  add object oPNDESSUP_1_26 as StdField with uid="XGUPNRKBHI",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PNDESSUP", cQueryName = "PNDESSUP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(45), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione aggiuntiva",;
    HelpContextID = 99652026,;
   bGlobalFont=.t.,;
    Height=21, Width=260, Left=139, Top=375, InputMask=replicate('X',45)

  add object oTOTDAR_1_28 as StdField with uid="SLBJBHJGPQ",rtseq=21,rtrep=.f.,;
    cFormVar = "w_TOTDAR", cQueryName = "TOTDAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale dare",;
    HelpContextID = 135303370,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=495, Top=375, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  add object oTOTAVE_1_29 as StdField with uid="ATRRCDXXLF",rtseq=22,rtrep=.f.,;
    cFormVar = "w_TOTAVE", cQueryName = "TOTAVE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale avere",;
    HelpContextID = 63148234,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=626, Top=375, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  add object oINIDAR_1_31 as StdField with uid="LBILQZZCBR",rtseq=23,rtrep=.f.,;
    cFormVar = "w_INIDAR", cQueryName = "INIDAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo precedente alla data di inizio periodo",;
    HelpContextID = 135348858,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=495, Top=77, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oINIDAR_1_31.mHide()
    with this.Parent.oContained
      return ((.w_INIDAR=0 AND .w_INIAVE<>0) or .w_FLAPER='S')
    endwith
  endfunc


  add object oObj_1_32 as cp_calclbl with uid="CAOEBXXQFR",left=325, top=423, width=167,height=14,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",alignment=1,;
    cEvent = "w_DATFIN Changed",;
    nPag=1;
    , HelpContextID = 63839770


  add object oObj_1_33 as cp_calclbl with uid="UCFGIJNNXN",left=336, top=83, width=156,height=14,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",alignment=1,;
    cEvent = "w_DATINI Changed",;
    nPag=1;
    , HelpContextID = 63839770


  add object oObj_1_34 as cp_runprogram with uid="XFPJPLUPVM",left=5, top=510, width=199,height=17,;
    caption='GSCG_BSV(Inizio  V)',;
   bGlobalFont=.t.,;
    prg='GSCG_BSV("Inizio", "V")',;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 182744668


  add object ZoomScad as cp_zoombox with uid="UDDJAIFUNC",left=1, top=100, width=751,height=250,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="PNT_DETT",cZoomFile="GSCG_SZM",bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bQueryOnDblClick=.f.,bRetriveAllRows=.f.,bAdvOptions=.t.,cZoomOnZoom="",cMenuFile="",bNoMenuHeaderProperty=.t.,;
    cEvent = "Calcola",;
    nPag=1;
    , HelpContextID = 63839770

  add object oFINAVE_1_43 as StdField with uid="ROZWSYSMMF",rtseq=28,rtrep=.f.,;
    cFormVar = "w_FINAVE", cQueryName = "FINAVE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo alla fine del periodo selezionato",;
    HelpContextID = 63174570,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=626, Top=423, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oFINAVE_1_43.mHide()
    with this.Parent.oContained
      return (.w_FINAVE=0)
    endwith
  endfunc

  add object oFINDAR_1_44 as StdField with uid="BFKENWOAHD",rtseq=29,rtrep=.f.,;
    cFormVar = "w_FINDAR", cQueryName = "FINDAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo alla fine del periodo selezionato",;
    HelpContextID = 135329706,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=495, Top=423, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oFINDAR_1_44.mHide()
    with this.Parent.oContained
      return (.w_FINDAR=0 AND .w_FINAVE<>0)
    endwith
  endfunc

  add object oDESCRI_1_46 as StdField with uid="YISYJVIDXY",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 109514,;
   bGlobalFont=.t.,;
    Height=21, Width=286, Left=308, Top=8, InputMask=replicate('X',40)


  add object oBtn_1_47 as StdButton with uid="XFXJCJDEUZ",left=157, top=447, width=48,height=45,;
    CpPicture="bmp\doc1.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento di origine";
    , HelpContextID = 27033142;
    , Caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_47.Click()
      with this.Parent.oContained
        do GSAR_BZD with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_47.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_MAGA='S')
      endwith
    endif
  endfunc


  add object oBtn_1_52 as StdButton with uid="DHFLNHBKMV",left=706, top=8, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Esegue la stampa della scheda con le selezioni impostate";
    , HelpContextID = 100047834;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_52.Click()
      with this.Parent.oContained
        GSCG_BSV(this.Parent.oContained,"Esegui", "S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_52.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_DATINI) AND NOT EMPTY(.w_DATFIN) AND NOT EMPTY(.w_CODICE))
      endwith
    endif
  endfunc


  add object COLORE as cp_calclbl with uid="WHVDTETLZN",left=4, top=353, width=34,height=17,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption=" ",;
    nPag=1;
    , HelpContextID = 63839770


  add object oObj_1_67 as cp_runprogram with uid="IBPGPEXIXO",left=5, top=529, width=199,height=17,;
    caption='GSCG_BSV(Fine V)',;
   bGlobalFont=.t.,;
    prg="GSCG_BSV('Fine', 'V')",;
    cEvent = "Edit Aborted",;
    nPag=1;
    , HelpContextID = 9845605

  add object oSALDOD_1_68 as StdField with uid="AAOJRHPSYS",rtseq=48,rtrep=.f.,;
    cFormVar = "w_SALDOD", cQueryName = "SALDOD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo movimenti visualizzati in dare",;
    HelpContextID = 87105242,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=495, Top=399, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oSALDOD_1_68.mHide()
    with this.Parent.oContained
      return (.w_TOTDAR-.w_TOTAVE<0)
    endwith
  endfunc

  add object oSALDOA_1_69 as StdField with uid="UOLAWPLNYX",rtseq=49,rtrep=.f.,;
    cFormVar = "w_SALDOA", cQueryName = "SALDOA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo movimenti visualizzati in avere",;
    HelpContextID = 137436890,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=626, Top=399, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oSALDOA_1_69.mHide()
    with this.Parent.oContained
      return (.w_TOTDAR-.w_TOTAVE>0)
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="VMDITUXAKY",Visible=.t., Left=5, Top=8,;
    Alignment=1, Width=71, Height=15,;
    Caption="Conto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="WMPAYGEVJZ",Visible=.t., Left=5, Top=40,;
    Alignment=1, Width=71, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="SIXLQFSAWO",Visible=.t., Left=402, Top=375,;
    Alignment=1, Width=91, Height=15,;
    Caption="Totali:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="ZUAYJAPCQJ",Visible=.t., Left=434, Top=38,;
    Alignment=1, Width=68, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="EHYDVQMOWB",Visible=.t., Left=134, Top=40,;
    Alignment=1, Width=34, Height=15,;
    Caption="Da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="ORMUKDZOZP",Visible=.t., Left=250, Top=40,;
    Alignment=1, Width=20, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="LAXTICQNIN",Visible=.t., Left=6, Top=375,;
    Alignment=1, Width=131, Height=18,;
    Caption="Desc.supplementare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="KDIHYMVUFU",Visible=.t., Left=329, Top=399,;
    Alignment=1, Width=165, Height=18,;
    Caption="Saldo movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_77 as StdString with uid="LUGNFBYLPF",Visible=.t., Left=44, Top=354,;
    Alignment=0, Width=689, Height=17,;
    Caption="= Reg. di competenza esterna al periodo selezionato o di chiusura o di apertura conti (tranne il primo esercizio)"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_szm','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
