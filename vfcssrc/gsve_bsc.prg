* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bsc                                                        *
*              Stampa incassi corrispettivi                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_49]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-12                                                      *
* Last revis.: 2008-01-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bsc",oParentObject)
return(i_retval)

define class tgsve_bsc as StdBatch
  * --- Local variables
  w_OK = .f.
  w_STAMPA1 = space(8)
  w_STAMPA = 0
  w_OREP = space(10)
  w_OQRY = space(10)
  * --- WorkFile variables
  OUT_PUTS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue la Stampa del Documento di Incasso Corrispettivi (da GSVE_MIC)
    this.w_OK = .T.
    this.w_STAMPA1 = "GSVE_MIC"
    this.w_STAMPA = 1
    this.w_OREP = " "
    this.w_OQRY = " "
    if EMPTY(this.oParentObject.w_INSERIAL)
      i_retcode = 'stop'
      return
    endif
    * --- Read from OUT_PUTS
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OUT_PUTS_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OUT_PUTS_idx,2],.t.,this.OUT_PUTS_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "OUNOMQUE,OUNOMREP"+;
        " from "+i_cTable+" OUT_PUTS where ";
            +"OUNOMPRG = "+cp_ToStrODBC(this.w_STAMPA1);
            +" and OUROWNUM = "+cp_ToStrODBC(this.w_STAMPA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        OUNOMQUE,OUNOMREP;
        from (i_cTable) where;
            OUNOMPRG = this.w_STAMPA1;
            and OUROWNUM = this.w_STAMPA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OQRY = NVL(cp_ToDate(_read_.OUNOMQUE),cp_NullValue(_read_.OUNOMQUE))
      this.w_OREP = NVL(cp_ToDate(_read_.OUNOMREP),cp_NullValue(_read_.OUNOMREP))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_OREP) OR EMPTY(this.w_OQRY)
      ah_ErrorMsg("Programma di stampa non definito")
      this.w_OK = .F.
    endif
    if this.w_OK = .T.
      * --- Cerca il bmp del logo con il nome del codice dell'azienda se non lo trova mette quello di default
      L_LOGO=GETBMP(I_CODAZI)
      * --- Cambio della Valuta in LIRE utilizzato in talune stampe
      l_cambio = GETCAM(g_PERVAL, I_DATSYS)
      ah_Msg("Stampa in corso...")
      vq_exec(alltrim(this.w_OQRY),this,"__TMP__")
      if used("__TMP__")
        UPDATE __TMP__ SET ;
        RAGSOC=SUBSTR(NVL(MV__NOTE, SPACE(140)), 1, 40), ;
        INDIRI=SUBSTR(NVL(MV__NOTE, SPACE(140)), 41, 35), ;
        CODCAP=SUBSTR(NVL(MV__NOTE, SPACE(140)), 76, 8), ;
        LOCALI=SUBSTR(NVL(MV__NOTE, SPACE(140)), 84, 30), ;
        PROVIN=SUBSTR(NVL(MV__NOTE, SPACE(140)), 114, 2), ;
        CODFIS=SUBSTR(NVL(MV__NOTE, SPACE(140)), 116, 16) WHERE NOT EMPTY(NVL(INSERIAL,""))
        select * FROM __TMP__ INTO CURSOR __TMP__ ORDER BY 1,7,8
        GO TOP
        CP_CHPRN(this.w_OREP, " ", this)
        WAIT CLEAR
      endif
      if used("__TMP__")
        select __TMP__
        use
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='OUT_PUTS'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
