* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_anm                                                        *
*              Nomenclature                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_13]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-13                                                      *
* Last revis.: 2009-11-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_anm"))

* --- Class definition
define class tgsar_anm as StdForm
  Top    = 18
  Left   = 21

  * --- Standard Properties
  Width  = 537
  Height = 130+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-11-18"
  HelpContextID=74877079
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  NOMENCLA_IDX = 0
  UNIMIS_IDX = 0
  cFile = "NOMENCLA"
  cKeySelect = "NMCODICE"
  cKeyWhere  = "NMCODICE=this.w_NMCODICE"
  cKeyWhereODBC = '"NMCODICE="+cp_ToStrODBC(this.w_NMCODICE)';

  cKeyWhereODBCqualified = '"NOMENCLA.NMCODICE="+cp_ToStrODBC(this.w_NMCODICE)';

  cPrg = "gsar_anm"
  cComment = "Nomenclature"
  icon = "anag.ico"
  cAutoZoom = 'GSAR0ANM'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_NMCODICE = space(8)
  w_NMDESCRI = space(51)
  w_NMUNISUP = space(3)
  w_DESUMI = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'NOMENCLA','gsar_anm')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_anmPag1","gsar_anm",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Nomenclatura")
      .Pages(1).HelpContextID = 254503287
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNMCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='UNIMIS'
    this.cWorkTables[2]='NOMENCLA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.NOMENCLA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.NOMENCLA_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_NMCODICE = NVL(NMCODICE,space(8))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_3_joined
    link_1_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from NOMENCLA where NMCODICE=KeySet.NMCODICE
    *
    i_nConn = i_TableProp[this.NOMENCLA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('NOMENCLA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "NOMENCLA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' NOMENCLA '
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'NMCODICE',this.w_NMCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESUMI = space(35)
        .w_NMCODICE = NVL(NMCODICE,space(8))
        .w_NMDESCRI = NVL(NMDESCRI,space(51))
        .w_NMUNISUP = NVL(NMUNISUP,space(3))
          if link_1_3_joined
            this.w_NMUNISUP = NVL(UMCODICE103,NVL(this.w_NMUNISUP,space(3)))
            this.w_DESUMI = NVL(UMDESCRI103,space(35))
          else
          .link_1_3('Load')
          endif
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(ah_MsgFormat("Se l'unit� di misura supplementare � vuota o uguale a 'ZZZ' si considera che essa%0non sia obbligatoria nella generazione degli elenchi INTRA"))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        cp_LoadRecExtFlds(this,'NOMENCLA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NMCODICE = space(8)
      .w_NMDESCRI = space(51)
      .w_NMUNISUP = space(3)
      .w_DESUMI = space(35)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
          if not(empty(.w_NMUNISUP))
          .link_1_3('Full')
          endif
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(ah_MsgFormat("Se l'unit� di misura supplementare � vuota o uguale a 'ZZZ' si considera che essa%0non sia obbligatoria nella generazione degli elenchi INTRA"))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'NOMENCLA')
    this.DoRTCalc(4,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oNMCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oNMDESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oNMUNISUP_1_3.enabled = i_bVal
      .Page1.oPag.oObj_1_7.enabled = i_bVal
      .Page1.oPag.oObj_1_8.enabled = i_bVal
      .Page1.oPag.oObj_1_9.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oNMCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oNMCODICE_1_1.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'NOMENCLA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.NOMENCLA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NMCODICE,"NMCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NMDESCRI,"NMDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NMUNISUP,"NMUNISUP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.NOMENCLA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])
    i_lTable = "NOMENCLA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.NOMENCLA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SNM with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.NOMENCLA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.NOMENCLA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into NOMENCLA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'NOMENCLA')
        i_extval=cp_InsertValODBCExtFlds(this,'NOMENCLA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(NMCODICE,NMDESCRI,NMUNISUP "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_NMCODICE)+;
                  ","+cp_ToStrODBC(this.w_NMDESCRI)+;
                  ","+cp_ToStrODBCNull(this.w_NMUNISUP)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'NOMENCLA')
        i_extval=cp_InsertValVFPExtFlds(this,'NOMENCLA')
        cp_CheckDeletedKey(i_cTable,0,'NMCODICE',this.w_NMCODICE)
        INSERT INTO (i_cTable);
              (NMCODICE,NMDESCRI,NMUNISUP  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_NMCODICE;
                  ,this.w_NMDESCRI;
                  ,this.w_NMUNISUP;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.NOMENCLA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.NOMENCLA_IDX,i_nConn)
      *
      * update NOMENCLA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'NOMENCLA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " NMDESCRI="+cp_ToStrODBC(this.w_NMDESCRI)+;
             ",NMUNISUP="+cp_ToStrODBCNull(this.w_NMUNISUP)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'NOMENCLA')
        i_cWhere = cp_PKFox(i_cTable  ,'NMCODICE',this.w_NMCODICE  )
        UPDATE (i_cTable) SET;
              NMDESCRI=this.w_NMDESCRI;
             ,NMUNISUP=this.w_NMUNISUP;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.NOMENCLA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.NOMENCLA_IDX,i_nConn)
      *
      * delete NOMENCLA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'NMCODICE',this.w_NMCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.NOMENCLA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.NOMENCLA_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(ah_MsgFormat("Se l'unit� di misura supplementare � vuota o uguale a 'ZZZ' si considera che essa%0non sia obbligatoria nella generazione degli elenchi INTRA"))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_7.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_8.Calculate(ah_MsgFormat("Se l'unit� di misura supplementare � vuota o uguale a 'ZZZ' si considera che essa%0non sia obbligatoria nella generazione degli elenchi INTRA"))
        .oPgFrm.Page1.oPag.oObj_1_9.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_7.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_9.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=NMUNISUP
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NMUNISUP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_NMUNISUP)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_NMUNISUP))
          select UMCODICE,UMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NMUNISUP)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NMUNISUP) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oNMUNISUP_1_3'),i_cWhere,'GSAR_AUM',"Unita di misura",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NMUNISUP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_NMUNISUP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_NMUNISUP)
            select UMCODICE,UMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NMUNISUP = NVL(_Link_.UMCODICE,space(3))
      this.w_DESUMI = NVL(_Link_.UMDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_NMUNISUP = space(3)
      endif
      this.w_DESUMI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NMUNISUP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.UMCODICE as UMCODICE103"+ ",link_1_3.UMDESCRI as UMDESCRI103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on NOMENCLA.NMUNISUP=link_1_3.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and NOMENCLA.NMUNISUP=link_1_3.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNMCODICE_1_1.value==this.w_NMCODICE)
      this.oPgFrm.Page1.oPag.oNMCODICE_1_1.value=this.w_NMCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oNMDESCRI_1_2.value==this.w_NMDESCRI)
      this.oPgFrm.Page1.oPag.oNMDESCRI_1_2.value=this.w_NMDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oNMUNISUP_1_3.value==this.w_NMUNISUP)
      this.oPgFrm.Page1.oPag.oNMUNISUP_1_3.value=this.w_NMUNISUP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESUMI_1_5.value==this.w_DESUMI)
      this.oPgFrm.Page1.oPag.oDESUMI_1_5.value=this.w_DESUMI
    endif
    cp_SetControlsValueExtFlds(this,'NOMENCLA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_NMCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNMCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_NMCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_anmPag1 as StdContainer
  Width  = 533
  height = 130
  stdWidth  = 533
  stdheight = 130
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNMCODICE_1_1 as StdField with uid="PQTDYCWLHY",rtseq=1,rtrep=.f.,;
    cFormVar = "w_NMCODICE", cQueryName = "NMCODICE",;
    bObbl = .t. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice identificante la nomenclatura dell'articolo",;
    HelpContextID = 234222821,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=78, Top=21, InputMask=replicate('X',8)

  add object oNMDESCRI_1_2 as StdField with uid="AKICEKQMHZ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_NMDESCRI", cQueryName = "NMDESCRI",;
    bObbl = .f. , nPag = 1, value=space(51), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della nomenclatura",;
    HelpContextID = 51373281,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=166, Top=21, InputMask=replicate('X',51)

  add object oNMUNISUP_1_3 as StdField with uid="PUVDICINQS",rtseq=3,rtrep=.f.,;
    cFormVar = "w_NMUNISUP", cQueryName = "NMUNISUP",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Unit� di misura supplementare",;
    HelpContextID = 61199578,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=78, Top=53, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_NMUNISUP"

  func oNMUNISUP_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oNMUNISUP_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNMUNISUP_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oNMUNISUP_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"Unita di misura",'',this.parent.oContained
  endproc
  proc oNMUNISUP_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_NMUNISUP
     i_obj.ecpSave()
  endproc

  add object oDESUMI_1_5 as StdField with uid="YUXFJPZSYW",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESUMI", cQueryName = "DESUMI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 224329162,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=166, Top=53, InputMask=replicate('X',35)


  add object oObj_1_7 as cp_runprogram with uid="AQMIYBKYXQ",left=-4, top=148, width=235,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_BNM('DEL')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 15560730


  add object oObj_1_8 as cp_calclbl with uid="FOXUMVTJGV",left=5, top=81, width=524,height=47,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 15560730


  add object oObj_1_9 as cp_runprogram with uid="DCVGAQYGPJ",left=-4, top=177, width=235,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSAR_BNM('UPD')",;
    cEvent = "Update end",;
    nPag=1;
    , HelpContextID = 15560730

  add object oStr_1_4 as StdString with uid="GSRDYFDFBQ",Visible=.t., Left=34, Top=21,;
    Alignment=1, Width=42, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="SPCOFMPITN",Visible=.t., Left=3, Top=53,;
    Alignment=1, Width=73, Height=15,;
    Caption="U.M. suppl.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_anm','NOMENCLA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".NMCODICE=NOMENCLA.NMCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
