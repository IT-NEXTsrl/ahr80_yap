* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bdv                                                        *
*              Elimina i codici di ricerca                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_9]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-15                                                      *
* Last revis.: 2016-07-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bdv",oParentObject)
return(i_retval)

define class tgsma_bdv as StdBatch
  * --- Local variables
  w_APPO = space(20)
  w_MESS = space(10)
  w_CODAZI = space(5)
  * --- WorkFile variables
  KEY_ARTI_idx=0
  TRADARTI_idx=0
  CONTROPA_idx=0
  PAR_RIOR_idx=0
  PAR_RIMA_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina le Chiavi di ricerca associate all'Articolo in caso di Cancellazione (Lanciato da GSMA_AAR, GSMA_AAS)
    this.w_CODAZI = i_CODAZI
    if this.oParentObject.cFunction="Query"
      * --- Controlla che l'articolo usato nei parametri vendite nel campo articoli per riferimenti
      this.w_MESS = "Impossibile cancellare, articolo presente come articolo per riferimenti nei parametri vendita"
      * --- Read from CONTROPA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTROPA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" CONTROPA where ";
              +"COCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
              +" and COARTDES = "+cp_ToStrODBC(this.oParentObject.w_ARCODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              COCODAZI = this.w_CODAZI;
              and COARTDES = this.oParentObject.w_ARCODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows>0
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=ah_MsgFormat(this.w_MESS)
        i_retcode = 'stop'
        return
      endif
      * --- Controlla che l articolo non sia usato nell anagrafica saldi lotti
      if g_MADV="S"
        this.w_MESS = "Articolo usato nell'anagrafica saldi/lotti%0Impossibile eliminare"
        * --- Select from gsar2bks
        do vq_exec with 'gsar2bks',this,'_Curs_gsar2bks','',.f.,.t.
        if used('_Curs_gsar2bks')
          select _Curs_gsar2bks
          locate for 1=1
          do while not(eof())
          if CONTA>0
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=ah_MsgFormat(this.w_MESS)
            i_retcode = 'stop'
            return
          endif
            select _Curs_gsar2bks
            continue
          enddo
          use
        endif
      endif
      if g_DISB="S"
        this.w_MESS = "Articolo usato nell'anagrafica distinta base%0Impossibile eliminare"
        * --- Select from gsma_bdv
        do vq_exec with 'gsma_bdv',this,'_Curs_gsma_bdv','',.f.,.t.
        if used('_Curs_gsma_bdv')
          select _Curs_gsma_bdv
          locate for 1=1
          do while not(eof())
          if NUMDIS>0
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=ah_MsgFormat(this.w_MESS)
            i_retcode = 'stop'
            return
          endif
            select _Curs_gsma_bdv
            continue
          enddo
          use
        endif
      endif
      * --- Se sono in Cancellazione, Elimina tutti i Codici di Ricerca
      Ah_Msg("Elimina codici di ricerca associati...")
      * --- Select from KEY_ARTI
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CACODICE  from "+i_cTable+" KEY_ARTI ";
            +" where CACODART="+cp_ToStrODBC(this.oParentObject.w_ARCODART)+"";
             ,"_Curs_KEY_ARTI")
      else
        select CACODICE from (i_cTable);
         where CACODART=this.oParentObject.w_ARCODART;
          into cursor _Curs_KEY_ARTI
      endif
      if used('_Curs_KEY_ARTI')
        select _Curs_KEY_ARTI
        locate for 1=1
        do while not(eof())
        this.w_APPO = _Curs_KEY_ARTI.CACODICE
        * --- Ricerca Eventuali Traduzioni
        * --- Delete from TRADARTI
        i_nConn=i_TableProp[this.TRADARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TRADARTI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"LGCODICE = "+cp_ToStrODBC(this.w_APPO);
                 )
        else
          delete from (i_cTable) where;
                LGCODICE = this.w_APPO;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
          select _Curs_KEY_ARTI
          continue
        enddo
        use
      endif
      * --- Delete from KEY_ARTI
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"CACODART = "+cp_ToStrODBC(this.oParentObject.w_ARCODART);
               )
      else
        delete from (i_cTable) where;
              CACODART = this.oParentObject.w_ARCODART;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from PAR_RIMA
      i_nConn=i_TableProp[this.PAR_RIMA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIMA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PRCODART = "+cp_ToStrODBC(this.oParentObject.w_ARCODART);
               )
      else
        delete from (i_cTable) where;
              PRCODART = this.oParentObject.w_ARCODART;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      * --- Delete from PAR_RIOR
      i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"PRCODART = "+cp_ToStrODBC(this.oParentObject.w_ARCODART);
               )
      else
        delete from (i_cTable) where;
              PRCODART = this.oParentObject.w_ARCODART;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
      WAIT CLEAR
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='TRADARTI'
    this.cWorkTables[3]='CONTROPA'
    this.cWorkTables[4]='PAR_RIOR'
    this.cWorkTables[5]='PAR_RIMA'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_gsar2bks')
      use in _Curs_gsar2bks
    endif
    if used('_Curs_gsma_bdv')
      use in _Curs_gsma_bdv
    endif
    if used('_Curs_KEY_ARTI')
      use in _Curs_KEY_ARTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
