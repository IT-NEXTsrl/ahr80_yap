* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_sis                                                        *
*              Stampa partite/scadenze                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_180]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-02                                                      *
* Last revis.: 2012-09-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_sis",oParentObject))

* --- Class definition
define class tgste_sis as StdForm
  Top    = 15
  Left   = 11

  * --- Standard Properties
  Width  = 621
  Height = 327+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-09-21"
  HelpContextID=258618473
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=52

  * --- Constant Properties
  _IDX = 0
  VALUTE_IDX = 0
  CONTI_IDX = 0
  BAN_CHE_IDX = 0
  COC_MAST_IDX = 0
  AGENTI_IDX = 0
  ZONE_IDX = 0
  CATECOMM_IDX = 0
  CACOCLFO_IDX = 0
  cPrg = "gste_sis"
  cComment = "Stampa partite/scadenze"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZI = space(5)
  w_SCAINI = ctod('  /  /  ')
  o_SCAINI = ctod('  /  /  ')
  w_SCAFIN = ctod('  /  /  ')
  w_REGINI = ctod('  /  /  ')
  w_REGFIN = ctod('  /  /  ')
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_CODCON = space(15)
  w_PAGRD = space(2)
  w_PAGBO = space(2)
  w_PAGRB = space(2)
  w_PAGRI = space(2)
  w_PAGCA = space(2)
  w_PAGMA = space(2)
  w_FLORIG = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_FLSOSP = space(1)
  w_FLSALD = space(1)
  o_FLSALD = space(1)
  w_FLPART = space(1)
  o_FLPART = space(1)
  w_DATPAR = ctod('  /  /  ')
  w_FLPART1 = space(1)
  w_FLGSOS = space(1)
  w_FLGNSO = space(1)
  w_ONUME = 0
  w_DATSTA = ctod('  /  /  ')
  w_DESCON = space(40)
  w_NUMPAR = space(31)
  w_BANAPP = space(10)
  w_DESBAN = space(50)
  w_BANNOS = space(15)
  w_DESNOS = space(35)
  w_NDOINI = 0
  w_ADOINI = space(10)
  w_DDOINI = ctod('  /  /  ')
  w_NDOFIN = 0
  w_ADOFIN = space(10)
  w_DDOFIN = ctod('  /  /  ')
  w_CATCOM = space(3)
  w_VALUTA = space(3)
  w_DESVAL = space(35)
  w_AGENTE = space(5)
  w_CODZON = space(3)
  w_DESAGE = space(35)
  w_AGEOBSO = ctod('  /  /  ')
  w_NEWPAG = space(1)
  w_PARTSN = space(1)
  w_DESZON = space(35)
  w_DESCOM = space(35)
  w_ACODCO = space(15)
  w_ADESCON = space(40)
  w_CATCON = space(3)
  w_DESCOT = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_sisPag1","gste_sis",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgste_sisPag2","gste_sis",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSCAINI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[8]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='BAN_CHE'
    this.cWorkTables[4]='COC_MAST'
    this.cWorkTables[5]='AGENTI'
    this.cWorkTables[6]='ZONE'
    this.cWorkTables[7]='CATECOMM'
    this.cWorkTables[8]='CACOCLFO'
    return(this.OpenAllTables(8))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSTE_BPS with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gste_sis
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZI=space(5)
      .w_SCAINI=ctod("  /  /  ")
      .w_SCAFIN=ctod("  /  /  ")
      .w_REGINI=ctod("  /  /  ")
      .w_REGFIN=ctod("  /  /  ")
      .w_TIPCON=space(1)
      .w_CODCON=space(15)
      .w_PAGRD=space(2)
      .w_PAGBO=space(2)
      .w_PAGRB=space(2)
      .w_PAGRI=space(2)
      .w_PAGCA=space(2)
      .w_PAGMA=space(2)
      .w_FLORIG=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_FLSOSP=space(1)
      .w_FLSALD=space(1)
      .w_FLPART=space(1)
      .w_DATPAR=ctod("  /  /  ")
      .w_FLPART1=space(1)
      .w_FLGSOS=space(1)
      .w_FLGNSO=space(1)
      .w_ONUME=0
      .w_DATSTA=ctod("  /  /  ")
      .w_DESCON=space(40)
      .w_NUMPAR=space(31)
      .w_BANAPP=space(10)
      .w_DESBAN=space(50)
      .w_BANNOS=space(15)
      .w_DESNOS=space(35)
      .w_NDOINI=0
      .w_ADOINI=space(10)
      .w_DDOINI=ctod("  /  /  ")
      .w_NDOFIN=0
      .w_ADOFIN=space(10)
      .w_DDOFIN=ctod("  /  /  ")
      .w_CATCOM=space(3)
      .w_VALUTA=space(3)
      .w_DESVAL=space(35)
      .w_AGENTE=space(5)
      .w_CODZON=space(3)
      .w_DESAGE=space(35)
      .w_AGEOBSO=ctod("  /  /  ")
      .w_NEWPAG=space(1)
      .w_PARTSN=space(1)
      .w_DESZON=space(35)
      .w_DESCOM=space(35)
      .w_ACODCO=space(15)
      .w_ADESCON=space(40)
      .w_CATCON=space(3)
      .w_DESCOT=space(35)
        .w_CODAZI = i_CODAZI
        .w_SCAINI = i_datsys
        .w_SCAFIN = i_datsys+365
          .DoRTCalc(4,5,.f.)
        .w_TIPCON = 'C'
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CODCON))
          .link_1_7('Full')
        endif
        .w_PAGRD = 'RD'
        .w_PAGBO = 'BO'
        .w_PAGRB = 'RB'
        .w_PAGRI = 'RI'
        .w_PAGCA = 'CA'
        .w_PAGMA = 'MA'
        .w_FLORIG = ' '
        .w_OBTEST = iif(empty(.w_SCAINI),i_INIDAT,.w_SCAINI)
          .DoRTCalc(16,16,.f.)
        .w_FLSOSP = 'T'
        .w_FLSALD = 'R'
        .w_FLPART = 'T'
        .w_DATPAR = cp_CharToDate('  -  -  ')
        .w_FLPART1 = IIF(.w_FLPART='T', ' ', .w_FLPART)
        .w_FLGSOS = IIF(.w_FLSOSP $ 'TN', ' ', 'S')
        .w_FLGNSO = IIF(.w_FLSOSP $ 'TS', ' ', 'S')
      .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
          .DoRTCalc(24,24,.f.)
        .w_DATSTA = i_DATSYS
        .DoRTCalc(26,28,.f.)
        if not(empty(.w_BANAPP))
          .link_2_2('Full')
        endif
        .DoRTCalc(29,30,.f.)
        if not(empty(.w_BANNOS))
          .link_2_4('Full')
        endif
        .DoRTCalc(31,38,.f.)
        if not(empty(.w_CATCOM))
          .link_2_12('Full')
        endif
        .DoRTCalc(39,39,.f.)
        if not(empty(.w_VALUTA))
          .link_2_13('Full')
        endif
          .DoRTCalc(40,40,.f.)
        .w_AGENTE = SPACE(5)
        .DoRTCalc(41,41,.f.)
        if not(empty(.w_AGENTE))
          .link_2_15('Full')
        endif
        .DoRTCalc(42,42,.f.)
        if not(empty(.w_CODZON))
          .link_2_16('Full')
        endif
          .DoRTCalc(43,44,.f.)
        .w_NEWPAG = ' '
        .DoRTCalc(46,49,.f.)
        if not(empty(.w_ACODCO))
          .link_1_47('Full')
        endif
        .DoRTCalc(50,51,.f.)
        if not(empty(.w_CATCON))
          .link_2_34('Full')
        endif
    endwith
    this.DoRTCalc(52,52,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,14,.t.)
        if .o_SCAINI<>.w_SCAINI
            .w_OBTEST = iif(empty(.w_SCAINI),i_INIDAT,.w_SCAINI)
        endif
        .DoRTCalc(16,19,.t.)
        if .o_FLSALD<>.w_FLSALD
            .w_DATPAR = cp_CharToDate('  -  -  ')
        endif
        if .o_FLPART<>.w_FLPART
            .w_FLPART1 = IIF(.w_FLPART='T', ' ', .w_FLPART)
        endif
            .w_FLGSOS = IIF(.w_FLSOSP $ 'TN', ' ', 'S')
            .w_FLGNSO = IIF(.w_FLSOSP $ 'TS', ' ', 'S')
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .DoRTCalc(24,40,.t.)
        if .o_TIPCON<>.w_TIPCON
            .w_AGENTE = SPACE(5)
          .link_2_15('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(42,52,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTIPCON_1_6.enabled = this.oPgFrm.Page1.oPag.oTIPCON_1_6.mCond()
    this.oPgFrm.Page2.oPag.oAGENTE_2_15.enabled = this.oPgFrm.Page2.oPag.oAGENTE_2_15.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLPART_1_22.visible=!this.oPgFrm.Page1.oPag.oFLPART_1_22.mHide()
    this.oPgFrm.Page1.oPag.oDATPAR_1_23.visible=!this.oPgFrm.Page1.oPag.oDATPAR_1_23.mHide()
    this.oPgFrm.Page1.oPag.oDATSTA_1_34.visible=!this.oPgFrm.Page1.oPag.oDATSTA_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_39.visible=!this.oPgFrm.Page1.oPag.oStr_1_39.mHide()
    this.oPgFrm.Page2.oPag.oCATCOM_2_12.visible=!this.oPgFrm.Page2.oPag.oCATCOM_2_12.mHide()
    this.oPgFrm.Page2.oPag.oAGENTE_2_15.visible=!this.oPgFrm.Page2.oPag.oAGENTE_2_15.mHide()
    this.oPgFrm.Page2.oPag.oCODZON_2_16.visible=!this.oPgFrm.Page2.oPag.oCODZON_2_16.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_27.visible=!this.oPgFrm.Page2.oPag.oStr_2_27.mHide()
    this.oPgFrm.Page2.oPag.oDESAGE_2_28.visible=!this.oPgFrm.Page2.oPag.oDESAGE_2_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page2.oPag.oDESZON_2_30.visible=!this.oPgFrm.Page2.oPag.oDESZON_2_30.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_31.visible=!this.oPgFrm.Page2.oPag.oStr_2_31.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_32.visible=!this.oPgFrm.Page2.oPag.oStr_2_32.mHide()
    this.oPgFrm.Page2.oPag.oDESCOM_2_33.visible=!this.oPgFrm.Page2.oPag.oDESCOM_2_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_45.visible=!this.oPgFrm.Page1.oPag.oStr_1_45.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page2.oPag.oCATCON_2_34.visible=!this.oPgFrm.Page2.oPag.oCATCON_2_34.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_35.visible=!this.oPgFrm.Page2.oPag.oStr_2_35.mHide()
    this.oPgFrm.Page2.oPag.oDESCOT_2_36.visible=!this.oPgFrm.Page2.oPag.oDESCOT_2_36.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCON
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_7'),i_cWhere,'GSAR_BZC',"Clienti/fornitori/conti",'GSTE_KMS.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore inesistente, non gestito a partite o obsoleto, oppure il codice iniziale � pi� grande del codice finale ")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_PARTSN = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_PARTSN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) And .w_PARTSN='S') AND (empty(.w_ACODCO) OR .w_CODCON<=.w_ACODCO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore inesistente, non gestito a partite o obsoleto, oppure il codice iniziale � pi� grande del codice finale ")
        endif
        this.w_CODCON = space(15)
        this.w_DESCON = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_PARTSN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BANAPP
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BANAPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABA',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_BANAPP)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_BANAPP))
          select BACODBAN,BADESBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BANAPP)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESBAN like "+cp_ToStrODBC(trim(this.w_BANAPP)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESBAN like "+cp_ToStr(trim(this.w_BANAPP)+"%");

            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_BANAPP) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oBANAPP_2_2'),i_cWhere,'GSAR_ABA',"Elenco banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BANAPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_BANAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_BANAPP)
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BANAPP = NVL(_Link_.BACODBAN,space(10))
      this.w_DESBAN = NVL(_Link_.BADESBAN,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_BANAPP = space(10)
      endif
      this.w_DESBAN = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BANAPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BANNOS
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BANNOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_BANNOS)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_BANNOS))
          select BACODBAN,BADESCRI,BADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BANNOS)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_BANNOS)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_BANNOS)+"%");

            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_BANNOS) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oBANNOS_2_4'),i_cWhere,'GSTE_ACB',"Conti banca",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BANNOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_BANNOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_BANNOS)
            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BANNOS = NVL(_Link_.BACODBAN,space(15))
      this.w_DESNOS = NVL(_Link_.BADESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_BANNOS = space(15)
      endif
      this.w_DESNOS = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente oppure obsoleto")
        endif
        this.w_BANNOS = space(15)
        this.w_DESNOS = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BANNOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCOM
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CATCOM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCATCOM_2_12'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CATCOM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCOM = NVL(_Link_.CTCODICE,space(3))
      this.w_DESCOM = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCOM = space(3)
      endif
      this.w_DESCOM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_VALUTA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_VALUTA))
          select VACODVAL,VADESVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VALUTA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_VALUTA)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_VALUTA)+"%");

            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_VALUTA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oVALUTA_2_13'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DESVAL = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AGENTE
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AGENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_AGENTE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_AGENTE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AGENTE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_AGENTE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_AGENTE)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AGENTE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oAGENTE_2_15'),i_cWhere,'GSAR_AGE',"Elenco agenti",'AGE1ZOOM.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AGENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_AGENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_AGENTE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AGENTE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_AGEOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AGENTE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_AGEOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AGENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODZON
  func Link_2_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_CODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_CODZON))
          select ZOCODZON,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oCODZON_2_16'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_CODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_CODZON)
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESZON = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODZON = space(3)
      endif
      this.w_DESZON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ACODCO
  func Link_1_47(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ACODCO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_ACODCO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_ACODCO))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ACODCO)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_ACODCO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_ACODCO)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ACODCO) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oACODCO_1_47'),i_cWhere,'GSAR_BZC',"Clienti/fornitori/conti",'GSTE_KMS.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore inesistente, non gestito a partite o obsoleto, oppure il codice iniziale � pi� grande del codice finale ")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ACODCO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_ACODCO);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_ACODCO)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ACODCO = NVL(_Link_.ANCODICE,space(15))
      this.w_ADESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_PARTSN = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ACODCO = space(15)
      endif
      this.w_ADESCON = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_PARTSN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) And .w_PARTSN='S') AND (.w_ACODCO>=.w_CODCON or empty(.w_CODCON))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore inesistente, non gestito a partite o obsoleto, oppure il codice iniziale � pi� grande del codice finale ")
        endif
        this.w_ACODCO = space(15)
        this.w_ADESCON = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_PARTSN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ACODCO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCON
  func Link_2_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_CATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_CATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oCATCON_2_34'),i_cWhere,'GSAR_AC2',"Categorie contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_CATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_CATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCON = NVL(_Link_.C2CODICE,space(3))
      this.w_DESCOT = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCON = space(3)
      endif
      this.w_DESCOT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSCAINI_1_2.value==this.w_SCAINI)
      this.oPgFrm.Page1.oPag.oSCAINI_1_2.value=this.w_SCAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCAFIN_1_3.value==this.w_SCAFIN)
      this.oPgFrm.Page1.oPag.oSCAFIN_1_3.value=this.w_SCAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oREGINI_1_4.value==this.w_REGINI)
      this.oPgFrm.Page1.oPag.oREGINI_1_4.value=this.w_REGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oREGFIN_1_5.value==this.w_REGFIN)
      this.oPgFrm.Page1.oPag.oREGFIN_1_5.value=this.w_REGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_6.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_7.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_7.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRD_1_8.RadioValue()==this.w_PAGRD)
      this.oPgFrm.Page1.oPag.oPAGRD_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGBO_1_9.RadioValue()==this.w_PAGBO)
      this.oPgFrm.Page1.oPag.oPAGBO_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRB_1_10.RadioValue()==this.w_PAGRB)
      this.oPgFrm.Page1.oPag.oPAGRB_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRI_1_11.RadioValue()==this.w_PAGRI)
      this.oPgFrm.Page1.oPag.oPAGRI_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGCA_1_12.RadioValue()==this.w_PAGCA)
      this.oPgFrm.Page1.oPag.oPAGCA_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGMA_1_13.RadioValue()==this.w_PAGMA)
      this.oPgFrm.Page1.oPag.oPAGMA_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLORIG_1_14.RadioValue()==this.w_FLORIG)
      this.oPgFrm.Page1.oPag.oFLORIG_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSOSP_1_20.RadioValue()==this.w_FLSOSP)
      this.oPgFrm.Page1.oPag.oFLSOSP_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSALD_1_21.RadioValue()==this.w_FLSALD)
      this.oPgFrm.Page1.oPag.oFLSALD_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPART_1_22.RadioValue()==this.w_FLPART)
      this.oPgFrm.Page1.oPag.oFLPART_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATPAR_1_23.value==this.w_DATPAR)
      this.oPgFrm.Page1.oPag.oDATPAR_1_23.value=this.w_DATPAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSTA_1_34.value==this.w_DATSTA)
      this.oPgFrm.Page1.oPag.oDATSTA_1_34.value=this.w_DATSTA
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_35.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_35.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMPAR_2_1.value==this.w_NUMPAR)
      this.oPgFrm.Page2.oPag.oNUMPAR_2_1.value=this.w_NUMPAR
    endif
    if not(this.oPgFrm.Page2.oPag.oBANAPP_2_2.value==this.w_BANAPP)
      this.oPgFrm.Page2.oPag.oBANAPP_2_2.value=this.w_BANAPP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESBAN_2_3.value==this.w_DESBAN)
      this.oPgFrm.Page2.oPag.oDESBAN_2_3.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page2.oPag.oBANNOS_2_4.value==this.w_BANNOS)
      this.oPgFrm.Page2.oPag.oBANNOS_2_4.value=this.w_BANNOS
    endif
    if not(this.oPgFrm.Page2.oPag.oDESNOS_2_5.value==this.w_DESNOS)
      this.oPgFrm.Page2.oPag.oDESNOS_2_5.value=this.w_DESNOS
    endif
    if not(this.oPgFrm.Page2.oPag.oNDOINI_2_6.value==this.w_NDOINI)
      this.oPgFrm.Page2.oPag.oNDOINI_2_6.value=this.w_NDOINI
    endif
    if not(this.oPgFrm.Page2.oPag.oADOINI_2_7.value==this.w_ADOINI)
      this.oPgFrm.Page2.oPag.oADOINI_2_7.value=this.w_ADOINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDDOINI_2_8.value==this.w_DDOINI)
      this.oPgFrm.Page2.oPag.oDDOINI_2_8.value=this.w_DDOINI
    endif
    if not(this.oPgFrm.Page2.oPag.oNDOFIN_2_9.value==this.w_NDOFIN)
      this.oPgFrm.Page2.oPag.oNDOFIN_2_9.value=this.w_NDOFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oADOFIN_2_10.value==this.w_ADOFIN)
      this.oPgFrm.Page2.oPag.oADOFIN_2_10.value=this.w_ADOFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDDOFIN_2_11.value==this.w_DDOFIN)
      this.oPgFrm.Page2.oPag.oDDOFIN_2_11.value=this.w_DDOFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCATCOM_2_12.value==this.w_CATCOM)
      this.oPgFrm.Page2.oPag.oCATCOM_2_12.value=this.w_CATCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oVALUTA_2_13.value==this.w_VALUTA)
      this.oPgFrm.Page2.oPag.oVALUTA_2_13.value=this.w_VALUTA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVAL_2_14.value==this.w_DESVAL)
      this.oPgFrm.Page2.oPag.oDESVAL_2_14.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oAGENTE_2_15.value==this.w_AGENTE)
      this.oPgFrm.Page2.oPag.oAGENTE_2_15.value=this.w_AGENTE
    endif
    if not(this.oPgFrm.Page2.oPag.oCODZON_2_16.value==this.w_CODZON)
      this.oPgFrm.Page2.oPag.oCODZON_2_16.value=this.w_CODZON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE_2_28.value==this.w_DESAGE)
      this.oPgFrm.Page2.oPag.oDESAGE_2_28.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oNEWPAG_1_42.RadioValue()==this.w_NEWPAG)
      this.oPgFrm.Page1.oPag.oNEWPAG_1_42.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oDESZON_2_30.value==this.w_DESZON)
      this.oPgFrm.Page2.oPag.oDESZON_2_30.value=this.w_DESZON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOM_2_33.value==this.w_DESCOM)
      this.oPgFrm.Page2.oPag.oDESCOM_2_33.value=this.w_DESCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oACODCO_1_47.value==this.w_ACODCO)
      this.oPgFrm.Page1.oPag.oACODCO_1_47.value=this.w_ACODCO
    endif
    if not(this.oPgFrm.Page1.oPag.oADESCON_1_48.value==this.w_ADESCON)
      this.oPgFrm.Page1.oPag.oADESCON_1_48.value=this.w_ADESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oCATCON_2_34.value==this.w_CATCON)
      this.oPgFrm.Page2.oPag.oCATCON_2_34.value=this.w_CATCON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCOT_2_36.value==this.w_DESCOT)
      this.oPgFrm.Page2.oPag.oDESCOT_2_36.value=this.w_DESCOT
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_scaini<=.w_scafin) or (empty(.w_scafin)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCAINI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La scadenza iniziale � maggiore della scadenza finale")
          case   not((.w_scaini<=.w_scafin) or (empty(.w_scaini)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCAFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La scadenza iniziale � maggiore della scadenza finale")
          case   not((.w_REGINI<=.w_REGFIN) or (empty(.w_REGFIN)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oREGINI_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data registrazione iniziale � maggiore della scadenza finale")
          case   not((.w_regini<=.w_regfin) or (empty(.w_regini)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oREGFIN_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data registrazione iniziale � maggiore della scadenza finale")
          case   not(((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) And .w_PARTSN='S') AND (empty(.w_ACODCO) OR .w_CODCON<=.w_ACODCO))  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCON_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore inesistente, non gestito a partite o obsoleto, oppure il codice iniziale � pi� grande del codice finale ")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and not(empty(.w_BANNOS))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oBANNOS_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente oppure obsoleto")
          case   not(((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) And .w_PARTSN='S') AND (.w_ACODCO>=.w_CODCON or empty(.w_CODCON)))  and not(empty(.w_ACODCO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oACODCO_1_47.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore inesistente, non gestito a partite o obsoleto, oppure il codice iniziale � pi� grande del codice finale ")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SCAINI = this.w_SCAINI
    this.o_TIPCON = this.w_TIPCON
    this.o_FLSALD = this.w_FLSALD
    this.o_FLPART = this.w_FLPART
    return

enddefine

* --- Define pages as container
define class tgste_sisPag1 as StdContainer
  Width  = 617
  height = 327
  stdWidth  = 617
  stdheight = 327
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSCAINI_1_2 as StdField with uid="BUACHACMJF",rtseq=2,rtrep=.f.,;
    cFormVar = "w_SCAINI", cQueryName = "SCAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La scadenza iniziale � maggiore della scadenza finale",;
    ToolTipText = "Data iniziale scadenza da ricercare",;
    HelpContextID = 20765658,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=99, Top=13

  func oSCAINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_scaini<=.w_scafin) or (empty(.w_scafin)))
    endwith
    return bRes
  endfunc

  add object oSCAFIN_1_3 as StdField with uid="AVZCQICAXG",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SCAFIN", cQueryName = "SCAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La scadenza iniziale � maggiore della scadenza finale",;
    ToolTipText = "Data finale scadenza da ricercare",;
    HelpContextID = 210754522,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=99, Top=41

  func oSCAFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_scaini<=.w_scafin) or (empty(.w_scaini)))
    endwith
    return bRes
  endfunc

  add object oREGINI_1_4 as StdField with uid="JPZWSXEPCG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_REGINI", cQueryName = "REGINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data registrazione iniziale � maggiore della scadenza finale",;
    ToolTipText = "Data registrazione iniziale da ricercare",;
    HelpContextID = 20740586,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=292, Top=13

  func oREGINI_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_REGINI<=.w_REGFIN) or (empty(.w_REGFIN)))
    endwith
    return bRes
  endfunc

  add object oREGFIN_1_5 as StdField with uid="PDJGLOUCJU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_REGFIN", cQueryName = "REGFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data registrazione iniziale � maggiore della scadenza finale",;
    ToolTipText = "Data registrazione finale da ricercare",;
    HelpContextID = 210729450,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=292, Top=41

  func oREGFIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_regini<=.w_regfin) or (empty(.w_regini)))
    endwith
    return bRes
  endfunc


  add object oTIPCON_1_6 as StdCombo with uid="WDXDHXNHSV",rtseq=6,rtrep=.f.,left=99,top=69,width=92,height=21;
    , ToolTipText = "Tipo partite/scadenze da selezionare, clienti, fornitori, conti generici";
    , HelpContextID = 204596682;
    , cFormVar="w_TIPCON",RowSource=""+"Clienti,"+"Fornitori,"+"Conti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_6.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oTIPCON_1_6.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_6.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      iif(this.Parent.oContained.w_TIPCON=='G',3,;
      0)))
  endfunc

  func oTIPCON_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not isalt() or g_COGE='S')
    endwith
   endif
  endfunc

  func oTIPCON_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCON)
        bRes2=.link_1_7('Full')
      endif
      if .not. empty(.w_ACODCO)
        bRes2=.link_1_47('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODCON_1_7 as StdField with uid="ICXFDOKTCK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore inesistente, non gestito a partite o obsoleto, oppure il codice iniziale � pi� grande del codice finale ",;
    ToolTipText = "Cliente/fornitore/conto di selezione scadenze",;
    HelpContextID = 204644570,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=97, Top=127, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori/conti",'GSTE_KMS.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCON_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oPAGRD_1_8 as StdCheck with uid="WYJEWFDOLH",rtseq=8,rtrep=.f.,left=97, top=183, caption="Rimessa diretta",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti con rimessa diretta",;
    HelpContextID = 181632522,;
    cFormVar="w_PAGRD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRD_1_8.RadioValue()
    return(iif(this.value =1,'RD',;
    'XX'))
  endfunc
  func oPAGRD_1_8.GetRadio()
    this.Parent.oContained.w_PAGRD = this.RadioValue()
    return .t.
  endfunc

  func oPAGRD_1_8.SetRadio()
    this.Parent.oContained.w_PAGRD=trim(this.Parent.oContained.w_PAGRD)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRD=='RD',1,;
      0)
  endfunc

  add object oPAGBO_1_9 as StdCheck with uid="UHUFUMTJOS",rtseq=9,rtrep=.f.,left=97, top=204, caption="Bonifico",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite bonifico",;
    HelpContextID = 171146762,;
    cFormVar="w_PAGBO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGBO_1_9.RadioValue()
    return(iif(this.value =1,'BO',;
    'XX'))
  endfunc
  func oPAGBO_1_9.GetRadio()
    this.Parent.oContained.w_PAGBO = this.RadioValue()
    return .t.
  endfunc

  func oPAGBO_1_9.SetRadio()
    this.Parent.oContained.w_PAGBO=trim(this.Parent.oContained.w_PAGBO)
    this.value = ;
      iif(this.Parent.oContained.w_PAGBO=='BO',1,;
      0)
  endfunc

  add object oPAGRB_1_10 as StdCheck with uid="LJJBJQGWOI",rtseq=10,rtrep=.f.,left=97, top=225, caption="Ric.bancaria/Ri.Ba.",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite ricevuta bancaria o RiBa",;
    HelpContextID = 183729674,;
    cFormVar="w_PAGRB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRB_1_10.RadioValue()
    return(iif(this.value =1,'RB',;
    'XX'))
  endfunc
  func oPAGRB_1_10.GetRadio()
    this.Parent.oContained.w_PAGRB = this.RadioValue()
    return .t.
  endfunc

  func oPAGRB_1_10.SetRadio()
    this.Parent.oContained.w_PAGRB=trim(this.Parent.oContained.w_PAGRB)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRB=='RB',1,;
      0)
  endfunc

  add object oPAGRI_1_11 as StdCheck with uid="CIJZFYBCCS",rtseq=11,rtrep=.f.,left=273, top=183, caption="R.I.D.",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite R.I.D.",;
    HelpContextID = 176389642,;
    cFormVar="w_PAGRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRI_1_11.RadioValue()
    return(iif(this.value =1,'RI',;
    'XX'))
  endfunc
  func oPAGRI_1_11.GetRadio()
    this.Parent.oContained.w_PAGRI = this.RadioValue()
    return .t.
  endfunc

  func oPAGRI_1_11.SetRadio()
    this.Parent.oContained.w_PAGRI=trim(this.Parent.oContained.w_PAGRI)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRI=='RI',1,;
      0)
  endfunc

  add object oPAGCA_1_12 as StdCheck with uid="WMQWIJEKXN",rtseq=12,rtrep=.f.,left=273, top=204, caption="Cambiale",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite cambiale",;
    HelpContextID = 185761290,;
    cFormVar="w_PAGCA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGCA_1_12.RadioValue()
    return(iif(this.value =1,'CA',;
    'XX'))
  endfunc
  func oPAGCA_1_12.GetRadio()
    this.Parent.oContained.w_PAGCA = this.RadioValue()
    return .t.
  endfunc

  func oPAGCA_1_12.SetRadio()
    this.Parent.oContained.w_PAGCA=trim(this.Parent.oContained.w_PAGCA)
    this.value = ;
      iif(this.Parent.oContained.w_PAGCA=='CA',1,;
      0)
  endfunc

  add object oPAGMA_1_13 as StdCheck with uid="OXYWDXMXXW",rtseq=13,rtrep=.f.,left=273, top=225, caption="M.AV.",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite M.AV. (mediante avviso)",;
    HelpContextID = 185105930,;
    cFormVar="w_PAGMA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGMA_1_13.RadioValue()
    return(iif(this.value =1,'MA',;
    'XX'))
  endfunc
  func oPAGMA_1_13.GetRadio()
    this.Parent.oContained.w_PAGMA = this.RadioValue()
    return .t.
  endfunc

  func oPAGMA_1_13.SetRadio()
    this.Parent.oContained.w_PAGMA=trim(this.Parent.oContained.w_PAGMA)
    this.value = ;
      iif(this.Parent.oContained.w_PAGMA=='MA',1,;
      0)
  endfunc

  add object oFLORIG_1_14 as StdCheck with uid="EBTGXOXZIU",rtseq=14,rtrep=.f.,left=448, top=183, caption="Dettaglio partite",;
    ToolTipText = "Se attivo: per le partite raggruppate vengono stampate le partite di origine.",;
    HelpContextID = 58913706,;
    cFormVar="w_FLORIG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLORIG_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLORIG_1_14.GetRadio()
    this.Parent.oContained.w_FLORIG = this.RadioValue()
    return .t.
  endfunc

  func oFLORIG_1_14.SetRadio()
    this.Parent.oContained.w_FLORIG=trim(this.Parent.oContained.w_FLORIG)
    this.value = ;
      iif(this.Parent.oContained.w_FLORIG=='S',1,;
      0)
  endfunc


  add object oFLSOSP_1_20 as StdCombo with uid="FMCTCNPNEF",rtseq=17,rtrep=.f.,left=501,top=14,width=113,height=21;
    , ToolTipText = "Ricerca le scadenze per il flag sospeso";
    , HelpContextID = 166048682;
    , cFormVar="w_FLSOSP",RowSource=""+"Tutte,"+"Non sospese,"+"Sospese", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLSOSP_1_20.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oFLSOSP_1_20.GetRadio()
    this.Parent.oContained.w_FLSOSP = this.RadioValue()
    return .t.
  endfunc

  func oFLSOSP_1_20.SetRadio()
    this.Parent.oContained.w_FLSOSP=trim(this.Parent.oContained.w_FLSOSP)
    this.value = ;
      iif(this.Parent.oContained.w_FLSOSP=='T',1,;
      iif(this.Parent.oContained.w_FLSOSP=='N',2,;
      iif(this.Parent.oContained.w_FLSOSP=='S',3,;
      0)))
  endfunc


  add object oFLSALD_1_21 as StdCombo with uid="OWEHCVYMJW",rtseq=18,rtrep=.f.,left=501,top=41,width=113,height=21;
    , ToolTipText = "Ricerca le scadenze saldate, non ancora saldate o tutte";
    , HelpContextID = 107197354;
    , cFormVar="w_FLSALD",RowSource=""+"Tutte,"+"Non saldate,"+"Saldate,"+"Solo parte aperta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLSALD_1_21.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    iif(this.value =3,'S',;
    iif(this.value =4,'R',;
    space(1))))))
  endfunc
  func oFLSALD_1_21.GetRadio()
    this.Parent.oContained.w_FLSALD = this.RadioValue()
    return .t.
  endfunc

  func oFLSALD_1_21.SetRadio()
    this.Parent.oContained.w_FLSALD=trim(this.Parent.oContained.w_FLSALD)
    this.value = ;
      iif(this.Parent.oContained.w_FLSALD=='T',1,;
      iif(this.Parent.oContained.w_FLSALD=='N',2,;
      iif(this.Parent.oContained.w_FLSALD=='S',3,;
      iif(this.Parent.oContained.w_FLSALD=='R',4,;
      0))))
  endfunc


  add object oFLPART_1_22 as StdCombo with uid="PRTIZEDRCX",rtseq=19,rtrep=.f.,left=501,top=68,width=113,height=21;
    , HelpContextID = 100918186;
    , cFormVar="w_FLPART",RowSource=""+"Tutte,"+"Saldo,"+"Creazione,"+"Acconto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLPART_1_22.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oFLPART_1_22.GetRadio()
    this.Parent.oContained.w_FLPART = this.RadioValue()
    return .t.
  endfunc

  func oFLPART_1_22.SetRadio()
    this.Parent.oContained.w_FLPART=trim(this.Parent.oContained.w_FLPART)
    this.value = ;
      iif(this.Parent.oContained.w_FLPART=='T',1,;
      iif(this.Parent.oContained.w_FLPART=='S',2,;
      iif(this.Parent.oContained.w_FLPART=='C',3,;
      iif(this.Parent.oContained.w_FLPART=='A',4,;
      0))))
  endfunc

  func oFLPART_1_22.mHide()
    with this.Parent.oContained
      return (.w_FLSALD<>'R')
    endwith
  endfunc

  add object oDATPAR_1_23 as StdField with uid="WCZJEEQMBZ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DATPAR", cQueryName = "DATPAR",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Filtra le partite aperte alla data",;
    HelpContextID = 151301834,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=501, Top=96

  func oDATPAR_1_23.mHide()
    with this.Parent.oContained
      return (.w_FLSALD<>'R')
    endwith
  endfunc


  add object oBtn_1_29 as StdButton with uid="SQNLIXYMPH",left=492, top=278, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 151606566;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        do GSTE_BPS with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_30 as cp_outputCombo with uid="ZLERXIVPWT",left=108, top=252, width=446,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 187814630


  add object oBtn_1_31 as StdButton with uid="VJEYGWVSZN",left=542, top=278, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 251301050;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDATSTA_1_34 as StdField with uid="JJVFHULYEB",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DATSTA", cQueryName = "DATSTA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di stampa",;
    HelpContextID = 147959498,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=292, Top=69, TabStop=.f.

  func oDATSTA_1_34.mHide()
    with this.Parent.oContained
      return (.w_ONUME<>6)
    endwith
  endfunc

  add object oDESCON_1_35 as StdField with uid="XZILIWPJAW",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 204585674,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=238, Top=127, InputMask=replicate('X',40)

  add object oNEWPAG_1_42 as StdCheck with uid="BRRNKYTCUP",rtseq=45,rtrep=.f.,left=448, top=204, caption="Salto pagina",;
    ToolTipText = "Attivo: viene effettuato il salto pagina per ogni cliente/fornitore nelle stampe ordinate x cli/for",;
    HelpContextID = 67402282,;
    cFormVar="w_NEWPAG", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNEWPAG_1_42.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oNEWPAG_1_42.GetRadio()
    this.Parent.oContained.w_NEWPAG = this.RadioValue()
    return .t.
  endfunc

  func oNEWPAG_1_42.SetRadio()
    this.Parent.oContained.w_NEWPAG=trim(this.Parent.oContained.w_NEWPAG)
    this.value = ;
      iif(this.Parent.oContained.w_NEWPAG=='S',1,;
      0)
  endfunc

  add object oACODCO_1_47 as StdField with uid="SUQKGSKDSO",rtseq=49,rtrep=.f.,;
    cFormVar = "w_ACODCO", cQueryName = "ACODCO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore inesistente, non gestito a partite o obsoleto, oppure il codice iniziale � pi� grande del codice finale ",;
    ToolTipText = "Cliente/fornitore/conto di selezione scadenze",;
    HelpContextID = 200342778,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=97, Top=156, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_ACODCO"

  func oACODCO_1_47.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_47('Part',this)
    endwith
    return bRes
  endfunc

  proc oACODCO_1_47.ecpDrop(oSource)
    this.Parent.oContained.link_1_47('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oACODCO_1_47.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oACODCO_1_47'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori/conti",'GSTE_KMS.CONTI_VZM',this.parent.oContained
  endproc
  proc oACODCO_1_47.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_ACODCO
     i_obj.ecpSave()
  endproc

  add object oADESCON_1_48 as StdField with uid="OOZXJPDDFM",rtseq=50,rtrep=.f.,;
    cFormVar = "w_ADESCON", cQueryName = "ADESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 199400442,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=238, Top=156, InputMask=replicate('X',40)

  add object oStr_1_15 as StdString with uid="HRHDNGMHAE",Visible=.t., Left=2, Top=13,;
    Alignment=1, Width=94, Height=15,;
    Caption="Scadenze da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="PMYSREYHZS",Visible=.t., Left=70, Top=41,;
    Alignment=1, Width=26, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="IBQAYEAHFT",Visible=.t., Left=2, Top=183,;
    Alignment=1, Width=94, Height=15,;
    Caption="Pagamenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="MJOOVSZMAE",Visible=.t., Left=378, Top=13,;
    Alignment=1, Width=119, Height=15,;
    Caption="Test sospese:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="VGFVDFYFIT",Visible=.t., Left=395, Top=41,;
    Alignment=1, Width=102, Height=15,;
    Caption="Test saldate:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="EUDLYBOGMQ",Visible=.t., Left=2, Top=252,;
    Alignment=1, Width=94, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="DKKXSABCFP",Visible=.t., Left=64, Top=69,;
    Alignment=1, Width=32, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="VNGFGUEZQD",Visible=.t., Left=2, Top=127,;
    Alignment=1, Width=94, Height=18,;
    Caption="Da cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'C')
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="SOQNUXLCBD",Visible=.t., Left=2, Top=127,;
    Alignment=1, Width=94, Height=18,;
    Caption="Da fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'F')
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="MREIEMXUSI",Visible=.t., Left=2, Top=127,;
    Alignment=1, Width=94, Height=18,;
    Caption="Da conto:"  ;
  , bGlobalFont=.t.

  func oStr_1_39.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'G')
    endwith
  endfunc

  add object oStr_1_40 as StdString with uid="NVBJZFZWIX",Visible=.t., Left=191, Top=13,;
    Alignment=1, Width=99, Height=18,;
    Caption="Da data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="BHEBGIAIYV",Visible=.t., Left=215, Top=41,;
    Alignment=1, Width=75, Height=18,;
    Caption="A data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="RDOGRSSRRK",Visible=.t., Left=202, Top=69,;
    Alignment=1, Width=88, Height=18,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (.w_ONUME<>6)
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="HBZSJYGTCN",Visible=.t., Left=407, Top=68,;
    Alignment=1, Width=90, Height=18,;
    Caption="Tipo partite:"  ;
  , bGlobalFont=.t.

  func oStr_1_45.mHide()
    with this.Parent.oContained
      return (.w_FLSALD<>'R')
    endwith
  endfunc

  add object oStr_1_46 as StdString with uid="LZOAPMJTTR",Visible=.t., Left=359, Top=95,;
    Alignment=1, Width=138, Height=18,;
    Caption="Partite aperte alla data:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (.w_FLSALD<>'R')
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="HRLNEVGMOL",Visible=.t., Left=3, Top=156,;
    Alignment=1, Width=94, Height=18,;
    Caption="A cliente:"  ;
  , bGlobalFont=.t.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'C')
    endwith
  endfunc

  add object oStr_1_50 as StdString with uid="CQUIURKXEO",Visible=.t., Left=3, Top=156,;
    Alignment=1, Width=94, Height=18,;
    Caption="A fornitore:"  ;
  , bGlobalFont=.t.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'F')
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="JYDQKCFJJY",Visible=.t., Left=2, Top=156,;
    Alignment=1, Width=94, Height=18,;
    Caption="A conto:"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (.w_TIPCON<>'G')
    endwith
  endfunc
enddefine
define class tgste_sisPag2 as StdContainer
  Width  = 617
  height = 327
  stdWidth  = 617
  stdheight = 327
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNUMPAR_2_1 as StdField with uid="JUQOMHYDXS",rtseq=27,rtrep=.f.,;
    cFormVar = "w_NUMPAR", cQueryName = "NUMPAR",;
    bObbl = .f. , nPag = 2, value=space(31), bMultilanguage =  .f.,;
    ToolTipText = "Numero partita di selezione",;
    HelpContextID = 151325226,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=151, Top=15, InputMask=replicate('X',31)

  add object oBANAPP_2_2 as StdField with uid="PLRTHMFISS",rtseq=28,rtrep=.f.,;
    cFormVar = "w_BANAPP", cQueryName = "BANAPP",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Banca di appoggio del cliente/fornitore",;
    HelpContextID = 170135274,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=151, Top=41, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_BANAPP"

  func oBANAPP_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oBANAPP_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBANAPP_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oBANAPP_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABA',"Elenco banche",'',this.parent.oContained
  endproc
  proc oBANAPP_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_BANAPP
     i_obj.ecpSave()
  endproc

  add object oDESBAN_2_3 as StdField with uid="XLWKXOVCNH",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 219331274,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=254, Top=41, InputMask=replicate('X',50)

  add object oBANNOS_2_4 as StdField with uid="BFMKOPRZXR",rtseq=30,rtrep=.f.,;
    cFormVar = "w_BANNOS", cQueryName = "BANNOS",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente oppure obsoleto",;
    ToolTipText = "Ns C/C comunicato a fornitore per RB o a cliente per bonifico",;
    HelpContextID = 120000234,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=151, Top=67, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_BANNOS"

  func oBANNOS_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oBANNOS_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBANNOS_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oBANNOS_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banca",'',this.parent.oContained
  endproc
  proc oBANNOS_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_BANNOS
     i_obj.ecpSave()
  endproc

  add object oDESNOS_2_5 as StdField with uid="OSCDGUJGEM",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DESNOS", cQueryName = "DESNOS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 119978698,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=287, Top=67, InputMask=replicate('X',35)

  add object oNDOINI_2_6 as StdField with uid="MFFRYELHLU",rtseq=32,rtrep=.f.,;
    cFormVar = "w_NDOINI", cQueryName = "NDOINI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di inizio selezione",;
    HelpContextID = 20708138,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=151, Top=93, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oADOINI_2_7 as StdField with uid="LFYMWAETPI",rtseq=33,rtrep=.f.,;
    cFormVar = "w_ADOINI", cQueryName = "ADOINI",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di inizio selezione",;
    HelpContextID = 20708346,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=279, Top=93, InputMask=replicate('X',10)

  add object oDDOINI_2_8 as StdField with uid="GVHSMJJJQT",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DDOINI", cQueryName = "DDOINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento inizio selezione",;
    HelpContextID = 20708298,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=490, Top=93

  add object oNDOFIN_2_9 as StdField with uid="YVXOAZLBNP",rtseq=35,rtrep=.f.,;
    cFormVar = "w_NDOFIN", cQueryName = "NDOFIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di fine selezione",;
    HelpContextID = 210697002,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=151, Top=119, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oADOFIN_2_10 as StdField with uid="WVYMVOQZXQ",rtseq=36,rtrep=.f.,;
    cFormVar = "w_ADOFIN", cQueryName = "ADOFIN",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di fine selezione",;
    HelpContextID = 210697210,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=279, Top=119, InputMask=replicate('X',10)

  add object oDDOFIN_2_11 as StdField with uid="YKXKJINOJS",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DDOFIN", cQueryName = "DDOFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento fine selezione",;
    HelpContextID = 210697162,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=490, Top=119

  add object oCATCOM_2_12 as StdField with uid="OSSUYKOIBF",rtseq=38,rtrep=.f.,;
    cFormVar = "w_CATCOM", cQueryName = "CATCOM",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categoria commerciale del cliente/fornitore",;
    HelpContextID = 221359834,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=151, Top=145, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_CATCOM"

  func oCATCOM_2_12.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oCATCOM_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCOM_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCOM_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCATCOM_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oCATCOM_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CATCOM
     i_obj.ecpSave()
  endproc

  add object oVALUTA_2_13 as StdField with uid="KITUVABRTZ",rtseq=39,rtrep=.f.,;
    cFormVar = "w_VALUTA", cQueryName = "VALUTA",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta, se vuota tutte",;
    HelpContextID = 147860906,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=151, Top=199, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALUTA"

  func oVALUTA_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oVALUTA_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVALUTA_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oVALUTA_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oVALUTA_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_VALUTA
     i_obj.ecpSave()
  endproc

  add object oDESVAL_2_14 as StdField with uid="IPQSGATYTZ",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 251574986,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=215, Top=199, InputMask=replicate('X',35)

  add object oAGENTE_2_15 as StdField with uid="LPZDJMOTNX",rtseq=41,rtrep=.f.,;
    cFormVar = "w_AGENTE", cQueryName = "AGENTE",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice agente di selezione (se vuoto=no selezione)",;
    HelpContextID = 81238266,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=151, Top=225, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_AGENTE"

  func oAGENTE_2_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCON='C')
    endwith
   endif
  endfunc

  func oAGENTE_2_15.mHide()
    with this.Parent.oContained
      return (Isalt()  )
    endwith
  endfunc

  func oAGENTE_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oAGENTE_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAGENTE_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oAGENTE_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Elenco agenti",'AGE1ZOOM.AGENTI_VZM',this.parent.oContained
  endproc
  proc oAGENTE_2_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_AGENTE
     i_obj.ecpSave()
  endproc

  add object oCODZON_2_16 as StdField with uid="SAAGWUFHPQ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_CODZON", cQueryName = "CODZON",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice zona del cliente/fornitore",;
    HelpContextID = 203137242,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=151, Top=251, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_CODZON"

  func oCODZON_2_16.mHide()
    with this.Parent.oContained
      return (Isalt()  )
    endwith
  endfunc

  func oCODZON_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODZON_2_16.ecpDrop(oSource)
    this.Parent.oContained.link_2_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODZON_2_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oCODZON_2_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc oCODZON_2_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_CODZON
     i_obj.ecpSave()
  endproc

  add object oDESAGE_2_28 as StdField with uid="LKEGMTSGUL",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 95664842,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=215, Top=225, InputMask=replicate('X',35)

  func oDESAGE_2_28.mHide()
    with this.Parent.oContained
      return (Isalt()  )
    endwith
  endfunc

  add object oDESZON_2_30 as StdField with uid="SHMILVTSKF",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESZON", cQueryName = "DESZON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 203078346,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=215, Top=251, InputMask=replicate('X',35)

  func oDESZON_2_30.mHide()
    with this.Parent.oContained
      return (Isalt()  )
    endwith
  endfunc

  add object oDESCOM_2_33 as StdField with uid="DMWAWYHAIR",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DESCOM", cQueryName = "DESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 221362890,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=215, Top=145, InputMask=replicate('X',35)

  func oDESCOM_2_33.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oCATCON_2_34 as StdField with uid="NIYEDLRJDN",rtseq=51,rtrep=.f.,;
    cFormVar = "w_CATCON", cQueryName = "CATCON",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categoria contabile del cliente/fornitore",;
    HelpContextID = 204582618,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=151, Top=172, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_CATCON"

  func oCATCON_2_34.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oCATCON_2_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCON_2_34.ecpDrop(oSource)
    this.Parent.oContained.link_2_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCON_2_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oCATCON_2_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili",'',this.parent.oContained
  endproc
  proc oCATCON_2_34.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_CATCON
     i_obj.ecpSave()
  endproc

  add object oDESCOT_2_36 as StdField with uid="XNVKVQVTZU",rtseq=52,rtrep=.f.,;
    cFormVar = "w_DESCOT", cQueryName = "DESCOT",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 103922378,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=215, Top=172, InputMask=replicate('X',35)

  func oDESCOT_2_36.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_2_17 as StdString with uid="OJNPKZHYTH",Visible=.t., Left=269, Top=93,;
    Alignment=2, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_18 as StdString with uid="LLRZQJNAKN",Visible=.t., Left=433, Top=93,;
    Alignment=1, Width=55, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="JTWYBEKTDW",Visible=.t., Left=269, Top=119,;
    Alignment=2, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="HHLBYQEEYA",Visible=.t., Left=433, Top=119,;
    Alignment=1, Width=55, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="BCXAOTEFTP",Visible=.t., Left=15, Top=199,;
    Alignment=1, Width=134, Height=15,;
    Caption="Codice valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="ELXUJQFUPN",Visible=.t., Left=15, Top=15,;
    Alignment=1, Width=134, Height=15,;
    Caption="Numero partita:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="SFGBWPEBBO",Visible=.t., Left=15, Top=67,;
    Alignment=1, Width=134, Height=22,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="EZXYCTGLBA",Visible=.t., Left=15, Top=41,;
    Alignment=1, Width=134, Height=15,;
    Caption="Banca di appoggio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="HPKOJQQYXJ",Visible=.t., Left=15, Top=93,;
    Alignment=1, Width=134, Height=15,;
    Caption="Da n. documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="XNNPVHYWCF",Visible=.t., Left=15, Top=119,;
    Alignment=1, Width=134, Height=15,;
    Caption="A n. documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="JBHFNNXGLO",Visible=.t., Left=15, Top=225,;
    Alignment=1, Width=134, Height=18,;
    Caption="Codice agente:"  ;
  , bGlobalFont=.t.

  func oStr_2_27.mHide()
    with this.Parent.oContained
      return (Isalt()  )
    endwith
  endfunc

  add object oStr_2_31 as StdString with uid="HAVOSCFVQR",Visible=.t., Left=15, Top=251,;
    Alignment=1, Width=134, Height=18,;
    Caption="Codice zona:"  ;
  , bGlobalFont=.t.

  func oStr_2_31.mHide()
    with this.Parent.oContained
      return (Isalt()  )
    endwith
  endfunc

  add object oStr_2_32 as StdString with uid="WYFOVQZXFO",Visible=.t., Left=15, Top=145,;
    Alignment=1, Width=134, Height=18,;
    Caption="Cat. commerciale:"  ;
  , bGlobalFont=.t.

  func oStr_2_32.mHide()
    with this.Parent.oContained
      return (Isalt()  )
    endwith
  endfunc

  add object oStr_2_35 as StdString with uid="APHSKVFXFQ",Visible=.t., Left=15, Top=172,;
    Alignment=1, Width=134, Height=18,;
    Caption="Cat. contabile:"  ;
  , bGlobalFont=.t.

  func oStr_2_35.mHide()
    with this.Parent.oContained
      return (Isalt()  )
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_sis','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
