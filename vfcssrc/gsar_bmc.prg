* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bmc                                                        *
*              Check sui mastri                                                *
*                                                                              *
*      Author: Zucchetti S.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][30]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-10-27                                                      *
* Last revis.: 2013-12-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bmc",oParentObject,m.pTipo)
return(i_retval)

define class tgsar_bmc as StdBatch
  * --- Local variables
  pTipo = space(1)
  w_MESS = space(10)
  w_OK = .f.
  w_CFUNC = space(10)
  w_CODICE = space(10)
  w_SEZIONE = space(1)
  w_FLCARC = space(1)
  w_SUFCON = space(5)
  w_CODCON = space(15)
  w_LENPOS = 0
  w_DESCRI = space(40)
  w_ANTCCAGG = space(0)
  w_DESSEZ = space(100)
  w_ErrorLog = .NULL.
  * --- WorkFile variables
  MASTRI_idx=0
  CONTI_idx=0
  AZIENDA_idx=0
  VOC_COST_idx=0
  COLLCENT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo sui MASTRI
    * --- Se pTipo vale 'C'  :Cancellazione
    *     Se pTipo vale 'V'  :Insert End e Update End
    *     Se pTipo vale 'D'  :Vario la sezione di bilancio.
    this.w_CFUNC = this.oParentObject.cFunction
    do case
      case this.pTipo="C"
        * --- Controllo che non abbia mastri figli
        this.w_OK = .T.
        if EMPTY(this.oParentObject.w_MCCONSUP) Or ( this.oParentObject.w_Mcnumliv>1 And not Empty(this.oParentObject.w_Mcconsup) )
          if EMPTY(this.oParentObject.w_MCCONSUP) AND G_APPLICATION="ADHOC REVOLUTION"
            this.w_MESS = ah_msgformat("Il mastro che si intende cancellare � di livello massimo%0Cancellare comunque?")
            this.w_OK = ah_YesNo(this.w_MESS)
            this.w_MESS = ah_msgformat("Transazione abbandonata")
          endif
          * --- Controllo che non abbia mastri figli
          if (EMPTY(this.oParentObject.w_MCCONSUP) Or ( this.oParentObject.w_Mcnumliv>1 And not Empty(this.oParentObject.w_Mcconsup) ) ) AND this.w_OK
            * --- Select from MASTRI
            i_nConn=i_TableProp[this.MASTRI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select COUNT(*) As Conta  from "+i_cTable+" MASTRI ";
                  +" where MCCONSUP= "+cp_ToStrODBC(this.oParentObject.w_MCCODICE)+"";
                   ,"_Curs_MASTRI")
            else
              select COUNT(*) As Conta from (i_cTable);
               where MCCONSUP= this.oParentObject.w_MCCODICE;
                into cursor _Curs_MASTRI
            endif
            if used('_Curs_MASTRI')
              select _Curs_MASTRI
              locate for 1=1
              do while not(eof())
              if Nvl(_Curs_MASTRI.Conta, 0 )>0
                this.w_MESS = ah_msgformat("Il mastro che si intende cancellare � riferimento di altri mastri%0Impossibile cancellare")
                this.w_OK = .F.
                exit
              endif
                select _Curs_MASTRI
                continue
              enddo
              use
            endif
          endif
        endif
        if this.w_OK AND g_APPLICATION="ADHOC REVOLUTION" AND g_GPOS="S"
          this.w_CODICE = this.oParentObject.w_MCCODICE
          do GSPS_BMC with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if Not this.w_OK
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_Mess
        endif
      case this.pTipo="V"
        * --- Lanciato sotto transazione, se ho uin errore a monte, non procedo...
        if bTrsErr
          i_retcode = 'stop'
          return
        endif
        if EMPTY(this.oParentObject.w_MCCONSUP) AND this.oParentObject.w_MCNUMLIV<g_MAXLIV
          this.w_MESS = ah_msgformat("Occorre inserire mastro di raggruppamento contabile")
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        else
          if this.oParentObject.w_Mcnumliv>1 And this.oParentObject.w_Mcsezbil<>this.oParentObject.w_Sezbila
            * --- Vado a leggere sui mastri tutti quelli di livello non massimo per calcolare
            *     la sezione di bilancio.
            * --- Select from MASTRI
            i_nConn=i_TableProp[this.MASTRI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2],.t.,this.MASTRI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select MCCODICE,MCCONSUP,MCSEZBIL  from "+i_cTable+" MASTRI ";
                  +" where MCCODICE<>"+cp_ToStrODBC(this.oParentObject.w_MCCODICE)+"";
                   ,"_Curs_MASTRI")
            else
              select MCCODICE,MCCONSUP,MCSEZBIL from (i_cTable);
               where MCCODICE<>this.oParentObject.w_MCCODICE;
                into cursor _Curs_MASTRI
            endif
            if used('_Curs_MASTRI')
              select _Curs_MASTRI
              locate for 1=1
              do while not(eof())
              * --- In linea teorica andrebbe fatta una query ma una SELECT 
              *     evita di creare un file nuovo per 6 mastri da escludere...
              if Not Empty( Nvl( _Curs_MASTRI.MCCONSUP ,"") )
                * --- Calcolo la sezione di bilancio
                this.w_SEZIONE = CALCSEZ(_Curs_MASTRI.MCCONSUP)
                if _Curs_MASTRI.MCSEZBIL<>this.w_SEZIONE
                  * --- Effettuo la scrittura nella tabella mastri della sezione di bilancio
                  * --- Write into MASTRI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.MASTRI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.MASTRI_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.MASTRI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"MCSEZBIL ="+cp_NullLink(cp_ToStrODBC(this.w_SEZIONE),'MASTRI','MCSEZBIL');
                        +i_ccchkf ;
                    +" where ";
                        +"MCCODICE = "+cp_ToStrODBC(_Curs_MASTRI.MCCODICE);
                           )
                  else
                    update (i_cTable) set;
                        MCSEZBIL = this.w_SEZIONE;
                        &i_ccchkf. ;
                     where;
                        MCCODICE = _Curs_MASTRI.MCCODICE;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              endif
                select _Curs_MASTRI
                continue
              enddo
              use
            endif
          endif
        endif
      case this.pTipo="D"
        if this.w_CFUNC = "Edit" And this.oParentObject.w_LIVMAS=1
          * --- Select from CONTI
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT(*) As CONTA  from "+i_cTable+" CONTI ";
                +" where ANCONSUP="+cp_ToStrODBC(this.oParentObject.w_Mccodice)+"";
                 ,"_Curs_CONTI")
          else
            select COUNT(*) As CONTA from (i_cTable);
             where ANCONSUP=this.oParentObject.w_Mccodice;
              into cursor _Curs_CONTI
          endif
          if used('_Curs_CONTI')
            select _Curs_CONTI
            locate for 1=1
            do while not(eof())
            if Nvl( _Curs_CONTI.CONTA, 0 )>0
              this.oParentObject.w_MCCONSUP = this.oParentObject.w_CONSMAS
              this.oParentObject.w_MCNUMLIV = this.oParentObject.w_LIVMAS
              this.oParentObject.w_MCSEZBIL = this.oParentObject.w_SEZBILA
              ah_ErrorMsg( "Impossibile modificare il livello del mastro perch� associato ad un conto" ,"","")
              * --- transaction error
              bTrsErr=.t.
              i_TrsMsg="Impossibile modificare il livello del mastro perch� associato ad un conto"
              exit
            endif
              select _Curs_CONTI
              continue
            enddo
            use
          endif
        endif
      case this.pTipo="G"
        * --- Inserisco conto contabile in automatico se previsto in dati azienda e il livello dei mastri � il primo
        if this.oParentObject.w_MCNUMLIV= 1 AND this.oParentObject.w_MCTIPMAS="G"
          * --- Read from AZIENDA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.AZIENDA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AZFLCARC,AZSUFCON"+;
              " from "+i_cTable+" AZIENDA where ";
                  +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AZFLCARC,AZSUFCON;
              from (i_cTable) where;
                  AZCODAZI = i_CODAZI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLCARC = NVL(cp_ToDate(_read_.AZFLCARC),cp_NullValue(_read_.AZFLCARC))
            this.w_SUFCON = NVL(cp_ToDate(_read_.AZSUFCON),cp_NullValue(_read_.AZSUFCON))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_FLCARC="S"
            this.w_ANTCCAGG = IIF(!(this.oParentObject.w_MCSEZBIL $ "CR" AND g_PERCCR="S") OR this.oParentObject.w_MCSEZBIL $"APO", "E","M")
            this.w_LENPOS = 15- LEN(this.w_SUFCON)
            this.w_CODCON = LEFT(ALLTRIM(this.oParentObject.w_MCCODICE), this.w_LENPOS)
            this.w_CODCON = this.w_CODCON+ ALLTRIM(this.w_SUFCON)
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANDESCRI"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC("G");
                    +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANDESCRI;
                from (i_cTable) where;
                    ANTIPCON = "G";
                    and ANCODICE = this.w_CODCON;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DESCRI = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_rows>0
              * --- Aggiorno il conto di analitia collegato se esistono
              if ((g_APPLICATION="ADHOC REVOLUTION" and this.oParentObject.w_AZCOACOG="S") or (g_APPLICATION<>"ADHOC REVOLUTION" and this.oParentObject.w_AZCOACOG$"CT"))
                if this.oParentObject.w_MCSEZBIL $ "CR"
                  * --- Try
                  local bErr_038CDF50
                  bErr_038CDF50=bTrsErr
                  this.Try_038CDF50()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    * --- accept error
                    bTrsErr=.f.
                  endif
                  bTrsErr=bTrsErr or bErr_038CDF50
                  * --- End
                endif
              endif
              ah_ErrorMsg("Il conto contabile %1 � gi� presente nella tabella CONTI","","",this.w_CODCON)
            else
              if !bTrsErr and ah_yesno("Vuoi caricare il conto collegato con codice %1?","", this.w_CODCON) 
 
                * --- Insert into CONTI
                i_nConn=i_TableProp[this.CONTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_ccchkf=''
                i_ccchkv=''
                this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CONTI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                              " ("+"ANTIPCON"+",ANCODICE"+",ANDESCRI"+",ANCONSUP"+",ANTIPSOT"+",ANPARTSN"+",ANCCTAGG"+",UTCC"+",UTCV"+",UTDV"+",UTDC"+",GESTGUID"+i_ccchkf+") values ("+;
                  cp_NullLink(cp_ToStrODBC("G"),'CONTI','ANTIPCON');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'CONTI','ANCODICE');
                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCDESCRI),'CONTI','ANDESCRI');
                  +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCCODICE),'CONTI','ANCONSUP');
                  +","+cp_NullLink(cp_ToStrODBC("X"),'CONTI','ANTIPSOT');
                  +","+cp_NullLink(cp_ToStrODBC(""),'CONTI','ANPARTSN');
                  +","+cp_NullLink(cp_ToStrODBC(this.w_ANTCCAGG),'CONTI','ANCCTAGG');
                  +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'CONTI','UTCC');
                  +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'CONTI','UTCV');
                  +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'CONTI','UTDV');
                  +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'CONTI','UTDC');
                  +","+cp_NullLink(cp_ToStrODBC(""),'CONTI','GESTGUID');
                       +i_ccchkv+")")
                else
                  cp_CheckDeletedKey(i_cTable,0,'ANTIPCON',"G",'ANCODICE',this.w_CODCON,'ANDESCRI',this.oParentObject.w_MCDESCRI,'ANCONSUP',this.oParentObject.w_MCCODICE,'ANTIPSOT',"X",'ANPARTSN',"",'ANCCTAGG',this.w_ANTCCAGG,'UTCC',i_CODUTE,'UTCV',i_CODUTE,'UTDV',SetInfoDate(g_CALUTD),'UTDC',SetInfoDate(g_CALUTD),'GESTGUID',"")
                  insert into (i_cTable) (ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP,ANTIPSOT,ANPARTSN,ANCCTAGG,UTCC,UTCV,UTDV,UTDC,GESTGUID &i_ccchkf. );
                     values (;
                       "G";
                       ,this.w_CODCON;
                       ,this.oParentObject.w_MCDESCRI;
                       ,this.oParentObject.w_MCCODICE;
                       ,"X";
                       ,"";
                       ,this.w_ANTCCAGG;
                       ,i_CODUTE;
                       ,i_CODUTE;
                       ,SetInfoDate(g_CALUTD);
                       ,SetInfoDate(g_CALUTD);
                       ,"";
                       &i_ccchkv. )
                  i_Rows=iif(bTrsErr,0,1)
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
                * --- Verifico se nei dati azienda � stato impostato il flag di sincronizzazione
                if ((g_APPLICATION="ADHOC REVOLUTION" and this.oParentObject.w_AZCOACOG="S") or (g_APPLICATION<>"ADHOC REVOLUTION" and this.oParentObject.w_AZCOACOG$"CT")) AND g_COAN="S"
                  if this.oParentObject.w_MCSEZBIL $ "CR"
                    * --- Se la voce di costo esiste gia non la ricrea
                    * --- Try
                    local bErr_039CF150
                    bErr_039CF150=bTrsErr
                    this.Try_039CF150()
                    * --- Catch
                    if !empty(i_Error)
                      i_ErrMsg=i_Error
                      i_Error=''
                      * --- accept error
                      bTrsErr=.f.
                    endif
                    bTrsErr=bTrsErr or bErr_039CF150
                    * --- End
                    * --- Try
                    local bErr_039CAB00
                    bErr_039CAB00=bTrsErr
                    this.Try_039CAB00()
                    * --- Catch
                    if !empty(i_Error)
                      i_ErrMsg=i_Error
                      i_Error=''
                      * --- accept error
                      bTrsErr=.f.
                    endif
                    bTrsErr=bTrsErr or bErr_039CAB00
                    * --- End
                  endif
                endif
                if !bTrsErr
                  this.w_DESSEZ = iCASE(this.oParentObject.w_MCSEZBIL="A",ah_msgformat("Attivit�"),this.oParentObject.w_MCSEZBIL="P",ah_msgformat("Passivit�"),this.oParentObject.w_MCSEZBIL="O",ah_msgformat("Ordine"),this.oParentObject.w_MCSEZBIL="C",ah_msgformat("Costi"),this.oParentObject.w_MCSEZBIL="R",ah_msgformat("Ricavi"),ah_msgformat("Transitori"))
                  ah_ErrorMsg("� stato creato il conto contabile: %1 con mastro di raggruppamento: %2 e sezione di bilancio: %3 ","","",this.w_CODCON, alltrim(this.oParentObject.w_MCCODICE), this.w_DESSEZ )
                endif
                if this.oParentObject.w_MCFLGESE $ "E-Q-V"
                  this.w_ErrorLog=createobject("AH_ErrorLog")
                  gscg_bps(this,"CI",this.w_CODCON,"B",this.w_ErrorLog,"")
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  Ah_msg("Caricamento servizi corrispondenti al Conto %1",.T.,.F.,.F., ALLTRIM(this.w_CODCON))
                  this.w_ErrorLog.PrintLog(this,"Segnalazioni in fase di caricamento")     
                endif
              endif
            endif
          endif
        endif
    endcase
  endproc
  proc Try_038CDF50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into VOC_COST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.VOC_COST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.VOC_COST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"VCTIPVOC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCSEZBIL ),'VOC_COST','VCTIPVOC');
          +i_ccchkf ;
      +" where ";
          +"VCCODICE = "+cp_ToStrODBC(this.w_CODCON);
          +" and VCCODMAS = "+cp_ToStrODBC(this.oParentObject.w_MCCODICE);
             )
    else
      update (i_cTable) set;
          VCTIPVOC = this.oParentObject.w_MCSEZBIL ;
          &i_ccchkf. ;
       where;
          VCCODICE = this.w_CODCON;
          and VCCODMAS = this.oParentObject.w_MCCODICE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_039CF150()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Try
    local bErr_039D7550
    bErr_039D7550=bTrsErr
    this.Try_039D7550()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      if this.oParentObject.w_AZCOACOG="C"
        * --- accept error
        bTrsErr=.f.
        * --- Insert into VOC_COST
        i_nConn=i_TableProp[this.VOC_COST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VOC_COST_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"VCCODICE"+",VCDESCRI"+",VCTIPVOC"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_CODCON),'VOC_COST','VCCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCDESCRI),'VOC_COST','VCDESCRI');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SEZBIL),'VOC_COST','VCTIPVOC');
          +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'VOC_COST','UTCC');
          +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'VOC_COST','UTCV');
          +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'VOC_COST','UTDC');
          +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'VOC_COST','UTDV');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'VCCODICE',this.w_CODCON,'VCDESCRI',this.oParentObject.w_MCDESCRI,'VCTIPVOC',this.oParentObject.w_SEZBIL,'UTCC',i_CODUTE,'UTCV',i_CODUTE,'UTDC',SetInfoDate(g_CALUTD),'UTDV',SetInfoDate(g_CALUTD))
          insert into (i_cTable) (VCCODICE,VCDESCRI,VCTIPVOC,UTCC,UTCV,UTDC,UTDV &i_ccchkf. );
             values (;
               this.w_CODCON;
               ,this.oParentObject.w_MCDESCRI;
               ,this.oParentObject.w_SEZBIL;
               ,i_CODUTE;
               ,i_CODUTE;
               ,SetInfoDate(g_CALUTD);
               ,SetInfoDate(g_CALUTD);
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        ah_ErrorMsg("Creata voce di analitica %1 di tipo %2%0Non esiste il mastro di raggruppamento %3 nell'archivio mastri voci di costo (analitica): inserire il mastro della voce manualmente.","","", alltrim (this.w_CODCON) , ICASE (alltrim(this.oParentObject.w_SEZBIL)="C","Costo",alltrim(this.oParentObject.w_SEZBIL)="R","Ricavo","Patrimoniale" ),alltrim(this.oParentObject.w_MCCODICE))
      endif
    endif
    bTrsErr=bTrsErr or bErr_039D7550
    * --- End
    return
  proc Try_039CAB00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- verifica inserimento modello se non gi� specificato
    * --- Select from COLLCENT
    i_nConn=i_TableProp[this.COLLCENT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.COLLCENT_idx,2],.t.,this.COLLCENT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select Count(MRCODICE) as Numocc  from "+i_cTable+" COLLCENT ";
          +" where MRTIPCON = 'G' AND MRCODICE = "+cp_ToStrODBC(this.w_CODCON)+"";
           ,"_Curs_COLLCENT")
    else
      select Count(MRCODICE) as Numocc from (i_cTable);
       where MRTIPCON = "G" AND MRCODICE = this.w_CODCON;
        into cursor _Curs_COLLCENT
    endif
    if used('_Curs_COLLCENT')
      select _Curs_COLLCENT
      locate for 1=1
      do while not(eof())
      if Nvl(_Curs_COLLCENT.Numocc, 0) = 0
        if g_APPLICATION<>"ADHOC REVOLUTION"
          GSAR1BMC(this,"G",this.w_CODCON)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Insert into COLLCENT
          i_nConn=i_TableProp[this.COLLCENT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COLLCENT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COLLCENT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MRTIPCON"+",MRPARAME"+",CPROWNUM"+",MRCODICE"+",MRCODVOC"+",MRROWORD"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC("G"),'COLLCENT','MRTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(1),'COLLCENT','MRPARAME');
            +","+cp_NullLink(cp_ToStrODBC(1),'COLLCENT','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'COLLCENT','MRCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'COLLCENT','MRCODVOC');
            +","+cp_NullLink(cp_ToStrODBC(1),'COLLCENT','MRROWORD');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MRTIPCON',"G",'MRPARAME',1,'CPROWNUM',1,'MRCODICE',this.w_CODCON,'MRCODVOC',this.w_CODCON,'MRROWORD',1)
            insert into (i_cTable) (MRTIPCON,MRPARAME,CPROWNUM,MRCODICE,MRCODVOC,MRROWORD &i_ccchkf. );
               values (;
                 "G";
                 ,1;
                 ,1;
                 ,this.w_CODCON;
                 ,this.w_CODCON;
                 ,1;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into COLLCENT
          i_nConn=i_TableProp[this.COLLCENT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.COLLCENT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.COLLCENT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MRTIPCON"+",MRPARAME"+",CPROWNUM"+",MRCODICE"+",MRCODVOC"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC("G"),'COLLCENT','MRTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(1),'COLLCENT','MRPARAME');
            +","+cp_NullLink(cp_ToStrODBC(1),'COLLCENT','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'COLLCENT','MRCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'COLLCENT','MRCODVOC');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MRTIPCON',"G",'MRPARAME',1,'CPROWNUM',1,'MRCODICE',this.w_CODCON,'MRCODVOC',this.w_CODCON)
            insert into (i_cTable) (MRTIPCON,MRPARAME,CPROWNUM,MRCODICE,MRCODVOC &i_ccchkf. );
               values (;
                 "G";
                 ,1;
                 ,1;
                 ,this.w_CODCON;
                 ,this.w_CODCON;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
        * --- Imposta il flag automatismo a manuale
        * --- Write into CONTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.CONTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"ANCCTAGG ="+cp_NullLink(cp_ToStrODBC("M"),'CONTI','ANCCTAGG');
              +i_ccchkf ;
          +" where ";
              +"ANTIPCON = "+cp_ToStrODBC("G");
              +" and ANCODICE = "+cp_ToStrODBC(this.w_CODCON);
                 )
        else
          update (i_cTable) set;
              ANCCTAGG = "M";
              &i_ccchkf. ;
           where;
              ANTIPCON = "G";
              and ANCODICE = this.w_CODCON;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
        select _Curs_COLLCENT
        continue
      enddo
      use
    endif
    return
  proc Try_039D7550()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into VOC_COST
    i_nConn=i_TableProp[this.VOC_COST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.VOC_COST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"VCCODICE"+",VCDESCRI"+",VCCODMAS"+",VCTIPVOC"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODCON),'VOC_COST','VCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCDESCRI),'VOC_COST','VCDESCRI');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MCCODICE),'VOC_COST','VCCODMAS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SEZBIL),'VOC_COST','VCTIPVOC');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'VOC_COST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'VOC_COST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'VOC_COST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'VOC_COST','UTDV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'VCCODICE',this.w_CODCON,'VCDESCRI',this.oParentObject.w_MCDESCRI,'VCCODMAS',this.oParentObject.w_MCCODICE,'VCTIPVOC',this.oParentObject.w_SEZBIL,'UTCC',i_CODUTE,'UTCV',i_CODUTE,'UTDC',SetInfoDate(g_CALUTD),'UTDV',SetInfoDate(g_CALUTD))
      insert into (i_cTable) (VCCODICE,VCDESCRI,VCCODMAS,VCTIPVOC,UTCC,UTCV,UTDC,UTDV &i_ccchkf. );
         values (;
           this.w_CODCON;
           ,this.oParentObject.w_MCDESCRI;
           ,this.oParentObject.w_MCCODICE;
           ,this.oParentObject.w_SEZBIL;
           ,i_CODUTE;
           ,i_CODUTE;
           ,SetInfoDate(g_CALUTD);
           ,SetInfoDate(g_CALUTD);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_ErrorMsg("Creata voce di analitica %1 di tipo %2","","", alltrim (this.w_CODCON) , ICASE (alltrim(this.oParentObject.w_SEZBIL)="C","Costo",alltrim(this.oParentObject.w_SEZBIL)="R","Ricavo","Patrimoniale" ))
    return


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='MASTRI'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='VOC_COST'
    this.cWorkTables[5]='COLLCENT'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_MASTRI')
      use in _Curs_MASTRI
    endif
    if used('_Curs_MASTRI')
      use in _Curs_MASTRI
    endif
    if used('_Curs_CONTI')
      use in _Curs_CONTI
    endif
    if used('_Curs_COLLCENT')
      use in _Curs_COLLCENT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
