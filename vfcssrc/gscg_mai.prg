* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mai                                                        *
*              Automatismi IVA                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_16]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-17                                                      *
* Last revis.: 2013-12-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_mai")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_mai")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_mai")
  return

* --- Class definition
define class tgscg_mai as StdPCForm
  Width  = 807
  Height = 126
  Top    = 12
  Left   = 22
  cComment = "Automatismi IVA"
  cPrg = "gscg_mai"
  HelpContextID=130688361
  add object cnt as tcgscg_mai
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_mai as PCContext
  w_AICODCAU = space(5)
  w_AITIPCLF = space(1)
  w_AICODCLF = space(15)
  w_OBTEST = space(8)
  w_DATOBSO = space(8)
  w_AICODIVA = space(5)
  w_AITIPCON = space(1)
  w_AITIPCOP = space(1)
  w_DESIVA = space(35)
  w_AICONDET = space(15)
  w_AITIPREG = space(1)
  w_AINUMREG = 0
  w_PERIND = 0
  w_AICONIND = space(15)
  w_AICONTRO = space(15)
  w_TIPSOT = space(1)
  w_TIPSO2 = space(1)
  w_DESAPP1 = space(40)
  w_DESCONTRO = space(60)
  w_DESAPP = space(40)
  proc Save(i_oFrom)
    this.w_AICODCAU = i_oFrom.w_AICODCAU
    this.w_AITIPCLF = i_oFrom.w_AITIPCLF
    this.w_AICODCLF = i_oFrom.w_AICODCLF
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_DATOBSO = i_oFrom.w_DATOBSO
    this.w_AICODIVA = i_oFrom.w_AICODIVA
    this.w_AITIPCON = i_oFrom.w_AITIPCON
    this.w_AITIPCOP = i_oFrom.w_AITIPCOP
    this.w_DESIVA = i_oFrom.w_DESIVA
    this.w_AICONDET = i_oFrom.w_AICONDET
    this.w_AITIPREG = i_oFrom.w_AITIPREG
    this.w_AINUMREG = i_oFrom.w_AINUMREG
    this.w_PERIND = i_oFrom.w_PERIND
    this.w_AICONIND = i_oFrom.w_AICONIND
    this.w_AICONTRO = i_oFrom.w_AICONTRO
    this.w_TIPSOT = i_oFrom.w_TIPSOT
    this.w_TIPSO2 = i_oFrom.w_TIPSO2
    this.w_DESAPP1 = i_oFrom.w_DESAPP1
    this.w_DESCONTRO = i_oFrom.w_DESCONTRO
    this.w_DESAPP = i_oFrom.w_DESAPP
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_AICODCAU = this.w_AICODCAU
    i_oTo.w_AITIPCLF = this.w_AITIPCLF
    i_oTo.w_AICODCLF = this.w_AICODCLF
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_DATOBSO = this.w_DATOBSO
    i_oTo.w_AICODIVA = this.w_AICODIVA
    i_oTo.w_AITIPCON = this.w_AITIPCON
    i_oTo.w_AITIPCOP = this.w_AITIPCOP
    i_oTo.w_DESIVA = this.w_DESIVA
    i_oTo.w_AICONDET = this.w_AICONDET
    i_oTo.w_AITIPREG = this.w_AITIPREG
    i_oTo.w_AINUMREG = this.w_AINUMREG
    i_oTo.w_PERIND = this.w_PERIND
    i_oTo.w_AICONIND = this.w_AICONIND
    i_oTo.w_AICONTRO = this.w_AICONTRO
    i_oTo.w_TIPSOT = this.w_TIPSOT
    i_oTo.w_TIPSO2 = this.w_TIPSO2
    i_oTo.w_DESAPP1 = this.w_DESAPP1
    i_oTo.w_DESCONTRO = this.w_DESCONTRO
    i_oTo.w_DESAPP = this.w_DESAPP
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscg_mai as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 807
  Height = 126
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-12-17"
  HelpContextID=130688361
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  CAUIVA_IDX = 0
  VOCIIVA_IDX = 0
  CONTI_IDX = 0
  cFile = "CAUIVA"
  cKeySelect = "AICODCAU,AITIPCLF,AICODCLF"
  cKeyWhere  = "AICODCAU=this.w_AICODCAU and AITIPCLF=this.w_AITIPCLF and AICODCLF=this.w_AICODCLF"
  cKeyDetail  = "AICODCAU=this.w_AICODCAU and AITIPCLF=this.w_AITIPCLF and AICODCLF=this.w_AICODCLF"
  cKeyWhereODBC = '"AICODCAU="+cp_ToStrODBC(this.w_AICODCAU)';
      +'+" and AITIPCLF="+cp_ToStrODBC(this.w_AITIPCLF)';
      +'+" and AICODCLF="+cp_ToStrODBC(this.w_AICODCLF)';

  cKeyDetailWhereODBC = '"AICODCAU="+cp_ToStrODBC(this.w_AICODCAU)';
      +'+" and AITIPCLF="+cp_ToStrODBC(this.w_AITIPCLF)';
      +'+" and AICODCLF="+cp_ToStrODBC(this.w_AICODCLF)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"CAUIVA.AICODCAU="+cp_ToStrODBC(this.w_AICODCAU)';
      +'+" and CAUIVA.AITIPCLF="+cp_ToStrODBC(this.w_AITIPCLF)';
      +'+" and CAUIVA.AICODCLF="+cp_ToStrODBC(this.w_AICODCLF)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'CAUIVA.CPROWNUM '
  cPrg = "gscg_mai"
  cComment = "Automatismi IVA"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 4
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AICODCAU = space(5)
  w_AITIPCLF = space(1)
  w_AICODCLF = space(15)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_AICODIVA = space(5)
  o_AICODIVA = space(5)
  w_AITIPCON = space(1)
  w_AITIPCOP = space(1)
  w_DESIVA = space(35)
  w_AICONDET = space(15)
  w_AITIPREG = space(1)
  w_AINUMREG = 0
  w_PERIND = 0
  w_AICONIND = space(15)
  w_AICONTRO = space(15)
  w_TIPSOT = space(1)
  w_TIPSO2 = space(1)
  w_DESAPP1 = space(40)
  w_DESCONTRO = space(60)
  w_DESAPP = space(40)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_maiPag1","gscg_mai",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='VOCIIVA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='CAUIVA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CAUIVA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CAUIVA_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_mai'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_2_9_joined
    link_2_9_joined=.f.
    local link_2_10_joined
    link_2_10_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from CAUIVA where AICODCAU=KeySet.AICODCAU
    *                            and AITIPCLF=KeySet.AITIPCLF
    *                            and AICODCLF=KeySet.AICODCLF
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.CAUIVA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAUIVA_IDX,2],this.bLoadRecFilter,this.CAUIVA_IDX,"gscg_mai")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CAUIVA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CAUIVA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CAUIVA '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_9_joined=this.AddJoinedLink_2_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_10_joined=this.AddJoinedLink_2_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AICODCAU',this.w_AICODCAU  ,'AITIPCLF',this.w_AITIPCLF  ,'AICODCLF',this.w_AICODCLF  )
      select * from (i_cTable) CAUIVA where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_AICODCAU = NVL(AICODCAU,space(5))
        .w_AITIPCLF = NVL(AITIPCLF,space(1))
        .w_AICODCLF = NVL(AICODCLF,space(15))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'CAUIVA')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESIVA = space(35)
          .w_PERIND = 0
          .w_TIPSOT = space(1)
          .w_TIPSO2 = space(1)
          .w_DESAPP1 = space(40)
          .w_DESCONTRO = space(60)
          .w_DESAPP = space(40)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_AICODIVA = NVL(AICODIVA,space(5))
          if link_2_1_joined
            this.w_AICODIVA = NVL(IVCODIVA201,NVL(this.w_AICODIVA,space(5)))
            this.w_DESIVA = NVL(IVDESIVA201,space(35))
            this.w_PERIND = NVL(IVPERIND201,0)
            this.w_DATOBSO = NVL(cp_ToDate(IVDTOBSO201),ctod("  /  /  "))
          else
          .link_2_1('Load')
          endif
          .w_AITIPCON = NVL(AITIPCON,space(1))
          .w_AITIPCOP = NVL(AITIPCOP,space(1))
          .w_AICONDET = NVL(AICONDET,space(15))
          if link_2_5_joined
            this.w_AICONDET = NVL(ANCODICE205,NVL(this.w_AICONDET,space(15)))
            this.w_DESAPP = NVL(ANDESCRI205,space(40))
            this.w_TIPSOT = NVL(ANTIPSOT205,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO205),ctod("  /  /  "))
          else
          .link_2_5('Load')
          endif
          .w_AITIPREG = NVL(AITIPREG,space(1))
          .w_AINUMREG = NVL(AINUMREG,0)
          .w_AICONIND = NVL(AICONIND,space(15))
          if link_2_9_joined
            this.w_AICONIND = NVL(ANCODICE209,NVL(this.w_AICONIND,space(15)))
            this.w_DESAPP1 = NVL(ANDESCRI209,space(40))
            this.w_TIPSO2 = NVL(ANTIPSOT209,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO209),ctod("  /  /  "))
          else
          .link_2_9('Load')
          endif
          .w_AICONTRO = NVL(AICONTRO,space(15))
          if link_2_10_joined
            this.w_AICONTRO = NVL(ANCODICE210,NVL(this.w_AICONTRO,space(15)))
            this.w_DESCONTRO = NVL(ANDESCRI210,space(60))
          else
          .link_2_10('Load')
          endif
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_AICODCAU=space(5)
      .w_AITIPCLF=space(1)
      .w_AICODCLF=space(15)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_AICODIVA=space(5)
      .w_AITIPCON=space(1)
      .w_AITIPCOP=space(1)
      .w_DESIVA=space(35)
      .w_AICONDET=space(15)
      .w_AITIPREG=space(1)
      .w_AINUMREG=0
      .w_PERIND=0
      .w_AICONIND=space(15)
      .w_AICONTRO=space(15)
      .w_TIPSOT=space(1)
      .w_TIPSO2=space(1)
      .w_DESAPP1=space(40)
      .w_DESCONTRO=space(60)
      .w_DESAPP=space(40)
      if .cFunction<>"Filter"
        .DoRTCalc(1,3,.f.)
        .w_OBTEST = i_datsys
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_AICODIVA))
         .link_2_1('Full')
        endif
        .w_AITIPCON = 'G'
        .w_AITIPCOP = 'G'
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_AICONDET))
         .link_2_5('Full')
        endif
        .w_AITIPREG = this.oParentObject .w_CTIPREG
        .w_AINUMREG = this.oParentObject .w_CNUMREG
        .DoRTCalc(13,13,.f.)
        .w_AICONIND = space(15)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_AICONIND))
         .link_2_9('Full')
        endif
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_AICONTRO))
         .link_2_10('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'CAUIVA')
    this.DoRTCalc(16,20,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oAITIPCOP_2_3.enabled = i_bVal
      .Page1.oPag.oAICONTRO_2_10.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'CAUIVA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CAUIVA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICODCAU,"AICODCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AITIPCLF,"AITIPCLF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AICODCLF,"AICODCLF",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_AICODIVA C(5);
      ,t_AITIPCOP C(1);
      ,t_DESIVA C(35);
      ,t_AICONDET C(15);
      ,t_AITIPREG N(3);
      ,t_AINUMREG N(2);
      ,t_PERIND N(3);
      ,t_AICONIND C(15);
      ,t_AICONTRO C(15);
      ,t_DESAPP1 C(40);
      ,t_DESCONTRO C(60);
      ,t_DESAPP C(40);
      ,CPROWNUM N(10);
      ,t_AITIPCON C(1);
      ,t_TIPSOT C(1);
      ,t_TIPSO2 C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_maibodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAICODIVA_2_1.controlsource=this.cTrsName+'.t_AICODIVA'
    this.oPgFRm.Page1.oPag.oAITIPCOP_2_3.controlsource=this.cTrsName+'.t_AITIPCOP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESIVA_2_4.controlsource=this.cTrsName+'.t_DESIVA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAICONDET_2_5.controlsource=this.cTrsName+'.t_AICONDET'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAITIPREG_2_6.controlsource=this.cTrsName+'.t_AITIPREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAINUMREG_2_7.controlsource=this.cTrsName+'.t_AINUMREG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPERIND_2_8.controlsource=this.cTrsName+'.t_PERIND'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oAICONIND_2_9.controlsource=this.cTrsName+'.t_AICONIND'
    this.oPgFRm.Page1.oPag.oAICONTRO_2_10.controlsource=this.cTrsName+'.t_AICONTRO'
    this.oPgFRm.Page1.oPag.oDESAPP1_2_13.controlsource=this.cTrsName+'.t_DESAPP1'
    this.oPgFRm.Page1.oPag.oDESCONTRO_2_14.controlsource=this.cTrsName+'.t_DESCONTRO'
    this.oPgFRm.Page1.oPag.oDESAPP_2_15.controlsource=this.cTrsName+'.t_DESAPP'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(68)
    this.AddVLine(238)
    this.AddVLine(381)
    this.AddVLine(501)
    this.AddVLine(572)
    this.AddVLine(637)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAICODIVA_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CAUIVA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAUIVA_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CAUIVA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAUIVA_IDX,2])
      *
      * insert into CAUIVA
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CAUIVA')
        i_extval=cp_InsertValODBCExtFlds(this,'CAUIVA')
        i_cFldBody=" "+;
                  "(AICODCAU,AITIPCLF,AICODCLF,AICODIVA,AITIPCON"+;
                  ",AITIPCOP,AICONDET,AITIPREG,AINUMREG,AICONIND"+;
                  ",AICONTRO,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_AICODCAU)+","+cp_ToStrODBC(this.w_AITIPCLF)+","+cp_ToStrODBC(this.w_AICODCLF)+","+cp_ToStrODBCNull(this.w_AICODIVA)+","+cp_ToStrODBC(this.w_AITIPCON)+;
             ","+cp_ToStrODBC(this.w_AITIPCOP)+","+cp_ToStrODBCNull(this.w_AICONDET)+","+cp_ToStrODBC(this.w_AITIPREG)+","+cp_ToStrODBC(this.w_AINUMREG)+","+cp_ToStrODBCNull(this.w_AICONIND)+;
             ","+cp_ToStrODBCNull(this.w_AICONTRO)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CAUIVA')
        i_extval=cp_InsertValVFPExtFlds(this,'CAUIVA')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'AICODCAU',this.w_AICODCAU,'AITIPCLF',this.w_AITIPCLF,'AICODCLF',this.w_AICODCLF)
        INSERT INTO (i_cTable) (;
                   AICODCAU;
                  ,AITIPCLF;
                  ,AICODCLF;
                  ,AICODIVA;
                  ,AITIPCON;
                  ,AITIPCOP;
                  ,AICONDET;
                  ,AITIPREG;
                  ,AINUMREG;
                  ,AICONIND;
                  ,AICONTRO;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_AICODCAU;
                  ,this.w_AITIPCLF;
                  ,this.w_AICODCLF;
                  ,this.w_AICODIVA;
                  ,this.w_AITIPCON;
                  ,this.w_AITIPCOP;
                  ,this.w_AICONDET;
                  ,this.w_AITIPREG;
                  ,this.w_AINUMREG;
                  ,this.w_AICONIND;
                  ,this.w_AICONTRO;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.CAUIVA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAUIVA_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (t_AICODIVA<>space(5) AND (t_AICONDET<>SPACE(15) OR t_AICONIND<>SPACE(15))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'CAUIVA')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'CAUIVA')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (t_AICODIVA<>space(5) AND (t_AICONDET<>SPACE(15) OR t_AICONIND<>SPACE(15))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update CAUIVA
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'CAUIVA')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " AICODIVA="+cp_ToStrODBCNull(this.w_AICODIVA)+;
                     ",AITIPCON="+cp_ToStrODBC(this.w_AITIPCON)+;
                     ",AITIPCOP="+cp_ToStrODBC(this.w_AITIPCOP)+;
                     ",AICONDET="+cp_ToStrODBCNull(this.w_AICONDET)+;
                     ",AITIPREG="+cp_ToStrODBC(this.w_AITIPREG)+;
                     ",AINUMREG="+cp_ToStrODBC(this.w_AINUMREG)+;
                     ",AICONIND="+cp_ToStrODBCNull(this.w_AICONIND)+;
                     ",AICONTRO="+cp_ToStrODBCNull(this.w_AICONTRO)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'CAUIVA')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      AICODIVA=this.w_AICODIVA;
                     ,AITIPCON=this.w_AITIPCON;
                     ,AITIPCOP=this.w_AITIPCOP;
                     ,AICONDET=this.w_AICONDET;
                     ,AITIPREG=this.w_AITIPREG;
                     ,AINUMREG=this.w_AINUMREG;
                     ,AICONIND=this.w_AICONIND;
                     ,AICONTRO=this.w_AICONTRO;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CAUIVA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CAUIVA_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (t_AICODIVA<>space(5) AND (t_AICONDET<>SPACE(15) OR t_AICONIND<>SPACE(15))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete CAUIVA
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (t_AICODIVA<>space(5) AND (t_AICONDET<>SPACE(15) OR t_AICONIND<>SPACE(15))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CAUIVA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CAUIVA_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,10,.t.)
        if .o_AICODIVA<>.w_AICODIVA
          .w_AITIPREG = this.oParentObject .w_CTIPREG
        endif
        if .o_AICODIVA<>.w_AICODIVA
          .w_AINUMREG = this.oParentObject .w_CNUMREG
        endif
        .DoRTCalc(13,13,.t.)
        if .o_AICODIVA<>.w_AICODIVA
          .w_AICONIND = space(15)
          .link_2_9('Full')
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(15,20,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_AITIPCON with this.w_AITIPCON
      replace t_TIPSOT with this.w_TIPSOT
      replace t_TIPSO2 with this.w_TIPSO2
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oAITIPCOP_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oAITIPCOP_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAICONDET_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAICONDET_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAITIPREG_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAITIPREG_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAINUMREG_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAINUMREG_2_7.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAICONIND_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oAICONIND_2_9.mCond()
    this.oPgFrm.Page1.oPag.oAICONTRO_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oAICONTRO_2_10.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    this.oPgFrm.Page1.oPag.oAITIPCOP_2_3.visible=!this.oPgFrm.Page1.oPag.oAITIPCOP_2_3.mHide()
    this.oPgFrm.Page1.oPag.oAICONTRO_2_10.visible=!this.oPgFrm.Page1.oPag.oAICONTRO_2_10.mHide()
    this.oPgFrm.Page1.oPag.oDESAPP1_2_13.visible=!this.oPgFrm.Page1.oPag.oDESAPP1_2_13.mHide()
    this.oPgFrm.Page1.oPag.oDESCONTRO_2_14.visible=!this.oPgFrm.Page1.oPag.oDESCONTRO_2_14.mHide()
    this.oPgFrm.Page1.oPag.oDESAPP_2_15.visible=!this.oPgFrm.Page1.oPag.oDESAPP_2_15.mHide()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AICODIVA
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AICODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_AICODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIND,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_AICODIVA))
          select IVCODIVA,IVDESIVA,IVPERIND,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AICODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_AICODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIND,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_AICODIVA)+"%");

            select IVCODIVA,IVDESIVA,IVPERIND,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AICODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oAICODIVA_2_1'),i_cWhere,'GSAR_AIV',"Codici IVA",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIND,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVPERIND,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AICODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIND,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_AICODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_AICODIVA)
            select IVCODIVA,IVDESIVA,IVPERIND,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AICODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
      this.w_PERIND = NVL(_Link_.IVPERIND,0)
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AICODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
      this.w_PERIND = 0
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
        endif
        this.w_AICODIVA = space(5)
        this.w_DESIVA = space(35)
        this.w_PERIND = 0
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AICODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.IVCODIVA as IVCODIVA201"+ ",link_2_1.IVDESIVA as IVDESIVA201"+ ",link_2_1.IVPERIND as IVPERIND201"+ ",link_2_1.IVDTOBSO as IVDTOBSO201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on CAUIVA.AICODIVA=link_2_1.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and CAUIVA.AICODIVA=link_2_1.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AICONDET
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AICONDET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_AICONDET)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_AITIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_AITIPCON;
                     ,'ANCODICE',trim(this.w_AICONDET))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AICONDET)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_AICONDET)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_AITIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_AICONDET)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_AITIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AICONDET) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oAICONDET_2_5'),i_cWhere,'GSAR_API',"Conti IVA",'GSCG_MAI.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AITIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente/obsoleto o di tipo non IVA")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_AITIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AICONDET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_AICONDET);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_AITIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_AITIPCON;
                       ,'ANCODICE',this.w_AICONDET)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AICONDET = NVL(_Link_.ANCODICE,space(15))
      this.w_DESAPP = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPSOT = NVL(_Link_.ANTIPSOT,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AICONDET = space(15)
      endif
      this.w_DESAPP = space(40)
      this.w_TIPSOT = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TIPSOT='I' OR EMPTY(.w_AICONDET)) AND (EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente/obsoleto o di tipo non IVA")
        endif
        this.w_AICONDET = space(15)
        this.w_DESAPP = space(40)
        this.w_TIPSOT = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AICONDET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.ANCODICE as ANCODICE205"+ ",link_2_5.ANDESCRI as ANDESCRI205"+ ",link_2_5.ANTIPSOT as ANTIPSOT205"+ ",link_2_5.ANDTOBSO as ANDTOBSO205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on CAUIVA.AICONDET=link_2_5.ANCODICE"+" and CAUIVA.AITIPCON=link_2_5.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and CAUIVA.AICONDET=link_2_5.ANCODICE(+)"'+'+" and CAUIVA.AITIPCON=link_2_5.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AICONIND
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AICONIND) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_AICONIND)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_AITIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_AITIPCON;
                     ,'ANCODICE',trim(this.w_AICONIND))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AICONIND)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_AICONIND)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_AITIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_AICONIND)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_AITIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AICONIND) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oAICONIND_2_9'),i_cWhere,'GSAR_API',"Conti indetraibili",'GSCG1MAI.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AITIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto indetraibile inesistente/obsoleto o di tipo IVA")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_AITIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AICONIND)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_AICONIND);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_AITIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_AITIPCON;
                       ,'ANCODICE',this.w_AICONIND)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AICONIND = NVL(_Link_.ANCODICE,space(15))
      this.w_DESAPP1 = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPSO2 = NVL(_Link_.ANTIPSOT,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AICONIND = space(15)
      endif
      this.w_DESAPP1 = space(40)
      this.w_TIPSO2 = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TIPSO2<>'I' OR EMPTY(.w_AICONIND)) AND (EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto indetraibile inesistente/obsoleto o di tipo IVA")
        endif
        this.w_AICONIND = space(15)
        this.w_DESAPP1 = space(40)
        this.w_TIPSO2 = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AICONIND Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_9.ANCODICE as ANCODICE209"+ ",link_2_9.ANDESCRI as ANDESCRI209"+ ",link_2_9.ANTIPSOT as ANTIPSOT209"+ ",link_2_9.ANDTOBSO as ANDTOBSO209"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_9 on CAUIVA.AICONIND=link_2_9.ANCODICE"+" and CAUIVA.AITIPCON=link_2_9.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_9"
          i_cKey=i_cKey+'+" and CAUIVA.AICONIND=link_2_9.ANCODICE(+)"'+'+" and CAUIVA.AITIPCON=link_2_9.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AICONTRO
  func Link_2_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AICONTRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_AICONTRO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_AITIPCOP);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_AITIPCOP;
                     ,'ANCODICE',trim(this.w_AICONTRO))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AICONTRO)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AICONTRO) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oAICONTRO_2_10'),i_cWhere,'GSAR_BZC',"Elenco clienti/fornitori/conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AITIPCOP<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_AITIPCOP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AICONTRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_AICONTRO);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_AITIPCOP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_AITIPCOP;
                       ,'ANCODICE',this.w_AICONTRO)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AICONTRO = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCONTRO = NVL(_Link_.ANDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_AICONTRO = space(15)
      endif
      this.w_DESCONTRO = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AICONTRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_10.ANCODICE as ANCODICE210"+ ",link_2_10.ANDESCRI as ANDESCRI210"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_10 on CAUIVA.AICONTRO=link_2_10.ANCODICE"+" and CAUIVA.AITIPCOP=link_2_10.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_10"
          i_cKey=i_cKey+'+" and CAUIVA.AICONTRO=link_2_10.ANCODICE(+)"'+'+" and CAUIVA.AITIPCOP=link_2_10.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oAITIPCOP_2_3.value==this.w_AITIPCOP)
      this.oPgFrm.Page1.oPag.oAITIPCOP_2_3.value=this.w_AITIPCOP
      replace t_AITIPCOP with this.oPgFrm.Page1.oPag.oAITIPCOP_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oAICONTRO_2_10.value==this.w_AICONTRO)
      this.oPgFrm.Page1.oPag.oAICONTRO_2_10.value=this.w_AICONTRO
      replace t_AICONTRO with this.oPgFrm.Page1.oPag.oAICONTRO_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAPP1_2_13.value==this.w_DESAPP1)
      this.oPgFrm.Page1.oPag.oDESAPP1_2_13.value=this.w_DESAPP1
      replace t_DESAPP1 with this.oPgFrm.Page1.oPag.oDESAPP1_2_13.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCONTRO_2_14.value==this.w_DESCONTRO)
      this.oPgFrm.Page1.oPag.oDESCONTRO_2_14.value=this.w_DESCONTRO
      replace t_DESCONTRO with this.oPgFrm.Page1.oPag.oDESCONTRO_2_14.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDESAPP_2_15.value==this.w_DESAPP)
      this.oPgFrm.Page1.oPag.oDESAPP_2_15.value=this.w_DESAPP
      replace t_DESAPP with this.oPgFrm.Page1.oPag.oDESAPP_2_15.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAICODIVA_2_1.value==this.w_AICODIVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAICODIVA_2_1.value=this.w_AICODIVA
      replace t_AICODIVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAICODIVA_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESIVA_2_4.value==this.w_DESIVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESIVA_2_4.value=this.w_DESIVA
      replace t_DESIVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESIVA_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAICONDET_2_5.value==this.w_AICONDET)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAICONDET_2_5.value=this.w_AICONDET
      replace t_AICONDET with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAICONDET_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAITIPREG_2_6.RadioValue()==this.w_AITIPREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAITIPREG_2_6.SetRadio()
      replace t_AITIPREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAITIPREG_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAINUMREG_2_7.value==this.w_AINUMREG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAINUMREG_2_7.value=this.w_AINUMREG
      replace t_AINUMREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAINUMREG_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPERIND_2_8.value==this.w_PERIND)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPERIND_2_8.value=this.w_PERIND
      replace t_PERIND with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPERIND_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAICONIND_2_9.value==this.w_AICONIND)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAICONIND_2_9.value=this.w_AICONIND
      replace t_AICONIND with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAICONIND_2_9.value
    endif
    cp_SetControlsValueExtFlds(this,'CAUIVA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and not(empty(.w_AICODIVA)) and (.w_AICODIVA<>space(5) AND (.w_AICONDET<>SPACE(15) OR .w_AICONIND<>SPACE(15)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAICODIVA_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice IVA obsoleto oppure inesistente")
        case   not(.w_AITIPCOP$"CFG") and (g_DETCON='S') and (.w_AICODIVA<>space(5) AND (.w_AICONDET<>SPACE(15) OR .w_AICONIND<>SPACE(15)))
          .oNewFocus=.oPgFrm.Page1.oPag.oAITIPCOP_2_3
          i_bRes = .f.
          i_bnoChk = .f.
        case   not((.w_TIPSOT='I' OR EMPTY(.w_AICONDET)) AND (EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)) and (NOT EMPTY(.w_AICODIVA)) and not(empty(.w_AICONDET)) and (.w_AICODIVA<>space(5) AND (.w_AICONDET<>SPACE(15) OR .w_AICONIND<>SPACE(15)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAICONDET_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Conto inesistente/obsoleto o di tipo non IVA")
        case   not(NOT (.w_AITIPREG<>'N' AND NOT EMPTY(.w_AICODIVA)) OR CHKREGIV(.w_AITIPREG,.w_AINUMREG)=1) and (.w_AITIPREG<>'N' AND NOT EMPTY(.w_AICODIVA)) and (.w_AICODIVA<>space(5) AND (.w_AICONDET<>SPACE(15) OR .w_AICONIND<>SPACE(15)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAINUMREG_2_7
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Il registro � associato a pi� attivit� o a nessuna attivit�")
        case   not((.w_TIPSO2<>'I' OR EMPTY(.w_AICONIND)) AND (EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)) and (.w_PERIND<>0 AND NOT EMPTY(.w_AICODIVA)) and not(empty(.w_AICONIND)) and (.w_AICODIVA<>space(5) AND (.w_AICONDET<>SPACE(15) OR .w_AICONIND<>SPACE(15)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAICONIND_2_9
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Conto indetraibile inesistente/obsoleto o di tipo IVA")
      endcase
      if .w_AICODIVA<>space(5) AND (.w_AICONDET<>SPACE(15) OR .w_AICONIND<>SPACE(15))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AICODIVA = this.w_AICODIVA
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(t_AICODIVA<>space(5) AND (t_AICONDET<>SPACE(15) OR t_AICONIND<>SPACE(15)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_AICODIVA=space(5)
      .w_AITIPCON=space(1)
      .w_AITIPCOP=space(1)
      .w_DESIVA=space(35)
      .w_AICONDET=space(15)
      .w_AITIPREG=space(1)
      .w_AINUMREG=0
      .w_PERIND=0
      .w_AICONIND=space(15)
      .w_AICONTRO=space(15)
      .w_TIPSOT=space(1)
      .w_TIPSO2=space(1)
      .w_DESAPP1=space(40)
      .w_DESCONTRO=space(60)
      .w_DESAPP=space(40)
      .DoRTCalc(1,6,.f.)
      if not(empty(.w_AICODIVA))
        .link_2_1('Full')
      endif
        .w_AITIPCON = 'G'
        .w_AITIPCOP = 'G'
      .DoRTCalc(9,10,.f.)
      if not(empty(.w_AICONDET))
        .link_2_5('Full')
      endif
        .w_AITIPREG = this.oParentObject .w_CTIPREG
        .w_AINUMREG = this.oParentObject .w_CNUMREG
      .DoRTCalc(13,13,.f.)
        .w_AICONIND = space(15)
      .DoRTCalc(14,14,.f.)
      if not(empty(.w_AICONIND))
        .link_2_9('Full')
      endif
      .DoRTCalc(15,15,.f.)
      if not(empty(.w_AICONTRO))
        .link_2_10('Full')
      endif
    endwith
    this.DoRTCalc(16,20,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_AICODIVA = t_AICODIVA
    this.w_AITIPCON = t_AITIPCON
    this.w_AITIPCOP = t_AITIPCOP
    this.w_DESIVA = t_DESIVA
    this.w_AICONDET = t_AICONDET
    this.w_AITIPREG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAITIPREG_2_6.RadioValue(.t.)
    this.w_AINUMREG = t_AINUMREG
    this.w_PERIND = t_PERIND
    this.w_AICONIND = t_AICONIND
    this.w_AICONTRO = t_AICONTRO
    this.w_TIPSOT = t_TIPSOT
    this.w_TIPSO2 = t_TIPSO2
    this.w_DESAPP1 = t_DESAPP1
    this.w_DESCONTRO = t_DESCONTRO
    this.w_DESAPP = t_DESAPP
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_AICODIVA with this.w_AICODIVA
    replace t_AITIPCON with this.w_AITIPCON
    replace t_AITIPCOP with this.w_AITIPCOP
    replace t_DESIVA with this.w_DESIVA
    replace t_AICONDET with this.w_AICONDET
    replace t_AITIPREG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oAITIPREG_2_6.ToRadio()
    replace t_AINUMREG with this.w_AINUMREG
    replace t_PERIND with this.w_PERIND
    replace t_AICONIND with this.w_AICONIND
    replace t_AICONTRO with this.w_AICONTRO
    replace t_TIPSOT with this.w_TIPSOT
    replace t_TIPSO2 with this.w_TIPSO2
    replace t_DESAPP1 with this.w_DESAPP1
    replace t_DESCONTRO with this.w_DESCONTRO
    replace t_DESAPP with this.w_DESAPP
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscg_maiPag1 as StdContainer
  Width  = 803
  height = 126
  stdWidth  = 803
  stdheight = 126
  resizeXpos=263
  resizeYpos=81
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=0, width=792,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=7,Field1="AICODIVA",Label1="Cod.IVA",Field2="DESIVA",Label2="Descrizione",Field3="AICONDET",Label3="Conto IVA",Field4="AITIPREG",Label4="Tipo registro IVA",Field5="AINUMREG",Label5="N.reg.",Field6="PERIND",Label6="%Ind.",Field7="AICONIND",Label7="Conto indetraibile",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 83032966

  add object oStr_1_6 as StdString with uid="TBRCDFLYPF",Visible=.t., Left=8, Top=100,;
    Alignment=1, Width=77, Height=18,;
    Caption="Contropartita:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (g_DETCON<>'S')
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="QCLVJJCTXA",Visible=.t., Left=595, Top=100,;
    Alignment=1, Width=47, Height=18,;
    Caption="IVA ind.:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (isahe())
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="GEFWSJUMUQ",Visible=.t., Left=394, Top=100,;
    Alignment=0, Width=44, Height=18,;
    Caption="IVA detr:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=19,;
    width=784+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*4*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=20,width=783+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*4*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='VOCIIVA|CONTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oAITIPCOP_2_3.Refresh()
      this.Parent.oAICONTRO_2_10.Refresh()
      this.Parent.oDESAPP1_2_13.Refresh()
      this.Parent.oDESCONTRO_2_14.Refresh()
      this.Parent.oDESAPP_2_15.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='VOCIIVA'
        oDropInto=this.oBodyCol.oRow.oAICODIVA_2_1
      case cFile='CONTI'
        oDropInto=this.oBodyCol.oRow.oAICONDET_2_5
      case cFile='CONTI'
        oDropInto=this.oBodyCol.oRow.oAICONIND_2_9
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oAITIPCOP_2_3 as StdTrsField with uid="SJSVIICDYJ",rtseq=8,rtrep=.t.,;
    cFormVar="w_AITIPCOP",value=space(1),;
    ToolTipText = "Tipo conto di contropartita associato alla riga IVA",;
    HelpContextID = 8677462,;
    cTotal="", bFixedPos=.t., cQueryName = "AITIPCOP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=91, Top=100, cSayPict=["!"], cGetPict=["!"], InputMask=replicate('X',1)

  func oAITIPCOP_2_3.mCond()
    with this.Parent.oContained
      return (g_DETCON='S')
    endwith
  endfunc

  func oAITIPCOP_2_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_DETCON<>'S')
    endwith
    endif
  endfunc

  func oAITIPCOP_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_AITIPCOP$"CFG")
      if .not. empty(.w_AICONTRO)
        bRes2=.link_2_10('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oAICONTRO_2_10 as StdTrsField with uid="TUHYRUACIM",rtseq=15,rtrep=.t.,;
    cFormVar="w_AICONTRO",value=space(15),;
    ToolTipText = "Conto di contropartita associato alla riga IVA",;
    HelpContextID = 23681109,;
    cTotal="", bFixedPos=.t., cQueryName = "AICONTRO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=113, Top=100, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_AITIPCOP", oKey_2_1="ANCODICE", oKey_2_2="this.w_AICONTRO"

  func oAICONTRO_2_10.mCond()
    with this.Parent.oContained
      return (g_DETCON='S')
    endwith
  endfunc

  func oAICONTRO_2_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_DETCON<>'S')
    endwith
    endif
  endfunc

  func oAICONTRO_2_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oAICONTRO_2_10.ecpDrop(oSource)
    this.Parent.oContained.link_2_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oAICONTRO_2_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_AITIPCOP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_AITIPCOP)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oAICONTRO_2_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti/fornitori/conti",'',this.parent.oContained
  endproc
  proc oAICONTRO_2_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_AITIPCOP
     i_obj.w_ANCODICE=this.parent.oContained.w_AICONTRO
    i_obj.ecpSave()
  endproc

  add object oDESAPP1_2_13 as StdTrsField with uid="HGLSQZCVFY",rtseq=18,rtrep=.t.,;
    cFormVar="w_DESAPP1",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione conto IVA indetraibile",;
    HelpContextID = 226251830,;
    cTotal="", bFixedPos=.t., cQueryName = "DESAPP1",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=150, Left=645, Top=100, InputMask=replicate('X',40), readonly=.t.,TabStop=.f.

  func oDESAPP1_2_13.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (isahe())
    endwith
    endif
  endfunc

  add object oDESCONTRO_2_14 as StdTrsField with uid="CHVMCHHIZX",rtseq=19,rtrep=.t.,;
    cFormVar="w_DESCONTRO",value=space(60),enabled=.f.,;
    HelpContextID = 76654216,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCONTRO",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=150, Left=232, Top=100, InputMask=replicate('X',60)

  func oDESCONTRO_2_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_DETCON<>'S' or isahe())
    endwith
    endif
  endfunc

  add object oDESAPP_2_15 as StdTrsField with uid="OZVQMYZUPF",rtseq=20,rtrep=.t.,;
    cFormVar="w_DESAPP",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione conto IVA detraibile",;
    HelpContextID = 226251830,;
    cTotal="", bFixedPos=.t., cQueryName = "DESAPP",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=150, Left=440, Top=100, InputMask=replicate('X',40), readonly=.t.,TabStop=.f.

  func oDESAPP_2_15.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (isahe())
    endwith
    endif
  endfunc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_maiBodyRow as CPBodyRowCnt
  Width=774
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oAICODIVA_2_1 as StdTrsField with uid="JYECFLVLSI",rtseq=6,rtrep=.t.,;
    cFormVar="w_AICODIVA",value=space(5),;
    ToolTipText = "Codice IVA",;
    HelpContextID = 171354041,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA obsoleto oppure inesistente",;
   bGlobalFont=.t.,;
    Height=17, Width=62, Left=-2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_AICODIVA"

  func oAICODIVA_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oAICODIVA_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oAICODIVA_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oAICODIVA_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'',this.parent.oContained
  endproc
  proc oAICODIVA_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_AICODIVA
    i_obj.ecpSave()
  endproc

  add object oDESIVA_2_4 as StdTrsField with uid="TVDHUSYROF",rtseq=9,rtrep=.t.,;
    cFormVar="w_DESIVA",value=space(35),enabled=.f.,;
    HelpContextID = 249844790,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=167, Left=63, Top=0, InputMask=replicate('X',35)

  add object oAICONDET_2_5 as StdTrsField with uid="UFCSVETIYT",rtseq=10,rtrep=.t.,;
    cFormVar="w_AICONDET",value=space(15),;
    ToolTipText = "Conto IVA detraibile",;
    HelpContextID = 244754342,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente/obsoleto o di tipo non IVA",;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=233, Top=0, cSayPict=[p_CON], cGetPict=[p_CON], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_AITIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_AICONDET"

  func oAICONDET_2_5.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICODIVA))
    endwith
  endfunc

  func oAICONDET_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oAICONDET_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oAICONDET_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_AITIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_AITIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oAICONDET_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_API',"Conti IVA",'GSCG_MAI.CONTI_VZM',this.parent.oContained
  endproc
  proc oAICONDET_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_API()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_AITIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_AICONDET
    i_obj.ecpSave()
  endproc

  add object oAITIPREG_2_6 as StdTrsCombo with uid="TFNVOSFDXV",rtrep=.t.,;
    cFormVar="w_AITIPREG", RowSource=""+"Vendite,"+"Acquisti,"+"Corr.scorporo,"+"Corr.ventilaz.,"+"No IVA" , ;
    ToolTipText = "Tipo registro IVA",;
    HelpContextID = 8099763,;
    Height=21, Width=93, Left=376, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oAITIPREG_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..AITIPREG,&i_cF..t_AITIPREG),this.value)
    return(iif(xVal =1,'V',;
    iif(xVal =2,'A',;
    iif(xVal =3,'C',;
    iif(xVal =4,'E',;
    iif(xVal =5,'N',;
    space(1)))))))
  endfunc
  func oAITIPREG_2_6.GetRadio()
    this.Parent.oContained.w_AITIPREG = this.RadioValue()
    return .t.
  endfunc

  func oAITIPREG_2_6.ToRadio()
    this.Parent.oContained.w_AITIPREG=trim(this.Parent.oContained.w_AITIPREG)
    return(;
      iif(this.Parent.oContained.w_AITIPREG=='V',1,;
      iif(this.Parent.oContained.w_AITIPREG=='A',2,;
      iif(this.Parent.oContained.w_AITIPREG=='C',3,;
      iif(this.Parent.oContained.w_AITIPREG=='E',4,;
      iif(this.Parent.oContained.w_AITIPREG=='N',5,;
      0))))))
  endfunc

  func oAITIPREG_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oAITIPREG_2_6.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_AICODIVA))
    endwith
  endfunc

  add object oAINUMREG_2_7 as StdTrsField with uid="JOPGEQDYGM",rtseq=12,rtrep=.t.,;
    cFormVar="w_AINUMREG",value=0,;
    ToolTipText = "Numero registro IVA",;
    HelpContextID = 10483635,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Il registro � associato a pi� attivit� o a nessuna attivit�",;
   bGlobalFont=.t.,;
    Height=17, Width=37, Left=520, Top=0, cSayPict=["99"], cGetPict=["99"]

  func oAINUMREG_2_7.mCond()
    with this.Parent.oContained
      return (.w_AITIPREG<>'N' AND NOT EMPTY(.w_AICODIVA))
    endwith
  endfunc

  func oAINUMREG_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT (.w_AITIPREG<>'N' AND NOT EMPTY(.w_AICODIVA)) OR CHKREGIV(.w_AITIPREG,.w_AINUMREG)=1)
    endwith
    return bRes
  endfunc

  add object oPERIND_2_8 as StdTrsField with uid="UVBBVMNBSQ",rtseq=13,rtrep=.t.,;
    cFormVar="w_PERIND",value=0,enabled=.f.,;
    HelpContextID = 23348470,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=54, Left=573, Top=0

  add object oAICONIND_2_9 as StdTrsField with uid="JNDIGHGRGX",rtseq=14,rtrep=.t.,;
    cFormVar="w_AICONIND",value=space(15),;
    ToolTipText = "Conto IVA indetraibile",;
    HelpContextID = 107567178,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Conto indetraibile inesistente/obsoleto o di tipo IVA",;
   bGlobalFont=.t.,;
    Height=17, Width=135, Left=634, Top=0, cSayPict=[p_CON], cGetPict=[p_CON], InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_AITIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_AICONIND"

  func oAICONIND_2_9.mCond()
    with this.Parent.oContained
      return (.w_PERIND<>0 AND NOT EMPTY(.w_AICODIVA))
    endwith
  endfunc

  func oAICONIND_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oAICONIND_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oAICONIND_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_AITIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_AITIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oAICONIND_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_API',"Conti indetraibili",'GSCG1MAI.CONTI_VZM',this.parent.oContained
  endproc
  proc oAICONIND_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_API()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_AITIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_AICONIND
    i_obj.ecpSave()
  endproc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oAICODIVA_2_1.When()
    return(.t.)
  proc oAICODIVA_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oAICODIVA_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=3
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mai','CAUIVA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AICODCAU=CAUIVA.AICODCAU";
  +" and "+i_cAliasName2+".AITIPCLF=CAUIVA.AITIPCLF";
  +" and "+i_cAliasName2+".AICODCLF=CAUIVA.AICODCLF";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
