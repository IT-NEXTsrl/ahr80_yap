* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_mcd                                                        *
*              Compilazione distinte                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_253]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-01                                                      *
* Last revis.: 2015-10-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_mcd"))

* --- Class definition
define class tgste_mcd as StdTrsForm
  Top    = 17
  Left   = 21

  * --- Standard Properties
  Width  = 819
  Height = 445+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-26"
  HelpContextID=97137769
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=95

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DIS_COMP_IDX = 0
  CONTI_IDX = 0
  CAU_CONT_IDX = 0
  CAU_DIST_IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  COC_MAST_IDX = 0
  CONTROPA_IDX = 0
  CATECOMM_IDX = 0
  ZONE_IDX = 0
  MASTRI_IDX = 0
  TIP_DIST_IDX = 0
  cFile = "DIS_COMP"
  cKeySelect = "DISERIAL"
  cKeyWhere  = "DISERIAL=this.w_DISERIAL"
  cKeyDetail  = "DISERIAL=this.w_DISERIAL"
  cKeyWhereODBC = '"DISERIAL="+cp_ToStrODBC(this.w_DISERIAL)';

  cKeyDetailWhereODBC = '"DISERIAL="+cp_ToStrODBC(this.w_DISERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DIS_COMP.DISERIAL="+cp_ToStrODBC(this.w_DISERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DIS_COMP.CPROWNUM '
  cPrg = "gste_mcd"
  cComment = "Compilazione distinte"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DISERIAL = space(10)
  w_CODAZI = space(5)
  w_VALNAZ = space(3)
  w_DATDIS = ctod('  /  /  ')
  o_DATDIS = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_CODESE = space(4)
  w_CAURIF = space(5)
  o_CAURIF = space(5)
  w_DESRIF = space(35)
  w_CATIPMAN = space(1)
  w_DICAUBON = space(5)
  w_DITIPMAN = space(1)
  w_DESCRI = space(35)
  w_SCAINI = ctod('  /  /  ')
  w_SCAFIN = ctod('  /  /  ')
  w_CODVAL = space(3)
  o_CODVAL = space(3)
  w_SIMVAL = space(5)
  w_DECTOT = 0
  w_TESVAL = 0
  w_DESAPP = space(35)
  w_CALCPICT = 0
  w_CAOVAL = 0
  w_SCOTRA = 0
  w_FLAVVI = space(1)
  w_DATVAL = ctod('  /  /  ')
  o_DATVAL = ctod('  /  /  ')
  w_FLCABI = space(1)
  w_TOTPAR = 0
  w_DATOBSO = space(10)
  w_IMPCOM = 0
  w_TIPSCA = space(1)
  w_TIPDIS = space(2)
  o_TIPDIS = space(2)
  w_FLBANC = space(1)
  w_DITIPCON = space(1)
  w_DICODCON = space(15)
  o_DICODCON = space(15)
  w_DIIMPPRO = 0
  o_DIIMPPRO = 0
  w_DISPECOM = 0
  w_DIIMPCAL = 0
  w_BAOBSO = ctod('  /  /  ')
  w_DESBAC = space(40)
  w_TOTALI = 0
  w_READPAR = space(5)
  w_CODCAU = space(5)
  o_CODCAU = space(5)
  w_DESCAU = space(35)
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_CODCON = space(15)
  w_CODBAN = space(15)
  w_DESCCON = space(40)
  w_DESBAN = space(40)
  w_DATDOCINI = ctod('  /  /  ')
  w_PTIPSEL = space(1)
  w_DATDOCFIN = ctod('  /  /  ')
  w_NUMDOCINI = 0
  w_NUMDOCFIN = 0
  w_ALFDOCINI = space(10)
  w_CATCOM = space(3)
  w_CODZON = space(3)
  w_CONSUP = space(15)
  w_TIPSEL = space(1)
  w_IMPMIN = 0
  w_FLRAGG = space(1)
  w_TIPREG = space(1)
  w_TIPBAN = space(1)
  w_PARTSAN = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATAOBSO = ctod('  /  /  ')
  w_DTOBSO = ctod('  /  /  ')
  w_IBAN = space(35)
  w_BBAN = space(30)
  w_BATIPO = space(1)
  w_FLRD1 = space(2)
  w_FLRI1 = space(2)
  w_FLMA1 = space(2)
  w_FLRB1 = space(2)
  w_FLBO1 = space(2)
  w_FLCA1 = space(2)
  w_SCELTA = space(1)
  w_TIPO = space(1)
  w_NUMDIS = space(10)
  w_CODCON1 = space(15)
  w_FLIBAN = space(1)
  w_CONSBF = space(1)
  w_CONSBF1 = space(1)
  w_CODABI = space(5)
  w_ALFDOCFIN = space(10)
  w_CDESCRI = space(35)
  w_ZDESCRI = space(35)
  w_MDESCRI = space(40)
  w_TIPMAS = space(1)
  w_NUMLIV = 0
  w_DATEFF = space(1)
  w_TOTALI1 = 0
  w_TOTRES1 = 0
  w_TOTRES = 0
  w_CODISO = space(3)
  w_CATIPDIS = space(2)
  w_TEST1 = .F.
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gste_mcd
  OkGene =.f.
  
    * --- Disattiva la Caption dell'elenco
    proc ecpLoad()
          * ----
          DoDefault()
          this.oPgFrm.Pages(3).caption=''
    endproc
  
  * ---- Disattiva i metodi Edit e Delete (posso solo variare)
  * ---- Sbianca la Caption della finestra
  Func ah_HasCPEvents(i_cOp)
   Return Not (Upper(i_cop)='ECPEDIT') Or (Upper(i_cop)='ECPDELETE')
  EndFunc
  
  * --- Salva
  proc ecpSave()
           * ----
           if this.CheckForm()
                 this.OkGene=.f.
                 this.NotifyEvent('Conferma')
                 if this.OkGene=.t.
                      this.bUpdated=.f.
                      this.ecpQuit()
                 endif
           endif
  endproc
  
  
    * --- Esce Subito
    proc ecpQuit()
          * ----
          DoDefault()
          If This.cFunction='Query'
           This.bUpdated=.f.
           Keyboard "{ESC}"
          Endif
  
    endproc
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DIS_COMP','gste_mcd')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_mcdPag1","gste_mcd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Compilazione")
      .Pages(1).HelpContextID = 229205296
      .Pages(2).addobject("oPag","tgste_mcdPag2")
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Parametri aggiuntivi")
      .Pages(2).HelpContextID = 113007014
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[12]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CAU_CONT'
    this.cWorkTables[3]='CAU_DIST'
    this.cWorkTables[4]='ESERCIZI'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='COC_MAST'
    this.cWorkTables[7]='CONTROPA'
    this.cWorkTables[8]='CATECOMM'
    this.cWorkTables[9]='ZONE'
    this.cWorkTables[10]='MASTRI'
    this.cWorkTables[11]='TIP_DIST'
    this.cWorkTables[12]='DIS_COMP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(12))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DIS_COMP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DIS_COMP_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_DISERIAL = NVL(DISERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_2_joined
    link_2_2_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DIS_COMP where DISERIAL=KeySet.DISERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DIS_COMP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_COMP_IDX,2],this.bLoadRecFilter,this.DIS_COMP_IDX,"gste_mcd")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DIS_COMP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DIS_COMP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DIS_COMP '
      link_2_2_joined=this.AddJoinedLink_2_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DISERIAL',this.w_DISERIAL  )
      select * from (i_cTable) DIS_COMP where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI = i_CODAZI
        .w_VALNAZ = g_PERVAL
        .w_DATDIS = i_datsys
        .w_CODESE = g_CODESE
        .w_CAURIF = space(5)
        .w_DESRIF = space(35)
        .w_CATIPMAN = space(1)
        .w_DESCRI = space(35)
        .w_SCAINI = .w_DATDIS
        .w_CODVAL = space(3)
        .w_SIMVAL = space(5)
        .w_DECTOT = 0
        .w_TESVAL = 0
        .w_DESAPP = space(35)
        .w_FLAVVI = space(1)
        .w_TOTPAR = 0
        .w_DATOBSO = space(10)
        .w_IMPCOM = 0
        .w_TIPSCA = ''
        .w_TIPDIS = ''
        .w_FLBANC = space(1)
        .w_TOTALI = 0
        .w_READPAR = .w_CODAZI
        .w_CODCAU = space(5)
        .w_DESCAU = space(35)
        .w_TIPCON = ''
        .w_CODBAN = space(15)
        .w_DESCCON = space(40)
        .w_DESBAN = space(40)
        .w_DATDOCINI = ctod("  /  /  ")
        .w_PTIPSEL = space(1)
        .w_DATDOCFIN = ctod("  /  /  ")
        .w_NUMDOCINI = 0
        .w_NUMDOCFIN = 0
        .w_ALFDOCINI = space(10)
        .w_CATCOM = space(3)
        .w_CODZON = space(3)
        .w_CONSUP = space(15)
        .w_TIPSEL = .w_PTIPSEL
        .w_IMPMIN = 0
        .w_FLRAGG = 'N'
        .w_TIPREG = space(1)
        .w_PARTSAN = space(1)
        .w_OBTEST = i_datsys
        .w_DATAOBSO = ctod("  /  /  ")
        .w_DTOBSO = ctod("  /  /  ")
        .w_IBAN = space(35)
        .w_BBAN = space(30)
        .w_FLRD1 = space(2)
        .w_FLRI1 = space(2)
        .w_FLMA1 = space(2)
        .w_FLRB1 = space(2)
        .w_FLBO1 = space(2)
        .w_FLCA1 = space(2)
        .w_TIPO = space(1)
        .w_CODCON1 = space(15)
        .w_FLIBAN = space(1)
        .w_CONSBF = space(1)
        .w_CONSBF1 = space(1)
        .w_ALFDOCFIN = space(10)
        .w_CDESCRI = space(35)
        .w_ZDESCRI = space(35)
        .w_MDESCRI = space(40)
        .w_TIPMAS = space(1)
        .w_NUMLIV = 0
        .w_DATEFF = space(1)
        .w_TOTALI1 = 0
        .w_CODISO = g_ISONAZ
        .w_TEST1 = .F.
        .w_DISERIAL = NVL(DISERIAL,space(10))
        .w_OBTEST = .w_DATDIS
          .link_1_6('Load')
          .link_1_7('Load')
        .w_DICAUBON = NVL(DICAUBON,space(5))
          * evitabile
          *.link_1_10('Load')
        .w_DITIPMAN = NVL(DITIPMAN,space(1))
        .w_SCAFIN = .w_DATDIS+120
          .link_1_15('Load')
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CAOVAL = IIF(EMPTY(.w_CODVAL) OR EMPTY(.w_DATVAL), 0, GETCAM(.w_CODVAL, .w_DATVAL, 7))
        .w_SCOTRA = 0
        .w_DATVAL = .w_DATDIS
        .w_FLCABI = ' '
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate(IIF(.w_TIPSCA='C', ah_MsgFormat("Attive"), IIF(.w_TIPSCA='F', ah_MsgFormat("Passive"),'')),'','')
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate(FUNDIS(.w_TIPDIS),'','')
          .link_4_1('Load')
          .link_4_2('Load')
        .w_CODCON = SPACE(15)
          .link_4_5('Load')
          .link_4_6('Load')
          .link_4_15('Load')
          .link_4_16('Load')
          .link_4_17('Load')
        .w_TIPBAN = 'G'
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .w_SCELTA = 'A'
        .w_NUMDIS = 'ZZZZZZZZZZ'
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate(Alltrim(IIF(.w_FLRB1='RB', 'R.B./RIBA',''))+' ' +Alltrim(IIF(.w_FLRD1='RD', 'Rim.Dir.',''))+ ' '+Alltrim(IIF(.w_FLMA1='MA', 'M.AV.',''))+ ' '+ Alltrim(iif(.w_FLRI1 $ 'RI-SD', 'R.I.D.', ''))+ ' '+ Alltrim(iif(.w_FLBO1 $ 'BO-SC-SE', 'Bonifico',''))+ ' ' +Alltrim(IIF(.w_FLCA1='CA', 'Cambiali', '')))
        .w_TOTRES1 = .w_TOTPAR-.w_TOTALI1
        .w_TOTRES = .w_TOTPAR-.w_TOTALI
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_CATIPDIS = .w_TIPDIS
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        cp_LoadRecExtFlds(this,'DIS_COMP')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTALI = 0
      this.w_TOTALI1 = 0
      scan
        with this
          .w_BAOBSO = ctod("  /  /  ")
          .w_DESBAC = space(40)
          .w_BATIPO = space(1)
          .w_CODABI = space(5)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_DITIPCON = NVL(DITIPCON,space(1))
          .w_DICODCON = NVL(DICODCON,space(15))
          if link_2_2_joined
            this.w_DICODCON = NVL(BACODBAN202,NVL(this.w_DICODCON,space(15)))
            this.w_DESBAC = NVL(BADESCRI202,space(40))
            this.w_BAOBSO = NVL(cp_ToDate(BADTOBSO202),ctod("  /  /  "))
            this.w_IBAN = NVL(BA__IBAN202,space(35))
            this.w_BBAN = NVL(BA__BBAN202,space(30))
            this.w_BATIPO = NVL(BATIPCON202,space(1))
            this.w_CONSBF1 = NVL(BACONSBF202,space(1))
            this.w_CODABI = NVL(BACODABI202,space(5))
          else
          .link_2_2('Load')
          endif
          .w_DIIMPPRO = NVL(DIIMPPRO,0)
          .w_DISPECOM = NVL(DISPECOM,0)
          .w_DIIMPCAL = NVL(DIIMPCAL,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTALI = .w_TOTALI+.w_DIIMPCAL
          .w_TOTALI1 = .w_TOTALI1+.w_DIIMPPRO
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_OBTEST = .w_DATDIS
        .w_SCAFIN = .w_DATDIS+120
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CAOVAL = IIF(EMPTY(.w_CODVAL) OR EMPTY(.w_DATVAL), 0, GETCAM(.w_CODVAL, .w_DATVAL, 7))
        .w_SCOTRA = 0
        .w_DATVAL = .w_DATDIS
        .w_FLCABI = ' '
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate(IIF(.w_TIPSCA='C', ah_MsgFormat("Attive"), IIF(.w_TIPSCA='F', ah_MsgFormat("Passive"),'')),'','')
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate(FUNDIS(.w_TIPDIS),'','')
        .w_CODCON = SPACE(15)
        .w_TIPBAN = 'G'
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .w_SCELTA = 'A'
        .w_NUMDIS = 'ZZZZZZZZZZ'
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate(Alltrim(IIF(.w_FLRB1='RB', 'R.B./RIBA',''))+' ' +Alltrim(IIF(.w_FLRD1='RD', 'Rim.Dir.',''))+ ' '+Alltrim(IIF(.w_FLMA1='MA', 'M.AV.',''))+ ' '+ Alltrim(iif(.w_FLRI1 $ 'RI-SD', 'R.I.D.', ''))+ ' '+ Alltrim(iif(.w_FLBO1 $ 'BO-SC-SE', 'Bonifico',''))+ ' ' +Alltrim(IIF(.w_FLCA1='CA', 'Cambiali', '')))
        .w_TOTRES1 = .w_TOTPAR-.w_TOTALI1
        .w_TOTRES = .w_TOTPAR-.w_TOTALI
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_CATIPDIS = .w_TIPDIS
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DISERIAL=space(10)
      .w_CODAZI=space(5)
      .w_VALNAZ=space(3)
      .w_DATDIS=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_CODESE=space(4)
      .w_CAURIF=space(5)
      .w_DESRIF=space(35)
      .w_CATIPMAN=space(1)
      .w_DICAUBON=space(5)
      .w_DITIPMAN=space(1)
      .w_DESCRI=space(35)
      .w_SCAINI=ctod("  /  /  ")
      .w_SCAFIN=ctod("  /  /  ")
      .w_CODVAL=space(3)
      .w_SIMVAL=space(5)
      .w_DECTOT=0
      .w_TESVAL=0
      .w_DESAPP=space(35)
      .w_CALCPICT=0
      .w_CAOVAL=0
      .w_SCOTRA=0
      .w_FLAVVI=space(1)
      .w_DATVAL=ctod("  /  /  ")
      .w_FLCABI=space(1)
      .w_TOTPAR=0
      .w_DATOBSO=space(10)
      .w_IMPCOM=0
      .w_TIPSCA=space(1)
      .w_TIPDIS=space(2)
      .w_FLBANC=space(1)
      .w_DITIPCON=space(1)
      .w_DICODCON=space(15)
      .w_DIIMPPRO=0
      .w_DISPECOM=0
      .w_DIIMPCAL=0
      .w_BAOBSO=ctod("  /  /  ")
      .w_DESBAC=space(40)
      .w_TOTALI=0
      .w_READPAR=space(5)
      .w_CODCAU=space(5)
      .w_DESCAU=space(35)
      .w_TIPCON=space(1)
      .w_CODCON=space(15)
      .w_CODBAN=space(15)
      .w_DESCCON=space(40)
      .w_DESBAN=space(40)
      .w_DATDOCINI=ctod("  /  /  ")
      .w_PTIPSEL=space(1)
      .w_DATDOCFIN=ctod("  /  /  ")
      .w_NUMDOCINI=0
      .w_NUMDOCFIN=0
      .w_ALFDOCINI=space(10)
      .w_CATCOM=space(3)
      .w_CODZON=space(3)
      .w_CONSUP=space(15)
      .w_TIPSEL=space(1)
      .w_IMPMIN=0
      .w_FLRAGG=space(1)
      .w_TIPREG=space(1)
      .w_TIPBAN=space(1)
      .w_PARTSAN=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATAOBSO=ctod("  /  /  ")
      .w_DTOBSO=ctod("  /  /  ")
      .w_IBAN=space(35)
      .w_BBAN=space(30)
      .w_BATIPO=space(1)
      .w_FLRD1=space(2)
      .w_FLRI1=space(2)
      .w_FLMA1=space(2)
      .w_FLRB1=space(2)
      .w_FLBO1=space(2)
      .w_FLCA1=space(2)
      .w_SCELTA=space(1)
      .w_TIPO=space(1)
      .w_NUMDIS=space(10)
      .w_CODCON1=space(15)
      .w_FLIBAN=space(1)
      .w_CONSBF=space(1)
      .w_CONSBF1=space(1)
      .w_CODABI=space(5)
      .w_ALFDOCFIN=space(10)
      .w_CDESCRI=space(35)
      .w_ZDESCRI=space(35)
      .w_MDESCRI=space(40)
      .w_TIPMAS=space(1)
      .w_NUMLIV=0
      .w_DATEFF=space(1)
      .w_TOTALI1=0
      .w_TOTRES1=0
      .w_TOTRES=0
      .w_CODISO=space(3)
      .w_CATIPDIS=space(2)
      .w_TEST1=.f.
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_CODAZI = i_CODAZI
        .w_VALNAZ = g_PERVAL
        .w_DATDIS = i_datsys
        .w_OBTEST = .w_DATDIS
        .w_CODESE = g_CODESE
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODESE))
         .link_1_6('Full')
        endif
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_CAURIF))
         .link_1_7('Full')
        endif
        .DoRTCalc(8,10,.f.)
        if not(empty(.w_DICAUBON))
         .link_1_10('Full')
        endif
        .w_DITIPMAN = iif(.w_FLRI1='RI',.w_CATIPMAN,'T')
        .DoRTCalc(12,12,.f.)
        .w_SCAINI = .w_DATDIS
        .w_SCAFIN = .w_DATDIS+120
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CODVAL))
         .link_1_15('Full')
        endif
        .DoRTCalc(16,19,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CAOVAL = IIF(EMPTY(.w_CODVAL) OR EMPTY(.w_DATVAL), 0, GETCAM(.w_CODVAL, .w_DATVAL, 7))
        .w_SCOTRA = 0
        .DoRTCalc(23,23,.f.)
        .w_DATVAL = .w_DATDIS
        .w_FLCABI = ' '
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate(IIF(.w_TIPSCA='C', ah_MsgFormat("Attive"), IIF(.w_TIPSCA='F', ah_MsgFormat("Passive"),'')),'','')
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate(FUNDIS(.w_TIPDIS),'','')
        .DoRTCalc(26,28,.f.)
        .w_TIPSCA = ''
        .w_TIPDIS = ''
        .DoRTCalc(31,31,.f.)
        .w_DITIPCON = 'G'
        .DoRTCalc(33,33,.f.)
        if not(empty(.w_DICODCON))
         .link_2_2('Full')
        endif
        .w_DIIMPPRO = 0
        .w_DISPECOM = IIF(g_COGE='S',.w_IMPCOM,0)
        .w_DIIMPCAL = .w_DIIMPPRO
        .DoRTCalc(37,39,.f.)
        .w_READPAR = .w_CODAZI
        .DoRTCalc(40,40,.f.)
        if not(empty(.w_READPAR))
         .link_4_1('Full')
        endif
        .DoRTCalc(41,41,.f.)
        if not(empty(.w_CODCAU))
         .link_4_2('Full')
        endif
        .DoRTCalc(42,42,.f.)
        .w_TIPCON = ''
        .w_CODCON = SPACE(15)
        .DoRTCalc(44,44,.f.)
        if not(empty(.w_CODCON))
         .link_4_5('Full')
        endif
        .DoRTCalc(45,45,.f.)
        if not(empty(.w_CODBAN))
         .link_4_6('Full')
        endif
        .DoRTCalc(46,54,.f.)
        if not(empty(.w_CATCOM))
         .link_4_15('Full')
        endif
        .DoRTCalc(55,55,.f.)
        if not(empty(.w_CODZON))
         .link_4_16('Full')
        endif
        .DoRTCalc(56,56,.f.)
        if not(empty(.w_CONSUP))
         .link_4_17('Full')
        endif
        .w_TIPSEL = .w_PTIPSEL
        .DoRTCalc(58,58,.f.)
        .w_FLRAGG = 'N'
        .DoRTCalc(60,60,.f.)
        .w_TIPBAN = 'G'
        .DoRTCalc(62,62,.f.)
        .w_OBTEST = i_datsys
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .DoRTCalc(64,74,.f.)
        .w_SCELTA = 'A'
        .DoRTCalc(76,76,.f.)
        .w_NUMDIS = 'ZZZZZZZZZZ'
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate(Alltrim(IIF(.w_FLRB1='RB', 'R.B./RIBA',''))+' ' +Alltrim(IIF(.w_FLRD1='RD', 'Rim.Dir.',''))+ ' '+Alltrim(IIF(.w_FLMA1='MA', 'M.AV.',''))+ ' '+ Alltrim(iif(.w_FLRI1 $ 'RI-SD', 'R.I.D.', ''))+ ' '+ Alltrim(iif(.w_FLBO1 $ 'BO-SC-SE', 'Bonifico',''))+ ' ' +Alltrim(IIF(.w_FLCA1='CA', 'Cambiali', '')))
        .DoRTCalc(78,90,.f.)
        .w_TOTRES1 = .w_TOTPAR-.w_TOTALI1
        .w_TOTRES = .w_TOTPAR-.w_TOTALI
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_CODISO = g_ISONAZ
        .w_CATIPDIS = .w_TIPDIS
        .w_TEST1 = .F.
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DIS_COMP')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gste_mcd
    Local CTRL_CONCON
    CTRL_CONCON= This.GetCtrl("w_DICAUBON")
    CTRL_CONCON.Popola()
    * --- Utilizzo la mHideControls() per rendere visibile la combo
    * --- anche in interroga.
    this.mHideControls()
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDATDIS_1_4.enabled = i_bVal
      .Page1.oPag.oCODESE_1_6.enabled = i_bVal
      .Page1.oPag.oCAURIF_1_7.enabled = i_bVal
      .Page1.oPag.oDICAUBON_1_10.enabled = i_bVal
      .Page1.oPag.oDITIPMAN_1_11.enabled = i_bVal
      .Page1.oPag.oDESCRI_1_12.enabled = i_bVal
      .Page1.oPag.oSCAINI_1_13.enabled = i_bVal
      .Page1.oPag.oSCAFIN_1_14.enabled = i_bVal
      .Page1.oPag.oCODVAL_1_15.enabled = i_bVal
      .Page1.oPag.oCAOVAL_1_21.enabled = i_bVal
      .Page1.oPag.oSCOTRA_1_22.enabled = i_bVal
      .Page1.oPag.oFLAVVI_1_23.enabled = i_bVal
      .Page1.oPag.oDATVAL_1_25.enabled = i_bVal
      .Page1.oPag.oFLCABI_1_26.enabled = i_bVal
      .Page2.oPag.oCODCAU_4_2.enabled = i_bVal
      .Page2.oPag.oTIPCON_4_4.enabled = i_bVal
      .Page2.oPag.oCODCON_4_5.enabled = i_bVal
      .Page2.oPag.oCODBAN_4_6.enabled = i_bVal
      .Page2.oPag.oDATDOCINI_4_9.enabled = i_bVal
      .Page2.oPag.oDATDOCFIN_4_11.enabled = i_bVal
      .Page2.oPag.oNUMDOCINI_4_12.enabled = i_bVal
      .Page2.oPag.oNUMDOCFIN_4_13.enabled = i_bVal
      .Page2.oPag.oALFDOCINI_4_14.enabled = i_bVal
      .Page2.oPag.oCATCOM_4_15.enabled = i_bVal
      .Page2.oPag.oCODZON_4_16.enabled = i_bVal
      .Page2.oPag.oCONSUP_4_17.enabled = i_bVal
      .Page2.oPag.oTIPSEL_4_18.enabled = i_bVal
      .Page2.oPag.oIMPMIN_4_19.enabled = i_bVal
      .Page2.oPag.oFLRAGG_4_20.enabled = i_bVal
      .Page2.oPag.oALFDOCFIN_4_33.enabled = i_bVal
      .Page1.oPag.oBtn_1_24.enabled = i_bVal
      .Page1.oPag.oBtn_1_28.enabled = i_bVal
      .Page1.oPag.oBtn_1_36.enabled = i_bVal
      .Page1.oPag.oObj_1_41.enabled = i_bVal
      .Page1.oPag.oObj_1_52.enabled = i_bVal
      .Page1.oPag.oObj_1_74.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DIS_COMP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DIS_COMP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DISERIAL,"DISERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DICAUBON,"DICAUBON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DITIPMAN,"DITIPMAN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DIS_COMP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_COMP_IDX,2])
    i_lTable = "DIS_COMP"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DIS_COMP_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_DICODCON C(15);
      ,t_DIIMPPRO N(18,4);
      ,t_DISPECOM N(18,4);
      ,t_DIIMPCAL N(18,4);
      ,t_DESBAC C(40);
      ,CPROWNUM N(10);
      ,t_DITIPCON C(1);
      ,t_BAOBSO D(8);
      ,t_BATIPO C(1);
      ,t_CODABI C(5);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgste_mcdbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDICODCON_2_2.controlsource=this.cTrsName+'.t_DICODCON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDIIMPPRO_2_3.controlsource=this.cTrsName+'.t_DIIMPPRO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDISPECOM_2_4.controlsource=this.cTrsName+'.t_DISPECOM'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDIIMPCAL_2_5.controlsource=this.cTrsName+'.t_DIIMPCAL'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDESBAC_2_7.controlsource=this.cTrsName+'.t_DESBAC'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(132)
    this.AddVLine(390)
    this.AddVLine(522)
    this.AddVLine(664)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICODCON_2_2
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DIS_COMP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_COMP_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DIS_COMP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_COMP_IDX,2])
      *
      * insert into DIS_COMP
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DIS_COMP')
        i_extval=cp_InsertValODBCExtFlds(this,'DIS_COMP')
        i_cFldBody=" "+;
                  "(DISERIAL,DICAUBON,DITIPMAN,DITIPCON,DICODCON"+;
                  ",DIIMPPRO,DISPECOM,DIIMPCAL,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DISERIAL)+","+cp_ToStrODBCNull(this.w_DICAUBON)+","+cp_ToStrODBC(this.w_DITIPMAN)+","+cp_ToStrODBC(this.w_DITIPCON)+","+cp_ToStrODBCNull(this.w_DICODCON)+;
             ","+cp_ToStrODBC(this.w_DIIMPPRO)+","+cp_ToStrODBC(this.w_DISPECOM)+","+cp_ToStrODBC(this.w_DIIMPCAL)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DIS_COMP')
        i_extval=cp_InsertValVFPExtFlds(this,'DIS_COMP')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DISERIAL',this.w_DISERIAL)
        INSERT INTO (i_cTable) (;
                   DISERIAL;
                  ,DICAUBON;
                  ,DITIPMAN;
                  ,DITIPCON;
                  ,DICODCON;
                  ,DIIMPPRO;
                  ,DISPECOM;
                  ,DIIMPCAL;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DISERIAL;
                  ,this.w_DICAUBON;
                  ,this.w_DITIPMAN;
                  ,this.w_DITIPCON;
                  ,this.w_DICODCON;
                  ,this.w_DIIMPPRO;
                  ,this.w_DISPECOM;
                  ,this.w_DIIMPCAL;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.DIS_COMP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_COMP_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_DICODCON) AND t_DIIMPPRO<>0) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DIS_COMP')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " DICAUBON="+cp_ToStrODBCNull(this.w_DICAUBON)+;
                 ",DITIPMAN="+cp_ToStrODBC(this.w_DITIPMAN)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DIS_COMP')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  DICAUBON=this.w_DICAUBON;
                 ,DITIPMAN=this.w_DITIPMAN;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_DICODCON) AND t_DIIMPPRO<>0) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DIS_COMP
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DIS_COMP')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " DICAUBON="+cp_ToStrODBCNull(this.w_DICAUBON)+;
                     ",DITIPMAN="+cp_ToStrODBC(this.w_DITIPMAN)+;
                     ",DITIPCON="+cp_ToStrODBC(this.w_DITIPCON)+;
                     ",DICODCON="+cp_ToStrODBCNull(this.w_DICODCON)+;
                     ",DIIMPPRO="+cp_ToStrODBC(this.w_DIIMPPRO)+;
                     ",DISPECOM="+cp_ToStrODBC(this.w_DISPECOM)+;
                     ",DIIMPCAL="+cp_ToStrODBC(this.w_DIIMPCAL)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DIS_COMP')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      DICAUBON=this.w_DICAUBON;
                     ,DITIPMAN=this.w_DITIPMAN;
                     ,DITIPCON=this.w_DITIPCON;
                     ,DICODCON=this.w_DICODCON;
                     ,DIIMPPRO=this.w_DIIMPPRO;
                     ,DISPECOM=this.w_DISPECOM;
                     ,DIIMPCAL=this.w_DIIMPCAL;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DIS_COMP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_COMP_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_DICODCON) AND t_DIIMPPRO<>0) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DIS_COMP
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_DICODCON) AND t_DIIMPPRO<>0) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DIS_COMP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_COMP_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
          .w_OBTEST = .w_DATDIS
        .DoRTCalc(6,9,.t.)
        if .o_CAURIF<>.w_CAURIF
          .link_1_10('Full')
        endif
        if .o_CAURIF<>.w_CAURIF
          .w_DITIPMAN = iif(.w_FLRI1='RI',.w_CATIPMAN,'T')
        endif
        .DoRTCalc(12,13,.t.)
        if .o_DATDIS<>.w_DATDIS
          .w_SCAFIN = .w_DATDIS+120
        endif
        if .o_CAURIF<>.w_CAURIF
          .link_1_15('Full')
        endif
        .DoRTCalc(16,19,.t.)
        if .o_CODVAL<>.w_CODVAL.or. .o_CAURIF<>.w_CAURIF
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        if .o_DATVAL<>.w_DATVAL.or. .o_CODVAL<>.w_CODVAL.or. .o_CAURIF<>.w_CAURIF
          .w_CAOVAL = IIF(EMPTY(.w_CODVAL) OR EMPTY(.w_DATVAL), 0, GETCAM(.w_CODVAL, .w_DATVAL, 7))
        endif
        if .o_TIPDIS<>.w_TIPDIS
          .w_SCOTRA = 0
        endif
        .DoRTCalc(23,23,.t.)
        if .o_DATDIS<>.w_DATDIS
          .w_DATVAL = .w_DATDIS
        endif
        if .o_CODCAU<>.w_CODCAU
          .w_FLCABI = ' '
        endif
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate(IIF(.w_TIPSCA='C', ah_MsgFormat("Attive"), IIF(.w_TIPSCA='F', ah_MsgFormat("Passive"),'')),'','')
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate(FUNDIS(.w_TIPDIS),'','')
        .DoRTCalc(26,33,.t.)
        if .o_DICODCON<>.w_DICODCON
          .w_TOTALI1 = .w_TOTALI1-.w_diimppro
          .w_DIIMPPRO = 0
          .w_TOTALI1 = .w_TOTALI1+.w_diimppro
        endif
        if .o_DICODCON<>.w_DICODCON.or. .o_CAURIF<>.w_CAURIF
          .w_DISPECOM = IIF(g_COGE='S',.w_IMPCOM,0)
        endif
        if .o_DICODCON<>.w_DICODCON.or. .o_DIIMPPRO<>.w_DIIMPPRO
          .w_TOTALI = .w_TOTALI-.w_diimpcal
          .w_DIIMPCAL = .w_DIIMPPRO
          .w_TOTALI = .w_TOTALI+.w_diimpcal
        endif
        .DoRTCalc(37,39,.t.)
          .link_4_1('Full')
        if .o_CAURIF<>.w_CAURIF
          .link_4_2('Full')
        endif
        .DoRTCalc(42,43,.t.)
        if .o_TIPCON<>.w_TIPCON
          .w_CODCON = SPACE(15)
          .link_4_5('Full')
        endif
        .DoRTCalc(45,60,.t.)
          .w_TIPBAN = 'G'
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .DoRTCalc(62,74,.t.)
          .w_SCELTA = 'A'
        .DoRTCalc(76,76,.t.)
          .w_NUMDIS = 'ZZZZZZZZZZ'
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate(Alltrim(IIF(.w_FLRB1='RB', 'R.B./RIBA',''))+' ' +Alltrim(IIF(.w_FLRD1='RD', 'Rim.Dir.',''))+ ' '+Alltrim(IIF(.w_FLMA1='MA', 'M.AV.',''))+ ' '+ Alltrim(iif(.w_FLRI1 $ 'RI-SD', 'R.I.D.', ''))+ ' '+ Alltrim(iif(.w_FLBO1 $ 'BO-SC-SE', 'Bonifico',''))+ ' ' +Alltrim(IIF(.w_FLCA1='CA', 'Cambiali', '')))
        .DoRTCalc(78,90,.t.)
          .w_TOTRES1 = .w_TOTPAR-.w_TOTALI1
          .w_TOTRES = .w_TOTPAR-.w_TOTALI
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(93,93,.t.)
          .w_CATIPDIS = .w_TIPDIS
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(95,95,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DITIPCON with this.w_DITIPCON
      replace t_BAOBSO with this.w_BAOBSO
      replace t_BATIPO with this.w_BATIPO
      replace t_CODABI with this.w_CODABI
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_43.Calculate(IIF(.w_TIPSCA='C', ah_MsgFormat("Attive"), IIF(.w_TIPSCA='F', ah_MsgFormat("Passive"),'')),'','')
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate(FUNDIS(.w_TIPDIS),'','')
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate(Alltrim(IIF(.w_FLRB1='RB', 'R.B./RIBA',''))+' ' +Alltrim(IIF(.w_FLRD1='RD', 'Rim.Dir.',''))+ ' '+Alltrim(IIF(.w_FLMA1='MA', 'M.AV.',''))+ ' '+ Alltrim(iif(.w_FLRI1 $ 'RI-SD', 'R.I.D.', ''))+ ' '+ Alltrim(iif(.w_FLBO1 $ 'BO-SC-SE', 'Bonifico',''))+ ' ' +Alltrim(IIF(.w_FLCA1='CA', 'Cambiali', '')))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCAOVAL_1_21.enabled = this.oPgFrm.Page1.oPag.oCAOVAL_1_21.mCond()
    this.oPgFrm.Page1.oPag.oSCOTRA_1_22.enabled = this.oPgFrm.Page1.oPag.oSCOTRA_1_22.mCond()
    this.oPgFrm.Page1.oPag.oFLAVVI_1_23.enabled = this.oPgFrm.Page1.oPag.oFLAVVI_1_23.mCond()
    this.oPgFrm.Page2.oPag.oCODCAU_4_2.enabled = this.oPgFrm.Page2.oPag.oCODCAU_4_2.mCond()
    this.oPgFrm.Page2.oPag.oCODCON_4_5.enabled = this.oPgFrm.Page2.oPag.oCODCON_4_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDIIMPPRO_2_3.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDIIMPPRO_2_3.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDISPECOM_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oDISPECOM_2_4.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDICAUBON_1_10.visible=!this.oPgFrm.Page1.oPag.oDICAUBON_1_10.mHide()
    this.oPgFrm.Page1.oPag.oDITIPMAN_1_11.visible=!this.oPgFrm.Page1.oPag.oDITIPMAN_1_11.mHide()
    this.oPgFrm.Page1.oPag.oFLAVVI_1_23.visible=!this.oPgFrm.Page1.oPag.oFLAVVI_1_23.mHide()
    this.oPgFrm.Page2.oPag.oCODCAU_4_2.visible=!this.oPgFrm.Page2.oPag.oCODCAU_4_2.mHide()
    this.oPgFrm.Page2.oPag.oDESCAU_4_3.visible=!this.oPgFrm.Page2.oPag.oDESCAU_4_3.mHide()
    this.oPgFrm.Page2.oPag.oStr_4_24.visible=!this.oPgFrm.Page2.oPag.oStr_4_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_66.visible=!this.oPgFrm.Page1.oPag.oStr_1_66.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_75.visible=!this.oPgFrm.Page1.oPag.oStr_1_75.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_43.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_52.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_63.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_74.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODESE
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_CODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_CODESE))
          select ESCODAZI,ESCODESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oCODESE_1_6'),i_cWhere,'',"Elenco esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_CODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_CODESE)
            select ESCODAZI,ESCODESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODESE = NVL(_Link_.ESCODESE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_CODESE = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAURIF
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_DIST_IDX,3]
    i_lTable = "CAU_DIST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_DIST_IDX,2], .t., this.CAU_DIST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_DIST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAURIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACD',True,'CAU_DIST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_CAURIF)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCAU,CATIPSCD,CACODVAD,CATIPDIS,CADIS_RD,CADIS_RI,CADIS_BO,CADIS_MA,CADIS_RB,CADIS_CA,CACODCAU,CAIMPCOM,CAFLAVVI,CATIPCON,CAFLBANC,CAFLIBAN,CACONSBF,CACAUBON,CAIMPMIN,CADATEFF,CATIPMAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_CAURIF))
          select CACODICE,CADESCAU,CATIPSCD,CACODVAD,CATIPDIS,CADIS_RD,CADIS_RI,CADIS_BO,CADIS_MA,CADIS_RB,CADIS_CA,CACODCAU,CAIMPCOM,CAFLAVVI,CATIPCON,CAFLBANC,CAFLIBAN,CACONSBF,CACAUBON,CAIMPMIN,CADATEFF,CATIPMAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAURIF)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAURIF) and !this.bDontReportError
            deferred_cp_zoom('CAU_DIST','*','CACODICE',cp_AbsName(oSource.parent,'oCAURIF_1_7'),i_cWhere,'GSTE_ACD',"Causali distinte",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCAU,CATIPSCD,CACODVAD,CATIPDIS,CADIS_RD,CADIS_RI,CADIS_BO,CADIS_MA,CADIS_RB,CADIS_CA,CACODCAU,CAIMPCOM,CAFLAVVI,CATIPCON,CAFLBANC,CAFLIBAN,CACONSBF,CACAUBON,CAIMPMIN,CADATEFF,CATIPMAN";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CADESCAU,CATIPSCD,CACODVAD,CATIPDIS,CADIS_RD,CADIS_RI,CADIS_BO,CADIS_MA,CADIS_RB,CADIS_CA,CACODCAU,CAIMPCOM,CAFLAVVI,CATIPCON,CAFLBANC,CAFLIBAN,CACONSBF,CACAUBON,CAIMPMIN,CADATEFF,CATIPMAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAURIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CADESCAU,CATIPSCD,CACODVAD,CATIPDIS,CADIS_RD,CADIS_RI,CADIS_BO,CADIS_MA,CADIS_RB,CADIS_CA,CACODCAU,CAIMPCOM,CAFLAVVI,CATIPCON,CAFLBANC,CAFLIBAN,CACONSBF,CACAUBON,CAIMPMIN,CADATEFF,CATIPMAN";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_CAURIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_CAURIF)
            select CACODICE,CADESCAU,CATIPSCD,CACODVAD,CATIPDIS,CADIS_RD,CADIS_RI,CADIS_BO,CADIS_MA,CADIS_RB,CADIS_CA,CACODCAU,CAIMPCOM,CAFLAVVI,CATIPCON,CAFLBANC,CAFLIBAN,CACONSBF,CACAUBON,CAIMPMIN,CADATEFF,CATIPMAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAURIF = NVL(_Link_.CACODICE,space(5))
      this.w_DESRIF = NVL(_Link_.CADESCAU,space(35))
      this.w_TIPSCA = NVL(_Link_.CATIPSCD,space(1))
      this.w_CODVAL = NVL(_Link_.CACODVAD,space(3))
      this.w_TIPDIS = NVL(_Link_.CATIPDIS,space(2))
      this.w_FLRD1 = NVL(_Link_.CADIS_RD,space(2))
      this.w_FLRI1 = NVL(_Link_.CADIS_RI,space(2))
      this.w_FLBO1 = NVL(_Link_.CADIS_BO,space(2))
      this.w_FLMA1 = NVL(_Link_.CADIS_MA,space(2))
      this.w_FLRB1 = NVL(_Link_.CADIS_RB,space(2))
      this.w_FLCA1 = NVL(_Link_.CADIS_CA,space(2))
      this.w_CODCAU = NVL(_Link_.CACODCAU,space(5))
      this.w_IMPCOM = NVL(_Link_.CAIMPCOM,0)
      this.w_FLAVVI = NVL(_Link_.CAFLAVVI,space(1))
      this.w_TIPO = NVL(_Link_.CATIPCON,space(1))
      this.w_FLBANC = NVL(_Link_.CAFLBANC,space(1))
      this.w_FLIBAN = NVL(_Link_.CAFLIBAN,space(1))
      this.w_CONSBF = NVL(_Link_.CACONSBF,space(1))
      this.w_DICAUBON = NVL(_Link_.CACAUBON,space(5))
      this.w_IMPMIN = NVL(_Link_.CAIMPMIN,0)
      this.w_DATEFF = NVL(_Link_.CADATEFF,space(1))
      this.w_CATIPMAN = NVL(_Link_.CATIPMAN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAURIF = space(5)
      endif
      this.w_DESRIF = space(35)
      this.w_TIPSCA = space(1)
      this.w_CODVAL = space(3)
      this.w_TIPDIS = space(2)
      this.w_FLRD1 = space(2)
      this.w_FLRI1 = space(2)
      this.w_FLBO1 = space(2)
      this.w_FLMA1 = space(2)
      this.w_FLRB1 = space(2)
      this.w_FLCA1 = space(2)
      this.w_CODCAU = space(5)
      this.w_IMPCOM = 0
      this.w_FLAVVI = space(1)
      this.w_TIPO = space(1)
      this.w_FLBANC = space(1)
      this.w_FLIBAN = space(1)
      this.w_CONSBF = space(1)
      this.w_DICAUBON = space(5)
      this.w_IMPMIN = 0
      this.w_DATEFF = space(1)
      this.w_CATIPMAN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_DIST_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_DIST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAURIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICAUBON
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DIST_IDX,3]
    i_lTable = "TIP_DIST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DIST_IDX,2], .t., this.TIP_DIST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DIST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICAUBON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DIST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DICAUDIS like "+cp_ToStrODBC(trim(this.w_DICAUBON)+"%");
                   +" and DICODISO="+cp_ToStrODBC(this.w_CODISO);
                   +" and DITIPDIS="+cp_ToStrODBC(this.w_TIPDIS);

          i_ret=cp_SQL(i_nConn,"select DICODISO,DITIPDIS,DICAUDIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DICODISO,DITIPDIS,DICAUDIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DICODISO',this.w_CODISO;
                     ,'DITIPDIS',this.w_TIPDIS;
                     ,'DICAUDIS',trim(this.w_DICAUBON))
          select DICODISO,DITIPDIS,DICAUDIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DICODISO,DITIPDIS,DICAUDIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICAUBON)==trim(_Link_.DICAUDIS) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICAUBON) and !this.bDontReportError
            deferred_cp_zoom('TIP_DIST','*','DICODISO,DITIPDIS,DICAUDIS',cp_AbsName(oSource.parent,'oDICAUBON_1_10'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODISO<>oSource.xKey(1);
           .or. this.w_TIPDIS<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DICODISO,DITIPDIS,DICAUDIS";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DICODISO,DITIPDIS,DICAUDIS;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DICODISO,DITIPDIS,DICAUDIS";
                     +" from "+i_cTable+" "+i_lTable+" where DICAUDIS="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DICODISO="+cp_ToStrODBC(this.w_CODISO);
                     +" and DITIPDIS="+cp_ToStrODBC(this.w_TIPDIS);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DICODISO',oSource.xKey(1);
                       ,'DITIPDIS',oSource.xKey(2);
                       ,'DICAUDIS',oSource.xKey(3))
            select DICODISO,DITIPDIS,DICAUDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICAUBON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DICODISO,DITIPDIS,DICAUDIS";
                   +" from "+i_cTable+" "+i_lTable+" where DICAUDIS="+cp_ToStrODBC(this.w_DICAUBON);
                   +" and DICODISO="+cp_ToStrODBC(this.w_CODISO);
                   +" and DITIPDIS="+cp_ToStrODBC(this.w_TIPDIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DICODISO',this.w_CODISO;
                       ,'DITIPDIS',this.w_TIPDIS;
                       ,'DICAUDIS',this.w_DICAUBON)
            select DICODISO,DITIPDIS,DICAUDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICAUBON = NVL(_Link_.DICAUDIS,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DICAUBON = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DIST_IDX,2])+'\'+cp_ToStr(_Link_.DICODISO,1)+'\'+cp_ToStr(_Link_.DITIPDIS,1)+'\'+cp_ToStr(_Link_.DICAUDIS,1)
      cp_ShowWarn(i_cKey,this.TIP_DIST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICAUBON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VASIMVAL,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CODVAL))
          select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VASIMVAL,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_CODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VASIMVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_CODVAL)+"%");

            select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VASIMVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCODVAL_1_15'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VASIMVAL,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VASIMVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VASIMVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VASIMVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESAPP = NVL(_Link_.VADESVAL,space(35))
      this.w_TESVAL = NVL(_Link_.VACAOVAL,0)
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_DESAPP = space(35)
      this.w_TESVAL = 0
      this.w_DECTOT = 0
      this.w_SIMVAL = space(5)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .T.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
        endif
        this.w_CODVAL = space(3)
        this.w_DESAPP = space(35)
        this.w_TESVAL = 0
        this.w_DECTOT = 0
        this.w_SIMVAL = space(5)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DICODCON
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DICODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_DICODCON)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO,BA__IBAN,BA__BBAN,BATIPCON,BACONSBF,BACODABI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_DICODCON))
          select BACODBAN,BADESCRI,BADTOBSO,BA__IBAN,BA__BBAN,BATIPCON,BACONSBF,BACODABI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DICODCON)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DICODCON) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oDICODCON_2_2'),i_cWhere,'GSTE_ACB',"Elenco banche",'GSTE_MCB.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO,BA__IBAN,BA__BBAN,BATIPCON,BACONSBF,BACODABI";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BADTOBSO,BA__IBAN,BA__BBAN,BATIPCON,BACONSBF,BACODABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DICODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO,BA__IBAN,BA__BBAN,BATIPCON,BACONSBF,BACODABI";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_DICODCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_DICODCON)
            select BACODBAN,BADESCRI,BADTOBSO,BA__IBAN,BA__BBAN,BATIPCON,BACONSBF,BACODABI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DICODCON = NVL(_Link_.BACODBAN,space(15))
      this.w_DESBAC = NVL(_Link_.BADESCRI,space(40))
      this.w_BAOBSO = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
      this.w_IBAN = NVL(_Link_.BA__IBAN,space(35))
      this.w_BBAN = NVL(_Link_.BA__BBAN,space(30))
      this.w_BATIPO = NVL(_Link_.BATIPCON,space(1))
      this.w_CONSBF1 = NVL(_Link_.BACONSBF,space(1))
      this.w_CODABI = NVL(_Link_.BACODABI,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_DICODCON = space(15)
      endif
      this.w_DESBAC = space(40)
      this.w_BAOBSO = ctod("  /  /  ")
      this.w_IBAN = space(35)
      this.w_BBAN = space(30)
      this.w_BATIPO = space(1)
      this.w_CONSBF1 = space(1)
      this.w_CODABI = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_BAOBSO) OR .w_BAOBSO>.w_OBTEST) AND (Not(((.w_TIPSCA='F' AND .w_TIPDIS $ 'BO-BE') OR (.w_TIPSCA='C' AND .w_TIPDIS $ 'RI-MA')) AND .w_FLIBAN='S' And ( ( EMPTY(.w_IBAN) Or EMPTY(.w_BBAN) ) ) Or  NOT EMPTY(.w_BATIPO) ) ) ) AND (ALLTRIM(.w_CONSBF)=ALLTRIM(.w_CONSBF1) OR .w_CONSBF='E')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto banca inesistente o obsoleto o incongruente o codice IBAN/BBAN mancanti")
        endif
        this.w_DICODCON = space(15)
        this.w_DESBAC = space(40)
        this.w_BAOBSO = ctod("  /  /  ")
        this.w_IBAN = space(35)
        this.w_BBAN = space(30)
        this.w_BATIPO = space(1)
        this.w_CONSBF1 = space(1)
        this.w_CODABI = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DICODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_2.BACODBAN as BACODBAN202"+ ",link_2_2.BADESCRI as BADESCRI202"+ ",link_2_2.BADTOBSO as BADTOBSO202"+ ",link_2_2.BA__IBAN as BA__IBAN202"+ ",link_2_2.BA__BBAN as BA__BBAN202"+ ",link_2_2.BATIPCON as BATIPCON202"+ ",link_2_2.BACONSBF as BACONSBF202"+ ",link_2_2.BACODABI as BACODABI202"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_2 on DIS_COMP.DICODCON=link_2_2.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_2"
          i_cKey=i_cKey+'+" and DIS_COMP.DICODCON=link_2_2.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=READPAR
  func Link_4_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_lTable = "CONTROPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2], .t., this.CONTROPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select COCODAZI,COTIPSEL";
                   +" from "+i_cTable+" "+i_lTable+" where COCODAZI="+cp_ToStrODBC(this.w_READPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'COCODAZI',this.w_READPAR)
            select COCODAZI,COTIPSEL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READPAR = NVL(_Link_.COCODAZI,space(5))
      this.w_PTIPSEL = NVL(_Link_.COTIPSEL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_READPAR = space(5)
      endif
      this.w_PTIPSEL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])+'\'+cp_ToStr(_Link_.COCODAZI,1)
      cp_ShowWarn(i_cKey,this.CONTROPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAU
  func Link_4_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CODCAU))
          select CCCODICE,CCDESCRI,CCTIPREG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCODCAU_4_2'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSTE_ACD.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CODCAU)
            select CCCODICE,CCDESCRI,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPREG = NVL(_Link_.CCTIPREG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAU = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_TIPREG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_CODCAU) OR .w_TIPREG $ 'NE'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente o incongruente o obsoleta")
        endif
        this.w_CODCAU = space(5)
        this.w_DESCAU = space(35)
        this.w_TIPREG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCON
  func Link_4_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_4_5'),i_cWhere,'GSAR_BZC',"Clienti / fornitori /conti",'GSTE_ASC.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANPARTSN,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_PARTSAN = NVL(_Link_.ANPARTSN,space(1))
      this.w_DATAOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCCON = space(40)
      this.w_PARTSAN = space(1)
      this.w_DATAOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes= (EMPTY(.w_DATAOBSO) OR .w_DATAOBSO>.w_OBTEST) AND .w_PARTSAN='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_CODCON = space(15)
        this.w_DESCCON = space(40)
        this.w_PARTSAN = space(1)
        this.w_DATAOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODBAN
  func Link_4_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_CODBAN)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_CODBAN))
          select BACODBAN,BADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODBAN)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_CODBAN)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_CODBAN)+"%");

            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODBAN) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oCODBAN_4_6'),i_cWhere,'GSTE_ACB',"Conti banca",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CODBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CODBAN)
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODBAN = NVL(_Link_.BACODBAN,space(15))
      this.w_DESBAN = NVL(_Link_.BADESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODBAN = space(15)
      endif
      this.w_DESBAN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATCOM
  func Link_4_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CATCOM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CTDESCRI like "+cp_ToStrODBC(trim(this.w_CATCOM)+"%");

            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CTDESCRI like "+cp_ToStr(trim(this.w_CATCOM)+"%");

            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCATCOM_4_15'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CATCOM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATCOM = NVL(_Link_.CTCODICE,space(3))
      this.w_CDESCRI = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATCOM = space(3)
      endif
      this.w_CDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODZON
  func Link_4_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_CODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_CODZON))
          select ZOCODZON,ZODESZON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ZODESZON like "+cp_ToStrODBC(trim(this.w_CODZON)+"%");

            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ZODESZON like "+cp_ToStr(trim(this.w_CODZON)+"%");

            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oCODZON_4_16'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_CODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_CODZON)
            select ZOCODZON,ZODESZON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_ZDESCRI = NVL(_Link_.ZODESZON,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODZON = space(3)
      endif
      this.w_ZDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CONSUP
  func Link_4_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONSUP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_CONSUP)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS,MCNUMLIV";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_CONSUP))
          select MCCODICE,MCDESCRI,MCTIPMAS,MCNUMLIV;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONSUP)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_CONSUP)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS,MCNUMLIV";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_CONSUP)+"%");

            select MCCODICE,MCDESCRI,MCTIPMAS,MCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CONSUP) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oCONSUP_4_17'),i_cWhere,'GSAR_AMC',"Mastri",'GSTE_MCD.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS,MCNUMLIV";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCTIPMAS,MCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONSUP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCTIPMAS,MCNUMLIV";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_CONSUP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_CONSUP)
            select MCCODICE,MCDESCRI,MCTIPMAS,MCNUMLIV;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONSUP = NVL(_Link_.MCCODICE,space(15))
      this.w_MDESCRI = NVL(_Link_.MCDESCRI,space(40))
      this.w_TIPMAS = NVL(_Link_.MCTIPMAS,space(1))
      this.w_NUMLIV = NVL(_Link_.MCNUMLIV,0)
    else
      if i_cCtrl<>'Load'
        this.w_CONSUP = space(15)
      endif
      this.w_MDESCRI = space(40)
      this.w_TIPMAS = space(1)
      this.w_NUMLIV = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NUMLIV=1 AND .w_TIPMAS<>'G'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Mastro non di tipo cliente\fornitore")
        endif
        this.w_CONSUP = space(15)
        this.w_MDESCRI = space(40)
        this.w_TIPMAS = space(1)
        this.w_NUMLIV = 0
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONSUP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDATDIS_1_4.value==this.w_DATDIS)
      this.oPgFrm.Page1.oPag.oDATDIS_1_4.value=this.w_DATDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESE_1_6.value==this.w_CODESE)
      this.oPgFrm.Page1.oPag.oCODESE_1_6.value=this.w_CODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oCAURIF_1_7.value==this.w_CAURIF)
      this.oPgFrm.Page1.oPag.oCAURIF_1_7.value=this.w_CAURIF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESRIF_1_8.value==this.w_DESRIF)
      this.oPgFrm.Page1.oPag.oDESRIF_1_8.value=this.w_DESRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oDICAUBON_1_10.RadioValue()==this.w_DICAUBON)
      this.oPgFrm.Page1.oPag.oDICAUBON_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDITIPMAN_1_11.RadioValue()==this.w_DITIPMAN)
      this.oPgFrm.Page1.oPag.oDITIPMAN_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_12.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_12.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCAINI_1_13.value==this.w_SCAINI)
      this.oPgFrm.Page1.oPag.oSCAINI_1_13.value=this.w_SCAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCAFIN_1_14.value==this.w_SCAFIN)
      this.oPgFrm.Page1.oPag.oSCAFIN_1_14.value=this.w_SCAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVAL_1_15.value==this.w_CODVAL)
      this.oPgFrm.Page1.oPag.oCODVAL_1_15.value=this.w_CODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_16.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_16.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCAOVAL_1_21.value==this.w_CAOVAL)
      this.oPgFrm.Page1.oPag.oCAOVAL_1_21.value=this.w_CAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSCOTRA_1_22.value==this.w_SCOTRA)
      this.oPgFrm.Page1.oPag.oSCOTRA_1_22.value=this.w_SCOTRA
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAVVI_1_23.RadioValue()==this.w_FLAVVI)
      this.oPgFrm.Page1.oPag.oFLAVVI_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATVAL_1_25.value==this.w_DATVAL)
      this.oPgFrm.Page1.oPag.oDATVAL_1_25.value=this.w_DATVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oFLCABI_1_26.RadioValue()==this.w_FLCABI)
      this.oPgFrm.Page1.oPag.oFLCABI_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTPAR_1_27.value==this.w_TOTPAR)
      this.oPgFrm.Page1.oPag.oTOTPAR_1_27.value=this.w_TOTPAR
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCAU_4_2.value==this.w_CODCAU)
      this.oPgFrm.Page2.oPag.oCODCAU_4_2.value=this.w_CODCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAU_4_3.value==this.w_DESCAU)
      this.oPgFrm.Page2.oPag.oDESCAU_4_3.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPCON_4_4.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page2.oPag.oTIPCON_4_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCON_4_5.value==this.w_CODCON)
      this.oPgFrm.Page2.oPag.oCODCON_4_5.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page2.oPag.oCODBAN_4_6.value==this.w_CODBAN)
      this.oPgFrm.Page2.oPag.oCODBAN_4_6.value=this.w_CODBAN
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCCON_4_7.value==this.w_DESCCON)
      this.oPgFrm.Page2.oPag.oDESCCON_4_7.value=this.w_DESCCON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESBAN_4_8.value==this.w_DESBAN)
      this.oPgFrm.Page2.oPag.oDESBAN_4_8.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page2.oPag.oDATDOCINI_4_9.value==this.w_DATDOCINI)
      this.oPgFrm.Page2.oPag.oDATDOCINI_4_9.value=this.w_DATDOCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDATDOCFIN_4_11.value==this.w_DATDOCFIN)
      this.oPgFrm.Page2.oPag.oDATDOCFIN_4_11.value=this.w_DATDOCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMDOCINI_4_12.value==this.w_NUMDOCINI)
      this.oPgFrm.Page2.oPag.oNUMDOCINI_4_12.value=this.w_NUMDOCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMDOCFIN_4_13.value==this.w_NUMDOCFIN)
      this.oPgFrm.Page2.oPag.oNUMDOCFIN_4_13.value=this.w_NUMDOCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oALFDOCINI_4_14.value==this.w_ALFDOCINI)
      this.oPgFrm.Page2.oPag.oALFDOCINI_4_14.value=this.w_ALFDOCINI
    endif
    if not(this.oPgFrm.Page2.oPag.oCATCOM_4_15.value==this.w_CATCOM)
      this.oPgFrm.Page2.oPag.oCATCOM_4_15.value=this.w_CATCOM
    endif
    if not(this.oPgFrm.Page2.oPag.oCODZON_4_16.value==this.w_CODZON)
      this.oPgFrm.Page2.oPag.oCODZON_4_16.value=this.w_CODZON
    endif
    if not(this.oPgFrm.Page2.oPag.oCONSUP_4_17.value==this.w_CONSUP)
      this.oPgFrm.Page2.oPag.oCONSUP_4_17.value=this.w_CONSUP
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPSEL_4_18.RadioValue()==this.w_TIPSEL)
      this.oPgFrm.Page2.oPag.oTIPSEL_4_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oIMPMIN_4_19.value==this.w_IMPMIN)
      this.oPgFrm.Page2.oPag.oIMPMIN_4_19.value=this.w_IMPMIN
    endif
    if not(this.oPgFrm.Page2.oPag.oFLRAGG_4_20.RadioValue()==this.w_FLRAGG)
      this.oPgFrm.Page2.oPag.oFLRAGG_4_20.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oALFDOCFIN_4_33.value==this.w_ALFDOCFIN)
      this.oPgFrm.Page2.oPag.oALFDOCFIN_4_33.value=this.w_ALFDOCFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oCDESCRI_4_38.value==this.w_CDESCRI)
      this.oPgFrm.Page2.oPag.oCDESCRI_4_38.value=this.w_CDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oZDESCRI_4_39.value==this.w_ZDESCRI)
      this.oPgFrm.Page2.oPag.oZDESCRI_4_39.value=this.w_ZDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oMDESCRI_4_40.value==this.w_MDESCRI)
      this.oPgFrm.Page2.oPag.oMDESCRI_4_40.value=this.w_MDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTRES1_3_3.value==this.w_TOTRES1)
      this.oPgFrm.Page1.oPag.oTOTRES1_3_3.value=this.w_TOTRES1
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTRES_3_4.value==this.w_TOTRES)
      this.oPgFrm.Page1.oPag.oTOTRES_3_4.value=this.w_TOTRES
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICODCON_2_2.value==this.w_DICODCON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICODCON_2_2.value=this.w_DICODCON
      replace t_DICODCON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICODCON_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIIMPPRO_2_3.value==this.w_DIIMPPRO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIIMPPRO_2_3.value=this.w_DIIMPPRO
      replace t_DIIMPPRO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIIMPPRO_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDISPECOM_2_4.value==this.w_DISPECOM)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDISPECOM_2_4.value=this.w_DISPECOM
      replace t_DISPECOM with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDISPECOM_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIIMPCAL_2_5.value==this.w_DIIMPCAL)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIIMPCAL_2_5.value=this.w_DIIMPCAL
      replace t_DIIMPCAL with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDIIMPCAL_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESBAC_2_7.value==this.w_DESBAC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESBAC_2_7.value=this.w_DESBAC
      replace t_DESBAC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDESBAC_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'DIS_COMP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATDIS))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oDATDIS_1_4.SetFocus()
            i_bnoObbl = !empty(.w_DATDIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODESE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCODESE_1_6.SetFocus()
            i_bnoObbl = !empty(.w_CODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAURIF))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCAURIF_1_7.SetFocus()
            i_bnoObbl = !empty(.w_CAURIF)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale distinta inesistente")
          case   (empty(.w_SCAFIN) or not(.w_SCAINI<=.w_SCAFIN))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oSCAFIN_1_14.SetFocus()
            i_bnoObbl = !empty(.w_SCAFIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODVAL) or not(CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .T.)))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCODVAL_1_15.SetFocus()
            i_bnoObbl = !empty(.w_CODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
          case   (empty(.w_CAOVAL))  and (.w_DATVAL<GETVALUT(g_PERVAL, 'VADATEUR') OR .w_TESVAL=0)
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oCAOVAL_1_21.SetFocus()
            i_bnoObbl = !empty(.w_CAOVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATVAL))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oDATVAL_1_25.SetFocus()
            i_bnoObbl = !empty(.w_DATVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data non � compresa in nessun esercizio definito")
          case   not(EMPTY(.w_CODCAU) OR .w_TIPREG $ 'NE')  and not(g_COGE<>'S')  and (g_COGE='S')  and not(empty(.w_CODCAU))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oCODCAU_4_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente o incongruente o obsoleta")
          case   not( (EMPTY(.w_DATAOBSO) OR .w_DATAOBSO>.w_OBTEST) AND .w_PARTSAN='S')  and (.w_TIPCON $ 'CFG')  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oCODCON_4_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not(.w_NUMLIV=1 AND .w_TIPMAS<>'G')  and not(empty(.w_CONSUP))
            .oPgFrm.ActivePage=2
            .oPgFrm.Page2.oPag.oCONSUP_4_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Mastro non di tipo cliente\fornitore")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (NOT EMPTY(t_DICODCON) AND t_DIIMPPRO<>0);
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(((EMPTY(.w_BAOBSO) OR .w_BAOBSO>.w_OBTEST) AND (Not(((.w_TIPSCA='F' AND .w_TIPDIS $ 'BO-BE') OR (.w_TIPSCA='C' AND .w_TIPDIS $ 'RI-MA')) AND .w_FLIBAN='S' And ( ( EMPTY(.w_IBAN) Or EMPTY(.w_BBAN) ) ) Or  NOT EMPTY(.w_BATIPO) ) ) ) AND (ALLTRIM(.w_CONSBF)=ALLTRIM(.w_CONSBF1) OR .w_CONSBF='E')) and not(empty(.w_DICODCON)) and (NOT EMPTY(.w_DICODCON) AND .w_DIIMPPRO<>0)
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDICODCON_2_2
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Conto banca inesistente o obsoleto o incongruente o codice IBAN/BBAN mancanti")
      endcase
      if NOT EMPTY(.w_DICODCON) AND .w_DIIMPPRO<>0
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATDIS = this.w_DATDIS
    this.o_CAURIF = this.w_CAURIF
    this.o_CODVAL = this.w_CODVAL
    this.o_DATVAL = this.w_DATVAL
    this.o_TIPDIS = this.w_TIPDIS
    this.o_DICODCON = this.w_DICODCON
    this.o_DIIMPPRO = this.w_DIIMPPRO
    this.o_CODCAU = this.w_CODCAU
    this.o_TIPCON = this.w_TIPCON
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_DICODCON) AND t_DIIMPPRO<>0)
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_DITIPCON=space(1)
      .w_DICODCON=space(15)
      .w_DIIMPPRO=0
      .w_DISPECOM=0
      .w_DIIMPCAL=0
      .w_BAOBSO=ctod("  /  /  ")
      .w_DESBAC=space(40)
      .w_BATIPO=space(1)
      .w_CODABI=space(5)
      .DoRTCalc(1,31,.f.)
        .w_DITIPCON = 'G'
      .DoRTCalc(33,33,.f.)
      if not(empty(.w_DICODCON))
        .link_2_2('Full')
      endif
        .w_DIIMPPRO = 0
        .w_DISPECOM = IIF(g_COGE='S',.w_IMPCOM,0)
        .w_DIIMPCAL = .w_DIIMPPRO
    endwith
    this.DoRTCalc(37,95,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_DITIPCON = t_DITIPCON
    this.w_DICODCON = t_DICODCON
    this.w_DIIMPPRO = t_DIIMPPRO
    this.w_DISPECOM = t_DISPECOM
    this.w_DIIMPCAL = t_DIIMPCAL
    this.w_BAOBSO = t_BAOBSO
    this.w_DESBAC = t_DESBAC
    this.w_BATIPO = t_BATIPO
    this.w_CODABI = t_CODABI
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_DITIPCON with this.w_DITIPCON
    replace t_DICODCON with this.w_DICODCON
    replace t_DIIMPPRO with this.w_DIIMPPRO
    replace t_DISPECOM with this.w_DISPECOM
    replace t_DIIMPCAL with this.w_DIIMPCAL
    replace t_BAOBSO with this.w_BAOBSO
    replace t_DESBAC with this.w_DESBAC
    replace t_BATIPO with this.w_BATIPO
    replace t_CODABI with this.w_CODABI
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTALI1 = .w_TOTALI1-.w_diimppro
        .w_TOTALI = .w_TOTALI-.w_diimpcal
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgste_mcdPag1 as StdContainer
  Width  = 815
  height = 445
  stdWidth  = 815
  stdheight = 445
  resizeXpos=204
  resizeYpos=293
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATDIS_1_4 as StdField with uid="BVIQHCLTMQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATDIS", cQueryName = "DATDIS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di compilazione delle distinte",;
    HelpContextID = 34558262,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=126, Top=9

  add object oCODESE_1_6 as StdField with uid="YOKBVKTUDK",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODESE", cQueryName = "CODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice esercizio di competenza",;
    HelpContextID = 189833434,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=379, Top=9, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_CODESE"

  func oCODESE_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODESE_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODESE_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oCODESE_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco esercizi",'',this.parent.oContained
  endproc

  add object oCAURIF_1_7 as StdField with uid="DKPTMPPAHL",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CAURIF", cQueryName = "CAURIF",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale distinta inesistente",;
    ToolTipText = "Causale distinta associata ai conti di riferimento selezionati",;
    HelpContextID = 182623962,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=126, Top=36, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_DIST", cZoomOnZoom="GSTE_ACD", oKey_1_1="CACODICE", oKey_1_2="this.w_CAURIF"

  func oCAURIF_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAURIF_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAURIF_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_DIST','*','CACODICE',cp_AbsName(this.parent,'oCAURIF_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACD',"Causali distinte",'',this.parent.oContained
  endproc
  proc oCAURIF_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_CAURIF
    i_obj.ecpSave()
  endproc

  add object oDESRIF_1_8 as StdField with uid="FNOTXNRWRL",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESRIF", cQueryName = "DESRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 182631114,;
   bGlobalFont=.t.,;
    Height=21, Width=241, Left=193, Top=36, InputMask=replicate('X',35)


  add object oDICAUBON_1_10 as StdZTamTableCombo with uid="TNQIARENKN",rtseq=10,rtrep=.f.,left=126,top=64,width=215,height=21;
    , ToolTipText = "Causale di pagamento";
    , HelpContextID = 238335612;
    , cFormVar="w_DICAUBON",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="TIP_DIST";
    , cTable='QUERY\GSTECACD.VQR',cKey='DICAUDIS',cValue='DIDESCRI',cOrderBy='DIDESCRI',xDefault=space(5);
  , bGlobalFont=.t.


  func oDICAUBON_1_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT .w_TEST1)
    endwith
    endif
  endfunc

  func oDICAUBON_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICAUBON_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc


  add object oDITIPMAN_1_11 as StdCombo with uid="OZAQRRZGGB",rtseq=11,rtrep=.f.,left=565,top=64,width=128,height=21;
    , ToolTipText = "Tipo SDD a cui devono appartenere le partite da inserire in distinta";
    , HelpContextID = 210000260;
    , cFormVar="w_DITIPMAN",RowSource=""+"CORE,"+"B2B,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oDITIPMAN_1_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DITIPMAN,&i_cF..t_DITIPMAN),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'B',;
    iif(xVal =3,'T',;
    space(1)))))
  endfunc
  func oDITIPMAN_1_11.GetRadio()
    this.Parent.oContained.w_DITIPMAN = this.RadioValue()
    return .t.
  endfunc

  func oDITIPMAN_1_11.ToRadio()
    this.Parent.oContained.w_DITIPMAN=trim(this.Parent.oContained.w_DITIPMAN)
    return(;
      iif(this.Parent.oContained.w_DITIPMAN=='C',1,;
      iif(this.Parent.oContained.w_DITIPMAN=='B',2,;
      iif(this.Parent.oContained.w_DITIPMAN=='T',3,;
      0))))
  endfunc

  func oDITIPMAN_1_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oDITIPMAN_1_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLRI1<>'RI' )
    endwith
    endif
  endfunc

  add object oDESCRI_1_12 as StdField with uid="JTQDXEFIXD",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale descrizione delle distinte",;
    HelpContextID = 123845322,;
   bGlobalFont=.t.,;
    Height=21, Width=308, Left=126, Top=92, InputMask=replicate('X',35)

  add object oSCAINI_1_13 as StdField with uid="UVXURJTRYD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_SCAINI", cQueryName = "SCAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio selezione scadenze",;
    HelpContextID = 127720410,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=126, Top=119

  add object oSCAFIN_1_14 as StdField with uid="ZALNPQUTTN",rtseq=14,rtrep=.f.,;
    cFormVar = "w_SCAFIN", cQueryName = "SCAFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di fine selezione scadenze",;
    HelpContextID = 49273818,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=239, Top=118

  func oSCAFIN_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_SCAINI<=.w_SCAFIN)
    endwith
    return bRes
  endfunc

  add object oCODVAL_1_15 as StdField with uid="HGHBERYQOS",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CODVAL", cQueryName = "CODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta inesistente o incongruente o obsoleta",;
    ToolTipText = "Codice valuta delle distinte",;
    HelpContextID = 90153178,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=126, Top=145, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_CODVAL"

  func oCODVAL_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVAL_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVAL_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCODVAL_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oCODVAL_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_CODVAL
    i_obj.ecpSave()
  endproc

  add object oSIMVAL_1_16 as StdField with uid="XNHAAFAAPO",rtseq=16,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 90117594,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=176, Top=145, InputMask=replicate('X',5)

  add object oCAOVAL_1_21 as StdField with uid="CYDTQBJSDF",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CAOVAL", cQueryName = "CAOVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio per conversione valuta partite in moneta di conto",;
    HelpContextID = 90111706,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=327, Top=145, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oCAOVAL_1_21.mCond()
    with this.Parent.oContained
      return (.w_DATVAL<GETVALUT(g_PERVAL, 'VADATEUR') OR .w_TESVAL=0)
    endwith
  endfunc

  add object oSCOTRA_1_22 as StdField with uid="RSSFYTVBDJ",rtseq=22,rtrep=.f.,;
    cFormVar = "w_SCOTRA", cQueryName = "SCOTRA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di sconto legata alle tratte",;
    HelpContextID = 256965594,;
   bGlobalFont=.t.,;
    Height=21, Width=60, Left=560, Top=118, cSayPict='"99.99"', cGetPict='"99.99"'

  func oSCOTRA_1_22.mCond()
    with this.Parent.oContained
      return (.w_TIPDIS='CA' AND .w_TIPSCA='C')
    endwith
  endfunc

  add object oFLAVVI_1_23 as StdCheck with uid="TMQCSAMZYN",rtseq=23,rtrep=.f.,left=673, top=118, caption="Avvisi",;
    ToolTipText = "Se attivo: abilita l'emissione avvisi a clienti / fornitori",;
    HelpContextID = 118477738,;
    cFormVar="w_FLAVVI", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oFLAVVI_1_23.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FLAVVI,&i_cF..t_FLAVVI),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oFLAVVI_1_23.GetRadio()
    this.Parent.oContained.w_FLAVVI = this.RadioValue()
    return .t.
  endfunc

  func oFLAVVI_1_23.ToRadio()
    this.Parent.oContained.w_FLAVVI=trim(this.Parent.oContained.w_FLAVVI)
    return(;
      iif(this.Parent.oContained.w_FLAVVI=='S',1,;
      0))
  endfunc

  func oFLAVVI_1_23.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oFLAVVI_1_23.mCond()
    with this.Parent.oContained
      return (g_COGE='S')
    endwith
  endfunc

  func oFLAVVI_1_23.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
    endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="AIRDIWUDDW",left=764, top=147, width=48,height=45,;
    CpPicture="bmp\calcola.bmp", caption="", nPag=1;
    , ToolTipText = "Calcola gli importi disponibili per le selezioni impostate.";
    , HelpContextID = 227537190;
    , Caption='\<Calcola';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        GSTE_BD1(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDATVAL_1_25 as StdField with uid="DAJJJCAATF",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DATVAL", cQueryName = "DATVAL",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data non � compresa in nessun esercizio definito",;
    ToolTipText = "Data di presentazione in banca della distinta, sulla quale sar� calcolata la valuta dell'operazione",;
    HelpContextID = 90091210,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=560, Top=145

  add object oFLCABI_1_26 as StdCheck with uid="PCCKAHDIJG",rtseq=25,rtrep=.f.,left=673, top=144, caption="ABI",;
    ToolTipText = "Se attivo: filtra le partite con codice ABI della banca di appoggio uguale a quello della banca di presentazione",;
    HelpContextID = 140817322,;
    cFormVar="w_FLCABI", bObbl = .f. , nPag = 1;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oFLCABI_1_26.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FLCABI,&i_cF..t_FLCABI),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oFLCABI_1_26.GetRadio()
    this.Parent.oContained.w_FLCABI = this.RadioValue()
    return .t.
  endfunc

  func oFLCABI_1_26.ToRadio()
    this.Parent.oContained.w_FLCABI=trim(this.Parent.oContained.w_FLCABI)
    return(;
      iif(this.Parent.oContained.w_FLCABI=='S',1,;
      0))
  endfunc

  func oFLCABI_1_26.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oTOTPAR_1_27 as StdField with uid="NOHCDYCWKC",rtseq=26,rtrep=.f.,;
    cFormVar = "w_TOTPAR", cQueryName = "TOTPAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo delle scadenze/partite",;
    HelpContextID = 10182710,;
   bGlobalFont=.t.,;
    Height=20, Width=128, Left=392, Top=170, cSayPict="v_PV(40+VVL)", cGetPict="v_PV(40+VVL)"


  add object oBtn_1_28 as StdButton with uid="DFEJOOROIK",left=713, top=400, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Assegna le scadenze ai conti selezionati.";
    , HelpContextID = 97109018;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_28.mCond()
    with this.Parent.oContained
      return (.w_TOTPAR<>0)
    endwith
  endfunc


  add object oBtn_1_36 as StdButton with uid="JCWLFUIAXR",left=764, top=400, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 89820346;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_36.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_41 as cp_runprogram with uid="QWQPEFIDLZ",left=198, top=460, width=192,height=19,;
    caption='GSTE_BD1(G)',;
   bGlobalFont=.t.,;
    prg="GSTE_BD1('G')",;
    cEvent = "Conferma",;
    nPag=1;
    , HelpContextID = 41106711


  add object oObj_1_43 as cp_calclbl with uid="HLXFDUICMI",left=563, top=9, width=71,height=28,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",FontBold =.t.,Alignment=0,;
    nPag=1;
    , HelpContextID = 80859878


  add object oObj_1_44 as cp_calclbl with uid="GTVEXWWXLY",left=563, top=36, width=215,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",FontBold =.t.,Alignment=0,;
    nPag=1;
    , HelpContextID = 80859878


  add object oObj_1_52 as cp_runprogram with uid="QROSWZWGTE",left=1, top=460, width=192,height=19,;
    caption='GSTE_BD3',;
   bGlobalFont=.t.,;
    prg="GSTE_BD3",;
    cEvent = "w_CAURIF Changed",;
    nPag=1;
    , HelpContextID = 40919961


  add object oObj_1_63 as cp_calclbl with uid="RPPNHKGLKX",left=563, top=90, width=249,height=23,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="",FontBold =.t.,Alignment=0,fontUnderline=.f.,bGlobalFont=.t.,alignment=1,fontname="Arial",fontsize=9,fontBold=.f.,fontItalic=.f.,;
    nPag=1;
    , HelpContextID = 80859878


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=2, top=194, width=808,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="DICODCON",Label1="Banca presentazione",Field2="DESBAC",Label2="Descrizione",Field3="DIIMPPRO",Label3="Importo proposto",Field4="DISPECOM",Label4="Comm. fisse",Field5="DIIMPCAL",Label5="Importo calcolato",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 218953082


  add object oObj_1_74 as cp_runprogram with uid="PLTDTLOATM",left=1, top=485, width=192,height=19,;
    caption='GSTE_BD1(Z)',;
   bGlobalFont=.t.,;
    prg="GSTE_BD1('Z')",;
    cEvent = "w_CAURIF Changed",;
    nPag=1;
    , HelpContextID = 41111575

  add object oStr_1_29 as StdString with uid="KAXTSLHLRW",Visible=.t., Left=232, Top=11,;
    Alignment=1, Width=144, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="YLWGCFLFUK",Visible=.t., Left=5, Top=94,;
    Alignment=1, Width=119, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="VKCJETPLED",Visible=.t., Left=5, Top=38,;
    Alignment=1, Width=119, Height=15,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="CEHEJVPNMV",Visible=.t., Left=226, Top=173,;
    Alignment=1, Width=162, Height=15,;
    Caption="Importo disponibile:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_33 as StdString with uid="UMKFGYFWNZ",Visible=.t., Left=5, Top=121,;
    Alignment=1, Width=119, Height=15,;
    Caption="Scadenze da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="AFPZEFGPXR",Visible=.t., Left=209, Top=121,;
    Alignment=1, Width=26, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="FATTXWIBRG",Visible=.t., Left=464, Top=120,;
    Alignment=1, Width=94, Height=15,;
    Caption="Sconto tratte:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="CCNLKMCWUX",Visible=.t., Left=259, Top=148,;
    Alignment=1, Width=65, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="IBCXDHHSMG",Visible=.t., Left=5, Top=147,;
    Alignment=1, Width=119, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="RFPZTGYZYM",Visible=.t., Left=5, Top=11,;
    Alignment=1, Width=119, Height=15,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="YKYVPERXVX",Visible=.t., Left=471, Top=11,;
    Alignment=1, Width=90, Height=15,;
    Caption="Tipo scadenze:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="EXZLHKHLGZ",Visible=.t., Left=471, Top=38,;
    Alignment=1, Width=90, Height=15,;
    Caption="Tipo distinte:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="YILMQBOIBW",Visible=.t., Left=444, Top=147,;
    Alignment=1, Width=114, Height=18,;
    Caption="Data presentazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_62 as StdString with uid="BFJSHPCSWN",Visible=.t., Left=467, Top=92,;
    Alignment=1, Width=94, Height=18,;
    Caption="Tipo pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_66 as StdString with uid="PEORFMRGCG",Visible=.t., Left=5, Top=66,;
    Alignment=1, Width=119, Height=18,;
    Caption="Causale bonifico:"  ;
  , bGlobalFont=.t.

  func oStr_1_66.mHide()
    with this.Parent.oContained
      return (NOT .w_TEST1)
    endwith
  endfunc

  add object oStr_1_68 as StdString with uid="OTZTFWFLPF",Visible=.t., Left=96, Top=374,;
    Alignment=1, Width=257, Height=18,;
    Caption="Residuo importo proposto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_69 as StdString with uid="ZUMSBENJXB",Visible=.t., Left=500, Top=374,;
    Alignment=1, Width=173, Height=18,;
    Caption="Residuo importo calcolato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_75 as StdString with uid="ZXHCYOQMRO",Visible=.t., Left=500, Top=66,;
    Alignment=1, Width=61, Height=18,;
    Caption="Tipo SDD:"  ;
  , bGlobalFont=.t.

  func oStr_1_75.mHide()
    with this.Parent.oContained
      return (.w_FLRI1 <>'RI')
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-8,top=213,;
    width=804+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-7,top=214,width=803+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='COC_MAST|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='COC_MAST'
        oDropInto=this.oBodyCol.oRow.oDICODCON_2_2
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTRES1_3_3 as StdField with uid="BUDJOSQUFI",rtseq=91,rtrep=.f.,;
    cFormVar="w_TOTRES1",value=0,enabled=.f.,;
    HelpContextID = 31285302,;
    cQueryName = "TOTRES1",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=355, Top=372, cSayPict=[v_PV(40+VVL)], cGetPict=[v_PV(40+VVL)]

  add object oTOTRES_3_4 as StdField with uid="RLJRNWVVFT",rtseq=92,rtrep=.f.,;
    cFormVar="w_TOTRES",value=0,enabled=.f.,;
    HelpContextID = 31285302,;
    cQueryName = "TOTRES",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=674, Top=372, cSayPict=[v_PV(40+VVL)], cGetPict=[v_PV(40+VVL)]
enddefine

  define class tgste_mcdPag2 as StdContainer
    Width  = 815
    height = 445
    stdWidth  = 815
    stdheight = 445
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODCAU_4_2 as StdField with uid="TZZNQHNBJJ",rtseq=41,rtrep=.f.,;
    cFormVar = "w_CODCAU", cQueryName = "CODCAU",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente o incongruente o obsoleta",;
    ToolTipText = "Causale per la contabilizzazione in primanota",;
    HelpContextID = 59596582,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=184, Top=20, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CODCAU"

  func oCODCAU_4_2.mCond()
    with this.Parent.oContained
      return (g_COGE='S')
    endwith
  endfunc

  func oCODCAU_4_2.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
    endif
  endfunc

  func oCODCAU_4_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAU_4_2.ecpDrop(oSource)
    this.Parent.oContained.link_4_2('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAU_4_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCODCAU_4_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSTE_ACD.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCODCAU_4_2.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CODCAU
    i_obj.ecpSave()
  endproc

  add object oDESCAU_4_3 as StdField with uid="IBUZQVCOPV",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 59655478,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=350, Top=20, InputMask=replicate('X',35)

  func oDESCAU_4_3.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
    endif
  endfunc


  add object oTIPCON_4_4 as StdCombo with uid="QVSSQPLDON",value=3,rtseq=43,rtrep=.f.,left=52,top=48,width=128,height=21;
    , ToolTipText = "Tipo conto di selezione";
    , HelpContextID = 43115978;
    , cFormVar="w_TIPCON",RowSource=""+"Cliente,"+"Fornitore,"+"No selezione", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oTIPCON_4_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TIPCON,&i_cF..t_TIPCON),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'F',;
    iif(xVal =3,'',;
    space(1)))))
  endfunc
  func oTIPCON_4_4.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_4_4.ToRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    return(;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      iif(this.Parent.oContained.w_TIPCON=='',3,;
      0))))
  endfunc

  func oTIPCON_4_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTIPCON_4_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCON)
        bRes2=.link_4_5('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODCON_4_5 as StdField with uid="GSYRZGJUSX",rtseq=44,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Codice cliente fornitore",;
    HelpContextID = 43163866,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=184, Top=48, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_4_5.mCond()
    with this.Parent.oContained
      return (.w_TIPCON $ 'CFG')
    endwith
  endfunc

  func oCODCON_4_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_4_5.ecpDrop(oSource)
    this.Parent.oContained.link_4_5('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_4_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_4_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti / fornitori /conti",'GSTE_ASC.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCON_4_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
    i_obj.ecpSave()
  endproc

  add object oCODBAN_4_6 as StdField with uid="JBDWOKCQQM",rtseq=45,rtrep=.f.,;
    cFormVar = "w_CODBAN", cQueryName = "CODBAN",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Ns C/C comunicato al fornitore per RB o a cliente per bonifico",;
    HelpContextID = 57909466,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=184, Top=76, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_CODBAN"

  func oCODBAN_4_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODBAN_4_6.ecpDrop(oSource)
    this.Parent.oContained.link_4_6('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODBAN_4_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oCODBAN_4_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banca",'',this.parent.oContained
  endproc
  proc oCODBAN_4_6.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_CODBAN
    i_obj.ecpSave()
  endproc

  add object oDESCCON_4_7 as StdField with uid="XNPDSEHUQV",rtseq=46,rtrep=.f.,;
    cFormVar = "w_DESCCON", cQueryName = "DESCCON",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 38910666,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=350, Top=48, InputMask=replicate('X',40)

  add object oDESBAN_4_8 as StdField with uid="LDDKTPIVQT",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 57850570,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=350, Top=76, InputMask=replicate('X',40)

  add object oDATDOCINI_4_9 as StdField with uid="YGOSHBYFSC",rtseq=48,rtrep=.f.,;
    cFormVar = "w_DATDOCINI", cQueryName = "DATDOCINI",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Filtro per data documento presente sulla partita",;
    HelpContextID = 227584492,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=184, Top=104

  add object oDATDOCFIN_4_11 as StdField with uid="VLYBHOOKUN",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DATDOCFIN", cQueryName = "DATDOCFIN",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Filtro per data documento presente sulla partita",;
    HelpContextID = 40851039,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=350, Top=104

  add object oNUMDOCINI_4_12 as StdField with uid="KGWWVHMLPE",rtseq=51,rtrep=.f.,;
    cFormVar = "w_NUMDOCINI", cQueryName = "NUMDOCINI",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di inizio selezione",;
    HelpContextID = 227607884,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=184, Top=132, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oNUMDOCFIN_4_13 as StdField with uid="MURWGCRMYK",rtseq=52,rtrep=.f.,;
    cFormVar = "w_NUMDOCFIN", cQueryName = "NUMDOCFIN",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di fine selezione",;
    HelpContextID = 40827647,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=350, Top=132, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oALFDOCINI_4_14 as StdField with uid="CHJUPBBDKL",rtseq=53,rtrep=.f.,;
    cFormVar = "w_ALFDOCINI", cQueryName = "ALFDOCINI",;
    bObbl = .f. , nPag = 4, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di inizio selezione",;
    HelpContextID = 227639068,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=184, Top=160, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oCATCOM_4_15 as StdField with uid="BDGMMYEBSH",rtseq=54,rtrep=.f.,;
    cFormVar = "w_CATCOM", cQueryName = "CATCOM",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categ. commerciale",;
    HelpContextID = 59879130,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=184, Top=188, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_CATCOM"

  func oCATCOM_4_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATCOM_4_15.ecpDrop(oSource)
    this.Parent.oContained.link_4_15('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATCOM_4_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCATCOM_4_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oCATCOM_4_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CATCOM
    i_obj.ecpSave()
  endproc

  add object oCODZON_4_16 as StdField with uid="GVZZHXJTUL",rtseq=55,rtrep=.f.,;
    cFormVar = "w_CODZON", cQueryName = "CODZON",;
    bObbl = .f. , nPag = 4, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice zona",;
    HelpContextID = 41656538,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=184, Top=216, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_CODZON"

  func oCODZON_4_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODZON_4_16.ecpDrop(oSource)
    this.Parent.oContained.link_4_16('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODZON_4_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oCODZON_4_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc oCODZON_4_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_CODZON
    i_obj.ecpSave()
  endproc

  add object oCONSUP_4_17 as StdField with uid="PBQRVSKHCV",rtseq=56,rtrep=.f.,;
    cFormVar = "w_CONSUP", cQueryName = "CONSUP",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Mastro non di tipo cliente\fornitore",;
    ToolTipText = "Mastro di raggrupp.",;
    HelpContextID = 2228442,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=184, Top=244, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_CONSUP"

  func oCONSUP_4_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONSUP_4_17.ecpDrop(oSource)
    this.Parent.oContained.link_4_17('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONSUP_4_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oCONSUP_4_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri",'GSTE_MCD.MASTRI_VZM',this.parent.oContained
  endproc
  proc oCONSUP_4_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_CONSUP
    i_obj.ecpSave()
  endproc


  add object oTIPSEL_4_18 as StdCombo with uid="ZMLVVFLODC",rtseq=57,rtrep=.f.,left=184,top=273,width=139,height=21;
    , ToolTipText = "Ordinamento delle partite durante l'assegnazione ai vari conti di contabilit�";
    , HelpContextID = 86107594;
    , cFormVar="w_TIPSEL",RowSource=""+"Data scadenza,"+"Importo crescente,"+"Importo decrescente", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oTIPSEL_4_18.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TIPSEL,&i_cF..t_TIPSEL),this.value)
    return(iif(xVal =1,'D',;
    iif(xVal =2,'C',;
    iif(xVal =3,'I',;
    space(1)))))
  endfunc
  func oTIPSEL_4_18.GetRadio()
    this.Parent.oContained.w_TIPSEL = this.RadioValue()
    return .t.
  endfunc

  func oTIPSEL_4_18.ToRadio()
    this.Parent.oContained.w_TIPSEL=trim(this.Parent.oContained.w_TIPSEL)
    return(;
      iif(this.Parent.oContained.w_TIPSEL=='D',1,;
      iif(this.Parent.oContained.w_TIPSEL=='C',2,;
      iif(this.Parent.oContained.w_TIPSEL=='I',3,;
      0))))
  endfunc

  func oTIPSEL_4_18.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oIMPMIN_4_19 as StdField with uid="TQMHBNPRUF",rtseq=58,rtrep=.f.,;
    cFormVar = "w_IMPMIN", cQueryName = "IMPMIN",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo minimo di selezione effetti da compilazione distinte",;
    HelpContextID = 48751226,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=184, Top=300, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oFLRAGG_4_20 as StdCheck with uid="OBEJFGSTQL",rtseq=59,rtrep=.f.,left=350, top=273, caption="Raggruppamento scadenze",;
    ToolTipText = "Se attivato tutte le scadenze dello stesso raggruppamento devono finire nella stessa distinta",;
    HelpContextID = 169067434,;
    cFormVar="w_FLRAGG", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oFLRAGG_4_20.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..FLRAGG,&i_cF..t_FLRAGG),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oFLRAGG_4_20.GetRadio()
    this.Parent.oContained.w_FLRAGG = this.RadioValue()
    return .t.
  endfunc

  func oFLRAGG_4_20.ToRadio()
    this.Parent.oContained.w_FLRAGG=trim(this.Parent.oContained.w_FLRAGG)
    return(;
      iif(this.Parent.oContained.w_FLRAGG=='S',1,;
      0))
  endfunc

  func oFLRAGG_4_20.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oALFDOCFIN_4_33 as StdField with uid="UDFMAGZZLL",rtseq=83,rtrep=.f.,;
    cFormVar = "w_ALFDOCFIN", cQueryName = "ALFDOCFIN",;
    bObbl = .f. , nPag = 4, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di fine selezione",;
    HelpContextID = 40796463,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=350, Top=160, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oCDESCRI_4_38 as StdField with uid="HXAGQHGXLI",rtseq=84,rtrep=.f.,;
    cFormVar = "w_CDESCRI", cQueryName = "CDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 256023514,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=350, Top=188, InputMask=replicate('X',35)

  add object oZDESCRI_4_39 as StdField with uid="EZCYTRAVNP",rtseq=85,rtrep=.f.,;
    cFormVar = "w_ZDESCRI", cQueryName = "ZDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 256023146,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=350, Top=216, InputMask=replicate('X',35)

  add object oMDESCRI_4_40 as StdField with uid="GPYMOYJYIM",rtseq=86,rtrep=.f.,;
    cFormVar = "w_MDESCRI", cQueryName = "MDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 256023354,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=350, Top=244, InputMask=replicate('X',40)

  add object oStr_4_23 as StdString with uid="KBWKFHLCPV",Visible=.t., Left=16, Top=78,;
    Alignment=1, Width=164, Height=15,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  add object oStr_4_24 as StdString with uid="FMRAZJBFJV",Visible=.t., Left=16, Top=20,;
    Alignment=1, Width=164, Height=15,;
    Caption="Causale contabile:"  ;
  , bGlobalFont=.t.

  func oStr_4_24.mHide()
    with this.Parent.oContained
      return (g_COGE<>'S')
    endwith
  endfunc

  add object oStr_4_29 as StdString with uid="GCOEUCJJPE",Visible=.t., Left=16, Top=270,;
    Alignment=1, Width=164, Height=18,;
    Caption="Ordine di assegnazione per:"  ;
  , bGlobalFont=.t.

  add object oStr_4_30 as StdString with uid="ZQRHQPFMCR",Visible=.t., Left=16, Top=105,;
    Alignment=1, Width=164, Height=18,;
    Caption="Data documento da:"  ;
  , bGlobalFont=.t.

  add object oStr_4_31 as StdString with uid="SMCKSHDWJW",Visible=.t., Left=324, Top=105,;
    Alignment=1, Width=18, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_4_32 as StdString with uid="ZTUVLCUJKG",Visible=.t., Left=16, Top=300,;
    Alignment=1, Width=164, Height=18,;
    Caption="Importo minimo:"  ;
  , bGlobalFont=.t.

  add object oStr_4_34 as StdString with uid="QXFCXSPDDF",Visible=.t., Left=324, Top=133,;
    Alignment=1, Width=18, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_4_35 as StdString with uid="FGJNGOTUZM",Visible=.t., Left=16, Top=133,;
    Alignment=1, Width=164, Height=18,;
    Caption="Numero documento da:"  ;
  , bGlobalFont=.t.

  add object oStr_4_36 as StdString with uid="PYGVZLENXL",Visible=.t., Left=324, Top=161,;
    Alignment=1, Width=18, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_4_37 as StdString with uid="YHYXORPVVE",Visible=.t., Left=16, Top=161,;
    Alignment=1, Width=164, Height=18,;
    Caption="Serie documento da:"  ;
  , bGlobalFont=.t.

  add object oStr_4_41 as StdString with uid="GBBCDVHBRQ",Visible=.t., Left=16, Top=187,;
    Alignment=1, Width=164, Height=18,;
    Caption="Categoria commerciale:"  ;
  , bGlobalFont=.t.

  add object oStr_4_42 as StdString with uid="YDDJXRHOVZ",Visible=.t., Left=16, Top=216,;
    Alignment=1, Width=164, Height=18,;
    Caption="Zone:"  ;
  , bGlobalFont=.t.

  add object oStr_4_43 as StdString with uid="RWRWCTBOZV",Visible=.t., Left=16, Top=243,;
    Alignment=1, Width=164, Height=18,;
    Caption="Mastro contabile:"  ;
  , bGlobalFont=.t.
enddefine

* --- Defining Body row
define class tgste_mcdBodyRow as CPBodyRowCnt
  Width=794
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oDICODCON_2_2 as StdTrsField with uid="JJBXSQUBQQ",rtseq=33,rtrep=.t.,;
    cFormVar="w_DICODCON",value=space(15),;
    ToolTipText = "Conto banca di presentazione",;
    HelpContextID = 238466684,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Conto banca inesistente o obsoleto o incongruente o codice IBAN/BBAN mancanti",;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=-2, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_DICODCON"

  func oDICODCON_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oDICODCON_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oDICODCON_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oDICODCON_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Elenco banche",'GSTE_MCB.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oDICODCON_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_DICODCON
    i_obj.ecpSave()
  endproc

  add object oDIIMPPRO_2_3 as StdTrsField with uid="UQEQWQRJAD",rtseq=34,rtrep=.t.,;
    cFormVar="w_DIIMPPRO",value=0,;
    ToolTipText = "Totale importo da ripartire",;
    HelpContextID = 260548997,;
    cTotal = "this.Parent.oContained.w_totali1", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=388, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oDIIMPPRO_2_3.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DICODCON))
    endwith
  endfunc

  add object oDISPECOM_2_4 as StdTrsField with uid="UFACWCLCZP",rtseq=35,rtrep=.t.,;
    cFormVar="w_DISPECOM",value=0,;
    ToolTipText = "Spese commissioni (in valuta di conto)",;
    HelpContextID = 237287037,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=520, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oDISPECOM_2_4.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_DICODCON) And g_COGE='S')
    endwith
  endfunc

  add object oDIIMPCAL_2_5 as StdTrsField with uid="WZKQRYKOWP",rtseq=36,rtrep=.t.,;
    cFormVar="w_DIIMPCAL",value=0,enabled=.f.,;
    ToolTipText = "Importo calcolato",;
    HelpContextID = 42445186,;
    cTotal = "this.Parent.oContained.w_totali", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=661, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)], width=124

  add object oDESBAC_2_7 as StdTrsField with uid="MAACLRDOHN",rtseq=38,rtrep=.t.,;
    cFormVar="w_DESBAC",value=space(40),enabled=.f.,;
    ToolTipText = "Descrizione conto banca",;
    HelpContextID = 242399946,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=254, Left=130, Top=0, InputMask=replicate('X',40)
  add object oLast as LastKeyMover
  * ---
  func oDICODCON_2_2.When()
    return(.t.)
  proc oDICODCON_2_2.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oDICODCON_2_2.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_mcd','DIS_COMP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DISERIAL=DIS_COMP.DISERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gste_mcd
define class StdZTamTableCombo as StdTableCombo

proc Popola()
    local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
    LOCAL i_fk,i_fd
    i_curs=sys(2015)
    IF LOWER(RIGHT(this.cTable,4))='.vqr'
      vq_exec(this.cTable,this.parent.ocontained,i_curs)
      i_fk=this.cKey
      i_fd=this.cValue
    else
      i_nIdx=cp_OpenTable(this.cTable)
      if i_nIdx<>0
        i_nConn=i_TableProp[i_nIdx,3]
        i_cTable=cp_SetAzi(i_TableProp[i_nIdx,2])
        i_n1=this.cKey
        i_n2=this.cValue
        IF !EMPTY(this.cOrderBy)
          i_n3=' order by '+this.cOrderBy
        ELSE
          i_n3=''
        ENDIF
        i_flt=IIF(EMPTY(this.tablefilter),'',' where '+this.tablefilter)
        if i_nConn<>0
          cp_sql(i_nConn,"select "+i_n1+" as combokey,"+i_n2+" as combodescr from "+i_cTable+i_flt+i_n3,i_curs)
        else
          select &i_n1 as combokey,&i_n2 as combodescr from (i_cTable) &i_flt &i_n3 into cursor (i_curs)
        ENDIF
        i_fk='combokey'
        i_fd='combodescr'
        cp_CloseTable(this.cTable)
      ENDIF
    ENDIF
    if used(i_curs)
      select (i_curs)
      this.nValues=reccount()
      dimension this.combovalues[MAX(1,this.nValues)]
      If this.nValues<1
       this.combovalues[1]=cp_NullValue(this.RadioValue())
      endif
      this.Clear
      i_bCharKey=type(i_fk)='C'
      do while !eof()
        this.AddItem(iif(type(i_fd)='C',ALLTRIM(&i_fd),ALLTRIM(str(&i_fd))))
        if i_bCharKey
          this.combovalues[recno()]=trim(&i_fk)
        else
          this.combovalues[recno()]=&i_fk
        endif
        skip
      enddo
      use
    endif

enddefine
* --- Fine Area Manuale
