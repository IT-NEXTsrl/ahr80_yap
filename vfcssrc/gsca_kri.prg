* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_kri                                                        *
*              Ripartizione per c./C.R.                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_14]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-21                                                      *
* Last revis.: 2007-07-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsca_kri",oParentObject))

* --- Class definition
define class tgsca_kri as StdForm
  Top    = 50
  Left   = 100

  * --- Standard Properties
  Width  = 441
  Height = 169
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-11"
  HelpContextID=152402583
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=8

  * --- Constant Properties
  _IDX = 0
  CAL_RIPA_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gsca_kri"
  cComment = "Ripartizione per c./C.R."
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_TIPMOV = space(1)
  w_PERIOD = space(5)
  w_DESPER = space(35)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_CODAZI = space(5)
  w_PERCOMPE = space(1)
  w_SUCOMMES = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsca_kriPag1","gsca_kri",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPMOV_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CAL_RIPA'
    this.cWorkTables[2]='AZIENDA'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCA_BRI with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsca_kri
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPMOV=space(1)
      .w_PERIOD=space(5)
      .w_DESPER=space(35)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_CODAZI=space(5)
      .w_PERCOMPE=space(1)
      .w_SUCOMMES=space(1)
        .w_TIPMOV = 'E'
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_PERIOD))
          .link_1_2('Full')
        endif
          .DoRTCalc(3,5,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODAZI))
          .link_1_12('Full')
        endif
        .w_PERCOMPE = ' '
        .w_SUCOMMES = ' '
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,5,.t.)
          .link_1_12('Full')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(7,8,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_6.enabled = this.oPgFrm.Page1.oPag.oBtn_1_6.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PERIOD
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAL_RIPA_IDX,3]
    i_lTable = "CAL_RIPA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAL_RIPA_IDX,2], .t., this.CAL_RIPA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAL_RIPA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PERIOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCA_ACA',True,'CAL_RIPA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CAPERIOD like "+cp_ToStrODBC(trim(this.w_PERIOD)+"%");

          i_ret=cp_SQL(i_nConn,"select CAPERIOD,CADESCRI,CADATINI,CADATFIN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CAPERIOD","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CAPERIOD',trim(this.w_PERIOD))
          select CAPERIOD,CADESCRI,CADATINI,CADATFIN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CAPERIOD into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PERIOD)==trim(_Link_.CAPERIOD) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PERIOD) and !this.bDontReportError
            deferred_cp_zoom('CAL_RIPA','*','CAPERIOD',cp_AbsName(oSource.parent,'oPERIOD_1_2'),i_cWhere,'GSCA_ACA',"Calendario di ripartizione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CAPERIOD,CADESCRI,CADATINI,CADATFIN";
                     +" from "+i_cTable+" "+i_lTable+" where CAPERIOD="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CAPERIOD',oSource.xKey(1))
            select CAPERIOD,CADESCRI,CADATINI,CADATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PERIOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CAPERIOD,CADESCRI,CADATINI,CADATFIN";
                   +" from "+i_cTable+" "+i_lTable+" where CAPERIOD="+cp_ToStrODBC(this.w_PERIOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CAPERIOD',this.w_PERIOD)
            select CAPERIOD,CADESCRI,CADATINI,CADATFIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PERIOD = NVL(_Link_.CAPERIOD,space(5))
      this.w_DESPER = NVL(_Link_.CADESCRI,space(35))
      this.w_DATINI = NVL(cp_ToDate(_Link_.CADATINI),ctod("  /  /  "))
      this.w_DATFIN = NVL(cp_ToDate(_Link_.CADATFIN),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_PERIOD = space(5)
      endif
      this.w_DESPER = space(35)
      this.w_DATINI = ctod("  /  /  ")
      this.w_DATFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAL_RIPA_IDX,2])+'\'+cp_ToStr(_Link_.CAPERIOD,1)
      cp_ShowWarn(i_cKey,this.CAL_RIPA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PERIOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZRIPCOM,AZFLRIPC";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZRIPCOM,AZFLRIPC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(5))
      this.w_PERCOMPE = NVL(_Link_.AZRIPCOM,space(1))
      this.w_SUCOMMES = NVL(_Link_.AZFLRIPC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(5)
      endif
      this.w_PERCOMPE = space(1)
      this.w_SUCOMMES = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPMOV_1_1.RadioValue()==this.w_TIPMOV)
      this.oPgFrm.Page1.oPag.oTIPMOV_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIOD_1_2.value==this.w_PERIOD)
      this.oPgFrm.Page1.oPag.oPERIOD_1_2.value=this.w_PERIOD
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPER_1_3.value==this.w_DESPER)
      this.oPgFrm.Page1.oPag.oDESPER_1_3.value=this.w_DESPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_4.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_4.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_5.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_5.value=this.w_DATFIN
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PERIOD))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPERIOD_1_2.SetFocus()
            i_bnoObbl = !empty(.w_PERIOD)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsca_kriPag1 as StdContainer
  Width  = 437
  height = 169
  stdWidth  = 437
  stdheight = 169
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPMOV_1_1 as StdCombo with uid="ABDIPEVUIB",rtseq=1,rtrep=.f.,left=101,top=12,width=100,height=21;
    , ToolTipText = "Tipo movimenti di analitica da elaborare: effettivi o previsionali";
    , HelpContextID = 72862006;
    , cFormVar="w_TIPMOV",RowSource=""+"Effettivi,"+"Previsionali", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPMOV_1_1.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oTIPMOV_1_1.GetRadio()
    this.Parent.oContained.w_TIPMOV = this.RadioValue()
    return .t.
  endfunc

  func oTIPMOV_1_1.SetRadio()
    this.Parent.oContained.w_TIPMOV=trim(this.Parent.oContained.w_TIPMOV)
    this.value = ;
      iif(this.Parent.oContained.w_TIPMOV=='E',1,;
      iif(this.Parent.oContained.w_TIPMOV=='P',2,;
      0))
  endfunc

  add object oPERIOD_1_2 as StdField with uid="VCRXHMSUJE",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PERIOD", cQueryName = "PERIOD",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Periodo da elaborare",;
    HelpContextID = 39052534,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=101, Top=55, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAL_RIPA", cZoomOnZoom="GSCA_ACA", oKey_1_1="CAPERIOD", oKey_1_2="this.w_PERIOD"

  func oPERIOD_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oPERIOD_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPERIOD_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAL_RIPA','*','CAPERIOD',cp_AbsName(this.parent,'oPERIOD_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCA_ACA',"Calendario di ripartizione",'',this.parent.oContained
  endproc
  proc oPERIOD_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSCA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CAPERIOD=this.parent.oContained.w_PERIOD
     i_obj.ecpSave()
  endproc

  add object oDESPER_1_3 as StdField with uid="LXAYFWODHH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESPER", cQueryName = "DESPER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 263910454,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=173, Top=55, InputMask=replicate('X',35)

  add object oDATINI_1_4 as StdField with uid="HWMDOWWRQK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 121897014,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=101, Top=85

  add object oDATFIN_1_5 as StdField with uid="XJXILKCKLN",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 200343606,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=219, Top=85


  add object oBtn_1_6 as StdButton with uid="ELGCRTQTOV",left=330, top=119, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Conferma";
    , HelpContextID = 152431334;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_6.Click()
      with this.Parent.oContained
        do GSCA_BRI with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_6.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_period))
      endwith
    endif
  endfunc


  add object oBtn_1_7 as StdButton with uid="KXXCKCJFAZ",left=382, top=119, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Annulla";
    , HelpContextID = 159720006;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_8 as StdString with uid="LIPKKSKXOT",Visible=.t., Left=4, Top=12,;
    Alignment=1, Width=94, Height=15,;
    Caption="Tipo movimenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="OJUWISBAZF",Visible=.t., Left=4, Top=55,;
    Alignment=1, Width=94, Height=15,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="KWGHQVWTRC",Visible=.t., Left=4, Top=85,;
    Alignment=1, Width=94, Height=15,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="HXJQQYUYQO",Visible=.t., Left=183, Top=85,;
    Alignment=1, Width=34, Height=15,;
    Caption="Al:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsca_kri','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
