* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_sag                                                        *
*              Stampa agenti                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_5]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-11                                                      *
* Last revis.: 2007-07-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_sag",oParentObject))

* --- Class definition
define class tgsar_sag as StdForm
  Top    = 50
  Left   = 100

  * --- Standard Properties
  Width  = 519
  Height = 225
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-05"
  HelpContextID=124352361
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Constant Properties
  _IDX = 0
  AGENTI_IDX = 0
  GRUPRO_IDX = 0
  cPrg = "gsar_sag"
  cComment = "Stampa agenti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_BCODICE = space(5)
  w_ECODICE = space(5)
  w_ZONA = space(5)
  w_PROVVIG = space(5)
  w_PDATINIZ = ctod('  /  /  ')
  o_PDATINIZ = ctod('  /  /  ')
  w_obsodat1 = space(1)
  w_DESCRI = space(50)
  w_DESCRI1 = space(50)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_TIPAGE = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_sagPag1","gsar_sag",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oBCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='AGENTI'
    this.cWorkTables[2]='GRUPRO'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsar_sag
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_BCODICE=space(5)
      .w_ECODICE=space(5)
      .w_ZONA=space(5)
      .w_PROVVIG=space(5)
      .w_PDATINIZ=ctod("  /  /  ")
      .w_obsodat1=space(1)
      .w_DESCRI=space(50)
      .w_DESCRI1=space(50)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_TIPAGE=space(1)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_BCODICE))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_ECODICE))
          .link_1_2('Full')
        endif
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_ZONA))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_PROVVIG))
          .link_1_4('Full')
        endif
        .w_PDATINIZ = i_datsys
        .w_obsodat1 = 'N'
      .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
          .DoRTCalc(7,8,.f.)
        .w_OBTEST = .w_PDATINIZ
          .DoRTCalc(10,10,.f.)
        .w_TIPAGE = 'C'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
        .DoRTCalc(1,8,.t.)
        if .o_PDATINIZ<>.w_PDATINIZ
            .w_OBTEST = .w_PDATINIZ
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(10,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_11.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_11.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=BCODICE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_BCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_BCODICE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BCODICE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_BCODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_BCODICE)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_BCODICE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oBCODICE_1_1'),i_cWhere,'',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_BCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_BCODICE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BCODICE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESCRI = NVL(_Link_.AGDESAGE,space(50))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_BCODICE = space(5)
      endif
      this.w_DESCRI = space(50)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_ECODICE)) OR  (.w_BCODICE<=.w_ECODICE))  and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_BCODICE = space(5)
        this.w_DESCRI = space(50)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ECODICE
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ECODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_ECODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_ECODICE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ECODICE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_ECODICE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_ECODICE)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ECODICE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oECODICE_1_2'),i_cWhere,'',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ECODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_ECODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_ECODICE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ECODICE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESCRI1 = NVL(_Link_.AGDESAGE,space(50))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ECODICE = space(5)
      endif
      this.w_DESCRI1 = space(50)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_ECODICE>=.w_BCODICE) or (empty(.w_Bcodice))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_ECODICE = space(5)
        this.w_DESCRI1 = space(50)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ECODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ZONA
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ZONA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_ZONA)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGTIPAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_ZONA))
          select AGCODAGE,AGTIPAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ZONA)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ZONA) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oZONA_1_3'),i_cWhere,'GSAR_AGE',"Agenti",'GSAR1AGE.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGTIPAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGTIPAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ZONA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGTIPAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_ZONA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_ZONA)
            select AGCODAGE,AGTIPAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ZONA = NVL(_Link_.AGCODAGE,space(5))
      this.w_TIPAGE = NVL(_Link_.AGTIPAGE,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ZONA = space(5)
      endif
      this.w_TIPAGE = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TIPAGE='C') AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Capo area inesistente oppure obsoleto")
        endif
        this.w_ZONA = space(5)
        this.w_TIPAGE = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ZONA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PROVVIG
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUPRO_IDX,3]
    i_lTable = "GRUPRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2], .t., this.GRUPRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PROVVIG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGP',True,'GRUPRO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_PROVVIG)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_PROVVIG))
          select GPCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PROVVIG)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PROVVIG) and !this.bDontReportError
            deferred_cp_zoom('GRUPRO','*','GPCODICE',cp_AbsName(oSource.parent,'oPROVVIG_1_4'),i_cWhere,'GSAR_AGP',"Provvigioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PROVVIG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_PROVVIG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_PROVVIG)
            select GPCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PROVVIG = NVL(_Link_.GPCODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PROVVIG = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUPRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PROVVIG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oBCODICE_1_1.value==this.w_BCODICE)
      this.oPgFrm.Page1.oPag.oBCODICE_1_1.value=this.w_BCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oECODICE_1_2.value==this.w_ECODICE)
      this.oPgFrm.Page1.oPag.oECODICE_1_2.value=this.w_ECODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oZONA_1_3.value==this.w_ZONA)
      this.oPgFrm.Page1.oPag.oZONA_1_3.value=this.w_ZONA
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVVIG_1_4.value==this.w_PROVVIG)
      this.oPgFrm.Page1.oPag.oPROVVIG_1_4.value=this.w_PROVVIG
    endif
    if not(this.oPgFrm.Page1.oPag.oPDATINIZ_1_5.value==this.w_PDATINIZ)
      this.oPgFrm.Page1.oPag.oPDATINIZ_1_5.value=this.w_PDATINIZ
    endif
    if not(this.oPgFrm.Page1.oPag.oobsodat1_1_6.RadioValue()==this.w_obsodat1)
      this.oPgFrm.Page1.oPag.oobsodat1_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_9.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_9.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_10.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_10.value=this.w_DESCRI1
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((empty(.w_ECODICE)) OR  (.w_BCODICE<=.w_ECODICE))  and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_BCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oBCODICE_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((.w_ECODICE>=.w_BCODICE) or (empty(.w_Bcodice))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_ECODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oECODICE_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not((.w_TIPAGE='C') AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_ZONA))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oZONA_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Capo area inesistente oppure obsoleto")
          case   (empty(.w_PDATINIZ))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPDATINIZ_1_5.SetFocus()
            i_bnoObbl = !empty(.w_PDATINIZ)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PDATINIZ = this.w_PDATINIZ
    return

enddefine

* --- Define pages as container
define class tgsar_sagPag1 as StdContainer
  Width  = 515
  height = 225
  stdWidth  = 515
  stdheight = 225
  resizeXpos=346
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oBCODICE_1_1 as StdField with uid="RUGEEAECOO",rtseq=1,rtrep=.f.,;
    cFormVar = "w_BCODICE", cQueryName = "BCODICE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Agente di inizio selezione",;
    HelpContextID = 7323670,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=117, Top=8, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_BCODICE"

  func oBCODICE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oBCODICE_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBCODICE_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oBCODICE_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Agenti",'',this.parent.oContained
  endproc

  add object oECODICE_1_2 as StdField with uid="HBKERADVMO",rtseq=2,rtrep=.f.,;
    cFormVar = "w_ECODICE", cQueryName = "ECODICE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Agente di fine selezione",;
    HelpContextID = 7323718,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=117, Top=37, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", oKey_1_1="AGCODAGE", oKey_1_2="this.w_ECODICE"

  func oECODICE_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oECODICE_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oECODICE_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oECODICE_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Agenti",'',this.parent.oContained
  endproc

  add object oZONA_1_3 as StdField with uid="AVMJATKNFV",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ZONA", cQueryName = "ZONA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Capo area inesistente oppure obsoleto",;
    ToolTipText = "Capo area selezionato",;
    HelpContextID = 119751274,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=117, Top=66, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_ZONA"

  func oZONA_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oZONA_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oZONA_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oZONA_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'GSAR1AGE.AGENTI_VZM',this.parent.oContained
  endproc
  proc oZONA_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_ZONA
     i_obj.ecpSave()
  endproc

  add object oPROVVIG_1_4 as StdField with uid="QZFDGWELYN",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PROVVIG", cQueryName = "PROVVIG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Categoria provvigione selezionata",;
    HelpContextID = 145633290,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=117, Top=95, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUPRO", cZoomOnZoom="GSAR_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_PROVVIG"

  func oPROVVIG_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oPROVVIG_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPROVVIG_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUPRO','*','GPCODICE',cp_AbsName(this.parent,'oPROVVIG_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGP',"Provvigioni",'',this.parent.oContained
  endproc
  proc oPROVVIG_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_PROVVIG
     i_obj.ecpSave()
  endproc

  add object oPDATINIZ_1_5 as StdField with uid="SQFYCRIZLP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PDATINIZ", cQueryName = "PDATINIZ",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di riferimento sul controllo dell'obsolescenza",;
    HelpContextID = 75570608,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=117, Top=123

  add object oobsodat1_1_6 as StdCheck with uid="SMYQIFUAIH",rtseq=6,rtrep=.f.,left=252, top=123, caption="Solo obsoleti",;
    ToolTipText = "Stampa solo obsoleti",;
    HelpContextID = 263380457,;
    cFormVar="w_obsodat1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oobsodat1_1_6.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oobsodat1_1_6.GetRadio()
    this.Parent.oContained.w_obsodat1 = this.RadioValue()
    return .t.
  endfunc

  func oobsodat1_1_6.SetRadio()
    this.Parent.oContained.w_obsodat1=trim(this.Parent.oContained.w_obsodat1)
    this.value = ;
      iif(this.Parent.oContained.w_obsodat1=='S',1,;
      0)
  endfunc

  add object oDESCRI_1_9 as StdField with uid="NSXHIDHJRZ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 117375542,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=179, Top=8, InputMask=replicate('X',50)

  add object oDESCRI1_1_10 as StdField with uid="TFGSAYYLTZ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 117375542,;
   bGlobalFont=.t.,;
    Height=21, Width=265, Left=179, Top=37, InputMask=replicate('X',50)


  add object oObj_1_11 as cp_outputCombo with uid="ZLERXIVPWT",left=117, top=151, width=384,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 53645286


  add object oBtn_1_12 as StdButton with uid="LCRGBOYHEM",left=401, top=176, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 17437222;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_13 as StdButton with uid="HZTGKBFHXG",left=453, top=176, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 117034938;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_13.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_7 as StdString with uid="BRGJYHYVYY",Visible=.t., Left=21, Top=8,;
    Alignment=1, Width=94, Height=15,;
    Caption="Da agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="ECQLBYLHVT",Visible=.t., Left=21, Top=37,;
    Alignment=1, Width=94, Height=15,;
    Caption="Ad agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="YUMTFCOKWY",Visible=.t., Left=21, Top=151,;
    Alignment=1, Width=94, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="OFEPGMBSEX",Visible=.t., Left=16, Top=95,;
    Alignment=1, Width=99, Height=15,;
    Caption="Cat.provvigione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_16 as StdString with uid="NGCLNVKRHR",Visible=.t., Left=21, Top=123,;
    Alignment=1, Width=94, Height=15,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="OOCKGXPFIN",Visible=.t., Left=21, Top=66,;
    Alignment=1, Width=94, Height=15,;
    Caption="Capo area:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_sag','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
