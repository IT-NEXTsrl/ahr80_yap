* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bcm                                                        *
*              Determina mvflerif dettaglio documenti                          *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_5]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-02-15                                                      *
* Last revis.: 2006-03-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pGest,pRowNum,pSerrif,pRowrif,pOldEvas
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bcm",oParentObject,m.pGest,m.pRowNum,m.pSerrif,m.pRowrif,m.pOldEvas)
return(i_retval)

define class tgsar_bcm as StdBatch
  * --- Local variables
  pGest = .NULL.
  pRowNum = 0
  pSerrif = space(10)
  pRowrif = 0
  pOldEvas = space(1)
  w_NUMREC = 0
  w_ROWINDEX = 0
  w_L_OLDEVAS = space(1)
  w_L_ROWNUM = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Uniformo MVFLERIF sulle altre righe che evadono la riga nel documento, intervento
    *     necessario per evitare che la query GSVE2AGG aggiorni in modo errato
    *     MVFLEVAS sul documento di origine.
    *     Riceve come parametri:
    *     a) Riferimento gestione documentale
    *     b) Riga in esame
    *     c) Riferimenti riga che evade
    *     d) Flag evaso documento di origine (utilzizato anche come var. di stato della riga)
    this.w_L_OLDEVAS = this.pOLDEVAS
    this.w_L_ROWNUM = this.pRowNum
    * --- Salvo la riga, per confermare tutte le caller eventualmente valorizzate
    this.pGest.SaveRow()     
    this.w_ROWINDEX = this.pGest.RowIndex()
    this.w_NUMREC = this.pGest.Search( "NVL(t_MVSERRIF, Space(10)) = "+cp_ToStrODBC(this.pSerrif)+" AND NVL(t_MVROWRIF, 0) = "+cp_ToStrODBC( this.pRowRif )+" AND CPROWNUM <> "+cp_ToStrODBC( this.w_L_ROWNUM )+ " AND NOT DELETED() ")
    do while this.w_NUMREC<>-1
      this.pGest.SetRow(this.w_NUMREC)     
      this.pGest.Set("w_MVFLERIF" , IIF( this.w_L_OLDEVAS="E" , "S" ," " ))     
      * --- Uniformo anche w_OLDEVAS per garantire il codice di valorizzazione
      *     di MVFLERIF in GSVE_BCK
      this.pGest.Set("w_OLDEVAS" , this.w_L_OLDEVAS)     
      this.w_NUMREC = this.pGest.Search( "NVL(t_MVSERRIF, Space(10)) = "+cp_ToStrODBC(this.pSerrif)+" AND NVL(t_MVROWRIF, 0) = "+cp_ToStrODBC( this.pRowRif )+" AND CPROWNUM <> "+cp_ToStrODBC( this.w_L_ROWNUM )+ " AND NOT DELETED()" , this.w_NUMREC)
    enddo
    * --- Mi rimetto nella riga in esame..
    this.pGest.SetRow(this.w_ROWINDEX)     
  endproc


  proc Init(oParentObject,pGest,pRowNum,pSerrif,pRowrif,pOldEvas)
    this.pGest=pGest
    this.pRowNum=pRowNum
    this.pSerrif=pSerrif
    this.pRowrif=pRowrif
    this.pOldEvas=pOldEvas
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pGest,pRowNum,pSerrif,pRowrif,pOldEvas"
endproc
