* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscf_kpl                                                        *
*              Pulizia log controllo flussi                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-03-14                                                      *
* Last revis.: 2012-07-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscf_kpl",oParentObject))

* --- Class definition
define class tgscf_kpl as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 689
  Height = 393
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-07-03"
  HelpContextID=118868631
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  XDC_TABLE_IDX = 0
  REF_MAST_IDX = 0
  cPrg = "gscf_kpl"
  cComment = "Pulizia log controllo flussi"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_REGOLA = space(10)
  w_REDESCRI = space(50)
  w_TABELLA = space(20)
  w_TBMNAME = space(30)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_FLMONINS = space(1)
  w_FLMONMOD = space(1)
  w_FLMONDEL = space(1)
  w_zoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscf_kplPag1","gscf_kpl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oREGOLA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_zoom = this.oPgFrm.Pages(1).oPag.zoom
    DoDefault()
    proc Destroy()
      this.w_zoom = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='XDC_TABLE'
    this.cWorkTables[2]='REF_MAST'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do gscf_bpl with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_REGOLA=space(10)
      .w_REDESCRI=space(50)
      .w_TABELLA=space(20)
      .w_TBMNAME=space(30)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_FLMONINS=space(1)
      .w_FLMONMOD=space(1)
      .w_FLMONDEL=space(1)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_REGOLA))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,3,.f.)
        if not(empty(.w_TABELLA))
          .link_1_4('Full')
        endif
          .DoRTCalc(4,6,.f.)
        .w_FLMONINS = 'I'
        .w_FLMONMOD = 'R'
        .w_FLMONDEL = 'D'
      .oPgFrm.Page1.oPag.zoom.Calculate()
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.zoom.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.zoom.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.zoom.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=REGOLA
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.REF_MAST_IDX,3]
    i_lTable = "REF_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2], .t., this.REF_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_REGOLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gscf_mre',True,'REF_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RESERIAL like "+cp_ToStrODBC(trim(this.w_REGOLA)+"%");

          i_ret=cp_SQL(i_nConn,"select RESERIAL,REDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RESERIAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RESERIAL',trim(this.w_REGOLA))
          select RESERIAL,REDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RESERIAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_REGOLA)==trim(_Link_.RESERIAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" REDESCRI like "+cp_ToStrODBC(trim(this.w_REGOLA)+"%");

            i_ret=cp_SQL(i_nConn,"select RESERIAL,REDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" REDESCRI like "+cp_ToStr(trim(this.w_REGOLA)+"%");

            select RESERIAL,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_REGOLA) and !this.bDontReportError
            deferred_cp_zoom('REF_MAST','*','RESERIAL',cp_AbsName(oSource.parent,'oREGOLA_1_1'),i_cWhere,'gscf_mre',"REGOLE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RESERIAL,REDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RESERIAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RESERIAL',oSource.xKey(1))
            select RESERIAL,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_REGOLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RESERIAL,REDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RESERIAL="+cp_ToStrODBC(this.w_REGOLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RESERIAL',this.w_REGOLA)
            select RESERIAL,REDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_REGOLA = NVL(_Link_.RESERIAL,space(10))
      this.w_REDESCRI = NVL(_Link_.REDESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_REGOLA = space(10)
      endif
      this.w_REDESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.REF_MAST_IDX,2])+'\'+cp_ToStr(_Link_.RESERIAL,1)
      cp_ShowWarn(i_cKey,this.REF_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_REGOLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TABELLA
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.XDC_TABLE_IDX,3]
    i_lTable = "XDC_TABLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2], .t., this.XDC_TABLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TABELLA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'XDC_TABLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TBNAME like "+cp_ToStrODBC(trim(this.w_TABELLA)+"%");

          i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TBNAME","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TBNAME',trim(this.w_TABELLA))
          select TBNAME,TBCOMMENT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TBNAME into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TABELLA)==trim(_Link_.TBNAME) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" TBCOMMENT like "+cp_ToStrODBC(trim(this.w_TABELLA)+"%");

            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" TBCOMMENT like "+cp_ToStr(trim(this.w_TABELLA)+"%");

            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_TABELLA) and !this.bDontReportError
            deferred_cp_zoom('XDC_TABLE','*','TBNAME',cp_AbsName(oSource.parent,'oTABELLA_1_4'),i_cWhere,'',"TABELLE",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                     +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',oSource.xKey(1))
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TABELLA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TBNAME,TBCOMMENT";
                   +" from "+i_cTable+" "+i_lTable+" where TBNAME="+cp_ToStrODBC(this.w_TABELLA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TBNAME',this.w_TABELLA)
            select TBNAME,TBCOMMENT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TABELLA = NVL(_Link_.TBNAME,space(20))
      this.w_TBMNAME = NVL(_Link_.TBCOMMENT,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_TABELLA = space(20)
      endif
      this.w_TBMNAME = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.XDC_TABLE_IDX,2])+'\'+cp_ToStr(_Link_.TBNAME,1)
      cp_ShowWarn(i_cKey,this.XDC_TABLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TABELLA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oREGOLA_1_1.value==this.w_REGOLA)
      this.oPgFrm.Page1.oPag.oREGOLA_1_1.value=this.w_REGOLA
    endif
    if not(this.oPgFrm.Page1.oPag.oREDESCRI_1_3.value==this.w_REDESCRI)
      this.oPgFrm.Page1.oPag.oREDESCRI_1_3.value=this.w_REDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTABELLA_1_4.value==this.w_TABELLA)
      this.oPgFrm.Page1.oPag.oTABELLA_1_4.value=this.w_TABELLA
    endif
    if not(this.oPgFrm.Page1.oPag.oTBMNAME_1_5.value==this.w_TBMNAME)
      this.oPgFrm.Page1.oPag.oTBMNAME_1_5.value=this.w_TBMNAME
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_7.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_7.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_9.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_9.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oFLMONINS_1_11.RadioValue()==this.w_FLMONINS)
      this.oPgFrm.Page1.oPag.oFLMONINS_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLMONMOD_1_12.RadioValue()==this.w_FLMONMOD)
      this.oPgFrm.Page1.oPag.oFLMONMOD_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLMONDEL_1_13.RadioValue()==this.w_FLMONDEL)
      this.oPgFrm.Page1.oPag.oFLMONDEL_1_13.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscf_kplPag1 as StdContainer
  Width  = 685
  height = 393
  stdWidth  = 685
  stdheight = 393
  resizeXpos=355
  resizeYpos=262
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oREGOLA_1_1 as StdField with uid="ZPJLDPKAJF",rtseq=1,rtrep=.f.,;
    cFormVar = "w_REGOLA", cQueryName = "REGOLA",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Regola selezionata",;
    HelpContextID = 220824854,;
   bGlobalFont=.t.,;
    Height=21, Width=92, Left=72, Top=5, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="REF_MAST", cZoomOnZoom="gscf_mre", oKey_1_1="RESERIAL", oKey_1_2="this.w_REGOLA"

  func oREGOLA_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oREGOLA_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oREGOLA_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'REF_MAST','*','RESERIAL',cp_AbsName(this.parent,'oREGOLA_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'gscf_mre',"REGOLE",'',this.parent.oContained
  endproc
  proc oREGOLA_1_1.mZoomOnZoom
    local i_obj
    i_obj=gscf_mre()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RESERIAL=this.parent.oContained.w_REGOLA
     i_obj.ecpSave()
  endproc

  add object oREDESCRI_1_3 as StdField with uid="EVVLALBFBN",rtseq=2,rtrep=.f.,;
    cFormVar = "w_REDESCRI", cQueryName = "REDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 7383713,;
   bGlobalFont=.t.,;
    Height=21, Width=205, Left=165, Top=5, InputMask=replicate('X',50)

  add object oTABELLA_1_4 as StdField with uid="KIPBJYZHPI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_TABELLA", cQueryName = "TABELLA",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Tabella selezionata",;
    HelpContextID = 132173514,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=72, Top=32, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="XDC_TABLE", oKey_1_1="TBNAME", oKey_1_2="this.w_TABELLA"

  func oTABELLA_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oTABELLA_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTABELLA_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'XDC_TABLE','*','TBNAME',cp_AbsName(this.parent,'oTABELLA_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"TABELLE",'',this.parent.oContained
  endproc

  add object oTBMNAME_1_5 as StdField with uid="KTMXBCDDHK",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TBMNAME", cQueryName = "TBMNAME",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 126295498,;
   bGlobalFont=.t.,;
    Height=21, Width=185, Left=185, Top=32, InputMask=replicate('X',30)

  add object oDATINI_1_7 as StdField with uid="PNLJJVIKHZ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Visualizza le operazioni a partire dalla data selezionata",;
    HelpContextID = 180072394,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=428, Top=5

  add object oDATFIN_1_9 as StdField with uid="ESFBIIFGZN",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Visualizza le operazioni fino alla data selezionata",;
    HelpContextID = 101625802,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=428, Top=32

  add object oFLMONINS_1_11 as StdCheck with uid="IVQTWDUKYU",rtseq=7,rtrep=.f.,left=518, top=4, caption="Inserimento",;
    ToolTipText = "Se attivo: visualizza le operazioni di tipo inserimento",;
    HelpContextID = 88730537,;
    cFormVar="w_FLMONINS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLMONINS_1_11.RadioValue()
    return(iif(this.value =1,'I',;
    'N'))
  endfunc
  func oFLMONINS_1_11.GetRadio()
    this.Parent.oContained.w_FLMONINS = this.RadioValue()
    return .t.
  endfunc

  func oFLMONINS_1_11.SetRadio()
    this.Parent.oContained.w_FLMONINS=trim(this.Parent.oContained.w_FLMONINS)
    this.value = ;
      iif(this.Parent.oContained.w_FLMONINS=='I',1,;
      0)
  endfunc

  add object oFLMONMOD_1_12 as StdCheck with uid="KKEPELKXYN",rtseq=8,rtrep=.f.,left=518, top=35, caption="Modifica",;
    ToolTipText = "Se attivo: visualizza le operazioni di tipo modifica",;
    HelpContextID = 155839386,;
    cFormVar="w_FLMONMOD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLMONMOD_1_12.RadioValue()
    return(iif(this.value =1,'R',;
    'N'))
  endfunc
  func oFLMONMOD_1_12.GetRadio()
    this.Parent.oContained.w_FLMONMOD = this.RadioValue()
    return .t.
  endfunc

  func oFLMONMOD_1_12.SetRadio()
    this.Parent.oContained.w_FLMONMOD=trim(this.Parent.oContained.w_FLMONMOD)
    this.value = ;
      iif(this.Parent.oContained.w_FLMONMOD=='R',1,;
      0)
  endfunc

  add object oFLMONDEL_1_13 as StdCheck with uid="VFENKHIEET",rtseq=9,rtrep=.f.,left=518, top=19, caption="Cancellazione",;
    ToolTipText = "Se attivo: visualizza le operazioni di tipo cancellazione",;
    HelpContextID = 263591006,;
    cFormVar="w_FLMONDEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLMONDEL_1_13.RadioValue()
    return(iif(this.value =1,'D',;
    'N'))
  endfunc
  func oFLMONDEL_1_13.GetRadio()
    this.Parent.oContained.w_FLMONDEL = this.RadioValue()
    return .t.
  endfunc

  func oFLMONDEL_1_13.SetRadio()
    this.Parent.oContained.w_FLMONDEL=trim(this.Parent.oContained.w_FLMONDEL)
    this.value = ;
      iif(this.Parent.oContained.w_FLMONDEL=='D',1,;
      0)
  endfunc


  add object oBtn_1_14 as StdButton with uid="JLCPPDXJAV",left=628, top=9, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la ricerca";
    , HelpContextID = 257784835;
    , Caption='\<Interroga';
  , bGlobalFont=.t.

    proc oBtn_1_14.Click()
      with this.Parent.oContained
        .NotifyEvent("Interroga")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object zoom as cp_szoombox with uid="ITMPYFUXOP",left=8, top=59, width=668,height=279,;
    caption='zoom',;
   bGlobalFont=.t.,;
    cZoomFile="gscf_kpl",bOptions=.f.,bAdvOptions=.f.,cZoomOnZoom="",cTable="LOGCNTFL",bQueryOnLoad=.f.,bReadOnly=.t.,cMenuFile="",;
    cEvent = "Interroga",;
    nPag=1;
    , HelpContextID = 126497174


  add object oBtn_1_16 as StdButton with uid="WOOVXXVTWQ",left=7, top=342, width=48,height=45,;
    CpPicture="BMP\Check.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutti i campi";
    , HelpContextID = 45390374;
    , Caption='\<Sel. tutti';
  , bGlobalFont=.t.

    proc oBtn_1_16.Click()
      with this.Parent.oContained
        update (.w_zoom.cCursor) set xchk=1
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_17 as StdButton with uid="NWXMVNCBXX",left=59, top=342, width=48,height=45,;
    CpPicture="BMP\UnCheck.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutti i campi";
    , HelpContextID = 213065722;
    , Caption='\<Desel. tutti';
  , bGlobalFont=.t.

    proc oBtn_1_17.Click()
      with this.Parent.oContained
        update (.w_zoom.cCursor) set xchk=0
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="VMPFRPKBFV",left=575, top=342, width=48,height=45,;
    CpPicture="bmp\ok.ico", caption="", nPag=1;
    , ToolTipText = "Premere per eliminare i dati selezionati";
    , HelpContextID = 119198774;
    , Caption='\<Ok';
  , bGlobalFont=.t.

    proc oBtn_1_18.Click()
      with this.Parent.oContained
        do gscf_bpl with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_19 as StdButton with uid="MTXNONHMHL",left=628, top=342, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 126186054;
    , Caption='\<Esci';
  , bGlobalFont=.t.

    proc oBtn_1_19.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_2 as StdString with uid="HJKXPABXDQ",Visible=.t., Left=29, Top=9,;
    Alignment=1, Width=43, Height=18,;
    Caption="Regola:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="KXDFQRTNBS",Visible=.t., Left=28, Top=36,;
    Alignment=1, Width=44, Height=18,;
    Caption="Tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="GPKAZWYIGU",Visible=.t., Left=371, Top=9,;
    Alignment=1, Width=57, Height=18,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="LTEAREFOQT",Visible=.t., Left=371, Top=36,;
    Alignment=1, Width=57, Height=18,;
    Caption="A data:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscf_kpl','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
