* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_brt                                                        *
*              Rettifiche di servizi                                           *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-03-06                                                      *
* Last revis.: 2015-03-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_brt",oParentObject,m.pParame)
return(i_retval)

define class tgsar_brt as StdBatch
  * --- Local variables
  pParame = space(3)
  w_PADRE = .NULL.
  w_ZOOM = space(10)
  w_NUMREC = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine creata per la gestione delle rettifiche di servizi. La routine viene
    *     lanciata dalla maschera Gsar_Kit
    this.w_ZOOM = this.oParentObject.w_ZoomRett
    this.w_PADRE = This.oParentObject.oParentObject
    do case
      case this.pParame="AGG"
        this.w_PADRE.MarkPos()     
        SELECT * FROM (this.w_ZOOM.cCursor) INTO CURSOR RETTIFICHE Where Xchk=1
        SELECT RETTIFICHE
        Go Top
        Scan
        this.w_PADRE.AddRow()     
        this.w_Padre.w_Piprogse=Rettifiche.Piprogse
        this.w_Padre.w_Piperser=Rettifiche.Perser
        this.w_Padre.w_Pimodinc=Rettifiche.Modinc
        this.w_Padre.w_Pipaepag=Rettifiche.Pipaepag
        this.w_Padre.w_Piannreg=Rettifiche.Piannreg
        this.w_Padre.w_Piimpnaz=Rettifiche.Piimpnaz
        this.w_Padre.w_Piimpval=Rettifiche.Piimpval
        this.w_Padre.w_Pinomenc=Rettifiche.Pinomenc
        this.w_Padre.w_Pinumfat=Rettifiche.Pinumdoc
        this.w_Padre.w_Pialffat=Rettifiche.Pialfdoc
        this.w_Padre.w_Pidatfat=iif(Empty(Cp_Todate(Rettifiche.Pidatdoc)),Cp_CharToDate("  -  -    "),Cp_Todate(Rettifiche.Pidatdoc))
        this.w_PADRE.SaveRow()     
        Endscan
        if used("RETTIFICHE")
          select RETTIFICHE
          use
        endif
        this.w_PADRE.RePos()     
        * --- Chiudo la maschera di rettifica servizi
         this.oParentObject.ecpQuit()
      case this.pParame="SEL"
        if USED(this.w_Zoom.cCursor) And RECCOUNT(this.w_Zoom.cCursor)>0
          this.w_NUMREC = RECNO()
          SELECT ( this.w_ZOOM.cCursor )
          COUNT FOR XCHK=1 TO w_COUNT
          if w_COUNT>0
            this.oParentObject.w_TESTSEL = "S"
          else
            this.oParentObject.w_TESTSEL = "D"
          endif
          GO this.w_NUMREC
        endif
    endcase
  endproc


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
