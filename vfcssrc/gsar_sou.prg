* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_sou                                                        *
*              Stampa output utente                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_7]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-13                                                      *
* Last revis.: 2009-12-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_sou",oParentObject))

* --- Class definition
define class tgsar_sou as StdForm
  Top    = 41
  Left   = 152

  * --- Standard Properties
  Width  = 590
  Height = 126
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-12-01"
  HelpContextID=157906793
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=3

  * --- Constant Properties
  _IDX = 0
  OUT_PUTS_IDX = 0
  cPrg = "gsar_sou"
  cComment = "Stampa output utente"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PROGRA1 = space(30)
  o_PROGRA1 = space(30)
  w_PROGRA = space(8)
  w_DESPRG = space(35)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_souPag1","gsar_sou",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPROGRA1_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='OUT_PUTS'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PROGRA1=space(30)
      .w_PROGRA=space(8)
      .w_DESPRG=space(35)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_PROGRA1))
          .link_1_1('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .w_PROGRA = ALLTRIM(.w_PROGRA1)+'%'
    endwith
    this.DoRTCalc(3,3,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
        .DoRTCalc(1,1,.t.)
        if .o_PROGRA1<>.w_PROGRA1
            .w_PROGRA = ALLTRIM(.w_PROGRA1)+'%'
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(3,3,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_2.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_2.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PROGRA1
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.OUT_PUTS_IDX,3]
    i_lTable = "OUT_PUTS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2], .t., this.OUT_PUTS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PROGRA1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'OUT_PUTS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OUNOMPRG like "+cp_ToStrODBC(trim(this.w_PROGRA1)+"%");

          i_ret=cp_SQL(i_nConn,"select OUNOMPRG,OUDESPRG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OUNOMPRG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OUNOMPRG',trim(this.w_PROGRA1))
          select OUNOMPRG,OUDESPRG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OUNOMPRG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PROGRA1)==trim(_Link_.OUNOMPRG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PROGRA1) and !this.bDontReportError
            deferred_cp_zoom('OUT_PUTS','*','OUNOMPRG',cp_AbsName(oSource.parent,'oPROGRA1_1_1'),i_cWhere,'',"Output utente",'GSAR_SOU.OUT_PUTS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OUNOMPRG,OUDESPRG";
                     +" from "+i_cTable+" "+i_lTable+" where OUNOMPRG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OUNOMPRG',oSource.xKey(1))
            select OUNOMPRG,OUDESPRG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PROGRA1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OUNOMPRG,OUDESPRG";
                   +" from "+i_cTable+" "+i_lTable+" where OUNOMPRG="+cp_ToStrODBC(this.w_PROGRA1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OUNOMPRG',this.w_PROGRA1)
            select OUNOMPRG,OUDESPRG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PROGRA1 = NVL(_Link_.OUNOMPRG,space(30))
      this.w_DESPRG = NVL(_Link_.OUDESPRG,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_PROGRA1 = space(30)
      endif
      this.w_DESPRG = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.OUT_PUTS_IDX,2])+'\'+cp_ToStr(_Link_.OUNOMPRG,1)
      cp_ShowWarn(i_cKey,this.OUT_PUTS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PROGRA1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPROGRA1_1_1.value==this.w_PROGRA1)
      this.oPgFrm.Page1.oPag.oPROGRA1_1_1.value=this.w_PROGRA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRG_1_8.value==this.w_DESPRG)
      this.oPgFrm.Page1.oPag.oDESPRG_1_8.value=this.w_DESPRG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PROGRA1 = this.w_PROGRA1
    return

enddefine

* --- Define pages as container
define class tgsar_souPag1 as StdContainer
  Width  = 586
  height = 126
  stdWidth  = 586
  stdheight = 126
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPROGRA1_1_1 as StdField with uid="KELDYNLCWY",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PROGRA1", cQueryName = "PROGRA1",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Programma selezionato (spazio = no selezione)",;
    HelpContextID = 218288118,;
   bGlobalFont=.t.,;
    Height=21, Width=220, Left=97, Top=13, cSayPict='REPL("!",30)', cGetPict='REPL("!",30)', InputMask=replicate('X',30), bHasZoom = .t. , cLinkFile="OUT_PUTS", oKey_1_1="OUNOMPRG", oKey_1_2="this.w_PROGRA1"

  func oPROGRA1_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oPROGRA1_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPROGRA1_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'OUT_PUTS','*','OUNOMPRG',cp_AbsName(this.parent,'oPROGRA1_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Output utente",'GSAR_SOU.OUT_PUTS_VZM',this.parent.oContained
  endproc


  add object oObj_1_2 as cp_outputCombo with uid="ZLERXIVPWT",left=97, top=48, width=479,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 20090854


  add object oBtn_1_3 as StdButton with uid="DSDSIVDZNX",left=476, top=76, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 252318246;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_3.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_4 as StdButton with uid="LZTTYHSNFS",left=529, top=76, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 150589370;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESPRG_1_8 as StdField with uid="UXGQLAADZM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DESPRG", cQueryName = "DESPRG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 51118646,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=322, Top=13, InputMask=replicate('X',35)

  add object oStr_1_5 as StdString with uid="MIJYIWJLXE",Visible=.t., Left=5, Top=48,;
    Alignment=1, Width=90, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="ZOWGRJGMUZ",Visible=.t., Left=5, Top=13,;
    Alignment=1, Width=90, Height=15,;
    Caption="Programma:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_sou','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
