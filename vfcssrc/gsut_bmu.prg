* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bmu                                                        *
*              Controlli sul dettaglio mail                                    *
*                                                                              *
*      Author: Zucchetti s.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-04-30                                                      *
* Last revis.: 2015-01-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pMOD
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bmu",oParentObject,m.pMOD)
return(i_retval)

define class tgsut_bmu as StdBatch
  * --- Local variables
  pMOD = space(1)
  w_GSUT_MMU = .NULL.
  w_RowID = 0
  w_GSUT_AAM = .NULL.
  w_AMPASSWD = space(254)
  w_PASSWD = space(254)
  w_PASSWDOLD = space(254)
  * --- WorkFile variables
  ACC_MAIL_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli sul dettaglio mail (GSUT_MMU)
    *     
    *     pMOD: 'C' check: controlla che non ci siano 2 check contemporaneamente in fase di modifica
    *                 'S' save:   controlli finali sul dettaglio mail, restituisce l'esito nell'apposita variabile caller
    *                 'N' new:   nuovo account mail
    this.w_GSUT_MMU = this.oParentObject
    do case
      case this.pMOD="C"
      case this.pMOD="S"
        this.oParentObject.w_RETURN = .T.
        if this.w_GSUT_MMU.NumRow() <> 0
          this.w_GSUT_MMU.Exec_Select("rowdupl", "t_UMCODAZI,t_UMFLGPEC,count(*) as nrows", "not empty(t_UMSERIAL)","","t_UMCODAZI,t_UMFLGPEC","count(*)>1")     
          if USED("rowdupl")
            Select rowdupl
            if RecCount()>0
              ah_errormsg("Errore: per l'azienda %2 esiste pi� di una configurazione della %1", "!","", iif(t_UMFLGPEC=2,"PEC", "Mail standard"), evl(t_UMCODAZI,"<default>"))
              this.oParentObject.w_RETURN = .F.
            endif
            use
          endif
        endif
      case this.pMOD="N"
        * --- nuovo account mail (caricamento rapido)
        this.w_GSUT_AAM = GSUT_AAM()
        if this.w_GSUT_AAM.bSec1
          this.w_GSUT_AAM.ecpLoad()     
          if this.w_GSUT_AAM.bSec4
            this.w_GSUT_AAM.ecpLoad()     
            this.w_GSUT_AAM.w_GSUT_MMU = this.w_GSUT_MMU
            this.w_GSUT_AAM.w_AM_INVIO = IIF(this.oParentObject.w_UMFLGPEC="S","P","S")
            this.w_GSUT_AAM.w_AMFLGPEC = this.oParentObject.w_UMFLGPEC
            this.w_GSUT_AAM.w_UTCODICE = this.oParentObject.oParentObject.w_UTCODICE
            this.w_GSUT_AAM.mCalc(.T.)     
            this.w_GSUT_AAM.SaveDependsOn()     
            this.w_GSUT_AAM.mEnableControls()     
          else
            this.w_GSUT_AAM.ecpQuit()     
          endif
        endif
      case this.pMOD="P"
        * --- Modifica pwd Account Mail
        if not empty(this.oParentObject.w_UMSERIAL)
          * --- Read from ACC_MAIL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ACC_MAIL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ACC_MAIL_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "AMPASSWD"+;
              " from "+i_cTable+" ACC_MAIL where ";
                  +"AMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_UMSERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              AMPASSWD;
              from (i_cTable) where;
                  AMSERIAL = this.oParentObject.w_UMSERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_AMPASSWD = NVL(cp_ToDate(_read_.AMPASSWD),cp_NullValue(_read_.AMPASSWD))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_PASSWD = CIFRACNF(this.w_AMPASSWD, "D")
          this.w_PASSWDOLD = this.w_AMPASSWD
          do gsut_kmp with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_AMPASSWD<>this.w_PASSWDOLD
            * --- Write into ACC_MAIL
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ACC_MAIL_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ACC_MAIL_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ACC_MAIL_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"AMPASSWD ="+cp_NullLink(cp_ToStrODBC(this.w_AMPASSWD),'ACC_MAIL','AMPASSWD');
                  +i_ccchkf ;
              +" where ";
                  +"AMSERIAL = "+cp_ToStrODBC(this.oParentObject.w_UMSERIAL);
                     )
            else
              update (i_cTable) set;
                  AMPASSWD = this.w_AMPASSWD;
                  &i_ccchkf. ;
               where;
                  AMSERIAL = this.oParentObject.w_UMSERIAL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
            ah_errormsg("Password modificata con successo.")
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pMOD)
    this.pMOD=pMOD
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ACC_MAIL'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pMOD"
endproc
