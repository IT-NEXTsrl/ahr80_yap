* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca_bvk                                                        *
*              Controlli in cancellazione voci di costo e ricavo               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-06-21                                                      *
* Last revis.: 2005-06-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca_bvk",oParentObject)
return(i_retval)

define class tgsca_bvk as StdBatch
  * --- Local variables
  w_OK = .f.
  * --- WorkFile variables
  VOC_COST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSCA_BVC evento Delete Init
    this.w_OK = .F.
    * --- Select from VOC_COST
    i_nConn=i_TableProp[this.VOC_COST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2],.t.,this.VOC_COST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select VCVOCOMA  from "+i_cTable+" VOC_COST ";
          +" where VCVOCOMA="+cp_ToStrODBC(this.oParentObject.w_VCCODICE)+"";
           ,"_Curs_VOC_COST")
    else
      select VCVOCOMA from (i_cTable);
       where VCVOCOMA=this.oParentObject.w_VCCODICE;
        into cursor _Curs_VOC_COST
    endif
    if used('_Curs_VOC_COST')
      select _Curs_VOC_COST
      locate for 1=1
      do while not(eof())
      this.w_OK = .T.
      exit
        select _Curs_VOC_COST
        continue
      enddo
      use
    endif
    if this.w_OK
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=Ah_Msgformat("Attenzione voce utilizzata come voce di costo\ricavo per omaggi, impossibile eliminare")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='VOC_COST'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_VOC_COST')
      use in _Curs_VOC_COST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
