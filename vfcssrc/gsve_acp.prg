* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_acp                                                        *
*              Contropartite pagamenti                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_20]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-18                                                      *
* Last revis.: 2010-01-19                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsve_acp")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsve_acp")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsve_acp")
  return

* --- Class definition
define class tgsve_acp as StdPCForm
  Width  = 687
  Height = 267
  Top    = 10
  Left   = 10
  cComment = "Contropartite pagamenti"
  cPrg = "gsve_acp"
  HelpContextID=158715287
  add object cnt as tcgsve_acp
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsve_acp as PCContext
  w_CPSERIAL = space(10)
  w_TIPCON = space(1)
  w_CPIMPPRE = 0
  w_CPCONPRE = space(15)
  w_CPIMPCON = 0
  w_CPCONCON = space(15)
  w_CPIMPASS = 0
  w_CPCONASS = space(15)
  w_CPIMPCAR = 0
  w_CPCONCAR = space(15)
  w_CPIMPFIN = 0
  w_CPCONFIN = space(15)
  w_DESCON = space(40)
  w_DESPRE = space(40)
  w_DESASS = space(40)
  w_DESCAR = space(40)
  w_DESFIN = space(40)
  w_DTOPRE = space(8)
  w_DTOCON = space(8)
  w_DTOASS = space(8)
  w_DTOCAR = space(8)
  w_DTOFIN = space(8)
  w_TOTACC = 0
  w_TOTRES = 0
  proc Save(oFrom)
    this.w_CPSERIAL = oFrom.w_CPSERIAL
    this.w_TIPCON = oFrom.w_TIPCON
    this.w_CPIMPPRE = oFrom.w_CPIMPPRE
    this.w_CPCONPRE = oFrom.w_CPCONPRE
    this.w_CPIMPCON = oFrom.w_CPIMPCON
    this.w_CPCONCON = oFrom.w_CPCONCON
    this.w_CPIMPASS = oFrom.w_CPIMPASS
    this.w_CPCONASS = oFrom.w_CPCONASS
    this.w_CPIMPCAR = oFrom.w_CPIMPCAR
    this.w_CPCONCAR = oFrom.w_CPCONCAR
    this.w_CPIMPFIN = oFrom.w_CPIMPFIN
    this.w_CPCONFIN = oFrom.w_CPCONFIN
    this.w_DESCON = oFrom.w_DESCON
    this.w_DESPRE = oFrom.w_DESPRE
    this.w_DESASS = oFrom.w_DESASS
    this.w_DESCAR = oFrom.w_DESCAR
    this.w_DESFIN = oFrom.w_DESFIN
    this.w_DTOPRE = oFrom.w_DTOPRE
    this.w_DTOCON = oFrom.w_DTOCON
    this.w_DTOASS = oFrom.w_DTOASS
    this.w_DTOCAR = oFrom.w_DTOCAR
    this.w_DTOFIN = oFrom.w_DTOFIN
    this.w_TOTACC = oFrom.w_TOTACC
    this.w_TOTRES = oFrom.w_TOTRES
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_CPSERIAL = this.w_CPSERIAL
    oTo.w_TIPCON = this.w_TIPCON
    oTo.w_CPIMPPRE = this.w_CPIMPPRE
    oTo.w_CPCONPRE = this.w_CPCONPRE
    oTo.w_CPIMPCON = this.w_CPIMPCON
    oTo.w_CPCONCON = this.w_CPCONCON
    oTo.w_CPIMPASS = this.w_CPIMPASS
    oTo.w_CPCONASS = this.w_CPCONASS
    oTo.w_CPIMPCAR = this.w_CPIMPCAR
    oTo.w_CPCONCAR = this.w_CPCONCAR
    oTo.w_CPIMPFIN = this.w_CPIMPFIN
    oTo.w_CPCONFIN = this.w_CPCONFIN
    oTo.w_DESCON = this.w_DESCON
    oTo.w_DESPRE = this.w_DESPRE
    oTo.w_DESASS = this.w_DESASS
    oTo.w_DESCAR = this.w_DESCAR
    oTo.w_DESFIN = this.w_DESFIN
    oTo.w_DTOPRE = this.w_DTOPRE
    oTo.w_DTOCON = this.w_DTOCON
    oTo.w_DTOASS = this.w_DTOASS
    oTo.w_DTOCAR = this.w_DTOCAR
    oTo.w_DTOFIN = this.w_DTOFIN
    oTo.w_TOTACC = this.w_TOTACC
    oTo.w_TOTRES = this.w_TOTRES
    PCContext::Load(oTo)
enddefine

define class tcgsve_acp as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 687
  Height = 267
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-01-19"
  HelpContextID=158715287
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=24

  * --- Constant Properties
  CON_PAGA_IDX = 0
  CONTI_IDX = 0
  cFile = "CON_PAGA"
  cKeySelect = "CPSERIAL"
  cKeyWhere  = "CPSERIAL=this.w_CPSERIAL"
  cKeyWhereODBC = '"CPSERIAL="+cp_ToStrODBC(this.w_CPSERIAL)';

  cKeyWhereODBCqualified = '"CON_PAGA.CPSERIAL="+cp_ToStrODBC(this.w_CPSERIAL)';

  cPrg = "gsve_acp"
  cComment = "Contropartite pagamenti"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CPSERIAL = space(10)
  w_TIPCON = space(1)
  w_CPIMPPRE = 0
  o_CPIMPPRE = 0
  w_CPCONPRE = space(15)
  w_CPIMPCON = 0
  o_CPIMPCON = 0
  w_CPCONCON = space(15)
  w_CPIMPASS = 0
  o_CPIMPASS = 0
  w_CPCONASS = space(15)
  w_CPIMPCAR = 0
  o_CPIMPCAR = 0
  w_CPCONCAR = space(15)
  w_CPIMPFIN = 0
  o_CPIMPFIN = 0
  w_CPCONFIN = space(15)
  w_DESCON = space(40)
  w_DESPRE = space(40)
  w_DESASS = space(40)
  w_DESCAR = space(40)
  w_DESFIN = space(40)
  w_DTOPRE = ctod('  /  /  ')
  w_DTOCON = ctod('  /  /  ')
  w_DTOASS = ctod('  /  /  ')
  w_DTOCAR = ctod('  /  /  ')
  w_DTOFIN = ctod('  /  /  ')
  w_TOTACC = 0
  w_TOTRES = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_acpPag1","gsve_acp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 217162742
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCPIMPPRE_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='CON_PAGA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CON_PAGA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CON_PAGA_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsve_acp'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CON_PAGA where CPSERIAL=KeySet.CPSERIAL
    *
    i_nConn = i_TableProp[this.CON_PAGA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_PAGA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CON_PAGA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CON_PAGA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CON_PAGA '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CPSERIAL',this.w_CPSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESCON = space(40)
        .w_DESPRE = space(40)
        .w_DESASS = space(40)
        .w_DESCAR = space(40)
        .w_DESFIN = space(40)
        .w_DTOPRE = ctod("  /  /  ")
        .w_DTOCON = ctod("  /  /  ")
        .w_DTOASS = ctod("  /  /  ")
        .w_DTOCAR = ctod("  /  /  ")
        .w_DTOFIN = ctod("  /  /  ")
        .w_CPSERIAL = NVL(CPSERIAL,space(10))
        .w_TIPCON = 'G'
        .w_CPIMPPRE = NVL(CPIMPPRE,0)
        .w_CPCONPRE = NVL(CPCONPRE,space(15))
          .link_1_5('Load')
        .w_CPIMPCON = NVL(CPIMPCON,0)
        .w_CPCONCON = NVL(CPCONCON,space(15))
          .link_1_8('Load')
        .w_CPIMPASS = NVL(CPIMPASS,0)
        .w_CPCONASS = NVL(CPCONASS,space(15))
          .link_1_11('Load')
        .w_CPIMPCAR = NVL(CPIMPCAR,0)
        .w_CPCONCAR = NVL(CPCONCAR,space(15))
          .link_1_14('Load')
        .w_CPIMPFIN = NVL(CPIMPFIN,0)
        .w_CPCONFIN = NVL(CPCONFIN,space(15))
          .link_1_17('Load')
        .w_TOTACC = this.oParentObject .w_MVACCONT
        .w_TOTRES = .w_TOTACC - (.w_CPIMPPRE + .w_CPIMPCON + .w_CPIMPASS+ .w_CPIMPFIN + .w_CPIMPCAR)
        cp_LoadRecExtFlds(this,'CON_PAGA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_CPSERIAL = space(10)
      .w_TIPCON = space(1)
      .w_CPIMPPRE = 0
      .w_CPCONPRE = space(15)
      .w_CPIMPCON = 0
      .w_CPCONCON = space(15)
      .w_CPIMPASS = 0
      .w_CPCONASS = space(15)
      .w_CPIMPCAR = 0
      .w_CPCONCAR = space(15)
      .w_CPIMPFIN = 0
      .w_CPCONFIN = space(15)
      .w_DESCON = space(40)
      .w_DESPRE = space(40)
      .w_DESASS = space(40)
      .w_DESCAR = space(40)
      .w_DESFIN = space(40)
      .w_DTOPRE = ctod("  /  /  ")
      .w_DTOCON = ctod("  /  /  ")
      .w_DTOASS = ctod("  /  /  ")
      .w_DTOCAR = ctod("  /  /  ")
      .w_DTOFIN = ctod("  /  /  ")
      .w_TOTACC = 0
      .w_TOTRES = 0
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_TIPCON = 'G'
          .DoRTCalc(3,3,.f.)
        .w_CPCONPRE = IIF(.w_CPIMPPRE=0,Space(15), .w_CPCONPRE)
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_CPCONPRE))
          .link_1_5('Full')
          endif
          .DoRTCalc(5,5,.f.)
        .w_CPCONCON = IIF(.w_CPIMPCON=0,Space(15), .w_CPCONCON)
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_CPCONCON))
          .link_1_8('Full')
          endif
          .DoRTCalc(7,7,.f.)
        .w_CPCONASS = IIF(.w_CPIMPASS=0,Space(15), .w_CPCONASS)
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_CPCONASS))
          .link_1_11('Full')
          endif
          .DoRTCalc(9,9,.f.)
        .w_CPCONCAR = IIF(.w_CPIMPCAR=0,Space(15), .w_CPCONCAR)
        .DoRTCalc(10,10,.f.)
          if not(empty(.w_CPCONCAR))
          .link_1_14('Full')
          endif
          .DoRTCalc(11,11,.f.)
        .w_CPCONFIN = IIF(.w_CPIMPFIN=0,Space(15), .w_CPCONFIN)
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_CPCONFIN))
          .link_1_17('Full')
          endif
          .DoRTCalc(13,22,.f.)
        .w_TOTACC = this.oParentObject .w_MVACCONT
        .w_TOTRES = .w_TOTACC - (.w_CPIMPPRE + .w_CPIMPCON + .w_CPIMPASS+ .w_CPIMPFIN + .w_CPIMPCAR)
      endif
    endwith
    cp_BlankRecExtFlds(this,'CON_PAGA')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCPIMPPRE_1_4.enabled = i_bVal
      .Page1.oPag.oCPCONPRE_1_5.enabled = i_bVal
      .Page1.oPag.oCPIMPCON_1_7.enabled = i_bVal
      .Page1.oPag.oCPCONCON_1_8.enabled = i_bVal
      .Page1.oPag.oCPIMPASS_1_10.enabled = i_bVal
      .Page1.oPag.oCPCONASS_1_11.enabled = i_bVal
      .Page1.oPag.oCPIMPCAR_1_13.enabled = i_bVal
      .Page1.oPag.oCPCONCAR_1_14.enabled = i_bVal
      .Page1.oPag.oCPIMPFIN_1_16.enabled = i_bVal
      .Page1.oPag.oCPCONFIN_1_17.enabled = i_bVal
      .Page1.oPag.oBtn_1_37.enabled = i_bVal
      .Page1.oPag.oBtn_1_38.enabled = .Page1.oPag.oBtn_1_38.mCond()
    endwith
    cp_SetEnabledExtFlds(this,'CON_PAGA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CON_PAGA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPSERIAL,"CPSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPIMPPRE,"CPIMPPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPCONPRE,"CPCONPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPIMPCON,"CPIMPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPCONCON,"CPCONCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPIMPASS,"CPIMPASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPCONASS,"CPCONASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPIMPCAR,"CPIMPCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPCONCAR,"CPCONCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPIMPFIN,"CPIMPFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPCONFIN,"CPCONFIN",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CON_PAGA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_PAGA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CON_PAGA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CON_PAGA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CON_PAGA')
        i_extval=cp_InsertValODBCExtFlds(this,'CON_PAGA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CPSERIAL,CPIMPPRE,CPCONPRE,CPIMPCON,CPCONCON"+;
                  ",CPIMPASS,CPCONASS,CPIMPCAR,CPCONCAR,CPIMPFIN"+;
                  ",CPCONFIN "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_CPSERIAL)+;
                  ","+cp_ToStrODBC(this.w_CPIMPPRE)+;
                  ","+cp_ToStrODBCNull(this.w_CPCONPRE)+;
                  ","+cp_ToStrODBC(this.w_CPIMPCON)+;
                  ","+cp_ToStrODBCNull(this.w_CPCONCON)+;
                  ","+cp_ToStrODBC(this.w_CPIMPASS)+;
                  ","+cp_ToStrODBCNull(this.w_CPCONASS)+;
                  ","+cp_ToStrODBC(this.w_CPIMPCAR)+;
                  ","+cp_ToStrODBCNull(this.w_CPCONCAR)+;
                  ","+cp_ToStrODBC(this.w_CPIMPFIN)+;
                  ","+cp_ToStrODBCNull(this.w_CPCONFIN)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CON_PAGA')
        i_extval=cp_InsertValVFPExtFlds(this,'CON_PAGA')
        cp_CheckDeletedKey(i_cTable,0,'CPSERIAL',this.w_CPSERIAL)
        INSERT INTO (i_cTable);
              (CPSERIAL,CPIMPPRE,CPCONPRE,CPIMPCON,CPCONCON,CPIMPASS,CPCONASS,CPIMPCAR,CPCONCAR,CPIMPFIN,CPCONFIN  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CPSERIAL;
                  ,this.w_CPIMPPRE;
                  ,this.w_CPCONPRE;
                  ,this.w_CPIMPCON;
                  ,this.w_CPCONCON;
                  ,this.w_CPIMPASS;
                  ,this.w_CPCONASS;
                  ,this.w_CPIMPCAR;
                  ,this.w_CPCONCAR;
                  ,this.w_CPIMPFIN;
                  ,this.w_CPCONFIN;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CON_PAGA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_PAGA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CON_PAGA_IDX,i_nConn)
      *
      * update CON_PAGA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CON_PAGA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CPIMPPRE="+cp_ToStrODBC(this.w_CPIMPPRE)+;
             ",CPCONPRE="+cp_ToStrODBCNull(this.w_CPCONPRE)+;
             ",CPIMPCON="+cp_ToStrODBC(this.w_CPIMPCON)+;
             ",CPCONCON="+cp_ToStrODBCNull(this.w_CPCONCON)+;
             ",CPIMPASS="+cp_ToStrODBC(this.w_CPIMPASS)+;
             ",CPCONASS="+cp_ToStrODBCNull(this.w_CPCONASS)+;
             ",CPIMPCAR="+cp_ToStrODBC(this.w_CPIMPCAR)+;
             ",CPCONCAR="+cp_ToStrODBCNull(this.w_CPCONCAR)+;
             ",CPIMPFIN="+cp_ToStrODBC(this.w_CPIMPFIN)+;
             ",CPCONFIN="+cp_ToStrODBCNull(this.w_CPCONFIN)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CON_PAGA')
        i_cWhere = cp_PKFox(i_cTable  ,'CPSERIAL',this.w_CPSERIAL  )
        UPDATE (i_cTable) SET;
              CPIMPPRE=this.w_CPIMPPRE;
             ,CPCONPRE=this.w_CPCONPRE;
             ,CPIMPCON=this.w_CPIMPCON;
             ,CPCONCON=this.w_CPCONCON;
             ,CPIMPASS=this.w_CPIMPASS;
             ,CPCONASS=this.w_CPCONASS;
             ,CPIMPCAR=this.w_CPIMPCAR;
             ,CPCONCAR=this.w_CPCONCAR;
             ,CPIMPFIN=this.w_CPIMPFIN;
             ,CPCONFIN=this.w_CPCONFIN;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CON_PAGA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CON_PAGA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CON_PAGA_IDX,i_nConn)
      *
      * delete CON_PAGA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CPSERIAL',this.w_CPSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CON_PAGA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CON_PAGA_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_TIPCON = 'G'
        .DoRTCalc(3,3,.t.)
        if .o_CPIMPPRE<>.w_CPIMPPRE
            .w_CPCONPRE = IIF(.w_CPIMPPRE=0,Space(15), .w_CPCONPRE)
          .link_1_5('Full')
        endif
        .DoRTCalc(5,5,.t.)
        if .o_CPIMPCON<>.w_CPIMPCON
            .w_CPCONCON = IIF(.w_CPIMPCON=0,Space(15), .w_CPCONCON)
          .link_1_8('Full')
        endif
        .DoRTCalc(7,7,.t.)
        if .o_CPIMPASS<>.w_CPIMPASS
            .w_CPCONASS = IIF(.w_CPIMPASS=0,Space(15), .w_CPCONASS)
          .link_1_11('Full')
        endif
        .DoRTCalc(9,9,.t.)
        if .o_CPIMPCAR<>.w_CPIMPCAR
            .w_CPCONCAR = IIF(.w_CPIMPCAR=0,Space(15), .w_CPCONCAR)
          .link_1_14('Full')
        endif
        .DoRTCalc(11,11,.t.)
        if .o_CPIMPFIN<>.w_CPIMPFIN
            .w_CPCONFIN = IIF(.w_CPIMPFIN=0,Space(15), .w_CPCONFIN)
          .link_1_17('Full')
        endif
        .DoRTCalc(13,22,.t.)
            .w_TOTACC = this.oParentObject .w_MVACCONT
            .w_TOTRES = .w_TOTACC - (.w_CPIMPPRE + .w_CPIMPCON + .w_CPIMPASS+ .w_CPIMPFIN + .w_CPIMPCAR)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCPCONPRE_1_5.enabled = this.oPgFrm.Page1.oPag.oCPCONPRE_1_5.mCond()
    this.oPgFrm.Page1.oPag.oCPCONCON_1_8.enabled = this.oPgFrm.Page1.oPag.oCPCONCON_1_8.mCond()
    this.oPgFrm.Page1.oPag.oCPCONASS_1_11.enabled = this.oPgFrm.Page1.oPag.oCPCONASS_1_11.mCond()
    this.oPgFrm.Page1.oPag.oCPCONCAR_1_14.enabled = this.oPgFrm.Page1.oPag.oCPCONCAR_1_14.mCond()
    this.oPgFrm.Page1.oPag.oCPCONFIN_1_17.enabled = this.oPgFrm.Page1.oPag.oCPCONFIN_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_3.visible=!this.oPgFrm.Page1.oPag.oStr_1_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_9.visible=!this.oPgFrm.Page1.oPag.oStr_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CPCONPRE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CPCONPRE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CPCONPRE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CPCONPRE))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CPCONPRE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CPCONPRE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CPCONPRE)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CPCONPRE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCPCONPRE_1_5'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CPCONPRE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CPCONPRE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CPCONPRE)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CPCONPRE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESPRE = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOPRE = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CPCONPRE = space(15)
      endif
      this.w_DESPRE = space(40)
      this.w_DTOPRE = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=i_DATSYS < .w_DTOPRE Or Empty(.w_DTOPRE)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        endif
        this.w_CPCONPRE = space(15)
        this.w_DESPRE = space(40)
        this.w_DTOPRE = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CPCONPRE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CPCONCON
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CPCONCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CPCONCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CPCONCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CPCONCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CPCONCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CPCONCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CPCONCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCPCONCON_1_8'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CPCONCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CPCONCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CPCONCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CPCONCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOCON = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CPCONCON = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DTOCON = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=i_DATSYS < .w_DTOCON Or Empty(.w_DTOCON)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        endif
        this.w_CPCONCON = space(15)
        this.w_DESCON = space(40)
        this.w_DTOCON = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CPCONCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CPCONASS
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CPCONASS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CPCONASS)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CPCONASS))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CPCONASS)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CPCONASS)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CPCONASS)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CPCONASS) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCPCONASS_1_11'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CPCONASS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CPCONASS);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CPCONASS)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CPCONASS = NVL(_Link_.ANCODICE,space(15))
      this.w_DESASS = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOASS = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CPCONASS = space(15)
      endif
      this.w_DESASS = space(40)
      this.w_DTOASS = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=i_DATSYS < .w_DTOASS Or Empty(.w_DTOASS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        endif
        this.w_CPCONASS = space(15)
        this.w_DESASS = space(40)
        this.w_DTOASS = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CPCONASS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CPCONCAR
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CPCONCAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CPCONCAR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CPCONCAR))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CPCONCAR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CPCONCAR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CPCONCAR)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CPCONCAR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCPCONCAR_1_14'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CPCONCAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CPCONCAR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CPCONCAR)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CPCONCAR = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCAR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOCAR = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CPCONCAR = space(15)
      endif
      this.w_DESCAR = space(40)
      this.w_DTOCAR = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=i_DATSYS < .w_DTOCAR Or Empty(.w_DTOCAR)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        endif
        this.w_CPCONCAR = space(15)
        this.w_DESCAR = space(40)
        this.w_DTOCAR = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CPCONCAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CPCONFIN
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CPCONFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CPCONFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CPCONFIN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CPCONFIN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CPCONFIN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CPCONFIN)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CPCONFIN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCPCONFIN_1_17'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CPCONFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CPCONFIN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CPCONFIN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CPCONFIN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFIN = NVL(_Link_.ANDESCRI,space(40))
      this.w_DTOFIN = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CPCONFIN = space(15)
      endif
      this.w_DESFIN = space(40)
      this.w_DTOFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=i_DATSYS < .w_DTOFIN Or Empty(.w_DTOFIN)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente o obsoleto")
        endif
        this.w_CPCONFIN = space(15)
        this.w_DESFIN = space(40)
        this.w_DTOFIN = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CPCONFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCPIMPPRE_1_4.value==this.w_CPIMPPRE)
      this.oPgFrm.Page1.oPag.oCPIMPPRE_1_4.value=this.w_CPIMPPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oCPCONPRE_1_5.value==this.w_CPCONPRE)
      this.oPgFrm.Page1.oPag.oCPCONPRE_1_5.value=this.w_CPCONPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oCPIMPCON_1_7.value==this.w_CPIMPCON)
      this.oPgFrm.Page1.oPag.oCPIMPCON_1_7.value=this.w_CPIMPCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCPCONCON_1_8.value==this.w_CPCONCON)
      this.oPgFrm.Page1.oPag.oCPCONCON_1_8.value=this.w_CPCONCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCPIMPASS_1_10.value==this.w_CPIMPASS)
      this.oPgFrm.Page1.oPag.oCPIMPASS_1_10.value=this.w_CPIMPASS
    endif
    if not(this.oPgFrm.Page1.oPag.oCPCONASS_1_11.value==this.w_CPCONASS)
      this.oPgFrm.Page1.oPag.oCPCONASS_1_11.value=this.w_CPCONASS
    endif
    if not(this.oPgFrm.Page1.oPag.oCPIMPCAR_1_13.value==this.w_CPIMPCAR)
      this.oPgFrm.Page1.oPag.oCPIMPCAR_1_13.value=this.w_CPIMPCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oCPCONCAR_1_14.value==this.w_CPCONCAR)
      this.oPgFrm.Page1.oPag.oCPCONCAR_1_14.value=this.w_CPCONCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oCPIMPFIN_1_16.value==this.w_CPIMPFIN)
      this.oPgFrm.Page1.oPag.oCPIMPFIN_1_16.value=this.w_CPIMPFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCPCONFIN_1_17.value==this.w_CPCONFIN)
      this.oPgFrm.Page1.oPag.oCPCONFIN_1_17.value=this.w_CPCONFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_18.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_18.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRE_1_19.value==this.w_DESPRE)
      this.oPgFrm.Page1.oPag.oDESPRE_1_19.value=this.w_DESPRE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESASS_1_20.value==this.w_DESASS)
      this.oPgFrm.Page1.oPag.oDESASS_1_20.value=this.w_DESASS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAR_1_21.value==this.w_DESCAR)
      this.oPgFrm.Page1.oPag.oDESCAR_1_21.value=this.w_DESCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFIN_1_22.value==this.w_DESFIN)
      this.oPgFrm.Page1.oPag.oDESFIN_1_22.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTACC_1_35.value==this.w_TOTACC)
      this.oPgFrm.Page1.oPag.oTOTACC_1_35.value=this.w_TOTACC
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTRES_1_39.value==this.w_TOTRES)
      this.oPgFrm.Page1.oPag.oTOTRES_1_39.value=this.w_TOTRES
    endif
    cp_SetControlsValueExtFlds(this,'CON_PAGA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CPCONPRE)) or not(i_DATSYS < .w_DTOPRE Or Empty(.w_DTOPRE)))  and (.w_CPIMPPRE<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCPCONPRE_1_5.SetFocus()
            i_bnoObbl = !empty(.w_CPCONPRE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente o obsoleto")
          case   ((empty(.w_CPCONCON)) or not(i_DATSYS < .w_DTOCON Or Empty(.w_DTOCON)))  and (.w_CPIMPCON<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCPCONCON_1_8.SetFocus()
            i_bnoObbl = !empty(.w_CPCONCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente o obsoleto")
          case   ((empty(.w_CPCONASS)) or not(i_DATSYS < .w_DTOASS Or Empty(.w_DTOASS)))  and (.w_CPIMPASS<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCPCONASS_1_11.SetFocus()
            i_bnoObbl = !empty(.w_CPCONASS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente o obsoleto")
          case   ((empty(.w_CPCONCAR)) or not(i_DATSYS < .w_DTOCAR Or Empty(.w_DTOCAR)))  and (.w_CPIMPCAR<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCPCONCAR_1_14.SetFocus()
            i_bnoObbl = !empty(.w_CPCONCAR)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente o obsoleto")
          case   ((empty(.w_CPCONFIN)) or not(i_DATSYS < .w_DTOFIN Or Empty(.w_DTOFIN)))  and (.w_CPIMPFIN<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCPCONFIN_1_17.SetFocus()
            i_bnoObbl = !empty(.w_CPCONFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente o obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsve_acp
      * --- Controllo che il totale dei pagamenti sia uguale al totale acconti
      With This
      If .w_CPIMPPRE + .w_CPIMPCON + .w_CPIMPASS + .w_CPIMPCAR + .w_CPIMPFIN <> .w_TOTACC
         If .w_CPIMPPRE + .w_CPIMPCON + .w_CPIMPASS + .w_CPIMPCAR + .w_CPIMPFIN <> 0
           i_bRes = .f.
           i_bnoChk = .f.
      	   i_cErrorMsg = Ah_MsgFormat("Totale pagamenti diverso dal totale acconti")
         Endif
      Endif
      EndWith
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CPIMPPRE = this.w_CPIMPPRE
    this.o_CPIMPCON = this.w_CPIMPCON
    this.o_CPIMPASS = this.w_CPIMPASS
    this.o_CPIMPCAR = this.w_CPIMPCAR
    this.o_CPIMPFIN = this.w_CPIMPFIN
    return

enddefine

* --- Define pages as container
define class tgsve_acpPag1 as StdContainer
  Width  = 683
  height = 267
  stdWidth  = 683
  stdheight = 267
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCPIMPPRE_1_4 as StdField with uid="AASDWPLSVA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CPIMPPRE", cQueryName = "CPIMPPRE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 20467093,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=106, Top=68, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  add object oCPCONPRE_1_5 as StdField with uid="RITVCHXJEU",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CPCONPRE", cQueryName = "CPCONPRE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente o obsoleto",;
    ToolTipText = "Verr� utilizzata in contabilizzazione per discriminare questo tipo di pagamento",;
    HelpContextID = 22457749,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=250, Top=68, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CPCONPRE"

  func oCPCONPRE_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CPIMPPRE<>0)
    endwith
   endif
  endfunc

  func oCPCONPRE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCPCONPRE_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCPCONPRE_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCPCONPRE_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCPCONPRE_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CPCONPRE
     i_obj.ecpSave()
  endproc

  add object oCPIMPCON_1_7 as StdField with uid="RMMCHCHNQP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CPIMPCON", cQueryName = "CPIMPCON",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 238570892,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=106, Top=98, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  add object oCPCONCON_1_8 as StdField with uid="GWHBREPBFT",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CPCONCON", cQueryName = "CPCONCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente o obsoleto",;
    ToolTipText = "Verr� utilizzata in contabilizzazione per discriminare questo tipo di pagamento",;
    HelpContextID = 240561548,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=250, Top=98, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CPCONCON"

  func oCPCONCON_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CPIMPCON<>0)
    endwith
   endif
  endfunc

  func oCPCONCON_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCPCONCON_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCPCONCON_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCPCONCON_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCPCONCON_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CPCONCON
     i_obj.ecpSave()
  endproc

  add object oCPIMPASS_1_10 as StdField with uid="PQVISDPJKZ",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CPIMPASS", cQueryName = "CPIMPASS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 3689863,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=106, Top=128, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  add object oCPCONASS_1_11 as StdField with uid="EMWZFJIJVO",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CPCONASS", cQueryName = "CPCONASS",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente o obsoleto",;
    ToolTipText = "Verr� utilizzata in contabilizzazione per discriminare questo tipo di pagamento",;
    HelpContextID = 5680519,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=250, Top=128, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CPCONASS"

  func oCPCONASS_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CPIMPASS<>0)
    endwith
   endif
  endfunc

  func oCPCONASS_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCPCONASS_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCPCONASS_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCPCONASS_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCPCONASS_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CPCONASS
     i_obj.ecpSave()
  endproc

  add object oCPIMPCAR_1_13 as StdField with uid="JSRRWOFQGZ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CPIMPCAR", cQueryName = "CPIMPCAR",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 238570888,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=106, Top=158, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  add object oCPCONCAR_1_14 as StdField with uid="RMUOVSHIAM",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CPCONCAR", cQueryName = "CPCONCAR",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente o obsoleto",;
    ToolTipText = "Verr� utilizzata in contabilizzazione per discriminare questo tipo di pagamento",;
    HelpContextID = 240561544,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=250, Top=158, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CPCONCAR"

  func oCPCONCAR_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CPIMPCAR<>0)
    endwith
   endif
  endfunc

  func oCPCONCAR_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCPCONCAR_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCPCONCAR_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCPCONCAR_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCPCONCAR_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CPCONCAR
     i_obj.ecpSave()
  endproc

  add object oCPIMPFIN_1_16 as StdField with uid="HIZMTIDQWN",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CPIMPFIN", cQueryName = "CPIMPFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 80196212,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=106, Top=188, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  add object oCPCONFIN_1_17 as StdField with uid="SPAOPWHVGT",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CPCONFIN", cQueryName = "CPCONFIN",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente o obsoleto",;
    ToolTipText = "Verr� utilizzata in contabilizzazione per discriminare questo tipo di pagamento",;
    HelpContextID = 78205556,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=250, Top=188, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CPCONFIN"

  func oCPCONFIN_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CPIMPFIN<>0)
    endwith
   endif
  endfunc

  func oCPCONFIN_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCPCONFIN_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCPCONFIN_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCPCONFIN_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oCPCONFIN_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CPCONFIN
     i_obj.ecpSave()
  endproc

  add object oDESCON_1_18 as StdField with uid="KBYQEKCVTZ",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 55687370,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=380, Top=98, InputMask=replicate('X',40)

  add object oDESPRE_1_19 as StdField with uid="MXMCUQAUMF",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESPRE", cQueryName = "DESPRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 202684618,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=380, Top=68, InputMask=replicate('X',40)

  add object oDESASS_1_20 as StdField with uid="IGHNRFYXPG",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESASS", cQueryName = "DESASS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 236173514,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=380, Top=128, InputMask=replicate('X',40)

  add object oDESCAR_1_21 as StdField with uid="KDTIFYAJSB",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESCAR", cQueryName = "DESCAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 3258570,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=380, Top=158, InputMask=replicate('X',40)

  add object oDESFIN_1_22 as StdField with uid="YYDQZBWVGX",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 61782218,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=380, Top=188, InputMask=replicate('X',40)

  add object oTOTACC_1_35 as StdField with uid="CNLMSQIFVU",rtseq=23,rtrep=.f.,;
    cFormVar = "w_TOTACC", cQueryName = "TOTACC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale acconti da ripartire sulle contropartite",;
    HelpContextID = 252943818,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=106, Top=10, cSayPict="v_PV(18)", cGetPict="v_GV(18)"


  add object oBtn_1_37 as StdButton with uid="QYACFJOWLI",left=573, top=217, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte impostate";
    , HelpContextID = 158744038;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_37.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction <> 'Query')
      endwith
    endif
  endfunc


  add object oBtn_1_38 as StdButton with uid="OVEULLOQUH",left=625, top=217, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire senza confermare le scelte";
    , HelpContextID = 166032710;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oTOTRES_1_39 as StdField with uid="GTJKNROICT",rtseq=24,rtrep=.f.,;
    cFormVar = "w_TOTRES", cQueryName = "TOTRES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale acconti da ripartire sulle contropartite",;
    HelpContextID = 249732554,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=380, Top=10, cSayPict="v_PV(18)", cGetPict="v_GV(18)"

  add object oStr_1_3 as StdString with uid="MVZIIKXUUH",Visible=.t., Left=6, Top=69,;
    Alignment=1, Width=94, Height=18,;
    Caption="Prepagato:"  ;
  , bGlobalFont=.t.

  func oStr_1_3.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_6 as StdString with uid="FTHETENGBO",Visible=.t., Left=6, Top=99,;
    Alignment=1, Width=94, Height=18,;
    Caption="Contanti:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_9 as StdString with uid="WLPRAPJNZX",Visible=.t., Left=6, Top=129,;
    Alignment=1, Width=94, Height=18,;
    Caption="Assegni:"  ;
  , bGlobalFont=.t.

  func oStr_1_9.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="ZOTROSWIRD",Visible=.t., Left=6, Top=159,;
    Alignment=1, Width=94, Height=18,;
    Caption="Carta/bancomat:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_15 as StdString with uid="PMOBMDNPCO",Visible=.t., Left=6, Top=189,;
    Alignment=1, Width=94, Height=18,;
    Caption="Finanziamento:"  ;
  , bGlobalFont=.t.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="TWQEKAJIXQ",Visible=.t., Left=107, Top=43,;
    Alignment=2, Width=136, Height=18,;
    Caption="Importi"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="TGRZVOAXVS",Visible=.t., Left=250, Top=43,;
    Alignment=2, Width=126, Height=18,;
    Caption="Contropartite"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="NKXONMCOSX",Visible=.t., Left=381, Top=43,;
    Alignment=2, Width=287, Height=18,;
    Caption="Descrizione"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="UUPFZUAEVH",Visible=.t., Left=8, Top=11,;
    Alignment=1, Width=92, Height=18,;
    Caption="Totale acconti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="RZSBMIZRTM",Visible=.t., Left=322, Top=11,;
    Alignment=1, Width=55, Height=18,;
    Caption="Residuo:"  ;
  , bGlobalFont=.t.

  add object oBox_1_26 as StdBox with uid="FQVSMKHESW",left=103, top=39, width=569,height=24

  add object oBox_1_27 as StdBox with uid="SEZZBVGEWS",left=103, top=39, width=2,height=178

  add object oBox_1_28 as StdBox with uid="MHEEGBMTIW",left=247, top=40, width=2,height=178

  add object oBox_1_29 as StdBox with uid="YXQNUFBVVR",left=377, top=40, width=2,height=178
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_acp','CON_PAGA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CPSERIAL=CON_PAGA.CPSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
