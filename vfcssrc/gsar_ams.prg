* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ams                                                        *
*              Metodi di calcolo spese di trasporto e imballo                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_29]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-15                                                      *
* Last revis.: 2013-02-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_ams"))

* --- Class definition
define class tgsar_ams as StdForm
  Top    = 10
  Left   = 32

  * --- Standard Properties
  Width  = 518
  Height = 264+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-02-28"
  HelpContextID=210335593
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=13

  * --- Constant Properties
  METCALSP_IDX = 0
  VALUTE_IDX = 0
  cFile = "METCALSP"
  cKeySelect = "MSCODICE"
  cKeyWhere  = "MSCODICE=this.w_MSCODICE"
  cKeyWhereODBC = '"MSCODICE="+cp_ToStrODBC(this.w_MSCODICE)';

  cKeyWhereODBCqualified = '"METCALSP.MSCODICE="+cp_ToStrODBC(this.w_MSCODICE)';

  cPrg = "gsar_ams"
  cComment = "Metodi di calcolo spese di trasporto e imballo"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MSCODICE = space(5)
  w_MSDESCRI = space(40)
  w_MSBASCAL = space(1)
  w_MSVALUTA = space(3)
  o_MSVALUTA = space(3)
  w_MSIMPFIS = 0
  w_MSIMPMIN = 0
  w_MSIMPMAX = 0
  w_SIMVAL = space(5)
  w_MSIMPFIS = 0
  w_MS_FLCPA = space(1)
  w_MSBASCAL = space(1)
  w_DECTOT = 0
  o_DECTOT = 0
  w_CALCPIC = 0

  * --- Children pointers
  GSAR_MSS = .NULL.
  GSAR_MSP = .NULL.
  GSAR_MSI = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'METCALSP','gsar_ams')
    stdPageFrame::Init()
    *set procedure to GSAR_MSS additive
    *set procedure to GSAR_MSP additive
    *set procedure to GSAR_MSI additive
    with this
      .Pages(1).addobject("oPag","tgsar_amsPag1","gsar_ams",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Metodi di calcolo spese di trasporto e imballo")
      .Pages(1).HelpContextID = 44334321
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oMSCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSAR_MSS
    *release procedure GSAR_MSP
    *release procedure GSAR_MSI
    * --- Area Manuale = Init Page Frame
    * --- gsar_ams
    
    WITH THIS.PARENT
    If IsAlt()
       .cComment = CP_TRANSLATE('Parametri spese generali e cassa di previdenza')
    Else
       .cComment = CP_TRANSLATE('Metodi di calcolo spese di trasporto e imballo')
    Endif
    Endwith
    
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='METCALSP'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.METCALSP_IDX,5],7]
    this.nPostItConn=i_TableProp[this.METCALSP_IDX,3]
  return

  function CreateChildren()
    this.GSAR_MSS = CREATEOBJECT('stdDynamicChild',this,'GSAR_MSS',this.oPgFrm.Page1.oPag.oLinkPC_1_13)
    this.GSAR_MSS.createrealchild()
    this.GSAR_MSP = CREATEOBJECT('stdDynamicChild',this,'GSAR_MSP',this.oPgFrm.Page1.oPag.oLinkPC_1_14)
    this.GSAR_MSP.createrealchild()
    this.GSAR_MSI = CREATEOBJECT('stdDynamicChild',this,'GSAR_MSI',this.oPgFrm.Page1.oPag.oLinkPC_1_15)
    this.GSAR_MSI.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAR_MSS)
      this.GSAR_MSS.DestroyChildrenChain()
      this.GSAR_MSS=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_13')
    if !ISNULL(this.GSAR_MSP)
      this.GSAR_MSP.DestroyChildrenChain()
      this.GSAR_MSP=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_14')
    if !ISNULL(this.GSAR_MSI)
      this.GSAR_MSI.DestroyChildrenChain()
      this.GSAR_MSI=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_15')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAR_MSS.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MSP.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MSI.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAR_MSS.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MSP.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MSI.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAR_MSS.NewDocument()
    this.GSAR_MSP.NewDocument()
    this.GSAR_MSI.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAR_MSS.SetKey(;
            .w_MSCODICE,"SCCODICE";
            )
      this.GSAR_MSP.SetKey(;
            .w_MSCODICE,"SPCODICE";
            )
      this.GSAR_MSI.SetKey(;
            .w_MSCODICE,"SICODICE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAR_MSS.ChangeRow(this.cRowID+'      1',1;
             ,.w_MSCODICE,"SCCODICE";
             )
      .GSAR_MSP.ChangeRow(this.cRowID+'      1',1;
             ,.w_MSCODICE,"SPCODICE";
             )
      .GSAR_MSI.ChangeRow(this.cRowID+'      1',1;
             ,.w_MSCODICE,"SICODICE";
             )
      .WriteTo_GSAR_MSI()
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAR_MSS)
        i_f=.GSAR_MSS.BuildFilter()
        if !(i_f==.GSAR_MSS.cQueryFilter)
          i_fnidx=.GSAR_MSS.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MSS.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MSS.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MSS.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MSS.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAR_MSP)
        i_f=.GSAR_MSP.BuildFilter()
        if !(i_f==.GSAR_MSP.cQueryFilter)
          i_fnidx=.GSAR_MSP.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MSP.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MSP.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MSP.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MSP.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAR_MSI)
        i_f=.GSAR_MSI.BuildFilter()
        if !(i_f==.GSAR_MSI.cQueryFilter)
          i_fnidx=.GSAR_MSI.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MSI.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MSI.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MSI.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MSI.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSAR_MSI()
  if at('gsar_msi',lower(this.GSAR_MSI.class))<>0
    if this.GSAR_MSI.w_DECTOT<>this.w_DECTOT
      this.GSAR_MSI.w_DECTOT = this.w_DECTOT
      this.GSAR_MSI.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_MSCODICE = NVL(MSCODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_5_joined
    link_1_5_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from METCALSP where MSCODICE=KeySet.MSCODICE
    *
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('METCALSP')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "METCALSP.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' METCALSP '
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MSCODICE',this.w_MSCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_SIMVAL = space(5)
        .w_DECTOT = 0
        .w_MSCODICE = NVL(MSCODICE,space(5))
        .w_MSDESCRI = NVL(MSDESCRI,space(40))
        .w_MSBASCAL = NVL(MSBASCAL,space(1))
        .w_MSVALUTA = NVL(MSVALUTA,space(3))
          if link_1_5_joined
            this.w_MSVALUTA = NVL(VACODVAL105,NVL(this.w_MSVALUTA,space(3)))
            this.w_SIMVAL = NVL(VASIMVAL105,space(5))
            this.w_DECTOT = NVL(VADECTOT105,0)
          else
          .link_1_5('Load')
          endif
        .w_MSIMPFIS = NVL(MSIMPFIS,0)
        .w_MSIMPMIN = NVL(MSIMPMIN,0)
        .w_MSIMPMAX = NVL(MSIMPMAX,0)
        .w_MSIMPFIS = NVL(MSIMPFIS,0)
        .w_MS_FLCPA = NVL(MS_FLCPA,space(1))
        .w_MSBASCAL = NVL(MSBASCAL,space(1))
        .w_CALCPIC = DEFPIC(.w_DECTOT)
        cp_LoadRecExtFlds(this,'METCALSP')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_MSCODICE = space(5)
      .w_MSDESCRI = space(40)
      .w_MSBASCAL = space(1)
      .w_MSVALUTA = space(3)
      .w_MSIMPFIS = 0
      .w_MSIMPMIN = 0
      .w_MSIMPMAX = 0
      .w_SIMVAL = space(5)
      .w_MSIMPFIS = 0
      .w_MS_FLCPA = space(1)
      .w_MSBASCAL = space(1)
      .w_DECTOT = 0
      .w_CALCPIC = 0
      if .cFunction<>"Filter"
          .DoRTCalc(1,2,.f.)
        .w_MSBASCAL = 'T'
        .w_MSVALUTA = g_PERVAL
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_MSVALUTA))
          .link_1_5('Full')
          endif
          .DoRTCalc(5,9,.f.)
        .w_MS_FLCPA = 'N'
        .w_MSBASCAL = 'T'
          .DoRTCalc(12,12,.f.)
        .w_CALCPIC = DEFPIC(.w_DECTOT)
      endif
    endwith
    cp_BlankRecExtFlds(this,'METCALSP')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMSCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oMSDESCRI_1_3.enabled = i_bVal
      .Page1.oPag.oMSBASCAL_1_4.enabled = i_bVal
      .Page1.oPag.oMSVALUTA_1_5.enabled = i_bVal
      .Page1.oPag.oMSIMPFIS_1_7.enabled = i_bVal
      .Page1.oPag.oMSIMPMIN_1_9.enabled = i_bVal
      .Page1.oPag.oMSIMPMAX_1_11.enabled = i_bVal
      .Page1.oPag.oMSIMPFIS_1_18.enabled = i_bVal
      .Page1.oPag.oMS_FLCPA_1_21.enabled_(i_bVal)
      .Page1.oPag.oMSBASCAL_1_22.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oMSCODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oMSCODICE_1_1.enabled = .t.
      endif
    endwith
    this.GSAR_MSS.SetStatus(i_cOp)
    this.GSAR_MSP.SetStatus(i_cOp)
    this.GSAR_MSI.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'METCALSP',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAR_MSS.SetChildrenStatus(i_cOp)
  *  this.GSAR_MSP.SetChildrenStatus(i_cOp)
  *  this.GSAR_MSI.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MSCODICE,"MSCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MSDESCRI,"MSDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MSBASCAL,"MSBASCAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MSVALUTA,"MSVALUTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MSIMPFIS,"MSIMPFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MSIMPMIN,"MSIMPMIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MSIMPMAX,"MSIMPMAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MSIMPFIS,"MSIMPFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MS_FLCPA,"MS_FLCPA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MSBASCAL,"MSBASCAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    i_lTable = "METCALSP"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.METCALSP_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.METCALSP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.METCALSP_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into METCALSP
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'METCALSP')
        i_extval=cp_InsertValODBCExtFlds(this,'METCALSP')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(MSCODICE,MSDESCRI,MSBASCAL,MSVALUTA,MSIMPFIS"+;
                  ",MSIMPMIN,MSIMPMAX,MS_FLCPA "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_MSCODICE)+;
                  ","+cp_ToStrODBC(this.w_MSDESCRI)+;
                  ","+cp_ToStrODBC(this.w_MSBASCAL)+;
                  ","+cp_ToStrODBCNull(this.w_MSVALUTA)+;
                  ","+cp_ToStrODBC(this.w_MSIMPFIS)+;
                  ","+cp_ToStrODBC(this.w_MSIMPMIN)+;
                  ","+cp_ToStrODBC(this.w_MSIMPMAX)+;
                  ","+cp_ToStrODBC(this.w_MS_FLCPA)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'METCALSP')
        i_extval=cp_InsertValVFPExtFlds(this,'METCALSP')
        cp_CheckDeletedKey(i_cTable,0,'MSCODICE',this.w_MSCODICE)
        INSERT INTO (i_cTable);
              (MSCODICE,MSDESCRI,MSBASCAL,MSVALUTA,MSIMPFIS,MSIMPMIN,MSIMPMAX,MS_FLCPA  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_MSCODICE;
                  ,this.w_MSDESCRI;
                  ,this.w_MSBASCAL;
                  ,this.w_MSVALUTA;
                  ,this.w_MSIMPFIS;
                  ,this.w_MSIMPMIN;
                  ,this.w_MSIMPMAX;
                  ,this.w_MS_FLCPA;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.METCALSP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.METCALSP_IDX,i_nConn)
      *
      * update METCALSP
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'METCALSP')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " MSDESCRI="+cp_ToStrODBC(this.w_MSDESCRI)+;
             ",MSBASCAL="+cp_ToStrODBC(this.w_MSBASCAL)+;
             ",MSVALUTA="+cp_ToStrODBCNull(this.w_MSVALUTA)+;
             ",MSIMPFIS="+cp_ToStrODBC(this.w_MSIMPFIS)+;
             ",MSIMPMIN="+cp_ToStrODBC(this.w_MSIMPMIN)+;
             ",MSIMPMAX="+cp_ToStrODBC(this.w_MSIMPMAX)+;
             ",MS_FLCPA="+cp_ToStrODBC(this.w_MS_FLCPA)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'METCALSP')
        i_cWhere = cp_PKFox(i_cTable  ,'MSCODICE',this.w_MSCODICE  )
        UPDATE (i_cTable) SET;
              MSDESCRI=this.w_MSDESCRI;
             ,MSBASCAL=this.w_MSBASCAL;
             ,MSVALUTA=this.w_MSVALUTA;
             ,MSIMPFIS=this.w_MSIMPFIS;
             ,MSIMPMIN=this.w_MSIMPMIN;
             ,MSIMPMAX=this.w_MSIMPMAX;
             ,MS_FLCPA=this.w_MS_FLCPA;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAR_MSS : Saving
      this.GSAR_MSS.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MSCODICE,"SCCODICE";
             )
      this.GSAR_MSS.mReplace()
      * --- GSAR_MSP : Saving
      this.GSAR_MSP.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MSCODICE,"SPCODICE";
             )
      this.GSAR_MSP.mReplace()
      * --- GSAR_MSI : Saving
      this.GSAR_MSI.ChangeRow(this.cRowID+'      1',0;
             ,this.w_MSCODICE,"SICODICE";
             )
      this.GSAR_MSI.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSAR_MSS : Deleting
    this.GSAR_MSS.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MSCODICE,"SCCODICE";
           )
    this.GSAR_MSS.mDelete()
    * --- GSAR_MSP : Deleting
    this.GSAR_MSP.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MSCODICE,"SPCODICE";
           )
    this.GSAR_MSP.mDelete()
    * --- GSAR_MSI : Deleting
    this.GSAR_MSI.ChangeRow(this.cRowID+'      1',0;
           ,this.w_MSCODICE,"SICODICE";
           )
    this.GSAR_MSI.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.METCALSP_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.METCALSP_IDX,i_nConn)
      *
      * delete METCALSP
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'MSCODICE',this.w_MSCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    if i_bUpd
      with this
        if  .o_DECTOT<>.w_DECTOT
          .WriteTo_GSAR_MSI()
        endif
        .DoRTCalc(1,12,.t.)
        if .o_MSVALUTA<>.w_MSVALUTA
            .w_CALCPIC = DEFPIC(.w_DECTOT)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMSBASCAL_1_4.enabled = this.oPgFrm.Page1.oPag.oMSBASCAL_1_4.mCond()
    this.oPgFrm.Page1.oPag.oMS_FLCPA_1_21.enabled_(this.oPgFrm.Page1.oPag.oMS_FLCPA_1_21.mCond())
    this.oPgFrm.Page1.oPag.oMSBASCAL_1_22.enabled = this.oPgFrm.Page1.oPag.oMSBASCAL_1_22.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMSBASCAL_1_4.visible=!this.oPgFrm.Page1.oPag.oMSBASCAL_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oMSIMPFIS_1_7.visible=!this.oPgFrm.Page1.oPag.oMSIMPFIS_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oMSIMPMIN_1_9.visible=!this.oPgFrm.Page1.oPag.oMSIMPMIN_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_10.visible=!this.oPgFrm.Page1.oPag.oStr_1_10.mHide()
    this.oPgFrm.Page1.oPag.oMSIMPMAX_1_11.visible=!this.oPgFrm.Page1.oPag.oMSIMPMAX_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_13.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_13.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_14.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_14.mHide()
    this.oPgFrm.Page1.oPag.oLinkPC_1_15.visible=!this.oPgFrm.Page1.oPag.oLinkPC_1_15.mHide()
    this.oPgFrm.Page1.oPag.oMSIMPFIS_1_18.visible=!this.oPgFrm.Page1.oPag.oMSIMPFIS_1_18.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_19.visible=!this.oPgFrm.Page1.oPag.oStr_1_19.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oMS_FLCPA_1_21.visible=!this.oPgFrm.Page1.oPag.oMS_FLCPA_1_21.mHide()
    this.oPgFrm.Page1.oPag.oMSBASCAL_1_22.visible=!this.oPgFrm.Page1.oPag.oMSBASCAL_1_22.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsar_ams
    if cevent='Init'
    If IsAlt()
       This.opGFRM.page1.Caption=CP_TRANSLATE('Parametri spese generali e cassa di previdenza')
    Else
       This.opGFRM.page1.Caption=CP_TRANSLATE('Metodi di calcolo spese di trasporto e imballo')
    Endif
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MSVALUTA
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MSVALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_MSVALUTA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_MSVALUTA))
          select VACODVAL,VASIMVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MSVALUTA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MSVALUTA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oMSVALUTA_1_5'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MSVALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VASIMVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_MSVALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_MSVALUTA)
            select VACODVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MSVALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_MSVALUTA = space(3)
      endif
      this.w_SIMVAL = space(5)
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MSVALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.VACODVAL as VACODVAL105"+ ",link_1_5.VASIMVAL as VASIMVAL105"+ ",link_1_5.VADECTOT as VADECTOT105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on METCALSP.MSVALUTA=link_1_5.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and METCALSP.MSVALUTA=link_1_5.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMSCODICE_1_1.value==this.w_MSCODICE)
      this.oPgFrm.Page1.oPag.oMSCODICE_1_1.value=this.w_MSCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oMSDESCRI_1_3.value==this.w_MSDESCRI)
      this.oPgFrm.Page1.oPag.oMSDESCRI_1_3.value=this.w_MSDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oMSBASCAL_1_4.RadioValue()==this.w_MSBASCAL)
      this.oPgFrm.Page1.oPag.oMSBASCAL_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMSVALUTA_1_5.value==this.w_MSVALUTA)
      this.oPgFrm.Page1.oPag.oMSVALUTA_1_5.value=this.w_MSVALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oMSIMPFIS_1_7.value==this.w_MSIMPFIS)
      this.oPgFrm.Page1.oPag.oMSIMPFIS_1_7.value=this.w_MSIMPFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oMSIMPMIN_1_9.value==this.w_MSIMPMIN)
      this.oPgFrm.Page1.oPag.oMSIMPMIN_1_9.value=this.w_MSIMPMIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMSIMPMAX_1_11.value==this.w_MSIMPMAX)
      this.oPgFrm.Page1.oPag.oMSIMPMAX_1_11.value=this.w_MSIMPMAX
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_16.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_16.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMSIMPFIS_1_18.value==this.w_MSIMPFIS)
      this.oPgFrm.Page1.oPag.oMSIMPFIS_1_18.value=this.w_MSIMPFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oMS_FLCPA_1_21.RadioValue()==this.w_MS_FLCPA)
      this.oPgFrm.Page1.oPag.oMS_FLCPA_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMSBASCAL_1_22.RadioValue()==this.w_MSBASCAL)
      this.oPgFrm.Page1.oPag.oMSBASCAL_1_22.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'METCALSP')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MSCODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMSCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_MSCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MSIMPMAX = 0 OR .w_MSIMPMIN <= .w_MSIMPMAX)  and not(IsAlt())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMSIMPMIN_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_MSIMPMAX = 0 OR .w_MSIMPMIN <= .w_MSIMPMAX)  and not(IsAlt())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oMSIMPMAX_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAR_MSS.CheckForm()
      if i_bres
        i_bres=  .GSAR_MSS.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MSP.CheckForm()
      if i_bres
        i_bres=  .GSAR_MSP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MSI.CheckForm()
      if i_bres
        i_bres=  .GSAR_MSI.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsar_ams
      IF NOT IsAlt()
          local L_PADRE, L_CONTA
          DO CASE
          CASE NOT INLIST( THIS.w_MSBASCAL, 'P', 'T', 'S')
                L_PADRE=THIS.GSAR_MSS
          CASE NOT INLIST( THIS.w_MSBASCAL, 'P', 'N', 'Q')
                L_PADRE=THIS.GSAR_MSI
          CASE THIS.w_MSBASCAL = 'P'
                L_PADRE=THIS.GSAR_MSP
          ENDCASE
          L_CONTA=L_PADRE.NumRow()
           IF L_CONTA=0
           AH_ERRORMSG("Numero insufficiente di righe ( minimo richiesto: 1)",48)
           i_bRes=.F.
           ENDIF
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MSVALUTA = this.w_MSVALUTA
    this.o_DECTOT = this.w_DECTOT
    * --- GSAR_MSS : Depends On
    this.GSAR_MSS.SaveDependsOn()
    * --- GSAR_MSP : Depends On
    this.GSAR_MSP.SaveDependsOn()
    * --- GSAR_MSI : Depends On
    this.GSAR_MSI.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsar_amsPag1 as StdContainer
  Width  = 514
  height = 264
  stdWidth  = 514
  stdheight = 264
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMSCODICE_1_1 as StdField with uid="SBTQYMEJAC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_MSCODICE", cQueryName = "MSCODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo",;
    HelpContextID = 17436939,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=109, Top=15, InputMask=replicate('X',5)

  add object oMSDESCRI_1_3 as StdField with uid="OBIYNVRJHD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_MSDESCRI", cQueryName = "MSDESCRI",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione metodo di calcolo",;
    HelpContextID = 200286479,;
   bGlobalFont=.t.,;
    Height=21, Width=300, Left=164, Top=15, InputMask=replicate('X',40)


  add object oMSBASCAL_1_4 as StdCombo with uid="IOEORPUPPX",rtseq=3,rtrep=.f.,left=109,top=47,width=206,height=21;
    , ToolTipText = "Base di calcolo";
    , HelpContextID = 68419310;
    , cFormVar="w_MSBASCAL",RowSource=""+"Totale righe documento,"+"Totale esclusi servizi,"+"Numero colli,"+"Quantit� confezioni,"+"Peso lordo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMSBASCAL_1_4.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    iif(this.value =4,'Q',;
    iif(this.value =5,'P',;
    space(1)))))))
  endfunc
  func oMSBASCAL_1_4.GetRadio()
    this.Parent.oContained.w_MSBASCAL = this.RadioValue()
    return .t.
  endfunc

  func oMSBASCAL_1_4.SetRadio()
    this.Parent.oContained.w_MSBASCAL=trim(this.Parent.oContained.w_MSBASCAL)
    this.value = ;
      iif(this.Parent.oContained.w_MSBASCAL=='T',1,;
      iif(this.Parent.oContained.w_MSBASCAL=='S',2,;
      iif(this.Parent.oContained.w_MSBASCAL=='N',3,;
      iif(this.Parent.oContained.w_MSBASCAL=='Q',4,;
      iif(this.Parent.oContained.w_MSBASCAL=='P',5,;
      0)))))
  endfunc

  func oMSBASCAL_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction = 'Load')
    endwith
   endif
  endfunc

  func oMSBASCAL_1_4.mHide()
    with this.Parent.oContained
      return (!IsAhr())
    endwith
  endfunc

  add object oMSVALUTA_1_5 as StdField with uid="QUMKPKNNNG",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MSVALUTA", cQueryName = "MSVALUTA",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 226312455,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=373, Top=47, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_MSVALUTA"

  func oMSVALUTA_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oMSVALUTA_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMSVALUTA_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oMSVALUTA_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oMSVALUTA_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_MSVALUTA
     i_obj.ecpSave()
  endproc

  add object oMSIMPFIS_1_7 as StdField with uid="BPVTNPPWNR",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MSIMPFIS", cQueryName = "MSIMPFIS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo fisso da attribuire sempre",;
    HelpContextID = 248017177,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=109, Top=76, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oMSIMPFIS_1_7.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oMSIMPMIN_1_9 as StdField with uid="BJRFZFHBXP",rtseq=6,rtrep=.f.,;
    cFormVar = "w_MSIMPMIN", cQueryName = "MSIMPMIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo minimo da attribuire",;
    HelpContextID = 171413228,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=109, Top=107, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oMSIMPMIN_1_9.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oMSIMPMIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MSIMPMAX = 0 OR .w_MSIMPMIN <= .w_MSIMPMAX)
    endwith
    return bRes
  endfunc

  add object oMSIMPMAX_1_11 as StdField with uid="ALHQUSLSFM",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MSIMPMAX", cQueryName = "MSIMPMAX",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo massimo da attribuire (se zero viene ignorato)",;
    HelpContextID = 97022238,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=373, Top=107, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oMSIMPMAX_1_11.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oMSIMPMAX_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MSIMPMAX = 0 OR .w_MSIMPMIN <= .w_MSIMPMAX)
    endwith
    return bRes
  endfunc


  add object oLinkPC_1_13 as stdDynamicChildContainer with uid="RLLSAUGOCM",left=27, top=138, width=460, height=126, bOnScreen=.t.;


  func oLinkPC_1_13.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (INLIST( .w_MSBASCAL, 'P', 'T', 'S') OR IsAlt())
     endwith
    endif
  endfunc


  add object oLinkPC_1_14 as stdDynamicChildContainer with uid="DFCCGEPFCU",left=27, top=138, width=460, height=126, bOnScreen=.t.;


  func oLinkPC_1_14.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_MSBASCAL <> 'P' OR IsAlt())
     endwith
    endif
  endfunc


  add object oLinkPC_1_15 as stdDynamicChildContainer with uid="ITDPJZMIXD",left=27, top=138, width=460, height=126, bOnScreen=.t.;


  func oLinkPC_1_15.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (INLIST( .w_MSBASCAL, 'P', 'N', 'Q') OR IsAlt())
     endwith
    endif
  endfunc

  add object oSIMVAL_1_16 as StdField with uid="NKVMLXNNTR",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 203315418,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=424, Top=47, InputMask=replicate('X',5)

  add object oMSIMPFIS_1_18 as StdField with uid="IAOUKSRDOS",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MSIMPFIS", cQueryName = "MSIMPFIS",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale",;
    HelpContextID = 248017177,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=373, Top=76, cSayPict='"999.99"', cGetPict='"999.99"'

  func oMSIMPFIS_1_18.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oMS_FLCPA_1_21 as StdRadio with uid="HRORDDPVAL",rtseq=10,rtrep=.f.,left=122, top=76, width=137,height=59;
    , ToolTipText = "Metodo relativo a cassa di previdenza oppure spese generali";
    , cFormVar="w_MS_FLCPA", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oMS_FLCPA_1_21.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Cassa previdenza"
      this.Buttons(1).HelpContextID = 75312889
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Spese generali"
      this.Buttons(2).HelpContextID = 75312889
      this.Buttons(2).Top=28
      this.SetAll("Width",135)
      this.SetAll("Height",30)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Metodo relativo a cassa di previdenza oppure spese generali")
      StdRadio::init()
    endproc

  func oMS_FLCPA_1_21.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oMS_FLCPA_1_21.GetRadio()
    this.Parent.oContained.w_MS_FLCPA = this.RadioValue()
    return .t.
  endfunc

  func oMS_FLCPA_1_21.SetRadio()
    this.Parent.oContained.w_MS_FLCPA=trim(this.Parent.oContained.w_MS_FLCPA)
    this.value = ;
      iif(this.Parent.oContained.w_MS_FLCPA=='S',1,;
      iif(this.Parent.oContained.w_MS_FLCPA=='N',2,;
      0))
  endfunc

  func oMS_FLCPA_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oMS_FLCPA_1_21.mHide()
    with this.Parent.oContained
      return (!IsAlt())
    endwith
  endfunc


  add object oMSBASCAL_1_22 as StdCombo with uid="BSYTXRILGA",rtseq=11,rtrep=.f.,left=109,top=47,width=206,height=21;
    , ToolTipText = "Base di calcolo";
    , HelpContextID = 68419310;
    , cFormVar="w_MSBASCAL",RowSource=""+"Totale righe documento,"+"Totale esclusi servizi,"+"Numero colli,"+"Peso lordo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMSBASCAL_1_22.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    iif(this.value =4,'P',;
    space(1))))))
  endfunc
  func oMSBASCAL_1_22.GetRadio()
    this.Parent.oContained.w_MSBASCAL = this.RadioValue()
    return .t.
  endfunc

  func oMSBASCAL_1_22.SetRadio()
    this.Parent.oContained.w_MSBASCAL=trim(this.Parent.oContained.w_MSBASCAL)
    this.value = ;
      iif(this.Parent.oContained.w_MSBASCAL=='T',1,;
      iif(this.Parent.oContained.w_MSBASCAL=='S',2,;
      iif(this.Parent.oContained.w_MSBASCAL=='N',3,;
      iif(this.Parent.oContained.w_MSBASCAL=='P',4,;
      0))))
  endfunc

  func oMSBASCAL_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction = 'Load')
    endwith
   endif
  endfunc

  func oMSBASCAL_1_22.mHide()
    with this.Parent.oContained
      return (!IsAhe())
    endwith
  endfunc

  add object oStr_1_2 as StdString with uid="SVJZEOCDKH",Visible=.t., Left=47, Top=15,;
    Alignment=1, Width=61, Height=19,;
    Caption="Metodo:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_6 as StdString with uid="MQGHARGDJR",Visible=.t., Left=3, Top=47,;
    Alignment=1, Width=105, Height=18,;
    Caption="Base di calcolo:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="KKLWYECRVQ",Visible=.t., Left=29, Top=76,;
    Alignment=1, Width=79, Height=18,;
    Caption="Importo fisso:"  ;
  , bGlobalFont=.t.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="RGPAFRVKXB",Visible=.t., Left=25, Top=107,;
    Alignment=1, Width=83, Height=18,;
    Caption="Importo min:"  ;
  , bGlobalFont=.t.

  func oStr_1_10.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="APEKZOQQKU",Visible=.t., Left=291, Top=107,;
    Alignment=1, Width=79, Height=18,;
    Caption="Importo max:"  ;
  , bGlobalFont=.t.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="OHBUOVELWI",Visible=.t., Left=321, Top=47,;
    Alignment=1, Width=49, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="LHQMUTPZFY",Visible=.t., Left=291, Top=76,;
    Alignment=1, Width=79, Height=18,;
    Caption="%:"  ;
  , bGlobalFont=.t.

  func oStr_1_19.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oStr_1_20 as StdString with uid="POLJIGXPEG",Visible=.t., Left=603, Top=83,;
    Alignment=0, Width=354, Height=18,;
    Caption="Il campo MSIMPFIS � duplicato (visibile solo se AlterEgo)"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_ams','METCALSP','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MSCODICE=METCALSP.MSCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
