* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kes                                                        *
*              Eliminazione registrazioni contabili                            *
*                                                                              *
*      Author: ZUCCHETTI SPA: CS                                               *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-06-06                                                      *
* Last revis.: 2014-02-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kes",oParentObject))

* --- Class definition
define class tgscg_kes as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 785
  Height = 440+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-02-24"
  HelpContextID=65676649
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=49

  * --- Constant Properties
  _IDX = 0
  CAU_CONT_IDX = 0
  cPrg = "gscg_kes"
  cComment = "Eliminazione registrazioni contabili"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_cOper = space(1)
  w_data1 = ctod('  /  /  ')
  w_data2 = ctod('  /  /  ')
  w_Num1 = 0
  w_Num1 = 0
  w_ADOINI = space(2)
  w_ADOINI = space(10)
  w_Num2 = 0
  w_Num2 = 0
  w_ADOFIN = space(2)
  w_ADOFIN = space(10)
  w_Prot1 = 0
  w_Prot1 = 0
  w_APRINI = space(2)
  w_APRINI = space(10)
  w_Prot2 = 0
  w_Prot2 = 0
  w_APRFIN = space(2)
  w_APRFIN = space(10)
  w_CODCAU = space(5)
  w_PROVVI = space(1)
  w_SELEZI = space(1)
  o_SELEZI = space(1)
  w_TESTSEL = space(1)
  w_AZIMAN = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_SERIALE = space(35)
  w_CCDESCRI = space(35)
  w_MSG = space(0)
  w_PNPRP = space(1)
  w_PNANNPRO = space(4)
  w_PNALFPRO = space(1)
  w_NUMPRO = 0
  w_CAUPNSEL = space(5)
  w_FLPPRO = space(1)
  w_PNTIPREG = space(10)
  w_PNCODCAU = space(5)
  w_PNDATREG = ctod('  /  /  ')
  w_PNALFDOC = space(2)
  w_PNNUMDOC = 0
  w_PNDATDOC = ctod('  /  /  ')
  w_PNCOMIVA = ctod('  /  /  ')
  w_PNDATPLA = ctod('  /  /  ')
  w_PNSERIAL = space(10)
  w_PNNUMPRO = 0
  w_PNTIPCLF = space(1)
  w_PNCODCLF = space(15)
  w_PNANNDOC = space(4)
  w_PNPRD = space(1)
  w_ZOOMPN = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kesPag1","gscg_kes",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Eliminazione")
      .Pages(2).addobject("oPag","tgscg_kesPag2","gscg_kes",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Log")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.odata1_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gscg_kes
    WITH THIS.PARENT
       .w_cOper=iif(vartype(oParentObject)='C',oParentObject,' ')
       .cComment=iif(vartype(oParentObject)='C','Conferma movimenti provvisori di primanota','Eliminazione registrazioni contabili')  
    ENDWITH   
    this.Pages(1).Caption=iif(vartype(oParentObject)='C','Conferma','Eliminazione')
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMPN = this.oPgFrm.Pages(1).oPag.ZOOMPN
    DoDefault()
    proc Destroy()
      this.w_ZOOMPN = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CAU_CONT'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_cOper=space(1)
      .w_data1=ctod("  /  /  ")
      .w_data2=ctod("  /  /  ")
      .w_Num1=0
      .w_Num1=0
      .w_ADOINI=space(2)
      .w_ADOINI=space(10)
      .w_Num2=0
      .w_Num2=0
      .w_ADOFIN=space(2)
      .w_ADOFIN=space(10)
      .w_Prot1=0
      .w_Prot1=0
      .w_APRINI=space(2)
      .w_APRINI=space(10)
      .w_Prot2=0
      .w_Prot2=0
      .w_APRFIN=space(2)
      .w_APRFIN=space(10)
      .w_CODCAU=space(5)
      .w_PROVVI=space(1)
      .w_SELEZI=space(1)
      .w_TESTSEL=space(1)
      .w_AZIMAN=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_SERIALE=space(35)
      .w_CCDESCRI=space(35)
      .w_MSG=space(0)
      .w_PNPRP=space(1)
      .w_PNANNPRO=space(4)
      .w_PNALFPRO=space(1)
      .w_NUMPRO=0
      .w_CAUPNSEL=space(5)
      .w_FLPPRO=space(1)
      .w_PNTIPREG=space(10)
      .w_PNCODCAU=space(5)
      .w_PNDATREG=ctod("  /  /  ")
      .w_PNALFDOC=space(2)
      .w_PNNUMDOC=0
      .w_PNDATDOC=ctod("  /  /  ")
      .w_PNCOMIVA=ctod("  /  /  ")
      .w_PNDATPLA=ctod("  /  /  ")
      .w_PNSERIAL=space(10)
      .w_PNNUMPRO=0
      .w_PNTIPCLF=space(1)
      .w_PNCODCLF=space(15)
      .w_PNANNDOC=space(4)
      .w_PNPRD=space(1)
        .w_cOper = iif(type('this.oParentobject')='C',this.oParentObject,' ')
        .w_data1 = g_INIESE
        .w_data2 = g_FINESE
        .DoRTCalc(4,20,.f.)
        if not(empty(.w_CODCAU))
          .link_1_20('Full')
        endif
        .w_PROVVI = iif(! empty(.w_cOper),'S',iif(isahe() or isapa(),'S',' '))
      .oPgFrm.Page1.oPag.ZOOMPN.Calculate()
        .w_SELEZI = 'D'
          .DoRTCalc(23,23,.f.)
        .w_AZIMAN = i_CODAZI
          .DoRTCalc(25,25,.f.)
        .w_OBTEST = i_DATSYS
        .w_SERIALE = .w_Zoompn.getVar('PNSERIAL')
      .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
          .DoRTCalc(28,29,.f.)
        .w_PNPRP = .w_ZOOMPN.GETVAR('PNPRP')
        .w_PNANNPRO = .w_ZOOMPN.GETVAR('PNANNPRO')
        .w_PNALFPRO = .w_ZOOMPN.GETVAR('PNALFPRO')
        .w_NUMPRO = .w_ZOOMPN.GETVAR('PNNUMPRO')
        .w_CAUPNSEL = .w_ZOOMPN.GETVAR('PNCODCAU')
        .DoRTCalc(34,34,.f.)
        if not(empty(.w_CAUPNSEL))
          .link_1_67('Full')
        endif
          .DoRTCalc(35,35,.f.)
        .w_PNTIPREG = .w_ZOOMPN.GETVAR('PNTIPREG')
        .w_PNCODCAU = .w_Zoompn.getVar('PNCODCAU')
        .w_PNDATREG = cp_todate(.w_Zoompn.getVar('PNDATREG'))
        .w_PNALFDOC = .w_Zoompn.getVar('PNALFDOC')
        .w_PNNUMDOC = .w_Zoompn.getVar('PNNUMDOC')
        .w_PNDATDOC = CP_TODATE(.w_Zoompn.getVar('PNDATDOC'))
        .w_PNCOMIVA = CP_TODATE(.w_Zoompn.getVar('PNCOMIVA'))
        .w_PNDATPLA = CP_TODATE(.w_Zoompn.getVar('PNDATPLA'))
        .w_PNSERIAL = .w_Zoompn.getVar('PNSERIAL')
        .w_PNNUMPRO = .w_Zoompn.getVar('PNNUMPRO')
      .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
        .w_PNTIPCLF = .w_Zoompn.getVar('PNTIPCLF')
        .w_PNCODCLF = .w_Zoompn.getVar('PNCODCLF')
        .w_PNANNDOC = .w_Zoompn.getVar('PNANNDOC')
        .w_PNPRD = .w_ZOOMPN.GETVAR('PNPRD')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_54.enabled = this.oPgFrm.Page1.oPag.oBtn_1_54.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_55.enabled = this.oPgFrm.Page1.oPag.oBtn_1_55.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_56.enabled = this.oPgFrm.Page1.oPag.oBtn_1_56.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_57.enabled = this.oPgFrm.Page1.oPag.oBtn_1_57.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_70.enabled = this.oPgFrm.Page1.oPag.oBtn_1_70.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMPN.Calculate()
        if .o_SELEZI<>.w_SELEZI
          .Calculate_JROJMCSSXL()
        endif
        .DoRTCalc(1,26,.t.)
            .w_SERIALE = .w_Zoompn.getVar('PNSERIAL')
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .DoRTCalc(28,29,.t.)
            .w_PNPRP = .w_ZOOMPN.GETVAR('PNPRP')
            .w_PNANNPRO = .w_ZOOMPN.GETVAR('PNANNPRO')
            .w_PNALFPRO = .w_ZOOMPN.GETVAR('PNALFPRO')
            .w_NUMPRO = .w_ZOOMPN.GETVAR('PNNUMPRO')
            .w_CAUPNSEL = .w_ZOOMPN.GETVAR('PNCODCAU')
          .link_1_67('Full')
        .DoRTCalc(35,35,.t.)
            .w_PNTIPREG = .w_ZOOMPN.GETVAR('PNTIPREG')
            .w_PNCODCAU = .w_Zoompn.getVar('PNCODCAU')
            .w_PNDATREG = cp_todate(.w_Zoompn.getVar('PNDATREG'))
            .w_PNALFDOC = .w_Zoompn.getVar('PNALFDOC')
            .w_PNNUMDOC = .w_Zoompn.getVar('PNNUMDOC')
            .w_PNDATDOC = CP_TODATE(.w_Zoompn.getVar('PNDATDOC'))
            .w_PNCOMIVA = CP_TODATE(.w_Zoompn.getVar('PNCOMIVA'))
            .w_PNDATPLA = CP_TODATE(.w_Zoompn.getVar('PNDATPLA'))
            .w_PNSERIAL = .w_Zoompn.getVar('PNSERIAL')
            .w_PNNUMPRO = .w_Zoompn.getVar('PNNUMPRO')
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
            .w_PNTIPCLF = .w_Zoompn.getVar('PNTIPCLF')
            .w_PNCODCLF = .w_Zoompn.getVar('PNCODCLF')
            .w_PNANNDOC = .w_Zoompn.getVar('PNANNDOC')
            .w_PNPRD = .w_ZOOMPN.GETVAR('PNPRD')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMPN.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_52.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
    endwith
  return

  proc Calculate_JROJMCSSXL()
    with this
          * --- W_selezi changed
          GSCG_BEM(this;
              ,'S';
             )
    endwith
  endproc
  proc Calculate_RBWCVUXNNO()
    with this
          * --- Aggiorna editabilit� bottone
          GSCG_BEM(this;
              ,'U';
             )
    endwith
  endproc
  proc Calculate_AOIQGUKGCT()
    with this
          * --- Esegui
          gscg_bem(this;
              ,'E';
             )
    endwith
  endproc
  proc Calculate_RAYKTBMKEN()
    with this
          * --- Apri
          gscg_bem(this;
              ,'A';
             )
    endwith
  endproc
  proc Calculate_WSFOZDQAYU()
    with this
          * --- Aggiorna
          gscg_bem(this;
              ,'O';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_27.enabled = this.oPgFrm.Page1.oPag.oBtn_1_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_54.enabled = this.oPgFrm.Page1.oPag.oBtn_1_54.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_70.enabled = this.oPgFrm.Page1.oPag.oBtn_1_70.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oNum1_1_4.visible=!this.oPgFrm.Page1.oPag.oNum1_1_4.mHide()
    this.oPgFrm.Page1.oPag.oNum1_1_5.visible=!this.oPgFrm.Page1.oPag.oNum1_1_5.mHide()
    this.oPgFrm.Page1.oPag.oADOINI_1_6.visible=!this.oPgFrm.Page1.oPag.oADOINI_1_6.mHide()
    this.oPgFrm.Page1.oPag.oADOINI_1_7.visible=!this.oPgFrm.Page1.oPag.oADOINI_1_7.mHide()
    this.oPgFrm.Page1.oPag.oNum2_1_8.visible=!this.oPgFrm.Page1.oPag.oNum2_1_8.mHide()
    this.oPgFrm.Page1.oPag.oNum2_1_9.visible=!this.oPgFrm.Page1.oPag.oNum2_1_9.mHide()
    this.oPgFrm.Page1.oPag.oADOFIN_1_10.visible=!this.oPgFrm.Page1.oPag.oADOFIN_1_10.mHide()
    this.oPgFrm.Page1.oPag.oADOFIN_1_11.visible=!this.oPgFrm.Page1.oPag.oADOFIN_1_11.mHide()
    this.oPgFrm.Page1.oPag.oProt1_1_12.visible=!this.oPgFrm.Page1.oPag.oProt1_1_12.mHide()
    this.oPgFrm.Page1.oPag.oProt1_1_13.visible=!this.oPgFrm.Page1.oPag.oProt1_1_13.mHide()
    this.oPgFrm.Page1.oPag.oAPRINI_1_14.visible=!this.oPgFrm.Page1.oPag.oAPRINI_1_14.mHide()
    this.oPgFrm.Page1.oPag.oAPRINI_1_15.visible=!this.oPgFrm.Page1.oPag.oAPRINI_1_15.mHide()
    this.oPgFrm.Page1.oPag.oProt2_1_16.visible=!this.oPgFrm.Page1.oPag.oProt2_1_16.mHide()
    this.oPgFrm.Page1.oPag.oProt2_1_17.visible=!this.oPgFrm.Page1.oPag.oProt2_1_17.mHide()
    this.oPgFrm.Page1.oPag.oAPRFIN_1_18.visible=!this.oPgFrm.Page1.oPag.oAPRFIN_1_18.mHide()
    this.oPgFrm.Page1.oPag.oAPRFIN_1_19.visible=!this.oPgFrm.Page1.oPag.oAPRFIN_1_19.mHide()
    this.oPgFrm.Page1.oPag.oPROVVI_1_21.visible=!this.oPgFrm.Page1.oPag.oPROVVI_1_21.mHide()
    this.oPgFrm.Page1.oPag.oSELEZI_1_24.visible=!this.oPgFrm.Page1.oPag.oSELEZI_1_24.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_25.visible=!this.oPgFrm.Page1.oPag.oBtn_1_25.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_26.visible=!this.oPgFrm.Page1.oPag.oBtn_1_26.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_27.visible=!this.oPgFrm.Page1.oPag.oBtn_1_27.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_28.visible=!this.oPgFrm.Page1.oPag.oBtn_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_53.visible=!this.oPgFrm.Page1.oPag.oStr_1_53.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_54.visible=!this.oPgFrm.Page1.oPag.oBtn_1_54.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_55.visible=!this.oPgFrm.Page1.oPag.oBtn_1_55.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_56.visible=!this.oPgFrm.Page1.oPag.oBtn_1_56.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_57.visible=!this.oPgFrm.Page1.oPag.oBtn_1_57.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_59.visible=!this.oPgFrm.Page1.oPag.oStr_1_59.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_60.visible=!this.oPgFrm.Page1.oPag.oStr_1_60.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_61.visible=!this.oPgFrm.Page1.oPag.oStr_1_61.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_70.visible=!this.oPgFrm.Page1.oPag.oBtn_1_70.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMPN.Event(cEvent)
        if lower(cEvent)==lower("w_zoompn row checked") or lower(cEvent)==lower("w_zoompn row unchecked") or lower(cEvent)==lower("zoompn menucheck")
          .Calculate_RBWCVUXNNO()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Esegui")
          .Calculate_AOIQGUKGCT()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Apri")
          .Calculate_RAYKTBMKEN()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_52.Event(cEvent)
        if lower(cEvent)==lower("Aggiorna")
          .Calculate_WSFOZDQAYU()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_80.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCAU
  func Link_1_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CODCAU)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CODCAU))
          select CCCODICE,CCDESCRI,CCDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAU)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CODCAU)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CODCAU)+"%");

            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCAU) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCODCAU_1_20'),i_cWhere,'',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CODCAU)
            select CCCODICE,CCDESCRI,CCDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_CCDESCRI = NVL(_Link_.CCDESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAU = space(5)
      endif
      this.w_CCDESCRI = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile inesistente o obsoleta")
        endif
        this.w_CODCAU = space(5)
        this.w_CCDESCRI = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUPNSEL
  func Link_1_67(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUPNSEL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUPNSEL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCFLPPRO";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUPNSEL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUPNSEL)
            select CCCODICE,CCFLPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUPNSEL = NVL(_Link_.CCCODICE,space(5))
      this.w_FLPPRO = NVL(_Link_.CCFLPPRO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUPNSEL = space(5)
      endif
      this.w_FLPPRO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUPNSEL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.odata1_1_2.value==this.w_data1)
      this.oPgFrm.Page1.oPag.odata1_1_2.value=this.w_data1
    endif
    if not(this.oPgFrm.Page1.oPag.odata2_1_3.value==this.w_data2)
      this.oPgFrm.Page1.oPag.odata2_1_3.value=this.w_data2
    endif
    if not(this.oPgFrm.Page1.oPag.oNum1_1_4.value==this.w_Num1)
      this.oPgFrm.Page1.oPag.oNum1_1_4.value=this.w_Num1
    endif
    if not(this.oPgFrm.Page1.oPag.oNum1_1_5.value==this.w_Num1)
      this.oPgFrm.Page1.oPag.oNum1_1_5.value=this.w_Num1
    endif
    if not(this.oPgFrm.Page1.oPag.oADOINI_1_6.value==this.w_ADOINI)
      this.oPgFrm.Page1.oPag.oADOINI_1_6.value=this.w_ADOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oADOINI_1_7.value==this.w_ADOINI)
      this.oPgFrm.Page1.oPag.oADOINI_1_7.value=this.w_ADOINI
    endif
    if not(this.oPgFrm.Page1.oPag.oNum2_1_8.value==this.w_Num2)
      this.oPgFrm.Page1.oPag.oNum2_1_8.value=this.w_Num2
    endif
    if not(this.oPgFrm.Page1.oPag.oNum2_1_9.value==this.w_Num2)
      this.oPgFrm.Page1.oPag.oNum2_1_9.value=this.w_Num2
    endif
    if not(this.oPgFrm.Page1.oPag.oADOFIN_1_10.value==this.w_ADOFIN)
      this.oPgFrm.Page1.oPag.oADOFIN_1_10.value=this.w_ADOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oADOFIN_1_11.value==this.w_ADOFIN)
      this.oPgFrm.Page1.oPag.oADOFIN_1_11.value=this.w_ADOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oProt1_1_12.value==this.w_Prot1)
      this.oPgFrm.Page1.oPag.oProt1_1_12.value=this.w_Prot1
    endif
    if not(this.oPgFrm.Page1.oPag.oProt1_1_13.value==this.w_Prot1)
      this.oPgFrm.Page1.oPag.oProt1_1_13.value=this.w_Prot1
    endif
    if not(this.oPgFrm.Page1.oPag.oAPRINI_1_14.value==this.w_APRINI)
      this.oPgFrm.Page1.oPag.oAPRINI_1_14.value=this.w_APRINI
    endif
    if not(this.oPgFrm.Page1.oPag.oAPRINI_1_15.value==this.w_APRINI)
      this.oPgFrm.Page1.oPag.oAPRINI_1_15.value=this.w_APRINI
    endif
    if not(this.oPgFrm.Page1.oPag.oProt2_1_16.value==this.w_Prot2)
      this.oPgFrm.Page1.oPag.oProt2_1_16.value=this.w_Prot2
    endif
    if not(this.oPgFrm.Page1.oPag.oProt2_1_17.value==this.w_Prot2)
      this.oPgFrm.Page1.oPag.oProt2_1_17.value=this.w_Prot2
    endif
    if not(this.oPgFrm.Page1.oPag.oAPRFIN_1_18.value==this.w_APRFIN)
      this.oPgFrm.Page1.oPag.oAPRFIN_1_18.value=this.w_APRFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oAPRFIN_1_19.value==this.w_APRFIN)
      this.oPgFrm.Page1.oPag.oAPRFIN_1_19.value=this.w_APRFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAU_1_20.value==this.w_CODCAU)
      this.oPgFrm.Page1.oPag.oCODCAU_1_20.value=this.w_CODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVVI_1_21.RadioValue()==this.w_PROVVI)
      this.oPgFrm.Page1.oPag.oPROVVI_1_21.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_24.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCRI_1_42.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oCCDESCRI_1_42.value=this.w_CCDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oMSG_2_1.value==this.w_MSG)
      this.oPgFrm.Page2.oPag.oMSG_2_1.value=this.w_MSG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_data1)) or not(empty(.w_DATA2) OR .w_DATA1<=.w_DATA2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odata1_1_2.SetFocus()
            i_bnoObbl = !empty(.w_data1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quella finale")
          case   ((empty(.w_data2)) or not(empty(.w_DATA1) OR .w_DATA1<=.w_DATA2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.odata2_1_3.SetFocus()
            i_bnoObbl = !empty(.w_data2)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � maggiore di quella finale")
          case   not(empty(.w_NUM2) OR .w_NUM1<=.w_NUM2)  and not(not(isahe() or isapa()))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNum1_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero documento inizio selezione maggiore del numero documento fine selezione")
          case   not(empty(.w_NUM2) OR .w_NUM1<=.w_NUM2)  and not(isahe() or isapa())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNum1_1_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero documento inizio selezione maggiore del numero documento fine selezione")
          case   not(empty(.w_NUM2) OR .w_NUM1<=.w_NUM2)  and not(not(isahe() or isapa()))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNum2_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero documento inizio selezione maggiore del numero documento fine selezione")
          case   not(empty(.w_NUM2) OR .w_NUM1<=.w_NUM2)  and not(isahe() or isapa())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNum2_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero documento inizio selezione maggiore del numero documento fine selezione")
          case   not(empty(.w_PROT2) OR .w_PROT1<=.w_PROT2)  and not(not(isahe() or isapa()))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oProt1_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero protocollo inizio selezione maggiore del numero protocollo fine selezione")
          case   not(empty(.w_PROT2) OR .w_PROT1<=.w_PROT2)  and not(isahe() or isapa())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oProt1_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero protocollo inizio selezione maggiore del numero protocollo fine selezione")
          case   not(empty(.w_PROT2) OR .w_PROT1<=.w_PROT2)  and not(not(isahe() or isapa()))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oProt2_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero protocollo inizio selezione maggiore del numero protocollo fine selezione")
          case   not(empty(.w_PROT2) OR .w_PROT1<=.w_PROT2)  and not(isahe() or isapa())
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oProt2_1_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero protocollo inizio selezione maggiore del numero protocollo fine selezione")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_CODCAU))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCAU_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile inesistente o obsoleta")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SELEZI = this.w_SELEZI
    return

enddefine

* --- Define pages as container
define class tgscg_kesPag1 as StdContainer
  Width  = 781
  height = 440
  stdWidth  = 781
  stdheight = 440
  resizeXpos=354
  resizeYpos=243
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object odata1_1_2 as StdField with uid="DEKBXPXYJK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_data1", cQueryName = "data1",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quella finale",;
    ToolTipText = "Data di registrazione di inizio selezione",;
    HelpContextID = 7437770,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=76, Top=12

  func odata1_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATA2) OR .w_DATA1<=.w_DATA2)
    endwith
    return bRes
  endfunc

  add object odata2_1_3 as StdField with uid="HVXAZNFCET",rtseq=3,rtrep=.f.,;
    cFormVar = "w_data2", cQueryName = "data2",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � maggiore di quella finale",;
    ToolTipText = "Data di registrazione di fine selezione",;
    HelpContextID = 6389194,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=77, Top=42

  func odata2_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATA1) OR .w_DATA1<=.w_DATA2)
    endwith
    return bRes
  endfunc

  add object oNum1_1_4 as StdField with uid="LCUSLCZHHO",rtseq=4,rtrep=.f.,;
    cFormVar = "w_Num1", cQueryName = "Num1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero documento inizio selezione maggiore del numero documento fine selezione",;
    ToolTipText = "Filtro su numero documento di inizio selezione",;
    HelpContextID = 61987626,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=248, Top=12, cSayPict='"999999"', cGetPict='"999999"'

  func oNum1_1_4.mHide()
    with this.Parent.oContained
      return (not(isahe() or isapa()))
    endwith
  endfunc

  func oNum1_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_NUM2) OR .w_NUM1<=.w_NUM2)
    endwith
    return bRes
  endfunc

  add object oNum1_1_5 as StdField with uid="LJLMXUGZZL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_Num1", cQueryName = "Num1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero documento inizio selezione maggiore del numero documento fine selezione",;
    ToolTipText = "Filtro su numero documento di inizio selezione",;
    HelpContextID = 61987626,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=248, Top=12, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNum1_1_5.mHide()
    with this.Parent.oContained
      return (isahe() or isapa())
    endwith
  endfunc

  func oNum1_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_NUM2) OR .w_NUM1<=.w_NUM2)
    endwith
    return bRes
  endfunc

  add object oADOINI_1_6 as StdField with uid="AZKJOLPMKX",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ADOINI", cQueryName = "ADOINI",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di inizio selezione",;
    HelpContextID = 96201978,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=318, Top=12, InputMask=replicate('X',2)

  func oADOINI_1_6.mHide()
    with this.Parent.oContained
      return (not(isahe() or isapa()))
    endwith
  endfunc

  add object oADOINI_1_7 as StdField with uid="DKJPIINLLV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ADOINI", cQueryName = "ADOINI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di inizio selezione",;
    HelpContextID = 96201978,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=383, Top=12, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oADOINI_1_7.mHide()
    with this.Parent.oContained
      return (isahe() or isapa())
    endwith
  endfunc

  add object oNum2_1_8 as StdField with uid="FWTWIHVSOM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_Num2", cQueryName = "Num2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero documento inizio selezione maggiore del numero documento fine selezione",;
    ToolTipText = "Filtro su numero documento di fine selezione",;
    HelpContextID = 61922090,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=248, Top=42, cSayPict='"999999"', cGetPict='"999999"'

  func oNum2_1_8.mHide()
    with this.Parent.oContained
      return (not(isahe() or isapa()))
    endwith
  endfunc

  func oNum2_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_NUM2) OR .w_NUM1<=.w_NUM2)
    endwith
    return bRes
  endfunc

  add object oNum2_1_9 as StdField with uid="WMYUUGXVZZ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_Num2", cQueryName = "Num2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero documento inizio selezione maggiore del numero documento fine selezione",;
    ToolTipText = "Filtro su numero documento di fine selezione",;
    HelpContextID = 61922090,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=248, Top=42, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oNum2_1_9.mHide()
    with this.Parent.oContained
      return (isahe() or isapa())
    endwith
  endfunc

  func oNum2_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_NUM2) OR .w_NUM1<=.w_NUM2)
    endwith
    return bRes
  endfunc

  add object oADOFIN_1_10 as StdField with uid="RFMZIHQEWC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_ADOFIN", cQueryName = "ADOFIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di fine selezione",;
    HelpContextID = 17755386,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=318, Top=42, InputMask=replicate('X',2)

  func oADOFIN_1_10.mHide()
    with this.Parent.oContained
      return (not(isahe() or isapa()))
    endwith
  endfunc

  add object oADOFIN_1_11 as StdField with uid="XKYYAFRUSA",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ADOFIN", cQueryName = "ADOFIN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di fine selezione",;
    HelpContextID = 17755386,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=383, Top=42, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oADOFIN_1_11.mHide()
    with this.Parent.oContained
      return (isahe() or isapa())
    endwith
  endfunc

  add object oProt1_1_12 as StdField with uid="NLJBOEFQRG",rtseq=12,rtrep=.f.,;
    cFormVar = "w_Prot1", cQueryName = "Prot1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero protocollo inizio selezione maggiore del numero protocollo fine selezione",;
    ToolTipText = "Filtro su numero protocollo di inizio selezione",;
    HelpContextID = 6209034,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=563, Top=12, cSayPict='"999999"', cGetPict='"999999"'

  func oProt1_1_12.mHide()
    with this.Parent.oContained
      return (not(isahe() or isapa()))
    endwith
  endfunc

  func oProt1_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_PROT2) OR .w_PROT1<=.w_PROT2)
    endwith
    return bRes
  endfunc

  add object oProt1_1_13 as StdField with uid="TUWVAKNBNR",rtseq=13,rtrep=.f.,;
    cFormVar = "w_Prot1", cQueryName = "Prot1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero protocollo inizio selezione maggiore del numero protocollo fine selezione",;
    ToolTipText = "Filtro su numero protocollo di inizio selezione",;
    HelpContextID = 6209034,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=563, Top=12, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oProt1_1_13.mHide()
    with this.Parent.oContained
      return (isahe() or isapa())
    endwith
  endfunc

  func oProt1_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_PROT2) OR .w_PROT1<=.w_PROT2)
    endwith
    return bRes
  endfunc

  add object oAPRINI_1_14 as StdField with uid="FOEPMXNWBU",rtseq=14,rtrep=.f.,;
    cFormVar = "w_APRINI", cQueryName = "APRINI",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Serie protocollo di inizio selezione",;
    HelpContextID = 96186618,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=633, Top=12, InputMask=replicate('X',2)

  func oAPRINI_1_14.mHide()
    with this.Parent.oContained
      return (not(isahe() or isapa()))
    endwith
  endfunc

  add object oAPRINI_1_15 as StdField with uid="IMQOTQOTSH",rtseq=15,rtrep=.f.,;
    cFormVar = "w_APRINI", cQueryName = "APRINI",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie protocollo di inizio selezione",;
    HelpContextID = 96186618,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=697, Top=12, InputMask=replicate('X',10)

  func oAPRINI_1_15.mHide()
    with this.Parent.oContained
      return (isahe() or isapa())
    endwith
  endfunc

  add object oProt2_1_16 as StdField with uid="ZNWTLOBPER",rtseq=16,rtrep=.f.,;
    cFormVar = "w_Prot2", cQueryName = "Prot2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero protocollo inizio selezione maggiore del numero protocollo fine selezione",;
    ToolTipText = "Filtro su numero protocollo di fine selezione",;
    HelpContextID = 5160458,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=563, Top=42, cSayPict='"999999"', cGetPict='"999999"'

  func oProt2_1_16.mHide()
    with this.Parent.oContained
      return (not(isahe() or isapa()))
    endwith
  endfunc

  func oProt2_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_PROT2) OR .w_PROT1<=.w_PROT2)
    endwith
    return bRes
  endfunc

  add object oProt2_1_17 as StdField with uid="QRWLMGQTLH",rtseq=17,rtrep=.f.,;
    cFormVar = "w_Prot2", cQueryName = "Prot2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero protocollo inizio selezione maggiore del numero protocollo fine selezione",;
    ToolTipText = "Filtro su numero protocollo di fine selezione",;
    HelpContextID = 5160458,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=563, Top=42, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oProt2_1_17.mHide()
    with this.Parent.oContained
      return (isahe() or isapa())
    endwith
  endfunc

  func oProt2_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_PROT2) OR .w_PROT1<=.w_PROT2)
    endwith
    return bRes
  endfunc

  add object oAPRFIN_1_18 as StdField with uid="ICQCTZJCPE",rtseq=18,rtrep=.f.,;
    cFormVar = "w_APRFIN", cQueryName = "APRFIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Serie protocollo di fine selezione",;
    HelpContextID = 17740026,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=633, Top=42, InputMask=replicate('X',2)

  func oAPRFIN_1_18.mHide()
    with this.Parent.oContained
      return (not(isahe() or isapa()))
    endwith
  endfunc

  add object oAPRFIN_1_19 as StdField with uid="ZIVWFRZGLR",rtseq=19,rtrep=.f.,;
    cFormVar = "w_APRFIN", cQueryName = "APRFIN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie protocollo di fine selezione",;
    HelpContextID = 17740026,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=697, Top=42, InputMask=replicate('X',10)

  func oAPRFIN_1_19.mHide()
    with this.Parent.oContained
      return (isahe() or isapa())
    endwith
  endfunc

  add object oCODCAU_1_20 as StdField with uid="UQXCFUOJXN",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CODCAU", cQueryName = "CODCAU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile inesistente o obsoleta",;
    ToolTipText = "Filtro su causale contabile",;
    HelpContextID = 177377754,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=248, Top=93, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", oKey_1_1="CCCODICE", oKey_1_2="this.w_CODCAU"

  func oCODCAU_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAU_1_20.ecpDrop(oSource)
    this.Parent.oContained.link_1_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAU_1_20.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCODCAU_1_20'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali contabili",'',this.parent.oContained
  endproc


  add object oPROVVI_1_21 as StdCombo with uid="EMLDBOXNIT",value=3,rtseq=21,rtrep=.f.,left=630,top=95,width=87,height=21;
    , ToolTipText = "Stato dei movimenti da visualizzare";
    , HelpContextID = 86957578;
    , cFormVar="w_PROVVI",RowSource=""+"Confermati,"+"Provvisori,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROVVI_1_21.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,' ',;
    ' '))))
  endfunc
  func oPROVVI_1_21.GetRadio()
    this.Parent.oContained.w_PROVVI = this.RadioValue()
    return .t.
  endfunc

  func oPROVVI_1_21.SetRadio()
    this.Parent.oContained.w_PROVVI=trim(this.Parent.oContained.w_PROVVI)
    this.value = ;
      iif(this.Parent.oContained.w_PROVVI=='N',1,;
      iif(this.Parent.oContained.w_PROVVI=='S',2,;
      iif(this.Parent.oContained.w_PROVVI=='',3,;
      0)))
  endfunc

  func oPROVVI_1_21.mHide()
    with this.Parent.oContained
      return (! empty(.w_cOper) )
    endwith
  endfunc


  add object oBtn_1_22 as StdButton with uid="WRBRDJQKOO",left=729, top=70, width=48,height=45,;
    CpPicture="bmp\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la ricerca delle registrazioni contabili";
    , HelpContextID = 187486585;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        GSCG_BEM(this.Parent.oContained,"I")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZOOMPN as cp_szoombox with uid="POUUEXYKLG",left=7, top=119, width=770,height=260,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="PNT_MAST",bAdvOptions=.f.,bOptions=.f.,bQueryOnLoad=.f.,bReadOnly=.f.,bQueryondblClick=.f.,cZoomFile="GSCG_KES",cMenuFile="",cZoomOnZoom="",;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 112320998

  add object oSELEZI_1_24 as StdRadio with uid="RDFUYVXOXW",rtseq=22,rtrep=.f.,left=18, top=393, width=129,height=32;
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_24.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 83892954
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 83892954
      this.Buttons(2).Top=15
      this.SetAll("Width",127)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oSELEZI_1_24.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_24.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_24.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc

  func oSELEZI_1_24.mHide()
    with this.Parent.oContained
      return (isahe() or isapa())
    endwith
  endfunc


  add object oBtn_1_25 as StdButton with uid="SDBOSTGGOV",left=567, top=391, width=48,height=45,;
    CpPicture="bmp\Elabora.ico", caption="", nPag=1;
    , ToolTipText = "Premere per inserire/modificare il numero protocollo nella registrazione selezionata";
    , HelpContextID = 187486585;
    , tabStop=.f., caption='Num.\<Prot.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      do GSCG_KPP with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TESTSEL#'S' and NOT .w_FLPPRO $ ' N' and .w_PNTIPREG = 'A')
      endwith
    endif
  endfunc

  func oBtn_1_25.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_cOper) OR isahe())
     endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="BNPKPPRNCC",left=621, top=391, width=48,height=45,;
    CpPicture="bmp\VERIFICA.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza la registrazione selezionata";
    , HelpContextID = 187486585;
    , tabStop=.f., Caption='Re\<g.Cont';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_SERIALE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc

  func oBtn_1_26.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (isahe() or isapa())
     endwith
    endif
  endfunc


  add object oBtn_1_27 as StdButton with uid="BYKGVIACKT",left=675, top=391, width=48,height=45,;
    CpPicture="bmp\OK.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per eliminare le registrazioni selezionate";
    , HelpContextID = 187486585;
    , tabStop=.f., caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_27.Click()
      with this.Parent.oContained
        .NotifyEvent('Esegui')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_27.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TESTSEL='S')
      endwith
    endif
  endfunc

  func oBtn_1_27.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (not empty(.w_cOper))
     endwith
    endif
  endfunc


  add object oBtn_1_28 as StdButton with uid="MNWKNLEHCQ",left=675, top=391, width=48,height=45,;
    CpPicture="bmp\OK.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare lo stato alle registrazioni selezionate";
    , HelpContextID = 187486585;
    , tabStop=.f., caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      with this.Parent.oContained
        .NotifyEvent('Aggiorna')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_28.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TESTSEL='S')
      endwith
    endif
  endfunc

  func oBtn_1_28.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_cOper))
     endwith
    endif
  endfunc


  add object oBtn_1_29 as StdButton with uid="FRSAGQFIZA",left=729, top=391, width=48,height=45,;
    CpPicture="bmp\ESC.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 58359226;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCCDESCRI_1_42 as StdField with uid="IKCTSTIVVW",rtseq=28,rtrep=.f.,;
    cFormVar = "w_CCDESCRI", cQueryName = "CCDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 76505711,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=315, Top=93, InputMask=replicate('X',35)


  add object oObj_1_52 as cp_runprogram with uid="CDCTMDAAOJ",left=-5, top=490, width=324,height=20,;
    caption='GSAR_BZP(w_SERIALE )',;
   bGlobalFont=.t.,;
    prg="GSAR_BZP(w_SERIALE)",;
    cEvent = "w_zoompn selected",;
    nPag=1;
    , HelpContextID = 187758135


  add object oBtn_1_54 as StdButton with uid="VMPKZGCRSK",left=174, top=391, width=48,height=45,;
    CpPicture="bmp\VERIFICA.BMP", caption="", nPag=1;
    , ToolTipText = "Visualizza la registrazione selezionata";
    , HelpContextID = 187486585;
    , tabStop=.f., Caption='Re\<g.Cont';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_54.Click()
      with this.Parent.oContained
        GSAR_BZP(this.Parent.oContained,.w_SERIALE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_54.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc

  func oBtn_1_54.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (not(isahe() or isapa()))
     endwith
    endif
  endfunc


  add object oBtn_1_55 as StdButton with uid="GQMUPTTTCY",left=15, top=391, width=48,height=45,;
    CpPicture="bmp\Check.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare i movimenti provvisori da eliminare";
    , HelpContextID = 25269270;
    , caption='\<Seleziona';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_55.Click()
      with this.Parent.oContained
        GSCG_BEM(this.Parent.oContained,"S")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_55.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (not(isahe() or isapa()))
     endwith
    endif
  endfunc


  add object oBtn_1_56 as StdButton with uid="ZCYHLDNLVF",left=68, top=391, width=48,height=45,;
    CpPicture="bmp\UnCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare i movimenti provvisori da eliminare";
    , HelpContextID = 25269270;
    , caption='\<Deselez.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_56.Click()
      with this.Parent.oContained
        GSCG_BEM(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_56.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (not(isahe() or isapa()))
     endwith
    endif
  endfunc


  add object oBtn_1_57 as StdButton with uid="GUQBLQMZBT",left=121, top=391, width=48,height=45,;
    CpPicture="bmp\InvCheck.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per invertire la selezione dei movimenti provvisori da eliminare";
    , HelpContextID = 25269270;
    , caption='\<Inv. Sel.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_57.Click()
      with this.Parent.oContained
        GSCG_BEM(this.Parent.oContained,"V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_57.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (not(isahe() or isapa()))
     endwith
    endif
  endfunc


  add object oBtn_1_70 as StdButton with uid="QHQULYLDXZ",left=567, top=391, width=48,height=45,;
    CpPicture="bmp\ModProt.bmp", caption="", nPag=1;
    , ToolTipText = "Conferma e modifica riferimento protocollo";
    , HelpContextID = 80342518;
    , Caption='\<Mod.Prot.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_70.Click()
      with this.Parent.oContained
        GSCG_BMS(this.Parent.oContained,"CNT")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_70.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE) AND .w_NUMPRO<>0 )
      endwith
    endif
  endfunc

  func oBtn_1_70.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return ((.w_cOper=' ') OR (not(isahe() OR isapa())))
     endwith
    endif
  endfunc


  add object oObj_1_80 as cp_runprogram with uid="ALVRXEICLG",left=-5, top=512, width=432,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSCG_BEM('O')",;
    cEvent = "ModificaProtocollo",;
    nPag=1;
    , HelpContextID = 112320998

  add object oStr_1_39 as StdString with uid="CJGNLZBPIF",Visible=.t., Left=140, Top=95,;
    Alignment=1, Width=103, Height=18,;
    Caption="Causale contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="EHJUESFLSW",Visible=.t., Left=7, Top=14,;
    Alignment=1, Width=66, Height=15,;
    Caption="Da data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="ERPMHZUMFE",Visible=.t., Left=11, Top=43,;
    Alignment=1, Width=63, Height=15,;
    Caption="A data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="XGNDWJRANV",Visible=.t., Left=166, Top=14,;
    Alignment=1, Width=77, Height=18,;
    Caption="Da n� doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="RRUYWXTHWC",Visible=.t., Left=176, Top=43,;
    Alignment=1, Width=67, Height=18,;
    Caption="A n� doc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="FTODWOQQEB",Visible=.t., Left=332, Top=392,;
    Alignment=0, Width=156, Height=18,;
    Caption="Movimenti provvisori"    , forecolor=RGB(255,0,0);
  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="WTZQXEEAYG",Visible=.t., Left=306, Top=17,;
    Alignment=0, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (not(isahe() or isapa()))
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="YPFGQZYRHM",Visible=.t., Left=306, Top=46,;
    Alignment=0, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (not(isahe() or isapa()))
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="SIMAYBOYBH",Visible=.t., Left=479, Top=14,;
    Alignment=1, Width=76, Height=18,;
    Caption="Da n� prot.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="FMZWRURNIN",Visible=.t., Left=488, Top=43,;
    Alignment=1, Width=67, Height=18,;
    Caption="A n� prot.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="LTFZMCVEYG",Visible=.t., Left=624, Top=17,;
    Alignment=0, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (not(isahe() or isapa()))
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="OYBQFPIDJE",Visible=.t., Left=624, Top=46,;
    Alignment=0, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (not(isahe() or isapa()))
    endwith
  endfunc

  add object oStr_1_53 as StdString with uid="JTOWJYRJRT",Visible=.t., Left=581, Top=98,;
    Alignment=1, Width=44, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  func oStr_1_53.mHide()
    with this.Parent.oContained
      return (! empty(.w_cOper))
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="XPFCSELDOD",Visible=.t., Left=372, Top=17,;
    Alignment=0, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (isahe() or isapa())
    endwith
  endfunc

  add object oStr_1_59 as StdString with uid="LEHZLXGYCH",Visible=.t., Left=372, Top=46,;
    Alignment=0, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_59.mHide()
    with this.Parent.oContained
      return (isahe() or isapa())
    endwith
  endfunc

  add object oStr_1_60 as StdString with uid="YNTMDEOQUV",Visible=.t., Left=685, Top=17,;
    Alignment=0, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_60.mHide()
    with this.Parent.oContained
      return (isahe() or isapa())
    endwith
  endfunc

  add object oStr_1_61 as StdString with uid="WHRAFYNRSE",Visible=.t., Left=685, Top=49,;
    Alignment=0, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_61.mHide()
    with this.Parent.oContained
      return (isahe() or isapa())
    endwith
  endfunc
enddefine
define class tgscg_kesPag2 as StdContainer
  Width  = 781
  height = 440
  stdWidth  = 781
  stdheight = 440
  resizeXpos=326
  resizeYpos=220
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMSG_2_1 as StdMemo with uid="SDVKETLQKB",rtseq=29,rtrep=.f.,;
    cFormVar = "w_MSG", cQueryName = "MSG",;
    bObbl = .f. , nPag = 2, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 65363258,;
   bGlobalFont=.t.,;
    Height=402, Width=655, Left=7, Top=16, tabstop = .f., readonly = .t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kes','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
