* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsor_btd                                                        *
*              Tracciabilita doc                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_42]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-21                                                      *
* Last revis.: 2014-03-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParams
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsor_btd",oParentObject,m.pParams)
return(i_retval)

define class tgsor_btd as StdBatch
  * --- Local variables
  pParams = space(1)
  w_OFSERIALE = space(10)
  w_ZOOM = space(10)
  w_PROG = .NULL.
  w_Ret = 0
  w_STATO = space(1)
  w_CONTA = 0
  w_ARRDIM = 0
  * --- WorkFile variables
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Il parametro � utilizato per 'R' - ricerca doc. collegati, 'Z' lancio dello Zoom documenti - 'D' dettaglio
    do case
      case this.pParams="R"
        * --- Calcolo il grafo dei Documenti Collegati a quello selezionato
        * --- Se vuoto il seriale verifico se i dati impostati identificano un documento
        if EMPTY(this.oParentObject.w_MVSERIAL)
          * --- La ricerca parte solo se non � stata attivata tramite pressione del flag
          if this.oPARENTOBJECT.currentEvent = "w_LEGIND Changed" or this.oPARENTOBJECT.currentEvent = "w_DOCESP Changed"
            i_retcode = 'stop'
            return
          endif
          * --- La ricerca avviene per Causale numero alfa e data (chiave candidata), se pi� di uno prende l'ultimo
          if this.oParentObject.w_VSRIF="S"
            * --- Ricerco per vostro riferimento
            * --- Select from DOC_MAST
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select MVSERIAL  from "+i_cTable+" DOC_MAST ";
                  +" where MVNUMEST="+cp_ToStrODBC(this.oParentObject.w_NUMRIF)+" AND MVALFEST="+cp_ToStrODBC(this.oParentObject.w_ALFRIF)+" AND MVDATEST="+cp_ToStrODBC(this.oParentObject.w_DATRIF)+" and MVTIPDOC="+cp_ToStrODBC(this.oParentObject.w_CAUDOC)+"";
                   ,"_Curs_DOC_MAST")
            else
              select MVSERIAL from (i_cTable);
               where MVNUMEST=this.oParentObject.w_NUMRIF AND MVALFEST=this.oParentObject.w_ALFRIF AND MVDATEST=this.oParentObject.w_DATRIF and MVTIPDOC=this.oParentObject.w_CAUDOC;
                into cursor _Curs_DOC_MAST
            endif
            if used('_Curs_DOC_MAST')
              select _Curs_DOC_MAST
              locate for 1=1
              do while not(eof())
              this.oParentObject.w_MVSERIAL = _Curs_DOC_MAST.MVSERIAL
                select _Curs_DOC_MAST
                continue
              enddo
              use
            endif
          else
            * --- Select from DOC_MAST
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select MVSERIAL  from "+i_cTable+" DOC_MAST ";
                  +" where MVNUMDOC="+cp_ToStrODBC(this.oParentObject.w_NUMDOC)+" AND MVALFDOC="+cp_ToStrODBC(this.oParentObject.w_ALFDOC)+" AND MVDATDOC="+cp_ToStrODBC(this.oParentObject.w_DATDOC)+" and MVTIPDOC="+cp_ToStrODBC(this.oParentObject.w_CAUDOC)+"";
                   ,"_Curs_DOC_MAST")
            else
              select MVSERIAL from (i_cTable);
               where MVNUMDOC=this.oParentObject.w_NUMDOC AND MVALFDOC=this.oParentObject.w_ALFDOC AND MVDATDOC=this.oParentObject.w_DATDOC and MVTIPDOC=this.oParentObject.w_CAUDOC;
                into cursor _Curs_DOC_MAST
            endif
            if used('_Curs_DOC_MAST')
              select _Curs_DOC_MAST
              locate for 1=1
              do while not(eof())
              this.oParentObject.w_MVSERIAL = _Curs_DOC_MAST.MVSERIAL
                select _Curs_DOC_MAST
                continue
              enddo
              use
            endif
          endif
          * --- Se NON � AlterEgo segnala la mancata identificazione del documento
          if EMPTY(this.oParentObject.w_MVSERIAL) AND NOT g_AGEN="S"
            * --- Nessun Documento Trovato
            ah_ErrorMsg("Gli estremi imputati non identificano nessun documento",,"")
            i_retcode = 'stop'
            return
          endif
          * --- Se � AlterEgo NON segnala la mancata identificazione del documento
          if EMPTY(this.oParentObject.w_MVSERIAL) AND g_AGEN="S"
            * --- Nessun Documento Trovato
            * --- Pulisco lo Zoom
            this.w_ZOOM = this.oParentObject.w_Zoom
            SELECT ( this.w_ZOOM.cCursor )
            ZAP
            this.w_ZOOM.grd.refresh()     
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Utilizzo 4 cursori, nel primo (NODI) vado a scrivere i seriali dei documenti trovati
        * --- verr� utilizzato per evitare di richiamare 2 volte il solito documento
        * --- Il secondo (TARGET) mi conterr� i risultato da visualizzae nella maschera
        * --- Il terzo e il quarto conterranno rispettivamente i documenti generati e fonte del documento che sto esaminando
        CREATE CURSOR NODI (SERIALE C(10))
        * --- Li rendo Scrivibili
        CreaCur=WRCURSOR("NODI")
        * --- Copio il cursore da visualizzare in Target
        this.w_ZOOM = this.oParentObject.w_Zoom
        SELECT ( this.w_ZOOM.cCursor )
        * --- Copio la struttura dello Zoom nel cursore Target
        AFIELD(STRUTTURA)
        CREATE CURSOR TARGET FROM ARRAY STRUTTURA
        CreaCur=WRCURSOR("TARGET")
        * --- I cursori ORIGINI e DESTINA hanno la seguente struttura
        * --- SERIALE,ROWNUM,....
        * --- Numero di ricorsioni
        nRicors = 0
        * --- Calcolo prima i documenti generati dal documento
        this.w_Ret = GSOR_BT2( this.oParentObject.w_MVSERIAL,"F", this.oParentObject.w_DOCESP, this.oParentObject.w_LEGIND, @nRicors)
        ah_Msg("Ricerca",.T.,)
        if this.w_Ret=-1
          * --- Raggiunto numero massimo di ricorsioni ipotizzabili 100
          ah_ErrorMsg("Raggiunto numero massimo di ricorsioni. Il risultato � parziale",,"")
        endif
        * --- Calcolo prima i documenti fonte del documento
        this.w_Ret = GSOR_BT2( this.oParentObject.w_MVSERIAL,"G", this.oParentObject.w_DOCESP, this.oParentObject.w_LEGIND, @nRicors)
        if this.w_Ret=-1
          * --- Raggiunto numero massimo di ricorsioni ipotizzabili 100
          ah_ErrorMsg("Raggiunto numero massimo di ricorsioni. Il risultato � parziale",,"")
        endif
        * --- Inserico il risultato nello Zoom della maschera
        * --- Pulisco lo Zoom
        Select ( this.w_ZOOM.cCursor ) 
 ZAP
        * --- Ai documenti derivati aggiungo il documento origine
        vq_exec("QUERY\GSOR2BTD.VQR",this, "origine" )
        if ISALT()
          select * from origine where prtipcau in (this.oParentObject.w_SELPRV, this.oParentObject.w_SELBOZ, this.oParentObject.w_SELNSP, this.oParentObject.w_SELALT, this.oParentObject.w_SELPRA, this.oParentObject.w_SELPRO, this.oParentObject.w_SELFAA, this.oParentObject.w_SELFAT, this.oParentObject.w_SELNCR) union;
          select * from target where prtipcau in (this.oParentObject.w_SELPRV, this.oParentObject.w_SELBOZ, this.oParentObject.w_SELNSP, this.oParentObject.w_SELALT, this.oParentObject.w_SELPRA, this.oParentObject.w_SELPRO, this.oParentObject.w_SELFAA, this.oParentObject.w_SELFAT, this.oParentObject.w_SELNCR) into array prova
        else
          select * from origine union;
          select * from target into array prova
        endif
        * --- Se AlterEgo allora determina lo stato dell'attivit�
        if g_AGEN="S"
          * --- Se w_STATO = 'A'
          *     lo stato dell'attivit� rimane invariato
          this.w_STATO = "A"
          * --- Variabile per ciclare sull'array
          this.w_CONTA = 0
          * --- Dimensione dell'array
          if Type("prova")<>"U"
            this.w_ARRDIM = ALEN(PROVA,1)
          else
            this.w_ARRDIM = 0
          endif
          * --- Se la dimensione dell'array � maggiore di 1:
          *     allora il documento � stato (perlomeno) importato in un altro documento;
          *     quindi lo stato dell'attivit� deve diventare 'Non modificabile'
          if this.w_ARRDIM>1
            * --- Stato = Non modificabile
            this.w_STATO = "N"
          endif
          do while this.w_CONTA<this.w_ARRDIM
            this.w_CONTA = this.w_CONTA+1
            * --- Se nell'elenco dei documenti ne esiste almeno uno di tipo fattura:
            *     allora lo stato dell'Attivit� deve diventare 'Fatturata'
            * --- Il dodicesimo campo dell'array prova contiene il campo MVCLADOC
            if PROVA(this.w_CONTA,12)="FA"
              * --- Stato = Fatturata
              this.w_STATO = "U"
            endif
          enddo
          if this.w_STATO<>"A"
            if alltrim(this.oParentObject.cprg)="gsag_aat"
              this.oParentObject.w_STATOATT=this.w_STATO
            endif
          endif
        endif
        * --- Nel caso in cui premo sul bottone tracciabilit� in caricamento il cursore � vuoto
        *     La union non restituirebbe nessun risultato e l'array Prova non verrebbe creato
        if Type("prova")<>"U"
          * --- Pulisco lo Zoom
          Select ( this.w_ZOOM.cCursor ) 
 ZAP
          insert into ( this.w_ZOOM.cCursor ) from array prova
          * --- Lo faccio posizionare sul primo documento
          select ( this.w_ZOOM.cCursor )
          go top
        endif
        * --- Refresh della Griglia
        this.w_ZOOM.grd.refresh()     
      case this.pParams="Z"
        * --- Chiudo la Maschera dello Zoom Documenti
        if NOT EMPTY(this.oParentObject.w_MVSERIAL)
          WITH this.oParentObject.oParentObject
          .w_MVSERIAL = this.oParentObject.w_MVSERIAL
          .w_NUMDO1 = this.oParentObject.w_NUMDOC
          .w_ALFDO1 = this.oParentObject.w_ALFDOC
          .w_DATDO1 = CP_TODATE(this.oParentObject.w_DATDOC)
          .w_NUMRI1 = this.oParentObject.w_NUMRIF
          .w_ALFRI1 = this.oParentObject.w_ALFRIF
          .w_DATRI1 = CP_TODATE(this.oParentObject.w_DATRIF)
          .NotifyEvent("EseguoZoom")
          ENDWITH 
        endif
        this.oparentObject.ecpQuit()
        i_retcode = 'stop'
        return
      case this.pParams="D"
        * --- Dettaglio Documenti
        gsar_bzm(this,this.oParentObject.w_SERIALE,-20)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pParams="O"
        this.w_PROG = GSOF_AOF()
        * --- carico l'offerta
        this.w_PROG.w_OFSERIAL = this.w_OFSERIALE
        this.w_PROG.QueryKeySet("OFSERIAL='"+ this.oParentObject.w_OFSERIALE +"'")     
        this.w_PROG.LoadRecWarn()     
    endcase
    this.oParentObject.w_NUMDOC = 0
    this.oParentObject.w_DATDOC = cp_CharToDate("  -  -    ")
    this.oParentObject.w_NUMRIF = 0
    this.oParentObject.w_DATRIF = cp_CharToDate("  -  -    ")
    if USED ("target")
      SELECT target
      USE
    endif
    if USED ( "nodi" )
      SELECT nodi
      USE
    endif
    if USED ( "origine" )
      SELECT origine
      USE
    endif
  endproc


  proc Init(oParentObject,pParams)
    this.pParams=pParams
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_DOC_MAST')
      use in _Curs_DOC_MAST
    endif
    if used('_Curs_DOC_MAST')
      use in _Curs_DOC_MAST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParams"
endproc
