* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bmk                                                        *
*              Documenti, controlli finali (sotto transazione)                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_1265]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1998-10-05                                                      *
* Last revis.: 2017-12-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPar
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bmk",oParentObject,m.pPar)
return(i_retval)

define class tgsve_bmk as StdBatch
  * --- Local variables
  pPar = .f.
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_PADRE = .NULL.
  w_OLDIMPCOM = 0
  w_IMPCOMDIF = 0
  w_NEWIMPCOM = 0
  w_APPSALCO = 0
  w_RESERRIF = space(10)
  w_REROWRIF = 0
  w_RENUMRIF = 0
  w_REKEYSAL = space(20)
  w_RECODMAG = space(5)
  w_RECODMAT = space(5)
  w_REFLIMPE = space(1)
  w_REF2IMPE = space(1)
  w_REFLORDI = space(1)
  w_REF2ORDI = space(1)
  w_REFLRISE = space(1)
  w_REF2RISE = space(1)
  w_REQTASAL = 0
  w_RE_FLEVAS = space(1)
  w_REQTAEVA = 0
  w_REIMPEVA = 0
  w_RETIPRIG = space(1)
  w_OKGEN = .f.
  w_SOTABGES = space(10)
  w_RSFILTPK = space(254)
  w_CODUTE = 0
  w_SERDOCESP = space(10)
  w_ORECO = 0
  w_NUMREC = 0
  w_NUMRIF_V = 0
  w_NOAGG = .f.
  w_DICODIVA = space(5)
  w_DDIMPDIC = 0
  w_IMP_DICHIAR = 0
  w_NUMRIGA = 0
  w_NUMRIGA_COLL = 0
  w_NUMRIGA_OLD = 0
  w_DIDICCOL = space(15)
  w_DIIMPDIC_COLL = 0
  w_DIIMPUTI_COLL = 0
  w_DIC_COLLEGATA = .f.
  w_DIIMPUTI = 0
  w_DIIMPDIC = 0
  w_DDIMPDIC_COLL = 0
  w_DICINTE_INDIRETTA = space(10)
  w_PARAM_FILTRO = space(1)
  w_IMPORTO_ORIGINE = 0
  w_DDIMPDIC_COLL_STO = 0
  w_SERIALE_DICHIARAZIONE = space(10)
  w_IMPORTO_DI_STORNO = 0
  w_STORNO_ORIGINE = .f.
  w_RIFDIC = space(10)
  w_DIDICCOL_FILTRO = space(10)
  w_VAR_DICHIARAZIONE = .f.
  w_DIDICCOL_ORIGINE = space(15)
  w_DIIMPUTI_ORIGINE = 0
  w_DDIMPDIC_ORIGINE = 0
  w_DICINTE_INDIRETTA_ORIGINE = space(10)
  w_NUMRIGA_ORIGINE = 0
  w_NUMRIGA_COLL_ORIGINE = 0
  w_DDIMPDIC_COLL_ORIGINE = 0
  w_DIIMPUTI_COLL_ORIGINE = 0
  w_DDIMPDIC_COLL_STO_ORIGINE = 0
  w_DDVISMAN = space(1)
  w_DDTIPAGG = space(1)
  w_DDRECCOL = space(10)
  w_CLADOC = space(2)
  w_DOC_RIFDIC = .f.
  w_DOCRIG = .f.
  w_DOCROW = 0
  w_DELESP = .f.
  w_EVNUMRIF = 0
  w_TmpFido = 0
  w_DMAGCAR = space(5)
  w_DMAGSCA = space(5)
  w_REFLEVAS = space(1)
  w_REF2EVAS = space(1)
  w_FAT_RAG = .f.
  w_RQTAIMP = 0
  w_RQTAIM1 = 0
  w_OTIPDOC = space(5)
  w_OFLRISC = space(1)
  w_RESERIAL = space(10)
  w_RETIPDOC = space(5)
  w_RENUMDOC = 0
  w_REALFDOC = space(10)
  w_REDATDOC = ctod("  /  /  ")
  w_OIMPEVA_R = 0
  w_EVAFLE = space(1)
  w_IMPCOM_V = 0
  w_VALCOM = space(3)
  w_COMDECTOT = 0
  w_FLARIF_V = space(1)
  w_RDAT = ctod("  /  /  ")
  w_FLAGG = space(1)
  w_SERRIF_V = space(10)
  w_SERRIF_V_AGG = space(10)
  w_ROWRIF_V_AGG = 0
  w_IMPNAZ_V_AGG = 0
  w_ROWRIF_V = 0
  w_FGMRIF_V = space(1)
  w_IMPNAZ_V = 0
  w_LOOP = 0
  w_FLELGM_V = space(1)
  w_TmpN = 0
  w_CODVAL = space(5)
  w_LDECCOM = 0
  w_RNUM = 0
  w_RALF = space(10)
  w_SPEBOL = 0
  w_CODCOL = space(5)
  w_ANFIDO = .f.
  w_SERPDA = space(10)
  w_ROWPDA = 0
  w_CODCONF = space(3)
  w_OK = .f.
  w_QTAMOV = 0
  w_OQTAUM1 = 0
  w_RIFMAG = space(5)
  w_VALESE = space(3)
  w_MESS = space(200)
  w_OQTAEV1 = 0
  w_OCAORIF = 0
  w_RIFMAT = space(5)
  w_TIPRIG = space(1)
  w_SERIAL = space(10)
  w_OQTASAL = 0
  w_FLRISE = space(1)
  w_FID1 = 0
  w_QTAUM1 = 0
  w_RCLADOC = space(2)
  w_SERFAT = space(10)
  w_ROWORD = 0
  w_OFLEVAS = space(1)
  w_OEFFEVA = ctod("  /  /  ")
  w_F2RISE = space(1)
  w_FID2 = 0
  w_FIDS4 = 0
  w_FIDS5 = 0
  w_FIDS6 = 0
  w_NUMRIF = 0
  w_FLORDI = space(1)
  w_FID3 = 0
  w_SERRIF = space(10)
  w_RDATDOC = ctod("  /  /  ")
  w_NQTAEV1 = 0
  w_F2ORDI = space(1)
  w_FID4 = 0
  w_CAUMAG = space(5)
  w_ROWRIF = 0
  w_NQTASAL = 0
  w_FLIMPE = space(1)
  w_FID5 = 0
  w_CAUCOL = space(5)
  w_CFUNC = space(10)
  w_FLARIF = space(1)
  w_F2IMPE = space(1)
  w_FID6 = 0
  w_CODMAG = space(5)
  w_FLAPP = .f.
  w_FLERIF = space(1)
  w_SEGNALA = .f.
  w_FIDRES = 0
  w_CODMAT = space(5)
  w_FLDEL = .f.
  w_FLUPD = .f.
  w_QTAR = 0
  w_IMPNAZ = 0
  w_KEYSAL = space(20)
  w_CODCOM = space(15)
  w_CODATT = space(15)
  w_OQTAMOV = 0
  w_QTAP = 0
  w_OIMPNAZ = 0
  w_OPREZZO = 0
  w_APPSAL = 0
  w_OQTAEVA = 0
  w_NQTAEVA = 0
  w_SRV = space(1)
  w_RMAG = space(5)
  w_OIMPEVA = 0
  w_OIMPDAE = 0
  w_PNDOC = space(1)
  w_RCAS = space(1)
  w_TOTDOC = 0
  w_TOTVALFIS = 0
  w_DR = ctod("  /  /  ")
  w_NR = space(6)
  w_PREZZO = 0
  w_RQTA = 0
  w_FGMRIF = space(1)
  w_FIDD = ctod("  /  /  ")
  w_DATGEN = ctod("  /  /  ")
  w_COART = space(20)
  w_RCPR = 0
  w_EVARIF = space(1)
  w_COVAR = space(20)
  w_APPO2 = .f.
  w_DATG1 = ctod("  /  /  ")
  w_APPO = 0
  w_IMPUTI = 0
  w_DATG2 = ctod("  /  /  ")
  w_DATG = ctod("  /  /  ")
  w_APPO1 = 0
  w_DATB1 = ctod("  /  /  ")
  w_DATB = ctod("  /  /  ")
  w_DATB2 = ctod("  /  /  ")
  w_PERIVA = 0
  w_RISPERIV = 0
  w_STORNA = .f.
  w_EVCASC = space(1)
  w_OKCAMBIO = .f.
  w_NCAOVAL = 0
  w_OQTAIM1 = 0
  w_FLELGM = space(1)
  w_LFLRISE = space(1)
  w_CLADOR = space(2)
  w_IVA = 0
  w_CODIVA = space(5)
  w_SPEINC = 0
  w_SPEIMB = 0
  w_SPETRA = 0
  w_CODIVE = space(3)
  w_CLDOOR = space(2)
  w_FLGOK = space(1)
  w_IVAACC = 0
  w_SPESACC = 0
  w_IVAINC = 0
  w_IVAIMB = 0
  w_IVATRA = 0
  w_CODIVAINC = space(5)
  w_CODIVAIMB = space(5)
  w_CODIVATRA = space(5)
  w_CODVALOR = space(3)
  w_CAMOR = 0
  w_FLOMAG = space(1)
  w_STATUS = space(1)
  w_IMPEVATOT = 0
  w_INDIVA = 0
  w_INDIVE = 0
  w_FLULCA = space(1)
  w_FLULPV = space(1)
  w_FLCASC = space(1)
  w_RVALULT = 0
  w_RPREZZO = 0
  w_RVALRIG = 0
  w_RVALMAG = 0
  w_OVALRIG = 0
  w_VALRIG = 0
  w_VALMAG = 0
  w_SCONT1 = 0
  w_SCONT2 = 0
  w_SCONT3 = 0
  w_SCONT4 = 0
  w_OSCONT1 = 0
  w_OSCONT2 = 0
  w_OSCONT3 = 0
  w_OSCONT4 = 0
  w_RVALNAZ = space(3)
  w_RCODVAL = space(3)
  w_RCAOVAL = 0
  w_DOCEVA = space(10)
  w_FLVESC = .f.
  w_QTAI = 0
  w_QTAO = 0
  w_QTAIMP = 0
  w_QTAIM1 = 0
  w_FLDISC = space(1)
  w_IMPCOM = 0
  w_STIMPCOM = 0
  w_ORD_EVA = 0
  w_OLDEVAS1 = space(1)
  w_ORDFLCOCO = space(1)
  w_ORIMPCOM = 0
  w_ORD_PRZ = 0
  w_CO1CODCOM = space(15)
  w_CO1CODCOS = space(5)
  w_CODATTCOM = space(15)
  w_ORDFLORCO = space(1)
  w_COAPPSAL = 0
  w_IMPCOMCOM = 0
  w_SIMVAL = space(5)
  w_OIMPSCO = 0
  w_OIMPACC = 0
  w_RFLSCOR = space(1)
  w_OLDSER = space(10)
  w_TIPFAT = space(1)
  w_CONTA = 0
  w_SUMIMPCOM = 0
  w_OLDFLEVAS = 0
  w_FLORCO = space(0)
  w_MATRUPD = .f.
  w_CODUBI = space(20)
  w_CODUB2 = space(20)
  w_COMAG = space(5)
  w_CODLOT = space(20)
  w_FLAGLO = space(1)
  w_FLAG2LO = space(1)
  w_QTAESI = 0
  w_OLDQTA = 0
  w_TESDIS = .f.
  w_ORDUBI = space(20)
  w_ORDUB2 = space(20)
  w_ORDLOT = space(20)
  w_SUQTRPER = 0
  w_SUQTAPER = 0
  w_RECODART = space(20)
  w_RECODCOM = space(15)
  w_COMMDEFA = space(15)
  w_COMMAPPO = space(15)
  w_SALCOM = space(1)
  w_COCODART = space(20)
  w_LASTDET = .f.
  w_OLDCODLOT = space(20)
  w_OLDFLSTAT = space(1)
  w_OLDKEYSAL = space(20)
  w_LOTMAG = space(5)
  w_ROWFAT = 0
  StatoRiga = space(1)
  w_PRSERIAL = space(10)
  w_TIPDOCCO = space(5)
  w_MOORERCO = 0
  w_MONORECO = 0
  w_COSTATUS = space(1)
  w_BASETOT = 0
  * --- WorkFile variables
  ADDE_SPE_idx=0
  ATTIVITA_idx=0
  CAN_TIER_idx=0
  CENCOST_idx=0
  CONTI_idx=0
  DES_DIVE_idx=0
  DET_DIFF_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  FAT_DIFF_idx=0
  GIORMAGA_idx=0
  LIS_SCAG_idx=0
  LIS_TINI_idx=0
  LOTTIART_idx=0
  MA_COSTI_idx=0
  PAR_RIOR_idx=0
  RAG_FATT_idx=0
  RIC_PREZ_idx=0
  RIGHEEVA_idx=0
  SALDIART_idx=0
  SIT_FIDI_idx=0
  TIP_DOCU_idx=0
  VALUTE_idx=0
  VOC_COST_idx=0
  VOCIIVA_idx=0
  PDA_DETT_idx=0
  DIC_INTE_idx=0
  TMPVEND1_idx=0
  SALDILOT_idx=0
  ALT_DETT_idx=0
  PRA_CONT_idx=0
  PRE_STAZ_idx=0
  ART_ICOL_idx=0
  SALDICOM_idx=0
  SALOTCOM_idx=0
  DICDINTE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Finali sull'archivio Documenti (da GSVE_MDV)
    * --- Viene eseguito in A.M. Replace e dall Evento Delete End (relativamento allo Storno Doc.Import)
    * --- Variabili di lavoro Definite nella Pag. 4
    this.w_OK = .T.
    this.w_MESS = "Transazione abbandonata"
    this.w_SERIAL = this.oParentObject.w_MVSERIAL
    this.w_FLDEL = .F.
    this.w_FLAPP = .T.
    this.w_SEGNALA = .F.
    this.w_TOTVALFIS = 0
    this.w_FIDS4 = 0
    this.w_FIDS5 = 0
    this.w_FIDS6 = 0
    this.w_OLDSER = "ZXCZXCZX"
    this.w_TIPFAT = " "
    this.oParentObject.w_IMPODL = .F.
    this.w_FLVESC = .F.
    this.w_STORNA = .F.
    this.w_MATRUPD = .F.
    * --- Ricalcola il Fido applicato al documento al momento del salvataggio 
    if this.pPar
      this.Page_15()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      return
    endif
    * --- istanzio oggetto per mess. incrementali
    this.w_oMESS=createobject("ah_message")
    this.w_PADRE = this.oParentObject
    * --- Non eseguo la mcalc all'uscita
    This.bUpdateParentObject=.f.
    if Type("this.w_PADRE.GSAC_MMP.class") = "C" 
      * --- Figlio (matricole/lotti di produzione) modificato
      this.w_MATRUPD = this.w_PADRE.GSAC_MMP.isachildupdated()
    endif
    * --- Controlli Preliminari
    if lower(this.w_PADRE.class)=="tgsar_bgd"
      this.w_CFUNC = "Load"
    else
      this.w_CFUNC = this.w_PADRE.cFunction
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_PADRE.MarkPos(.T.)     
    endif
    if this.w_OK
      * --- Mi assicuro che la variabile sia space di 15 (per evitare problemi con Oracle)
      this.w_COMMDEFA = NVL(g_PPCODCOM, SPACE(15))
      if lower(this.w_PADRE.class)=="tgsar_bgd"
        this.Page_10()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        * --- Scorro le righe modificate, quindi 
        *     a) Rispettano la condizione di FullRow
        *     b) hanno i_SRV='A' o 'U' o ( Deleted() e non aggiunte ) se st� salvando, oppure tutte se st� cancellando
        ah_Msg("Controlli finali")
        * --- Test se cancellata
        this.w_FLDEL = this.w_CFUNC="Query"
        this.w_PADRE.FirstRow()     
        * --- Primo giro sulle righe non cancellate...
        do while Not this.w_PADRE.Eof_Trs()
          this.w_PADRE.SetRow()     
          * --- Scorro le righe piene, cancellate (se non in salvataggio), o aggiunte e modificate
          *     se non cancellate
          if this.w_PADRE.FullRow() And ( this.w_PADRE.RowStatus() $ "A-U" Or this.w_FLDEL or this.w_MATRUPD)
            this.Page_10()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            if IsAlt()
              this.w_FLUPD = this.w_PADRE.RowStatus() = "U"
              this.Page_14()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          if this.w_PADRE.FullRow() 
            this.Page_12()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          oflevas=this.w_PADRE.GET("MVFLEVAS")
          this.w_OLDFLEVAS = IIF(TYPE("oflevas")="N", NVL(this.w_PADRE.GET("MVFLEVAS"), 0), IIF(EMPTY(NVL(this.w_PADRE.GET("MVFLEVAS")," ")),0,1) )
          if this.w_PADRE.FullRow() and this.w_PADRE.RowStatus() = "U" and nvl(this.oParentObject.w_MVIMPCOM,0)>0 and g_COMM="S"
            * --- La riga gestisce la commessa, devo sistemare l'ordinato su MA_COSTI se riapro una riga evasa 
            *     perch� l'anagrafica dei documenti mi riapre di default la quantit� completa senza considerare se ci sono delle fatture o dei DDT che 
            *     evadono parzialmente la riga.
            if this.oParentObject.w_MVFLORCO $ "-+=" AND NOT EMPTY(this.oParentObject.w_MVCODCOM) AND NOT EMPTY(this.oParentObject.w_MVCODATT) AND NOT EMPTY(this.oParentObject.w_MVCODCOS) and this.oParentObject.w_MVFLEVAS<>"S" and this.w_OLDFLEVAS=1
              * --- Select from DOC_DETT
              i_nConn=i_TableProp[this.DOC_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select sum(MVIMPCOM) as SUMIMPCOM  from "+i_cTable+" DOC_DETT ";
                    +" where MVSERRIF="+cp_ToStrODBC(this.oParentObject.w_MVSERIAL)+" and MVROWRIF="+cp_ToStrODBC(this.oParentObject.w_CPROWNUM)+"";
                     ,"_Curs_DOC_DETT")
              else
                select sum(MVIMPCOM) as SUMIMPCOM from (i_cTable);
                 where MVSERRIF=this.oParentObject.w_MVSERIAL and MVROWRIF=this.oParentObject.w_CPROWNUM;
                  into cursor _Curs_DOC_DETT
              endif
              if used('_Curs_DOC_DETT')
                select _Curs_DOC_DETT
                locate for 1=1
                do while not(eof())
                this.w_SUMIMPCOM = SUMIMPCOM
                  select _Curs_DOC_DETT
                  continue
                enddo
                use
              endif
              if nvl(this.w_SUMIMPCOM,0)<>0
                * --- Corrreggo il saldo
                this.w_FLORCO = iif(this.oParentObject.w_MVFLORCO="+","-","+")
                * --- Write into MA_COSTI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.MA_COSTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
                  i_cOp1=cp_SetTrsOp(this.w_FLORCO,'CSORDIN','this.w_SUMIMPCOM',this.w_SUMIMPCOM,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"CSORDIN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSORDIN');
                      +i_ccchkf ;
                  +" where ";
                      +"CSCODCOM = "+cp_ToStrODBC(this.oParentObject.w_MVCODCOM);
                      +" and CSCODCOS = "+cp_ToStrODBC(this.oParentObject.w_MVCODCOS);
                      +" and CSCODMAT = "+cp_ToStrODBC(this.oParentObject.w_MVCODATT);
                      +" and CSTIPSTR = "+cp_ToStrODBC("A");
                         )
                else
                  update (i_cTable) set;
                      CSORDIN = &i_cOp1.;
                      &i_ccchkf. ;
                   where;
                      CSCODCOM = this.oParentObject.w_MVCODCOM;
                      and CSCODCOS = this.oParentObject.w_MVCODCOS;
                      and CSCODMAT = this.oParentObject.w_MVCODATT;
                      and CSTIPSTR = "A";

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
          endif
          if this.w_PADRE.FullRow() and this.w_PADRE.RowStatus() = "U" and nvl(this.oParentObject.w_MVIMPCOM,0)=0 and g_COMM="S"
            * --- La riga gestisce la commessa, devo sistemare l'ordinato su MA_COSTI se evado una riga aperta
            *     perch� l'anagrafica dei documenti mi evade di default la quantit� completa senza considerare se ci sono delle fatture o dei DDT che 
            *     evadono parzialmente la riga.
            if this.oParentObject.w_MVFLORCO $ "-+=" AND NOT EMPTY(this.oParentObject.w_MVCODCOM) AND NOT EMPTY(this.oParentObject.w_MVCODATT) AND NOT EMPTY(this.oParentObject.w_MVCODCOS) and this.oParentObject.w_MVFLEVAS="S" and this.w_OLDFLEVAS=0
              * --- Select from DOC_DETT
              i_nConn=i_TableProp[this.DOC_DETT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select sum(MVIMPCOM) as SUMIMPCOM  from "+i_cTable+" DOC_DETT ";
                    +" where MVSERRIF="+cp_ToStrODBC(this.oParentObject.w_MVSERIAL)+" and MVROWRIF="+cp_ToStrODBC(this.oParentObject.w_CPROWNUM)+"";
                     ,"_Curs_DOC_DETT")
              else
                select sum(MVIMPCOM) as SUMIMPCOM from (i_cTable);
                 where MVSERRIF=this.oParentObject.w_MVSERIAL and MVROWRIF=this.oParentObject.w_CPROWNUM;
                  into cursor _Curs_DOC_DETT
              endif
              if used('_Curs_DOC_DETT')
                select _Curs_DOC_DETT
                locate for 1=1
                do while not(eof())
                this.w_SUMIMPCOM = SUMIMPCOM
                  select _Curs_DOC_DETT
                  continue
                enddo
                use
              endif
              if nvl(this.w_SUMIMPCOM,0)<>0
                * --- Corrreggo il saldo
                * --- Write into MA_COSTI
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.MA_COSTI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
                  i_cOp1=cp_SetTrsOp(this.oParentObject.w_MVFLORCO,'CSORDIN','this.w_SUMIMPCOM',this.w_SUMIMPCOM,'update',i_nConn)
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"CSORDIN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSORDIN');
                      +i_ccchkf ;
                  +" where ";
                      +"CSCODCOM = "+cp_ToStrODBC(this.oParentObject.w_MVCODCOM);
                      +" and CSTIPSTR = "+cp_ToStrODBC("A");
                      +" and CSCODMAT = "+cp_ToStrODBC(this.oParentObject.w_MVCODATT);
                      +" and CSCODCOS = "+cp_ToStrODBC(this.oParentObject.w_MVCODCOS);
                         )
                else
                  update (i_cTable) set;
                      CSORDIN = &i_cOp1.;
                      &i_ccchkf. ;
                   where;
                      CSCODCOM = this.oParentObject.w_MVCODCOM;
                      and CSTIPSTR = "A";
                      and CSCODMAT = this.oParentObject.w_MVCODATT;
                      and CSCODCOS = this.oParentObject.w_MVCODCOS;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
          endif
          if g_COMM="S" AND this.w_PADRE.FullRow() and this.w_PADRE.RowStatus() = "U" 
            * --- La riga gestisce la commessa, devo sistemare l'ordinato su MA_COSTI se modifico una riga parzialmente evasa
            this.w_OLDIMPCOM = nvl(this.w_PADRE.GET("MVIMPCOM"),0)
            if this.oParentObject.w_MVFLORCO $ "-+=" AND NOT EMPTY(this.oParentObject.w_MVCODCOM) AND NOT EMPTY(this.oParentObject.w_MVCODATT) AND NOT EMPTY(this.oParentObject.w_MVCODCOS) and this.w_OLDIMPCOM<>this.oParentObject.w_MVIMPCOM 
              if this.oParentObject.w_MVFLEVAS<>"S"
                this.w_NEWIMPCOM = (((this.oParentObject.w_MVIMPCOM-this.w_OLDIMPCOM)/this.oParentObject.w_MVQTAUM1)* ( this.oParentObject.w_MVQTAUM1-this.oParentObject.w_MVQTAEV1)) - (this.oParentObject.w_MVIMPCOM - this.w_OLDIMPCOM)
                if nvl(this.w_NEWIMPCOM,0)<>0
                  * --- Corrreggo il saldo
                  * --- Write into MA_COSTI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.MA_COSTI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
                    i_cOp1=cp_SetTrsOp(this.oParentObject.w_MVFLORCO,'CSORDIN','this.w_NEWIMPCOM',this.w_NEWIMPCOM,'update',i_nConn)
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"CSORDIN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSORDIN');
                        +i_ccchkf ;
                    +" where ";
                        +"CSCODCOM = "+cp_ToStrODBC(this.oParentObject.w_MVCODCOM);
                        +" and CSTIPSTR = "+cp_ToStrODBC("A");
                        +" and CSCODMAT = "+cp_ToStrODBC(this.oParentObject.w_MVCODATT);
                        +" and CSCODCOS = "+cp_ToStrODBC(this.oParentObject.w_MVCODCOS);
                           )
                  else
                    update (i_cTable) set;
                        CSORDIN = &i_cOp1.;
                        &i_ccchkf. ;
                     where;
                        CSCODCOM = this.oParentObject.w_MVCODCOM;
                        and CSTIPSTR = "A";
                        and CSCODMAT = this.oParentObject.w_MVCODATT;
                        and CSCODCOS = this.oParentObject.w_MVCODCOS;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              else
                * --- modifico un ordine gi� evaso ma scatta lo stesso l'aggiornamento da template in ma_costi  perch� l'mvimpcom iniziale � valorizzato
                *     quindi devo rettificarlo
                if this.w_OLDIMPCOM<>0 AND this.oParentObject.w_MVIMPCOM=0
                  * --- Write into MA_COSTI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.MA_COSTI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
                    i_cOp1=cp_SetTrsOp(this.oParentObject.w_MVFLORCO,'CSORDIN','this.w_OLDIMPCOM',this.w_OLDIMPCOM,'update',i_nConn)
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"CSORDIN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSORDIN');
                        +i_ccchkf ;
                    +" where ";
                        +"CSCODCOM = "+cp_ToStrODBC(this.oParentObject.w_MVCODCOM);
                        +" and CSTIPSTR = "+cp_ToStrODBC("A");
                        +" and CSCODMAT = "+cp_ToStrODBC(this.oParentObject.w_MVCODATT);
                        +" and CSCODCOS = "+cp_ToStrODBC(this.oParentObject.w_MVCODCOS);
                           )
                  else
                    update (i_cTable) set;
                        CSORDIN = &i_cOp1.;
                        &i_ccchkf. ;
                     where;
                        CSCODCOM = this.oParentObject.w_MVCODCOM;
                        and CSTIPSTR = "A";
                        and CSCODMAT = this.oParentObject.w_MVCODATT;
                        and CSCODCOS = this.oParentObject.w_MVCODCOS;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                else
                  this.w_NEWIMPCOM = (((this.oParentObject.w_MVIMPCOM-this.w_OLDIMPCOM)/this.oParentObject.w_MVQTAUM1)* ( this.oParentObject.w_MVQTAUM1-this.oParentObject.w_MVQTAEV1)) - (this.oParentObject.w_MVIMPCOM - this.w_OLDIMPCOM)
                  * --- Write into MA_COSTI
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.MA_COSTI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
                    i_cOp1=cp_SetTrsOp(this.oParentObject.w_MVFLORCO,'CSORDIN','this.w_NEWIMPCOM',this.w_NEWIMPCOM,'update',i_nConn)
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"CSORDIN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSORDIN');
                        +i_ccchkf ;
                    +" where ";
                        +"CSCODCOM = "+cp_ToStrODBC(this.oParentObject.w_MVCODCOM);
                        +" and CSTIPSTR = "+cp_ToStrODBC("A");
                        +" and CSCODMAT = "+cp_ToStrODBC(this.oParentObject.w_MVCODATT);
                        +" and CSCODCOS = "+cp_ToStrODBC(this.oParentObject.w_MVCODCOS);
                           )
                  else
                    update (i_cTable) set;
                        CSORDIN = &i_cOp1.;
                        &i_ccchkf. ;
                     where;
                        CSCODCOM = this.oParentObject.w_MVCODCOM;
                        and CSTIPSTR = "A";
                        and CSCODMAT = this.oParentObject.w_MVCODATT;
                        and CSCODCOS = this.oParentObject.w_MVCODCOS;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              endif
            endif
          endif
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARSALCOM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_MVCODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARSALCOM;
              from (i_cTable) where;
                  ARCODART = this.oParentObject.w_MVCODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_PADRE.FullRow() and this.w_PADRE.RowStatus() = "U" and this.w_SALCOM="S" AND ( (this.oParentObject.w_MVFLEVAS="S" and this.w_OLDFLEVAS=0) OR (this.oParentObject.w_MVFLEVAS<>"S" and this.w_OLDFLEVAS=1) )
            * --- La riga gestisce la commessa, devo sistemare l'ordinato su SALDICOM se evado una riga aperta
            if this.oParentObject.w_MVFLEVAS="S" and this.w_OLDFLEVAS=0
              this.w_APPSALCO = - (this.oParentObject.w_MVQTAUM1-this.oParentObject.w_MVQTAEV1)
            else
              this.w_APPSALCO = this.oParentObject.w_MVQTAUM1-this.oParentObject.w_MVQTAEV1
            endif
            this.Page_17()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          * --- Se tutto ok passo alla prossima riga altrimenti esco...
          if this.w_OK
            this.w_PADRE.NextRow()     
          else
            Exit
          endif
        enddo
        * --- Secondo giro, se tutto Ok sulle righe cancellate...
        if this.w_OK And this.w_CFUNC<>"Query"
          this.w_PADRE.FirstRowDel()     
          * --- Test Se riga Eliminata
          this.w_FLDEL = .T.
          this.w_FLUPD = .F.
          do while Not this.w_PADRE.Eof_Trs()
            this.w_PADRE.SetRow()     
            this.Page_10()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.Page_14()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.Page_12()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Se tutto ok passo alla prossima riga altrimenti esco...
            if this.w_OK
              this.w_PADRE.NextRowDel()     
            else
              Exit
            endif
          enddo
        endif
        if this.w_OK
          this.Page_11()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.w_OK AND this.w_CFUNC<>"Query"
          * --- Aggiorna dati Evasione Righe Importate
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
            do vq_exec with 'GSVE2AGG',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                    +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                    +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVFLEVAS = _t2.MVFLERIF";
                +",MVQTASAL = _t2.MVQTASAL";
                +",MVEFFEVA = _t2.MVEFFEVA";
                +i_ccchkf;
                +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                    +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                    +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
                +"DOC_DETT.MVFLEVAS = _t2.MVFLERIF";
                +",DOC_DETT.MVQTASAL = _t2.MVQTASAL";
                +",DOC_DETT.MVEFFEVA = _t2.MVEFFEVA";
                +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                    +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
                    +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
                +"MVFLEVAS,";
                +"MVQTASAL,";
                +"MVEFFEVA";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.MVFLERIF,";
                +"t2.MVQTASAL,";
                +"t2.MVEFFEVA";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                    +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                    +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
                +"MVFLEVAS = _t2.MVFLERIF";
                +",MVQTASAL = _t2.MVQTASAL";
                +",MVEFFEVA = _t2.MVEFFEVA";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                    +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                    +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVFLEVAS = (select MVFLERIF from "+i_cQueryTable+" where "+i_cWhere+")";
                +",MVQTASAL = (select MVQTASAL from "+i_cQueryTable+" where "+i_cWhere+")";
                +",MVEFFEVA = (select MVEFFEVA from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Gestione Righe Evase senza Importazione
        *     Il cursore � pieno solo in caricamento e solo se ho eseguito un import
        if this.w_OK And this.w_CFUNC<>"Query" And Not Empty ( this.oParentObject.w_Cur_Ev_Row ) And USED( this.oParentObject.w_Cur_Ev_Row )
          if Reccount( this.oParentObject.w_Cur_Ev_Row ) >0
            =WRCURSOR( this.oParentObject.w_Cur_Ev_Row )
            * --- Creo la tabella temporanea utilizzata per aggiornare RIGHEEVA i SALDI 
            *     ed il documento di origine (in modo da stornare solo le righe effettivamente
            *     aggiunte a questo salvataggio)
            * --- Create temporary table TMPVEND1
            i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            i_nConn=i_TableProp[this.RIGHEEVA_idx,3] && recupera la connessione
            i_cTable=cp_SetAzi(i_TableProp[this.RIGHEEVA_idx,2])
            cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
                  +" where 1=0";
                  )
            this.TMPVEND1_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
            * --- Eseguo la Scan For Not Deleted() poich� nel GSVE_BI3 nel caso di Documento di Origine
            *     con No Evasione setto a Deleted le righe relative nel cursore in questione
            SELECT ( this.oParentObject.w_Cur_Ev_Row ) 
 GO TOP 
 SCAN
            this.w_RESERRIF = RESERRIF
            this.w_REROWRIF = REROWRIF
            this.w_RENUMRIF = -20
            * --- Leggo dal documento di Origine i dati per la valorizzazione della Tabella Righe evase a 0
            * --- Read from DOC_DETT
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVKEYSAL,MVCODMAG,MVCODMAT,MVFLIMPE,MVF2IMPE,MVFLORDI,MVF2ORDI,MVQTASAL,MVFLRISE,MVF2RISE,MVFLEVAS,MVQTAEVA,MVIMPEVA,MVTIPRIG,MVCODART,MVCODCOM"+;
                " from "+i_cTable+" DOC_DETT where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_RESERRIF);
                    +" and CPROWNUM = "+cp_ToStrODBC(this.w_REROWRIF);
                    +" and MVNUMRIF = "+cp_ToStrODBC(this.w_RENUMRIF);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVKEYSAL,MVCODMAG,MVCODMAT,MVFLIMPE,MVF2IMPE,MVFLORDI,MVF2ORDI,MVQTASAL,MVFLRISE,MVF2RISE,MVFLEVAS,MVQTAEVA,MVIMPEVA,MVTIPRIG,MVCODART,MVCODCOM;
                from (i_cTable) where;
                    MVSERIAL = this.w_RESERRIF;
                    and CPROWNUM = this.w_REROWRIF;
                    and MVNUMRIF = this.w_RENUMRIF;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_REKEYSAL = NVL(cp_ToDate(_read_.MVKEYSAL),cp_NullValue(_read_.MVKEYSAL))
              this.w_RECODMAG = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
              this.w_RECODMAT = NVL(cp_ToDate(_read_.MVCODMAT),cp_NullValue(_read_.MVCODMAT))
              this.w_REFLIMPE = NVL(cp_ToDate(_read_.MVFLIMPE),cp_NullValue(_read_.MVFLIMPE))
              this.w_REF2IMPE = NVL(cp_ToDate(_read_.MVF2IMPE),cp_NullValue(_read_.MVF2IMPE))
              this.w_REFLORDI = NVL(cp_ToDate(_read_.MVFLORDI),cp_NullValue(_read_.MVFLORDI))
              this.w_REF2ORDI = NVL(cp_ToDate(_read_.MVF2ORDI),cp_NullValue(_read_.MVF2ORDI))
              this.w_REQTASAL = NVL(cp_ToDate(_read_.MVQTASAL),cp_NullValue(_read_.MVQTASAL))
              this.w_REFLRISE = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
              this.w_REF2RISE = NVL(cp_ToDate(_read_.MVF2RISE),cp_NullValue(_read_.MVF2RISE))
              this.w_RE_FLEVAS = NVL(cp_ToDate(_read_.MVFLEVAS),cp_NullValue(_read_.MVFLEVAS))
              this.w_REQTAEVA = NVL(cp_ToDate(_read_.MVQTAEVA),cp_NullValue(_read_.MVQTAEVA))
              this.w_REIMPEVA = NVL(cp_ToDate(_read_.MVIMPEVA),cp_NullValue(_read_.MVIMPEVA))
              this.w_RETIPRIG = NVL(cp_ToDate(_read_.MVTIPRIG),cp_NullValue(_read_.MVTIPRIG))
              this.w_RECODART = NVL(cp_ToDate(_read_.MVCODART),cp_NullValue(_read_.MVCODART))
              this.w_RECODCOM = NVL(cp_ToDate(_read_.MVCODCOM),cp_NullValue(_read_.MVCODCOM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Se trovo dati importazione valorizzati, allora significa che un'altro utente
            *     ha evaso la riga che questo utente sta tentanto di evadere senza importare.
            if this.w_RE_FLEVAS="S" Or ( this.w_REQTAEVA<>0 And this.w_RETIPRIG<>"F") Or ( this.w_REIMPEVA<>0 And this.w_RETIPRIG="F")
              this.w_MESS = Ah_MsgFormat("Impossibile evadere riga senza importazione, gia evasa da altro utente")
              Use In ( this.oParentObject.w_Cur_Ev_Row ) 
              this.w_OK = .F.
              Exit
            else
              * --- Insert into TMPVEND1
              i_nConn=i_TableProp[this.TMPVEND1_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.TMPVEND1_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"RESERRIF"+",REROWRIF"+",RENUMRIF"+",RESERIAL"+",REFLAGEV"+",REKEYSAL"+",RECODMAG"+",RECODMAT"+",REQTASAL"+",REFLIMPE"+",REF2IMPE"+",REFLORDI"+",REF2ORDI"+",REFLRISE"+",REF2RISE"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_RESERRIF),'TMPVEND1','RESERRIF');
                +","+cp_NullLink(cp_ToStrODBC(this.w_REROWRIF),'TMPVEND1','REROWRIF');
                +","+cp_NullLink(cp_ToStrODBC(this.w_RENUMRIF),'TMPVEND1','RENUMRIF');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'TMPVEND1','RESERIAL');
                +","+cp_NullLink(cp_ToStrODBC("S"),'TMPVEND1','REFLAGEV');
                +","+cp_NullLink(cp_ToStrODBC(this.w_REKEYSAL),'TMPVEND1','REKEYSAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_RECODMAG),'TMPVEND1','RECODMAG');
                +","+cp_NullLink(cp_ToStrODBC(this.w_RECODMAT),'TMPVEND1','RECODMAT');
                +","+cp_NullLink(cp_ToStrODBC(this.w_REQTASAL),'TMPVEND1','REQTASAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_REFLIMPE),'TMPVEND1','REFLIMPE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_REF2IMPE),'TMPVEND1','REF2IMPE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_REFLORDI),'TMPVEND1','REFLORDI');
                +","+cp_NullLink(cp_ToStrODBC(this.w_REF2ORDI),'TMPVEND1','REF2ORDI');
                +","+cp_NullLink(cp_ToStrODBC(this.w_REFLRISE),'TMPVEND1','REFLRISE');
                +","+cp_NullLink(cp_ToStrODBC(this.w_REF2RISE),'TMPVEND1','REF2RISE');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'RESERRIF',this.w_RESERRIF,'REROWRIF',this.w_REROWRIF,'RENUMRIF',this.w_RENUMRIF,'RESERIAL',this.oParentObject.w_MVSERIAL,'REFLAGEV',"S",'REKEYSAL',this.w_REKEYSAL,'RECODMAG',this.w_RECODMAG,'RECODMAT',this.w_RECODMAT,'REQTASAL',this.w_REQTASAL,'REFLIMPE',this.w_REFLIMPE,'REF2IMPE',this.w_REF2IMPE,'REFLORDI',this.w_REFLORDI)
                insert into (i_cTable) (RESERRIF,REROWRIF,RENUMRIF,RESERIAL,REFLAGEV,REKEYSAL,RECODMAG,RECODMAT,REQTASAL,REFLIMPE,REF2IMPE,REFLORDI,REF2ORDI,REFLRISE,REF2RISE &i_ccchkf. );
                   values (;
                     this.w_RESERRIF;
                     ,this.w_REROWRIF;
                     ,this.w_RENUMRIF;
                     ,this.oParentObject.w_MVSERIAL;
                     ,"S";
                     ,this.w_REKEYSAL;
                     ,this.w_RECODMAG;
                     ,this.w_RECODMAT;
                     ,this.w_REQTASAL;
                     ,this.w_REFLIMPE;
                     ,this.w_REF2IMPE;
                     ,this.w_REFLORDI;
                     ,this.w_REF2ORDI;
                     ,this.w_REFLRISE;
                     ,this.w_REF2RISE;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
            endif
            ENDSCAN
          endif
        endif
        * --- Aggiornamento documento di Origine e Saldi nel caso di Evasione senza Importazione
        *     Vengono eseguite sia in caricamento che in cancellazione poich� le query sono fatte
        *     in modo che i valori siano gi� corretti
        *     In Cancellazione sempre; in Caricamento e Modifica solo se esiste il cursore di righe evase senza import ed � valorizzato
        if this.w_OK And ( this.w_CFUNC="Query" Or (this.w_CFUNC<>"Query" And (Not Empty ( this.oParentObject.w_Cur_Ev_Row ) ) And Used( this.oParentObject.w_Cur_Ev_Row ) And Reccount( this.oParentObject.w_Cur_Ev_Row ) >0))
          if this.w_CFUNC="Query"
            * --- Se in cancellazione il temporaneo utilzizato dalla query non � stato
            *     popolato da nessuno lo rileggo da RIGHEEVA
            * --- Create temporary table TMPVEND1
            i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
            i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
            i_nConn=i_TableProp[this.RIGHEEVA_idx,3] && recupera la connessione
            i_cTable=cp_SetAzi(i_TableProp[this.RIGHEEVA_idx,2])
            cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
                  +" where RESERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL)+"";
                  )
            this.TMPVEND1_idx=i_nIdx
            i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          endif
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="MVSERIAL,CPROWNUM,MVNUMRIF"
            do vq_exec with 'gsve_rev',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                    +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                    +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVFLEVAS = _t2.MVFLEVAS";
                +",MVQTASAL = DOC_DETT.MVQTASAL-_t2.MVQTASAL";
                +i_ccchkf;
                +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                    +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                    +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
                +"DOC_DETT.MVFLEVAS = _t2.MVFLEVAS";
                +",DOC_DETT.MVQTASAL = DOC_DETT.MVQTASAL-_t2.MVQTASAL";
                +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                    +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
                    +" and "+"DOC_DETT.MVNUMRIF = t2.MVNUMRIF";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
                +"MVFLEVAS,";
                +"MVQTASAL";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"t2.MVFLEVAS,";
                +"MVQTASAL-t2.MVQTASAL";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                    +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                    +" and "+"DOC_DETT.MVNUMRIF = _t2.MVNUMRIF";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
                +"MVFLEVAS = _t2.MVFLEVAS";
                +",MVQTASAL = DOC_DETT.MVQTASAL-_t2.MVQTASAL";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                    +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                    +" and "+i_cTable+".MVNUMRIF = "+i_cQueryTable+".MVNUMRIF";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"MVFLEVAS = (select MVFLEVAS from "+i_cQueryTable+" where "+i_cWhere+")";
                +",MVQTASAL = (select "+i_cTable+".MVQTASAL-MVQTASAL from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          if i_nConn<>0
            local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
            declare i_aIndex[1]
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_aIndex(1)="SLCODICE,SLCODMAG"
            do vq_exec with 'gsve1rev',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)	
            i_cDB=cp_GetDatabaseType(i_nConn)
            do case
            case i_cDB="SQLServer"
              i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
                    +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTOPER = SALDIART.SLQTOPER+_t2.ORDI";
                +",SLQTIPER = SALDIART.SLQTIPER+_t2.IMPE";
                +",SLQTRPER = SALDIART.SLQTRPER+_t2.RISE";
                +i_ccchkf;
                +" from "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 where "+i_cWhere)
            case i_cDB="MySQL"
              i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
                    +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART, "+i_cQueryTable+" _t2 set ";
                +"SALDIART.SLQTOPER = SALDIART.SLQTOPER+_t2.ORDI";
                +",SALDIART.SLQTIPER = SALDIART.SLQTIPER+_t2.IMPE";
                +",SALDIART.SLQTRPER = SALDIART.SLQTRPER+_t2.RISE";
                +Iif(Empty(i_ccchkf),"",",SALDIART.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                +" where "+i_cWhere)
            case i_cDB="Oracle"
              i_cWhere="SALDIART.SLCODICE = t2.SLCODICE";
                    +" and "+"SALDIART.SLCODMAG = t2.SLCODMAG";
              
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set (";
                +"SLQTOPER,";
                +"SLQTIPER,";
                +"SLQTRPER";
                +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                +"SLQTOPER+t2.ORDI,";
                +"SLQTIPER+t2.IMPE,";
                +"SLQTRPER+t2.RISE";
                +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
            case i_cDB="PostgreSQL"
              i_cWhere="SALDIART.SLCODICE = _t2.SLCODICE";
                    +" and "+"SALDIART.SLCODMAG = _t2.SLCODMAG";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDIART set ";
                +"SLQTOPER = SALDIART.SLQTOPER+_t2.ORDI";
                +",SLQTIPER = SALDIART.SLQTIPER+_t2.IMPE";
                +",SLQTRPER = SALDIART.SLQTRPER+_t2.RISE";
                +i_ccchkf;
                +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
            otherwise
              i_cWhere=i_cTable+".SLCODICE = "+i_cQueryTable+".SLCODICE";
                    +" and "+i_cTable+".SLCODMAG = "+i_cQueryTable+".SLCODMAG";
          
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"SLQTOPER = (select "+i_cTable+".SLQTOPER+ORDI from "+i_cQueryTable+" where "+i_cWhere+")";
                +",SLQTIPER = (select "+i_cTable+".SLQTIPER+IMPE from "+i_cQueryTable+" where "+i_cWhere+")";
                +",SLQTRPER = (select "+i_cTable+".SLQTRPER+RISE from "+i_cQueryTable+" where "+i_cWhere+")";
                +i_ccchkf;
                +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            endcase
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARSALCOM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_RECODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARSALCOM;
              from (i_cTable) where;
                  ARCODART = this.w_RECODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if this.w_SALCOM="S"
            if empty(nvl(this.w_RECODCOM,""))
              this.w_COMMAPPO = this.w_COMMDEFA
            else
              this.w_COMMAPPO = this.w_RECODCOM
            endif
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="SCCODICE,SCCODMAG,SCCODCAN"
              do vq_exec with 'gsve2rev',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
                      +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
                      +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER = SALDICOM.SCQTOPER+_t2.ORDI";
                  +",SCQTIPER = SALDICOM.SCQTIPER+_t2.IMPE";
                  +",SCQTRPER = SALDICOM.SCQTRPER+_t2.RISE";
                  +i_ccchkf;
                  +" from "+i_cTable+" SALDICOM, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
                      +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
                      +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM, "+i_cQueryTable+" _t2 set ";
                  +"SALDICOM.SCQTOPER = SALDICOM.SCQTOPER+_t2.ORDI";
                  +",SALDICOM.SCQTIPER = SALDICOM.SCQTIPER+_t2.IMPE";
                  +",SALDICOM.SCQTRPER = SALDICOM.SCQTRPER+_t2.RISE";
                  +Iif(Empty(i_ccchkf),"",",SALDICOM.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="SALDICOM.SCCODICE = t2.SCCODICE";
                      +" and "+"SALDICOM.SCCODMAG = t2.SCCODMAG";
                      +" and "+"SALDICOM.SCCODCAN = t2.SCCODCAN";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM set (";
                  +"SCQTOPER,";
                  +"SCQTIPER,";
                  +"SCQTRPER";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"SCQTOPER+t2.ORDI,";
                  +"SCQTIPER+t2.IMPE,";
                  +"SCQTRPER+t2.RISE";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="SALDICOM.SCCODICE = _t2.SCCODICE";
                      +" and "+"SALDICOM.SCCODMAG = _t2.SCCODMAG";
                      +" and "+"SALDICOM.SCCODCAN = _t2.SCCODCAN";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" SALDICOM set ";
                  +"SCQTOPER = SALDICOM.SCQTOPER+_t2.ORDI";
                  +",SCQTIPER = SALDICOM.SCQTIPER+_t2.IMPE";
                  +",SCQTRPER = SALDICOM.SCQTRPER+_t2.RISE";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".SCCODICE = "+i_cQueryTable+".SCCODICE";
                      +" and "+i_cTable+".SCCODMAG = "+i_cQueryTable+".SCCODMAG";
                      +" and "+i_cTable+".SCCODCAN = "+i_cQueryTable+".SCCODCAN";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"SCQTOPER = (select "+i_cTable+".SCQTOPER+ORDI from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",SCQTIPER = (select "+i_cTable+".SCQTIPER+IMPE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",SCQTRPER = (select "+i_cTable+".SCQTRPER+RISE from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          if this.w_CFUNC="Query"
            * --- Se cancello il documento, elimino dalla tabella delle righe Evase Senza Import il record relativo
            * --- Delete from RIGHEEVA
            i_nConn=i_TableProp[this.RIGHEEVA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RIGHEEVA_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"RESERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                     )
            else
              delete from (i_cTable) where;
                    RESERIAL = this.oParentObject.w_MVSERIAL;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          else
            * --- Insert into RIGHEEVA
            i_nConn=i_TableProp[this.RIGHEEVA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RIGHEEVA_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_cTempTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
              i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.RIGHEEVA_idx)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
          * --- Drop temporary table TMPVEND1
          i_nIdx=cp_GetTableDefIdx('TMPVEND1')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMPVEND1')
          endif
        endif
        if this.w_OK AND this.oParentObject.w_MVFLPROV<>"S" AND g_MADV="S"
          ah_Msg("Controlli finali lotti / ubicazioni")
          * --- Test se cancellata
          this.w_FLDEL = this.w_CFUNC="Query"
          this.w_PADRE.FirstRow()     
          * --- Primo giro sulle righe non cancellate...
          do while Not this.w_PADRE.Eof_Trs()
            this.w_PADRE.SetRow()     
            * --- Scorro le righe piene, cancellate (se non in salvataggio), o aggiunte e modificate
            *     se non cancellate
            if this.w_PADRE.FullRow() And ( this.w_PADRE.RowStatus() $ "A-U" Or this.w_FLDEL or this.w_MATRUPD)
              * --- Ho almeno una riga in append\modificata 
              this.w_OKGEN = .T.
              this.Page_9()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            * --- Se tutto ok passo alla prossima riga altrimenti esco...
            if this.w_OK
              this.w_PADRE.NextRow()     
            else
              Exit
            endif
          enddo
          * --- Secondo giro, se tutto Ok sulle righe cancellate...
          if this.w_OK And this.w_CFUNC<>"Query"
            this.w_PADRE.FirstRowDel()     
            * --- Test Se riga Eliminata
            this.w_FLDEL = .T.
            do while Not this.w_PADRE.Eof_Trs()
              this.w_PADRE.SetRow()     
              this.w_OKGEN = .T.
              this.Page_9()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              * --- Se tutto ok passo alla prossima riga altrimenti esco...
              if this.w_OK
                this.w_PADRE.NextRowDel()     
              else
                Exit
              endif
            enddo
          endif
        endif
        * --- Elimino documento di esplosione prodotto finito legato alla testata documento
        this.w_DELESP = (this.oParentObject.w_OLDTES <> Alltrim(this.oParentObject.w_MVCODCON)+Alltrim(this.oParentObject.w_MVTCOLIS)+Alltrim(DTOC(this.oParentObject.w_MVDATDOC))) or this.w_CFUNC="Query" or this.oParentObject.w_MVTIPIMB$"RC" or this.w_OKGEN
        if this.w_OK AND g_EACD="S" AND this.w_CFUNC<>"Load" AND NOT EMPTY(this.oParentObject.w_MVRIFESP) And this.w_DELESP
          this.w_DOCEVA = this.oParentObject.w_MVRIFESP
          this.w_DOCRIG = .F.
          this.Page_8()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        WAIT CLEAR
      endif
    endif
    * --- Se Documento non Provvisorio controllo Fido
    *     Attenzione OFLPROV viene messo a 'S' anche da batch GSVE_BND per modifica causale.
    *     In questo caso anche se � una modifica deve ricalcolare il rischio poich� � stata modificata la causale.
    if this.w_OK AND (this.w_CFUNC="Load" Or this.w_CFUNC="Edit" ) AND this.oParentObject.w_MVFLPROV="N" And g_PERFID="S" AND this.oParentObject.w_FLRISC$"SD" AND this.oParentObject.w_MVTIPCON="C" AND NOT EMPTY(this.oParentObject.w_MVCODCON)
      * --- Controllo del Rischio
      * --- Storno l'acconto se contabilizzato..
      *     (per ora questo accade sempre visto che questo codice scatta solo alla
      *     conferma del caricamento)
      this.w_TOTDOC = this.oParentObject.w_TOTFATTU - iIf ( Empty( this.oParentObject.w_MVRIFACC ) , this.oParentObject.w_MVACCONT , 0 )
      * --- Verifico se � stato inserito uno sconto forzato
      if this.oParentObject.w_MVIMPFIN <> 0
        this.w_TOTDOC = this.w_TOTDOC - this.oParentObject.w_MVIMPFIN
      endif
      * --- Ritenute attive se presenti le sottraggo dal totale documento.
      if this.oParentObject.w_MVRITATT <> 0
        this.w_TOTDOC = this.w_TOTDOC - this.oParentObject.w_MVRITATT
      endif
      * --- w_SPEBOL sono presenti solo in fattura negli altri casi sono zero
      this.w_SPEBOL = this.oParentObject.w_MVSPEBOL
      if this.oParentObject.w_MVCODVAL<>this.oParentObject.w_MVVALNAZ
        * --- Se il Documento e' in Valuta Converte in Moneta di Conto
        this.w_TOTDOC = VAL2MON(this.w_TOTDOC, this.oParentObject.w_MVCAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_MVDATDOC, this.oParentObject.w_MVVALNAZ,this.oParentObject.w_DECTOP)
        this.w_SPEBOL = VAL2MON(this.w_SPEBOL, this.oParentObject.w_MVCAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_MVDATDOC, this.oParentObject.w_MVVALNAZ, this.oParentObject.w_DECTOP)
      endif
      * --- Toglie Totale Documento
      this.w_FIDRES = this.oParentObject.w_MAXFID - this.w_TOTDOC
      this.w_FID1 = 0
      this.w_FID2 = 0
      this.w_FID3 = 0
      this.w_FID4 = 0
      this.w_FID5 = 0
      this.w_FID6 = 0
      this.w_FIDD = cp_CharToDate("  -  -  ")
      * --- Storna l importo del fido del documento (l importo originario prima delle modifiche la documento)
      if this.w_CFUNC="Edit" 
        * --- Try
        local bErr_04DEB078
        bErr_04DEB078=bTrsErr
        this.Try_04DEB078()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04DEB078
        * --- End
      endif
      * --- Read from SIT_FIDI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.SIT_FIDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SIT_FIDI_idx,2],.t.,this.SIT_FIDI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "FIIMPPAP,FIIMPESC,FIIMPESO,FIIMPORD,FIIMPDDT,FIIMPFAT,FIDATELA"+;
          " from "+i_cTable+" SIT_FIDI where ";
              +"FICODCLI = "+cp_ToStrODBC(this.oParentObject.w_XCONORN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          FIIMPPAP,FIIMPESC,FIIMPESO,FIIMPORD,FIIMPDDT,FIIMPFAT,FIDATELA;
          from (i_cTable) where;
              FICODCLI = this.oParentObject.w_XCONORN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FID1 = NVL(cp_ToDate(_read_.FIIMPPAP),cp_NullValue(_read_.FIIMPPAP))
        this.w_FID2 = NVL(cp_ToDate(_read_.FIIMPESC),cp_NullValue(_read_.FIIMPESC))
        this.w_FID3 = NVL(cp_ToDate(_read_.FIIMPESO),cp_NullValue(_read_.FIIMPESO))
        this.w_FID4 = NVL(cp_ToDate(_read_.FIIMPORD),cp_NullValue(_read_.FIIMPORD))
        this.w_FID5 = NVL(cp_ToDate(_read_.FIIMPDDT),cp_NullValue(_read_.FIIMPDDT))
        this.w_FID6 = NVL(cp_ToDate(_read_.FIIMPFAT),cp_NullValue(_read_.FIIMPFAT))
        this.w_FIDD = NVL(cp_ToDate(_read_.FIDATELA),cp_NullValue(_read_.FIDATELA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Toglie Totale Altri Documenti
      * --- Anagrafica Fido gia presente ?
      this.w_ANFIDO = i_Rows<>0
      if this.w_ANFIDO
        this.w_FIDRES = this.w_FIDRES - (this.w_FID1+this.w_FID2+this.w_FID3+this.w_FID4+this.w_FID5+this.w_FID6)
      endif
      * --- Riaggiungo al Fido Residuo gli Importi dei Documenti Importati in proporzione sul totale Documento (sarebbero conteggiati 2 volte)
      this.w_FIDRES = this.w_FIDRES + (this.w_FIDS4+this.w_FIDS5+this.w_FIDS6)
      if this.oParentObject.w_FLFIDO="S"
        * --- Se ho controllo fido attivo sul cliente controllo massimo importo fido e Ordinabile
        if this.w_FIDRES<0 AND this.oParentObject.w_FLCRIS="S"
          this.w_oPART = this.w_oMESS.addmsgpartNL("Superato massimo importo fido cliente%0Residuo: %1 alla data elaborazione: %2")
          this.w_oPART.addParam(ALLTRIM(STR(this.w_FIDRES,18+this.oParentObject.w_DECTOP,this.oParentObject.w_DECTOP)))     
          this.w_oPART.addParam(DTOC(this.w_FIDD))     
          if lower(this.w_PADRE.class)=="tgsar_bgd"
            if !this.w_PADRE.w_PADRE.bNOcalcFido and this.w_PADRE.w_bLastRow
              this.w_OK = .T.
              if this.oParentObject.w_FLBLVE="S"
                this.w_PADRE.w_OK = .F.
                this.w_PADRE.w_RETVAL = this.w_oMESS.ComposeMessage()
              else
                this.w_PADRE.w_PADRE.AddLogMsg("W",this.w_oMESS.ComposeMessage())     
              endif
            endif
          else
            if this.oParentObject.w_FLBLVE="S"
              this.w_OK = .F.
              this.w_MESS = this.w_oMESS.ComposeMessage()
            else
              this.w_oMESS.AddMsgPart("Confermi ugualmente?")     
              this.w_OK = this.w_oMESS.ah_YesNo()
              this.w_MESS = ah_Msgformat("Transazione abbandonata")
            endif
          endif
        endif
        if this.w_OK AND this.oParentObject.w_MVFLVEAC="V" AND this.oParentObject.w_MVCLADOC="OR" AND this.oParentObject.w_MAXORD<>0 AND this.w_TOTDOC>this.oParentObject.w_MAXORD AND this.oParentObject.w_FLCRIS="S"
          * --- Massimo Ordinabile
          this.w_oPART = this.w_oMESS.addmsgpartNL("Superato massimo ordinabile cliente%0Max.ordinabile: %1")
          this.w_oPART.addParam(ALLTRIM(STR(this.oParentObject.w_MAXORD,18+this.oParentObject.w_DECTOP,this.oParentObject.w_DECTOP)))     
          if this.oParentObject.w_FLBLVE="S"
            this.w_OK = .F.
            this.w_MESS = this.w_oMESS.ComposeMessage()
          else
            this.w_oMESS.AddMsgPart("Confermi ugualmente?")     
            this.w_OK = this.w_oMESS.ah_YesNo()
            this.w_MESS = ah_Msgformat("Transazione abbandonata")
          endif
        endif
      endif
      if this.w_OK
        * --- Aggiorna Rischio relativo al Documento
        this.w_FID4 = cp_ROUND(IIF(this.oParentObject.w_MVCLADOC="OR" AND ( lower(this.w_PADRE.class)<>"tgsar_bgd" OR (lower(this.w_PADRE.class)=="tgsar_bgd" AND this.w_PADRE.w_bLastRow)), this.w_TOTDOC-this.w_SPEBOL, 0) - this.w_FIDS4,this.oParentObject.w_DECTOP)
        this.w_FID5 = cp_ROUND(IIF(this.oParentObject.w_MVCLADOC="DT" AND ( lower(this.w_PADRE.class)<>"tgsar_bgd" OR (lower(this.w_PADRE.class)=="tgsar_bgd" AND this.w_PADRE.w_bLastRow)), this.w_TOTDOC - this.w_SPEBOL, 0) - this.w_FIDS5,this.oParentObject.w_DECTOP)
        if NOT this.oParentObject.w_MVCLADOC $ "OR-DT"
          * --- Fatture e note di credito 
          this.w_FID6 = this.w_TOTDOC-cp_ROUND(this.w_FIDS6,this.oParentObject.w_DECTOP)
        else
          * --- Dt e ordini
          this.w_FID6 = 0
        endif
        * --- Se sulla causale ho specificato che il documento deve aumentare il rischio
        *     metto il valore in negativo
        *     Se si arriva all'interno dell'if generale del fido FLRISC pu� valere S o A ma controllo
        *     comunque entrambi per sicurezza
        this.w_FID4 = IIF(this.oParentObject.w_FLRISC="S",1,IIF(this.oParentObject.w_FLRISC="D",-1,0)) * this.w_FID4
        this.w_FID5 = IIF(this.oParentObject.w_FLRISC="S",1,IIF(this.oParentObject.w_FLRISC="D",-1,0)) * this.w_FID5
        this.w_FID6 = IIF(this.oParentObject.w_FLRISC="S",1,IIF(this.oParentObject.w_FLRISC="D",-1,0)) * this.w_FID6
        * --- Aggiorno l'anagrafica Fido
        * --- Try
        local bErr_04E14720
        bErr_04E14720=bTrsErr
        this.Try_04E14720()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04E14720
        * --- End
      endif
    endif
    if this.oParentObject.w_TDTOTDOC<>"E" AND this.oParentObject.w_TDIMPMIN<>0 AND this.w_CFUNC<>"Query" AND this.oParentObject.w_MVTIPCON="C" And this.w_OK
      this.Page_16()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Verifica Dichiarazione di Intento con Importo Utilizzato
    if this.w_OK AND ( (NOT EMPTY(this.oParentObject.w_MVRIFDIC) AND this.oParentObject.w_TIPDIC $ "CF" AND this.oParentObject.w_OPEDIC$"IO") OR ( NOT EMPTY(this.oParentObject.w_OLDRIFDIC) AND EMPTY(this.oParentObject.w_MVRIFDIC) ) ) AND this.oParentObject.w_FLSILI="S" AND ((lower(this.w_PADRE.class)=="tgsar_bgd" AND this.w_PADRE.w_bLastRow) Or lower(this.w_PADRE.class)<>"tgsar_bgd" )
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Check Caricamento automatico (pagina Spedizioni nei Documenti)
    if this.w_OK And lower(this.w_PADRE.class)<>"tgsar_bgd" AND this.oParentObject.w_CARICAU="S" And Not Empty(this.oParentObject.w_MVCODCON) And Not Empty(this.oParentObject.w_MVTIPCON) 
      * --- Se non indico un codice, la procedura mi propone un progressivo
      if EMPTY(this.oParentObject.w_DDCODDES)
        this.w_CONTA = 0
        * --- Select from DES_DIVE
        i_nConn=i_TableProp[this.DES_DIVE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES  from "+i_cTable+" DES_DIVE ";
              +" where DDCODICE="+cp_ToStrODBC(this.oParentObject.w_MVCODCON)+" AND DDTIPCON="+cp_ToStrODBC(this.oParentObject.w_MVTIPCON)+"";
               ,"_Curs_DES_DIVE")
        else
          select DDTIPCON,DDCODICE,DDCODDES from (i_cTable);
           where DDCODICE=this.oParentObject.w_MVCODCON AND DDTIPCON=this.oParentObject.w_MVTIPCON;
            into cursor _Curs_DES_DIVE
        endif
        if used('_Curs_DES_DIVE')
          select _Curs_DES_DIVE
          locate for 1=1
          do while not(eof())
          this.w_CONTA = this.w_CONTA+1
            select _Curs_DES_DIVE
            continue
          enddo
          use
        endif
        this.w_CONTA = this.w_CONTA+1
        this.oParentObject.w_DDCODDES = RIGHT(("00000"+alltrim(STR(this.w_CONTA))), 5)
        * --- Read from DES_DIVE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DES_DIVE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" DES_DIVE where ";
                +"DDTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
                +" and DDCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODCON);
                +" and DDCODDES = "+cp_ToStrODBC(this.oParentObject.w_DDCODDES);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                DDTIPCON = this.oParentObject.w_MVTIPCON;
                and DDCODICE = this.oParentObject.w_MVCODCON;
                and DDCODDES = this.oParentObject.w_DDCODDES;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Controllo che non vengano inseriti codici uguali
        do while i_ROWS<>0
          this.w_CONTA = this.w_CONTA+1
          this.oParentObject.w_DDCODDES = RIGHT(("00000"+alltrim(STR(this.w_CONTA))), 5)
          * --- Read from DES_DIVE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DES_DIVE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "*"+;
              " from "+i_cTable+" DES_DIVE where ";
                  +"DDTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
                  +" and DDCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODCON);
                  +" and DDCODDES = "+cp_ToStrODBC(this.oParentObject.w_DDCODDES);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              *;
              from (i_cTable) where;
                  DDTIPCON = this.oParentObject.w_MVTIPCON;
                  and DDCODICE = this.oParentObject.w_MVCODCON;
                  and DDCODDES = this.oParentObject.w_DDCODDES;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        enddo
      else
        * --- Read from DES_DIVE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DES_DIVE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2],.t.,this.DES_DIVE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" DES_DIVE where ";
                +"DDTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
                +" and DDCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODCON);
                +" and DDCODDES = "+cp_ToStrODBC(this.oParentObject.w_DDCODDES);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                DDTIPCON = this.oParentObject.w_MVTIPCON;
                and DDCODICE = this.oParentObject.w_MVCODCON;
                and DDCODDES = this.oParentObject.w_DDCODDES;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if EMPTY( this.oParentObject.w_DDCODNAZ )
        this.oParentObject.w_DDCODNAZ = this.oParentObject.w_CODNAZ
      endif
      * --- Se il codice sede � corretto lo inserisce nella tabella
      if i_ROWS=0
        * --- Insert into DES_DIVE
        i_nConn=i_TableProp[this.DES_DIVE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DES_DIVE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DES_DIVE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"DDTIPCON"+",DDCODICE"+",DDCODDES"+",DDNOMDES"+",DDINDIRI"+",DD___CAP"+",DDLOCALI"+",DDPROVIN"+",DDTIPRIF"+",DDCODVET"+",DDCODPOR"+",DDCODSPE"+",DDCODNAZ"+",DDPREDEF"+",DD_EMAIL"+",DD_EMPEC"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVTIPCON),'DES_DIVE','DDTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODCON),'DES_DIVE','DDCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DDCODDES),'DES_DIVE','DDCODDES');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DDNOMDES),'DES_DIVE','DDNOMDES');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DDINDIRI),'DES_DIVE','DDINDIRI');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DD__CAP),'DES_DIVE','DD___CAP');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DDLOCALI),'DES_DIVE','DDLOCALI');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DDPROVIN),'DES_DIVE','DDPROVIN');
          +","+cp_NullLink(cp_ToStrODBC("CO"),'DES_DIVE','DDTIPRIF');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODVET),'DES_DIVE','DDCODVET');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODPOR),'DES_DIVE','DDCODPOR');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVCODSPE),'DES_DIVE','DDCODSPE');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DDCODNAZ),'DES_DIVE','DDCODNAZ');
          +","+cp_NullLink(cp_ToStrODBC("N"),'DES_DIVE','DDPREDEF');
          +","+cp_NullLink(cp_ToStrODBC(Space(254)),'DES_DIVE','DD_EMAIL');
          +","+cp_NullLink(cp_ToStrODBC(Space(254)),'DES_DIVE','DD_EMPEC');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'DDTIPCON',this.oParentObject.w_MVTIPCON,'DDCODICE',this.oParentObject.w_MVCODCON,'DDCODDES',this.oParentObject.w_DDCODDES,'DDNOMDES',this.oParentObject.w_DDNOMDES,'DDINDIRI',this.oParentObject.w_DDINDIRI,'DD___CAP',this.oParentObject.w_DD__CAP,'DDLOCALI',this.oParentObject.w_DDLOCALI,'DDPROVIN',this.oParentObject.w_DDPROVIN,'DDTIPRIF',"CO",'DDCODVET',this.oParentObject.w_MVCODVET,'DDCODPOR',this.oParentObject.w_MVCODPOR,'DDCODSPE',this.oParentObject.w_MVCODSPE)
          insert into (i_cTable) (DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDINDIRI,DD___CAP,DDLOCALI,DDPROVIN,DDTIPRIF,DDCODVET,DDCODPOR,DDCODSPE,DDCODNAZ,DDPREDEF,DD_EMAIL,DD_EMPEC &i_ccchkf. );
             values (;
               this.oParentObject.w_MVTIPCON;
               ,this.oParentObject.w_MVCODCON;
               ,this.oParentObject.w_DDCODDES;
               ,this.oParentObject.w_DDNOMDES;
               ,this.oParentObject.w_DDINDIRI;
               ,this.oParentObject.w_DD__CAP;
               ,this.oParentObject.w_DDLOCALI;
               ,this.oParentObject.w_DDPROVIN;
               ,"CO";
               ,this.oParentObject.w_MVCODVET;
               ,this.oParentObject.w_MVCODPOR;
               ,this.oParentObject.w_MVCODSPE;
               ,this.oParentObject.w_DDCODNAZ;
               ,"N";
               ,Space(254);
               ,Space(254);
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Write into DOC_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVCODDES ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DDCODDES),'DOC_MAST','MVCODDES');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                 )
        else
          update (i_cTable) set;
              MVCODDES = this.oParentObject.w_DDCODDES;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.oParentObject.w_MVSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if g_REVI="S" 
          if this.oParentObject.w_MVTIPCON="C"
            this.w_SOTABGES = "GSAR_ACL"
            this.w_RSFILTPK = "w_ANTIPCON=" + cp_toStrODBC(this.oParentObject.w_MVTIPCON) + ";w_ANCODICE=" + cp_toStrODBC(this.oParentObject.w_MVCODCON) + ";w_SOTABGES='GSAR_ACL'"
          else
            this.w_SOTABGES = "GSAR_AFR"
            this.w_RSFILTPK = "w_ANTIPCON=" + cp_toStrODBC(this.oParentObject.w_MVTIPCON) + ";w_ANCODICE=" + cp_toStrODBC(this.oParentObject.w_MVCODCON) + ";w_SOTABGES='GSAR_AFR'"
          endif
          this.w_CODUTE = i_CODUTE
          GSRV_BIQ(this,this.w_RSFILTPK, "InsertQueue", "", .F. , this.w_SOTABGES, "A", .F., .T., this.w_CODUTE) 
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        this.w_MESS = ah_Msgformat("Codice sede gi� presente nella tabella sedi")
        this.w_OK = .F.
      endif
    endif
    * --- Controllo se generato Ordine Da PDA
    if this.w_OK And this.oParentObject.w_MVCLADOC="OR" And this.oParentObject.w_MVFLVEAC="A" And this.w_CFUNC="Query" AND lower(this.w_PADRE.class)<>"tgsar_bgd"
      * --- Select from PDA_DETT
      i_nConn=i_TableProp[this.PDA_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PDA_DETT_idx,2],.t.,this.PDA_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select PDSERIAL,PDROWNUM  from "+i_cTable+" PDA_DETT ";
            +" where PDSERRIF = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL)+"";
             ,"_Curs_PDA_DETT")
      else
        select PDSERIAL,PDROWNUM from (i_cTable);
         where PDSERRIF = this.oParentObject.w_MVSERIAL;
          into cursor _Curs_PDA_DETT
      endif
      if used('_Curs_PDA_DETT')
        select _Curs_PDA_DETT
        locate for 1=1
        do while not(eof())
        this.w_SERPDA = _Curs_PDA_DETT.PDSERIAL
        this.w_ROWPDA = _Curs_PDA_DETT.PDROWNUM
        * --- Nel caso l'ordine � stato generato da PDA, rimetto la PDA Confermata ed elimino il riferimento all'ordine
        * --- Write into PDA_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PDA_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PDA_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PDA_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PDSERRIF ="+cp_NullLink(cp_ToStrODBC(Space(10)),'PDA_DETT','PDSERRIF');
          +",PDSTATUS ="+cp_NullLink(cp_ToStrODBC("S"),'PDA_DETT','PDSTATUS');
              +i_ccchkf ;
          +" where ";
              +"PDSERIAL = "+cp_ToStrODBC(this.w_SERPDA);
              +" and PDROWNUM = "+cp_ToStrODBC(this.w_ROWPDA);
                 )
        else
          update (i_cTable) set;
              PDSERRIF = Space(10);
              ,PDSTATUS = "S";
              &i_ccchkf. ;
           where;
              PDSERIAL = this.w_SERPDA;
              and PDROWNUM = this.w_ROWPDA;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
          select _Curs_PDA_DETT
          continue
        enddo
        use
      endif
    endif
    if lower(this.w_PADRE.class)<>"tgsar_bgd"
      this.w_PADRE.RePos()     
    endif
    * --- I controlli disponibilit� vanno effettuati dopo tutte le scritture per 
    *     gestione evasione / esplosione distinte...
    if this.w_OK And this.oParentObject.w_MVFLPROV<>"S" And g_PERDIS="S" AND lower(this.w_PADRE.class)<>"tgsar_bgd"
      * --- Verifico per gli articoli con check disponibilit� senza conferma
      *     (controllo gia svolto in GSVE_BCK sul transitorio). Replicato
      *     sotto transazione per assicurarsi che il check sia sicuro
      this.w_OK = GSAR_BDA( This , "", this.w_PADRE )
    endif
    if this.w_OK And (g_PERLOT="S" OR g_PERUBI="S") AND lower(this.w_PADRE.class)<>"tgsar_bgd"
      * --- Controllo Disponibilita' Lotti/Ubicazioni
      this.w_OK = GSAR_BLK( This , "", this.w_PADRE )
    endif
    * --- Se sono in cancellazione Provo a riprostinare i progressivi 
    *     (solo se sono in cancellazione dell'ultimo)
    *     Il documento non deve essere generato dal POS poich� in questo caso
    *     il POS riutilizza lo stesso progressivo
    if this.w_OK And this.w_CFUNC="Query" And Not this.oParentObject.w_MVFLOFFE $ "DI"
      GSAR_BRP(this,this.oParentObject.w_MVFLVEAC, this.oParentObject.w_MVANNDOC, this.oParentObject.w_MVPRD, this.oParentObject.w_MVNUMDOC, this.oParentObject.w_MVALFDOC, this.oParentObject.w_MVPRP, this.oParentObject.w_MVNUMEST, this.oParentObject.w_MVALFEST, this.oParentObject.w_MVANNPRO, this.oParentObject.w_MVCLADOC, this.oParentObject.w_MVEMERIC)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if this.w_OK And this.w_CFUNC="Edit" AND this.oParentObject.w_OLDMVALFDOC <> this.oParentObject.w_MVALFDOC
      * --- In variazione, se � stata modificata la serie bisogna ripristinare il progressivo della vecchia serie
      GSAR_BRP(this,this.oParentObject.w_MVFLVEAC, this.oParentObject.w_MVANNDOC, this.oParentObject.w_MVPRD, this.oParentObject.w_OLDMVNUMDOC, this.oParentObject.w_OLDMVALFDOC, this.oParentObject.w_MVPRP, this.oParentObject.w_MVNUMEST, this.oParentObject.w_MVALFEST, this.oParentObject.w_MVANNPRO, this.oParentObject.w_MVCLADOC, this.oParentObject.w_MVEMERIC)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Inoltre, se il numero documento rimane lo stesso, bisogna comunque forzare l'aggiornamento del progressivo
      if this.oParentObject.w_OLDMVNUMDOC = this.oParentObject.w_MVNUMDOC
        GSAR_BRP(this,this.oParentObject.w_MVFLVEAC, this.oParentObject.w_MVANNDOC, this.oParentObject.w_MVPRD, this.oParentObject.w_MVNUMDOC-1, this.oParentObject.w_MVALFDOC, this.oParentObject.w_MVPRP, this.oParentObject.w_MVNUMEST, this.oParentObject.w_MVALFEST, this.oParentObject.w_MVANNPRO, this.oParentObject.w_MVCLADOC, this.oParentObject.w_MVEMERIC)
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    * --- Controllo se il documento che sto cancellando � stato generato da una esplosione
    *     documenti per Distinta/Kit. In questo caso devo ripulire il campo MVRIFESP dal documento
    *     di origine per far si che non punti pi� ad un documento che non esiste
    if this.w_OK And this.w_CFUNC = "Query" and g_EACD="S"
      this.w_SERDOCESP = CHKDOCESP(this.oParentObject.w_MVSERIAL)
      if Not Empty(this.w_SERDOCESP)
        * --- Non so se � un documento di prodotti finiti o componenti
        *     Provo prima a pulire le righe
        *     Poi la testata
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVRIFESC ="+cp_NullLink(cp_ToStrODBC(Space(10)),'DOC_DETT','MVRIFESC');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDOCESP);
              +" and MVRIFESC = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                 )
        else
          update (i_cTable) set;
              MVRIFESC = Space(10);
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_SERDOCESP;
              and MVRIFESC = this.oParentObject.w_MVSERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if i_Rows=0
          * --- Write into DOC_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVRIFESP ="+cp_NullLink(cp_ToStrODBC(Space(10)),'DOC_MAST','MVRIFESP');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERDOCESP);
                   )
          else
            update (i_cTable) set;
                MVRIFESP = Space(10);
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERDOCESP;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
    endif
    * --- Se siamo in AlterEgoTop
    if this.w_OK AND IsAlt()
      this.Page_13()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      gsal_baf(this,this.oParentObject.w_MVSERIAL,"D",Cp_chartodate("  -  -    "),Cp_chartodate("  -  -    "),"A")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      if this.w_CFUNC="Query" and this.w_OK
        * --- Try
        local bErr_04E0DD60
        bErr_04E0DD60=bTrsErr
        this.Try_04E0DD60()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_04E0DD60
        * --- End
      endif
    endif
    if this.w_OK AND lower(this.w_PADRE.class)=="tgsar_bgd"
      this.w_PADRE.w_TmpFido = this.w_TmpFido
      this.w_PADRE.w_FIDS4 = this.w_FIDS4
      this.w_PADRE.w_FIDS5 = this.w_FIDS5
      this.w_PADRE.w_FIDS6 = this.w_FIDS6
    endif
    if Not this.w_OK AND lower(this.w_PADRE.class)<>"tgsar_bgd"
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
    endif
  endproc
  proc Try_04DEB078()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SIT_FIDI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SIT_FIDI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SIT_FIDI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SIT_FIDI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"FIIMPORD =FIIMPORD- "+cp_ToStrODBC(this.oParentObject.w_FIDO4);
      +",FIIMPDDT =FIIMPDDT- "+cp_ToStrODBC(this.oParentObject.w_FIDO5);
      +",FIIMPFAT =FIIMPFAT- "+cp_ToStrODBC(this.oParentObject.w_FIDO6);
          +i_ccchkf ;
      +" where ";
          +"FICODCLI = "+cp_ToStrODBC(this.oParentObject.w_XCONORN);
             )
    else
      update (i_cTable) set;
          FIIMPORD = FIIMPORD - this.oParentObject.w_FIDO4;
          ,FIIMPDDT = FIIMPDDT - this.oParentObject.w_FIDO5;
          ,FIIMPFAT = FIIMPFAT - this.oParentObject.w_FIDO6;
          &i_ccchkf. ;
       where;
          FICODCLI = this.oParentObject.w_XCONORN;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04E14720()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if Not this.w_ANFIDO
      * --- Insert into SIT_FIDI
      i_nConn=i_TableProp[this.SIT_FIDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SIT_FIDI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SIT_FIDI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"FICODCLI"+",FIIMPORD"+",FIIMPDDT"+",FIIMPFAT"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_XCONORN),'SIT_FIDI','FICODCLI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FID4),'SIT_FIDI','FIIMPORD');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FID5),'SIT_FIDI','FIIMPDDT');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FID6),'SIT_FIDI','FIIMPFAT');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'FICODCLI',this.oParentObject.w_XCONORN,'FIIMPORD',this.w_FID4,'FIIMPDDT',this.w_FID5,'FIIMPFAT',this.w_FID6)
        insert into (i_cTable) (FICODCLI,FIIMPORD,FIIMPDDT,FIIMPFAT &i_ccchkf. );
           values (;
             this.oParentObject.w_XCONORN;
             ,this.w_FID4;
             ,this.w_FID5;
             ,this.w_FID6;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    else
      * --- Write into SIT_FIDI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SIT_FIDI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SIT_FIDI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SIT_FIDI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"FIIMPORD =FIIMPORD+ "+cp_ToStrODBC(this.w_FID4);
        +",FIIMPDDT =FIIMPDDT+ "+cp_ToStrODBC(this.w_FID5);
        +",FIIMPFAT =FIIMPFAT+ "+cp_ToStrODBC(this.w_FID6);
            +i_ccchkf ;
        +" where ";
            +"FICODCLI = "+cp_ToStrODBC(this.oParentObject.w_XCONORN);
               )
      else
        update (i_cTable) set;
            FIIMPORD = FIIMPORD + this.w_FID4;
            ,FIIMPDDT = FIIMPDDT + this.w_FID5;
            ,FIIMPFAT = FIIMPFAT + this.w_FID6;
            &i_ccchkf. ;
         where;
            FICODCLI = this.oParentObject.w_XCONORN;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return
  proc Try_04E0DD60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Aggiorno riferimento proforma nelle fatture di acconto
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVRIFPRO ="+cp_NullLink(cp_ToStrODBC(Space(10)),'DOC_MAST','MVRIFPRO');
          +i_ccchkf ;
      +" where ";
          +"MVRIFPRO = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
             )
    else
      update (i_cTable) set;
          MVRIFPRO = Space(10);
          &i_ccchkf. ;
       where;
          MVRIFPRO = this.oParentObject.w_MVSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questa Parte viene valutata solo se e' Stata Inserita, Variata o eliminata una Riga associata ad un Documento di Import
    * --- Riaggiorna la Quantita' da Evadere sul Documento Origine e Storna la Stessa dai Saldi
    * --- Legge le Quantita' dal Doc Origine (MVQTAEVA si presume gia' aggiornata attraverso i Link in GSVE_MDV)
    this.w_RIFMAT = Space(5)
    this.w_RIFMAG = Space(5)
    this.w_ORDUBI = Space(20)
    this.w_ORDUB2 = Space(20)
    this.w_ORDLOT = Space(20)
    this.w_F2IMPE = " "
    this.w_F2ORDI = " "
    this.w_F2RISE = " "
    this.w_OFLEVAS = " "
    this.w_FLORDI = " "
    this.w_FLIMPE = " "
    this.w_FLRISE = " "
    this.w_FLULCA = " "
    this.w_FLULPV = " "
    this.w_FLCASC = " "
    this.w_OIMPEVA = 0
    this.w_OIMPNAZ = 0
    this.w_RVALULT = 0
    this.w_RPREZZO = 0
    this.w_RVALRIG = 0
    this.w_OVALRIG = 0
    this.w_KEYSAL = Space(20)
    this.w_OIMPDAE = 0
    this.w_OQTAEV1 = 0
    this.w_OQTAMOV = 0
    this.w_OQTAEVA = 0
    this.w_OQTAUM1 = 0
    this.w_OQTASAL = 0
    this.w_DATGEN = cp_CharToDate("  -  -  ")
    this.w_NQTASAL = 0
    this.w_NQTAEV1 = 0
    this.w_ORDFLCOCO = " "
    this.w_ORDFLORCO = " "
    this.w_ORIMPCOM = 0
    this.w_ORD_PRZ = 0
    this.w_CO1CODCOM = Space(15)
    this.w_CO1CODCOS = Space(5)
    this.w_CODATTCOM = Space(15)
    this.w_OSCONT1 = 0
    this.w_OSCONT2 = 0
    this.w_OSCONT3 = 0
    this.w_OSCONT4 = 0
    this.w_OIMPSCO = 0
    this.w_OIMPACC = 0
    this.w_RVALMAG = 0
    * --- Read from DOC_DETT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "*"+;
        " from "+i_cTable+" DOC_DETT where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF);
            +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        *;
        from (i_cTable) where;
            MVSERIAL = this.w_SERRIF;
            and CPROWNUM = this.w_ROWRIF;
            and MVNUMRIF = this.w_NUMRIF;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CODATTCOM = NVL(cp_ToDate(_read_.MVCODATT),cp_NullValue(_read_.MVCODATT))
      this.w_CO1CODCOM = NVL(cp_ToDate(_read_.MVCODCOM),cp_NullValue(_read_.MVCODCOM))
      this.w_CO1CODCOS = NVL(cp_ToDate(_read_.MVCODCOS),cp_NullValue(_read_.MVCODCOS))
      this.w_RIFMAG = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
      this.w_RIFMAT = NVL(cp_ToDate(_read_.MVCODMAT),cp_NullValue(_read_.MVCODMAT))
      this.w_DATGEN = NVL(cp_ToDate(_read_.MVDATGEN),cp_NullValue(_read_.MVDATGEN))
      this.w_F2IMPE = NVL(cp_ToDate(_read_.MVF2IMPE),cp_NullValue(_read_.MVF2IMPE))
      this.w_F2ORDI = NVL(cp_ToDate(_read_.MVF2ORDI),cp_NullValue(_read_.MVF2ORDI))
      this.w_F2RISE = NVL(cp_ToDate(_read_.MVF2RISE),cp_NullValue(_read_.MVF2RISE))
      this.w_ORDFLCOCO = NVL(cp_ToDate(_read_.MVFLCOCO),cp_NullValue(_read_.MVFLCOCO))
      this.w_OFLEVAS = NVL(cp_ToDate(_read_.MVFLEVAS),cp_NullValue(_read_.MVFLEVAS))
      this.w_FLIMPE = NVL(cp_ToDate(_read_.MVFLIMPE),cp_NullValue(_read_.MVFLIMPE))
      this.w_FLOMAG = NVL(cp_ToDate(_read_.MVFLOMAG),cp_NullValue(_read_.MVFLOMAG))
      this.w_ORDFLORCO = NVL(cp_ToDate(_read_.MVFLORCO),cp_NullValue(_read_.MVFLORCO))
      this.w_FLORDI = NVL(cp_ToDate(_read_.MVFLORDI),cp_NullValue(_read_.MVFLORDI))
      this.w_FLRISE = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
      this.w_FLULCA = NVL(cp_ToDate(_read_.MVFLULCA),cp_NullValue(_read_.MVFLULCA))
      this.w_FLULPV = NVL(cp_ToDate(_read_.MVFLULPV),cp_NullValue(_read_.MVFLULPV))
      this.w_ORIMPCOM = NVL(cp_ToDate(_read_.MVIMPCOM),cp_NullValue(_read_.MVIMPCOM))
      this.w_OIMPEVA = NVL(cp_ToDate(_read_.MVIMPEVA),cp_NullValue(_read_.MVIMPEVA))
      this.w_OIMPNAZ = NVL(cp_ToDate(_read_.MVIMPNAZ),cp_NullValue(_read_.MVIMPNAZ))
      this.w_KEYSAL = NVL(cp_ToDate(_read_.MVKEYSAL),cp_NullValue(_read_.MVKEYSAL))
      this.w_ORD_PRZ = NVL(cp_ToDate(_read_.MVPREZZO),cp_NullValue(_read_.MVPREZZO))
      this.w_OQTAEVA = NVL(cp_ToDate(_read_.MVQTAEVA),cp_NullValue(_read_.MVQTAEVA))
      this.w_OQTAEV1 = NVL(cp_ToDate(_read_.MVQTAEV1),cp_NullValue(_read_.MVQTAEV1))
      this.w_OQTASAL = NVL(cp_ToDate(_read_.MVQTASAL),cp_NullValue(_read_.MVQTASAL))
      this.w_OQTAMOV = NVL(cp_ToDate(_read_.MVQTAMOV),cp_NullValue(_read_.MVQTAMOV))
      this.w_OQTAUM1 = NVL(cp_ToDate(_read_.MVQTAUM1),cp_NullValue(_read_.MVQTAUM1))
      this.w_RVALMAG = NVL(cp_ToDate(_read_.MVVALMAG),cp_NullValue(_read_.MVVALMAG))
      this.w_RVALULT = NVL(cp_ToDate(_read_.MVVALULT),cp_NullValue(_read_.MVVALULT))
      this.w_FLCASC = NVL(cp_ToDate(_read_.MVFLCASC),cp_NullValue(_read_.MVFLCASC))
      this.w_OSCONT1 = NVL(cp_ToDate(_read_.MVSCONT1),cp_NullValue(_read_.MVSCONT1))
      this.w_OSCONT2 = NVL(cp_ToDate(_read_.MVSCONT2),cp_NullValue(_read_.MVSCONT2))
      this.w_OSCONT3 = NVL(cp_ToDate(_read_.MVSCONT3),cp_NullValue(_read_.MVSCONT3))
      this.w_OSCONT4 = NVL(cp_ToDate(_read_.MVSCONT4),cp_NullValue(_read_.MVSCONT4))
      this.w_OVALRIG = NVL(cp_ToDate(_read_.MVVALRIG),cp_NullValue(_read_.MVVALRIG))
      this.w_OIMPACC = NVL(cp_ToDate(_read_.MVIMPACC),cp_NullValue(_read_.MVIMPACC))
      this.w_OIMPSCO = NVL(cp_ToDate(_read_.MVIMPSCO),cp_NullValue(_read_.MVIMPSCO))
      this.w_OEFFEVA = NVL(cp_ToDate(_read_.MVEFFEVA),cp_NullValue(_read_.MVEFFEVA))
      this.w_SERRIF_V = NVL(cp_ToDate(_read_.MVSERRIF),cp_NullValue(_read_.MVSERRIF))
      this.w_ROWRIF_V = NVL(cp_ToDate(_read_.MVROWRIF),cp_NullValue(_read_.MVROWRIF))
      this.w_FGMRIF_V = NVL(cp_ToDate(_read_.MVFLELGM),cp_NullValue(_read_.MVFLELGM))
      this.w_FLARIF_V = NVL(cp_ToDate(_read_.MVFLARIF),cp_NullValue(_read_.MVFLARIF))
      this.w_ORDUBI = NVL(cp_ToDate(_read_.MVCODUBI),cp_NullValue(_read_.MVCODUBI))
      this.w_ORDUB2 = NVL(cp_ToDate(_read_.MVCODUB2),cp_NullValue(_read_.MVCODUB2))
      this.w_ORDLOT = NVL(cp_ToDate(_read_.MVCODLOT),cp_NullValue(_read_.MVCODLOT))
      this.w_COCODART = NVL(cp_ToDate(_read_.MVCODART),cp_NullValue(_read_.MVCODART))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Ricostruisco la variabile per pilotare la rivalorizzazione leggendo
    *     w_FGMRIF_V dal documento originario
    this.w_OIMPDAE = this.w_ORD_PRZ
    this.w_OIMPEVA_R = this.w_OIMPEVA
    this.w_FGMRIF = IIF(this.oParentObject.w_MVCLADOC $ "FA-NC", "S", "N")
    this.w_FGMRIF = IIF( this.w_TIPRIG $ "RM" AND this.w_FGMRIF_V="S" AND this.w_FLELGM<>"S", this.w_FGMRIF, "N")
    * --- Ho riga che storna l'evasione o fattura raggruppata, in quest'ultimo caso il 
    *     campo MVFLARIF � bianco
    this.w_FGMRIF = IIF( (this.w_FLARIF<>" " Or this.w_FAT_RAG ) AND this.w_SRV $ "AU", this.w_FGMRIF, "N")
    if lower(this.w_PADRE.class)<>"tgsar_bgd"
      if this.w_FGMRIF="S"
        * --- Leggo data documento origine e catagoria documento e la confronto con data ultimo carico / scarico
        *     (lette dal transitorio)
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVDATDOC,MVCLADOC"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVDATDOC,MVCLADOC;
            from (i_cTable) where;
                MVSERIAL = this.w_SERRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_RDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
          this.w_RCLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Rivalorizzo solo se documento di origine � un DDT o documento interno
        this.w_FGMRIF = IIF( (this.w_RCLADOC $ "DT-DI" ) , this.w_FGMRIF, "N")
        * --- Verifico se la riga evasa � stata oggetto di ripartizione spese...
        if this.w_FGMRIF="S"
          * --- Read from ADDE_SPE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ADDE_SPE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ADDE_SPE_idx,2],.t.,this.ADDE_SPE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "RLSERFAT"+;
              " from "+i_cTable+" ADDE_SPE where ";
                  +"RLSERDOC = "+cp_ToStrODBC(this.w_SERRIF);
                  +" and RLNUMDOC = "+cp_ToStrODBC(this.w_ROWRIF);
                  +" and RLROWDOC = "+cp_ToStrODBC(this.w_NUMRIF);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              RLSERFAT;
              from (i_cTable) where;
                  RLSERDOC = this.w_SERRIF;
                  and RLNUMDOC = this.w_ROWRIF;
                  and RLROWDOC = this.w_NUMRIF;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SERFAT = NVL(cp_ToDate(_read_.RLSERFAT),cp_NullValue(_read_.RLSERFAT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if Not Empty( this.w_SERFAT )
            * --- Read from DOC_MAST
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DOC_MAST_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "MVDATDOC,MVNUMDOC,MVALFDOC"+;
                " from "+i_cTable+" DOC_MAST where ";
                    +"MVSERIAL = "+cp_ToStrODBC(this.w_SERFAT);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                MVDATDOC,MVNUMDOC,MVALFDOC;
                from (i_cTable) where;
                    MVSERIAL = this.w_SERFAT;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_RDAT = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
              this.w_RNUM = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
              this.w_RALF = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            * --- Leggo estremi fattura spese...
            this.w_MESS = "Riga [%1]: impossibile confermare, la riga che evade � stata oggetto di ripartizione spese%0Annullare la ripartizione spese prima di rivalorizzare%0Fattura spese n. %2 del %3"
            this.w_MESS = ah_Msgformat(this.w_MESS, this.w_NR, ALLTRIM(STR(this.w_RNUM)) + IIF(EMPTY(this.w_RALF), " ", "/"+Alltrim(this.w_RALF)), DTOC(this.w_RDAT) )
            this.w_OK = .F.
          endif
        endif
      endif
    endif
    if this.w_OK
      this.w_IMPNAZ_V = this.w_OIMPNAZ
      this.w_RPREZZO = this.w_OIMPDAE
      this.w_RVALRIG = this.w_OVALRIG
      this.w_STIMPCOM = this.w_ORIMPCOM
      if this.w_FGMRIF="S" OR this.w_EVARIF="S" OR this.w_FAT_RAG
        if Not this.w_FAT_RAG
          * --- Filtra solo se la riga e' Eliminata, Inserita o Variata (puo' essere una operazione piuttosto pesante)
          this.w_FLAPP = IIF(this.w_SRV="A" AND this.w_FLDEL=.F., .T., .F.)
          this.w_NQTAEVA = 0
          this.w_NQTAEV1 = 0
          this.w_NQTASAL = 0
        endif
        * --- x Scrivere nuovo Valore Riga se aggiornamento
        if this.w_FGMRIF="S"
          * --- Se il documento di origine movimenta il consuntivo rivalorizzo la produzione su commessa
          if Not Empty ( this.w_ORDFLCOCO ) 
            * --- Se il documento che evade non ha la commessa IMPCOM vale 0 debbo per 
            *     cui appoggiarmi a MVIMPNAZ
            if this.w_IMPCOM=0 
              * --- Se importo di commessa non calcolato sul documento eseguo
              *     il calcolo, verifico la valuta della commessa nel doc. di origine
              if this.w_CO1CODCOM <> this.oParentObject.w_MVCODCOM
                * --- Read from CAN_TIER
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.CAN_TIER_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "CNCODVAL"+;
                    " from "+i_cTable+" CAN_TIER where ";
                        +"CNCODCAN = "+cp_ToStrODBC(this.w_CO1CODCOM);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    CNCODVAL;
                    from (i_cTable) where;
                        CNCODCAN = this.w_CO1CODCOM;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CODVAL = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Se devo applicare un cambio...
                if this.oParentObject.w_MVVALNAZ<>this.w_CODVAL
                  * --- Read from VALUTE
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.VALUTE_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "VADECTOT"+;
                      " from "+i_cTable+" VALUTE where ";
                          +"VACODVAL = "+cp_ToStrODBC(this.w_CODVAL);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      VADECTOT;
                      from (i_cTable) where;
                          VACODVAL = this.w_CODVAL;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_LDECCOM = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                else
                  this.w_LDECCOM = this.oParentObject.w_DECTOP
                endif
              else
                this.w_CODVAL = this.oParentObject.w_COCODVAL
                this.w_LDECCOM = this.oParentObject.w_DECCOM
              endif
              if this.oParentObject.w_MVVALNAZ=this.w_CODVAL
                this.w_STIMPCOM = this.w_IMPNAZ
              else
                this.w_STIMPCOM = CAIMPCOM( IIF(Empty(this.w_CO1CODCOM),"S", " "), this.oParentObject.w_MVVALNAZ, this.w_CODVAL, this.w_IMPNAZ, this.oParentObject.w_CAONAZ, IIF(EMPTY(this.oParentObject.w_MVDATDOC), this.oParentObject.w_MVDATREG, this.oParentObject.w_MVDATDOC), this.w_LDECCOM )
              endif
            else
              this.w_STIMPCOM = this.w_IMPCOM
            endif
          endif
          this.w_OIMPNAZ = this.w_IMPNAZ
          if this.w_QTAUM1<>this.w_OQTAUM1 AND this.w_QTAUM1<>0
            * --- Se c'e' stata una evasione parziale ricalcola per il totale importo Origine
            this.w_OIMPNAZ = cp_ROUND((this.w_IMPNAZ/this.w_QTAUM1) * this.w_OQTAUM1, this.oParentObject.w_DECTOP )
            this.w_STIMPCOM = IIF ( Not Empty ( this.w_ORDFLCOCO ) , cp_ROUND((this.w_STIMPCOM/this.w_QTAUM1) * this.w_OQTAUM1, IIF(this.w_CODVAL=g_CODLIR,0,2) ) , this.w_STIMPCOM )
          endif
          * --- Se rivalorizzo ed il documento che evade � una fattura 
          *     scrivo nel documento d'origine anche prezzo e sconti valore di riga
          *     e di magazzino in valuta di documento.
          *     La rivalorizzazione non storna in cancellazione eventuali rivalorizzazioni
          *     impostate
          if this.oParentObject.w_MVCLADOC="FA" And Not this.w_FLDEL
            this.w_OSCONT1 = this.w_SCONT1
            this.w_OSCONT2 = this.w_SCONT2
            this.w_OSCONT3 = this.w_SCONT3
            this.w_OSCONT4 = this.w_SCONT4
            this.w_RPREZZO = this.w_PREZZO
            * --- Svolgo la stessa proporzione anche per questi campi partendo dal dato della fattura..
            *     Se il documento � da rivalorizzare deve NECESSARIAMENTE essere
            *     espresso con la stessa valuta del documento che lo evade.
            *     Unica eccezzione a ci� � un movimento in Lire evadibile con uno in Euro.
            *     Questa casistica � escluda in quanto:
            *     a) Un DDT diviene fattura entro il mese
            *     b) Non pu� esistere un documento da rivalorizzare in Lire oggi (2003)
            if this.w_QTAUM1<>this.w_OQTAUM1 AND this.w_QTAUM1<>0
              * --- Se c'e' stata una evasione parziale ricalcola per il totale importo Origine
              this.w_RVALRIG = cp_ROUND((this.w_VALRIG/this.w_QTAUM1) * this.w_OQTAUM1, this.oParentObject.w_DECTOP )
              this.w_RVALMAG = cp_ROUND((this.w_VALMAG/this.w_QTAUM1) * this.w_OQTAUM1, this.oParentObject.w_DECTOP )
            else
              this.w_RVALRIG = this.w_VALRIG
              this.w_RVALMAG = this.w_VALMAG
            endif
            * --- Aggiornamento ultimo costo/prezzo
            * --- Avviene solo se il documento destinazione non movimenta il magazzino,
            *     cio� se MVKEYSAL � vuoto.
            if this.w_FLOMAG<>"S" And this.w_FLCASC$"-+" And Not Empty(this.w_KEYSAL) And Not Empty(this.w_RIFMAG) AND EMPTY(NVL(this.oParentObject.w_MVKEYSAL, Space(20))) AND lower(this.w_PADRE.class)<>"tgsar_bgd"
              this.w_FLULPV = IIF(this.w_FLCASC="-" AND this.w_RDATDOC>=this.oParentObject.w_ULTSCA AND this.w_RPREZZO<>0, "=", " ")
              this.w_FLULCA = IIF(this.w_FLCASC="+" AND this.w_RDATDOC>=this.oParentObject.w_ULTCAR AND this.w_RPREZZO<>0, "=", " ")
              * --- Documenti con stessa valuta di conto, non serve convertire
              this.w_RVALULT = IIF(this.w_OQTAUM1=0, 0, cp_ROUND(this.w_OIMPNAZ/this.w_OQTAUM1, this.oParentObject.w_DECTOU))
              if this.w_FLULPV = "="
                * --- Try
                local bErr_04D74748
                bErr_04D74748=bTrsErr
                this.Try_04D74748()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                endif
                bTrsErr=bTrsErr or bErr_04D74748
                * --- End
              endif
              if this.w_FLULCA = "="
                * --- Try
                local bErr_04D74B68
                bErr_04D74B68=bTrsErr
                this.Try_04D74B68()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                endif
                bTrsErr=bTrsErr or bErr_04D74B68
                * --- End
              endif
            endif
          endif
          if g_COMM="S" And Not Empty ( this.w_ORDFLCOCO ) AND lower(this.w_PADRE.class)<>"tgsar_bgd"
            this.w_TmpN = this.w_STIMPCOM - this.w_ORIMPCOM
            if this.w_TmpN<> 0
              * --- Write into MA_COSTI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.MA_COSTI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
                i_cOp1=cp_SetTrsOp(this.w_ORDFLCOCO,'CSCONSUN','this.w_TmpN',this.w_TmpN,'update',i_nConn)
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CSCONSUN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSCONSUN');
                    +i_ccchkf ;
                +" where ";
                    +"CSCODCOM = "+cp_ToStrODBC(this.w_CO1CODCOM);
                    +" and CSTIPSTR = "+cp_ToStrODBC("A");
                    +" and CSCODMAT = "+cp_ToStrODBC(this.w_CODATTCOM);
                    +" and CSCODCOS = "+cp_ToStrODBC(this.w_CO1CODCOS);
                       )
              else
                update (i_cTable) set;
                    CSCONSUN = &i_cOp1.;
                    &i_ccchkf. ;
                 where;
                    CSCODCOM = this.w_CO1CODCOM;
                    and CSTIPSTR = "A";
                    and CSCODMAT = this.w_CODATTCOM;
                    and CSCODCOS = this.w_CO1CODCOS;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
        endif
        this.w_APPSAL = 0
        if lower(this.w_PADRE.class)<>"tgsar_bgd"
          if Not this.w_FAT_RAG
            if this.w_TIPRIG<>"F" 
              * --- Solo la prima riga che evade storna il saldo di magazzino.
              *     Quindi se sono in fase di conferma:
              *      - ricerco una riga prima di me non cancellata. 
              *     
              *      - Se sono in una riga cancellata ricerco o una riga cancellata 
              *     prima di me oppure una qualsiasi riga modificata.
              *     
              *     Se invece sono in cancellazione dell'intero documento:
              *      - ricerco una riga prima di me (stato indifferente)
              this.w_ORECO = IIF( Eof( this.w_PADRE.cTrsName ) , RECNO( this.w_PADRE.cTrsName )-1 , RECNO( this.w_PADRE.cTrsName) )
              if this.w_CFUNC="Query"
                * --- F5
                this.w_NUMREC = this.w_PADRE.Search( "NVL(t_MVSERRIF, Space(10)) = "+cp_ToStrODBC(this.w_SERRIF)+" AND NVL(t_MVROWRIF, 0) = "+cp_ToStrODBC(this.w_ROWRIF)+" AND CPROWNUM < "+cp_ToStrODBC(this.w_ROWORD) )
              else
                if this.w_FLDEL
                  * --- Riga cancellata..
                  this.w_NUMREC = this.w_PADRE.Search( "NVL(t_MVSERRIF, Space(10)) = "+cp_ToStrODBC(this.w_SERRIF)+" AND NVL(t_MVROWRIF, 0) = "+cp_ToStrODBC(this.w_ROWRIF)+" AND CPROWNUM <> "+cp_ToStrODBC(this.w_ROWORD)+ " AND (( I_SRV='U' Or (not DELETED() AND I_SRV='A' ))Or (( Deleted() And i_Srv<>'A' ) And  CPROWNUM < "+cp_ToStrODBC(this.w_ROWORD)+ ")) ")
                else
                  * --- Riga modificata / riga aggiunta
                  this.w_NUMREC = this.w_PADRE.Search( "NVL(t_MVSERRIF, Space(10)) = "+cp_ToStrODBC(this.w_SERRIF)+" AND NVL(t_MVROWRIF, 0) = "+cp_ToStrODBC(this.w_ROWRIF)+" AND CPROWNUM < "+cp_ToStrODBC(this.w_ROWORD)+ " AND ( I_SRV='U' Or (not DELETED() AND I_SRV='A' )) ")
                endif
              endif
              this.w_PADRE.SetRow(this.w_ORECO)     
              * --- Non eseguo storno dei saldi perch� esiste una riga che ha gi� stornato
              *     x la riga evasa...
              if this.w_NUMREC=-1
                * --- Valorizzo w_APPSAL per stornare i saldi di magazzino...
                if this.oParentObject.w_MVFLERIF="S" And not this.w_FLDEL
                  this.w_APPSAL = -this.w_OQTASAL
                else
                  this.w_APPSAL = -this.w_OQTASAL + ( this.w_OQTAUM1-this.w_OQTAEV1 )
                endif
              endif
            endif
            do case
              case this.w_FLARIF="+"
                this.w_NQTASAL = this.w_OQTASAL+ IIF(this.w_FLDEL,this.w_QTAIM1,-(this.w_QTAIM1-this.w_OQTAIM1))
              case this.w_FLARIF="-"
                this.w_NQTASAL = this.w_OQTASAL + IIF(this.w_FLDEL,-this.w_QTAIM1,(this.w_QTAIM1-this.w_OQTAIM1))
                this.w_APPSAL = -this.w_APPSAL
            endcase
            * --- Se Riga Forfettaria storna l'Importo Evaso
            this.w_OPREZZO = IIF(this.w_FLDEL, 0, this.w_PREZZO)-this.w_OPREZZO
            if this.oParentObject.w_MVCODVAL=g_PERVAL AND this.w_OCAORIF<>0 AND this.w_OCAORIF<>1
              * --- Se Documento Destinazione e' in EURO e il doc di Origine e' in altra valuta converte a quest'ultima...
              this.w_OPREZZO = cp_ROUND(this.w_OPREZZO * this.w_OCAORIF, 4)
            endif
            if this.w_TIPRIG="F"
              do case
                case this.w_FLARIF="+" 
                  this.w_OIMPEVA = this.w_OIMPEVA + this.w_OPREZZO
                case this.w_FLARIF="-" 
                  this.w_OIMPEVA = this.w_OIMPEVA - this.w_OPREZZO
                case this.w_FLARIF="=" 
                  this.w_OIMPEVA = this.w_OPREZZO
              endcase
              * --- Controlla se totalmente evaso per evitare eventuali differenze di conversione
              if this.w_OCAORIF<>0 AND this.w_OCAORIF<>1 AND this.w_OIMPEVA<>0 AND this.w_OIMPDAE<>0
                * --- Se gli importi arrotondati al decimale Unitario sono Uguali chiude o riapre tutto
                this.w_APPO = cp_ROUND(this.w_OIMPDAE, IIF(this.w_OCAORIF>1000, 0, 2))
                this.w_APPO1 = cp_ROUND(this.w_OIMPEVA, IIF(this.w_OCAORIF>1000, 0, 2))
                this.w_OIMPEVA = IIF(this.w_APPO=this.w_APPO1, this.w_OIMPDAE, IIF(this.w_APPO1=0, 0, this.w_OIMPEVA))
              endif
            else
              this.w_OIMPEVA = 0
            endif
          endif
          if this.w_EVARIF="S" OR this.w_FAT_RAG or w_EVANOIMPCOM
            * --- Se Evasione da Import  (anche se Rivalorizzo)
            * --- Try
            local bErr_04D9F4F0
            bErr_04D9F4F0=bTrsErr
            this.Try_04D9F4F0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_04D9F4F0
            * --- End
            if g_COMM="S" AND this.w_ORDFLORCO $ "-+=" AND NOT EMPTY(this.w_CO1CODCOM) AND NOT EMPTY(this.w_CODATTCOM) AND NOT EMPTY(this.w_CO1CODCOS)
              * --- Calcolo la quantit� x Aggiornare i saldi Costi Attivit�
              this.w_COAPPSAL = this.w_NQTASAL - this.w_OQTASAL
              * --- Storna impegnato ordinato Saldi attivit� di commessa
              if this.w_COAPPSAL<>0 OR this.w_OIMPEVA<>0 OR this.w_ORD_EVA<>0
                this.Page_7()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
              endif
            endif
            if this.w_APPSAL<>0 AND NOT EMPTY(this.w_KEYSAL) 
              * --- Se Variata Qta da Evadere Aggiorna anche i Saldi Ordinato/Impegnato
              if NOT EMPTY(this.w_FLRISE+this.w_FLORDI+this.w_FLIMPE) AND NOT EMPTY(this.w_RIFMAG)
                * --- Try
                local bErr_04D9D390
                bErr_04D9D390=bTrsErr
                this.Try_04D9D390()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                endif
                bTrsErr=bTrsErr or bErr_04D9D390
                * --- End
              endif
              * --- Eventuale Mag.Collegato
              if NOT EMPTY(this.w_F2RISE+this.w_F2ORDI+this.w_F2IMPE) AND NOT EMPTY(this.w_RIFMAT)
                * --- Try
                local bErr_04D9EB30
                bErr_04D9EB30=bTrsErr
                this.Try_04D9EB30()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- accept error
                  bTrsErr=.f.
                endif
                bTrsErr=bTrsErr or bErr_04D9EB30
                * --- End
              endif
            endif
          else
            * --- Se solo Rivalorizzazione
            * --- Try
            local bErr_04D98170
            bErr_04D98170=bTrsErr
            this.Try_04D98170()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_04D98170
            * --- End
          endif
        endif
        * --- Storno eventuali righe Importate per calcolo Rischio
        if Not this.w_FAT_RAG And Not this.w_FLDEL AND this.w_TIPRIG<>"D" AND (this.w_CFUNC="Load" Or (this.w_CFUNC="Edit" And this.oParentObject.w_OFLPROV="S") ) AND this.oParentObject.w_MVFLPROV="N" And g_PERFID="S" AND this.oParentObject.w_FLFIDO="S" AND this.oParentObject.w_FLRISC$"SD" AND this.oParentObject.w_MVTIPCON="C" AND NOT EMPTY(this.oParentObject.w_MVCODCON)
          this.Page_6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.w_FLAGG$"+-" And this.w_TIPRIG="R" And this.w_OK AND lower(this.w_PADRE.class)<>"tgsar_bgd"
          * --- Variazione a valore
          if this.w_FGMRIF="S" 
            * --- Se il documento conteporaneamente rivalorizza e varia il valore
            *     lo segnalo e inibisco la conferma
            this.w_MESS = ah_Msgformat("Riga [%1]: impossibile confermare, verificare la causale di magazzino [variazione a valore attiva]%0Il documento gia rivalorizza la riga d'origine", this.w_NR)
            this.w_OK = .F.
          else
            * --- Garanzia che non vada in loop...
            *     Al decimo antenato esce...
            this.w_LOOP = 10
            * --- Flag movimento fiscale del documento antenato
            this.w_FLELGM_V = this.w_FGMRIF_V
            this.w_NUMRIF_V = this.w_NUMRIF
            if this.w_SRV="A" 
              * --- Se il documento evaso non movimenta il magazzino fiscale cerco
              *     un antenato
              if this.w_FLELGM_V<>"S"
                * --- Va all'indietro sul ciclo documentale per determinare la riga
                *     antenata che movimenta il magazzino fiscalmente...
                *     (verifico che la catena sia di evasioni vere Check evasione doc.collegati
                *     w_FLARIF_V=+ o -). Una riga raggruppata ha FLARIF vuoto..
                do while this.w_FLELGM_V<>"S" And Not Empty( this.w_SERRIF_V ) And Not Empty( this.w_FLARIF_V ) And this.w_LOOP>0 
                  this.w_SERRIF_V_AGG = this.w_SERRIF_V
                  this.w_ROWRIF_V_AGG = this.w_ROWRIF_V
                  * --- Read from DOC_DETT
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.DOC_DETT_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "MVSERRIF,MVROWRIF,MVFLELGM,MVIMPNAZ,MVCODCOM,MVCODCOS,MVCODATT,MVFLCOCO,MVFLARIF,MVNUMRIF"+;
                      " from "+i_cTable+" DOC_DETT where ";
                          +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF_V);
                          +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF_V);
                          +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      MVSERRIF,MVROWRIF,MVFLELGM,MVIMPNAZ,MVCODCOM,MVCODCOS,MVCODATT,MVFLCOCO,MVFLARIF,MVNUMRIF;
                      from (i_cTable) where;
                          MVSERIAL = this.w_SERRIF_V;
                          and CPROWNUM = this.w_ROWRIF_V;
                          and MVNUMRIF = this.w_NUMRIF;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_SERRIF_V = NVL(cp_ToDate(_read_.MVSERRIF),cp_NullValue(_read_.MVSERRIF))
                    this.w_ROWRIF_V = NVL(cp_ToDate(_read_.MVROWRIF),cp_NullValue(_read_.MVROWRIF))
                    this.w_FLELGM_V = NVL(cp_ToDate(_read_.MVFLELGM),cp_NullValue(_read_.MVFLELGM))
                    this.w_IMPNAZ_V = NVL(cp_ToDate(_read_.MVIMPNAZ),cp_NullValue(_read_.MVIMPNAZ))
                    this.w_CO1CODCOM = NVL(cp_ToDate(_read_.MVCODCOM),cp_NullValue(_read_.MVCODCOM))
                    this.w_CO1CODCOS = NVL(cp_ToDate(_read_.MVCODCOS),cp_NullValue(_read_.MVCODCOS))
                    this.w_CODATTCOM = NVL(cp_ToDate(_read_.MVCODATT),cp_NullValue(_read_.MVCODATT))
                    this.w_ORDFLCOCO = NVL(cp_ToDate(_read_.MVFLCOCO),cp_NullValue(_read_.MVFLCOCO))
                    this.w_FLARIF_V = NVL(cp_ToDate(_read_.MVFLARIF),cp_NullValue(_read_.MVFLARIF))
                    this.w_NUMRIF_V = NVL(cp_ToDate(_read_.MVNUMRIF),cp_NullValue(_read_.MVNUMRIF))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  this.w_LOOP = this.w_LOOP - 1
                enddo
                * --- Posso uscire dal ciclo perch�:
                *     a) Non ho documenti antenati (ROWRIF=0)
                *     b) Si interrompe l'evasione ed il documento non mov. il magazzino (w_FLARIF vuoto e FLELGM<>'S')
                *     c) Ho trovato un documento che non movimenta il magazzino (FLELGM<>'S')
                *     d) Ho percorso all'indietro 10 passi mi fermo... (w_LOOP=0)
                *     e) Nella catena documentale ho una fattura differita raggruppata (w_NUMRIF=-90)
                if this.w_NUMRIF_V=-90
                  * --- Se nella catena documentale incontro una fattura raggruppata interrompo
                  *     e do un messaggio.
                  *     Il problema � che in questo caso dovrei procedere con una rivalorizzazione
                  *     con proporzione. Cosa indubbiamente fattibile in linea teorica ma che presenta
                  *     problemi in quanto comporterebbe la gestione degli arrotondamente ed una 
                  *     magiore complessita dell'algoritmo di ricerca all'indietro.
                  this.w_MESS = ah_Msgformat("Riga [%1]: impossibile determinare un documento precedente nella catena documentale%0a causa della presenza di un piano di fatturazione raggruppato per articolo [variazione a valore attiva]", this.w_NR)
                  this.w_LOOP = -1
                  this.w_OK = .F.
                else
                  if this.w_LOOP=0 Or this.w_FLELGM_V<>"S" Or ( Empty( this.w_FLARIF_V ) And this.w_FLELGM_V<>"S" )
                    this.w_MESS = ah_Msgformat("Riga [%1]: impossibile determinare un documento precedente nella catena documentale che movimenti il magazzino%0Verificare la causale magazzino associata al documento [variazione a valore attiva]", this.w_NR)
                    this.w_OK = .F.
                  else
                    * --- Se nuova riga w_IMPNAZ_V_AGG parte da zero...
                    this.w_IMPNAZ_V_AGG = 0
                  endif
                endif
              else
                * --- Riferimenti utilizzati per aggiornare il documento antenato che
                *     movimenta il magazzino (w_IMPNAZ_V valorizzato ad inizio pagina)
                this.w_SERRIF_V_AGG = this.w_SERRIF
                this.w_ROWRIF_V_AGG = this.w_ROWRIF
                this.w_IMPNAZ_V_AGG = 0
              endif
            else
              * --- Se modifica/cancello rileggo sulla tabella relazione l'importo
              *     da stornare, leggo inoltre i riferimenti del documento da stornare
              *     
              *     Se attiva produzione su commessa leggo anche commessa
              *     attivit� e codice costo dal documento antenato..
              *     Comunque leggo l'importo originario per verificare, in caso di aggiornamento
              *     che diminuisce il valore, che lo storno non mi valorizzi in negativo il magazzino
              * --- Read from ADDE_SPE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.ADDE_SPE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.ADDE_SPE_idx,2],.t.,this.ADDE_SPE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "RLSERDOC,RLNUMDOC,RLIMPNAZ"+;
                  " from "+i_cTable+" ADDE_SPE where ";
                      +"RLSERFAT = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                      +" and RLNUMFAT = "+cp_ToStrODBC(this.w_ROWORD);
                      +" and RLROWFAT = "+cp_ToStrODBC(this.w_NUMRIF);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  RLSERDOC,RLNUMDOC,RLIMPNAZ;
                  from (i_cTable) where;
                      RLSERFAT = this.oParentObject.w_MVSERIAL;
                      and RLNUMFAT = this.w_ROWORD;
                      and RLROWFAT = this.w_NUMRIF;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_SERRIF_V_AGG = NVL(cp_ToDate(_read_.RLSERDOC),cp_NullValue(_read_.RLSERDOC))
                this.w_ROWRIF_V_AGG = NVL(cp_ToDate(_read_.RLNUMDOC),cp_NullValue(_read_.RLNUMDOC))
                this.w_IMPNAZ_V_AGG = NVL(cp_ToDate(_read_.RLIMPNAZ),cp_NullValue(_read_.RLIMPNAZ))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if g_COMM="S" Or this.w_FLAGG="-"
                * --- Read from DOC_DETT
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.DOC_DETT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "MVCODCOM,MVCODCOS,MVCODATT,MVFLCOCO,MVIMPNAZ"+;
                    " from "+i_cTable+" DOC_DETT where ";
                        +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF_V_AGG);
                        +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF_V_AGG);
                        +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    MVCODCOM,MVCODCOS,MVCODATT,MVFLCOCO,MVIMPNAZ;
                    from (i_cTable) where;
                        MVSERIAL = this.w_SERRIF_V_AGG;
                        and CPROWNUM = this.w_ROWRIF_V_AGG;
                        and MVNUMRIF = this.w_NUMRIF;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_CO1CODCOM = NVL(cp_ToDate(_read_.MVCODCOM),cp_NullValue(_read_.MVCODCOM))
                  this.w_CO1CODCOS = NVL(cp_ToDate(_read_.MVCODCOS),cp_NullValue(_read_.MVCODCOS))
                  this.w_CODATTCOM = NVL(cp_ToDate(_read_.MVCODATT),cp_NullValue(_read_.MVCODATT))
                  this.w_ORDFLCOCO = NVL(cp_ToDate(_read_.MVFLCOCO),cp_NullValue(_read_.MVFLCOCO))
                  this.w_IMPNAZ_V = NVL(cp_ToDate(_read_.MVIMPNAZ),cp_NullValue(_read_.MVIMPNAZ))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
            endif
            if this.w_OK
              * --- Se cancellazione storno l'importo letto nella tabella relazione altrimenti
              *     vado per differenza (se nuovo w_IMPNAZ_V_AGG vale 0)
              this.w_IMPNAZ_V_AGG = IIF( this.w_FLDEL , 0 , this.w_IMPNAZ ) - this.w_IMPNAZ_V_AGG
              * --- Se cambia qualcosa....
              if this.w_IMPNAZ_V_AGG<>0
                if this.w_FLAGG="-" And this.w_IMPNAZ_V_AGG>this.w_IMPNAZ_V
                  * --- Se diminuisco il valore verifico se la diminuzione possa provocare 
                  *     su MVIMPNAZ un valore negativo...
                  this.w_oPART = this.w_oMESS.addmsgpartNL("Riga [%1]: impossibile confermare, valore da stornare a magazzino sul documento antenato [%2]%0superiore al valore gi� presente [%3]")
                  this.w_oPART.addParam( this.w_NR)     
                  this.w_oPART.addParam(Alltrim(Tran( this.w_IMPNAZ_V_AGG , v_PV[38+VVL] )))     
                  this.w_oPART.addParam(Alltrim(Tran( this.w_IMPNAZ_V , v_PV[38+VVL] )))     
                  this.w_oMESS.AddMsgPart("Verificare la causale magazzino associata al documento [variazione a valore attiva]")     
                  this.w_MESS = this.w_oMESS.ComposeMessage()
                  this.w_OK = .F.
                else
                  if this.w_FLDEL
                    * --- Elimino la relazione...
                    * --- Delete from ADDE_SPE
                    i_nConn=i_TableProp[this.ADDE_SPE_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.ADDE_SPE_idx,2])
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    if i_nConn<>0
                      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                            +"RLSERFAT = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                            +" and RLNUMFAT = "+cp_ToStrODBC(this.w_ROWORD);
                            +" and RLROWFAT = "+cp_ToStrODBC(this.w_NUMRIF);
                             )
                    else
                      delete from (i_cTable) where;
                            RLSERFAT = this.oParentObject.w_MVSERIAL;
                            and RLNUMFAT = this.w_ROWORD;
                            and RLROWFAT = this.w_NUMRIF;

                      i_Rows=_tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      * --- Error: delete not accepted
                      i_Error=MSG_DELETE_ERROR
                      return
                    endif
                  else
                    * --- Se nuova riga creo un'entrata nella tabella relazione altrimenti
                    *     modifico il record presente
                    if this.w_SRV="A"
                      * --- Insert into ADDE_SPE
                      i_nConn=i_TableProp[this.ADDE_SPE_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.ADDE_SPE_idx,2])
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_ccchkf=''
                      i_ccchkv=''
                      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ADDE_SPE_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                    " ("+"RLSERFAT"+",RLNUMFAT"+",RLROWFAT"+",RLSERDOC"+",RLNUMDOC"+",RLROWDOC"+",RLIMPNAZ"+i_ccchkf+") values ("+;
                        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'ADDE_SPE','RLSERFAT');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'ADDE_SPE','RLNUMFAT');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIF),'ADDE_SPE','RLROWFAT');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_SERRIF_V_AGG),'ADDE_SPE','RLSERDOC');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_ROWRIF_V_AGG),'ADDE_SPE','RLNUMDOC');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIF),'ADDE_SPE','RLROWDOC');
                        +","+cp_NullLink(cp_ToStrODBC(this.w_IMPNAZ_V_AGG),'ADDE_SPE','RLIMPNAZ');
                             +i_ccchkv+")")
                      else
                        cp_CheckDeletedKey(i_cTable,0,'RLSERFAT',this.oParentObject.w_MVSERIAL,'RLNUMFAT',this.w_ROWORD,'RLROWFAT',this.w_NUMRIF,'RLSERDOC',this.w_SERRIF_V_AGG,'RLNUMDOC',this.w_ROWRIF_V_AGG,'RLROWDOC',this.w_NUMRIF,'RLIMPNAZ',this.w_IMPNAZ_V_AGG)
                        insert into (i_cTable) (RLSERFAT,RLNUMFAT,RLROWFAT,RLSERDOC,RLNUMDOC,RLROWDOC,RLIMPNAZ &i_ccchkf. );
                           values (;
                             this.oParentObject.w_MVSERIAL;
                             ,this.w_ROWORD;
                             ,this.w_NUMRIF;
                             ,this.w_SERRIF_V_AGG;
                             ,this.w_ROWRIF_V_AGG;
                             ,this.w_NUMRIF;
                             ,this.w_IMPNAZ_V_AGG;
                             &i_ccchkv. )
                        i_Rows=iif(bTrsErr,0,1)
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if i_Rows<0 or bTrsErr
                        * --- Error: insert not accepted
                        i_Error=MSG_INSERT_ERROR
                        return
                      endif
                    else
                      * --- Aggiungo sempre, importo in valore assoluto
                      * --- Write into ADDE_SPE
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.ADDE_SPE_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.ADDE_SPE_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.ADDE_SPE_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"RLIMPNAZ =RLIMPNAZ+ "+cp_ToStrODBC(this.w_IMPNAZ_V_AGG);
                            +i_ccchkf ;
                        +" where ";
                            +"RLSERFAT = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                            +" and RLNUMFAT = "+cp_ToStrODBC(this.w_ROWORD);
                            +" and RLROWFAT = "+cp_ToStrODBC(this.w_NUMRIF);
                            +" and RLSERDOC = "+cp_ToStrODBC(this.w_SERRIF_V_AGG);
                            +" and RLNUMDOC = "+cp_ToStrODBC(this.w_ROWRIF_V_AGG);
                            +" and RLROWDOC = "+cp_ToStrODBC(this.w_NUMRIF);
                               )
                      else
                        update (i_cTable) set;
                            RLIMPNAZ = RLIMPNAZ + this.w_IMPNAZ_V_AGG;
                            &i_ccchkf. ;
                         where;
                            RLSERFAT = this.oParentObject.w_MVSERIAL;
                            and RLNUMFAT = this.w_ROWORD;
                            and RLROWFAT = this.w_NUMRIF;
                            and RLSERDOC = this.w_SERRIF_V_AGG;
                            and RLNUMDOC = this.w_ROWRIF_V_AGG;
                            and RLROWDOC = this.w_NUMRIF;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error=MSG_WRITE_ERROR
                        return
                      endif
                    endif
                  endif
                  if g_COMM="S" And Not Empty( this.w_CODATTCOM ) And Not Empty( this.w_CO1CODCOS ) And Not Empty( this.w_ORDFLCOCO )
                    * --- Read from CAN_TIER
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.CAN_TIER_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "CNCODVAL"+;
                        " from "+i_cTable+" CAN_TIER where ";
                            +"CNCODCAN = "+cp_ToStrODBC(this.w_CO1CODCOM);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        CNCODVAL;
                        from (i_cTable) where;
                            CNCODCAN = this.w_CO1CODCOM;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_VALCOM = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    * --- Read from VALUTE
                    i_nOldArea=select()
                    if used('_read_')
                      select _read_
                      use
                    endif
                    i_nConn=i_TableProp[this.VALUTE_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
                    if i_nConn<>0
                      cp_sqlexec(i_nConn,"select "+;
                        "VADECTOT"+;
                        " from "+i_cTable+" VALUTE where ";
                            +"VACODVAL = "+cp_ToStrODBC(this.w_VALCOM);
                             ,"_read_")
                      i_Rows=iif(used('_read_'),reccount(),0)
                    else
                      select;
                        VADECTOT;
                        from (i_cTable) where;
                            VACODVAL = this.w_VALCOM;
                         into cursor _read_
                      i_Rows=_tally
                    endif
                    if used('_read_')
                      locate for 1=1
                      this.w_COMDECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
                      use
                    else
                      * --- Error: sql sentence error.
                      i_Error = MSG_READ_ERROR
                      return
                    endif
                    select (i_nOldArea)
                    if this.w_VALCOM<>this.oParentObject.w_MVVALNAZ
                      this.w_IMPCOM_V = VAL2CAM(this.w_IMPNAZ_V_AGG , this.oParentObject.w_MVVALNAZ , this.w_VALCOM , this.oParentObject.w_CAONAZ , i_DATSYS , this.w_COMDECTOT)
                    else
                      this.w_IMPCOM_V = this.w_IMPNAZ_V_AGG
                    endif
                    * --- Aggiorna i saldi di commessa
                    * --- Write into MA_COSTI
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
                      i_cOp1=cp_SetTrsOp(this.w_FLAGG,'CSCONSUN','this.w_IMPCOM_V',this.w_IMPCOM_V,'update',i_nConn)
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"CSCONSUN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSCONSUN');
                          +i_ccchkf ;
                      +" where ";
                          +"CSCODCOM = "+cp_ToStrODBC(this.w_CO1CODCOM);
                          +" and CSTIPSTR = "+cp_ToStrODBC("A");
                          +" and CSCODMAT = "+cp_ToStrODBC(this.w_CODATTCOM);
                          +" and CSCODCOS = "+cp_ToStrODBC(this.w_CO1CODCOS);
                             )
                    else
                      update (i_cTable) set;
                          CSCONSUN = &i_cOp1.;
                          &i_ccchkf. ;
                       where;
                          CSCODCOM = this.w_CO1CODCOM;
                          and CSTIPSTR = "A";
                          and CSCODMAT = this.w_CODATTCOM;
                          and CSCODCOS = this.w_CO1CODCOS;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  else
                    this.w_IMPCOM_V = 0
                  endif
                  * --- Gestione aggiornamento ultimo prezzo / ultimo costo
                  GSAR_BAC(this, this.w_SERRIF_V_AGG , this.w_ROWRIF_V_AGG , this.w_NUMRIF , iif( this.w_FLAGG="-",-1,1 ) * this.w_IMPNAZ_V_AGG , .T. )
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  * --- Storno dal documento d'origine (primo antenato
                  *      che valorizza il magazzino) il valore del documento attuale
                  * --- Write into DOC_DETT
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.DOC_DETT_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
                    i_cOp1=cp_SetTrsOp(this.w_FLAGG,'MVIMPNAZ','this.w_IMPNAZ_V_AGG',this.w_IMPNAZ_V_AGG,'update',i_nConn)
                    i_cOp2=cp_SetTrsOp(this.w_FLAGG,'MVIMPCOM','this.w_IMPCOM_V',this.w_IMPCOM_V,'update',i_nConn)
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"MVIMPNAZ ="+cp_NullLink(i_cOp1,'DOC_DETT','MVIMPNAZ');
                    +",MVIMPCOM ="+cp_NullLink(i_cOp2,'DOC_DETT','MVIMPCOM');
                        +i_ccchkf ;
                    +" where ";
                        +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF_V_AGG);
                        +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF_V_AGG);
                        +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                           )
                  else
                    update (i_cTable) set;
                        MVIMPNAZ = &i_cOp1.;
                        ,MVIMPCOM = &i_cOp2.;
                        &i_ccchkf. ;
                     where;
                        MVSERIAL = this.w_SERRIF_V_AGG;
                        and CPROWNUM = this.w_ROWRIF_V_AGG;
                        and MVNUMRIF = this.w_NUMRIF;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                endif
              endif
            endif
          endif
        endif
      endif
    endif
  endproc
  proc Try_04D74748()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLCODVAV ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVVALNAZ),'SALDIART','SLCODVAV');
      +",SLDATUPV ="+cp_NullLink(cp_ToStrODBC(this.w_RDATDOC),'SALDIART','SLDATUPV');
      +",SLVALUPV ="+cp_NullLink(cp_ToStrODBC(this.w_RVALULT),'SALDIART','SLVALUPV');
          +i_ccchkf ;
      +" where ";
          +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
          +" and SLCODMAG = "+cp_ToStrODBC(this.w_RIFMAG);
             )
    else
      update (i_cTable) set;
          SLCODVAV = this.oParentObject.w_MVVALNAZ;
          ,SLDATUPV = this.w_RDATDOC;
          ,SLVALUPV = this.w_RVALULT;
          &i_ccchkf. ;
       where;
          SLCODICE = this.w_KEYSAL;
          and SLCODMAG = this.w_RIFMAG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04D74B68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLCODVAA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVVALNAZ),'SALDIART','SLCODVAA');
      +",SLDATUCA ="+cp_NullLink(cp_ToStrODBC(this.w_RDATDOC),'SALDIART','SLDATUCA');
      +",SLVALUCA ="+cp_NullLink(cp_ToStrODBC(this.w_RVALULT),'SALDIART','SLVALUCA');
          +i_ccchkf ;
      +" where ";
          +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
          +" and SLCODMAG = "+cp_ToStrODBC(this.w_RIFMAG);
             )
    else
      update (i_cTable) set;
          SLCODVAA = this.oParentObject.w_MVVALNAZ;
          ,SLDATUCA = this.w_RDATDOC;
          ,SLVALUCA = this.w_RVALULT;
          &i_ccchkf. ;
       where;
          SLCODICE = this.w_KEYSAL;
          and SLCODMAG = this.w_RIFMAG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_04D9F4F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.w_FLDEL AND this.w_FAT_RAG
        * --- Se cancellazione di riga Raggruppata generata da fatturazione differita devo forzare anche 
        *     l'azzeramento dellle quantit� evase visto che il Link non lo pu� fare direttamente
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVQTAEV1 =MVQTAEV1- "+cp_ToStrODBC(this.w_RQTAIM1);
          +",MVQTAEVA =MVQTAEVA- "+cp_ToStrODBC(this.w_RQTAIMP);
          +",MVEFFEVA ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVEFFEVA');
          +",MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVDATGEN');
          +",MVQTASAL =MVQTASAL+ "+cp_ToStrODBC(this.w_APPSAL);
          +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
          +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OIMPEVA),'DOC_DETT','MVIMPEVA');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF);
              +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                 )
        else
          update (i_cTable) set;
              MVQTAEV1 = MVQTAEV1 - this.w_RQTAIM1;
              ,MVQTAEVA = MVQTAEVA - this.w_RQTAIMP;
              ,MVEFFEVA = cp_CharToDate("  -  -  ");
              ,MVDATGEN = cp_CharToDate("  -  -  ");
              ,MVQTASAL = MVQTASAL + this.w_APPSAL;
              ,MVFLEVAS = " ";
              ,MVIMPEVA = this.w_OIMPEVA;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_SERRIF;
              and CPROWNUM = this.w_ROWRIF;
              and MVNUMRIF = this.w_NUMRIF;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_FLDEL AND Not this.w_FAT_RAG AND this.w_TIPRIG<>"F"
        * --- Cancellaziione di riga o del documento riassegno solo campi:
        *     MVDATGEN,MVFLEVAS,MVQTASAL,MVEFFEVA,MVIMPEVA
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVQTASAL =MVQTASAL+ "+cp_ToStrODBC(this.w_APPSAL);
          +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OIMPEVA),'DOC_DETT','MVIMPEVA');
          +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
          +",MVEFFEVA ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVEFFEVA');
          +",MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVDATGEN');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF);
              +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                 )
        else
          update (i_cTable) set;
              MVQTASAL = MVQTASAL + this.w_APPSAL;
              ,MVIMPEVA = this.w_OIMPEVA;
              ,MVFLEVAS = " ";
              ,MVEFFEVA = cp_CharToDate("  -  -  ");
              ,MVDATGEN = cp_CharToDate("  -  -  ");
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_SERRIF;
              and CPROWNUM = this.w_ROWRIF;
              and MVNUMRIF = this.w_NUMRIF;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_TIPRIG="F" 
        * --- Se righe Forfettarie a differenza degli altri casi  devo:
        *      -azzerare\valorizzare ad 1 qta evasa\ qtasal in cancellazione.
        *      -valorizzare ad 1\azzerare qta evase\ qtasal se modifica
        this.w_NQTAEVA = IIF(this.w_FLDEL,0,1)
        this.w_NQTASAL = IIF(this.w_FLDEL,1,0)
        this.w_OFLEVAS = IIF(this.w_FLERIF="S","S"," ")
        * --- Controllo anche valori negativi e righe a importo zero
        this.w_OFLEVAS = IIF(this.w_FLDEL AND (abs(this.w_OIMPEVA)<abs(this.w_ORD_PRZ) Or this.w_OIMPEVA=0 and this.w_ORD_PRZ=0)," ",this.w_OFLEVAS)
        this.w_OEFFEVA = IIF(this.w_FLDEL,cp_CharToDate("  -  -  "),this.w_OEFFEVA)
        this.w_DATGEN = IIF(this.w_FLDEL,cp_CharToDate("  -  -  "),this.w_DATGEN)
        this.w_APPSAL = this.w_NQTASAL-this.w_OQTASAL
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_NQTASAL),'DOC_DETT','MVQTASAL');
          +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OIMPEVA),'DOC_DETT','MVIMPEVA');
          +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(this.w_OFLEVAS),'DOC_DETT','MVFLEVAS');
          +",MVEFFEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OEFFEVA),'DOC_DETT','MVEFFEVA');
          +",MVDATGEN ="+cp_NullLink(cp_ToStrODBC(this.w_DATGEN),'DOC_DETT','MVDATGEN');
          +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(this.w_NQTAEVA),'DOC_DETT','MVQTAEVA');
          +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(this.w_NQTAEVA),'DOC_DETT','MVQTAEV1');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF);
              +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                 )
        else
          update (i_cTable) set;
              MVQTASAL = this.w_NQTASAL;
              ,MVIMPEVA = this.w_OIMPEVA;
              ,MVFLEVAS = this.w_OFLEVAS;
              ,MVEFFEVA = this.w_OEFFEVA;
              ,MVDATGEN = this.w_DATGEN;
              ,MVQTAEVA = this.w_NQTAEVA;
              ,MVQTAEV1 = this.w_NQTAEVA;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_SERRIF;
              and CPROWNUM = this.w_ROWRIF;
              and MVNUMRIF = this.w_NUMRIF;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      otherwise
        * --- In Ogni Caso Azzera sempre l'Importo Evaso se no Forfettaria ed esegue rivalorizzazione
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVDATGEN');
          +",MVFLULCA ="+cp_NullLink(cp_ToStrODBC(this.w_FLULCA),'DOC_DETT','MVFLULCA');
          +",MVFLULPV ="+cp_NullLink(cp_ToStrODBC(this.w_FLULPV),'DOC_DETT','MVFLULPV');
          +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(this.w_STIMPCOM),'DOC_DETT','MVIMPCOM');
          +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(this.w_OIMPEVA),'DOC_DETT','MVIMPEVA');
          +",MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_OIMPNAZ),'DOC_DETT','MVIMPNAZ');
          +",MVPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_RPREZZO),'DOC_DETT','MVPREZZO');
          +",MVSCONT1 ="+cp_NullLink(cp_ToStrODBC(this.w_OSCONT1),'DOC_DETT','MVSCONT1');
          +",MVSCONT2 ="+cp_NullLink(cp_ToStrODBC(this.w_OSCONT2),'DOC_DETT','MVSCONT2');
          +",MVSCONT3 ="+cp_NullLink(cp_ToStrODBC(this.w_OSCONT3),'DOC_DETT','MVSCONT3');
          +",MVSCONT4 ="+cp_NullLink(cp_ToStrODBC(this.w_OSCONT4),'DOC_DETT','MVSCONT4');
          +",MVVALMAG ="+cp_NullLink(cp_ToStrODBC(this.w_RVALMAG),'DOC_DETT','MVVALMAG');
          +",MVVALRIG ="+cp_NullLink(cp_ToStrODBC(this.w_RVALRIG),'DOC_DETT','MVVALRIG');
          +",MVVALULT ="+cp_NullLink(cp_ToStrODBC(this.w_RVALULT),'DOC_DETT','MVVALULT');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF);
              +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                 )
        else
          update (i_cTable) set;
              MVDATGEN = cp_CharToDate("  -  -  ");
              ,MVFLULCA = this.w_FLULCA;
              ,MVFLULPV = this.w_FLULPV;
              ,MVIMPCOM = this.w_STIMPCOM;
              ,MVIMPEVA = this.w_OIMPEVA;
              ,MVIMPNAZ = this.w_OIMPNAZ;
              ,MVPREZZO = this.w_RPREZZO;
              ,MVSCONT1 = this.w_OSCONT1;
              ,MVSCONT2 = this.w_OSCONT2;
              ,MVSCONT3 = this.w_OSCONT3;
              ,MVSCONT4 = this.w_OSCONT4;
              ,MVVALMAG = this.w_RVALMAG;
              ,MVVALRIG = this.w_RVALRIG;
              ,MVVALULT = this.w_RVALULT;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_SERRIF;
              and CPROWNUM = this.w_ROWRIF;
              and MVNUMRIF = this.w_NUMRIF;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
    endcase
    return
  proc Try_04D9D390()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_FLRISE,'SLQTRPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
      +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
      +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
          +i_ccchkf ;
      +" where ";
          +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
          +" and SLCODMAG = "+cp_ToStrODBC(this.w_RIFMAG);
             )
    else
      update (i_cTable) set;
          SLQTOPER = &i_cOp1.;
          ,SLQTIPER = &i_cOp2.;
          ,SLQTRPER = &i_cOp3.;
          &i_ccchkf. ;
       where;
          SLCODICE = this.w_KEYSAL;
          and SLCODMAG = this.w_RIFMAG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if g_MADV = "S" AND (!EMPTY (this.w_ORDLOT) OR !EMPTY(this.w_ORDUBI) )
      * --- Write into SALDILOT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDILOT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_FLRISE,'SUQTRPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDILOT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SUQTRPER ="+cp_NullLink(i_cOp1,'SALDILOT','SUQTRPER');
            +i_ccchkf ;
        +" where ";
            +"SUCODART = "+cp_ToStrODBC(this.w_KEYSAL);
            +" and SUCODMAG = "+cp_ToStrODBC(this.w_RIFMAG);
            +" and SUCODUBI = "+cp_ToStrODBC(this.w_ORDUBI);
            +" and SUCODLOT = "+cp_ToStrODBC(this.w_ORDLOT);
               )
      else
        update (i_cTable) set;
            SUQTRPER = &i_cOp1.;
            &i_ccchkf. ;
         where;
            SUCODART = this.w_KEYSAL;
            and SUCODMAG = this.w_RIFMAG;
            and SUCODUBI = this.w_ORDUBI;
            and SUCODLOT = this.w_ORDLOT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARSALCOM"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_COCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARSALCOM;
        from (i_cTable) where;
            ARCODART = this.w_COCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_SALCOM="S"
      if empty(nvl(this.w_CO1CODCOM,""))
        this.w_COMMAPPO = this.w_COMMDEFA
      else
        this.w_COMMAPPO = this.w_CO1CODCOM
      endif
      * --- Write into SALDICOM
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_FLRISE,'SCQTRPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
        +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
        +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
            +i_ccchkf ;
        +" where ";
            +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
            +" and SCCODMAG = "+cp_ToStrODBC(this.w_RIFMAG);
            +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
               )
      else
        update (i_cTable) set;
            SCQTOPER = &i_cOp1.;
            ,SCQTIPER = &i_cOp2.;
            ,SCQTRPER = &i_cOp3.;
            &i_ccchkf. ;
         where;
            SCCODICE = this.w_KEYSAL;
            and SCCODMAG = this.w_RIFMAG;
            and SCCODCAN = this.w_COMMAPPO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if g_MADV = "S" AND (!EMPTY (this.w_ORDLOT) OR !EMPTY(this.w_ORDUBI) )
        * --- Write into SALOTCOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALOTCOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_FLRISE,'SMQTRPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SMQTRPER ="+cp_NullLink(i_cOp1,'SALOTCOM','SMQTRPER');
              +i_ccchkf ;
          +" where ";
              +"SMCODART = "+cp_ToStrODBC(this.w_KEYSAL);
              +" and SMCODMAG = "+cp_ToStrODBC(this.w_RIFMAG);
              +" and SMCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
              +" and SMCODUBI = "+cp_ToStrODBC(this.w_ORDUBI);
              +" and SMCODLOT = "+cp_ToStrODBC(this.w_ORDLOT);
                 )
        else
          update (i_cTable) set;
              SMQTRPER = &i_cOp1.;
              &i_ccchkf. ;
           where;
              SMCODART = this.w_KEYSAL;
              and SMCODMAG = this.w_RIFMAG;
              and SMCODCAN = this.w_COMMAPPO;
              and SMCODUBI = this.w_ORDUBI;
              and SMCODLOT = this.w_ORDLOT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    return
  proc Try_04D9EB30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDIART
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_F2ORDI,'SLQTOPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.w_F2IMPE,'SLQTIPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.w_F2RISE,'SLQTRPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLQTOPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTOPER');
      +",SLQTIPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTIPER');
      +",SLQTRPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTRPER');
          +i_ccchkf ;
      +" where ";
          +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
          +" and SLCODMAG = "+cp_ToStrODBC(this.w_RIFMAT);
             )
    else
      update (i_cTable) set;
          SLQTOPER = &i_cOp1.;
          ,SLQTIPER = &i_cOp2.;
          ,SLQTRPER = &i_cOp3.;
          &i_ccchkf. ;
       where;
          SLCODICE = this.w_KEYSAL;
          and SLCODMAG = this.w_RIFMAT;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if g_MADV = "S" AND (!EMPTY (this.w_CODLOT) OR !EMPTY(this.w_ORDUB2) )
      * --- Write into SALDILOT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDILOT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_F2RISE,'SUQTRPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDILOT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SUQTRPER ="+cp_NullLink(i_cOp1,'SALDILOT','SUQTRPER');
            +i_ccchkf ;
        +" where ";
            +"SUCODART = "+cp_ToStrODBC(this.w_KEYSAL);
            +" and SUCODMAG = "+cp_ToStrODBC(this.w_RIFMAT);
            +" and SUCODLOT = "+cp_ToStrODBC(this.w_ORDLOT);
            +" and SUCODUBI = "+cp_ToStrODBC(this.w_ORDUB2);
               )
      else
        update (i_cTable) set;
            SUQTRPER = &i_cOp1.;
            &i_ccchkf. ;
         where;
            SUCODART = this.w_KEYSAL;
            and SUCODMAG = this.w_RIFMAT;
            and SUCODLOT = this.w_ORDLOT;
            and SUCODUBI = this.w_ORDUB2;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.w_SALCOM="S"
      * --- Write into SALDICOM
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDICOM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_F2ORDI,'SCQTOPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_F2IMPE,'SCQTIPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_F2RISE,'SCQTRPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
        +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
        +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
            +i_ccchkf ;
        +" where ";
            +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
            +" and SCCODMAG = "+cp_ToStrODBC(this.w_RIFMAT);
            +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
               )
      else
        update (i_cTable) set;
            SCQTOPER = &i_cOp1.;
            ,SCQTIPER = &i_cOp2.;
            ,SCQTRPER = &i_cOp3.;
            &i_ccchkf. ;
         where;
            SCCODICE = this.w_KEYSAL;
            and SCCODMAG = this.w_RIFMAT;
            and SCCODCAN = this.w_COMMAPPO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      if g_MADV = "S" AND (!EMPTY (this.w_ORDLOT) OR !EMPTY(this.w_ORDUBI) )
        * --- Write into SALOTCOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALOTCOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_F2RISE,'SMQTRPER','this.w_APPSAL',this.w_APPSAL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SMQTRPER ="+cp_NullLink(i_cOp1,'SALOTCOM','SMQTRPER');
              +i_ccchkf ;
          +" where ";
              +"SMCODART = "+cp_ToStrODBC(this.w_KEYSAL);
              +" and SMCODMAG = "+cp_ToStrODBC(this.w_RIFMAT);
              +" and SMCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
              +" and SMCODUBI = "+cp_ToStrODBC(this.w_ORDUB2);
              +" and SMCODLOT = "+cp_ToStrODBC(this.w_ORDLOT);
                 )
        else
          update (i_cTable) set;
              SMQTRPER = &i_cOp1.;
              &i_ccchkf. ;
           where;
              SMCODART = this.w_KEYSAL;
              and SMCODMAG = this.w_RIFMAT;
              and SMCODCAN = this.w_COMMAPPO;
              and SMCODUBI = this.w_ORDUB2;
              and SMCODLOT = this.w_ORDLOT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    return
  proc Try_04D98170()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVIMPNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_OIMPNAZ),'DOC_DETT','MVIMPNAZ');
      +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(this.w_STIMPCOM),'DOC_DETT','MVIMPCOM');
      +",MVVALULT ="+cp_NullLink(cp_ToStrODBC(this.w_RVALULT),'DOC_DETT','MVVALULT');
      +",MVPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_RPREZZO),'DOC_DETT','MVPREZZO');
      +",MVSCONT1 ="+cp_NullLink(cp_ToStrODBC(this.w_OSCONT1),'DOC_DETT','MVSCONT1');
      +",MVSCONT2 ="+cp_NullLink(cp_ToStrODBC(this.w_OSCONT2),'DOC_DETT','MVSCONT2');
      +",MVSCONT3 ="+cp_NullLink(cp_ToStrODBC(this.w_OSCONT3),'DOC_DETT','MVSCONT3');
      +",MVSCONT4 ="+cp_NullLink(cp_ToStrODBC(this.w_OSCONT4),'DOC_DETT','MVSCONT4');
      +",MVFLULPV ="+cp_NullLink(cp_ToStrODBC(this.w_FLULPV),'DOC_DETT','MVFLULPV');
      +",MVFLULCA ="+cp_NullLink(cp_ToStrODBC(this.w_FLULCA),'DOC_DETT','MVFLULCA');
      +",MVVALRIG ="+cp_NullLink(cp_ToStrODBC(this.w_RVALRIG),'DOC_DETT','MVVALRIG');
      +",MVVALMAG ="+cp_NullLink(cp_ToStrODBC(this.w_RVALMAG),'DOC_DETT','MVVALMAG');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF);
          +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
             )
    else
      update (i_cTable) set;
          MVIMPNAZ = this.w_OIMPNAZ;
          ,MVIMPCOM = this.w_STIMPCOM;
          ,MVVALULT = this.w_RVALULT;
          ,MVPREZZO = this.w_RPREZZO;
          ,MVSCONT1 = this.w_OSCONT1;
          ,MVSCONT2 = this.w_OSCONT2;
          ,MVSCONT3 = this.w_OSCONT3;
          ,MVSCONT4 = this.w_OSCONT4;
          ,MVFLULPV = this.w_FLULPV;
          ,MVFLULCA = this.w_FLULCA;
          ,MVVALRIG = this.w_RVALRIG;
          ,MVVALMAG = this.w_RVALMAG;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_SERRIF;
          and CPROWNUM = this.w_ROWRIF;
          and MVNUMRIF = this.w_NUMRIF;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica Dichiarazione di Intento con Agg.to Importo utilizzato
    this.w_DICINTE_INDIRETTA = Space(10)
    this.w_NOAGG = .F.
    this.w_DIC_COLLEGATA = .F.
    this.w_IMPORTO_ORIGINE = 0
    this.w_IMPUTI = 0
    this.w_IMPORTO_DI_STORNO = 0
    * --- Importo utilizzato
    this.w_DIIMPUTI = 0
    this.w_DIIMPDIC = 0
    this.w_DIIMPUTI_COLL = 0
    this.w_DIIMPDIC_COLL = 0
    this.w_DDIMPDIC = 0
    this.w_IMP_DICHIAR = 0
    this.w_DDIMPDIC_COLL_STO = 0
    this.w_DDIMPDIC_COLL_STO_ORIGINE = 0
    this.w_STORNO_ORIGINE = .T.
    this.w_VAR_DICHIARAZIONE = .F.
    this.w_RIFDIC = this.oParentObject.w_MVRIFDIC
    this.w_DIIMPUTI_ORIGINE = 0
    this.w_DDIMPDIC_ORIGINE = 0
    this.w_NUMRIGA_ORIGINE = 0
    this.w_NUMRIGA_COLL_ORIGINE = 0
    this.w_DDIMPDIC_COLL_ORIGINE = 0
    this.w_DIIMPUTI_COLL_ORIGINE = 0
    this.w_DDVISMAN = "N"
    this.w_DDRECCOL = Space(10)
    this.w_CLADOC = iif( (this.oParentObject.w_MVFLVEAC="V" And this.oParentObject.w_MVEMERIC="A" ) Or (this.oParentObject.w_MVFLVEAC="A" And this.oParentObject.w_MVEMERIC="V" ),"NC",this.oParentObject.w_MVCLADOC)
    * --- Effettuo operazioni differenti a seconda che sia presente una dichiarazione
    *     di intento associata al documento
    if Not Empty(this.oParentObject.w_MVRIFDIC)
      * --- Read from DIC_INTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DIC_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DIIMPUTI,DIIMPDIC,DICODIVA,DIDICCOL"+;
          " from "+i_cTable+" DIC_INTE where ";
              +"DISERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVRIFDIC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DIIMPUTI,DIIMPDIC,DICODIVA,DIDICCOL;
          from (i_cTable) where;
              DISERIAL = this.oParentObject.w_MVRIFDIC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DIIMPUTI = NVL(cp_ToDate(_read_.DIIMPUTI),cp_NullValue(_read_.DIIMPUTI))
        this.w_DIIMPDIC = NVL(cp_ToDate(_read_.DIIMPDIC),cp_NullValue(_read_.DIIMPDIC))
        this.w_DICODIVA = NVL(cp_ToDate(_read_.DICODIVA),cp_NullValue(_read_.DICODIVA))
        this.w_DIDICCOL = NVL(cp_ToDate(_read_.DIDICCOL),cp_NullValue(_read_.DIDICCOL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se il documento non � una nota di credito e la dichiarazione di intento
      *     associata ha una dichiarazione collegata, bisogna andare a leggere
      *     l'importo dichiarato e l'importo utilizzato.
      if Not Empty(this.w_DIDICCOL) And this.w_CLADOC<>"NC" And lower(this.w_PADRE.class)<>"tgsar_bgd"
        * --- Read from DIC_INTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIC_INTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DIIMPDIC,DIIMPUTI"+;
            " from "+i_cTable+" DIC_INTE where ";
                +"DISERIAL = "+cp_ToStrODBC(this.w_DIDICCOL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DIIMPDIC,DIIMPUTI;
            from (i_cTable) where;
                DISERIAL = this.w_DIDICCOL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DIIMPDIC_COLL = NVL(cp_ToDate(_read_.DIIMPDIC),cp_NullValue(_read_.DIIMPDIC))
          this.w_DIIMPUTI_COLL = NVL(cp_ToDate(_read_.DIIMPUTI),cp_NullValue(_read_.DIIMPUTI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DIC_COLLEGATA = .T.
      endif
      if lower(this.w_PADRE.class)=="tgsar_bgd" AND this.w_PADRE.w_bLastRow
        if this.w_DICODIVA=ALLTRIM(this.w_PADRE.w_PADRE.w_MVACIVA1) And this.w_PADRE.w_PADRE.w_MVAFLOM1="X"
          this.w_IMPUTI = this.w_PADRE.w_PADRE.w_MVAIMPN1
        endif
        if this.w_DICODIVA=ALLTRIM(this.w_PADRE.w_PADRE.w_MVACIVA2) And this.w_PADRE.w_PADRE.w_MVAFLOM2="X"
          this.w_IMPUTI = this.w_IMPUTI + this.w_PADRE.w_PADRE.w_MVAIMPN2
        endif
        if this.w_DICODIVA=ALLTRIM(this.w_PADRE.w_PADRE.w_MVACIVA3) And this.w_PADRE.w_PADRE.w_MVAFLOM3="X"
          this.w_IMPUTI = this.w_IMPUTI + this.w_PADRE.w_PADRE.w_MVAIMPN3
        endif
        if this.w_DICODIVA=ALLTRIM(this.w_PADRE.w_PADRE.w_MVACIVA4) And this.w_PADRE.w_PADRE.w_MVAFLOM4="X"
          this.w_IMPUTI = this.w_IMPUTI + this.w_PADRE.w_PADRE.w_MVAIMPN4
        endif
        if this.w_DICODIVA=ALLTRIM(this.w_PADRE.w_PADRE.w_MVACIVA5) And this.w_PADRE.w_PADRE.w_MVAFLOM5="X"
          this.w_IMPUTI = this.w_IMPUTI + this.w_PADRE.w_PADRE.w_MVAIMPN5
        endif
        if this.w_DICODIVA=ALLTRIM(this.w_PADRE.w_PADRE.w_MVACIVA6) And this.w_PADRE.w_PADRE.w_MVAFLOM6="X"
          this.w_IMPUTI = this.w_IMPUTI + this.w_PADRE.w_PADRE.w_MVAIMPN6
        endif
      else
        if this.w_DICODIVA=this.oParentObject.w_MVACIVA1 And this.oParentObject.w_MVAFLOM1="X"
          this.w_IMPUTI = this.oParentObject.w_MVAIMPN1
        endif
        if this.w_DICODIVA=this.oParentObject.w_MVACIVA2 And this.oParentObject.w_MVAFLOM2="X"
          this.w_IMPUTI = this.w_IMPUTI + this.oParentObject.w_MVAIMPN2
        endif
        if this.w_DICODIVA=this.oParentObject.w_MVACIVA3 And this.oParentObject.w_MVAFLOM3="X"
          this.w_IMPUTI = this.w_IMPUTI + this.oParentObject.w_MVAIMPN3
        endif
        if this.w_DICODIVA=this.oParentObject.w_MVACIVA4 And this.oParentObject.w_MVAFLOM4="X"
          this.w_IMPUTI = this.w_IMPUTI + this.oParentObject.w_MVAIMPN4
        endif
        if this.w_DICODIVA=this.oParentObject.w_MVACIVA5 And this.oParentObject.w_MVAFLOM5="X"
          this.w_IMPUTI = this.w_IMPUTI + this.oParentObject.w_MVAIMPN5
        endif
        if this.w_DICODIVA=this.oParentObject.w_MVACIVA6 And this.oParentObject.w_MVAFLOM6="X"
          this.w_IMPUTI = this.w_IMPUTI + this.oParentObject.w_MVAIMPN6
        endif
      endif
      * --- La variabile DIIMPDIC contiene il valore delle righe IVA che hanno il
      *     codice IVA indentico al codice IVA esenzione ( dichiarazione di intento )
      this.w_DDIMPDIC = this.w_IMPUTI
    else
      this.w_RIFDIC = this.oParentObject.w_OLDRIFDIC
      * --- Verifico se la dichiarazione di intento che era associata al documento ha
      *     una dichiarazione di intento collegata
      * --- Read from DIC_INTE
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DIC_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DIIMPUTI,DIDICCOL"+;
          " from "+i_cTable+" DIC_INTE where ";
              +"DISERIAL = "+cp_ToStrODBC(this.w_RIFDIC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DIIMPUTI,DIDICCOL;
          from (i_cTable) where;
              DISERIAL = this.w_RIFDIC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DIIMPUTI = NVL(cp_ToDate(_read_.DIIMPUTI),cp_NullValue(_read_.DIIMPUTI))
        this.w_DIDICCOL = NVL(cp_ToDate(_read_.DIDICCOL),cp_NullValue(_read_.DIDICCOL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if Not Empty(this.w_DIDICCOL)
        * --- Read from DIC_INTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DIC_INTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "DIIMPUTI"+;
            " from "+i_cTable+" DIC_INTE where ";
                +"DISERIAL = "+cp_ToStrODBC(this.w_DIDICCOL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            DIIMPUTI;
            from (i_cTable) where;
                DISERIAL = this.w_DIDICCOL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DIIMPUTI_COLL = NVL(cp_ToDate(_read_.DIIMPUTI),cp_NullValue(_read_.DIIMPUTI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_DIC_COLLEGATA = .T.
      endif
    endif
    this.w_DIDICCOL_FILTRO = this.w_DIDICCOL
    * --- Valuta di Riferimento
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VASIMVAL"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(g_PERVAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VASIMVAL;
        from (i_cTable) where;
            VACODVAL = g_PERVAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_SIMVAL = NVL(cp_ToDate(_read_.VASIMVAL),cp_NullValue(_read_.VASIMVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if g_PERVAL<>this.oParentObject.w_MVCODVAL AND (this.w_IMPUTI<>0 OR this.w_CFUNC $ "Edit-Load")
      * --- Converte l'importo Utilizzato alla Valuta della Dichiarazione
      this.w_IMPUTI = Cp_Round(VAL2MON(this.w_IMPUTI, this.oParentObject.w_MVCAOVAL, g_CAOVAL, this.oParentObject.w_MVDATDOC, g_PERVAL,g_PERPVL),g_PERPVL)
      this.oParentObject.w_OIMPUTI = Cp_Round(VAL2MON(this.oParentObject.w_OIMPUTI, this.oParentObject.w_MVCAOVAL, g_CAOVAL, this.oParentObject.w_MVDATDOC, g_PERVAL,g_PERPVL),g_PERPVL)
      this.w_DDIMPDIC = Cp_Round(VAL2MON(this.w_DDIMPDIC, this.oParentObject.w_MVCAOVAL, g_CAOVAL, this.oParentObject.w_MVDATDOC, g_PERVAL,g_PERPVL),g_PERPVL)
    endif
    * --- Effettuiamo operazioni differenti a seconda dello stato del documento
    do case
      case this.w_CFUNC="Load"
        * --- Nel caso sia presente una dichiarazione di intento collegata, questa
        *     sar� utilizzata solo nel caso in cui l'importo dichiarato sia maggiore dell'importo
        *     utilizzato e nel caso in cui che la dichiarazione collegata sia gi� stata utilizzata
        *     in un altro documento.
        if this.w_DIIMPUTI=this.w_DIIMPDIC
          this.w_DIC_COLLEGATA = .F.
        endif
        if this.w_DIC_COLLEGATA
          * --- Select from DICDINTE13
          do vq_exec with 'DICDINTE13',this,'_Curs_DICDINTE13','',.f.,.t.
          if used('_Curs_DICDINTE13')
            select _Curs_DICDINTE13
            locate for 1=1
            do while not(eof())
            * --- Se esiste un record nella query Dicdinte13 vuol dire che la dichiarazione di intento
            *     collegata � gi� stata associata ad un altro documento
            this.w_DIC_COLLEGATA = .F.
            exit
              select _Curs_DICDINTE13
              continue
            enddo
            use
          endif
          * --- Verifico se � possibile abbinare la dichiarazione di intento collegata.
          *     Controllo le data di inizio e fine validit� 
          if this.w_DIC_COLLEGATA
            * --- Select from DICDINTE26
            do vq_exec with 'DICDINTE26',this,'_Curs_DICDINTE26','',.f.,.t.
            if used('_Curs_DICDINTE26')
              select _Curs_DICDINTE26
              locate for 1=1
              do while not(eof())
              this.w_DIC_COLLEGATA = .F.
              exit
                select _Curs_DICDINTE26
                continue
              enddo
              use
            endif
          endif
        endif
        * --- Verifichiamo se la somma dell'importo del documento pi� l'importo utilizzato � maggiore dell'importo dichiarato
        if this.w_DIIMPUTI+ IIF(this.w_CLADOC="NC",-1,1)*this.w_IMPUTI>this.w_DIIMPDIC AND this.w_DIC_COLLEGATA
          * --- Importo utilizzato sulla riga della dichiarazione di intento,
          *     calcolato come differenza tra l'importo dichiarato ed utilizzato
          this.w_DDIMPDIC = this.w_DIIMPDIC-this.w_DIIMPUTI
          * --- Importo utilizzato sulla dichiarazione di intento valorizzato con l'importo 
          *     dichiarato
          this.w_DIIMPUTI = this.w_DIIMPDIC
          * --- Importo utilizzato sulla riga della dichiarazione di intento,
          *     calcolato come differenza tra l'importo calcolato sul documento
          *     e l'importo inserito nella dichiarazione di intento principale
          this.w_DDIMPDIC_COLL = this.w_IMPUTI- this.w_DDIMPDIC
          * --- Importo utilizzato sulla dichiarazione di intento collegata, calcolato come 
          *     somma tra l'importo utilizzato e la differenza tra l'importo calcolato sul documento
          *     e l'importo inserito nella dichiarazione di intento principale
          this.w_DIIMPUTI_COLL = this.w_DIIMPUTI_COLL+ this.w_DDIMPDIC_COLL
        else
          * --- Calcolo l'importo utilizzato andando a sommare il valore presente nella
          *     dichiarazione di intento con il totale del documento
          this.w_DIIMPUTI = this.w_DIIMPUTI + IIF(this.w_CLADOC="NC",-1,1)*this.w_IMPUTI
          this.w_DDIMPDIC = IIF(this.w_CLADOC="NC",-1,1)*this.w_DDIMPDIC
          this.w_DIC_COLLEGATA = .F.
        endif
        * --- Controlliamo che l'importo utilizzato non sia maggiore dell'importo dichiarato,
        *     tenendo conto dell'eventuale dichiarazione collegata
        if this.w_DIIMPUTI+IIF(this.w_DIC_COLLEGATA,this.w_DIIMPUTI_COLL,0)>this.w_DIIMPDIC+IIF(this.w_DIC_COLLEGATA,this.w_DIIMPDIC_COLL,0)
          if lower(this.w_PADRE.class)=="tgsar_bgd" AND this.w_PADRE.w_bLastRow
            this.w_PADRE.w_OK = .F.
            this.w_PADRE.w_RETVAL = ah_MsgFormat( "Importo utilizzato per la dichiarazione di intento superiore al consentito%0Importo utilizzato: %1%0Importo dichiarazione: %2", ALLTR(STR(this.w_DIIMPUTI+IIF(this.w_DIC_COLLEGATA,this.w_DIIMPUTI_COLL,0),18,this.oParentObject.w_DECTOP)+" "+this.w_SIMVAL), ALLTR(STR(this.w_DIIMPDIC+IIF(this.w_DIC_COLLEGATA,this.w_DIIMPDIC_COLL,0),18,this.oParentObject.w_DECTOP)+" "+this.w_SIMVAL ))
            this.w_OK = .F.
          endif
          if lower(this.w_PADRE.class)<>"tgsar_bgd"
            this.w_MESS = "Importo utilizzato per la dichiarazione di intento superiore al consentito%0Importo utilizzato: %1%0Importo dichiarazione: %2"
            this.w_MESS = ah_Msgformat(this.w_MESS, ALLTR(STR(this.w_DIIMPUTI+IIF(this.w_DIC_COLLEGATA,this.w_DIIMPUTI_COLL,0),18,this.oParentObject.w_DECTOP)+" "+this.w_SIMVAL), ALLTR(STR(this.w_DIIMPDIC+IIF(this.w_DIC_COLLEGATA,this.w_DIIMPDIC_COLL,0),18,this.oParentObject.w_DECTOP)+" "+this.w_SIMVAL))
            this.w_OK = .F.
          endif
        endif
        if this.w_OK
          this.w_NUMRIGA = 0
          this.w_NUMRIGA_COLL = 0
          * --- Risalgo all'ultimo numero di riga della dichiarazione di intento principale e
          *     di quella collegata
          * --- Select from DICDINTE9
          do vq_exec with 'DICDINTE9',this,'_Curs_DICDINTE9','',.f.,.t.
          if used('_Curs_DICDINTE9')
            select _Curs_DICDINTE9
            locate for 1=1
            do while not(eof())
            this.w_NUMRIGA = IIF(_Curs_DICDINTE9.ORDINA="A",_Curs_DICDINTE9.CPROWNUM,this.w_NUMRIGA)
            this.w_NUMRIGA_COLL = IIF(_Curs_DICDINTE9.ORDINA="B",_Curs_DICDINTE9.CPROWNUM,this.w_NUMRIGA_COLL)
              select _Curs_DICDINTE9
              continue
            enddo
            use
          endif
          * --- Try
          local bErr_04D46AF0
          bErr_04D46AF0=bTrsErr
          this.Try_04D46AF0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04D46AF0
          * --- End
        endif
      case this.w_CFUNC="Query"
        * --- Se l'importo del documento che stiamo cancellando � uguale a zero non
        *     effettuiamo nessuno operazione
        if this.w_IMPUTI<>0
          * --- Nel caso in cui la dichiarazione presente nel documento abbia una dichiarazione
          *     collegata, dobbiamo verificare se questa dichiarazione � stata utilizzata
          *     nel documento , per eventualmente stornare il valore
          if this.w_DIC_COLLEGATA
            this.w_PARAM_FILTRO = "C"
            this.w_SERIALE_DICHIARAZIONE = this.w_DIDICCOL
            * --- Select from DICDINTE8
            do vq_exec with 'DICDINTE8',this,'_Curs_DICDINTE8','',.f.,.t.
            if used('_Curs_DICDINTE8')
              select _Curs_DICDINTE8
              locate for 1=1
              do while not(eof())
              * --- Se esiste un record nella query Dicdinte8 vuol dire che al documento
              *     sono associate due dichiarazioni di intento
              this.w_DICINTE_INDIRETTA = _Curs_DICDINTE8.DDSERIAL
              this.w_DDIMPDIC_COLL_STO = _Curs_DICDINTE8.DDIMPDIC
              exit
                select _Curs_DICDINTE8
                continue
              enddo
              use
            endif
          endif
          * --- Nel caso in cui il documento sia stato suddiviso in due dichiarazione di intento
          *     devo andare a leggere i valori da stornare sulla tabella "Dicdinte"
          if Not Empty(this.w_DICINTE_INDIRETTA)
            * --- Leggo il valore della dichiarazione di intento collegata
            this.w_PARAM_FILTRO = "P"
            this.w_SERIALE_DICHIARAZIONE = this.oParentObject.w_MVRIFDIC
            * --- Select from DICDINTE8
            do vq_exec with 'DICDINTE8',this,'_Curs_DICDINTE8','',.f.,.t.
            if used('_Curs_DICDINTE8')
              select _Curs_DICDINTE8
              locate for 1=1
              do while not(eof())
              * --- Calcolo il valore dell'importo utilizzato della dichiarazione di intento collegata
              this.w_DIIMPUTI = this.w_DIIMPUTI- _Curs_DICDINTE8.DDIMPDIC
              this.w_DIIMPDIC = _Curs_DICDINTE8.DDIMPDIC
              exit
                select _Curs_DICDINTE8
                continue
              enddo
              use
            endif
            this.w_DIIMPUTI_COLL = this.w_DIIMPUTI_COLL- this.w_DDIMPDIC_COLL_STO
          else
            * --- Nel caso in cui la dichiarazione di intento collegata non sia presente 
            *     per il documento in questione il valore da stornare deve essere letto
            *     dal valore del documento di orgine
            this.w_DIC_COLLEGATA = .F.
            this.w_DIIMPDIC = IIF(this.w_CLADOC="NC", -1, 1)* this.oParentObject.w_OIMPUTI
            this.w_DIIMPUTI = this.w_DIIMPUTI-IIF(this.w_CLADOC="NC", -1, 1)* this.oParentObject.w_OIMPUTI
          endif
          this.w_NUMRIGA = 0
          this.w_NUMRIGA_COLL = 0
          * --- Risalgo all'ultimo numero di riga della dichiarazione di intento principale e
          *     di quella collegata
          * --- Select from DICDINTE9
          do vq_exec with 'DICDINTE9',this,'_Curs_DICDINTE9','',.f.,.t.
          if used('_Curs_DICDINTE9')
            select _Curs_DICDINTE9
            locate for 1=1
            do while not(eof())
            this.w_NUMRIGA = IIF(_Curs_DICDINTE9.ORDINA="A",_Curs_DICDINTE9.CPROWNUM,this.w_NUMRIGA)
            this.w_NUMRIGA_COLL = IIF(_Curs_DICDINTE9.ORDINA="B",_Curs_DICDINTE9.CPROWNUM,this.w_NUMRIGA_COLL)
              select _Curs_DICDINTE9
              continue
            enddo
            use
          endif
          * --- Try
          local bErr_04D50610
          bErr_04D50610=bTrsErr
          this.Try_04D50610()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_04D50610
          * --- End
        endif
      case this.w_CFUNC="Edit"
        * --- Nel caso in cui la dichiarazione di intento non sia stata modificata
        *     e l'importo non sia stato variato sul documento non effettuo nessuna
        *     operazione
        if ! (this.oParentObject.w_MVRIFDIC=this.oParentObject.w_OLDRIFDIC And this.w_IMPUTI=this.oParentObject.w_OIMPUTI)
          * --- Assegno il valore 'M' ( Modifica )  al tipo aggiornamento
          this.w_DDTIPAGG = "M"
          * --- Nel caso sia presente una dichiarazione di intento collegata, questa
          *     sar� utilizzata solo nel caso in cui la dichiarazione collegata non sia gi� stata utilizzata
          *     in un altro documento.
          if this.w_DIC_COLLEGATA
            * --- Select from DICDINTE13
            do vq_exec with 'DICDINTE13',this,'_Curs_DICDINTE13','',.f.,.t.
            if used('_Curs_DICDINTE13')
              select _Curs_DICDINTE13
              locate for 1=1
              do while not(eof())
              * --- Se esiste un record nella query Dicdinte13 vuol dire che la dichiarazione di intento
              *     collegata � gi� stata associata ad un altro documento
              this.w_DIC_COLLEGATA = .F.
              exit
                select _Curs_DICDINTE13
                continue
              enddo
              use
            endif
            * --- Verifico se � possibile abbinare la dichiarazione di intento collegata.
            *     Controllo le data di inizio e fine validit� 
            if this.w_DIC_COLLEGATA
              * --- Select from DICDINTE26
              do vq_exec with 'DICDINTE26',this,'_Curs_DICDINTE26','',.f.,.t.
              if used('_Curs_DICDINTE26')
                select _Curs_DICDINTE26
                locate for 1=1
                do while not(eof())
                this.w_DIC_COLLEGATA = .F.
                exit
                  select _Curs_DICDINTE26
                  continue
                enddo
                use
              endif
            endif
          endif
          * --- Controllo se � stata modificata la dichiarazione di intento sul documento
          if Not Empty(this.oParentObject.w_MVRIFDIC) And Not Empty(this.oParentObject.w_OLDRIFDIC) And this.oParentObject.w_MVRIFDIC<>this.oParentObject.w_OLDRIFDIC
            * --- Controllo se per il documento in questione sono presenti due dichiarazioni
            *     di intento.
            this.w_PARAM_FILTRO = "C"
            this.w_SERIALE_DICHIARAZIONE = this.oParentObject.w_MVRIFDIC
            * --- Select from DICDINTE8
            do vq_exec with 'DICDINTE8',this,'_Curs_DICDINTE8','',.f.,.t.
            if used('_Curs_DICDINTE8')
              select _Curs_DICDINTE8
              locate for 1=1
              do while not(eof())
              this.w_DIIMPUTI = this.w_DIIMPUTI-_Curs_DICDINTE8.DDIMPDIC
              this.w_DDIMPDIC_COLL_STO_ORIGINE = _Curs_DICDINTE8.DDIMPDIC
              this.w_VAR_DICHIARAZIONE = .T.
              exit
                select _Curs_DICDINTE8
                continue
              enddo
              use
            endif
          endif
          * --- Se siamo in variazione e non � stata modificata la dichiarazione di intento
          *     storniamo dall'importo calcolato l'importo di origine del documento
          if this.oParentObject.w_MVRIFDIC=this.oParentObject.w_OLDRIFDIC
            this.w_IMPUTI = this.w_IMPUTI - this.oParentObject.w_OIMPUTI
          endif
          if this.w_DIC_COLLEGATA
            * --- Controlliamo se la dichiarazione di intento ha una dichiarazione di intento
            *     collegata e questa � stata utilizzata all'interno del documento
            this.w_PARAM_FILTRO = "C"
            this.w_SERIALE_DICHIARAZIONE = this.w_DIDICCOL
            * --- Select from DICDINTE8
            do vq_exec with 'DICDINTE8',this,'_Curs_DICDINTE8','',.f.,.t.
            if used('_Curs_DICDINTE8')
              select _Curs_DICDINTE8
              locate for 1=1
              do while not(eof())
              * --- Se esiste un record nella query Dicdinte8 vuol dire che al documento
              *     sono associate due dichiarazioni di intento
              this.w_DICINTE_INDIRETTA = _Curs_DICDINTE8.DDSERIAL
              this.w_DDIMPDIC_COLL_STO = _Curs_DICDINTE8.DDIMPDIC
              exit
                select _Curs_DICDINTE8
                continue
              enddo
              use
            endif
          endif
          * --- Controllo se sul documento � presente una dichiarazione di intento
          if Not Empty(this.oParentObject.w_MVRIFDIC)
            * --- Controlliamo se il documento ha una dichiarazione di intento collegata
            *     oppure se l'importo utilizzato pi� il valore del documento siano maggiore dell'importo
            *     dichiarato
            if (this.w_DIIMPUTI+ IIF(this.w_CLADOC="NC",-1,1)*this.w_IMPUTI>this.w_DIIMPDIC AND this.w_DIC_COLLEGATA) Or Not Empty(this.w_DICINTE_INDIRETTA)
              * --- Controllo se per il documento � presente una dichiarazione di intento collegata
              if Not Empty(this.w_DICINTE_INDIRETTA)
                * --- Controllo se il valore del documento supera il valore dell'importo dichiarato meno
                *     l'importo utilizzato
                if this.w_DIIMPUTI+ IIF(this.w_CLADOC="NC",-1,1)*this.w_IMPUTI+this.w_DDIMPDIC_COLL_STO>this.w_DIIMPDIC
                  this.w_IMP_DICHIAR = this.w_DIIMPDIC-this.w_DIIMPUTI
                  this.w_DDIMPDIC = this.w_IMP_DICHIAR
                  * --- L'importo da stornare sulla dichiarazione di intento deve essere letto 
                  *     dalla tabella "DICDINTE"
                  this.w_PARAM_FILTRO = "P"
                  this.w_SERIALE_DICHIARAZIONE = this.oParentObject.w_MVRIFDIC
                  * --- Select from DICDINTE8
                  do vq_exec with 'DICDINTE8',this,'_Curs_DICDINTE8','',.f.,.t.
                  if used('_Curs_DICDINTE8')
                    select _Curs_DICDINTE8
                    locate for 1=1
                    do while not(eof())
                    * --- Se esiste un record nella query Dicdinte8 vuol dire che al documento
                    *     sono associate due dichiarazioni di intento
                    this.w_DDIMPDIC = this.w_DDIMPDIC+Nvl(_Curs_DICDINTE8.DDIMPDIC,0)
                    this.w_IMPORTO_DI_STORNO = Nvl(_Curs_DICDINTE8.DDIMPDIC,0)
                    exit
                      select _Curs_DICDINTE8
                      continue
                    enddo
                    use
                  endif
                  * --- Importo utilizzato sulla dichiarazione di intento valorizzato con l'importo 
                  *     dichiarato
                  this.w_DIIMPUTI = this.w_DIIMPDIC
                  * --- Importo utilizzato sulla dichiarazione di intento collegata, calcolato come 
                  *     somma tra l'importo utilizzato e la differenza tra l'importo calcolato sul documento
                  *     e l'importo inserito nella dichiarazione di intento principale
                  this.w_DIIMPUTI_COLL = this.w_DIIMPUTI_COLL+(this.w_IMPUTI - this.w_IMP_DICHIAR)
                  * --- Importo utilizzato sulla riga della dichiarazione di intento,
                  *     calcolato come differenza tra l'importo calcolato sul documento
                  *     e l'importo inserito nella dichiarazione di intento principale
                  this.w_DDIMPDIC_COLL = this.w_DDIMPDIC_COLL_STO + (this.w_IMPUTI- this.w_IMP_DICHIAR)
                else
                  this.w_DDIMPDIC_COLL = 0
                  this.w_DIIMPUTI_COLL = this.w_DIIMPUTI_COLL - this.w_DDIMPDIC_COLL_STO
                  this.w_IMP_DICHIAR = this.w_IMPUTI+this.w_DDIMPDIC_COLL_STO
                  this.w_DIIMPUTI = this.w_DIIMPUTI + IIF(this.w_CLADOC="NC",-1,1)*this.w_IMP_DICHIAR
                  this.w_DDIMPDIC = IIF(this.w_CLADOC="NC",-1,1)*this.w_DDIMPDIC
                  * --- L'importo da stornare sulla dichiarazione di intento deve essere letto 
                  *     dalla tabella "DICDINTE"
                  this.w_PARAM_FILTRO = "P"
                  this.w_SERIALE_DICHIARAZIONE = this.oParentObject.w_MVRIFDIC
                  * --- Select from DICDINTE8
                  do vq_exec with 'DICDINTE8',this,'_Curs_DICDINTE8','',.f.,.t.
                  if used('_Curs_DICDINTE8')
                    select _Curs_DICDINTE8
                    locate for 1=1
                    do while not(eof())
                    * --- Se esiste un record nella query Dicdinte8 vuol dire che al documento
                    *     sono associate due dichiarazioni di intento
                    this.w_IMPORTO_ORIGINE = Nvl(_Curs_DICDINTE8.DDIMPDIC,0)*-1
                    exit
                      select _Curs_DICDINTE8
                      continue
                    enddo
                    use
                  endif
                  this.w_STORNO_ORIGINE = .F.
                endif
              else
                * --- Importo utilizzato sulla riga della dichiarazione di intento,
                *     calcolato come differenza tra l'importo dichiarato ed utilizzato
                this.w_IMP_DICHIAR = this.w_DIIMPDIC-this.w_DIIMPUTI
                this.w_DDIMPDIC = this.w_IMP_DICHIAR
                * --- Controllo se presente una dichiarazione di intento di origine
                if Not Empty(this.oParentObject.w_OLDRIFDIC)
                  * --- L'importo da stornare sulla dichiarazione di intento deve essere letto 
                  *     dalla tabella "DICDINTE"
                  this.w_PARAM_FILTRO = "P"
                  this.w_SERIALE_DICHIARAZIONE = this.oParentObject.w_MVRIFDIC
                  * --- Select from DICDINTE8
                  do vq_exec with 'DICDINTE8',this,'_Curs_DICDINTE8','',.f.,.t.
                  if used('_Curs_DICDINTE8')
                    select _Curs_DICDINTE8
                    locate for 1=1
                    do while not(eof())
                    * --- Se esiste un record nella query Dicdinte8 vuol dire che al documento
                    *     sono associate due dichiarazioni di intento
                    this.w_DDIMPDIC = this.w_DDIMPDIC+Nvl(_Curs_DICDINTE8.DDIMPDIC,0)
                    this.w_IMPORTO_DI_STORNO = Nvl(_Curs_DICDINTE8.DDIMPDIC,0)
                    exit
                      select _Curs_DICDINTE8
                      continue
                    enddo
                    use
                  endif
                else
                  this.w_IMPORTO_DI_STORNO = this.w_DDIMPDIC
                endif
                * --- Importo utilizzato sulla dichiarazione di intento valorizzato con l'importo 
                *     dichiarato
                this.w_DIIMPUTI = this.w_DIIMPDIC
                * --- Controllo se � stata modificata la dichiarazione di intento
                if Not Empty(this.oParentObject.w_MVRIFDIC) And Not Empty(this.oParentObject.w_OLDRIFDIC) And this.oParentObject.w_MVRIFDIC<>this.oParentObject.w_OLDRIFDIC AND this.w_DIDICCOL=this.oParentObject.w_OLDRIFDIC
                  this.w_PARAM_FILTRO = "P"
                  this.w_SERIALE_DICHIARAZIONE = this.oParentObject.w_OLDRIFDIC
                  this.w_DOC_RIFDIC = .F.
                  * --- L'importo da stornare sulla dichiarazione di intento deve essere letto 
                  *     dalla tabella "DICDINTE"
                  * --- Select from DICDINTE8
                  do vq_exec with 'DICDINTE8',this,'_Curs_DICDINTE8','',.f.,.t.
                  if used('_Curs_DICDINTE8')
                    select _Curs_DICDINTE8
                    locate for 1=1
                    do while not(eof())
                    this.w_DOC_RIFDIC = .T.
                    this.w_DIIMPUTI_COLL = (this.w_DIIMPUTI_COLL - Nvl(_Curs_DICDINTE8.DDIMPDIC,0)) + (this.w_IMPUTI - this.w_IMP_DICHIAR)
                    exit
                      select _Curs_DICDINTE8
                      continue
                    enddo
                    use
                  endif
                  if ! this.w_DOC_RIFDIC
                    this.w_DIIMPUTI_COLL = (this.w_DIIMPUTI_COLL - this.oParentObject.w_OIMPUTI) + (this.w_IMPUTI - this.w_IMP_DICHIAR)
                  endif
                else
                  this.w_DIIMPUTI_COLL = this.w_DIIMPUTI_COLL+(this.w_IMPUTI - this.w_IMP_DICHIAR)
                endif
                this.w_DDIMPDIC_COLL = this.w_DDIMPDIC_COLL_STO + (this.w_IMPUTI- this.w_IMP_DICHIAR)
              endif
            else
              * --- Calcolo l'importo utilizzato andando a sommare il valore presente nella
              *     dichiarazione di intento con il totale del documento
              this.w_DIIMPUTI = this.w_DIIMPUTI + IIF(this.w_CLADOC="NC",-1,1)*this.w_IMPUTI
              this.w_DDIMPDIC = IIF(this.w_CLADOC="NC",-1,1)*this.w_DDIMPDIC
              this.w_DIC_COLLEGATA = .F.
            endif
            * --- Verifichiamo se la somma dell'importo del documento pi� l'importo utilizzato � maggiore dell'importo dichiarato
            if (this.w_DIIMPUTI+IIF(this.w_DIC_COLLEGATA,this.w_DIIMPUTI_COLL,0)) >(this.w_DIIMPDIC+IIF(this.w_DIC_COLLEGATA,this.w_DIIMPDIC_COLL,0))
              if lower(this.w_PADRE.class)=="tgsar_bgd" AND this.w_PADRE.w_bLastRow
                this.w_PADRE.w_OK = .F.
                this.w_PADRE.w_RETVAL = ah_MsgFormat( "Importo utilizzato per la dichiarazione di intento superiore al consentito%0Importo utilizzato: %1%0Importo dichiarazione: %2", ALLTR(STR(this.w_DIIMPUTI+IIF(this.w_DIC_COLLEGATA,this.w_DIIMPUTI_COLL,0),18,this.oParentObject.w_DECTOP)+" "+this.w_SIMVAL), ALLTR(STR(this.w_DIIMPUTI+IIF(this.w_DIC_COLLEGATA,this.w_DIIMPUTI_COLL,0),18,this.oParentObject.w_DECTOP)+" "+this.w_SIMVAL ))
                this.w_OK = .F.
              endif
              if lower(this.w_PADRE.class)<>"tgsar_bgd"
                this.w_MESS = "Importo utilizzato per la dichiarazione di intento superiore al consentito%0Importo utilizzato: %1%0Importo dichiarazione: %2"
                this.w_MESS = ah_Msgformat(this.w_MESS, ALLTR(STR(this.w_DIIMPUTI+IIF(this.w_DIC_COLLEGATA,this.w_DIIMPUTI_COLL,0),18,this.oParentObject.w_DECTOP)+" "+this.w_SIMVAL), ALLTR(STR(this.w_DIIMPDIC+IIF(this.w_DIC_COLLEGATA,this.w_DIIMPDIC_COLL,0),18,this.oParentObject.w_DECTOP)+" "+this.w_SIMVAL))
                this.w_OK = .F.
              endif
            endif
          endif
          if this.w_OK
            * --- A seconda che il documento sia una fattura oppure una nota di credito
            *     moltiplico l'importo per 1 o -1
            if this.w_STORNO_ORIGINE
              this.w_IMPORTO_ORIGINE = IIF(Not Empty(this.w_DICINTE_INDIRETTA),this.w_IMPORTO_DI_STORNO*-1,IIF(this.w_CLADOC="NC", 1, -1)*this.oParentObject.w_OIMPUTI)
            endif
            this.w_NUMRIGA = 0
            this.w_NUMRIGA_COLL = 0
            * --- Risalgo all'ultimo numero di riga della dichiarazione di intento principale e
            *     di quella collegata
            * --- Select from DICDINTE9
            do vq_exec with 'DICDINTE9',this,'_Curs_DICDINTE9','',.f.,.t.
            if used('_Curs_DICDINTE9')
              select _Curs_DICDINTE9
              locate for 1=1
              do while not(eof())
              this.w_NUMRIGA = IIF(_Curs_DICDINTE9.ORDINA="A",_Curs_DICDINTE9.CPROWNUM,this.w_NUMRIGA)
              this.w_NUMRIGA_COLL = IIF(_Curs_DICDINTE9.ORDINA="B",_Curs_DICDINTE9.CPROWNUM,this.w_NUMRIGA_COLL)
                select _Curs_DICDINTE9
                continue
              enddo
              use
            endif
            * --- Try
            local bErr_04D599A0
            bErr_04D599A0=bTrsErr
            this.Try_04D599A0()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_04D599A0
            * --- End
          endif
        endif
    endcase
  endproc
  proc Try_04D46AF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Inserimento dati nella dichiarazione di intento principale ( Tabelle Dicdinte e Dic_Inte )
    this.w_NUMRIGA = this.w_NUMRIGA+1
    this.w_DDVISMAN = IIF(this.w_DIC_COLLEGATA=.t.,"I",this.w_DDVISMAN)
    * --- Insert into DICDINTE
    i_nConn=i_TableProp[this.DICDINTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DICDINTE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DICDINTE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DDSERIAL"+",CPROWNUM"+",CPROWORD"+",DDDOCSER"+",DDIMPDIC"+",DDTIPINS"+",DDDATOPE"+",DDTIPAGG"+",DDCODUTE"+",DDVISMAN"+",DDRECCOL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVRIFDIC),'DICDINTE','DDSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'DICDINTE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA*10),'DICDINTE','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DICDINTE','DDDOCSER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DDIMPDIC),'DICDINTE','DDIMPDIC');
      +","+cp_NullLink(cp_ToStrODBC("D"),'DICDINTE','DDTIPINS');
      +","+cp_NullLink(cp_ToStrODBC(i_datsys),'DICDINTE','DDDATOPE');
      +","+cp_NullLink(cp_ToStrODBC("I"),'DICDINTE','DDTIPAGG');
      +","+cp_NullLink(cp_ToStrODBC(i_codute),'DICDINTE','DDCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DDVISMAN),'DICDINTE','DDVISMAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DDRECCOL),'DICDINTE','DDRECCOL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DDSERIAL',this.oParentObject.w_MVRIFDIC,'CPROWNUM',this.w_NUMRIGA,'CPROWORD',this.w_NUMRIGA*10,'DDDOCSER',this.oParentObject.w_MVSERIAL,'DDIMPDIC',this.w_DDIMPDIC,'DDTIPINS',"D",'DDDATOPE',i_datsys,'DDTIPAGG',"I",'DDCODUTE',i_codute,'DDVISMAN',this.w_DDVISMAN,'DDRECCOL',this.w_DDRECCOL)
      insert into (i_cTable) (DDSERIAL,CPROWNUM,CPROWORD,DDDOCSER,DDIMPDIC,DDTIPINS,DDDATOPE,DDTIPAGG,DDCODUTE,DDVISMAN,DDRECCOL &i_ccchkf. );
         values (;
           this.oParentObject.w_MVRIFDIC;
           ,this.w_NUMRIGA;
           ,this.w_NUMRIGA*10;
           ,this.oParentObject.w_MVSERIAL;
           ,this.w_DDIMPDIC;
           ,"D";
           ,i_datsys;
           ,"I";
           ,i_codute;
           ,this.w_DDVISMAN;
           ,this.w_DDRECCOL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into DIC_INTE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DIC_INTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DIIMPUTI ="+cp_NullLink(cp_ToStrODBC(this.w_DIIMPUTI),'DIC_INTE','DIIMPUTI');
          +i_ccchkf ;
      +" where ";
          +"DISERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVRIFDIC);
             )
    else
      update (i_cTable) set;
          DIIMPUTI = this.w_DIIMPUTI;
          &i_ccchkf. ;
       where;
          DISERIAL = this.oParentObject.w_MVRIFDIC;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Se la variabile w_Dic_Collegata � valorizzata a true allora vuol dire
    *     che devo aggiungere una riga anche nella dichiarazione di intento collegata
    if this.w_DIC_COLLEGATA
      * --- Inserimento dati nella dichiarazione di intento collegata ( Tabelle Dicdinte e Dic_Inte )
      this.w_NUMRIGA_COLL = this.w_NUMRIGA_COLL+1
      * --- Insert into DICDINTE
      i_nConn=i_TableProp[this.DICDINTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DICDINTE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DICDINTE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DDSERIAL"+",CPROWNUM"+",CPROWORD"+",DDDOCSER"+",DDIMPDIC"+",DDTIPINS"+",DDDATOPE"+",DDTIPAGG"+",DDCODUTE"+",DDVISMAN"+",DDRECCOL"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_DIDICCOL),'DICDINTE','DDSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA_COLL),'DICDINTE','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA_COLL*10),'DICDINTE','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DICDINTE','DDDOCSER');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DDIMPDIC_COLL),'DICDINTE','DDIMPDIC');
        +","+cp_NullLink(cp_ToStrODBC("I"),'DICDINTE','DDTIPINS');
        +","+cp_NullLink(cp_ToStrODBC(i_datsys),'DICDINTE','DDDATOPE');
        +","+cp_NullLink(cp_ToStrODBC("I"),'DICDINTE','DDTIPAGG');
        +","+cp_NullLink(cp_ToStrODBC(i_codute),'DICDINTE','DDCODUTE');
        +","+cp_NullLink(cp_ToStrODBC("N"),'DICDINTE','DDVISMAN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DDRECCOL),'DICDINTE','DDRECCOL');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DDSERIAL',this.w_DIDICCOL,'CPROWNUM',this.w_NUMRIGA_COLL,'CPROWORD',this.w_NUMRIGA_COLL*10,'DDDOCSER',this.oParentObject.w_MVSERIAL,'DDIMPDIC',this.w_DDIMPDIC_COLL,'DDTIPINS',"I",'DDDATOPE',i_datsys,'DDTIPAGG',"I",'DDCODUTE',i_codute,'DDVISMAN',"N",'DDRECCOL',this.w_DDRECCOL)
        insert into (i_cTable) (DDSERIAL,CPROWNUM,CPROWORD,DDDOCSER,DDIMPDIC,DDTIPINS,DDDATOPE,DDTIPAGG,DDCODUTE,DDVISMAN,DDRECCOL &i_ccchkf. );
           values (;
             this.w_DIDICCOL;
             ,this.w_NUMRIGA_COLL;
             ,this.w_NUMRIGA_COLL*10;
             ,this.oParentObject.w_MVSERIAL;
             ,this.w_DDIMPDIC_COLL;
             ,"I";
             ,i_datsys;
             ,"I";
             ,i_codute;
             ,"N";
             ,this.w_DDRECCOL;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Write into DIC_INTE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DIC_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DIIMPUTI ="+cp_NullLink(cp_ToStrODBC(this.w_DIIMPUTI_COLL),'DIC_INTE','DIIMPUTI');
            +i_ccchkf ;
        +" where ";
            +"DISERIAL = "+cp_ToStrODBC(this.w_DIDICCOL);
               )
      else
        update (i_cTable) set;
            DIIMPUTI = this.w_DIIMPUTI_COLL;
            &i_ccchkf. ;
         where;
            DISERIAL = this.w_DIDICCOL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return
  proc Try_04D50610()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Creo una riga sulla tabella Dicdinte di importo negativo per stornare il valore
    *     della dichiarazione di intento. Nel caso il documento non sia presente nella
    *     tabella Dicdinte non inserisco una riga di storno
    this.w_DIIMPDIC = this.w_DIIMPDIC*-1
    this.w_NUMRIGA = this.w_NUMRIGA+1
    * --- Insert into DICDINTE
    i_nConn=i_TableProp[this.DICDINTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DICDINTE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DICDINTE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DDSERIAL"+",CPROWNUM"+",CPROWORD"+",DDDOCSER"+",DDIMPDIC"+",DDTIPINS"+",DDDATOPE"+",DDTIPAGG"+",DDCODUTE"+",DDVISMAN"+",DDRECCOL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVRIFDIC),'DICDINTE','DDSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'DICDINTE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA*10),'DICDINTE','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DICDINTE','DDDOCSER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DIIMPDIC),'DICDINTE','DDIMPDIC');
      +","+cp_NullLink(cp_ToStrODBC("D"),'DICDINTE','DDTIPINS');
      +","+cp_NullLink(cp_ToStrODBC(i_datsys),'DICDINTE','DDDATOPE');
      +","+cp_NullLink(cp_ToStrODBC("C"),'DICDINTE','DDTIPAGG');
      +","+cp_NullLink(cp_ToStrODBC(i_codute),'DICDINTE','DDCODUTE');
      +","+cp_NullLink(cp_ToStrODBC("N"),'DICDINTE','DDVISMAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DDRECCOL),'DICDINTE','DDRECCOL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DDSERIAL',this.oParentObject.w_MVRIFDIC,'CPROWNUM',this.w_NUMRIGA,'CPROWORD',this.w_NUMRIGA*10,'DDDOCSER',this.oParentObject.w_MVSERIAL,'DDIMPDIC',this.w_DIIMPDIC,'DDTIPINS',"D",'DDDATOPE',i_datsys,'DDTIPAGG',"C",'DDCODUTE',i_codute,'DDVISMAN',"N",'DDRECCOL',this.w_DDRECCOL)
      insert into (i_cTable) (DDSERIAL,CPROWNUM,CPROWORD,DDDOCSER,DDIMPDIC,DDTIPINS,DDDATOPE,DDTIPAGG,DDCODUTE,DDVISMAN,DDRECCOL &i_ccchkf. );
         values (;
           this.oParentObject.w_MVRIFDIC;
           ,this.w_NUMRIGA;
           ,this.w_NUMRIGA*10;
           ,this.oParentObject.w_MVSERIAL;
           ,this.w_DIIMPDIC;
           ,"D";
           ,i_datsys;
           ,"C";
           ,i_codute;
           ,"N";
           ,this.w_DDRECCOL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Aggiorno l'importo utilizzato sulla dichiarazione di intento principale
    * --- Write into DIC_INTE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DIC_INTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DIIMPUTI ="+cp_NullLink(cp_ToStrODBC(this.w_DIIMPUTI),'DIC_INTE','DIIMPUTI');
          +i_ccchkf ;
      +" where ";
          +"DISERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVRIFDIC);
             )
    else
      update (i_cTable) set;
          DIIMPUTI = this.w_DIIMPUTI;
          &i_ccchkf. ;
       where;
          DISERIAL = this.oParentObject.w_MVRIFDIC;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_DIC_COLLEGATA
      this.w_DDIMPDIC_COLL_STO = this.w_DDIMPDIC_COLL_STO*-1
      this.w_NUMRIGA_COLL = this.w_NUMRIGA_COLL+1
      * --- Nel caso sia presente per il documento una dichiarazione di intento
      *     collegata creo una riga sulla tabella Dicdinte di importo negativo per stornare
      *     il valore della dichiarazione di intento
      * --- Insert into DICDINTE
      i_nConn=i_TableProp[this.DICDINTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DICDINTE_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DICDINTE_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"DDSERIAL"+",CPROWNUM"+",CPROWORD"+",DDDOCSER"+",DDIMPDIC"+",DDTIPINS"+",DDDATOPE"+",DDTIPAGG"+",DDCODUTE"+",DDVISMAN"+",DDRECCOL"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_DICINTE_INDIRETTA),'DICDINTE','DDSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA_COLL),'DICDINTE','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA_COLL*10),'DICDINTE','CPROWORD');
        +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DICDINTE','DDDOCSER');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DDIMPDIC_COLL_STO),'DICDINTE','DDIMPDIC');
        +","+cp_NullLink(cp_ToStrODBC("I"),'DICDINTE','DDTIPINS');
        +","+cp_NullLink(cp_ToStrODBC(i_datsys),'DICDINTE','DDDATOPE');
        +","+cp_NullLink(cp_ToStrODBC("C"),'DICDINTE','DDTIPAGG');
        +","+cp_NullLink(cp_ToStrODBC(i_codute),'DICDINTE','DDCODUTE');
        +","+cp_NullLink(cp_ToStrODBC("N"),'DICDINTE','DDVISMAN');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DDRECCOL),'DICDINTE','DDRECCOL');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'DDSERIAL',this.w_DICINTE_INDIRETTA,'CPROWNUM',this.w_NUMRIGA_COLL,'CPROWORD',this.w_NUMRIGA_COLL*10,'DDDOCSER',this.oParentObject.w_MVSERIAL,'DDIMPDIC',this.w_DDIMPDIC_COLL_STO,'DDTIPINS',"I",'DDDATOPE',i_datsys,'DDTIPAGG',"C",'DDCODUTE',i_codute,'DDVISMAN',"N",'DDRECCOL',this.w_DDRECCOL)
        insert into (i_cTable) (DDSERIAL,CPROWNUM,CPROWORD,DDDOCSER,DDIMPDIC,DDTIPINS,DDDATOPE,DDTIPAGG,DDCODUTE,DDVISMAN,DDRECCOL &i_ccchkf. );
           values (;
             this.w_DICINTE_INDIRETTA;
             ,this.w_NUMRIGA_COLL;
             ,this.w_NUMRIGA_COLL*10;
             ,this.oParentObject.w_MVSERIAL;
             ,this.w_DDIMPDIC_COLL_STO;
             ,"I";
             ,i_datsys;
             ,"C";
             ,i_codute;
             ,"N";
             ,this.w_DDRECCOL;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
      * --- Aggiorno l'importo utilizzato sulla dichiarazione di intento collegata
      * --- Write into DIC_INTE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DIC_INTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DIIMPUTI ="+cp_NullLink(cp_ToStrODBC(this.w_DIIMPUTI_COLL),'DIC_INTE','DIIMPUTI');
            +i_ccchkf ;
        +" where ";
            +"DISERIAL = "+cp_ToStrODBC(this.w_DIDICCOL);
               )
      else
        update (i_cTable) set;
            DIIMPUTI = this.w_DIIMPUTI_COLL;
            &i_ccchkf. ;
         where;
            DISERIAL = this.w_DIDICCOL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return
  proc Try_04D599A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Verifico se il documento che stiamo variando aveva una dichiarazione di intento
    *     associato, nel caso cos� non fosse non inserisco la riga di storno
    if Not Empty(this.oParentObject.w_OLDRIFDIC)
      * --- Nel caso di modifica devo inserire due nuovi record sulla tabella DICDINTE
      *     uno per lo storno del valore ed uno per il nuovo valore inserito nel documento.
      *     Devo operare in maniere differente se l'utente ha variato la dichiarazione
      *     di intento associata al documento
      if this.oParentObject.w_MVRIFDIC=this.oParentObject.w_OLDRIFDIC
        this.w_DDRECCOL = Sys(2015)
        this.w_NUMRIGA = this.w_NUMRIGA+1
        * --- Insert into DICDINTE
        i_nConn=i_TableProp[this.DICDINTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DICDINTE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DICDINTE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"DDSERIAL"+",CPROWNUM"+",CPROWORD"+",DDDOCSER"+",DDIMPDIC"+",DDTIPINS"+",DDDATOPE"+",DDTIPAGG"+",DDCODUTE"+",DDVISMAN"+",DDRECCOL"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVRIFDIC),'DICDINTE','DDSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'DICDINTE','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA*10),'DICDINTE','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DICDINTE','DDDOCSER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_IMPORTO_ORIGINE),'DICDINTE','DDIMPDIC');
          +","+cp_NullLink(cp_ToStrODBC("D"),'DICDINTE','DDTIPINS');
          +","+cp_NullLink(cp_ToStrODBC(i_datsys),'DICDINTE','DDDATOPE');
          +","+cp_NullLink(cp_ToStrODBC("M"),'DICDINTE','DDTIPAGG');
          +","+cp_NullLink(cp_ToStrODBC(i_codute),'DICDINTE','DDCODUTE');
          +","+cp_NullLink(cp_ToStrODBC("N"),'DICDINTE','DDVISMAN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DDRECCOL),'DICDINTE','DDRECCOL');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'DDSERIAL',this.oParentObject.w_MVRIFDIC,'CPROWNUM',this.w_NUMRIGA,'CPROWORD',this.w_NUMRIGA*10,'DDDOCSER',this.oParentObject.w_MVSERIAL,'DDIMPDIC',this.w_IMPORTO_ORIGINE,'DDTIPINS',"D",'DDDATOPE',i_datsys,'DDTIPAGG',"M",'DDCODUTE',i_codute,'DDVISMAN',"N",'DDRECCOL',this.w_DDRECCOL)
          insert into (i_cTable) (DDSERIAL,CPROWNUM,CPROWORD,DDDOCSER,DDIMPDIC,DDTIPINS,DDDATOPE,DDTIPAGG,DDCODUTE,DDVISMAN,DDRECCOL &i_ccchkf. );
             values (;
               this.oParentObject.w_MVRIFDIC;
               ,this.w_NUMRIGA;
               ,this.w_NUMRIGA*10;
               ,this.oParentObject.w_MVSERIAL;
               ,this.w_IMPORTO_ORIGINE;
               ,"D";
               ,i_datsys;
               ,"M";
               ,i_codute;
               ,"N";
               ,this.w_DDRECCOL;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Nel caso di dichiarazione collegata devo verificare se per il documento era
        *     gi� stata associata la dichiarazione di intento. In questo caso devo effettuare
        *     prima uno storno e poi successivamente un inserimento, oppure se non era
        *     ancora associata devo inserire solo il record
        if this.w_DIC_COLLEGATA
          if Not Empty(this.w_DICINTE_INDIRETTA)
            this.w_NUMRIGA_COLL = this.w_NUMRIGA_COLL+1
            this.w_DDIMPDIC_COLL_STO = this.w_DDIMPDIC_COLL_STO*-1
            * --- Insert into DICDINTE
            i_nConn=i_TableProp[this.DICDINTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DICDINTE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DICDINTE_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"DDSERIAL"+",CPROWNUM"+",CPROWORD"+",DDDOCSER"+",DDIMPDIC"+",DDTIPINS"+",DDDATOPE"+",DDTIPAGG"+",DDCODUTE"+",DDVISMAN"+",DDRECCOL"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_DIDICCOL),'DICDINTE','DDSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA_COLL),'DICDINTE','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA_COLL*10),'DICDINTE','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DICDINTE','DDDOCSER');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DDIMPDIC_COLL_STO),'DICDINTE','DDIMPDIC');
              +","+cp_NullLink(cp_ToStrODBC("I"),'DICDINTE','DDTIPINS');
              +","+cp_NullLink(cp_ToStrODBC(i_datsys),'DICDINTE','DDDATOPE');
              +","+cp_NullLink(cp_ToStrODBC("M"),'DICDINTE','DDTIPAGG');
              +","+cp_NullLink(cp_ToStrODBC(i_codute),'DICDINTE','DDCODUTE');
              +","+cp_NullLink(cp_ToStrODBC("N"),'DICDINTE','DDVISMAN');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DDRECCOL),'DICDINTE','DDRECCOL');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'DDSERIAL',this.w_DIDICCOL,'CPROWNUM',this.w_NUMRIGA_COLL,'CPROWORD',this.w_NUMRIGA_COLL*10,'DDDOCSER',this.oParentObject.w_MVSERIAL,'DDIMPDIC',this.w_DDIMPDIC_COLL_STO,'DDTIPINS',"I",'DDDATOPE',i_datsys,'DDTIPAGG',"M",'DDCODUTE',i_codute,'DDVISMAN',"N",'DDRECCOL',this.w_DDRECCOL)
              insert into (i_cTable) (DDSERIAL,CPROWNUM,CPROWORD,DDDOCSER,DDIMPDIC,DDTIPINS,DDDATOPE,DDTIPAGG,DDCODUTE,DDVISMAN,DDRECCOL &i_ccchkf. );
                 values (;
                   this.w_DIDICCOL;
                   ,this.w_NUMRIGA_COLL;
                   ,this.w_NUMRIGA_COLL*10;
                   ,this.oParentObject.w_MVSERIAL;
                   ,this.w_DDIMPDIC_COLL_STO;
                   ,"I";
                   ,i_datsys;
                   ,"M";
                   ,i_codute;
                   ,"N";
                   ,this.w_DDRECCOL;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
          this.w_NUMRIGA_COLL = this.w_NUMRIGA_COLL+1
          * --- Insert into DICDINTE
          i_nConn=i_TableProp[this.DICDINTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DICDINTE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DICDINTE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DDSERIAL"+",CPROWNUM"+",CPROWORD"+",DDDOCSER"+",DDIMPDIC"+",DDTIPINS"+",DDDATOPE"+",DDTIPAGG"+",DDCODUTE"+",DDVISMAN"+",DDRECCOL"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_DIDICCOL),'DICDINTE','DDSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA_COLL),'DICDINTE','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA_COLL*10),'DICDINTE','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DICDINTE','DDDOCSER');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DDIMPDIC_COLL),'DICDINTE','DDIMPDIC');
            +","+cp_NullLink(cp_ToStrODBC("I"),'DICDINTE','DDTIPINS');
            +","+cp_NullLink(cp_ToStrODBC(i_datsys),'DICDINTE','DDDATOPE');
            +","+cp_NullLink(cp_ToStrODBC("M"),'DICDINTE','DDTIPAGG');
            +","+cp_NullLink(cp_ToStrODBC(i_codute),'DICDINTE','DDCODUTE');
            +","+cp_NullLink(cp_ToStrODBC("N"),'DICDINTE','DDVISMAN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DDRECCOL),'DICDINTE','DDRECCOL');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DDSERIAL',this.w_DIDICCOL,'CPROWNUM',this.w_NUMRIGA_COLL,'CPROWORD',this.w_NUMRIGA_COLL*10,'DDDOCSER',this.oParentObject.w_MVSERIAL,'DDIMPDIC',this.w_DDIMPDIC_COLL,'DDTIPINS',"I",'DDDATOPE',i_datsys,'DDTIPAGG',"M",'DDCODUTE',i_codute,'DDVISMAN',"N",'DDRECCOL',this.w_DDRECCOL)
            insert into (i_cTable) (DDSERIAL,CPROWNUM,CPROWORD,DDDOCSER,DDIMPDIC,DDTIPINS,DDDATOPE,DDTIPAGG,DDCODUTE,DDVISMAN,DDRECCOL &i_ccchkf. );
               values (;
                 this.w_DIDICCOL;
                 ,this.w_NUMRIGA_COLL;
                 ,this.w_NUMRIGA_COLL*10;
                 ,this.oParentObject.w_MVSERIAL;
                 ,this.w_DDIMPDIC_COLL;
                 ,"I";
                 ,i_datsys;
                 ,"M";
                 ,i_codute;
                 ,"N";
                 ,this.w_DDRECCOL;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_DDVISMAN = IIF(this.w_DDIMPDIC_COLL<>0,"I",this.w_DDVISMAN)
          * --- Write into DIC_INTE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DIC_INTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DIIMPUTI ="+cp_NullLink(cp_ToStrODBC(this.w_DIIMPUTI_COLL),'DIC_INTE','DIIMPUTI');
                +i_ccchkf ;
            +" where ";
                +"DISERIAL = "+cp_ToStrODBC(this.w_DIDICCOL);
                   )
          else
            update (i_cTable) set;
                DIIMPUTI = this.w_DIIMPUTI_COLL;
                &i_ccchkf. ;
             where;
                DISERIAL = this.w_DIDICCOL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      else
        * --- Effettuo calcoli differenti a seconda se sul documento � presente una
        *     dichiarazione di intento
        if Empty(this.oParentObject.w_MVRIFDIC)
          * --- Verifico se la dichiarazione di intento che era associata al documento
          *     aveva collegato due dichiarazioni di intento
          if Not Empty(this.w_DICINTE_INDIRETTA)
            this.w_PARAM_FILTRO = "P"
            this.w_SERIALE_DICHIARAZIONE = this.w_RIFDIC
            * --- L'importo da stornare sulla dichiarazione di intento deve essere letto 
            *     dalla tabella "DICDINTE"
            * --- Select from DICDINTE8
            do vq_exec with 'DICDINTE8',this,'_Curs_DICDINTE8','',.f.,.t.
            if used('_Curs_DICDINTE8')
              select _Curs_DICDINTE8
              locate for 1=1
              do while not(eof())
              * --- Se esiste un record nella query Dicdinte8 vuol dire che al documento
              *     sono associate due dichiarazioni di intento
              this.w_DIIMPUTI = this.w_DIIMPUTI- _Curs_DICDINTE8.DDIMPDIC
              this.w_DDIMPDIC = _Curs_DICDINTE8.DDIMPDIC*-1
              exit
                select _Curs_DICDINTE8
                continue
              enddo
              use
            endif
            this.w_DIIMPUTI_COLL = this.w_DIIMPUTI_COLL-this.w_DDIMPDIC_COLL_STO
            this.w_DDIMPDIC_COLL_STO = this.w_DDIMPDIC_COLL_STO*-1
            this.w_NUMRIGA_COLL = this.w_NUMRIGA_COLL+1
            * --- Insert into DICDINTE
            i_nConn=i_TableProp[this.DICDINTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DICDINTE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DICDINTE_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"DDSERIAL"+",CPROWNUM"+",CPROWORD"+",DDDOCSER"+",DDIMPDIC"+",DDTIPINS"+",DDDATOPE"+",DDTIPAGG"+",DDCODUTE"+",DDVISMAN"+",DDRECCOL"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_DICINTE_INDIRETTA),'DICDINTE','DDSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA_COLL),'DICDINTE','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA_COLL*10),'DICDINTE','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DICDINTE','DDDOCSER');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DDIMPDIC_COLL_STO),'DICDINTE','DDIMPDIC');
              +","+cp_NullLink(cp_ToStrODBC("I"),'DICDINTE','DDTIPINS');
              +","+cp_NullLink(cp_ToStrODBC(i_datsys),'DICDINTE','DDDATOPE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DDTIPAGG),'DICDINTE','DDTIPAGG');
              +","+cp_NullLink(cp_ToStrODBC(i_codute),'DICDINTE','DDCODUTE');
              +","+cp_NullLink(cp_ToStrODBC("N"),'DICDINTE','DDVISMAN');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DDRECCOL),'DICDINTE','DDRECCOL');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'DDSERIAL',this.w_DICINTE_INDIRETTA,'CPROWNUM',this.w_NUMRIGA_COLL,'CPROWORD',this.w_NUMRIGA_COLL*10,'DDDOCSER',this.oParentObject.w_MVSERIAL,'DDIMPDIC',this.w_DDIMPDIC_COLL_STO,'DDTIPINS',"I",'DDDATOPE',i_datsys,'DDTIPAGG',this.w_DDTIPAGG,'DDCODUTE',i_codute,'DDVISMAN',"N",'DDRECCOL',this.w_DDRECCOL)
              insert into (i_cTable) (DDSERIAL,CPROWNUM,CPROWORD,DDDOCSER,DDIMPDIC,DDTIPINS,DDDATOPE,DDTIPAGG,DDCODUTE,DDVISMAN,DDRECCOL &i_ccchkf. );
                 values (;
                   this.w_DICINTE_INDIRETTA;
                   ,this.w_NUMRIGA_COLL;
                   ,this.w_NUMRIGA_COLL*10;
                   ,this.oParentObject.w_MVSERIAL;
                   ,this.w_DDIMPDIC_COLL_STO;
                   ,"I";
                   ,i_datsys;
                   ,this.w_DDTIPAGG;
                   ,i_codute;
                   ,"N";
                   ,this.w_DDRECCOL;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            * --- Write into DIC_INTE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DIC_INTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DIIMPUTI ="+cp_NullLink(cp_ToStrODBC(this.w_DIIMPUTI_COLL),'DIC_INTE','DIIMPUTI');
                  +i_ccchkf ;
              +" where ";
                  +"DISERIAL = "+cp_ToStrODBC(this.w_DIDICCOL);
                     )
            else
              update (i_cTable) set;
                  DIIMPUTI = this.w_DIIMPUTI_COLL;
                  &i_ccchkf. ;
               where;
                  DISERIAL = this.w_DIDICCOL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            this.w_DIIMPUTI = this.w_DIIMPUTI-IIF(this.w_CLADOC="NC", -1, 1)* this.oParentObject.w_OIMPUTI
            this.w_DDIMPDIC = IIF(this.w_CLADOC="NC", 1, -1)* this.oParentObject.w_OIMPUTI
          endif
        else
          * --- Se per la dichiarazione di intento era presente una riga indiretta
          *     associata al documento, devo effettuare lo storno della riga
          this.w_DDRECCOL = Sys(2015)
          if this.w_VAR_DICHIARAZIONE
            this.w_DDIMPDIC_COLL_STO = this.w_DDIMPDIC_COLL_STO_ORIGINE*-1
            this.w_NUMRIGA = this.w_NUMRIGA+1
            * --- Insert into DICDINTE
            i_nConn=i_TableProp[this.DICDINTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DICDINTE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DICDINTE_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"DDSERIAL"+",CPROWNUM"+",CPROWORD"+",DDDOCSER"+",DDIMPDIC"+",DDTIPINS"+",DDDATOPE"+",DDTIPAGG"+",DDCODUTE"+",DDVISMAN"+",DDRECCOL"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVRIFDIC),'DICDINTE','DDSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'DICDINTE','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA*10),'DICDINTE','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DICDINTE','DDDOCSER');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DDIMPDIC_COLL_STO),'DICDINTE','DDIMPDIC');
              +","+cp_NullLink(cp_ToStrODBC("I"),'DICDINTE','DDTIPINS');
              +","+cp_NullLink(cp_ToStrODBC(i_datsys),'DICDINTE','DDDATOPE');
              +","+cp_NullLink(cp_ToStrODBC("M"),'DICDINTE','DDTIPAGG');
              +","+cp_NullLink(cp_ToStrODBC(i_codute),'DICDINTE','DDCODUTE');
              +","+cp_NullLink(cp_ToStrODBC("N"),'DICDINTE','DDVISMAN');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DDRECCOL),'DICDINTE','DDRECCOL');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'DDSERIAL',this.oParentObject.w_MVRIFDIC,'CPROWNUM',this.w_NUMRIGA,'CPROWORD',this.w_NUMRIGA*10,'DDDOCSER',this.oParentObject.w_MVSERIAL,'DDIMPDIC',this.w_DDIMPDIC_COLL_STO,'DDTIPINS',"I",'DDDATOPE',i_datsys,'DDTIPAGG',"M",'DDCODUTE',i_codute,'DDVISMAN',"N",'DDRECCOL',this.w_DDRECCOL)
              insert into (i_cTable) (DDSERIAL,CPROWNUM,CPROWORD,DDDOCSER,DDIMPDIC,DDTIPINS,DDDATOPE,DDTIPAGG,DDCODUTE,DDVISMAN,DDRECCOL &i_ccchkf. );
                 values (;
                   this.oParentObject.w_MVRIFDIC;
                   ,this.w_NUMRIGA;
                   ,this.w_NUMRIGA*10;
                   ,this.oParentObject.w_MVSERIAL;
                   ,this.w_DDIMPDIC_COLL_STO;
                   ,"I";
                   ,i_datsys;
                   ,"M";
                   ,i_codute;
                   ,"N";
                   ,this.w_DDRECCOL;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
          * --- Dobbiamo stornare i dati della dichiarazione di intento che prima era collegata al
          *     documento
          this.w_RIFDIC = this.oParentObject.w_OLDRIFDIC
          * --- Devo verificare se la dichiarazione di intento che era presente nel documento
          *     ha una dichiarazione di intento collegata solo nel caso non abbia gi� stornato il valore
          * --- Read from DIC_INTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DIC_INTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DIDICCOL,DIIMPUTI"+;
              " from "+i_cTable+" DIC_INTE where ";
                  +"DISERIAL = "+cp_ToStrODBC(this.w_RIFDIC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DIDICCOL,DIIMPUTI;
              from (i_cTable) where;
                  DISERIAL = this.w_RIFDIC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DIDICCOL_ORIGINE = NVL(cp_ToDate(_read_.DIDICCOL),cp_NullValue(_read_.DIDICCOL))
            this.w_DIIMPUTI_ORIGINE = NVL(cp_ToDate(_read_.DIIMPUTI),cp_NullValue(_read_.DIIMPUTI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if Not Empty (this.w_DIDICCOL_ORIGINE) And ! this.w_VAR_DICHIARAZIONE
            * --- Controllo se nel documento erano associate due dichiarazioni di intento
            this.w_PARAM_FILTRO = "C"
            this.w_SERIALE_DICHIARAZIONE = this.w_DIDICCOL_ORIGINE
            * --- Select from DICDINTE8
            do vq_exec with 'DICDINTE8',this,'_Curs_DICDINTE8','',.f.,.t.
            if used('_Curs_DICDINTE8')
              select _Curs_DICDINTE8
              locate for 1=1
              do while not(eof())
              * --- Se esiste un record nella query Dicdinte8 vuol dire che al documento
              *     sono associate due dichiarazioni di intento
              this.w_DICINTE_INDIRETTA_ORIGINE = _Curs_DICDINTE8.DDSERIAL
              this.w_DDIMPDIC_COLL_ORIGINE = Nvl(_Curs_DICDINTE8.DDIMPDIC,0)
              this.w_DIIMPUTI_COLL_ORIGINE = Nvl(_Curs_DICDINTE8.DIIMPUTI,0)-Nvl(_Curs_DICDINTE8.DDIMPDIC,0)
              exit
                select _Curs_DICDINTE8
                continue
              enddo
              use
            endif
            this.w_DIDICCOL_FILTRO = this.w_DIDICCOL_ORIGINE
          else
            this.w_DIDICCOL_FILTRO = Space(10)
          endif
          * --- Select from DICDINTE9
          do vq_exec with 'DICDINTE9',this,'_Curs_DICDINTE9','',.f.,.t.
          if used('_Curs_DICDINTE9')
            select _Curs_DICDINTE9
            locate for 1=1
            do while not(eof())
            this.w_NUMRIGA_ORIGINE = IIF(_Curs_DICDINTE9.ORDINA="A",_Curs_DICDINTE9.CPROWNUM,this.w_NUMRIGA_ORIGINE)
            this.w_NUMRIGA_COLL_ORIGINE = IIF(_Curs_DICDINTE9.ORDINA="B",_Curs_DICDINTE9.CPROWNUM,this.w_NUMRIGA_COLL_ORIGINE)
              select _Curs_DICDINTE9
              continue
            enddo
            use
          endif
          if Empty(this.w_DICINTE_INDIRETTA_ORIGINE) And ! this.w_VAR_DICHIARAZIONE
            * --- Storno il valore sulla dichiarazione di intento di origine
            this.w_DIIMPUTI_ORIGINE = this.w_DIIMPUTI_ORIGINE-IIF(this.w_CLADOC="NC", -1, 1)* this.oParentObject.w_OIMPUTI
            this.w_DDIMPDIC_ORIGINE = IIF(this.w_CLADOC="NC", 1, -1)* this.oParentObject.w_OIMPUTI
          else
            * --- Controllo se ho gi� effettuato lo storno della dichirazione di intento che
            *     era collegata
            if ! this.w_VAR_DICHIARAZIONE
              this.w_DDIMPDIC_COLL_ORIGINE = this.w_DDIMPDIC_COLL_ORIGINE*-1
              this.w_NUMRIGA_COLL_ORIGINE = this.w_NUMRIGA_COLL_ORIGINE+1
              * --- Insert into DICDINTE
              i_nConn=i_TableProp[this.DICDINTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DICDINTE_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_ccchkf=''
              i_ccchkv=''
              this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DICDINTE_idx,i_nConn)
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                            " ("+"DDSERIAL"+",CPROWNUM"+",CPROWORD"+",DDDOCSER"+",DDIMPDIC"+",DDTIPINS"+",DDDATOPE"+",DDTIPAGG"+",DDCODUTE"+",DDVISMAN"+",DDRECCOL"+i_ccchkf+") values ("+;
                cp_NullLink(cp_ToStrODBC(this.w_DICINTE_INDIRETTA_ORIGINE),'DICDINTE','DDSERIAL');
                +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA_COLL_ORIGINE),'DICDINTE','CPROWNUM');
                +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA_COLL_ORIGINE*10),'DICDINTE','CPROWORD');
                +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DICDINTE','DDDOCSER');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DDIMPDIC_COLL_ORIGINE),'DICDINTE','DDIMPDIC');
                +","+cp_NullLink(cp_ToStrODBC("I"),'DICDINTE','DDTIPINS');
                +","+cp_NullLink(cp_ToStrODBC(i_datsys),'DICDINTE','DDDATOPE');
                +","+cp_NullLink(cp_ToStrODBC("M"),'DICDINTE','DDTIPAGG');
                +","+cp_NullLink(cp_ToStrODBC(i_codute),'DICDINTE','DDCODUTE');
                +","+cp_NullLink(cp_ToStrODBC("N"),'DICDINTE','DDVISMAN');
                +","+cp_NullLink(cp_ToStrODBC(this.w_DDRECCOL),'DICDINTE','DDRECCOL');
                     +i_ccchkv+")")
              else
                cp_CheckDeletedKey(i_cTable,0,'DDSERIAL',this.w_DICINTE_INDIRETTA_ORIGINE,'CPROWNUM',this.w_NUMRIGA_COLL_ORIGINE,'CPROWORD',this.w_NUMRIGA_COLL_ORIGINE*10,'DDDOCSER',this.oParentObject.w_MVSERIAL,'DDIMPDIC',this.w_DDIMPDIC_COLL_ORIGINE,'DDTIPINS',"I",'DDDATOPE',i_datsys,'DDTIPAGG',"M",'DDCODUTE',i_codute,'DDVISMAN',"N",'DDRECCOL',this.w_DDRECCOL)
                insert into (i_cTable) (DDSERIAL,CPROWNUM,CPROWORD,DDDOCSER,DDIMPDIC,DDTIPINS,DDDATOPE,DDTIPAGG,DDCODUTE,DDVISMAN,DDRECCOL &i_ccchkf. );
                   values (;
                     this.w_DICINTE_INDIRETTA_ORIGINE;
                     ,this.w_NUMRIGA_COLL_ORIGINE;
                     ,this.w_NUMRIGA_COLL_ORIGINE*10;
                     ,this.oParentObject.w_MVSERIAL;
                     ,this.w_DDIMPDIC_COLL_ORIGINE;
                     ,"I";
                     ,i_datsys;
                     ,"M";
                     ,i_codute;
                     ,"N";
                     ,this.w_DDRECCOL;
                     &i_ccchkv. )
                i_Rows=iif(bTrsErr,0,1)
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if i_Rows<0 or bTrsErr
                * --- Error: insert not accepted
                i_Error=MSG_INSERT_ERROR
                return
              endif
              * --- Write into DIC_INTE
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.DIC_INTE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"DIIMPUTI ="+cp_NullLink(cp_ToStrODBC(this.w_DIIMPUTI_COLL_ORIGINE),'DIC_INTE','DIIMPUTI');
                    +i_ccchkf ;
                +" where ";
                    +"DISERIAL = "+cp_ToStrODBC(this.w_DICINTE_INDIRETTA_ORIGINE);
                       )
              else
                update (i_cTable) set;
                    DIIMPUTI = this.w_DIIMPUTI_COLL_ORIGINE;
                    &i_ccchkf. ;
                 where;
                    DISERIAL = this.w_DICINTE_INDIRETTA_ORIGINE;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
            this.w_PARAM_FILTRO = "P"
            this.w_SERIALE_DICHIARAZIONE = this.w_RIFDIC
            * --- L'importo da stornare sulla dichiarazione di intento deve essere letto 
            *     dalla tabella "DICDINTE"
            * --- Select from DICDINTE8
            do vq_exec with 'DICDINTE8',this,'_Curs_DICDINTE8','',.f.,.t.
            if used('_Curs_DICDINTE8')
              select _Curs_DICDINTE8
              locate for 1=1
              do while not(eof())
              * --- Se esiste un record nella query Dicdinte8 vuol dire che al documento
              *     sono associate due dichiarazioni di intento
              this.w_DIIMPUTI_ORIGINE = this.w_DIIMPUTI_ORIGINE - _Curs_DICDINTE8.DDIMPDIC
              this.w_DDIMPDIC_ORIGINE = _Curs_DICDINTE8.DDIMPDIC*-1
              exit
                select _Curs_DICDINTE8
                continue
              enddo
              use
            endif
          endif
          this.w_NUMRIGA_ORIGINE = this.w_NUMRIGA_ORIGINE+1
          this.w_DDVISMAN = IIF(this.w_DIC_COLLEGATA=.t.,"I",this.w_DDVISMAN)
          * --- Insert into DICDINTE
          i_nConn=i_TableProp[this.DICDINTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DICDINTE_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DICDINTE_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"DDSERIAL"+",CPROWNUM"+",CPROWORD"+",DDDOCSER"+",DDIMPDIC"+",DDTIPINS"+",DDDATOPE"+",DDTIPAGG"+",DDCODUTE"+",DDVISMAN"+",DDRECCOL"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.oParentObject.w_OLDRIFDIC),'DICDINTE','DDSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA_ORIGINE),'DICDINTE','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA_ORIGINE*10),'DICDINTE','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DICDINTE','DDDOCSER');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DDIMPDIC_ORIGINE),'DICDINTE','DDIMPDIC');
            +","+cp_NullLink(cp_ToStrODBC("D"),'DICDINTE','DDTIPINS');
            +","+cp_NullLink(cp_ToStrODBC(i_datsys),'DICDINTE','DDDATOPE');
            +","+cp_NullLink(cp_ToStrODBC("M"),'DICDINTE','DDTIPAGG');
            +","+cp_NullLink(cp_ToStrODBC(i_codute),'DICDINTE','DDCODUTE');
            +","+cp_NullLink(cp_ToStrODBC("N"),'DICDINTE','DDVISMAN');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DDRECCOL),'DICDINTE','DDRECCOL');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'DDSERIAL',this.oParentObject.w_OLDRIFDIC,'CPROWNUM',this.w_NUMRIGA_ORIGINE,'CPROWORD',this.w_NUMRIGA_ORIGINE*10,'DDDOCSER',this.oParentObject.w_MVSERIAL,'DDIMPDIC',this.w_DDIMPDIC_ORIGINE,'DDTIPINS',"D",'DDDATOPE',i_datsys,'DDTIPAGG',"M",'DDCODUTE',i_codute,'DDVISMAN',"N",'DDRECCOL',this.w_DDRECCOL)
            insert into (i_cTable) (DDSERIAL,CPROWNUM,CPROWORD,DDDOCSER,DDIMPDIC,DDTIPINS,DDDATOPE,DDTIPAGG,DDCODUTE,DDVISMAN,DDRECCOL &i_ccchkf. );
               values (;
                 this.oParentObject.w_OLDRIFDIC;
                 ,this.w_NUMRIGA_ORIGINE;
                 ,this.w_NUMRIGA_ORIGINE*10;
                 ,this.oParentObject.w_MVSERIAL;
                 ,this.w_DDIMPDIC_ORIGINE;
                 ,"D";
                 ,i_datsys;
                 ,"M";
                 ,i_codute;
                 ,"N";
                 ,this.w_DDRECCOL;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          if this.w_DIDICCOL<>this.oParentObject.w_OLDRIFDIC OR ! this.w_DIC_COLLEGATA
            * --- Write into DIC_INTE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DIC_INTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DIIMPUTI ="+cp_NullLink(cp_ToStrODBC(this.w_DIIMPUTI_ORIGINE),'DIC_INTE','DIIMPUTI');
                  +i_ccchkf ;
              +" where ";
                  +"DISERIAL = "+cp_ToStrODBC(this.w_RIFDIC);
                     )
            else
              update (i_cTable) set;
                  DIIMPUTI = this.w_DIIMPUTI_ORIGINE;
                  &i_ccchkf. ;
               where;
                  DISERIAL = this.w_RIFDIC;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          this.w_RIFDIC = this.oParentObject.w_MVRIFDIC
          * --- Se la variabile w_Dic_Collegata � valorizzata a true allora vuol dire
          *     che devo aggiungere una riga anche nella dichiarazione di intento collegata
          if this.w_DIC_COLLEGATA
            this.w_NUMRIGA_COLL = IIF(this.w_DIDICCOL=this.oParentObject.w_OLDRIFDIC,this.w_NUMRIGA_ORIGINE+1,this.w_NUMRIGA_COLL+1)
            * --- Insert into DICDINTE
            i_nConn=i_TableProp[this.DICDINTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DICDINTE_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DICDINTE_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"DDSERIAL"+",CPROWNUM"+",CPROWORD"+",DDDOCSER"+",DDIMPDIC"+",DDTIPINS"+",DDDATOPE"+",DDTIPAGG"+",DDCODUTE"+",DDVISMAN"+",DDRECCOL"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_DIDICCOL),'DICDINTE','DDSERIAL');
              +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA_COLL),'DICDINTE','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA_COLL*10),'DICDINTE','CPROWORD');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DICDINTE','DDDOCSER');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DDIMPDIC_COLL),'DICDINTE','DDIMPDIC');
              +","+cp_NullLink(cp_ToStrODBC("I"),'DICDINTE','DDTIPINS');
              +","+cp_NullLink(cp_ToStrODBC(i_datsys),'DICDINTE','DDDATOPE');
              +","+cp_NullLink(cp_ToStrODBC("M"),'DICDINTE','DDTIPAGG');
              +","+cp_NullLink(cp_ToStrODBC(i_codute),'DICDINTE','DDCODUTE');
              +","+cp_NullLink(cp_ToStrODBC("N"),'DICDINTE','DDVISMAN');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DDRECCOL),'DICDINTE','DDRECCOL');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'DDSERIAL',this.w_DIDICCOL,'CPROWNUM',this.w_NUMRIGA_COLL,'CPROWORD',this.w_NUMRIGA_COLL*10,'DDDOCSER',this.oParentObject.w_MVSERIAL,'DDIMPDIC',this.w_DDIMPDIC_COLL,'DDTIPINS',"I",'DDDATOPE',i_datsys,'DDTIPAGG',"M",'DDCODUTE',i_codute,'DDVISMAN',"N",'DDRECCOL',this.w_DDRECCOL)
              insert into (i_cTable) (DDSERIAL,CPROWNUM,CPROWORD,DDDOCSER,DDIMPDIC,DDTIPINS,DDDATOPE,DDTIPAGG,DDCODUTE,DDVISMAN,DDRECCOL &i_ccchkf. );
                 values (;
                   this.w_DIDICCOL;
                   ,this.w_NUMRIGA_COLL;
                   ,this.w_NUMRIGA_COLL*10;
                   ,this.oParentObject.w_MVSERIAL;
                   ,this.w_DDIMPDIC_COLL;
                   ,"I";
                   ,i_datsys;
                   ,"M";
                   ,i_codute;
                   ,"N";
                   ,this.w_DDRECCOL;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            * --- Write into DIC_INTE
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DIC_INTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DIIMPUTI ="+cp_NullLink(cp_ToStrODBC(this.w_DIIMPUTI_COLL),'DIC_INTE','DIIMPUTI');
                  +i_ccchkf ;
              +" where ";
                  +"DISERIAL = "+cp_ToStrODBC(this.w_DIDICCOL);
                     )
            else
              update (i_cTable) set;
                  DIIMPUTI = this.w_DIIMPUTI_COLL;
                  &i_ccchkf. ;
               where;
                  DISERIAL = this.w_DIDICCOL;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
      endif
    else
      * --- Nel caso in cui nel documento non era presente la dichiarazione di intento
      *     ma � stata aggiunta in modifica il tipo aggiornamento deve diventare uguale
      *     ad "Inserimento"
      if this.w_DIC_COLLEGATA
        this.w_NUMRIGA_COLL = this.w_NUMRIGA_COLL+1
        * --- Insert into DICDINTE
        i_nConn=i_TableProp[this.DICDINTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DICDINTE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DICDINTE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"DDSERIAL"+",CPROWNUM"+",CPROWORD"+",DDDOCSER"+",DDIMPDIC"+",DDTIPINS"+",DDDATOPE"+",DDTIPAGG"+",DDCODUTE"+",DDVISMAN"+",DDRECCOL"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_DIDICCOL),'DICDINTE','DDSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA_COLL),'DICDINTE','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA_COLL*10),'DICDINTE','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DICDINTE','DDDOCSER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DDIMPDIC_COLL),'DICDINTE','DDIMPDIC');
          +","+cp_NullLink(cp_ToStrODBC("I"),'DICDINTE','DDTIPINS');
          +","+cp_NullLink(cp_ToStrODBC(i_datsys),'DICDINTE','DDDATOPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DDTIPAGG),'DICDINTE','DDTIPAGG');
          +","+cp_NullLink(cp_ToStrODBC(i_codute),'DICDINTE','DDCODUTE');
          +","+cp_NullLink(cp_ToStrODBC("N"),'DICDINTE','DDVISMAN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_DDRECCOL),'DICDINTE','DDRECCOL');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'DDSERIAL',this.w_DIDICCOL,'CPROWNUM',this.w_NUMRIGA_COLL,'CPROWORD',this.w_NUMRIGA_COLL*10,'DDDOCSER',this.oParentObject.w_MVSERIAL,'DDIMPDIC',this.w_DDIMPDIC_COLL,'DDTIPINS',"I",'DDDATOPE',i_datsys,'DDTIPAGG',this.w_DDTIPAGG,'DDCODUTE',i_codute,'DDVISMAN',"N",'DDRECCOL',this.w_DDRECCOL)
          insert into (i_cTable) (DDSERIAL,CPROWNUM,CPROWORD,DDDOCSER,DDIMPDIC,DDTIPINS,DDDATOPE,DDTIPAGG,DDCODUTE,DDVISMAN,DDRECCOL &i_ccchkf. );
             values (;
               this.w_DIDICCOL;
               ,this.w_NUMRIGA_COLL;
               ,this.w_NUMRIGA_COLL*10;
               ,this.oParentObject.w_MVSERIAL;
               ,this.w_DDIMPDIC_COLL;
               ,"I";
               ,i_datsys;
               ,this.w_DDTIPAGG;
               ,i_codute;
               ,"N";
               ,this.w_DDRECCOL;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Write into DIC_INTE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DIC_INTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"DIIMPUTI ="+cp_NullLink(cp_ToStrODBC(this.w_DIIMPUTI_COLL),'DIC_INTE','DIIMPUTI');
              +i_ccchkf ;
          +" where ";
              +"DISERIAL = "+cp_ToStrODBC(this.w_DIDICCOL);
                 )
        else
          update (i_cTable) set;
              DIIMPUTI = this.w_DIIMPUTI_COLL;
              &i_ccchkf. ;
           where;
              DISERIAL = this.w_DIDICCOL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_DDVISMAN = "I"
      endif
    endif
    this.w_NUMRIGA = this.w_NUMRIGA+1
    * --- Insert into DICDINTE
    i_nConn=i_TableProp[this.DICDINTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DICDINTE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DICDINTE_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DDSERIAL"+",CPROWNUM"+",CPROWORD"+",DDDOCSER"+",DDIMPDIC"+",DDTIPINS"+",DDDATOPE"+",DDTIPAGG"+",DDCODUTE"+",DDVISMAN"+",DDRECCOL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_RIFDIC),'DICDINTE','DDSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA),'DICDINTE','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_NUMRIGA*10),'DICDINTE','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'DICDINTE','DDDOCSER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DDIMPDIC),'DICDINTE','DDIMPDIC');
      +","+cp_NullLink(cp_ToStrODBC("D"),'DICDINTE','DDTIPINS');
      +","+cp_NullLink(cp_ToStrODBC(i_datsys),'DICDINTE','DDDATOPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DDTIPAGG),'DICDINTE','DDTIPAGG');
      +","+cp_NullLink(cp_ToStrODBC(i_codute),'DICDINTE','DDCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DDVISMAN),'DICDINTE','DDVISMAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DDRECCOL),'DICDINTE','DDRECCOL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DDSERIAL',this.w_RIFDIC,'CPROWNUM',this.w_NUMRIGA,'CPROWORD',this.w_NUMRIGA*10,'DDDOCSER',this.oParentObject.w_MVSERIAL,'DDIMPDIC',this.w_DDIMPDIC,'DDTIPINS',"D",'DDDATOPE',i_datsys,'DDTIPAGG',this.w_DDTIPAGG,'DDCODUTE',i_codute,'DDVISMAN',this.w_DDVISMAN,'DDRECCOL',this.w_DDRECCOL)
      insert into (i_cTable) (DDSERIAL,CPROWNUM,CPROWORD,DDDOCSER,DDIMPDIC,DDTIPINS,DDDATOPE,DDTIPAGG,DDCODUTE,DDVISMAN,DDRECCOL &i_ccchkf. );
         values (;
           this.w_RIFDIC;
           ,this.w_NUMRIGA;
           ,this.w_NUMRIGA*10;
           ,this.oParentObject.w_MVSERIAL;
           ,this.w_DDIMPDIC;
           ,"D";
           ,i_datsys;
           ,this.w_DDTIPAGG;
           ,i_codute;
           ,this.w_DDVISMAN;
           ,this.w_DDRECCOL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into DIC_INTE
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DIC_INTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DIC_INTE_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DIIMPUTI ="+cp_NullLink(cp_ToStrODBC(this.w_DIIMPUTI),'DIC_INTE','DIIMPUTI');
          +i_ccchkf ;
      +" where ";
          +"DISERIAL = "+cp_ToStrODBC(this.w_RIFDIC);
             )
    else
      update (i_cTable) set;
          DIIMPUTI = this.w_DIIMPUTI;
          &i_ccchkf. ;
       where;
          DISERIAL = this.w_RIFDIC;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili Inizializzate
    * --- Gestione rischio
    * --- Gestione Progetti
    * --- Variabili Lotti
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Operazioni a livello di testata documento
    if this.w_CFUNC = "Query"
      do case
        case g_OFFE="S" AND this.oParentObject.w_MVFLOFFE="S"
          * --- Toglie Riferimenti a Offerta
          GSOF_BMK(this,"D", this.oParentObject.w_MVSERIAL)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        case g_GPOS="S" AND this.oParentObject.w_MVFLOFFE $ "DF"
          * --- Verifica se Documento Eliminabile ed elimina riferimenti in MDRIFCOR (se Differita) o MDRIFDOC (se Fattura Fiscale)
          GSPS_BKD(this,this.oParentObject.w_MVSERIAL, this.oParentObject.w_MVFLOFFE)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Se w_STORNA .T. deve stornare i gli scarichi dai Saldi in quanto restano eseguiti sulla Vendita Negozio
          this.w_STORNA = this.w_OK
        case this.oParentObject.w_FLGEFA="B" And NOT EMPTY(this.oParentObject.w_MVRIFFAD)
          * --- Legge il Tipo Fatturazione associata al Cliente
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANTIPFAT"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_MVTIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVCODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANTIPFAT;
              from (i_cTable) where;
                  ANTIPCON = this.oParentObject.w_MVTIPCON;
                  and ANCODICE = this.oParentObject.w_MVCODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPFAT = NVL(cp_ToDate(_read_.ANTIPFAT),cp_NullValue(_read_.ANTIPFAT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Elimina il Riferimento al Documento sul Dettaglio Fatture Differite
          * --- Delete from DET_DIFF
          i_nConn=i_TableProp[this.DET_DIFF_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DET_DIFF_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                  +"DPSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVRIFFAD);
                  +" and DPSERDOC = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                   )
          else
            delete from (i_cTable) where;
                  DPSERIAL = this.oParentObject.w_MVRIFFAD;
                  and DPSERDOC = this.oParentObject.w_MVSERIAL;

            i_Rows=_tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          * --- .............se non ci sono documenti cancello anche la testata
          * --- Se attiva la nuova gesionte righe descrittive non svolgo la vecchia WRITE..
          this.w_LASTDET = GSVE_BGE( This , "NEW" , this.oParentObject.w_MVRIFFAD )
          * --- Ho eliminato tutte le fatture del piano ?
          if GSVE_BGE( This, "CHECK" , this.oParentObject.w_MVRIFFAD )
            * --- Delete from FAT_DIFF
            i_nConn=i_TableProp[this.FAT_DIFF_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.FAT_DIFF_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"PSSERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVRIFFAD);
                     )
            else
              delete from (i_cTable) where;
                    PSSERIAL = this.oParentObject.w_MVRIFFAD;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
            if this.w_LASTDET
              GSVE_BGE(this, "RIAPRE" , this.oParentObject.w_MVRIFFAD )
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
        case g_COMM="S" And NOT EMPTY(this.oParentObject.w_MVMOVCOM)
          * --- Verifico che il documento non si� stato generato da una tempificazione
          *     Vado a leggere nel  Movimento di commessa attitiv� e Commessa (l'utente potrebbe modificare proprio quelle)
          *     Chiamo un Batch esterno, i movimenti NON SONO NELL'ANALISI PRINCIPALE
          GSPC_BDO(this,this.oParentObject.w_MVMOVCOM, "T")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if NOT EMPTY(this.w_CODCOM) AND NOT EMPTY(this.w_CODATT)
            this.w_STATUS = " "
            * --- Read from ATTIVITA
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.ATTIVITA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "AT_STATO"+;
                " from "+i_cTable+" ATTIVITA where ";
                    +"ATCODCOM = "+cp_ToStrODBC(this.w_CODCOM);
                    +" and ATTIPATT = "+cp_ToStrODBC("A");
                    +" and ATCODATT = "+cp_ToStrODBC(this.w_CODATT);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                AT_STATO;
                from (i_cTable) where;
                    ATCODCOM = this.w_CODCOM;
                    and ATTIPATT = "A";
                    and ATCODATT = this.w_CODATT;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_STATUS = NVL(cp_ToDate(_read_.AT_STATO),cp_NullValue(_read_.AT_STATO))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            do case
              case this.w_STATUS="L"
                this.w_OK = .F.
                this.w_MESS = ah_Msgformat("Attivit� lanciata. Impossibile variare")
              case this.w_STATUS="F"
                this.w_OK = .F.
                this.w_MESS = ah_Msgformat("Attivita completatata. Impossibile variare")
              otherwise
                * --- Stempifico l'Attivit�
                * --- Write into ATTIVITA
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.ATTIVITA_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIVITA_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"AT_STATO ="+cp_NullLink(cp_ToStrODBC("C"),'ATTIVITA','AT_STATO');
                      +i_ccchkf ;
                  +" where ";
                      +"ATCODCOM = "+cp_ToStrODBC(this.w_CODCOM);
                      +" and ATTIPATT = "+cp_ToStrODBC("A");
                      +" and ATCODATT = "+cp_ToStrODBC(this.w_CODATT);
                         )
                else
                  update (i_cTable) set;
                      AT_STATO = "C";
                      &i_ccchkf. ;
                   where;
                      ATCODCOM = this.w_CODCOM;
                      and ATTIPATT = "A";
                      and ATCODATT = this.w_CODATT;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
            endcase
          endif
          if this.w_OK AND this.oParentObject.w_FLCASH="S"
            * --- Elimina Eventuali Scadenze Commesse
            GSPC_BDO(this,this.oParentObject.w_MVSERIAL, "S")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
      endcase
    endif
    if this.w_OK AND g_PROD="S" AND NOT EMPTY(this.oParentObject.w_MVRIFODL) And Not ( UPPER(ALLTRIM(this.oParentObject.w_MVRIFODL))= "CARICO" OR UPPER(ALLTRIM(this.oParentObject.w_MVRIFODL)) = "SCARICO" )
      * --- Documento Generato da piano ODL storna i Riferimenti all'ODL
      GSCO_BDO(this,IIF( this.w_CFUNC = "Query" , "E","V"), this.oParentObject.w_MVSERIAL, this.oParentObject.w_OFLPROV, this.oParentObject.w_MVFLPROV)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Nel Caso in cui passo il Documento da Stato provvisorio a confermato
      *     devo stornare i saldi sull'OCL
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Storno dal documento di origine il rischio...
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVCLADOC,MVSPEINC,MVSPETRA,MVSPEIMB,MVIVAINC,MVIVAIMB,MVIVATRA,MVCODIVE,MVCODVAL,MVCAOVAL,MVTIPDOC"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVCLADOC,MVSPEINC,MVSPETRA,MVSPEIMB,MVIVAINC,MVIVAIMB,MVIVATRA,MVCODIVE,MVCODVAL,MVCAOVAL,MVTIPDOC;
        from (i_cTable) where;
            MVSERIAL = this.w_SERRIF;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CLDOOR = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
      this.w_SPEINC = NVL(cp_ToDate(_read_.MVSPEINC),cp_NullValue(_read_.MVSPEINC))
      this.w_SPETRA = NVL(cp_ToDate(_read_.MVSPETRA),cp_NullValue(_read_.MVSPETRA))
      this.w_SPEIMB = NVL(cp_ToDate(_read_.MVSPEIMB),cp_NullValue(_read_.MVSPEIMB))
      this.w_CODIVAINC = NVL(cp_ToDate(_read_.MVIVAINC),cp_NullValue(_read_.MVIVAINC))
      this.w_CODIVAIMB = NVL(cp_ToDate(_read_.MVIVAIMB),cp_NullValue(_read_.MVIVAIMB))
      this.w_CODIVATRA = NVL(cp_ToDate(_read_.MVIVATRA),cp_NullValue(_read_.MVIVATRA))
      this.w_CODIVE = NVL(cp_ToDate(_read_.MVCODIVE),cp_NullValue(_read_.MVCODIVE))
      this.w_CODVALOR = NVL(cp_ToDate(_read_.MVCODVAL),cp_NullValue(_read_.MVCODVAL))
      this.w_CAMOR = NVL(cp_ToDate(_read_.MVCAOVAL),cp_NullValue(_read_.MVCAOVAL))
      this.w_OTIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_OFLRISC = " "
    * --- Verifico se la causale del documento di origine partecipa
    *     al rischio, se si allora storno...
    * --- Read from TIP_DOCU
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TDFLRISC"+;
        " from "+i_cTable+" TIP_DOCU where ";
            +"TDTIPDOC = "+cp_ToStrODBC(this.w_OTIPDOC);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TDFLRISC;
        from (i_cTable) where;
            TDTIPDOC = this.w_OTIPDOC;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_OFLRISC = NVL(cp_ToDate(_read_.TDFLRISC),cp_NullValue(_read_.TDFLRISC))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if this.w_OFLRISC$"SD"
      * --- Storno la parte evasa...
      *     Due casi tipo riga forfettaria guida OIMPEVA caso riga normale guida QTAIMP
      if this.w_TIPRIG="F"
        * --- Differenza tra la parte evasa prima e dopo l'evasione
        this.w_IMPEVATOT = ( this.w_OIMPNAZ / this.w_ORD_PRZ ) * ( Min( this.w_OIMPEVA , this.w_ORD_PRZ ) - this.w_OIMPEVA_R )
        * --- Se riga che chiude aggiungo l'eventuale parte non evasa...
        if this.w_FLERIF="S" And ( this.w_ORD_PRZ >this.w_OIMPEVA )
          this.w_IMPEVATOT = this.w_IMPEVATOT + ( this.w_OIMPNAZ / this.w_ORD_PRZ ) * ( this.w_ORD_PRZ - this.w_OIMPEVA )
        endif
      else
        * --- If w_OQTAEVA<>w_QTAIMP solo per evitare problemi di calcolo..
        *     (Non gestito Import Lire/Euro)
        if this.w_OQTAMOV <> this.w_QTAIMP
          this.w_IMPEVATOT = (this.w_OIMPNAZ / this.w_OQTAMOV )* this.w_QTAIMP
        else
          this.w_IMPEVATOT = this.w_OIMPNAZ
        endif
        if this.w_FLERIF="S" And (this.w_OQTAMOV - this.w_OQTAEVA)<>0 AND lower(this.w_PADRE.class)<>"tgsar_bgd"
          * --- La riga chiude il documento d'origine, devo stornare dal rischio
          *     anche la parte che eventualmente non � stata importata (quindi aperta sul doc. origine)
          *     (Es. Ordine da 10 chiuso con un DDT da 4)
          this.w_IMPEVATOT = this.w_IMPEVATOT + ( this.w_OIMPNAZ / this.w_OQTAMOV ) * (this.w_OQTAMOV - this.w_OQTAEVA)
        endif
      endif
      * --- Leggo percentuali IVA
      if NOT EMPTY(this.w_CODIVE)
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIVA,IVPERIND"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIVA,IVPERIND;
            from (i_cTable) where;
                IVCODIVA = this.w_CODIVE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_RISPERIV = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          this.w_INDIVE = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_IVAINC = this.w_RISPERIV
        this.w_IVAIMB = this.w_RISPERIV
        this.w_IVATRA = this.w_RISPERIV
      else
        if NOT EMPTY(this.w_CODIVA)
          * --- Read from VOCIIVA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOCIIVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "IVPERIVA,IVPERIND"+;
              " from "+i_cTable+" VOCIIVA where ";
                  +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              IVPERIVA,IVPERIND;
              from (i_cTable) where;
                  IVCODIVA = this.w_CODIVA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_RISPERIV = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
            this.w_INDIVA = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if EMPTY(this.w_CODIVAIMB)
          this.w_CODIVAIMB = g_COIIMB
        endif
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIVA"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVAIMB);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIVA;
            from (i_cTable) where;
                IVCODIVA = this.w_CODIVAIMB;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_IVAIMB = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if EMPTY(this.w_CODIVAINC)
          this.w_CODIVAINC = g_COIINC
        endif
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIVA"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVAINC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIVA;
            from (i_cTable) where;
                IVCODIVA = this.w_CODIVAINC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_IVAINC = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if EMPTY(this.w_CODIVATRA)
          this.w_CODIVATRA = g_COITRA
        endif
        * --- Read from VOCIIVA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VOCIIVA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "IVPERIVA"+;
            " from "+i_cTable+" VOCIIVA where ";
                +"IVCODIVA = "+cp_ToStrODBC(this.w_CODIVATRA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            IVPERIVA;
            from (i_cTable) where;
                IVCODIVA = this.w_CODIVATRA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_IVATRA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Applico IVA importo evaso...
      this.w_IVA = cp_ROUND((this.w_IMPEVATOT/100)*this.w_RISPERIV,this.oParentObject.w_DECTOP)
      this.w_IVAACC = 0
      this.w_SPESACC = 0
      this.w_FLGOK = "N"
      * --- Controllo se nel documento esiste almeno una riga evasa
      *     in tal caso non storno le spese accessorie.
      * --- Select from GSVEBBER
      do vq_exec with 'GSVEBBER',this,'_Curs_GSVEBBER','',.f.,.t.
      if used('_Curs_GSVEBBER')
        select _Curs_GSVEBBER
        locate for 1=1
        do while not(eof())
        * --- Se il numero di riga della riga che evade coincide con la riga in esame
        *     significa che solo questo documento ha per ora evaso il documento di 
        *     origine e la riga in esame � quella con il CPROWNUM pi� basso (Query
        *     esegue un Min)
        if Nvl(_Curs_GSVEBBER.NUMROW ,0)=this.w_ROWORD
          this.w_SPESACC = this.w_SPEINC+this.w_SPETRA+this.w_SPEIMB
          if NOT EMPTY(this.w_RISPERIV)
            this.w_IVAACC = cp_ROUND((this.w_SPEINC/100)*this.w_IVAINC+(this.w_SPEIMB/100)*this.w_IVAIMB+(this.w_SPETRA/100)*this.w_IVATRA,this.oParentObject.w_DECTOP)
          endif
          if this.w_CODVALOR<>this.oParentObject.w_MVVALNAZ AND (this.w_SPESACC<>0 OR this.w_IVAACC<>0)
            * --- Se il Documento e' in Valuta Converte in Moneta di Conto
            this.w_SPESACC = cp_ROUND(VAL2MON(this.w_SPESACC, this.w_CAMOR, this.oParentObject.w_CAONAZ, this.oParentObject.w_MVDATDOC, this.oParentObject.w_MVVALNAZ), this.oParentObject.w_DECTOP)
            this.w_IVAACC = cp_ROUND(VAL2MON(this.w_IVAACC, this.w_CAMOR, this.oParentObject.w_CAONAZ, this.oParentObject.w_MVDATDOC, this.oParentObject.w_MVVALNAZ), this.oParentObject.w_DECTOP)
          endif
          this.w_FLGOK = "S"
        endif
          select _Curs_GSVEBBER
          continue
        enddo
        use
      endif
      * --- Se � la prima riga che evade il documento di origine allora
      *     storno anche le spese di piede
      if this.w_FLGOK="S"
        this.w_TmpFido = this.w_SPESACC+this.w_IVAACC
      else
        this.w_TmpFido = 0
      endif
      * --- A seconda del tipo omaggio di riga aggiungo all'importo da stornare al fido
      do case
        case this.w_FLOMAG="X"
          this.w_TmpFido = this.w_TmpFido + this.w_IMPEVATOT + this.w_IVA
        case this.w_FLOMAG="I"
          this.w_TmpFido = this.w_TmpFido + this.w_IVA
      endcase
      * --- Controllo documento di origine e aggiorno l'importo conseguente 
      *     (Ordini in essere 4, 
      *     DDT 5,
      *     Fatture non contabilizzate 6)
      do case
        case this.w_CLDOOR="OR"
          this.w_FIDS4 = this.w_FIDS4 + this.w_TmpFido
        case this.w_CLDOOR="DT"
          this.w_FIDS5 = this.w_FIDS5 + this.w_TmpFido
        otherwise
          this.w_FIDS6 = this.w_FIDS6 + this.w_TmpFido
      endcase
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Progetti - Aggiorno Saldi Attivit� (Import gestisce solo Ordinato)
    * --- Try
    local bErr_0521F480
    bErr_0521F480=bTrsErr
    this.Try_0521F480()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_0521F480
    * --- End
  endproc
  proc Try_0521F480()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Se evasione per valore storno l'importo evaso (Cambiandolo dalla valuta del documento nella valuta della commessa)
    *     Nel caso di cancellazione w_OIMPEVA potrebbe valere zero !
    if this.w_OIMPEVA<>0 OR this.w_ORD_EVA<>0
      * --- L'importo evasione � espresso nella valuta del documento
      this.w_IMPCOMCOM = IIF(this.w_OLDEVAS1 $ "SE", this.w_ORD_PRZ, MIN(this.w_ORD_PRZ, this.w_ORD_EVA))
      if this.w_TIPRIG="F" and this.oParentObject.w_OLDEVAS="E"
        this.w_IMPCOMCOM = this.w_IMPCOMCOM*-1
      else
        this.w_IMPCOMCOM = this.w_IMPCOMCOM - IIF(this.w_OFLEVAS="S", this.w_ORD_PRZ, MIN(this.w_ORD_PRZ, this.w_OIMPEVA))
      endif
      * --- Eseguo la proporzione (per considerare eventuali importi ripartiti su riga)
      if this.w_ORD_PRZ<>0 And this.w_ORD_PRZ<>this.w_ORIMPCOM
        this.w_IMPCOMCOM = (this.w_IMPCOMCOM / this.w_ORD_PRZ) * this.w_ORIMPCOM
      endif
    else
      * --- Import per Q.t� (tutto nella valuta della commessa)
      *     Se riga non saldata completamente e cambiata la Quantit�
      if this.w_COAPPSAL<>0 And this.w_OQTAMOV<>0
        if this.w_FLDEL AND this.w_ORIMPCOM=0
          if this.w_ORIMPCOM=0
            * --- Nel caso in cui cancello un DDT che evade ordine totalmente evaso w_orimpcom � uguale a 0 e devo ripristinarlo
            this.w_IMPCOMCOM = (this.w_OIMPNAZ/this.w_OQTAUM1)* ( this.w_OQTAUM1-this.w_OQTAEV1)
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(this.w_IMPCOMCOM),'DOC_DETT','MVIMPCOM');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF);
                  +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                     )
            else
              update (i_cTable) set;
                  MVIMPCOM = this.w_IMPCOMCOM;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.w_SERRIF;
                  and CPROWNUM = this.w_ROWRIF;
                  and MVNUMRIF = this.w_NUMRIF;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        else
          * --- Se non importo tutto faccio la proporzione tra la quantit� dell'ordine e la Qt� del DDT
          this.w_IMPCOMCOM = (this.w_ORIMPCOM / this.w_OQTAMOV ) * this.w_COAPPSAL
        endif
      endif
    endif
    * --- Applico l'arrotondamento...
    if this.w_CO1CODCOM = this.oParentObject.w_MVCODCOM
      this.w_IMPCOMCOM = cp_ROUND(this.w_IMPCOMCOM, this.oParentObject.w_DECCOM )
    else
      * --- Read from CAN_TIER
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAN_TIER_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2],.t.,this.CAN_TIER_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CNCODVAL"+;
          " from "+i_cTable+" CAN_TIER where ";
              +"CNCODCAN = "+cp_ToStrODBC(this.w_CO1CODCOM);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CNCODVAL;
          from (i_cTable) where;
              CNCODCAN = this.w_CO1CODCOM;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODVAL = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se devo applicare un cambio...
      if this.oParentObject.w_MVVALNAZ<>this.w_CODVAL
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_CODVAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VADECTOT;
            from (i_cTable) where;
                VACODVAL = this.w_CODVAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_LDECCOM = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      else
        this.w_LDECCOM = this.oParentObject.w_DECTOP
      endif
      this.w_IMPCOMCOM = cp_ROUND(this.w_IMPCOMCOM, this.w_LDECCOM )
    endif
    * --- Write into MA_COSTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MA_COSTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
      i_cOp1=cp_SetTrsOp(this.w_ORDFLORCO,'CSORDIN','this.w_IMPCOMCOM',this.w_IMPCOMCOM,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CSORDIN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSORDIN');
          +i_ccchkf ;
      +" where ";
          +"CSCODCOM = "+cp_ToStrODBC(this.w_CO1CODCOM);
          +" and CSTIPSTR = "+cp_ToStrODBC("A");
          +" and CSCODMAT = "+cp_ToStrODBC(this.w_CODATTCOM);
          +" and CSCODCOS = "+cp_ToStrODBC(this.w_CO1CODCOS);
             )
    else
      update (i_cTable) set;
          CSORDIN = &i_cOp1.;
          &i_ccchkf. ;
       where;
          CSCODCOM = this.w_CO1CODCOM;
          and CSTIPSTR = "A";
          and CSCODMAT = this.w_CODATTCOM;
          and CSCODCOS = this.w_CO1CODCOS;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione Documento di Evasione Componenti/Prodotti Finiti
    this.w_MESS = GSVE_BEC( this, this.w_DOCEVA, this.oParentObject.w_MVSERIAL, this.w_DOCROW, this.w_DOCRIG)
    this.w_OK = EMPTY(this.w_MESS)
    if this.w_OK AND !this.w_DOCRIG
      * --- Azzero variabile caller per segnalare la gsar_bea di rigenerare il documento di testata
      this.oParentObject.w_MVRIFESP = SPACE(10)
    endif
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_NR = ALLTRIM(STR(this.oParentObject.w_CPROWORD))
    this.w_KEYSAL = NVL(this.oParentObject.w_MVKEYSAL, Space(20))
    this.w_CODMAG = NVL(this.oParentObject.w_MVCODMAG, Space(5))
    this.w_CODMAT = NVL(this.oParentObject.w_MVCODMAT, Space(5))
    this.w_FLAGLO = NVL(this.oParentObject.w_MVFLLOTT," ")
    this.w_FLAG2LO = NVL(this.oParentObject.w_MVF2LOTT," ")
    this.w_CODLOT = NVL(this.oParentObject.w_MVCODLOT,Space(20))
    * --- Recupera dal transitorio il vecchio valore del codice lotto e codice articolo eventualmente modificati per aggiornare lo status
    this.w_OLDCODLOT = NVL(this.w_PADRE.GET("MVCODLOT"), this.w_CODLOT)
    this.w_OLDKEYSAL = NVL(this.w_PADRE.GET("MVKEYSAL"), this.w_COART)
    this.w_CODUBI = NVL(this.oParentObject.w_MVCODUBI,Space(20))
    this.w_CODUB2 = NVL(this.oParentObject.w_MVCODUB2,Space(20))
    this.w_COMAG = this.w_CODMAG
    this.w_OLDQTA = NVL( this.w_PADRE. Get( "MVQTAUM1" ) ,0)
    this.w_QTAUM1 = NVL(this.oParentObject.w_MVQTAUM1, 0)
    this.w_SERRIF = NVL(this.oParentObject.w_MVSERRIF, Space(10))
    this.w_TIPRIG = NVL(this.oParentObject.w_MVTIPRIG," ")
    this.w_COART = NVL(this.oParentObject.w_MVCODART, Space(20))
    this.w_LFLRISE = NVL(this.oParentObject.w_MVFLRISE," ")
    if this.w_OK And (((g_PERLOT="S" AND this.oParentObject.w_FLLOTT $ "SC" ) OR g_PERUBI="S") AND this.w_TIPRIG $ "RM" ) 
      do case
        case this.oParentObject.w_FLSTAT="E" 
          if (((this.w_QTAUM1<this.w_OLDQTA OR this.w_FLDEL ) AND (this.w_FLAGLO="-" OR this.w_FLAG2LO="-")) OR (this.w_QTAUM1>this.w_OLDQTA AND (this.w_FLAGLO="+" OR this.w_FLAG2LO="+"))) 
            * --- Aggiorno Status in caso di Modifica della Quantit� o Cancellazione di uno Scarico
            if Not Empty(this.w_CODLOT)
              this.w_QTAESI = 0
              this.w_TESDIS = .F.
              this.w_SUQTRPER = 0
              this.w_SUQTAPER = 0
              * --- Select from SALDILOT
              i_nConn=i_TableProp[this.SALDILOT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2],.t.,this.SALDILOT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select SUM (SUQTAPER) as SUQTAPER , SUM (SUQTRPER) as SUQTRPER  from "+i_cTable+" SALDILOT ";
                    +" where "+cp_ToStrODBC(this.w_COART)+"=SUCODART AND "+cp_ToStrODBC(this.w_CODLOT)+"=SUCODLOT";
                    +" group by SUCODART,SUCODLOT";
                     ,"_Curs_SALDILOT")
              else
                select SUM (SUQTAPER) as SUQTAPER , SUM (SUQTRPER) as SUQTRPER from (i_cTable);
                 where this.w_COART=SUCODART AND this.w_CODLOT=SUCODLOT;
                 group by SUCODART,SUCODLOT;
                  into cursor _Curs_SALDILOT
              endif
              if used('_Curs_SALDILOT')
                select _Curs_SALDILOT
                locate for 1=1
                do while not(eof())
                this.w_TESDIS = .T.
                this.w_QTAESI = this.w_QTAESI +_Curs_SALDILOT.SUQTAPER - _Curs_SALDILOT.SUQTRPER
                  select _Curs_SALDILOT
                  continue
                enddo
                use
              endif
              this.w_QTAESI = icase(this.w_FLAGLO="-" OR this.w_FLAG2LO="-",this.w_QTAESI +this.w_OLDQTA - this.w_QTAUM1,this.w_FLAGLO="+" OR this.w_FLAG2LO="+",this.w_QTAESI -this.w_OLDQTA + this.w_QTAUM1)
              if this.w_QTAESI>0 AND this.w_TESDIS AND EMPTY(this.w_LFLRISE)
                * --- Write into LOTTIART
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.LOTTIART_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.LOTTIART_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"LOFLSTAT ="+cp_NullLink(cp_ToStrODBC("D"),'LOTTIART','LOFLSTAT');
                      +i_ccchkf ;
                  +" where ";
                      +"LOCODICE = "+cp_ToStrODBC(this.w_CODLOT);
                      +" and LOCODART = "+cp_ToStrODBC(this.w_COART);
                         )
                else
                  update (i_cTable) set;
                      LOFLSTAT = "D";
                      &i_ccchkf. ;
                   where;
                      LOCODICE = this.w_CODLOT;
                      and LOCODART = this.w_COART;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error='errore scrittura lotti'
                  return
                endif
              endif
            endif
          else
            if EMPTY(this.w_SERRIF) and this.w_OLDQTA<>this.w_QTAUM1 AND this.oParentObject.w_DISLOT="S"
              this.w_MESS = ah_Msgformat("Disponibilit� lotto: %1 esaurita", ALLTRIM(this.w_CODLOT) )
              this.w_OK = .F.
            endif
          endif
        case this.oParentObject.w_FLSTAT="S"
          if this.w_QTAUM1>this.w_OLDQTA And Not this.w_FLDEL
            this.w_MESS = ah_Msgformat("Lotto: %1 sospeso", ALLTRIM(this.w_CODLOT) )
            this.w_OK = .F.
          endif
        otherwise
          * --- Aggiorno Status Recuperando esistenza generale del Lotto
          * --- Testa anche se evade un doc. che movimenta il riservato
          if this.w_FLAGLO $ "+-" AND EMPTY(this.oParentObject.w_FLRRIF)
            this.w_QTAESI = 0
            this.w_TESDIS = .F.
            this.w_SUQTRPER = 0
            this.w_SUQTAPER = 0
            if Not Empty(this.w_CODLOT)
              * --- Select from SALDILOT
              i_nConn=i_TableProp[this.SALDILOT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2],.t.,this.SALDILOT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select SUM (SUQTAPER) as SUQTAPER , SUM (SUQTRPER) as SUQTRPER  from "+i_cTable+" SALDILOT ";
                    +" where "+cp_ToStrODBC(this.w_COART)+"=SUCODART AND "+cp_ToStrODBC(this.w_CODLOT)+"=SUCODLOT";
                    +" group by SUCODART,SUCODLOT";
                     ,"_Curs_SALDILOT")
              else
                select SUM (SUQTAPER) as SUQTAPER , SUM (SUQTRPER) as SUQTRPER from (i_cTable);
                 where this.w_COART=SUCODART AND this.w_CODLOT=SUCODLOT;
                 group by SUCODART,SUCODLOT;
                  into cursor _Curs_SALDILOT
              endif
              if used('_Curs_SALDILOT')
                select _Curs_SALDILOT
                locate for 1=1
                do while not(eof())
                this.w_TESDIS = .T.
                this.w_QTAESI = this.w_QTAESI +_Curs_SALDILOT.SUQTAPER - _Curs_SALDILOT.SUQTRPER
                  select _Curs_SALDILOT
                  continue
                enddo
                use
              endif
              if this.w_QTAESI<=0 AND this.w_TESDIS
                * --- Write into LOTTIART
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.LOTTIART_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.LOTTIART_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"LOFLSTAT ="+cp_NullLink(cp_ToStrODBC("E"),'LOTTIART','LOFLSTAT');
                      +i_ccchkf ;
                  +" where ";
                      +"LOCODICE = "+cp_ToStrODBC(this.w_CODLOT);
                      +" and LOCODART = "+cp_ToStrODBC(this.w_COART);
                         )
                else
                  update (i_cTable) set;
                      LOFLSTAT = "E";
                      &i_ccchkf. ;
                   where;
                      LOCODICE = this.w_CODLOT;
                      and LOCODART = this.w_COART;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
            if Not Empty(this.w_CODUBI)
              * --- Select from SALDILOT
              i_nConn=i_TableProp[this.SALDILOT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2],.t.,this.SALDILOT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select SUM (SUQTAPER) as SUQTAPER , SUM (SUQTRPER) as SUQTRPER  from "+i_cTable+" SALDILOT ";
                    +" where "+cp_ToStrODBC(this.w_COART)+"=SUCODART AND "+cp_ToStrODBC(this.w_CODUBI)+"=SUCODUBI";
                    +" group by SUCODART,SUCODLOT";
                     ,"_Curs_SALDILOT")
              else
                select SUM (SUQTAPER) as SUQTAPER , SUM (SUQTRPER) as SUQTRPER from (i_cTable);
                 where this.w_COART=SUCODART AND this.w_CODUBI=SUCODUBI;
                 group by SUCODART,SUCODLOT;
                  into cursor _Curs_SALDILOT
              endif
              if used('_Curs_SALDILOT')
                select _Curs_SALDILOT
                locate for 1=1
                do while not(eof())
                this.w_TESDIS = .T.
                this.w_QTAESI = this.w_QTAESI +_Curs_SALDILOT.SUQTAPER - _Curs_SALDILOT.SUQTRPER
                  select _Curs_SALDILOT
                  continue
                enddo
                use
              endif
            endif
            if this.w_QTAESI<0 AND this.oParentObject.w_FLDISP$ "SC"
              do case
                case Not Empty(this.w_CODLOT) AND Not Empty(this.w_CODUBI)
                  this.w_MESS = ah_MsgFormat("Articolo: %1%0Magazzino: %2%0Disponibilit� lotto: %3%0Ubicazione: %4%0Disponibilit� contabile articolo negativa (%5)%0", ALLTR(this.w_COART), this.w_CODMAG, ALLTRIM(this.w_CODLOT), ALLTRIM(this.w_CODUBI), TRAN(this.w_QTAESI,"@Z"+v_PQ(12)))
                  this.w_OK = .F.
                case Not Empty(this.w_CODLOT)
                  if this.oParentObject.w_DISLOT = "S"
                    * --- Il controllo sulla disponibilit� del lotto � attivo
                    this.w_MESS = ah_MsgFormat("Articolo: %1%0Magazzino: %2%0Disponibilit� lotto: %3%0Disponibilit� contabile articolo negativa (%4)%0", ALLTR(this.w_COART), this.w_CODMAG, ALLTRIM(this.w_CODLOT), TRAN(this.w_QTAESI,"@Z"+v_PQ(12)))
                    this.w_OK = .F.
                  endif
                case Not Empty(this.w_CODUBI)
                  this.w_MESS = ah_MsgFormat("Articolo: %1%0Magazzino: %2%0Ubicazione: %3%0Disponibilit� contabile articolo negativa (%4)%0", ALLTR(this.w_COART), this.w_CODMAG, ALLTRIM(this.w_CODUBI), TRAN(this.w_QTAESI,"@Z"+v_PQ(12)))
                  this.w_OK = .F.
                otherwise
                  this.w_MESS = ah_MsgFormat("Articolo: %1%0Magazzino: %2%0Disponibilit� contabile articolo negativa (%3)%0", ALLTR(this.w_COART), this.w_CODMAG, TRAN(this.w_QTAESI,"@Z"+v_PQ(12)))
                  this.w_OK = .F.
              endcase
            endif
          endif
      endcase
      if (this.w_CODLOT<>this.w_OLDCODLOT OR this.w_COART<>this.w_OLDKEYSAL) AND this.w_CFUNC="Edit"
        * --- Read from LOTTIART
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.LOTTIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2],.t.,this.LOTTIART_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "LOFLSTAT"+;
            " from "+i_cTable+" LOTTIART where ";
                +"LOCODICE = "+cp_ToStrODBC(this.w_OLDCODLOT);
                +" and LOCODART = "+cp_ToStrODBC(this.w_OLDKEYSAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            LOFLSTAT;
            from (i_cTable) where;
                LOCODICE = this.w_OLDCODLOT;
                and LOCODART = this.w_OLDKEYSAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLDFLSTAT = NVL(cp_ToDate(_read_.LOFLSTAT),cp_NullValue(_read_.LOFLSTAT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        do case
          case this.w_OLDFLSTAT="E" 
            if (this.w_FLAGLO="-" OR this.w_FLAG2LO="-") OR (this.w_QTAUM1>this.w_OLDQTA AND (this.w_FLAGLO="+" OR this.w_FLAG2LO="+"))
              * --- Aggiorno Status in caso di Modifica della Quantit� o Cancellazione di uno Scarico
              if Not Empty(this.w_OLDCODLOT)
                this.w_QTAESI = 0
                this.w_TESDIS = .F.
                this.w_SUQTRPER = 0
                this.w_SUQTAPER = 0
                * --- Select from SALDILOT
                i_nConn=i_TableProp[this.SALDILOT_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2],.t.,this.SALDILOT_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select SUM (SUQTAPER) as SUQTAPER , SUM (SUQTRPER) as SUQTRPER  from "+i_cTable+" SALDILOT ";
                      +" where "+cp_ToStrODBC(this.w_OLDKEYSAL)+"=SUCODART AND "+cp_ToStrODBC(this.w_OLDCODLOT)+"=SUCODLOT";
                      +" group by SUCODART,SUCODLOT";
                       ,"_Curs_SALDILOT")
                else
                  select SUM (SUQTAPER) as SUQTAPER , SUM (SUQTRPER) as SUQTRPER from (i_cTable);
                   where this.w_OLDKEYSAL=SUCODART AND this.w_OLDCODLOT=SUCODLOT;
                   group by SUCODART,SUCODLOT;
                    into cursor _Curs_SALDILOT
                endif
                if used('_Curs_SALDILOT')
                  select _Curs_SALDILOT
                  locate for 1=1
                  do while not(eof())
                  this.w_TESDIS = .T.
                  this.w_QTAESI = this.w_QTAESI +_Curs_SALDILOT.SUQTAPER - _Curs_SALDILOT.SUQTRPER
                    select _Curs_SALDILOT
                    continue
                  enddo
                  use
                endif
                this.w_QTAESI = icase(this.w_FLAGLO="-" OR this.w_FLAG2LO="-",this.w_QTAESI +this.w_OLDQTA - this.w_QTAUM1,this.w_FLAGLO="+" OR this.w_FLAG2LO="+",this.w_QTAESI -this.w_OLDQTA + this.w_QTAUM1)
                if this.w_QTAESI>0 AND this.w_TESDIS AND EMPTY(this.w_LFLRISE)
                  * --- Write into LOTTIART
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.LOTTIART_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.LOTTIART_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"LOFLSTAT ="+cp_NullLink(cp_ToStrODBC("D"),'LOTTIART','LOFLSTAT');
                        +i_ccchkf ;
                    +" where ";
                        +"LOCODICE = "+cp_ToStrODBC(this.w_OLDCODLOT);
                        +" and LOCODART = "+cp_ToStrODBC(this.w_OLDKEYSAL);
                           )
                  else
                    update (i_cTable) set;
                        LOFLSTAT = "D";
                        &i_ccchkf. ;
                     where;
                        LOCODICE = this.w_OLDCODLOT;
                        and LOCODART = this.w_OLDKEYSAL;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error='errore scrittura lotti'
                    return
                  endif
                endif
              endif
            endif
          case this.w_OLDFLSTAT="D" 
            * --- Aggiorno Status Recuperando esistenza generale del Lotto
            * --- Testa anche se evade un doc. che movimenta il riservato
            if this.w_FLAGLO $ "+-" AND EMPTY(this.oParentObject.w_FLRRIF)
              this.w_QTAESI = 0
              this.w_TESDIS = .F.
              this.w_SUQTRPER = 0
              this.w_SUQTAPER = 0
              * --- Select from SALDILOT
              i_nConn=i_TableProp[this.SALDILOT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2],.t.,this.SALDILOT_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select SUM (SUQTAPER) as SUQTAPER , SUM (SUQTRPER) as SUQTRPER  from "+i_cTable+" SALDILOT ";
                    +" where "+cp_ToStrODBC(this.w_OLDKEYSAL)+"=SUCODART AND "+cp_ToStrODBC(this.w_OLDCODLOT)+"=SUCODLOT";
                    +" group by SUCODART,SUCODLOT";
                     ,"_Curs_SALDILOT")
              else
                select SUM (SUQTAPER) as SUQTAPER , SUM (SUQTRPER) as SUQTRPER from (i_cTable);
                 where this.w_OLDKEYSAL=SUCODART AND this.w_OLDCODLOT=SUCODLOT;
                 group by SUCODART,SUCODLOT;
                  into cursor _Curs_SALDILOT
              endif
              if used('_Curs_SALDILOT')
                select _Curs_SALDILOT
                locate for 1=1
                do while not(eof())
                this.w_TESDIS = .T.
                this.w_QTAESI = this.w_QTAESI +_Curs_SALDILOT.SUQTAPER - _Curs_SALDILOT.SUQTRPER
                  select _Curs_SALDILOT
                  continue
                enddo
                use
              endif
              if this.w_QTAESI<=0 AND this.w_TESDIS
                * --- Write into LOTTIART
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                i_nConn=i_TableProp[this.LOTTIART_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.LOTTIART_idx,2])
                i_ccchkf=''
                this.SetCCCHKVarsWrite(@i_ccchkf,this.LOTTIART_idx,i_nConn)
                if i_nConn<>0
                  i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"LOFLSTAT ="+cp_NullLink(cp_ToStrODBC("E"),'LOTTIART','LOFLSTAT');
                      +i_ccchkf ;
                  +" where ";
                      +"LOCODICE = "+cp_ToStrODBC(this.w_OLDCODLOT);
                      +" and LOCODART = "+cp_ToStrODBC(this.w_OLDKEYSAL);
                         )
                else
                  update (i_cTable) set;
                      LOFLSTAT = "E";
                      &i_ccchkf. ;
                   where;
                      LOCODICE = this.w_OLDCODLOT;
                      and LOCODART = this.w_OLDKEYSAL;

                  i_Rows = _tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  i_Error=MSG_WRITE_ERROR
                  return
                endif
              endif
            endif
        endcase
      endif
    endif
  endproc


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ciclo sul dettaglio
    this.w_ROWORD = this.oParentObject.w_CPROWNUM
    this.w_QTAMOV = NVL(this.oParentObject.w_MVQTAMOV, 0)
    this.w_QTAUM1 = NVL(this.oParentObject.w_MVQTAUM1, 0)
    this.w_QTAIMP = NVL(this.oParentObject.w_MVQTAIMP, 0)
    this.w_QTAIM1 = NVL(this.oParentObject.w_MVQTAIM1, 0)
    this.w_PREZZO = NVL(this.oParentObject.w_MVPREZZO, 0) 
    * --- w_OPREZZO � local e modificato per ora non lo trasformo in caller..
    if lower(this.w_PADRE.class)=="tgsar_bgd"
      this.w_OPREZZO = 0
      this.w_OQTAIM1 = 0
    else
      this.w_OQTAIM1 = NVL( this.w_PADRE.Get("MVQTAIM1") , 0)
      this.w_OPREZZO = NVL( this.w_PADRE.Get( "t_OPREZZO" ) , 0)
    endif
    this.w_VALRIG = NVL(this.oParentObject.w_MVVALRIG, 0)
    this.w_VALMAG = NVL(this.oParentObject.w_MVVALMAG, 0)
    this.w_IMPNAZ = NVL(this.oParentObject.w_MVIMPNAZ, 0)
    this.w_SCONT1 = NVL(this.oParentObject.w_MVSCONT1, 0)
    this.w_SCONT2 = NVL(this.oParentObject.w_MVSCONT2, 0)
    this.w_SCONT3 = NVL(this.oParentObject.w_MVSCONT3, 0)
    this.w_SCONT4 = NVL(this.oParentObject.w_MVSCONT4, 0)
    this.w_PERIVA = NVL(this.w_PERIVA, 0)
    this.w_KEYSAL = NVL(this.oParentObject.w_MVKEYSAL, Space(20))
    this.w_CAUMAG = NVL(this.oParentObject.w_MVCAUMAG, " ")
    this.w_CAUCOL = NVL(this.oParentObject.w_MVCAUCOL, " ")
    this.w_CODMAG = NVL(this.oParentObject.w_MVCODMAG, " ")
    this.w_CODMAT = NVL(this.oParentObject.w_MVCODMAT, " ")
    this.w_FLELGM = NVL(this.oParentObject.w_MVFLELGM,"")
    if lower(this.w_PADRE.class)<>"tgsar_bgd"
      * --- Verifica se variati dati sensibili dich. di produzione
      this.oParentObject.w_IMPODL = IIF(this.oParentObject.w_CLARIF="OR" AND NOT EMPTY(this.oParentObject.w_ODLRIF) AND NOT EMPTY(this.oParentObject.w_MVSERRIF) AND this.oParentObject.w_MVROWRIF>0, .T., this.oParentObject.w_IMPODL)
    endif
    * --- Notifica se il Documento ha generato dei Documenti di Evasione Componenti
    this.w_FLVESC = IIF(NOT EMPTY(this.oParentObject.w_MVRIFESC), .T., this.w_FLVESC)
    this.w_NR = ALLTRIM(STR(this.oParentObject.w_CPROWORD))
    this.w_SERRIF = NVL(this.oParentObject.w_MVSERRIF, Space(10))
    this.w_ROWRIF = NVL(this.oParentObject.w_MVROWRIF, 0)
    this.w_NUMRIF = NVL(this.oParentObject.w_MVNUMRIF, 0)
    this.w_OCAORIF = NVL(this.oParentObject.w_CAORIF, 0)
    if lower(this.w_PADRE.class)=="tgsar_bgd"
      this.w_SRV = "A"
    else
      this.w_SRV = this.w_PADRE.RowStatus()
    endif
    this.w_COART = NVL(this.oParentObject.w_MVCODART, Space(20))
    this.w_TIPRIG = NVL(this.oParentObject.w_MVTIPRIG," ")
    this.w_FLARIF = NVL(this.oParentObject.w_MVFLARIF, " ")
    this.w_FLERIF = NVL(this.oParentObject.w_MVFLERIF, " ")
    this.w_CODIVA = NVL(this.oParentObject.w_MVCODIVA, " ")
    this.w_EVCASC = NVL(this.oParentObject.w_MVFLCASC, " ")
    * --- Utilizzati in caso di Modulo Gestione Progetti attivo (g_COMM='S')
    this.w_IMPCOM = NVL(this.oParentObject.w_MVIMPCOM, 0)
    this.w_ORD_EVA = NVL(this.oParentObject.w_ORDEVA, 0)
    this.w_OLDEVAS1 = NVL(this.oParentObject.w_OLDEVAS, "  ")
    this.w_TOTVALFIS = this.w_TOTVALFIS + IIF(this.w_FLDEL, 0, this.w_IMPNAZ)
    * --- Test se Evade il Documento di Origine
    this.w_EVARIF = IIF(this.w_FLARIF<>" " AND (this.w_SRV $ "UA" OR this.w_FLDEL), "S", "N")
    this.w_FLAGLO = NVL(this.oParentObject.w_MVFLLOTT," ")
    this.w_FLAG2LO = NVL(this.oParentObject.w_MVF2LOTT," ")
    if lower(this.w_PADRE.class)=="tgsar_bgd"
      this.w_OLDQTA = this.w_PADRE.w_MVQTAUM1
    else
      this.w_OLDQTA = NVL( this.w_PADRE. Get( "MVQTAUM1" ) ,0)
    endif
    this.w_CODLOT = NVL(this.oParentObject.w_MVCODLOT,Space(20))
    this.w_LFLRISE = NVL(this.oParentObject.w_MVFLRISE," ")
    * --- Test Packing List
    this.w_CODCOL = NVL(this.oParentObject.w_MVCODCOL,Space(5))
    this.w_CODCONF = NVL(this.w_CODCONF,Space(3))
    * --- Variazione a valore, non attiva sugli ordini (GSOR_MDV)
    *     In realt� il campo esiste su tutte e tre le gestioni
    this.w_FLAGG = NVL( this.oParentObject.w_MV_FLAGG , Space(1) )
    * --- Utilizzo queste due variabili per avere il valore di MVFLEVAS e t_MVFLEVAS entrambi appartenenti al Trs
    *     Questo poich� negli Ordini il Flag MVFLEVAS � gestito sulla riga e quindi il suo valore in modifica � numerico
    *     In acquisto e vendite � invece di tipo carattere.
    this.w_REFLEVAS = this.oParentObject.w_MVFLEVAS
    if lower(this.w_PADRE.class)=="tgsar_bgd"
      this.w_REF2EVAS = this.w_REFLEVAS
    else
      this.w_REF2EVAS = IIF( VarType( this.w_PADRE.Get("MVFLEVAS") ) = "C" , this.w_PADRE.Get("MVFLEVAS") , IIF( this.w_PADRE.Get("MVFLEVAS")=1,"S" , " " ))
    endif
    * --- Controllo riga evasa da documento senza Import (Evasione con valore 0)
    *     MVFLEVAS � il valore precedente di MVFLEVAS (presente su cTrsName poich� � nel Link su MVROWRIF)
    if ( (this.w_CFUNC="Edit" And this.w_REF2EVAS="S" And this.w_REFLEVAS<>"S") Or ( this.w_CFUNC="Edit" And this.w_FLDEL)) And ( (Nvl(this.oParentObject.w_MVQTAEVA,0)=0 And this.oParentObject.w_MVTIPRIG<>"F") Or (Nvl(this.oParentObject.w_MVIMPEVA,0)=0 And this.oParentObject.w_MVTIPRIG="F") ) AND lower(this.w_PADRE.class)<>"tgsar_bgd"
      * --- La riga � stata evasa in fase di importazione documenti mettendo quantit�/Importo 0
      *     Se tolgo il Flag evaso elimino la riga da RIGHEEVA.
      *     I Saldi e la quantit� saldata MVQTASAL vengono gi� stornati dal Link in MVCODMAG/T
      * --- Delete from RIGHEEVA
      i_nConn=i_TableProp[this.RIGHEEVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIGHEEVA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
              +"RESERRIF = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
              +" and REROWRIF = "+cp_ToStrODBC(this.w_ROWORD);
              +" and RENUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
               )
      else
        delete from (i_cTable) where;
              RESERRIF = this.oParentObject.w_MVSERIAL;
              and REROWRIF = this.w_ROWORD;
              and RENUMRIF = this.w_NUMRIF;

        i_Rows=_tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        * --- Error: delete not accepted
        i_Error=MSG_DELETE_ERROR
        return
      endif
    endif
    if this.w_OK And this.w_CFUNC="Query" And this.w_REFLEVAS="S" And ( (Nvl(this.oParentObject.w_MVQTAEVA,0)=0 And this.oParentObject.w_MVTIPRIG<>"F") Or (Nvl(this.oParentObject.w_MVIMPEVA,0)=0 And this.oParentObject.w_MVTIPRIG="F") ) AND lower(this.w_PADRE.class)<>"tgsar_bgd"
      * --- Controllo cancellazione documento con riga evasa senza import
      *     Se presente ancora il documento che ha evaso la riga,
      *     non � possibile cancellare il documento di origine.
      * --- Read from RIGHEEVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.RIGHEEVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIGHEEVA_idx,2],.t.,this.RIGHEEVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "RESERIAL"+;
          " from "+i_cTable+" RIGHEEVA where ";
              +"RESERRIF = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
              +" and REROWRIF = "+cp_ToStrODBC(this.w_ROWORD);
              +" and RENUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          RESERIAL;
          from (i_cTable) where;
              RESERRIF = this.oParentObject.w_MVSERIAL;
              and REROWRIF = this.w_ROWORD;
              and RENUMRIF = this.w_NUMRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_RESERIAL = NVL(cp_ToDate(_read_.RESERIAL),cp_NullValue(_read_.RESERIAL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if i_Rows > 0
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVTIPDOC,MVNUMDOC,MVDATDOC,MVALFDOC"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_RESERIAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVTIPDOC,MVNUMDOC,MVDATDOC,MVALFDOC;
            from (i_cTable) where;
                MVSERIAL = this.w_RESERIAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_RETIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
          this.w_RENUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
          this.w_REDATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
          this.w_REALFDOC = NVL(cp_ToDate(_read_.MVALFDOC),cp_NullValue(_read_.MVALFDOC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_MESS = ah_Msgformat("Riga movimento: %1%0Evasa senza importazione nel doc.: %2 n. %3 del %4%0Per cancellare il documento eliminare %2 o togliere check evasione o eliminare riga con F6",this.w_NR, this.w_RETIPDOC, Alltrim(Str(this.w_RENUMDOC,15))+IIF(Empty(this.w_REALFDOC),"","/"+Alltrim(this.w_REALFDOC)), DTOC(this.w_REDATDOC) )
        this.w_OK = .F.
      endif
    endif
    if this.w_OK And NOT EMPTY(this.w_SERRIF)
      * --- Verifica Se riga associata ad un Import Documenti 
      if Not this.w_LASTDET And this.oParentObject.w_FLGEFA="B" AND this.w_CFUNC="Query" AND this.w_SERRIF<>this.w_OLDSER AND this.w_TIPFAT $ "CE" And NOT EMPTY(this.oParentObject.w_MVRIFFAD) AND lower(this.w_PADRE.class)<>"tgsar_bgd"
        * --- Toglie riferimento evasione alle Righe descrittive solo se tipo fatturazione per ordine o ordine+Destinazione
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVDATGEN ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVDATGEN');
          +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
              +" and MVTIPRIG = "+cp_ToStrODBC("D");
                 )
        else
          update (i_cTable) set;
              MVDATGEN = cp_CharToDate("  -  -  ");
              ,MVFLEVAS = " ";
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_SERRIF;
              and MVTIPRIG = "D";

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_OLDSER = this.w_SERRIF
      endif
      * --- Se evado una fattura raggruppata, il documento che evade ha 
      *     MVNUMRIF=-90 (per funzionare Link MVSERRIF su documenti ).
      *     In questo caso la Select su fatture raggruppate non mi restituisce nessun 
      *     record, devo cmq richiamare pag2 con w_SERRIF, w_ROWRIF letti dalla riga.
      *     
      *     Verifico se il documento � all'interno di un piano di fatturazione
      * --- Utilizzato a pag2, per gestire fattura raggruppata o evasione normale
      *     (sostituisce il vecchio controllo w_NUMRIF=-90) non corretto in quanto
      *     se evado una fattura raggruppata, il documento che la evade ha
      *     anch'esso w_NUMRIF=-90
      this.w_FAT_RAG = Not Empty(this.oParentObject.w_MVRIFFAD) AND this.w_NUMRIF=-90
      do case
        case this.w_FAT_RAG
          * --- Cicla sui dettagli riferimenti associati alla Riga fattura Differita (raggruppata)
          * --- Select from RAG_FATT
          i_nConn=i_TableProp[this.RAG_FATT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RAG_FATT_idx,2],.t.,this.RAG_FATT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" RAG_FATT ";
                +" where FRSERFAT="+cp_ToStrODBC(this.oParentObject.w_MVSERIAL)+" AND FRROWFAT="+cp_ToStrODBC(this.w_ROWORD)+"";
                 ,"_Curs_RAG_FATT")
          else
            select * from (i_cTable);
             where FRSERFAT=this.oParentObject.w_MVSERIAL AND FRROWFAT=this.w_ROWORD;
              into cursor _Curs_RAG_FATT
          endif
          if used('_Curs_RAG_FATT')
            select _Curs_RAG_FATT
            locate for 1=1
            do while not(eof())
            this.w_SERRIF = NVL(_Curs_RAG_FATT.FRSERDDT, Space(10))
            this.w_ROWRIF = NVL(_Curs_RAG_FATT.FRROWDDT, 0)
            this.w_NUMRIF = NVL(_Curs_RAG_FATT.FRNUMDDT, 0)
            this.w_RQTAIMP = NVL(_Curs_RAG_FATT.FRQTAIMP, 0)
            this.w_RQTAIM1 = NVL(_Curs_RAG_FATT.FRQTAIM1, 0)
            if (NOT EMPTY(this.w_SERRIF)) AND this.w_ROWRIF<>0
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
              select _Curs_RAG_FATT
              continue
            enddo
            use
          endif
          * --- Se Cancellazione, Elimina Riferimenti FD Raggruppate
          if this.w_FLDEL
            * --- Delete from RAG_FATT
            i_nConn=i_TableProp[this.RAG_FATT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.RAG_FATT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                    +"FRSERFAT = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                    +" and FRROWFAT = "+cp_ToStrODBC(this.w_ROWORD);
                     )
            else
              delete from (i_cTable) where;
                    FRSERFAT = this.oParentObject.w_MVSERIAL;
                    and FRROWFAT = this.w_ROWORD;

              i_Rows=_tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              * --- Error: delete not accepted
              i_Error=MSG_DELETE_ERROR
              return
            endif
          endif
        case this.w_ROWRIF<>0
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
    endif
    if this.w_OK AND g_GPOS="S" AND this.w_STORNA AND NOT EMPTY(this.w_KEYSAL) AND NOT EMPTY(this.w_CODMAG) And this.w_QTAUM1<>0 AND this.w_EVCASC $ "+-" AND lower(this.w_PADRE.class)<>"tgsar_bgd"
      * --- Se Doc. generato da vendita Dettaglio, Storna la quantita del Venduto (o Reso) dai Saldi in quanto comanda il documento di Vendita Negozio
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_EVCASC,'SLQTAPER','this.w_QTAUM1',this.w_QTAUM1,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
            +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
               )
      else
        update (i_cTable) set;
            SLQTAPER = &i_cOp1.;
            &i_ccchkf. ;
         where;
            SLCODICE = this.w_KEYSAL;
            and SLCODMAG = this.w_CODMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_LOTMAG = NVL( this.w_PADRE.Get("MVLOTMAG") , " ")
      if Not Empty(this.w_LOTMAG)
        * --- Write into SALDILOT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDILOT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDILOT_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_EVCASC,'SUQTAPER','this.w_QTAUM1',this.w_QTAUM1,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDILOT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SUQTAPER ="+cp_NullLink(i_cOp1,'SALDILOT','SUQTAPER');
              +i_ccchkf ;
          +" where ";
              +"SUCODART = "+cp_ToStrODBC(this.w_KEYSAL);
              +" and SUCODMAG = "+cp_ToStrODBC(this.w_LOTMAG);
                 )
        else
          update (i_cTable) set;
              SUQTAPER = &i_cOp1.;
              &i_ccchkf. ;
           where;
              SUCODART = this.w_KEYSAL;
              and SUCODMAG = this.w_LOTMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      * --- Read from ART_ICOL
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ARSALCOM"+;
          " from "+i_cTable+" ART_ICOL where ";
              +"ARCODART = "+cp_ToStrODBC(this.w_COART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ARSALCOM;
          from (i_cTable) where;
              ARCODART = this.w_COART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_SALCOM="S"
        if empty(nvl(this.oParentObject.w_MVCODCOM,""))
          this.w_COMMAPPO = this.w_COMMDEFA
        else
          this.w_COMMAPPO = this.oParentObject.w_MVCODCOM
        endif
        * --- Write into SALDICOM
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICOM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_EVCASC,'SCQTAPER','this.w_QTAUM1',this.w_QTAUM1,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
              +i_ccchkf ;
          +" where ";
              +"SCCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
              +" and SCCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
              +" and SCCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                 )
        else
          update (i_cTable) set;
              SCQTAPER = &i_cOp1.;
              &i_ccchkf. ;
           where;
              SCCODICE = this.w_KEYSAL;
              and SCCODMAG = this.w_CODMAG;
              and SCCODCAN = this.w_COMMAPPO;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if g_MADV = "S" AND Not Empty(this.w_LOTMAG)
          * --- Write into SALOTCOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALOTCOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALOTCOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_EVCASC,'SMQTAPER','this.w_QTAUM1',this.w_QTAUM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALOTCOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SMQTAPER ="+cp_NullLink(i_cOp1,'SALOTCOM','SMQTAPER');
                +i_ccchkf ;
            +" where ";
                +"SMCODART = "+cp_ToStrODBC(this.w_KEYSAL);
                +" and SMCODMAG = "+cp_ToStrODBC(this.w_LOTMAG);
                +" and SMCODCAN = "+cp_ToStrODBC(this.w_COMMAPPO);
                   )
          else
            update (i_cTable) set;
                SMQTAPER = &i_cOp1.;
                &i_ccchkf. ;
             where;
                SMCODART = this.w_KEYSAL;
                and SMCODMAG = this.w_LOTMAG;
                and SMCODCAN = this.w_COMMAPPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
    endif
  endproc


  procedure Page_11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifico la presenza delle matricole!
    if this.w_CFUNC<>"Query" And g_MATR="S" And this.oParentObject.w_MVDATREG>=nvl(g_DATMAT,cp_CharToDate("  /  /    "))
      if this.w_OK 
        * --- Verifico congruenza numero matricole con qt� di riga (SE NON IN FASE DI CORR. ERORRI LOTTI / UBICAZIONI)
        if NOT this.oParentObject.w_TESLOT
          * --- Select from GSVE_BMT
          do vq_exec with 'GSVE_BMT',this,'_Curs_GSVE_BMT','',.f.,.t.
          if used('_Curs_GSVE_BMT')
            select _Curs_GSVE_BMT
            locate for 1=1
            do while not(eof())
            this.w_OK = .F.
            if NOT EMPTY(NVL(_Curs_GSVE_BMT.DOMAGSCA," "))
              * --- Se si tratta di un trasferimento determino il magazzino di carico\scarico corretto del documento
              this.w_DMAGCAR = Nvl(_Curs_GSVE_BMT.DOMAGCAR,Space(5) )
              this.w_DMAGSCA = Nvl(_Curs_GSVE_BMT.DOMAGSCA,Space(5) )
            else
              this.w_DMAGCAR = NVL(_Curs_GSVE_BMT.DOMAGCAR,Space(5))
              this.w_DMAGSCA = NVL(_Curs_GSVE_BMT.DOMAGCAR,Space(5))
            endif
            do case
              case Nvl(_Curs_GSVE_BMT.qtaum1,0)<>Nvl(_Curs_GSVE_BMT.qtamat,0)
                if cp_Round(Nvl(qtaum1,0),0)<>Nvl(qtaum1,0)
                  this.w_MESS = ah_Msgformat("Riga %1 impossibile movimentare articoli gestiti a matricole per quantit� frazionabili", Alltrim( Str( _Curs_GSVE_BMT.ROWORD ) ) )
                else
                  this.w_MESS = ah_Msgformat("Riga %1 con numero matricole incongruente. Qt� riga: %2, matricole movimentate: %3", Alltrim( Str( _Curs_GSVE_BMT.ROWORD ) ), Alltrim( Str( _Curs_GSVE_BMT.QTAUM1 ) ), Alltrim( Str( _Curs_GSVE_BMT.QTAMAT ) ) )
                endif
              case ALLTRIM(Nvl(_Curs_GSVE_BMT.Mvcodice,Space(20)))<>ALLTRIM(Nvl(_Curs_GSVE_BMT.Mtkeysal,Space(20)))
                this.w_MESS = ah_Msgformat("Riga %1 con articolo incongruente. Articolo documento: %2, articolo matricole: %3", Alltrim( Str( _Curs_GSVE_BMT.ROWORD ) ), Alltrim( _Curs_GSVE_BMT.MVCODICE ), Alltrim( _Curs_GSVE_BMT.MTKEYSAL ) )
              case Nvl(_Curs_GSVE_BMT.Mvcodlot,Space(20)) <> Nvl(_Curs_GSVE_BMT.Mtcodlot,Space(20)) AND g_PERLOT="S"
                this.w_MESS = ah_Msgformat("Riga %1 con lotto incongruente. Lotto documento: %2, lotto matricole: %3", Alltrim( Str( _Curs_GSVE_BMT.ROWORD ) ), Alltrim( NVL(_Curs_GSVE_BMT.MVCODLOT,Space(20)) ), Alltrim( NVL(_Curs_GSVE_BMT.MTCODLOT,Space(20)) ) )
              case ALLTRIM(Nvl(_Curs_GSVE_BMT.Mtmagpri,Space(5)))<>Alltrim(this.w_DMAGCAR)
                this.w_MESS = ah_Msgformat("Riga %1 con magazzino di carico incongruente. Magazzino documento: %2, magazzino matricole: %3", Alltrim( Str( _Curs_GSVE_BMT.ROWORD ) ), Alltrim(this.w_DMAGCAR), Alltrim( NVL(_Curs_GSVE_BMT.MTMAGPRI,Space(5))) )
              case Nvl(_Curs_GSVE_BMT.Mvcodubi,Space(20))<>Nvl(_Curs_GSVE_BMT.Mtcodubi,Space(20)) AND g_PERUBI="S"
                this.w_MESS = ah_Msgformat("Riga %1 con ubicazione incongruente. Ubicazione documento: %2, ubicazione matricole: %3", Alltrim(Str(_Curs_GSVE_BMT.ROWORD ) ), Alltrim( NVL(_Curs_GSVE_BMT.MVCODUBI,Space(20)) ), Alltrim(NVL(_Curs_GSVE_BMT.MTCODUBI,Space(20)) ))
              case ALLTRIM(Nvl(Mtmagsca,Space(5)))<>Alltrim(this.w_DMAGSCA) and Not Empty(this.w_DMAGSCA)
                this.w_MESS = ah_Msgformat("Riga %1 con magazzino di scarico incongruente. Magazzino documento: %2, magazzino matricole: %3", Alltrim( Str( _Curs_GSVE_BMT.ROWORD ) ) , Alltrim(this.w_DMAGSCA), Alltrim( NVL(_Curs_GSVE_BMT.MTMAGSCA,Space(5))) )
            endcase
            Exit
              select _Curs_GSVE_BMT
              continue
            enddo
            use
          endif
        endif
      endif
      if this.w_OK 
        * --- Verifica se il documento contiene la stessa matricola due volte (cosa vietata)
        * --- Se ci� avviene la variabile caller w_OK viene posta a false
        * --- Select from GSVE_BML
        do vq_exec with 'GSVE_BML',this,'_Curs_GSVE_BML','',.f.,.t.
        if used('_Curs_GSVE_BML')
          select _Curs_GSVE_BML
          locate for 1=1
          do while not(eof())
          this.w_OK = ( _Curs_GSVE_BML.CODMAT<=1 )
          Exit
            select _Curs_GSVE_BML
            continue
          enddo
          use
        endif
      endif
    endif
  endproc


  procedure Page_12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_OK AND g_EACD="S" AND this.w_CFUNC<>"Load" And Not Empty(this.oParentObject.w_MVRIFESC) And IIF( this.w_CFUNC ="Edit",Not this.oParentObject.w_TESCOMP,.T.)
      this.w_DOCEVA = this.oParentObject.w_MVRIFESC
      this.w_DOCROW = this.oParentObject.w_CPROWNUM
      if this.w_OK
        this.w_DOCRIG = .T.
        this.Page_8()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        Exit
      endif
    endif
    if this.w_PADRE.RowStatus() $ "A-U-D" And g_EACD="S" And this.w_CFUNC<>"Load"
      * --- Se in modifica aggiungo una riga in append sul dettaglio devo sempre e comunque
      *     eliminare il documento di esplosione legato alla testata (prodotto finito) che altrimenti
      *     verrebbe eliminato solo se modifico cliente, data, listino.
      this.oParentObject.w_OLDTES = "XXX"
    endif
  endproc


  procedure Page_13
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento tabella esterna contenente ulteriori campi del dettaglio
    do case
      case this.w_CFUNC = "Query"
        * --- CANCELLAZIONE DOCUMENTO
        * --- Try
        local bErr_05340FA0
        bErr_05340FA0=bTrsErr
        this.Try_05340FA0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          this.w_MESS = ah_Msgformat("Impossibile eliminare righe documento nella tabella ALT_DETT")
          this.w_OK = .F.
        endif
        bTrsErr=bTrsErr or bErr_05340FA0
        * --- End
        this.w_ROWFAT = 0
        * --- Aggiorno evasione nelle proforme
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="MVSERIAL,CPROWNUM"
          do vq_exec with 'GSVEDBMK',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                  +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVQTAEVA = _t2.MVQTAEVA";
              +",MVQTAEV1 = _t2.MVQTAEV1";
              +",MVFLEVAS = _t2.MVFLEVAS";
              +i_ccchkf;
              +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                  +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
              +"DOC_DETT.MVQTAEVA = _t2.MVQTAEVA";
              +",DOC_DETT.MVQTAEV1 = _t2.MVQTAEV1";
              +",DOC_DETT.MVFLEVAS = _t2.MVFLEVAS";
              +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                  +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
              +"MVQTAEVA,";
              +"MVQTAEV1,";
              +"MVFLEVAS";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.MVQTAEVA,";
              +"t2.MVQTAEV1,";
              +"t2.MVFLEVAS";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                  +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
              +"MVQTAEVA = _t2.MVQTAEVA";
              +",MVQTAEV1 = _t2.MVQTAEV1";
              +",MVFLEVAS = _t2.MVFLEVAS";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVQTAEVA = (select MVQTAEVA from "+i_cQueryTable+" where "+i_cWhere+")";
              +",MVQTAEV1 = (select MVQTAEV1 from "+i_cQueryTable+" where "+i_cWhere+")";
              +",MVFLEVAS = (select MVFLEVAS from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Write into PRE_STAZ
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
        if i_nConn<>0
          local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
          declare i_aIndex[1]
          i_cQueryTable=cp_getTempTableName(i_nConn)
          i_aIndex(1)="PRSERIAL"
          do vq_exec with 'GSVEDPRE',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)	
          i_cDB=cp_GetDatabaseType(i_nConn)
          do case
          case i_cDB="SQLServer"
            i_cWhere="PRE_STAZ.PRSERIAL = _t2.PRSERIAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PRRIFFAT = _t2.PRRIFFAT";
              +",PRROWFAT = _t2.PRROWFAT";
              +",PRRIFPRO = _t2.PRRIFPRO";
              +",PRROWPRO = _t2.PRROWPRO";
              +",PRRIFNOT = _t2.PRRIFNOT";
              +",PRROWNOT = _t2.PRROWNOT";
              +",PRRIFBOZ = _t2.PRRIFBOZ";
              +",PRROWBOZ = _t2.PRROWBOZ";
              +",PRROWFAA = _t2.PRROWFAA";
              +",PRROWPRA = _t2.PRROWPRA";
              +",PRRIFFAA = _t2.PRRIFFAA";
              +",PRRIFPRA = _t2.PRRIFPRA";
              +i_ccchkf;
              +" from "+i_cTable+" PRE_STAZ, "+i_cQueryTable+" _t2 where "+i_cWhere)
          case i_cDB="MySQL"
            i_cWhere="PRE_STAZ.PRSERIAL = _t2.PRSERIAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PRE_STAZ, "+i_cQueryTable+" _t2 set ";
              +"PRE_STAZ.PRRIFFAT = _t2.PRRIFFAT";
              +",PRE_STAZ.PRROWFAT = _t2.PRROWFAT";
              +",PRE_STAZ.PRRIFPRO = _t2.PRRIFPRO";
              +",PRE_STAZ.PRROWPRO = _t2.PRROWPRO";
              +",PRE_STAZ.PRRIFNOT = _t2.PRRIFNOT";
              +",PRE_STAZ.PRROWNOT = _t2.PRROWNOT";
              +",PRE_STAZ.PRRIFBOZ = _t2.PRRIFBOZ";
              +",PRE_STAZ.PRROWBOZ = _t2.PRROWBOZ";
              +",PRE_STAZ.PRROWFAA = _t2.PRROWFAA";
              +",PRE_STAZ.PRROWPRA = _t2.PRROWPRA";
              +",PRE_STAZ.PRRIFFAA = _t2.PRRIFFAA";
              +",PRE_STAZ.PRRIFPRA = _t2.PRRIFPRA";
              +Iif(Empty(i_ccchkf),"",",PRE_STAZ.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
              +" where "+i_cWhere)
          case i_cDB="Oracle"
            i_cWhere="PRE_STAZ.PRSERIAL = t2.PRSERIAL";
            
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PRE_STAZ set (";
              +"PRRIFFAT,";
              +"PRROWFAT,";
              +"PRRIFPRO,";
              +"PRROWPRO,";
              +"PRRIFNOT,";
              +"PRROWNOT,";
              +"PRRIFBOZ,";
              +"PRROWBOZ,";
              +"PRROWFAA,";
              +"PRROWPRA,";
              +"PRRIFFAA,";
              +"PRRIFPRA";
              +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
              +"t2.PRRIFFAT,";
              +"t2.PRROWFAT,";
              +"t2.PRRIFPRO,";
              +"t2.PRROWPRO,";
              +"t2.PRRIFNOT,";
              +"t2.PRROWNOT,";
              +"t2.PRRIFBOZ,";
              +"t2.PRROWBOZ,";
              +"t2.PRROWFAA,";
              +"t2.PRROWPRA,";
              +"t2.PRRIFFAA,";
              +"t2.PRRIFPRA";
              +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
              +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
          case i_cDB="PostgreSQL"
            i_cWhere="PRE_STAZ.PRSERIAL = _t2.PRSERIAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" PRE_STAZ set ";
              +"PRRIFFAT = _t2.PRRIFFAT";
              +",PRROWFAT = _t2.PRROWFAT";
              +",PRRIFPRO = _t2.PRRIFPRO";
              +",PRROWPRO = _t2.PRROWPRO";
              +",PRRIFNOT = _t2.PRRIFNOT";
              +",PRROWNOT = _t2.PRROWNOT";
              +",PRRIFBOZ = _t2.PRRIFBOZ";
              +",PRROWBOZ = _t2.PRROWBOZ";
              +",PRROWFAA = _t2.PRROWFAA";
              +",PRROWPRA = _t2.PRROWPRA";
              +",PRRIFFAA = _t2.PRRIFFAA";
              +",PRRIFPRA = _t2.PRRIFPRA";
              +i_ccchkf;
              +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
          otherwise
            i_cWhere=i_cTable+".PRSERIAL = "+i_cQueryTable+".PRSERIAL";
        
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PRRIFFAT = (select PRRIFFAT from "+i_cQueryTable+" where "+i_cWhere+")";
              +",PRROWFAT = (select PRROWFAT from "+i_cQueryTable+" where "+i_cWhere+")";
              +",PRRIFPRO = (select PRRIFPRO from "+i_cQueryTable+" where "+i_cWhere+")";
              +",PRROWPRO = (select PRROWPRO from "+i_cQueryTable+" where "+i_cWhere+")";
              +",PRRIFNOT = (select PRRIFNOT from "+i_cQueryTable+" where "+i_cWhere+")";
              +",PRROWNOT = (select PRROWNOT from "+i_cQueryTable+" where "+i_cWhere+")";
              +",PRRIFBOZ = (select PRRIFBOZ from "+i_cQueryTable+" where "+i_cWhere+")";
              +",PRROWBOZ = (select PRROWBOZ from "+i_cQueryTable+" where "+i_cWhere+")";
              +",PRROWFAA = (select PRROWFAA from "+i_cQueryTable+" where "+i_cWhere+")";
              +",PRROWPRA = (select PRROWPRA from "+i_cQueryTable+" where "+i_cWhere+")";
              +",PRRIFFAA = (select PRRIFFAA from "+i_cQueryTable+" where "+i_cWhere+")";
              +",PRRIFPRA = (select PRRIFPRA from "+i_cQueryTable+" where "+i_cWhere+")";
              +i_ccchkf;
              +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
          endcase
          cp_DropTempTable(i_nConn,i_cQueryTable)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      otherwise
        * --- MODIFICA/NUOVO DOCUMENTO
        this.w_PADRE.MarkPos()     
        this.w_PADRE.FirstRow()     
        * --- Cicla sulle righe NON cancellate
        do while Not this.w_PADRE.Eof_Trs()
          if this.w_PADRE.FullRow()
            this.w_PADRE.SetRow()     
            do case
              case this.w_PADRE.RowStatus() = "A"
                * --- Riga aggiunta
                * --- Try
                local bErr_05356D68
                bErr_05356D68=bTrsErr
                this.Try_05356D68()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  this.w_MESS = ah_Msgformat("Impossibile inserire riga nella tabella ALT_DETT")
                  this.w_OK = .F.
                  exit
                endif
                bTrsErr=bTrsErr or bErr_05356D68
                * --- End
                this.w_PRSERIAL = this.oParentObject.w_MVSERRIF
                do case
                  case this.oParentObject.w_PRTIPCAU="F"
                    * --- Write into PRE_STAZ
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"PRRIFFAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'PRE_STAZ','PRRIFFAT');
                      +",PRROWFAT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'PRE_STAZ','PRROWFAT');
                          +i_ccchkf ;
                      +" where ";
                          +"PRSERIAL = "+cp_ToStrODBC(this.w_PRSERIAL);
                             )
                    else
                      update (i_cTable) set;
                          PRRIFFAT = this.oParentObject.w_MVSERIAL;
                          ,PRROWFAT = this.oParentObject.w_CPROWNUM;
                          &i_ccchkf. ;
                       where;
                          PRSERIAL = this.w_PRSERIAL;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  case this.oParentObject.w_PRTIPCAU="S"
                    * --- Write into PRE_STAZ
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"PRRIFPRO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'PRE_STAZ','PRRIFPRO');
                      +",PRROWPRO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'PRE_STAZ','PRROWPRO');
                          +i_ccchkf ;
                      +" where ";
                          +"PRSERIAL = "+cp_ToStrODBC(this.w_PRSERIAL);
                             )
                    else
                      update (i_cTable) set;
                          PRRIFPRO = this.oParentObject.w_MVSERIAL;
                          ,PRROWPRO = this.oParentObject.w_CPROWNUM;
                          &i_ccchkf. ;
                       where;
                          PRSERIAL = this.w_PRSERIAL;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  case this.oParentObject.w_PRTIPCAU="N"
                    * --- Write into PRE_STAZ
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"PRRIFNOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'PRE_STAZ','PRRIFNOT');
                      +",PRROWNOT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'PRE_STAZ','PRROWNOT');
                          +i_ccchkf ;
                      +" where ";
                          +"PRSERIAL = "+cp_ToStrODBC(this.w_PRSERIAL);
                             )
                    else
                      update (i_cTable) set;
                          PRRIFNOT = this.oParentObject.w_MVSERIAL;
                          ,PRROWNOT = this.oParentObject.w_CPROWNUM;
                          &i_ccchkf. ;
                       where;
                          PRSERIAL = this.w_PRSERIAL;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  case this.oParentObject.w_PRTIPCAU="B"
                    * --- Write into PRE_STAZ
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"PRRIFBOZ ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'PRE_STAZ','PRRIFBOZ');
                      +",PRROWBOZ ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'PRE_STAZ','PRROWBOZ');
                          +i_ccchkf ;
                      +" where ";
                          +"PRSERIAL = "+cp_ToStrODBC(this.w_PRSERIAL);
                             )
                    else
                      update (i_cTable) set;
                          PRRIFBOZ = this.oParentObject.w_MVSERIAL;
                          ,PRROWBOZ = this.oParentObject.w_CPROWNUM;
                          &i_ccchkf. ;
                       where;
                          PRSERIAL = this.w_PRSERIAL;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  case this.oParentObject.w_PRTIPCAU="A"
                    if this.oParentObject.w_MVNUMRIF=-20 AND this.oParentObject.w_MVROWRIF>0
                      * --- Write into PRE_STAZ
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"PRRIFFAA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'PRE_STAZ','PRRIFFAA');
                        +",PRROWFAA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'PRE_STAZ','PRROWFAA');
                            +i_ccchkf ;
                        +" where ";
                            +"PRRIFPRA = "+cp_ToStrODBC(this.w_PRSERIAL);
                            +" and PRROWPRA = "+cp_ToStrODBC(this.oParentObject.w_MVROWRIF);
                               )
                      else
                        update (i_cTable) set;
                            PRRIFFAA = this.oParentObject.w_MVSERIAL;
                            ,PRROWFAA = this.oParentObject.w_CPROWNUM;
                            &i_ccchkf. ;
                         where;
                            PRRIFPRA = this.w_PRSERIAL;
                            and PRROWPRA = this.oParentObject.w_MVROWRIF;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error=MSG_WRITE_ERROR
                        return
                      endif
                    else
                      * --- Write into PRE_STAZ
                      i_commit = .f.
                      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                        cp_BeginTrs()
                        i_commit = .t.
                      endif
                      i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
                      i_ccchkf=''
                      this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
                      if i_nConn<>0
                        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"PRRIFFAA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'PRE_STAZ','PRRIFFAA');
                        +",PRROWFAA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'PRE_STAZ','PRROWFAA');
                            +i_ccchkf ;
                        +" where ";
                            +"PRSERIAL = "+cp_ToStrODBC(this.w_PRSERIAL);
                               )
                      else
                        update (i_cTable) set;
                            PRRIFFAA = this.oParentObject.w_MVSERIAL;
                            ,PRROWFAA = this.oParentObject.w_CPROWNUM;
                            &i_ccchkf. ;
                         where;
                            PRSERIAL = this.w_PRSERIAL;

                        i_Rows = _tally
                      endif
                      if i_commit
                        cp_EndTrs(.t.)
                      endif
                      if bTrsErr
                        i_Error=MSG_WRITE_ERROR
                        return
                      endif
                    endif
                  case this.oParentObject.w_PRTIPCAU="P"
                    * --- Write into PRE_STAZ
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"PRRIFPRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'PRE_STAZ','PRRIFPRA');
                      +",PRROWPRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'PRE_STAZ','PRROWPRA');
                          +i_ccchkf ;
                      +" where ";
                          +"PRSERIAL = "+cp_ToStrODBC(this.w_PRSERIAL);
                             )
                    else
                      update (i_cTable) set;
                          PRRIFPRA = this.oParentObject.w_MVSERIAL;
                          ,PRROWPRA = this.oParentObject.w_CPROWNUM;
                          &i_ccchkf. ;
                       where;
                          PRSERIAL = this.w_PRSERIAL;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                endcase
              case this.w_PADRE.RowStatus() = "U"
                * --- Riga modificata
                * --- Try
                local bErr_0536DAC8
                bErr_0536DAC8=bTrsErr
                this.Try_0536DAC8()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  this.w_MESS = ah_Msgformat("Impossibile eseguire aggiornamento di riga nella tabella ALT_DETT")
                  this.w_OK = .F.
                  exit
                endif
                bTrsErr=bTrsErr or bErr_0536DAC8
                * --- End
            endcase
          endif
          * --- Aggiorno evasione nelle proforme
          if this.w_PADRE.RowStatus() $ "A-U"
            this.w_ROWFAT = this.oParentObject.w_CPROWNUM
            * --- Write into DOC_DETT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
            if i_nConn<>0
              local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
              declare i_aIndex[1]
              i_cQueryTable=cp_getTempTableName(i_nConn)
              i_aIndex(1)="MVSERIAL,CPROWNUM"
              do vq_exec with 'GSVEPBMK',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
              i_cDB=cp_GetDatabaseType(i_nConn)
              do case
              case i_cDB="SQLServer"
                i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVQTAEVA = _t2.MVQTAEVA";
                  +",MVQTAEV1 = _t2.MVQTAEV1";
                  +",MVFLEVAS = _t2.MVFLEVAS";
                  +i_ccchkf;
                  +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
              case i_cDB="MySQL"
                i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
                  +"DOC_DETT.MVQTAEVA = _t2.MVQTAEVA";
                  +",DOC_DETT.MVQTAEV1 = _t2.MVQTAEV1";
                  +",DOC_DETT.MVFLEVAS = _t2.MVFLEVAS";
                  +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                  +" where "+i_cWhere)
              case i_cDB="Oracle"
                i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
                
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
                  +"MVQTAEVA,";
                  +"MVQTAEV1,";
                  +"MVFLEVAS";
                  +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                  +"t2.MVQTAEVA,";
                  +"t2.MVQTAEV1,";
                  +"t2.MVFLEVAS";
                  +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                  +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
              case i_cDB="PostgreSQL"
                i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                      +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
                  +"MVQTAEVA = _t2.MVQTAEVA";
                  +",MVQTAEV1 = _t2.MVQTAEV1";
                  +",MVFLEVAS = _t2.MVFLEVAS";
                  +i_ccchkf;
                  +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
              otherwise
                i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                      +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
            
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                  +"MVQTAEVA = (select MVQTAEVA from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVQTAEV1 = (select MVQTAEV1 from "+i_cQueryTable+" where "+i_cWhere+")";
                  +",MVFLEVAS = (select MVFLEVAS from "+i_cQueryTable+" where "+i_cWhere+")";
                  +i_ccchkf;
                  +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
              endcase
              cp_DropTempTable(i_nConn,i_cQueryTable)
            else
              error "not yet implemented!"
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          this.w_PADRE.NextRow()     
        enddo
        this.w_PADRE.FirstRowDel()     
        * --- Cicla sulle righe Cancellate
        do while Not this.w_PADRE.Eof_Trs()
          if this.w_PADRE.FullRow()
            this.w_PADRE.SetRow()     
            this.StatoRiga = this.w_PADRE.Get("I_SRV")
            * --- In presenza di riga modificata o invariata
            if this.StatoRiga = "U" OR EMPTY(this.StatoRiga)
              * --- Try
              local bErr_0533CE00
              bErr_0533CE00=bTrsErr
              this.Try_0533CE00()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                this.w_MESS = ah_Msgformat("Impossibile eseguire cancellazione di riga nella tabella ALT_DETT")
                this.w_OK = .F.
                exit
              endif
              bTrsErr=bTrsErr or bErr_0533CE00
              * --- End
              * --- Se esiste, aggiorniamo la prestazione
              do case
                case this.oParentObject.w_PRTIPCAU="F"
                  * --- Aggiorno evasione nelle proforme
                  this.w_ROWFAT = this.oParentObject.w_CPROWNUM
                  * --- Write into DOC_DETT
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.DOC_DETT_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
                  if i_nConn<>0
                    local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
                    declare i_aIndex[1]
                    i_cQueryTable=cp_getTempTableName(i_nConn)
                    i_aIndex(1)="MVSERIAL,CPROWNUM"
                    do vq_exec with 'GSVEDBMK',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)	
                    i_cDB=cp_GetDatabaseType(i_nConn)
                    do case
                    case i_cDB="SQLServer"
                      i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                            +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                  
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"MVQTAEVA = _t2.MVQTAEVA";
                        +",MVQTAEV1 = _t2.MVQTAEV1";
                        +",MVFLEVAS = _t2.MVFLEVAS";
                        +i_ccchkf;
                        +" from "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 where "+i_cWhere)
                    case i_cDB="MySQL"
                      i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                            +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                  
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT, "+i_cQueryTable+" _t2 set ";
                        +"DOC_DETT.MVQTAEVA = _t2.MVQTAEVA";
                        +",DOC_DETT.MVQTAEV1 = _t2.MVQTAEV1";
                        +",DOC_DETT.MVFLEVAS = _t2.MVFLEVAS";
                        +Iif(Empty(i_ccchkf),"",",DOC_DETT.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
                        +" where "+i_cWhere)
                    case i_cDB="Oracle"
                      i_cWhere="DOC_DETT.MVSERIAL = t2.MVSERIAL";
                            +" and "+"DOC_DETT.CPROWNUM = t2.CPROWNUM";
                      
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set (";
                        +"MVQTAEVA,";
                        +"MVQTAEV1,";
                        +"MVFLEVAS";
                        +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
                        +"t2.MVQTAEVA,";
                        +"t2.MVQTAEV1,";
                        +"t2.MVFLEVAS";
                        +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
                        +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
                    case i_cDB="PostgreSQL"
                      i_cWhere="DOC_DETT.MVSERIAL = _t2.MVSERIAL";
                            +" and "+"DOC_DETT.CPROWNUM = _t2.CPROWNUM";
                  
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" DOC_DETT set ";
                        +"MVQTAEVA = _t2.MVQTAEVA";
                        +",MVQTAEV1 = _t2.MVQTAEV1";
                        +",MVFLEVAS = _t2.MVFLEVAS";
                        +i_ccchkf;
                        +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
                    otherwise
                      i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
                            +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
                  
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                        +"MVQTAEVA = (select MVQTAEVA from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVQTAEV1 = (select MVQTAEV1 from "+i_cQueryTable+" where "+i_cWhere+")";
                        +",MVFLEVAS = (select MVFLEVAS from "+i_cQueryTable+" where "+i_cWhere+")";
                        +i_ccchkf;
                        +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
                    endcase
                    cp_DropTempTable(i_nConn,i_cQueryTable)
                  else
                    error "not yet implemented!"
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                  * --- Write into PRE_STAZ
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"PRRIFFAT ="+cp_NullLink(cp_ToStrODBC(""),'PRE_STAZ','PRRIFFAT');
                    +",PRROWFAT ="+cp_NullLink(cp_ToStrODBC(0),'PRE_STAZ','PRROWFAT');
                        +i_ccchkf ;
                    +" where ";
                        +"PRRIFFAT = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                        +" and PRROWFAT = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                           )
                  else
                    update (i_cTable) set;
                        PRRIFFAT = "";
                        ,PRROWFAT = 0;
                        &i_ccchkf. ;
                     where;
                        PRRIFFAT = this.oParentObject.w_MVSERIAL;
                        and PRROWFAT = this.oParentObject.w_CPROWNUM;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                case this.oParentObject.w_PRTIPCAU="S"
                  * --- Write into PRE_STAZ
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"PRRIFPRO ="+cp_NullLink(cp_ToStrODBC(""),'PRE_STAZ','PRRIFPRO');
                    +",PRROWPRO ="+cp_NullLink(cp_ToStrODBC(0),'PRE_STAZ','PRROWPRO');
                        +i_ccchkf ;
                    +" where ";
                        +"PRRIFPRO = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                        +" and PRROWPRO = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                           )
                  else
                    update (i_cTable) set;
                        PRRIFPRO = "";
                        ,PRROWPRO = 0;
                        &i_ccchkf. ;
                     where;
                        PRRIFPRO = this.oParentObject.w_MVSERIAL;
                        and PRROWPRO = this.oParentObject.w_CPROWNUM;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                case this.oParentObject.w_PRTIPCAU="N"
                  * --- Write into PRE_STAZ
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"PRRIFNOT ="+cp_NullLink(cp_ToStrODBC(""),'PRE_STAZ','PRRIFNOT');
                    +",PRROWNOT ="+cp_NullLink(cp_ToStrODBC(0),'PRE_STAZ','PRROWNOT');
                        +i_ccchkf ;
                    +" where ";
                        +"PRRIFNOT = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                        +" and PRROWNOT = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                           )
                  else
                    update (i_cTable) set;
                        PRRIFNOT = "";
                        ,PRROWNOT = 0;
                        &i_ccchkf. ;
                     where;
                        PRRIFNOT = this.oParentObject.w_MVSERIAL;
                        and PRROWNOT = this.oParentObject.w_CPROWNUM;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                case this.oParentObject.w_PRTIPCAU="B"
                  * --- Write into PRE_STAZ
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"PRRIFBOZ ="+cp_NullLink(cp_ToStrODBC(""),'PRE_STAZ','PRRIFBOZ');
                    +",PRROWBOZ ="+cp_NullLink(cp_ToStrODBC(0),'PRE_STAZ','PRROWBOZ');
                        +i_ccchkf ;
                    +" where ";
                        +"PRRIFBOZ = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                        +" and PRROWBOZ = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                           )
                  else
                    update (i_cTable) set;
                        PRRIFBOZ = "";
                        ,PRROWBOZ = 0;
                        &i_ccchkf. ;
                     where;
                        PRRIFBOZ = this.oParentObject.w_MVSERIAL;
                        and PRROWBOZ = this.oParentObject.w_CPROWNUM;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
                case this.oParentObject.w_PRTIPCAU="A"
                  if this.oParentObject.w_MVNUMRIF=-20 AND this.oParentObject.w_MVROWRIF>0
                    * --- Write into PRE_STAZ
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"PRRIFFAA ="+cp_NullLink(cp_ToStrODBC(""),'PRE_STAZ','PRRIFFAA');
                      +",PRROWFAA ="+cp_NullLink(cp_ToStrODBC(0),'PRE_STAZ','PRROWFAA');
                          +i_ccchkf ;
                      +" where ";
                          +"PRRIFPRA = "+cp_ToStrODBC(this.oParentObject.w_MVSERRIF);
                          +" and PRROWPRA = "+cp_ToStrODBC(this.oParentObject.w_MVROWRIF);
                             )
                    else
                      update (i_cTable) set;
                          PRRIFFAA = "";
                          ,PRROWFAA = 0;
                          &i_ccchkf. ;
                       where;
                          PRRIFPRA = this.oParentObject.w_MVSERRIF;
                          and PRROWPRA = this.oParentObject.w_MVROWRIF;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  else
                    * --- Write into PRE_STAZ
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
                    i_ccchkf=''
                    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                      +"PRRIFFAA ="+cp_NullLink(cp_ToStrODBC(""),'PRE_STAZ','PRRIFFAA');
                      +",PRROWFAA ="+cp_NullLink(cp_ToStrODBC(0),'PRE_STAZ','PRROWFAA');
                          +i_ccchkf ;
                      +" where ";
                          +"PRRIFFAA = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                          +" and PRROWFAA = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                             )
                    else
                      update (i_cTable) set;
                          PRRIFFAA = "";
                          ,PRROWFAA = 0;
                          &i_ccchkf. ;
                       where;
                          PRRIFFAA = this.oParentObject.w_MVSERIAL;
                          and PRROWFAA = this.oParentObject.w_CPROWNUM;

                      i_Rows = _tally
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if bTrsErr
                      i_Error=MSG_WRITE_ERROR
                      return
                    endif
                  endif
                case this.oParentObject.w_PRTIPCAU="P"
                  * --- Write into PRE_STAZ
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_nConn=i_TableProp[this.PRE_STAZ_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.PRE_STAZ_idx,2])
                  i_ccchkf=''
                  this.SetCCCHKVarsWrite(@i_ccchkf,this.PRE_STAZ_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                    +"PRRIFPRA ="+cp_NullLink(cp_ToStrODBC(""),'PRE_STAZ','PRRIFPRA');
                    +",PRROWPRA ="+cp_NullLink(cp_ToStrODBC(0),'PRE_STAZ','PRROWPRA');
                        +i_ccchkf ;
                    +" where ";
                        +"PRRIFPRA = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
                        +" and PRROWPRA = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                           )
                  else
                    update (i_cTable) set;
                        PRRIFPRA = "";
                        ,PRROWPRA = 0;
                        &i_ccchkf. ;
                     where;
                        PRRIFPRA = this.oParentObject.w_MVSERIAL;
                        and PRROWPRA = this.oParentObject.w_CPROWNUM;

                    i_Rows = _tally
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if bTrsErr
                    i_Error=MSG_WRITE_ERROR
                    return
                  endif
              endcase
            endif
          endif
          this.w_PADRE.NextRowDel()     
        enddo
        this.w_PADRE.RePos()     
    endcase
  endproc
  proc Try_05340FA0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from ALT_DETT
    i_nConn=i_TableProp[this.ALT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ALT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"DESERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
             )
    else
      delete from (i_cTable) where;
            DESERIAL = this.oParentObject.w_MVSERIAL;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_05356D68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into ALT_DETT
    i_nConn=i_TableProp[this.ALT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ALT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ALT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"DESERIAL"+",DEROWNUM"+",DENUMRIF"+",DEOREEFF"+",DEMINEFF"+",DECOSINT"+",DEPREMIN"+",DEPREMAX"+",DEGAZUFF"+",DETIPPRA"+",DEMATPRA"+",DEPARASS"+",DECALDIR"+",DECOECAL"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVSERIAL),'ALT_DETT','DESERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CPROWNUM),'ALT_DETT','DEROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_MVNUMRIF),'ALT_DETT','DENUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEOREEFF),'ALT_DETT','DEOREEFF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEMINEFF),'ALT_DETT','DEMINEFF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DECOSINT),'ALT_DETT','DECOSINT');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEPREMIN),'ALT_DETT','DEPREMIN');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEPREMAX),'ALT_DETT','DEPREMAX');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEGAZUFF),'ALT_DETT','DEGAZUFF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DETIPPRA),'ALT_DETT','DETIPPRA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEMATPRA),'ALT_DETT','DEMATPRA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CNPARASS),'ALT_DETT','DEPARASS');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CNCALDIR),'ALT_DETT','DECALDIR');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CNCOECAL),'ALT_DETT','DECOECAL');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'DESERIAL',this.oParentObject.w_MVSERIAL,'DEROWNUM',this.oParentObject.w_CPROWNUM,'DENUMRIF',this.oParentObject.w_MVNUMRIF,'DEOREEFF',this.oParentObject.w_DEOREEFF,'DEMINEFF',this.oParentObject.w_DEMINEFF,'DECOSINT',this.oParentObject.w_DECOSINT,'DEPREMIN',this.oParentObject.w_DEPREMIN,'DEPREMAX',this.oParentObject.w_DEPREMAX,'DEGAZUFF',this.oParentObject.w_DEGAZUFF,'DETIPPRA',this.oParentObject.w_DETIPPRA,'DEMATPRA',this.oParentObject.w_DEMATPRA,'DEPARASS',this.oParentObject.w_CNPARASS)
      insert into (i_cTable) (DESERIAL,DEROWNUM,DENUMRIF,DEOREEFF,DEMINEFF,DECOSINT,DEPREMIN,DEPREMAX,DEGAZUFF,DETIPPRA,DEMATPRA,DEPARASS,DECALDIR,DECOECAL &i_ccchkf. );
         values (;
           this.oParentObject.w_MVSERIAL;
           ,this.oParentObject.w_CPROWNUM;
           ,this.oParentObject.w_MVNUMRIF;
           ,this.oParentObject.w_DEOREEFF;
           ,this.oParentObject.w_DEMINEFF;
           ,this.oParentObject.w_DECOSINT;
           ,this.oParentObject.w_DEPREMIN;
           ,this.oParentObject.w_DEPREMAX;
           ,this.oParentObject.w_DEGAZUFF;
           ,this.oParentObject.w_DETIPPRA;
           ,this.oParentObject.w_DEMATPRA;
           ,this.oParentObject.w_CNPARASS;
           ,this.oParentObject.w_CNCALDIR;
           ,this.oParentObject.w_CNCOECAL;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_0536DAC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into ALT_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ALT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ALT_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ALT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"DEOREEFF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEOREEFF),'ALT_DETT','DEOREEFF');
      +",DEMINEFF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEMINEFF),'ALT_DETT','DEMINEFF');
      +",DECOSINT ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DECOSINT),'ALT_DETT','DECOSINT');
      +",DEPREMIN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEPREMIN),'ALT_DETT','DEPREMIN');
      +",DEPREMAX ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEPREMAX),'ALT_DETT','DEPREMAX');
      +",DEGAZUFF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEGAZUFF),'ALT_DETT','DEGAZUFF');
      +",DETIPPRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DETIPPRA),'ALT_DETT','DETIPPRA');
      +",DEMATPRA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DEMATPRA),'ALT_DETT','DEMATPRA');
      +",DEPARASS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CNPARASS),'ALT_DETT','DEPARASS');
      +",DECALDIR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CNCALDIR),'ALT_DETT','DECALDIR');
      +",DECOECAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CNCOECAL),'ALT_DETT','DECOECAL');
          +i_ccchkf ;
      +" where ";
          +"DESERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
          +" and DEROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
          +" and DENUMRIF = "+cp_ToStrODBC(this.oParentObject.w_MVNUMRIF);
             )
    else
      update (i_cTable) set;
          DEOREEFF = this.oParentObject.w_DEOREEFF;
          ,DEMINEFF = this.oParentObject.w_DEMINEFF;
          ,DECOSINT = this.oParentObject.w_DECOSINT;
          ,DEPREMIN = this.oParentObject.w_DEPREMIN;
          ,DEPREMAX = this.oParentObject.w_DEPREMAX;
          ,DEGAZUFF = this.oParentObject.w_DEGAZUFF;
          ,DETIPPRA = this.oParentObject.w_DETIPPRA;
          ,DEMATPRA = this.oParentObject.w_DEMATPRA;
          ,DEPARASS = this.oParentObject.w_CNPARASS;
          ,DECALDIR = this.oParentObject.w_CNCALDIR;
          ,DECOECAL = this.oParentObject.w_CNCOECAL;
          &i_ccchkf. ;
       where;
          DESERIAL = this.oParentObject.w_MVSERIAL;
          and DEROWNUM = this.oParentObject.w_CPROWNUM;
          and DENUMRIF = this.oParentObject.w_MVNUMRIF;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_0533CE00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from ALT_DETT
    i_nConn=i_TableProp[this.ALT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ALT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"DESERIAL = "+cp_ToStrODBC(this.oParentObject.w_MVSERIAL);
            +" and DEROWNUM = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
            +" and DENUMRIF = "+cp_ToStrODBC(this.oParentObject.w_MVNUMRIF);
             )
    else
      delete from (i_cTable) where;
            DESERIAL = this.oParentObject.w_MVSERIAL;
            and DEROWNUM = this.oParentObject.w_CPROWNUM;
            and DENUMRIF = this.oParentObject.w_MVNUMRIF;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return


  procedure Page_14
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione contratto della pratica
    if this.oParentObject.w_PRESTA="P"
      * --- Se prestazione a tempo
      * --- Lettura dati contratto della pratica
      * --- Read from PRA_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.PRA_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRA_CONT_idx,2],.t.,this.PRA_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CPTIPDOC,CPMOORER,CPMONORE"+;
          " from "+i_cTable+" PRA_CONT where ";
              +"CPCODCON = "+cp_ToStrODBC(this.oParentObject.w_CONTRACN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CPTIPDOC,CPMOORER,CPMONORE;
          from (i_cTable) where;
              CPCODCON = this.oParentObject.w_CONTRACN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_TIPDOCCO = NVL(cp_ToDate(_read_.CPTIPDOC),cp_NullValue(_read_.CPTIPDOC))
        this.w_MOORERCO = NVL(cp_ToDate(_read_.CPMOORER),cp_NullValue(_read_.CPMOORER))
        this.w_MONORECO = NVL(cp_ToDate(_read_.CPMONORE),cp_NullValue(_read_.CPMONORE))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NVL(this.w_TIPDOCCO,"")=this.oParentObject.w_MVTIPDOC
        * --- Se il tipo documento � uguale al tipo documento che aggiorna i consuntivi del contratto
        if this.oParentObject.w_STATUSCO="I"
          * --- Se il contratto � In corso
          if this.oParentObject.w_TIPCONCO<>"C"
            * --- Se il tipo condizioni del contratto � a monte ore (diverso da canoni predeterminati)
            this.w_MOORERCO = this.w_MOORERCO+(IIF(this.w_FLDEL,1,-1)*cp_ROUND(NVL(this.oParentObject.w_MVQTAMOV, 0)*NVL(this.oParentObject.w_DUR_ORE, 0),2))+(IIF(this.w_FLUPD,1,0)*cp_ROUND(this.oParentObject.w_OMONORE,2))
            this.w_COSTATUS = this.oParentObject.w_STATUSCO
            if this.w_MOORERCO<=0 AND this.w_CFUNC<>"Query"
              * --- Verifica se si � superato il monte ore dando un log informativo e, dietro conferma dell'utente, imposta a Chiuso lo stato del contratto
              if ah_YesNo("Attenzione, � stato superato il monte ore previsto dal contratto. Vuoi chiudere il contratto?")
                this.w_COSTATUS = "C"
              endif
            endif
            * --- Aggiorna i dati del contratto
            * --- Write into PRA_CONT
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.PRA_CONT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.PRA_CONT_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.PRA_CONT_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CPMOORER ="+cp_NullLink(cp_ToStrODBC(this.w_MOORERCO),'PRA_CONT','CPMOORER');
              +",CPSTATUS ="+cp_NullLink(cp_ToStrODBC(this.w_COSTATUS),'PRA_CONT','CPSTATUS');
                  +i_ccchkf ;
              +" where ";
                  +"CPCODCON = "+cp_ToStrODBC(this.oParentObject.w_CONTRACN);
                     )
            else
              update (i_cTable) set;
                  CPMOORER = this.w_MOORERCO;
                  ,CPSTATUS = this.w_COSTATUS;
                  &i_ccchkf. ;
               where;
                  CPCODCON = this.oParentObject.w_CONTRACN;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        else
          * --- Se il contratto � Chiuso
          * --- Interrompe l'elaborazione (caricamento o modifica)
          if this.w_CFUNC<>"Query"
            this.w_MESS = Ah_MsgFormat("Impossibile salvare, il contratto utilizzato � chiuso")
            this.w_OK = .F.
          else
            if this.oParentObject.w_TIPCONCO<>"C"
              * --- Se siamo in cancellazione permette di proseguire e aggiorna comunque il monte ore residuo
              * --- Non cambia lo stato del contratto che rimane chiuso
              this.w_MOORERCO = this.w_MOORERCO+(IIF(this.w_FLDEL,1,-1)*cp_ROUND(NVL(this.oParentObject.w_MVQTAMOV, 0)*NVL(this.oParentObject.w_DUR_ORE, 0),2))+(IIF(this.w_FLUPD,1,0)*cp_ROUND(this.oParentObject.w_OMONORE,2))
              * --- Verifica se si � superato il monte ore dando un log informativo e, dietro conferma dell'utente, imposta a Chiuso lo stato del contratto
              this.w_MESS = ah_Msg("Attenzione, il contratto utilizzato nel documento � chiuso e non verr� riaperto",.F.)
              * --- Aggiorna i dati del contratto
              * --- Write into PRA_CONT
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.PRA_CONT_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PRA_CONT_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.PRA_CONT_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"CPMOORER ="+cp_NullLink(cp_ToStrODBC(this.w_MOORERCO),'PRA_CONT','CPMOORER');
                    +i_ccchkf ;
                +" where ";
                    +"CPCODCON = "+cp_ToStrODBC(this.oParentObject.w_CONTRACN);
                       )
              else
                update (i_cTable) set;
                    CPMOORER = this.w_MOORERCO;
                    &i_ccchkf. ;
                 where;
                    CPCODCON = this.oParentObject.w_CONTRACN;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
          endif
        endif
      endif
    endif
  endproc


  procedure Page_15
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricalcola l' IMPORTO DEL FIDO RELATIVO AL DOCUMENTO (precedentemente salvato in SIT_FIDI) in modo da usarlo per stornarlo in caso di modifica del documento 
    * --- Controllo del Rischio
    * --- Storno l'acconto se contabilizzato..
    *     (per ora questo accade sempre visto che questo codice scatta solo alla
    *     conferma del caricamento)
    this.w_TOTDOC = this.oParentObject.w_TOTFATTU - iIf ( Empty( this.oParentObject.w_MVRIFACC ) , this.oParentObject.w_MVACCONT , 0 )
    * --- Verifico se � stato inserito uno sconto forzato
    if this.oParentObject.w_MVIMPFIN <> 0
      this.w_TOTDOC = this.w_TOTDOC - this.oParentObject.w_MVIMPFIN
    endif
    * --- Ritenute attive se presenti le sottraggo dal totale documento.
    if this.oParentObject.w_MVRITATT <> 0
      this.w_TOTDOC = this.w_TOTDOC - this.oParentObject.w_MVRITATT
    endif
    * --- w_SPEBOL sono presenti solo in fattura negli altri casi sono zero
    this.w_SPEBOL = this.oParentObject.w_MVSPEBOL
    if this.oParentObject.w_MVCODVAL<>this.oParentObject.w_MVVALNAZ
      * --- Se il Documento e' in Valuta Converte in Moneta di Conto
      this.w_TOTDOC = VAL2MON(this.w_TOTDOC, this.oParentObject.w_MVCAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_MVDATDOC, this.oParentObject.w_MVVALNAZ,this.oParentObject.w_DECTOP)
      this.w_SPEBOL = VAL2MON(this.w_SPEBOL, this.oParentObject.w_MVCAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_MVDATDOC, this.oParentObject.w_MVVALNAZ, this.oParentObject.w_DECTOP)
    endif
    this.w_FID4 = 0
    this.w_FID5 = 0
    this.w_FID6 = 0
    * --- Aggiorna Rischio relativo al Documento
    this.w_FID4 = cp_ROUND(IIF(this.oParentObject.w_MVCLADOC="OR", this.w_TOTDOC-this.w_SPEBOL, 0) ,this.oParentObject.w_DECTOP)
    this.w_FID5 = cp_ROUND(IIF(this.oParentObject.w_MVCLADOC="DT", this.w_TOTDOC - this.w_SPEBOL, 0) ,this.oParentObject.w_DECTOP)
    if NOT this.oParentObject.w_MVCLADOC $ "OR-DT"
      * --- Fatture e note di credito 
      this.w_FID6 = this.w_TOTDOC
    else
      * --- Dt e ordini
      this.w_FID6 = 0
    endif
    * --- Se sulla causale ho specificato che il documento deve aumentare il rischio
    *     metto il valore in negativo
    *     Se si arriva all'interno dell'if generale del fido FLRISC pu� valere S o A ma controllo
    *     comunque entrambi per sicurezza
    * --- Memorizza l IMPORTO DEL FIDO RELATIVO AL DOCUMENTO (precedentemente salvato in SIT_FIDI) usato per stornarlo in caso di modifica del documento 
    this.oParentObject.w_FIDO4 = IIF(this.oParentObject.w_FLRISC="S",1,IIF(this.oParentObject.w_FLRISC="D",-1,0)) * this.w_FID4
    this.oParentObject.w_FIDO5 = IIF(this.oParentObject.w_FLRISC="S",1,IIF(this.oParentObject.w_FLRISC="D",-1,0)) * this.w_FID5
    this.oParentObject.w_FIDO6 = IIF(this.oParentObject.w_FLRISC="S",1,IIF(this.oParentObject.w_FLRISC="D",-1,0)) * this.w_FID6
  endproc


  procedure Page_16
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo importo minimo 
    if this.oParentObject.w_TDIVAMIN="N"
      this.w_BASETOT = this.oParentObject.w_TOTFATTU-this.oParentObject.w_TOTIMPOS
    else
      this.w_BASETOT = this.oParentObject.w_TOTFATTU
    endif
    if this.oParentObject.w_MVCODVAL<>this.oParentObject.w_MVVALNAZ
      this.w_BASETOT = VAL2MON(this.w_BASETOT, this.oParentObject.w_MVCAOVAL, this.oParentObject.w_CAONAZ, this.oParentObject.w_MVDATDOC, this.oParentObject.w_MVVALNAZ, this.oParentObject.w_DECTOP)
    endif
    if this.w_BASETOT< this.oParentObject.w_TDIMPMIN
      if this.oParentObject.w_MVFLPROV="S" OR this.oParentObject.w_TDTOTDOC="A"
        if lower(this.w_PADRE.class)=="tgsar_bgd"
          if this.w_PADRE.w_bLastRow
            this.w_PADRE.w_PADRE.AddLogMsg("W",ah_MsgFormat("Totale documento inferiore al valore minimo impostato nella causale: %2 %1.", ALLTRIM(Tran(this.oParentObject.w_TDIMPMIN,"@z "+v_pv[20])), g_PERVAL ))     
          endif
        else
          if !Ah_YesNo("Totale documento inferiore al valore minimo impostato nella causale: %2 %1. Confermi ugualmente?",, ALLTRIM(Tran(this.oParentObject.w_TDIMPMIN,"@z "+v_pv[20])), g_PERVAL)
            this.w_OK = .F.
          endif
        endif
      else
        this.w_OK = .F.
        if lower(this.w_PADRE.class)=="tgsar_bgd"
          if this.w_PADRE.w_bLastRow
            this.w_PADRE.w_OK = this.w_OK
            this.w_PADRE.w_RETVAL = Ah_MsgFormat("Totale documento inferiore al valore minimo impostato nella causale: %2 %1.%0Generazione documento annullata", ALLTRIM(Tran(this.oParentObject.w_TDIMPMIN,"@z "+v_pv[20])), g_PERVAL)
          endif
          this.w_OK = .T.
        else
          this.w_MESS = Ah_MsgFormat("Totale documento inferiore al valore minimo impostato nella causale: %2 %1. Impossibile confermare", ALLTRIM(Tran(this.oParentObject.w_TDIMPMIN,"@z "+v_pv[20])), g_PERVAL)
        endif
      endif
    endif
  endproc


  procedure Page_17
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_APPSALCO<>0 AND NOT EMPTY(this.oParentObject.w_MVKEYSAL) 
      * --- Se Variata Qta da Evadere Aggiorna anche i Saldi Ordinato/Impegnato
      if NOT EMPTY(this.oParentObject.w_MVFLRISE+this.oParentObject.w_MVFLORDI+this.oParentObject.w_MVFLIMPE) AND NOT EMPTY(this.oParentObject.w_MVCODMAG)
        * --- Try
        local bErr_053F24B8
        bErr_053F24B8=bTrsErr
        this.Try_053F24B8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_053F24B8
        * --- End
      endif
      * --- Eventuale Mag.Collegato
      if NOT EMPTY(this.oParentObject.w_MVF2RISE+this.oParentObject.w_MVF2ORDI+this.oParentObject.w_MVF2IMPE) AND NOT EMPTY(this.oParentObject.w_MVCODMAT)
        * --- Try
        local bErr_053F0CB8
        bErr_053F0CB8=bTrsErr
        this.Try_053F0CB8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_053F0CB8
        * --- End
      endif
    endif
  endproc
  proc Try_053F24B8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.oParentObject.w_MVFLORDI,'SCQTOPER','this.w_APPSALCO',this.w_APPSALCO,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.oParentObject.w_MVFLIMPE,'SCQTIPER','this.w_APPSALCO',this.w_APPSALCO,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.oParentObject.w_MVFLRISE,'SCQTRPER','this.w_APPSALCO',this.w_APPSALCO,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
      +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
      +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
          +i_ccchkf ;
      +" where ";
          +"SCCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVKEYSAL);
          +" and SCCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVCODMAG);
          +" and SCCODCAN = "+cp_ToStrODBC(this.oParentObject.w_MVCODCOM);
             )
    else
      update (i_cTable) set;
          SCQTOPER = &i_cOp1.;
          ,SCQTIPER = &i_cOp2.;
          ,SCQTRPER = &i_cOp3.;
          &i_ccchkf. ;
       where;
          SCCODICE = this.oParentObject.w_MVKEYSAL;
          and SCCODMAG = this.oParentObject.w_MVCODMAG;
          and SCCODCAN = this.oParentObject.w_MVCODCOM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return
  proc Try_053F0CB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into SALDICOM
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
      i_cOp1=cp_SetTrsOp(this.oParentObject.w_MVF2ORDI,'SCQTOPER','this.w_APPSALCO',this.w_APPSALCO,'update',i_nConn)
      i_cOp2=cp_SetTrsOp(this.oParentObject.w_MVF2IMPE,'SCQTIPER','this.w_APPSALCO',this.w_APPSALCO,'update',i_nConn)
      i_cOp3=cp_SetTrsOp(this.oParentObject.w_MVF2RISE,'SCQTRPER','this.w_APPSALCO',this.w_APPSALCO,'update',i_nConn)
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SCQTOPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTOPER');
      +",SCQTIPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTIPER');
      +",SCQTRPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTRPER');
          +i_ccchkf ;
      +" where ";
          +"SCCODICE = "+cp_ToStrODBC(this.oParentObject.w_MVKEYSAL);
          +" and SCCODMAG = "+cp_ToStrODBC(this.oParentObject.w_MVCODMAT);
          +" and SCCODCAN = "+cp_ToStrODBC(this.oParentObject.w_MVCODCOM);
             )
    else
      update (i_cTable) set;
          SCQTOPER = &i_cOp1.;
          ,SCQTIPER = &i_cOp2.;
          ,SCQTRPER = &i_cOp3.;
          &i_ccchkf. ;
       where;
          SCCODICE = this.oParentObject.w_MVKEYSAL;
          and SCCODMAG = this.oParentObject.w_MVCODMAT;
          and SCCODCAN = this.oParentObject.w_MVCODCOM;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pPar)
    this.pPar=pPar
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,37)]
    this.cWorkTables[1]='ADDE_SPE'
    this.cWorkTables[2]='ATTIVITA'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='CENCOST'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='DES_DIVE'
    this.cWorkTables[7]='DET_DIFF'
    this.cWorkTables[8]='DOC_DETT'
    this.cWorkTables[9]='DOC_MAST'
    this.cWorkTables[10]='DOC_RATE'
    this.cWorkTables[11]='FAT_DIFF'
    this.cWorkTables[12]='GIORMAGA'
    this.cWorkTables[13]='LIS_SCAG'
    this.cWorkTables[14]='LIS_TINI'
    this.cWorkTables[15]='LOTTIART'
    this.cWorkTables[16]='MA_COSTI'
    this.cWorkTables[17]='PAR_RIOR'
    this.cWorkTables[18]='RAG_FATT'
    this.cWorkTables[19]='RIC_PREZ'
    this.cWorkTables[20]='RIGHEEVA'
    this.cWorkTables[21]='SALDIART'
    this.cWorkTables[22]='SIT_FIDI'
    this.cWorkTables[23]='TIP_DOCU'
    this.cWorkTables[24]='VALUTE'
    this.cWorkTables[25]='VOC_COST'
    this.cWorkTables[26]='VOCIIVA'
    this.cWorkTables[27]='PDA_DETT'
    this.cWorkTables[28]='DIC_INTE'
    this.cWorkTables[29]='*TMPVEND1'
    this.cWorkTables[30]='SALDILOT'
    this.cWorkTables[31]='ALT_DETT'
    this.cWorkTables[32]='PRA_CONT'
    this.cWorkTables[33]='PRE_STAZ'
    this.cWorkTables[34]='ART_ICOL'
    this.cWorkTables[35]='SALDICOM'
    this.cWorkTables[36]='SALOTCOM'
    this.cWorkTables[37]='DICDINTE'
    return(this.OpenAllTables(37))

  proc CloseCursors()
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_DES_DIVE')
      use in _Curs_DES_DIVE
    endif
    if used('_Curs_PDA_DETT')
      use in _Curs_PDA_DETT
    endif
    if used('_Curs_DICDINTE13')
      use in _Curs_DICDINTE13
    endif
    if used('_Curs_DICDINTE26')
      use in _Curs_DICDINTE26
    endif
    if used('_Curs_DICDINTE9')
      use in _Curs_DICDINTE9
    endif
    if used('_Curs_DICDINTE8')
      use in _Curs_DICDINTE8
    endif
    if used('_Curs_DICDINTE8')
      use in _Curs_DICDINTE8
    endif
    if used('_Curs_DICDINTE9')
      use in _Curs_DICDINTE9
    endif
    if used('_Curs_DICDINTE13')
      use in _Curs_DICDINTE13
    endif
    if used('_Curs_DICDINTE26')
      use in _Curs_DICDINTE26
    endif
    if used('_Curs_DICDINTE8')
      use in _Curs_DICDINTE8
    endif
    if used('_Curs_DICDINTE8')
      use in _Curs_DICDINTE8
    endif
    if used('_Curs_DICDINTE8')
      use in _Curs_DICDINTE8
    endif
    if used('_Curs_DICDINTE8')
      use in _Curs_DICDINTE8
    endif
    if used('_Curs_DICDINTE8')
      use in _Curs_DICDINTE8
    endif
    if used('_Curs_DICDINTE8')
      use in _Curs_DICDINTE8
    endif
    if used('_Curs_DICDINTE9')
      use in _Curs_DICDINTE9
    endif
    if used('_Curs_DICDINTE8')
      use in _Curs_DICDINTE8
    endif
    if used('_Curs_DICDINTE8')
      use in _Curs_DICDINTE8
    endif
    if used('_Curs_DICDINTE9')
      use in _Curs_DICDINTE9
    endif
    if used('_Curs_DICDINTE8')
      use in _Curs_DICDINTE8
    endif
    if used('_Curs_GSVEBBER')
      use in _Curs_GSVEBBER
    endif
    if used('_Curs_SALDILOT')
      use in _Curs_SALDILOT
    endif
    if used('_Curs_SALDILOT')
      use in _Curs_SALDILOT
    endif
    if used('_Curs_SALDILOT')
      use in _Curs_SALDILOT
    endif
    if used('_Curs_SALDILOT')
      use in _Curs_SALDILOT
    endif
    if used('_Curs_SALDILOT')
      use in _Curs_SALDILOT
    endif
    if used('_Curs_RAG_FATT')
      use in _Curs_RAG_FATT
    endif
    if used('_Curs_GSVE_BMT')
      use in _Curs_GSVE_BMT
    endif
    if used('_Curs_GSVE_BML')
      use in _Curs_GSVE_BML
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPar"
endproc
