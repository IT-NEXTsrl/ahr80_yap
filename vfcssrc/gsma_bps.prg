* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bps                                                        *
*              Politica delle scorte                                           *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-11-06                                                      *
* Last revis.: 2014-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bps",oParentObject)
return(i_retval)

define class tgsma_bps as StdBatch
  * --- Local variables
  w_DATREG = ctod("  /  /  ")
  w_QTATOT = 0
  w_MAGRAG = space(5)
  w_TOTGIO = 0
  w_QTAIN = 0
  w_NUMGIO = 0
  w_QTAOUT = 0
  w_MAGAZ = space(5)
  w_QTACAR = 0
  w_QTASCA = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Politica delle Scorte
    * --- da GSMA_SPS
    * --- Variabili per il report
    USE IN SELECT("CURSCOR")
     
 MAGAZZIN = this.oParentObject.w_CODMAG 
 TIPMOV = this.oParentObject.w_TIPMOV 
 CODFAM = this.oParentObject.w_CODFAM 
 SELPRO = this.oParentObject.w_SELPRO 
 CODGRU = this.oParentObject.w_CODGRU 
 POLSCO = this.oParentObject.w_POLSCO 
 CODCAT = this.oParentObject.w_CODCAT 
 MD = this.oParentObject.w_MD 
 MA = this.oParentObject.w_MA 
 CODMAR = this.oParentObject.w_CODMAR 
 CARTIC = this.oParentObject.w_CODART 
 TIPART = this.oParentObject.w_TIPART
    CVARI =iif(UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", this.oParentObject.w_CODVAR,"") 
 CODPLA = iif(UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE", this.oParentObject.w_CODPLA,"")
    * --- Seleziono il Magazzino Principale
    if NOT EMPTY(NVL(this.oParentObject.w_COLLEFIS,SPACE(5)))
      this.w_MAGRAG = this.oParentObject.w_COLLEFIS
    else
      this.w_MAGRAG = this.oParentObject.w_CODMAG
    endif
    this.w_TOTGIO = VAL(SYS(11,this.oParentObject.w_MA))-VAL(SYS(11,this.oParentObject.w_MD)) + 1
    * --- Creazione cursore Saldi e Movimenti di Magazzino
    ah_msg("Calcolo movimenti articoli...")
    vq_exec("query\GSMASSSP",this,"SALDIART")
    vq_exec("query\GSMA2SSP",this,"APPIART")
    if UPPER( g_APPLICATION ) = "AD HOC ENTERPRISE"
       
 Select APPIART 
 GO TOP 
 SCAN FOR EMPTY(NVL(MMCODVAR," ")) 
 REPLACE MMCODVAR WITH SPACE(20) 
 ENDSCAN
      * --- Nel Cursore TEMP inserisco tutti i dati relativi ai movimenti di magazzino pi� l'esistenza e la descrizione dell'articolo
       
 SELECT APPIART.*,SLQTAPER,DESART FROM SALDIART INNER JOIN APPIART; 
 ON SLCODART=MMCODART and nvl(SLCODVAR," ")=nvl(MMCODVAR," ") INTO Cursor TEMP
    else
      * --- Nel Cursore TEMP inserisco tutti i dati relativi ai movimenti di magazzino pi� l'esistenza e la descrizione dell'articolo
       
 SELECT APPIART.*,SLQTAPER,DESART FROM SALDIART INNER JOIN APPIART; 
 ON SLCODICE=MMCODART INTO Cursor TEMP
    endif
     
 CREATE CURSOR CurScor (CODMAG C(5), CODART C(20),DESART C(40), CODVAR C(20), ESIST N(12,3),DATREG D(8),; 
 TIPDOC C(5), NUMREG N(6),CODUTE N(4),QTACAR N(12,3),QTASCA N(12,3),; 
 QTAIN N(12,3),QTAOUT N(12,3),GIORNI N(6), TOTGIO N(6), ARUNMIS C(3))
    if reccount("TEMP")=0
      AH_ERRORMSG("Per la selezione effettuata non esistono dati da elaborare",48)
    else
       
 SELECT TEMP 
 GO TOP
      SCAN
      this.w_DATREG = CP_TODATE(MMDATREG)
      this.w_NUMGIO = VAL(SYS(11,this.w_DATREG))-VAL(SYS(11,this.oParentObject.w_MD)) + 1
      this.w_QTAIN = QTACAR * this.w_NUMGIO
      this.w_QTAOUT = QTASCA * this.w_NUMGIO
       
 INSERT INTO CurScor values (TEMP.MMCODMAG,TEMP.MMCODART,TEMP.DESART,NVL(TEMP.MMCODVAR," "),TEMP.SLQTAPER,this.w_DATREG,; 
 TEMP.MVTIPDOC,TEMP.MMNUMREG,TEMP.MMCODUTE,TEMP.QTACAR,TEMP.QTASCA,this.w_QTAIN,this.w_QTAOUT,this.w_NUMGIO,this.w_TOTGIO,TEMP.ARUNMIS1)
      SELECT TEMP
      ENDSCAN
      * --- Lancio del report.Testo il valore della combo Tipo di Stampa.
      if this.oParentObject.w_POLSCO="G"
        SELECT * FROM CurScor into Cursor __TMP__ order by CODART,CODVAR,DATREG,ARUNMIS
        cp_chprn("QUERY\GSMA_SSP.FRX","",this.oParentObject)
      else
         
 SELECT CODART,CODVAR,MAX(DESART) AS DESART,MAX(ESIST) AS ESIST,; 
 SUM(QTASCA) AS QTASCA,SUM (QTAIN) AS QTAIN,SUM(QTAOUT) AS QTAOUT, MAX(TOTGIO) AS TOTGIO,ARUNMIS; 
 FROM CurScor into Cursor __TMP__; 
 GROUP BY CODART,CODVAR,ARUNMIS; 
 ORDER BY CODART,CODVAR,ARUNMIS
        do case
          case this.oParentObject.w_POLSCO="P"
            cp_chprn("QUERY\GSMA1SSP.FRX","",this.oParentObject)
          case this.oParentObject.w_POLSCO="R"
            cp_chprn("QUERY\GSMA2SSP.FRX","",this.oParentObject)
          case this.oParentObject.w_POLSCO="C"
            cp_chprn("QUERY\GSMA3SSP.FRX","",this.oParentObject)
        endcase
      endif
    endif
    USE IN SELECT("__tmp__")
    USE IN SELECT("SALDIART")
    USE IN SELECT("APPIART")
    USE IN SELECT("CurScor")
    USE IN SELECT("TEMP")
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
