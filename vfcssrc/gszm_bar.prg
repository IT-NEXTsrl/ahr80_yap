* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bar                                                        *
*              Menu contestuale articoli                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_21]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-07-06                                                      *
* Last revis.: 2017-06-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pFUNZ,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bar",oParentObject,m.pFUNZ,m.pOPER)
return(i_retval)

define class tgszm_bar as StdBatch
  * --- Local variables
  pFUNZ = space(2)
  pOPER = space(1)
  w_OBJECT = .NULL.
  w_PROG = .NULL.
  w_ARCODART = space(20)
  w_CACODART = space(20)
  w_ARTIPART = space(2)
  w_ARTIPBAR = space(1)
  w_TIPBAR = space(1)
  w_ARFLPECO = space(1)
  w_ARUNMIS1 = space(3)
  w_FLFRAZ = space(1)
  w_UTDC = ctod("  /  /  ")
  w_ARDESART = space(40)
  cComment = space(20)
  w_UTDC = ctod("  /  /  ")
  w_ARDESART = space(40)
  cComment = space(20)
  * --- WorkFile variables
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  UNIMIS_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Routine men� contestuale Articoli
    * --- Assegno i valori alle variabili
    if this.pOPER = "A"
      this.w_ARCODART = g_oMenu.getbyindexkeyvalue(1)
    else
      this.w_CACODART = g_oMenu.getbyindexkeyvalue(1)
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODART"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_CACODART);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODART;
          from (i_cTable) where;
              CACODICE = this.w_CACODART;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ARCODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    do case
      case this.pFUNZ = "00"
        * --- Select from ART_ICOL
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select *  from "+i_cTable+" ART_ICOL ";
              +" where ARCODART="+cp_ToStrODBC(this.w_ARCODART)+"";
               ,"_Curs_ART_ICOL")
        else
          select * from (i_cTable);
           where ARCODART=this.w_ARCODART;
            into cursor _Curs_ART_ICOL
        endif
        if used('_Curs_ART_ICOL')
          select _Curs_ART_ICOL
          locate for 1=1
          do while not(eof())
          g_oMenu.Add_oFields("_Curs_ART_ICOL")
          exit
            select _Curs_ART_ICOL
            continue
          enddo
          use
        endif
        this.w_ARTIPART = g_oMenu.getfieldsvalue("ARTIPART")
        do case
          case this.w_ARTIPART $ "FM-FO-DE"
            * --- Servizio
            this.w_OBJECT = GSMA_AAS()
          case this.w_ARTIPART $ "PF-SE-PH-MP-FS"
            * --- Anagrafica Articoli
            this.w_OBJECT = GSMA_AAR()
          case this.w_ARTIPART="CC"
            * --- Anagrafica Articoli condifurabili
            this.w_OBJECT = GSCR_AAR()
        endcase
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.EcpFilter()     
        * --- Valorizzo i campi chiave
        this.w_OBJECT.w_ARCODART = this.w_ARCODART
        * --- Carico il record richiesto
        this.w_OBJECT.EcpSave()     
        this.w_OBJECT = .NULL.
      case this.pFUNZ = "01"
        * --- Codici di Ricerca
        this.w_TIPBAR = "0"
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARTIPBAR,ARFLPECO,ARUNMIS1"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARTIPBAR,ARFLPECO,ARUNMIS1;
            from (i_cTable) where;
                ARCODART = this.w_ARCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ARTIPBAR = NVL(cp_ToDate(_read_.ARTIPBAR),cp_NullValue(_read_.ARTIPBAR))
          this.w_ARFLPECO = NVL(cp_ToDate(_read_.ARFLPECO),cp_NullValue(_read_.ARFLPECO))
          this.w_ARUNMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from UNIMIS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.UNIMIS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.UNIMIS_idx,2],.t.,this.UNIMIS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "UMFLFRAZ"+;
            " from "+i_cTable+" UNIMIS where ";
                +"UMCODICE = "+cp_ToStrODBC(this.w_ARUNMIS1);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            UMFLFRAZ;
            from (i_cTable) where;
                UMCODICE = this.w_ARUNMIS1;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLFRAZ = NVL(cp_ToDate(_read_.UMFLFRAZ),cp_NullValue(_read_.UMFLFRAZ))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        do GSMA_KCA with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pFUNZ = "02"
        * --- Saldi Articoli
        do GSMA_KCS with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pFUNZ = "03"
        * --- Prezzi
        do GSMA_KUC with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pFUNZ = "04"
        * --- Costi
        do GSMA_KUF with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pFUNZ = "05"
        * --- Schede
        GSAR_BKK(this,"G")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pFUNZ = "06"
        * --- Visualizza
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARDESART,UTDC"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARDESART,UTDC;
            from (i_cTable) where;
                ARCODART = this.w_ARCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ARDESART = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
          this.w_UTDC = NVL(cp_ToDate(_read_.UTDC),cp_NullValue(_read_.UTDC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.cComment = ah_Msgformat("ARTICOLI")
        GSUT_BCV(this,"ART_ICOL",this.w_ARCODART,"GSMA_AAR","V")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pFUNZ = "07"
        * --- Visalizzazione Mov. di Magazzino
        this.w_OBJECT = GSMA_SZM()
        if !this.w_OBJECT.bSec1
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_CODART = this.w_ARCODART
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODART")
        this.w_PROG.bUpd = .t.
        this.w_OBJECT.SetControlsValue()     
        this.w_PROG.Valid()     
        this.w_OBJECT.w_CODART2 = this.w_ARCODART
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODART2")
        this.w_PROG.bUpd = .t.
        this.w_OBJECT.SetControlsValue()     
        this.w_PROG.Valid()     
        DO GSMA_BVM WITH this.w_OBJECT
        this.w_PROG = .null.
        this.w_OBJECT = .null.
      case this.pFUNZ = "08"
        * --- Visalizzazione Doc. Vendita
        this.w_OBJECT = GSVE_SZM()
        if !this.w_OBJECT.bSec1
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_CODART = this.w_ARCODART
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODART")
        this.w_PROG.bUpd = .t.
        this.w_OBJECT.SetControlsValue()     
        this.w_PROG.Valid()     
        DO GSVE_BVM WITH this.w_OBJECT,"InitV"
        this.w_PROG = .null.
        this.w_OBJECT = .null.
      case this.pFUNZ = "09"
        * --- Visalizzazione Doc. Acquisto
        this.w_OBJECT = GSAC_SZM()
        if !this.w_OBJECT.bSec1
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_CODART = this.w_ARCODART
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODART")
        this.w_PROG.bUpd = .t.
        this.w_OBJECT.SetControlsValue()     
        this.w_PROG.Valid()     
        DO GSVE_BVM WITH this.w_OBJECT,"InitA"
        this.w_PROG = .null.
        this.w_OBJECT = .null.
      case this.pFUNZ = "10"
        * --- Visalizzazione Ordini
        this.w_OBJECT = GSOR_SZM()
        if !this.w_OBJECT.bSec1
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_CODART = this.w_ARCODART
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODART")
        this.w_PROG.bUpd = .t.
        this.w_OBJECT.SetControlsValue()     
        this.w_PROG.Valid()     
        DO GSOR_BVM WITH this.w_OBJECT,"Init"
        DO GSOR_BVM WITH this.w_OBJECT,"O"
        this.w_PROG = .null.
        this.w_OBJECT = .null.
      case this.pFUNZ = "11"
        * --- Schede magazzino
        this.w_OBJECT = GSMA_SSM()
        if !this.w_OBJECT.bSec1
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_CODINI = this.w_ARCODART
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODINI")
        this.w_PROG.bUpd = .t.
        this.w_OBJECT.SetControlsValue()     
        this.w_PROG.Valid()     
        this.w_OBJECT.w_CODFIN = this.w_ARCODART
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODFIN")
        this.w_PROG.bUpd = .t.
        this.w_OBJECT.SetControlsValue()     
        this.w_PROG.Valid()     
        this.w_PROG = .null.
        this.w_OBJECT = .null.
      case this.pFUNZ = "12"
        * --- Controllo Scorte
        this.w_OBJECT = GSMA_SSC()
        if !this.w_OBJECT.bSec1
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_CODART = this.w_ARCODART
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODART")
        this.w_PROG.bUpd = .t.
        this.w_OBJECT.SetControlsValue()     
        this.w_PROG.Valid()     
        this.w_PROG = .null.
        this.w_OBJECT = .null.
      case this.pFUNZ = "13"
        * --- Inventario fisico
        this.w_OBJECT = GSMA_SIF()
        if !this.w_OBJECT.bSec1
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_ARTINI = this.w_ARCODART
        this.w_PROG = this.w_OBJECT.GetcTRL("w_ARTINI")
        this.w_PROG.bUpd = .t.
        this.w_OBJECT.SetControlsValue()     
        this.w_PROG.Valid()     
        this.w_OBJECT.w_ARTFIN = this.w_ARCODART
        this.w_PROG = this.w_OBJECT.GetcTRL("w_ARTFIN")
        this.w_PROG.bUpd = .t.
        this.w_OBJECT.SetControlsValue()     
        this.w_PROG.Valid()     
        this.w_PROG = .null.
        this.w_OBJECT = .null.
      case this.pFUNZ = "14"
        * --- Prospetto Venduto
        this.w_OBJECT = GSMA_SDV()
        this.w_OBJECT.w_CODART = this.w_ARCODART
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODART")
        this.w_PROG.bUpd = .t.
        this.w_OBJECT.SetControlsValue()     
        this.w_PROG.Valid()     
        this.w_OBJECT.w_CODART1 = this.w_ARCODART
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODART1")
        this.w_PROG.bUpd = .t.
        this.w_OBJECT.SetControlsValue()     
        this.w_PROG.Valid()     
        this.w_PROG = .null.
        this.w_OBJECT = .null.
      case this.pFUNZ = "15"
        * --- Prospetto Acquisti
        this.w_OBJECT = GSAC_SPA()
        if !this.w_OBJECT.bSec1
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.w_CODART = this.w_ARCODART
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODART")
        this.w_PROG.bUpd = .t.
        this.w_OBJECT.SetControlsValue()     
        this.w_PROG.Valid()     
        this.w_OBJECT.w_CODART1 = this.w_ARCODART
        this.w_PROG = this.w_OBJECT.GetcTRL("w_CODART1")
        this.w_PROG.bUpd = .t.
        this.w_OBJECT.SetControlsValue()     
        this.w_PROG.Valid()     
        this.w_PROG = .null.
        this.w_OBJECT = .null.
      case this.pFUNZ = "16"
        * --- Cattura
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARDESART,UTDC"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_ARCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARDESART,UTDC;
            from (i_cTable) where;
                ARCODART = this.w_ARCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_ARDESART = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
          this.w_UTDC = NVL(cp_ToDate(_read_.UTDC),cp_NullValue(_read_.UTDC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.cComment = ah_Msgformat("ARTICOLI")
        GSUT_BCV(this,"ART_ICOL",this.w_ARCODART,"GSMA_AAR","C")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pFUNZ = "17"
        * --- Manutenzione prezzi
        this.w_OBJECT = GSMA_MLS()
        if !(this.w_OBJECT.bSec1)
          i_retcode = 'stop'
          return
        endif
        this.w_OBJECT.EcpFilter()     
        * --- Valorizzo i campi chiave
        this.w_OBJECT.w_LICODART = this.w_ARCODART
        * --- Carico il record richiesto
        this.w_OBJECT.EcpSave()     
        this.w_OBJECT = .NULL.
    endcase
  endproc


  proc Init(oParentObject,pFUNZ,pOPER)
    this.pFUNZ=pFUNZ
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='UNIMIS'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_ART_ICOL')
      use in _Curs_ART_ICOL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pFUNZ,pOPER"
endproc
