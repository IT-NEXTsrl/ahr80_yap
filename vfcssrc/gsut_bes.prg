* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bes                                                        *
*              Acquisizione file per maschera                                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF]                                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-14                                                      *
* Last revis.: 2016-07-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_Archivio,w_Chiave,w_Operazione,w_ALLOK
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bes",oParentObject,m.w_Archivio,m.w_Chiave,m.w_Operazione,m.w_ALLOK)
return(i_retval)

define class tgsut_bes as StdBatch
  * --- Local variables
  w_Archivio = space(20)
  w_Chiave = space(50)
  w_Operazione = space(1)
  w_ALLOK = space(1)
  w_TMPN = 0
  w_FLGUTE = space(1)
  w_OP = space(2)
  w_UIDEXT = space(50)
  GRID = .NULL.
  w_CODUTE = 0
  w_CODAZI = space(5)
  w_PERIODO = ctod("  /  /  ")
  w_TMPC = space(100)
  w_IDSERIAL = space(15)
  w_IDOGGETT = space(100)
  w_IDTABKEY = space(20)
  w_CRITPERI = space(2)
  w_TMPL = .f.
  w_NOMEFILE = space(20)
  w_TIPOFILE = space(50)
  w_posizpunto = 0
  w_ImgPat = space(254)
  w_ChiaveRicerca = space(20)
  w_LIPATIMG = space(254)
  w_LICLADOC = space(10)
  w_IDMODALL = space(1)
  w_IDORIFIL = space(250)
  w_WRONGOP = .f.
  w_ControllaEsisteFile = space(1)
  w_LTIPOALL = space(5)
  w_LCLASALL = space(5)
  w_LALLPRIN = space(1)
  w_LNOTEALL = space(0)
  w_LPUBWEB = space(1)
  w_PATHABS = space(254)
  w_IDALLPRI = space(1)
  w_CDSERVER = space(20)
  w_CD__PORT = 0
  w_CD__USER = space(20)
  w_CDPASSWD = space(20)
  w_CDARCHIVE = space(20)
  w_OPLOGICO = .f.
  w_CODPRAT = space(15)
  w_DESCATTPRI = space(60)
  w_CLAPRA = space(1)
  w_VALATTPRI = space(150)
  w_CAMPATPRI = space(15)
  w_KEY_FIELDS = space(250)
  w_TABNAME = space(10)
  w_KEYRECORD = space(60)
  w_IDUTEEXP = 0
  w_IDWEBAPPL = space(2)
  w_GSUT_BES = space(0)
  w_NUMFILTRI = 0
  w_VALORE = space(10)
  w_GSUT_MA1 = .NULL.
  w_FLDFILTER = space(10)
  w_OLD_TABNAME = space(10)
  w_OLD_CAMPATPRI = space(15)
  w_OLD_DESCATTPRI = space(60)
  w_OLD_VALATTPRI = space(150)
  * --- WorkFile variables
  PROMCLAS_idx=0
  PRO_PVIS_idx=0
  CPUSERS_idx=0
  CPUSRGRP_idx=0
  PROMINDI_idx=0
  DMPARAM_idx=0
  TMPZ_idx=0
  CAN_TIER_idx=0
  TMPVEND1_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato dalla maschera GSUT_KFG: crea cursore per visualizzare l'elenco dei file associati al codice selezionato 
    * --- Il prossimo parametro serve per non entrare in loop nel caso in cui per l'estensione del file allegato non � presente un relativo programma di gestione
    * --- Verifico subito se il parametro � stato passato per non doverlo ricontrollare ad ogni successivo IF
    if Vartype(this.w_Operazione)<>"C"
      this.w_Operazione = "XXXXXXXXXX"
    endif
    * --- Dichiaro le variabili filtro introdotte nella nuova 'Ricerca documenti archiviati'  (Maschera presente quando non � attivo il modulo 'DOCM')
    * --- variabili E.D.S.
    * --- Variabili AET
    * --- Variabili attributo primario
    * --- Le variabili seguenti sono presenti solo se non � attivo il modulo 'DOCM'
    * --- Codice pratica (Solo AET)
    * --- Cancellazione tabelle temporanee
    * --- Delete from TMPZ
    i_nConn=i_TableProp[this.TMPZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"1 = "+cp_ToStrODBC(1);
             )
    else
      delete from (i_cTable) where;
            1 = 1;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    if this.w_Operazione<>"INVIOMASSIVO"
      if Not Empty( this.oParentObject.w_SELCODCLA) And SUBSTR( CHKPECLA( this.oParentObject.w_SELCODCLA, i_CODUTE), 1, 1 ) <> "S"
        this.oParentObject.NotifyEvent("Carica")
        i_retcode = 'stop'
        return
      endif
      * --- Costruisco la variabile pubblica da passare alla query che costruisce il 
      *     temporaneo con all'interno gli indici che corrispondono ai criteri di ricerca.
      *     
      *     La var. pubblica (g_GSUT_BES) � di fatto l'elenco delle condizioni da
      *     applicare in Or tra di loro. Quindi se ad esempio voglio filtrare i DDT di vendita
      *     di ANGELI con numero 2 e del magazzino AU essa varr�
      *     ( PRODINDI.IDCODATT = 'INTESTATARIO' AND PRODINDI.IDVALATT = 'CANALOGICA' ) OR 
      *     (PRODINDI.IDCODATT = 'NUMERODDT' AND PRODINDI.IDVALNUM = 2 ) OR 
      *     ( IDCODATT = 'MAGAZZINO' AND IDVALATT = 'AU' )
      * --- Contiene il numero di filtro che andiamo ad applicare sugli attributi, 
      *     se non ne applico passo 0
      if isahe() and this.w_Operazione="INIZIA"
        this.w_GSUT_BES = "1=0"
      else
        this.w_GSUT_BES = "1=1"
      endif
      if Isalt() and this.w_Operazione<>"INIZIA" and Empty(this.oParentObject.w_SELOGGETTO) and (Empty(This.oparentobject.w_CODPRAT) AND this.w_ARCHIVIO = "CAN_TIER") and Empty(this.oParentObject.w_SELNOMEFILE) and Empty(this.oParentObject.w_SELNOMEORIG) and Empty(this.oParentObject.w_CLASALL) and Empty(this.oParentObject.w_TIPOALL) and !Ah_yesno("Attenzione, nessun filtro significativo impostato continuare ugualmente?")
        i_retcode = 'stop'
        return
      endif
      if empty(this.oParentObject.w_SELCODCLA)
        this.w_NUMFILTRI = 0
      else
        this.w_GSUT_MA1 = this.oParentObject.GSUT_MA1
        this.w_GSUT_MA1.MarkPos()     
        SELECT (this.w_GSUT_MA1.cTrsName) 
 Go top
        do while not this.w_GSUT_MA1.Eof_Trs()
          this.w_GSUT_MA1.SetRow()     
          this.w_OP = alltrim(this.w_GSUT_MA1.w_OPERAT)
          * --- Verifico congruit� operatore in base al tipo attributo
          do case
            case this.w_GSUT_MA1.w_IDTIPATT="D"
              this.w_WRONGOP = ! INLIST(this.w_OP,"=",">","<",">=","<=","<>")
            case this.w_GSUT_MA1.w_IDTIPATT="N"
              this.w_WRONGOP = ! INLIST(this.w_OP,"IN","NOT IN","=","<>",">","<",">=","<=")
            otherwise
              this.w_WRONGOP = ! INLIST(this.w_OP,"IN","NOT IN","LIKE","=","<>",">","<",">=","<=")
          endcase
          if Not Empty (Nvl( this.w_GSUT_MA1.w_IDVALATT,"") ) AND NOT this.w_WRONGOP
            this.w_NUMFILTRI = this.w_NUMFILTRI + 1
            this.w_TMPC = this.w_GSUT_MA1.w_IDVALATT
            do case
              case this.w_GSUT_MA1.w_IDTIPATT="D"
                if UPPER(this.w_OP)<>"IN" AND UPPER(this.w_OP)<>"NOT IN"
                  if UPPER(this.w_OP)="LIKE" AND NOT("%"$this.w_TMPC)
                    this.w_TMPC = ALLTRIM(this.w_TMPC)+"%"
                  endif
                  this.w_VALORE = Cp_ToStrOdbc ( Cp_CharToDate( this.w_TMPC ) )
                else
                  this.w_VALORE = this.w_TMPC
                endif
                this.w_FLDFILTER = "IDVALDAT"
              case this.w_GSUT_MA1.w_IDTIPATT="N"
                if UPPER(this.w_OP)<>"IN" AND UPPER(this.w_OP)<>"NOT IN"
                  this.w_VALORE = cp_tostrODBC(val( this.w_TMPC ))
                else
                  this.w_VALORE = this.w_TMPC
                endif
                this.w_FLDFILTER = "IDVALNUM"
              otherwise
                if UPPER(this.w_OP)<>"IN" AND UPPER(this.w_OP)<>"NOT IN"
                  this.w_VALORE = Cp_ToStrODBC( Rtrim(this.w_TMPC) )
                else
                  this.w_VALORE = this.w_TMPC
                endif
                this.w_FLDFILTER = "IDVALATT"
            endcase
            if this.w_NUMFILTRI = 1
              this.w_GSUT_BES = "1=0"
            endif
            if !Isalt() or ! (Upper(Alltrim(this.w_GSUT_MA1.w_IDCODATT))=="DESCRIZIONE")
              this.w_GSUT_BES = this.w_GSUT_BES + " Or (PRODINDI.IDCODATT="+ Cp_ToStrODBC(this.w_GSUT_MA1.w_IDCODATT)+" And " + alltrim(this.w_FLDFILTER)+ " " + alltrim(this.w_OP) + " " + alltrim(this.w_VALORE) + ")"
            else
              this.w_NUMFILTRI = this.w_NUMFILTRI - 1
            endif
          endif
          * --- L'errore di operatore non valido viene dato solo in presenza di un filtro valido
          if this.w_WRONGOP AND Not Empty (Nvl( this.w_GSUT_MA1.w_IDVALATT,"") ) 
            Ah_ErrorMsg("Presenza di operatore non valido per l'attributo %1", 48, "", this.w_GSUT_MA1.w_IDCODATT)
          endif
          this.w_GSUT_MA1.NextRow()     
        enddo
        this.w_GSUT_MA1.RePos()     
      endif
      * --- Valorizzo le variabili filtro con le rispettive variabili 'caller' gestite nella maschera GSUT_KFG
      this.w_LTIPOALL = iif(vartype(this.oParentObject.w_TIPOALL)="C",this.oParentObject.w_TIPOALL," ")
      this.w_LCLASALL = iif(vartype(this.oParentObject.w_CLASALL)="C",this.oParentObject.w_CLASALL," ")
      this.w_LPUBWEB = iif(vartype(this.oParentObject.w_IDWEBAPP)="C",this.oParentObject.w_IDWEBAPP,"T")
      this.w_LNOTEALL = iif(vartype(this.oParentObject.w_SELNOTEALL)="C",this.oParentObject.w_SELNOTEALL," ")
      this.w_LALLPRIN = iif(vartype(this.oParentObject.w_ALLPRIN)="C",this.oParentObject.w_ALLPRIN," ")
      this.w_NOMEFILE = space(20)
      this.w_posizpunto = 0
      this.w_TIPOFILE = space(50)
      this.w_ImgPat = space(254)
      this.w_ChiaveRicerca = space(20)
      this.w_LIPATIMG = ""
      this.w_CODUTE = i_CODUTE
      if InList(this.w_Operazione, "SETPAR", "PREFER", "INIZIA")
        * --- Legge eventuali preferenze per l'utente corrente
        this.w_IDTABKEY = this.w_ARCHIVIO
        this.w_CODAZI = i_CODAZI
        if g_DOCM = "S"
          * --- Legge preferenze per utente
          * --- Read from PRO_PVIS
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.PRO_PVIS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PRO_PVIS_idx,2],.t.,this.PRO_PVIS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "PVCODCLA,PVPERVIS,PVSELUTE"+;
              " from "+i_cTable+" PRO_PVIS where ";
                  +"PVNOMTAB = "+cp_ToStrODBC(this.w_IDTABKEY);
                  +" and PVTIPFIL = "+cp_ToStrODBC("U");
                  +" and PVCODUTE = "+cp_ToStrODBC(i_CODUTE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              PVCODCLA,PVPERVIS,PVSELUTE;
              from (i_cTable) where;
                  PVNOMTAB = this.w_IDTABKEY;
                  and PVTIPFIL = "U";
                  and PVCODUTE = i_CODUTE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_SELCODCLA = NVL(cp_ToDate(_read_.PVCODCLA),cp_NullValue(_read_.PVCODCLA))
            this.oParentObject.w_SELDATARC = NVL(cp_ToDate(_read_.PVPERVIS),cp_NullValue(_read_.PVPERVIS))
            this.w_FLGUTE = NVL(cp_ToDate(_read_.PVSELUTE),cp_NullValue(_read_.PVSELUTE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if i_ROWS=0
            this.w_TMPL = .F.
            this.w_CODUTE = i_CODUTE
            * --- Select from CPUSRGRP
            i_nConn=i_TableProp[this.CPUSRGRP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CPUSRGRP_idx,2],.t.,this.CPUSRGRP_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" CPUSRGRP ";
                  +" where USERCODE = "+cp_ToStrODBC(this.w_CODUTE)+"";
                   ,"_Curs_CPUSRGRP")
            else
              select * from (i_cTable);
               where USERCODE = this.w_CODUTE;
                into cursor _Curs_CPUSRGRP
            endif
            if used('_Curs_CPUSRGRP')
              select _Curs_CPUSRGRP
              locate for 1=1
              do while not(eof())
              this.w_TMPN = nvl(_Curs_CPUSRGRP.GROUPCODE, 0)
              * --- Read from PRO_PVIS
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PRO_PVIS_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PRO_PVIS_idx,2],.t.,this.PRO_PVIS_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PVCODCLA,PVPERVIS,PVSELUTE"+;
                  " from "+i_cTable+" PRO_PVIS where ";
                      +"PVNOMTAB = "+cp_ToStrODBC(this.w_IDTABKEY);
                      +" and PVTIPFIL = "+cp_ToStrODBC("G");
                      +" and PVCODGRU = "+cp_ToStrODBC(this.w_TMPN);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PVCODCLA,PVPERVIS,PVSELUTE;
                  from (i_cTable) where;
                      PVNOMTAB = this.w_IDTABKEY;
                      and PVTIPFIL = "G";
                      and PVCODGRU = this.w_TMPN;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_SELCODCLA = NVL(cp_ToDate(_read_.PVCODCLA),cp_NullValue(_read_.PVCODCLA))
                this.oParentObject.w_SELDATARC = NVL(cp_ToDate(_read_.PVPERVIS),cp_NullValue(_read_.PVPERVIS))
                this.w_FLGUTE = NVL(cp_ToDate(_read_.PVSELUTE),cp_NullValue(_read_.PVSELUTE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if i_Rows>0
                this.w_TMPL = .T.
                exit
              endif
                select _Curs_CPUSRGRP
                continue
              enddo
              use
            endif
            if not this.w_TMPL
              * --- Read from PRO_PVIS
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PRO_PVIS_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PRO_PVIS_idx,2],.t.,this.PRO_PVIS_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PVCODCLA,PVPERVIS,PVSELUTE"+;
                  " from "+i_cTable+" PRO_PVIS where ";
                      +"PVNOMTAB = "+cp_ToStrODBC(this.w_IDTABKEY);
                      +" and PVTIPFIL = "+cp_ToStrODBC("D");
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PVCODCLA,PVPERVIS,PVSELUTE;
                  from (i_cTable) where;
                      PVNOMTAB = this.w_IDTABKEY;
                      and PVTIPFIL = "D";
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.oParentObject.w_SELCODCLA = NVL(cp_ToDate(_read_.PVCODCLA),cp_NullValue(_read_.PVCODCLA))
                this.oParentObject.w_SELDATARC = NVL(cp_ToDate(_read_.PVPERVIS),cp_NullValue(_read_.PVPERVIS))
                this.w_FLGUTE = NVL(cp_ToDate(_read_.PVSELUTE),cp_NullValue(_read_.PVSELUTE))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          endif
          * --- Se non esistono preferenze di ricerca legate alla tabella
          if empty(this.oParentObject.w_SELCODCLA) and this.oParentObject.w_SELDATARC=0
            * --- Read from DMPARAM
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.DMPARAM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DMPARAM_idx,2],.t.,this.DMPARAM_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "PAPERVIS,PASOLUTE"+;
                " from "+i_cTable+" DMPARAM where ";
                    +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                    +" and PATIPFIL = "+cp_ToStrODBC("U");
                    +" and PACODUTE = "+cp_ToStrODBC(this.w_CODUTE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                PAPERVIS,PASOLUTE;
                from (i_cTable) where;
                    PACODAZI = this.w_CODAZI;
                    and PATIPFIL = "U";
                    and PACODUTE = this.w_CODUTE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_SELDATARC = NVL(cp_ToDate(_read_.PAPERVIS),cp_NullValue(_read_.PAPERVIS))
              this.w_FLGUTE = NVL(cp_ToDate(_read_.PASOLUTE),cp_NullValue(_read_.PASOLUTE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_ROWS=0
              this.w_TMPL = .F.
              * --- Select from CPUSRGRP
              i_nConn=i_TableProp[this.CPUSRGRP_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.CPUSRGRP_idx,2],.t.,this.CPUSRGRP_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select * from "+i_cTable+" CPUSRGRP ";
                    +" where USERCODE = "+cp_ToStrODBC(this.w_CODUTE)+"";
                     ,"_Curs_CPUSRGRP")
              else
                select * from (i_cTable);
                 where USERCODE = this.w_CODUTE;
                  into cursor _Curs_CPUSRGRP
              endif
              if used('_Curs_CPUSRGRP')
                select _Curs_CPUSRGRP
                locate for 1=1
                do while not(eof())
                this.w_TMPN = nvl(_Curs_CPUSRGRP.GROUPCODE, 0)
                * --- Read from DMPARAM
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.DMPARAM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DMPARAM_idx,2],.t.,this.DMPARAM_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PAPERVIS,PASOLUTE"+;
                    " from "+i_cTable+" DMPARAM where ";
                        +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                        +" and PATIPFIL = "+cp_ToStrODBC("G");
                        +" and PACODGRU = "+cp_ToStrODBC(this.w_TMPN);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PAPERVIS,PASOLUTE;
                    from (i_cTable) where;
                        PACODAZI = this.w_CODAZI;
                        and PATIPFIL = "G";
                        and PACODGRU = this.w_TMPN;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.oParentObject.w_SELDATARC = NVL(cp_ToDate(_read_.PAPERVIS),cp_NullValue(_read_.PAPERVIS))
                  this.w_FLGUTE = NVL(cp_ToDate(_read_.PASOLUTE),cp_NullValue(_read_.PASOLUTE))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if i_Rows>0
                  this.w_TMPL = .T.
                  exit
                endif
                  select _Curs_CPUSRGRP
                  continue
                enddo
                use
              endif
              if not this.w_TMPL
                * --- Read from DMPARAM
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.DMPARAM_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.DMPARAM_idx,2],.t.,this.DMPARAM_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "PAPERVIS,PASOLUTE"+;
                    " from "+i_cTable+" DMPARAM where ";
                        +"PACODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                        +" and PATIPFIL = "+cp_ToStrODBC("D");
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    PAPERVIS,PASOLUTE;
                    from (i_cTable) where;
                        PACODAZI = this.w_CODAZI;
                        and PATIPFIL = "D";
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.oParentObject.w_SELDATARC = NVL(cp_ToDate(_read_.PAPERVIS),cp_NullValue(_read_.PAPERVIS))
                  this.w_FLGUTE = NVL(cp_ToDate(_read_.PASOLUTE),cp_NullValue(_read_.PASOLUTE))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
              endif
            endif
          endif
          * --- Risetta date
          this.oParentObject.w_SELDATARC = IIF(this.oParentObject.w_SELDATARC=0, 1, this.oParentObject.w_SELDATARC)
          this.oParentObject.w_ADATFIN = IIF(this.oParentObject.w_SELDATARC=1, I_FINDAT, i_DATSYS)
          this.oParentObject.w_ADATINI = IIF(this.oParentObject.w_SELDATARC=1, i_INIDAT, this.oParentObject.w_ADATFIN-this.oParentObject.w_SELDATARC)
          this.oParentObject.w_SELUTEARC = IIF(this.w_FLGUTE="S",i_CODUTE,0)
          if this.oParentObject.w_SELUTEARC>0
            * --- Read from CPUSERS
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CPUSERS_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CPUSERS_idx,2],.t.,this.CPUSERS_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "NAME"+;
                " from "+i_cTable+" CPUSERS where ";
                    +"CODE = "+cp_ToStrODBC(this.oParentObject.w_SELUTEARC);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                NAME;
                from (i_cTable) where;
                    CODE = this.oParentObject.w_SELUTEARC;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.oParentObject.w_ADESUTE = NVL(cp_ToDate(_read_.NAME),cp_NullValue(_read_.NAME))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        endif
      endif
       
 Public g_GSUT_BES 
 g_GSUT_BES = this.w_GSUT_BES
      this.oParentObject.w_EXPRFILTRO = g_GSUT_BES
      * --- Cancella tutto quello che � nello zoom
      this.GRID = this.oParentObject.w_ZoomGF.GRD
      * --- Se funziona vai avanti
    endif
    if this.w_Operazione<>"PREFER" and this.w_Operazione<>"INIZIA"
      if empty(this.oParentObject.w_SELCODCLA)
        * --- Se la classe documentale � vuota viene disattivato il check Controlla esistenza dei file archiviati
        this.w_ControllaEsisteFile = "N"
        * --- La query DM01VISU filtra gi� le sole classi per le quali l'utente ha diritto di visualizzazione
        *     
        *     Esegue query di ricerca documenti. Filtri:
        *     - w_Archivio: nome della tabella su cui fare ricerca
        *     - w_CodUte: codice dell'utente che richiede visualizzazione
        if this.w_Operazione<>"INVIOMASSIVO"
          * --- Insert into TMPZ
          i_nConn=i_TableProp[this.TMPZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"DM01VISU",this.TMPZ_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into TMPZ
          i_nConn=i_TableProp[this.TMPZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          if i_nConn<>0
            i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"GSRV6BCH",this.TMPZ_idx)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      else
        * --- Read from PROMCLAS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PROMCLAS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PROMCLAS_idx,2],.t.,this.PROMCLAS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CDDESCLA,CDTIPRAG,CDPATSTD,CDSERVER,CD__PORT,CD__USER,CDPASSWD,CDARCHIVE                    ,CDFLCTRL,CDCLAPRA"+;
            " from "+i_cTable+" PROMCLAS where ";
                +"CDCODCLA = "+cp_ToStrODBC(this.oParentObject.w_SELCODCLA);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CDDESCLA,CDTIPRAG,CDPATSTD,CDSERVER,CD__PORT,CD__USER,CDPASSWD,CDARCHIVE                    ,CDFLCTRL,CDCLAPRA;
            from (i_cTable) where;
                CDCODCLA = this.oParentObject.w_SELCODCLA;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_DESCLA = NVL(cp_ToDate(_read_.CDDESCLA),cp_NullValue(_read_.CDDESCLA))
          this.w_CRITPERI = NVL(cp_ToDate(_read_.CDTIPRAG),cp_NullValue(_read_.CDTIPRAG))
          this.w_LIPATIMG = NVL(cp_ToDate(_read_.CDPATSTD),cp_NullValue(_read_.CDPATSTD))
          this.w_CDSERVER = NVL(cp_ToDate(_read_.CDSERVER),cp_NullValue(_read_.CDSERVER))
          this.w_CD__PORT = NVL(cp_ToDate(_read_.CD__PORT),cp_NullValue(_read_.CD__PORT))
          this.w_CD__USER = NVL(cp_ToDate(_read_.CD__USER),cp_NullValue(_read_.CD__USER))
          this.w_CDPASSWD = NVL(cp_ToDate(_read_.CDPASSWD),cp_NullValue(_read_.CDPASSWD))
          this.w_CDARCHIVE = NVL(cp_ToDate(_read_.CDARCHIVE                    ),cp_NullValue(_read_.CDARCHIVE                    ))
          this.w_ControllaEsisteFile = NVL(cp_ToDate(_read_.CDFLCTRL),cp_NullValue(_read_.CDFLCTRL))
          this.w_CLAPRA = NVL(cp_ToDate(_read_.CDCLAPRA),cp_NullValue(_read_.CDCLAPRA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_LICLADOC = this.oParentObject.w_SELCODCLA
        * --- Insert into TMPZ
        i_nConn=i_TableProp[this.TMPZ_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"DM12VISU",this.TMPZ_idx)
        else
          error "not yet implemented!"
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        * --- Valorizzo la colonna dell'attributo primario 
      endif
      if i_rows = 1
        this.oParentObject.w_ALLPRIUNI = .T.
      endif
      * --- Create temporary table TMPVEND1
      i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
      i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
      declare indexes_FYDUVSPDMJ[3]
      indexes_FYDUVSPDMJ[1]='CDTABKEY'
      indexes_FYDUVSPDMJ[2]='CDCAMCUR'
      indexes_FYDUVSPDMJ[3]='IDVALATT'
      vq_exec('gsuttbes',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,@indexes_FYDUVSPDMJ,.f.)
      this.TMPVEND1_idx=i_nIdx
      i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
      this.w_OLD_TABNAME = "@@@@@@"
      this.w_OLD_CAMPATPRI = "@@@@@@"
      this.w_OLD_VALATTPRI = "@@@@@@"
      this.w_OLD_DESCATTPRI = "@@@@@@"
      * --- Select from GSUT1BES
      do vq_exec with 'GSUT1BES',this,'_Curs_GSUT1BES','',.f.,.t.
      if used('_Curs_GSUT1BES')
        select _Curs_GSUT1BES
        locate for 1=1
        do while not(eof())
        * --- Recupero i campi chiavi della tabella
        this.w_TABNAME = alltrim(nvl(_Curs_GSUT1BES.CDTABKEY," "))
        if this.w_OLD_TABNAME<>this.w_TABNAME
          this.w_KEY_FIELDS = ALLTRIM(cp_KeyToSQL(I_DCX.GetIdxDef(this.w_TABNAME,1)))
        endif
        this.w_CAMPATPRI = alltrim(nvl(_Curs_GSUT1BES.CDCAMCUR," "))
        if at(this.w_CAMPATPRI,this.w_KEY_FIELDS) > 0
          this.w_VALATTPRI = alltrim(nvl(_Curs_GSUT1BES.IDVALATT," "))
          if this.w_OLD_TABNAME<>this.w_TABNAME OR this.w_OLD_VALATTPRI<>this.w_VALATTPRI OR this.w_OLD_CAMPATPRI<>this.w_CAMPATPRI
            * --- Write into TMPVEND1
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVEND1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"CDATTPRI ="+cp_NullLink(cp_ToStrODBC(this.w_VALATTPRI),'TMPVEND1','CDATTPRI');
                  +i_ccchkf ;
              +" where ";
                  +"CDTABKEY = "+cp_ToStrODBC(this.w_TABNAME);
                  +" and CDCAMCUR = "+cp_ToStrODBC(this.w_CAMPATPRI);
                  +" and IDVALATT = "+cp_ToStrODBC(this.w_VALATTPRI);
                     )
            else
              update (i_cTable) set;
                  CDATTPRI = this.w_VALATTPRI;
                  &i_ccchkf. ;
               where;
                  CDTABKEY = this.w_TABNAME;
                  and CDCAMCUR = this.w_CAMPATPRI;
                  and IDVALATT = this.w_VALATTPRI;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        else
          this.w_DESCATTPRI = alltrim(nvl(_Curs_GSUT1BES.IDVALATT," "))
          if this.w_OLD_TABNAME<>this.w_TABNAME OR this.w_OLD_DESCATTPRI<>this.w_DESCATTPRI OR this.w_OLD_CAMPATPRI<>this.w_CAMPATPRI
            * --- Write into TMPVEND1
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.TMPVEND1_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"DESATPRI ="+cp_NullLink(cp_ToStrODBC(this.w_DESCATTPRI),'TMPVEND1','DESATPRI');
                  +i_ccchkf ;
              +" where ";
                  +"CDTABKEY = "+cp_ToStrODBC(this.w_TABNAME);
                  +" and CDCAMCUR = "+cp_ToStrODBC(this.w_CAMPATPRI);
                  +" and IDVALATT = "+cp_ToStrODBC(this.w_DESCATTPRI);
                     )
            else
              update (i_cTable) set;
                  DESATPRI = this.w_DESCATTPRI;
                  &i_ccchkf. ;
               where;
                  CDTABKEY = this.w_TABNAME;
                  and CDCAMCUR = this.w_CAMPATPRI;
                  and IDVALATT = this.w_DESCATTPRI;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        endif
        this.w_OLD_TABNAME = this.w_TABNAME
        this.w_OLD_CAMPATPRI = this.w_CAMPATPRI
        this.w_OLD_VALATTPRI = this.w_VALATTPRI
        this.w_OLD_DESCATTPRI = this.w_DESCATTPRI
        * --- Inserisco il valore e la descrizione dell'attributo primario per l'indice attuale
          select _Curs_GSUT1BES
          continue
        enddo
        use
      endif
      * --- Write into TMPZ
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
      if i_nConn<>0
        local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
        declare i_aIndex[1]
        i_cQueryTable=cp_getTempTableName(i_nConn)
        i_aIndex(1)="GFKEYINDIC"
        do vq_exec with 'gsutubes',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZ_idx,i_nConn)	
        i_cDB=cp_GetDatabaseType(i_nConn)
        do case
        case i_cDB="SQLServer"
          i_cWhere="TMPZ.GFKEYINDIC = _t2.GFKEYINDIC";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CDATTPRI = _t2.CDATTPRI";
            +",DESATPRI = _t2.DESATPRI";
            +i_ccchkf;
            +" from "+i_cTable+" TMPZ, "+i_cQueryTable+" _t2 where "+i_cWhere)
        case i_cDB="MySQL"
          i_cWhere="TMPZ.GFKEYINDIC = _t2.GFKEYINDIC";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPZ, "+i_cQueryTable+" _t2 set ";
            +"TMPZ.CDATTPRI = _t2.CDATTPRI";
            +",TMPZ.DESATPRI = _t2.DESATPRI";
            +Iif(Empty(i_ccchkf),"",",TMPZ.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
            +" where "+i_cWhere)
        case i_cDB="Oracle"
          i_cWhere="TMPZ.GFKEYINDIC = t2.GFKEYINDIC";
          
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPZ set (";
            +"CDATTPRI,";
            +"DESATPRI";
            +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
            +"t2.CDATTPRI,";
            +"t2.DESATPRI";
            +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
            +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
        case i_cDB="PostgreSQL"
          i_cWhere="TMPZ.GFKEYINDIC = _t2.GFKEYINDIC";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPZ set ";
            +"CDATTPRI = _t2.CDATTPRI";
            +",DESATPRI = _t2.DESATPRI";
            +i_ccchkf;
            +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
        otherwise
          i_cWhere=i_cTable+".GFKEYINDIC = "+i_cQueryTable+".GFKEYINDIC";
      
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CDATTPRI = (select CDATTPRI from "+i_cQueryTable+" where "+i_cWhere+")";
            +",DESATPRI = (select DESATPRI from "+i_cQueryTable+" where "+i_cWhere+")";
            +i_ccchkf;
            +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
        endcase
        cp_DropTempTable(i_nConn,i_cQueryTable)
      else
        error "not yet implemented!"
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Drop temporary table TMPVEND1
      i_nIdx=cp_GetTableDefIdx('TMPVEND1')
      if i_nIdx<>0
        cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
        cp_RemoveTableDef('TMPVEND1')
      endif
      * --- Select from TMPZ
      i_nConn=i_TableProp[this.TMPZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2],.t.,this.TMPZ_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select *  from "+i_cTable+" TMPZ ";
            +" order by GFDESCFILE";
             ,"_Curs_TMPZ")
      else
        select * from (i_cTable);
         order by GFDESCFILE;
          into cursor _Curs_TMPZ
      endif
      if used('_Curs_TMPZ')
        select _Curs_TMPZ
        locate for 1=1
        do while not(eof())
        if empty(this.oParentObject.w_SELCODCLA)
          this.w_CRITPERI = nvl(_Curs_TMPZ.CDTIPRAG, " ")
          this.w_LIPATIMG = nvl(_Curs_TMPZ.CDPATSTD, " ")
        endif
        this.w_PERIODO = _Curs_TMPZ.GFPERIODO
        this.w_IDMODALL = nvl(_Curs_TMPZ.IDMODALL, "F")
        this.w_PATHABS = nvl(_Curs_TMPZ.GFPATHFILE, "  ")
        this.w_IDSERIAL = nvl(_Curs_TMPZ.GFKEYINDIC, " ")
        this.w_LICLADOC = nvl(_Curs_TMPZ.GFCLASDOCU, " ")
        this.w_NOMEFILE = rtrim(nvl(_Curs_TMPZ.GFNOMEFILE," "))
        this.w_IDOGGETT = nvl(_Curs_TMPZ.GFDESCFILE, " ")
        this.w_IDORIFIL = iif(this.w_IDMODALL="L",nvl(_Curs_TMPZ.IDORIFIL,""),"")
        this.w_IDALLPRI = nvl(_Curs_TMPZ.IDALLPRI, "  ")
        if this.w_IDALLPRI = "S"
          this.oParentObject.w_ALLPRIUNI = .T.
        endif
        this.w_IDUTEEXP = NVL(_Curs_TMPZ.IDUTEEXP,0)
        this.w_IDWEBAPPL = nvl(_Curs_TMPZ.IDWEBAPP, " ") + NVL(_Curs_TMPZ.IDMODALL," ")
        this.w_IMGPAT = ADDBS(justpath(NVL(_Curs_TMPZ.GFORIGFILE," ")))
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
          select _Curs_TMPZ
          continue
        enddo
        use
      endif
       
 Release g_GSUT_BES
      if this.w_Operazione="QUERY2"
        this.oparentobject.opgfrm.activepage=1
      endif
      * --- Fase 3 - Refresh Zoom ====================================================================================================
      if !(InList(this.w_Operazione, "INIZIA", "INVIOMASSIVO"))
        this.oParentObject.NotifyEvent("Carica")
        * --- Eseguo l'operazione definita per l'allegato principale solo se esiste un relativo programma di gestione 
        this.w_OPLOGICO = iif(type(".cFunction")="C",.cFunction="Query" ,.t.)
        if this.oParentObject.w_ALLPRIUNI and this.w_OPLOGICO and (vartype(this.w_ALLOK) = "L" or (vartype(this.w_ALLOK) = "C" and this.w_ALLOK # "F"))
          * --- Gestisco l'operazione associato all'allegato, definita nella funzionalit�: 'Servi Fax, email...' 
          this.oParentObject.NotifyEvent("GestOp")
        endif
      endif
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Distingue i due casi .... collegamento o copia file ...
    do case
      case this.w_IDMODALL=="F"
        * --- Gestione del periodo (determina percorsi...) => Determina w_ImgPat
        * --- Verifico se � presente il percorso assoluto di archiviazione dell'allegato
        do case
          case LEFT(this.w_IDWEBAPPL,1)#"S" AND RIGHT(this.w_IDWEBAPPL,1)#"I" 
 
            * --- Ricalcolo il percorso solo se l'archiaviazione non � di tipo WEB
            if not empty(this.w_PATHABS)
              this.w_IMGPAT = alltrim(this.w_PATHABS)
            else
              this.w_IMGPAT = DCMPATAR(this.w_LIPATIMG, this.w_CRITPERI, this.w_PERIODO, this.w_LICLADOC)
            endif
          case Empty(justdrive(this.w_IMGPAT))
            this.w_IMGPAT = ADDBS(justpath(nvl(_Curs_TMPZ.IDORIFIL," ")))
        endcase
      case this.w_IDMODALL=="L"
        * --- Nel caso di collegamento il percorso � quello in ORIFIL
        this.w_IMGPAT = LEFT(this.w_IDORIFIL, RAT("\", this.w_IDORIFIL))
    endcase
    * --- Acquisizione della directory corrente - PARTE COMUNE
    if (this.w_ControllaEsisteFile="S" AND cp_fileexist(iif(this.w_IDMODALL=="I",this.w_IMGPAT,this.w_IMGPAT+this.w_NOMEFILE))) OR this.w_ControllaEsisteFile="N" OR INLIST(this.w_IDMODALL,"E","I")
      if empty(this.w_IDOGGETT)
        * --- Passa w_CHIAVERICERCA e determina w_TIPOFILE
        this.w_posizpunto = rat(".",this.w_NOMEFILE)
        this.w_ChiaveRicerca = alltrim(lower(substr(this.w_NOMEFILE,this.w_posizpunto)))
        this.w_TIPOFILE = GETEXTDESC(this.w_ChiaveRicerca)
      else
        this.w_TIPOFILE = this.w_IDOGGETT
      endif
      * --- Aggiorno i campi relativi al Path e alla descrizione del file (allegato)
      * --- Write into TMPZ
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.TMPZ_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZ_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"GFPATHFILE ="+cp_NullLink(cp_ToStrODBC(this.w_IMGPAT),'TMPZ','GFPATHFILE');
        +",GFDESCFILE ="+cp_NullLink(cp_ToStrODBC(this.w_TIPOFILE),'TMPZ','GFDESCFILE');
        +",ISFILE ="+cp_NullLink(cp_ToStrODBC(IIF(this.w_IDUTEEXP<>0, "E", "S")),'TMPZ','ISFILE');
            +i_ccchkf ;
        +" where ";
            +"GFKEYINDIC = "+cp_ToStrODBC(this.w_IDSERIAL);
               )
      else
        update (i_cTable) set;
            GFPATHFILE = this.w_IMGPAT;
            ,GFDESCFILE = this.w_TIPOFILE;
            ,ISFILE = IIF(this.w_IDUTEEXP<>0, "E", "S");
            &i_ccchkf. ;
         where;
            GFKEYINDIC = this.w_IDSERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,w_Archivio,w_Chiave,w_Operazione,w_ALLOK)
    this.w_Archivio=w_Archivio
    this.w_Chiave=w_Chiave
    this.w_Operazione=w_Operazione
    this.w_ALLOK=w_ALLOK
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='PROMCLAS'
    this.cWorkTables[2]='PRO_PVIS'
    this.cWorkTables[3]='CPUSERS'
    this.cWorkTables[4]='CPUSRGRP'
    this.cWorkTables[5]='PROMINDI'
    this.cWorkTables[6]='DMPARAM'
    this.cWorkTables[7]='TMPZ'
    this.cWorkTables[8]='CAN_TIER'
    this.cWorkTables[9]='*TMPVEND1'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_CPUSRGRP')
      use in _Curs_CPUSRGRP
    endif
    if used('_Curs_CPUSRGRP')
      use in _Curs_CPUSRGRP
    endif
    if used('_Curs_GSUT1BES')
      use in _Curs_GSUT1BES
    endif
    if used('_Curs_TMPZ')
      use in _Curs_TMPZ
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_Archivio,w_Chiave,w_Operazione,w_ALLOK"
endproc
