* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_mpv                                                        *
*              Movimenti di provvigioni                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_58]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-15                                                      *
* Last revis.: 2015-10-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_mpv"))

* --- Class definition
define class tgsve_mpv as StdTrsForm
  Top    = 0
  Left   = 4

  * --- Standard Properties
  Width  = 868
  Height = 395+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-15"
  HelpContextID=147468905
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=55

  * --- Master/Detail Properties
  cTrsName=''

  * --- Constant Properties
  MOP_MAST_IDX = 0
  MOP_DETT_IDX = 0
  AGENTI_IDX = 0
  DOC_MAST_IDX = 0
  ESERCIZI_IDX = 0
  CONTI_IDX = 0
  VALUTE_IDX = 0
  cFile = "MOP_MAST"
  cFileDetail = "MOP_DETT"
  cKeySelect = "MPSERIAL"
  cKeyWhere  = "MPSERIAL=this.w_MPSERIAL"
  cKeyDetail  = "MPSERIAL=this.w_MPSERIAL"
  cKeyWhereODBC = '"MPSERIAL="+cp_ToStrODBC(this.w_MPSERIAL)';

  cKeyDetailWhereODBC = '"MPSERIAL="+cp_ToStrODBC(this.w_MPSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'
  cKeyWhereODBCqualified = '"MOP_DETT.MPSERIAL="+cp_ToStrODBC(this.w_MPSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'MOP_DETT.CPROWNUM '
  cPrg = "gsve_mpv"
  cComment = "Movimenti di provvigioni"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_MPSERIAL = space(10)
  w_CODAZE = space(5)
  w_MPNUMREG = 0
  w_MPCODESE = space(4)
  w_MPDATREG = ctod('  /  /  ')
  o_MPDATREG = ctod('  /  /  ')
  w_MPVALNAZ = space(3)
  w_MPNUMDOC = 0
  w_MPALFDOC = space(10)
  w_MPDATDOC = ctod('  /  /  ')
  o_MPDATDOC = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_MPCODVAL = space(3)
  o_MPCODVAL = space(3)
  w_SIMVAL = space(5)
  w_CAOVAL = 0
  w_MPCAOVAL = 0
  w_CALCPICT = 0
  w_DECTOT = 0
  w_MPTIPCON = space(1)
  w_MPCODCON = space(15)
  w_MPCODAGE = space(5)
  o_MPCODAGE = space(5)
  w_DESCAGE = space(35)
  w_CAPZON = space(5)
  o_CAPZON = space(5)
  w_TIPAGEN = space(1)
  w_MPCODCAP = space(5)
  w_DESCAPZ = space(35)
  w_MPDESSUP = space(50)
  w_DESCRI = space(40)
  w_DATOBSO = ctod('  /  /  ')
  w_MPRIFDOC = space(10)
  w_FLVEAC = space(1)
  w_CLADOC = space(2)
  w_DESAPP = space(35)
  w_TIPOAG = space(1)
  w_MPDATSCA = ctod('  /  /  ')
  w_MPTOTIMP = 0
  o_MPTOTIMP = 0
  w_MPPERPRA = 0
  o_MPPERPRA = 0
  w_MPTOTAGE = 0
  w_MPTOTIM2 = 0
  o_MPTOTIM2 = 0
  w_MPPERPRC = 0
  o_MPPERPRC = 0
  w_MPTOTZON = 0
  w_MPTIPMAT = space(2)
  w_MPDATMAT = ctod('  /  /  ')
  w_MPDATLIQ = ctod('  /  /  ')
  w_MPFLSOSP = space(1)
  w_TOTIMP = 0
  w_TOTAGE = 0
  w_TOTAREA = 0
  w_DTOBSO = ctod('  /  /  ')
  w_MPAGSOSP = space(1)
  w_DATOBS = ctod('  /  /  ')
  w_DATOBS1 = ctod('  /  /  ')
  w_TOTIMPC = 0
  w_UTCC = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_UTCV = 0

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_MPSERIAL = this.W_MPSERIAL
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_MPCODESE = this.W_MPCODESE
  op_MPNUMREG = this.W_MPNUMREG
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOP_MAST','gsve_mpv')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_mpvPag1","gsve_mpv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Movimenti di provvigione")
      .Pages(1).HelpContextID = 199497470
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='AGENTI'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='ESERCIZI'
    this.cWorkTables[4]='CONTI'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='MOP_MAST'
    this.cWorkTables[7]='MOP_DETT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(7))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOP_MAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOP_MAST_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_MPSERIAL = NVL(MPSERIAL,space(10))
    endwith
  endproc

  * --- Reading record and initializing Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn,i_cTF,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_11_joined
    link_1_11_joined=.f.
    local link_1_18_joined
    link_1_18_joined=.f.
    local link_1_19_joined
    link_1_19_joined=.f.
    local link_1_23_joined
    link_1_23_joined=.f.
    local link_1_41_joined
    link_1_41_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading record from Master table
    *
    * select * from MOP_MAST where MPSERIAL=KeySet.MPSERIAL
    *
    i_nConn = i_TableProp[this.MOP_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOP_MAST_IDX,2],this.bLoadRecFilter,this.MOP_MAST_IDX,"gsve_mpv")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOP_MAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOP_MAST.*"
      i_cKey = Strtran(this.cKeyWhereODBCqualified,"MOP_DETT.","MOP_MAST.")
      i_cTable = i_cTable+' MOP_MAST '
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_18_joined=this.AddJoinedLink_1_18(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_19_joined=this.AddJoinedLink_1_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_23_joined=this.AddJoinedLink_1_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_41_joined=this.AddJoinedLink_1_41(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'MPSERIAL',this.w_MPSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    i_cTF = this.cCursorTrs
    if this.bLoaded
      with this
        .w_CODAZE = i_CODAZI
        .w_CAOVAL = 0
        .w_DECTOT = 0
        .w_DESCAGE = space(35)
        .w_CAPZON = space(5)
        .w_TIPAGEN = space(1)
        .w_DESCAPZ = space(35)
        .w_DESCRI = space(40)
        .w_DATOBSO = ctod("  /  /  ")
        .w_FLVEAC = space(1)
        .w_CLADOC = space(2)
        .w_DESAPP = space(35)
        .w_TIPOAG = 'C'
        .w_TOTIMP = 0
        .w_TOTAGE = 0
        .w_TOTAREA = 0
        .w_DTOBSO = ctod("  /  /  ")
        .w_DATOBS = ctod("  /  /  ")
        .w_DATOBS1 = ctod("  /  /  ")
        .w_TOTIMPC = 0
        .w_MPSERIAL = NVL(MPSERIAL,space(10))
        .op_MPSERIAL = .w_MPSERIAL
        .w_MPNUMREG = NVL(MPNUMREG,0)
        .op_MPNUMREG = .w_MPNUMREG
        .w_MPCODESE = NVL(MPCODESE,space(4))
        .op_MPCODESE = .w_MPCODESE
          * evitabile
          *.link_1_4('Load')
        .w_MPDATREG = NVL(cp_ToDate(MPDATREG),ctod("  /  /  "))
        .w_MPVALNAZ = NVL(MPVALNAZ,space(3))
        .w_MPNUMDOC = NVL(MPNUMDOC,0)
        .w_MPALFDOC = NVL(MPALFDOC,space(10))
        .w_MPDATDOC = NVL(cp_ToDate(MPDATDOC),ctod("  /  /  "))
        .w_OBTEST = .w_MPDATDOC
        .w_MPCODVAL = NVL(MPCODVAL,space(3))
          if link_1_11_joined
            this.w_MPCODVAL = NVL(VACODVAL111,NVL(this.w_MPCODVAL,space(3)))
            this.w_DESAPP = NVL(VADESVAL111,space(35))
            this.w_CAOVAL = NVL(VACAOVAL111,0)
            this.w_DECTOT = NVL(VADECTOT111,0)
            this.w_DTOBSO = NVL(cp_ToDate(VADTOBSO111),ctod("  /  /  "))
            this.w_SIMVAL = NVL(VASIMVAL111,space(5))
          else
          .link_1_11('Load')
          endif
        .w_SIMVAL = IIF(EMPTY(.w_MPCODVAL),' ',.w_SIMVAL)
        .w_MPCAOVAL = NVL(MPCAOVAL,0)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_MPTIPCON = NVL(MPTIPCON,space(1))
        .w_MPCODCON = NVL(MPCODCON,space(15))
          if link_1_18_joined
            this.w_MPCODCON = NVL(ANCODICE118,NVL(this.w_MPCODCON,space(15)))
            this.w_DESCRI = NVL(ANDESCRI118,space(40))
            this.w_DATOBS = NVL(cp_ToDate(ANDTOBSO118),ctod("  /  /  "))
          else
          .link_1_18('Load')
          endif
        .w_MPCODAGE = NVL(MPCODAGE,space(5))
          if link_1_19_joined
            this.w_MPCODAGE = NVL(AGCODAGE119,NVL(this.w_MPCODAGE,space(5)))
            this.w_DESCAGE = NVL(AGDESAGE119,space(35))
            this.w_TIPAGEN = NVL(AGTIPAGE119,space(1))
            this.w_CAPZON = NVL(AGCZOAGE119,space(5))
            this.w_DATOBSO = NVL(cp_ToDate(AGDTOBSO119),ctod("  /  /  "))
          else
          .link_1_19('Load')
          endif
        .w_MPCODCAP = NVL(MPCODCAP,space(5))
          if link_1_23_joined
            this.w_MPCODCAP = NVL(AGCODAGE123,NVL(this.w_MPCODCAP,space(5)))
            this.w_DESCAPZ = NVL(AGDESAGE123,space(35))
            this.w_DATOBS1 = NVL(cp_ToDate(AGDTOBSO123),ctod("  /  /  "))
            this.w_TIPOAG = NVL(AGTIPAGE123,space(1))
          else
          .link_1_23('Load')
          endif
        .w_MPDESSUP = NVL(MPDESSUP,space(50))
        .w_MPRIFDOC = NVL(MPRIFDOC,space(10))
          if link_1_41_joined
            this.w_MPRIFDOC = NVL(MVSERIAL141,NVL(this.w_MPRIFDOC,space(10)))
            this.w_FLVEAC = NVL(MVFLVEAC141,space(1))
            this.w_CLADOC = NVL(MVCLADOC141,space(2))
          else
          .link_1_41('Load')
          endif
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .w_MPAGSOSP = NVL(MPAGSOSP,space(1))
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'MOP_MAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      * --- Select reading records from Detail table
      *
      * select * from MOP_DETT where MPSERIAL=KeySet.MPSERIAL
      *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gsve_mpv
      * --- Setta order per Data di Maturazione
      i_cOrder = "order by MPDATSCA"
      * --- Fine Area Manuale
      If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
      endif
      i_nConn = i_TableProp[this.MOP_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOP_DETT_IDX,2])
      if i_nConn<>0
        i_nFlds = i_dcx.getfieldscount('MOP_DETT')
        i_cDatabaseType = cp_GetDatabaseType(i_nConn)
        i_cSel = "MOP_DETT.* "
        i_cKey = this.cKeyWhereODBCqualified
        i_cTable = i_cTable+" MOP_DETT"
        =cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursorTrs)
      else
        *i_cKey = this.cKeyWhere
        i_cKey = cp_PKFox(i_cTable  ,'MPSERIAL',this.w_MPSERIAL  )
        select * from (i_cTable) MOP_DETT where &i_cKey &i_cOrder into cursor (this.cCursorTrs) nofilter
      endif
      this.i_nRowNum = 0
      this.w_TOTIMP = 0
      this.w_TOTAGE = 0
      this.w_TOTAREA = 0
      this.w_TOTIMPC = 0
      scan
        with this
          .w_CPROWNUM = CPROWNUM
          .w_MPDATSCA = NVL(cp_ToDate(MPDATSCA),ctod("  /  /  "))
          .w_MPTOTIMP = NVL(MPTOTIMP,0)
          .w_MPPERPRA = NVL(MPPERPRA,0)
          .w_MPTOTAGE = NVL(MPTOTAGE,0)
          .w_MPTOTIM2 = NVL(MPTOTIM2,0)
          .w_MPPERPRC = NVL(MPPERPRC,0)
          .w_MPTOTZON = NVL(MPTOTZON,0)
          .w_MPTIPMAT = NVL(MPTIPMAT,space(2))
          .w_MPDATMAT = NVL(cp_ToDate(MPDATMAT),ctod("  /  /  "))
          .w_MPDATLIQ = NVL(cp_ToDate(MPDATLIQ),ctod("  /  /  "))
          .w_MPFLSOSP = NVL(MPFLSOSP,space(1))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTIMP = .w_TOTIMP+.w_MPTOTIMP
          .w_TOTAGE = .w_TOTAGE+.w_MPTOTAGE
          .w_TOTAREA = .w_TOTAREA+.w_MPTOTZON
          .w_TOTIMPC = .w_TOTIMPC+.w_MPTOTIM2
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursorTrs)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cTrsName)
      if reccount()=0
        this.InitRow()
      endif
      with this
        .w_OBTEST = .w_MPDATDOC
        .w_SIMVAL = IIF(EMPTY(.w_MPCODVAL),' ',.w_SIMVAL)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_1_40.enabled = .oPgFrm.Page1.oPag.oBtn_1_40.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc


  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_MPSERIAL=space(10)
      .w_CODAZE=space(5)
      .w_MPNUMREG=0
      .w_MPCODESE=space(4)
      .w_MPDATREG=ctod("  /  /  ")
      .w_MPVALNAZ=space(3)
      .w_MPNUMDOC=0
      .w_MPALFDOC=space(10)
      .w_MPDATDOC=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_MPCODVAL=space(3)
      .w_SIMVAL=space(5)
      .w_CAOVAL=0
      .w_MPCAOVAL=0
      .w_CALCPICT=0
      .w_DECTOT=0
      .w_MPTIPCON=space(1)
      .w_MPCODCON=space(15)
      .w_MPCODAGE=space(5)
      .w_DESCAGE=space(35)
      .w_CAPZON=space(5)
      .w_TIPAGEN=space(1)
      .w_MPCODCAP=space(5)
      .w_DESCAPZ=space(35)
      .w_MPDESSUP=space(50)
      .w_DESCRI=space(40)
      .w_DATOBSO=ctod("  /  /  ")
      .w_MPRIFDOC=space(10)
      .w_FLVEAC=space(1)
      .w_CLADOC=space(2)
      .w_DESAPP=space(35)
      .w_TIPOAG=space(1)
      .w_MPDATSCA=ctod("  /  /  ")
      .w_MPTOTIMP=0
      .w_MPPERPRA=0
      .w_MPTOTAGE=0
      .w_MPTOTIM2=0
      .w_MPPERPRC=0
      .w_MPTOTZON=0
      .w_MPTIPMAT=space(2)
      .w_MPDATMAT=ctod("  /  /  ")
      .w_MPDATLIQ=ctod("  /  /  ")
      .w_MPFLSOSP=space(1)
      .w_TOTIMP=0
      .w_TOTAGE=0
      .w_TOTAREA=0
      .w_DTOBSO=ctod("  /  /  ")
      .w_MPAGSOSP=space(1)
      .w_DATOBS=ctod("  /  /  ")
      .w_DATOBS1=ctod("  /  /  ")
      .w_TOTIMPC=0
      .w_UTCC=0
      .w_UTDC=ctot("")
      .w_UTDV=ctot("")
      .w_UTCV=0
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
        .w_CODAZE = i_CODAZI
        .DoRTCalc(3,3,.f.)
        .w_MPCODESE = STR(YEAR(i_DATSYS),4,0)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_MPCODESE))
         .link_1_4('Full')
        endif
        .w_MPDATREG = i_datsys
        .w_MPVALNAZ = IIF(EMPTY(.w_MPVALNAZ), g_PERVAL, .w_MPVALNAZ)
        .DoRTCalc(7,8,.f.)
        .w_MPDATDOC = i_datsys
        .w_OBTEST = .w_MPDATDOC
        .w_MPCODVAL = g_perval
        .DoRTCalc(11,11,.f.)
        if not(empty(.w_MPCODVAL))
         .link_1_11('Full')
        endif
        .w_SIMVAL = IIF(EMPTY(.w_MPCODVAL),' ',.w_SIMVAL)
        .DoRTCalc(13,13,.f.)
        .w_MPCAOVAL = GETCAM(.w_MPCODVAL, IIF(EMPTY(.w_MPDATDOC), .w_MPDATREG, .w_MPDATDOC),0)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .DoRTCalc(16,16,.f.)
        .w_MPTIPCON = 'C'
        .DoRTCalc(18,18,.f.)
        if not(empty(.w_MPCODCON))
         .link_1_18('Full')
        endif
        .DoRTCalc(19,19,.f.)
        if not(empty(.w_MPCODAGE))
         .link_1_19('Full')
        endif
        .DoRTCalc(20,22,.f.)
        .w_MPCODCAP = .w_CAPZON
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_MPCODCAP))
         .link_1_23('Full')
        endif
        .DoRTCalc(24,28,.f.)
        if not(empty(.w_MPRIFDOC))
         .link_1_41('Full')
        endif
        .DoRTCalc(29,31,.f.)
        .w_TIPOAG = 'C'
        .DoRTCalc(33,35,.f.)
        .w_MPTOTAGE = cp_ROUND(.w_MPTOTIMP * .w_MPPERPRA/100,.w_DECTOT)
        .w_MPTOTIM2 = IIF(NOT EMPTY(.w_MPCODCAP),IIF(.w_MPTOTIMP<>0, .w_MPTOTIMP, .w_MPTOTIM2),0)
        .DoRTCalc(38,38,.f.)
        .w_MPTOTZON = cp_ROUND(.w_MPTOTIM2 * .w_MPPERPRC/100,.w_DECTOT)
        .w_MPTIPMAT = 'FA'
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .DoRTCalc(41,47,.f.)
        .w_MPAGSOSP = 'C'
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOP_MAST')
    this.DoRTCalc(49,55,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oMPNUMREG_1_3.enabled = i_bVal
      .Page1.oPag.oMPCODESE_1_4.enabled = i_bVal
      .Page1.oPag.oMPDATREG_1_5.enabled = i_bVal
      .Page1.oPag.oMPNUMDOC_1_7.enabled = i_bVal
      .Page1.oPag.oMPALFDOC_1_8.enabled = i_bVal
      .Page1.oPag.oMPDATDOC_1_9.enabled = i_bVal
      .Page1.oPag.oMPCODVAL_1_11.enabled = i_bVal
      .Page1.oPag.oMPCAOVAL_1_14.enabled = i_bVal
      .Page1.oPag.oMPCODCON_1_18.enabled = i_bVal
      .Page1.oPag.oMPCODAGE_1_19.enabled = i_bVal
      .Page1.oPag.oMPCODCAP_1_23.enabled = i_bVal
      .Page1.oPag.oMPDESSUP_1_25.enabled = i_bVal
      .Page1.oPag.oMPAGSOSP_1_50.enabled = i_bVal
      .Page1.oPag.oBtn_1_40.enabled = .Page1.oPag.oBtn_1_40.mCond()
      .Page1.oPag.oObj_1_46.enabled = i_bVal
      .Page1.oPag.oObj_1_53.enabled = i_bVal
      .Page1.oPag.oObj_1_54.enabled = i_bVal
      .Page1.oPag.oObj_1_55.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oMPNUMREG_1_3.enabled = .t.
        .Page1.oPag.oMPDATREG_1_5.enabled = .t.
        .Page1.oPag.oMPNUMDOC_1_7.enabled = .t.
        .Page1.oPag.oMPDATDOC_1_9.enabled = .t.
        .Page1.oPag.oMPCODCON_1_18.enabled = .t.
        .Page1.oPag.oMPCODAGE_1_19.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'MOP_MAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOP_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOP_MAST_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEMOP","i_codazi,w_MPSERIAL")
      cp_AskTableProg(this,i_nConn,"PRMOP","i_codazi,w_MPCODESE,w_MPNUMREG")
      .op_codazi = .w_codazi
      .op_MPSERIAL = .w_MPSERIAL
      .op_codazi = .w_codazi
      .op_MPCODESE = .w_MPCODESE
      .op_MPNUMREG = .w_MPNUMREG
    endwith
    this.SetControlsValue()
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOP_MAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPSERIAL,"MPSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPNUMREG,"MPNUMREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPCODESE,"MPCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPDATREG,"MPDATREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPVALNAZ,"MPVALNAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPNUMDOC,"MPNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPALFDOC,"MPALFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPDATDOC,"MPDATDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPCODVAL,"MPCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPCAOVAL,"MPCAOVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPTIPCON,"MPTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPCODCON,"MPCODCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPCODAGE,"MPCODAGE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPCODCAP,"MPCODCAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPDESSUP,"MPDESSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPRIFDOC,"MPRIFDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_MPAGSOSP,"MPAGSOSP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor only with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOP_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOP_MAST_IDX,2])
    i_lTable = "MOP_MAST"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOP_MAST_IDX,5],6]
    if i_nConn<>0
     LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_MPDATSCA D(8);
      ,t_MPTOTIMP N(18,4);
      ,t_MPPERPRA N(5,2);
      ,t_MPTOTAGE N(18,4);
      ,t_MPTOTIM2 N(18,4);
      ,t_MPPERPRC N(5,2);
      ,t_MPTOTZON N(18,4);
      ,t_MPTIPMAT N(3);
      ,t_MPDATMAT D(8);
      ,t_MPDATLIQ D(8);
      ,t_MPFLSOSP N(3);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsve_mpvbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPDATSCA_2_1.controlsource=this.cTrsName+'.t_MPDATSCA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPTOTIMP_2_2.controlsource=this.cTrsName+'.t_MPTOTIMP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPPERPRA_2_3.controlsource=this.cTrsName+'.t_MPPERPRA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPTOTAGE_2_4.controlsource=this.cTrsName+'.t_MPTOTAGE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPTOTIM2_2_5.controlsource=this.cTrsName+'.t_MPTOTIM2'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPPERPRC_2_6.controlsource=this.cTrsName+'.t_MPPERPRC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPTOTZON_2_7.controlsource=this.cTrsName+'.t_MPTOTZON'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPTIPMAT_2_8.controlsource=this.cTrsName+'.t_MPTIPMAT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPDATMAT_2_9.controlsource=this.cTrsName+'.t_MPDATMAT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPDATLIQ_2_10.controlsource=this.cTrsName+'.t_MPDATLIQ'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oMPFLSOSP_2_11.controlsource=this.cTrsName+'.t_MPFLSOSP'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(81)
    this.AddVLine(185)
    this.AddVLine(242)
    this.AddVLine(331)
    this.AddVLine(434)
    this.AddVLine(489)
    this.AddVLine(577)
    this.AddVLine(671)
    this.AddVLine(749)
    this.AddVLine(826)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPDATSCA_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  * --- Insert new Record in Master table
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_nnn
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOP_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOP_MAST_IDX,2])
      with this
          cp_NextTableProg(this,i_nConn,"SEMOP","i_codazi,w_MPSERIAL")
          cp_NextTableProg(this,i_nConn,"PRMOP","i_codazi,w_MPCODESE,w_MPNUMREG")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MOP_MAST
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOP_MAST')
        i_extval=cp_InsertValODBCExtFlds(this,'MOP_MAST')
        local i_cFld
        i_cFld=" "+;
                  "(MPSERIAL,MPNUMREG,MPCODESE,MPDATREG,MPVALNAZ"+;
                  ",MPNUMDOC,MPALFDOC,MPDATDOC,MPCODVAL,MPCAOVAL"+;
                  ",MPTIPCON,MPCODCON,MPCODAGE,MPCODCAP,MPDESSUP"+;
                  ",MPRIFDOC,MPAGSOSP,UTCC,UTDC,UTDV"+;
                  ",UTCV"+i_extfld+",CPCCCHK) "
        i_nnn="INSERT INTO "+i_cTable+i_cFld+" VALUES ("+;
                    cp_ToStrODBC(this.w_MPSERIAL)+;
                    ","+cp_ToStrODBC(this.w_MPNUMREG)+;
                    ","+cp_ToStrODBCNull(this.w_MPCODESE)+;
                    ","+cp_ToStrODBC(this.w_MPDATREG)+;
                    ","+cp_ToStrODBC(this.w_MPVALNAZ)+;
                    ","+cp_ToStrODBC(this.w_MPNUMDOC)+;
                    ","+cp_ToStrODBC(this.w_MPALFDOC)+;
                    ","+cp_ToStrODBC(this.w_MPDATDOC)+;
                    ","+cp_ToStrODBCNull(this.w_MPCODVAL)+;
                    ","+cp_ToStrODBC(this.w_MPCAOVAL)+;
                    ","+cp_ToStrODBC(this.w_MPTIPCON)+;
                    ","+cp_ToStrODBCNull(this.w_MPCODCON)+;
                    ","+cp_ToStrODBCNull(this.w_MPCODAGE)+;
                    ","+cp_ToStrODBCNull(this.w_MPCODCAP)+;
                    ","+cp_ToStrODBC(this.w_MPDESSUP)+;
                    ","+cp_ToStrODBCNull(this.w_MPRIFDOC)+;
                    ","+cp_ToStrODBC(this.w_MPAGSOSP)+;
                    ","+cp_ToStrODBC(this.w_UTCC)+;
                    ","+cp_ToStrODBC(this.w_UTDC)+;
                    ","+cp_ToStrODBC(this.w_UTDV)+;
                    ","+cp_ToStrODBC(this.w_UTCV)+;
              i_extval+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn, i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOP_MAST')
        i_extval=cp_InsertValVFPExtFlds(this,'MOP_MAST')
        cp_CheckDeletedKey(i_cTable,0,'MPSERIAL',this.w_MPSERIAL)
        INSERT INTO (i_cTable);
              (MPSERIAL,MPNUMREG,MPCODESE,MPDATREG,MPVALNAZ,MPNUMDOC,MPALFDOC,MPDATDOC,MPCODVAL,MPCAOVAL,MPTIPCON,MPCODCON,MPCODAGE,MPCODCAP,MPDESSUP,MPRIFDOC,MPAGSOSP,UTCC,UTDC,UTDV,UTCV &i_extfld. ,CPCCCHK) VALUES (;
                  this.w_MPSERIAL;
                  ,this.w_MPNUMREG;
                  ,this.w_MPCODESE;
                  ,this.w_MPDATREG;
                  ,this.w_MPVALNAZ;
                  ,this.w_MPNUMDOC;
                  ,this.w_MPALFDOC;
                  ,this.w_MPDATDOC;
                  ,this.w_MPCODVAL;
                  ,this.w_MPCAOVAL;
                  ,this.w_MPTIPCON;
                  ,this.w_MPCODCON;
                  ,this.w_MPCODAGE;
                  ,this.w_MPCODCAP;
                  ,this.w_MPDESSUP;
                  ,this.w_MPRIFDOC;
                  ,this.w_MPAGSOSP;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_UTCV;
        &i_extval. ,cp_NewCCChk())
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOP_DETT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOP_DETT_IDX,2])
      *
      * insert into MOP_DETT
      *
      this.NotifyEvent('Insert row start')
      i_TN = this.cTrsName
      local i_cFldBody,i_cFldValBody
      i_cFldBody=" "+;
                  "(MPSERIAL,MPDATSCA,MPTOTIMP,MPPERPRA,MPTOTAGE"+;
                  ",MPTOTIM2,MPPERPRC,MPTOTZON,MPTIPMAT,MPDATMAT"+;
                  ",MPDATLIQ,MPFLSOSP,CPROWNUM,CPCCCHK)"
      if i_nConn<>0
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_MPSERIAL)+","+cp_ToStrODBC(this.w_MPDATSCA)+","+cp_ToStrODBC(this.w_MPTOTIMP)+","+cp_ToStrODBC(this.w_MPPERPRA)+","+cp_ToStrODBC(this.w_MPTOTAGE)+;
             ","+cp_ToStrODBC(this.w_MPTOTIM2)+","+cp_ToStrODBC(this.w_MPPERPRC)+","+cp_ToStrODBC(this.w_MPTOTZON)+","+cp_ToStrODBC(this.w_MPTIPMAT)+","+cp_ToStrODBC(this.w_MPDATMAT)+;
             ","+cp_ToStrODBC(this.w_MPDATLIQ)+","+cp_ToStrODBC(this.w_MPFLSOSP)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody )
      else
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'MPSERIAL',this.w_MPSERIAL)
        local i_cFldValBodyFox
        i_cFldValBodyFox=" "+;
                "(this.w_MPSERIAL,this.w_MPDATSCA,this.w_MPTOTIMP,this.w_MPPERPRA,this.w_MPTOTAGE"+;
                ",this.w_MPTOTIM2,this.w_MPPERPRC,this.w_MPTOTZON,this.w_MPTIPMAT,this.w_MPDATMAT"+;
                ",this.w_MPDATLIQ,this.w_MPFLSOSP,i_nCntLine,cp_NewCCChk())"
        INSERT INTO (i_cTable) &i_cFldBody VALUES &i_cFldValBodyFox
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_nnn,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.MOP_MAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOP_MAST_IDX,2])
      i_nModRow = 1
      if this.bHeaderUpdated and i_bEditing
        this.mRestoreTrs(.t.)
        this.mUpdateTrs(.t.)
        i_NF = this.cCursor
        i_OldCCCHK = iif(i_bEditing,&i_NF..CPCCCHK,'')
        *
        * update MOP_MAST
        *
        this.NotifyEvent('Update start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_extfld=cp_ReplaceODBCExtFlds(this,'MOP_MAST')
          i_nnn="UPDATE "+i_cTable+" SET "+;
             " MPNUMREG="+cp_ToStrODBC(this.w_MPNUMREG)+;
             ",MPCODESE="+cp_ToStrODBCNull(this.w_MPCODESE)+;
             ",MPDATREG="+cp_ToStrODBC(this.w_MPDATREG)+;
             ",MPVALNAZ="+cp_ToStrODBC(this.w_MPVALNAZ)+;
             ",MPNUMDOC="+cp_ToStrODBC(this.w_MPNUMDOC)+;
             ",MPALFDOC="+cp_ToStrODBC(this.w_MPALFDOC)+;
             ",MPDATDOC="+cp_ToStrODBC(this.w_MPDATDOC)+;
             ",MPCODVAL="+cp_ToStrODBCNull(this.w_MPCODVAL)+;
             ",MPCAOVAL="+cp_ToStrODBC(this.w_MPCAOVAL)+;
             ",MPTIPCON="+cp_ToStrODBC(this.w_MPTIPCON)+;
             ",MPCODCON="+cp_ToStrODBCNull(this.w_MPCODCON)+;
             ",MPCODAGE="+cp_ToStrODBCNull(this.w_MPCODAGE)+;
             ",MPCODCAP="+cp_ToStrODBCNull(this.w_MPCODCAP)+;
             ",MPDESSUP="+cp_ToStrODBC(this.w_MPDESSUP)+;
             ",MPRIFDOC="+cp_ToStrODBCNull(this.w_MPRIFDOC)+;
             ",MPAGSOSP="+cp_ToStrODBC(this.w_MPAGSOSP)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
             +i_extfld+" WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
        else
          * i_cWhere = this.cKeyWhere
          i_extfld=cp_ReplaceVFPExtFlds(this,'MOP_MAST')
          i_cWhere = cp_PKFox(i_cTable  ,'MPSERIAL',this.w_MPSERIAL  )
          UPDATE (i_cTable) SET;
              MPNUMREG=this.w_MPNUMREG;
             ,MPCODESE=this.w_MPCODESE;
             ,MPDATREG=this.w_MPDATREG;
             ,MPVALNAZ=this.w_MPVALNAZ;
             ,MPNUMDOC=this.w_MPNUMDOC;
             ,MPALFDOC=this.w_MPALFDOC;
             ,MPDATDOC=this.w_MPDATDOC;
             ,MPCODVAL=this.w_MPCODVAL;
             ,MPCAOVAL=this.w_MPCAOVAL;
             ,MPTIPCON=this.w_MPTIPCON;
             ,MPCODCON=this.w_MPCODCON;
             ,MPCODAGE=this.w_MPCODAGE;
             ,MPCODCAP=this.w_MPCODCAP;
             ,MPDESSUP=this.w_MPDESSUP;
             ,MPRIFDOC=this.w_MPRIFDOC;
             ,MPAGSOSP=this.w_MPAGSOSP;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,UTCV=this.w_UTCV;
             ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Update end')
      endif
      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (!EMPTY(t_MPDATSCA) and (t_MPTOTIMP<>0 or t_MPTOTIM2<>0) AND (t_MPTOTAGE<>0 OR t_MPTOTZON<>0)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK = iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nConn = i_TableProp[this.MOP_DETT_IDX,3]
          i_cTable = cp_SetAzi(i_TableProp[this.MOP_DETT_IDX,2])
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              *
              * delete from MOP_DETT
              *
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              this.mUpdateTrsDetail()
              * --- Insert new row in the database table
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update MOP_DETT
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail(.t.)
              this.mUpdateTrsDetail(.t.)
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " MPDATSCA="+cp_ToStrODBC(this.w_MPDATSCA)+;
                     ",MPTOTIMP="+cp_ToStrODBC(this.w_MPTOTIMP)+;
                     ",MPPERPRA="+cp_ToStrODBC(this.w_MPPERPRA)+;
                     ",MPTOTAGE="+cp_ToStrODBC(this.w_MPTOTAGE)+;
                     ",MPTOTIM2="+cp_ToStrODBC(this.w_MPTOTIM2)+;
                     ",MPPERPRC="+cp_ToStrODBC(this.w_MPPERPRC)+;
                     ",MPTOTZON="+cp_ToStrODBC(this.w_MPTOTZON)+;
                     ",MPTIPMAT="+cp_ToStrODBC(this.w_MPTIPMAT)+;
                     ",MPDATMAT="+cp_ToStrODBC(this.w_MPDATMAT)+;
                     ",MPDATLIQ="+cp_ToStrODBC(this.w_MPDATLIQ)+;
                     ",MPFLSOSP="+cp_ToStrODBC(this.w_MPFLSOSP)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      MPDATSCA=this.w_MPDATSCA;
                     ,MPTOTIMP=this.w_MPTOTIMP;
                     ,MPPERPRA=this.w_MPPERPRA;
                     ,MPTOTAGE=this.w_MPTOTAGE;
                     ,MPTOTIM2=this.w_MPTOTIM2;
                     ,MPPERPRC=this.w_MPPERPRC;
                     ,MPTOTZON=this.w_MPTOTZON;
                     ,MPTIPMAT=this.w_MPTIPMAT;
                     ,MPDATMAT=this.w_MPDATMAT;
                     ,MPDATLIQ=this.w_MPDATLIQ;
                     ,MPFLSOSP=this.w_MPFLSOSP;
                     ,CPCCCHK=cp_NewCCChk() WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)1
        this.SetRow(RecCount(this.ctrsname), .f.)
        this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gsve_mpv
    this.notifyEvent('Controllifinali')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (!EMPTY(t_MPDATSCA) and (t_MPTOTIMP<>0 or t_MPTOTIM2<>0) AND (t_MPTOTAGE<>0 OR t_MPTOTZON<>0)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        i_nConn = i_TableProp[this.MOP_DETT_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MOP_DETT_IDX,2])
        *
        * delete MOP_DETT
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)3
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      if i_nModRow>0 and not(bTrsErr)
        i_NF = this.cCursor
        i_OldCCCHK = &i_NF..CPCCCHK
        i_nConn = i_TableProp[this.MOP_MAST_IDX,3]
        i_cTable = cp_SetAzi(i_TableProp[this.MOP_MAST_IDX,2])
        *
        * delete MOP_MAST
        *
        this.NotifyEvent('Delete start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                         " WHERE "+&i_cWhere+" and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete end')
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (!EMPTY(t_MPDATSCA) and (t_MPTOTIMP<>0 or t_MPTOTIM2<>0) AND (t_MPTOTAGE<>0 OR t_MPTOTZON<>0)) and I_SRV<>'A'
          this.WorkFromTrs()
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)4
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOP_MAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOP_MAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,9,.t.)
          .w_OBTEST = .w_MPDATDOC
        .DoRTCalc(11,11,.t.)
          .w_SIMVAL = IIF(EMPTY(.w_MPCODVAL),' ',.w_SIMVAL)
        .DoRTCalc(13,13,.t.)
        if .o_MPCODVAL<>.w_MPCODVAL.or. .o_MPDATDOC<>.w_MPDATDOC.or. .o_MPDATREG<>.w_MPDATREG
          .w_MPCAOVAL = GETCAM(.w_MPCODVAL, IIF(EMPTY(.w_MPDATDOC), .w_MPDATREG, .w_MPDATDOC),0)
        endif
        if .o_MPCODVAL<>.w_MPCODVAL
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(16,22,.t.)
        if .o_CAPZON<>.w_CAPZON
          .w_MPCODCAP = .w_CAPZON
          .link_1_23('Full')
        endif
        .DoRTCalc(24,27,.t.)
          .link_1_41('Full')
        .DoRTCalc(29,35,.t.)
        if .o_MPTOTIMP<>.w_MPTOTIMP.or. .o_MPPERPRA<>.w_MPPERPRA
          .w_TOTAGE = .w_TOTAGE-.w_mptotage
          .w_MPTOTAGE = cp_ROUND(.w_MPTOTIMP * .w_MPPERPRA/100,.w_DECTOT)
          .w_TOTAGE = .w_TOTAGE+.w_mptotage
        endif
        if .o_MPTOTIMP<>.w_MPTOTIMP
          .w_TOTIMPC = .w_TOTIMPC-.w_mptotim2
          .w_MPTOTIM2 = IIF(NOT EMPTY(.w_MPCODCAP),IIF(.w_MPTOTIMP<>0, .w_MPTOTIMP, .w_MPTOTIM2),0)
          .w_TOTIMPC = .w_TOTIMPC+.w_mptotim2
        endif
        .DoRTCalc(38,38,.t.)
        if .o_MPTOTIMP<>.w_MPTOTIMP.or. .o_MPTOTIM2<>.w_MPTOTIM2.or. .o_MPPERPRC<>.w_MPPERPRC
          .w_TOTAREA = .w_TOTAREA-.w_mptotzon
          .w_MPTOTZON = cp_ROUND(.w_MPTOTIM2 * .w_MPPERPRC/100,.w_DECTOT)
          .w_TOTAREA = .w_TOTAREA+.w_mptotzon
        endif
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEMOP","i_codazi,w_MPSERIAL")
          .op_MPSERIAL = .w_MPSERIAL
        endif
        if .op_codazi<>.w_codazi .or. .op_MPCODESE<>.w_MPCODESE
           cp_AskTableProg(this,i_nConn,"PRMOP","i_codazi,w_MPCODESE,w_MPNUMREG")
          .op_MPNUMREG = .w_MPNUMREG
        endif
        .op_codazi = .w_codazi
        .op_codazi = .w_codazi
        .op_MPCODESE = .w_MPCODESE
      endwith
      this.DoRTCalc(40,55,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_46.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_53.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_54.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oMPCAOVAL_1_14.enabled = this.oPgFrm.Page1.oPag.oMPCAOVAL_1_14.mCond()
    this.oPgFrm.Page1.oPag.oMPCODCAP_1_23.enabled = this.oPgFrm.Page1.oPag.oMPCODCAP_1_23.mCond()
    this.oPgFrm.Page1.oPag.oMPAGSOSP_1_50.enabled = this.oPgFrm.Page1.oPag.oMPAGSOSP_1_50.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_40.enabled = this.oPgFrm.Page1.oPag.oBtn_1_40.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    oField= this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPDATSCA_2_1
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPPERPRC_2_6.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPPERPRC_2_6.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPTOTZON_2_7.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oMPTOTZON_2_7.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oMPCAOVAL_1_14.visible=!this.oPgFrm.Page1.oPag.oMPCAOVAL_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_48.visible=!this.oPgFrm.Page1.oPag.oStr_1_48.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_46.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_53.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_54.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_55.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=MPCODESE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MPCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KES',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_MPCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZE);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZE;
                     ,'ESCODESE',trim(this.w_MPCODESE))
          select ESCODAZI,ESCODESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MPCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MPCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oMPCODESE_1_4'),i_cWhere,'GSAR_KES',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MPCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_MPCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZE;
                       ,'ESCODESE',this.w_MPCODESE)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MPCODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_MPVALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_MPCODESE = space(4)
      endif
      this.w_MPVALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MPCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MPCODVAL
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MPCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_MPCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VADTOBSO,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_MPCODVAL))
          select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VADTOBSO,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MPCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_MPCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VADTOBSO,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_MPCODVAL)+"%");

            select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VADTOBSO,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MPCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oMPCODVAL_1_11'),i_cWhere,'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VADTOBSO,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VADTOBSO,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MPCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VADTOBSO,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_MPCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_MPCODVAL)
            select VACODVAL,VADESVAL,VACAOVAL,VADECTOT,VADTOBSO,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MPCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESAPP = NVL(_Link_.VADESVAL,space(35))
      this.w_CAOVAL = NVL(_Link_.VACAOVAL,0)
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_MPCODVAL = space(3)
      endif
      this.w_DESAPP = space(35)
      this.w_CAOVAL = 0
      this.w_DECTOT = 0
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
        endif
        this.w_MPCODVAL = space(3)
        this.w_DESAPP = space(35)
        this.w_CAOVAL = 0
        this.w_DECTOT = 0
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_SIMVAL = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MPCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.VACODVAL as VACODVAL111"+ ",link_1_11.VADESVAL as VADESVAL111"+ ",link_1_11.VACAOVAL as VACAOVAL111"+ ",link_1_11.VADECTOT as VADECTOT111"+ ",link_1_11.VADTOBSO as VADTOBSO111"+ ",link_1_11.VASIMVAL as VASIMVAL111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on MOP_MAST.MPCODVAL=link_1_11.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and MOP_MAST.MPCODVAL=link_1_11.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MPCODCON
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MPCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_MPCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MPTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_MPTIPCON;
                     ,'ANCODICE',trim(this.w_MPCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MPCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_MPCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MPTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_MPCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_MPTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MPCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oMPCODCON_1_18'),i_cWhere,'GSAR_BZC',"Anagrafica clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_MPTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_MPTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MPCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_MPCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_MPTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_MPTIPCON;
                       ,'ANCODICE',this.w_MPCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MPCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBS = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MPCODCON = space(15)
      endif
      this.w_DESCRI = space(40)
      this.w_DATOBS = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBS>.w_MPDATDOC OR EMPTY(.w_DATOBS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_MPCODCON = space(15)
        this.w_DESCRI = space(40)
        this.w_DATOBS = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MPCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_18(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_18.ANCODICE as ANCODICE118"+ ",link_1_18.ANDESCRI as ANDESCRI118"+ ",link_1_18.ANDTOBSO as ANDTOBSO118"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_18 on MOP_MAST.MPCODCON=link_1_18.ANCODICE"+" and MOP_MAST.MPTIPCON=link_1_18.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_18"
          i_cKey=i_cKey+'+" and MOP_MAST.MPCODCON=link_1_18.ANCODICE(+)"'+'+" and MOP_MAST.MPTIPCON=link_1_18.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MPCODAGE
  func Link_1_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MPCODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_MPCODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGTIPAGE,AGCZOAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_MPCODAGE))
          select AGCODAGE,AGDESAGE,AGTIPAGE,AGCZOAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MPCODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_MPCODAGE)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGTIPAGE,AGCZOAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_MPCODAGE)+"%");

            select AGCODAGE,AGDESAGE,AGTIPAGE,AGCZOAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MPCODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oMPCODAGE_1_19'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGTIPAGE,AGCZOAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGTIPAGE,AGCZOAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MPCODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGTIPAGE,AGCZOAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_MPCODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_MPCODAGE)
            select AGCODAGE,AGDESAGE,AGTIPAGE,AGCZOAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MPCODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESCAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_TIPAGEN = NVL(_Link_.AGTIPAGE,space(1))
      this.w_CAPZON = NVL(_Link_.AGCZOAGE,space(5))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_MPCODAGE = space(5)
      endif
      this.w_DESCAGE = space(35)
      this.w_TIPAGEN = space(1)
      this.w_CAPZON = space(5)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATOBSO>.w_MPDATDOC OR EMPTY(.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Agente inesistente oppure obsoleto")
        endif
        this.w_MPCODAGE = space(5)
        this.w_DESCAGE = space(35)
        this.w_TIPAGEN = space(1)
        this.w_CAPZON = space(5)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MPCODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_19.AGCODAGE as AGCODAGE119"+ ",link_1_19.AGDESAGE as AGDESAGE119"+ ",link_1_19.AGTIPAGE as AGTIPAGE119"+ ",link_1_19.AGCZOAGE as AGCZOAGE119"+ ",link_1_19.AGDTOBSO as AGDTOBSO119"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_19 on MOP_MAST.MPCODAGE=link_1_19.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_19"
          i_cKey=i_cKey+'+" and MOP_MAST.MPCODAGE=link_1_19.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MPCODCAP
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MPCODCAP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_MPCODCAP)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_MPCODCAP))
          select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MPCODCAP)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_MPCODCAP) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oMPCODCAP_1_23'),i_cWhere,'GSAR_AGE',"Agenti",'GSAR1AGE.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MPCODCAP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_MPCODCAP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_MPCODCAP)
            select AGCODAGE,AGDESAGE,AGDTOBSO,AGTIPAGE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MPCODCAP = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESCAPZ = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBS1 = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
      this.w_TIPOAG = NVL(_Link_.AGTIPAGE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_MPCODCAP = space(5)
      endif
      this.w_DESCAPZ = space(35)
      this.w_DATOBS1 = ctod("  /  /  ")
      this.w_TIPOAG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TIPOAG='C') AND (.w_DATOBS1>.w_MPDATDOC OR EMPTY(.w_DATOBS1))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Capo area inesistente oppure obsoleto")
        endif
        this.w_MPCODCAP = space(5)
        this.w_DESCAPZ = space(35)
        this.w_DATOBS1 = ctod("  /  /  ")
        this.w_TIPOAG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MPCODCAP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_23.AGCODAGE as AGCODAGE123"+ ",link_1_23.AGDESAGE as AGDESAGE123"+ ",link_1_23.AGDTOBSO as AGDTOBSO123"+ ",link_1_23.AGTIPAGE as AGTIPAGE123"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_23 on MOP_MAST.MPCODCAP=link_1_23.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_23"
          i_cKey=i_cKey+'+" and MOP_MAST.MPCODCAP=link_1_23.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=MPRIFDOC
  func Link_1_41(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DOC_MAST_IDX,3]
    i_lTable = "DOC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2], .t., this.DOC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MPRIFDOC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MPRIFDOC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MVSERIAL,MVFLVEAC,MVCLADOC";
                   +" from "+i_cTable+" "+i_lTable+" where MVSERIAL="+cp_ToStrODBC(this.w_MPRIFDOC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MVSERIAL',this.w_MPRIFDOC)
            select MVSERIAL,MVFLVEAC,MVCLADOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MPRIFDOC = NVL(_Link_.MVSERIAL,space(10))
      this.w_FLVEAC = NVL(_Link_.MVFLVEAC,space(1))
      this.w_CLADOC = NVL(_Link_.MVCLADOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_MPRIFDOC = space(10)
      endif
      this.w_FLVEAC = space(1)
      this.w_CLADOC = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MVSERIAL,1)
      cp_ShowWarn(i_cKey,this.DOC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MPRIFDOC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_41(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DOC_MAST_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DOC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_41.MVSERIAL as MVSERIAL141"+ ",link_1_41.MVFLVEAC as MVFLVEAC141"+ ",link_1_41.MVCLADOC as MVCLADOC141"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_41 on MOP_MAST.MPRIFDOC=link_1_41.MVSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_41"
          i_cKey=i_cKey+'+" and MOP_MAST.MPRIFDOC=link_1_41.MVSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oMPNUMREG_1_3.value==this.w_MPNUMREG)
      this.oPgFrm.Page1.oPag.oMPNUMREG_1_3.value=this.w_MPNUMREG
    endif
    if not(this.oPgFrm.Page1.oPag.oMPCODESE_1_4.value==this.w_MPCODESE)
      this.oPgFrm.Page1.oPag.oMPCODESE_1_4.value=this.w_MPCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oMPDATREG_1_5.value==this.w_MPDATREG)
      this.oPgFrm.Page1.oPag.oMPDATREG_1_5.value=this.w_MPDATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oMPNUMDOC_1_7.value==this.w_MPNUMDOC)
      this.oPgFrm.Page1.oPag.oMPNUMDOC_1_7.value=this.w_MPNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMPALFDOC_1_8.value==this.w_MPALFDOC)
      this.oPgFrm.Page1.oPag.oMPALFDOC_1_8.value=this.w_MPALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMPDATDOC_1_9.value==this.w_MPDATDOC)
      this.oPgFrm.Page1.oPag.oMPDATDOC_1_9.value=this.w_MPDATDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMPCODVAL_1_11.value==this.w_MPCODVAL)
      this.oPgFrm.Page1.oPag.oMPCODVAL_1_11.value=this.w_MPCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_12.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_12.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMPCAOVAL_1_14.value==this.w_MPCAOVAL)
      this.oPgFrm.Page1.oPag.oMPCAOVAL_1_14.value=this.w_MPCAOVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oMPCODCON_1_18.value==this.w_MPCODCON)
      this.oPgFrm.Page1.oPag.oMPCODCON_1_18.value=this.w_MPCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oMPCODAGE_1_19.value==this.w_MPCODAGE)
      this.oPgFrm.Page1.oPag.oMPCODAGE_1_19.value=this.w_MPCODAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAGE_1_20.value==this.w_DESCAGE)
      this.oPgFrm.Page1.oPag.oDESCAGE_1_20.value=this.w_DESCAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oMPCODCAP_1_23.value==this.w_MPCODCAP)
      this.oPgFrm.Page1.oPag.oMPCODCAP_1_23.value=this.w_MPCODCAP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAPZ_1_24.value==this.w_DESCAPZ)
      this.oPgFrm.Page1.oPag.oDESCAPZ_1_24.value=this.w_DESCAPZ
    endif
    if not(this.oPgFrm.Page1.oPag.oMPDESSUP_1_25.value==this.w_MPDESSUP)
      this.oPgFrm.Page1.oPag.oMPDESSUP_1_25.value=this.w_MPDESSUP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_26.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_26.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIMP_3_1.value==this.w_TOTIMP)
      this.oPgFrm.Page1.oPag.oTOTIMP_3_1.value=this.w_TOTIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTAGE_3_2.value==this.w_TOTAGE)
      this.oPgFrm.Page1.oPag.oTOTAGE_3_2.value=this.w_TOTAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTAREA_3_3.value==this.w_TOTAREA)
      this.oPgFrm.Page1.oPag.oTOTAREA_3_3.value=this.w_TOTAREA
    endif
    if not(this.oPgFrm.Page1.oPag.oMPAGSOSP_1_50.RadioValue()==this.w_MPAGSOSP)
      this.oPgFrm.Page1.oPag.oMPAGSOSP_1_50.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTIMPC_3_4.value==this.w_TOTIMPC)
      this.oPgFrm.Page1.oPag.oTOTIMPC_3_4.value=this.w_TOTIMPC
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPDATSCA_2_1.value==this.w_MPDATSCA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPDATSCA_2_1.value=this.w_MPDATSCA
      replace t_MPDATSCA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPDATSCA_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPTOTIMP_2_2.value==this.w_MPTOTIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPTOTIMP_2_2.value=this.w_MPTOTIMP
      replace t_MPTOTIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPTOTIMP_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPPERPRA_2_3.value==this.w_MPPERPRA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPPERPRA_2_3.value=this.w_MPPERPRA
      replace t_MPPERPRA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPPERPRA_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPTOTAGE_2_4.value==this.w_MPTOTAGE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPTOTAGE_2_4.value=this.w_MPTOTAGE
      replace t_MPTOTAGE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPTOTAGE_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPTOTIM2_2_5.value==this.w_MPTOTIM2)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPTOTIM2_2_5.value=this.w_MPTOTIM2
      replace t_MPTOTIM2 with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPTOTIM2_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPPERPRC_2_6.value==this.w_MPPERPRC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPPERPRC_2_6.value=this.w_MPPERPRC
      replace t_MPPERPRC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPPERPRC_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPTOTZON_2_7.value==this.w_MPTOTZON)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPTOTZON_2_7.value=this.w_MPTOTZON
      replace t_MPTOTZON with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPTOTZON_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPTIPMAT_2_8.RadioValue()==this.w_MPTIPMAT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPTIPMAT_2_8.SetRadio()
      replace t_MPTIPMAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPTIPMAT_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPDATMAT_2_9.value==this.w_MPDATMAT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPDATMAT_2_9.value=this.w_MPDATMAT
      replace t_MPDATMAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPDATMAT_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPDATLIQ_2_10.value==this.w_MPDATLIQ)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPDATLIQ_2_10.value=this.w_MPDATLIQ
      replace t_MPDATLIQ with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPDATLIQ_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPFLSOSP_2_11.RadioValue()==this.w_MPFLSOSP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPFLSOSP_2_11.SetRadio()
      replace t_MPFLSOSP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPFLSOSP_2_11.value
    endif
    cp_SetControlsValueExtFlds(this,'MOP_MAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_MPCODESE))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMPCODESE_1_4.SetFocus()
            i_bnoObbl = !empty(.w_MPCODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MPDATREG))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMPDATREG_1_5.SetFocus()
            i_bnoObbl = !empty(.w_MPDATREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MPNUMDOC))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMPNUMDOC_1_7.SetFocus()
            i_bnoObbl = !empty(.w_MPNUMDOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero documento obbligatorio")
          case   (empty(.w_MPDATDOC) or not(.w_MPDATDOC<=.w_MPDATREG or  .cfunction='Edit'))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMPDATDOC_1_9.SetFocus()
            i_bnoObbl = !empty(.w_MPDATDOC)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data doc. deve essere minore della data di registrazione e non vuota")
          case   (empty(.w_MPCODVAL) or not(CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMPCODVAL_1_11.SetFocus()
            i_bnoObbl = !empty(.w_MPCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
          case   (empty(.w_MPCAOVAL))  and not(.w_CAOVAL<>0)  and (GETVALUT(g_PERVAL, 'VADATEUR')>=IIF(EMPTY(.w_MPDATDOC), .w_MPDATREG, .w_MPDATDOC) OR .w_CAOVAL=0)
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMPCAOVAL_1_14.SetFocus()
            i_bnoObbl = !empty(.w_MPCAOVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MPCODCON) or not(.w_DATOBS>.w_MPDATDOC OR EMPTY(.w_DATOBS)))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMPCODCON_1_18.SetFocus()
            i_bnoObbl = !empty(.w_MPCODCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   (empty(.w_MPCODAGE) or not(.w_DATOBSO>.w_MPDATDOC OR EMPTY(.w_DATOBSO)))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMPCODAGE_1_19.SetFocus()
            i_bnoObbl = !empty(.w_MPCODAGE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Agente inesistente oppure obsoleto")
          case   not((.w_TIPOAG='C') AND (.w_DATOBS1>.w_MPDATDOC OR EMPTY(.w_DATOBS1)))  and (.w_TIPAGEN='A')  and not(empty(.w_MPCODCAP))
            .oPgFrm.ActivePage=1
            .oPgFrm.Page1.oPag.oMPCODCAP_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Capo area inesistente oppure obsoleto")
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_MPDATSCA) and (.w_MPTOTIMP<>0 or .w_MPTOTIM2<>0) AND (.w_MPTOTAGE<>0 OR .w_MPTOTZON<>0) and (!EMPTY(.w_MPDATSCA) and (.w_MPTOTIMP<>0 or .w_MPTOTIM2<>0) AND (.w_MPTOTAGE<>0 OR .w_MPTOTZON<>0))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPDATSCA_2_1
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
      endcase
      if !EMPTY(.w_MPDATSCA) and (.w_MPTOTIMP<>0 or .w_MPTOTIM2<>0) AND (.w_MPTOTAGE<>0 OR .w_MPTOTZON<>0)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_MPDATREG = this.w_MPDATREG
    this.o_MPDATDOC = this.w_MPDATDOC
    this.o_MPCODVAL = this.w_MPCODVAL
    this.o_MPCODAGE = this.w_MPCODAGE
    this.o_CAPZON = this.w_CAPZON
    this.o_MPTOTIMP = this.w_MPTOTIMP
    this.o_MPPERPRA = this.w_MPPERPRA
    this.o_MPTOTIM2 = this.w_MPTOTIM2
    this.o_MPPERPRC = this.w_MPPERPRC
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(!EMPTY(t_MPDATSCA) and (t_MPTOTIMP<>0 or t_MPTOTIM2<>0) AND (t_MPTOTAGE<>0 OR t_MPTOTZON<>0))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_MPDATSCA=ctod("  /  /  ")
      .w_MPTOTIMP=0
      .w_MPPERPRA=0
      .w_MPTOTAGE=0
      .w_MPTOTIM2=0
      .w_MPPERPRC=0
      .w_MPTOTZON=0
      .w_MPTIPMAT=space(2)
      .w_MPDATMAT=ctod("  /  /  ")
      .w_MPDATLIQ=ctod("  /  /  ")
      .w_MPFLSOSP=space(1)
      .DoRTCalc(1,35,.f.)
        .w_MPTOTAGE = cp_ROUND(.w_MPTOTIMP * .w_MPPERPRA/100,.w_DECTOT)
        .w_MPTOTIM2 = IIF(NOT EMPTY(.w_MPCODCAP),IIF(.w_MPTOTIMP<>0, .w_MPTOTIMP, .w_MPTOTIM2),0)
      .DoRTCalc(38,38,.f.)
        .w_MPTOTZON = cp_ROUND(.w_MPTOTIM2 * .w_MPPERPRC/100,.w_DECTOT)
        .w_MPTIPMAT = 'FA'
    endwith
    this.DoRTCalc(41,55,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_MPDATSCA = t_MPDATSCA
    this.w_MPTOTIMP = t_MPTOTIMP
    this.w_MPPERPRA = t_MPPERPRA
    this.w_MPTOTAGE = t_MPTOTAGE
    this.w_MPTOTIM2 = t_MPTOTIM2
    this.w_MPPERPRC = t_MPPERPRC
    this.w_MPTOTZON = t_MPTOTZON
    this.w_MPTIPMAT = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPTIPMAT_2_8.RadioValue(.t.)
    this.w_MPDATMAT = t_MPDATMAT
    this.w_MPDATLIQ = t_MPDATLIQ
    this.w_MPFLSOSP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPFLSOSP_2_11.RadioValue(.t.)
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_MPDATSCA with this.w_MPDATSCA
    replace t_MPTOTIMP with this.w_MPTOTIMP
    replace t_MPPERPRA with this.w_MPPERPRA
    replace t_MPTOTAGE with this.w_MPTOTAGE
    replace t_MPTOTIM2 with this.w_MPTOTIM2
    replace t_MPPERPRC with this.w_MPPERPRC
    replace t_MPTOTZON with this.w_MPTOTZON
    replace t_MPTIPMAT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPTIPMAT_2_8.ToRadio()
    replace t_MPDATMAT with this.w_MPDATMAT
    replace t_MPDATLIQ with this.w_MPDATLIQ
    replace t_MPFLSOSP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oMPFLSOSP_2_11.ToRadio()
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTIMP = .w_TOTIMP-.w_mptotimp
        .w_TOTAGE = .w_TOTAGE-.w_mptotage
        .w_TOTIMPC = .w_TOTIMPC-.w_mptotim2
        .w_TOTAREA = .w_TOTAREA-.w_mptotzon
        .SetControlsValue()
      endwith
  EndProc
enddefine

* --- Define pages as container
define class tgsve_mpvPag1 as StdContainer
  Width  = 864
  height = 395
  stdWidth  = 864
  stdheight = 395
  resizeXpos=588
  resizeYpos=296
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oMPNUMREG_1_3 as StdField with uid="PHYGRVROCM",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MPNUMREG", cQueryName = "MPNUMREG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero di registrazione",;
    HelpContextID = 241173261,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=107, Top=12, cSayPict='"999999"', cGetPict='"999999"'

  proc oMPNUMREG_1_3.mBefore
    with this.Parent.oContained
      this.cQueryName = "MPCODESE,MPNUMREG"
    endwith
  endproc

  add object oMPCODESE_1_4 as StdField with uid="YCKDXTLPHR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MPCODESE", cQueryName = "MPCODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di competenza del movimento di provvigione",;
    HelpContextID = 13193995,;
   bGlobalFont=.t.,;
    Height=21, Width=49, Left=186, Top=12, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", cZoomOnZoom="GSAR_KES", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZE", oKey_2_1="ESCODESE", oKey_2_2="this.w_MPCODESE"

  func oMPCODESE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oMPCODESE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMPCODESE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZE)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oMPCODESE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KES',"Esercizi",'',this.parent.oContained
  endproc
  proc oMPCODESE_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KES()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ESCODAZI=w_CODAZE
     i_obj.w_ESCODESE=this.parent.oContained.w_MPCODESE
    i_obj.ecpSave()
  endproc

  add object oMPDATREG_1_5 as StdField with uid="QCYZJWRSBN",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MPDATREG", cQueryName = "MPDATREG",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del movimento di provvigione",;
    HelpContextID = 247161613,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=274, Top=12

  add object oMPNUMDOC_1_7 as StdField with uid="YOSUIFNBBB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_MPNUMDOC", cQueryName = "MPNUMDOC",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero documento obbligatorio",;
    ToolTipText = "Numero del documento",;
    HelpContextID = 6292233,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=107, Top=43, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  proc oMPNUMDOC_1_7.mBefore
    with this.Parent.oContained
      this.cQueryName = "MPCODESE,MPNUMDOC"
    endwith
  endproc

  add object oMPALFDOC_1_8 as StdField with uid="NAEJEMNLBB",rtseq=8,rtrep=.f.,;
    cFormVar = "w_MPALFDOC", cQueryName = "MPALFDOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Parte alfanumerica del documento",;
    HelpContextID = 266744585,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=245, Top=43, cSayPict="'!!!!!!!!!!'", cGetPict="'!!!!!!!!!!'", InputMask=replicate('X',10)

  add object oMPDATDOC_1_9 as StdField with uid="EMWSUWOTIM",rtseq=9,rtrep=.f.,;
    cFormVar = "w_MPDATDOC", cQueryName = "MPDATDOC",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data doc. deve essere minore della data di registrazione e non vuota",;
    ToolTipText = "Data del documento",;
    HelpContextID = 12280585,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=455, Top=43

  func oMPDATDOC_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_MPDATDOC<=.w_MPDATREG or  .cfunction='Edit')
    endwith
    return bRes
  endfunc

  add object oMPCODVAL_1_11 as StdField with uid="IIPVZVCXUZ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_MPCODVAL", cQueryName = "MPCODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta inesistente o incongruente o obsoleta",;
    ToolTipText = "Codice valuta del documento",;
    HelpContextID = 29971218,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=685, Top=43, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_MPCODVAL"

  func oMPCODVAL_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oMPCODVAL_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMPCODVAL_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oMPCODVAL_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oMPCODVAL_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_MPCODVAL
    i_obj.ecpSave()
  endproc

  add object oSIMVAL_1_12 as StdField with uid="KYVJFCBHJX",rtseq=12,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 127986726,;
   bGlobalFont=.t.,;
    Height=21, Width=54, Left=736, Top=43, InputMask=replicate('X',5)

  add object oMPCAOVAL_1_14 as StdField with uid="CRXBGISSFP",rtseq=14,rtrep=.f.,;
    cFormVar = "w_MPCAOVAL", cQueryName = "MPCAOVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio del documento",;
    HelpContextID = 40588050,;
   bGlobalFont=.t.,;
    Height=21, Width=105, Left=685, Top=74, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oMPCAOVAL_1_14.mCond()
    with this.Parent.oContained
      return (GETVALUT(g_PERVAL, 'VADATEUR')>=IIF(EMPTY(.w_MPDATDOC), .w_MPDATREG, .w_MPDATDOC) OR .w_CAOVAL=0)
    endwith
  endfunc

  func oMPCAOVAL_1_14.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CAOVAL<>0)
    endwith
    endif
  endfunc

  add object oMPCODCON_1_18 as StdField with uid="EFKAEGUKPN",rtseq=18,rtrep=.f.,;
    cFormVar = "w_MPCODCON", cQueryName = "MPCODCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice del cliente selezionato",;
    HelpContextID = 248075028,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=107, Top=74, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_MPTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_MPCODCON"

  func oMPCODCON_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oMPCODCON_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMPCODCON_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_MPTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_MPTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oMPCODCON_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Anagrafica clienti",'',this.parent.oContained
  endproc
  proc oMPCODCON_1_18.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_MPTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_MPCODCON
    i_obj.ecpSave()
  endproc

  add object oMPCODAGE_1_19 as StdField with uid="XSEJHIHHAJ",rtseq=19,rtrep=.f.,;
    cFormVar = "w_MPCODAGE", cQueryName = "MPCODAGE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Agente inesistente oppure obsoleto",;
    ToolTipText = "Codice agente selezionato",;
    HelpContextID = 53914869,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=107, Top=105, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_MPCODAGE"

  func oMPCODAGE_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oMPCODAGE_1_19.ecpDrop(oSource)
    this.Parent.oContained.link_1_19('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMPCODAGE_1_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oMPCODAGE_1_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oMPCODAGE_1_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_MPCODAGE
    i_obj.ecpSave()
  endproc

  add object oDESCAGE_1_20 as StdField with uid="VGOYIWHCHU",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCAGE", cQueryName = "DESCAGE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 42878774,;
   bGlobalFont=.t.,;
    Height=21, Width=294, Left=172, Top=105, InputMask=replicate('X',35)

  add object oMPCODCAP_1_23 as StdField with uid="CELZDYNIZW",rtseq=23,rtrep=.f.,;
    cFormVar = "w_MPCODCAP", cQueryName = "MPCODCAP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Capo area inesistente oppure obsoleto",;
    ToolTipText = "Codice del capo area selezionato",;
    HelpContextID = 248075030,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=107, Top=136, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_MPCODCAP"

  func oMPCODCAP_1_23.mCond()
    with this.Parent.oContained
      return (.w_TIPAGEN='A')
    endwith
  endfunc

  func oMPCODCAP_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oMPCODCAP_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMPCODCAP_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oMPCODCAP_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'GSAR1AGE.AGENTI_VZM',this.parent.oContained
  endproc
  proc oMPCODCAP_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_MPCODCAP
    i_obj.ecpSave()
  endproc

  add object oDESCAPZ_1_24 as StdField with uid="WKTNQRMUIW",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCAPZ", cQueryName = "DESCAPZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 74561738,;
   bGlobalFont=.t.,;
    Height=21, Width=294, Left=172, Top=136, InputMask=replicate('X',35)

  add object oMPDESSUP_1_25 as StdField with uid="IJVYTQCQHS",rtseq=25,rtrep=.f.,;
    cFormVar = "w_MPDESSUP", cQueryName = "MPDESSUP",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione supplementare",;
    HelpContextID = 263152406,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=107, Top=167, InputMask=replicate('X',50)

  add object oDESCRI_1_26 as StdField with uid="BJEIRYTDBQ",rtseq=26,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 94258998,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=245, Top=74, InputMask=replicate('X',40)


  add object oBtn_1_40 as StdButton with uid="WFRFVXYFEF",left=742, top=136, width=48,height=45,;
    CpPicture="bmp\documenti.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai documenti";
    , HelpContextID = 58816570;
    , Caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_40.Click()
      with this.Parent.oContained
        GSVE_BZM(this.Parent.oContained,.w_MPRIFDOC, .w_FLVEAC+.w_CLADOC)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_40.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_MPRIFDOC))
    endwith
  endfunc


  add object oObj_1_46 as cp_runprogram with uid="XDSDXUDOIR",left=144, top=408, width=212,height=22,;
    caption='GSVE_BPD(C)',;
   bGlobalFont=.t.,;
    prg="GSVE_BPD('C')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 259218218


  add object oMPAGSOSP_1_50 as StdCombo with uid="OIUBRLDJLT",rtseq=48,rtrep=.f.,left=697,top=12,width=93,height=21;
    , ToolTipText = "Se sospeso: il movimento di provvigione non pu� essere liquidato";
    , HelpContextID = 196162326;
    , cFormVar="w_MPAGSOSP",RowSource=""+"Sospeso,"+"Confermato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMPAGSOSP_1_50.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MPAGSOSP,&i_cF..t_MPAGSOSP),this.value)
    return(iif(xVal =1,'S',;
    iif(xVal =2,'C',;
    space(1))))
  endfunc
  func oMPAGSOSP_1_50.GetRadio()
    this.Parent.oContained.w_MPAGSOSP = this.RadioValue()
    return .t.
  endfunc

  func oMPAGSOSP_1_50.ToRadio()
    this.Parent.oContained.w_MPAGSOSP=trim(this.Parent.oContained.w_MPAGSOSP)
    return(;
      iif(this.Parent.oContained.w_MPAGSOSP=='S',1,;
      iif(this.Parent.oContained.w_MPAGSOSP=='C',2,;
      0)))
  endfunc

  func oMPAGSOSP_1_50.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oMPAGSOSP_1_50.mCond()
    with this.Parent.oContained
      return (.w_DATOBSO>.w_MPDATREG OR EMPTY(.w_DATOBSO))
    endwith
  endfunc


  add object oObj_1_53 as cp_runprogram with uid="PLBHOGATWN",left=575, top=406, width=249,height=22,;
    caption='GSVE_BPD(F)',;
   bGlobalFont=.t.,;
    prg="GSVE_BPD('F')",;
    cEvent = "w_MPAGSOSP Changed",;
    nPag=1;
    , HelpContextID = 259218986


  add object oObj_1_54 as cp_runprogram with uid="OGJWRDGYYC",left=357, top=406, width=212,height=22,;
    caption='GSVE_BPD(I)',;
   bGlobalFont=.t.,;
    prg="GSVE_BPD('I')",;
    cEvent = "w_MPDATREG Changed,w_MPCODAGE Changed,Edit Started",;
    nPag=1;
    , HelpContextID = 259219754


  add object oObj_1_55 as cp_runprogram with uid="FYKKSLWUBG",left=144, top=435, width=208,height=22,;
    caption='GSVE_BPD(S)',;
   bGlobalFont=.t.,;
    prg="GSVE_BPD('S')",;
    cEvent = "Controllifinali",;
    nPag=1;
    , HelpContextID = 259222314


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=193, width=849,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=11,Field1="MPDATSCA",Label1="Scadenza",Field2="MPTOTIMP",Label2="Imponibile ag.",Field3="MPPERPRA",Label3="% Prov.",Field4="MPTOTAGE",Label4="Agente",Field5="MPTOTIM2",Label5="Imponibile c.a.",Field6="MPPERPRC",Label6="% Prov.",Field7="MPTOTZON",Label7="Capo area",Field8="MPTIPMAT",Label8="Tipo maturaz.",Field9="MPDATMAT",Label9="Maturazione",Field10="MPDATLIQ",Label10="Liquidazione",Field11="MPFLSOSP",Label11="Sosp.",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 168621946

  add object oStr_1_27 as StdString with uid="UDXKXXJFZG",Visible=.t., Left=2, Top=12,;
    Alignment=1, Width=103, Height=15,;
    Caption="Registrazione n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_28 as StdString with uid="HOQEZFKFRM",Visible=.t., Left=173, Top=13,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="EWKUNLNWPJ",Visible=.t., Left=239, Top=12,;
    Alignment=1, Width=32, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_30 as StdString with uid="ICERQQETGC",Visible=.t., Left=2, Top=43,;
    Alignment=1, Width=103, Height=15,;
    Caption="Documento n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="NISPPQDPBM",Visible=.t., Left=235, Top=44,;
    Alignment=2, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="RXJAEHJDVQ",Visible=.t., Left=405, Top=43,;
    Alignment=1, Width=47, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="GLLPMWZXHJ",Visible=.t., Left=637, Top=43,;
    Alignment=1, Width=47, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="WXRFRCHDDE",Visible=.t., Left=619, Top=75,;
    Alignment=1, Width=65, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (.w_CAOVAL<>0)
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="CRFEFMGFBQ",Visible=.t., Left=2, Top=74,;
    Alignment=1, Width=103, Height=15,;
    Caption="Cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="CBMLUNNSRV",Visible=.t., Left=2, Top=105,;
    Alignment=1, Width=103, Height=15,;
    Caption="Agente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="GZCWTKOSHS",Visible=.t., Left=2, Top=136,;
    Alignment=1, Width=103, Height=15,;
    Caption="Capo area:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="NTMYKWSVZG",Visible=.t., Left=2, Top=167,;
    Alignment=1, Width=103, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="VYLXAPBEMN",Visible=.t., Left=478, Top=105,;
    Alignment=0, Width=103, Height=15,;
    Caption="Agente obsoleto"  ;
  , bGlobalFont=.t.

  func oStr_1_48.mHide()
    with this.Parent.oContained
      return (.w_DATOBSO>.w_MPDATREG OR EMPTY(.w_DATOBSO))
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="TPJRETZSFM",Visible=.t., Left=589, Top=12,;
    Alignment=1, Width=103, Height=15,;
    Caption="Stato movimento:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=212,;
    width=860+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=213,width=859+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTIMP_3_1 as StdField with uid="SOIZVERKSU",rtseq=44,rtrep=.f.,;
    cFormVar="w_TOTIMP",value=0,enabled=.f.,;
    ToolTipText = "Totale imponibile",;
    HelpContextID = 206856758,;
    cQueryName = "TOTIMP",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=103, Left=79, Top=371, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]

  add object oTOTAGE_3_2 as StdField with uid="NKMHUVMVTG",rtseq=45,rtrep=.f.,;
    cFormVar="w_TOTAGE",value=0,enabled=.f.,;
    ToolTipText = "Totale importo agente",;
    HelpContextID = 15491638,;
    cQueryName = "TOTAGE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=231, Top=371, cSayPict=[v_PV(36+VVL)], cGetPict=[v_GV(36+VVL)]

  add object oTOTAREA_3_3 as StdField with uid="JZPNNTYUZZ",rtseq=46,rtrep=.f.,;
    cFormVar="w_TOTAREA",value=0,enabled=.f.,;
    ToolTipText = "Totale importo capo area",;
    HelpContextID = 27025974,;
    cQueryName = "TOTAREA",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=461, Top=371, cSayPict=[v_PV(36+VVL)], cGetPict=[v_GV(36+VVL)]

  add object oTOTIMPC_3_4 as StdField with uid="LVXCEMVPIZ",rtseq=51,rtrep=.f.,;
    cFormVar="w_TOTIMPC",value=0,enabled=.f.,;
    ToolTipText = "Totale imponibile",;
    HelpContextID = 206856758,;
    cQueryName = "TOTIMPC",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=103, Left=329, Top=371, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]
enddefine

* --- Defining Body row
define class tgsve_mpvBodyRow as CPBodyRowCnt
  Width=850
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oMPDATSCA_2_1 as StdTrsField with uid="PYFSQVRXJI",rtseq=33,rtrep=.t.,;
    cFormVar="w_MPDATSCA",value=ctod("  /  /  "),;
    ToolTipText = "Data di scadenza selezionata",;
    HelpContextID = 263938823,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=-2, Top=0

  func oMPDATSCA_2_1.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=(.w_MPTOTIMP<>0 or .w_MPTOTIM2<>0) AND (.w_MPTOTAGE<>0 OR .w_MPTOTZON<>0)
    endwith
    return i_bres
  endfunc

  add object oMPTOTIMP_2_2 as StdTrsField with uid="YGRSROOGFE",rtseq=34,rtrep=.t.,;
    cFormVar="w_MPTOTIMP",value=0,;
    ToolTipText = "Imponibile selezionato",;
    HelpContextID = 171285738,;
    cTotal = "this.Parent.oContained.w_totimp", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=103, Left=75, Top=0, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]

  add object oMPPERPRA_2_3 as StdTrsField with uid="JCYVFTZWRX",rtseq=35,rtrep=.t.,;
    cFormVar="w_MPPERPRA",value=0,;
    ToolTipText = "Percentuale provvigione dell'agente selezionato",;
    HelpContextID = 211821319,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=56, Left=178, Top=0, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oMPTOTAGE_2_4 as StdTrsField with uid="YIZPNQPYUG",rtseq=36,rtrep=.t.,;
    cFormVar="w_MPTOTAGE",value=0,;
    ToolTipText = "Importo provvigione della provvigione dell'agente",;
    HelpContextID = 37068021,;
    cTotal = "this.Parent.oContained.w_totage", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=88, Left=236, Top=0, cSayPict=[v_PV(36+VVL)], cGetPict=[v_GV(36+VVL)]

  add object oMPTOTIM2_2_5 as StdTrsField with uid="QAFZLKSHQV",rtseq=37,rtrep=.t.,;
    cFormVar="w_MPTOTIM2",value=0,;
    HelpContextID = 171285768,;
    cTotal = "this.Parent.oContained.w_totimpc", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=103, Left=324, Top=0, cSayPict=[v_PV(38+VVL)], cGetPict=[v_GV(38+VVL)]

  add object oMPPERPRC_2_6 as StdTrsField with uid="TDTMFMRZNE",rtseq=38,rtrep=.t.,;
    cFormVar="w_MPPERPRC",value=0,;
    ToolTipText = "Percentuale provvigione del capo area selezionato",;
    HelpContextID = 211821321,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=54, Left=427, Top=0, cSayPict=["999.99"], cGetPict=["999.99"]

  func oMPPERPRC_2_6.mCond()
    with this.Parent.oContained
      return (.w_TIPAGEN='A' AND NOT EMPTY(.w_MPCODCAP))
    endwith
  endfunc

  add object oMPTOTZON_2_7 as StdTrsField with uid="ZBFOTRUTFA",rtseq=39,rtrep=.t.,;
    cFormVar="w_MPTOTZON",value=0,;
    ToolTipText = "Importo della provvigione del capo area",;
    HelpContextID = 113926932,;
    cTotal = "this.Parent.oContained.w_totarea", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=86, Left=483, Top=0, cSayPict=[v_PV(36+VVL)], cGetPict=[v_GV(36+VVL)]

  func oMPTOTZON_2_7.mCond()
    with this.Parent.oContained
      return (.w_TIPAGEN='A')
    endwith
  endfunc

  add object oMPTIPMAT_2_8 as StdTrsCombo with uid="UJLZHVXJEZ",rtrep=.t.,;
    cFormVar="w_MPTIPMAT", RowSource=""+"Dt.fattura,"+"Dt.scadenza,"+"Dt.incasso" , ;
    ToolTipText = "Tipo maturazione",;
    HelpContextID = 159671066,;
    Height=22, Width=92, Left=572, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oMPTIPMAT_2_8.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MPTIPMAT,&i_cF..t_MPTIPMAT),this.value)
    return(iif(xVal =1,'FA',;
    iif(xVal =2,'SC',;
    iif(xVal =3,'IN',;
    space(2)))))
  endfunc
  func oMPTIPMAT_2_8.GetRadio()
    this.Parent.oContained.w_MPTIPMAT = this.RadioValue()
    return .t.
  endfunc

  func oMPTIPMAT_2_8.ToRadio()
    this.Parent.oContained.w_MPTIPMAT=trim(this.Parent.oContained.w_MPTIPMAT)
    return(;
      iif(this.Parent.oContained.w_MPTIPMAT=='FA',1,;
      iif(this.Parent.oContained.w_MPTIPMAT=='SC',2,;
      iif(this.Parent.oContained.w_MPTIPMAT=='IN',3,;
      0))))
  endfunc

  func oMPTIPMAT_2_8.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oMPDATMAT_2_9 as StdTrsField with uid="XUERCGFIRF",rtseq=41,rtrep=.t.,;
    cFormVar="w_MPDATMAT",value=ctod("  /  /  "),;
    ToolTipText = "Data di maturazione selezionata",;
    HelpContextID = 163275546,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=665, Top=0

  add object oMPDATLIQ_2_10 as StdTrsField with uid="TNWAWKXSVN",rtseq=42,rtrep=.t.,;
    cFormVar="w_MPDATLIQ",value=ctod("  /  /  "),;
    ToolTipText = "Data di liquidazione selezionata",;
    HelpContextID = 121937129,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=72, Left=744, Top=0

  add object oMPFLSOSP_2_11 as StdTrsCheck with uid="JSCRRONCUQ",rtrep=.t.,;
    cFormVar="w_MPFLSOSP",  caption="",;
    ToolTipText = "Flag sospeso",;
    HelpContextID = 196510486,;
    Left=823, Top=0, Width=22,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.F.;
   , bGlobalFont=.t.


  func oMPFLSOSP_2_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..MPFLSOSP,&i_cF..t_MPFLSOSP),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oMPFLSOSP_2_11.GetRadio()
    this.Parent.oContained.w_MPFLSOSP = this.RadioValue()
    return .t.
  endfunc

  func oMPFLSOSP_2_11.ToRadio()
    this.Parent.oContained.w_MPFLSOSP=trim(this.Parent.oContained.w_MPFLSOSP)
    return(;
      iif(this.Parent.oContained.w_MPFLSOSP=='S',1,;
      0))
  endfunc

  func oMPFLSOSP_2_11.SetRadio()
    this.value=this.ToRadio()
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oMPDATSCA_2_1.When()
    return(.t.)
  proc oMPDATSCA_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oMPDATSCA_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_mpv','MOP_MAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".MPSERIAL=MOP_MAST.MPSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master/Detail"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
