* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_beg                                                        *
*              ESPLOSIONE CONTRIBUTI ACCESSORI                                 *
*                                                                              *
*      Author: Zucchetti - AT                                                  *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-10-29                                                      *
* Last revis.: 2011-12-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODART,pTIPCON,pCODCON,pDATRIF,pCODIVA,pUNIMIS,pQTAMOV,pPREZZO,pCODVAL,pPERCIVA,pUMFRAZ,pTIPESPL,pCURSORE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_beg",oParentObject,m.pCODART,m.pTIPCON,m.pCODCON,m.pDATRIF,m.pCODIVA,m.pUNIMIS,m.pQTAMOV,m.pPREZZO,m.pCODVAL,m.pPERCIVA,m.pUMFRAZ,m.pTIPESPL,m.pCURSORE)
return(i_retval)

define class tgsar_beg as StdBatch
  * --- Local variables
  pCODART = space(20)
  pTIPCON = space(1)
  pCODCON = space(15)
  pDATRIF = ctod("  /  /  ")
  pCODIVA = space(5)
  pUNIMIS = space(3)
  pQTAMOV = 0
  pPREZZO = 0
  pCODVAL = space(3)
  pPERCIVA = 0
  pUMFRAZ = space(1)
  pTIPESPL = space(3)
  pCURSORE = space(10)
  w_CATCONTR = space(5)
  w_TIPCONTR = space(15)
  w_PERAPP = 0
  w_IMPCONTR = 0
  w_SERVIZIO = space(20)
  w_CATSEMPL = space(15)
  w_TIPCAT = space(1)
  w_QTAMOV = 0
  w_UNIMIS = space(3)
  w_PESO = 0
  w_TIPCON = space(1)
  w_CODCON = space(15)
  w_APPLSUPESO = space(1)
  w_CODART = space(20)
  w_PESONETTO = 0
  w_PESOLORDO = 0
  w_PESONETTO2 = 0
  w_PESOLORDO2 = 0
  w_PESONETTO3 = 0
  w_PESOLORDO3 = 0
  w_UNIMIS1 = space(3)
  w_UNIMIS2 = space(3)
  w_UNIMIS3 = space(3)
  w_SISCOLLE = space(15)
  w_LIVELLODIDETTAGLIO = 0
  w_FLARID = space(1)
  w_MCPERAPP1 = 0
  w_MCFLARID1 = space(1)
  w_MCPERAPP2 = 0
  w_MCFLARID2 = space(1)
  w_MCPERAPP3 = 0
  w_MCFLARID3 = space(1)
  w_TPAPPPES = space(1)
  w_TPBASRIF = space(1)
  w_TPUNMIKG = space(3)
  w_CGPERCEN = 0
  w_CGIMPNET = 0
  w_CGIMPZER = space(1)
  w_UMFLFRAZ = space(1)
  * --- WorkFile variables
  CONTRART_idx=0
  CONTRINT_idx=0
  ART_ICOL_idx=0
  KEY_ARTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esplosione contributi accessori
    *     Parametri ingresso
    *     pCODART    Codice di ricerca
    *     pTIPCON
    *     pCODCON   Tipo e codice intestatario
    *     pDATRIF      Data documento, se assente data registrazione
    *     pCODIVA     Codice Iva
    *     pUNIMIS      Unit� di misura di riga
    *     pQTAMOV   Quantit� movimimentata di riga 
    *     pPREZZO   Prezzo movimimentata di riga (Necessario se presente la quota percentuale in alternativa all'importo netto iva)
    *     pCODVAL Valuta del documento
    *     pPERCIVA Percentuale IVA da applicare in antiscorporo
    *     pUMFRAZ Unit� di misura articolo padre frazionabile (se applico percentuale alla quantit�)
    *     pTIPESPL    Tipo di esplosione ('KIT' , 'ACC' per spese acc. senza app. peso, 'PES'  per spese acc. con appl. peso
    *     pCURSORE  Nome cursore
    *     
    *     Il cursore in uscita conterr� i seguenti dati
    *     - Codice servizio componente
    *     - Quantit�
    *     - unit� di misura
    *     - prezzo
    *     - codice IVA
    *     -categoria contributo
    * --- Categoria e tipo contributo
    * --- Percentuale applicazione contributo
    * --- Importo contributo
    * --- Servizio recuperato nelle categorie accessori (SEMPLICI)
    * --- Categorie semplice dettagliata nella cat.composta associata al codice articolo
    * --- Tipo categoria
    * --- Calcolo del peso in caso di attivazione su peso
    * --- Tipo e codice conto
    this.w_TIPCON = NVL( this.pTIPCON , " " )
    this.w_CODCON = NVL( this.pCODCON , "               " )
    if EMPTY( this.w_CODCON )
      * --- Documenti senza intestatario, legati all'archivio CONTROPA tramite i campi
      *     COTIPCOA = '0' E COCODCOA='000000000000000'
      this.w_TIPCON = "0"
      this.w_CODCON = "000000000000000"
    endif
    if this.pTIPESPL = "KIT"
      * --- Esplosione componenti del kit
      vq_exec("query\GSAR_KIT", this, this.pCURSORE)
    else
      this.w_APPLSUPESO = IIF(this.pTIPESPL="PES","S","N")
      * --- Cursore di output contenente i servizi collegati al contributo
      Create Cursor (this.pCURSORE) (TIPO C(3), CODICE C(20), QTAMOV N(12,3), UNIMIS C(3), PREZZO N(18,5), CODIVA C(5), CATCON C(5)) 
      * --- Dato l'articolo recupero le categorie contributo associate e quindi i servizi da esplodere sulle righe
      this.w_CATCONTR = REPL("#",8)
      * --- Leggo, dato il codice di ricerca, l'articolo per filtrare sulla query..
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CACODART,CAPESNE3,CAPESLO3,CAUNIMIS"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.pCodArt);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CACODART,CAPESNE3,CAPESLO3,CAUNIMIS;
          from (i_cTable) where;
              CACODICE = this.pCodArt;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CODART = NVL(cp_ToDate(_read_.CACODART),cp_NullValue(_read_.CACODART))
        this.w_PESONETTO3 = NVL(cp_ToDate(_read_.CAPESNE3),cp_NullValue(_read_.CAPESNE3))
        this.w_PESOLORDO3 = NVL(cp_ToDate(_read_.CAPESLO3),cp_NullValue(_read_.CAPESLO3))
        this.w_UNIMIS3 = NVL(cp_ToDate(_read_.CAUNIMIS),cp_NullValue(_read_.CAUNIMIS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Se categoria semplice la query estrae gi� il dettaglio valido per UM e date validit� (la data di fine � sempre ricalcolata a partire dalla data di inizio del record precedente
      *     Se categoria composta occorre ciclare su tutte le categorie semplici specificate (NESSUN FILTRO SULLA DATA)
      * --- Select from GSAR_BEG
      do vq_exec with 'GSAR_BEG',this,'_Curs_GSAR_BEG','',.f.,.t.
      if used('_Curs_GSAR_BEG')
        select _Curs_GSAR_BEG
        locate for 1=1
        do while not(eof())
        if this.w_CATCONTR = REPL("#",8)
          * --- Legge il peso netto e lordo dall'an. articolo
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARPESLOR,ARPESNET,ARPESNE2,ARPESLO2,ARUNMIS1,ARUNMIS2"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_CODART);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARPESLOR,ARPESNET,ARPESNE2,ARPESLO2,ARUNMIS1,ARUNMIS2;
              from (i_cTable) where;
                  ARCODART = this.w_CODART;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_PESOLORDO = NVL(cp_ToDate(_read_.ARPESLOR),cp_NullValue(_read_.ARPESLOR))
            this.w_PESONETTO = NVL(cp_ToDate(_read_.ARPESNET),cp_NullValue(_read_.ARPESNET))
            this.w_PESONETTO2 = NVL(cp_ToDate(_read_.ARPESNE2),cp_NullValue(_read_.ARPESNE2))
            this.w_PESOLORDO2 = NVL(cp_ToDate(_read_.ARPESLO2),cp_NullValue(_read_.ARPESLO2))
            this.w_UNIMIS1 = NVL(cp_ToDate(_read_.ARUNMIS1),cp_NullValue(_read_.ARUNMIS1))
            this.w_UNIMIS2 = NVL(cp_ToDate(_read_.ARUNMIS2),cp_NullValue(_read_.ARUNMIS2))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Determino il peso netto / lordo corretto in base all'unit� di misura
          do case
            case this.w_UNIMIS1= this.pUnimis
              * --- Tutto ok, gia lette le variabili giuste
            case this.w_UNIMIS2= this.pUnimis
              this.w_PESONETTO = this.w_PESONETTO2
              this.w_PESOLORDO = this.w_PESOLORDO2
            case this.w_UNIMIS3= this.pUnimis
              this.w_PESONETTO = this.w_PESONETTO3
              this.w_PESOLORDO = this.w_PESOLORDO3
            otherwise
              this.w_PESONETTO = 0
              this.w_PESOLORDO = 0
          endcase
        endif
        * --- La categoria e il tipo contributo accessori sono cambiati (sar� solo per i composti)
        *     (� sufficiente testare la categoria, chiave primaria dell'archivio CATMCONT)
        *     La routine preleva sempre la prima riga del contributo, eventuali altre righe
        *     sono escluse a parit� di contributo
        if this.w_CATCONTR <> _Curs_GSAR_BEG.CGCODICE 
          * --- Dati contributo
          this.w_CATCONTR = _Curs_GSAR_BEG.CGCODICE 
          this.w_TIPCONTR = _Curs_GSAR_BEG.CGCONTRI 
          * --- Tipo categoria Semplice/Composto
          this.w_TIPCAT = _Curs_GSAR_BEG.CGTIPCAT
          * --- Recupero dal Cliente/Fornitore la percentuale di applicazione del contributo
          *     Il contributo dettagliato o calcolato viene moltiplicato per la % di applicazione specificata sull' intestatario (100% default)
          this.w_SISCOLLE = _Curs_GSAR_BEG.CGSISCOL
          this.w_LIVELLODIDETTAGLIO = 0
          this.w_FLARID = ""
          this.w_MCPERAPP1 = 0
          this.w_MCFLARID1 = ""
          this.w_MCPERAPP2 = 0
          this.w_MCFLARID2 = ""
          this.w_MCPERAPP3 = 0
          this.w_MCFLARID3 = ""
          this.w_PERAPP = 0
          * --- Ricerco nel dettaglio contributi clienti/fornitori se la categoria � ammissibile
          * --- Select from CONTRINT
          i_nConn=i_TableProp[this.CONTRINT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTRINT_idx,2],.t.,this.CONTRINT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" CONTRINT ";
                +" where MCTIPINT = "+cp_ToStrODBC(this.w_TIPCON)+" AND MCCODINT = "+cp_ToStrODBC(this.w_CODCON)+" AND MCTIPCAT = "+cp_ToStrODBC(this.w_TIPCONTR)+" And(MCCODSIS = "+cp_ToStrODBC(this.w_SISCOLLE)+" Or MCCODSIS is null)And(MCCODCAT = "+cp_ToStrODBC(this.w_CATCONTR)+" Or MCCODCAT is null)And "+cp_ToStrODBC(this.pDATRIF)+" >= MCDATATT AND "+cp_ToStrODBC(this.pDATRIF)+" <= MCDATSCA";
                 ,"_Curs_CONTRINT")
          else
            select * from (i_cTable);
             where MCTIPINT = this.w_TIPCON AND MCCODINT = this.w_CODCON AND MCTIPCAT = this.w_TIPCONTR And(MCCODSIS = this.w_SISCOLLE Or MCCODSIS is null)And(MCCODCAT = this.w_CATCONTR Or MCCODCAT is null)And this.pDATRIF >= MCDATATT AND this.pDATRIF <= MCDATSCA;
              into cursor _Curs_CONTRINT
          endif
          if used('_Curs_CONTRINT')
            select _Curs_CONTRINT
            locate for 1=1
            do while not(eof())
            * --- Cerca i dati da applicare dando la preferenza nel seguente ordine:
            *     -1
            *     CONTRINT->MCTIPCAT=w_TIPCONTR AND CONTRINT->MCCODSIS=w_SISCOLLE AND CONTRINT->MCCODCAT=w_CATCONTR
            *     Corrispondono il tipo contributo, il sistema collettivo e la categoria contributo
            *     -2
            *     CONTRINT->MCTIPCAT=w_TIPCONTR AND CONTRINT->MCCODSIS=w_SISCOLLE AND EMPTY(NVL(CONTRINT->MCCODCAT,""))
            *     Corrispondono il tipo contributo e il sistema collettivo
            *     -3
            *     CONTRINT->MCTIPCAT=w_TIPCONTR AND EMPTY(NVL(CONTRINT->MCCODSIS,"")) AND EMPTY(NVL(CONTRINT->MCCODCAT,""))
            *     Corrisponde solo il tipo contributo 
            do case
              case _Curs_CONTRINT.MCTIPCAT=this.w_TIPCONTR AND _Curs_CONTRINT.MCCODSIS=this.w_SISCOLLE AND _Curs_CONTRINT.MCCODCAT=this.w_CATCONTR
                this.w_LIVELLODIDETTAGLIO = 1
                this.w_MCPERAPP1 = _Curs_CONTRINT.MCPERAPP
                this.w_MCFLARID1 = _Curs_CONTRINT.MCFLARID
                exit
              case _Curs_CONTRINT.MCTIPCAT=this.w_TIPCONTR AND _Curs_CONTRINT.MCCODSIS=this.w_SISCOLLE AND EMPTY(NVL(_Curs_CONTRINT.MCCODCAT,""))
                this.w_LIVELLODIDETTAGLIO = 2
                this.w_MCPERAPP2 = _Curs_CONTRINT.MCPERAPP
                this.w_MCFLARID2 = _Curs_CONTRINT.MCFLARID
              case _Curs_CONTRINT.MCTIPCAT=this.w_TIPCONTR AND EMPTY(NVL(_Curs_CONTRINT.MCCODSIS,"")) AND EMPTY(NVL(_Curs_CONTRINT.MCCODCAT,""))
                this.w_LIVELLODIDETTAGLIO = 3
                this.w_MCPERAPP3 = _Curs_CONTRINT.MCPERAPP
                this.w_MCFLARID3 = _Curs_CONTRINT.MCFLARID
            endcase
              select _Curs_CONTRINT
              continue
            enddo
            use
          endif
          do case
            case this.w_LIVELLODIDETTAGLIO=1
              this.w_PERAPP = this.w_MCPERAPP1
              this.w_FLARID = this.w_MCFLARID1
            case this.w_LIVELLODIDETTAGLIO=2
              this.w_PERAPP = this.w_MCPERAPP2
              this.w_FLARID = this.w_MCFLARID2
            case this.w_LIVELLODIDETTAGLIO=3
              this.w_PERAPP = this.w_MCPERAPP3
              this.w_FLARID = this.w_MCFLARID3
          endcase
          * --- Se la categoria � ammissibile anche per il cliente la valuto...
          if this.w_LIVELLODIDETTAGLIO>0
            this.w_UMFLFRAZ = _Curs_GSAR_BEG.UMFLFRAZ
            if this.w_TIPCAT = "S" 
              * --- Categoria semplice, recupero direttamente informazioni necessarie (servizio, prezzo...)
              this.w_SERVIZIO = _Curs_GSAR_BEG.CGSERVIZ 
              this.w_TPAPPPES = _Curs_GSAR_BEG.TPAPPPES
              this.w_TPBASRIF = _Curs_GSAR_BEG.TPBASRIF
              this.w_TPUNMIKG = _Curs_GSAR_BEG.TPUNMIKG
              this.w_CGPERCEN = _Curs_GSAR_BEG.CGPERCEN
              this.w_CGIMPNET = _Curs_GSAR_BEG.CGIMPNET
              this.w_CGIMPZER = _Curs_GSAR_BEG.CGIMPZER
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            else
              * --- Se l'articolo ha associato una categoria di tipo "composto" recupera 
              *     le categorie semplici collegate a quella composta e le applica 
              *     singolarmente con le medesime  regole (u.m. ecc).
              this.w_CATSEMPL = _Curs_GSAR_BEG.CGCATSEM 
              * --- Se categoria composta devo cambiare i termini della rottura
              this.w_CATCONTR = this.w_CATSEMPL
              * --- Select from GSARSBEG
              do vq_exec with 'GSARSBEG',this,'_Curs_GSARSBEG','',.f.,.t.
              if used('_Curs_GSARSBEG')
                select _Curs_GSARSBEG
                locate for 1=1
                do while not(eof())
                * --- Se categoria semplice la query estrae gi� il dettaglio valido per UM e date validit� (la data di fine � sempre ricalcolata a partire dalla data di inizio del record precedente
                *     Se categoria composta occorre ciclare su tutte le categorie semplici specificate (NESSUN FILTRO SULLA DATA)
                * --- Categoria semplice, recupero informazioni necessarie (servizio, prezzo...)
                this.w_SERVIZIO = _Curs_GSARSBEG.CGSERVIZ 
                this.w_TPAPPPES = _Curs_GSARSBEG.TPAPPPES
                this.w_TPBASRIF = _Curs_GSARSBEG.TPBASRIF
                this.w_TPUNMIKG = _Curs_GSARSBEG.TPUNMIKG
                this.w_CGPERCEN = _Curs_GSARSBEG.CGPERCEN
                this.w_CGIMPNET = _Curs_GSARSBEG.CGIMPNET
                this.w_CGIMPZER = _Curs_GSARSBEG.CGIMPZER
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                  select _Curs_GSARSBEG
                  continue
                enddo
                use
              endif
            endif
          endif
        endif
          select _Curs_GSAR_BEG
          continue
        enddo
        use
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- La quantit� deve essere calcolata in base al rapporto con KG nel caso di applicazione su peso
    if this.w_TPAPPPES = "S"
      do case
        case this.w_TPBASRIF = "N"
          * --- Converte in base al peso netto
          this.w_PESO = this.w_PESONETTO
        case this.w_TPBASRIF = "L"
          * --- Converte in base al peso lordo
          this.w_PESO = this.w_PESOLORDO
        case this.w_TPBASRIF = "T"
          * --- Converte in base alla tara
          this.w_PESO = this.w_PESOLORDO - this.w_PESONETTO
      endcase
      * --- Se il peso per U.M. � 0, la quantit� non viene convertita
      if this.w_PESO <> 0
        this.w_QTAMOV = this.pQTAMOV * this.w_PESO
      else
        this.w_QTAMOV = 0
      endif
      this.w_UNIMIS = this.w_TPUNMIKG
    else
      this.w_QTAMOV = this.pQTAMOV
      this.w_UNIMIS = this.pUNIMIS
      * --- Se riporto l'unit� di misura del padre la frazionabilit� o meno la determino in base al parametro
      this.w_UMFLFRAZ = this.pUMFRAZ
    endif
    do case
      case this.w_CGPERCEN <> 0
        * --- Se la categoria dettaglia la % e non l'importo del contributo il calcolo avviene ->%*prezzo articolo (netto di riga).
        this.w_IMPCONTR = cp_ROUND( this.pPREZZO * this.w_CGPERCEN/100, 5)
      case this.w_CGIMPNET<> 0
        * --- La categoria dettaglia direttamente l'importo
        this.w_IMPCONTR = this.w_CGIMPNET 
        * --- Applica la percentuale di iva (prezzi nelle categorie sempre al netto di IVA se il documento ha il flag di scorporo)
        if this.pPERCIVA <> 0
          this.w_IMPCONTR = this.w_IMPCONTR * ( 1 + this.pPERCIVA/100 )
        endif
      otherwise
        this.w_IMPCONTR = 0
    endcase
    * --- Il contributo dettagliato o calcolato viene moltiplicato per la % di applicazione specificata sull' intestatario (100% default)
    if this.w_PERAPP<>100
      do case
        case this.w_FLARID="Q"
          if this.w_CGIMPZER <> "S"
            this.w_QTAMOV = cp_ROUND( this.w_QTAMOV * this.w_PERAPP/100, 5)
          else
            * --- Nel caso di riduzione a quantit�, se la percentuale fosse 0 la riga scomparirebbe
            this.w_QTAMOV = 1
          endif
        case this.w_FLARID="I" Or this.w_FLARID="N"
          this.w_IMPCONTR = cp_ROUND( this.w_IMPCONTR * this.w_PERAPP/100, 5)
      endcase
    endif
    * --- Ad ogni modo arrotondo la quantit�...
    if this.w_UMFLFRAZ="S"
      * --- Se non frazionabile all'intero...
      this.w_QTAMOV = INT( this.w_QTAMOV )
    else
      * --- Se frazionabile arrotondo in base a quanto specificato in azienda...
      this.w_QTAMOV = cp_ROUND( this.w_QTAMOV, g_PERPQT)
    endif
    * --- Inserimento dettaglio nel cursore di output
    *     Se qtamov a 0 significa che ho un contributo a peso che non ha
    *     nell'unit� di misura del documento i pesi in Kg per cui
    *     non creo il contributo.
    *     Se la cosa pu� dare problemi occorrerr� gestire la msg di ritorno per
    *     gestire il caso ed eventualmente marcare la riga come non valida...
    if this.w_QTAMOV<>0
      Insert into (this.pCURSORE) Values (this.pTIPESPL,this.w_SERVIZIO, this.w_QTAMOV, this.w_UNIMIS, this.w_IMPCONTR, this.pCODIVA,this.w_CATCONTR)
    endif
  endproc


  proc Init(oParentObject,pCODART,pTIPCON,pCODCON,pDATRIF,pCODIVA,pUNIMIS,pQTAMOV,pPREZZO,pCODVAL,pPERCIVA,pUMFRAZ,pTIPESPL,pCURSORE)
    this.pCODART=pCODART
    this.pTIPCON=pTIPCON
    this.pCODCON=pCODCON
    this.pDATRIF=pDATRIF
    this.pCODIVA=pCODIVA
    this.pUNIMIS=pUNIMIS
    this.pQTAMOV=pQTAMOV
    this.pPREZZO=pPREZZO
    this.pCODVAL=pCODVAL
    this.pPERCIVA=pPERCIVA
    this.pUMFRAZ=pUMFRAZ
    this.pTIPESPL=pTIPESPL
    this.pCURSORE=pCURSORE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CONTRART'
    this.cWorkTables[2]='CONTRINT'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='KEY_ARTI'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_GSAR_BEG')
      use in _Curs_GSAR_BEG
    endif
    if used('_Curs_CONTRINT')
      use in _Curs_CONTRINT
    endif
    if used('_Curs_GSARSBEG')
      use in _Curs_GSARSBEG
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODART,pTIPCON,pCODCON,pDATRIF,pCODIVA,pUNIMIS,pQTAMOV,pPREZZO,pCODVAL,pPERCIVA,pUMFRAZ,pTIPESPL,pCURSORE"
endproc
