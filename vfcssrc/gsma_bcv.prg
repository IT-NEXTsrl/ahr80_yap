* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bcv                                                        *
*              Crea i codici di ricerca artic                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_87]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-15                                                      *
* Last revis.: 2018-07-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bcv",oParentObject,m.pOper)
return(i_retval)

define class tgsma_bcv as StdBatch
  * --- Local variables
  pOper = space(1)
  w_OPER = space(1)
  w_AUTOBAR = .f.
  w_TIPCON = space(1)
  w_TROV = .f.
  w_CODCON = space(15)
  w_CODBAR = space(10)
  w___TIPO = space(1)
  w_KEYBAR = space(41)
  w_CODKEY = space(20)
  w_MESS = space(10)
  w_DESART = space(40)
  w_DESSUP = space(10)
  w_OKINS = .f.
  w_ERROR = space(100)
  w_MAXCOD = 0
  w_TmpN = 0
  w_OLDBAR = space(41)
  w_CONTA = 0
  w_OLDESART = space(20)
  w_OLDESSUP = space(0)
  w_OLDTINVA = ctod("  /  /  ")
  w_OLDTOBSO = ctod("  /  /  ")
  w_DESCRI = space(20)
  w_DESSU = space(0)
  w_DTINVA = ctod("  /  /  ")
  w_DTOBSO = ctod("  /  /  ")
  w_OLDSALCOM = space(1)
  w_MOD = .f.
  w_DAT = .f.
  w_MSG = space(20)
  w_MOD1 = .f.
  w_MOD2 = .f.
  w_CONTA1 = 0
  w_CODI = space(10)
  w_DESCRI2 = space(20)
  w_DESSU2 = space(0)
  w_DTINVA2 = ctod("  /  /  ")
  w_DTOBSO2 = ctod("  /  /  ")
  w_CATIPBAR = space(1)
  w_CAFLSTAM = space(1)
  w_CAOPERAT = space(1)
  w_CAMOLTIP = 0
  w_APPARCODART = space(20)
  * --- WorkFile variables
  KEY_ARTI_idx=0
  ART_ICOL_idx=0
  DISTBASE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Crea/Aggiorna le Chiavi di Ricerca (Lanciato da GSMA_AAR, GSMA_AAS)
    * --- I = Insert End; U = Update Start; P = Altro Caso
    this.w_OPER = this.pOper
    if this.w_OPER="B" AND this.oParentObject.w_ARTIPBAR<>"0"
      This.OparentObject.NotifyEvent("CaricaBar")
    endif
    this.w_MESS = ah_msgformat("Transazione abbandonata")
    * --- Inizializza Campi da Aggiornare in KEY_ARTI
    this.w_TIPCON = "R"
    this.w_CODCON = SPACE(15)
    this.w___TIPO = "R"
    this.w_CODKEY = this.oParentObject.w_ARCODART
    this.w_DESART = this.oParentObject.w_ARDESART
    this.w_DESSUP = this.oParentObject.w_ARDESSUP
    this.w_TROV = .T.
    this.w_AUTOBAR = .F.
    do case
      case this.oParentObject.w_ARTIPART="AC"
        * --- Articolo Composto
        this.w_TIPCON = "A"
        this.w___TIPO = "A"
      case this.oParentObject.w_ARTIPART="FO"
        * --- Servizio Forfait
        this.w_TIPCON = "I"
        this.w___TIPO = "F"
      case this.oParentObject.w_ARTIPART="FM"
        * --- Servizio Fuori Magazzino
        this.w_TIPCON = "M"
        this.w___TIPO = "M"
      case this.oParentObject.w_ARTIPART="DE"
        * --- Servizio Descrittivo
        this.w_TIPCON = "D"
        this.w___TIPO = "D"
    endcase
    do case
      case this.w_OPER="P"
        * --- Caso Particolare P refresh sui Dati Articolo/Magazzini al cambio tipo Gestione, Provenuienza, Distinta
        this.oParentObject.GSMA_APR.mCalc(.T.)
        this.oParentObject.GSMA_APR.GSMA_MPR.mCalc(.T.)
        i_retcode = 'stop'
        return
      case this.w_OPER="U"
        * --- Verifico quali dati Articolo ho variato effettivamente
        * --- Read from KEY_ARTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CADESART,CADESSUP,CADTINVA,CADTOBSO"+;
            " from "+i_cTable+" KEY_ARTI where ";
                +"CACODICE = "+cp_ToStrODBC(this.w_CODKEY);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CADESART,CADESSUP,CADTINVA,CADTOBSO;
            from (i_cTable) where;
                CACODICE = this.w_CODKEY;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_DESCRI = NVL(cp_ToDate(_read_.CADESART),cp_NullValue(_read_.CADESART))
          this.w_DESSU = NVL(cp_ToDate(_read_.CADESSUP),cp_NullValue(_read_.CADESSUP))
          this.w_DTINVA = NVL(cp_ToDate(_read_.CADTINVA),cp_NullValue(_read_.CADTINVA))
          this.w_DTOBSO = NVL(cp_ToDate(_read_.CADTOBSO),cp_NullValue(_read_.CADTOBSO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARDESART,ARDESSUP,ARDTINVA,ARDTOBSO,ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_ARCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARDESART,ARDESSUP,ARDTINVA,ARDTOBSO,ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.oParentObject.w_ARCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLDESART = NVL(cp_ToDate(_read_.ARDESART),cp_NullValue(_read_.ARDESART))
          this.w_OLDESSUP = NVL(cp_ToDate(_read_.ARDESSUP),cp_NullValue(_read_.ARDESSUP))
          this.w_OLDTINVA = NVL(cp_ToDate(_read_.ARDTINVA),cp_NullValue(_read_.ARDTINVA))
          this.w_OLDTOBSO = NVL(cp_ToDate(_read_.ARDTOBSO),cp_NullValue(_read_.ARDTOBSO))
          this.w_OLDSALCOM = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- uso la condizione Not == per evitare problemi su stringhe vuote
        if Not this.w_OLDESART==this.w_DESART
          this.w_DESCRI = this.w_DESART
        endif
        if Not this.w_OLDESSUP==this.w_DESSUP
          this.w_DESSU = this.w_DESSUP
        endif
        if this.w_OLDTINVA<>this.oParentObject.w_ARDTINVA
          this.w_DTINVA = this.oParentObject.w_ARDTINVA
        endif
        if this.w_OLDTOBSO <> this.oParentObject.w_ARDTOBSO
          this.w_DTOBSO = this.oParentObject.w_ARDTOBSO
        endif
        if Not this.w_OLDSALCOM==this.oParentObject.w_ARSALCOM
          this.w_MSG = "Attenzione! A seguito della modifica del flag sar� necessario eseguire la procedura di ricostruzione dei saldi di magazzino"
          ah_ErrorMsg(this.w_MSG,64,"")
        endif
        * --- Sono in Variazione , proseguo solo se modifico dati dei Codici
        * --- Effettuo l'aggiornamento dei codici di ricerca solo nel caso in cui uno dei
        *     dati presenti in anagrafica sia stato modificato
        if this.oParentObject.w_OFATELE<>this.oParentObject.w_NFATELE
          * --- Write into KEY_ARTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CACODTIP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODTIP),'KEY_ARTI','CACODTIP');
            +",CACODVAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODVAL),'KEY_ARTI','CACODVAL');
            +",CACODCLF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODCLF),'KEY_ARTI','CACODCLF');
            +",CAFLGESC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARFLGESC),'KEY_ARTI','CAFLGESC');
                +i_ccchkf ;
            +" where ";
                +"CACODART = "+cp_ToStrODBC(this.oParentObject.w_ARCODART);
                   )
          else
            update (i_cTable) set;
                CACODTIP = this.oParentObject.w_ARCODTIP;
                ,CACODVAL = this.oParentObject.w_ARCODVAL;
                ,CACODCLF = this.oParentObject.w_ARCODCLF;
                ,CAFLGESC = this.oParentObject.w_ARFLGESC;
                &i_ccchkf. ;
             where;
                CACODART = this.oParentObject.w_ARCODART;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        if this.oParentObject.w_OMODKEY<>this.oParentObject.w_NMODKEY OR this.oParentObject.w_ODATKEY<>this.oParentObject.w_NDATKEY
          this.w_CONTA = 0
          * --- Aggiorno sempre il Codice di ricerca uguale al Codice Articolo
          * --- Try
          local bErr_054A12A0
          bErr_054A12A0=bTrsErr
          this.Try_054A12A0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_054A12A0
          * --- End
          * --- Cerco se in KEY_ARTI ci sono altri codici associati all'articolo. Se ci sono lancio il messaggio.
          * --- Select from KEY_ARTI
          i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select CACODICE,CACODART  from "+i_cTable+" KEY_ARTI ";
                +" where CACODICE<>"+cp_ToStrODBC(this.oParentObject.w_ARCODART)+" AND CACODART="+cp_ToStrODBC(this.oParentObject.w_ARCODART)+"";
                 ,"_Curs_KEY_ARTI")
          else
            select CACODICE,CACODART from (i_cTable);
             where CACODICE<>this.oParentObject.w_ARCODART AND CACODART=this.oParentObject.w_ARCODART;
              into cursor _Curs_KEY_ARTI
          endif
          if used('_Curs_KEY_ARTI')
            select _Curs_KEY_ARTI
            locate for 1=1
            do while not(eof())
            this.w_CONTA = this.w_CONTA+1
              select _Curs_KEY_ARTI
              continue
            enddo
            use
          endif
          if this.w_CONTA>0
            if this.oParentObject.w_OMODKEY<>this.oParentObject.w_NMODKEY
              if ah_YesNo("Attenzione! Verr� aggiornato il codice di ricerca principale. Aggiorno anche gli altri codici di ricerca associati all'articolo?")
                this.w_MOD = .T.
              endif
            endif
            if this.oParentObject.w_ODATKEY<>this.oParentObject.w_NDATKEY
              if this.w_MOD
                this.w_MSG = "Aggiorno anche la data di obsolescenza dei codici di ricerca?"
              else
                this.w_MSG = "Attenzione! Verr� aggiornato il codice di ricerca principale. Aggiorno la data di obsolescenza anche degli altri codici di ricerca?"
              endif
              if ah_YesNo(this.w_MSG)
                this.w_DAT = .T.
              endif
            endif
          endif
          if g_DISB="S"
            * --- Controllo se ci sono componenti in distinta con lo stesso codice di ricerca base da aggiornare
            this.w_CONTA = 0
            * --- Select from DISTBASE
            i_nConn=i_TableProp[this.DISTBASE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2],.t.,this.DISTBASE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select COUNT(DBCODCOM) AS NUMCOMP  from "+i_cTable+" DISTBASE ";
                  +" where DBCODCOM = "+cp_ToStrODBC(this.w_CODKEY)+"";
                   ,"_Curs_DISTBASE")
            else
              select COUNT(DBCODCOM) AS NUMCOMP from (i_cTable);
               where DBCODCOM = this.w_CODKEY;
                into cursor _Curs_DISTBASE
            endif
            if used('_Curs_DISTBASE')
              select _Curs_DISTBASE
              locate for 1=1
              do while not(eof())
              this.w_CONTA = _Curs_DISTBASE.NUMCOMP
                select _Curs_DISTBASE
                continue
              enddo
              use
            endif
            if this.w_CONTA > 0
              if ah_YesNo("Attenzione! Sono presenti componenti in distinta base riferiti al codice di ricerca principale%0Aggiorno anche la descrizione dei componenti in distinta base?")
                this.w_MOD1 = .T.
              endif
            endif
            if this.w_MOD AND this.w_MOD1
              this.w_CONTA1 = 0
              * --- Select from DISTBASE
              i_nConn=i_TableProp[this.DISTBASE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2],.t.,this.DISTBASE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select COUNT(DBCODCOM) AS NUMCOMP  from "+i_cTable+" DISTBASE ";
                    +" where DBCODCOM <> "+cp_ToStrODBC(this.w_CODKEY)+" and DBARTCOM = "+cp_ToStrODBC(this.w_CODKEY)+"";
                     ,"_Curs_DISTBASE")
              else
                select COUNT(DBCODCOM) AS NUMCOMP from (i_cTable);
                 where DBCODCOM <> this.w_CODKEY and DBARTCOM = this.w_CODKEY;
                  into cursor _Curs_DISTBASE
              endif
              if used('_Curs_DISTBASE')
                select _Curs_DISTBASE
                locate for 1=1
                do while not(eof())
                this.w_CONTA1 = this.w_CONTA1+1
                  select _Curs_DISTBASE
                  continue
                enddo
                use
              endif
              if this.w_CONTA1>0
                if ah_YesNo("Attenzione! Sono presenti componenti in distinta base riferiti a codici di ricerca diversi  da quello principale%0Aggiorno anche la descrizione dei componenti in distinta base?")
                  this.w_MOD2 = .T.
                endif
              endif
            endif
          endif
        else
          i_retcode = 'stop'
          return
        endif
      case this.w_OPER="I" AND this.oParentObject.w_ARTIPBAR $ "12"
        * --- Per ora Gestiti i soli Codici EAN8 / EAN13
        this.w_AUTOBAR = ah_YesNo("Si vuole generare anche i codici a barre?")
    endcase
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_TROV = .T.
    if Not Empty ( this.w_ERROR )
      ah_ErrorMsg(this.w_ERROR,"!","",this.w_APPARCODART)
    endif
    if this.w_TROV=.F. AND this.w_OPER<>"U"
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc
  proc Try_054A12A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into KEY_ARTI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"CADESART ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'KEY_ARTI','CADESART');
      +",CADESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_DESSU),'KEY_ARTI','CADESSUP');
      +",CADTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_DTINVA),'KEY_ARTI','CADTINVA');
      +",CADTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_DTOBSO),'KEY_ARTI','CADTOBSO');
      +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'KEY_ARTI','UTCV');
      +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'KEY_ARTI','UTDV');
          +i_ccchkf ;
      +" where ";
          +"CACODICE = "+cp_ToStrODBC(this.w_CODKEY);
             )
    else
      update (i_cTable) set;
          CADESART = this.w_DESCRI;
          ,CADESSUP = this.w_DESSU;
          ,CADTINVA = this.w_DTINVA;
          ,CADTOBSO = this.w_DTOBSO;
          ,UTCV = i_CODUTE;
          ,UTDV = SetInfoDate(g_CALUTD);
          &i_ccchkf. ;
       where;
          CACODICE = this.w_CODKEY;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrittura dei Codici
    if this.w_OPER="U"
      * --- Se Variazione Aggiorno i campi suscettibili di Modifica anche sui Codici di Ricerca
      * --- Select from KEY_ARTI
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CACODICE, CADESART, CADESSUP, CADTINVA, CADTOBSO  from "+i_cTable+" KEY_ARTI ";
            +" where CACODART="+cp_ToStrODBC(this.w_CODKEY)+"";
             ,"_Curs_KEY_ARTI")
      else
        select CACODICE, CADESART, CADESSUP, CADTINVA, CADTOBSO from (i_cTable);
         where CACODART=this.w_CODKEY;
          into cursor _Curs_KEY_ARTI
      endif
      if used('_Curs_KEY_ARTI')
        select _Curs_KEY_ARTI
        locate for 1=1
        do while not(eof())
        this.w_CODI = NVL(_Curs_KEY_ARTI.CACODICE,SPACE(10))
        this.w_DESCRI2 = NVL(_Curs_KEY_ARTI.CADESART,SPACE(20))
        this.w_DESSU2 = NVL(_Curs_KEY_ARTI.CADESSUP,SPACE(10))
        this.w_DTINVA2 = NVL(_Curs_KEY_ARTI.CADTINVA,cp_CharToDate("  -  -    "))
        this.w_DTOBSO2 = NVL(_Curs_KEY_ARTI.CADTOBSO,cp_CharToDate("  -  -    "))
        if Not this.w_OLDESART==this.w_DESART
          this.w_DESCRI2 = this.w_DESART
        endif
        if Not this.w_OLDESSUP==this.w_DESSUP
          this.w_DESSU2 = this.w_DESSUP
        endif
        if this.w_OLDTINVA<>this.oParentObject.w_ARDTINVA
          this.w_DTINVA2 = this.oParentObject.w_ARDTINVA
        endif
        if this.w_DTOBSO2 <> this.oParentObject.w_ARDTOBSO
          this.w_DTOBSO2 = this.oParentObject.w_ARDTOBSO
        endif
        * --- Try
        local bErr_05483980
        bErr_05483980=bTrsErr
        this.Try_05483980()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_05483980
        * --- End
          select _Curs_KEY_ARTI
          continue
        enddo
        use
      endif
      * --- Try
      local bErr_054869E0
      bErr_054869E0=bTrsErr
      this.Try_054869E0()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_054869E0
      * --- End
    else
      * --- Modifica Barcode o Inserimento...
      * --- Se Inserimento, Scrive il Nuovo Codice di Ricerca
      * --- Verifico che il codice di ricerca non sia gi� presente e modificato
      * --- Read from KEY_ARTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2],.t.,this.KEY_ARTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CATIPBAR,CAFLSTAM,CAOPERAT,CAMOLTIP"+;
          " from "+i_cTable+" KEY_ARTI where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_CODKEY);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CATIPBAR,CAFLSTAM,CAOPERAT,CAMOLTIP;
          from (i_cTable) where;
              CACODICE = this.w_CODKEY;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CATIPBAR = NVL(cp_ToDate(_read_.CATIPBAR),cp_NullValue(_read_.CATIPBAR))
        this.w_CAFLSTAM = NVL(cp_ToDate(_read_.CAFLSTAM),cp_NullValue(_read_.CAFLSTAM))
        this.w_CAOPERAT = NVL(cp_ToDate(_read_.CAOPERAT),cp_NullValue(_read_.CAOPERAT))
        this.w_CAMOLTIP = NVL(cp_ToDate(_read_.CAMOLTIP),cp_NullValue(_read_.CAMOLTIP))
        use
        if i_Rows=0
          this.w_CATIPBAR = iif(this.oParentObject.w_ARTIPBAR="3" and this.oParentObject.w_TIPBAR<>"0",this.oParentObject.w_TIPBAR,"0")
          this.w_CAFLSTAM = iif(this.oParentObject.w_ARTIPBAR="3" and this.oParentObject.w_TIPBAR<>"0","S"," ")
          this.w_CAOPERAT = "*"
          this.w_CAMOLTIP = 1
        endif
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.oParentObject.w_ARTIPBAR="3" and this.oParentObject.w_TIPBAR<>"0"
        * --- Codice Articolo Barcode
        if i_rows=0
          * --- Try
          local bErr_054555F8
          bErr_054555F8=bTrsErr
          this.Try_054555F8()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_054555F8
          * --- End
        endif
        * --- Write into KEY_ARTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CADESART ="+cp_NullLink(cp_ToStrODBC(this.w_DESART),'KEY_ARTI','CADESART');
          +",CADESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'KEY_ARTI','CADESSUP');
          +",CACODART ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODART),'KEY_ARTI','CACODART');
          +",CATIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'KEY_ARTI','CATIPCON');
          +",CA__TIPO ="+cp_NullLink(cp_ToStrODBC(this.w___TIPO),'KEY_ARTI','CA__TIPO');
          +",CATIPBAR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_TIPBAR),'KEY_ARTI','CATIPBAR');
          +",CAFLSTAM ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLSTAM),'KEY_ARTI','CAFLSTAM');
          +",CAOPERAT ="+cp_NullLink(cp_ToStrODBC(this.w_CAOPERAT),'KEY_ARTI','CAOPERAT');
          +",CAMOLTIP ="+cp_NullLink(cp_ToStrODBC(this.w_CAMOLTIP),'KEY_ARTI','CAMOLTIP');
          +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_codute),'KEY_ARTI','UTCV');
          +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'KEY_ARTI','UTDV');
          +",CADTOBSO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARDTOBSO),'KEY_ARTI','CADTOBSO');
          +",CACODCON ="+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'KEY_ARTI','CACODCON');
          +",CADTINVA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARDTINVA),'KEY_ARTI','CADTINVA');
          +",CACODTIP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODTIP),'KEY_ARTI','CACODTIP');
          +",CACODVAL ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODVAL),'KEY_ARTI','CACODVAL');
          +",CACODCLF ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODCLF),'KEY_ARTI','CACODCLF');
          +",CAFLGESC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARFLGESC),'KEY_ARTI','CAFLGESC');
              +i_ccchkf ;
          +" where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_CODKEY);
                 )
        else
          update (i_cTable) set;
              CADESART = this.w_DESART;
              ,CADESSUP = this.w_DESSUP;
              ,CACODART = this.oParentObject.w_ARCODART;
              ,CATIPCON = this.w_TIPCON;
              ,CA__TIPO = this.w___TIPO;
              ,CATIPBAR = this.oParentObject.w_TIPBAR;
              ,CAFLSTAM = this.w_CAFLSTAM;
              ,CAOPERAT = this.w_CAOPERAT;
              ,CAMOLTIP = this.w_CAMOLTIP;
              ,UTCV = i_codute;
              ,UTDV = SetInfoDate( g_CALUTD );
              ,CADTOBSO = this.oParentObject.w_ARDTOBSO;
              ,CACODCON = this.w_CODCON;
              ,CADTINVA = this.oParentObject.w_ARDTINVA;
              ,CACODTIP = this.oParentObject.w_ARCODTIP;
              ,CACODVAL = this.oParentObject.w_ARCODVAL;
              ,CACODCLF = this.oParentObject.w_ARCODCLF;
              ,CAFLGESC = this.oParentObject.w_ARFLGESC;
              &i_ccchkf. ;
           where;
              CACODICE = this.w_CODKEY;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        if i_rows=0
          * --- Try
          local bErr_05454D58
          bErr_05454D58=bTrsErr
          this.Try_05454D58()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_05454D58
          * --- End
        endif
        * --- Write into KEY_ARTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CADESART ="+cp_NullLink(cp_ToStrODBC(this.w_DESART),'KEY_ARTI','CADESART');
          +",CADESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'KEY_ARTI','CADESSUP');
          +",CACODART ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODART),'KEY_ARTI','CACODART');
          +",CATIPCON ="+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'KEY_ARTI','CATIPCON');
          +",CA__TIPO ="+cp_NullLink(cp_ToStrODBC(this.w___TIPO),'KEY_ARTI','CA__TIPO');
          +",CATIPBAR ="+cp_NullLink(cp_ToStrODBC(this.w_CATIPBAR),'KEY_ARTI','CATIPBAR');
          +",CAFLSTAM ="+cp_NullLink(cp_ToStrODBC(this.w_CAFLSTAM),'KEY_ARTI','CAFLSTAM');
          +",CAOPERAT ="+cp_NullLink(cp_ToStrODBC(this.w_CAOPERAT),'KEY_ARTI','CAOPERAT');
          +",CAMOLTIP ="+cp_NullLink(cp_ToStrODBC(this.w_CAMOLTIP),'KEY_ARTI','CAMOLTIP');
          +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_codute),'KEY_ARTI','UTCV');
          +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'KEY_ARTI','UTDV');
          +",CADTOBSO ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARDTOBSO),'KEY_ARTI','CADTOBSO');
          +",CACODCON ="+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'KEY_ARTI','CACODCON');
          +",CADTINVA ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARDTINVA),'KEY_ARTI','CADTINVA');
              +i_ccchkf ;
          +" where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_CODKEY);
                 )
        else
          update (i_cTable) set;
              CADESART = this.w_DESART;
              ,CADESSUP = this.w_DESSUP;
              ,CACODART = this.oParentObject.w_ARCODART;
              ,CATIPCON = this.w_TIPCON;
              ,CA__TIPO = this.w___TIPO;
              ,CATIPBAR = this.w_CATIPBAR;
              ,CAFLSTAM = this.w_CAFLSTAM;
              ,CAOPERAT = this.w_CAOPERAT;
              ,CAMOLTIP = this.w_CAMOLTIP;
              ,UTCV = i_codute;
              ,UTDV = SetInfoDate( g_CALUTD );
              ,CADTOBSO = this.oParentObject.w_ARDTOBSO;
              ,CACODCON = this.w_CODCON;
              ,CADTINVA = this.oParentObject.w_ARDTINVA;
              &i_ccchkf. ;
           where;
              CACODICE = this.w_CODKEY;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      if this.w_AUTOBAR=.T.
        * --- Aggiunge il Barcode Calcolo l'AutoNumber
        * --- L'autonumber viene assegnato alla tabella KEY_ARTI
        this.w_OLDBAR = "@@@@@@@@@@"
        this.w_OKINS = .T.
        this.w_TmpN = 0
        do while this.w_OKINS
          this.w_CODBAR = SPACE(10)
          i_nConn=i_TableProp[this.KEY_ARTI_IDX, 3]
          this.w_KEYBAR = RIGHT("99"+ALLTRIM(g_PREEAN),2)
          do case
            case this.oParentObject.w_ARTIPBAR="1"
              * --- EAN 8
              cp_NextTableProg(this,i_nConn,"COBAR8","i_codazi,w_CODBAR")
              * --- Calcolo la chiave del codice a barre - Aggiungo uno 0 per simulare il CheckSum
              this.w_KEYBAR = this.w_KEYBAR+RIGHT("00000"+ALLTRIM(this.w_CODBAR),5)+"0"
              this.w_MAXCOD = 99999
            case this.oParentObject.w_ARTIPBAR="2"
              * --- EAN 13
              cp_NextTableProg(this,i_nConn,"COBAR13","i_codazi,w_CODBAR")
              * --- Calcolo la chiave del codice a barre - Aggiungo uno 0 per simulare il CheckSum
              if this.w_KEYBAR="99"
                * --- Se i primi 2 Caratteri sono 99 il Codice e' di tipo Interno, non usa il Cod.Produttore, sfrutta tutto l' Autonumber (10 cifre)
                this.w_KEYBAR = this.w_KEYBAR+RIGHT("0000000000"+ALLTRIM(this.w_CODBAR),10)+"0"
                this.w_MAXCOD = 99999999
              else
                if LEN(ALLTRIM(g_BARPRO))=5
                  this.w_KEYBAR = this.w_KEYBAR+RIGHT("00000"+ALLTRIM(g_BARPRO),5)
                  this.w_KEYBAR = this.w_KEYBAR+RIGHT("00000"+ALLTRIM(this.w_CODBAR),5)+"0"
                  this.w_MAXCOD = 99999
                else
                  this.w_KEYBAR = this.w_KEYBAR+RIGHT("00000"+ALLTRIM(g_BARPRO),7)
                  this.w_KEYBAR = this.w_KEYBAR+RIGHT("00000"+ALLTRIM(this.w_CODBAR),3)+"0"
                  this.w_MAXCOD = 999
                endif
              endif
          endcase
          * --- Calcolo il Checksum Corretto
          this.w_KEYBAR = CALCBAR(this.w_KEYBAR, this.oParentObject.w_ARTIPBAR, 1)
          if RIGHT(SPACE(41)+this.w_KEYBAR,41)<>RIGHT(SPACE(41)+this.w_OLDBAR,41)
            ah_Msg("Scrittura barcode...%1",.T.,.F.,.F.,this.w_KEYBAR)
            * --- Try
            local bErr_054591C8
            bErr_054591C8=bTrsErr
            this.Try_054591C8()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
              this.w_TmpN = this.w_TmpN+1
              this.w_OKINS = .T.
            endif
            bTrsErr=bTrsErr or bErr_054591C8
            * --- End
            this.w_OLDBAR = this.w_KEYBAR
          else
            this.w_TmpN = this.w_MAXCOD+1
          endif
          if this.w_TmpN>this.w_MAXCOD
            this.w_OKINS = .F.
            * --- Errore generazione Codice a Barre - utilizzo una variaible per gestire la messagistica, questo errore dovrebbe essere
            * --- poco frequente, succede solo se utilizzo tutti i codici a barre disponibili
            this.w_ERROR = "Errore non generato codice per %1"
            this.w_APPARCODART = this.oParentObject.w_ARCODART
          endif
        enddo
      endif
    endif
  endproc
  proc Try_05483980()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do case
      case this.w_MOD AND this.w_DAT
        * --- Write into KEY_ARTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CADESART ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRI2),'KEY_ARTI','CADESART');
          +",CADESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_DESSU2),'KEY_ARTI','CADESSUP');
          +",CADTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_DTINVA2),'KEY_ARTI','CADTINVA');
          +",CADTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_DTOBSO2),'KEY_ARTI','CADTOBSO');
          +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'KEY_ARTI','UTCV');
          +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'KEY_ARTI','UTDV');
              +i_ccchkf ;
          +" where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_CODI);
                 )
        else
          update (i_cTable) set;
              CADESART = this.w_DESCRI2;
              ,CADESSUP = this.w_DESSU2;
              ,CADTINVA = this.w_DTINVA2;
              ,CADTOBSO = this.w_DTOBSO2;
              ,UTCV = i_CODUTE;
              ,UTDV = SetInfoDate(g_CALUTD);
              &i_ccchkf. ;
           where;
              CACODICE = this.w_CODI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_MOD
        * --- Write into KEY_ARTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CADESART ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRI2),'KEY_ARTI','CADESART');
          +",CADESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_DESSU2),'KEY_ARTI','CADESSUP');
          +",CADTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_DTINVA2),'KEY_ARTI','CADTINVA');
          +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'KEY_ARTI','UTCV');
          +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'KEY_ARTI','UTDV');
              +i_ccchkf ;
          +" where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_CODI);
                 )
        else
          update (i_cTable) set;
              CADESART = this.w_DESCRI2;
              ,CADESSUP = this.w_DESSU2;
              ,CADTINVA = this.w_DTINVA2;
              ,UTCV = i_CODUTE;
              ,UTDV = SetInfoDate(g_CALUTD);
              &i_ccchkf. ;
           where;
              CACODICE = this.w_CODI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      case this.w_DAT
        * --- Write into KEY_ARTI
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.KEY_ARTI_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"CADTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_DTOBSO2),'KEY_ARTI','CADTOBSO');
          +",UTCV ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'KEY_ARTI','UTCV');
          +",UTDV ="+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'KEY_ARTI','UTDV');
              +i_ccchkf ;
          +" where ";
              +"CACODICE = "+cp_ToStrODBC(this.w_CODI);
                 )
        else
          update (i_cTable) set;
              CADTOBSO = this.w_DTOBSO2;
              ,UTCV = i_CODUTE;
              ,UTDV = SetInfoDate(g_CALUTD);
              &i_ccchkf. ;
           where;
              CACODICE = this.w_CODI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
    endcase
    return
  proc Try_054869E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if this.w_MOD1 and this.w_MOD2
      * --- Write into DISTBASE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DISTBASE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DISTBASE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DBDESCOM ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'DISTBASE','DBDESCOM');
            +i_ccchkf ;
        +" where ";
            +"DBARTCOM = "+cp_ToStrODBC(this.w_CODKEY);
               )
      else
        update (i_cTable) set;
            DBDESCOM = this.w_DESCRI;
            &i_ccchkf. ;
         where;
            DBARTCOM = this.w_CODKEY;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.w_MOD1
      * --- Write into DISTBASE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DISTBASE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DISTBASE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DBDESCOM ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'DISTBASE','DBDESCOM');
            +i_ccchkf ;
        +" where ";
            +"DBCODCOM = "+cp_ToStrODBC(this.w_CODKEY);
               )
      else
        update (i_cTable) set;
            DBDESCOM = this.w_DESCRI;
            &i_ccchkf. ;
         where;
            DBCODCOM = this.w_CODKEY;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    if this.w_MOD2
      * --- Write into DISTBASE
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DISTBASE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DISTBASE_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DISTBASE_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"DBDESCOM ="+cp_NullLink(cp_ToStrODBC(this.w_DESCRI),'DISTBASE','DBDESCOM');
            +i_ccchkf ;
        +" where ";
            +"DBCODCOM <> "+cp_ToStrODBC(this.w_CODKEY);
            +" and DBARTCOM = "+cp_ToStrODBC(this.w_CODKEY);
               )
      else
        update (i_cTable) set;
            DBDESCOM = this.w_DESCRI;
            &i_ccchkf. ;
         where;
            DBCODCOM <> this.w_CODKEY;
            and DBARTCOM = this.w_CODKEY;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return
  proc Try_054555F8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into KEY_ARTI
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.KEY_ARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CACODICE"+",CADESART"+",CADESSUP"+",CACODART"+",CATIPCON"+",CA__TIPO"+",CATIPBAR"+",CAFLSTAM"+",CAOPERAT"+",CAMOLTIP"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",CACODCON"+",CADTINVA"+",CADTOBSO"+",CACODTIP"+",CACODVAL"+",CACODCLF"+",CAFLGESC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODKEY),'KEY_ARTI','CACODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESART),'KEY_ARTI','CADESART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'KEY_ARTI','CADESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODART),'KEY_ARTI','CACODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'KEY_ARTI','CATIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w___TIPO),'KEY_ARTI','CA__TIPO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CATIPBAR),'KEY_ARTI','CATIPBAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAFLSTAM),'KEY_ARTI','CAFLSTAM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAOPERAT),'KEY_ARTI','CAOPERAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAMOLTIP),'KEY_ARTI','CAMOLTIP');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'KEY_ARTI','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(0),'KEY_ARTI','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'KEY_ARTI','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'KEY_ARTI','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'KEY_ARTI','CACODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARDTINVA),'KEY_ARTI','CADTINVA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARDTOBSO),'KEY_ARTI','CADTOBSO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODTIP),'KEY_ARTI','CACODTIP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODVAL),'KEY_ARTI','CACODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODCLF),'KEY_ARTI','CACODCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARFLGESC),'KEY_ARTI','CAFLGESC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CACODICE',this.w_CODKEY,'CADESART',this.w_DESART,'CADESSUP',this.w_DESSUP,'CACODART',this.oParentObject.w_ARCODART,'CATIPCON',this.w_TIPCON,'CA__TIPO',this.w___TIPO,'CATIPBAR',this.w_CATIPBAR,'CAFLSTAM',this.w_CAFLSTAM,'CAOPERAT',this.w_CAOPERAT,'CAMOLTIP',this.w_CAMOLTIP,'UTCC',i_CODUTE,'UTCV',0)
      insert into (i_cTable) (CACODICE,CADESART,CADESSUP,CACODART,CATIPCON,CA__TIPO,CATIPBAR,CAFLSTAM,CAOPERAT,CAMOLTIP,UTCC,UTCV,UTDC,UTDV,CACODCON,CADTINVA,CADTOBSO,CACODTIP,CACODVAL,CACODCLF,CAFLGESC &i_ccchkf. );
         values (;
           this.w_CODKEY;
           ,this.w_DESART;
           ,this.w_DESSUP;
           ,this.oParentObject.w_ARCODART;
           ,this.w_TIPCON;
           ,this.w___TIPO;
           ,this.w_CATIPBAR;
           ,this.w_CAFLSTAM;
           ,this.w_CAOPERAT;
           ,this.w_CAMOLTIP;
           ,i_CODUTE;
           ,0;
           ,SetInfoDate( g_CALUTD );
           ,cp_CharToDate("  -  -    ");
           ,this.w_CODCON;
           ,this.oParentObject.w_ARDTINVA;
           ,this.oParentObject.w_ARDTOBSO;
           ,this.oParentObject.w_ARCODTIP;
           ,this.oParentObject.w_ARCODVAL;
           ,this.oParentObject.w_ARCODCLF;
           ,this.oParentObject.w_ARFLGESC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_05454D58()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into KEY_ARTI
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.KEY_ARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CACODICE"+",CADESART"+",CADESSUP"+",CACODART"+",CATIPCON"+",CA__TIPO"+",CATIPBAR"+",CAFLSTAM"+",CAOPERAT"+",CAMOLTIP"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",CACODCON"+",CADTINVA"+",CADTOBSO"+",CACODTIP"+",CACODVAL"+",CACODCLF"+",CAFLGESC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODKEY),'KEY_ARTI','CACODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESART),'KEY_ARTI','CADESART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'KEY_ARTI','CADESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODART),'KEY_ARTI','CACODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'KEY_ARTI','CATIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w___TIPO),'KEY_ARTI','CA__TIPO');
      +","+cp_NullLink(cp_ToStrODBC("0"),'KEY_ARTI','CATIPBAR');
      +","+cp_NullLink(cp_ToStrODBC(" "),'KEY_ARTI','CAFLSTAM');
      +","+cp_NullLink(cp_ToStrODBC("*"),'KEY_ARTI','CAOPERAT');
      +","+cp_NullLink(cp_ToStrODBC(1),'KEY_ARTI','CAMOLTIP');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'KEY_ARTI','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(0),'KEY_ARTI','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'KEY_ARTI','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'KEY_ARTI','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'KEY_ARTI','CACODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARDTINVA),'KEY_ARTI','CADTINVA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARDTOBSO),'KEY_ARTI','CADTOBSO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODTIP),'KEY_ARTI','CACODTIP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODVAL),'KEY_ARTI','CACODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODCLF),'KEY_ARTI','CACODCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARFLGESC),'KEY_ARTI','CAFLGESC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CACODICE',this.w_CODKEY,'CADESART',this.w_DESART,'CADESSUP',this.w_DESSUP,'CACODART',this.oParentObject.w_ARCODART,'CATIPCON',this.w_TIPCON,'CA__TIPO',this.w___TIPO,'CATIPBAR',"0",'CAFLSTAM'," ",'CAOPERAT',"*",'CAMOLTIP',1,'UTCC',i_CODUTE,'UTCV',0)
      insert into (i_cTable) (CACODICE,CADESART,CADESSUP,CACODART,CATIPCON,CA__TIPO,CATIPBAR,CAFLSTAM,CAOPERAT,CAMOLTIP,UTCC,UTCV,UTDC,UTDV,CACODCON,CADTINVA,CADTOBSO,CACODTIP,CACODVAL,CACODCLF,CAFLGESC &i_ccchkf. );
         values (;
           this.w_CODKEY;
           ,this.w_DESART;
           ,this.w_DESSUP;
           ,this.oParentObject.w_ARCODART;
           ,this.w_TIPCON;
           ,this.w___TIPO;
           ,"0";
           ," ";
           ,"*";
           ,1;
           ,i_CODUTE;
           ,0;
           ,SetInfoDate( g_CALUTD );
           ,cp_CharToDate("  -  -    ");
           ,this.w_CODCON;
           ,this.oParentObject.w_ARDTINVA;
           ,this.oParentObject.w_ARDTOBSO;
           ,this.oParentObject.w_ARCODTIP;
           ,this.oParentObject.w_ARCODVAL;
           ,this.oParentObject.w_ARCODCLF;
           ,this.oParentObject.w_ARFLGESC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_054591C8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_OKINS = .F.
    * --- Insert into KEY_ARTI
    i_nConn=i_TableProp[this.KEY_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.KEY_ARTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.KEY_ARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CACODICE"+",CADESART"+",CADESSUP"+",CACODART"+",CATIPCON"+",CA__TIPO"+",CATIPBAR"+",CAFLSTAM"+",CAOPERAT"+",CAMOLTIP"+",UTCC"+",UTCV"+",UTDC"+",UTDV"+",CADTOBSO"+",CACODCON"+",CADTINVA"+",CACODTIP"+",CACODVAL"+",CACODCLF"+",CAFLGESC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_KEYBAR),'KEY_ARTI','CACODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESART),'KEY_ARTI','CADESART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'KEY_ARTI','CADESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODART),'KEY_ARTI','CACODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_TIPCON),'KEY_ARTI','CATIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w___TIPO),'KEY_ARTI','CA__TIPO');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARTIPBAR),'KEY_ARTI','CATIPBAR');
      +","+cp_NullLink(cp_ToStrODBC("S"),'KEY_ARTI','CAFLSTAM');
      +","+cp_NullLink(cp_ToStrODBC("*"),'KEY_ARTI','CAOPERAT');
      +","+cp_NullLink(cp_ToStrODBC(1),'KEY_ARTI','CAMOLTIP');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'KEY_ARTI','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(0),'KEY_ARTI','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'KEY_ARTI','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'KEY_ARTI','UTDV');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARDTOBSO),'KEY_ARTI','CADTOBSO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODCON),'KEY_ARTI','CACODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARDTINVA),'KEY_ARTI','CADTINVA');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODTIP),'KEY_ARTI','CACODTIP');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODVAL),'KEY_ARTI','CACODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARCODCLF),'KEY_ARTI','CACODCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_ARFLGESC),'KEY_ARTI','CAFLGESC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CACODICE',this.w_KEYBAR,'CADESART',this.w_DESART,'CADESSUP',this.w_DESSUP,'CACODART',this.oParentObject.w_ARCODART,'CATIPCON',this.w_TIPCON,'CA__TIPO',this.w___TIPO,'CATIPBAR',this.oParentObject.w_ARTIPBAR,'CAFLSTAM',"S",'CAOPERAT',"*",'CAMOLTIP',1,'UTCC',i_CODUTE,'UTCV',0)
      insert into (i_cTable) (CACODICE,CADESART,CADESSUP,CACODART,CATIPCON,CA__TIPO,CATIPBAR,CAFLSTAM,CAOPERAT,CAMOLTIP,UTCC,UTCV,UTDC,UTDV,CADTOBSO,CACODCON,CADTINVA,CACODTIP,CACODVAL,CACODCLF,CAFLGESC &i_ccchkf. );
         values (;
           this.w_KEYBAR;
           ,this.w_DESART;
           ,this.w_DESSUP;
           ,this.oParentObject.w_ARCODART;
           ,this.w_TIPCON;
           ,this.w___TIPO;
           ,this.oParentObject.w_ARTIPBAR;
           ,"S";
           ,"*";
           ,1;
           ,i_CODUTE;
           ,0;
           ,SetInfoDate( g_CALUTD );
           ,cp_CharToDate("  -  -    ");
           ,this.oParentObject.w_ARDTOBSO;
           ,this.w_CODCON;
           ,this.oParentObject.w_ARDTINVA;
           ,this.oParentObject.w_ARCODTIP;
           ,this.oParentObject.w_ARCODVAL;
           ,this.oParentObject.w_ARCODCLF;
           ,this.oParentObject.w_ARFLGESC;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='KEY_ARTI'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='DISTBASE'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_KEY_ARTI')
      use in _Curs_KEY_ARTI
    endif
    if used('_Curs_DISTBASE')
      use in _Curs_DISTBASE
    endif
    if used('_Curs_DISTBASE')
      use in _Curs_DISTBASE
    endif
    if used('_Curs_KEY_ARTI')
      use in _Curs_KEY_ARTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
