* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_agp                                                        *
*              Giroconto IVA indetraibile da pro-rata                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-03-16                                                      *
* Last revis.: 2013-04-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_agp"))

* --- Class definition
define class tgscg_agp as StdForm
  Top    = 7
  Left   = 16

  * --- Standard Properties
  Width  = 620
  Height = 409+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-04-09"
  HelpContextID=225827479
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=52

  * --- Constant Properties
  PROGRATA_IDX = 0
  BUSIUNIT_IDX = 0
  CONTI_IDX = 0
  CAU_CONT_IDX = 0
  AZIENDA_IDX = 0
  ESERCIZI_IDX = 0
  cFile = "PROGRATA"
  cKeySelect = "GPSERIAL"
  cKeyWhere  = "GPSERIAL=this.w_GPSERIAL"
  cKeyWhereODBC = '"GPSERIAL="+cp_ToStrODBC(this.w_GPSERIAL)';

  cKeyWhereODBCqualified = '"PROGRATA.GPSERIAL="+cp_ToStrODBC(this.w_GPSERIAL)';

  cPrg = "gscg_agp"
  cComment = "Giroconto IVA indetraibile da pro-rata"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AZIENDA = space(5)
  w_GPSERIAL = space(10)
  w_GPCODAZI = space(5)
  w_GPDATGEN = ctod('  /  /  ')
  o_GPDATGEN = ctod('  /  /  ')
  w_GPCODESE = space(4)
  w_GPDESCRI = space(50)
  w_GPFLDATD = space(1)
  o_GPFLDATD = space(1)
  w_GPCAUCON = space(5)
  o_GPCAUCON = space(5)
  w_GPFLRGSC = space(1)
  w_GPSTORNO = space(1)
  o_GPSTORNO = space(1)
  w_GP_CONTO = space(15)
  w_GPFLTCCO = space(5)
  w_GPBUSUNI = space(3)
  w_GPDATINI = ctod('  /  /  ')
  o_GPDATINI = ctod('  /  /  ')
  w_GPDATFIN = ctod('  /  /  ')
  w_GPREGINI = ctod('  /  /  ')
  o_GPREGINI = ctod('  /  /  ')
  w_GPREGFIN = ctod('  /  /  ')
  w_GPCIVINI = ctod('  /  /  ')
  o_GPCIVINI = ctod('  /  /  ')
  w_GPCIVFIN = ctod('  /  /  ')
  w_GPSTAREG = space(1)
  w_BUDESCRI = space(40)
  w_GPTIPCON = space(1)
  w_ANDESCRI = space(40)
  w_CCDESCRI = space(35)
  w_CCDESCFL = space(35)
  w_CCGESRIT = space(1)
  w_CCFLASSE = space(1)
  w_CCFLSALI = space(1)
  w_CCFLSALF = space(1)
  w_CCFLPART = space(1)
  w_CCTIPREG = space(1)
  w_CCFLTREG = space(1)
  w_CCFLRIFE = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_CCDTOBSO = ctod('  /  /  ')
  w_GPOBSCAC = ctod('  /  /  ')
  w_CCFLANAL = space(1)
  w_ASRIFEPN = space(10)
  w_CCFLSTAT = space(1)
  w_FLBUANAL = space(1)
  w_CONCON = ctod('  /  /  ')
  w_STALIG = ctod('  /  /  ')
  w_NUMREG = 0
  w_CCFLTREGC = space(1)
  w_CCFLRIFEC = space(1)
  w_CCTIPDOC = space(2)
  w_ANCONSUP = space(15)
  w_AZCONPRO = space(15)
  w_AZCONIND = space(15)
  w_AZFLGIND = space(1)
  w_AZNEWIVA = space(1)
  w_FLGIND = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_GPSERIAL = this.W_GPSERIAL
  w_Zoom = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PROGRATA','gscg_agp')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_agpPag1","gscg_agp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Giroconto IVA indetraibile da pro-rata")
      .Pages(1).HelpContextID = 164656125
      .Pages(2).addobject("oPag","tgscg_agpPag2","gscg_agp",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Movimenti generati")
      .Pages(2).HelpContextID = 100841067
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oGPSERIAL_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
    proc Init()
      this.w_Zoom = this.oPgFrm.Pages(2).oPag.Zoom
      DoDefault()
    proc Destroy()
      this.w_Zoom = .NULL.
      DoDefault()

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='BUSIUNIT'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='CAU_CONT'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='ESERCIZI'
    this.cWorkTables[6]='PROGRATA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PROGRATA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PROGRATA_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_GPSERIAL = NVL(GPSERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_10_joined
    link_1_10_joined=.f.
    local link_1_15_joined
    link_1_15_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PROGRATA where GPSERIAL=KeySet.GPSERIAL
    *
    i_nConn = i_TableProp[this.PROGRATA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROGRATA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PROGRATA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PROGRATA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PROGRATA '
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_15_joined=this.AddJoinedLink_1_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'GPSERIAL',this.w_GPSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AZIENDA = I_CODAZI
        .w_GPCODAZI = i_codazi
        .w_BUDESCRI = space(40)
        .w_GPTIPCON = 'G'
        .w_ANDESCRI = space(40)
        .w_CCDESCRI = space(35)
        .w_CCDESCFL = space(35)
        .w_CCGESRIT = 'N'
        .w_CCFLASSE = space(1)
        .w_CCFLSALI = space(1)
        .w_CCFLSALF = space(1)
        .w_CCFLPART = space(1)
        .w_CCTIPREG = 'N'
        .w_CCFLTREG = space(1)
        .w_CCFLRIFE = space(1)
        .w_CCDTOBSO = ctod("  /  /  ")
        .w_GPOBSCAC = ctod("  /  /  ")
        .w_CCFLANAL = space(1)
        .w_CCFLSTAT = 'N'
        .w_FLBUANAL = space(1)
        .w_CONCON = ctod("  /  /  ")
        .w_STALIG = ctod("  /  /  ")
        .w_NUMREG = 0
        .w_CCFLTREGC = space(1)
        .w_CCFLRIFEC = space(1)
        .w_CCTIPDOC = space(2)
        .w_ANCONSUP = space(15)
        .w_AZCONPRO = space(15)
        .w_AZCONIND = space(15)
        .w_AZFLGIND = space(1)
        .w_AZNEWIVA = space(1)
        .w_FLGIND = space(1)
          .link_1_1('Load')
        .w_GPSERIAL = NVL(GPSERIAL,space(10))
        .op_GPSERIAL = .w_GPSERIAL
        .w_GPDATGEN = NVL(cp_ToDate(GPDATGEN),ctod("  /  /  "))
        .w_GPCODESE = NVL(GPCODESE,space(4))
          .link_1_6('Load')
        .w_GPDESCRI = NVL(GPDESCRI,space(50))
        .w_GPFLDATD = NVL(GPFLDATD,space(1))
        .w_GPCAUCON = NVL(GPCAUCON,space(5))
          if link_1_10_joined
            this.w_GPCAUCON = NVL(CCCODICE110,NVL(this.w_GPCAUCON,space(5)))
            this.w_CCDESCRI = NVL(CCDESCRI110,space(35))
            this.w_CCDTOBSO = NVL(cp_ToDate(CCDTOBSO110),ctod("  /  /  "))
            this.w_CCFLANAL = NVL(CCFLANAL110,space(1))
            this.w_CCFLTREG = NVL(CCTIPREG110,space(1))
            this.w_CCFLPART = NVL(CCFLPART110,space(1))
            this.w_CCFLSALI = NVL(CCFLSALI110,space(1))
            this.w_CCFLSALF = NVL(CCFLSALF110,space(1))
            this.w_CCFLRIFE = NVL(CCFLRIFE110,space(1))
            this.w_CCFLASSE = NVL(CCFLASSE110,space(1))
          else
          .link_1_10('Load')
          endif
        .w_GPFLRGSC = NVL(GPFLRGSC,space(1))
        .w_GPSTORNO = NVL(GPSTORNO,space(1))
        .w_GP_CONTO = NVL(GP_CONTO,space(15))
          .link_1_13('Load')
        .w_GPFLTCCO = NVL(GPFLTCCO,space(5))
          if link_1_15_joined
            this.w_GPFLTCCO = NVL(CCCODICE115,NVL(this.w_GPFLTCCO,space(5)))
            this.w_CCDESCFL = NVL(CCDESCRI115,space(35))
            this.w_GPOBSCAC = NVL(cp_ToDate(CCDTOBSO115),ctod("  /  /  "))
            this.w_CCFLTREGC = NVL(CCTIPREG115,space(1))
            this.w_CCFLRIFEC = NVL(CCFLRIFE115,space(1))
            this.w_CCTIPDOC = NVL(CCTIPDOC115,space(2))
          else
          .link_1_15('Load')
          endif
        .w_GPBUSUNI = NVL(GPBUSUNI,space(3))
        .w_GPDATINI = NVL(cp_ToDate(GPDATINI),ctod("  /  /  "))
        .w_GPDATFIN = NVL(cp_ToDate(GPDATFIN),ctod("  /  /  "))
        .w_GPREGINI = NVL(cp_ToDate(GPREGINI),ctod("  /  /  "))
        .w_GPREGFIN = NVL(cp_ToDate(GPREGFIN),ctod("  /  /  "))
        .w_GPCIVINI = NVL(cp_ToDate(GPCIVINI),ctod("  /  /  "))
        .w_GPCIVFIN = NVL(cp_ToDate(GPCIVFIN),ctod("  /  /  "))
        .w_GPSTAREG = NVL(GPSTAREG,space(1))
        .w_OBTEST = .w_GPDATGEN
        .oPgFrm.Page2.oPag.Zoom.Calculate()
        .w_ASRIFEPN = IIF(empty(.w_GPSERIAL),'',.w_Zoom.getVar('PNSERIAL'))
        .oPgFrm.Page2.oPag.oObj_2_4.Calculate()
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'PROGRATA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZIENDA = space(5)
      .w_GPSERIAL = space(10)
      .w_GPCODAZI = space(5)
      .w_GPDATGEN = ctod("  /  /  ")
      .w_GPCODESE = space(4)
      .w_GPDESCRI = space(50)
      .w_GPFLDATD = space(1)
      .w_GPCAUCON = space(5)
      .w_GPFLRGSC = space(1)
      .w_GPSTORNO = space(1)
      .w_GP_CONTO = space(15)
      .w_GPFLTCCO = space(5)
      .w_GPBUSUNI = space(3)
      .w_GPDATINI = ctod("  /  /  ")
      .w_GPDATFIN = ctod("  /  /  ")
      .w_GPREGINI = ctod("  /  /  ")
      .w_GPREGFIN = ctod("  /  /  ")
      .w_GPCIVINI = ctod("  /  /  ")
      .w_GPCIVFIN = ctod("  /  /  ")
      .w_GPSTAREG = space(1)
      .w_BUDESCRI = space(40)
      .w_GPTIPCON = space(1)
      .w_ANDESCRI = space(40)
      .w_CCDESCRI = space(35)
      .w_CCDESCFL = space(35)
      .w_CCGESRIT = space(1)
      .w_CCFLASSE = space(1)
      .w_CCFLSALI = space(1)
      .w_CCFLSALF = space(1)
      .w_CCFLPART = space(1)
      .w_CCTIPREG = space(1)
      .w_CCFLTREG = space(1)
      .w_CCFLRIFE = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_CCDTOBSO = ctod("  /  /  ")
      .w_GPOBSCAC = ctod("  /  /  ")
      .w_CCFLANAL = space(1)
      .w_ASRIFEPN = space(10)
      .w_CCFLSTAT = space(1)
      .w_FLBUANAL = space(1)
      .w_CONCON = ctod("  /  /  ")
      .w_STALIG = ctod("  /  /  ")
      .w_NUMREG = 0
      .w_CCFLTREGC = space(1)
      .w_CCFLRIFEC = space(1)
      .w_CCTIPDOC = space(2)
      .w_ANCONSUP = space(15)
      .w_AZCONPRO = space(15)
      .w_AZCONIND = space(15)
      .w_AZFLGIND = space(1)
      .w_AZNEWIVA = space(1)
      .w_FLGIND = space(1)
      if .cFunction<>"Filter"
        .w_AZIENDA = I_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_AZIENDA))
          .link_1_1('Full')
          endif
          .DoRTCalc(2,2,.f.)
        .w_GPCODAZI = i_codazi
        .w_GPDATGEN = i_datsys
        .w_GPCODESE = CALCESER(.w_GPDATGEN)
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_GPCODESE))
          .link_1_6('Full')
          endif
          .DoRTCalc(6,6,.f.)
        .w_GPFLDATD = 'N'
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_GPCAUCON))
          .link_1_10('Full')
          endif
        .w_GPFLRGSC = IIF(EMPTY(NVL(.w_GPFLRGSC, ' ')) OR .w_CCFLRIFE<>'N', 'N', IIF(.w_GPFLDATD='S', 'N', .w_GPFLRGSC))
        .w_GPSTORNO = 'M'
        .w_GP_CONTO = IIF(.w_GPSTORNO='C', .w_GP_CONTO, SPACE(15))
        .DoRTCalc(11,11,.f.)
          if not(empty(.w_GP_CONTO))
          .link_1_13('Full')
          endif
        .DoRTCalc(12,12,.f.)
          if not(empty(.w_GPFLTCCO))
          .link_1_15('Full')
          endif
        .w_GPBUSUNI = g_CODBUN
        .w_GPDATINI = IIF(.w_GPDATGEN>=i_INIDAT and .w_GPDATGEN<=i_FINDAT,date(iif(month(.w_GPDATGEN)<>1,year(.w_GPDATGEN),year(.w_GPDATGEN)-1),iif(month(.w_GPDATGEN)<>1,month(.w_GPDATGEN)-1,12),1),.w_GPDATINI)
        .w_GPDATFIN = IIF(.w_GPDATINI>=i_INIDAT and .w_GPDATINI<=i_FINDAT,date(iif(month(.w_GPDATINI)=12,year(.w_GPDATINI)+1,year(.w_GPDATINI)),iif(month(.w_GPDATINI)=12,1,month(.w_GPDATINI)+1),1)-1,.w_GPDATFIN)
        .w_GPREGINI = .w_GPDATINI
        .w_GPREGFIN = IIF(.w_GPREGINI>=i_INIDAT and .w_GPREGINI<=i_FINDAT,date(iif(month(.w_GPREGINI)=12,year(.w_GPREGINI)+1,year(.w_GPREGINI)),iif(month(.w_GPREGINI)=12,1,month(.w_GPREGINI)+1),1)-1, .w_GPREGFIN)
        .w_GPCIVINI = .w_GPDATINI
        .w_GPCIVFIN = IIF(.w_GPCIVINI>=i_INIDAT and .w_GPCIVINI<=i_FINDAT,date(iif(month(.w_GPCIVINI)=12,year(.w_GPCIVINI)+1,year(.w_GPCIVINI)),iif(month(.w_GPCIVINI)=12,1,month(.w_GPCIVINI)+1),1)-1, .w_GPCIVFIN)
        .w_GPSTAREG = 'N'
          .DoRTCalc(21,21,.f.)
        .w_GPTIPCON = 'G'
          .DoRTCalc(23,25,.f.)
        .w_CCGESRIT = 'N'
          .DoRTCalc(27,30,.f.)
        .w_CCTIPREG = 'N'
          .DoRTCalc(32,33,.f.)
        .w_OBTEST = .w_GPDATGEN
        .oPgFrm.Page2.oPag.Zoom.Calculate()
          .DoRTCalc(35,37,.f.)
        .w_ASRIFEPN = IIF(empty(.w_GPSERIAL),'',.w_Zoom.getVar('PNSERIAL'))
        .w_CCFLSTAT = 'N'
        .oPgFrm.Page2.oPag.oObj_2_4.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'PROGRATA')
    this.DoRTCalc(40,52,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_agp
    this.bUpdated=this.cFunction='Load'
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PROGRATA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROGRATA_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEGIP","i_codazi,w_GPSERIAL")
      .op_codazi = .w_codazi
      .op_GPSERIAL = .w_GPSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oGPSERIAL_1_2.enabled = i_bVal
      .Page1.oPag.oGPDATGEN_1_5.enabled = i_bVal
      .Page1.oPag.oGPCODESE_1_6.enabled = i_bVal
      .Page1.oPag.oGPDESCRI_1_8.enabled = i_bVal
      .Page1.oPag.oGPFLDATD_1_9.enabled = i_bVal
      .Page1.oPag.oGPCAUCON_1_10.enabled = i_bVal
      .Page1.oPag.oGPFLRGSC_1_11.enabled = i_bVal
      .Page1.oPag.oGPSTORNO_1_12.enabled = i_bVal
      .Page1.oPag.oGP_CONTO_1_13.enabled = i_bVal
      .Page1.oPag.oGPFLTCCO_1_15.enabled = i_bVal
      .Page1.oPag.oGPDATINI_1_18.enabled = i_bVal
      .Page1.oPag.oGPDATFIN_1_20.enabled = i_bVal
      .Page1.oPag.oGPREGINI_1_22.enabled = i_bVal
      .Page1.oPag.oGPREGFIN_1_24.enabled = i_bVal
      .Page1.oPag.oGPCIVINI_1_25.enabled = i_bVal
      .Page1.oPag.oGPCIVFIN_1_26.enabled = i_bVal
      .Page1.oPag.oGPSTAREG_1_28.enabled = i_bVal
      .Page2.oPag.oBtn_2_3.enabled = .Page2.oPag.oBtn_2_3.mCond()
      .Page1.oPag.oBtn_1_62.enabled = i_bVal
      .Page2.oPag.oObj_2_4.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oGPSERIAL_1_2.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oGPSERIAL_1_2.enabled = .t.
        .Page1.oPag.oGPDATGEN_1_5.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'PROGRATA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PROGRATA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPSERIAL,"GPSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPDATGEN,"GPDATGEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCODESE,"GPCODESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPDESCRI,"GPDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPFLDATD,"GPFLDATD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCAUCON,"GPCAUCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPFLRGSC,"GPFLRGSC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPSTORNO,"GPSTORNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GP_CONTO,"GP_CONTO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPFLTCCO,"GPFLTCCO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPBUSUNI,"GPBUSUNI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPDATINI,"GPDATINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPDATFIN,"GPDATFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPREGINI,"GPREGINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPREGFIN,"GPREGFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCIVINI,"GPCIVINI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPCIVFIN,"GPCIVFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GPSTAREG,"GPSTAREG",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PROGRATA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROGRATA_IDX,2])
    i_lTable = "PROGRATA"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PROGRATA_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PROGRATA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROGRATA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PROGRATA_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEGIP","i_codazi,w_GPSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PROGRATA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PROGRATA')
        i_extval=cp_InsertValODBCExtFlds(this,'PROGRATA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(GPSERIAL,GPDATGEN,GPCODESE,GPDESCRI,GPFLDATD"+;
                  ",GPCAUCON,GPFLRGSC,GPSTORNO,GP_CONTO,GPFLTCCO"+;
                  ",GPBUSUNI,GPDATINI,GPDATFIN,GPREGINI,GPREGFIN"+;
                  ",GPCIVINI,GPCIVFIN,GPSTAREG "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_GPSERIAL)+;
                  ","+cp_ToStrODBC(this.w_GPDATGEN)+;
                  ","+cp_ToStrODBCNull(this.w_GPCODESE)+;
                  ","+cp_ToStrODBC(this.w_GPDESCRI)+;
                  ","+cp_ToStrODBC(this.w_GPFLDATD)+;
                  ","+cp_ToStrODBCNull(this.w_GPCAUCON)+;
                  ","+cp_ToStrODBC(this.w_GPFLRGSC)+;
                  ","+cp_ToStrODBC(this.w_GPSTORNO)+;
                  ","+cp_ToStrODBCNull(this.w_GP_CONTO)+;
                  ","+cp_ToStrODBCNull(this.w_GPFLTCCO)+;
                  ","+cp_ToStrODBC(this.w_GPBUSUNI)+;
                  ","+cp_ToStrODBC(this.w_GPDATINI)+;
                  ","+cp_ToStrODBC(this.w_GPDATFIN)+;
                  ","+cp_ToStrODBC(this.w_GPREGINI)+;
                  ","+cp_ToStrODBC(this.w_GPREGFIN)+;
                  ","+cp_ToStrODBC(this.w_GPCIVINI)+;
                  ","+cp_ToStrODBC(this.w_GPCIVFIN)+;
                  ","+cp_ToStrODBC(this.w_GPSTAREG)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PROGRATA')
        i_extval=cp_InsertValVFPExtFlds(this,'PROGRATA')
        cp_CheckDeletedKey(i_cTable,0,'GPSERIAL',this.w_GPSERIAL)
        INSERT INTO (i_cTable);
              (GPSERIAL,GPDATGEN,GPCODESE,GPDESCRI,GPFLDATD,GPCAUCON,GPFLRGSC,GPSTORNO,GP_CONTO,GPFLTCCO,GPBUSUNI,GPDATINI,GPDATFIN,GPREGINI,GPREGFIN,GPCIVINI,GPCIVFIN,GPSTAREG  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_GPSERIAL;
                  ,this.w_GPDATGEN;
                  ,this.w_GPCODESE;
                  ,this.w_GPDESCRI;
                  ,this.w_GPFLDATD;
                  ,this.w_GPCAUCON;
                  ,this.w_GPFLRGSC;
                  ,this.w_GPSTORNO;
                  ,this.w_GP_CONTO;
                  ,this.w_GPFLTCCO;
                  ,this.w_GPBUSUNI;
                  ,this.w_GPDATINI;
                  ,this.w_GPDATFIN;
                  ,this.w_GPREGINI;
                  ,this.w_GPREGFIN;
                  ,this.w_GPCIVINI;
                  ,this.w_GPCIVFIN;
                  ,this.w_GPSTAREG;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PROGRATA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROGRATA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PROGRATA_IDX,i_nConn)
      *
      * update PROGRATA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PROGRATA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " GPDATGEN="+cp_ToStrODBC(this.w_GPDATGEN)+;
             ",GPCODESE="+cp_ToStrODBCNull(this.w_GPCODESE)+;
             ",GPDESCRI="+cp_ToStrODBC(this.w_GPDESCRI)+;
             ",GPFLDATD="+cp_ToStrODBC(this.w_GPFLDATD)+;
             ",GPCAUCON="+cp_ToStrODBCNull(this.w_GPCAUCON)+;
             ",GPFLRGSC="+cp_ToStrODBC(this.w_GPFLRGSC)+;
             ",GPSTORNO="+cp_ToStrODBC(this.w_GPSTORNO)+;
             ",GP_CONTO="+cp_ToStrODBCNull(this.w_GP_CONTO)+;
             ",GPFLTCCO="+cp_ToStrODBCNull(this.w_GPFLTCCO)+;
             ",GPBUSUNI="+cp_ToStrODBC(this.w_GPBUSUNI)+;
             ",GPDATINI="+cp_ToStrODBC(this.w_GPDATINI)+;
             ",GPDATFIN="+cp_ToStrODBC(this.w_GPDATFIN)+;
             ",GPREGINI="+cp_ToStrODBC(this.w_GPREGINI)+;
             ",GPREGFIN="+cp_ToStrODBC(this.w_GPREGFIN)+;
             ",GPCIVINI="+cp_ToStrODBC(this.w_GPCIVINI)+;
             ",GPCIVFIN="+cp_ToStrODBC(this.w_GPCIVFIN)+;
             ",GPSTAREG="+cp_ToStrODBC(this.w_GPSTAREG)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PROGRATA')
        i_cWhere = cp_PKFox(i_cTable  ,'GPSERIAL',this.w_GPSERIAL  )
        UPDATE (i_cTable) SET;
              GPDATGEN=this.w_GPDATGEN;
             ,GPCODESE=this.w_GPCODESE;
             ,GPDESCRI=this.w_GPDESCRI;
             ,GPFLDATD=this.w_GPFLDATD;
             ,GPCAUCON=this.w_GPCAUCON;
             ,GPFLRGSC=this.w_GPFLRGSC;
             ,GPSTORNO=this.w_GPSTORNO;
             ,GP_CONTO=this.w_GP_CONTO;
             ,GPFLTCCO=this.w_GPFLTCCO;
             ,GPBUSUNI=this.w_GPBUSUNI;
             ,GPDATINI=this.w_GPDATINI;
             ,GPDATFIN=this.w_GPDATFIN;
             ,GPREGINI=this.w_GPREGINI;
             ,GPREGFIN=this.w_GPREGFIN;
             ,GPCIVINI=this.w_GPCIVINI;
             ,GPCIVFIN=this.w_GPCIVFIN;
             ,GPSTAREG=this.w_GPSTAREG;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PROGRATA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PROGRATA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PROGRATA_IDX,i_nConn)
      *
      * delete PROGRATA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'GPSERIAL',this.w_GPSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PROGRATA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PROGRATA_IDX,2])
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,4,.t.)
        if .o_GPDATGEN<>.w_GPDATGEN
            .w_GPCODESE = CALCESER(.w_GPDATGEN)
          .link_1_6('Full')
        endif
        .DoRTCalc(6,8,.t.)
        if .o_GPFLDATD<>.w_GPFLDATD.or. .o_GPCAUCON<>.w_GPCAUCON
            .w_GPFLRGSC = IIF(EMPTY(NVL(.w_GPFLRGSC, ' ')) OR .w_CCFLRIFE<>'N', 'N', IIF(.w_GPFLDATD='S', 'N', .w_GPFLRGSC))
        endif
        .DoRTCalc(10,10,.t.)
        if .o_GPSTORNO<>.w_GPSTORNO
            .w_GP_CONTO = IIF(.w_GPSTORNO='C', .w_GP_CONTO, SPACE(15))
          .link_1_13('Full')
        endif
        .DoRTCalc(12,13,.t.)
        if .o_GPDATGEN<>.w_GPDATGEN
            .w_GPDATINI = IIF(.w_GPDATGEN>=i_INIDAT and .w_GPDATGEN<=i_FINDAT,date(iif(month(.w_GPDATGEN)<>1,year(.w_GPDATGEN),year(.w_GPDATGEN)-1),iif(month(.w_GPDATGEN)<>1,month(.w_GPDATGEN)-1,12),1),.w_GPDATINI)
        endif
        if .o_GPDATINI<>.w_GPDATINI
            .w_GPDATFIN = IIF(.w_GPDATINI>=i_INIDAT and .w_GPDATINI<=i_FINDAT,date(iif(month(.w_GPDATINI)=12,year(.w_GPDATINI)+1,year(.w_GPDATINI)),iif(month(.w_GPDATINI)=12,1,month(.w_GPDATINI)+1),1)-1,.w_GPDATFIN)
        endif
        if .o_GPDATINI<>.w_GPDATINI
            .w_GPREGINI = .w_GPDATINI
        endif
        if .o_GPREGINI<>.w_GPREGINI.or. .o_GPDATINI<>.w_GPDATINI
            .w_GPREGFIN = IIF(.w_GPREGINI>=i_INIDAT and .w_GPREGINI<=i_FINDAT,date(iif(month(.w_GPREGINI)=12,year(.w_GPREGINI)+1,year(.w_GPREGINI)),iif(month(.w_GPREGINI)=12,1,month(.w_GPREGINI)+1),1)-1, .w_GPREGFIN)
        endif
        if .o_GPDATINI<>.w_GPDATINI
            .w_GPCIVINI = .w_GPDATINI
        endif
        if .o_GPCIVINI<>.w_GPCIVINI.or. .o_GPDATINI<>.w_GPDATINI
            .w_GPCIVFIN = IIF(.w_GPCIVINI>=i_INIDAT and .w_GPCIVINI<=i_FINDAT,date(iif(month(.w_GPCIVINI)=12,year(.w_GPCIVINI)+1,year(.w_GPCIVINI)),iif(month(.w_GPCIVINI)=12,1,month(.w_GPCIVINI)+1),1)-1, .w_GPCIVFIN)
        endif
        .DoRTCalc(20,33,.t.)
        if .o_GPDATGEN<>.w_GPDATGEN
            .w_OBTEST = .w_GPDATGEN
        endif
        .oPgFrm.Page2.oPag.Zoom.Calculate()
        .DoRTCalc(35,37,.t.)
            .w_ASRIFEPN = IIF(empty(.w_GPSERIAL),'',.w_Zoom.getVar('PNSERIAL'))
        .oPgFrm.Page2.oPag.oObj_2_4.Calculate()
        * --- Area Manuale = Calculate
        * --- gscg_agp
        IF (.cfunction='Query')
        .mEnableControls()
        ENDIF
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
           cp_AskTableProg(this,i_nConn,"SEGIP","i_codazi,w_GPSERIAL")
          .op_GPSERIAL = .w_GPSERIAL
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(39,52,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page2.oPag.Zoom.Calculate()
        .oPgFrm.Page2.oPag.oObj_2_4.Calculate()
    endwith
  return

  proc Calculate_ULUJOKNQCU()
    with this
          * --- GSCG_BGR (save) - insert end
          GSCG_BGR(this;
              ,'SAVE';
             )
    endwith
  endproc
  proc Calculate_KBJZGJITQH()
    with this
          * --- GSCG_BGR (delet) - delete start
          GSCG_BGR(this;
              ,'DELET';
             )
    endwith
  endproc
  proc Calculate_UTJLKJAQCJ()
    with this
          * --- GSCG_BGR (recins) - record inserted
          GSCG_BGR(this;
              ,'RECINS';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oGPFLRGSC_1_11.enabled = this.oPgFrm.Page1.oPag.oGPFLRGSC_1_11.mCond()
    this.oPgFrm.Page1.oPag.oGP_CONTO_1_13.enabled = this.oPgFrm.Page1.oPag.oGP_CONTO_1_13.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_3.enabled = this.oPgFrm.Page2.oPag.oBtn_2_3.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Insert end")
          .Calculate_ULUJOKNQCU()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.Zoom.Event(cEvent)
        if lower(cEvent)==lower("Delete start")
          .Calculate_KBJZGJITQH()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Record Inserted")
          .Calculate_UTJLKJAQCJ()
          bRefresh=.t.
        endif
      .oPgFrm.Page2.oPag.oObj_2_4.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZIENDA
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIENDA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIENDA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZSTALIG,AZCONPRO,AZFLGIND,AZCONIND,AZNEWIVA";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_AZIENDA)
            select AZCODAZI,AZSTALIG,AZCONPRO,AZFLGIND,AZCONIND,AZNEWIVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIENDA = NVL(_Link_.AZCODAZI,space(5))
      this.w_STALIG = NVL(cp_ToDate(_Link_.AZSTALIG),ctod("  /  /  "))
      this.w_AZCONPRO = NVL(_Link_.AZCONPRO,space(15))
      this.w_AZFLGIND = NVL(_Link_.AZFLGIND,space(1))
      this.w_AZCONIND = NVL(_Link_.AZCONIND,space(15))
      this.w_AZNEWIVA = NVL(_Link_.AZNEWIVA,space(1))
      this.w_FLGIND = NVL(_Link_.AZFLGIND,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZIENDA = space(5)
      endif
      this.w_STALIG = ctod("  /  /  ")
      this.w_AZCONPRO = space(15)
      this.w_AZFLGIND = space(1)
      this.w_AZCONIND = space(15)
      this.w_AZNEWIVA = space(1)
      this.w_FLGIND = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIENDA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPCODESE
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCODESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_GPCODESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESCONCON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_AZIENDA;
                     ,'ESCODESE',trim(this.w_GPCODESE))
          select ESCODAZI,ESCODESE,ESCONCON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCODESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GPCODESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oGPCODESE_1_6'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_AZIENDA<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESCONCON";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESCONCON;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESCONCON";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESCONCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCODESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESCONCON";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_GPCODESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_AZIENDA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_AZIENDA;
                       ,'ESCODESE',this.w_GPCODESE)
            select ESCODAZI,ESCODESE,ESCONCON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCODESE = NVL(_Link_.ESCODESE,space(4))
      this.w_CONCON = NVL(cp_ToDate(_Link_.ESCONCON),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_GPCODESE = space(4)
      endif
      this.w_CONCON = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCODESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPCAUCON
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPCAUCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_GPCAUCON)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCFLANAL,CCTIPREG,CCFLPART,CCFLSALI,CCFLSALF,CCFLRIFE,CCFLASSE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_GPCAUCON))
          select CCCODICE,CCDESCRI,CCDTOBSO,CCFLANAL,CCTIPREG,CCFLPART,CCFLSALI,CCFLSALF,CCFLRIFE,CCFLASSE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPCAUCON)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_GPCAUCON)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCFLANAL,CCTIPREG,CCFLPART,CCFLSALI,CCFLSALF,CCFLRIFE,CCFLASSE";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_GPCAUCON)+"%");

            select CCCODICE,CCDESCRI,CCDTOBSO,CCFLANAL,CCTIPREG,CCFLPART,CCFLSALI,CCFLSALF,CCFLRIFE,CCFLASSE;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_GPCAUCON) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oGPCAUCON_1_10'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSCG_AGP.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCFLANAL,CCTIPREG,CCFLPART,CCFLSALI,CCFLSALF,CCFLRIFE,CCFLASSE";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO,CCFLANAL,CCTIPREG,CCFLPART,CCFLSALI,CCFLSALF,CCFLRIFE,CCFLASSE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPCAUCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCFLANAL,CCTIPREG,CCFLPART,CCFLSALI,CCFLSALF,CCFLRIFE,CCFLASSE";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_GPCAUCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_GPCAUCON)
            select CCCODICE,CCDESCRI,CCDTOBSO,CCFLANAL,CCTIPREG,CCFLPART,CCFLSALI,CCFLSALF,CCFLRIFE,CCFLASSE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPCAUCON = NVL(_Link_.CCCODICE,space(5))
      this.w_CCDESCRI = NVL(_Link_.CCDESCRI,space(35))
      this.w_CCDTOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_CCFLANAL = NVL(_Link_.CCFLANAL,space(1))
      this.w_CCFLTREG = NVL(_Link_.CCTIPREG,space(1))
      this.w_CCFLPART = NVL(_Link_.CCFLPART,space(1))
      this.w_CCFLSALI = NVL(_Link_.CCFLSALI,space(1))
      this.w_CCFLSALF = NVL(_Link_.CCFLSALF,space(1))
      this.w_CCFLRIFE = NVL(_Link_.CCFLRIFE,space(1))
      this.w_CCFLASSE = NVL(_Link_.CCFLASSE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_GPCAUCON = space(5)
      endif
      this.w_CCDESCRI = space(35)
      this.w_CCDTOBSO = ctod("  /  /  ")
      this.w_CCFLANAL = space(1)
      this.w_CCFLTREG = space(1)
      this.w_CCFLPART = space(1)
      this.w_CCFLSALI = space(1)
      this.w_CCFLSALF = space(1)
      this.w_CCFLRIFE = space(1)
      this.w_CCFLASSE = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CCFLTREG = 'N' AND .w_CCFLPART = 'N' AND .w_CCFLSALF = 'N' AND .w_CCFLSALI = 'N' AND (.w_CCDTOBSO > .w_OBTEST OR EMPTY(.w_CCDTOBSO)) AND .w_CCFLASSE<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile non valida, inesistente oppure obsoleta")
        endif
        this.w_GPCAUCON = space(5)
        this.w_CCDESCRI = space(35)
        this.w_CCDTOBSO = ctod("  /  /  ")
        this.w_CCFLANAL = space(1)
        this.w_CCFLTREG = space(1)
        this.w_CCFLPART = space(1)
        this.w_CCFLSALI = space(1)
        this.w_CCFLSALF = space(1)
        this.w_CCFLRIFE = space(1)
        this.w_CCFLASSE = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPCAUCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 10 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+10<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.CCCODICE as CCCODICE110"+ ",link_1_10.CCDESCRI as CCDESCRI110"+ ",link_1_10.CCDTOBSO as CCDTOBSO110"+ ",link_1_10.CCFLANAL as CCFLANAL110"+ ",link_1_10.CCTIPREG as CCTIPREG110"+ ",link_1_10.CCFLPART as CCFLPART110"+ ",link_1_10.CCFLSALI as CCFLSALI110"+ ",link_1_10.CCFLSALF as CCFLSALF110"+ ",link_1_10.CCFLRIFE as CCFLRIFE110"+ ",link_1_10.CCFLASSE as CCFLASSE110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on PROGRATA.GPCAUCON=link_1_10.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and PROGRATA.GPCAUCON=link_1_10.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+10
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=GP_CONTO
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GP_CONTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_GP_CONTO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_GPTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_GPTIPCON;
                     ,'ANCODICE',trim(this.w_GP_CONTO))
          select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GP_CONTO)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_GP_CONTO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_GPTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_GP_CONTO)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_GPTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_GP_CONTO) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oGP_CONTO_1_13'),i_cWhere,'GSAR_API',"Conti di storno",'CONTIECO.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_GPTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_GPTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GP_CONTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_GP_CONTO);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_GPTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_GPTIPCON;
                       ,'ANCODICE',this.w_GP_CONTO)
            select ANTIPCON,ANCODICE,ANDESCRI,ANCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GP_CONTO = NVL(_Link_.ANCODICE,space(15))
      this.w_ANDESCRI = NVL(_Link_.ANDESCRI,space(40))
      this.w_ANCONSUP = NVL(_Link_.ANCONSUP,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_GP_CONTO = space(15)
      endif
      this.w_ANDESCRI = space(40)
      this.w_ANCONSUP = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CALCSEZ(.w_ANCONSUP)$'C/R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_GP_CONTO = space(15)
        this.w_ANDESCRI = space(40)
        this.w_ANCONSUP = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GP_CONTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GPFLTCCO
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GPFLTCCO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_GPFLTCCO)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLRIFE,CCTIPDOC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_GPFLTCCO))
          select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLRIFE,CCTIPDOC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GPFLTCCO)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_GPFLTCCO)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLRIFE,CCTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_GPFLTCCO)+"%");

            select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLRIFE,CCTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_GPFLTCCO) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oGPFLTCCO_1_15'),i_cWhere,'',"Causali contabili",'GSCG1AGP.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLRIFE,CCTIPDOC";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLRIFE,CCTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GPFLTCCO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLRIFE,CCTIPDOC";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_GPFLTCCO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_GPFLTCCO)
            select CCCODICE,CCDESCRI,CCDTOBSO,CCTIPREG,CCFLRIFE,CCTIPDOC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GPFLTCCO = NVL(_Link_.CCCODICE,space(5))
      this.w_CCDESCFL = NVL(_Link_.CCDESCRI,space(35))
      this.w_GPOBSCAC = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_CCFLTREGC = NVL(_Link_.CCTIPREG,space(1))
      this.w_CCFLRIFEC = NVL(_Link_.CCFLRIFE,space(1))
      this.w_CCTIPDOC = NVL(_Link_.CCTIPDOC,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_GPFLTCCO = space(5)
      endif
      this.w_CCDESCFL = space(35)
      this.w_GPOBSCAC = ctod("  /  /  ")
      this.w_CCFLTREGC = space(1)
      this.w_CCFLRIFEC = space(1)
      this.w_CCTIPDOC = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_CCFLTREGC = 'A' AND (.w_GPOBSCAC>.w_OBTEST OR EMPTY(.w_GPOBSCAC)) AND .w_CCFLRIFEC = 'F' AND NOT(.w_CCTIPDOC$'FE-NE')
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale contabile non valida, inesistente oppure obsoleta")
        endif
        this.w_GPFLTCCO = space(5)
        this.w_CCDESCFL = space(35)
        this.w_GPOBSCAC = ctod("  /  /  ")
        this.w_CCFLTREGC = space(1)
        this.w_CCFLRIFEC = space(1)
        this.w_CCTIPDOC = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GPFLTCCO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 6 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+6<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_15.CCCODICE as CCCODICE115"+ ",link_1_15.CCDESCRI as CCDESCRI115"+ ",link_1_15.CCDTOBSO as CCDTOBSO115"+ ",link_1_15.CCTIPREG as CCTIPREG115"+ ",link_1_15.CCFLRIFE as CCFLRIFE115"+ ",link_1_15.CCTIPDOC as CCTIPDOC115"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_15 on PROGRATA.GPFLTCCO=link_1_15.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_15"
          i_cKey=i_cKey+'+" and PROGRATA.GPFLTCCO=link_1_15.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+6
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oGPSERIAL_1_2.value==this.w_GPSERIAL)
      this.oPgFrm.Page1.oPag.oGPSERIAL_1_2.value=this.w_GPSERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDATGEN_1_5.value==this.w_GPDATGEN)
      this.oPgFrm.Page1.oPag.oGPDATGEN_1_5.value=this.w_GPDATGEN
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCODESE_1_6.value==this.w_GPCODESE)
      this.oPgFrm.Page1.oPag.oGPCODESE_1_6.value=this.w_GPCODESE
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDESCRI_1_8.value==this.w_GPDESCRI)
      this.oPgFrm.Page1.oPag.oGPDESCRI_1_8.value=this.w_GPDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oGPFLDATD_1_9.RadioValue()==this.w_GPFLDATD)
      this.oPgFrm.Page1.oPag.oGPFLDATD_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCAUCON_1_10.value==this.w_GPCAUCON)
      this.oPgFrm.Page1.oPag.oGPCAUCON_1_10.value=this.w_GPCAUCON
    endif
    if not(this.oPgFrm.Page1.oPag.oGPFLRGSC_1_11.RadioValue()==this.w_GPFLRGSC)
      this.oPgFrm.Page1.oPag.oGPFLRGSC_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGPSTORNO_1_12.RadioValue()==this.w_GPSTORNO)
      this.oPgFrm.Page1.oPag.oGPSTORNO_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGP_CONTO_1_13.value==this.w_GP_CONTO)
      this.oPgFrm.Page1.oPag.oGP_CONTO_1_13.value=this.w_GP_CONTO
    endif
    if not(this.oPgFrm.Page1.oPag.oGPFLTCCO_1_15.value==this.w_GPFLTCCO)
      this.oPgFrm.Page1.oPag.oGPFLTCCO_1_15.value=this.w_GPFLTCCO
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDATINI_1_18.value==this.w_GPDATINI)
      this.oPgFrm.Page1.oPag.oGPDATINI_1_18.value=this.w_GPDATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGPDATFIN_1_20.value==this.w_GPDATFIN)
      this.oPgFrm.Page1.oPag.oGPDATFIN_1_20.value=this.w_GPDATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGPREGINI_1_22.value==this.w_GPREGINI)
      this.oPgFrm.Page1.oPag.oGPREGINI_1_22.value=this.w_GPREGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGPREGFIN_1_24.value==this.w_GPREGFIN)
      this.oPgFrm.Page1.oPag.oGPREGFIN_1_24.value=this.w_GPREGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCIVINI_1_25.value==this.w_GPCIVINI)
      this.oPgFrm.Page1.oPag.oGPCIVINI_1_25.value=this.w_GPCIVINI
    endif
    if not(this.oPgFrm.Page1.oPag.oGPCIVFIN_1_26.value==this.w_GPCIVFIN)
      this.oPgFrm.Page1.oPag.oGPCIVFIN_1_26.value=this.w_GPCIVFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oGPSTAREG_1_28.RadioValue()==this.w_GPSTAREG)
      this.oPgFrm.Page1.oPag.oGPSTAREG_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_37.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_37.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCRI_1_38.value==this.w_CCDESCRI)
      this.oPgFrm.Page1.oPag.oCCDESCRI_1_38.value=this.w_CCDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCCDESCFL_1_39.value==this.w_CCDESCFL)
      this.oPgFrm.Page1.oPag.oCCDESCFL_1_39.value=this.w_CCDESCFL
    endif
    cp_SetControlsValueExtFlds(this,'PROGRATA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_GPDATGEN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPDATGEN_1_5.SetFocus()
            i_bnoObbl = !empty(.w_GPDATGEN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GPCODESE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPCODESE_1_6.SetFocus()
            i_bnoObbl = !empty(.w_GPCODESE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GPCAUCON)) or not(.w_CCFLTREG = 'N' AND .w_CCFLPART = 'N' AND .w_CCFLSALF = 'N' AND .w_CCFLSALI = 'N' AND (.w_CCDTOBSO > .w_OBTEST OR EMPTY(.w_CCDTOBSO)) AND .w_CCFLASSE<>'S'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPCAUCON_1_10.SetFocus()
            i_bnoObbl = !empty(.w_GPCAUCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile non valida, inesistente oppure obsoleta")
          case   ((empty(.w_GP_CONTO)) or not(CALCSEZ(.w_ANCONSUP)$'C/R'))  and (.w_GPSTORNO='C')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGP_CONTO_1_13.SetFocus()
            i_bnoObbl = !empty(.w_GP_CONTO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_CCFLTREGC = 'A' AND (.w_GPOBSCAC>.w_OBTEST OR EMPTY(.w_GPOBSCAC)) AND .w_CCFLRIFEC = 'F' AND NOT(.w_CCTIPDOC$'FE-NE'))  and not(empty(.w_GPFLTCCO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPFLTCCO_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale contabile non valida, inesistente oppure obsoleta")
          case   (empty(.w_GPDATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPDATINI_1_18.SetFocus()
            i_bnoObbl = !empty(.w_GPDATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale deve essere minore o uguale alla data finale e le date non devono appartenere a due anni solari differenti")
          case   ((empty(.w_GPDATFIN)) or not(EMPTY(.w_GPDATINI) Or .w_GPDATINI<=.w_GPDATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPDATFIN_1_20.SetFocus()
            i_bnoObbl = !empty(.w_GPDATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale deve essere maggiore o uguale alla data iniziale")
          case   (empty(.w_GPREGINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPREGINI_1_22.SetFocus()
            i_bnoObbl = !empty(.w_GPREGINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale deve essere minore o uguale alla data finale e le date non devono appartenere a due anni solari differenti")
          case   ((empty(.w_GPREGFIN)) or not(EMPTY(.w_GPREGINI) Or (.w_GPREGINI<=.w_GPREGFIN AND YEAR(.w_GPREGINI)=YEAR(.w_GPREGFIN) )))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPREGFIN_1_24.SetFocus()
            i_bnoObbl = !empty(.w_GPREGFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale deve essere maggiore o uguale alla data iniziale e le date non devono appartenere a due anni solari differenti")
          case   (empty(.w_GPCIVINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPCIVINI_1_25.SetFocus()
            i_bnoObbl = !empty(.w_GPCIVINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale deve essere minore o uguale alla data finale e le date non devono appartenere a due anni solari differenti")
          case   ((empty(.w_GPCIVFIN)) or not(EMPTY(.w_GPCIVINI) Or (.w_GPCIVINI<=.w_GPCIVFIN AND YEAR(.w_GPCIVINI)=YEAR(.w_GPCIVFIN) )))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGPCIVFIN_1_26.SetFocus()
            i_bnoObbl = !empty(.w_GPCIVFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data finale deve essere maggiore o uguale alla data iniziale e le date non devono appartenere a due anni solari differenti")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gscg_agp
      vq_exec('query\gscgDbgp', this, '_curs_check')
      if used("_curs_check")
        if reccount('_curs_check')>0 and not ah_yesno("Esistono giroconti da pro-rata con selezione sugli stessi movimenti.%0Si vuole procedere con la generazione?")
          i_bRes = .f.
          i_cErrorMsg=ah_MsgFormat("Operazione abbandonata")
        endif
        select _curs_check
        use
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_GPDATGEN = this.w_GPDATGEN
    this.o_GPFLDATD = this.w_GPFLDATD
    this.o_GPCAUCON = this.w_GPCAUCON
    this.o_GPSTORNO = this.w_GPSTORNO
    this.o_GPDATINI = this.w_GPDATINI
    this.o_GPREGINI = this.w_GPREGINI
    this.o_GPCIVINI = this.w_GPCIVINI
    return

  func CanEdit()
    local i_res
    i_res=.F.
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile modificare"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgscg_agpPag1 as StdContainer
  Width  = 616
  height = 409
  stdWidth  = 616
  stdheight = 409
  resizeXpos=346
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGPSERIAL_1_2 as StdField with uid="QUKRGNWDYU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_GPSERIAL", cQueryName = "GPSERIAL",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 69181518,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=180, Top=12, InputMask=replicate('X',10)

  add object oGPDATGEN_1_5 as StdField with uid="XNGKOKWOAN",rtseq=4,rtrep=.f.,;
    cFormVar = "w_GPDATGEN", cQueryName = "GPDATGEN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data generazione del movimento da generare",;
    HelpContextID = 167473076,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=180, Top=42

  add object oGPCODESE_1_6 as StdField with uid="HSHDRIIJGI",rtseq=5,rtrep=.f.,;
    cFormVar = "w_GPCODESE", cQueryName = "GPCODESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio di competenza della registrazione",;
    HelpContextID = 150380629,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=468, Top=42, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_AZIENDA", oKey_2_1="ESCODESE", oKey_2_2="this.w_GPCODESE"

  func oGPCODESE_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCODESE_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPCODESE_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_AZIENDA)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_AZIENDA)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oGPCODESE_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oGPDESCRI_1_8 as StdField with uid="NWLFCTXSDJ",rtseq=6,rtrep=.f.,;
    cFormVar = "w_GPDESCRI", cQueryName = "GPDESCRI",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 168857681,;
   bGlobalFont=.t.,;
    Height=21, Width=364, Left=180, Top=72, InputMask=replicate('X',50)

  add object oGPFLDATD_1_9 as StdCheck with uid="PVFJQSZUIN",rtseq=7,rtrep=.f.,left=180, top=96, caption="Giroconto in data registrazione documento di origine",;
    ToolTipText = "I movimenti generati mantengono la data registrazione del movimento di origine",;
    HelpContextID = 217673814,;
    cFormVar="w_GPFLDATD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGPFLDATD_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGPFLDATD_1_9.GetRadio()
    this.Parent.oContained.w_GPFLDATD = this.RadioValue()
    return .t.
  endfunc

  func oGPFLDATD_1_9.SetRadio()
    this.Parent.oContained.w_GPFLDATD=trim(this.Parent.oContained.w_GPFLDATD)
    this.value = ;
      iif(this.Parent.oContained.w_GPFLDATD=='S',1,;
      0)
  endfunc

  add object oGPCAUCON_1_10 as StdField with uid="THMGGZGFAM",rtseq=8,rtrep=.f.,;
    cFormVar = "w_GPCAUCON", cQueryName = "GPCAUCON",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile non valida, inesistente oppure obsoleta",;
    ToolTipText = "Causale contabile del movimento da generare",;
    HelpContextID = 167026764,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=180, Top=119, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_GPCAUCON"

  func oGPCAUCON_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPCAUCON_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPCAUCON_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oGPCAUCON_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSCG_AGP.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oGPCAUCON_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_GPCAUCON
     i_obj.ecpSave()
  endproc

  add object oGPFLRGSC_1_11 as StdCheck with uid="CTGHOEKFMH",rtseq=9,rtrep=.f.,left=180, top=143, caption="Scritture raggruppate",;
    ToolTipText = "genera un'unica scrittura raggruppata",;
    HelpContextID = 102330455,;
    cFormVar="w_GPFLRGSC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oGPFLRGSC_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oGPFLRGSC_1_11.GetRadio()
    this.Parent.oContained.w_GPFLRGSC = this.RadioValue()
    return .t.
  endfunc

  func oGPFLRGSC_1_11.SetRadio()
    this.Parent.oContained.w_GPFLRGSC=trim(this.Parent.oContained.w_GPFLRGSC)
    this.value = ;
      iif(this.Parent.oContained.w_GPFLRGSC=='S',1,;
      0)
  endfunc

  func oGPFLRGSC_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPFLDATD='N' AND .w_CCFLRIFE='N')
    endwith
   endif
  endfunc


  add object oGPSTORNO_1_12 as StdCombo with uid="HNEOVOLWUA",rtseq=10,rtrep=.f.,left=181,top=167,width=216,height=21;
    , ToolTipText = "Movimento generato su un unico conto di storno";
    , HelpContextID = 188784715;
    , cFormVar="w_GPSTORNO",RowSource=""+"Conti movimentati,"+"Conto specifico", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGPSTORNO_1_12.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oGPSTORNO_1_12.GetRadio()
    this.Parent.oContained.w_GPSTORNO = this.RadioValue()
    return .t.
  endfunc

  func oGPSTORNO_1_12.SetRadio()
    this.Parent.oContained.w_GPSTORNO=trim(this.Parent.oContained.w_GPSTORNO)
    this.value = ;
      iif(this.Parent.oContained.w_GPSTORNO=='M',1,;
      iif(this.Parent.oContained.w_GPSTORNO=='C',2,;
      0))
  endfunc

  add object oGP_CONTO_1_13 as StdField with uid="BKSJXIQWLM",rtseq=11,rtrep=.f.,;
    cFormVar = "w_GP_CONTO", cQueryName = "GP_CONTO",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Conto di storno",;
    HelpContextID = 11476917,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=181, Top=194, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_GPTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_GP_CONTO"

  func oGP_CONTO_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GPSTORNO='C')
    endwith
   endif
  endfunc

  func oGP_CONTO_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oGP_CONTO_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGP_CONTO_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_GPTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_GPTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oGP_CONTO_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_API',"Conti di storno",'CONTIECO.CONTI_VZM',this.parent.oContained
  endproc
  proc oGP_CONTO_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_API()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_GPTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_GP_CONTO
     i_obj.ecpSave()
  endproc

  add object oGPFLTCCO_1_15 as StdField with uid="TYUDEJDEPM",rtseq=12,rtrep=.f.,;
    cFormVar = "w_GPFLTCCO", cQueryName = "GPFLTCCO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale contabile non valida, inesistente oppure obsoleta",;
    ToolTipText = "Causale contabile da filtrare nella selezione dei movimenti",;
    HelpContextID = 167342155,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=181, Top=248, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", oKey_1_1="CCCODICE", oKey_1_2="this.w_GPFLTCCO"

  func oGPFLTCCO_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oGPFLTCCO_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGPFLTCCO_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oGPFLTCCO_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali contabili",'GSCG1AGP.CAU_CONT_VZM',this.parent.oContained
  endproc

  add object oGPDATINI_1_18 as StdField with uid="QXUTELZKVK",rtseq=14,rtrep=.f.,;
    cFormVar = "w_GPDATINI", cQueryName = "GPDATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale deve essere minore o uguale alla data finale e le date non devono appartenere a due anni solari differenti",;
    ToolTipText = "Da data documento",;
    HelpContextID = 67407953,;
   bGlobalFont=.t.,;
    Height=22, Width=76, Left=181, Top=274

  add object oGPDATFIN_1_20 as StdField with uid="FZJNGWPIMU",rtseq=15,rtrep=.f.,;
    cFormVar = "w_GPDATFIN", cQueryName = "GPDATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale deve essere maggiore o uguale alla data iniziale",;
    ToolTipText = "A data documento",;
    HelpContextID = 150695860,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=468, Top=274

  func oGPDATFIN_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_GPDATINI) Or .w_GPDATINI<=.w_GPDATFIN)
    endwith
    return bRes
  endfunc

  add object oGPREGINI_1_22 as StdField with uid="TPSRXEJBUF",rtseq=16,rtrep=.f.,;
    cFormVar = "w_GPREGINI", cQueryName = "GPREGINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale deve essere minore o uguale alla data finale e le date non devono appartenere a due anni solari differenti",;
    ToolTipText = "Da data registrazione",;
    HelpContextID = 80719953,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=181, Top=300

  add object oGPREGFIN_1_24 as StdField with uid="KXUGXLWCLZ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_GPREGFIN", cQueryName = "GPREGFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale deve essere maggiore o uguale alla data iniziale e le date non devono appartenere a due anni solari differenti",;
    ToolTipText = "A data registrazione",;
    HelpContextID = 137383860,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=468, Top=300

  func oGPREGFIN_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_GPREGINI) Or (.w_GPREGINI<=.w_GPREGFIN AND YEAR(.w_GPREGINI)=YEAR(.w_GPREGFIN) ))
    endwith
    return bRes
  endfunc

  add object oGPCIVINI_1_25 as StdField with uid="VPLYAPLRPN",rtseq=18,rtrep=.f.,;
    cFormVar = "w_GPCIVINI", cQueryName = "GPCIVINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale deve essere minore o uguale alla data finale e le date non devono appartenere a due anni solari differenti",;
    ToolTipText = "Da data competenza IVA",;
    HelpContextID = 64790609,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=180, Top=326

  add object oGPCIVFIN_1_26 as StdField with uid="TROTCTLPGC",rtseq=19,rtrep=.f.,;
    cFormVar = "w_GPCIVFIN", cQueryName = "GPCIVFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data finale deve essere maggiore o uguale alla data iniziale e le date non devono appartenere a due anni solari differenti",;
    ToolTipText = "A data competenza IVA",;
    HelpContextID = 153313204,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=467, Top=326

  func oGPCIVFIN_1_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_GPCIVINI) Or (.w_GPCIVINI<=.w_GPCIVFIN AND YEAR(.w_GPCIVINI)=YEAR(.w_GPCIVFIN) ))
    endwith
    return bRes
  endfunc


  add object oGPSTAREG_1_28 as StdCombo with uid="NIHIXIBQKF",rtseq=20,rtrep=.f.,left=181,top=352,width=146,height=21;
    , ToolTipText = "Stato registrazione da filtrare";
    , HelpContextID = 64970669;
    , cFormVar="w_GPSTAREG",RowSource=""+"Confermate,"+"Provvisorie,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGPSTAREG_1_28.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oGPSTAREG_1_28.GetRadio()
    this.Parent.oContained.w_GPSTAREG = this.RadioValue()
    return .t.
  endfunc

  func oGPSTAREG_1_28.SetRadio()
    this.Parent.oContained.w_GPSTAREG=trim(this.Parent.oContained.w_GPSTAREG)
    this.value = ;
      iif(this.Parent.oContained.w_GPSTAREG=='N',1,;
      iif(this.Parent.oContained.w_GPSTAREG=='S',2,;
      iif(this.Parent.oContained.w_GPSTAREG=='T',3,;
      0)))
  endfunc

  add object oANDESCRI_1_37 as StdField with uid="IRIAFOHWAQ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 168858289,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=317, Top=194, InputMask=replicate('X',40)

  add object oCCDESCRI_1_38 as StdField with uid="SIWWMBVRZU",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CCDESCRI", cQueryName = "CCDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 168861073,;
   bGlobalFont=.t.,;
    Height=21, Width=299, Left=252, Top=119, InputMask=replicate('X',35)

  add object oCCDESCFL_1_39 as StdField with uid="JONWTIOBYB",rtseq=25,rtrep=.f.,;
    cFormVar = "w_CCDESCFL", cQueryName = "CCDESCFL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 99574386,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=252, Top=248, InputMask=replicate('X',35)


  add object oBtn_1_62 as StdButton with uid="AITCBYAXMM",left=564, top=363, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per generare i movimenti";
    , HelpContextID = 167631770;
    , caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_62.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_4 as StdString with uid="CWPQSMPIAG",Visible=.t., Left=8, Top=15,;
    Alignment=1, Width=169, Height=18,;
    Caption="Seriale generazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="ONRVCHCJJO",Visible=.t., Left=15, Top=44,;
    Alignment=1, Width=162, Height=18,;
    Caption="Data registrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="HAZSECKLKJ",Visible=.t., Left=8, Top=123,;
    Alignment=1, Width=169, Height=18,;
    Caption="Causale contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="RPAJODUVFS",Visible=.t., Left=8, Top=250,;
    Alignment=1, Width=169, Height=18,;
    Caption="Causale contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="AONXPSXSEX",Visible=.t., Left=8, Top=276,;
    Alignment=1, Width=169, Height=18,;
    Caption="Da data documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="AYTBTYSFTE",Visible=.t., Left=297, Top=276,;
    Alignment=1, Width=169, Height=18,;
    Caption="A data documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="MEORYVOSXE",Visible=.t., Left=8, Top=302,;
    Alignment=1, Width=169, Height=18,;
    Caption="Da data registrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="XBMLQCJWVK",Visible=.t., Left=297, Top=302,;
    Alignment=1, Width=169, Height=18,;
    Caption="A data registrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="GUYYTGKROA",Visible=.t., Left=8, Top=354,;
    Alignment=1, Width=169, Height=18,;
    Caption="Stato registrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="USIHYNTUZP",Visible=.t., Left=8, Top=75,;
    Alignment=1, Width=169, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="EMVWSLWMEC",Visible=.t., Left=8, Top=170,;
    Alignment=1, Width=169, Height=18,;
    Caption="Storno su:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="NZRHCYNVDW",Visible=.t., Left=8, Top=198,;
    Alignment=1, Width=169, Height=18,;
    Caption="Conto di storno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="NZJFTKIJBW",Visible=.t., Left=40, Top=223,;
    Alignment=0, Width=112, Height=18,;
    Caption="Filtri"  ;
  , bGlobalFont=.t.

  add object oStr_1_63 as StdString with uid="LSBNNHZLNC",Visible=.t., Left=306, Top=44,;
    Alignment=1, Width=160, Height=18,;
    Caption="Esercizio registrazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_64 as StdString with uid="FTRHGEZJTM",Visible=.t., Left=7, Top=328,;
    Alignment=1, Width=169, Height=18,;
    Caption="Da data competenza IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="QDWWAWLFRU",Visible=.t., Left=296, Top=328,;
    Alignment=1, Width=169, Height=18,;
    Caption="A data competenza IVA:"  ;
  , bGlobalFont=.t.

  add object oBox_1_34 as StdBox with uid="TVOBQZZZEE",left=35, top=239, width=581,height=2
enddefine
define class tgscg_agpPag2 as StdContainer
  Width  = 616
  height = 409
  stdWidth  = 616
  stdheight = 409
  resizeXpos=174
  resizeYpos=121
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object Zoom as cp_zoombox with uid="STQPWCLWTQ",left=-1, top=5, width=614,height=356,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="PNT_MAST",cZoomFile="GSCG_AGP",bOptions=.t.,bAdvOptions=.t.,bReadOnly=.t.,bRetriveAllRows=.f.,bQueryOnLoad=.t.,cMenuFile="",cZoomOnZoom="",;
    cEvent = "Init,Delete end,Insert end,Update end,Load",;
    nPag=2;
    , HelpContextID = 133045786


  add object oBtn_2_3 as StdButton with uid="VLJSABGYME",left=6, top=363, width=48,height=45,;
    CpPicture="bmp\VERIFICA.bmp", caption="", nPag=2;
    , ToolTipText = "Visualizza la registrazione di prima nota associata";
    , HelpContextID = 165024266;
    , tabstop=.f.,Caption='\<Reg.Cont.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_3.Click()
      with this.Parent.oContained
        do GSCG1BGR with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_3.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(NVL(.w_ASRIFEPN,'')))
      endwith
    endif
  endfunc


  add object oObj_2_4 as cp_runprogram with uid="FIPIPWUQTT",left=4, top=430, width=200,height=25,;
    caption='GSCG1BGR',;
   bGlobalFont=.t.,;
    prg="GSCG1BGR",;
    cEvent = "w_zoom selected",;
    nPag=2;
    , ToolTipText = "Apre la registrazione di prima nota dallo zoom";
    , HelpContextID = 47276728
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_agp','PROGRATA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".GPSERIAL=PROGRATA.GPSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
