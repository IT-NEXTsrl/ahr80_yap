* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bkk                                                        *
*              Lancia programma collegato                                      *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_186]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2011-06-24                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bkk",oParentObject,m.pTIPO)
return(i_retval)

define class tgsar_bkk as StdBatch
  * --- Local variables
  pTIPO = space(1)
  w_PROG = .NULL.
  w_OBJ = .NULL.
  w_PNOTA = .f.
  w_PTIPCON = space(1)
  w_PCODCON = space(15)
  w_PCODESE = space(4)
  w_PDATFIN = ctod("  /  /  ")
  w_CODLING = space(3)
  w_OBJ2 = .NULL.
  w_SERDOC = space(10)
  w_SERDOC = space(10)
  w_SERDCO = space(10)
  w_MVCODCON = space(15)
  w_MVTIPCON = space(1)
  w_PNTIPCON = space(1)
  w_PNCODCON = space(15)
  w_PNCODESE = space(4)
  w_PNDATREG = ctod("  /  /  ")
  w_SERPNT = space(10)
  w_OBJ1 = .NULL.
  w_OBJ2 = .NULL.
  * --- WorkFile variables
  CONTI_idx=0
  DOC_MAST_idx=0
  PNT_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia varie maschere dalle seguenti anagrafiche:  GSAR_ACL, GSAR_AFR, GSMA_AAR
    * --- chiave...
    * --- Var.per Clienti e Fornitori
    * --- Var. per Articoli
    * --- Saldi Conti Generici
    do case
      case this.pTIPO="A"
        * --- Apre masch.VISUALIZZA SCHEDE CONTABILI
        * --- VARIABILI DA PASSARE ALLA MASCHERA
        this.w_PNOTA = .T.
        this.w_PTIPCON = this.oParentObject.w_ANTIPCON
        this.w_PCODCON = this.oParentObject.w_ANCODICE
        this.w_PCODESE = g_CODESE
        this.w_PDATFIN = g_FINESE
        do GSCG_SZM with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTIPO="B"
        do case
          case this.oParentObject.w_ANTIPCON="C"
            * --- Apre masch.VISUALIZZA DOCUMENTI DI VENDITA
            this.w_PROG = GSVE_SZM()
            * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
            if !(this.w_PROG.bSec1)
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              i_retcode = 'stop'
              return
            endif
            this.w_PROG.w_CLISEL = this.oParentObject.w_ANCODICE
            this.w_OBJ = this.w_PROG.GetcTRL("w_CLISEL")
            this.w_OBJ.Check()     
            this.w_PROG.w_MAGALT = "X"
            this.w_PROG.NotifyEvent("LanciaCLI")     
          case this.oParentObject.w_ANTIPCON="F"
            * --- Apre masch.VISUALIZZA DOCUMENTI D'ACQUISTO
            this.w_PROG = GSAC_SZM()
            * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
            if !(this.w_PROG.bSec1)
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              i_retcode = 'stop'
              return
            endif
            this.w_PROG.w_CLISEL = this.oParentObject.w_ANCODICE
            this.w_OBJ = this.w_PROG.GetcTRL("w_CLISEL")
            this.w_OBJ.Check()     
            this.w_PROG.w_MAGALT = "X"
            this.w_PROG.NotifyEvent("LanciaFOR")     
        endcase
      case this.pTIPO="C"
        do case
          case this.oParentObject.w_ANTIPCON="C"
            * --- Apre masch.VISUALIZZA ORDINI relativo al Cliente
            this.w_PROG = GSOR_SZM()
            * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
            if !(this.w_PROG.bSec1)
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              i_retcode = 'stop'
              return
            endif
            this.w_PROG.w_FLVEAC1 = "V"
            this.w_PROG.w_TIPCLF = "C"
            this.w_PROG.o_TIPCLF = "C"
            this.w_PROG.w_CLISEL = this.oParentObject.w_ANCODICE
            this.w_OBJ = this.w_PROG.GetcTRL("w_CLISEL")
            this.w_OBJ.Check()     
            this.w_PROG.mCalc(.T.)     
            this.w_PROG.NotifyEvent("LanciaCLI")     
          case this.oParentObject.w_ANTIPCON="F"
            * --- Apre masch.VISUALIZZA ORDINI relativo al Fornitore
            this.w_PROG = GSOR_SZM()
            * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
            if !(this.w_PROG.bSec1)
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              i_retcode = 'stop'
              return
            endif
            this.w_PROG.w_FLVEAC1 = "A"
            this.w_PROG.w_TIPCLF = "F"
            this.w_PROG.o_TIPCLF = "F"
            this.w_PROG.w_CLISEL = this.oParentObject.w_ANCODICE
            this.w_OBJ = this.w_PROG.GetcTRL("w_CLISEL")
            this.w_OBJ.Check()     
            this.w_PROG.mCalc(.T.)     
            this.w_PROG.NotifyEvent("LanciaCLI")     
        endcase
      case this.pTIPO="D"
        do case
          case this.oParentObject.w_ANTIPCON="C"
            * --- Apre masch.PROSPETTO DEL VENDUTO
            this.w_PROG = GSMA_SDV()
            * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
            if !(this.w_PROG.bSec1)
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              i_retcode = 'stop'
              return
            endif
            this.w_PROG.w_cliente = this.oParentObject.w_ANCODICE
            this.w_OBJ = this.w_PROG.GetcTRL("w_cliente")
            this.w_OBJ.Check()     
            this.w_PROG.mCalc(.T.)     
          case this.oParentObject.w_ANTIPCON="F"
            * --- Apre masch.PROSPETTO DEGLI ACQUISTI
            this.w_PROG = GSAC_SPA()
            * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
            if !(this.w_PROG.bSec1)
              this.Pag2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              i_retcode = 'stop'
              return
            endif
            this.w_PROG.w_cliente = this.oParentObject.w_ANCODICE
            this.w_OBJ = this.w_PROG.GetcTRL("w_cliente")
            this.w_OBJ.Check()     
            this.w_PROG.mCalc(.T.)     
        endcase
      case this.pTIPO$ "E-L"
        * --- Apre masch.MANUTENZIONE PARTITE relative al Cliente
        if Not Empty(this.oParentObject.w_ANCODICE)
          this.w_PROG = GSTE_KMS()
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if !(this.w_PROG.bSec1) 
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_retcode = 'stop'
            return
          endif
          this.w_PROG.w_CODCON = this.oParentObject.w_ANCODICE
          this.w_OBJ = this.w_PROG.GetcTRL("w_CODCON")
          if this.oParentObject.w_ANTIPCON="F"
            this.w_PROG.w_TIPCON = "F"
          endif
          this.w_OBJ.Value = this.oParentObject.w_ANCODICE
          this.w_OBJ.Check()     
          this.w_PROG.w_CODCON = this.oParentObject.w_ANCODICE
          if this.pTIPO="L"
            this.w_PROG.w_NDOINI = this.oParentObject.w_LNUMDOC
            this.w_PROG.w_NDOFIN = this.oParentObject.w_LNUMDOC
            this.w_PROG.w_ADOINI = this.oParentObject.w_LALFDOC
            this.w_PROG.w_ADOFIN = this.oParentObject.w_LALFDOC
            this.w_PROG.w_DDOINI = this.oParentObject.w_LDATDOC
            this.w_PROG.w_DDOFIN = this.oParentObject.w_LDATDOC
            this.w_PROG.w_SCAINI = g_DATMIN
            this.w_PROG.w_SCAFIN = g_FINESE
            this.w_PROG.w_FLSALD = "T"
          endif
          this.w_PROG.NotifyEvent("LanciaCLI ")     
        else
          Ah_errormsg("Attenzione documento privo di intestatario")
        endif
      case this.pTIPO="F"
        if this.oParentObject.w_ANTIPCON $ "C-F"
          * --- Apre masch.STAMPA ESTRATTO CONTO relativa al Clienti\Fornitori
          this.w_PROG = GSTE_KSC()
          * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
          if !(this.w_PROG.bSec1)
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            i_retcode = 'stop'
            return
          endif
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANCODLIN"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_ANCODICE);
                  +" and ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_ANTIPCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANCODLIN;
              from (i_cTable) where;
                  ANCODICE = this.oParentObject.w_ANCODICE;
                  and ANTIPCON = this.oParentObject.w_ANTIPCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODLING = NVL(cp_ToDate(_read_.ANCODLIN),cp_NullValue(_read_.ANCODLIN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_PROG.w_TIPCLF = this.oParentObject.w_ANTIPCON
          this.w_PROG.w_CODLIN = this.w_CODLING
          this.w_PROG.o_CODLIN = this.w_CODLING
          this.w_OBJ = this.w_PROG.GetcTRL("w_CODLIN")
          this.w_OBJ.Check()     
          this.w_PROG.w_CODINI = this.oParentObject.w_ANCODICE
          this.w_OBJ = this.w_PROG.GetcTRL("w_CODINI")
          this.w_OBJ.Check()     
          this.w_PROG.w_CODFIN = this.oParentObject.w_ANCODICE
          this.w_OBJ = this.w_PROG.GetcTRL("w_CODFIN")
          this.w_OBJ.Check()     
          this.w_PROG.mCalc(.T.)     
        endif
      case this.pTIPO="G"
        * --- Apre masch.VISUALIZZA SCHEDE DI MAGAZZINO dall'anagrafica Articoli
        this.w_PROG = GSMA_SZM()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_CODART = this.oParentObject.w_ARCODART
        this.w_OBJ = this.w_PROG.GetcTRL("w_CODART")
        this.w_OBJ.Check()     
        this.w_PROG.w_CODART2 = this.oParentObject.w_ARCODART
        this.w_OBJ2 = this.w_PROG.GetcTRL("w_CODART2")
        this.w_OBJ2.Check()     
        this.w_PROG.NotifyEvent("LanciaART")     
      case this.pTIPO="H"
        * --- Apre masch.MANUTENZIONE PARTITE relative al Cliente
        this.w_PROG = GSTE_KMS("E")
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1) 
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_ROWSCAD = -1
        this.w_PROG.w_VISUAL = .F.
        this.w_PROG.w_FLSALD = "T"
        this.w_PROG.w_SCAINI = g_DATMIN
        this.w_PROG.NotifyEvent("LanciaCLI ")     
      case this.pTIPO="I"
        if Not Empty(this.oParentObject.w_SERIALE)
          * --- Recupero intestatario e riferimento alla primanota
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVRIFCON,MVRIFDCO,MVCODCON,MVTIPCON"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVRIFCON,MVRIFDCO,MVCODCON,MVTIPCON;
              from (i_cTable) where;
                  MVSERIAL = this.oParentObject.w_SERIALE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_SERDOC = NVL(cp_ToDate(_read_.MVRIFCON),cp_NullValue(_read_.MVRIFCON))
            this.w_SERDCO = NVL(cp_ToDate(_read_.MVRIFDCO),cp_NullValue(_read_.MVRIFDCO))
            this.w_MVCODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
            this.w_MVTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if  Empty(this.w_SERDOC) AND Empty(this.w_SERDCO)
            if Type("g_SCHEDULER")<>"C" Or cp_GetGlobalVar("g_SCHEDULER") <> "S"
              * --- Lo schedulatore � attivo
              cp_SetGlobalVar("g_SCHEDULER","S")
            endif
            this.w_OBJ = gsal_kcd()
            this.w_OBJ.w_MVSERIAL = this.oParentObject.w_SERIALE
            this.w_OBJ.Ecpsave()     
            cp_SetGlobalVar("g_SCHEDULER","N")
          endif
        endif
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVRIFCON,MVRIFDCO,MVCODCON,MVTIPCON"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVRIFCON,MVRIFDCO,MVCODCON,MVTIPCON;
            from (i_cTable) where;
                MVSERIAL = this.oParentObject.w_SERIALE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SERDOC = NVL(cp_ToDate(_read_.MVRIFCON),cp_NullValue(_read_.MVRIFCON))
          this.w_SERDCO = NVL(cp_ToDate(_read_.MVRIFDCO),cp_NullValue(_read_.MVRIFDCO))
          this.w_MVCODCON = NVL(cp_ToDate(_read_.MVCODCON),cp_NullValue(_read_.MVCODCON))
          this.w_MVTIPCON = NVL(cp_ToDate(_read_.MVTIPCON),cp_NullValue(_read_.MVTIPCON))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if  Not Empty(this.w_SERDOC) or Not Empty(this.w_SERDCO)
          this.w_PROG = GSAR_ASC("A")
          this.w_PROG.Ecpload()     
          this.w_PROG.w_SERDOC = IIF(Not Empty(this.w_SERDCO),this.w_SERDCO,this.w_SERDOC)
          this.w_PROG.w_SCTIPCLF = this.w_MVTIPCON
          = setvaluelinked ( "M" , this.w_PROG , "w_SCCODCLF" , this.w_MVCODCON,this.w_MVTIPCON)
          this.w_PROG.Notifyevent("w_SCCODCLF Changed")     
          this.w_PROG.GSTE_MPA.Notifyevent("Salda")     
          this.w_PROG = .Null.
          this.w_OBJ = .Null.
        endif
      case this.pTIPO="P"
        * --- Visualizza schede di primanota
        * --- PNRIFDOC non � univoco nel caso di contabilizzazione acconti
        * --- Read from DOC_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVRIFCON"+;
            " from "+i_cTable+" DOC_MAST where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVRIFCON;
            from (i_cTable) where;
                MVSERIAL = this.oParentObject.w_SERIALE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SERPNT = NVL(cp_ToDate(_read_.MVRIFCON),cp_NullValue(_read_.MVRIFCON))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from PNT_MAST
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2],.t.,this.PNT_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PNTIPCLF,PNCODCLF,PNCODESE,PNDATREG"+;
            " from "+i_cTable+" PNT_MAST where ";
                +"PNSERIAL = "+cp_ToStrODBC(this.w_SERPNT);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PNTIPCLF,PNCODCLF,PNCODESE,PNDATREG;
            from (i_cTable) where;
                PNSERIAL = this.w_SERPNT;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PNTIPCON = NVL(cp_ToDate(_read_.PNTIPCLF),cp_NullValue(_read_.PNTIPCLF))
          this.w_PNCODCON = NVL(cp_ToDate(_read_.PNCODCLF),cp_NullValue(_read_.PNCODCLF))
          this.w_PNCODESE = NVL(cp_ToDate(_read_.PNCODESE),cp_NullValue(_read_.PNCODESE))
          this.w_PNDATREG = NVL(cp_ToDate(_read_.PNDATREG),cp_NullValue(_read_.PNDATREG))
          use
          if i_Rows=0
            Ah_errormsg("Attenzione, documento non contabilizzato")
            i_retcode = 'stop'
            return
          endif
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        do GSCG_BZM with this
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pTIPO="Q"
        * --- Apre masch.disponibilt� del tempo
        this.w_PROG = GSOR_SDT()
        * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
        if !(this.w_PROG.bSec1)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
        this.w_PROG.w_CODINI = this.oParentObject.w_ARCODART
        this.w_OBJ = this.w_PROG.GetcTRL("w_CODINI")
        this.w_OBJ.Check()     
        this.w_PROG.w_CODFIN = this.oParentObject.w_ARCODART
        this.w_OBJ1 = this.w_PROG.GetcTRL("w_CODFIN")
        this.w_OBJ1.Check()     
        this.w_PROG.w_SELMOV = "T"
        this.w_PROG.w_CODMAG = g_MAGAZI
        this.w_OBJ2 = this.w_PROG.GetcTRL("w_CODMAG")
        this.w_OBJ2.Check()     
        this.w_PROG.mCalc(.T.)     
    endcase
    if this.pTIPO<> "H" 
      this.bUpdateParentObject=.F.
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Messaggio di errore
    Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
  endproc


  proc Init(oParentObject,pTIPO)
    this.pTIPO=pTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='DOC_MAST'
    this.cWorkTables[3]='PNT_MAST'
    return(this.OpenAllTables(3))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO"
endproc
