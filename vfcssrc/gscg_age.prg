* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_age                                                        *
*              Generazione file allegati clienti/fornitori                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-07-13                                                      *
* Last revis.: 2012-04-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_age"))

* --- Class definition
define class tgscg_age as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 692
  Height = 325+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-04-05"
  HelpContextID=42607977
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=41

  * --- Constant Properties
  DAELGENE_IDX = 0
  AZIENDA_IDX = 0
  TITOLARI_IDX = 0
  SEDIAZIE_IDX = 0
  cFile = "DAELGENE"
  cKeySelect = "GESERIAL"
  cKeyWhere  = "GESERIAL=this.w_GESERIAL"
  cKeyWhereODBC = '"GESERIAL="+cp_ToStrODBC(this.w_GESERIAL)';

  cKeyWhereODBCqualified = '"DAELGENE.GESERIAL="+cp_ToStrODBC(this.w_GESERIAL)';

  cPrg = "gscg_age"
  cComment = "Generazione file allegati clienti/fornitori"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_GESERIAL = space(10)
  o_GESERIAL = space(10)
  w_GE__ANNO = space(4)
  w_GEDATINV = ctod('  /  /  ')
  w_CODAZI1 = space(5)
  w_OKALL = .F.
  w_CODAZI1 = space(5)
  w_COGTIT1 = space(25)
  w_NOMTIT1 = space(25)
  w_LOCTIT1 = space(30)
  w_DATNAS1 = ctod('  /  /  ')
  w_PROTIT1 = space(2)
  w_TIPSL = space(2)
  w_SEDILEG = space(5)
  w_SESIGLA = space(2)
  w_SECOMUNE = space(40)
  w_AZPERAZI = space(1)
  w_AZPIVAZI = space(12)
  w_AZCODFIS = space(16)
  w_GERAGSOC = space(70)
  w_GECOGNOM = space(26)
  w_GE__NOME = space(25)
  w_GECODFIS = space(16)
  w_GEPARIVA = space(11)
  w_GE_SESSO = space(1)
  w_GEDATNAS = ctod('  /  /  ')
  w_GECOMUNE = space(40)
  w_GEPROVIN = space(2)
  w_NOCODFIS = space(1)
  w_GEPROGIN = 0
  w_MAXNORIG = 0
  w_GECODSOG = space(16)
  w_AZCODNAZ = space(3)
  w_DirName = space(200)
  w_GENOMFIL = space(254)
  o_GENOMFIL = space(254)
  w_AZLOCAZI = space(30)
  w_AZPROAZI = space(2)
  w_SESSO1 = space(1)
  w_GECODINT = space(16)
  o_GECODINT = space(16)
  w_GECODCAF = space(5)
  w_GEIMPTRA = space(1)
  o_GEIMPTRA = space(1)
  w_GEDATIMP = ctod('  /  /  ')

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_GESERIAL = this.W_GESERIAL
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'DAELGENE','gscg_age')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_agePag1","gscg_age",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dati generali")
      .Pages(1).HelpContextID = 233375988
      .Pages(2).addobject("oPag","tgscg_agePag2","gscg_age",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati aggiuntivi")
      .Pages(2).HelpContextID = 34481107
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oGESERIAL_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='TITOLARI'
    this.cWorkTables[3]='SEDIAZIE'
    this.cWorkTables[4]='DAELGENE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DAELGENE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DAELGENE_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_GESERIAL = NVL(GESERIAL,space(10))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DAELGENE where GESERIAL=KeySet.GESERIAL
    *
    i_nConn = i_TableProp[this.DAELGENE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DAELGENE_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DAELGENE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DAELGENE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DAELGENE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'GESERIAL',this.w_GESERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CODAZI1 = i_CODAZI
        .w_CODAZI1 = i_CODAZI
        .w_COGTIT1 = space(25)
        .w_NOMTIT1 = space(25)
        .w_LOCTIT1 = space(30)
        .w_DATNAS1 = ctod("  /  /  ")
        .w_PROTIT1 = space(2)
        .w_TIPSL = 'SL'
        .w_SEDILEG = i_CODAZI
        .w_SESIGLA = space(2)
        .w_SECOMUNE = space(40)
        .w_AZPERAZI = space(1)
        .w_AZPIVAZI = space(12)
        .w_AZCODFIS = space(16)
        .w_NOCODFIS = ' '
        .w_MAXNORIG = 11650-3
        .w_AZCODNAZ = space(3)
        .w_DirName = sys(5)+sys(2003)+'\'
        .w_AZLOCAZI = space(30)
        .w_AZPROAZI = space(2)
        .w_SESSO1 = space(1)
        .w_GESERIAL = NVL(GESERIAL,space(10))
        .op_GESERIAL = .w_GESERIAL
        .w_GE__ANNO = NVL(GE__ANNO,space(4))
        .w_GEDATINV = NVL(cp_ToDate(GEDATINV),ctod("  /  /  "))
          .link_1_4('Load')
        .w_OKALL = iif(g_APPLICATION="ad hoc ENTERPRISE" , ((TYPE("g_AHEAESALCF")='C' AND g_AHEAESALCF='S') OR (TYPE("g_AHEAESTELE")='C' AND g_AHEAESTELE='S')), ((TYPE("g_AHRAVSALCF")='C' AND g_AHRAVSALCF='S') OR (TYPE("g_AHRAVEALCF")='C' AND g_AHRAVEALCF='S') OR (TYPE("g_AHRAVETELE")='C' AND g_AHRAVETELE='S') OR (TYPE("g_AHRAVSTELE")='C' AND g_AHRAVSTELE='S')))
          .link_1_6('Load')
          .link_1_13('Load')
        .w_GERAGSOC = NVL(GERAGSOC,space(70))
        .w_GECOGNOM = NVL(GECOGNOM,space(26))
        .w_GE__NOME = NVL(GE__NOME,space(25))
        .w_GECODFIS = NVL(GECODFIS,space(16))
        .w_GEPARIVA = NVL(GEPARIVA,space(11))
        .w_GE_SESSO = NVL(GE_SESSO,space(1))
        .w_GEDATNAS = NVL(cp_ToDate(GEDATNAS),ctod("  /  /  "))
        .w_GECOMUNE = NVL(GECOMUNE,space(40))
        .w_GEPROVIN = NVL(GEPROVIN,space(2))
        .w_GEPROGIN = NVL(GEPROGIN,0)
        .w_GECODSOG = NVL(GECODSOG,space(16))
        .w_GENOMFIL = NVL(GENOMFIL,space(254))
        .w_GECODINT = NVL(GECODINT,space(16))
        .w_GECODCAF = NVL(GECODCAF,space(5))
        .w_GEIMPTRA = NVL(GEIMPTRA,space(1))
        .w_GEDATIMP = NVL(cp_ToDate(GEDATIMP),ctod("  /  /  "))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'DAELGENE')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gscg_age
    endproc
    
    proc TerminateEdit()
        local bRes
        if type("this.activeControl.class")='C' and lower(this.activeControl.class)='stdbutton'
          this.bOkToTerminate=.t.
        else
          this.bOkToTerminate=.f.
          this.__dummy__.enabled=.t.
          this.__dummy__.SetFocus()
        endif
        return(this.bOkToTerminate)
    
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_GESERIAL = space(10)
      .w_GE__ANNO = space(4)
      .w_GEDATINV = ctod("  /  /  ")
      .w_CODAZI1 = space(5)
      .w_OKALL = .f.
      .w_CODAZI1 = space(5)
      .w_COGTIT1 = space(25)
      .w_NOMTIT1 = space(25)
      .w_LOCTIT1 = space(30)
      .w_DATNAS1 = ctod("  /  /  ")
      .w_PROTIT1 = space(2)
      .w_TIPSL = space(2)
      .w_SEDILEG = space(5)
      .w_SESIGLA = space(2)
      .w_SECOMUNE = space(40)
      .w_AZPERAZI = space(1)
      .w_AZPIVAZI = space(12)
      .w_AZCODFIS = space(16)
      .w_GERAGSOC = space(70)
      .w_GECOGNOM = space(26)
      .w_GE__NOME = space(25)
      .w_GECODFIS = space(16)
      .w_GEPARIVA = space(11)
      .w_GE_SESSO = space(1)
      .w_GEDATNAS = ctod("  /  /  ")
      .w_GECOMUNE = space(40)
      .w_GEPROVIN = space(2)
      .w_NOCODFIS = space(1)
      .w_GEPROGIN = 0
      .w_MAXNORIG = 0
      .w_GECODSOG = space(16)
      .w_AZCODNAZ = space(3)
      .w_DirName = space(200)
      .w_GENOMFIL = space(254)
      .w_AZLOCAZI = space(30)
      .w_AZPROAZI = space(2)
      .w_SESSO1 = space(1)
      .w_GECODINT = space(16)
      .w_GECODCAF = space(5)
      .w_GEIMPTRA = space(1)
      .w_GEDATIMP = ctod("  /  /  ")
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_GE__ANNO = alltrim(STR(YEAR(i_DATSYS)-1))
          .DoRTCalc(3,3,.f.)
        .w_CODAZI1 = i_CODAZI
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_CODAZI1))
          .link_1_4('Full')
          endif
        .w_OKALL = iif(g_APPLICATION="ad hoc ENTERPRISE" , ((TYPE("g_AHEAESALCF")='C' AND g_AHEAESALCF='S') OR (TYPE("g_AHEAESTELE")='C' AND g_AHEAESTELE='S')), ((TYPE("g_AHRAVSALCF")='C' AND g_AHRAVSALCF='S') OR (TYPE("g_AHRAVEALCF")='C' AND g_AHRAVEALCF='S') OR (TYPE("g_AHRAVETELE")='C' AND g_AHRAVETELE='S') OR (TYPE("g_AHRAVSTELE")='C' AND g_AHRAVSTELE='S')))
        .w_CODAZI1 = i_CODAZI
        .DoRTCalc(6,6,.f.)
          if not(empty(.w_CODAZI1))
          .link_1_6('Full')
          endif
          .DoRTCalc(7,11,.f.)
        .w_TIPSL = 'SL'
        .w_SEDILEG = i_CODAZI
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_SEDILEG))
          .link_1_13('Full')
          endif
          .DoRTCalc(14,18,.f.)
        .w_GERAGSOC = LEFT(UPPER(g_RAGAZI),70)
        .w_GECOGNOM = UPPER(.w_COGTIT1)
        .w_GE__NOME = UPPER(.w_NOMTIT1)
        .w_GECODFIS = left(upper(.w_AZCODFIS),16)
        .w_GEPARIVA = left(.w_AZPIVAZI, 11)
        .w_GE_SESSO = iif(.w_AZPERAZI='S' and not empty(.w_SESSO1), .w_SESSO1, 'M')
        .w_GEDATNAS = iif(.w_AZPERAZI='S', .w_DATNAS1, .w_GEDATNAS)
        .w_GECOMUNE = upper(iif(.w_AZPERAZI='S' and not empty(nvl(.w_LOCTIT1,'')), .w_LOCTIT1, iif(.w_AZPERAZI<>'S' and not empty(nvl(.w_SECOMUNE,'')), .w_SECOMUNE, .w_AZLOCAZI)))
        .w_GEPROVIN = upper(iif(.w_AZPERAZI='S' and not empty(nvl(.w_PROTIT1,'')), .w_PROTIT1, iif(.w_AZPERAZI<>'S' and not empty(nvl(.w_SESIGLA,'')), .w_SESIGLA, .w_AZPROAZI)))
        .w_NOCODFIS = ' '
          .DoRTCalc(29,29,.f.)
        .w_MAXNORIG = 11650-3
          .DoRTCalc(31,32,.f.)
        .w_DirName = sys(5)+sys(2003)+'\'
        .w_GENOMFIL = iif(file(.w_GENOMFIL), .w_GENOMFIL, IIF(.cFunction='Load',left(.w_DirName+'ELECF_'+alltrim(i_CODAZI)+'_'+.w_GESERIAL+space(254),254),''))
          .DoRTCalc(35,38,.f.)
        .w_GECODCAF = IIF(.w_GEIMPTRA='1' or Empty( .w_GECODINT ) ,space(5),.w_GECODCAF)
        .w_GEIMPTRA = iif( Empty( .w_GECODINT) , '0' , '1' )
        .w_GEDATIMP = iif( Empty( .w_GECODINT ) , Ctod('  -  -    ') , .w_GEDATIMP )
      endif
    endwith
    cp_BlankRecExtFlds(this,'DAELGENE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gscg_age
    this.bUpdated=.t.
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DAELGENE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DAELGENE_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"SEELE","i_CODAZI,w_GESERIAL")
      .op_CODAZI = .w_CODAZI
      .op_GESERIAL = .w_GESERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oGESERIAL_1_1.enabled = i_bVal
      .Page1.oPag.oGE__ANNO_1_2.enabled = i_bVal
      .Page1.oPag.oGEDATINV_1_3.enabled = i_bVal
      .Page1.oPag.oGERAGSOC_1_20.enabled = i_bVal
      .Page1.oPag.oGECOGNOM_1_21.enabled = i_bVal
      .Page1.oPag.oGE__NOME_1_22.enabled = i_bVal
      .Page1.oPag.oGECODFIS_1_24.enabled = i_bVal
      .Page1.oPag.oGEPARIVA_1_25.enabled = i_bVal
      .Page1.oPag.oGE_SESSO_1_26.enabled = i_bVal
      .Page1.oPag.oGEDATNAS_1_28.enabled = i_bVal
      .Page1.oPag.oGECOMUNE_1_30.enabled = i_bVal
      .Page1.oPag.oGEPROVIN_1_32.enabled = i_bVal
      .Page1.oPag.oNOCODFIS_1_33.enabled = i_bVal
      .Page2.oPag.oMAXNORIG_2_2.enabled = i_bVal
      .Page2.oPag.oGECODSOG_2_3.enabled = i_bVal
      .Page1.oPag.oGENOMFIL_1_44.enabled = i_bVal
      .Page2.oPag.oGECODINT_2_16.enabled = i_bVal
      .Page2.oPag.oGECODCAF_2_17.enabled = i_bVal
      .Page2.oPag.oGEIMPTRA_2_18.enabled = i_bVal
      .Page2.oPag.oGEDATIMP_2_20.enabled = i_bVal
      .Page1.oPag.oBtn_1_39.enabled = i_bVal
      .Page1.oPag.oBtn_1_41.enabled = i_bVal
      .Page1.oPag.oBtn_1_56.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oGESERIAL_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oGESERIAL_1_1.enabled = .t.
        .Page1.oPag.oGE__ANNO_1_2.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'DAELGENE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DAELGENE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GESERIAL,"GESERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GE__ANNO,"GE__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GEDATINV,"GEDATINV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GERAGSOC,"GERAGSOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GECOGNOM,"GECOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GE__NOME,"GE__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GECODFIS,"GECODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GEPARIVA,"GEPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GE_SESSO,"GE_SESSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GEDATNAS,"GEDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GECOMUNE,"GECOMUNE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GEPROVIN,"GEPROVIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GEPROGIN,"GEPROGIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GECODSOG,"GECODSOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GENOMFIL,"GENOMFIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GECODINT,"GECODINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GECODCAF,"GECODCAF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GEIMPTRA,"GEIMPTRA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_GEDATIMP,"GEDATIMP",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DAELGENE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DAELGENE_IDX,2])
    i_lTable = "DAELGENE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.DAELGENE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DAELGENE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DAELGENE_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DAELGENE_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"SEELE","i_CODAZI,w_GESERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DAELGENE
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DAELGENE')
        i_extval=cp_InsertValODBCExtFlds(this,'DAELGENE')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(GESERIAL,GE__ANNO,GEDATINV,GERAGSOC,GECOGNOM"+;
                  ",GE__NOME,GECODFIS,GEPARIVA,GE_SESSO,GEDATNAS"+;
                  ",GECOMUNE,GEPROVIN,GEPROGIN,GECODSOG,GENOMFIL"+;
                  ",GECODINT,GECODCAF,GEIMPTRA,GEDATIMP "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_GESERIAL)+;
                  ","+cp_ToStrODBC(this.w_GE__ANNO)+;
                  ","+cp_ToStrODBC(this.w_GEDATINV)+;
                  ","+cp_ToStrODBC(this.w_GERAGSOC)+;
                  ","+cp_ToStrODBC(this.w_GECOGNOM)+;
                  ","+cp_ToStrODBC(this.w_GE__NOME)+;
                  ","+cp_ToStrODBC(this.w_GECODFIS)+;
                  ","+cp_ToStrODBC(this.w_GEPARIVA)+;
                  ","+cp_ToStrODBC(this.w_GE_SESSO)+;
                  ","+cp_ToStrODBC(this.w_GEDATNAS)+;
                  ","+cp_ToStrODBC(this.w_GECOMUNE)+;
                  ","+cp_ToStrODBC(this.w_GEPROVIN)+;
                  ","+cp_ToStrODBC(this.w_GEPROGIN)+;
                  ","+cp_ToStrODBC(this.w_GECODSOG)+;
                  ","+cp_ToStrODBC(this.w_GENOMFIL)+;
                  ","+cp_ToStrODBC(this.w_GECODINT)+;
                  ","+cp_ToStrODBC(this.w_GECODCAF)+;
                  ","+cp_ToStrODBC(this.w_GEIMPTRA)+;
                  ","+cp_ToStrODBC(this.w_GEDATIMP)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DAELGENE')
        i_extval=cp_InsertValVFPExtFlds(this,'DAELGENE')
        cp_CheckDeletedKey(i_cTable,0,'GESERIAL',this.w_GESERIAL)
        INSERT INTO (i_cTable);
              (GESERIAL,GE__ANNO,GEDATINV,GERAGSOC,GECOGNOM,GE__NOME,GECODFIS,GEPARIVA,GE_SESSO,GEDATNAS,GECOMUNE,GEPROVIN,GEPROGIN,GECODSOG,GENOMFIL,GECODINT,GECODCAF,GEIMPTRA,GEDATIMP  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_GESERIAL;
                  ,this.w_GE__ANNO;
                  ,this.w_GEDATINV;
                  ,this.w_GERAGSOC;
                  ,this.w_GECOGNOM;
                  ,this.w_GE__NOME;
                  ,this.w_GECODFIS;
                  ,this.w_GEPARIVA;
                  ,this.w_GE_SESSO;
                  ,this.w_GEDATNAS;
                  ,this.w_GECOMUNE;
                  ,this.w_GEPROVIN;
                  ,this.w_GEPROGIN;
                  ,this.w_GECODSOG;
                  ,this.w_GENOMFIL;
                  ,this.w_GECODINT;
                  ,this.w_GECODCAF;
                  ,this.w_GEIMPTRA;
                  ,this.w_GEDATIMP;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DAELGENE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DAELGENE_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DAELGENE_IDX,i_nConn)
      *
      * update DAELGENE
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DAELGENE')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " GE__ANNO="+cp_ToStrODBC(this.w_GE__ANNO)+;
             ",GEDATINV="+cp_ToStrODBC(this.w_GEDATINV)+;
             ",GERAGSOC="+cp_ToStrODBC(this.w_GERAGSOC)+;
             ",GECOGNOM="+cp_ToStrODBC(this.w_GECOGNOM)+;
             ",GE__NOME="+cp_ToStrODBC(this.w_GE__NOME)+;
             ",GECODFIS="+cp_ToStrODBC(this.w_GECODFIS)+;
             ",GEPARIVA="+cp_ToStrODBC(this.w_GEPARIVA)+;
             ",GE_SESSO="+cp_ToStrODBC(this.w_GE_SESSO)+;
             ",GEDATNAS="+cp_ToStrODBC(this.w_GEDATNAS)+;
             ",GECOMUNE="+cp_ToStrODBC(this.w_GECOMUNE)+;
             ",GEPROVIN="+cp_ToStrODBC(this.w_GEPROVIN)+;
             ",GEPROGIN="+cp_ToStrODBC(this.w_GEPROGIN)+;
             ",GECODSOG="+cp_ToStrODBC(this.w_GECODSOG)+;
             ",GENOMFIL="+cp_ToStrODBC(this.w_GENOMFIL)+;
             ",GECODINT="+cp_ToStrODBC(this.w_GECODINT)+;
             ",GECODCAF="+cp_ToStrODBC(this.w_GECODCAF)+;
             ",GEIMPTRA="+cp_ToStrODBC(this.w_GEIMPTRA)+;
             ",GEDATIMP="+cp_ToStrODBC(this.w_GEDATIMP)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DAELGENE')
        i_cWhere = cp_PKFox(i_cTable  ,'GESERIAL',this.w_GESERIAL  )
        UPDATE (i_cTable) SET;
              GE__ANNO=this.w_GE__ANNO;
             ,GEDATINV=this.w_GEDATINV;
             ,GERAGSOC=this.w_GERAGSOC;
             ,GECOGNOM=this.w_GECOGNOM;
             ,GE__NOME=this.w_GE__NOME;
             ,GECODFIS=this.w_GECODFIS;
             ,GEPARIVA=this.w_GEPARIVA;
             ,GE_SESSO=this.w_GE_SESSO;
             ,GEDATNAS=this.w_GEDATNAS;
             ,GECOMUNE=this.w_GECOMUNE;
             ,GEPROVIN=this.w_GEPROVIN;
             ,GEPROGIN=this.w_GEPROGIN;
             ,GECODSOG=this.w_GECODSOG;
             ,GENOMFIL=this.w_GENOMFIL;
             ,GECODINT=this.w_GECODINT;
             ,GECODCAF=this.w_GECODCAF;
             ,GEIMPTRA=this.w_GEIMPTRA;
             ,GEDATIMP=this.w_GEDATIMP;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DAELGENE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DAELGENE_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DAELGENE_IDX,i_nConn)
      *
      * delete DAELGENE
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'GESERIAL',this.w_GESERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DAELGENE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DAELGENE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
          .link_1_4('Full')
            .w_OKALL = iif(g_APPLICATION="ad hoc ENTERPRISE" , ((TYPE("g_AHEAESALCF")='C' AND g_AHEAESALCF='S') OR (TYPE("g_AHEAESTELE")='C' AND g_AHEAESTELE='S')), ((TYPE("g_AHRAVSALCF")='C' AND g_AHRAVSALCF='S') OR (TYPE("g_AHRAVEALCF")='C' AND g_AHRAVEALCF='S') OR (TYPE("g_AHRAVETELE")='C' AND g_AHRAVETELE='S') OR (TYPE("g_AHRAVSTELE")='C' AND g_AHRAVSTELE='S')))
          .link_1_6('Full')
        .DoRTCalc(7,12,.t.)
          .link_1_13('Full')
        .DoRTCalc(14,33,.t.)
        if .o_GESERIAL<>.w_GESERIAL
            .w_GENOMFIL = iif(file(.w_GENOMFIL), .w_GENOMFIL, IIF(.cFunction='Load',left(.w_DirName+'ELECF_'+alltrim(i_CODAZI)+'_'+.w_GESERIAL+space(254),254),''))
        endif
        if .o_GENOMFIL<>.w_GENOMFIL
          .Calculate_ZFEOJBUZPN()
        endif
        .DoRTCalc(35,38,.t.)
        if .o_GEIMPTRA<>.w_GEIMPTRA.or. .o_GECODINT<>.w_GECODINT
            .w_GECODCAF = IIF(.w_GEIMPTRA='1' or Empty( .w_GECODINT ) ,space(5),.w_GECODCAF)
        endif
        if .o_GECODINT<>.w_GECODINT
            .w_GEIMPTRA = iif( Empty( .w_GECODINT) , '0' , '1' )
        endif
        if .o_GECODINT<>.w_GECODINT
            .w_GEDATIMP = iif( Empty( .w_GECODINT ) , Ctod('  -  -    ') , .w_GEDATIMP )
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"SEELE","i_CODAZI,w_GESERIAL")
          .op_GESERIAL = .w_GESERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_ZJFMMVWDUN()
    with this
          * --- Inizializzo nome file
          .w_GENOMFIL = left(.w_DirName+'ELECF_'+alltrim(i_CODAZI)+'_'+.w_GESERIAL+space(254),254)
    endwith
  endproc
  proc Calculate_ZFEOJBUZPN()
    with this
          * --- Inizializzo nome file
          .w_GENOMFIL = iif(not empty(.w_GENOMFIL), .w_GENOMFIL, left(.w_DirName+'ELECF_'+alltrim(i_CODAZI)+'_'+.w_GESERIAL+space(254),254))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oGECODCAF_2_17.enabled = this.oPgFrm.Page2.oPag.oGECODCAF_2_17.mCond()
    this.oPgFrm.Page2.oPag.oGEIMPTRA_2_18.enabled = this.oPgFrm.Page2.oPag.oGEIMPTRA_2_18.mCond()
    this.oPgFrm.Page2.oPag.oGEDATIMP_2_20.enabled = this.oPgFrm.Page2.oPag.oGEDATIMP_2_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_41.enabled = this.oPgFrm.Page1.oPag.oBtn_1_41.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_56.enabled = this.oPgFrm.Page1.oPag.oBtn_1_56.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oGERAGSOC_1_20.visible=!this.oPgFrm.Page1.oPag.oGERAGSOC_1_20.mHide()
    this.oPgFrm.Page1.oPag.oGECOGNOM_1_21.visible=!this.oPgFrm.Page1.oPag.oGECOGNOM_1_21.mHide()
    this.oPgFrm.Page1.oPag.oGE__NOME_1_22.visible=!this.oPgFrm.Page1.oPag.oGE__NOME_1_22.mHide()
    this.oPgFrm.Page1.oPag.oGE_SESSO_1_26.visible=!this.oPgFrm.Page1.oPag.oGE_SESSO_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oGEDATNAS_1_28.visible=!this.oPgFrm.Page1.oPag.oGEDATNAS_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    this.oPgFrm.Page1.oPag.oNOCODFIS_1_33.visible=!this.oPgFrm.Page1.oPag.oNOCODFIS_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page2.oPag.oGEPROGIN_2_1.visible=!this.oPgFrm.Page2.oPag.oGEPROGIN_2_1.mHide()
    this.oPgFrm.Page2.oPag.oMAXNORIG_2_2.visible=!this.oPgFrm.Page2.oPag.oMAXNORIG_2_2.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_5.visible=!this.oPgFrm.Page2.oPag.oStr_2_5.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_47.visible=!this.oPgFrm.Page1.oPag.oStr_1_47.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_50.visible=!this.oPgFrm.Page1.oPag.oStr_1_50.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_11.visible=!this.oPgFrm.Page2.oPag.oStr_2_11.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_12.visible=!this.oPgFrm.Page2.oPag.oStr_2_12.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("New record")
          .Calculate_ZJFMMVWDUN()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZI1
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZPIVAZI,AZCOFAZI,AZPERAZI,AZCODNAZ,AZLOCAZI,AZPROAZI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI1)
            select AZCODAZI,AZPIVAZI,AZCOFAZI,AZPERAZI,AZCODNAZ,AZLOCAZI,AZPROAZI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI1 = NVL(_Link_.AZCODAZI,space(5))
      this.w_AZPIVAZI = NVL(_Link_.AZPIVAZI,space(12))
      this.w_AZCODFIS = NVL(_Link_.AZCOFAZI,space(16))
      this.w_AZPERAZI = NVL(_Link_.AZPERAZI,space(1))
      this.w_AZCODNAZ = NVL(_Link_.AZCODNAZ,space(3))
      this.w_AZLOCAZI = NVL(_Link_.AZLOCAZI,space(30))
      this.w_AZPROAZI = NVL(_Link_.AZPROAZI,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI1 = space(5)
      endif
      this.w_AZPIVAZI = space(12)
      this.w_AZCODFIS = space(16)
      this.w_AZPERAZI = space(1)
      this.w_AZCODNAZ = space(3)
      this.w_AZLOCAZI = space(30)
      this.w_AZPROAZI = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI1
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TITOLARI_IDX,3]
    i_lTable = "TITOLARI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2], .t., this.TITOLARI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TTCODAZI,TTCOGTIT,TTNOMTIT,TTDATNAS,TTLUONAS,TTPRONAS,TT_SESSO";
                   +" from "+i_cTable+" "+i_lTable+" where TTCODAZI="+cp_ToStrODBC(this.w_CODAZI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TTCODAZI',this.w_CODAZI1)
            select TTCODAZI,TTCOGTIT,TTNOMTIT,TTDATNAS,TTLUONAS,TTPRONAS,TT_SESSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI1 = NVL(_Link_.TTCODAZI,space(5))
      this.w_COGTIT1 = NVL(_Link_.TTCOGTIT,space(25))
      this.w_NOMTIT1 = NVL(_Link_.TTNOMTIT,space(25))
      this.w_DATNAS1 = NVL(cp_ToDate(_Link_.TTDATNAS),ctod("  /  /  "))
      this.w_LOCTIT1 = NVL(_Link_.TTLUONAS,space(30))
      this.w_PROTIT1 = NVL(_Link_.TTPRONAS,space(2))
      this.w_SESSO1 = NVL(_Link_.TT_SESSO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI1 = space(5)
      endif
      this.w_COGTIT1 = space(25)
      this.w_NOMTIT1 = space(25)
      this.w_DATNAS1 = ctod("  /  /  ")
      this.w_LOCTIT1 = space(30)
      this.w_PROTIT1 = space(2)
      this.w_SESSO1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TITOLARI_IDX,2])+'\'+cp_ToStr(_Link_.TTCODAZI,1)
      cp_ShowWarn(i_cKey,this.TITOLARI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SEDILEG
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SEDIAZIE_IDX,3]
    i_lTable = "SEDIAZIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2], .t., this.SEDIAZIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SEDILEG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SEDILEG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN";
                   +" from "+i_cTable+" "+i_lTable+" where SECODAZI="+cp_ToStrODBC(this.w_SEDILEG);
                   +" and SETIPRIF="+cp_ToStrODBC(this.w_TIPSL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SETIPRIF',this.w_TIPSL;
                       ,'SECODAZI',this.w_SEDILEG)
            select SETIPRIF,SECODAZI,SELOCALI,SEPROVIN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SEDILEG = NVL(_Link_.SECODAZI,space(5))
      this.w_SECOMUNE = NVL(_Link_.SELOCALI,space(40))
      this.w_SESIGLA = NVL(_Link_.SEPROVIN,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_SEDILEG = space(5)
      endif
      this.w_SECOMUNE = space(40)
      this.w_SESIGLA = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SEDIAZIE_IDX,2])+'\'+cp_ToStr(_Link_.SETIPRIF,1)+'\'+cp_ToStr(_Link_.SECODAZI,1)
      cp_ShowWarn(i_cKey,this.SEDIAZIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SEDILEG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oGESERIAL_1_1.value==this.w_GESERIAL)
      this.oPgFrm.Page1.oPag.oGESERIAL_1_1.value=this.w_GESERIAL
    endif
    if not(this.oPgFrm.Page1.oPag.oGE__ANNO_1_2.value==this.w_GE__ANNO)
      this.oPgFrm.Page1.oPag.oGE__ANNO_1_2.value=this.w_GE__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oGEDATINV_1_3.value==this.w_GEDATINV)
      this.oPgFrm.Page1.oPag.oGEDATINV_1_3.value=this.w_GEDATINV
    endif
    if not(this.oPgFrm.Page1.oPag.oGERAGSOC_1_20.value==this.w_GERAGSOC)
      this.oPgFrm.Page1.oPag.oGERAGSOC_1_20.value=this.w_GERAGSOC
    endif
    if not(this.oPgFrm.Page1.oPag.oGECOGNOM_1_21.value==this.w_GECOGNOM)
      this.oPgFrm.Page1.oPag.oGECOGNOM_1_21.value=this.w_GECOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oGE__NOME_1_22.value==this.w_GE__NOME)
      this.oPgFrm.Page1.oPag.oGE__NOME_1_22.value=this.w_GE__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oGECODFIS_1_24.value==this.w_GECODFIS)
      this.oPgFrm.Page1.oPag.oGECODFIS_1_24.value=this.w_GECODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oGEPARIVA_1_25.value==this.w_GEPARIVA)
      this.oPgFrm.Page1.oPag.oGEPARIVA_1_25.value=this.w_GEPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oGE_SESSO_1_26.RadioValue()==this.w_GE_SESSO)
      this.oPgFrm.Page1.oPag.oGE_SESSO_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oGEDATNAS_1_28.value==this.w_GEDATNAS)
      this.oPgFrm.Page1.oPag.oGEDATNAS_1_28.value=this.w_GEDATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oGECOMUNE_1_30.value==this.w_GECOMUNE)
      this.oPgFrm.Page1.oPag.oGECOMUNE_1_30.value=this.w_GECOMUNE
    endif
    if not(this.oPgFrm.Page1.oPag.oGEPROVIN_1_32.value==this.w_GEPROVIN)
      this.oPgFrm.Page1.oPag.oGEPROVIN_1_32.value=this.w_GEPROVIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNOCODFIS_1_33.RadioValue()==this.w_NOCODFIS)
      this.oPgFrm.Page1.oPag.oNOCODFIS_1_33.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGEPROGIN_2_1.value==this.w_GEPROGIN)
      this.oPgFrm.Page2.oPag.oGEPROGIN_2_1.value=this.w_GEPROGIN
    endif
    if not(this.oPgFrm.Page2.oPag.oMAXNORIG_2_2.value==this.w_MAXNORIG)
      this.oPgFrm.Page2.oPag.oMAXNORIG_2_2.value=this.w_MAXNORIG
    endif
    if not(this.oPgFrm.Page2.oPag.oGECODSOG_2_3.value==this.w_GECODSOG)
      this.oPgFrm.Page2.oPag.oGECODSOG_2_3.value=this.w_GECODSOG
    endif
    if not(this.oPgFrm.Page1.oPag.oGENOMFIL_1_44.value==this.w_GENOMFIL)
      this.oPgFrm.Page1.oPag.oGENOMFIL_1_44.value=this.w_GENOMFIL
    endif
    if not(this.oPgFrm.Page2.oPag.oGECODINT_2_16.value==this.w_GECODINT)
      this.oPgFrm.Page2.oPag.oGECODINT_2_16.value=this.w_GECODINT
    endif
    if not(this.oPgFrm.Page2.oPag.oGECODCAF_2_17.value==this.w_GECODCAF)
      this.oPgFrm.Page2.oPag.oGECODCAF_2_17.value=this.w_GECODCAF
    endif
    if not(this.oPgFrm.Page2.oPag.oGEIMPTRA_2_18.RadioValue()==this.w_GEIMPTRA)
      this.oPgFrm.Page2.oPag.oGEIMPTRA_2_18.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oGEDATIMP_2_20.value==this.w_GEDATIMP)
      this.oPgFrm.Page2.oPag.oGEDATIMP_2_20.value=this.w_GEDATIMP
    endif
    cp_SetControlsValueExtFlds(this,'DAELGENE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_GE__ANNO)) or not(Val(.w_GE__ANNO)>2005 and Val(.w_GE__ANNO)<2100))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGE__ANNO_1_2.SetFocus()
            i_bnoObbl = !empty(.w_GE__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GERAGSOC))  and not(.w_AZPERAZI='S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGERAGSOC_1_20.SetFocus()
            i_bnoObbl = !empty(.w_GERAGSOC)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GECOGNOM))  and not(.w_AZPERAZI<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGECOGNOM_1_21.SetFocus()
            i_bnoObbl = !empty(.w_GECOGNOM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GE__NOME))  and not(.w_AZPERAZI<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGE__NOME_1_22.SetFocus()
            i_bnoObbl = !empty(.w_GE__NOME)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GECODFIS)) or not(IIF(EMPTY(.w_GECODFIS), .t., CHKCFP(.w_GECODFIS,"CF"))))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGECODFIS_1_24.SetFocus()
            i_bnoObbl = !empty(.w_GECODFIS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GEPARIVA)) or not(CHKCFP(.w_GEPARIVA,"PI", "", "", "")))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGEPARIVA_1_25.SetFocus()
            i_bnoObbl = !empty(.w_GEPARIVA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GEDATNAS))  and not(.w_AZPERAZI<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGEDATNAS_1_28.SetFocus()
            i_bnoObbl = !empty(.w_GEDATNAS)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GECOMUNE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGECOMUNE_1_30.SetFocus()
            i_bnoObbl = !empty(.w_GECOMUNE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_GEPROVIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGEPROVIN_1_32.SetFocus()
            i_bnoObbl = !empty(.w_GEPROVIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_MAXNORIG))  and not(.cFunction<>'Edit' and .cFunction<>'Load')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMAXNORIG_2_2.SetFocus()
            i_bnoObbl = !empty(.w_MAXNORIG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(IIF(EMPTY(.w_GECODSOG),.t.,CHKCFP(.w_GECODSOG,"CF")))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGECODSOG_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(len(alltrim(.w_GENOMFIL))-rat('\',.w_GENOMFIL)<26)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oGENOMFIL_1_44.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il nome del file non pu� essere pi� lungo di 25 caratteri")
          case   not(IIF(EMPTY(.w_GECODINT),.t.,CHKCFP(.w_GECODINT,"CF")))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGECODINT_2_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_GEIMPTRA)) or not((.w_GEIMPTRA<>'0' And not Empty( .w_GECODINT) ) Or (.w_GEIMPTRA='0' And Empty( .w_GECODINT) )))  and (Not Empty( .w_GECODINT))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGEIMPTRA_2_18.SetFocus()
            i_bnoObbl = !empty(.w_GEIMPTRA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((Not Empty( .w_GEDATIMP ) And Not Empty( .w_GECODINT )) Or Empty( .w_GECODINT ))  and (Not Empty( .w_GECODINT ))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGEDATIMP_2_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_GESERIAL = this.w_GESERIAL
    this.o_GENOMFIL = this.w_GENOMFIL
    this.o_GECODINT = this.w_GECODINT
    this.o_GEIMPTRA = this.w_GEIMPTRA
    return

enddefine

* --- Define pages as container
define class tgscg_agePag1 as StdContainer
  Width  = 688
  height = 325
  stdWidth  = 688
  stdheight = 325
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGESERIAL_1_1 as StdField with uid="URMQRUJAJQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_GESERIAL", cQueryName = "GESERIAL",nZero=10,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Progressivo comunicazione",;
    HelpContextID = 199251122,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=188, Top=23, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  add object oGE__ANNO_1_2 as StdField with uid="FIQDRWTIER",rtseq=2,rtrep=.f.,;
    cFormVar = "w_GE__ANNO", cQueryName = "GE__ANNO",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di imposta",;
    HelpContextID = 1370955,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=395, Top=23, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oGE__ANNO_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Val(.w_GE__ANNO)>2005 and Val(.w_GE__ANNO)<2100)
    endwith
    return bRes
  endfunc

  add object oGEDATINV_1_3 as StdField with uid="KNBEEHUHFT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_GEDATINV", cQueryName = "GEDATINV",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data invio",;
    HelpContextID = 67410756,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=543, Top=23

  add object oGERAGSOC_1_20 as StdField with uid="TEWAKWWTRY",rtseq=19,rtrep=.f.,;
    cFormVar = "w_GERAGSOC", cQueryName = "GERAGSOC",;
    bObbl = .t. , nPag = 1, value=space(70), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale",;
    HelpContextID = 86787241,;
   bGlobalFont=.t.,;
    Height=21, Width=666, Left=17, Top=105, cSayPict='repl("!",80)', cGetPict='repl("!",80)', InputMask=replicate('X',70)

  func oGERAGSOC_1_20.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
  endfunc

  add object oGECOGNOM_1_21 as StdField with uid="NDXFVVGNRC",rtseq=20,rtrep=.f.,;
    cFormVar = "w_GECOGNOM", cQueryName = "GECOGNOM",;
    bObbl = .t. , nPag = 1, value=space(26), bMultilanguage =  .f.,;
    ToolTipText = "Cognome",;
    HelpContextID = 3757235,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=17, Top=105, cSayPict='repl("!",26)', cGetPict='repl("!",26)', InputMask=replicate('X',26)

  func oGECOGNOM_1_21.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
  endfunc

  add object oGE__NOME_1_22 as StdField with uid="WNRLYOIYZE",rtseq=21,rtrep=.f.,;
    cFormVar = "w_GE__NOME", cQueryName = "GE__NOME",;
    bObbl = .t. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Nome",;
    HelpContextID = 239397717,;
   bGlobalFont=.t.,;
    Height=21, Width=216, Left=243, Top=105, cSayPict='repl("!",25)', cGetPict='repl("!",25)', InputMask=replicate('X',25)

  func oGE__NOME_1_22.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
  endfunc

  add object oGECODFIS_1_24 as StdField with uid="SSLSXJKOLN",rtseq=22,rtrep=.f.,;
    cFormVar = "w_GECODFIS", cQueryName = "GECODFIS",;
    bObbl = .t. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale",;
    HelpContextID = 133606215,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=17, Top=145, cSayPict='repl("!",20)', cGetPict='repl("!",20)', InputMask=replicate('X',16)

  func oGECODFIS_1_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (IIF(EMPTY(.w_GECODFIS), .t., CHKCFP(.w_GECODFIS,"CF")))
    endwith
    return bRes
  endfunc

  add object oGEPARIVA_1_25 as StdField with uid="IOFKXWSFXD",rtseq=23,rtrep=.f.,;
    cFormVar = "w_GEPARIVA", cQueryName = "GEPARIVA",;
    bObbl = .t. , nPag = 1, value=space(11), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA",;
    HelpContextID = 198976679,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=243, Top=145, cSayPict='repl("!",11)', cGetPict='repl("!",11)', InputMask=replicate('X',11)

  func oGEPARIVA_1_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_GEPARIVA,"PI", "", "", ""))
    endwith
    return bRes
  endfunc


  add object oGE_SESSO_1_26 as StdCombo with uid="JFNBIOTYLU",rtseq=24,rtrep=.f.,left=243,top=172,width=68,height=21;
    , ToolTipText = "Sesso";
    , HelpContextID = 85922997;
    , cFormVar="w_GE_SESSO",RowSource=""+"M - maschio,"+"F - femmina", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oGE_SESSO_1_26.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oGE_SESSO_1_26.GetRadio()
    this.Parent.oContained.w_GE_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oGE_SESSO_1_26.SetRadio()
    this.Parent.oContained.w_GE_SESSO=trim(this.Parent.oContained.w_GE_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_GE_SESSO=='M',1,;
      iif(this.Parent.oContained.w_GE_SESSO=='F',2,;
      0))
  endfunc

  func oGE_SESSO_1_26.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
  endfunc

  add object oGEDATNAS_1_28 as StdField with uid="AAGLTBBONB",rtseq=25,rtrep=.f.,;
    cFormVar = "w_GEDATNAS", cQueryName = "GEDATNAS",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita",;
    HelpContextID = 16475321,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=402, Top=172

  func oGEDATNAS_1_28.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
  endfunc

  add object oGECOMUNE_1_30 as StdField with uid="HOYDVWKJSI",rtseq=26,rtrep=.f.,;
    cFormVar = "w_GECOMUNE", cQueryName = "GECOMUNE",;
    bObbl = .t. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune o stato estero di nascita / della sede legale",;
    HelpContextID = 140946261,;
   bGlobalFont=.t.,;
    Height=21, Width=321, Left=243, Top=199, cSayPict='repl("!",40)', cGetPict='repl("!",40)', InputMask=replicate('X',40), bHasZoom = .t. 

  proc oGECOMUNE_1_30.mZoom
      with this.Parent.oContained
        ZOOMROUTINE(this.Parent.oContained,"C","",".w_GECOMUNE",".w_GEPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oGEPROVIN_1_32 as StdField with uid="QIKSCXIRCF",rtseq=27,rtrep=.f.,;
    cFormVar = "w_GEPROVIN", cQueryName = "GEPROVIN",;
    bObbl = .t. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita / della sede legale",;
    HelpContextID = 121822028,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=243, Top=226, cSayPict='repl("!",2)', cGetPict='repl("!",2)', InputMask=replicate('X',2), bHasZoom = .t. 

  proc oGEPROVIN_1_32.mZoom
      with this.Parent.oContained
        ZOOMROUTINE(this.Parent.oContained,"P","","",".w_GEPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNOCODFIS_1_33 as StdCheck with uid="WJNBRRYUNS",rtseq=28,rtrep=.f.,left=243, top=247, caption="Non riportare il codice fiscale dei soggetti",;
    ToolTipText = "Non riportare il codice fiscale dei soggetti nel file della comunicazione",;
    HelpContextID = 133603543,;
    cFormVar="w_NOCODFIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNOCODFIS_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oNOCODFIS_1_33.GetRadio()
    this.Parent.oContained.w_NOCODFIS = this.RadioValue()
    return .t.
  endfunc

  func oNOCODFIS_1_33.SetRadio()
    this.Parent.oContained.w_NOCODFIS=trim(this.Parent.oContained.w_NOCODFIS)
    this.value = ;
      iif(this.Parent.oContained.w_NOCODFIS=='S',1,;
      0)
  endfunc

  func oNOCODFIS_1_33.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Edit' and .cFunction<>'Load')
    endwith
  endfunc


  add object oBtn_1_39 as StdButton with uid="TQEMIMWBDV",left=453, top=292, width=21,height=20,;
    caption="...", nPag=1;
    , ToolTipText = "Seleziona la cartella dove si vuole salvare il file";
    , HelpContextID = 42406954;
  , bGlobalFont=.t.

    proc oBtn_1_39.Click()
      with this.Parent.oContained
        do SfogliaDir with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_41 as StdButton with uid="AYLATODQCL",left=577, top=277, width=48,height=45,;
    CpPicture="bmp\SAVE.bmp", caption="", nPag=1;
    , ToolTipText = "Genera file";
    , HelpContextID = 48337942;
    , caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      with this.Parent.oContained
        do GSCG_BGE with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_41.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_GE__ANNO) and not empty(.w_GENOMFIL) )
      endwith
    endif
  endfunc

  add object oGENOMFIL_1_44 as StdField with uid="YMHDUTCNMO",rtseq=34,rtrep=.f.,;
    cFormVar = "w_GENOMFIL", cQueryName = "GENOMFIL",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Il nome del file non pu� essere pi� lungo di 25 caratteri",;
    ToolTipText = "Nome del percorso e del file per l'inoltro telematico",;
    HelpContextID = 124123982,;
   bGlobalFont=.t.,;
    Height=21, Width=361, Left=84, Top=292, InputMask=replicate('X',254)

  func oGENOMFIL_1_44.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (len(alltrim(.w_GENOMFIL))-rat('\',.w_GENOMFIL)<26)
    endwith
    return bRes
  endfunc


  add object oBtn_1_56 as StdButton with uid="OISXWWNKDA",left=631, top=277, width=48,height=45,;
    CpPicture="bmp\SAVE.bmp", caption="", nPag=1;
    , ToolTipText = "Esporta file per APRI";
    , HelpContextID = 37466362;
    , caption='\<Exp. APRI';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_56.Click()
      with this.Parent.oContained
        GSCG_BGE(this.Parent.oContained, .T. )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_56.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_GE__ANNO) and not empty(.w_GENOMFIL))
      endwith
    endif
  endfunc

  add object oStr_1_19 as StdString with uid="IWASQOELRB",Visible=.t., Left=302, Top=23,;
    Alignment=1, Width=92, Height=18,;
    Caption="Anno di imposta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="CBXAXJHAGL",Visible=.t., Left=445, Top=23,;
    Alignment=1, Width=97, Height=18,;
    Caption="Data invio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="AUBAIABODV",Visible=.t., Left=192, Top=172,;
    Alignment=1, Width=47, Height=18,;
    Caption="Sesso:"  ;
  , bGlobalFont=.t.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="SAVQXEKMGH",Visible=.t., Left=305, Top=172,;
    Alignment=1, Width=96, Height=18,;
    Caption="Data di nascita:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="GYQQQDJFXD",Visible=.t., Left=3, Top=199,;
    Alignment=1, Width=236, Height=18,;
    Caption="Comune o stato estero della sede legale:"  ;
  , bGlobalFont=.t.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="FLIPWSKFLK",Visible=.t., Left=26, Top=226,;
    Alignment=1, Width=213, Height=18,;
    Caption="Provincia di nascita:"  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="YWKNGIHRKX",Visible=.t., Left=27, Top=23,;
    Alignment=1, Width=155, Height=18,;
    Caption="Progressivo comunicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="PMAPUHRRHC",Visible=.t., Left=17, Top=85,;
    Alignment=0, Width=328, Height=18,;
    Caption="Denominazione, ragione sociale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="ELERXBUKNP",Visible=.t., Left=17, Top=127,;
    Alignment=0, Width=94, Height=18,;
    Caption="Codice fiscale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_40 as StdString with uid="SORSEKCKVG",Visible=.t., Left=6, Top=292,;
    Alignment=1, Width=69, Height=15,;
    Caption="Nome file:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="LDPBFCNMHA",Visible=.t., Left=17, Top=85,;
    Alignment=0, Width=136, Height=18,;
    Caption="Cognome"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="WHFDLCOOIL",Visible=.t., Left=243, Top=85,;
    Alignment=0, Width=136, Height=18,;
    Caption="Nome"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_47.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
  endfunc

  add object oStr_1_48 as StdString with uid="BGYXKIEHZI",Visible=.t., Left=4, Top=65,;
    Alignment=0, Width=378, Height=18,;
    Caption="Dati identificativi del contribuente"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_49 as StdString with uid="PELFGSXGLT",Visible=.t., Left=243, Top=127,;
    Alignment=0, Width=94, Height=18,;
    Caption="Partita IVA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_50 as StdString with uid="GSTUTMZPTD",Visible=.t., Left=3, Top=199,;
    Alignment=1, Width=236, Height=18,;
    Caption="Comune o stato estero di nascita:"  ;
  , bGlobalFont=.t.

  func oStr_1_50.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI<>'S')
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="AUFNJPWLRN",Visible=.t., Left=26, Top=226,;
    Alignment=1, Width=213, Height=18,;
    Caption="Provincia della sede legale:"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (.w_AZPERAZI='S')
    endwith
  endfunc

  add object oBox_1_45 as StdBox with uid="UAWJUOVHFZ",left=2, top=62, width=623,height=0
enddefine
define class tgscg_agePag2 as StdContainer
  Width  = 688
  height = 325
  stdWidth  = 688
  stdheight = 325
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oGEPROGIN_2_1 as StdField with uid="EUAYGFJKHH",rtseq=29,rtrep=.f.,;
    cFormVar = "w_GEPROGIN", cQueryName = "GEPROGIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Progressivo dell'invio telematico",;
    HelpContextID = 105044812,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=272, Top=39, cSayPict='"9999"', cGetPict='"9999"'

  func oGEPROGIN_2_1.mHide()
    with this.Parent.oContained
      return (.w_GEPROGIN=0)
    endwith
  endfunc

  add object oMAXNORIG_2_2 as StdField with uid="YWPJFYVBEH",rtseq=30,rtrep=.f.,;
    cFormVar = "w_MAXNORIG", cQueryName = "MAXNORIG",;
    bObbl = .t. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero masssimo di righe soggetti per file per il limite di 3 MB compressi (esclusi record di testa e di coda)",;
    HelpContextID = 189161203,;
   bGlobalFont=.t.,;
    Height=21, Width=111, Left=532, Top=39, cSayPict="v_PV[30]", cGetPict="v_GV[30]"

  func oMAXNORIG_2_2.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Edit' and .cFunction<>'Load')
    endwith
  endfunc

  add object oGECODSOG_2_3 as StdField with uid="EUIUCNUXWW",rtseq=31,rtrep=.f.,;
    cFormVar = "w_GECODSOG", cQueryName = "GECODSOG",;
    bObbl = .f. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del soggetto",;
    HelpContextID = 84497581,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=272, Top=116, cSayPict='repl("!",16)', cGetPict='repl("!",16)', InputMask=replicate('X',16)

  func oGECODSOG_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (IIF(EMPTY(.w_GECODSOG),.t.,CHKCFP(.w_GECODSOG,"CF")))
    endwith
    return bRes
  endfunc

  add object oGECODINT_2_16 as StdField with uid="CNXKMRFHEG",rtseq=38,rtrep=.f.,;
    cFormVar = "w_GECODINT", cQueryName = "GECODINT",;
    bObbl = .f. , nPag = 2, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale dell'Intermediario",;
    HelpContextID = 83274566,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=272, Top=198, cSayPict='repl("!",16)', cGetPict='repl("!",16)', InputMask=replicate('X',16)

  func oGECODINT_2_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (IIF(EMPTY(.w_GECODINT),.t.,CHKCFP(.w_GECODINT,"CF")))
    endwith
    return bRes
  endfunc

  add object oGECODCAF_2_17 as StdField with uid="EVJZTMALPD",rtseq=39,rtrep=.f.,;
    cFormVar = "w_GECODCAF", cQueryName = "GECODCAF",nZero=5,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Numero iscrizione all'albo dei C.A.F.",;
    HelpContextID = 84497580,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=272, Top=221, cSayPict='"99999"', cGetPict='"99999"', InputMask=replicate('X',5)

  func oGECODCAF_2_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty( .w_GECODINT ))
    endwith
   endif
  endfunc


  add object oGEIMPTRA_2_18 as StdCombo with uid="PWNQJWDNCW",rtseq=40,rtrep=.f.,left=272,top=246,width=319,height=21;
    , ToolTipText = "Impegno a trasmettere in via telematica la comunicazione";
    , HelpContextID = 113751207;
    , cFormVar="w_GEIMPTRA",RowSource=""+"Comunicazione predisposta dal contribuente,"+"Comunicazione predisposta da chi effettua l'invio,"+"Nessuno", bObbl = .t. , nPag = 2;
  , bGlobalFont=.t.


  func oGEIMPTRA_2_18.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'0',;
    ' '))))
  endfunc
  func oGEIMPTRA_2_18.GetRadio()
    this.Parent.oContained.w_GEIMPTRA = this.RadioValue()
    return .t.
  endfunc

  func oGEIMPTRA_2_18.SetRadio()
    this.Parent.oContained.w_GEIMPTRA=trim(this.Parent.oContained.w_GEIMPTRA)
    this.value = ;
      iif(this.Parent.oContained.w_GEIMPTRA=='1',1,;
      iif(this.Parent.oContained.w_GEIMPTRA=='2',2,;
      iif(this.Parent.oContained.w_GEIMPTRA=='0',3,;
      0)))
  endfunc

  func oGEIMPTRA_2_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty( .w_GECODINT))
    endwith
   endif
  endfunc

  func oGEIMPTRA_2_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_GEIMPTRA<>'0' And not Empty( .w_GECODINT) ) Or (.w_GEIMPTRA='0' And Empty( .w_GECODINT) ))
    endwith
    return bRes
  endfunc

  add object oGEDATIMP_2_20 as StdField with uid="ZRWZBFZIHN",rtseq=41,rtrep=.f.,;
    cFormVar = "w_GEDATIMP", cQueryName = "GEDATIMP",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dell'Impegno",;
    HelpContextID = 67410762,;
   bGlobalFont=.t.,;
    Height=21, Width=86, Left=272, Top=273

  func oGEDATIMP_2_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Not Empty( .w_GECODINT ))
    endwith
   endif
  endfunc

  func oGEDATIMP_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((Not Empty( .w_GEDATIMP ) And Not Empty( .w_GECODINT )) Or Empty( .w_GECODINT ))
    endwith
    return bRes
  endfunc

  add object oStr_2_4 as StdString with uid="CEDWKEYBMQ",Visible=.t., Left=68, Top=116,;
    Alignment=1, Width=201, Height=18,;
    Caption="Codice fiscale del soggetto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="BEKJDVRFZR",Visible=.t., Left=53, Top=43,;
    Alignment=1, Width=216, Height=18,;
    Caption="Progressivo dell'invio telematico:"  ;
  , bGlobalFont=.t.

  func oStr_2_5.mHide()
    with this.Parent.oContained
      return (.w_GEPROGIN=0)
    endwith
  endfunc

  add object oStr_2_8 as StdString with uid="CSEXUGCIJD",Visible=.t., Left=6, Top=93,;
    Alignment=0, Width=425, Height=18,;
    Caption="Soggetto obbligato (da compilarsi solo se diverso dal contribuente)"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_9 as StdString with uid="GDYLTXWAVQ",Visible=.t., Left=6, Top=16,;
    Alignment=0, Width=422, Height=18,;
    Caption="Numero progressivo se invio gestito su diversi file"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_11 as StdString with uid="DDGDYKCPMK",Visible=.t., Left=314, Top=41,;
    Alignment=1, Width=213, Height=18,;
    Caption="Max n� righe soggetti per file:"  ;
  , bGlobalFont=.t.

  func oStr_2_11.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Edit' and .cFunction<>'Load')
    endwith
  endfunc

  add object oStr_2_12 as StdString with uid="XKCPEAOKRZ",Visible=.t., Left=364, Top=63,;
    Alignment=0, Width=323, Height=18,;
    Caption="(come default � impostato su un file ascii di 20 MB)"  ;
  , bGlobalFont=.t.

  func oStr_2_12.mHide()
    with this.Parent.oContained
      return (.cFunction<>'Edit' and .cFunction<>'Load')
    endwith
  endfunc

  add object oStr_2_13 as StdString with uid="JXRVTWLWCL",Visible=.t., Left=6, Top=175,;
    Alignment=0, Width=253, Height=18,;
    Caption="Impegno alla presentazione telematica"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_14 as StdString with uid="JUQCIHPUXW",Visible=.t., Left=26, Top=221,;
    Alignment=1, Width=243, Height=18,;
    Caption="Numero di iscrizione all'albo del C.A.F.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_15 as StdString with uid="EDYHSBFJMD",Visible=.t., Left=26, Top=198,;
    Alignment=1, Width=243, Height=15,;
    Caption="Codice Fiscale dell'Intermediario:"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="FGGRWLAFDY",Visible=.t., Left=26, Top=273,;
    Alignment=1, Width=243, Height=18,;
    Caption="Data dell'impegno:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="IDRWHNGYTA",Visible=.t., Left=26, Top=245,;
    Alignment=1, Width=243, Height=18,;
    Caption="Impegno alla presentazione telematica:"  ;
  , bGlobalFont=.t.

  add object oBox_2_6 as StdBox with uid="QFSRKXIJKT",left=0, top=170, width=623,height=0

  add object oBox_2_7 as StdBox with uid="VTWXBNPLCJ",left=0, top=87, width=623,height=0

  add object oBox_2_10 as StdBox with uid="GIRYBNOQBA",left=0, top=11, width=623,height=0
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_age','DAELGENE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".GESERIAL=DAELGENE.GESERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_age
proc SfogliaDir (parent)
local PathName, oField
  PathName = cp_GetDir()
  if !empty(PathName)
    parent.w_DirName = PathName
    parent.w_GENOMFIL=left(parent.w_DirName+'ELECF_'+alltrim(i_CODAZI)+'_'+parent.w_GESERIAL+space(254),254)
  endif
endproc

Proc ZoomRoutine()
parameters oParentObject,pTIPOPE,pCODCAP,pLOCALI,pCODPRO,pDESPRO
 GSAR_BXC(oParentObject,pTIPOPE,pCODCAP,pLOCALI,pCODPRO,pDESPRO)
EndProc
* --- Fine Area Manuale
