* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bap                                                        *
*              Ricerca partite per valutazione att/pass in divisa              *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF]                                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-12-11                                                      *
* Last revis.: 2009-01-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bap",oParentObject,m.pParame)
return(i_retval)

define class tgscg_bap as StdBatch
  * --- Local variables
  pParame = space(3)
  w_PADRE = .NULL.
  w_CODESE = space(4)
  w_PN_SERIAL = space(10)
  w_SER_FILTRO = space(10)
  w_MESS = space(256)
  w_APPO = 0
  w_APPO1 = 0
  w_FLPROV = space(1)
  w_PNCAOVAL = 0
  w_PNVALNAZ = space(3)
  w_DECTOT = 0
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_PTORIGSA = space(1)
  w_CPROWNUM = 0
  w_CPROWNUMP = 0
  w_PNSERIAL = space(10)
  w_PTNUMPAR = space(31)
  w_PTCODVAL = space(3)
  w_PTTIPCON = space(1)
  w_PTCAOAPE = 0
  w_PTCODCON = space(15)
  w_PTMODPAG = space(10)
  w_PTDATSCA = ctod("  /  /  ")
  w_SALDATA = space(1)
  w_PNDATREG = ctod("  /  /  ")
  w_PNFLLIBG = space(1)
  w_STALIG = ctod("  /  /  ")
  w_DATBLO = ctod("  /  /  ")
  w_PNNUMRER = 0
  w_DVPNSERI = space(10)
  w_CAOOLD = 0
  w_CHIUSO = space(1)
  w_SCACON = space(1)
  w_CONSEL1 = space(15)
  w_VALUTA = space(3)
  w_CODBUN = space(3)
  w_FSALD = space(1)
  w_FLDOCORI = space(1)
  w_DDOFIN = ctod("  /  /  ")
  w_COMPLETA = .f.
  w_LFLCABI = space(1)
  w_LDITISCA = space(1)
  w_ABICOD = space(5)
  w_LCODABI = space(5)
  w_NUMCONTIND = 0
  w_DIFCAM = 0
  w_LOOP = 0
  w_OK = .f.
  w_oMESS = .NULL.
  w_oPART = .NULL.
  w_SERVAL = space(10)
  w_VALDESCRI = space(50)
  w_VALDATREG = ctod("  /  /  ")
  w_TOTPOS = 0
  w_TOTNEG = 0
  w_TOTDIFPOS = 0
  w_TOTDIFNEG = 0
  w_NOELABORA = .f.
  w_DVPASERI = space(10)
  w_DVROWORD = 0
  w_DVROWNUM = 0
  w_DVORIGSA = space(1)
  w_PTDATSCA = ctod("  /  /  ")
  w_PTTIPCON = space(1)
  w_PTCODCON = space(15)
  w_PTMODPAG = space(10)
  w_PTCODVAL = space(3)
  w_MPTIPPAG = space(2)
  w_MASK = .NULL.
  w_ROWNUM = 0
  w_PNTIPCON = space(1)
  w_PNCODCON = space(15)
  w_PNIMPDAR = 0
  w_PNIMPAVE = 0
  w_PNFLSALD = space(1)
  w_PNFLSALI = space(1)
  w_PNFLSALF = space(1)
  * --- WorkFile variables
  ZOOMPART_idx=0
  VALDATPA_idx=0
  PAR_TITE_idx=0
  MOVICOST_idx=0
  PNT_DETT_idx=0
  PNT_MAST_idx=0
  AZIENDA_idx=0
  SALDICON_idx=0
  ESERCIZI_idx=0
  VAL_ATPA_idx=0
  TMPTPAR_TITE_idx=0
  MOD_PAGA_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dall'anagrafica GSCG_AAP. Questo batch viene utilizzato per
    *     poter ricercare le partite da inserire nello zoom.
    this.w_PADRE = This.oParentObject
    do case
      case this.pParame="ELA"
        this.w_PNCAOVAL = g_CAOVAL
        this.w_PNVALNAZ = g_PERVAL
        this.w_DECTOT = g_PERPVL
        ah_msg( "Elaborazione partite..." )
        if g_APPLICATION = "ad hoc ENTERPRISE"
          this.w_SCACON = this.oParentObject.w_APCLFOGE
          this.w_CONSEL1 = this.oParentObject.w_APCODCON
          this.w_VALUTA = this.oParentObject.w_APCODVAL
          this.w_CODBUN = this.oParentObject.w_APBUSUNI
          this.w_FSALD = "N"
          this.w_FLDOCORI = "S"
          this.w_DDOFIN = this.oParentObject.w_APDATREG
          this.w_COMPLETA = .f.
          * --- Create temporary table ZOOMPART
          i_nIdx=cp_AddTableDef('ZOOMPART') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          i_nConn=i_TableProp[this.PAR_TITE_idx,3] && recupera la connessione
          i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
          cp_CreateTempTable(i_nConn,i_cTempTable,"* "," from "+i_cTable;
                +" where 1=2";
                )
          this.ZOOMPART_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          GSTE_BVS (this,this)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Create temporary table TMPTPAR_TITE
          i_nIdx=cp_AddTableDef('TMPTPAR_TITE') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('GSCG_BAP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.TMPTPAR_TITE_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Drop temporary table ZOOMPART
          i_nIdx=cp_GetTableDefIdx('ZOOMPART')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('ZOOMPART')
          endif
          * --- Create temporary table ZOOMPART
          i_nIdx=cp_AddTableDef('ZOOMPART') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('QUERY\GSCG_BAP_1',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.ZOOMPART_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
          * --- Riempio lo zoom con il temporaneo...
          * --- Select from ZOOMPART
          i_nConn=i_TableProp[this.ZOOMPART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2],.t.,this.ZOOMPART_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select * from "+i_cTable+" ZOOMPART ";
                +" order by PTDATSCA,PTNUMPAR,PTTIPCON,PTCODCON,PTSERIAL";
                 ,"_Curs_ZOOMPART")
          else
            select * from (i_cTable);
             order by PTDATSCA,PTNUMPAR,PTTIPCON,PTCODCON,PTSERIAL;
              into cursor _Curs_ZOOMPART
          endif
          if used('_Curs_ZOOMPART')
            select _Curs_ZOOMPART
            locate for 1=1
            do while not(eof())
            * --- Converto e calcolo i seguenti campi: Ptimpvna Ptcaochs Ptimpchs Ptdifcam
            this.w_APPO = cp_ROUND(VAL2MON(_Curs_ZOOMPART.PTTOTIMP,_Curs_ZOOMPART.PTCAOAPE,this.w_PNCAOVAL,this.oParentObject.w_APDATREG,this.w_PNVALNAZ),this.w_DECTOT)
            this.w_APPO1 = cp_ROUND(VAL2MON(_Curs_ZOOMPART.PTTOTIMP,this.oParentObject.w_APCAOVAL,this.w_PNCAOVAL,this.oParentObject.w_APDATREG,this.w_PNVALNAZ),this.w_DECTOT)
            this.w_DIFCAM = this.w_APPO - this.w_APPO1
            * --- Write into ZOOMPART
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.ZOOMPART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.ZOOMPART_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"PTIMPVNA ="+cp_NullLink(cp_ToStrODBC(this.w_APPO),'ZOOMPART','PTIMPVNA');
              +",PTCAOCHS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_APCAOVAL),'ZOOMPART','PTCAOCHS');
              +",PTIMPCHS ="+cp_NullLink(cp_ToStrODBC(this.w_APPO1),'ZOOMPART','PTIMPCHS');
              +",DIFCAM ="+cp_NullLink(cp_ToStrODBC(Abs( this.w_DIFCAM )),'ZOOMPART','DIFCAM');
              +",PTDIFCAM ="+cp_NullLink(cp_ToStrODBC(this.w_DIFCAM),'ZOOMPART','PTDIFCAM');
                  +i_ccchkf ;
              +" where ";
                  +"PTSERIAL = "+cp_ToStrODBC(_Curs_ZOOMPART.PTSERIAL);
                  +" and PTROWORD = "+cp_ToStrODBC(_Curs_ZOOMPART.PTROWORD);
                  +" and CPROWNUM = "+cp_ToStrODBC(_Curs_ZOOMPART.CPROWNUM);
                     )
            else
              update (i_cTable) set;
                  PTIMPVNA = this.w_APPO;
                  ,PTCAOCHS = this.oParentObject.w_APCAOVAL;
                  ,PTIMPCHS = this.w_APPO1;
                  ,DIFCAM = Abs( this.w_DIFCAM );
                  ,PTDIFCAM = this.w_DIFCAM;
                  &i_ccchkf. ;
               where;
                  PTSERIAL = _Curs_ZOOMPART.PTSERIAL;
                  and PTROWORD = _Curs_ZOOMPART.PTROWORD;
                  and CPROWNUM = _Curs_ZOOMPART.CPROWNUM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
              select _Curs_ZOOMPART
              continue
            enddo
            use
          endif
        else
          * --- Creo il temporaneo con l'elenco delle partite non saldate
          *     Stessa elaborazione delle distinte, filtro solo sulla valuta e sul conto.
          *     (il filtro sulla data � sulla data documento non posso applicarlo qui, devo attendere
          *     la parte aperta )
          GSTE_BPA (this, cp_CharToDate("  /  /    ") , cp_CharToDate("  /  /    ") , this.oParentObject.w_APCLFOGE , this.oParentObject.w_APCODCON, this.oParentObject.w_APCODCON , this.oParentObject.w_APCODVAL , "", "RB","BO","RD","RI","MA","CA", Repl("X",10)+"A", IIF(this.oParentObject.w_APFLSOSP="S", "N","S"))
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          vq_exec("query\GSCG1BAP.VQR",this,"PAR_APE")
          * --- Drop temporary table TMP_PART_APE
          i_nIdx=cp_GetTableDefIdx('TMP_PART_APE')
          if i_nIdx<>0
            cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
            cp_RemoveTableDef('TMP_PART_APE')
          endif
          if RECCOUNT("PAR_APE")>0
            * --- Variabili richieste come caller dal batch se lanciato da distinta...
            *     (non utilizzate dall'elaborazione - obsolete)
            this.w_LDITISCA = "G" &&IIF( this.oParentObject.w_APCLFOGE="F", "F" ,"C")
            Cur = WrCursor("PAR_APE")
            GSTE_BCP(this,"D","PAR_APE")
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Verifico la presenza di eventuali scadenze da cont. indiretta
            this.w_NUMCONTIND = 0
            * --- Verifico eventuali scadenze in contenzioso
             
 Select Par_Ape 
 Count For TipDoc="NO" And FLCRSA="C" And ptroword>0 To this.w_NUMCONTIND
            if this.w_NUMCONTIND<>0
              * --- Verifico se eventuali scadenze derivanti da contabilizzazione indiretta non
              *     siano gia presenti tra le scadenze in distinta.
              *     
              *     Questo per evitare il caso di contabilizzazione indiretta seguita da un 
              *     contenzioso.
              * --- Recupero tutte le partite derivanti da insouto presenti in distinta (PTFLINDI vuoto perch� ptroword>0)
               
 Select DTOS(CP_TODATE(DATSCA))+LEFT(ALLTRIM(NUMPAR)+SPACE(14),14)+TIPCON+ ; 
 LEFT(ALLTRIM(CODCON)+SPACE(15),15)+LEFT(ALLTRIM(CODVAL)+SPACE(3), 3)+; 
 Nvl(PTSERRIF,Space(10))+Str(nvl(PTORDRIF,0),4)+Str(nvl(PTNUMRIF,0),3) As CHIAVE; 
 From Par_Ape Where TipDoc="NO" And FLCRSA="C" And ptroword>0; 
 Into Cursor NOSALD2 NoFilter Order By CHIAVE
              * --- Elimino eventuali scadenze da cont. indiretta (ptflindi pieno) riaperte che hanno stessi estermi 
              *     di partite in insoluto.
              *     Scadenze pre 2.2 riferimenti vuoti quindi elimino tutte le partite da cont. indiretta
              *     con stessi riferimenti di partite in contezioso.
              *     Scadenze post 2.2 utilizzo oltre ai riferimenti anche il fatto che entrambe le
              *     scadenze  (insoluto e contabilizzazione indiretta) puntano alla stessa partita
              *     di creazione
               
 Delete From Par_Ape Where DTOS(CP_TODATE(DATSCA))+LEFT(ALLTRIM(NUMPAR)+SPACE(14),14)+TIPCON+ ; 
 LEFT(ALLTRIM(CODCON)+SPACE(15),15)+LEFT(ALLTRIM(CODVAL)+SPACE(3), 3)+; 
 Nvl(PTSERRIF,Space(10))+Str(nvl(PTORDRIF,0),4)+Str(nvl(PTNUMRIF,0),3) ; 
 In (Select CHIAVE From NOSALD2) And Not Empty( Flindi ) 
              Use in NOSALD2
            endif
            * --- Filtro per data documento, lo faccio adesso perch� prima devo calcolare la parte residua
            *     Eseguo una go top per problema riscontrato in Oracle\Db2 nella select successiva che ripescava record eliminati nella
            *     precedente delete
            select Par_ape 
 Go top
             
 Select * From PAR_APE Where PTDATREG <= this.oParentObject.w_APDATREG AND Not Deleted() ; 
 Into Cursor Selepart NoFilter
            Cur = WrCursor("SELEPART")
          else
            * --- Non ci sono partite non saldate, se seleziono solo le saldate, non serve fare questo giro...
             
 Select Par_Ape 
 Zap 
 Select * From Par_Ape Into Cursor SelePart NoFilter
            Cur = WrCursor("SELEPART")
          endif
          * --- Svuoto ZOOMPART
          *     Una Delete va in errore non trova ZOOMPART_idx
          * --- Create temporary table ZOOMPART
          i_nIdx=cp_AddTableDef('ZOOMPART') && aggiunge la definizione nella lista delle tabelle
          i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
          vq_exec('QUERY\GSCG2AAP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
          this.ZOOMPART_idx=i_nIdx
          i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
           
 Use in PAR_APE 
 Select Selepart 
 Go Top
          * --- Riempio lo zoom con il temporaneo...
          do while Not Eof ( "Selepart" )
            * --- Converto e calcolo i seguenti campi: Ptimpvna Ptcaochs Ptimpchs Ptdifcam
            this.w_APPO = cp_ROUND(VAL2MON(Selepart.TOTIMP,Selepart.CAOAPE,this.w_PNCAOVAL,this.oParentObject.w_APDATREG,this.w_PNVALNAZ),this.w_DECTOT)
            this.w_APPO1 = cp_ROUND(VAL2MON(Selepart.TOTIMP,this.oParentObject.w_APCAOVAL,this.w_PNCAOVAL,this.oParentObject.w_APDATREG,this.w_PNVALNAZ),this.w_DECTOT)
            this.w_DIFCAM = this.w_APPO - this.w_APPO1
            * --- Insert into ZOOMPART
            i_nConn=i_TableProp[this.ZOOMPART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ZOOMPART_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ZOOMPART_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"PTDATSCA"+",PTNUMPAR"+",PTTIPCON"+",PTCODCON"+",PT_SEGNO"+",PTTOTIMP"+",PTCAOAPE"+",PTIMPVNA"+",PTCAOCHS"+",PTIMPCHS"+",DIFCAM"+",PTSERIAL"+",PTORIGSA"+",PTROWORD"+",CPROWNUM"+",ANDESCRI"+",PNCODBUN"+",PTDIFCAM"+",PTCODVAL"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(Selepart.DATSCA),'ZOOMPART','PTDATSCA');
              +","+cp_NullLink(cp_ToStrODBC(Selepart.NUMPAR),'ZOOMPART','PTNUMPAR ');
              +","+cp_NullLink(cp_ToStrODBC(Selepart.TIPCON),'ZOOMPART','PTTIPCON ');
              +","+cp_NullLink(cp_ToStrODBC(SelePart.CODCON),'ZOOMPART','PTCODCON');
              +","+cp_NullLink(cp_ToStrODBC(SelePart.SEGNO),'ZOOMPART','PT_SEGNO ');
              +","+cp_NullLink(cp_ToStrODBC(SelePart.TOTIMP),'ZOOMPART','PTTOTIMP');
              +","+cp_NullLink(cp_ToStrODBC(SelePart.CAOAPE),'ZOOMPART','PTCAOAPE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_APPO),'ZOOMPART','PTIMPVNA');
              +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_APCAOVAL),'ZOOMPART','PTCAOCHS');
              +","+cp_NullLink(cp_ToStrODBC(this.w_APPO1),'ZOOMPART','PTIMPCHS');
              +","+cp_NullLink(cp_ToStrODBC(Abs( this.w_DIFCAM )),'ZOOMPART','DIFCAM');
              +","+cp_NullLink(cp_ToStrODBC(SelePart.PTSERIAL),'ZOOMPART','PTSERIAL');
              +","+cp_NullLink(cp_ToStrODBC("  "),'ZOOMPART','PTORIGSA');
              +","+cp_NullLink(cp_ToStrODBC(SelePart.PTROWORD),'ZOOMPART','PTROWORD');
              +","+cp_NullLink(cp_ToStrODBC(SelePart.CPROWNUM),'ZOOMPART','CPROWNUM');
              +","+cp_NullLink(cp_ToStrODBC(SelePart.ANDESCRI),'ZOOMPART','ANDESCRI');
              +","+cp_NullLink(cp_ToStrODBC("   "),'ZOOMPART','PNCODBUN');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DIFCAM),'ZOOMPART','PTDIFCAM');
              +","+cp_NullLink(cp_ToStrODBC(SelePart.CODVAL),'ZOOMPART','PTCODVAL');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'PTDATSCA',Selepart.DATSCA,'PTNUMPAR ',Selepart.NUMPAR,'PTTIPCON ',Selepart.TIPCON,'PTCODCON',SelePart.CODCON,'PT_SEGNO ',SelePart.SEGNO,'PTTOTIMP',SelePart.TOTIMP,'PTCAOAPE',SelePart.CAOAPE,'PTIMPVNA',this.w_APPO,'PTCAOCHS',this.oParentObject.w_APCAOVAL,'PTIMPCHS',this.w_APPO1,'DIFCAM',Abs( this.w_DIFCAM ),'PTSERIAL',SelePart.PTSERIAL)
              insert into (i_cTable) (PTDATSCA,PTNUMPAR ,PTTIPCON ,PTCODCON,PT_SEGNO ,PTTOTIMP,PTCAOAPE,PTIMPVNA,PTCAOCHS,PTIMPCHS,DIFCAM,PTSERIAL,PTORIGSA,PTROWORD,CPROWNUM,ANDESCRI,PNCODBUN,PTDIFCAM,PTCODVAL &i_ccchkf. );
                 values (;
                   Selepart.DATSCA;
                   ,Selepart.NUMPAR;
                   ,Selepart.TIPCON;
                   ,SelePart.CODCON;
                   ,SelePart.SEGNO;
                   ,SelePart.TOTIMP;
                   ,SelePart.CAOAPE;
                   ,this.w_APPO;
                   ,this.oParentObject.w_APCAOVAL;
                   ,this.w_APPO1;
                   ,Abs( this.w_DIFCAM );
                   ,SelePart.PTSERIAL;
                   ,"  ";
                   ,SelePart.PTROWORD;
                   ,SelePart.CPROWNUM;
                   ,SelePart.ANDESCRI;
                   ,"   ";
                   ,this.w_DIFCAM;
                   ,SelePart.CODVAL;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
             
 Select Selepart 
 Skip
          enddo
           
 Use in SelePart
        endif
         
 Select ( this.oParentObject.w_ZOOMSCAD.cCursor ) 
 Go Top
        * --- Setta Proprieta' Campi del Cursore
        this.w_LOOP = 1
        do while this.w_LOOP <= this.oParentObject.w_ZOOMSCAD.grd.ColumnCount
          if NOT "XCHK" $ UPPER(this.oParentObject.w_ZOOMSCAD.grd.Columns[ this.w_LOOP ].ControlSource)
            this.oParentObject.w_ZOOMSCAD.grd.Columns[  this.w_LOOP ].Enabled = .F.
          endif
          this.w_LOOP = this.w_LOOP + 1
        enddo
        * --- Refresh della griglia
        this.w_PADRE.NotifyEvent("Ricerca")     
        * --- Valorizzo seleziona tutto..
        this.oParentObject.w_SELEZI = "D"
      case this.pParame="VID"
        if NOT EMPTY(this.oParentObject.w_SERIALE)
          this.w_PADRE.NotifyEvent("RefreshDettaglio")     
        endif
      case this.pParame="STA"
         
 L_DECIMALI=this.oParentObject.w_DECIMALI 
 L_DECTOT=g_PERPVL 
 L_APCAOVAL=this.oParentObject.w_APCAOVAL 
 L_APDATREG=this.oParentObject.w_APDATREG 
 L_APCLFOGE=this.oParentObject.w_APCLFOGE 
 L_APBUSUNI=this.oParentObject.w_APBUSUNI 
 L_APCODVAL=this.oParentObject.w_APCODVAL 
 L_APCODCON=this.oParentObject.w_APCODCON 
 L_DESCONTO=this.oParentObject.w_DESCONTO
        * --- Lancio il report
        SELECT * , IIF(PT_SEGNO = "D", -1, 1) * PTDIFCAM AS DIFFERENZA,; 
 IIF(iif(PT_SEGNO = "D", -1, 1) * PTDIFCAM > 0, this.oParentObject.w_DIFPOS, this.oParentObject.w_DIFNEG) AS CONTO ; 
 FROM ( this.oParentObject.w_ZOOMSCAD.cCursor ) INTO CURSOR __TMP__ ORDER BY PTDATSCA,PTNUMPAR,PTTIPCON,PTCODCON,PTSERIAL
        * --- Lancio il report
        CP_CHPRN("QUERY\GSCG_BAP.XLT","",this.oParentObject)
        if used("__tmp__")
          select __tmp__
          use
        endif
      case this.pParame="INT"
         
 L_DECIMALI=this.oParentObject.w_DECIMALI 
 L_DECTOT=g_PERPVL 
 L_APCAOVAL=this.oParentObject.w_APCAOVAL 
 L_APDATREG=this.oParentObject.w_APDATREG 
 L_APCLFOGE=this.oParentObject.w_APCLFOGE 
 L_APBUSUNI=this.oParentObject.w_APBUSUNI 
 L_APCODVAL=this.oParentObject.w_APCODVAL 
 L_APCODCON=this.oParentObject.w_APCODCON
        vx_exec("QUERY\GSCG1SAB.VQR, QUERY\GSCG1SAB.FRX",this)
      case this.pParame="DEL" Or this.pParame="ELP"
        this.w_SER_FILTRO = IIF( this.pParame="ELP" , this.oParentObject.w_SERIALE , Space(10) )
        * --- Istanzia messaggio
        this.w_oMESS=createobject("AH_MESSAGE")
        * --- Verifico se successivamente alla valutazione che si sta eliminando
        *     vi siano altre valutazioni per la partita, se cos� inibisco la cancellazione..
        this.w_OK = .T.
        * --- Select from GSCG9BAP
        do vq_exec with 'GSCG9BAP',this,'_Curs_GSCG9BAP','',.f.,.t.
        if used('_Curs_GSCG9BAP')
          select _Curs_GSCG9BAP
          locate for 1=1
          do while not(eof())
          * --- Leggo la descrizione e la data..
          this.w_SERVAL = padl( Alltrim( Str( Nvl( _Curs_GSCG9BAP.SERIALE ,0 ) ) ) ,10,"0")
          * --- Read from VAL_ATPA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VAL_ATPA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VAL_ATPA_idx,2],.t.,this.VAL_ATPA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "APDATREG,APDESCRI"+;
              " from "+i_cTable+" VAL_ATPA where ";
                  +"APSERIAL = "+cp_ToStrODBC(this.w_SERVAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              APDATREG,APDESCRI;
              from (i_cTable) where;
                  APSERIAL = this.w_SERVAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_VALDATREG = NVL(cp_ToDate(_read_.APDATREG),cp_NullValue(_read_.APDATREG))
            this.w_VALDESCRI = NVL(cp_ToDate(_read_.APDESCRI),cp_NullValue(_read_.APDESCRI))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_OK = .F.
          this.w_oPART = this.w_oMESS.AddMsgPart("Impossibile rimuovere la registrazione. Esistono valutazione successive.%0Seriale [%1] %2 del %3")
          this.w_oPART.AddParam(this.w_SERVAL)     
          this.w_oPART.AddParam(Alltrim(this.w_VALDESCRI))     
          this.w_oPART.AddParam(Dtoc(this.w_VALDATREG))     
            select _Curs_GSCG9BAP
            continue
          enddo
          use
        endif
        if this.w_OK
          if this.pParame="ELP"
            * --- Come primo controllo devo verificare che siano presenti almeno due registrazioni
            *     di primanota associate alla valutazione altrimenti la cancellazione deve essere fatta
            *     tramite il tasto F5.
             
 SELECT ( this.oParentObject.w_MOVIMENTI.cCursor ) 
 GO TOP
            if RECCOUNT( this.oParentObject.w_MOVIMENTI.cCursor )=1
              this.w_oMESS.AddMsgPart("Impossibile rimuovere l'ultima registrazione. Eliminare l'intera valutazione")     
              this.w_OK = .F.
            else
              if Not ah_YesNo("La cancellazione del movimento eliminer� anche%0la registrazione di primanota associata.%0Confermi la cancellazione?")
                this.w_oMESS.AddMsgPart("Cancellazione annullata")     
                this.w_OK = .F.
              endif
            endif
          else
            AH_ERRORMSG("La cancellazione della valutazione eliminer� tutte le registrazione di primanota associate",48)
          endif
          if this.w_OK
            this.w_PN_SERIAL = space(10)
            * --- Select from GSCG7BAP
            do vq_exec with 'GSCG7BAP',this,'_Curs_GSCG7BAP','',.f.,.t.
            if used('_Curs_GSCG7BAP')
              select _Curs_GSCG7BAP
              locate for 1=1
              do while not(eof())
              * --- Verifico che il movimento che si intende cancellare non sia confermato.
              this.w_FLPROV = _Curs_GSCG7BAP.Pnflprov
              this.w_PNDATREG = _Curs_GSCG7BAP.Pndatreg
              this.w_PNFLLIBG = _Curs_GSCG7BAP.Pnfllibg
              this.w_PNNUMRER = _Curs_GSCG7BAP.Pnnumrer
              this.w_DVPNSERI = _Curs_GSCG7BAP.Dvpnseri
              this.w_CODESE = _Curs_GSCG7BAP.Pncompet
              this.w_CAOOLD = Nvl(_Curs_GSCG7BAP.PTCAOOLD,0)
              if this.w_FLPROV<>"S" 
                this.w_STALIG = cp_CharToDate("  -  -  ")
                this.w_DATBLO = cp_CharToDate("  -  -  ")
                * --- Leggo la data di ultima stampa libro giornale e la data di blocco.
                * --- Read from AZIENDA
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.AZIENDA_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "AZSTALIG,AZDATBLO"+;
                    " from "+i_cTable+" AZIENDA where ";
                        +"AZCODAZI = "+cp_ToStrODBC(i_codazi);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    AZSTALIG,AZDATBLO;
                    from (i_cTable) where;
                        AZCODAZI = i_codazi;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
                  this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                * --- Verifico se la data di registrazione del movimento che si intende cancellare �
                *     maggiore della data di consolidamento
                if g_APPLICATION = "ad hoc ENTERPRISE"
                  * --- Leggo sulla tabella ESERCIZI il flag esercizio chiuso
                  * --- Read from ESERCIZI
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.ESERCIZI_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "ESCHIUSO"+;
                      " from "+i_cTable+" ESERCIZI where ";
                          +"ESCODAZI = "+cp_ToStrODBC(i_codazi);
                          +" and ESCODESE = "+cp_ToStrODBC(this.w_CODESE);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      ESCHIUSO;
                      from (i_cTable) where;
                          ESCODAZI = i_codazi;
                          and ESCODESE = this.w_CODESE;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_CHIUSO = NVL(cp_ToDate(_read_.ESCHIUSO),cp_NullValue(_read_.ESCHIUSO))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                  if this.w_PNDATREG<=g_CONCON
                    this.w_oPART = this.w_oMESS.AddMsgPartNL("Impossibile cancellare reg:. %1 del %2")
                    this.w_oPART.AddParam(ALLTRIM(STR(this.w_PNNUMRER)))     
                    this.w_oPART.AddParam(ALLTRIM(DTOC(this.w_PNDATREG)))     
                    this.w_oMESS.AddMsgPart("Data registrazione inferiore o uguale alla data di consolidamento primanota")     
                    this.w_OK = .F.
                  endif
                endif
                if this.w_OK AND this.w_PNDATREG<=this.w_STALIG
                  this.w_oPART = this.w_oMESS.AddMsgPartNL("Impossibile cancellare reg:. %1 del %2")
                  this.w_oPART.AddParam(ALLTRIM(STR(this.w_PNNUMRER)))     
                  this.w_oPART.AddParam(ALLTRIM(DTOC(this.w_PNDATREG)))     
                  this.w_oMESS.AddMsgPart("Data registrazione inferiore o uguale a ultima stampa libro giornale")     
                  this.w_OK = .F.
                endif
                if this.w_OK AND this.w_CHIUSO="S"
                  this.w_oPART = this.w_oMESS.AddMsgPartNL("Impossibile cancellare reg:. %1 del %2")
                  this.w_oPART.AddParam(ALLTRIM(STR(this.w_PNNUMRER)))     
                  this.w_oPART.AddParam(ALLTRIM(DTOC(this.w_PNDATREG)))     
                  this.w_oMESS.AddMsgPart("L'esercizio di competenza risulta chiuso.")     
                  this.w_OK = .F.
                endif
                if this.w_OK AND this.w_PNDATREG <= this.w_DATBLO AND NOT(EMPTY(NVL(this.w_DATBLO,cp_CharToDate("  -  -  "))))
                  this.w_oPART = this.w_oMESS.AddMsgPartNL("Impossibile cancellare reg:. %1 del %2")
                  this.w_oPART.AddParam(ALLTRIM(STR(this.w_PNNUMRER)))     
                  this.w_oPART.AddParam(ALLTRIM(DTOC(this.w_PNDATREG)))     
                  this.w_oMESS.AddMsgPart("Stampa libro giornale, contabilizzazione in corso con selezione comprendente la registrazione")     
                  this.w_OK = .F.
                endif
                if this.w_OK AND this.w_PNFLLIBG="S"
                  this.w_oPART = this.w_oMESS.AddMsgPartNL("Impossibile cancellare reg:. %1 del %2")
                  this.w_oPART.AddParam(ALLTRIM(STR(this.w_PNNUMRER)))     
                  this.w_oPART.AddParam(ALLTRIM(DTOC(this.w_PNDATREG)))     
                  this.w_oMESS.AddMsgPart("Registrazione gi� stampata sul libro giornale")     
                  this.w_OK = .F.
                endif
              endif
              if this.w_OK
                this.w_PTNUMPAR = iif(g_APPLICATION = "ad hoc ENTERPRISE",LEFT(_Curs_GSCG7BAP.Ptnumpar,14),_Curs_GSCG7BAP.Ptnumpar)
                this.w_PTCODVAL = _Curs_GSCG7BAP.Ptcodval
                this.w_PTTIPCON = _Curs_GSCG7BAP.Pttipcon
                this.w_PTCODCON = _Curs_GSCG7BAP.Ptcodcon
                this.w_PTMODPAG = _Curs_GSCG7BAP.Ptmodpag
                this.w_PTDATSCA = _Curs_GSCG7BAP.Ptdatsca
                this.w_PTSERIAL = _Curs_GSCG7BAP.Dvpaseri
                this.w_PTROWORD = _Curs_GSCG7BAP.Dvroword
                this.w_PTORIGSA = _Curs_GSCG7BAP.Dvorigsa
                this.w_CPROWNUM = _Curs_GSCG7BAP.Dvrownum
                this.w_PTCAOAPE = _Curs_GSCG7BAP.Ptcaoold
                this.w_SALDATA = ""
                * --- Prima di effettuare la cancellazione bisogna verificare che le partite associate
                *     non siano gi� state saldate.
                if g_APPLICATION = "ad hoc ENTERPRISE"
                  if empty(nvl(this.w_PTSERIAL,space(10)))
                    this.w_SALDATA = " "
                  else
                    VQ_EXEC("QUERY\GSCG9BAP",this,"Partite")
                    Select Partite
                    this.w_SALDATA = iif(Nvl(Partite.Saldata,0)>1, "S", " ")
                    if used("Partite")
                      Select Partite
                      use
                    endif
                  endif
                else
                  this.w_SALDATA = IIF( GSTE_BSP( This , this.w_PTSERIAL , this.w_PTROWORD, this.w_CPROWNUM ) , "S" ,"" )
                endif
                * --- Se la variabile w_SALDATA non vale 'S' vuol dire che la partita pu� essere
                *     cancellata.
                if Empty( this.w_SALDATA )
                  * --- Try
                  local bErr_03A47690
                  bErr_03A47690=bTrsErr
                  this.Try_03A47690()
                  * --- Catch
                  if !empty(i_Error)
                    i_ErrMsg=i_Error
                    i_Error=''
                    this.w_oMESS.AddMsgPart("Impossibile cancellare registrazione di primanota")     
                    this.w_OK = .F.
                  endif
                  bTrsErr=bTrsErr or bErr_03A47690
                  * --- End
                else
                  this.w_OK = .F.
                  this.w_oPART = this.w_oMESS.AddMsgPartNL("Impossibile cancellare il movimento perch� la partita associata%0numero %1 data scad. %2 codice conto %3 mod. pag. %4 valuta %5 risulta saldata o abbinata")
                  this.w_oPART.AddParam(alltrim(this.w_Ptnumpar))     
                  this.w_oPART.AddParam(dtoc(this.w_Ptdatsca))     
                  this.w_oPART.AddParam(alltrim(this.w_Ptcodcon))     
                  this.w_oPART.AddParam(alltrim(this.w_Ptmodpag))     
                  this.w_oPART.AddParam(alltrim(this.w_Ptcodval))     
                endif
              else
                Exit
              endif
                select _Curs_GSCG7BAP
                continue
              enddo
              use
            endif
          endif
        endif
        this.oParentObject.NotifyEvent("RefreshZoom")
        if Not this.w_OK
          * --- Se cancello la singola registrazione non posso inviare un Transaction Error,
          *     mostro il msg..
          if this.pParame="DEL"
            this.w_MESS = this.w_oMESS.ComposeMessage()
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
          else
            this.w_oMESS.AH_ERRORMSG(48)     
          endif
        endif
      case this.pParame="FINEDEL"
        ah_ErrorMsg("Aggiornamento completato come richiesto","!","")
        * --- Aggiorno lo zoom...
        this.w_PADRE.NotifyEvent("After_Del_PN")     
        this.w_PADRE.w_Movimenti.setfocus()
      case this.pParame="Blank"
        * --- Creo il temporaneo, se non esiste..
        * --- Create temporary table ZOOMPART
        i_nIdx=cp_AddTableDef('ZOOMPART') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        vq_exec('QUERY\GSCG2AAP',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
        this.ZOOMPART_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        * --- Imposto per lo zoom la query su ZOOMPART...
        *     (NOn esiste un punto per creare il termporaneo prima della determinazioen della struttura dello zoom)
        this.oParentObject.w_ZOOMSCAD.cCpQueryName = "QUERY\GSCG3AAP"
        this.oParentObject.w_MOVIMENTI.Visible = Not this.w_PADRE.cFunction="Load"
        this.oParentObject.w_DETTAGLIO.Visible = Not this.w_PADRE.cFunction="Load"
        this.oParentObject.w_ZOOMSCAD.Visible = this.w_PADRE.cFunction="Load"
        if this.w_PADRE.cFunction="Load"
          this.w_PADRE.NotifyEvent("Ricerca")     
          * --- Non eseguo la mCalc per non avere il doppio msg al cambio della valuta
          *     sul cambio, questo perch� niente dipende dallo zoom altrimenti dovrei 
          *     eseguire la mCalc per queste dipedenze
          this.bUpdateParentObject = .F.
        endif
      case this.pParame="Done"
        * --- Rimuovo il termpoaneo ZOOMPART...
        * --- Drop temporary table ZOOMPART
        i_nIdx=cp_GetTableDefIdx('ZOOMPART')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('ZOOMPART')
        endif
      case this.pParame="CHECKFORM"
        * --- Controllo i campi contenenti le differenze di conversione.
        *     Verifico che entrambi i campi non siano obsoleti
        if NOT EMPTY(this.oParentObject.w_DATOBSO1) AND this.oParentObject.w_DATOBSO1<=this.oParentObject.w_OBTEST
          this.oParentObject.w_ERR_MSG = ah_MsgFormat("Il conto utilizzato per rilevare una differenza cambi passiva � obsoleto")
          i_retcode = 'stop'
          i_retval = .F.
          return
        endif
        if NOT EMPTY(this.oParentObject.w_DATOBSO2) AND this.oParentObject.w_DATOBSO2<=this.oParentObject.w_OBTEST
          this.oParentObject.w_ERR_MSG = ah_MsgFormat("Il conto utilizzato per rilevare una differenza cambi attiva � obsoleto")
          i_retcode = 'stop'
          i_retval = .F.
          return
        endif
        * --- Verifico i dati selezionati, la verifica muta la mutare del check raggruppa
        *     scritture...
        if this.oParentObject.w_APRAGSCR="S"
          * --- Raggruppo il cursore per Business Unit, Tipo Conto e Codice Conto e sommo 
          *     gli importi contenenti la differenza cambi.
          *     Il campo Tipo mi serve per sapere se la riga che st� analizzando � una riga di
          *     differenze di conversione oppure il codice conto valutato.
           
 Select Pttipcon,Ptcodcon,Pncodbun,Pt_Segno,Sum(Ptdifcam) as Ptdifcam,(Ptdifcam-Ptdifcam) as Pnimpdar,; 
 (Ptdifcam-Ptdifcam) as Pnimpave,"C" as Tipo; 
 from ( this.oParentObject.w_ZOOMSCAD.cCursor ) into cursor Diffconv order by Pncodbun,Pttipcon,Ptcodcon; 
 group by Pncodbun,Pttipcon,Ptcodcon,Pt_Segno where xchk=1
          =WRCURSOR("Diffconv")
          if reccount("Diffconv")=0
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.oParentObject.w_ERR_MSG = ah_MsgFormat("Attenzione non � stata selezionata nemmeno una scadenza")
            i_retcode = 'stop'
            i_retval = .F.
            return
          endif
          * --- Raggruppo il cursore per Business Unit per tipo conto,codice conto e segno
          *     e calcolo la differenza cambi.
          *     Questo cursore mi serve per poter calcolare la differenza cambi per Business Unit.
           
 Select Pttipcon,Ptcodcon,Pncodbun,Pt_Segno,Ptdifcam; 
 from ( this.oParentObject.w_ZOOMSCAD.cCursor ) into cursor Totali ; 
 order by Pncodbun,Pttipcon,Ptcodcon where xchk=1
          * --- Ciclo il cursore totali per aggiungere le righe di differenza cambi distinte per 
          *     business unit.
          this.w_TOTPOS = 0
          this.w_TOTNEG = 0
          Select Totali
          Go Top
          Scan
          this.w_TOTDIFPOS = IIF(iif(PT_SEGNO = "D", -1, 1) * PTDIFCAM > 0, ABS(PTDIFCAM), 0)
          this.w_TOTDIFNEG = IIF(iif(PT_SEGNO = "D", -1, 1) * PTDIFCAM < 0, ABS(PTDIFCAM),0) 
          this.w_TOTPOS = this.w_TOTPOS+this.w_TOTDIFPOS
          this.w_TOTNEG = this.w_TOTNEG+this.w_TOTDIFNEG
          SELECT DIFFCONV 
 APPEND BLANK
          REPLACE PTTIPCON WITH "G" 
 REPLACE PTCODCON WITH IIF(this.w_TOTDIFPOS<>0,this.oParentObject.w_DIFPOS,this.oParentObject.w_DIFNEG) 
 REPLACE PNCODBUN WITH IIF(EMPTY(nvl(TOTALI.PNCODBUN,SPACE(3))), g_CODBUN, TOTALI.PNCODBUN) 
 REPLACE PNIMPDAR WITH IIF(this.w_TOTDIFNEG<>0,this.w_TOTDIFNEG,0) 
 REPLACE PNIMPAVE WITH IIF(this.w_TOTDIFPOS<>0,this.w_TOTDIFPOS,0) 
 REPLACE PTDIFCAM WITH IIF(this.w_TOTDIFNEG<>0,this.w_TOTDIFNEG,this.w_TOTDIFPOS) 
 REPLACE TIPO WITH "D"
          SELECT TOTALI
          ENDSCAN
          * --- Controllo che sia stata selezionata almeno una partita con differenza cambio
          if this.w_TOTPOS=0 and this.w_TOTNEG=0
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            ah_errormsg ("Attenzione tutte le partite selezionate hanno differenza cambi nulla",48)
            i_retcode = 'stop'
            i_retval = .F.
            return
          endif
          * --- Raggruppo il cursore DIFFCONV per non avere duplicate le righe di differenze di conversione.
           
 Select Pttipcon,Ptcodcon,Pncodbun,Pt_Segno,Sum(Ptdifcam) as Ptdifcam,Sum(Pnimpdar) as Pnimpdar,; 
 Sum(Pnimpave) as Pnimpave,Tipo as Tipo; 
 from Diffconv into cursor CursElabora order by Pncodbun,Tipo,Pttipcon,Ptcodcon ; 
 group by Pncodbun,Tipo,Pttipcon,Ptcodcon,Pt_Segno 
          * --- Cursore che conterr� il dettaglio delle partite che devono essere aggiornate.
          if g_APPLICATION = "ad hoc ENTERPRISE"
            Select Ptserial,Ptroword,Ptorigsa,Cprownum,Ptcaoape,Ptdifcam,PttotImp,NumSca,Pt_segno from ( this.oParentObject.w_ZOOMSCAD.cCursor ) into cursor Partite order by Ptserial where xchk=1
          else
            Select Ptserial,Ptroword,Ptorigsa,Cprownum,Ptcaoape,Ptdifcam,PttotImp from ( this.oParentObject.w_ZOOMSCAD.cCursor ) into cursor Partite order by Ptserial where xchk=1
          endif
        else
          Select * from ( this.oParentObject.w_ZOOMSCAD.cCursor ) into cursor CursElabora order by Pncodbun,Pttipcon,Ptcodcon where xchk=1
          * --- Controllo se l'utente ha selezionato almeno un record
          if reccount("CursElabora")=0
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.oParentObject.w_ERR_MSG = ah_MsgFormat("Attenzione non � stata selezionata nemmeno una scadenza")
            i_retcode = 'stop'
            i_retval = .F.
            return
          endif
          Select CursElabora
          COUNT FOR PTDIFCAM<>0 TO L_TOTALE
          if L_TOTALE=0
            this.Pag3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.oParentObject.w_ERR_MSG = ah_MsgFormat("Attenzione tutte le partite selezionate hanno differenza cambi nulla")
            i_retcode = 'stop'
            i_retval = .F.
            return
          endif
        endif
        * --- Chiudo tutti i cursori di servizio, tranne CursElabora che � utilizzato da GSCG_BAM..
        this.w_NOELABORA = .T.
        this.Pag3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        i_retcode = 'stop'
        i_retval = .T.
        return
      case this.pParame="SELEZ"
        * --- Seleziona/Deseleziona le Partite/Scadenze in Distinta (Lanciato da GSCG_AAP)
        UPDATE ( this.oParentObject.w_ZOOMSCAD.cCursor ) SET XCHK = iif( this.oParentObject.w_SELEZI="S" ,1, 0 ) 
      case this.pParame="ORIGINE"
        * --- Dato il seriale della valutazione e delal reg. di prima nota selezionata
        *     determino la partita ad esso selezionata...
        if g_APPLICATION = "ADHOC REVOLUTION"
          * --- Read from VALDATPA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALDATPA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALDATPA_idx,2],.t.,this.VALDATPA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "DVPASERI,DVROWNUM,DVROWORD,DVORIGSA"+;
              " from "+i_cTable+" VALDATPA where ";
                  +"DVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_APSERIAL);
                  +" and DVPNSERI = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              DVPASERI,DVROWNUM,DVROWORD,DVORIGSA;
              from (i_cTable) where;
                  DVSERIAL = this.oParentObject.w_APSERIAL;
                  and DVPNSERI = this.oParentObject.w_SERIALE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DVPASERI = NVL(cp_ToDate(_read_.DVPASERI),cp_NullValue(_read_.DVPASERI))
            this.w_DVROWNUM = NVL(cp_ToDate(_read_.DVROWNUM),cp_NullValue(_read_.DVROWNUM))
            this.w_DVROWORD = NVL(cp_ToDate(_read_.DVROWORD),cp_NullValue(_read_.DVROWORD))
            this.w_DVORIGSA = NVL(cp_ToDate(_read_.DVORIGSA),cp_NullValue(_read_.DVORIGSA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          GSTE_BZP(this,this.w_DVPASERI, this.w_DVROWORD, this.w_DVROWNUM,0, 0,0)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          if this.oParentObject.cFunction="Load"
            this.w_PTDATSCA = this.oParentObject.w_PTDATSCA
            this.w_PTTIPCON = this.oParentObject.w_PTTIPCON
            this.w_PTCODCON = this.oParentObject.w_PTCODCON
            this.w_PTNUMPAR = iif(g_APPLICATION = "ad hoc ENTERPRISE",LEFT(this.oParentObject.w_PTNUMPAR,14),this.oParentObject.w_PTNUMPAR)
            this.w_PTMODPAG = this.oParentObject.w_PTMODPAG
            this.w_PTCODVAL = this.oParentObject.w_PTCODVAL
          else
            * --- Select from VALDATPA
            i_nConn=i_TableProp[this.VALDATPA_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VALDATPA_idx,2],.t.,this.VALDATPA_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select DVPASERI,DVORIGSA,DVROWORD,DVROWNUM  from "+i_cTable+" VALDATPA ";
                  +" where DVSERIAL="+cp_ToStrODBC(this.oParentObject.w_APSERIAL)+" AND DVPNSERI="+cp_ToStrODBC(this.oParentObject.w_SERIALE)+"";
                   ,"_Curs_VALDATPA")
            else
              select DVPASERI,DVORIGSA,DVROWORD,DVROWNUM from (i_cTable);
               where DVSERIAL=this.oParentObject.w_APSERIAL AND DVPNSERI=this.oParentObject.w_SERIALE;
                into cursor _Curs_VALDATPA
            endif
            if used('_Curs_VALDATPA')
              select _Curs_VALDATPA
              locate for 1=1
              do while not(eof())
              this.w_DVPASERI = _Curs_VALDATPA.DVPASERI
              this.w_DVROWORD = _Curs_VALDATPA.DVROWORD
              this.w_DVROWNUM = _Curs_VALDATPA.DVROWNUM
              this.w_DVORIGSA = _Curs_VALDATPA.DVORIGSA
              * --- Read from PAR_TITE
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.PAR_TITE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2],.t.,this.PAR_TITE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "PTTIPCON,PTCODCON,PTDATSCA,PTNUMPAR,PTCODVAL,PTMODPAG"+;
                  " from "+i_cTable+" PAR_TITE where ";
                      +"PTSERIAL = "+cp_ToStrODBC(this.w_DVPASERI);
                      +" and PTORIGSA = "+cp_ToStrODBC(this.w_DVORIGSA);
                      +" and PTROWORD = "+cp_ToStrODBC(this.w_DVROWORD);
                      +" and CPROWNUM = "+cp_ToStrODBC(this.w_DVROWNUM);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  PTTIPCON,PTCODCON,PTDATSCA,PTNUMPAR,PTCODVAL,PTMODPAG;
                  from (i_cTable) where;
                      PTSERIAL = this.w_DVPASERI;
                      and PTORIGSA = this.w_DVORIGSA;
                      and PTROWORD = this.w_DVROWORD;
                      and CPROWNUM = this.w_DVROWNUM;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_PTTIPCON = NVL(cp_ToDate(_read_.PTTIPCON),cp_NullValue(_read_.PTTIPCON))
                this.w_PTCODCON = NVL(cp_ToDate(_read_.PTCODCON),cp_NullValue(_read_.PTCODCON))
                this.w_PTDATSCA = NVL(cp_ToDate(_read_.PTDATSCA),cp_NullValue(_read_.PTDATSCA))
                this.w_PTNUMPAR = NVL(cp_ToDate(_read_.PTNUMPAR),cp_NullValue(_read_.PTNUMPAR))
                this.w_PTCODVAL = NVL(cp_ToDate(_read_.PTCODVAL),cp_NullValue(_read_.PTCODVAL))
                this.w_PTMODPAG = NVL(cp_ToDate(_read_.PTMODPAG),cp_NullValue(_read_.PTMODPAG))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
              if i_Rows>0
                EXIT
              endif
                select _Curs_VALDATPA
                continue
              enddo
              use
            endif
          endif
          * --- Read from MOD_PAGA
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.MOD_PAGA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MOD_PAGA_idx,2],.t.,this.MOD_PAGA_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MPTIPPAG"+;
              " from "+i_cTable+" MOD_PAGA where ";
                  +"MPCODICE = "+cp_ToStrODBC(this.w_PTMODPAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MPTIPPAG;
              from (i_cTable) where;
                  MPCODICE = this.w_PTMODPAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MPTIPPAG = NVL(cp_ToDate(_read_.MPTIPPAG),cp_NullValue(_read_.MPTIPPAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if not empty(this.w_PTDATSCA)
            this.w_MASK = GSTE_KMS()
            * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
            if !(this.w_MASK.bSec1)
              i_retcode = 'stop'
              return
            endif
            * --- inizializzo la chiave della Prima Nota
            this.w_MASK.w_SCAINI = cp_TODATE(this.w_PTDATSCA)
            this.w_MASK.w_SCAFIN = cp_TODATE(this.w_PTDATSCA)
            this.w_MASK.w_FSALD = "T"
            this.w_MASK.w_SCACLI = iif(this.w_PTTIPCON="C","C","X")
            this.w_MASK.w_CLISEL1 = iif(this.w_PTTIPCON="C",this.w_PTCODCON,"")
            this.w_MASK.w_SCAFOR = iif(this.w_PTTIPCON="F","F","X")
            this.w_MASK.w_FORSEL1 = iif(this.w_PTTIPCON="F",this.w_PTCODCON,"")
            this.w_MASK.w_SCACON = iif(this.w_PTTIPCON="G","G","X")
            this.w_MASK.w_CONSEL1 = iif(this.w_PTTIPCON="G",this.w_PTCODCON,"")
            this.w_MASK.w_VALUTA = this.w_PTCODVAL
            this.w_MASK.w_PARINI = this.w_PTNUMPAR
            this.w_MASK.w_PAGRD = iif(this.w_MPTIPPAG="RD","RD","XX")
            this.w_MASK.w_PAGRI = iif(this.w_MPTIPPAG="RI","RI","XX")
            this.w_MASK.w_PAGRA = iif(this.w_MPTIPPAG="RA","RA","XX")
            this.w_MASK.w_PAGMA = iif(this.w_MPTIPPAG="MA","MA","XX")
            this.w_MASK.w_PAGBO = iif(this.w_MPTIPPAG="BO","BO","XX")
            this.w_MASK.w_PAGRB = iif(this.w_MPTIPPAG="RB","RB","XX")
            this.w_MASK.w_PAGCA = iif(this.w_MPTIPPAG="CA","CA","XX")
            this.w_Mask.SaveDependsOn() 
 this.w_Mask.SetControlsValue() 
 this.w_Mask.mHideControls() 
 this.w_Mask.NotifyEvent("Interroga")
          else
            ah_ErrorMsg("Impossibile recuperare la scadenza valutata o da valutare")
          endif
        endif
    endcase
  endproc
  proc Try_03A47690()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Devo lanciare l'eliminazione del movimento di primanota,solo quando cambia il
    *     seriale del movimento. Effettuo questa operazione perch� ad un unico movimento
    *     possono essere associate pi� partite.
    if this.w_DVPNSERI<>this.w_PN_SERIAL
      this.w_PN_SERIAL = this.w_DVPNSERI
      * --- L'aggiornamento dei saldi deve essere effettuato solo se il movimento � provvisorio
      if this.w_FLPROV<>"S" 
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        * --- Delete from MOVICOST
        i_nConn=i_TableProp[this.MOVICOST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"MRSERIAL = "+cp_ToStrODBC(this.w_DVPNSERI);
                +" and MRROWORD > "+cp_ToStrODBC(0);
                 )
        else
          delete from (i_cTable) where;
                MRSERIAL = this.w_DVPNSERI;
                and MRROWORD > 0;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from PNT_DETT
        i_nConn=i_TableProp[this.PNT_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"PNSERIAL = "+cp_ToStrODBC(this.w_DVPNSERI);
                 )
        else
          delete from (i_cTable) where;
                PNSERIAL = this.w_DVPNSERI;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from PNT_MAST
        i_nConn=i_TableProp[this.PNT_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"PNSERIAL = "+cp_ToStrODBC(this.w_DVPNSERI);
                 )
        else
          delete from (i_cTable) where;
                PNSERIAL = this.w_DVPNSERI;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
    endif
    * --- Delete from VALDATPA
    i_nConn=i_TableProp[this.VALDATPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALDATPA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"DVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_APSERIAL);
            +" and DVPNSERI = "+cp_ToStrODBC(this.w_DVPNSERI);
             )
    else
      delete from (i_cTable) where;
            DVSERIAL = this.oParentObject.w_APSERIAL;
            and DVPNSERI = this.w_DVPNSERI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    if this.w_CAOOLD<>0
      if g_APPLICATION = "ad hoc ENTERPRISE"
        * --- Write into PAR_TITE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTCAOAPE ="+cp_NullLink(cp_ToStrODBC(this.w_CAOOLD),'PAR_TITE','PTCAOAPE');
          +",PTCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_CAOOLD),'PAR_TITE','PTCAOVAL');
              +i_ccchkf ;
          +" where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
              +" and PTORIGSA = "+cp_ToStrODBC(this.w_PTORIGSA);
              +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
              +" and PTCAOVAL = "+cp_ToStrODBC(this.oParentObject.w_APCAOVAL);
                 )
        else
          update (i_cTable) set;
              PTCAOAPE = this.w_CAOOLD;
              ,PTCAOVAL = this.w_CAOOLD;
              &i_ccchkf. ;
           where;
              PTSERIAL = this.w_PTSERIAL;
              and PTORIGSA = this.w_PTORIGSA;
              and PTROWORD = this.w_PTROWORD;
              and CPROWNUM = this.w_CPROWNUM;
              and PTCAOVAL = this.oParentObject.w_APCAOVAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        * --- Write into PAR_TITE
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.PAR_TITE_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"PTCAOAPE ="+cp_NullLink(cp_ToStrODBC(this.w_CAOOLD),'PAR_TITE','PTCAOAPE');
          +",PTCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_CAOOLD),'PAR_TITE','PTCAOVAL');
              +i_ccchkf ;
          +" where ";
              +"PTSERIAL = "+cp_ToStrODBC(this.w_PTSERIAL);
              +" and PTROWORD = "+cp_ToStrODBC(this.w_PTROWORD);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
                 )
        else
          update (i_cTable) set;
              PTCAOAPE = this.w_CAOOLD;
              ,PTCAOVAL = this.w_CAOOLD;
              &i_ccchkf. ;
           where;
              PTSERIAL = this.w_PTSERIAL;
              and PTROWORD = this.w_PTROWORD;
              and CPROWNUM = this.w_CPROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Select from PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2],.t.,this.PNT_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" PNT_DETT ";
          +" where PNSERIAL="+cp_ToStrODBC(this.w_DVPNSERI)+"";
           ,"_Curs_PNT_DETT")
    else
      select * from (i_cTable);
       where PNSERIAL=this.w_DVPNSERI;
        into cursor _Curs_PNT_DETT
    endif
    if used('_Curs_PNT_DETT')
      select _Curs_PNT_DETT
      locate for 1=1
      do while not(eof())
      this.w_ROWNUM = CPROWNUM
      if NVL(this.w_ROWNUM, 0)<>0
        this.w_PNTIPCON = _Curs_PNT_DETT.PNTIPCON
        this.w_PNCODCON = _Curs_PNT_DETT.PNCODCON
        this.w_PNIMPDAR = NVL(_Curs_PNT_DETT.PNIMPDAR, 0)
        this.w_PNIMPAVE = NVL(_Curs_PNT_DETT.PNIMPAVE, 0)
        * --- Aggiorno i saldi legati alla registrazione di Prima Nota
        this.w_PNFLSALD = NVL(_Curs_PNT_DETT.PNFLSALD,"=")
        this.w_PNFLSALI = NVL(_Curs_PNT_DETT.PNFLSALI,"=")
        this.w_PNFLSALF = NVL(_Curs_PNT_DETT.PNFLSALF,"=")
        * --- Inverte i Flag per Storno Saldi
        this.w_PNFLSALD = IIF(this.w_PNFLSALD="-", "+", IIF(this.w_PNFLSALD="+", "-", " "))
        this.w_PNFLSALI = IIF(this.w_PNFLSALI="-", "+", IIF(this.w_PNFLSALI="+", "-", " "))
        this.w_PNFLSALF = IIF(this.w_PNFLSALF="-", "+", IIF(this.w_PNFLSALF="+", "-", " "))
        * --- Aggiorna Saldi
        * --- Write into SALDICON
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDICON_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_PNFLSALD,'SLDARPER','this.w_PNIMPDAR',this.w_PNIMPDAR,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_PNFLSALD,'SLAVEPER','this.w_PNIMPAVE',this.w_PNIMPAVE,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_PNFLSALI,'SLDARINI','this.w_PNIMPDAR',this.w_PNIMPDAR,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_PNFLSALI,'SLAVEINI','this.w_PNIMPAVE',this.w_PNIMPAVE,'update',i_nConn)
          i_cOp5=cp_SetTrsOp(this.w_PNFLSALF,'SLDARFIN','this.w_PNIMPDAR',this.w_PNIMPDAR,'update',i_nConn)
          i_cOp6=cp_SetTrsOp(this.w_PNFLSALF,'SLAVEFIN','this.w_PNIMPAVE',this.w_PNIMPAVE,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLDARPER ="+cp_NullLink(i_cOp1,'SALDICON','SLDARPER');
          +",SLAVEPER ="+cp_NullLink(i_cOp2,'SALDICON','SLAVEPER');
          +",SLDARINI ="+cp_NullLink(i_cOp3,'SALDICON','SLDARINI');
          +",SLAVEINI ="+cp_NullLink(i_cOp4,'SALDICON','SLAVEINI');
          +",SLDARFIN ="+cp_NullLink(i_cOp5,'SALDICON','SLDARFIN');
          +",SLAVEFIN ="+cp_NullLink(i_cOp6,'SALDICON','SLAVEFIN');
              +i_ccchkf ;
          +" where ";
              +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
              +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
              +" and SLCODESE = "+cp_ToStrODBC(this.w_CODESE);
                 )
        else
          update (i_cTable) set;
              SLDARPER = &i_cOp1.;
              ,SLAVEPER = &i_cOp2.;
              ,SLDARINI = &i_cOp3.;
              ,SLAVEINI = &i_cOp4.;
              ,SLDARFIN = &i_cOp5.;
              ,SLAVEFIN = &i_cOp6.;
              &i_ccchkf. ;
           where;
              SLTIPCON = this.w_PNTIPCON;
              and SLCODICE = this.w_PNCODCON;
              and SLCODESE = this.w_CODESE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Elimina i Centri di Costo Associati alla Reg.di P.N.
        * --- Delete from MOVICOST
        i_nConn=i_TableProp[this.MOVICOST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"MRSERIAL = "+cp_ToStrODBC(this.w_DVPNSERI);
                +" and MRROWORD = "+cp_ToStrODBC(this.w_ROWNUM);
                 )
        else
          delete from (i_cTable) where;
                MRSERIAL = this.w_DVPNSERI;
                and MRROWORD = this.w_ROWNUM;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      endif
        select _Curs_PNT_DETT
        continue
      enddo
      use
    endif
    * --- Delete from PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_DVPNSERI);
             )
    else
      delete from (i_cTable) where;
            PNSERIAL = this.w_DVPNSERI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PNSERIAL = "+cp_ToStrODBC(this.w_DVPNSERI);
             )
    else
      delete from (i_cTable) where;
            PNSERIAL = this.w_DVPNSERI;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rimuove cursori..
    if used("Diffconv")
      select Diffconv
      use
    endif
    if used("Totali")
      select Totali
      use
    endif
    if Not this.w_NOELABORA
      if used("CursElabora")
        Select CursElabora
        use
      endif
    endif
  endproc


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,12)]
    this.cWorkTables[1]='*ZOOMPART'
    this.cWorkTables[2]='VALDATPA'
    this.cWorkTables[3]='PAR_TITE'
    this.cWorkTables[4]='MOVICOST'
    this.cWorkTables[5]='PNT_DETT'
    this.cWorkTables[6]='PNT_MAST'
    this.cWorkTables[7]='AZIENDA'
    this.cWorkTables[8]='SALDICON'
    this.cWorkTables[9]='ESERCIZI'
    this.cWorkTables[10]='VAL_ATPA'
    this.cWorkTables[11]='*TMPTPAR_TITE'
    this.cWorkTables[12]='MOD_PAGA'
    return(this.OpenAllTables(12))

  proc CloseCursors()
    if used('_Curs_PNT_DETT')
      use in _Curs_PNT_DETT
    endif
    if used('_Curs_ZOOMPART')
      use in _Curs_ZOOMPART
    endif
    if used('_Curs_GSCG9BAP')
      use in _Curs_GSCG9BAP
    endif
    if used('_Curs_GSCG7BAP')
      use in _Curs_GSCG7BAP
    endif
    if used('_Curs_VALDATPA')
      use in _Curs_VALDATPA
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
