* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_beo                                                        *
*              Export/import contatti                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_1071]                                           *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-12-13                                                      *
* Last revis.: 2017-05-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARA
* --- Area Manuale = Header
* --- gsar_beo
* Dichiarazione array per aggiornamento OFF_NOMI
Dimension ArrCampi_ON[1,2], Arrkey_ON[1,2]
* Campo carattere � il nome della colonna da aggiornare
ArrCampi_ON[1,2]=""
* Campo carattere � il nome del campo chiave
Arrkey_ON[1,1]=""

* Dichiarazione array per aggiornamento NOM_CONT
Dimension ArrCampi_NC[1,2], Arrkey_NC[2,2]
* Campo carattere � il nome della colonna da aggiornare
ArrCampi_NC[1,2]=""
* Campo carattere � il nome del campo chiave
* In questo caso le chiavi sono due: il codice OFF_NOMI e quello NOM_CONT
Arrkey_NC[1,1]=""
Arrkey_NC[2,1]=""

* Dichiarazione array per aggiornamento OFF_NOMI
Dimension ArrChiavi_ON[1,3], ArrChiavi_NC[1,3]
ArrChiavi_ON[1,1]=""
ArrChiavi_NC[1,1]=""


* Dichiarazione array per aggiornamento NOM_ATTR
Dimension ArrCampi_NA[1,2], Arrkey_NA[2,2]
* Campo carattere � il nome della colonna da aggiornare
ArrCampi_NA[1,2]=""
Arrkey_NA[1,1]=""
Arrkey_NA[2,1]=""
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_beo",oParentObject,m.pPARA)
return(i_retval)

define class tgsar_beo as StdBatch
  * --- Local variables
  pPARA = space(1)
  w_MESS = space(10)
  w_DESATTR = space(35)
  w_CODATTR = space(20)
  w_NOMEEXPO = space(40)
  w_APPCODCON = space(5)
  w_CODICECONTATTO = space(5)
  w_LEVEL = 0
  w_PROGCONT = 0
  loAHR = .NULL.
  loRaggr = .NULL.
  w_CODNINS = space(20)
  w_BODY = space(0)
  w_KoIns = .f.
  w_TipCatDefault = space(10)
  loOutlook = .NULL.
  loNameSpace = .NULL.
  loFolder = .NULL.
  loContact = .NULL.
  loContattiOutlook = .NULL.
  oContDaAnalizzare = .NULL.
  oContatto = .NULL.
  w_AppoNome = space(10)
  w_AppoFolder = .NULL.
  w_AppoContact = .NULL.
  w_ProgrFold = 0
  w_NUMCONTATTI = 0
  w_DaContr = 0
  w_ChiaveOutlook = space(30)
  w_CodiceOutlNominativo = space(30)
  w_CodiceOutlContatto = space(30)
  w_CodiceOutlAzienda = space(30)
  w_CodiceNominativo = space(30)
  w_CodiceContatto = space(30)
  w_AppoSubjectName = space(30)
  w_AppoCompanyName = space(30)
  w_Comodo = space(10)
  w_MAXCODCONT = space(5)
  w_ResMsg = .NULL.
  w_MsgInizio = space(100)
  w_MsgFine = space(100)
  w_RESOCON1 = space(0)
  w_MESBLOK = space(0)
  w_RESOCON1 = space(0)
  w_ANNULLA = .f.
  w_ErrorCNT = 0
  w_TipoCursore = space(1)
  w_CampoConfronto = space(10)
  w_CntChiavi_ON = 0
  w_CntChiavi_NC = 0
  w_ParametroFind = space(10)
  w_FLNuovoCont = space(1)
  w_MCNOMTAB = space(15)
  w_TipoElemOutl = space(1)
  w_FiltroCampi = space(0)
  w_ListaCampi = space(0)
  w_DAIM = .f.
  w_Write_Table = space(1)
  w_Write_Table = space(1)
  w_MsgCartella = space(100)
  UsrEntryId = .NULL.
  loRecipient = .NULL.
  w_ChiaveOutlook = space(30)
  w_NUM = 0
  w_NUM2 = 0
  w_NCAR = 0
  w_OK = .f.
  w_MAXROWORD = 0
  w_CntFields_ON = 0
  w_CntFields_NC = 0
  w_CntFields_NA = 0
  w_NomeTabella = space(10)
  w_NomeProp = space(10)
  i_qry = .NULL.
  i_qry = .NULL.
  w_StrCategories = space(250)
  w_DesCategoria = space(250)
  w_CTCODICE = space(10)
  w_NewCat = space(1)
  w_NumCat = 0
  w_CtrlCTCODICE = space(10)
  w_InsCateg = space(1)
  * --- WorkFile variables
  OFF_NOMI_idx=0
  NOM_CONT_idx=0
  RUO_CONT_idx=0
  AGENTI_idx=0
  XDC_FIELDS_idx=0
  MAP_SINC_idx=0
  CAT_ATTR_idx=0
  NOM_ATTR_idx=0
  TIP_CATT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSAR_KEX (Export contatti) e GSAR_KIC (Import contatti)
    * --- Parametro di scelta E=Export, I=Import
    * --- Import
    * --- Export
    * --- Filtro GSAR_MOU
    * --- Codice nominativo per attributi
    * --- Controllo se aggiorno nominativo+contatto
    this.w_KoIns = .F.
    * --- Dichiarazione variabili VisualFox per gestione errori
    PRIVATE L_OldErr, L_Err
    L_OldErr = ON("ERROR")
    L_Err = .F.
    * --- * potrei includere
    *     * #INCLUDE "MSOutl9.H"
    *     * ho bisogno solo di poche definizioni, copio quelle che mi servono
     
 #define olFolderContacts 10 
 #define olContactItem 2 
 #define olText 1 
 #define olPublicFoldersAllPublicFolders 18
    PRIVATE MSOUTL_CODICE
    MSOUTL_CODICE = "Zucchetti"
    PRIVATE MSOUTL_PROVEN
    MSOUTL_PROVEN = ah_Msgformat("Provenienza")
    * --- Flag per gestione messaggi inserimeno contatti/persone/nominativi 
    * --- Gestione errori
    this.w_FiltroCampi = "N"
    this.w_ListaCampi = ""
    * --- Dichiariamo una variabile per ciascun campo di nominativi e persone
    *     Inoltre inizializziamo il cursore MAPPACAMPI che utilizzeremo durante tutta l'elaborazione
    * --- Preparazione cursore MappaImport - Avvalora le variabili da contatto
    this.w_TipoCursore = "I"
    vq_exec("QUERY\GSAR_BEO.VQR",this,"MappaImport")
    SELECT "MappaImport"
    if RECCOUNT()=0
      * --- Nessuna mappatura specificata
      USE IN SELECT("MappaImport")
      ah_ErrorMsg("Attenzione! Non � stata caricata nessuna mappatura dei campi!")
      i_retcode = 'stop'
      return
    endif
    * --- Preparazione cursore MappaExport - Avvalora il contatto da campo
    this.w_TipoCursore = "E"
    vq_exec("QUERY\GSAR_BEO.VQR",this,"MappaExport")
    * --- Cerchiamo nel cursore quale propriet� corrisponde al campo di ricerca
    if this.pPARA="E"
      SELECT "MappaExport"
    else
      SELECT "MappaImport"
    endif
    this.w_CntChiavi_ON = 1
    this.w_CntChiavi_NC = 1
    SCAN FOR MCFL_KEY="S"
    * --- Gestiamo con IF anzich� CASE poich� potrei avere una stessa chiave per PF ed EN
    do case
      case ALLTRIM(MCNOMTAB)="OFF_NOMI"
        if this.pPARA="E"
          * --- Export
           
 Dimension ArrChiavi_ON[ this.w_CntChiavi_ON, 3 ]
          if MCTIPEXP="S"
            ArrChiavi_ON[ this.w_CntChiavi_ON,1 ] = MCEXPRES
          else
            ArrChiavi_ON[ this.w_CntChiavi_ON,1 ] = MCGESTIO
          endif
          ArrChiavi_ON[ this.w_CntChiavi_ON,2 ] = MCMSOUTL
          ArrChiavi_ON[ this.w_CntChiavi_ON,3 ] = FLTYPE
        else
          * --- Import
           
 Dimension ArrChiavi_ON[ this.w_CntChiavi_ON, 3 ] 
 ArrChiavi_ON[ this.w_CntChiavi_ON,1 ] = MCGESTIO 
 ArrChiavi_ON[ this.w_CntChiavi_ON,2 ] = FLTYPE
        endif
        this.w_CntChiavi_ON = this.w_CntChiavi_ON+ 1
      case ALLTRIM(MCNOMTAB)="NOM_CONT"
        * --- NOM_CONT
        if this.pPARA="E"
          * --- Export
           
 Dimension ArrChiavi_NC[ this.w_CntChiavi_NC, 3 ]
          if MCTIPEXP="S"
            ArrChiavi_NC[ this.w_CntChiavi_NC,1 ] = MCEXPRES
          else
            ArrChiavi_NC[ this.w_CntChiavi_NC,1 ] = MCGESTIO
          endif
          ArrChiavi_NC[ this.w_CntChiavi_NC,2 ] = MCMSOUTL
          ArrChiavi_NC[ this.w_CntChiavi_NC,3 ] = FLTYPE
        else
          * --- Import
           
 Dimension ArrChiavi_NC[ this.w_CntChiavi_NC, 3 ] 
 ArrChiavi_NC[ this.w_CntChiavi_NC,1 ] = MCGESTIO 
 ArrChiavi_NC[ this.w_CntChiavi_NC,2 ] = FLTYPE
        endif
        this.w_CntChiavi_NC = this.w_CntChiavi_NC+ 1
      case ALLTRIM(MCNOMTAB)="NOM_ATTR"
        * --- NOM_ATTR - nessuna chiave
    endcase
    ENDSCAN
    if EMPTY(ArrChiavi_ON(1,1) ) &&&&OR EMPTY( ArrChiavi_NC(1,1) )
      ah_ErrorMsg("Attenzione! Specificare le chiavi di mappatura!")
      USE IN SELECT("MappaImport")
      USE IN SELECT("MappaExport")
      USE IN SELECT("Raggr")
      * --- Al termine del esportazion riattiva il decorator dei form 
      DecoratorState ("A")
      i_retcode = 'stop'
      return
    endif
    * --- Leggiamo da XDC_FIELDS tutti i campi utilizzando la query GSAR_MOU, che filtra per la variabile MCNOMTAB
    this.w_MCNOMTAB = "OFF_NOMI"
    vq_exec("QUERY\GSAR_MOU.VQR",this,"__APP__")
    SELECT __APP__
    GO TOP
    SCAN
    w_TmpCampo = FLNAME
    w_TmpVaria = "PRIVATE w_"+w_TmpCampo
    &w_TmpVaria
    w_TmpVaria = "w_"+w_TmpCampo
    do case
      case FLTYPE="N"
        &w_TmpVaria = 0
      case FLTYPE="D"
        &w_TmpVaria = CTOD("  -  -  ")
      otherwise
        &w_TmpVaria = ""
    endcase
    ENDSCAN
    USE IN SELECT("__APP__")
    this.w_MCNOMTAB = "NOM_CONT"
    vq_exec("QUERY\GSAR_MOU.VQR",this,"__APP__")
    SELECT __APP__
    GO TOP
    SCAN
    w_TmpCampo = FLNAME
    w_TmpVaria = "PRIVATE w_"+w_TmpCampo
    &w_TmpVaria
    w_TmpVaria = "w_"+w_TmpCampo
    do case
      case FLTYPE="N"
        &w_TmpVaria = 0
      case FLTYPE="D"
        &w_TmpVaria = CTOD("  -  -  ")
      otherwise
        &w_TmpVaria = ""
    endcase
    ENDSCAN
    USE IN SELECT("__APP__")
    this.w_MCNOMTAB = "NOM_ATTR"
    vq_exec("QUERY\GSAR_MOU.VQR",this,"__APP__")
    SELECT __APP__
    if RECCOUNT()>0
      * --- Sono gestiti gli attributi nella mappatura - leggiamo la categoria attributi da utilizzare
      * --- Select from TIP_CATT
      i_nConn=i_TableProp[this.TIP_CATT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.TIP_CATT_idx,2],.t.,this.TIP_CATT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MIN(TCCODICE) AS TCCODICE  from "+i_cTable+" TIP_CATT ";
             ,"_Curs_TIP_CATT")
      else
        select MIN(TCCODICE) AS TCCODICE from (i_cTable);
          into cursor _Curs_TIP_CATT
      endif
      if used('_Curs_TIP_CATT')
        select _Curs_TIP_CATT
        locate for 1=1
        do while not(eof())
        this.w_TipCatDefault = NVL(TCCODICE,"")
          select _Curs_TIP_CATT
          continue
        enddo
        use
      endif
      if EMPTY(this.w_TipCatDefault)
        * --- Manca un tipo categoria attributi da utilizzare - non pu� importare attributi nuovi
        ah_ErrorMsg("Attenzione! E' necessario carica un tipo categorie attributi!") 
        * --- Al termine del esportazion riattiva il decorator dei form 
        DecoratorState ("A")
        i_retcode = 'stop'
        return
      endif
    endif
    SELECT __APP__
    GO TOP
    SCAN
    w_TmpCampo = FLNAME
    w_TmpVaria = "PRIVATE w_"+w_TmpCampo
    &w_TmpVaria
    w_TmpVaria = "w_"+w_TmpCampo
    do case
      case FLTYPE="N"
        &w_TmpVaria = 0
      case FLTYPE="D"
        &w_TmpVaria = CTOD("  -  -  ")
      otherwise
        &w_TmpVaria = ""
    endcase
    ENDSCAN
    USE IN SELECT("__APP__")
    this.w_ErrorCNT = 0
    this.w_MESBLOK = ""
    this.w_RESOCON1 = ""
    * --- Assegna a loContact la cartella di Outlook, a loContattiOutlook gli item e in w_NUMCONTATTI il numero di contatti della cartella
    this.w_MsgInizio = Ah_MsgFormat("Inizio operazione %1 contatti %2",Ah_MsgFormat(IIF(this.pPara="E","export","import")),TTOC(DATETIME()))+CHR(13)+CHR(10)
    * --- Disabilita il Decoartor altrimento si generano errori quando l utente muove il mouse sul form
    DecoratorState ("D")
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.pPARA="E"
      this.Page_10()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Controlliamo se ci sono elementi da importare
      if this.w_NUMCONTATTI >0
        * --- Ci sono contatti da analizzare
        this.Page_9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        * --- Nessun contatto
        ah_ErrorMsg("Nessun contatto da analizzare!")
      endif
    endif
    this.Page_8()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Al termine del esportazion riattiva il decorator dei form 
    DecoratorState ("A")
    this.w_MsgFine = Ah_MsgFormat("Fine operazione %1",TTOC(DATETIME()))
    if Ah_YesNo("Visualizzare il log di elaborazione?")
      * --- Per sapere se ci sono errori controlliamo w_ResMsg.ah_MsgParts.Count> 0 
      this.w_RESOCON1 = this.w_MsgInizio + this.w_ResMsg.ComposeMessage() + this.w_MsgFine
      this.w_ANNULLA = .F.
      this.w_DAIM = .T.
      do GSVE_KLG with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    USE IN SELECT("MappaImport")
    USE IN SELECT("MappaExport")
    USE IN SELECT("Raggr")
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento OFF_NOMI: il codice � in w_CodiceNominativo
    *     Procediamo solo se NON esite il contatto oppure se stiamo inserendo un nuominativo nuovo
    * --- Prepariamo la chiave
    Arrkey_ON(1,1)= "NOCODICE"
    Arrkey_ON(1,2)= this.w_CodiceNominativo
    this.w_KoIns = .F.
    if ALEN(ArrCampi_ON,1)=1 AND EMPTY(ArrCampi_ON[1,2 ])
      * --- Non � stato inserito nessun elemento nell'array per l'aggiornamento
      this.w_KoIns = .T.
    else
      Private L_nRec 
 L_nRec= -1
      this.w_Write_Table = ""
      * --- begin transaction
      cp_BeginTrs()
      * --- Try
      local bErr_04FF3AA8
      bErr_04FF3AA8=bTrsErr
      this.Try_04FF3AA8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04FF3AA8
      * --- End
      if Not Empty( this.w_Write_Table )
        this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Errore di UPDATE"))     
      else
        this.w_KoIns = L_nRec<=0
      endif
    endif
    if this.w_KoIns
      this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Aggiornamento del nominativo %1 non effettuato", this.w_CodiceNominativo ))     
    endif
    * --- Inserisce il valore di w_ChiaveOutlook nel nominativo
    this.Page_11()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Devo aggiornare anche il contatto aggiungendo la user property ma solo se questa non esiste.
    *     Attenzione!! Non inserisce NULLA per il codice contatto
    L_Err = .F.
    ON ERROR L_Err=.T.
    if ISNULL( this.oContatto.UserProperties(MSOUTL_CODICE) )
      this.oContatto.UserProperties.Add(MSOUTL_CODICE, olText)     
    endif
    this.oContatto.UserProperties(MSOUTL_CODICE).Value = this.w_ChiaveOutlook
    if ISNULL( this.oContatto.UserProperties(MSOUTL_PROVEN) )
      this.oContatto.UserProperties.Add(MSOUTL_PROVEN, olText)     
    endif
    this.oContatto.UserProperties(MSOUTL_PROVEN).Value = alltrim( upper( g_APPLICATION ) )
    this.oContatto.Save()     
    ON ERROR &L_OldErr
  endproc
  proc Try_04FF3AA8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_Write_Table = WRITETABLE( "OFF_NOMI" , @ArrCampi_ON ,@Arrkey_ON , , , "",@L_nRec)
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento Contatto: il codice del nominativo � in w_CodiceNominativo mentre il codice del contatto � in w_CodiceContatto
    this.w_KoIns = .F.
    if ALEN(ArrCampi_NC,1)=1 AND EMPTY(ArrCampi_NC[1,2 ])
      * --- Non � stato inserito nessun elemento nell'array per l'aggiornamento
      this.w_KoIns = .T.
    else
      Private L_nRec 
 L_nRec= -1
      this.w_Write_Table = ""
      * --- Prepariamo la chiave
      Arrkey_NC(1,1)= "NCCODICE"
      Arrkey_NC(1,2)= this.w_CodiceNominativo
      Arrkey_NC(2,1)= "NCCODCON"
      Arrkey_NC(2,2)= this.w_CodiceContatto
      * --- begin transaction
      cp_BeginTrs()
      * --- Try
      local bErr_04FEEDC8
      bErr_04FEEDC8=bTrsErr
      this.Try_04FEEDC8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_04FEEDC8
      * --- End
      if Not Empty( this.w_Write_Table )
        this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Errore di UPDATE"))     
      else
        this.w_KoIns = L_nRec<=0
      endif
    endif
    if this.w_KoIns
      this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Aggiornamento contatto %1 per il nominativo %2 non effettuato",this.w_CodiceContatto, this.w_CodiceNominativo))     
    endif
    * --- Ribadisce il valore di w_ChiaveOutlook nel nominativo: l'operazione potrebbe gi� essere stata fatta aggiornando OFF_NOMI
    this.Page_11()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Per semplicit� aggiorniamo sempre il codice dell'contatto
    L_Err = .F.
    ON ERROR L_Err=.T.
    if ISNULL( this.oContatto.UserProperties(MSOUTL_CODICE) ) 
      * --- Se non esiste aggiungiamo la propriet�
      this.oContatto.UserProperties.Add(MSOUTL_CODICE, olText)     
    endif
    this.oContatto.UserProperties(MSOUTL_CODICE).Value = this.w_ChiaveOutlook
    if ISNULL( this.oContatto.UserProperties(MSOUTL_PROVEN) )
      this.oContatto.UserProperties.Add(MSOUTL_PROVEN, olText)     
    endif
    this.oContatto.UserProperties(MSOUTL_PROVEN).Value = alltrim( upper( g_APPLICATION ) )
    this.oContatto.Save()     
    ON ERROR &L_OldErr
  endproc
  proc Try_04FEEDC8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_Write_Table = WRITETABLE( "NOM_CONT" , @ArrCampi_NC ,@Arrkey_NC , , , "",@L_nRec)
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Inserisce solamente il nuovo contatto in Outlook.
    *     Per gli altri dati si utilizza la fase di aggiornamento
    L_Err = .F.
    ON ERROR L_Err=.T.
    this.oContatto = .null.
    * --- Add(olContactItem) restituisce errore
    this.oContatto = this.loContattiOutlook.Add( olContactItem )
    if L_Err
      this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Fallita creazione del contatto %1",IIF(!EMPTY(this.w_AppoSubjectName),ALLTRIM(this.w_AppoSubjectName),ALLTRIM(this.w_AppoCompanyName))))     
    else
      this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Creato contatto %1",IIF(!EMPTY(this.w_AppoSubjectName),ALLTRIM(this.w_AppoSubjectName),ALLTRIM(this.w_AppoCompanyName))))     
    endif
    ON ERROR &L_OldErr
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Outlook: aggiorna il contatto
    L_Err = .F.
    this.w_NOMEEXPO = ""
    this.w_CodiceNominativo = NOCODICE
    this.w_CodiceContatto = NCCODCON
    this.oContatto.FirstName = ""
    this.Page_18()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    L_Err = .F.
    ON ERROR L_Err=.T.
    if ISNULL( this.oContatto.UserProperties(MSOUTL_CODICE) ) 
      * --- Se non esiste aggiungiamo la propriet�
      this.oContatto.UserProperties.Add(MSOUTL_CODICE, olText)     
    endif
    this.oContatto.UserProperties(MSOUTL_CODICE).Value = this.w_ChiaveOutlook
    * --- Crea un campo ulteriore per ident. AHR/AHE
    if ISNULL( this.oContatto.UserProperties(MSOUTL_PROVEN) )
      this.oContatto.UserProperties.Add(MSOUTL_PROVEN, olText)     
    endif
    this.oContatto.UserProperties(MSOUTL_PROVEN).Value = alltrim( upper( g_APPLICATION ) )
    this.oContatto.Save()     
    ON ERROR &L_OldErr
    * --- Se dobbiamo notificare un messaggio, w_FLNuovoCont='N'' significa che NON � un nuovo inserimento 
    *     e che quindi si pu� notificare, mentre in caso contrario � gi� stata fatta la notifica di creazione
    if L_Err
      this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Fallito aggiornamento del contatto %1",IIF(!EMPTY(this.w_AppoCompanyName),ALLTRIM(this.w_AppoCompanyName),ALLTRIM(this.w_AppoSubjectName))))     
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Outlook: si posiziona nella cartella passata come parametro
    *     Ho due alternative: sono nella cartella principale oppure sono in una cartella parallela. 
    *     In entrambi i casi, w_AppoFolder � la cartella iniziale e quindi cerco tra le subdirectory di questa.
    *     La cartella ottenuta sar� "appoggiata" in w_AppoContact
    L_Err = .F.
    ON ERROR L_Err=.T.
    this.w_ProgrFold = 1
    this.w_AppoContact = .NULL.
    do while this.w_ProgrFold<= this.w_AppoFolder.Folders.Count
      * --- Cerco la cartella w_AppoFolder
      if ALLTRIM(UPPER(this.w_AppoFolder.Folders.item(this.w_ProgrFold).Name)) == ALLTRIM(UPPER(this.w_AppoNome))
        * --- Cartella trovata!
        this.w_AppoContact = this.w_AppoFolder.Folders.item(this.w_ProgrFold)
        * --- Aggiorniamo il progressivo con il massimo valore consentito in modo ad forzare l'uscita del while senza utilzzare una var. di appoggio
        this.w_ProgrFold = this.w_AppoFolder.Folders.Count + 1
      endif
      this.w_ProgrFold = this.w_ProgrFold + 1
    enddo
    ON ERROR &L_OldErr
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione istanza Outlook e apertura log
    * --- * potrei includere  #INCLUDE "MSOutl9.H" ma servono solo poche definizioni
    * --- Gestione errori: dobbiamo gestire L_OldErr come statement perch� la ON ERROR segnala errore con l'espansione di macro
    L_Err = .F.
    ON ERROR L_Err=.T.
    * --- Creo una sessione di Outlook
    this.loOutlook = CREATEOBJECT("Outlook.Application")
    if L_Err OR ISNULL(this.loOutlook)
      * --- Non ha creato l'istanza
      ON ERROR &L_OldErr
      ah_ErrorMsg("Errore nella creazione di un'istanza di Outlook")
      * --- Al termine del esportazion riattiva il decorator dei form 
      DecoratorState ("A")
      i_retcode = 'stop'
      return
    endif
    * --- * Inizializzo il NameSpace
    this.loNamespace = this.loOutlook.GetNamespace("MAPI")
    * --- * Individuo la cartella di default
    if this.oParentObject.w_FlPublic="S"
      * --- Ricerchiamo la cartella pubblica
      this.UsrEntryId = this.loNamespace.CurrentUser.EntryID
      this.loRecipient = this.loNamespace.GetRecipientFromID(this.UsrEntryID)
      this.loFolder = this.loNamespace.GetSharedDefaultFolder( this.loRecipient , olFolderContacts )
      if L_Err OR ISNULL(this.loFolder)
        * --- Non esiste una cartella pubblica di default
        this.w_AppoFolder = this.loNameSpace
        this.w_AppoNome = "Cartelle pubbliche"
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_AppoFolder = this.w_AppoContact
        this.w_AppoNome = "Tutte le cartelle pubbliche"
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.w_AppoFolder = this.w_AppoContact
        this.w_AppoNome = "Contatti"
        this.Page_6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Se esiste, loFolder � la cartella "Cartelle pubbliche\Tutte le cartelle pubbliche\Contatti"
        this.loFolder = this.w_AppoContact
      endif
      this.w_MsgCartella = "Impossibile accedere al profilo oppure alla cartella pubblica specificata!"
    else
      this.loFolder = this.loNamespace.GetDefaultFolder( olFolderContacts )
      this.w_MsgCartella = "Impossibile accedere al profilo oppure alla cartella specificata!"
    endif
    ON ERROR &L_OldErr
    if L_Err OR ISNULL(this.loFolder)
      * --- Non � stato scelto un profilo
      ah_ErrorMsg(this.w_MsgCartella)
      * --- Al termine del esportazion riattiva il decorator dei form 
      DecoratorState ("A")
      i_retcode = 'stop'
      return
    endif
    if EMPTY(this.oParentObject.w_OFNOME)
      * --- Deve utilizzare la cartella di defult
      L_Err = .F.
      ON ERROR L_Err=.T.
      this.loContact = this.loFolder
      ON ERROR &L_OldErr
    else
      * --- Cerca la sottocartella dal nome w_AppoNome: dato  restituisce w_AppoContact
      this.w_AppoNome = this.oParentObject.w_OFNOME
      this.w_AppoFolder = this.loFolder
      this.Page_16()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      L_Err = .F.
      ON ERROR L_Err=.T.
      this.loContact = this.w_AppoContact
      ON ERROR &L_OldErr
    endif
    ON ERROR L_Err=.T.
    this.loContattiOutlook = this.loContact.Items
    this.w_NUMCONTATTI = this.loContattiOutlook.Count
    ON ERROR &L_OldErr
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura log
    * --- Drop temporary table TMPDPOFF
    i_nIdx=cp_GetTableDefIdx('TMPDPOFF')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPDPOFF')
    endif
    ah_Msg("Operazione completata")
    ON ERROR &L_OldErr
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Import: abbiamo in loContact la cartella, in loContattiOtlook gli elementi e in w_NUMCONTATTI il numero di contatti gi� presenti
    AH_Msg("Inizio import CONTATTI..." )
    * --- *********************************************************************************
    *     Decidere se fare qui una RESTRICT oppure se controllare ogni item
    *     *********************************************************************************
    L_Err = .F.
    ON ERROR L_Err=.T.
    this.oContDaAnalizzare = this.loContattiOutlook
    if !EMPTY(this.oParentObject.w_OFCODCON1)
      this.oContDaAnalizzare.Restrict("[FullName] >= ' "+ALLTRIM(this.oParentObject.w_OFCODNOM1)+"'")     
    endif
    if !EMPTY(this.oParentObject.w_OFCODCON3)
      this.oContDaAnalizzare.Restrict("[FullName] <= ' "+ALLTRIM(this.oParentObject.w_OFCODNOM3)+"'")     
    endif
    if !EMPTY(this.oParentObject.w_OFDESNOM1)
      this.oContDaAnalizzare.Restrict("[CompanyName] >= ' "+ALLTRIM(this.oParentObject.w_OFDESNOM1)+"'")     
    endif
    if !EMPTY(this.oParentObject.w_OFDESNOM3)
      this.oContDaAnalizzare.Restrict("[CompanyName] <= ' "+ALLTRIM(this.oParentObject.w_OFDESNOM3)+"'")     
    endif
    if NOT EMPTY(this.oParentObject.w_OFCODNOM1)
      this.oContDaAnalizzare.Restrict("[CustomerId] >= ' "+ALLTRIM(this.oParentObject.w_OFCODNOM1)+"'")     
    endif
    if NOT EMPTY(this.oParentObject.w_OFCODNOM3)
      this.oContDaAnalizzare.Restrict("[CustomerId] <= ' "+ALLTRIM(this.oParentObject.w_OFCODNOM3)+"'")     
    endif
    if this.pPara="I"
      * --- In caso di import ci serve un ordinamento per descrizione di societ� o di fullname
      this.oContDaAnalizzare.Sort("FullName")     
    else
      * --- In caso di export ci serve un ordinamento per codice outlook in modo da velocizzare la ricerca
      this.oContDaAnalizzare.Sort(MSOUTL_CODICE)     
      this.oContDaAnalizzare.Sort("FullName")     
    endif
    * --- Ordinando per societ� prima di tutto elaboriamo i records che ne fanno parte
    ON ERROR &L_OldErr
    * --- Gestiamo nella variabile w_DaContr il numero di appuntamenti che ancora ci restano da controllare
    this.w_DaContr = this.w_NUMCONTATTI
    L_Err = .F.
    do while this.w_DaContr > 0 AND !L_Err
      L_Err = .F.
      ON ERROR L_Err=.T.
      * --- Procediamo decrescendo in modo che non sia necessario un ulteriore sort 
      *     e affinch� Outlook non vada in confusione decrementando la Count
      this.oContatto = this.oContDaAnalizzare.Item( this.w_DaContr )
      ON ERROR &L_OldErr
      this.w_CodiceNominativo = ""
      this.w_CodiceContatto = ""
      * --- Dovrei fare qui i controlli per vedere se prima di tutto soddisfa i filtri specificati in maschera
      * --- Dato oContatto, avvalora le variabili mappando i campi
      this.Page_17()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Dopo aver avvalorato le variabili, conosciamo la denominazione e il companyName
      *     NON utilizziamo il fullName ma ci limitiamo al Subject, in modo da non vedere i titoli specificati
      this.w_AppoSubjectName = this.oContatto.Subject
      this.w_AppoCompanyName = this.oContatto.CompanyName
      if !ISNULL( this.oContatto.UserProperties(MSOUTL_CODICE) ) AND !EMPTY( this.oContatto.UserProperties(MSOUTL_CODICE).Value )
        this.w_ChiaveOutlook = this.oContatto.UserProperties(MSOUTL_CODICE).Value
        * --- Il contatto era gi� stato esportato in precedenza
        this.Page_12()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Prima di procedere controlliamo che abbia il codice azienda corretto, altrimenti scartiamo
        if this.w_CodiceOutlAzienda==i_CodAzi
          * --- E' dell'azienda attiva: controlliamo se � gi� presente in OFF_ATTI e quindi deve essere aggiornato
          *     oppure se si deve procedere con la cancellazione
          this.w_CodiceNominativo = this.w_CodiceOutlNominativo
          this.w_CodiceContatto = this.w_CodiceOutlContatto
          this.Page_13()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        * --- Ancora non abbiamo una chiave esterna per Outlook
        this.w_ChiaveOutlook = ""
        * --- Devo controllare se � gi� stato importato in precedenza (e quindi aggiornare) oppure se non � mai stato importato (e quindi devo importare), controllando sempre per DESCRIZIONE.
        *     NON controlliamo i valori eventualmente presenti in CustomerID e OrganizationID perch� potrebbero essere di un'altra azienda o comunque non contenere valori corretti
        this.Page_19()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_DaContr = this.w_DaContr - 1
    enddo
    AH_ErrorMsg("Import contatti terminato",64)
  endproc


  procedure Page_10
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Export: abbiamo in loContact la cartella, in loContattiOtlook gli elementi e in w_NUMCONTATTI il numero di contatti gi� presenti
    * --- Con la nuova mappatura utilizziamo GSAR1BEO, in cui i nomi dei campi sono gli stessi di OFF_NOMI
    *     Prima utilizzava la query GSOF_KEI
    vq_exec("QUERY\GSAR1BEO.VQR",this,"RAGGR")
    if reccount("RAGGR")=0 
      AH_ErrorMsg("Nessun contatto da esportare",48)
      * --- Al termine del esportazion riattiva il decorator dei form 
      DecoratorState ("A")
      i_retcode = 'stop'
      return
    endif
    * --- w_AppoFolder ci serve per tutte le ricerche e non sar� mai modificato durante l'elaborazione
    L_Err = .F.
    ON ERROR L_Err=.T.
    this.w_AppoFolder = this.loContact
    ON ERROR &L_OldErr
    * --- Se � specificata una cartella in w_OFNOME, noi siamo gi� posizionati in essa.
    *     Creiamo le cartelle relative ai raggruppamenti, ma solo se � specificato un raggruppamento
    *     Ci troviamo in loContact, quindi dobbiamo utilizzare questa cartella come riferimento
    if this.oParentObject.w_OFRAGGR<>"N"
      AH_Msg("Creazione cartelle per raggruppamenti" )
      L_Err = .F.
      ON ERROR L_Err=.T.
      SELECT DISTINCT ORDINE FROM RAGGR INTO CURSOR FOLDTOCREATE
      ON ERROR &L_OldErr
      * --- C'� senz'altro almeno una cartella di raggruppamento
      SCAN
      this.w_FLNuovoCont = "S"
      * --- Per ciascuna cartella controlliamo l'esistenza: nel caso in cui non sia presente dobbiamo crearla (sempre all'interno del folde attuale).
      *     Ci limitiamo a ceracare nel primo livello.
      * --- Cerca la sottocartella dal nome w_AppoNome: dato  restituisce w_AppoContact
      this.w_AppoNome = ORDINE
      this.Page_16()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.loContact = this.w_AppoContact
      ENDSCAN
      * --- Abbiamo mantenuto in loFolder il nome della cartella di origine, quindi ci riposizioniamo in questa
      this.loContact = this.loFolder
      USE IN SELECT("FOLDTOCREATE")
    endif
    * --- Abbiaom gi� loContattiOutolook e w_NumContatti
    AH_Msg("Inizio export CONTATTI..." )
    SELECT RAGGR
    GO TOP
    SCAN
    * --- Se � specificato un raggruppamento e se il nome di ORDINE � diverso dal nome della cartella, allora devo cambiare cartella e posizionarmi su di essa.
    *     A questo punto controllo: se ci sono gi� elementi inseriti, significa che ogni volta che inseriamo un elemento dobbiamo controllare se � gi� stato esportato, 
    *     altrimenti possiamo inserire tutto senza nessun tipo di controllo.
    if this.oParentObject.w_OFRAGGR<>"N" AND !ALLTRIM(this.loContact.Name)==ALLTRIM(ORDINE)
      * --- C'� un ordinamento e quindi devo controllare se sono nella cartella giusta.
      *     Se non ci sono mi devo posizionare
      this.w_AppoNome = RAGGR.ORDINE
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      L_Err = .F.
      ON ERROR L_Err=.T.
      this.loContact = this.w_AppoContact
      this.loContattiOutlook = this.loContact.Items
      this.w_NUMCONTATTI = this.loContattiOutlook.Count
      ON ERROR &L_OldErr
    endif
    * --- Cerco se il contatto � gi� stato esportato: in w_NumContatti abbiamo il numero di elementi presenti nella cartella al momento del posizionamento.
    *     Memorizziamo nella variabile w_FLNuovoCont se � o meno un nuovo contatto
    * --- Prepariamo il codice univoco del nominativo
    this.w_CodiceNominativo = NOCODICE
    this.w_CodiceContatto = NCCODCON
    this.w_AppoCompanyName = NODESCRI
    this.w_AppoSubjectName = NCPERSON
    this.Page_11()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Il contatto eventualmente reperito sar� memorizzato in oContatto
    this.w_FLNuovoCont = "S"
    if this.w_NUMCONTATTI>0
      * --- C'erano gi� elementi nella cartella, cerco se fosse gi� stato esportato.
      *     Effettua una prima ricerca tramite il codice: se trova il contatto, successivamente lo aggiorna
      L_Err = .F.
      ON ERROR L_Err=.T.
      this.oContatto = this.loContattiOutlook.Find("["+MSOUTL_CODICE+'] = "'+this.w_ChiaveOutlook+'"')
      ON ERROR &L_OldErr
      * --- Se il contatto con il codice non esiste, lo cerca per nome
      if ISNULL(this.oContatto)
        * --- Non c'� nessun elemento con codice corrispondente.
        *     Procediamo con la ricerca
        this.w_ParametroFind = ""
        * --- Impostazione parametro di ricerca
        * --- Scandiamo l'array delle propriet� per comporre la frase di ricerca
        FOR cnt=1 TO ALEN(ArrChiavi_ON, 1)
        w_TmpCampo = ALLTRIM( ArrChiavi_ON(Cnt , 2) )
        w_TmpVaria = STRTRAN( ALLTRIM( ArrChiavi_ON(Cnt , 1) ), ".","RAGGR.")
        w_TmpVaria2 = &w_TmpVaria
        if !EMPTY(w_TmpVaria2)
          * --- C'� un filtro sui nominativi
          if ArrChiavi_ON(Cnt , 3)="C"
            this.w_ParametroFind = this.w_ParametroFind+IIF(!EMPTY(this.w_ParametroFind)," AND ","")+"["+ALLTRIM(w_TmpCampo)+"] = '"+ALLTRIM(w_TmpVaria2 )+"'"
          else
            this.w_ParametroFind = this.w_ParametroFind+IIF(!EMPTY(this.w_ParametroFind)," AND ","")+"["+ALLTRIM(w_TmpCampo)+"] = "+ALLTRIM(w_TmpVaria2 )
          endif
        endif
        ENDFOR
        if !EMPTY( ArrChiavi_NC(1,1) )
          * --- Scandiamo l'array delle propriet� per comporre la frase di ricerca
          FOR cnt=1 TO ALEN(ArrChiavi_NC, 1)
          w_TmpCampo = ALLTRIM( ArrChiavi_NC(Cnt , 2) )
          w_TmpVaria = STRTRAN( ALLTRIM( ArrChiavi_NC(Cnt , 1) ), ".","RAGGR.")
          w_TmpVaria2 = &w_TmpVaria
          if !EMPTY(w_TmpVaria2)
            * --- C'� un filtro su NOM_CONT
            if ArrChiavi_NC(Cnt , 3)="C"
              this.w_ParametroFind = this.w_ParametroFind+IIF(!EMPTY(this.w_ParametroFind)," AND ","")+"["+ALLTRIM(w_TmpCampo)+"] = '"+ALLTRIM(w_TmpVaria2 )+"'"
            else
              this.w_ParametroFind = this.w_ParametroFind+IIF(!EMPTY(this.w_ParametroFind)," AND ","")+"["+ALLTRIM(w_TmpCampo)+"] = "+ALLTRIM(w_TmpVaria2 )
            endif
          endif
          ENDFOR
        endif
        L_Err = .F.
        ON ERROR L_Err=.T.
        this.oContatto = this.loContattiOutlook.Find( this.w_ParametroFind )
        ON ERROR &L_OldErr
        if !ISNULL(this.oContatto) AND (ISNULL( this.oContatto.UserProperties(MSOUTL_CODICE) ) OR EMPTY( this.oContatto.UserProperties(MSOUTL_CODICE).Value ))
          * --- Ha trovato e il contatto non aveva nessun codice
          this.w_FLNuovoCont = "N"
        endif
      else
        * --- Abbiamo trovato il contatto relativo al nominativo
        this.w_FLNuovoCont = "N"
      endif
    endif
    if this.w_FLNuovoCont="S"
      * --- Inserisce il nuovo contatto
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Sia per i nuovi contatti che per i vecchi procede con l'aggiornamento dei dati
    this.Page_5()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    ENDSCAN
  endproc


  procedure Page_11
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Preparazione codice
    * --- Nel caso in cui NON ci sia il codice contatto, inseriamo 6 caratteri invece dei 5 in modo da poter poi controllare successivamente se ci sono o meno le persone
    this.w_ChiaveOutlook = ALLTRIM(i_CodAzi+"#"+this.w_CodiceNominativo+"#"+IIF(!EMPTY(this.w_CodiceContatto),this.w_CodiceContatto,"@@@@@@"))
  endproc


  procedure Page_12
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Decodifica codice: dato w_ChiaveOutloo estrae il codice azienda, quello del nominativo e dell'eventuale contatto
    this.w_CodiceOutlAzienda = ""
    this.w_CodiceOutlNominativo = ""
    this.w_CodiceOutlContatto = ""
    this.w_CodiceOutlAzienda = SUBSTR(this.w_ChiaveOutlook, 1, AT("#", this.w_ChiaveOutlook, 1)-1)
    this.w_CodiceOutlNominativo = SUBSTR(this.w_ChiaveOutlook, AT("#", this.w_ChiaveOutlook, 1)+1,AT("#", this.w_ChiaveOutlook, 2)-AT("#", this.w_ChiaveOutlook, 1)-1)
    this.w_CodiceOutlContatto = SUBSTR(this.w_ChiaveOutlook, AT("#", this.w_ChiaveOutlook, 2)+1)
  endproc


  procedure Page_13
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dobbiamo aggiornare il gestionale, sempre che esista sempre il record in OFF_NOMI.
    * --- Read from OFF_NOMI
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "NOCODICE"+;
        " from "+i_cTable+" OFF_NOMI where ";
            +"NOCODICE = "+cp_ToStrODBC(this.w_CodiceNominativo);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        NOCODICE;
        from (i_cTable) where;
            NOCODICE = this.w_CodiceNominativo;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_Comodo = NVL(cp_ToDate(_read_.NOCODICE),cp_NullValue(_read_.NOCODICE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if !EMPTY(this.w_COMODO)
      if this.w_CodiceContatto=="@@@@@@"
        * --- Non dobbiamo gestire NOM_CONT
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_21()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        * --- Esiste anche la persona, quindi dobbiamo gestire NOM_CONT
        * --- Read from NOM_CONT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.NOM_CONT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2],.t.,this.NOM_CONT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NCCODCON"+;
            " from "+i_cTable+" NOM_CONT where ";
                +"NCCODICE = "+cp_ToStrODBC(this.w_CodiceNominativo);
                +" and NCCODCON = "+cp_ToStrODBC(this.w_CodiceContatto);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NCCODCON;
            from (i_cTable) where;
                NCCODICE = this.w_CodiceNominativo;
                and NCCODCON = this.w_CodiceContatto;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_Comodo = NVL(cp_ToDate(_read_.NCCODCON),cp_NullValue(_read_.NCCODCON))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if EMPTY(this.w_COMODO)
          * --- La persona non esiste pi�: la inserisce riutilizzando come codice w_CodiceContatto
          this.Page_15()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Aggiorna
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_21()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      * --- Manca il nominativo in OFF_NOMI
      *     Ci limitiamo a segnalare l'assenza, ma si potrebbe procedere con oContatto.Delete()
      this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Nominativo con codice %1 mancante",this.w_CodiceNominativo))     
    endif
  endproc


  procedure Page_14
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Insert in OFF_NOMI il codice w_CODNINS
    this.w_CODNINS = LEFT(IIF(!EMPTY(this.w_AppoCompanyName),ALLTRIM(this.w_AppoCompanyName),ALLTRIM(this.w_AppoSubjectName)), 20)
    * --- Try
    local bErr_04F996A0
    bErr_04F996A0=bTrsErr
    this.Try_04F996A0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Impossibile creare il nominativo %1", IIF(!EMPTY(this.w_AppoCompanyName),ALLTRIM(this.w_AppoCompanyName),ALLTRIM(this.w_AppoSubjectName))))     
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
    endif
    bTrsErr=bTrsErr or bErr_04F996A0
    * --- End
  endproc
  proc Try_04F996A0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if this.oParentObject.w_FLGNUM="S"
      * --- Nel caso di codifica numerica ricalcolo il progressivo
      this.w_CODNINS = SPACE(15)
      i_Conn=i_TableProp[this.OFF_NOMI_IDX, 3]
      cp_NextTableProg(this, i_Conn, "PRNUNMOF", "i_codazi,w_CODNINS", .T.)
      p_LL=LEN(ALLTRIM(p_NOM))
      * --- Ridimensiono il codice in base alla 'maschera'  impostata sui parametri negozio principale
      this.w_CODNINS = RIGHT(ALLTRIM(this.w_CODNINS),p_LL)
    else
      * --- Codifica cliente maggiore o solita codifica
      this.w_NUM = 1
      this.w_NUM2 = 1
      this.w_OK = .F.
      this.w_NCAR = LEN(p_NOM)-1
      if LEN(ALLTRIM(this.w_CODNINS))>LEN(p_NOM)
        this.w_CODNINS = LEFT(ALLTRIM(this.w_CODNINS),LEN(p_NOM))
      endif
      * --- Controllo sulla lunghezza del codice da creare
      do while this.w_OK=.F. AND this.w_NCAR>=0
        * --- Read from OFF_NOMI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" OFF_NOMI where ";
                +"NOCODICE = "+cp_ToStrODBC(this.w_CODNINS);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                NOCODICE = this.w_CODNINS;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Nel caso esista gi� la codifica ne viene costruita una
        if i_Rows<>0 AND this.w_NUM <= 999999
          this.w_NUM = this.w_NUM+1
          this.w_NUM2 = this.w_NUM2+1
          if this.w_NUM2=10
            this.w_NCAR = this.w_NCAR-1
            this.w_NUM2 = 0
          endif
          this.w_CODNINS = LEFT(this.w_CODNINS,this.w_NCAR)+ALLTRIM(STR(this.w_NUM))
        else
          this.w_OK = .T.
        endif
      enddo
      * --- Gestire segnalazione: if W_OK � FALSE significa che non � riuscito a creare un codice per il nominativo
    endif
    * --- Insert into OFF_NOMI
    i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OFF_NOMI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"NOCODICE"+",NOTIPNOM"+",UTCC"+",UTDC"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CODNINS),'OFF_NOMI','NOCODICE');
      +","+cp_NullLink(cp_ToStrODBC(IIF(IsAlt(), "T", "L")),'OFF_NOMI','NOTIPNOM');
      +","+cp_NullLink(cp_ToStrODBC(i_CodUte),'OFF_NOMI','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate(g_CALUTD)),'OFF_NOMI','UTDC');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'NOCODICE',this.w_CODNINS,'NOTIPNOM',IIF(IsAlt(), "T", "L"),'UTCC',i_CodUte,'UTDC',SetInfoDate(g_CALUTD))
      insert into (i_cTable) (NOCODICE,NOTIPNOM,UTCC,UTDC &i_ccchkf. );
         values (;
           this.w_CODNINS;
           ,IIF(IsAlt(), "T", "L");
           ,i_CodUte;
           ,SetInfoDate(g_CALUTD);
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    * --- Adesso aggiorna il nominativo come standard. Il codice del nominativo deve essere in w_CodiceNominativo
    this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Creazione del nominativo %1", IIF(!EMPTY(this.w_AppoCompanyName),ALLTRIM(this.w_AppoCompanyName),ALLTRIM(this.w_AppoSubjectName))))     
    this.w_CodiceNominativo = this.w_CODNINS
    return


  procedure Page_15
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiunge il contatto in NOM_CONT: calcola il massimo codice da utilizzare.
    *     Il codice del nominativo � in w_CodiceNominativo, mentre il codice del nuovo contatto sar� in w_CodiceContatto
    this.w_MAXCODCONT = ""
    this.w_MAXROWORD = 0
    * --- Select from NOM_CONT
    i_nConn=i_TableProp[this.NOM_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2],.t.,this.NOM_CONT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MAX(CPROWORD) AS MAXROWORD  from "+i_cTable+" NOM_CONT ";
          +" where NCCODICE="+cp_ToStrODBC(this.w_CodiceNominativo)+"";
           ,"_Curs_NOM_CONT")
    else
      select MAX(CPROWORD) AS MAXROWORD from (i_cTable);
       where NCCODICE=this.w_CodiceNominativo;
        into cursor _Curs_NOM_CONT
    endif
    if used('_Curs_NOM_CONT')
      select _Curs_NOM_CONT
      locate for 1=1
      do while not(eof())
      this.w_MAXROWORD = NVL(MAXROWORD, 0)
        select _Curs_NOM_CONT
        continue
      enddo
      use
    endif
    this.w_MAXROWORD = this.w_MAXROWORD+10
    * --- w_CodiceContatto � pieno se il contatto era stato esportato e successivamente � stato cancellato nel gestionale,
    *     nel qual caso tentiamo di riutilizzare il codice
    if EMPTY(this.w_CodiceContatto)
      * --- E' un nuovo contatto e non ha nessun codice specificato.
      *     Dobbiamo fare una seconda select perch� aggiungiamo il filtro sul codice like OU per ricalcolarlo
      * --- Select from NOM_CONT
      i_nConn=i_TableProp[this.NOM_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2],.t.,this.NOM_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MAX(NCCODCON) AS MAXCODCONT  from "+i_cTable+" NOM_CONT ";
            +" where NCCODICE="+cp_ToStrODBC(this.w_CodiceNominativo)+" AND NOM_CONT.NCCODCON LIKE 'OU%'";
             ,"_Curs_NOM_CONT")
      else
        select MAX(NCCODCON) AS MAXCODCONT from (i_cTable);
         where NCCODICE=this.w_CodiceNominativo AND NOM_CONT.NCCODCON LIKE "OU%";
          into cursor _Curs_NOM_CONT
      endif
      if used('_Curs_NOM_CONT')
        select _Curs_NOM_CONT
        locate for 1=1
        do while not(eof())
        this.w_MAXCODCONT = NVL(MAXCODCONT, SPACE(5))
          select _Curs_NOM_CONT
          continue
        enddo
        use
      endif
      this.w_CodiceContatto = "OU"+PADL( ALLTRIM(STR(VAL(SUBSTR(this.w_MAXCODCONT,3,3))+1 )) ,3,"0")
    endif
    * --- Insert into NOM_CONT
    i_nConn=i_TableProp[this.NOM_CONT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.NOM_CONT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"NCCODICE"+",NCCODCON"+",CPROWORD"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CodiceNominativo),'NOM_CONT','NCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CodiceContatto),'NOM_CONT','NCCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAXROWORD),'NOM_CONT','CPROWORD');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'NCCODICE',this.w_CodiceNominativo,'NCCODCON',this.w_CodiceContatto,'CPROWORD',this.w_MAXROWORD)
      insert into (i_cTable) (NCCODICE,NCCODCON,CPROWORD &i_ccchkf. );
         values (;
           this.w_CodiceNominativo;
           ,this.w_CodiceContatto;
           ,this.w_MAXROWORD;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Creazione del contatto %1 per il nominativo %2",this.w_CodiceContatto, this.w_CodiceNominativo))     
  endproc


  procedure Page_16
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cerca la directory dal nome w_AppoNome: dato w_AppoFolder restituisce w_AppoContact
    this.Page_6()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if ISNULL(this.w_AppoContact)
      if this.pPARA="E"
        * --- Export, crea la cartella
        L_Err = .F.
        ON ERROR L_Err=.T.
        this.w_AppoContact = this.w_AppoFolder.folders.Add(ALLTRIM(this.w_AppoNome))
        ON ERROR &L_OldErr
      else
        * --- Esce e segnala errore
        ah_ErrorMsg("Cartella non presente!")
        * --- Al termine del esportazion riattiva il decorator dei form 
        DecoratorState ("A")
        i_retcode = 'stop'
        return
      endif
    endif
  endproc


  procedure Page_17
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Invece di lavorare con le propriet� del contatto utilizziamo le variabili mappandole
    *     Prepariamo gi� l'array da passare alla WRITETABLE con cui aggiornare il gestionale
    this.w_CntFields_ON = 1
    this.w_CntFields_NC = 1
    this.w_CntFields_NA = 1
    Dimension ArrCampi_ON[1,2] 
 ArrCampi_ON[1,2]="" 
 
 Dimension ArrCampi_NC[1,2] 
 ArrCampi_NC[1,2]="" 
 
 Dimension ArrCampi_NA[1,2] 
 ArrCampi_NA[1,2]=""
    SELECT "MappaImport"
    GO TOP
    SCAN
    this.w_NomeTabella = MCNOMTAB
    w_TmpCampo = MCGESTIO
    w_TmpVaria = "w_"+w_TmpCampo 
    * --- La variabile assume il valore della propriet� oppure valuta l'espressione
    if MCTIPEXP="S" AND !EMPTY(MCEXPRES)
      * --- Per evitare che .UserProperties('Prop').Value sia sostituito, usiamo && e lo sostituiamo
      w_TmpVaria2 = ALLTRIM(STRTRAN(MCEXPRES,".","this.ocontatto."))
      w_TmpVaria2 = ALLTRIM(STRTRAN(w_TmpVaria2,"&*&","."))
    else
      w_TmpCampo2 = MCMSOUTL
      w_TmpVaria2 = "this.oContatto."+w_TmpCampo2 
    endif
    * --- Gestiamo l'init della variabile in modo da "pulire" i valori relativi all'ultimo contatto analizzato
    L_Err = .F.
    ON ERROR L_Err=.T.
    do case
      case FLTYPE="N"
        * --- Carattere, controlliamo la lunghezza
        &w_TmpVaria = 0
      case FLTYPE="D"
        &w_TmpVaria = CTOD("  -  -  ")
      otherwise
        &w_TmpVaria = ""
    endcase
    if !ISNULL( &w_TmpVaria2 ) AND !EMPTY( &w_TmpVaria2 )
      * --- Per controllare tipizzazione e lunghezza campo abbiamo inserito la join con XDC_FIELDS in GSAR_BEO.VQR
      VarTemporanea = &w_TmpVaria2 
      do case
        case Fltype="C"
          * --- Campo carattere
          VarTemporanea = LEFT(ALLTRIM(VarTemporanea), FLLENGHT)
        case Fltype="D"
          * --- Campo data
          if VarTemporanea < i_IniDat OR VarTemporanea > i_FinDat 
            * --- Data oltre limiti consentiti da database
            VarTemporanea = CTOD("  -  -  ")
            * --- Ci comportiamo come se ci fosse stato un errore ed ignoriamo la propriet�
            L_Err = .T.
          endif
      endcase
      * --- Non utilizziamo &w_TmpVaria = &w_TmpVaria2 al fine di controllare tipi e lunghezze
      &w_TmpVaria = VarTemporanea
      * --- Inseriamo in ArrCampi_ON e ArrCampi_NC i valori delle variabili che abbiamo appena valutato in modo che siano aggiornati in OFF_NOMI e NOM_CONT
      *     Utilizzeremo certamente l'array di OFF_NOMI mentre non � detto che si debba aggiornare anche NOM_CONT.
      *     
      *     ArrCampi_ON e ArrCampi_NC sono definti nell'area manuale Header
      ON ERROR &L_OldErr
      if L_Err
        * --- Non fa nulla, ignora la propriet�
        L_Err = .F.
      else
        do case
          case MCNOMTAB="OFF_NOMI"
            * --- Campo di OFF_NOMI
             
 Dimension ArrCampi_ON[ this.w_CntFields_ON , 2 ] 
 ArrCampi_ON[ this.w_CntFields_ON ,1 ] = w_TmpCampo 
 ArrCampi_ON[ this.w_CntFields_ON ,2 ] = VarTemporanea
            this.w_CntFields_ON = this.w_CntFields_ON + 1
          case MCNOMTAB="NOM_CONT"
            * --- Campo di NOM_CONT
            *     Potebbe anche non essere utilizzato
             
 Dimension ArrCampi_NC[ this.w_CntFields_NC , 2 ] 
 ArrCampi_NC[ this.w_CntFields_NC ,1 ] = w_TmpCampo 
 ArrCampi_NC[ this.w_CntFields_NC ,2 ] = VarTemporanea
            this.w_CntFields_NC = this.w_CntFields_NC + 1
          case MCNOMTAB="NOM_ATTR"
            * --- Campo di NOM_ATTR
            *     Potebbe anche non essere utilizzato
             
 Dimension ArrCampi_NA[ this.w_CntFields_NA , 2 ] 
 ArrCampi_NA[ this.w_CntFields_NA ,1 ] = w_TmpCampo 
 ArrCampi_NA[ this.w_CntFields_NA ,2 ] = VarTemporanea
            this.w_CntFields_NA = this.w_CntFields_NA + 1
        endcase
      endif
    endif
    ENDSCAN
  endproc


  procedure Page_18
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorniamo solo le propriet� mappate, sempre utilizzando il cursore MappaExport
    SELECT MappaExport
    GO TOP
    SCAN
    w_TmpCampo2 = ALLTRIM(MCMSOUTL)
    w_TmpVaria2 = "this.oContatto."+w_TmpCampo2 
    if MCTIPEXP="S" AND !EMPTY(MCEXPRES)
      w_TmpCampo = ALLTRIM(STRTRAN(MCEXPRES,".","RAGGR."))
    else
      w_TmpCampo = "RAGGR."+MCGESTIO
    endif
    * --- Si valuta l'espressione ed eventualmente il contatto assume il valore della valutazione
    L_Err = .F.
    * --- Evitiamo la segnalazione d'errore
    ON ERROR L_Err=.T.
    if !ISNULL( &w_TmpCampo ) AND !EMPTY( &w_TmpCampo )
      w_Var_Appoggio= &w_TmpCampo
      * --- Nel caso in cui si tratti di una UserProperties, prima dobbiamo aggiungerla
      if UPPER(LEFT(w_TmpCampo2,15))=="USERPROPERTIES(" AND UPPER(RIGHT(w_TmpCampo2,6))==".VALUE"
        * --- La UserProperties non esite nel contatto
        *     La creiamo di tipo testo
        this.w_NomeProp = SUBSTR(w_TmpCampo2, AT("('",w_TmpCampo2)+2, AT("')",w_TmpCampo2)-AT("('",w_TmpCampo2)-2)
        if ISNULL( this.oContatto.UserProperties(this.w_NomeProp) )
          this.oContatto.UserProperties.Add(this.w_NomeProp, olText)     
        endif
      endif
      if VARTYPE(w_Var_Appoggio)="C"
        * --- Campo di tipo carattere, facciamo la ALLTRIM
        &w_TmpVaria2 = ALLTRIM(w_Var_Appoggio)
      else
        &w_TmpVaria2 = w_Var_Appoggio
      endif
    endif
    ON ERROR &L_OldErr
    ENDSCAN
    * --- Ci riposizioniamo sul cursore di esportazione dati
    SELECT RAGGR
  endproc


  procedure Page_19
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Facciamo la prima ricerca utilizzando il campo specificato nei parametri
    this.w_CodiceNominativo = ""
    * --- Non impostiamo L_Err perch� eventualmente � gi� stato impostato a pagina 9
    * --- Try
    local bErr_04F56FC0
    bErr_04F56FC0=bTrsErr
    this.Try_04F56FC0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      AddMsgNL("Errore in esecuzione query dinamica",this)
      AddMsgNL("Errore: %1",this,Message())
      this.Errore = .t.
      exit
    endif
    bTrsErr=bTrsErr or bErr_04F56FC0
    * --- End
    this.w_OK = .F.
    if !L_Err
      * --- Dobbiamo controllare se la persona ha valorizzato almeno uno dei campi di mappatura su NOM_CONT
      ON ERROR L_Err=.T.
      FOR cnt=1 TO ALEN(ArrChiavi_NC, 1)
      w_TmpCampo = ALLTRIM( ArrChiavi_NC(Cnt , 1) )
      w_TmpVaria = "w_"+w_TmpCampo 
      w_TmpVaria2 = &w_TmpVaria
      ON ERROR &L_OldErr
      if L_Err
        ah_ErrorMsg("Attenzione! Non � stato possibile valutare l'espressione chiave!")
        EXIT
      else
        if !EMPTY(w_TmpVaria2)
          this.w_OK = .T.
          EXIT
        endif
      endif
      ENDFOR
      if this.w_OK
        * --- Gestione NOM_CONT
        if EMPTY(this.w_CodiceNominativo)
          * --- Persona di societ� ma manca la societ�
          * --- Procede con l'inserimento
          this.Page_14()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Page_21()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if EMPTY(this.w_CodiceNominativo)
          this.w_ResMsg.AddMsgPartNL(ah_MsgFormat("Per il contatto %1 manca la societ� %2",this.oContatto.FullName, this.oContatto.CompanyName))     
        else
          * --- Persona o societ�, procede con l'inserimento
          this.i_qry=createobject("cpquery")
          this.i_qry.mLoadFile("Contatti","NOM_CONT","NOM_CONT")     
          this.i_qry.mLoadField("MIN(NCCODCON)","NCCODCON","C",10,0)     
          this.i_qry.mLoadWhere("NOM_CONT.NCCODICE = "+cp_ToStrODBC(this.w_CodiceNominativo)+" AND ",".f.",.f.)     
          * --- Scandiamo l'array delle propriet� per comporre la frase di ricerca
          FOR cnt=1 TO ALEN(ArrChiavi_NC, 1)
          w_TmpCampo = ALLTRIM( ArrChiavi_NC(Cnt , 1) )
          w_TmpVaria = "w_"+w_TmpCampo 
          w_TmpVaria2 = &w_TmpVaria
          if !EMPTY(w_TmpVaria2)
            if ArrChiavi_NC(Cnt , 2)="C"
              this.i_qry.mLoadWhere("[TRIM(NOM_CONT."+w_TmpCampo+")] like [TRIM("+cp_ToStrODBC(w_TmpVaria2)+")] AND ",".f.",.f.)     
            else
              this.i_qry.mLoadWhere("NOM_CONT."+w_TmpCampo+" = "+cp_ToStrODBC(w_TmpVaria2)+" AND ",".f.",.f.)     
            endif
          endif
          ENDFOR
          this.i_qry.mDoQuery("CursQUERY","Exec",.F.,.F.,.F.,.F.,.F.)     
          if Used("CursQUERY")
            Select CursQUERY
            this.w_CodiceContatto = NVL(NCCODCON,SPACE(5))
          endif
          USE IN SELECT("CursQUERY")
          this.i_qry = .NULL.
          if EMPTY(this.w_CodiceContatto)
            this.Page_15()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.Page_3()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        * --- Gestione OFF_NOMI
        if EMPTY(this.w_CodiceNominativo)
          * --- Procede con l'inserimento
          this.Page_14()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Page_21()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
  endproc
  proc Try_04F56FC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.i_qry=createobject("cpquery")
    this.i_qry.mLoadFile("Nominativi","OFF_NOMI","OFF_NOMI")     
    this.i_qry.mLoadField("MIN(NOCODICE)","NOCODICE","C",10,0)     
    * --- Scandiamo l'array delle propriet� per comporre la frase di ricerca
    FOR cnt=1 TO ALEN(ArrChiavi_ON, 1)
    ON ERROR L_Err=.T.
    w_TmpCampo = ALLTRIM( ArrChiavi_ON(Cnt , 1) )
    w_TmpVaria = "w_"+w_TmpCampo 
    w_TmpVaria2 = &w_TmpVaria
    ON ERROR &L_OldErr
    if L_Err
      ah_ErrorMsg("Attenzione! Non � stato possibile valutare l'espressione chiave!")
      EXIT
    else
      if !EMPTY(w_TmpVaria2)
        if ArrChiavi_ON(Cnt , 2)="C"
          this.i_qry.mLoadWhere("[TRIM(OFF_NOMI."+w_TmpCampo+")] like [TRIM("+cp_ToStrODBC(w_TmpVaria2)+")] AND ",".f.",.f.)     
        else
          this.i_qry.mLoadWhere("OFF_NOMI."+w_TmpCampo+" = "+cp_ToStrODBC(w_TmpVaria2)+" AND ",".f.",.f.)     
        endif
      endif
    endif
    ENDFOR
    if L_Err
      * --- Errore nella valutazione delle chiavi di import
      * --- Al termine del esportazion riattiva il decorator dei form 
      DecoratorState ("A")
      i_retcode = 'stop'
      return
    endif
    this.i_qry.mDoQuery("CursQUERY","Exec",.F.,.F.,.F.,.F.,.F.)     
    if Used("CursQUERY")
      Select CursQUERY
      this.w_CodiceNominativo = NVL(NOCODICE,"")
    endif
    USE IN SELECT("CursQUERY")
    this.i_qry = .NULL.
    return


  procedure Page_20
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Filtro XDC_Fields
  endproc


  procedure Page_21
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiornamento NOM_ATTR
    this.w_StrCategories = ArrCampi_NA[1,2 ]
    if !EMPTY(this.w_StrCategories) AND !EMPTY(this.w_TipCatDefault)
      * --- E' presente una categoria nel contatto - va gestita negli attributi
      *     Supponiamo che il campo contenga la DESCRIZIONE, e da questa dobbiamo ottenere le descrizioni delle categorie.
      * --- Nella variabile w_NewAttr memorizziamo se il nominativo � nuovo oppure no
      do while LEN(this.w_StrCategories)>0
        this.w_NewCat = "N"
        this.w_CTCODICE = ""
        * --- Abbiamo una descrizione di categoria da analizzare
        if AT(";", this.w_StrCategories)>0
          this.w_DesCategoria = ALLTRIM(LEFT(this.w_StrCategories, AT(";", this.w_StrCategories) - 1))
          this.w_StrCategories = ALLTRIM(SUBSTR(this.w_StrCategories, AT(";", this.w_StrCategories)+1))
        else
          this.w_DesCategoria = this.w_StrCategories
          this.w_StrCategories = ""
        endif
        * --- Cerchiamo una categoria che abbia per descrizione quella riportata nel contatto
        this.w_InsCateg = "S"
        * --- Read from CAT_ATTR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CAT_ATTR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CAT_ATTR_idx,2],.t.,this.CAT_ATTR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "CTCODICE"+;
            " from "+i_cTable+" CAT_ATTR where ";
                +"CTDESCRI = "+cp_ToStrODBC(this.w_DesCategoria);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            CTCODICE;
            from (i_cTable) where;
                CTDESCRI = this.w_DesCategoria;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CTCODICE = NVL(cp_ToDate(_read_.CTCODICE),cp_NullValue(_read_.CTCODICE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_NumCat = 0
        do while EMPTY(this.w_CTCODICE) AND this.w_NumCat<=9999999999
          * --- Non esite negli archivi una categoria che corrisponde alla descrizione della categoria di Outlook
          *     oppure il codice testato � gi� presente
          this.w_NumCat = this.w_NumCat+1
          this.w_NewCat = "S"
          this.w_CTCODICE = PADL(ALLTRIM(STR(this.w_NumCat)),10,"0")
          this.w_CtrlCTCODICE = ""
          * --- Read from CAT_ATTR
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAT_ATTR_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAT_ATTR_idx,2],.t.,this.CAT_ATTR_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CTCODICE"+;
              " from "+i_cTable+" CAT_ATTR where ";
                  +"CTCODICE = "+cp_ToStrODBC(this.w_CTCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CTCODICE;
              from (i_cTable) where;
                  CTCODICE = this.w_CTCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CtrlCTCODICE = NVL(cp_ToDate(_read_.CTCODICE),cp_NullValue(_read_.CTCODICE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if !EMPTY(this.w_CtrlCTCODICE)
            * --- La categoria attributo esisteva gi�
            this.w_CTCODICE = ""
          else
            * --- Inserisce la categoria
            this.w_InsCateg = "S"
            * --- Insert into CAT_ATTR
            i_nConn=i_TableProp[this.CAT_ATTR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAT_ATTR_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CAT_ATTR_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"CTCODICE"+",CTDESCRI"+",CTTIPCAT"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_CTCODICE),'CAT_ATTR','CTCODICE');
              +","+cp_NullLink(cp_ToStrODBC(this.w_DesCategoria),'CAT_ATTR','CTDESCRI');
              +","+cp_NullLink(cp_ToStrODBC(this.w_TipCatDefault),'CAT_ATTR','CTTIPCAT');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'CTCODICE',this.w_CTCODICE,'CTDESCRI',this.w_DesCategoria,'CTTIPCAT',this.w_TipCatDefault)
              insert into (i_cTable) (CTCODICE,CTDESCRI,CTTIPCAT &i_ccchkf. );
                 values (;
                   this.w_CTCODICE;
                   ,this.w_DesCategoria;
                   ,this.w_TipCatDefault;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
          endif
        enddo
        if !EMPTY(this.w_CTCODICE)
          * --- Esiste un codice - pu� procedere
          if this.w_NewCat<>"S"
            * --- La categoria era presente e pertanto potremmo avere degli attributi del nominativo
            * --- Read from NOM_ATTR
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.NOM_ATTR_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.NOM_ATTR_idx,2],.t.,this.NOM_ATTR_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "ANCODCAT"+;
                " from "+i_cTable+" NOM_ATTR where ";
                    +"ANCODICE = "+cp_ToStrODBC(this.w_CodiceNominativo);
                    +" and ANCODCAT = "+cp_ToStrODBC(this.w_CTCODICE);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                ANCODCAT;
                from (i_cTable) where;
                    ANCODICE = this.w_CodiceNominativo;
                    and ANCODCAT = this.w_CTCODICE;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CtrlCTCODICE = NVL(cp_ToDate(_read_.ANCODCAT),cp_NullValue(_read_.ANCODCAT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if !EMPTY(this.w_CtrlCTCODICE)
              this.w_InsCateg = "N"
            endif
          endif
          * --- w_InsCateg='N' quando w_NewCat='N' (categoria gi� presente) e record presente in NOM_ATTR
          if this.w_InsCateg="S"
            * --- Try
            local bErr_04F5F990
            bErr_04F5F990=bTrsErr
            this.Try_04F5F990()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_04F5F990
            * --- End
          endif
        endif
      enddo
    endif
  endproc
  proc Try_04F5F990()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into NOM_ATTR
    i_nConn=i_TableProp[this.NOM_ATTR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.NOM_ATTR_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.NOM_ATTR_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"ANCODICE"+",ANCODCAT"+",ANDESCAT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CodiceNominativo),'NOM_ATTR','ANCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CTCODICE),'NOM_ATTR','ANCODCAT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DesCategoria),'NOM_ATTR','ANDESCAT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'ANCODICE',this.w_CodiceNominativo,'ANCODCAT',this.w_CTCODICE,'ANDESCAT',this.w_DesCategoria)
      insert into (i_cTable) (ANCODICE,ANCODCAT,ANDESCAT &i_ccchkf. );
         values (;
           this.w_CodiceNominativo;
           ,this.w_CTCODICE;
           ,this.w_DesCategoria;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pPARA)
    this.pPARA=pPARA
    this.w_ResMsg=createobject("Ah_Message")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='OFF_NOMI'
    this.cWorkTables[2]='NOM_CONT'
    this.cWorkTables[3]='RUO_CONT'
    this.cWorkTables[4]='AGENTI'
    this.cWorkTables[5]='XDC_FIELDS'
    this.cWorkTables[6]='MAP_SINC'
    this.cWorkTables[7]='CAT_ATTR'
    this.cWorkTables[8]='NOM_ATTR'
    this.cWorkTables[9]='TIP_CATT'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_TIP_CATT')
      use in _Curs_TIP_CATT
    endif
    if used('_Curs_NOM_CONT')
      use in _Curs_NOM_CONT
    endif
    if used('_Curs_NOM_CONT')
      use in _Curs_NOM_CONT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARA"
endproc
