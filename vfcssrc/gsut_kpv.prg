* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kpv                                                        *
*              Visualizzazione politiche di sicurezza utenti                   *
*                                                                              *
*      Author: Zucchetti S.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_45]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-06-10                                                      *
* Last revis.: 2012-11-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut_kpv
*Questa gestione la pu� usare solo l'amministratore
if not cp_IsAdministrator(.t.)
  AH_ERRORMSG("Accesso negato",'stop',"Gestione sicurezza")
  return null
endif
* --- Fine Area Manuale
return(createobject("tgsut_kpv",oParentObject))

* --- Class definition
define class tgsut_kpv as StdForm
  Top    = 2
  Left   = 2

  * --- Standard Properties
  Width  = 776
  Height = 429
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-06"
  HelpContextID=149504873
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Constant Properties
  _IDX = 0
  CPUSRGRP_IDX = 0
  CPUSERS_IDX = 0
  POL_SIC_IDX = 0
  POL_UTE_IDX = 0
  cPrg = "gsut_kpv"
  cComment = "Visualizzazione politiche di sicurezza utenti"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_AHE = space(10)
  w_VALMAX = 0
  w_ULTACCINI = ctod('  /  /  ')
  w_ULTACCFIN = ctod('  /  /  ')
  w_UTENTE = 0
  o_UTENTE = 0
  w_ULTAGGINI = ctod('  /  /  ')
  w_ULTAGGFIN = ctod('  /  /  ')
  w_MOTBLO = space(1)
  w_BLOCCO = space(1)
  w_PWDSCAINI = ctod('  /  /  ')
  o_PWDSCAINI = ctod('  /  /  ')
  w_PWDSCAFIN = ctod('  /  /  ')
  o_PWDSCAFIN = ctod('  /  /  ')
  w_PWDSCAINI1 = ctod('  /  /  ')
  w_PWDSCAFIN1 = ctod('  /  /  ')
  w_SELEZ = space(1)
  w_UTENTE1 = 0
  w_UTEDESC = space(20)
  w_VALAM = 0
  w_FLGAM = space(1)
  w_ZOOM = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_kpv
  * --- DB2 va in errore eseguendo la query
  * --- dello zoom se queste due date sono vuote
  w_PWDSCAINI1= i_INIDAT
  w_PWDSCAFIN1= i_FINDAT
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kpvPag1","gsut_kpv",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oULTACCINI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOM = this.oPgFrm.Pages(1).oPag.ZOOM
    DoDefault()
    proc Destroy()
      this.w_ZOOM = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CPUSRGRP'
    this.cWorkTables[2]='CPUSERS'
    this.cWorkTables[3]='POL_SIC'
    this.cWorkTables[4]='POL_UTE'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AHE=space(10)
      .w_VALMAX=0
      .w_ULTACCINI=ctod("  /  /  ")
      .w_ULTACCFIN=ctod("  /  /  ")
      .w_UTENTE=0
      .w_ULTAGGINI=ctod("  /  /  ")
      .w_ULTAGGFIN=ctod("  /  /  ")
      .w_MOTBLO=space(1)
      .w_BLOCCO=space(1)
      .w_PWDSCAINI=ctod("  /  /  ")
      .w_PWDSCAFIN=ctod("  /  /  ")
      .w_PWDSCAINI1=ctod("  /  /  ")
      .w_PWDSCAFIN1=ctod("  /  /  ")
      .w_SELEZ=space(1)
      .w_UTENTE1=0
      .w_UTEDESC=space(20)
      .w_VALAM=0
      .w_FLGAM=space(1)
        .w_AHE = 'AHE'
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_AHE))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,5,.f.)
        if not(empty(.w_UTENTE))
          .link_1_5('Full')
        endif
          .DoRTCalc(6,7,.f.)
        .w_MOTBLO = 'T'
        .w_BLOCCO = 'T'
          .DoRTCalc(10,11,.f.)
        .w_PWDSCAINI1 = IIF( Empty( .w_PWDSCAINI ) , i_INIDAT  , .w_PWDSCAINI )
        .w_PWDSCAFIN1 = IIF( Empty( .w_PWDSCAFIN ) , i_FINDAT  , .w_PWDSCAFIN )
      .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .w_SELEZ = 'N'
      .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .w_UTENTE1 = .w_UTENTE
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_UTENTE1))
          .link_1_29('Full')
        endif
      .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
    endwith
    this.DoRTCalc(16,18,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_19.enabled = this.oPgFrm.Page1.oPag.oBtn_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,11,.t.)
        if .o_PWDSCAINI<>.w_PWDSCAINI
            .w_PWDSCAINI1 = IIF( Empty( .w_PWDSCAINI ) , i_INIDAT  , .w_PWDSCAINI )
        endif
        if .o_PWDSCAFIN<>.w_PWDSCAFIN
            .w_PWDSCAFIN1 = IIF( Empty( .w_PWDSCAFIN ) , i_FINDAT  , .w_PWDSCAFIN )
        endif
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .DoRTCalc(14,14,.t.)
        if .o_UTENTE<>.w_UTENTE
            .w_UTENTE1 = .w_UTENTE
          .link_1_29('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(16,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOM.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_18.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_21.visible=!this.oPgFrm.Page1.oPag.oBtn_1_21.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOM.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_18.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AHE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.POL_SIC_IDX,3]
    i_lTable = "POL_SIC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2], .t., this.POL_SIC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AHE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AHE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSSERIAL,PSVALMAX,PSVALMAA,PS__AMMI";
                   +" from "+i_cTable+" "+i_lTable+" where PSSERIAL="+cp_ToStrODBC(this.w_AHE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSSERIAL',this.w_AHE)
            select PSSERIAL,PSVALMAX,PSVALMAA,PS__AMMI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AHE = NVL(_Link_.PSSERIAL,space(10))
      this.w_VALMAX = NVL(_Link_.PSVALMAX,0)
      this.w_VALAM = NVL(_Link_.PSVALMAA,0)
      this.w_FLGAM = NVL(_Link_.PS__AMMI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AHE = space(10)
      endif
      this.w_VALMAX = 0
      this.w_VALAM = 0
      this.w_FLGAM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.POL_SIC_IDX,2])+'\'+cp_ToStr(_Link_.PSSERIAL,1)
      cp_ShowWarn(i_cKey,this.POL_SIC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AHE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UTENTE
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.POL_UTE_IDX,3]
    i_lTable = "POL_UTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.POL_UTE_IDX,2], .t., this.POL_UTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.POL_UTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTENTE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_APU',True,'POL_UTE')
        if i_nConn<>0
          i_cWhere = " PUCODUTE="+cp_ToStrODBC(this.w_UTENTE);

          i_ret=cp_SQL(i_nConn,"select PUCODUTE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PUCODUTE',this.w_UTENTE)
          select PUCODUTE;
              from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
          i_reccount = _tally
        endif
        if i_reccount>1
          if !empty(this.w_UTENTE) and !this.bDontReportError
            deferred_cp_zoom('POL_UTE','*','PUCODUTE',cp_AbsName(oSource.parent,'oUTENTE_1_5'),i_cWhere,'GSUT_APU',"Utenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PUCODUTE";
                     +" from "+i_cTable+" "+i_lTable+" where PUCODUTE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PUCODUTE',oSource.xKey(1))
            select PUCODUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTENTE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PUCODUTE";
                   +" from "+i_cTable+" "+i_lTable+" where PUCODUTE="+cp_ToStrODBC(this.w_UTENTE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PUCODUTE',this.w_UTENTE)
            select PUCODUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTENTE = NVL(_Link_.PUCODUTE,0)
    else
      if i_cCtrl<>'Load'
        this.w_UTENTE = 0
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.POL_UTE_IDX,2])+'\'+cp_ToStr(_Link_.PUCODUTE,1)
      cp_ShowWarn(i_cKey,this.POL_UTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTENTE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=UTENTE1
  func Link_1_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CPUSERS_IDX,3]
    i_lTable = "CPUSERS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2], .t., this.CPUSERS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UTENTE1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UTENTE1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CODE,NAME";
                   +" from "+i_cTable+" "+i_lTable+" where CODE="+cp_ToStrODBC(this.w_UTENTE1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CODE',this.w_UTENTE1)
            select CODE,NAME;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UTENTE1 = NVL(_Link_.CODE,0)
      this.w_UTEDESC = NVL(_Link_.NAME,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_UTENTE1 = 0
      endif
      this.w_UTEDESC = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CPUSERS_IDX,2])+'\'+cp_ToStr(_Link_.CODE,1)
      cp_ShowWarn(i_cKey,this.CPUSERS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UTENTE1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oULTACCINI_1_3.value==this.w_ULTACCINI)
      this.oPgFrm.Page1.oPag.oULTACCINI_1_3.value=this.w_ULTACCINI
    endif
    if not(this.oPgFrm.Page1.oPag.oULTACCFIN_1_4.value==this.w_ULTACCFIN)
      this.oPgFrm.Page1.oPag.oULTACCFIN_1_4.value=this.w_ULTACCFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oUTENTE_1_5.value==this.w_UTENTE)
      this.oPgFrm.Page1.oPag.oUTENTE_1_5.value=this.w_UTENTE
    endif
    if not(this.oPgFrm.Page1.oPag.oULTAGGINI_1_6.value==this.w_ULTAGGINI)
      this.oPgFrm.Page1.oPag.oULTAGGINI_1_6.value=this.w_ULTAGGINI
    endif
    if not(this.oPgFrm.Page1.oPag.oULTAGGFIN_1_7.value==this.w_ULTAGGFIN)
      this.oPgFrm.Page1.oPag.oULTAGGFIN_1_7.value=this.w_ULTAGGFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oMOTBLO_1_8.RadioValue()==this.w_MOTBLO)
      this.oPgFrm.Page1.oPag.oMOTBLO_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBLOCCO_1_9.RadioValue()==this.w_BLOCCO)
      this.oPgFrm.Page1.oPag.oBLOCCO_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPWDSCAINI_1_10.value==this.w_PWDSCAINI)
      this.oPgFrm.Page1.oPag.oPWDSCAINI_1_10.value=this.w_PWDSCAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oPWDSCAFIN_1_11.value==this.w_PWDSCAFIN)
      this.oPgFrm.Page1.oPag.oPWDSCAFIN_1_11.value=this.w_PWDSCAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZ_1_16.RadioValue()==this.w_SELEZ)
      this.oPgFrm.Page1.oPag.oSELEZ_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oUTEDESC_1_30.value==this.w_UTEDESC)
      this.oPgFrm.Page1.oPag.oUTEDESC_1_30.value=this.w_UTEDESC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_UTENTE = this.w_UTENTE
    this.o_PWDSCAINI = this.w_PWDSCAINI
    this.o_PWDSCAFIN = this.w_PWDSCAFIN
    return

enddefine

* --- Define pages as container
define class tgsut_kpvPag1 as StdContainer
  Width  = 772
  height = 429
  stdWidth  = 772
  stdheight = 429
  resizeXpos=315
  resizeYpos=274
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oULTACCINI_1_3 as StdField with uid="LAPNBYGZQT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ULTACCINI", cQueryName = "ULTACCINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Da data ultimo accesso",;
    HelpContextID = 24292572,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=134, Top=10

  add object oULTACCFIN_1_4 as StdField with uid="FUNKDBFMUT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ULTACCFIN", cQueryName = "ULTACCFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "A data ultimo accesso",;
    HelpContextID = 244142959,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=232, Top=10

  add object oUTENTE_1_5 as StdField with uid="ULJPHHNEXV",rtseq=5,rtrep=.f.,;
    cFormVar = "w_UTENTE", cQueryName = "UTENTE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Codice utente",;
    HelpContextID = 27878982,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=426, Top=10, cSayPict='"999"', cGetPict='"999"', bHasZoom = .t. , cLinkFile="POL_UTE", cZoomOnZoom="GSUT_APU", oKey_1_1="PUCODUTE", oKey_1_2="this.w_UTENTE"

  func oUTENTE_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oUTENTE_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oUTENTE_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'POL_UTE','*','PUCODUTE',cp_AbsName(this.parent,'oUTENTE_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_APU',"Utenti",'',this.parent.oContained
  endproc
  proc oUTENTE_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSUT_APU()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PUCODUTE=this.parent.oContained.w_UTENTE
     i_obj.ecpSave()
  endproc

  add object oULTAGGINI_1_6 as StdField with uid="QEGLBHANTY",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ULTAGGINI", cQueryName = "ULTAGGINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Da data ultimo aggiornamento",;
    HelpContextID = 221424860,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=134, Top=37

  add object oULTAGGFIN_1_7 as StdField with uid="MDOFDLMOMR",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ULTAGGFIN", cQueryName = "ULTAGGFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "A data ultimo aggiornamento",;
    HelpContextID = 221424785,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=232, Top=37


  add object oMOTBLO_1_8 as StdCombo with uid="WDOSMAIQGU",rtseq=8,rtrep=.f.,left=426,top=40,width=156,height=21;
    , ToolTipText = "Motivo blocco";
    , HelpContextID = 186536134;
    , cFormVar="w_MOTBLO",RowSource=""+"Scaduta password,"+"Scaduto account,"+"Num. max tentativi,"+"Blocco manuale,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMOTBLO_1_8.RadioValue()
    return(iif(this.value =1,'P',;
    iif(this.value =2,'A',;
    iif(this.value =3,'S',;
    iif(this.value =4,'B',;
    iif(this.value =5,'T',;
    space(1)))))))
  endfunc
  func oMOTBLO_1_8.GetRadio()
    this.Parent.oContained.w_MOTBLO = this.RadioValue()
    return .t.
  endfunc

  func oMOTBLO_1_8.SetRadio()
    this.Parent.oContained.w_MOTBLO=trim(this.Parent.oContained.w_MOTBLO)
    this.value = ;
      iif(this.Parent.oContained.w_MOTBLO=='P',1,;
      iif(this.Parent.oContained.w_MOTBLO=='A',2,;
      iif(this.Parent.oContained.w_MOTBLO=='S',3,;
      iif(this.Parent.oContained.w_MOTBLO=='B',4,;
      iif(this.Parent.oContained.w_MOTBLO=='T',5,;
      0)))))
  endfunc


  add object oBLOCCO_1_9 as StdCombo with uid="AEKXDWIHLR",rtseq=9,rtrep=.f.,left=645,top=40,width=119,height=21;
    , ToolTipText = "Stato blocco utente";
    , HelpContextID = 177143062;
    , cFormVar="w_BLOCCO",RowSource=""+"Attivo,"+"Disattivo,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oBLOCCO_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    iif(this.value =3,'T',;
    space(1)))))
  endfunc
  func oBLOCCO_1_9.GetRadio()
    this.Parent.oContained.w_BLOCCO = this.RadioValue()
    return .t.
  endfunc

  func oBLOCCO_1_9.SetRadio()
    this.Parent.oContained.w_BLOCCO=trim(this.Parent.oContained.w_BLOCCO)
    this.value = ;
      iif(this.Parent.oContained.w_BLOCCO=='S',1,;
      iif(this.Parent.oContained.w_BLOCCO=='N',2,;
      iif(this.Parent.oContained.w_BLOCCO=='T',3,;
      0)))
  endfunc

  add object oPWDSCAINI_1_10 as StdField with uid="DIAYSOIFWN",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PWDSCAINI", cQueryName = "PWDSCAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Da data scadenza password",;
    HelpContextID = 56730156,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=134, Top=64

  add object oPWDSCAFIN_1_11 as StdField with uid="DNKKDMPCXL",rtseq=11,rtrep=.f.,;
    cFormVar = "w_PWDSCAFIN", cQueryName = "PWDSCAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "A data scadenza password",;
    HelpContextID = 211705375,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=232, Top=64


  add object oBtn_1_14 as StdButton with uid="PSTUCDPWKC",left=714, top=66, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Ricerca";
    , HelpContextID = 27417366;
    , caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSUT_BPV(this.Parent.oContained, "ZOOM" )
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZOOM as cp_szoombox with uid="GRLFTMBZCT",left=11, top=109, width=750,height=267,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="POL_UTE",cZoomFile="GSUT_KPV",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,;
    cEvent = "Aggiorna",;
    nPag=1;
    , HelpContextID = 28492774

  add object oSELEZ_1_16 as StdRadio with uid="TGWCCTDDOX",rtseq=14,rtrep=.f.,left=13, top=381, width=135,height=35;
    , ToolTipText = "Seleziona/deseleziona utenti";
    , cFormVar="w_SELEZ", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZ_1_16.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 50280666
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 50280666
      this.Buttons(2).Top=16
      this.SetAll("Width",133)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona utenti")
      StdRadio::init()
    endproc

  func oSELEZ_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oSELEZ_1_16.GetRadio()
    this.Parent.oContained.w_SELEZ = this.RadioValue()
    return .t.
  endfunc

  func oSELEZ_1_16.SetRadio()
    this.Parent.oContained.w_SELEZ=trim(this.Parent.oContained.w_SELEZ)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZ=='S',1,;
      iif(this.Parent.oContained.w_SELEZ=='N',2,;
      0))
  endfunc


  add object oBtn_1_17 as StdButton with uid="VXMFGCGPUI",left=175, top=381, width=48,height=45,;
    CpPicture="bmp\Dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aprire l'anagrafica politica utenti";
    , HelpContextID = 149504778;
    , caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSUT_BPV(this.Parent.oContained,"DETTAGLI")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_18 as cp_runprogram with uid="YYBIRJJWWX",left=4, top=448, width=273,height=22,;
    caption='GSUT_BPV(SELEZ)',;
   bGlobalFont=.t.,;
    prg="GSUT_BPV('SELEZ')",;
    cEvent = "w_SELEZ Changed",;
    nPag=1;
    , HelpContextID = 234949180


  add object oBtn_1_19 as StdButton with uid="NBNZLVVZGS",left=605, top=381, width=48,height=45,;
    CpPicture="bmp\sblocca.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per sbloccare gli utenti selezionati";
    , HelpContextID = 149504778;
    , caption='\<Sblocca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_19.Click()
      with this.Parent.oContained
        GSUT_BPV(this.Parent.oContained,"SBLOCCA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_20 as StdButton with uid="XKAZGVJAOE",left=658, top=381, width=48,height=45,;
    CpPicture="bmp\blocca.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per bloccare gli utenti selezionati";
    , HelpContextID = 149504778;
    , caption='\<Blocca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        GSUT_BPV(this.Parent.oContained,"BLOCCA")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_21 as StdButton with uid="OFLKLPHFIY",left=711, top=381, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 149504778;
    , caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
     endwith
    endif
  endfunc

  add object oUTEDESC_1_30 as StdField with uid="NLJNBJGMCM",rtseq=16,rtrep=.f.,;
    cFormVar = "w_UTEDESC", cQueryName = "UTEDESC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione utente",;
    HelpContextID = 246376006,;
   bGlobalFont=.t.,;
    Height=21, Width=284, Left=480, Top=10, InputMask=replicate('X',20)


  add object oObj_1_33 as cp_runprogram with uid="VWERZMRXGJ",left=281, top=448, width=273,height=22,;
    caption='GSUT_BPV(DETTAGLI)',;
   bGlobalFont=.t.,;
    prg="GSUT_BPV('DETTAGLI')",;
    cEvent = "w_zoom selected",;
    nPag=1;
    , ToolTipText = "Apre anagrafica politiche utente";
    , HelpContextID = 168220392


  add object oObj_1_34 as cp_runprogram with uid="EUSJWSDGNU",left=559, top=448, width=177,height=22,;
    caption='GSUT_BPV(DONE)',;
   bGlobalFont=.t.,;
    prg="GSUT_BPV('DONE')",;
    cEvent = "Done",;
    nPag=1;
    , ToolTipText = "Rimuove eventuale temporaneo per zoom";
    , HelpContextID = 218340156


  add object oObj_1_35 as cp_runprogram with uid="DIMJAGMWOK",left=4, top=471, width=177,height=22,;
    caption='GSUT_BPV(ZOOM)',;
   bGlobalFont=.t.,;
    prg="GSUT_BPV('ZOOM')",;
    cEvent = "Zoom",;
    nPag=1;
    , ToolTipText = "Aggiorna lo zoom";
    , HelpContextID = 226799932

  add object oStr_1_22 as StdString with uid="TPTILBUHWC",Visible=.t., Left=208, Top=14,;
    Alignment=1, Width=23, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="TKXQGOTIKH",Visible=.t., Left=208, Top=41,;
    Alignment=1, Width=23, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="IOFQRFLXOX",Visible=.t., Left=208, Top=68,;
    Alignment=1, Width=23, Height=18,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="CQVOFEEBZQ",Visible=.t., Left=43, Top=14,;
    Alignment=1, Width=89, Height=18,;
    Caption="Ultimo accesso:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="VIQMWMUFWN",Visible=.t., Left=6, Top=41,;
    Alignment=1, Width=126, Height=18,;
    Caption="Ultimo aggiornamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="WEHVLKLMTM",Visible=.t., Left=16, Top=68,;
    Alignment=1, Width=116, Height=18,;
    Caption="Scadenza password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="PMOUBFQVBE",Visible=.t., Left=373, Top=14,;
    Alignment=1, Width=52, Height=18,;
    Caption="Utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="UCYHHZWETN",Visible=.t., Left=311, Top=41,;
    Alignment=1, Width=114, Height=18,;
    Caption="Motivo blocco:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="AHLABXWZIF",Visible=.t., Left=585, Top=41,;
    Alignment=1, Width=56, Height=18,;
    Caption="Blocco:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kpv','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
