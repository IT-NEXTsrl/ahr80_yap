* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bac                                                        *
*              Gestione conf articolo                                          *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-09                                                      *
* Last revis.: 2015-07-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_OPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bac",oParentObject,m.w_OPER)
return(i_retval)

define class tgsma_bac as StdBatch
  * --- Local variables
  w_OPER = space(10)
  GSMA_MAC = .NULL.
  GSMA_MAD = .NULL.
  GSMA_AMP = .NULL.
  w_GESCAR = space(1)
  w_ARCONCAR = space(1)
  * --- WorkFile variables
  CONF_ART_idx=0
  CONF_CAR_idx=0
  CONFDCAR_idx=0
  ART_ICOL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Detail Caratt. di Conf. Articolo (GSMA_MAC)
    this.GSMA_MAC = this.oParentObject
    this.GSMA_MAD = this.GSMA_MAC.GSMA_MAD
    this.GSMA_AMP = this.GSMA_MAC.GSMA_AMP
    cCurMAD = this.GSMA_MAD.cTrsname
    if this.w_OPER="UPD_CCAR"
      * --- Aggiorna flag Art. gestito a Caratteristiche - campo CAGESCAR di KEY_ARTI
      this.w_ARCONCAR = this.GSMA_MAC.oParentObject.w_ARCONCAR
      this.w_GESCAR = "N"
      * --- Select from CONF_ART
      i_nConn=i_TableProp[this.CONF_ART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONF_ART_idx,2],.t.,this.CONF_ART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CCCODICE  from "+i_cTable+" CONF_ART ";
            +" where CCCODART="+cp_ToStrODBC(this.oParentObject.w_CCCODART)+"";
             ,"_Curs_CONF_ART")
      else
        select CCCODICE from (i_cTable);
         where CCCODART=this.oParentObject.w_CCCODART;
          into cursor _Curs_CONF_ART
      endif
      if used('_Curs_CONF_ART')
        select _Curs_CONF_ART
        locate for 1=1
        do while not(eof())
        this.w_GESCAR = "S"
          select _Curs_CONF_ART
          continue
        enddo
        use
      endif
      * --- Write into ART_ICOL
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ART_ICOL_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ART_ICOL_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ARGESCAR ="+cp_NullLink(cp_ToStrODBC(this.w_GESCAR),'ART_ICOL','ARGESCAR');
        +",ARCONCAR ="+cp_NullLink(cp_ToStrODBC(this.w_ARCONCAR),'ART_ICOL','ARCONCAR');
            +i_ccchkf ;
        +" where ";
            +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_CCCODART);
               )
      else
        update (i_cTable) set;
            ARGESCAR = this.w_GESCAR;
            ,ARCONCAR = this.w_ARCONCAR;
            &i_ccchkf. ;
         where;
            ARCODART = this.oParentObject.w_CCCODART;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc


  proc Init(oParentObject,w_OPER)
    this.w_OPER=w_OPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='CONF_ART'
    this.cWorkTables[2]='CONF_CAR'
    this.cWorkTables[3]='CONFDCAR'
    this.cWorkTables[4]='ART_ICOL'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_CONF_ART')
      use in _Curs_CONF_ART
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_OPER"
endproc
