* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mnu                                                        *
*              Elenco campi tabella                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-03-23                                                      *
* Last revis.: 2012-11-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mnu")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mnu")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mnu")
  return

* --- Class definition
define class tgsar_mnu as StdPCForm
  Width  = 519
  Height = 172
  Top    = 10
  Left   = 10
  cComment = "Elenco campi tabella"
  cPrg = "gsar_mnu"
  HelpContextID=180975465
  add object cnt as tcgsar_mnu
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mnu as PCContext
  w_TABELLA = space(30)
  w_NACODAZI = space(5)
  w_NATIPGES = space(2)
  w_CPROWORD = 0
  w_NA_CAMPO = space(30)
  w_NA__TIPO = space(1)
  w_NALENGHT = 0
  w_NADECIMA = 0
  w_NAVALORE = space(20)
  proc Save(i_oFrom)
    this.w_TABELLA = i_oFrom.w_TABELLA
    this.w_NACODAZI = i_oFrom.w_NACODAZI
    this.w_NATIPGES = i_oFrom.w_NATIPGES
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_NA_CAMPO = i_oFrom.w_NA_CAMPO
    this.w_NA__TIPO = i_oFrom.w_NA__TIPO
    this.w_NALENGHT = i_oFrom.w_NALENGHT
    this.w_NADECIMA = i_oFrom.w_NADECIMA
    this.w_NAVALORE = i_oFrom.w_NAVALORE
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_TABELLA = this.w_TABELLA
    i_oTo.w_NACODAZI = this.w_NACODAZI
    i_oTo.w_NATIPGES = this.w_NATIPGES
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_NA_CAMPO = this.w_NA_CAMPO
    i_oTo.w_NA__TIPO = this.w_NA__TIPO
    i_oTo.w_NALENGHT = this.w_NALENGHT
    i_oTo.w_NADECIMA = this.w_NADECIMA
    i_oTo.w_NAVALORE = this.w_NAVALORE
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mnu as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 519
  Height = 172
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-05"
  HelpContextID=180975465
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  NUMAUT_C_IDX = 0
  XDC_FIELDS_IDX = 0
  cFile = "NUMAUT_C"
  cKeySelect = "NACODAZI,NATIPGES"
  cKeyWhere  = "NACODAZI=this.w_NACODAZI and NATIPGES=this.w_NATIPGES"
  cKeyDetail  = "NACODAZI=this.w_NACODAZI and NATIPGES=this.w_NATIPGES"
  cKeyWhereODBC = '"NACODAZI="+cp_ToStrODBC(this.w_NACODAZI)';
      +'+" and NATIPGES="+cp_ToStrODBC(this.w_NATIPGES)';

  cKeyDetailWhereODBC = '"NACODAZI="+cp_ToStrODBC(this.w_NACODAZI)';
      +'+" and NATIPGES="+cp_ToStrODBC(this.w_NATIPGES)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"NUMAUT_C.NACODAZI="+cp_ToStrODBC(this.w_NACODAZI)';
      +'+" and NUMAUT_C.NATIPGES="+cp_ToStrODBC(this.w_NATIPGES)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'NUMAUT_C.CPROWORD '
  cPrg = "gsar_mnu"
  cComment = "Elenco campi tabella"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TABELLA = space(30)
  w_NACODAZI = space(5)
  w_NATIPGES = space(2)
  w_CPROWORD = 0
  w_NA_CAMPO = space(30)
  w_NA__TIPO = space(1)
  o_NA__TIPO = space(1)
  w_NALENGHT = 0
  w_NADECIMA = 0
  w_NAVALORE = space(20)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mnuPag1","gsar_mnu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='XDC_FIELDS'
    this.cWorkTables[2]='NUMAUT_C'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(2))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.NUMAUT_C_IDX,5],7]
    this.nPostItConn=i_TableProp[this.NUMAUT_C_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_mnu'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- gsar_mnu
    this.w_TABELLA = this.oParentObject.w_NOMETAB
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from NUMAUT_C where NACODAZI=KeySet.NACODAZI
    *                            and NATIPGES=KeySet.NATIPGES
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.NUMAUT_C_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_C_IDX,2],this.bLoadRecFilter,this.NUMAUT_C_IDX,"gsar_mnu")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('NUMAUT_C')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "NUMAUT_C.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' NUMAUT_C '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'NACODAZI',this.w_NACODAZI  ,'NATIPGES',this.w_NATIPGES  )
      select * from (i_cTable) NUMAUT_C where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_TABELLA = left(this.oParentObject .w_NOMETAB+repl(' ',20),20)
        .w_NACODAZI = NVL(NACODAZI,space(5))
        .w_NATIPGES = NVL(NATIPGES,space(2))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'NUMAUT_C')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_NA_CAMPO = NVL(NA_CAMPO,space(30))
          .w_NA__TIPO = NVL(NA__TIPO,space(1))
          .w_NALENGHT = NVL(NALENGHT,0)
          .w_NADECIMA = NVL(NADECIMA,0)
          .w_NAVALORE = NVL(NAVALORE,space(20))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_TABELLA = left(this.oParentObject .w_NOMETAB+repl(' ',20),20)
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_TABELLA=space(30)
      .w_NACODAZI=space(5)
      .w_NATIPGES=space(2)
      .w_CPROWORD=10
      .w_NA_CAMPO=space(30)
      .w_NA__TIPO=space(1)
      .w_NALENGHT=0
      .w_NADECIMA=0
      .w_NAVALORE=space(20)
      if .cFunction<>"Filter"
        .w_TABELLA = left(this.oParentObject .w_NOMETAB+repl(' ',20),20)
        .DoRTCalc(2,6,.f.)
        .w_NALENGHT = iif(.w_NA__TIPO='L',1,iif(.w_NA__TIPO='D',8,.w_NALENGHT))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'NUMAUT_C')
    this.DoRTCalc(8,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'NUMAUT_C',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.NUMAUT_C_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NACODAZI,"NACODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_NATIPGES,"NATIPGES",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(4);
      ,t_NA_CAMPO C(30);
      ,t_NA__TIPO N(3);
      ,t_NALENGHT N(5);
      ,t_NADECIMA N(5);
      ,t_NAVALORE C(20);
      ,CPROWNUM N(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mnubodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oNA_CAMPO_2_2.controlsource=this.cTrsName+'.t_NA_CAMPO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oNA__TIPO_2_3.controlsource=this.cTrsName+'.t_NA__TIPO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oNALENGHT_2_4.controlsource=this.cTrsName+'.t_NALENGHT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oNADECIMA_2_5.controlsource=this.cTrsName+'.t_NADECIMA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oNAVALORE_2_6.controlsource=this.cTrsName+'.t_NAVALORE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(63)
    this.AddVLine(164)
    this.AddVLine(236)
    this.AddVLine(280)
    this.AddVLine(324)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.NUMAUT_C_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_C_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.NUMAUT_C_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_C_IDX,2])
      *
      * insert into NUMAUT_C
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'NUMAUT_C')
        i_extval=cp_InsertValODBCExtFlds(this,'NUMAUT_C')
        i_cFldBody=" "+;
                  "(NACODAZI,NATIPGES,CPROWORD,NA_CAMPO,NA__TIPO"+;
                  ",NALENGHT,NADECIMA,NAVALORE,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_NACODAZI)+","+cp_ToStrODBC(this.w_NATIPGES)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_NA_CAMPO)+","+cp_ToStrODBC(this.w_NA__TIPO)+;
             ","+cp_ToStrODBC(this.w_NALENGHT)+","+cp_ToStrODBC(this.w_NADECIMA)+","+cp_ToStrODBC(this.w_NAVALORE)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'NUMAUT_C')
        i_extval=cp_InsertValVFPExtFlds(this,'NUMAUT_C')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'NACODAZI',this.w_NACODAZI,'NATIPGES',this.w_NATIPGES)
        INSERT INTO (i_cTable) (;
                   NACODAZI;
                  ,NATIPGES;
                  ,CPROWORD;
                  ,NA_CAMPO;
                  ,NA__TIPO;
                  ,NALENGHT;
                  ,NADECIMA;
                  ,NAVALORE;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_NACODAZI;
                  ,this.w_NATIPGES;
                  ,this.w_CPROWORD;
                  ,this.w_NA_CAMPO;
                  ,this.w_NA__TIPO;
                  ,this.w_NALENGHT;
                  ,this.w_NADECIMA;
                  ,this.w_NAVALORE;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.NUMAUT_C_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_C_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CPROWORD))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'NUMAUT_C')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'NUMAUT_C')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CPROWORD))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update NUMAUT_C
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'NUMAUT_C')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",NA_CAMPO="+cp_ToStrODBC(this.w_NA_CAMPO)+;
                     ",NA__TIPO="+cp_ToStrODBC(this.w_NA__TIPO)+;
                     ",NALENGHT="+cp_ToStrODBC(this.w_NALENGHT)+;
                     ",NADECIMA="+cp_ToStrODBC(this.w_NADECIMA)+;
                     ",NAVALORE="+cp_ToStrODBC(this.w_NAVALORE)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'NUMAUT_C')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,NA_CAMPO=this.w_NA_CAMPO;
                     ,NA__TIPO=this.w_NA__TIPO;
                     ,NALENGHT=this.w_NALENGHT;
                     ,NADECIMA=this.w_NADECIMA;
                     ,NAVALORE=this.w_NAVALORE;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.NUMAUT_C_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_C_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CPROWORD))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete NUMAUT_C
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CPROWORD))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.NUMAUT_C_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_C_IDX,2])
    if i_bUpd
      with this
          .w_TABELLA = left(this.oParentObject .w_NOMETAB+repl(' ',20),20)
        .DoRTCalc(2,6,.t.)
        if .o_NA__TIPO<>.w_NA__TIPO
          .w_NALENGHT = iif(.w_NA__TIPO='L',1,iif(.w_NA__TIPO='D',8,.w_NALENGHT))
        endif
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(8,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oNADECIMA_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oNADECIMA_2_5.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNA_CAMPO_2_2.value==this.w_NA_CAMPO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNA_CAMPO_2_2.value=this.w_NA_CAMPO
      replace t_NA_CAMPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNA_CAMPO_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNA__TIPO_2_3.RadioValue()==this.w_NA__TIPO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNA__TIPO_2_3.SetRadio()
      replace t_NA__TIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNA__TIPO_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNALENGHT_2_4.value==this.w_NALENGHT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNALENGHT_2_4.value=this.w_NALENGHT
      replace t_NALENGHT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNALENGHT_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNADECIMA_2_5.value==this.w_NADECIMA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNADECIMA_2_5.value=this.w_NADECIMA
      replace t_NADECIMA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNADECIMA_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNAVALORE_2_6.value==this.w_NAVALORE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNAVALORE_2_6.value=this.w_NAVALORE
      replace t_NAVALORE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNAVALORE_2_6.value
    endif
    cp_SetControlsValueExtFlds(this,'NUMAUT_C')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not(EMPTY(.w_NAVALORE) OR (.w_NA__TIPO='C' AND .w_NALENGHT>=LEN(ALLTRIM(.w_NAVALORE))) OR (.w_NA__TIPO='N' AND .w_NALENGHT>=LEN(ALLTRIM(.w_NAVALORE))) OR (.w_NA__TIPO='D' AND LEN(ALLTRIM(.w_NAVALORE))=10 AND SUBSTR(ALLTRIM(.w_NAVALORE),3,1)='-' AND SUBSTR(ALLTRIM(.w_NAVALORE),6,1)='-')) and (not(Empty(.w_CPROWORD)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNAVALORE_2_6
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Lunghezza non consentita o formato non corretto")
      endcase
      if not(Empty(.w_CPROWORD))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_NA__TIPO = this.w_NA__TIPO
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CPROWORD)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999,cp_maxroword()+10)
      .w_NA_CAMPO=space(30)
      .w_NA__TIPO=space(1)
      .w_NALENGHT=0
      .w_NADECIMA=0
      .w_NAVALORE=space(20)
      .DoRTCalc(1,6,.f.)
        .w_NALENGHT = iif(.w_NA__TIPO='L',1,iif(.w_NA__TIPO='D',8,.w_NALENGHT))
    endwith
    this.DoRTCalc(8,9,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_NA_CAMPO = t_NA_CAMPO
    this.w_NA__TIPO = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNA__TIPO_2_3.RadioValue(.t.)
    this.w_NALENGHT = t_NALENGHT
    this.w_NADECIMA = t_NADECIMA
    this.w_NAVALORE = t_NAVALORE
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_NA_CAMPO with this.w_NA_CAMPO
    replace t_NA__TIPO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oNA__TIPO_2_3.ToRadio()
    replace t_NALENGHT with this.w_NALENGHT
    replace t_NADECIMA with this.w_NADECIMA
    replace t_NAVALORE with this.w_NAVALORE
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mnuPag1 as StdContainer
  Width  = 515
  height = 172
  stdWidth  = 515
  stdheight = 172
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=27, width=504,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="CPROWORD",Label1=" Seq.za",Field2="NA_CAMPO",Label2="Campo",Field3="NA__TIPO",Label3="Tipo",Field4="NALENGHT",Label4="Lung.",Field5="NADECIMA",Label5="Dec.",Field6="NAVALORE",Label6="Valore",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 135115386

  add object oStr_1_5 as StdString with uid="LSABGZBVSC",Visible=.t., Left=6, Top=4,;
    Alignment=0, Width=344, Height=19,;
    Caption="Valorizzazione campi/variabili per verifica"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_4 as StdBox with uid="OQYRJSFFUB",left=6, top=23, width=502,height=1

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-3,top=46,;
    width=498+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-2,top=47,width=497+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mnuBodyRow as CPBodyRowCnt
  Width=488
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="KEYQQTQLCM",rtseq=4,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 167443818,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=55, Left=-2, Top=0, cSayPict=["9999"], cGetPict=["9999"]

  add object oNA_CAMPO_2_2 as StdTrsField with uid="QUBYOZAJRY",rtseq=5,rtrep=.t.,;
    cFormVar="w_NA_CAMPO",value=space(30),;
    HelpContextID = 110083877,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=98, Left=56, Top=0, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oNA_CAMPO_2_2.mZoom
      with this.Parent.oContained
        GSAR_BNU(this.Parent.oContained,"V")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oNA__TIPO_2_3 as StdTrsCombo with uid="TFUGFVMGMQ",rtrep=.t.,;
    cFormVar="w_NA__TIPO", RowSource=""+"Carattere,"+"Numerico,"+"Data,"+"Logico,"+"Memo" , ;
    HelpContextID = 64732965,;
    Height=21, Width=69, Left=157, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oNA__TIPO_2_3.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..NA__TIPO,&i_cF..t_NA__TIPO),this.value)
    return(iif(xVal =1,'C',;
    iif(xVal =2,'N',;
    iif(xVal =3,'D',;
    iif(xVal =4,'L',;
    iif(xVal =5,'M',;
    space(1)))))))
  endfunc
  func oNA__TIPO_2_3.GetRadio()
    this.Parent.oContained.w_NA__TIPO = this.RadioValue()
    return .t.
  endfunc

  func oNA__TIPO_2_3.ToRadio()
    this.Parent.oContained.w_NA__TIPO=trim(this.Parent.oContained.w_NA__TIPO)
    return(;
      iif(this.Parent.oContained.w_NA__TIPO=='C',1,;
      iif(this.Parent.oContained.w_NA__TIPO=='N',2,;
      iif(this.Parent.oContained.w_NA__TIPO=='D',3,;
      iif(this.Parent.oContained.w_NA__TIPO=='L',4,;
      iif(this.Parent.oContained.w_NA__TIPO=='M',5,;
      0))))))
  endfunc

  func oNA__TIPO_2_3.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oNALENGHT_2_4 as StdTrsField with uid="SQPLPUTIPK",rtseq=7,rtrep=.t.,;
    cFormVar="w_NALENGHT",value=0,;
    HelpContextID = 245330134,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=229, Top=0, cSayPict=["99"], cGetPict=["99"]

  add object oNADECIMA_2_5 as StdTrsField with uid="SQKBWYPNXQ",rtseq=8,rtrep=.t.,;
    cFormVar="w_NADECIMA",value=0,;
    HelpContextID = 223342825,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=41, Left=273, Top=0, cSayPict=["99"], cGetPict=["99"]

  func oNADECIMA_2_5.mCond()
    with this.Parent.oContained
      return (.w_NA__TIPO='N')
    endwith
  endfunc

  add object oNAVALORE_2_6 as StdTrsField with uid="JWHFSDECDH",rtseq=9,rtrep=.t.,;
    cFormVar="w_NAVALORE",value=space(20),;
    HelpContextID = 155004699,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Lunghezza non consentita o formato non corretto",;
   bGlobalFont=.t.,;
    Height=17, Width=166, Left=317, Top=0, InputMask=replicate('X',20)

  func oNAVALORE_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_NAVALORE) OR (.w_NA__TIPO='C' AND .w_NALENGHT>=LEN(ALLTRIM(.w_NAVALORE))) OR (.w_NA__TIPO='N' AND .w_NALENGHT>=LEN(ALLTRIM(.w_NAVALORE))) OR (.w_NA__TIPO='D' AND LEN(ALLTRIM(.w_NAVALORE))=10 AND SUBSTR(ALLTRIM(.w_NAVALORE),3,1)='-' AND SUBSTR(ALLTRIM(.w_NAVALORE),6,1)='-'))
    endwith
    return bRes
  endfunc
  add object oLast as LastKeyMover
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mnu','NUMAUT_C','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".NACODAZI=NUMAUT_C.NACODAZI";
  +" and "+i_cAliasName2+".NATIPGES=NUMAUT_C.NATIPGES";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
