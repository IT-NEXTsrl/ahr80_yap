* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bcs                                                        *
*              Seleziona tutti -crea az                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_17]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-13                                                      *
* Last revis.: 2014-03-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bcs",oParentObject)
return(i_retval)

define class tgsut_bcs as StdBatch
  * --- Local variables
  siono = space(1)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Seleziona / Deseleziona tutto (da GSUT_KCA)
    * --- seleziona o deseleziona tutti
    this.siono = IIF(this.oParentObject.w_SELEZI="S", "S", " ")
    this.oParentObject.w_CONTA = this.siono
    this.oParentObject.w_pagame = this.siono
    this.oParentObject.w_ARTICOLI = this.siono
    this.oParentObject.w_mastri = this.siono
    this.oParentObject.w_forni = this.siono
    this.oParentObject.w_PROVVI = this.siono
    this.oParentObject.w_listini = this.siono
    this.oParentObject.w_contro = this.siono
    this.oParentObject.w_CONTRA1 = this.siono
    this.oParentObject.w_cambi = this.siono
    this.oParentObject.w_agenti = this.siono
    this.oParentObject.w_MESSAGGI = this.siono
    this.oParentObject.w_caumaga = this.siono
    this.oParentObject.w_nomecla = this.siono
    this.oParentObject.w_ARTMAG = this.siono
    this.oParentObject.w_conti = this.siono
    this.oParentObject.w_cauconta = this.siono
    this.oParentObject.w_TBLSCO = this.siono
    this.oParentObject.w_clienti = this.siono
    this.oParentObject.w_spedi = this.siono
    this.oParentObject.w_caudocu = this.siono
    this.oParentObject.w_Caumovcc = this.siono
    this.oParentObject.w_maga = this.siono
    this.oParentObject.w_TESO = this.siono
    this.oParentObject.w_vendi = this.siono
    this.oParentObject.w_lib_imma = this.siono
    this.oParentObject.w_CATELIST = this.siono
    this.oParentObject.w_PARALIST = this.siono
    this.oParentObject.w_ANAL = IIF(this.oParentObject.w_FANAL="S", this.siono, " ")
    this.oParentObject.w_SBIL = this.siono
    this.oParentObject.w_IMPO = IIF(this.oParentObject.w_FIMPO="S", this.siono, " ")
    this.oParentObject.w_CESPI = IIF(g_CESP="S", this.siono, " ")
    this.oParentObject.w_STATIS = IIF(g_STAT="S", this.siono, " ")
    this.oParentObject.w_ANABIL = IIF(g_BILC="S", this.siono, " ")
    this.oParentObject.w_DISB = IIF(g_DISB="S", this.siono, " ")
    this.oParentObject.w_VEFA = IIF(g_VEFA="S", this.siono, " ")
    this.oParentObject.w_COMM = IIF(g_COMM="S", this.siono, " ")
    this.oParentObject.w_framode = this.siono
    this.oParentObject.w_FLDISB = IIF((g_DISB="S" AND this.oParentObject.w_DISB="S") OR g_DISB="N","S","N")
    this.oParentObject.w_FLVEFA = IIF((g_VEFA="S" AND this.oParentObject.w_VEFA="S") OR g_VEFA="N","S","N")
    this.oParentObject.w_MATRICOLE = IIF(g_MATR="S", this.siono, " ")
    this.oParentObject.w_STLEGAL = IIF(IsAlt(), this.siono, " ")
    this.oParentObject.w_modconta = this.siono
    this.oParentObject.w_ATTSER = IIF("AGEN" $ UPPER(i_cModules), this.siono, " ")
    this.oParentObject.w_DIPE = IIF("AGEN" $ UPPER(i_cModules), this.siono, " ")
    this.oParentObject.w_GRCP = IIF("AGEN" $ UPPER(i_cModules), this.siono, " ")
    this.oParentObject.w_MANS = IIF("AGEN" $ UPPER(i_cModules), this.siono, " ")
    this.oParentObject.w_MOEL = IIF("AGEN" $ UPPER(i_cModules), this.siono, " ")
    this.oParentObject.w_RAAT = IIF("AGEN" $ UPPER(i_cModules), this.siono, " ")
    this.oParentObject.w_TIAT = IIF("AGEN" $ UPPER(i_cModules), this.siono, " ")
    this.oParentObject.w_TPAT = IIF("AGEN" $ UPPER(i_cModules), this.siono, " ")
    this.oParentObject.w_RAPR = IIF("AGEN" $ UPPER(i_cModules), this.siono, " ")
    this.oParentObject.w_ATTSER = IIF("AGEN" $ UPPER(i_cModules), this.siono, " ")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
