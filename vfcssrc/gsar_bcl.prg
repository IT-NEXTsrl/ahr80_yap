* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bcl                                                        *
*              Batch che lancia i contratti                                    *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_10]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2013-01-29                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bcl",oParentObject)
return(i_retval)

define class tgsar_bcl as StdBatch
  * --- Local variables
  w_CONUMERO = space(15)
  w_OGGETTO = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia i Contratti del Cliente (da GSAR_ACL)
    * --- Carica il Contratto
    vx_exec("QUERY\GSAR_BCL.VZM",this)
    if NOT EMPTY(NVL(this.w_CONUMERO, " "))
      if this.oParentObject.w_ANTIPCON="F"
        this.w_OGGETTO = GSAC_MCO()
      else
        this.w_OGGETTO = GSVE_MCO()
      endif
      * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
      if !(this.w_OGGETTO.bSec1)
        Ah_ErrorMsg("Impossibile aprire la gestione collegata!",48,"")
        i_retcode = 'stop'
        return
      endif
      this.w_OGGETTO.ecpFilter()     
      this.w_OGGETTO.w_CONUMERO = this.w_CONUMERO
      this.w_OGGETTO.ecpSave()     
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
