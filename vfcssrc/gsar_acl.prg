* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_acl                                                        *
*              Clienti                                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [161] [VRS_568]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-12                                                      *
* Last revis.: 2018-07-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_acl"))

* --- Class definition
define class tgsar_acl as StdForm
  Top    = 2
  Left   = 22

  * --- Standard Properties
  Width  = 773
  Height = 554+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-07-25"
  HelpContextID=158763159
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=263

  * --- Constant Properties
  CONTI_IDX = 0
  BAN_CHE_IDX = 0
  AGENTI_IDX = 0
  PAG_AMEN_IDX = 0
  LISTINI_IDX = 0
  VALUTE_IDX = 0
  GRUPRO_IDX = 0
  CATECOMM_IDX = 0
  LINGUE_IDX = 0
  CAT_SCMA_IDX = 0
  NAZIONI_IDX = 0
  CACOCLFO_IDX = 0
  SIT_FIDI_IDX = 0
  ZONE_IDX = 0
  VOCIIVA_IDX = 0
  DES_DIVE_IDX = 0
  MASTRI_IDX = 0
  COC_MAST_IDX = 0
  STU_PIAC_IDX = 0
  STUMPIAC_IDX = 0
  MAGAZZIN_IDX = 0
  METCALSP_IDX = 0
  COD_CATA_IDX = 0
  TIPCODIV_IDX = 0
  COD_AREO_IDX = 0
  CON_CONS_IDX = 0
  GRP_DEFA_IDX = 0
  VASTRUTT_IDX = 0
  GRU_INTE_IDX = 0
  TRI_BUTI_IDX = 0
  CONTRINT_IDX = 0
  PAR_OFFE_IDX = 0
  SAL_NOMI_IDX = 0
  TIP_CLIE_IDX = 0
  MODCLDAT_IDX = 0
  NUMAUT_M_IDX = 0
  DORATING_IDX = 0
  VOC_FINZ_IDX = 0
  cFile = "CONTI"
  cKeySelect = "ANTIPCON,ANCODICE"
  cQueryFilter="ANTIPCON='C'"
  cKeyWhere  = "ANTIPCON=this.w_ANTIPCON and ANCODICE=this.w_ANCODICE"
  cKeyWhereODBC = '"ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON)';
      +'+" and ANCODICE="+cp_ToStrODBC(this.w_ANCODICE)';

  cKeyWhereODBCqualified = '"CONTI.ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON)';
      +'+" and CONTI.ANCODICE="+cp_ToStrODBC(this.w_ANCODICE)';

  cPrg = "gsar_acl"
  cComment = "Clienti"
  icon = "anag.ico"
  cAutoZoom = 'GSAR0ACL'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AUTOAZI = space(5)
  w_AUTO = space(1)
  o_AUTO = space(1)
  w_ANTIPCON = space(1)
  o_ANTIPCON = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_ANCODICE = space(15)
  o_ANCODICE = space(15)
  w_ANDESCRI = space(60)
  w_ANDESCR2 = space(60)
  w_VALATT = space(20)
  w_TABKEY = space(8)
  w_DELIMM = .F.
  w_ANINDIRI = space(35)
  w_ANINDIR2 = space(35)
  w_AN___CAP = space(8)
  w_ANLOCALI = space(30)
  w_ANPROVIN = space(2)
  w_ANNAZION = space(3)
  w_DESNAZ = space(35)
  w_CODISO = space(3)
  w_ANCODFIS = space(16)
  o_ANCODFIS = space(16)
  w_ANPARIVA = space(12)
  w_ANTELEFO = space(18)
  w_ANTELFAX = space(18)
  w_ANNUMCEL = space(18)
  w_ANINDWEB = space(254)
  w_AN_EMAIL = space(254)
  w_AN_SKYPE = space(50)
  w_AN_EMPEC = space(254)
  w_ANCODSAL = space(5)
  w_ANPERFIS = space(1)
  o_ANPERFIS = space(1)
  w_ANCOGNOM = space(50)
  w_AN__NOME = space(50)
  w_AN_SESSO = space(1)
  w_ANLOCNAS = space(30)
  w_ANPRONAS = space(2)
  w_ANDATNAS = ctod('  /  /  ')
  w_ANNUMCAR = space(18)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_ANCHKSTA = space(1)
  w_ANCHKMAI = space(1)
  o_ANCHKMAI = space(1)
  w_ANCHKPEC = space(1)
  o_ANCHKPEC = space(1)
  w_ANCHKFAX = space(1)
  w_ANCHKCPZ = space(1)
  o_ANCHKCPZ = space(1)
  w_ANCHKWWP = space(1)
  w_ANCHKPTL = space(1)
  w_ANCHKFIR = space(1)
  w_ANCODEST = space(7)
  w_ANCODCLA = space(5)
  w_ANCODPEC = space(0)
  w_ANDTOBSO = ctod('  /  /  ')
  w_ANDTINVA = ctod('  /  /  ')
  w_ANTIPSOT = space(1)
  w_DANOM = .F.
  w_ERR = space(10)
  w_CODCOM = space(4)
  w_CONTA = 0
  o_CONTA = 0
  w_TIPRIF = space(1)
  w_ANCATOPE = space(2)
  w_CODI = space(15)
  w_RAG1 = space(40)
  w_CODAZI1 = space(5)
  w_ANCONSUP = space(15)
  w_DESSUP = space(40)
  w_ANCATCON = space(5)
  w_DESCON = space(35)
  w_PROSTU = space(8)
  o_PROSTU = space(8)
  w_CODSTU = space(3)
  o_CODSTU = space(3)
  w_ANCONRIF = space(15)
  w_DESORI = space(40)
  w_ANTIPRIF = space(1)
  w_ANCODIVA = space(5)
  w_NULIV = 0
  w_DESIVA = space(35)
  w_ANTIPOPE = space(10)
  w_ANCONCAU = space(15)
  w_TIDESCRI = space(30)
  w_ANFLRITE = space(1)
  o_ANFLRITE = space(1)
  w_ANCODIRP = space(5)
  o_ANCODIRP = space(5)
  w_FLIRPE = space(1)
  w_CAUPRE2 = space(2)
  w_ANCAURIT = space(2)
  w_TIPMAS = space(1)
  w_PERIVA = 0
  w_DATOBSO = ctod('  /  /  ')
  w_ANOPETRE = space(1)
  w_ANTIPPRE = space(1)
  w_ANFLAACC = space(1)
  w_ANFLESIG = space(1)
  o_ANFLESIG = space(1)
  w_AFFLINTR = space(1)
  o_AFFLINTR = space(1)
  w_ANIVASOS = space(1)
  w_ANFLBLLS = space(1)
  o_ANFLBLLS = space(1)
  w_ANCOFISC = space(25)
  w_ANSCIPAG = space(1)
  w_ANFLSOAL = space(1)
  o_ANFLSOAL = space(1)
  w_ANPARTSN = space(1)
  w_ANFLFIDO = space(1)
  w_ANFLSGRE = space(1)
  o_ANFLSGRE = space(1)
  w_ANFLBODO = space(1)
  w_ANCODSTU = space(10)
  o_ANCODSTU = space(10)
  w_ANCODSOG = space(8)
  w_ANCODCAT = space(4)
  w_TIDTOBSO = ctod('  /  /  ')
  w_MINCON = space(6)
  w_MAXCON = space(6)
  w_DESCAU = space(40)
  w_DESIRPEF = space(60)
  w_MASTRO = space(15)
  w_CATEGORIA = space(5)
  w_RIFDIC = space(10)
  w_ANNDIC = space(4)
  w_DATDIC = ctod('  /  /  ')
  w_NUMDIC = 0
  w_CODI = space(15)
  w_RAG1 = space(40)
  w_ANCATCOM = space(3)
  w_DESCAC = space(35)
  w_ANFLCONA = space(1)
  w_ANCATSCM = space(5)
  w_ANFLGCAU = space(1)
  w_DESSCM = space(35)
  w_ANTIPFAT = space(1)
  w_ANGRUPRO = space(5)
  w_ANCODISO = space(3)
  w_DESGPP = space(35)
  w_ANCONCON = space(1)
  w_FLSCM = space(1)
  w_FLPRO = space(1)
  w_ANCODLIN = space(3)
  w_DESLIN = space(30)
  w_ANCODVAL = space(3)
  w_DESVAL = space(35)
  w_ANNUMLIS = space(5)
  w_ANCODCUC = space(2)
  w_AN1SCONT = 0
  o_AN1SCONT = 0
  w_ANGRPDEF = space(5)
  w_AN2SCONT = 0
  w_ANCODZON = space(3)
  w_ANCODAG1 = space(5)
  w_ANTIPOCL = space(5)
  w_ANMAGTER = space(5)
  w_ANCODORN = space(15)
  w_ANCODPOR = space(10)
  w_ANMTDCLC = space(3)
  w_ANMCALSI = space(5)
  w_ANMCALST = space(5)
  w_DESLIS = space(40)
  w_VALLIS = space(3)
  w_IVALIS = space(1)
  w_DESZON = space(35)
  w_DESAGE1 = space(35)
  w_DESMAG = space(30)
  w_ANBOLFAT = space(1)
  w_ANPREBOL = space(1)
  w_ANFLAPCA = space(1)
  w_ANSCORPO = space(1)
  w_ANFLCODI = space(1)
  w_ANCLIPOS = space(1)
  w_ANFLGCPZ = space(1)
  w_FLGCPZ = space(1)
  w_ANFLIMBA = space(1)
  w_ANFLPRIV = space(1)
  w_FLPRIV = space(1)
  w_ANCODESC = space(5)
  w_DTOBSO = ctod('  /  /  ')
  w_OLDCLPOS = space(1)
  w_MSDESIMB = space(40)
  w_MSDESTRA = space(40)
  w_DESORN = space(40)
  w_GDDESCRI = space(40)
  w_DESCRI = space(40)
  w_ANMCALSI = space(5)
  w_ANMCALST = space(5)
  w_DESTIP = space(60)
  w_CODI = space(15)
  w_RAG1 = space(40)
  w_TIPCC = space(1)
  w_ANCODPAG = space(5)
  w_DESPAG = space(30)
  w_ANGIOFIS = 0
  w_ANFLINCA = space(1)
  w_ANSPEINC = 0
  w_AN1MESCL = 0
  o_AN1MESCL = 0
  w_DESMES1 = space(12)
  w_ANGIOSC1 = 0
  w_AN2MESCL = 0
  o_AN2MESCL = 0
  w_DESMES2 = space(12)
  w_ANGIOSC2 = 0
  w_ANCODBAN = space(10)
  w_DESBAN = space(42)
  w_ANNUMCOR = space(25)
  o_ANNUMCOR = space(25)
  w_ANCINABI = space(1)
  o_ANCINABI = space(1)
  w_DESBA2 = space(42)
  w_AN__BBAN = space(30)
  o_AN__BBAN = space(30)
  w_ANCODBA2 = space(15)
  w_CONSBF = space(1)
  w_AN__IBAN = space(35)
  w_ANFLRAGG = space(1)
  w_ANFLGAVV = space(1)
  w_OLDANTIIDRI = space(1)
  w_OLDANIBARID = space(16)
  w_ANDATAVV = ctod('  /  /  ')
  w_ANIDRIDY = space(1)
  o_ANIDRIDY = space(1)
  w_ANTIIDRI = 0
  o_ANTIIDRI = 0
  w_ANIBARID = space(16)
  o_ANIBARID = space(16)
  w_ANPAGFOR = space(5)
  w_TIPSOT = space(1)
  w_ANFLACBD = space(1)
  w_CODPAG = space(5)
  w_ANGESCON = space(1)
  o_ANGESCON = space(1)
  w_ANFLGCON = space(1)
  w_ANPAGPAR = space(5)
  o_ANPAGPAR = space(5)
  w_ANDATMOR = ctod('  /  /  ')
  w_ANFLESIM = space(1)
  o_ANFLESIM = space(1)
  w_ANSAGINT = 0
  o_ANSAGINT = 0
  w_ANSPRINT = space(1)
  w_DESPAR = space(30)
  w_CODI = space(15)
  w_RAG1 = space(40)
  w_CODI = space(15)
  w_RAG1 = space(40)
  w_ANFLBLVE = space(1)
  w_ANVALFID = 0
  w_ANMAXORD = 0
  w_READFID = space(15)
  w_FIDDAT = ctod('  /  /  ')
  w_FIDPAP = 0
  w_FIDESO = 0
  w_FIDESC = 0
  w_FIDORD = 0
  w_FIDDDT = 0
  w_FIDFAT = 0
  w_FIDRES1 = 0
  w_FIDRES2 = 0
  w_CODI = space(15)
  w_RAG1 = space(40)
  w_CODI = space(15)
  w_RAG1 = space(40)
  w_CODI = space(15)
  w_RAG1 = space(40)
  w_ANCODGRU = space(10)
  w_DESGRU = space(35)
  w_CODI = space(15)
  w_RAG1 = space(40)
  w_AN__NOTE = space(0)
  w_ANRATING = space(2)
  w_RADESCRI = space(50)
  w_ANGIORIT = 0
  w_ANVOCFIN = space(6)
  w_DFDESCRI = space(60)
  w_ANESCDOF = space(1)
  w_ANDESPAR = space(1)
  w_CODI = space(15)
  w_RAG1 = space(40)
  w_PADESCRI = space(30)
  w_CONTROLLO = .F.
  w_CONSUP = space(15)
  w_ANDESCR3 = space(60)
  w_ANDESCR4 = space(60)
  w_ANINDIR3 = space(35)
  w_ANINDIR4 = space(35)
  w_ANLOCAL2 = space(30)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_codazi = this.W_codazi
  op_ANCODICE = this.W_ANCODICE

  * --- Children pointers
  GSAR_MSA = .NULL.
  GSAR_MIN = .NULL.
  GSAR_MCC = .NULL.
  GSAR_MCO = .NULL.
  GSAR_MDD = .NULL.
  GSAR_MSE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsar_acl
  w_PaginaNote = 0
  w_BckpForClr = 0
  p_aut=0
  cCODICEDEF=' '
  PROCEDURE CodiceRid(obj)
  IF (this.cfunction='Load')
  return
  ENDIF
  IF ((NVL (this.w_ANTIIDRI,0)<>NVL(this.w_OLDANTIIDRI,0) AND NVL(this.w_OLDANTIIDRI,0)<>0  ) OR ( NVL(this.w_ANIBARID,"")<>NVL (this.w_OLDANIBARID,"") AND NVL (this.w_OLDANIBARID," ")<>" " ))
  IF (AH_YESNO ("Qualora si dovessero ricevere, dopo la modifica, esiti relativi a effetti presentati con il precedente ID bancario, la procedura non riuscir� a ricondurli al cliente. Confermare la modifica?") )
  this.w_OLDANTIIDRI=this.w_ANTIIDRI
  this.w_OLDANIBARID=this.w_ANIBARID
  ELSE
  this.w_ANTIIDRI=this.w_OLDANTIIDRI
  this.w_ANIBARID=this.w_OLDANIBARID
  ENDIF
  ENDIF
  ENDPROC
  *Ridefinizione Ecpsave
  PROCEDURE ecpSave()
      private oggetto,cObj,bcontrollo
      if  VARTYPE(g_GSAR_KCC)='O'
        oggetto=g_GSAR_KCC
      Endif
      bcontrollo=.t.
      DoDefault()
  if  VARTYPE(OGGETTO)='O'
     IF bControllo
      * solo se passo i controlli eseguo salvataggio maschera trasforma(gsar_kcc)
        OGGETTO.w_ANCODICE=this.cCODICEDEF
        cObj=OGGETTO.getCtrl('w_ANCODICE')
        cObj.value=this.cCODICEDEF
        OGGETTO.w_NOTIFICA='S'
        OGGETTO.ECPSAVE()
        this.ecpquery()
        this.ecpquit()
        release g_GSAR_KCC
     Endif
     cObj=null
     oggetto=null
  ENDIF
  ENDPROC
  
  
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=12, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'CONTI','gsar_acl')
    stdPageFrame::Init()
    *set procedure to GSAR_MSA additive
    with this
      .Pages(1).addobject("oPag","tgsar_aclPag1","gsar_acl",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Anagrafici")
      .Pages(1).HelpContextID = 234587489
      .Pages(2).addobject("oPag","tgsar_aclPag2","gsar_acl",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Contabili")
      .Pages(2).HelpContextID = 33678114
      .Pages(3).addobject("oPag","tgsar_aclPag3","gsar_acl",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Vendite")
      .Pages(3).HelpContextID = 193866922
      .Pages(4).addobject("oPag","tgsar_aclPag4","gsar_acl",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("Pagamenti")
      .Pages(4).HelpContextID = 95315450
      .Pages(5).addobject("oPag","tgsar_aclPag5","gsar_acl",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Contenzioso")
      .Pages(5).HelpContextID = 239683199
      .Pages(6).addobject("oPag","tgsar_aclPag6","gsar_acl",6)
      .Pages(6).oPag.Visible=.t.
      .Pages(6).Caption=cp_Translate("Rischio")
      .Pages(6).HelpContextID = 111073514
      .Pages(7).addobject("oPag","tgsar_aclPag7","gsar_acl",7)
      .Pages(7).oPag.Visible=.t.
      .Pages(7).Caption=cp_Translate("Riferimenti")
      .Pages(7).HelpContextID = 168387163
      .Pages(8).addobject("oPag","tgsar_aclPag8","gsar_acl",8)
      .Pages(8).oPag.Visible=.t.
      .Pages(8).Caption=cp_Translate("Sedi")
      .Pages(8).HelpContextID = 166081318
      .Pages(9).addobject("oPag","tgsar_aclPag9","gsar_acl",9)
      .Pages(9).oPag.Visible=.t.
      .Pages(9).Caption=cp_Translate("EDI")
      .Pages(9).HelpContextID = 159080774
      .Pages(10).addobject("oPag","tgsar_aclPag10","gsar_acl",10)
      .Pages(10).oPag.Visible=.t.
      .Pages(10).Caption=cp_Translate("Note")
      .Pages(10).HelpContextID = 165887190
      .Pages(11).addobject("oPag","tgsar_aclPag11","gsar_acl",11)
      .Pages(11).oPag.Visible=.t.
      .Pages(11).Caption=cp_Translate("Dati DocFinance")
      .Pages(11).HelpContextID = 170884089
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oANCODICE_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSAR_MSA
    * --- Area Manuale = Init Page Frame
    * --- gsar_acl
     this.Pages(3).Caption=cp_Translate(IIF(IsAlt(),"Parcellazione","Vendite"))
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[38]
    this.cWorkTables[1]='BAN_CHE'
    this.cWorkTables[2]='AGENTI'
    this.cWorkTables[3]='PAG_AMEN'
    this.cWorkTables[4]='LISTINI'
    this.cWorkTables[5]='VALUTE'
    this.cWorkTables[6]='GRUPRO'
    this.cWorkTables[7]='CATECOMM'
    this.cWorkTables[8]='LINGUE'
    this.cWorkTables[9]='CAT_SCMA'
    this.cWorkTables[10]='NAZIONI'
    this.cWorkTables[11]='CACOCLFO'
    this.cWorkTables[12]='SIT_FIDI'
    this.cWorkTables[13]='ZONE'
    this.cWorkTables[14]='VOCIIVA'
    this.cWorkTables[15]='DES_DIVE'
    this.cWorkTables[16]='MASTRI'
    this.cWorkTables[17]='COC_MAST'
    this.cWorkTables[18]='STU_PIAC'
    this.cWorkTables[19]='STUMPIAC'
    this.cWorkTables[20]='MAGAZZIN'
    this.cWorkTables[21]='METCALSP'
    this.cWorkTables[22]='COD_CATA'
    this.cWorkTables[23]='TIPCODIV'
    this.cWorkTables[24]='COD_AREO'
    this.cWorkTables[25]='CON_CONS'
    this.cWorkTables[26]='GRP_DEFA'
    this.cWorkTables[27]='VASTRUTT'
    this.cWorkTables[28]='GRU_INTE'
    this.cWorkTables[29]='TRI_BUTI'
    this.cWorkTables[30]='CONTRINT'
    this.cWorkTables[31]='PAR_OFFE'
    this.cWorkTables[32]='SAL_NOMI'
    this.cWorkTables[33]='TIP_CLIE'
    this.cWorkTables[34]='MODCLDAT'
    this.cWorkTables[35]='NUMAUT_M'
    this.cWorkTables[36]='DORATING'
    this.cWorkTables[37]='VOC_FINZ'
    this.cWorkTables[38]='CONTI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(38))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONTI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONTI_IDX,3]
  return

  function CreateChildren()
    this.GSAR_MSA = CREATEOBJECT('stdLazyChild',this,'GSAR_MSA')
    this.GSAR_MIN = CREATEOBJECT('stdLazyChild',this,'GSAR_MIN')
    this.GSAR_MCC = CREATEOBJECT('stdDynamicChild',this,'GSAR_MCC',this.oPgFrm.Page4.oPag.oLinkPC_4_26)
    this.GSAR_MCO = CREATEOBJECT('stdDynamicChild',this,'GSAR_MCO',this.oPgFrm.Page7.oPag.oLinkPC_7_4)
    this.GSAR_MDD = CREATEOBJECT('stdDynamicChild',this,'GSAR_MDD',this.oPgFrm.Page8.oPag.oLinkPC_8_4)
    this.GSAR_MDD.createrealchild()
    this.GSAR_MSE = CREATEOBJECT('stdDynamicChild',this,'GSAR_MSE',this.oPgFrm.Page9.oPag.oLinkPC_9_6)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAR_MSA)
      this.GSAR_MSA.DestroyChildrenChain()
      this.GSAR_MSA=.NULL.
    endif
    if !ISNULL(this.GSAR_MIN)
      this.GSAR_MIN.DestroyChildrenChain()
      this.GSAR_MIN=.NULL.
    endif
    if !ISNULL(this.GSAR_MCC)
      this.GSAR_MCC.DestroyChildrenChain()
      this.GSAR_MCC=.NULL.
    endif
    this.oPgFrm.Page4.oPag.RemoveObject('oLinkPC_4_26')
    if !ISNULL(this.GSAR_MCO)
      this.GSAR_MCO.DestroyChildrenChain()
      this.GSAR_MCO=.NULL.
    endif
    this.oPgFrm.Page7.oPag.RemoveObject('oLinkPC_7_4')
    if !ISNULL(this.GSAR_MDD)
      this.GSAR_MDD.DestroyChildrenChain()
      this.GSAR_MDD=.NULL.
    endif
    this.oPgFrm.Page8.oPag.RemoveObject('oLinkPC_8_4')
    if !ISNULL(this.GSAR_MSE)
      this.GSAR_MSE.DestroyChildrenChain()
      this.GSAR_MSE=.NULL.
    endif
    this.oPgFrm.Page9.oPag.RemoveObject('oLinkPC_9_6')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAR_MSA.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MIN.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MCC.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MCO.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MDD.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSAR_MSE.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAR_MSA.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MIN.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MCC.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MCO.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MDD.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSAR_MSE.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAR_MSA.NewDocument()
    this.GSAR_MIN.NewDocument()
    this.GSAR_MCC.NewDocument()
    this.GSAR_MCO.NewDocument()
    this.GSAR_MDD.NewDocument()
    this.GSAR_MSE.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAR_MSA.SetKey(;
            .w_ANCODICE,"SSCODICE";
            ,.w_ANTIPCON,"SSTIPCON";
            )
      this.GSAR_MIN.SetKey(;
            .w_ANTIPCON,"MCTIPINT";
            ,.w_ANCODICE,"MCCODINT";
            )
      this.GSAR_MCC.SetKey(;
            .w_ANTIPCON,"CCTIPCON";
            ,.w_ANCODICE,"CCCODCON";
            )
      this.GSAR_MCO.SetKey(;
            .w_ANTIPCON,"COTIPCON";
            ,.w_ANCODICE,"COCODCON";
            )
      this.GSAR_MDD.SetKey(;
            .w_ANTIPCON,"DDTIPCON";
            ,.w_ANCODICE,"DDCODICE";
            )
      this.GSAR_MSE.SetKey(;
            .w_ANTIPCON,"SITIPCON";
            ,.w_ANCODICE,"SICODCON";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAR_MSA.ChangeRow(this.cRowID+'      1',1;
             ,.w_ANCODICE,"SSCODICE";
             ,.w_ANTIPCON,"SSTIPCON";
             )
      .GSAR_MIN.ChangeRow(this.cRowID+'      1',1;
             ,.w_ANTIPCON,"MCTIPINT";
             ,.w_ANCODICE,"MCCODINT";
             )
      .GSAR_MCC.ChangeRow(this.cRowID+'      1',1;
             ,.w_ANTIPCON,"CCTIPCON";
             ,.w_ANCODICE,"CCCODCON";
             )
      .GSAR_MCO.ChangeRow(this.cRowID+'      1',1;
             ,.w_ANTIPCON,"COTIPCON";
             ,.w_ANCODICE,"COCODCON";
             )
      .WriteTo_GSAR_MCO()
      .GSAR_MDD.ChangeRow(this.cRowID+'      1',1;
             ,.w_ANTIPCON,"DDTIPCON";
             ,.w_ANCODICE,"DDCODICE";
             )
      .GSAR_MSE.ChangeRow(this.cRowID+'      1',1;
             ,.w_ANTIPCON,"SITIPCON";
             ,.w_ANCODICE,"SICODCON";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAR_MCC)
        i_f=.GSAR_MCC.BuildFilter()
        if !(i_f==.GSAR_MCC.cQueryFilter)
          i_fnidx=.GSAR_MCC.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MCC.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MCC.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MCC.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MCC.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAR_MCO)
        i_f=.GSAR_MCO.BuildFilter()
        if !(i_f==.GSAR_MCO.cQueryFilter)
          i_fnidx=.GSAR_MCO.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MCO.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MCO.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MCO.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MCO.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAR_MDD)
        i_f=.GSAR_MDD.BuildFilter()
        if !(i_f==.GSAR_MDD.cQueryFilter)
          i_fnidx=.GSAR_MDD.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MDD.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MDD.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MDD.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MDD.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSAR_MSE)
        i_f=.GSAR_MSE.BuildFilter()
        if !(i_f==.GSAR_MSE.cQueryFilter)
          i_fnidx=.GSAR_MSE.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MSE.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MSE.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MSE.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MSE.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

procedure WriteTo_GSAR_MCO()
  if at('gsar_mco',lower(this.GSAR_MCO.class))<>0
    if this.GSAR_MCO.w_VRCODCON<>this.w_ANCODICE or this.GSAR_MCO.w_VRTIPCON<>this.w_ANTIPCON
      this.GSAR_MCO.w_VRCODCON = this.w_ANCODICE
      this.GSAR_MCO.w_VRTIPCON = this.w_ANTIPCON
      this.GSAR_MCO.mCalc(.t.)
    endif
  endif
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_ANTIPCON = NVL(ANTIPCON,space(1))
      .w_ANCODICE = NVL(ANCODICE,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_16_joined
    link_1_16_joined=.f.
    local link_2_7_joined
    link_2_7_joined=.f.
    local link_2_9_joined
    link_2_9_joined=.f.
    local link_2_17_joined
    link_2_17_joined=.f.
    local link_2_24_joined
    link_2_24_joined=.f.
    local link_3_3_joined
    link_3_3_joined=.f.
    local link_3_6_joined
    link_3_6_joined=.f.
    local link_3_10_joined
    link_3_10_joined=.f.
    local link_3_17_joined
    link_3_17_joined=.f.
    local link_3_19_joined
    link_3_19_joined=.f.
    local link_3_21_joined
    link_3_21_joined=.f.
    local link_3_24_joined
    link_3_24_joined=.f.
    local link_3_26_joined
    link_3_26_joined=.f.
    local link_3_27_joined
    link_3_27_joined=.f.
    local link_3_28_joined
    link_3_28_joined=.f.
    local link_3_29_joined
    link_3_29_joined=.f.
    local link_3_30_joined
    link_3_30_joined=.f.
    local link_3_31_joined
    link_3_31_joined=.f.
    local link_3_33_joined
    link_3_33_joined=.f.
    local link_3_34_joined
    link_3_34_joined=.f.
    local link_3_97_joined
    link_3_97_joined=.f.
    local link_3_98_joined
    link_3_98_joined=.f.
    local link_4_4_joined
    link_4_4_joined=.f.
    local link_4_17_joined
    link_4_17_joined=.f.
    local link_4_23_joined
    link_4_23_joined=.f.
    local link_4_36_joined
    link_4_36_joined=.f.
    local link_5_3_joined
    link_5_3_joined=.f.
    local link_9_4_joined
    link_9_4_joined=.f.
    local link_11_2_joined
    link_11_2_joined=.f.
    local link_11_7_joined
    link_11_7_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CONTI where ANTIPCON=KeySet.ANTIPCON
    *                            and ANCODICE=KeySet.ANCODICE
    *
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONTI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONTI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONTI '
      link_1_16_joined=this.AddJoinedLink_1_16(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_7_joined=this.AddJoinedLink_2_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_9_joined=this.AddJoinedLink_2_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_17_joined=this.AddJoinedLink_2_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_24_joined=this.AddJoinedLink_2_24(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_3_joined=this.AddJoinedLink_3_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_6_joined=this.AddJoinedLink_3_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_10_joined=this.AddJoinedLink_3_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_17_joined=this.AddJoinedLink_3_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_19_joined=this.AddJoinedLink_3_19(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_21_joined=this.AddJoinedLink_3_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_24_joined=this.AddJoinedLink_3_24(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_26_joined=this.AddJoinedLink_3_26(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_27_joined=this.AddJoinedLink_3_27(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_28_joined=this.AddJoinedLink_3_28(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_29_joined=this.AddJoinedLink_3_29(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_30_joined=this.AddJoinedLink_3_30(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_31_joined=this.AddJoinedLink_3_31(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_33_joined=this.AddJoinedLink_3_33(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_34_joined=this.AddJoinedLink_3_34(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_97_joined=this.AddJoinedLink_3_97(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_3_98_joined=this.AddJoinedLink_3_98(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_4_joined=this.AddJoinedLink_4_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_17_joined=this.AddJoinedLink_4_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_23_joined=this.AddJoinedLink_4_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_36_joined=this.AddJoinedLink_4_36(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_5_3_joined=this.AddJoinedLink_5_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_9_4_joined=this.AddJoinedLink_9_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_11_2_joined=this.AddJoinedLink_11_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_11_7_joined=this.AddJoinedLink_11_7(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'ANTIPCON',this.w_ANTIPCON  ,'ANCODICE',this.w_ANCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_AUTO = ' '
        .w_VALATT = "C"+.w_ANCODICE
        .w_TABKEY = 'CONTI'
        .w_DELIMM = .f.
        .w_DESNAZ = space(35)
        .w_CODISO = space(3)
        .w_DANOM = .f.
        .w_ERR = space(10)
        .w_CODCOM = space(4)
        .w_CONTA = .w_CONTA
        .w_CODAZI1 = i_codazi
        .w_DESSUP = space(40)
        .w_DESCON = space(35)
        .w_PROSTU = space(8)
        .w_CODSTU = space(3)
        .w_DESORI = space(40)
        .w_NULIV = 0
        .w_DESIVA = space(35)
        .w_TIDESCRI = space(30)
        .w_FLIRPE = space(1)
        .w_CAUPRE2 = space(2)
        .w_TIPMAS = space(1)
        .w_PERIVA = 0
        .w_DATOBSO = ctod("  /  /  ")
        .w_TIDTOBSO = ctod("  /  /  ")
        .w_MINCON = space(6)
        .w_MAXCON = space(6)
        .w_DESCAU = space(40)
        .w_DESIRPEF = space(60)
        .w_MASTRO = space(15)
        .w_CATEGORIA = space(5)
        .w_DESCAC = space(35)
        .w_DESSCM = space(35)
        .w_ANCODISO = g_ISONAZ
        .w_DESGPP = space(35)
        .w_FLSCM = space(1)
        .w_FLPRO = space(1)
        .w_DESLIN = space(30)
        .w_DESVAL = space(35)
        .w_DESLIS = space(40)
        .w_VALLIS = space(3)
        .w_IVALIS = space(1)
        .w_DESZON = space(35)
        .w_DESAGE1 = space(35)
        .w_DESMAG = space(30)
        .w_DTOBSO = ctod("  /  /  ")
        .w_MSDESIMB = space(40)
        .w_MSDESTRA = space(40)
        .w_DESORN = space(40)
        .w_GDDESCRI = space(40)
        .w_DESCRI = space(40)
        .w_DESTIP = space(60)
        .w_DESPAG = space(30)
        .w_DESBAN = space(42)
        .w_DESBA2 = space(42)
        .w_CONSBF = space(1)
        .w_OLDANTIIDRI = 1
        .w_OLDANIBARID = ''
        .w_TIPSOT = space(1)
        .w_CODPAG = space(5)
        .w_DESPAR = space(30)
        .w_FIDDAT = ctod("  /  /  ")
        .w_FIDPAP = 0
        .w_FIDESO = 0
        .w_FIDESC = 0
        .w_FIDORD = 0
        .w_FIDDDT = 0
        .w_FIDFAT = 0
        .w_DESGRU = space(35)
        .w_RADESCRI = space(50)
        .w_DFDESCRI = space(60)
        .w_PADESCRI = space(30)
        .w_CONTROLLO = .f.
        .w_CONSUP = space(15)
        .w_AUTOAZI = i_CODAZI
          .link_1_1('Load')
        .w_ANTIPCON = NVL(ANTIPCON,space(1))
        .w_OBTEST = i_datsys
        .w_ANCODICE = NVL(ANCODICE,space(15))
        .op_ANCODICE = .w_ANCODICE
        .w_ANDESCRI = NVL(ANDESCRI,space(60))
        .w_ANDESCR2 = NVL(ANDESCR2,space(60))
        .w_ANINDIRI = NVL(ANINDIRI,space(35))
        .w_ANINDIR2 = NVL(ANINDIR2,space(35))
        .w_AN___CAP = NVL(AN___CAP,space(8))
        .w_ANLOCALI = NVL(ANLOCALI,space(30))
        .w_ANPROVIN = NVL(ANPROVIN,space(2))
        .w_ANNAZION = NVL(ANNAZION,space(3))
          if link_1_16_joined
            this.w_ANNAZION = NVL(NACODNAZ116,NVL(this.w_ANNAZION,space(3)))
            this.w_DESNAZ = NVL(NADESNAZ116,space(35))
            this.w_CODISO = NVL(NACODISO116,space(3))
          else
          .link_1_16('Load')
          endif
        .w_ANCODFIS = NVL(ANCODFIS,space(16))
        .w_ANPARIVA = NVL(ANPARIVA,space(12))
        .w_ANTELEFO = NVL(ANTELEFO,space(18))
        .w_ANTELFAX = NVL(ANTELFAX,space(18))
        .w_ANNUMCEL = NVL(ANNUMCEL,space(18))
        .w_ANINDWEB = NVL(ANINDWEB,space(254))
        .w_AN_EMAIL = NVL(AN_EMAIL,space(254))
        .w_AN_SKYPE = NVL(AN_SKYPE,space(50))
        .w_AN_EMPEC = NVL(AN_EMPEC,space(254))
        .w_ANCODSAL = NVL(ANCODSAL,space(5))
          * evitabile
          *.link_1_28('Load')
        .w_ANPERFIS = NVL(ANPERFIS,space(1))
        .w_ANCOGNOM = NVL(ANCOGNOM,space(50))
        .w_AN__NOME = NVL(AN__NOME,space(50))
        .w_AN_SESSO = NVL(AN_SESSO,space(1))
        .w_ANLOCNAS = NVL(ANLOCNAS,space(30))
        .w_ANPRONAS = NVL(ANPRONAS,space(2))
        .w_ANDATNAS = NVL(cp_ToDate(ANDATNAS),ctod("  /  /  "))
        .w_ANNUMCAR = NVL(ANNUMCAR,space(18))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_ANCHKSTA = NVL(ANCHKSTA,space(1))
        .w_ANCHKMAI = NVL(ANCHKMAI,space(1))
        .w_ANCHKPEC = NVL(ANCHKPEC,space(1))
        .w_ANCHKFAX = NVL(ANCHKFAX,space(1))
        .w_ANCHKCPZ = NVL(ANCHKCPZ,space(1))
        .w_ANCHKWWP = NVL(ANCHKWWP,space(1))
        .w_ANCHKPTL = NVL(ANCHKPTL,space(1))
        .w_ANCHKFIR = NVL(ANCHKFIR,space(1))
        .w_ANCODEST = NVL(ANCODEST,space(7))
        .w_ANCODCLA = NVL(ANCODCLA,space(5))
        .w_ANCODPEC = NVL(ANCODPEC,space(0))
        .w_ANDTOBSO = NVL(cp_ToDate(ANDTOBSO),ctod("  /  /  "))
        .w_ANDTINVA = NVL(cp_ToDate(ANDTINVA),ctod("  /  /  "))
        .w_ANTIPSOT = NVL(ANTIPSOT,space(1))
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_92.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
        .w_TIPRIF = 'F'
        .w_ANCATOPE = NVL(ANCATOPE,space(2))
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
          .link_2_6('Load')
        .w_ANCONSUP = NVL(ANCONSUP,space(15))
          if link_2_7_joined
            this.w_ANCONSUP = NVL(MCCODICE207,NVL(this.w_ANCONSUP,space(15)))
            this.w_DESSUP = NVL(MCDESCRI207,space(40))
            this.w_NULIV = NVL(MCNUMLIV207,0)
            this.w_TIPMAS = NVL(MCTIPMAS207,space(1))
            this.w_CODSTU = NVL(MCCODSTU207,space(3))
            this.w_PROSTU = NVL(MCPROSTU207,space(8))
            this.w_CONSUP = NVL(MCCONSUP207,space(15))
          else
          .link_2_7('Load')
          endif
        .w_ANCATCON = NVL(ANCATCON,space(5))
          if link_2_9_joined
            this.w_ANCATCON = NVL(C2CODICE209,NVL(this.w_ANCATCON,space(5)))
            this.w_DESCON = NVL(C2DESCRI209,space(35))
          else
          .link_2_9('Load')
          endif
        .w_ANCONRIF = NVL(ANCONRIF,space(15))
          .link_2_14('Load')
        .w_ANTIPRIF = NVL(ANTIPRIF,space(1))
        .w_ANCODIVA = NVL(ANCODIVA,space(5))
          if link_2_17_joined
            this.w_ANCODIVA = NVL(IVCODIVA217,NVL(this.w_ANCODIVA,space(5)))
            this.w_DESIVA = NVL(IVDESIVA217,space(35))
            this.w_PERIVA = NVL(IVPERIVA217,0)
            this.w_DATOBSO = NVL(cp_ToDate(IVDTOBSO217),ctod("  /  /  "))
          else
          .link_2_17('Load')
          endif
        .w_ANTIPOPE = NVL(ANTIPOPE,space(10))
          .link_2_20('Load')
        .w_ANCONCAU = NVL(ANCONCAU,space(15))
          .link_2_21('Load')
        .w_ANFLRITE = NVL(ANFLRITE,space(1))
        .w_ANCODIRP = NVL(ANCODIRP,space(5))
          if link_2_24_joined
            this.w_ANCODIRP = NVL(TRCODTRI224,NVL(this.w_ANCODIRP,space(5)))
            this.w_DESIRPEF = NVL(TRDESTRI224,space(60))
            this.w_FLIRPE = NVL(TRFLACON224,space(1))
            this.w_CAUPRE2 = NVL(TRCAUPRE2224,space(2))
          else
          .link_2_24('Load')
          endif
        .w_ANCAURIT = NVL(ANCAURIT,space(2))
        .w_ANOPETRE = NVL(ANOPETRE,space(1))
        .w_ANTIPPRE = NVL(ANTIPPRE,space(1))
        .w_ANFLAACC = NVL(ANFLAACC,space(1))
        .w_ANFLESIG = NVL(ANFLESIG,space(1))
        .w_AFFLINTR = NVL(AFFLINTR,space(1))
        .w_ANIVASOS = NVL(ANIVASOS,space(1))
        .w_ANFLBLLS = NVL(ANFLBLLS,space(1))
        .w_ANCOFISC = NVL(ANCOFISC,space(25))
        .w_ANSCIPAG = NVL(ANSCIPAG,space(1))
        .w_ANFLSOAL = NVL(ANFLSOAL,space(1))
        .w_ANPARTSN = NVL(ANPARTSN,space(1))
        .w_ANFLFIDO = NVL(ANFLFIDO,space(1))
        .w_ANFLSGRE = NVL(ANFLSGRE,space(1))
        .w_ANFLBODO = NVL(ANFLBODO,space(1))
        .w_ANCODSTU = NVL(ANCODSTU,space(10))
        .w_ANCODSOG = NVL(ANCODSOG,space(8))
        .w_ANCODCAT = NVL(ANCODCAT,space(4))
          * evitabile
          *.link_2_55('Load')
        .w_RIFDIC = ''
        .w_ANNDIC = ''
        .w_DATDIC = ''
        .w_NUMDIC = ''
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
        .w_ANCATCOM = NVL(ANCATCOM,space(3))
          if link_3_3_joined
            this.w_ANCATCOM = NVL(CTCODICE303,NVL(this.w_ANCATCOM,space(3)))
            this.w_DESCAC = NVL(CTDESCRI303,space(35))
          else
          .link_3_3('Load')
          endif
        .w_ANFLCONA = NVL(ANFLCONA,space(1))
        .w_ANCATSCM = NVL(ANCATSCM,space(5))
          if link_3_6_joined
            this.w_ANCATSCM = NVL(CSCODICE306,NVL(this.w_ANCATSCM,space(5)))
            this.w_DESSCM = NVL(CSDESCRI306,space(35))
            this.w_FLSCM = NVL(CSTIPCAT306,space(1))
          else
          .link_3_6('Load')
          endif
        .w_ANFLGCAU = NVL(ANFLGCAU,space(1))
        .w_ANTIPFAT = NVL(ANTIPFAT,space(1))
        .w_ANGRUPRO = NVL(ANGRUPRO,space(5))
          if link_3_10_joined
            this.w_ANGRUPRO = NVL(GPCODICE310,NVL(this.w_ANGRUPRO,space(5)))
            this.w_DESGPP = NVL(GPDESCRI310,space(35))
            this.w_FLPRO = NVL(GPTIPPRO310,space(1))
          else
          .link_3_10('Load')
          endif
        .w_ANCONCON = NVL(ANCONCON,space(1))
          * evitabile
          *.link_3_14('Load')
        .w_ANCODLIN = NVL(ANCODLIN,space(3))
          if link_3_17_joined
            this.w_ANCODLIN = NVL(LUCODICE317,NVL(this.w_ANCODLIN,space(3)))
            this.w_DESLIN = NVL(LUDESCRI317,space(30))
          else
          .link_3_17('Load')
          endif
        .w_ANCODVAL = NVL(ANCODVAL,space(3))
          if link_3_19_joined
            this.w_ANCODVAL = NVL(VACODVAL319,NVL(this.w_ANCODVAL,space(3)))
            this.w_DESVAL = NVL(VADESVAL319,space(35))
            this.w_DTOBSO = NVL(cp_ToDate(VADTOBSO319),ctod("  /  /  "))
          else
          .link_3_19('Load')
          endif
        .w_ANNUMLIS = NVL(ANNUMLIS,space(5))
          if link_3_21_joined
            this.w_ANNUMLIS = NVL(LSCODLIS321,NVL(this.w_ANNUMLIS,space(5)))
            this.w_DESLIS = NVL(LSDESLIS321,space(40))
            this.w_VALLIS = NVL(LSVALLIS321,space(3))
            this.w_IVALIS = NVL(LSIVALIS321,space(1))
          else
          .link_3_21('Load')
          endif
        .w_ANCODCUC = NVL(ANCODCUC,space(2))
        .w_AN1SCONT = NVL(AN1SCONT,0)
        .w_ANGRPDEF = NVL(ANGRPDEF,space(5))
          if link_3_24_joined
            this.w_ANGRPDEF = NVL(GDCODICE324,NVL(this.w_ANGRPDEF,space(5)))
            this.w_GDDESCRI = NVL(GDDESCRI324,space(40))
          else
          .link_3_24('Load')
          endif
        .w_AN2SCONT = NVL(AN2SCONT,0)
        .w_ANCODZON = NVL(ANCODZON,space(3))
          if link_3_26_joined
            this.w_ANCODZON = NVL(ZOCODZON326,NVL(this.w_ANCODZON,space(3)))
            this.w_DESZON = NVL(ZODESZON326,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(ZODTOBSO326),ctod("  /  /  "))
          else
          .link_3_26('Load')
          endif
        .w_ANCODAG1 = NVL(ANCODAG1,space(5))
          if link_3_27_joined
            this.w_ANCODAG1 = NVL(AGCODAGE327,NVL(this.w_ANCODAG1,space(5)))
            this.w_DESAGE1 = NVL(AGDESAGE327,space(35))
            this.w_DATOBSO = NVL(cp_ToDate(AGDTOBSO327),ctod("  /  /  "))
          else
          .link_3_27('Load')
          endif
        .w_ANTIPOCL = NVL(ANTIPOCL,space(5))
          if link_3_28_joined
            this.w_ANTIPOCL = NVL(TCCODICE328,NVL(this.w_ANTIPOCL,space(5)))
            this.w_DESTIP = NVL(TCDESCRI328,space(60))
          else
          .link_3_28('Load')
          endif
        .w_ANMAGTER = NVL(ANMAGTER,space(5))
          if link_3_29_joined
            this.w_ANMAGTER = NVL(MGCODMAG329,NVL(this.w_ANMAGTER,space(5)))
            this.w_DESMAG = NVL(MGDESMAG329,space(30))
          else
          .link_3_29('Load')
          endif
        .w_ANCODORN = NVL(ANCODORN,space(15))
          if link_3_30_joined
            this.w_ANCODORN = NVL(ANCODICE330,NVL(this.w_ANCODORN,space(15)))
            this.w_DESORN = NVL(ANDESCRI330,space(40))
          else
          .link_3_30('Load')
          endif
        .w_ANCODPOR = NVL(ANCODPOR,space(10))
          if link_3_31_joined
            this.w_ANCODPOR = NVL(PPCODICE331,NVL(this.w_ANCODPOR,space(10)))
            this.w_DESCRI = NVL(PPDESCRI331,space(40))
          else
          .link_3_31('Load')
          endif
        .w_ANMTDCLC = NVL(ANMTDCLC,space(3))
          * evitabile
          *.link_3_32('Load')
        .w_ANMCALSI = NVL(ANMCALSI,space(5))
          if link_3_33_joined
            this.w_ANMCALSI = NVL(MSCODICE333,NVL(this.w_ANMCALSI,space(5)))
            this.w_MSDESIMB = NVL(MSDESCRI333,space(40))
          else
          .link_3_33('Load')
          endif
        .w_ANMCALST = NVL(ANMCALST,space(5))
          if link_3_34_joined
            this.w_ANMCALST = NVL(MSCODICE334,NVL(this.w_ANMCALST,space(5)))
            this.w_MSDESTRA = NVL(MSDESCRI334,space(40))
          else
          .link_3_34('Load')
          endif
        .w_ANBOLFAT = NVL(ANBOLFAT,space(1))
        .w_ANPREBOL = NVL(ANPREBOL,space(1))
        .w_ANFLAPCA = NVL(ANFLAPCA,space(1))
        .w_ANSCORPO = NVL(ANSCORPO,space(1))
        .w_ANFLCODI = NVL(ANFLCODI,space(1))
        .w_ANCLIPOS = NVL(ANCLIPOS,space(1))
        .w_ANFLGCPZ = NVL(ANFLGCPZ,space(1))
        .w_FLGCPZ = .w_ANFLGCPZ
        .w_ANFLIMBA = NVL(ANFLIMBA,space(1))
        .w_ANFLPRIV = NVL(ANFLPRIV,space(1))
        .w_FLPRIV = .w_ANFLPRIV
        .w_ANCODESC = NVL(ANCODESC,space(5))
        .w_OLDCLPOS = .w_ANCLIPOS
        .w_ANMCALSI = NVL(ANMCALSI,space(5))
          if link_3_97_joined
            this.w_ANMCALSI = NVL(MSCODICE397,NVL(this.w_ANMCALSI,space(5)))
            this.w_MSDESIMB = NVL(MSDESCRI397,space(40))
          else
          .link_3_97('Load')
          endif
        .w_ANMCALST = NVL(ANMCALST,space(5))
          if link_3_98_joined
            this.w_ANMCALST = NVL(MSCODICE398,NVL(this.w_ANMCALST,space(5)))
            this.w_MSDESTRA = NVL(MSDESCRI398,space(40))
          else
          .link_3_98('Load')
          endif
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
        .w_TIPCC = 'G'
        .w_ANCODPAG = NVL(ANCODPAG,space(5))
          if link_4_4_joined
            this.w_ANCODPAG = NVL(PACODICE404,NVL(this.w_ANCODPAG,space(5)))
            this.w_DESPAG = NVL(PADESCRI404,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(PADTOBSO404),ctod("  /  /  "))
          else
          .link_4_4('Load')
          endif
        .w_ANGIOFIS = NVL(ANGIOFIS,0)
        .w_ANFLINCA = NVL(ANFLINCA,space(1))
        .w_ANSPEINC = NVL(ANSPEINC,0)
        .w_AN1MESCL = NVL(AN1MESCL,0)
        .w_DESMES1 = LEFT(IIF(.w_AN1MESCL>0 AND .w_AN1MESCL<13, g_MESE[.w_AN1MESCL], "")+SPACE(12),12)
        .w_ANGIOSC1 = NVL(ANGIOSC1,0)
        .w_AN2MESCL = NVL(AN2MESCL,0)
        .w_DESMES2 = LEFT(IIF(.w_AN2MESCL>0 AND .w_AN2MESCL<13, g_MESE[.w_AN2MESCL], "")+SPACE(12),12)
        .w_ANGIOSC2 = NVL(ANGIOSC2,0)
        .w_ANCODBAN = NVL(ANCODBAN,space(10))
          if link_4_17_joined
            this.w_ANCODBAN = NVL(BACODBAN417,NVL(this.w_ANCODBAN,space(10)))
            this.w_DESBAN = NVL(BADESBAN417,space(42))
          else
          .link_4_17('Load')
          endif
        .w_ANNUMCOR = NVL(ANNUMCOR,space(25))
        .w_ANCINABI = NVL(ANCINABI,space(1))
        .w_AN__BBAN = NVL(AN__BBAN,space(30))
        .w_ANCODBA2 = NVL(ANCODBA2,space(15))
          if link_4_23_joined
            this.w_ANCODBA2 = NVL(BACODBAN423,NVL(this.w_ANCODBA2,space(15)))
            this.w_DESBA2 = NVL(BADESCRI423,space(42))
            this.w_DATOBSO = NVL(cp_ToDate(BADTOBSO423),ctod("  /  /  "))
            this.w_CONSBF = NVL(BACONSBF423,space(1))
          else
          .link_4_23('Load')
          endif
        .w_AN__IBAN = NVL(AN__IBAN,space(35))
        .w_ANFLRAGG = NVL(ANFLRAGG,space(1))
        .w_ANFLGAVV = NVL(ANFLGAVV,space(1))
        .w_ANDATAVV = NVL(cp_ToDate(ANDATAVV),ctod("  /  /  "))
        .w_ANIDRIDY = NVL(ANIDRIDY,space(1))
        .w_ANTIIDRI = NVL(ANTIIDRI,0)
        .w_ANIBARID = NVL(ANIBARID,space(16))
        .w_ANPAGFOR = NVL(ANPAGFOR,space(5))
          if link_4_36_joined
            this.w_ANPAGFOR = NVL(PACODICE436,NVL(this.w_ANPAGFOR,space(5)))
            this.w_PADESCRI = NVL(PADESCRI436,space(30))
          else
          .link_4_36('Load')
          endif
        .w_ANFLACBD = NVL(ANFLACBD,space(1))
        .w_ANGESCON = NVL(ANGESCON,space(1))
        .w_ANFLGCON = NVL(ANFLGCON,space(1))
        .w_ANPAGPAR = NVL(ANPAGPAR,space(5))
          if link_5_3_joined
            this.w_ANPAGPAR = NVL(PACODICE503,NVL(this.w_ANPAGPAR,space(5)))
            this.w_DESPAR = NVL(PADESCRI503,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(PADTOBSO503),ctod("  /  /  "))
          else
          .link_5_3('Load')
          endif
        .w_ANDATMOR = NVL(cp_ToDate(ANDATMOR),ctod("  /  /  "))
        .w_ANFLESIM = NVL(ANFLESIM,space(1))
        .w_ANSAGINT = NVL(ANSAGINT,0)
        .w_ANSPRINT = NVL(ANSPRINT,space(1))
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
        .w_ANFLBLVE = NVL(ANFLBLVE,space(1))
        .w_ANVALFID = NVL(ANVALFID,0)
        .w_ANMAXORD = NVL(ANMAXORD,0)
        .w_READFID = IIF( .cFunction<>'Load', .w_ANCODICE ,Space(15) )
          .link_6_10('Load')
        .w_FIDRES1 = .w_ANVALFID-(.w_FIDPAP+.w_FIDESC+.w_FIDESO+.w_FIDORD+.w_FIDDDT+.w_FIDFAT)
        .w_FIDRES2 = .w_ANVALFID-(.w_FIDPAP+.w_FIDESC+.w_FIDESO+.w_FIDORD+.w_FIDDDT+.w_FIDFAT)
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
        .w_ANCODGRU = NVL(ANCODGRU,space(10))
          if link_9_4_joined
            this.w_ANCODGRU = NVL(GRCODICE904,NVL(this.w_ANCODGRU,space(10)))
            this.w_DESGRU = NVL(GRDESCRI904,space(35))
          else
          .link_9_4('Load')
          endif
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
        .w_AN__NOTE = NVL(AN__NOTE,space(0))
        .w_ANRATING = NVL(ANRATING,space(2))
          if link_11_2_joined
            this.w_ANRATING = NVL(RACODICE1102,NVL(this.w_ANRATING,space(2)))
            this.w_RADESCRI = NVL(RADESCRI1102,space(50))
          else
          .link_11_2('Load')
          endif
        .w_ANGIORIT = NVL(ANGIORIT,0)
        .w_ANVOCFIN = NVL(ANVOCFIN,space(6))
          if link_11_7_joined
            this.w_ANVOCFIN = NVL(DFVOCFIN1107,NVL(this.w_ANVOCFIN,space(6)))
            this.w_DFDESCRI = NVL(DFDESCRI1107,space(60))
          else
          .link_11_7('Load')
          endif
        .w_ANESCDOF = NVL(ANESCDOF,space(1))
        .w_ANDESPAR = NVL(ANDESPAR,space(1))
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
        .w_ANDESCR3 = NVL(ANDESCR3,space(60))
        .w_ANDESCR4 = NVL(ANDESCR4,space(60))
        .w_ANINDIR3 = NVL(ANINDIR3,space(35))
        .w_ANINDIR4 = NVL(ANINDIR4,space(35))
        .w_ANLOCAL2 = NVL(ANLOCAL2,space(30))
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'CONTI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_83.enabled = this.oPgFrm.Page1.oPag.oBtn_1_83.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_84.enabled = this.oPgFrm.Page1.oPag.oBtn_1_84.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_85.enabled = this.oPgFrm.Page1.oPag.oBtn_1_85.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_87.enabled = this.oPgFrm.Page1.oPag.oBtn_1_87.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_89.enabled = this.oPgFrm.Page1.oPag.oBtn_1_89.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_94.enabled = this.oPgFrm.Page1.oPag.oBtn_1_94.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_95.enabled = this.oPgFrm.Page1.oPag.oBtn_1_95.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_104.enabled = this.oPgFrm.Page1.oPag.oBtn_1_104.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_105.enabled = this.oPgFrm.Page1.oPag.oBtn_1_105.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_106.enabled = this.oPgFrm.Page1.oPag.oBtn_1_106.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_57.enabled = this.oPgFrm.Page2.oPag.oBtn_2_57.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_59.enabled = this.oPgFrm.Page2.oPag.oBtn_2_59.mCond()
      this.oPgFrm.Page2.oPag.oBtn_2_61.enabled = this.oPgFrm.Page2.oPag.oBtn_2_61.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_56.enabled = this.oPgFrm.Page3.oPag.oBtn_3_56.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_57.enabled = this.oPgFrm.Page3.oPag.oBtn_3_57.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_58.enabled = this.oPgFrm.Page3.oPag.oBtn_3_58.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_59.enabled = this.oPgFrm.Page3.oPag.oBtn_3_59.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_61.enabled = this.oPgFrm.Page3.oPag.oBtn_3_61.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_63.enabled = this.oPgFrm.Page3.oPag.oBtn_3_63.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_65.enabled = this.oPgFrm.Page3.oPag.oBtn_3_65.mCond()
      this.oPgFrm.Page3.oPag.oBtn_3_67.enabled = this.oPgFrm.Page3.oPag.oBtn_3_67.mCond()
      this.oPgFrm.Page4.oPag.oBtn_4_15.enabled = this.oPgFrm.Page4.oPag.oBtn_4_15.mCond()
      this.oPgFrm.Page4.oPag.oBtn_4_16.enabled = this.oPgFrm.Page4.oPag.oBtn_4_16.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_115.enabled = this.oPgFrm.Page1.oPag.oBtn_1_115.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_122.enabled = this.oPgFrm.Page1.oPag.oBtn_1_122.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- gsar_acl
    Local CTRL_CONCON
    CTRL_CONCON= This.GetCtrl("w_ANCONCON")
    CTRL_CONCON.Popola()
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AUTOAZI = space(5)
      .w_AUTO = space(1)
      .w_ANTIPCON = space(1)
      .w_OBTEST = ctod("  /  /  ")
      .w_ANCODICE = space(15)
      .w_ANDESCRI = space(60)
      .w_ANDESCR2 = space(60)
      .w_VALATT = space(20)
      .w_TABKEY = space(8)
      .w_DELIMM = .f.
      .w_ANINDIRI = space(35)
      .w_ANINDIR2 = space(35)
      .w_AN___CAP = space(8)
      .w_ANLOCALI = space(30)
      .w_ANPROVIN = space(2)
      .w_ANNAZION = space(3)
      .w_DESNAZ = space(35)
      .w_CODISO = space(3)
      .w_ANCODFIS = space(16)
      .w_ANPARIVA = space(12)
      .w_ANTELEFO = space(18)
      .w_ANTELFAX = space(18)
      .w_ANNUMCEL = space(18)
      .w_ANINDWEB = space(254)
      .w_AN_EMAIL = space(254)
      .w_AN_SKYPE = space(50)
      .w_AN_EMPEC = space(254)
      .w_ANCODSAL = space(5)
      .w_ANPERFIS = space(1)
      .w_ANCOGNOM = space(50)
      .w_AN__NOME = space(50)
      .w_AN_SESSO = space(1)
      .w_ANLOCNAS = space(30)
      .w_ANPRONAS = space(2)
      .w_ANDATNAS = ctod("  /  /  ")
      .w_ANNUMCAR = space(18)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_ANCHKSTA = space(1)
      .w_ANCHKMAI = space(1)
      .w_ANCHKPEC = space(1)
      .w_ANCHKFAX = space(1)
      .w_ANCHKCPZ = space(1)
      .w_ANCHKWWP = space(1)
      .w_ANCHKPTL = space(1)
      .w_ANCHKFIR = space(1)
      .w_ANCODEST = space(7)
      .w_ANCODCLA = space(5)
      .w_ANCODPEC = space(0)
      .w_ANDTOBSO = ctod("  /  /  ")
      .w_ANDTINVA = ctod("  /  /  ")
      .w_ANTIPSOT = space(1)
      .w_DANOM = .f.
      .w_ERR = space(10)
      .w_CODCOM = space(4)
      .w_CONTA = 0
      .w_TIPRIF = space(1)
      .w_ANCATOPE = space(2)
      .w_CODI = space(15)
      .w_RAG1 = space(40)
      .w_CODAZI1 = space(5)
      .w_ANCONSUP = space(15)
      .w_DESSUP = space(40)
      .w_ANCATCON = space(5)
      .w_DESCON = space(35)
      .w_PROSTU = space(8)
      .w_CODSTU = space(3)
      .w_ANCONRIF = space(15)
      .w_DESORI = space(40)
      .w_ANTIPRIF = space(1)
      .w_ANCODIVA = space(5)
      .w_NULIV = 0
      .w_DESIVA = space(35)
      .w_ANTIPOPE = space(10)
      .w_ANCONCAU = space(15)
      .w_TIDESCRI = space(30)
      .w_ANFLRITE = space(1)
      .w_ANCODIRP = space(5)
      .w_FLIRPE = space(1)
      .w_CAUPRE2 = space(2)
      .w_ANCAURIT = space(2)
      .w_TIPMAS = space(1)
      .w_PERIVA = 0
      .w_DATOBSO = ctod("  /  /  ")
      .w_ANOPETRE = space(1)
      .w_ANTIPPRE = space(1)
      .w_ANFLAACC = space(1)
      .w_ANFLESIG = space(1)
      .w_AFFLINTR = space(1)
      .w_ANIVASOS = space(1)
      .w_ANFLBLLS = space(1)
      .w_ANCOFISC = space(25)
      .w_ANSCIPAG = space(1)
      .w_ANFLSOAL = space(1)
      .w_ANPARTSN = space(1)
      .w_ANFLFIDO = space(1)
      .w_ANFLSGRE = space(1)
      .w_ANFLBODO = space(1)
      .w_ANCODSTU = space(10)
      .w_ANCODSOG = space(8)
      .w_ANCODCAT = space(4)
      .w_TIDTOBSO = ctod("  /  /  ")
      .w_MINCON = space(6)
      .w_MAXCON = space(6)
      .w_DESCAU = space(40)
      .w_DESIRPEF = space(60)
      .w_MASTRO = space(15)
      .w_CATEGORIA = space(5)
      .w_RIFDIC = space(10)
      .w_ANNDIC = space(4)
      .w_DATDIC = ctod("  /  /  ")
      .w_NUMDIC = 0
      .w_CODI = space(15)
      .w_RAG1 = space(40)
      .w_ANCATCOM = space(3)
      .w_DESCAC = space(35)
      .w_ANFLCONA = space(1)
      .w_ANCATSCM = space(5)
      .w_ANFLGCAU = space(1)
      .w_DESSCM = space(35)
      .w_ANTIPFAT = space(1)
      .w_ANGRUPRO = space(5)
      .w_ANCODISO = space(3)
      .w_DESGPP = space(35)
      .w_ANCONCON = space(1)
      .w_FLSCM = space(1)
      .w_FLPRO = space(1)
      .w_ANCODLIN = space(3)
      .w_DESLIN = space(30)
      .w_ANCODVAL = space(3)
      .w_DESVAL = space(35)
      .w_ANNUMLIS = space(5)
      .w_ANCODCUC = space(2)
      .w_AN1SCONT = 0
      .w_ANGRPDEF = space(5)
      .w_AN2SCONT = 0
      .w_ANCODZON = space(3)
      .w_ANCODAG1 = space(5)
      .w_ANTIPOCL = space(5)
      .w_ANMAGTER = space(5)
      .w_ANCODORN = space(15)
      .w_ANCODPOR = space(10)
      .w_ANMTDCLC = space(3)
      .w_ANMCALSI = space(5)
      .w_ANMCALST = space(5)
      .w_DESLIS = space(40)
      .w_VALLIS = space(3)
      .w_IVALIS = space(1)
      .w_DESZON = space(35)
      .w_DESAGE1 = space(35)
      .w_DESMAG = space(30)
      .w_ANBOLFAT = space(1)
      .w_ANPREBOL = space(1)
      .w_ANFLAPCA = space(1)
      .w_ANSCORPO = space(1)
      .w_ANFLCODI = space(1)
      .w_ANCLIPOS = space(1)
      .w_ANFLGCPZ = space(1)
      .w_FLGCPZ = space(1)
      .w_ANFLIMBA = space(1)
      .w_ANFLPRIV = space(1)
      .w_FLPRIV = space(1)
      .w_ANCODESC = space(5)
      .w_DTOBSO = ctod("  /  /  ")
      .w_OLDCLPOS = space(1)
      .w_MSDESIMB = space(40)
      .w_MSDESTRA = space(40)
      .w_DESORN = space(40)
      .w_GDDESCRI = space(40)
      .w_DESCRI = space(40)
      .w_ANMCALSI = space(5)
      .w_ANMCALST = space(5)
      .w_DESTIP = space(60)
      .w_CODI = space(15)
      .w_RAG1 = space(40)
      .w_TIPCC = space(1)
      .w_ANCODPAG = space(5)
      .w_DESPAG = space(30)
      .w_ANGIOFIS = 0
      .w_ANFLINCA = space(1)
      .w_ANSPEINC = 0
      .w_AN1MESCL = 0
      .w_DESMES1 = space(12)
      .w_ANGIOSC1 = 0
      .w_AN2MESCL = 0
      .w_DESMES2 = space(12)
      .w_ANGIOSC2 = 0
      .w_ANCODBAN = space(10)
      .w_DESBAN = space(42)
      .w_ANNUMCOR = space(25)
      .w_ANCINABI = space(1)
      .w_DESBA2 = space(42)
      .w_AN__BBAN = space(30)
      .w_ANCODBA2 = space(15)
      .w_CONSBF = space(1)
      .w_AN__IBAN = space(35)
      .w_ANFLRAGG = space(1)
      .w_ANFLGAVV = space(1)
      .w_OLDANTIIDRI = space(1)
      .w_OLDANIBARID = space(16)
      .w_ANDATAVV = ctod("  /  /  ")
      .w_ANIDRIDY = space(1)
      .w_ANTIIDRI = 0
      .w_ANIBARID = space(16)
      .w_ANPAGFOR = space(5)
      .w_TIPSOT = space(1)
      .w_ANFLACBD = space(1)
      .w_CODPAG = space(5)
      .w_ANGESCON = space(1)
      .w_ANFLGCON = space(1)
      .w_ANPAGPAR = space(5)
      .w_ANDATMOR = ctod("  /  /  ")
      .w_ANFLESIM = space(1)
      .w_ANSAGINT = 0
      .w_ANSPRINT = space(1)
      .w_DESPAR = space(30)
      .w_CODI = space(15)
      .w_RAG1 = space(40)
      .w_CODI = space(15)
      .w_RAG1 = space(40)
      .w_ANFLBLVE = space(1)
      .w_ANVALFID = 0
      .w_ANMAXORD = 0
      .w_READFID = space(15)
      .w_FIDDAT = ctod("  /  /  ")
      .w_FIDPAP = 0
      .w_FIDESO = 0
      .w_FIDESC = 0
      .w_FIDORD = 0
      .w_FIDDDT = 0
      .w_FIDFAT = 0
      .w_FIDRES1 = 0
      .w_FIDRES2 = 0
      .w_CODI = space(15)
      .w_RAG1 = space(40)
      .w_CODI = space(15)
      .w_RAG1 = space(40)
      .w_CODI = space(15)
      .w_RAG1 = space(40)
      .w_ANCODGRU = space(10)
      .w_DESGRU = space(35)
      .w_CODI = space(15)
      .w_RAG1 = space(40)
      .w_AN__NOTE = space(0)
      .w_ANRATING = space(2)
      .w_RADESCRI = space(50)
      .w_ANGIORIT = 0
      .w_ANVOCFIN = space(6)
      .w_DFDESCRI = space(60)
      .w_ANESCDOF = space(1)
      .w_ANDESPAR = space(1)
      .w_CODI = space(15)
      .w_RAG1 = space(40)
      .w_PADESCRI = space(30)
      .w_CONTROLLO = .f.
      .w_CONSUP = space(15)
      .w_ANDESCR3 = space(60)
      .w_ANDESCR4 = space(60)
      .w_ANINDIR3 = space(35)
      .w_ANINDIR4 = space(35)
      .w_ANLOCAL2 = space(30)
      if .cFunction<>"Filter"
        .w_AUTOAZI = i_CODAZI
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_AUTOAZI))
          .link_1_1('Full')
          endif
        .w_AUTO = ' '
        .w_ANTIPCON = 'C'
        .w_OBTEST = i_datsys
        .w_ANCODICE = iif(.cFunction='Load' AND .w_AUTO='S', 'AUTO',.w_ANCODICE)
          .DoRTCalc(6,7,.f.)
        .w_VALATT = "C"+.w_ANCODICE
        .w_TABKEY = 'CONTI'
        .w_DELIMM = .f.
        .DoRTCalc(11,16,.f.)
          if not(empty(.w_ANNAZION))
          .link_1_16('Full')
          endif
        .DoRTCalc(17,28,.f.)
          if not(empty(.w_ANCODSAL))
          .link_1_28('Full')
          endif
        .w_ANPERFIS = 'N'
          .DoRTCalc(30,31,.f.)
        .w_AN_SESSO = 'M'
          .DoRTCalc(33,47,.f.)
        .w_ANCHKFIR = iif(.w_ANCHKMAI='S' or .w_ANCHKPEC='S' or .w_ANCHKCPZ = 'S',.w_ANCHKFIR,'N')
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_92.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
          .DoRTCalc(49,57,.f.)
        .w_CONTA = .w_CONTA
        .w_TIPRIF = 'F'
        .w_ANCATOPE = 'CF'
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
        .w_CODAZI1 = i_codazi
        .DoRTCalc(63,63,.f.)
          if not(empty(.w_CODAZI1))
          .link_2_6('Full')
          endif
        .w_ANCONSUP = iif(.cFunction='Query',Space(15),.w_MASTRO)
        .DoRTCalc(64,64,.f.)
          if not(empty(.w_ANCONSUP))
          .link_2_7('Full')
          endif
          .DoRTCalc(65,65,.f.)
        .w_ANCATCON = iif(.cFunction='Query',Space(5),.w_CATEGORIA)
        .DoRTCalc(66,66,.f.)
          if not(empty(.w_ANCATCON))
          .link_2_9('Full')
          endif
        .DoRTCalc(67,70,.f.)
          if not(empty(.w_ANCONRIF))
          .link_2_14('Full')
          endif
          .DoRTCalc(71,71,.f.)
        .w_ANTIPRIF = IIF(EMPTY(.w_ANCONRIF), ' ', 'F')
        .DoRTCalc(73,73,.f.)
          if not(empty(.w_ANCODIVA))
          .link_2_17('Full')
          endif
        .DoRTCalc(74,76,.f.)
          if not(empty(.w_ANTIPOPE))
          .link_2_20('Full')
          endif
        .DoRTCalc(77,77,.f.)
          if not(empty(.w_ANCONCAU))
          .link_2_21('Full')
          endif
          .DoRTCalc(78,79,.f.)
        .w_ANCODIRP = IIF(.w_ANFLRITE='S',IIF(IsAlt(),(READALTE('I')),.w_ANCODIRP),space(5))
        .DoRTCalc(80,80,.f.)
          if not(empty(.w_ANCODIRP))
          .link_2_24('Full')
          endif
          .DoRTCalc(81,82,.f.)
        .w_ANCAURIT = .w_CAUPRE2
          .DoRTCalc(84,86,.f.)
        .w_ANOPETRE = 'N'
        .w_ANTIPPRE = iif(isalt(), 'S', 'N')
          .DoRTCalc(89,89,.f.)
        .w_ANFLESIG = 'N'
          .DoRTCalc(91,91,.f.)
        .w_ANIVASOS = 'N'
        .w_ANFLBLLS = 'N'
        .w_ANCOFISC = iif(.w_ANFLBLLS='S',.w_ANCOFISC,SPACE(25))
        .w_ANSCIPAG = 'N'
        .w_ANFLSOAL = 'N'
        .w_ANPARTSN = g_PERPAR
          .DoRTCalc(98,98,.f.)
        .w_ANFLSGRE = 'N'
        .w_ANFLBODO = iif(.w_AFFLINTR='S' or .w_ANFLSOAL='S' or g_TRAEXP = 'N',' ',.w_ANFLBODO)
        .w_ANCODSTU = IIF(g_TRAEXP<>'G',SPACE(9),.w_ANCODSTU)
        .DoRTCalc(102,103,.f.)
          if not(empty(.w_ANCODCAT))
          .link_2_55('Full')
          endif
          .DoRTCalc(104,110,.f.)
        .w_RIFDIC = ''
        .w_ANNDIC = ''
        .w_DATDIC = ''
        .w_NUMDIC = ''
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
        .DoRTCalc(117,117,.f.)
          if not(empty(.w_ANCATCOM))
          .link_3_3('Full')
          endif
          .DoRTCalc(118,118,.f.)
        .w_ANFLCONA = 'U'
        .DoRTCalc(120,120,.f.)
          if not(empty(.w_ANCATSCM))
          .link_3_6('Full')
          endif
        .w_ANFLGCAU = 'N'
          .DoRTCalc(122,122,.f.)
        .w_ANTIPFAT = "R"
        .DoRTCalc(124,124,.f.)
          if not(empty(.w_ANGRUPRO))
          .link_3_10('Full')
          endif
        .w_ANCODISO = g_ISONAZ
          .DoRTCalc(126,126,.f.)
        .w_ANCONCON = IIF(g_ISONAZ='ITA','E','1')
        .DoRTCalc(127,127,.f.)
          if not(empty(.w_ANCONCON))
          .link_3_14('Full')
          endif
          .DoRTCalc(128,129,.f.)
        .w_ANCODLIN = g_CODLIN
        .DoRTCalc(130,130,.f.)
          if not(empty(.w_ANCODLIN))
          .link_3_17('Full')
          endif
        .DoRTCalc(131,132,.f.)
          if not(empty(.w_ANCODVAL))
          .link_3_19('Full')
          endif
        .DoRTCalc(133,134,.f.)
          if not(empty(.w_ANNUMLIS))
          .link_3_21('Full')
          endif
        .DoRTCalc(135,137,.f.)
          if not(empty(.w_ANGRPDEF))
          .link_3_24('Full')
          endif
        .w_AN2SCONT = 0
        .DoRTCalc(139,139,.f.)
          if not(empty(.w_ANCODZON))
          .link_3_26('Full')
          endif
        .DoRTCalc(140,140,.f.)
          if not(empty(.w_ANCODAG1))
          .link_3_27('Full')
          endif
        .DoRTCalc(141,141,.f.)
          if not(empty(.w_ANTIPOCL))
          .link_3_28('Full')
          endif
        .DoRTCalc(142,142,.f.)
          if not(empty(.w_ANMAGTER))
          .link_3_29('Full')
          endif
        .DoRTCalc(143,143,.f.)
          if not(empty(.w_ANCODORN))
          .link_3_30('Full')
          endif
        .DoRTCalc(144,144,.f.)
          if not(empty(.w_ANCODPOR))
          .link_3_31('Full')
          endif
        .DoRTCalc(145,145,.f.)
          if not(empty(.w_ANMTDCLC))
          .link_3_32('Full')
          endif
        .DoRTCalc(146,146,.f.)
          if not(empty(.w_ANMCALSI))
          .link_3_33('Full')
          endif
        .DoRTCalc(147,147,.f.)
          if not(empty(.w_ANMCALST))
          .link_3_34('Full')
          endif
          .DoRTCalc(148,153,.f.)
        .w_ANBOLFAT = "N"
        .w_ANPREBOL = "N"
          .DoRTCalc(156,156,.f.)
        .w_ANSCORPO = "N"
          .DoRTCalc(158,158,.f.)
        .w_ANCLIPOS = ' '
        .w_ANFLGCPZ = iif(g_REVI='S', 'S', 'N')
        .w_FLGCPZ = .w_ANFLGCPZ
        .w_ANFLIMBA = 'N'
        .w_ANFLPRIV = 'N'
        .w_FLPRIV = .w_ANFLPRIV
          .DoRTCalc(165,166,.f.)
        .w_OLDCLPOS = .w_ANCLIPOS
        .DoRTCalc(168,173,.f.)
          if not(empty(.w_ANMCALSI))
          .link_3_97('Full')
          endif
        .DoRTCalc(174,174,.f.)
          if not(empty(.w_ANMCALST))
          .link_3_98('Full')
          endif
          .DoRTCalc(175,175,.f.)
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
        .w_TIPCC = 'G'
        .w_ANCODPAG = iif(.cFunction='Query',Space(5),.w_CODPAG)
        .DoRTCalc(179,179,.f.)
          if not(empty(.w_ANCODPAG))
          .link_4_4('Full')
          endif
          .DoRTCalc(180,184,.f.)
        .w_DESMES1 = LEFT(IIF(.w_AN1MESCL>0 AND .w_AN1MESCL<13, g_MESE[.w_AN1MESCL], "")+SPACE(12),12)
        .w_ANGIOSC1 = 0
        .w_AN2MESCL = 0
        .w_DESMES2 = LEFT(IIF(.w_AN2MESCL>0 AND .w_AN2MESCL<13, g_MESE[.w_AN2MESCL], "")+SPACE(12),12)
        .w_ANGIOSC2 = 0
        .DoRTCalc(190,190,.f.)
          if not(empty(.w_ANCODBAN))
          .link_4_17('Full')
          endif
        .DoRTCalc(191,196,.f.)
          if not(empty(.w_ANCODBA2))
          .link_4_23('Full')
          endif
          .DoRTCalc(197,199,.f.)
        .w_ANFLGAVV = 'N'
        .w_OLDANTIIDRI = 1
        .w_OLDANIBARID = ''
          .DoRTCalc(203,203,.f.)
        .w_ANIDRIDY = 'N'
        .w_ANTIIDRI = 1
        .w_ANIBARID = ''
        .DoRTCalc(207,207,.f.)
          if not(empty(.w_ANPAGFOR))
          .link_4_36('Full')
          endif
          .DoRTCalc(208,208,.f.)
        .w_ANFLACBD = ' '
          .DoRTCalc(210,210,.f.)
        .w_ANGESCON = 'N'
        .w_ANFLGCON = IIF(.w_ANGESCON<>'B',' ',.w_ANFLGCON)
        .w_ANPAGPAR = iif(.w_ANGESCON<>'M',SPACE(5),.w_ANPAGPAR)
        .DoRTCalc(213,213,.f.)
          if not(empty(.w_ANPAGPAR))
          .link_5_3('Full')
          endif
        .w_ANDATMOR = cp_CharToDate('  -  -  ')
          .DoRTCalc(215,215,.f.)
        .w_ANSAGINT = IIF(.w_ANFLESIM='S', 0, .w_ANSAGINT)
        .w_ANSPRINT = IIF(.w_ANFLESIM='S' OR .w_ANSAGINT=0,'N',.w_ANSPRINT)
          .DoRTCalc(218,218,.f.)
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
          .DoRTCalc(223,225,.f.)
        .w_READFID = IIF( .cFunction<>'Load', .w_ANCODICE ,Space(15) )
        .DoRTCalc(226,226,.f.)
          if not(empty(.w_READFID))
          .link_6_10('Full')
          endif
          .DoRTCalc(227,233,.f.)
        .w_FIDRES1 = .w_ANVALFID-(.w_FIDPAP+.w_FIDESC+.w_FIDESO+.w_FIDORD+.w_FIDDDT+.w_FIDFAT)
        .w_FIDRES2 = .w_ANVALFID-(.w_FIDPAP+.w_FIDESC+.w_FIDESO+.w_FIDORD+.w_FIDDDT+.w_FIDFAT)
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
        .DoRTCalc(242,242,.f.)
          if not(empty(.w_ANCODGRU))
          .link_9_4('Full')
          endif
          .DoRTCalc(243,243,.f.)
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
        .DoRTCalc(246,247,.f.)
          if not(empty(.w_ANRATING))
          .link_11_2('Full')
          endif
        .DoRTCalc(248,250,.f.)
          if not(empty(.w_ANVOCFIN))
          .link_11_7('Full')
          endif
          .DoRTCalc(251,251,.f.)
        .w_ANESCDOF = 'N'
        .w_ANDESPAR = 'N'
        .w_CODI = .w_ANCODICE
        .w_RAG1 = .w_ANDESCRI
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONTI')
    this.DoRTCalc(256,263,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_83.enabled = this.oPgFrm.Page1.oPag.oBtn_1_83.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_84.enabled = this.oPgFrm.Page1.oPag.oBtn_1_84.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_85.enabled = this.oPgFrm.Page1.oPag.oBtn_1_85.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_87.enabled = this.oPgFrm.Page1.oPag.oBtn_1_87.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_89.enabled = this.oPgFrm.Page1.oPag.oBtn_1_89.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_94.enabled = this.oPgFrm.Page1.oPag.oBtn_1_94.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_95.enabled = this.oPgFrm.Page1.oPag.oBtn_1_95.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_104.enabled = this.oPgFrm.Page1.oPag.oBtn_1_104.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_105.enabled = this.oPgFrm.Page1.oPag.oBtn_1_105.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_106.enabled = this.oPgFrm.Page1.oPag.oBtn_1_106.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_57.enabled = this.oPgFrm.Page2.oPag.oBtn_2_57.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_59.enabled = this.oPgFrm.Page2.oPag.oBtn_2_59.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_61.enabled = this.oPgFrm.Page2.oPag.oBtn_2_61.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_56.enabled = this.oPgFrm.Page3.oPag.oBtn_3_56.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_57.enabled = this.oPgFrm.Page3.oPag.oBtn_3_57.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_58.enabled = this.oPgFrm.Page3.oPag.oBtn_3_58.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_59.enabled = this.oPgFrm.Page3.oPag.oBtn_3_59.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_61.enabled = this.oPgFrm.Page3.oPag.oBtn_3_61.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_63.enabled = this.oPgFrm.Page3.oPag.oBtn_3_63.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_65.enabled = this.oPgFrm.Page3.oPag.oBtn_3_65.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_67.enabled = this.oPgFrm.Page3.oPag.oBtn_3_67.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_15.enabled = this.oPgFrm.Page4.oPag.oBtn_4_15.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_16.enabled = this.oPgFrm.Page4.oPag.oBtn_4_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_115.enabled = this.oPgFrm.Page1.oPag.oBtn_1_115.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_122.enabled = this.oPgFrm.Page1.oPag.oBtn_1_122.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gsar_acl
    Local CTRL_CONCON
    CTRL_CONCON= This.GetCtrl("w_ANCONCON")
    CTRL_CONCON.Popola()
    if UPPER(this.cfunction)='LOAD'
       this.notifyEvent('CaricaDati')
    endif
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    with this	
      if g_CFNUME='S' AND .p_AUT=0
        cp_AskTableProg(this,i_nConn,"PRNUCL","i_codazi,w_ANCODICE")
      endif
      .op_codazi = .w_codazi
      .op_ANCODICE = .w_ANCODICE
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oANCODICE_1_5.enabled = i_bVal
      .Page1.oPag.oANDESCRI_1_6.enabled = i_bVal
      .Page1.oPag.oANDESCR2_1_7.enabled = i_bVal
      .Page1.oPag.oANINDIRI_1_11.enabled = i_bVal
      .Page1.oPag.oANINDIR2_1_12.enabled = i_bVal
      .Page1.oPag.oAN___CAP_1_13.enabled = i_bVal
      .Page1.oPag.oANLOCALI_1_14.enabled = i_bVal
      .Page1.oPag.oANPROVIN_1_15.enabled = i_bVal
      .Page1.oPag.oANNAZION_1_16.enabled = i_bVal
      .Page1.oPag.oANCODFIS_1_19.enabled = i_bVal
      .Page1.oPag.oANPARIVA_1_20.enabled = i_bVal
      .Page1.oPag.oANTELEFO_1_21.enabled = i_bVal
      .Page1.oPag.oANTELFAX_1_22.enabled = i_bVal
      .Page1.oPag.oANNUMCEL_1_23.enabled = i_bVal
      .Page1.oPag.oANINDWEB_1_24.enabled = i_bVal
      .Page1.oPag.oAN_EMAIL_1_25.enabled = i_bVal
      .Page1.oPag.oAN_SKYPE_1_26.enabled = i_bVal
      .Page1.oPag.oAN_EMPEC_1_27.enabled = i_bVal
      .Page1.oPag.oANCODSAL_1_28.enabled = i_bVal
      .Page1.oPag.oANPERFIS_1_29.enabled = i_bVal
      .Page1.oPag.oANCOGNOM_1_30.enabled = i_bVal
      .Page1.oPag.oAN__NOME_1_31.enabled = i_bVal
      .Page1.oPag.oAN_SESSO_1_32.enabled = i_bVal
      .Page1.oPag.oANLOCNAS_1_33.enabled = i_bVal
      .Page1.oPag.oANPRONAS_1_34.enabled = i_bVal
      .Page1.oPag.oANDATNAS_1_35.enabled = i_bVal
      .Page1.oPag.oANNUMCAR_1_37.enabled = i_bVal
      .Page1.oPag.oANCHKSTA_1_57.enabled = i_bVal
      .Page1.oPag.oANCHKMAI_1_58.enabled = i_bVal
      .Page1.oPag.oANCHKPEC_1_59.enabled = i_bVal
      .Page1.oPag.oANCHKFAX_1_60.enabled = i_bVal
      .Page1.oPag.oANCHKCPZ_1_61.enabled = i_bVal
      .Page1.oPag.oANCHKWWP_1_62.enabled = i_bVal
      .Page1.oPag.oANCHKPTL_1_63.enabled = i_bVal
      .Page1.oPag.oANCHKFIR_1_64.enabled = i_bVal
      .Page1.oPag.oANCODEST_1_65.enabled = i_bVal
      .Page1.oPag.oANCODCLA_1_66.enabled = i_bVal
      .Page1.oPag.oANCODPEC_1_67.enabled = i_bVal
      .Page1.oPag.oANDTOBSO_1_68.enabled = i_bVal
      .Page1.oPag.oANDTINVA_1_72.enabled = i_bVal
      .Page2.oPag.oANCONSUP_2_7.enabled = i_bVal
      .Page2.oPag.oANCATCON_2_9.enabled = i_bVal
      .Page2.oPag.oANCONRIF_2_14.enabled = i_bVal
      .Page2.oPag.oANCODIVA_2_17.enabled = i_bVal
      .Page2.oPag.oANTIPOPE_2_20.enabled = i_bVal
      .Page2.oPag.oANCONCAU_2_21.enabled = i_bVal
      .Page2.oPag.oANFLRITE_2_23.enabled = i_bVal
      .Page2.oPag.oANCODIRP_2_24.enabled = i_bVal
      .Page2.oPag.oANCAURIT_2_27.enabled = i_bVal
      .Page2.oPag.oANOPETRE_2_36.enabled = i_bVal
      .Page2.oPag.oANTIPPRE_2_38.enabled = i_bVal
      .Page2.oPag.oANFLAACC_2_40.enabled = i_bVal
      .Page2.oPag.oANFLESIG_2_41.enabled = i_bVal
      .Page2.oPag.oAFFLINTR_2_42.enabled = i_bVal
      .Page2.oPag.oANIVASOS_2_43.enabled = i_bVal
      .Page2.oPag.oANFLBLLS_2_44.enabled = i_bVal
      .Page2.oPag.oANCOFISC_2_45.enabled = i_bVal
      .Page2.oPag.oANSCIPAG_2_46.enabled = i_bVal
      .Page2.oPag.oANFLSOAL_2_47.enabled = i_bVal
      .Page2.oPag.oANPARTSN_2_48.enabled = i_bVal
      .Page2.oPag.oANFLFIDO_2_49.enabled = i_bVal
      .Page2.oPag.oANFLSGRE_2_50.enabled = i_bVal
      .Page2.oPag.oANFLBODO_2_51.enabled = i_bVal
      .Page2.oPag.oANCODSTU_2_52.enabled = i_bVal
      .Page2.oPag.oANCODSOG_2_54.enabled = i_bVal
      .Page2.oPag.oANCODCAT_2_55.enabled = i_bVal
      .Page3.oPag.oANCATCOM_3_3.enabled = i_bVal
      .Page3.oPag.oANFLCONA_3_5.enabled = i_bVal
      .Page3.oPag.oANCATSCM_3_6.enabled = i_bVal
      .Page3.oPag.oANFLGCAU_3_7.enabled = i_bVal
      .Page3.oPag.oANTIPFAT_3_9.enabled = i_bVal
      .Page3.oPag.oANGRUPRO_3_10.enabled = i_bVal
      .Page3.oPag.oANCONCON_3_14.enabled = i_bVal
      .Page3.oPag.oANCODLIN_3_17.enabled = i_bVal
      .Page3.oPag.oANCODVAL_3_19.enabled = i_bVal
      .Page3.oPag.oANNUMLIS_3_21.enabled = i_bVal
      .Page3.oPag.oANCODCUC_3_22.enabled = i_bVal
      .Page3.oPag.oAN1SCONT_3_23.enabled = i_bVal
      .Page3.oPag.oANGRPDEF_3_24.enabled = i_bVal
      .Page3.oPag.oAN2SCONT_3_25.enabled = i_bVal
      .Page3.oPag.oANCODZON_3_26.enabled = i_bVal
      .Page3.oPag.oANCODAG1_3_27.enabled = i_bVal
      .Page3.oPag.oANTIPOCL_3_28.enabled = i_bVal
      .Page3.oPag.oANMAGTER_3_29.enabled = i_bVal
      .Page3.oPag.oANCODORN_3_30.enabled = i_bVal
      .Page3.oPag.oANCODPOR_3_31.enabled = i_bVal
      .Page3.oPag.oANMTDCLC_3_32.enabled = i_bVal
      .Page3.oPag.oANMCALSI_3_33.enabled = i_bVal
      .Page3.oPag.oANMCALST_3_34.enabled = i_bVal
      .Page3.oPag.oANBOLFAT_3_41.enabled = i_bVal
      .Page3.oPag.oANPREBOL_3_42.enabled = i_bVal
      .Page3.oPag.oANFLAPCA_3_43.enabled = i_bVal
      .Page3.oPag.oANSCORPO_3_45.enabled = i_bVal
      .Page3.oPag.oANFLCODI_3_46.enabled = i_bVal
      .Page3.oPag.oANCLIPOS_3_47.enabled = i_bVal
      .Page3.oPag.oANFLGCPZ_3_48.enabled = i_bVal
      .Page3.oPag.oANFLIMBA_3_50.enabled = i_bVal
      .Page3.oPag.oANFLPRIV_3_51.enabled = i_bVal
      .Page3.oPag.oANCODESC_3_54.enabled = i_bVal
      .Page3.oPag.oANMCALSI_3_97.enabled = i_bVal
      .Page3.oPag.oANMCALST_3_98.enabled = i_bVal
      .Page4.oPag.oANCODPAG_4_4.enabled = i_bVal
      .Page4.oPag.oANGIOFIS_4_6.enabled = i_bVal
      .Page4.oPag.oANFLINCA_4_7.enabled = i_bVal
      .Page4.oPag.oANSPEINC_4_8.enabled = i_bVal
      .Page4.oPag.oAN1MESCL_4_9.enabled = i_bVal
      .Page4.oPag.oANGIOSC1_4_11.enabled = i_bVal
      .Page4.oPag.oAN2MESCL_4_12.enabled = i_bVal
      .Page4.oPag.oANGIOSC2_4_14.enabled = i_bVal
      .Page4.oPag.oANCODBAN_4_17.enabled = i_bVal
      .Page4.oPag.oANCODBA2_4_23.enabled = i_bVal
      .Page4.oPag.oANFLRAGG_4_27.enabled = i_bVal
      .Page4.oPag.oANFLGAVV_4_28.enabled = i_bVal
      .Page4.oPag.oANDATAVV_4_32.enabled = i_bVal
      .Page4.oPag.oANIDRIDY_4_33.enabled = i_bVal
      .Page4.oPag.oANTIIDRI_4_34.enabled = i_bVal
      .Page4.oPag.oANIBARID_4_35.enabled = i_bVal
      .Page4.oPag.oANPAGFOR_4_36.enabled = i_bVal
      .Page5.oPag.oANGESCON_5_1.enabled = i_bVal
      .Page5.oPag.oANFLGCON_5_2.enabled = i_bVal
      .Page5.oPag.oANPAGPAR_5_3.enabled = i_bVal
      .Page5.oPag.oANDATMOR_5_4.enabled = i_bVal
      .Page5.oPag.oANFLESIM_5_5.enabled = i_bVal
      .Page5.oPag.oANSAGINT_5_6.enabled = i_bVal
      .Page5.oPag.oANSPRINT_5_7.enabled = i_bVal
      .Page6.oPag.oANFLBLVE_6_3.enabled = i_bVal
      .Page6.oPag.oANVALFID_6_4.enabled = i_bVal
      .Page6.oPag.oANMAXORD_6_5.enabled = i_bVal
      .Page9.oPag.oANCODGRU_9_4.enabled = i_bVal
      .Page10.oPag.oAN__NOTE_10_4.enabled = i_bVal
      .Page11.oPag.oANRATING_11_2.enabled = i_bVal
      .Page11.oPag.oANGIORIT_11_6.enabled = i_bVal
      .Page11.oPag.oANVOCFIN_11_7.enabled = i_bVal
      .Page11.oPag.oANESCDOF_11_9.enabled = i_bVal
      .Page11.oPag.oANDESPAR_11_10.enabled = i_bVal
      .Page1.oPag.oBtn_1_83.enabled = .Page1.oPag.oBtn_1_83.mCond()
      .Page1.oPag.oBtn_1_84.enabled = .Page1.oPag.oBtn_1_84.mCond()
      .Page1.oPag.oBtn_1_85.enabled = .Page1.oPag.oBtn_1_85.mCond()
      .Page1.oPag.oBtn_1_87.enabled = .Page1.oPag.oBtn_1_87.mCond()
      .Page1.oPag.oBtn_1_89.enabled = .Page1.oPag.oBtn_1_89.mCond()
      .Page1.oPag.oBtn_1_94.enabled = .Page1.oPag.oBtn_1_94.mCond()
      .Page1.oPag.oBtn_1_95.enabled = .Page1.oPag.oBtn_1_95.mCond()
      .Page1.oPag.oBtn_1_101.enabled = i_bVal
      .Page1.oPag.oBtn_1_104.enabled = .Page1.oPag.oBtn_1_104.mCond()
      .Page1.oPag.oBtn_1_105.enabled = .Page1.oPag.oBtn_1_105.mCond()
      .Page1.oPag.oBtn_1_106.enabled = .Page1.oPag.oBtn_1_106.mCond()
      .Page2.oPag.oBtn_2_53.enabled = i_bVal
      .Page2.oPag.oBtn_2_57.enabled = .Page2.oPag.oBtn_2_57.mCond()
      .Page2.oPag.oBtn_2_59.enabled = .Page2.oPag.oBtn_2_59.mCond()
      .Page2.oPag.oBtn_2_61.enabled = .Page2.oPag.oBtn_2_61.mCond()
      .Page3.oPag.oBtn_3_56.enabled = .Page3.oPag.oBtn_3_56.mCond()
      .Page3.oPag.oBtn_3_57.enabled = .Page3.oPag.oBtn_3_57.mCond()
      .Page3.oPag.oBtn_3_58.enabled = .Page3.oPag.oBtn_3_58.mCond()
      .Page3.oPag.oBtn_3_59.enabled = .Page3.oPag.oBtn_3_59.mCond()
      .Page3.oPag.oBtn_3_61.enabled = .Page3.oPag.oBtn_3_61.mCond()
      .Page3.oPag.oBtn_3_63.enabled = .Page3.oPag.oBtn_3_63.mCond()
      .Page3.oPag.oBtn_3_65.enabled = .Page3.oPag.oBtn_3_65.mCond()
      .Page3.oPag.oBtn_3_67.enabled = .Page3.oPag.oBtn_3_67.mCond()
      .Page4.oPag.oBtn_4_15.enabled = .Page4.oPag.oBtn_4_15.mCond()
      .Page4.oPag.oBtn_4_16.enabled = .Page4.oPag.oBtn_4_16.mCond()
      .Page1.oPag.oBtn_1_115.enabled = .Page1.oPag.oBtn_1_115.mCond()
      .Page1.oPag.oBtn_1_122.enabled = .Page1.oPag.oBtn_1_122.mCond()
      .Page2.oPag.oBtn_2_89.enabled = i_bVal
      .Page1.oPag.oObj_1_74.enabled = i_bVal
      .Page1.oPag.oObj_1_76.enabled = i_bVal
      .Page1.oPag.oObj_1_77.enabled = i_bVal
      .Page1.oPag.oObj_1_78.enabled = i_bVal
      .Page1.oPag.oObj_1_79.enabled = i_bVal
      .Page1.oPag.oObj_1_80.enabled = i_bVal
      .Page1.oPag.oObj_1_92.enabled = i_bVal
      .Page1.oPag.oObj_1_93.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oANCODICE_1_5.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oANCODICE_1_5.enabled = .t.
        .Page1.oPag.oANDESCRI_1_6.enabled = .t.
        .Page1.oPag.oANPARIVA_1_20.enabled = .t.
      endif
    endwith
    this.GSAR_MSA.SetStatus(i_cOp)
    this.GSAR_MIN.SetStatus(i_cOp)
    this.GSAR_MCC.SetStatus(i_cOp)
    this.GSAR_MCO.SetStatus(i_cOp)
    this.GSAR_MDD.SetStatus(i_cOp)
    this.GSAR_MSE.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'CONTI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAR_MSA.SetChildrenStatus(i_cOp)
  *  this.GSAR_MIN.SetChildrenStatus(i_cOp)
  *  this.GSAR_MCC.SetChildrenStatus(i_cOp)
  *  this.GSAR_MCO.SetChildrenStatus(i_cOp)
  *  this.GSAR_MDD.SetChildrenStatus(i_cOp)
  *  this.GSAR_MSE.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPCON,"ANTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODICE,"ANCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDESCRI,"ANDESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDESCR2,"ANDESCR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANINDIRI,"ANINDIRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANINDIR2,"ANINDIR2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN___CAP,"AN___CAP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANLOCALI,"ANLOCALI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANPROVIN,"ANPROVIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANNAZION,"ANNAZION",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODFIS,"ANCODFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANPARIVA,"ANPARIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTELEFO,"ANTELEFO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTELFAX,"ANTELFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANNUMCEL,"ANNUMCEL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANINDWEB,"ANINDWEB",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN_EMAIL,"AN_EMAIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN_SKYPE,"AN_SKYPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN_EMPEC,"AN_EMPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODSAL,"ANCODSAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANPERFIS,"ANPERFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCOGNOM,"ANCOGNOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN__NOME,"AN__NOME",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN_SESSO,"AN_SESSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANLOCNAS,"ANLOCNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANPRONAS,"ANPRONAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDATNAS,"ANDATNAS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANNUMCAR,"ANNUMCAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCHKSTA,"ANCHKSTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCHKMAI,"ANCHKMAI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCHKPEC,"ANCHKPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCHKFAX,"ANCHKFAX",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCHKCPZ,"ANCHKCPZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCHKWWP,"ANCHKWWP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCHKPTL,"ANCHKPTL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCHKFIR,"ANCHKFIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODEST,"ANCODEST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODCLA,"ANCODCLA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODPEC,"ANCODPEC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDTOBSO,"ANDTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDTINVA,"ANDTINVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPSOT,"ANTIPSOT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCATOPE,"ANCATOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCONSUP,"ANCONSUP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCATCON,"ANCATCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCONRIF,"ANCONRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPRIF,"ANTIPRIF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODIVA,"ANCODIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPOPE,"ANTIPOPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCONCAU,"ANCONCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLRITE,"ANFLRITE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODIRP,"ANCODIRP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCAURIT,"ANCAURIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANOPETRE,"ANOPETRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPPRE,"ANTIPPRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLAACC,"ANFLAACC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLESIG,"ANFLESIG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AFFLINTR,"AFFLINTR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANIVASOS,"ANIVASOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLBLLS,"ANFLBLLS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCOFISC,"ANCOFISC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANSCIPAG,"ANSCIPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLSOAL,"ANFLSOAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANPARTSN,"ANPARTSN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLFIDO,"ANFLFIDO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLSGRE,"ANFLSGRE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLBODO,"ANFLBODO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODSTU,"ANCODSTU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODSOG,"ANCODSOG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODCAT,"ANCODCAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCATCOM,"ANCATCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLCONA,"ANFLCONA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCATSCM,"ANCATSCM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLGCAU,"ANFLGCAU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPFAT,"ANTIPFAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANGRUPRO,"ANGRUPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCONCON,"ANCONCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODLIN,"ANCODLIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODVAL,"ANCODVAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANNUMLIS,"ANNUMLIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODCUC,"ANCODCUC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN1SCONT,"AN1SCONT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANGRPDEF,"ANGRPDEF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN2SCONT,"AN2SCONT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODZON,"ANCODZON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODAG1,"ANCODAG1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIPOCL,"ANTIPOCL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANMAGTER,"ANMAGTER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODORN,"ANCODORN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODPOR,"ANCODPOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANMTDCLC,"ANMTDCLC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANMCALSI,"ANMCALSI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANMCALST,"ANMCALST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANBOLFAT,"ANBOLFAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANPREBOL,"ANPREBOL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLAPCA,"ANFLAPCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANSCORPO,"ANSCORPO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLCODI,"ANFLCODI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCLIPOS,"ANCLIPOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLGCPZ,"ANFLGCPZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLIMBA,"ANFLIMBA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLPRIV,"ANFLPRIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODESC,"ANCODESC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANMCALSI,"ANMCALSI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANMCALST,"ANMCALST",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODPAG,"ANCODPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANGIOFIS,"ANGIOFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLINCA,"ANFLINCA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANSPEINC,"ANSPEINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN1MESCL,"AN1MESCL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANGIOSC1,"ANGIOSC1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN2MESCL,"AN2MESCL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANGIOSC2,"ANGIOSC2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODBAN,"ANCODBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANNUMCOR,"ANNUMCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCINABI,"ANCINABI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN__BBAN,"AN__BBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODBA2,"ANCODBA2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN__IBAN,"AN__IBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLRAGG,"ANFLRAGG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLGAVV,"ANFLGAVV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDATAVV,"ANDATAVV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANIDRIDY,"ANIDRIDY",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANTIIDRI,"ANTIIDRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANIBARID,"ANIBARID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANPAGFOR,"ANPAGFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLACBD,"ANFLACBD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANGESCON,"ANGESCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLGCON,"ANFLGCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANPAGPAR,"ANPAGPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDATMOR,"ANDATMOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLESIM,"ANFLESIM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANSAGINT,"ANSAGINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANSPRINT,"ANSPRINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANFLBLVE,"ANFLBLVE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANVALFID,"ANVALFID",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANMAXORD,"ANMAXORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANCODGRU,"ANCODGRU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AN__NOTE,"AN__NOTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANRATING,"ANRATING",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANGIORIT,"ANGIORIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANVOCFIN,"ANVOCFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANESCDOF,"ANESCDOF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDESPAR,"ANDESPAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDESCR3,"ANDESCR3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANDESCR4,"ANDESCR4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANINDIR3,"ANINDIR3",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANINDIR4,"ANINDIR4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_ANLOCAL2,"ANLOCAL2",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- gsar_acl
    * --- aggiunge alla Chiave ulteriore filtro su Tipo Conto
    IF NOT EMPTY(i_cWhere)
       IF AT('ANTIPSOT', UPPER(i_cWhere))=0
          i_cWhere=i_cWhere+" and ANTIPCON='C'"
       ENDIF
    ENDIF
    
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    i_lTable = "CONTI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.CONTI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      with this
        PrintConto(this,"C")
      endwith
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- gsar_acl
    ** Previene eventuale inserimento di Codici non numerici dal calcolo Autonumber
    local p_POS, p_COD
    p_COD = ALLTRIM(this.w_ANCODICE)
    FOR p_POS=1 TO LEN(p_COD)
       this.p_AUT = IIF(SUBSTR(p_COD, p_POS, 1)$'0123456789', this.p_AUT, 1)
    ENDFOR
    
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CONTI_IDX,i_nConn)
      with this
        if g_CFNUME='S' AND .p_AUT=0
          cp_NextTableProg(this,i_nConn,"PRNUCL","i_codazi,w_ANCODICE")
        endif
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CONTI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONTI')
        i_extval=cp_InsertValODBCExtFlds(this,'CONTI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(ANTIPCON,ANCODICE,ANDESCRI,ANDESCR2,ANINDIRI"+;
                  ",ANINDIR2,AN___CAP,ANLOCALI,ANPROVIN,ANNAZION"+;
                  ",ANCODFIS,ANPARIVA,ANTELEFO,ANTELFAX,ANNUMCEL"+;
                  ",ANINDWEB,AN_EMAIL,AN_SKYPE,AN_EMPEC,ANCODSAL"+;
                  ",ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANLOCNAS"+;
                  ",ANPRONAS,ANDATNAS,ANNUMCAR,UTCC,UTCV"+;
                  ",UTDC,UTDV,ANCHKSTA,ANCHKMAI,ANCHKPEC"+;
                  ",ANCHKFAX,ANCHKCPZ,ANCHKWWP,ANCHKPTL,ANCHKFIR"+;
                  ",ANCODEST,ANCODCLA,ANCODPEC,ANDTOBSO,ANDTINVA"+;
                  ",ANTIPSOT,ANCATOPE,ANCONSUP,ANCATCON,ANCONRIF"+;
                  ",ANTIPRIF,ANCODIVA,ANTIPOPE,ANCONCAU,ANFLRITE"+;
                  ",ANCODIRP,ANCAURIT,ANOPETRE,ANTIPPRE,ANFLAACC"+;
                  ",ANFLESIG,AFFLINTR,ANIVASOS,ANFLBLLS,ANCOFISC"+;
                  ",ANSCIPAG,ANFLSOAL,ANPARTSN,ANFLFIDO,ANFLSGRE"+;
                  ",ANFLBODO,ANCODSTU,ANCODSOG,ANCODCAT,ANCATCOM"+;
                  ",ANFLCONA,ANCATSCM,ANFLGCAU,ANTIPFAT,ANGRUPRO"+;
                  ",ANCONCON,ANCODLIN,ANCODVAL,ANNUMLIS,ANCODCUC"+;
                  ",AN1SCONT,ANGRPDEF,AN2SCONT,ANCODZON,ANCODAG1"+;
                  ",ANTIPOCL,ANMAGTER,ANCODORN,ANCODPOR,ANMTDCLC"+;
                  ",ANMCALSI,ANMCALST,ANBOLFAT,ANPREBOL,ANFLAPCA"+;
                  ",ANSCORPO,ANFLCODI,ANCLIPOS,ANFLGCPZ,ANFLIMBA"+;
                  ",ANFLPRIV,ANCODESC,ANCODPAG,ANGIOFIS,ANFLINCA"+;
                  ",ANSPEINC,AN1MESCL,ANGIOSC1,AN2MESCL,ANGIOSC2"+;
                  ",ANCODBAN,ANNUMCOR,ANCINABI,AN__BBAN,ANCODBA2"+;
                  ",AN__IBAN,ANFLRAGG,ANFLGAVV,ANDATAVV,ANIDRIDY"+;
                  ",ANTIIDRI,ANIBARID,ANPAGFOR,ANFLACBD,ANGESCON"+;
                  ",ANFLGCON,ANPAGPAR,ANDATMOR,ANFLESIM,ANSAGINT"+;
                  ",ANSPRINT,ANFLBLVE,ANVALFID,ANMAXORD,ANCODGRU"+;
                  ",AN__NOTE,ANRATING,ANGIORIT,ANVOCFIN,ANESCDOF"+;
                  ",ANDESPAR,ANDESCR3,ANDESCR4,ANINDIR3,ANINDIR4"+;
                  ",ANLOCAL2 "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_ANTIPCON)+;
                  ","+cp_ToStrODBC(this.w_ANCODICE)+;
                  ","+cp_ToStrODBC(this.w_ANDESCRI)+;
                  ","+cp_ToStrODBC(this.w_ANDESCR2)+;
                  ","+cp_ToStrODBC(this.w_ANINDIRI)+;
                  ","+cp_ToStrODBC(this.w_ANINDIR2)+;
                  ","+cp_ToStrODBC(this.w_AN___CAP)+;
                  ","+cp_ToStrODBC(this.w_ANLOCALI)+;
                  ","+cp_ToStrODBC(this.w_ANPROVIN)+;
                  ","+cp_ToStrODBCNull(this.w_ANNAZION)+;
                  ","+cp_ToStrODBC(this.w_ANCODFIS)+;
                  ","+cp_ToStrODBC(this.w_ANPARIVA)+;
                  ","+cp_ToStrODBC(this.w_ANTELEFO)+;
                  ","+cp_ToStrODBC(this.w_ANTELFAX)+;
                  ","+cp_ToStrODBC(this.w_ANNUMCEL)+;
                  ","+cp_ToStrODBC(this.w_ANINDWEB)+;
                  ","+cp_ToStrODBC(this.w_AN_EMAIL)+;
                  ","+cp_ToStrODBC(this.w_AN_SKYPE)+;
                  ","+cp_ToStrODBC(this.w_AN_EMPEC)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODSAL)+;
                  ","+cp_ToStrODBC(this.w_ANPERFIS)+;
                  ","+cp_ToStrODBC(this.w_ANCOGNOM)+;
                  ","+cp_ToStrODBC(this.w_AN__NOME)+;
                  ","+cp_ToStrODBC(this.w_AN_SESSO)+;
                  ","+cp_ToStrODBC(this.w_ANLOCNAS)+;
                  ","+cp_ToStrODBC(this.w_ANPRONAS)+;
                  ","+cp_ToStrODBC(this.w_ANDATNAS)+;
                  ","+cp_ToStrODBC(this.w_ANNUMCAR)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_ANCHKSTA)+;
                  ","+cp_ToStrODBC(this.w_ANCHKMAI)+;
                  ","+cp_ToStrODBC(this.w_ANCHKPEC)+;
                  ","+cp_ToStrODBC(this.w_ANCHKFAX)+;
                  ","+cp_ToStrODBC(this.w_ANCHKCPZ)+;
                  ","+cp_ToStrODBC(this.w_ANCHKWWP)+;
                  ","+cp_ToStrODBC(this.w_ANCHKPTL)+;
                  ","+cp_ToStrODBC(this.w_ANCHKFIR)+;
                  ","+cp_ToStrODBC(this.w_ANCODEST)+;
                  ","+cp_ToStrODBC(this.w_ANCODCLA)+;
                  ","+cp_ToStrODBC(this.w_ANCODPEC)+;
                  ","+cp_ToStrODBC(this.w_ANDTOBSO)+;
                  ","+cp_ToStrODBC(this.w_ANDTINVA)+;
                  ","+cp_ToStrODBC(this.w_ANTIPSOT)+;
                  ","+cp_ToStrODBC(this.w_ANCATOPE)+;
                  ","+cp_ToStrODBCNull(this.w_ANCONSUP)+;
                  ","+cp_ToStrODBCNull(this.w_ANCATCON)+;
                  ","+cp_ToStrODBCNull(this.w_ANCONRIF)+;
                  ","+cp_ToStrODBC(this.w_ANTIPRIF)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODIVA)+;
                  ","+cp_ToStrODBCNull(this.w_ANTIPOPE)+;
                  ","+cp_ToStrODBCNull(this.w_ANCONCAU)+;
                  ","+cp_ToStrODBC(this.w_ANFLRITE)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODIRP)+;
                  ","+cp_ToStrODBC(this.w_ANCAURIT)+;
                  ","+cp_ToStrODBC(this.w_ANOPETRE)+;
                  ","+cp_ToStrODBC(this.w_ANTIPPRE)+;
                  ","+cp_ToStrODBC(this.w_ANFLAACC)+;
                  ","+cp_ToStrODBC(this.w_ANFLESIG)+;
                  ","+cp_ToStrODBC(this.w_AFFLINTR)+;
                  ","+cp_ToStrODBC(this.w_ANIVASOS)+;
                  ","+cp_ToStrODBC(this.w_ANFLBLLS)+;
                  ","+cp_ToStrODBC(this.w_ANCOFISC)+;
                  ","+cp_ToStrODBC(this.w_ANSCIPAG)+;
                  ","+cp_ToStrODBC(this.w_ANFLSOAL)+;
                  ","+cp_ToStrODBC(this.w_ANPARTSN)+;
                  ","+cp_ToStrODBC(this.w_ANFLFIDO)+;
                  ","+cp_ToStrODBC(this.w_ANFLSGRE)+;
                  ","+cp_ToStrODBC(this.w_ANFLBODO)+;
                  ","+cp_ToStrODBC(this.w_ANCODSTU)+;
                  ","+cp_ToStrODBC(this.w_ANCODSOG)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODCAT)+;
                  ","+cp_ToStrODBCNull(this.w_ANCATCOM)+;
                  ","+cp_ToStrODBC(this.w_ANFLCONA)+;
                  ","+cp_ToStrODBCNull(this.w_ANCATSCM)+;
                  ","+cp_ToStrODBC(this.w_ANFLGCAU)+;
                  ","+cp_ToStrODBC(this.w_ANTIPFAT)+;
                  ","+cp_ToStrODBCNull(this.w_ANGRUPRO)+;
                  ","+cp_ToStrODBCNull(this.w_ANCONCON)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODLIN)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODVAL)+;
                  ","+cp_ToStrODBCNull(this.w_ANNUMLIS)+;
                  ","+cp_ToStrODBC(this.w_ANCODCUC)+;
                  ","+cp_ToStrODBC(this.w_AN1SCONT)+;
                  ","+cp_ToStrODBCNull(this.w_ANGRPDEF)+;
                  ","+cp_ToStrODBC(this.w_AN2SCONT)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODZON)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODAG1)+;
                  ","+cp_ToStrODBCNull(this.w_ANTIPOCL)+;
                  ","+cp_ToStrODBCNull(this.w_ANMAGTER)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODORN)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODPOR)+;
                  ","+cp_ToStrODBCNull(this.w_ANMTDCLC)+;
                  ","+cp_ToStrODBCNull(this.w_ANMCALSI)+;
                  ","+cp_ToStrODBCNull(this.w_ANMCALST)+;
                  ","+cp_ToStrODBC(this.w_ANBOLFAT)+;
                  ","+cp_ToStrODBC(this.w_ANPREBOL)+;
                  ","+cp_ToStrODBC(this.w_ANFLAPCA)+;
             ""
             i_nnn=i_nnn+;
                  ","+cp_ToStrODBC(this.w_ANSCORPO)+;
                  ","+cp_ToStrODBC(this.w_ANFLCODI)+;
                  ","+cp_ToStrODBC(this.w_ANCLIPOS)+;
                  ","+cp_ToStrODBC(this.w_ANFLGCPZ)+;
                  ","+cp_ToStrODBC(this.w_ANFLIMBA)+;
                  ","+cp_ToStrODBC(this.w_ANFLPRIV)+;
                  ","+cp_ToStrODBC(this.w_ANCODESC)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODPAG)+;
                  ","+cp_ToStrODBC(this.w_ANGIOFIS)+;
                  ","+cp_ToStrODBC(this.w_ANFLINCA)+;
                  ","+cp_ToStrODBC(this.w_ANSPEINC)+;
                  ","+cp_ToStrODBC(this.w_AN1MESCL)+;
                  ","+cp_ToStrODBC(this.w_ANGIOSC1)+;
                  ","+cp_ToStrODBC(this.w_AN2MESCL)+;
                  ","+cp_ToStrODBC(this.w_ANGIOSC2)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODBAN)+;
                  ","+cp_ToStrODBC(this.w_ANNUMCOR)+;
                  ","+cp_ToStrODBC(this.w_ANCINABI)+;
                  ","+cp_ToStrODBC(this.w_AN__BBAN)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODBA2)+;
                  ","+cp_ToStrODBC(this.w_AN__IBAN)+;
                  ","+cp_ToStrODBC(this.w_ANFLRAGG)+;
                  ","+cp_ToStrODBC(this.w_ANFLGAVV)+;
                  ","+cp_ToStrODBC(this.w_ANDATAVV)+;
                  ","+cp_ToStrODBC(this.w_ANIDRIDY)+;
                  ","+cp_ToStrODBC(this.w_ANTIIDRI)+;
                  ","+cp_ToStrODBC(this.w_ANIBARID)+;
                  ","+cp_ToStrODBCNull(this.w_ANPAGFOR)+;
                  ","+cp_ToStrODBC(this.w_ANFLACBD)+;
                  ","+cp_ToStrODBC(this.w_ANGESCON)+;
                  ","+cp_ToStrODBC(this.w_ANFLGCON)+;
                  ","+cp_ToStrODBCNull(this.w_ANPAGPAR)+;
                  ","+cp_ToStrODBC(this.w_ANDATMOR)+;
                  ","+cp_ToStrODBC(this.w_ANFLESIM)+;
                  ","+cp_ToStrODBC(this.w_ANSAGINT)+;
                  ","+cp_ToStrODBC(this.w_ANSPRINT)+;
                  ","+cp_ToStrODBC(this.w_ANFLBLVE)+;
                  ","+cp_ToStrODBC(this.w_ANVALFID)+;
                  ","+cp_ToStrODBC(this.w_ANMAXORD)+;
                  ","+cp_ToStrODBCNull(this.w_ANCODGRU)+;
                  ","+cp_ToStrODBC(this.w_AN__NOTE)+;
                  ","+cp_ToStrODBCNull(this.w_ANRATING)+;
                  ","+cp_ToStrODBC(this.w_ANGIORIT)+;
                  ","+cp_ToStrODBCNull(this.w_ANVOCFIN)+;
                  ","+cp_ToStrODBC(this.w_ANESCDOF)+;
                  ","+cp_ToStrODBC(this.w_ANDESPAR)+;
                  ","+cp_ToStrODBC(this.w_ANDESCR3)+;
                  ","+cp_ToStrODBC(this.w_ANDESCR4)+;
                  ","+cp_ToStrODBC(this.w_ANINDIR3)+;
                  ","+cp_ToStrODBC(this.w_ANINDIR4)+;
                  ","+cp_ToStrODBC(this.w_ANLOCAL2)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONTI')
        i_extval=cp_InsertValVFPExtFlds(this,'CONTI')
        cp_CheckDeletedKey(i_cTable,0,'ANTIPCON',this.w_ANTIPCON,'ANCODICE',this.w_ANCODICE)
        INSERT INTO (i_cTable);
              (ANTIPCON,ANCODICE,ANDESCRI,ANDESCR2,ANINDIRI,ANINDIR2,AN___CAP,ANLOCALI,ANPROVIN,ANNAZION,ANCODFIS,ANPARIVA,ANTELEFO,ANTELFAX,ANNUMCEL,ANINDWEB,AN_EMAIL,AN_SKYPE,AN_EMPEC,ANCODSAL,ANPERFIS,ANCOGNOM,AN__NOME,AN_SESSO,ANLOCNAS,ANPRONAS,ANDATNAS,ANNUMCAR,UTCC,UTCV,UTDC,UTDV,ANCHKSTA,ANCHKMAI,ANCHKPEC,ANCHKFAX,ANCHKCPZ,ANCHKWWP,ANCHKPTL,ANCHKFIR,ANCODEST,ANCODCLA,ANCODPEC,ANDTOBSO,ANDTINVA,ANTIPSOT,ANCATOPE,ANCONSUP,ANCATCON,ANCONRIF,ANTIPRIF,ANCODIVA,ANTIPOPE,ANCONCAU,ANFLRITE,ANCODIRP,ANCAURIT,ANOPETRE,ANTIPPRE,ANFLAACC,ANFLESIG,AFFLINTR,ANIVASOS,ANFLBLLS,ANCOFISC,ANSCIPAG,ANFLSOAL,ANPARTSN,ANFLFIDO,ANFLSGRE,ANFLBODO,ANCODSTU,ANCODSOG,ANCODCAT,ANCATCOM,ANFLCONA,ANCATSCM,ANFLGCAU,ANTIPFAT,ANGRUPRO,ANCONCON,ANCODLIN,ANCODVAL,ANNUMLIS,ANCODCUC,AN1SCONT,ANGRPDEF,AN2SCONT,ANCODZON,ANCODAG1,ANTIPOCL,ANMAGTER,ANCODORN,ANCODPOR,ANMTDCLC,ANMCALSI,ANMCALST,ANBOLFAT,ANPREBOL,ANFLAPCA,ANSCORPO,ANFLCODI,ANCLIPOS,ANFLGCPZ,ANFLIMBA,ANFLPRIV,ANCODESC,ANCODPAG,ANGIOFIS,ANFLINCA,ANSPEINC,AN1MESCL,ANGIOSC1,AN2MESCL,ANGIOSC2,ANCODBAN,ANNUMCOR,ANCINABI,AN__BBAN,ANCODBA2,AN__IBAN,ANFLRAGG,ANFLGAVV,ANDATAVV,ANIDRIDY,ANTIIDRI,ANIBARID,ANPAGFOR,ANFLACBD,ANGESCON,ANFLGCON,ANPAGPAR,ANDATMOR,ANFLESIM,ANSAGINT,ANSPRINT,ANFLBLVE,ANVALFID,ANMAXORD,ANCODGRU,AN__NOTE,ANRATING,ANGIORIT,ANVOCFIN,ANESCDOF,ANDESPAR,ANDESCR3,ANDESCR4,ANINDIR3,ANINDIR4,ANLOCAL2  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_ANTIPCON;
                  ,this.w_ANCODICE;
                  ,this.w_ANDESCRI;
                  ,this.w_ANDESCR2;
                  ,this.w_ANINDIRI;
                  ,this.w_ANINDIR2;
                  ,this.w_AN___CAP;
                  ,this.w_ANLOCALI;
                  ,this.w_ANPROVIN;
                  ,this.w_ANNAZION;
                  ,this.w_ANCODFIS;
                  ,this.w_ANPARIVA;
                  ,this.w_ANTELEFO;
                  ,this.w_ANTELFAX;
                  ,this.w_ANNUMCEL;
                  ,this.w_ANINDWEB;
                  ,this.w_AN_EMAIL;
                  ,this.w_AN_SKYPE;
                  ,this.w_AN_EMPEC;
                  ,this.w_ANCODSAL;
                  ,this.w_ANPERFIS;
                  ,this.w_ANCOGNOM;
                  ,this.w_AN__NOME;
                  ,this.w_AN_SESSO;
                  ,this.w_ANLOCNAS;
                  ,this.w_ANPRONAS;
                  ,this.w_ANDATNAS;
                  ,this.w_ANNUMCAR;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_ANCHKSTA;
                  ,this.w_ANCHKMAI;
                  ,this.w_ANCHKPEC;
                  ,this.w_ANCHKFAX;
                  ,this.w_ANCHKCPZ;
                  ,this.w_ANCHKWWP;
                  ,this.w_ANCHKPTL;
                  ,this.w_ANCHKFIR;
                  ,this.w_ANCODEST;
                  ,this.w_ANCODCLA;
                  ,this.w_ANCODPEC;
                  ,this.w_ANDTOBSO;
                  ,this.w_ANDTINVA;
                  ,this.w_ANTIPSOT;
                  ,this.w_ANCATOPE;
                  ,this.w_ANCONSUP;
                  ,this.w_ANCATCON;
                  ,this.w_ANCONRIF;
                  ,this.w_ANTIPRIF;
                  ,this.w_ANCODIVA;
                  ,this.w_ANTIPOPE;
                  ,this.w_ANCONCAU;
                  ,this.w_ANFLRITE;
                  ,this.w_ANCODIRP;
                  ,this.w_ANCAURIT;
                  ,this.w_ANOPETRE;
                  ,this.w_ANTIPPRE;
                  ,this.w_ANFLAACC;
                  ,this.w_ANFLESIG;
                  ,this.w_AFFLINTR;
                  ,this.w_ANIVASOS;
                  ,this.w_ANFLBLLS;
                  ,this.w_ANCOFISC;
                  ,this.w_ANSCIPAG;
                  ,this.w_ANFLSOAL;
                  ,this.w_ANPARTSN;
                  ,this.w_ANFLFIDO;
                  ,this.w_ANFLSGRE;
                  ,this.w_ANFLBODO;
                  ,this.w_ANCODSTU;
                  ,this.w_ANCODSOG;
                  ,this.w_ANCODCAT;
                  ,this.w_ANCATCOM;
                  ,this.w_ANFLCONA;
                  ,this.w_ANCATSCM;
                  ,this.w_ANFLGCAU;
                  ,this.w_ANTIPFAT;
                  ,this.w_ANGRUPRO;
                  ,this.w_ANCONCON;
                  ,this.w_ANCODLIN;
                  ,this.w_ANCODVAL;
                  ,this.w_ANNUMLIS;
                  ,this.w_ANCODCUC;
                  ,this.w_AN1SCONT;
                  ,this.w_ANGRPDEF;
                  ,this.w_AN2SCONT;
                  ,this.w_ANCODZON;
                  ,this.w_ANCODAG1;
                  ,this.w_ANTIPOCL;
                  ,this.w_ANMAGTER;
                  ,this.w_ANCODORN;
                  ,this.w_ANCODPOR;
                  ,this.w_ANMTDCLC;
                  ,this.w_ANMCALSI;
                  ,this.w_ANMCALST;
                  ,this.w_ANBOLFAT;
                  ,this.w_ANPREBOL;
                  ,this.w_ANFLAPCA;
                  ,this.w_ANSCORPO;
                  ,this.w_ANFLCODI;
                  ,this.w_ANCLIPOS;
                  ,this.w_ANFLGCPZ;
                  ,this.w_ANFLIMBA;
                  ,this.w_ANFLPRIV;
                  ,this.w_ANCODESC;
                  ,this.w_ANCODPAG;
                  ,this.w_ANGIOFIS;
                  ,this.w_ANFLINCA;
                  ,this.w_ANSPEINC;
                  ,this.w_AN1MESCL;
                  ,this.w_ANGIOSC1;
                  ,this.w_AN2MESCL;
                  ,this.w_ANGIOSC2;
                  ,this.w_ANCODBAN;
                  ,this.w_ANNUMCOR;
                  ,this.w_ANCINABI;
                  ,this.w_AN__BBAN;
                  ,this.w_ANCODBA2;
                  ,this.w_AN__IBAN;
                  ,this.w_ANFLRAGG;
                  ,this.w_ANFLGAVV;
                  ,this.w_ANDATAVV;
                  ,this.w_ANIDRIDY;
                  ,this.w_ANTIIDRI;
                  ,this.w_ANIBARID;
                  ,this.w_ANPAGFOR;
                  ,this.w_ANFLACBD;
                  ,this.w_ANGESCON;
                  ,this.w_ANFLGCON;
                  ,this.w_ANPAGPAR;
                  ,this.w_ANDATMOR;
                  ,this.w_ANFLESIM;
                  ,this.w_ANSAGINT;
                  ,this.w_ANSPRINT;
                  ,this.w_ANFLBLVE;
                  ,this.w_ANVALFID;
                  ,this.w_ANMAXORD;
                  ,this.w_ANCODGRU;
                  ,this.w_AN__NOTE;
                  ,this.w_ANRATING;
                  ,this.w_ANGIORIT;
                  ,this.w_ANVOCFIN;
                  ,this.w_ANESCDOF;
                  ,this.w_ANDESPAR;
                  ,this.w_ANDESCR3;
                  ,this.w_ANDESCR4;
                  ,this.w_ANINDIR3;
                  ,this.w_ANINDIR4;
                  ,this.w_ANLOCAL2;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- gsar_acl
    **Controllo che altri utenti non abbiano lo stesso codice Studio 
    w_MESS=''
    IF NOT CHK_STUDIO(this.w_ANTIPCON,this.w_ANCODICE,this.w_ANCODSTU,.T.) 
    local objcod
    this.getctrl ("w_ANCODICE")
    objcod.value=this.w_ANCODICE
    objcod.valid()
    this.mcalc(.T.)
    bTrsErr=.T.
    i_TrsMsg=w_MESS
    ENDIF
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CONTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CONTI_IDX,i_nConn)
      *
      * update CONTI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CONTI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " ANDESCRI="+cp_ToStrODBC(this.w_ANDESCRI)+;
             ",ANDESCR2="+cp_ToStrODBC(this.w_ANDESCR2)+;
             ",ANINDIRI="+cp_ToStrODBC(this.w_ANINDIRI)+;
             ",ANINDIR2="+cp_ToStrODBC(this.w_ANINDIR2)+;
             ",AN___CAP="+cp_ToStrODBC(this.w_AN___CAP)+;
             ",ANLOCALI="+cp_ToStrODBC(this.w_ANLOCALI)+;
             ",ANPROVIN="+cp_ToStrODBC(this.w_ANPROVIN)+;
             ",ANNAZION="+cp_ToStrODBCNull(this.w_ANNAZION)+;
             ",ANCODFIS="+cp_ToStrODBC(this.w_ANCODFIS)+;
             ",ANPARIVA="+cp_ToStrODBC(this.w_ANPARIVA)+;
             ",ANTELEFO="+cp_ToStrODBC(this.w_ANTELEFO)+;
             ",ANTELFAX="+cp_ToStrODBC(this.w_ANTELFAX)+;
             ",ANNUMCEL="+cp_ToStrODBC(this.w_ANNUMCEL)+;
             ",ANINDWEB="+cp_ToStrODBC(this.w_ANINDWEB)+;
             ",AN_EMAIL="+cp_ToStrODBC(this.w_AN_EMAIL)+;
             ",AN_SKYPE="+cp_ToStrODBC(this.w_AN_SKYPE)+;
             ",AN_EMPEC="+cp_ToStrODBC(this.w_AN_EMPEC)+;
             ",ANCODSAL="+cp_ToStrODBCNull(this.w_ANCODSAL)+;
             ",ANPERFIS="+cp_ToStrODBC(this.w_ANPERFIS)+;
             ",ANCOGNOM="+cp_ToStrODBC(this.w_ANCOGNOM)+;
             ",AN__NOME="+cp_ToStrODBC(this.w_AN__NOME)+;
             ",AN_SESSO="+cp_ToStrODBC(this.w_AN_SESSO)+;
             ",ANLOCNAS="+cp_ToStrODBC(this.w_ANLOCNAS)+;
             ",ANPRONAS="+cp_ToStrODBC(this.w_ANPRONAS)+;
             ",ANDATNAS="+cp_ToStrODBC(this.w_ANDATNAS)+;
             ",ANNUMCAR="+cp_ToStrODBC(this.w_ANNUMCAR)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",ANCHKSTA="+cp_ToStrODBC(this.w_ANCHKSTA)+;
             ",ANCHKMAI="+cp_ToStrODBC(this.w_ANCHKMAI)+;
             ",ANCHKPEC="+cp_ToStrODBC(this.w_ANCHKPEC)+;
             ",ANCHKFAX="+cp_ToStrODBC(this.w_ANCHKFAX)+;
             ",ANCHKCPZ="+cp_ToStrODBC(this.w_ANCHKCPZ)+;
             ",ANCHKWWP="+cp_ToStrODBC(this.w_ANCHKWWP)+;
             ",ANCHKPTL="+cp_ToStrODBC(this.w_ANCHKPTL)+;
             ",ANCHKFIR="+cp_ToStrODBC(this.w_ANCHKFIR)+;
             ",ANCODEST="+cp_ToStrODBC(this.w_ANCODEST)+;
             ",ANCODCLA="+cp_ToStrODBC(this.w_ANCODCLA)+;
             ",ANCODPEC="+cp_ToStrODBC(this.w_ANCODPEC)+;
             ",ANDTOBSO="+cp_ToStrODBC(this.w_ANDTOBSO)+;
             ",ANDTINVA="+cp_ToStrODBC(this.w_ANDTINVA)+;
             ",ANTIPSOT="+cp_ToStrODBC(this.w_ANTIPSOT)+;
             ",ANCATOPE="+cp_ToStrODBC(this.w_ANCATOPE)+;
             ",ANCONSUP="+cp_ToStrODBCNull(this.w_ANCONSUP)+;
             ",ANCATCON="+cp_ToStrODBCNull(this.w_ANCATCON)+;
             ",ANCONRIF="+cp_ToStrODBCNull(this.w_ANCONRIF)+;
             ",ANTIPRIF="+cp_ToStrODBC(this.w_ANTIPRIF)+;
             ",ANCODIVA="+cp_ToStrODBCNull(this.w_ANCODIVA)+;
             ",ANTIPOPE="+cp_ToStrODBCNull(this.w_ANTIPOPE)+;
             ",ANCONCAU="+cp_ToStrODBCNull(this.w_ANCONCAU)+;
             ",ANFLRITE="+cp_ToStrODBC(this.w_ANFLRITE)+;
             ",ANCODIRP="+cp_ToStrODBCNull(this.w_ANCODIRP)+;
             ",ANCAURIT="+cp_ToStrODBC(this.w_ANCAURIT)+;
             ",ANOPETRE="+cp_ToStrODBC(this.w_ANOPETRE)+;
             ",ANTIPPRE="+cp_ToStrODBC(this.w_ANTIPPRE)+;
             ",ANFLAACC="+cp_ToStrODBC(this.w_ANFLAACC)+;
             ",ANFLESIG="+cp_ToStrODBC(this.w_ANFLESIG)+;
             ",AFFLINTR="+cp_ToStrODBC(this.w_AFFLINTR)+;
             ",ANIVASOS="+cp_ToStrODBC(this.w_ANIVASOS)+;
             ",ANFLBLLS="+cp_ToStrODBC(this.w_ANFLBLLS)+;
             ",ANCOFISC="+cp_ToStrODBC(this.w_ANCOFISC)+;
             ",ANSCIPAG="+cp_ToStrODBC(this.w_ANSCIPAG)+;
             ",ANFLSOAL="+cp_ToStrODBC(this.w_ANFLSOAL)+;
             ",ANPARTSN="+cp_ToStrODBC(this.w_ANPARTSN)+;
             ",ANFLFIDO="+cp_ToStrODBC(this.w_ANFLFIDO)+;
             ",ANFLSGRE="+cp_ToStrODBC(this.w_ANFLSGRE)+;
             ",ANFLBODO="+cp_ToStrODBC(this.w_ANFLBODO)+;
             ",ANCODSTU="+cp_ToStrODBC(this.w_ANCODSTU)+;
             ",ANCODSOG="+cp_ToStrODBC(this.w_ANCODSOG)+;
             ",ANCODCAT="+cp_ToStrODBCNull(this.w_ANCODCAT)+;
             ",ANCATCOM="+cp_ToStrODBCNull(this.w_ANCATCOM)+;
             ",ANFLCONA="+cp_ToStrODBC(this.w_ANFLCONA)+;
             ",ANCATSCM="+cp_ToStrODBCNull(this.w_ANCATSCM)+;
             ",ANFLGCAU="+cp_ToStrODBC(this.w_ANFLGCAU)+;
             ",ANTIPFAT="+cp_ToStrODBC(this.w_ANTIPFAT)+;
             ",ANGRUPRO="+cp_ToStrODBCNull(this.w_ANGRUPRO)+;
             ",ANCONCON="+cp_ToStrODBCNull(this.w_ANCONCON)+;
             ",ANCODLIN="+cp_ToStrODBCNull(this.w_ANCODLIN)+;
             ",ANCODVAL="+cp_ToStrODBCNull(this.w_ANCODVAL)+;
             ",ANNUMLIS="+cp_ToStrODBCNull(this.w_ANNUMLIS)+;
             ",ANCODCUC="+cp_ToStrODBC(this.w_ANCODCUC)+;
             ",AN1SCONT="+cp_ToStrODBC(this.w_AN1SCONT)+;
             ",ANGRPDEF="+cp_ToStrODBCNull(this.w_ANGRPDEF)+;
             ",AN2SCONT="+cp_ToStrODBC(this.w_AN2SCONT)+;
             ",ANCODZON="+cp_ToStrODBCNull(this.w_ANCODZON)+;
             ",ANCODAG1="+cp_ToStrODBCNull(this.w_ANCODAG1)+;
             ",ANTIPOCL="+cp_ToStrODBCNull(this.w_ANTIPOCL)+;
             ",ANMAGTER="+cp_ToStrODBCNull(this.w_ANMAGTER)+;
             ",ANCODORN="+cp_ToStrODBCNull(this.w_ANCODORN)+;
             ",ANCODPOR="+cp_ToStrODBCNull(this.w_ANCODPOR)+;
             ",ANMTDCLC="+cp_ToStrODBCNull(this.w_ANMTDCLC)+;
             ",ANMCALSI="+cp_ToStrODBCNull(this.w_ANMCALSI)+;
             ",ANMCALST="+cp_ToStrODBCNull(this.w_ANMCALST)+;
             ",ANBOLFAT="+cp_ToStrODBC(this.w_ANBOLFAT)+;
             ",ANPREBOL="+cp_ToStrODBC(this.w_ANPREBOL)+;
             ",ANFLAPCA="+cp_ToStrODBC(this.w_ANFLAPCA)+;
             ",ANSCORPO="+cp_ToStrODBC(this.w_ANSCORPO)+;
             ",ANFLCODI="+cp_ToStrODBC(this.w_ANFLCODI)+;
             ""
             i_nnn=i_nnn+;
             ",ANCLIPOS="+cp_ToStrODBC(this.w_ANCLIPOS)+;
             ",ANFLGCPZ="+cp_ToStrODBC(this.w_ANFLGCPZ)+;
             ",ANFLIMBA="+cp_ToStrODBC(this.w_ANFLIMBA)+;
             ",ANFLPRIV="+cp_ToStrODBC(this.w_ANFLPRIV)+;
             ",ANCODESC="+cp_ToStrODBC(this.w_ANCODESC)+;
             ",ANCODPAG="+cp_ToStrODBCNull(this.w_ANCODPAG)+;
             ",ANGIOFIS="+cp_ToStrODBC(this.w_ANGIOFIS)+;
             ",ANFLINCA="+cp_ToStrODBC(this.w_ANFLINCA)+;
             ",ANSPEINC="+cp_ToStrODBC(this.w_ANSPEINC)+;
             ",AN1MESCL="+cp_ToStrODBC(this.w_AN1MESCL)+;
             ",ANGIOSC1="+cp_ToStrODBC(this.w_ANGIOSC1)+;
             ",AN2MESCL="+cp_ToStrODBC(this.w_AN2MESCL)+;
             ",ANGIOSC2="+cp_ToStrODBC(this.w_ANGIOSC2)+;
             ",ANCODBAN="+cp_ToStrODBCNull(this.w_ANCODBAN)+;
             ",ANNUMCOR="+cp_ToStrODBC(this.w_ANNUMCOR)+;
             ",ANCINABI="+cp_ToStrODBC(this.w_ANCINABI)+;
             ",AN__BBAN="+cp_ToStrODBC(this.w_AN__BBAN)+;
             ",ANCODBA2="+cp_ToStrODBCNull(this.w_ANCODBA2)+;
             ",AN__IBAN="+cp_ToStrODBC(this.w_AN__IBAN)+;
             ",ANFLRAGG="+cp_ToStrODBC(this.w_ANFLRAGG)+;
             ",ANFLGAVV="+cp_ToStrODBC(this.w_ANFLGAVV)+;
             ",ANDATAVV="+cp_ToStrODBC(this.w_ANDATAVV)+;
             ",ANIDRIDY="+cp_ToStrODBC(this.w_ANIDRIDY)+;
             ",ANTIIDRI="+cp_ToStrODBC(this.w_ANTIIDRI)+;
             ",ANIBARID="+cp_ToStrODBC(this.w_ANIBARID)+;
             ",ANPAGFOR="+cp_ToStrODBCNull(this.w_ANPAGFOR)+;
             ",ANFLACBD="+cp_ToStrODBC(this.w_ANFLACBD)+;
             ",ANGESCON="+cp_ToStrODBC(this.w_ANGESCON)+;
             ",ANFLGCON="+cp_ToStrODBC(this.w_ANFLGCON)+;
             ",ANPAGPAR="+cp_ToStrODBCNull(this.w_ANPAGPAR)+;
             ",ANDATMOR="+cp_ToStrODBC(this.w_ANDATMOR)+;
             ",ANFLESIM="+cp_ToStrODBC(this.w_ANFLESIM)+;
             ",ANSAGINT="+cp_ToStrODBC(this.w_ANSAGINT)+;
             ",ANSPRINT="+cp_ToStrODBC(this.w_ANSPRINT)+;
             ",ANFLBLVE="+cp_ToStrODBC(this.w_ANFLBLVE)+;
             ",ANVALFID="+cp_ToStrODBC(this.w_ANVALFID)+;
             ",ANMAXORD="+cp_ToStrODBC(this.w_ANMAXORD)+;
             ",ANCODGRU="+cp_ToStrODBCNull(this.w_ANCODGRU)+;
             ",AN__NOTE="+cp_ToStrODBC(this.w_AN__NOTE)+;
             ",ANRATING="+cp_ToStrODBCNull(this.w_ANRATING)+;
             ",ANGIORIT="+cp_ToStrODBC(this.w_ANGIORIT)+;
             ",ANVOCFIN="+cp_ToStrODBCNull(this.w_ANVOCFIN)+;
             ",ANESCDOF="+cp_ToStrODBC(this.w_ANESCDOF)+;
             ",ANDESPAR="+cp_ToStrODBC(this.w_ANDESPAR)+;
             ",ANDESCR3="+cp_ToStrODBC(this.w_ANDESCR3)+;
             ",ANDESCR4="+cp_ToStrODBC(this.w_ANDESCR4)+;
             ",ANINDIR3="+cp_ToStrODBC(this.w_ANINDIR3)+;
             ",ANINDIR4="+cp_ToStrODBC(this.w_ANINDIR4)+;
             ",ANLOCAL2="+cp_ToStrODBC(this.w_ANLOCAL2)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CONTI')
        i_cWhere = cp_PKFox(i_cTable  ,'ANTIPCON',this.w_ANTIPCON  ,'ANCODICE',this.w_ANCODICE  )
        UPDATE (i_cTable) SET;
              ANDESCRI=this.w_ANDESCRI;
             ,ANDESCR2=this.w_ANDESCR2;
             ,ANINDIRI=this.w_ANINDIRI;
             ,ANINDIR2=this.w_ANINDIR2;
             ,AN___CAP=this.w_AN___CAP;
             ,ANLOCALI=this.w_ANLOCALI;
             ,ANPROVIN=this.w_ANPROVIN;
             ,ANNAZION=this.w_ANNAZION;
             ,ANCODFIS=this.w_ANCODFIS;
             ,ANPARIVA=this.w_ANPARIVA;
             ,ANTELEFO=this.w_ANTELEFO;
             ,ANTELFAX=this.w_ANTELFAX;
             ,ANNUMCEL=this.w_ANNUMCEL;
             ,ANINDWEB=this.w_ANINDWEB;
             ,AN_EMAIL=this.w_AN_EMAIL;
             ,AN_SKYPE=this.w_AN_SKYPE;
             ,AN_EMPEC=this.w_AN_EMPEC;
             ,ANCODSAL=this.w_ANCODSAL;
             ,ANPERFIS=this.w_ANPERFIS;
             ,ANCOGNOM=this.w_ANCOGNOM;
             ,AN__NOME=this.w_AN__NOME;
             ,AN_SESSO=this.w_AN_SESSO;
             ,ANLOCNAS=this.w_ANLOCNAS;
             ,ANPRONAS=this.w_ANPRONAS;
             ,ANDATNAS=this.w_ANDATNAS;
             ,ANNUMCAR=this.w_ANNUMCAR;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,ANCHKSTA=this.w_ANCHKSTA;
             ,ANCHKMAI=this.w_ANCHKMAI;
             ,ANCHKPEC=this.w_ANCHKPEC;
             ,ANCHKFAX=this.w_ANCHKFAX;
             ,ANCHKCPZ=this.w_ANCHKCPZ;
             ,ANCHKWWP=this.w_ANCHKWWP;
             ,ANCHKPTL=this.w_ANCHKPTL;
             ,ANCHKFIR=this.w_ANCHKFIR;
             ,ANCODEST=this.w_ANCODEST;
             ,ANCODCLA=this.w_ANCODCLA;
             ,ANCODPEC=this.w_ANCODPEC;
             ,ANDTOBSO=this.w_ANDTOBSO;
             ,ANDTINVA=this.w_ANDTINVA;
             ,ANTIPSOT=this.w_ANTIPSOT;
             ,ANCATOPE=this.w_ANCATOPE;
             ,ANCONSUP=this.w_ANCONSUP;
             ,ANCATCON=this.w_ANCATCON;
             ,ANCONRIF=this.w_ANCONRIF;
             ,ANTIPRIF=this.w_ANTIPRIF;
             ,ANCODIVA=this.w_ANCODIVA;
             ,ANTIPOPE=this.w_ANTIPOPE;
             ,ANCONCAU=this.w_ANCONCAU;
             ,ANFLRITE=this.w_ANFLRITE;
             ,ANCODIRP=this.w_ANCODIRP;
             ,ANCAURIT=this.w_ANCAURIT;
             ,ANOPETRE=this.w_ANOPETRE;
             ,ANTIPPRE=this.w_ANTIPPRE;
             ,ANFLAACC=this.w_ANFLAACC;
             ,ANFLESIG=this.w_ANFLESIG;
             ,AFFLINTR=this.w_AFFLINTR;
             ,ANIVASOS=this.w_ANIVASOS;
             ,ANFLBLLS=this.w_ANFLBLLS;
             ,ANCOFISC=this.w_ANCOFISC;
             ,ANSCIPAG=this.w_ANSCIPAG;
             ,ANFLSOAL=this.w_ANFLSOAL;
             ,ANPARTSN=this.w_ANPARTSN;
             ,ANFLFIDO=this.w_ANFLFIDO;
             ,ANFLSGRE=this.w_ANFLSGRE;
             ,ANFLBODO=this.w_ANFLBODO;
             ,ANCODSTU=this.w_ANCODSTU;
             ,ANCODSOG=this.w_ANCODSOG;
             ,ANCODCAT=this.w_ANCODCAT;
             ,ANCATCOM=this.w_ANCATCOM;
             ,ANFLCONA=this.w_ANFLCONA;
             ,ANCATSCM=this.w_ANCATSCM;
             ,ANFLGCAU=this.w_ANFLGCAU;
             ,ANTIPFAT=this.w_ANTIPFAT;
             ,ANGRUPRO=this.w_ANGRUPRO;
             ,ANCONCON=this.w_ANCONCON;
             ,ANCODLIN=this.w_ANCODLIN;
             ,ANCODVAL=this.w_ANCODVAL;
             ,ANNUMLIS=this.w_ANNUMLIS;
             ,ANCODCUC=this.w_ANCODCUC;
             ,AN1SCONT=this.w_AN1SCONT;
             ,ANGRPDEF=this.w_ANGRPDEF;
             ,AN2SCONT=this.w_AN2SCONT;
             ,ANCODZON=this.w_ANCODZON;
             ,ANCODAG1=this.w_ANCODAG1;
             ,ANTIPOCL=this.w_ANTIPOCL;
             ,ANMAGTER=this.w_ANMAGTER;
             ,ANCODORN=this.w_ANCODORN;
             ,ANCODPOR=this.w_ANCODPOR;
             ,ANMTDCLC=this.w_ANMTDCLC;
             ,ANMCALSI=this.w_ANMCALSI;
             ,ANMCALST=this.w_ANMCALST;
             ,ANBOLFAT=this.w_ANBOLFAT;
             ,ANPREBOL=this.w_ANPREBOL;
             ,ANFLAPCA=this.w_ANFLAPCA;
             ,ANSCORPO=this.w_ANSCORPO;
             ,ANFLCODI=this.w_ANFLCODI;
             ,ANCLIPOS=this.w_ANCLIPOS;
             ,ANFLGCPZ=this.w_ANFLGCPZ;
             ,ANFLIMBA=this.w_ANFLIMBA;
             ,ANFLPRIV=this.w_ANFLPRIV;
             ,ANCODESC=this.w_ANCODESC;
             ,ANCODPAG=this.w_ANCODPAG;
             ,ANGIOFIS=this.w_ANGIOFIS;
             ,ANFLINCA=this.w_ANFLINCA;
             ,ANSPEINC=this.w_ANSPEINC;
             ,AN1MESCL=this.w_AN1MESCL;
             ,ANGIOSC1=this.w_ANGIOSC1;
             ,AN2MESCL=this.w_AN2MESCL;
             ,ANGIOSC2=this.w_ANGIOSC2;
             ,ANCODBAN=this.w_ANCODBAN;
             ,ANNUMCOR=this.w_ANNUMCOR;
             ,ANCINABI=this.w_ANCINABI;
             ,AN__BBAN=this.w_AN__BBAN;
             ,ANCODBA2=this.w_ANCODBA2;
             ,AN__IBAN=this.w_AN__IBAN;
             ,ANFLRAGG=this.w_ANFLRAGG;
             ,ANFLGAVV=this.w_ANFLGAVV;
             ,ANDATAVV=this.w_ANDATAVV;
             ,ANIDRIDY=this.w_ANIDRIDY;
             ,ANTIIDRI=this.w_ANTIIDRI;
             ,ANIBARID=this.w_ANIBARID;
             ,ANPAGFOR=this.w_ANPAGFOR;
             ,ANFLACBD=this.w_ANFLACBD;
             ,ANGESCON=this.w_ANGESCON;
             ,ANFLGCON=this.w_ANFLGCON;
             ,ANPAGPAR=this.w_ANPAGPAR;
             ,ANDATMOR=this.w_ANDATMOR;
             ,ANFLESIM=this.w_ANFLESIM;
             ,ANSAGINT=this.w_ANSAGINT;
             ,ANSPRINT=this.w_ANSPRINT;
             ,ANFLBLVE=this.w_ANFLBLVE;
             ,ANVALFID=this.w_ANVALFID;
             ,ANMAXORD=this.w_ANMAXORD;
             ,ANCODGRU=this.w_ANCODGRU;
             ,AN__NOTE=this.w_AN__NOTE;
             ,ANRATING=this.w_ANRATING;
             ,ANGIORIT=this.w_ANGIORIT;
             ,ANVOCFIN=this.w_ANVOCFIN;
             ,ANESCDOF=this.w_ANESCDOF;
             ,ANDESPAR=this.w_ANDESPAR;
             ,ANDESCR3=this.w_ANDESCR3;
             ,ANDESCR4=this.w_ANDESCR4;
             ,ANINDIR3=this.w_ANINDIR3;
             ,ANINDIR4=this.w_ANINDIR4;
             ,ANLOCAL2=this.w_ANLOCAL2;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAR_MSA : Saving
      this.GSAR_MSA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ANCODICE,"SSCODICE";
             ,this.w_ANTIPCON,"SSTIPCON";
             )
      this.GSAR_MSA.mReplace()
      * --- GSAR_MIN : Saving
      this.GSAR_MIN.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ANTIPCON,"MCTIPINT";
             ,this.w_ANCODICE,"MCCODINT";
             )
      this.GSAR_MIN.mReplace()
      * --- GSAR_MCC : Saving
      this.GSAR_MCC.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ANTIPCON,"CCTIPCON";
             ,this.w_ANCODICE,"CCCODCON";
             )
      this.GSAR_MCC.mReplace()
      * --- GSAR_MCO : Saving
      this.GSAR_MCO.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ANTIPCON,"COTIPCON";
             ,this.w_ANCODICE,"COCODCON";
             )
      this.GSAR_MCO.mReplace()
      * --- GSAR_MDD : Saving
      this.GSAR_MDD.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ANTIPCON,"DDTIPCON";
             ,this.w_ANCODICE,"DDCODICE";
             )
      this.GSAR_MDD.mReplace()
      * --- GSAR_MSE : Saving
      this.GSAR_MSE.ChangeRow(this.cRowID+'      1',0;
             ,this.w_ANTIPCON,"SITIPCON";
             ,this.w_ANCODICE,"SICODCON";
             )
      this.GSAR_MSE.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsar_acl
    *--- Cliente Variato
    if this.cFunction="Edit" and not(bTrsErr) and (g_OFFE='S' OR g_AGEN='S') AND !This.w_DANOM
      * --- Aggiorna Riferimenti Cliente sui Nominativi Offerte
      this.NotifyEvent('ClienteVariato')
    endif
    if this.cFunction="Edit" and not(bTrsErr) and g_GPOS='S' AND (this.w_ANCLIPOS='S' OR this.w_OLDCLPOS='S') AND !This.w_DANOM
      * --- Aggiorna Riferimenti Cliente sui Nominativi Pos
      this.NotifyEvent('CliPosVariato')
    endif
    * --- Esegue Controlli Finali
       this.NotifyEvent('ControlliFinali')
    
    **Controllo che altri utenti non abbiano lo stesso codice Studio 
    w_MESS=''
    IF NOT CHK_STUDIO(this.w_ANTIPCON,this.w_ANCODICE,this.w_ANCODSTU,.T.) 
    local objcod
    this.getctrl ("w_ANCODICE")
    objcod.value=this.w_ANCODICE
    objcod.valid()
    this.mcalc(.T.)
    bTrsErr=.T.
    i_TrsMsg=w_MESS
    ENDIF
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- gsar_acl
    if not(bTrsErr) and g_CESP='S'
      this.NotifyEvent('CliCesEliminato')
    endif
    * --- Fine Area Manuale
    * --- GSAR_MSA : Deleting
    this.GSAR_MSA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ANCODICE,"SSCODICE";
           ,this.w_ANTIPCON,"SSTIPCON";
           )
    this.GSAR_MSA.mDelete()
    * --- GSAR_MIN : Deleting
    this.GSAR_MIN.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ANTIPCON,"MCTIPINT";
           ,this.w_ANCODICE,"MCCODINT";
           )
    this.GSAR_MIN.mDelete()
    * --- GSAR_MCC : Deleting
    this.GSAR_MCC.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ANTIPCON,"CCTIPCON";
           ,this.w_ANCODICE,"CCCODCON";
           )
    this.GSAR_MCC.mDelete()
    * --- GSAR_MCO : Deleting
    this.GSAR_MCO.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ANTIPCON,"COTIPCON";
           ,this.w_ANCODICE,"COCODCON";
           )
    this.GSAR_MCO.mDelete()
    * --- GSAR_MDD : Deleting
    this.GSAR_MDD.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ANTIPCON,"DDTIPCON";
           ,this.w_ANCODICE,"DDCODICE";
           )
    this.GSAR_MDD.mDelete()
    * --- GSAR_MSE : Deleting
    this.GSAR_MSE.ChangeRow(this.cRowID+'      1',0;
           ,this.w_ANTIPCON,"SITIPCON";
           ,this.w_ANCODICE,"SICODCON";
           )
    this.GSAR_MSE.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONTI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CONTI_IDX,i_nConn)
      *
      * delete CONTI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'ANTIPCON',this.w_ANTIPCON  ,'ANCODICE',this.w_ANCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- gsar_acl
    if not(bTrsErr) and g_GPOS='S' AND this.w_ANCLIPOS='S'
      * --- Elimina Riferimenti Cliente sui Nominativi Pos
      this.NotifyEvent('CliPosEliminato')
    endif
    
    
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    if i_bUpd
      with this
            .w_AUTOAZI = i_CODAZI
          .link_1_1('Full')
        .DoRTCalc(2,3,.t.)
            .w_OBTEST = i_datsys
        if .o_AUTO<>.w_AUTO
            .w_ANCODICE = iif(.cFunction='Load' AND .w_AUTO='S', 'AUTO',.w_ANCODICE)
        endif
        .DoRTCalc(6,47,.t.)
        if .o_ANCHKMAI<>.w_ANCHKMAI.or. .o_ANCHKPEC<>.w_ANCHKPEC.or. .o_ANCHKCPZ<>.w_ANCHKCPZ
            .w_ANCHKFIR = iif(.w_ANCHKMAI='S' or .w_ANCHKPEC='S' or .w_ANCHKCPZ = 'S',.w_ANCHKFIR,'N')
        endif
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_92.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
        if .o_ANPERFIS<>.w_ANPERFIS
          .Calculate_WWPJDQWIRN()
        endif
        if .o_ANPERFIS<>.w_ANPERFIS
          .Calculate_QWKRQHGBHI()
        endif
        if .o_ANCODFIS<>.w_ANCODFIS
          .Calculate_XNLBETFNTC()
        endif
        .DoRTCalc(49,58,.t.)
            .w_TIPRIF = 'F'
        .DoRTCalc(60,60,.t.)
            .w_CODI = .w_ANCODICE
            .w_RAG1 = .w_ANDESCRI
          .link_2_6('Full')
        .DoRTCalc(64,71,.t.)
            .w_ANTIPRIF = IIF(EMPTY(.w_ANCONRIF), ' ', 'F')
        .DoRTCalc(73,79,.t.)
        if .o_ANFLRITE<>.w_ANFLRITE
            .w_ANCODIRP = IIF(.w_ANFLRITE='S',IIF(IsAlt(),(READALTE('I')),.w_ANCODIRP),space(5))
          .link_2_24('Full')
        endif
        .DoRTCalc(81,82,.t.)
        if .o_ANCODIRP<>.w_ANCODIRP
            .w_ANCAURIT = .w_CAUPRE2
        endif
        if .o_ANFLSGRE<>.w_ANFLSGRE
          .Calculate_FMDIRHAKGI()
        endif
        .DoRTCalc(84,91,.t.)
        if .o_ANFLESIG<>.w_ANFLESIG
            .w_ANIVASOS = 'N'
        endif
        .DoRTCalc(93,93,.t.)
        if .o_ANFLBLLS<>.w_ANFLBLLS
            .w_ANCOFISC = iif(.w_ANFLBLLS='S',.w_ANCOFISC,SPACE(25))
        endif
        .DoRTCalc(95,95,.t.)
        if .o_ANFLBLLS<>.w_ANFLBLLS
            .w_ANFLSOAL = 'N'
        endif
        .DoRTCalc(97,99,.t.)
        if .o_ANFLSOAL<>.w_ANFLSOAL.or. .o_AFFLINTR<>.w_AFFLINTR
            .w_ANFLBODO = iif(.w_AFFLINTR='S' or .w_ANFLSOAL='S' or g_TRAEXP = 'N',' ',.w_ANFLBODO)
        endif
        if .o_CODSTU<>.w_CODSTU.or. .o_PROSTU<>.w_PROSTU
            .w_ANCODSTU = IIF(g_TRAEXP<>'G',SPACE(9),.w_ANCODSTU)
        endif
        if .o_CODSTU<>.w_CODSTU.or. .o_PROSTU<>.w_PROSTU
          .Calculate_QAXUOQUTMC()
        endif
        if .o_ANCODSTU<>.w_ANCODSTU
          .Calculate_FIRXFFUGWY()
        endif
        if .o_CODSTU<>.w_CODSTU.or. .o_PROSTU<>.w_PROSTU
          .Calculate_NODBJNDQUR()
        endif
        .DoRTCalc(102,110,.t.)
            .w_RIFDIC = ''
            .w_ANNDIC = ''
            .w_DATDIC = ''
            .w_NUMDIC = ''
            .w_CODI = .w_ANCODICE
            .w_RAG1 = .w_ANDESCRI
        .DoRTCalc(117,137,.t.)
        if .o_AN1SCONT<>.w_AN1SCONT
            .w_AN2SCONT = 0
        endif
        .DoRTCalc(139,160,.t.)
        if .o_ANCODICE<>.w_ANCODICE
            .w_FLGCPZ = .w_ANFLGCPZ
        endif
        .DoRTCalc(162,163,.t.)
        if .o_ANCODICE<>.w_ANCODICE
            .w_FLPRIV = .w_ANFLPRIV
        endif
        .DoRTCalc(165,166,.t.)
        if .o_ANCODICE<>.w_ANCODICE
            .w_OLDCLPOS = .w_ANCLIPOS
        endif
        .DoRTCalc(168,175,.t.)
            .w_CODI = .w_ANCODICE
            .w_RAG1 = .w_ANDESCRI
            .w_TIPCC = 'G'
        .DoRTCalc(179,184,.t.)
            .w_DESMES1 = LEFT(IIF(.w_AN1MESCL>0 AND .w_AN1MESCL<13, g_MESE[.w_AN1MESCL], "")+SPACE(12),12)
        if .o_AN1MESCL<>.w_AN1MESCL
            .w_ANGIOSC1 = 0
        endif
        if .o_AN1MESCL<>.w_AN1MESCL
            .w_AN2MESCL = 0
        endif
            .w_DESMES2 = LEFT(IIF(.w_AN2MESCL>0 AND .w_AN2MESCL<13, g_MESE[.w_AN2MESCL], "")+SPACE(12),12)
        if .o_AN2MESCL<>.w_AN2MESCL
            .w_ANGIOSC2 = 0
        endif
        if .o_ANIDRIDY<>.w_ANIDRIDY
          .Calculate_IRPKOAKLYU()
        endif
        if .o_ANTIIDRI<>.w_ANTIIDRI.or. .o_ANIBARID<>.w_ANIBARID
          .Calculate_DLGMYOICKZ()
        endif
        .DoRTCalc(190,211,.t.)
        if .o_ANGESCON<>.w_ANGESCON
            .w_ANFLGCON = IIF(.w_ANGESCON<>'B',' ',.w_ANFLGCON)
        endif
        if .o_ANGESCON<>.w_ANGESCON
            .w_ANPAGPAR = iif(.w_ANGESCON<>'M',SPACE(5),.w_ANPAGPAR)
          .link_5_3('Full')
        endif
        if .o_ANPAGPAR<>.w_ANPAGPAR
            .w_ANDATMOR = cp_CharToDate('  -  -  ')
        endif
        .DoRTCalc(215,215,.t.)
        if .o_ANFLESIM<>.w_ANFLESIM
            .w_ANSAGINT = IIF(.w_ANFLESIM='S', 0, .w_ANSAGINT)
        endif
        if .o_ANFLESIM<>.w_ANFLESIM.or. .o_ANSAGINT<>.w_ANSAGINT
            .w_ANSPRINT = IIF(.w_ANFLESIM='S' OR .w_ANSAGINT=0,'N',.w_ANSPRINT)
        endif
        .DoRTCalc(218,218,.t.)
            .w_CODI = .w_ANCODICE
            .w_RAG1 = .w_ANDESCRI
            .w_CODI = .w_ANCODICE
            .w_RAG1 = .w_ANDESCRI
        .DoRTCalc(223,225,.t.)
        if .o_ANCODICE<>.w_ANCODICE
            .w_READFID = IIF( .cFunction<>'Load', .w_ANCODICE ,Space(15) )
          .link_6_10('Full')
        endif
        .DoRTCalc(227,233,.t.)
            .w_FIDRES1 = .w_ANVALFID-(.w_FIDPAP+.w_FIDESC+.w_FIDESO+.w_FIDORD+.w_FIDDDT+.w_FIDFAT)
            .w_FIDRES2 = .w_ANVALFID-(.w_FIDPAP+.w_FIDESC+.w_FIDESO+.w_FIDORD+.w_FIDDDT+.w_FIDFAT)
            .w_CODI = .w_ANCODICE
            .w_RAG1 = .w_ANDESCRI
        if  .o_ANCODICE<>.w_ANCODICE.or. .o_ANTIPCON<>.w_ANTIPCON
          .WriteTo_GSAR_MCO()
        endif
            .w_CODI = .w_ANCODICE
            .w_RAG1 = .w_ANDESCRI
            .w_CODI = .w_ANCODICE
            .w_RAG1 = .w_ANDESCRI
        .DoRTCalc(242,243,.t.)
            .w_CODI = .w_ANCODICE
            .w_RAG1 = .w_ANDESCRI
        .DoRTCalc(246,253,.t.)
            .w_CODI = .w_ANCODICE
            .w_RAG1 = .w_ANDESCRI
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_codazi<>.w_codazi
          if g_CFNUME='S' AND .p_AUT=0
             cp_AskTableProg(this,i_nConn,"PRNUCL","i_codazi,w_ANCODICE")
          endif
          .op_ANCODICE = .w_ANCODICE
        endif
        .op_codazi = .w_codazi
      endwith
      this.DoRTCalc(256,263,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_76.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_77.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_78.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_79.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_80.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_92.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_93.Calculate()
    endwith
  return

  proc Calculate_WWPJDQWIRN()
    with this
          * --- Divide ragione sociale
          GSAR_BDR(this;
              ,'C';
             )
    endwith
  endproc
  proc Calculate_QWKRQHGBHI()
    with this
          * --- Sbianco inf. persona fisica
          .w_ANCOGNOM = iif(.w_ANPERFIS='S',.w_ANCOGNOM,'')
          .w_AN__NOME = iif(.w_ANPERFIS='S',.w_AN__NOME,'')
          .w_ANLOCNAS = iif(.w_ANPERFIS='S',.w_ANLOCNAS,'')
          .w_ANPRONAS = iif(.w_ANPERFIS='S',.w_ANPRONAS,'')
          .w_ANDATNAS = iif(.w_ANPERFIS='S',.w_ANDATNAS,Cp_CharToDate('  -  -    '))
          .w_AN_SESSO = iif(.w_ANPERFIS='S',.w_AN_SESSO,'')
          .w_ANNUMCAR = ''
    endwith
  endproc
  proc Calculate_XNLBETFNTC()
    with this
          * --- w_ANCODFIS changed
          .w_ERR = cfanag(.w_ANCODFIS,'V')
          .w_ANPERFIS = iif(.w_ERR, .w_ANPERFIS , 'S' )
          .w_ANDATNAS = iif(.w_ERR, .w_ANDATNAS, IIF (.w_CONTA<>.o_CONTA, .w_ANDATNAS ,cfanag(.w_ANCODFIS,'D') ))
          .w_AN_SESSO = iif(.w_ERR,  .w_AN_SESSO , cfanag(.w_ANCODFIS,'S'))
          .w_ANLOCNAS = iif(.w_ERR, .w_ANLOCNAS , cfanag(.w_ANCODFIS,'L'))
          .w_ANPRONAS = iif(.w_ERR,  .w_ANPRONAS , cfanag(.w_ANCODFIS,'P'))
          .w_CODCOM = iif(.w_ERR, .w_CODCOM , SUBSTR(.w_ANCODFIS,12,4))
    endwith
  endproc
  proc Calculate_OLYGVFQXTR()
    with this
          * --- Calcola auto
          GSAR_BSS(this;
             )
    endwith
  endproc
  proc Calculate_UIROVJBFQO()
    with this
          * --- recupera progressivo
          GSAR_BRK(this;
              ,.w_ANTIPCON;
              ,.w_ANCODICE;
             )
    endwith
  endproc
  proc Calculate_PGQNPFGCXZ()
    with this
          * --- Colora pagina note
          GSUT_BCN(this;
              ,"COLORA";
              ,.w_AN__NOTE;
             )
    endwith
  endproc
  proc Calculate_GRJFLMWOLU()
    with this
          * --- Numero pagina note
          GSUT_BCN(this;
              ,"PAGINA";
              ,"Note";
             )
    endwith
  endproc
  proc Calculate_NTGPIXRFKL()
    with this
          * --- Elimina nominativi
          gsar_bkn(this;
             )
    endwith
  endproc
  proc Calculate_CZGHMRBITS()
    with this
          * --- Cancellazione allegati collegati
          gsma_bae(this;
              ,'I';
             )
    endwith
  endproc
  proc Calculate_FMDIRHAKGI()
    with this
          * --- Gsar_bsn w_anflsgre changed
          gsar_bsn(this;
             )
    endwith
  endproc
  proc Calculate_QAXUOQUTMC()
    with this
          * --- Calcolo sottocontostudio
          .w_ANCODSTU = Icase(g_LEMC='S' AND  !g_TRAEXP$('N-G') AND EMPTY(.w_ANCODSTU),GSLM_BCF(this, .w_ANTIPCON, .w_ANCODICE, .w_CODSTU , .w_ANCONSUP, ,.w_PROSTU ),g_TRAEXP='G',.w_ANCODSTU,' ')
    endwith
  endproc
  proc Calculate_FIRXFFUGWY()
    with this
          * --- Simulo zerofill ancodstu 
          .w_ANCODSTU = IIF(g_TRAEXP<>'G',Left(IIF( ( Left( Alltrim( .w_ANCODSTU ) ,1 ) <>"0" And  Len( Alltrim( .w_ANCODSTU ) )=Len( "999999" ) ) Or Left( Alltrim( .w_ANCODSTU ) ,1 ) ="0" , .w_ANCODSTU , IIF(g_LEMC<>'S' OR g_TRAEXP='N','',Right( Repl('0',Len( "999999" ))+Alltrim( .w_ANCODSTU ) ,Len( "999999" ) )  ))+'      ',10),Alltrim( .w_ANCODSTU ))
    endwith
  endproc
  proc Calculate_NODBJNDQUR()
    with this
          * --- Calcolo estremi intervallo sottoconto studio
          IIF(g_LEMC='S' AND  !g_TRAEXP$('N-G'), GSLM_BCF(this;
              ,'';
              ,'';
              ,.w_CODSTU;
              ,'';
              ,.w_ANCODSTU;
              ,.w_PROSTU);
              ,1=1;
             )
    endwith
  endproc
  proc Calculate_VISJNUHVVI()
    with this
          * --- Scorporo a piede fattura
          MessFlgScorporo(this;
             )
    endwith
  endproc
  proc Calculate_IRPKOAKLYU()
    with this
          * --- inizializza codice RID
          .w_ANTIIDRI = IIF (NVL(.w_ANTIIDRI,0)=0 ,1,.w_ANTIIDRI)
    endwith
  endproc
  proc Calculate_DLGMYOICKZ()
    with this
          * --- Conferma modifica ai campi del codice rid
          this.CodiceRid(this;
             )
    endwith
  endproc
  proc Calculate_SIGJGJWAYX()
    with this
          * --- Memorizza codici i vecchi codicei rid
          .w_OLDANIBARID = .w_ANIBARID
          .w_OLDANTIIDRI = .w_ANTIIDRI
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oANCODICE_1_5.enabled = this.oPgFrm.Page1.oPag.oANCODICE_1_5.mCond()
    this.oPgFrm.Page1.oPag.oANCOGNOM_1_30.enabled = this.oPgFrm.Page1.oPag.oANCOGNOM_1_30.mCond()
    this.oPgFrm.Page1.oPag.oAN__NOME_1_31.enabled = this.oPgFrm.Page1.oPag.oAN__NOME_1_31.mCond()
    this.oPgFrm.Page1.oPag.oAN_SESSO_1_32.enabled = this.oPgFrm.Page1.oPag.oAN_SESSO_1_32.mCond()
    this.oPgFrm.Page1.oPag.oANLOCNAS_1_33.enabled = this.oPgFrm.Page1.oPag.oANLOCNAS_1_33.mCond()
    this.oPgFrm.Page1.oPag.oANPRONAS_1_34.enabled = this.oPgFrm.Page1.oPag.oANPRONAS_1_34.mCond()
    this.oPgFrm.Page1.oPag.oANDATNAS_1_35.enabled = this.oPgFrm.Page1.oPag.oANDATNAS_1_35.mCond()
    this.oPgFrm.Page1.oPag.oANNUMCAR_1_37.enabled = this.oPgFrm.Page1.oPag.oANNUMCAR_1_37.mCond()
    this.oPgFrm.Page1.oPag.oANCHKCPZ_1_61.enabled = this.oPgFrm.Page1.oPag.oANCHKCPZ_1_61.mCond()
    this.oPgFrm.Page1.oPag.oANCHKFIR_1_64.enabled = this.oPgFrm.Page1.oPag.oANCHKFIR_1_64.mCond()
    this.oPgFrm.Page2.oPag.oANCONCAU_2_21.enabled = this.oPgFrm.Page2.oPag.oANCONCAU_2_21.mCond()
    this.oPgFrm.Page2.oPag.oANFLRITE_2_23.enabled = this.oPgFrm.Page2.oPag.oANFLRITE_2_23.mCond()
    this.oPgFrm.Page2.oPag.oANCODIRP_2_24.enabled = this.oPgFrm.Page2.oPag.oANCODIRP_2_24.mCond()
    this.oPgFrm.Page2.oPag.oANCAURIT_2_27.enabled = this.oPgFrm.Page2.oPag.oANCAURIT_2_27.mCond()
    this.oPgFrm.Page2.oPag.oANFLESIG_2_41.enabled = this.oPgFrm.Page2.oPag.oANFLESIG_2_41.mCond()
    this.oPgFrm.Page2.oPag.oANIVASOS_2_43.enabled = this.oPgFrm.Page2.oPag.oANIVASOS_2_43.mCond()
    this.oPgFrm.Page2.oPag.oANCOFISC_2_45.enabled = this.oPgFrm.Page2.oPag.oANCOFISC_2_45.mCond()
    this.oPgFrm.Page2.oPag.oANFLSOAL_2_47.enabled = this.oPgFrm.Page2.oPag.oANFLSOAL_2_47.mCond()
    this.oPgFrm.Page2.oPag.oANPARTSN_2_48.enabled = this.oPgFrm.Page2.oPag.oANPARTSN_2_48.mCond()
    this.oPgFrm.Page2.oPag.oANFLFIDO_2_49.enabled = this.oPgFrm.Page2.oPag.oANFLFIDO_2_49.mCond()
    this.oPgFrm.Page2.oPag.oANFLBODO_2_51.enabled = this.oPgFrm.Page2.oPag.oANFLBODO_2_51.mCond()
    this.oPgFrm.Page2.oPag.oANCODSTU_2_52.enabled = this.oPgFrm.Page2.oPag.oANCODSTU_2_52.mCond()
    this.oPgFrm.Page2.oPag.oANCODSOG_2_54.enabled = this.oPgFrm.Page2.oPag.oANCODSOG_2_54.mCond()
    this.oPgFrm.Page3.oPag.oANFLGCAU_3_7.enabled = this.oPgFrm.Page3.oPag.oANFLGCAU_3_7.mCond()
    this.oPgFrm.Page3.oPag.oAN2SCONT_3_25.enabled = this.oPgFrm.Page3.oPag.oAN2SCONT_3_25.mCond()
    this.oPgFrm.Page3.oPag.oANMCALSI_3_33.enabled = this.oPgFrm.Page3.oPag.oANMCALSI_3_33.mCond()
    this.oPgFrm.Page3.oPag.oANMCALST_3_34.enabled = this.oPgFrm.Page3.oPag.oANMCALST_3_34.mCond()
    this.oPgFrm.Page3.oPag.oANCLIPOS_3_47.enabled = this.oPgFrm.Page3.oPag.oANCLIPOS_3_47.mCond()
    this.oPgFrm.Page3.oPag.oANFLGCPZ_3_48.enabled = this.oPgFrm.Page3.oPag.oANFLGCPZ_3_48.mCond()
    this.oPgFrm.Page3.oPag.oANFLIMBA_3_50.enabled = this.oPgFrm.Page3.oPag.oANFLIMBA_3_50.mCond()
    this.oPgFrm.Page3.oPag.oANFLPRIV_3_51.enabled = this.oPgFrm.Page3.oPag.oANFLPRIV_3_51.mCond()
    this.oPgFrm.Page3.oPag.oANCODESC_3_54.enabled = this.oPgFrm.Page3.oPag.oANCODESC_3_54.mCond()
    this.oPgFrm.Page3.oPag.oANMCALSI_3_97.enabled = this.oPgFrm.Page3.oPag.oANMCALSI_3_97.mCond()
    this.oPgFrm.Page3.oPag.oANMCALST_3_98.enabled = this.oPgFrm.Page3.oPag.oANMCALST_3_98.mCond()
    this.oPgFrm.Page4.oPag.oANSPEINC_4_8.enabled = this.oPgFrm.Page4.oPag.oANSPEINC_4_8.mCond()
    this.oPgFrm.Page4.oPag.oANGIOSC1_4_11.enabled = this.oPgFrm.Page4.oPag.oANGIOSC1_4_11.mCond()
    this.oPgFrm.Page4.oPag.oAN2MESCL_4_12.enabled = this.oPgFrm.Page4.oPag.oAN2MESCL_4_12.mCond()
    this.oPgFrm.Page4.oPag.oANGIOSC2_4_14.enabled = this.oPgFrm.Page4.oPag.oANGIOSC2_4_14.mCond()
    this.oPgFrm.Page4.oPag.oANTIIDRI_4_34.enabled = this.oPgFrm.Page4.oPag.oANTIIDRI_4_34.mCond()
    this.oPgFrm.Page4.oPag.oANIBARID_4_35.enabled = this.oPgFrm.Page4.oPag.oANIBARID_4_35.mCond()
    this.oPgFrm.Page5.oPag.oANFLGCON_5_2.enabled = this.oPgFrm.Page5.oPag.oANFLGCON_5_2.mCond()
    this.oPgFrm.Page5.oPag.oANPAGPAR_5_3.enabled = this.oPgFrm.Page5.oPag.oANPAGPAR_5_3.mCond()
    this.oPgFrm.Page5.oPag.oANDATMOR_5_4.enabled = this.oPgFrm.Page5.oPag.oANDATMOR_5_4.mCond()
    this.oPgFrm.Page5.oPag.oANSAGINT_5_6.enabled = this.oPgFrm.Page5.oPag.oANSAGINT_5_6.mCond()
    this.oPgFrm.Page5.oPag.oANSPRINT_5_7.enabled = this.oPgFrm.Page5.oPag.oANSPRINT_5_7.mCond()
    this.oPgFrm.Page6.oPag.oANFLBLVE_6_3.enabled = this.oPgFrm.Page6.oPag.oANFLBLVE_6_3.mCond()
    this.oPgFrm.Page6.oPag.oANVALFID_6_4.enabled = this.oPgFrm.Page6.oPag.oANVALFID_6_4.mCond()
    this.oPgFrm.Page6.oPag.oANMAXORD_6_5.enabled = this.oPgFrm.Page6.oPag.oANMAXORD_6_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_83.enabled = this.oPgFrm.Page1.oPag.oBtn_1_83.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_84.enabled = this.oPgFrm.Page1.oPag.oBtn_1_84.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_85.enabled = this.oPgFrm.Page1.oPag.oBtn_1_85.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_87.enabled = this.oPgFrm.Page1.oPag.oBtn_1_87.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_89.enabled = this.oPgFrm.Page1.oPag.oBtn_1_89.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_94.enabled = this.oPgFrm.Page1.oPag.oBtn_1_94.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_95.enabled = this.oPgFrm.Page1.oPag.oBtn_1_95.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_101.enabled = this.oPgFrm.Page1.oPag.oBtn_1_101.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_104.enabled = this.oPgFrm.Page1.oPag.oBtn_1_104.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_105.enabled = this.oPgFrm.Page1.oPag.oBtn_1_105.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_106.enabled = this.oPgFrm.Page1.oPag.oBtn_1_106.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_53.enabled = this.oPgFrm.Page2.oPag.oBtn_2_53.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_57.enabled = this.oPgFrm.Page2.oPag.oBtn_2_57.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_59.enabled = this.oPgFrm.Page2.oPag.oBtn_2_59.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_61.enabled = this.oPgFrm.Page2.oPag.oBtn_2_61.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_56.enabled = this.oPgFrm.Page3.oPag.oBtn_3_56.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_57.enabled = this.oPgFrm.Page3.oPag.oBtn_3_57.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_58.enabled = this.oPgFrm.Page3.oPag.oBtn_3_58.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_59.enabled = this.oPgFrm.Page3.oPag.oBtn_3_59.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_61.enabled = this.oPgFrm.Page3.oPag.oBtn_3_61.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_63.enabled = this.oPgFrm.Page3.oPag.oBtn_3_63.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_65.enabled = this.oPgFrm.Page3.oPag.oBtn_3_65.mCond()
    this.oPgFrm.Page3.oPag.oBtn_3_67.enabled = this.oPgFrm.Page3.oPag.oBtn_3_67.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_15.enabled = this.oPgFrm.Page4.oPag.oBtn_4_15.mCond()
    this.oPgFrm.Page4.oPag.oBtn_4_16.enabled = this.oPgFrm.Page4.oPag.oBtn_4_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_122.enabled = this.oPgFrm.Page1.oPag.oBtn_1_122.mCond()
    this.oPgFrm.Page3.oPag.oLinkPC_3_44.enabled = this.oPgFrm.Page3.oPag.oLinkPC_3_44.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Pages(6).enabled=not(!g_PERFID='S' OR (IsAlt() AND (g_COGE<>'S' OR this.w_ANFLFIDO<>'S')))
    this.oPgFrm.Pages(7).enabled=not(IsAlt())
    this.oPgFrm.Pages(9).enabled=not(!g_VEFA='S')
    local i_show5
    i_show5=not(!g_PERFID='S' OR (IsAlt() AND (g_COGE<>'S' OR this.w_ANFLFIDO<>'S')))
    this.oPgFrm.Pages(6).enabled=i_show5 and not(!g_PERFID='S' OR (IsAlt() AND (g_COGE<>'S' OR this.w_ANFLFIDO<>'S')))
    this.oPgFrm.Pages(6).caption=iif(i_show5,cp_translate("Rischio"),"")
    this.oPgFrm.Pages(6).oPag.visible=this.oPgFrm.Pages(6).enabled
    local i_show6
    i_show6=not(IsAlt())
    this.oPgFrm.Pages(7).enabled=i_show6 and not(IsAlt())
    this.oPgFrm.Pages(7).caption=iif(i_show6,cp_translate("Riferimenti"),"")
    this.oPgFrm.Pages(7).oPag.visible=this.oPgFrm.Pages(7).enabled
    local i_show8
    i_show8=not(!g_VEFA='S')
    this.oPgFrm.Pages(9).enabled=i_show8 and not(!g_VEFA='S')
    this.oPgFrm.Pages(9).caption=iif(i_show8,cp_translate("EDI"),"")
    this.oPgFrm.Pages(9).oPag.visible=this.oPgFrm.Pages(9).enabled
    local i_show10
    i_show10=not(g_ISDF<>'S')
    this.oPgFrm.Pages(11).enabled=i_show10
    this.oPgFrm.Pages(11).caption=iif(i_show10,cp_translate("Dati DocFinance"),"")
    this.oPgFrm.Pages(11).oPag.visible=this.oPgFrm.Pages(11).enabled
    this.oPgFrm.Page1.oPag.oANCODSAL_1_28.visible=!this.oPgFrm.Page1.oPag.oANCODSAL_1_28.mHide()
    this.oPgFrm.Page1.oPag.oANCHKSTA_1_57.visible=!this.oPgFrm.Page1.oPag.oANCHKSTA_1_57.mHide()
    this.oPgFrm.Page1.oPag.oANCHKMAI_1_58.visible=!this.oPgFrm.Page1.oPag.oANCHKMAI_1_58.mHide()
    this.oPgFrm.Page1.oPag.oANCHKPEC_1_59.visible=!this.oPgFrm.Page1.oPag.oANCHKPEC_1_59.mHide()
    this.oPgFrm.Page1.oPag.oANCHKFAX_1_60.visible=!this.oPgFrm.Page1.oPag.oANCHKFAX_1_60.mHide()
    this.oPgFrm.Page1.oPag.oANCHKCPZ_1_61.visible=!this.oPgFrm.Page1.oPag.oANCHKCPZ_1_61.mHide()
    this.oPgFrm.Page1.oPag.oANCHKWWP_1_62.visible=!this.oPgFrm.Page1.oPag.oANCHKWWP_1_62.mHide()
    this.oPgFrm.Page1.oPag.oANCHKPTL_1_63.visible=!this.oPgFrm.Page1.oPag.oANCHKPTL_1_63.mHide()
    this.oPgFrm.Page1.oPag.oANCHKFIR_1_64.visible=!this.oPgFrm.Page1.oPag.oANCHKFIR_1_64.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_70.visible=!this.oPgFrm.Page1.oPag.oStr_1_70.mHide()
    this.oPgFrm.Page1.oPag.oANDTINVA_1_72.visible=!this.oPgFrm.Page1.oPag.oANDTINVA_1_72.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_83.visible=!this.oPgFrm.Page1.oPag.oBtn_1_83.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_84.visible=!this.oPgFrm.Page1.oPag.oBtn_1_84.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_85.visible=!this.oPgFrm.Page1.oPag.oBtn_1_85.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_89.visible=!this.oPgFrm.Page1.oPag.oBtn_1_89.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_90.visible=!this.oPgFrm.Page1.oPag.oStr_1_90.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_94.visible=!this.oPgFrm.Page1.oPag.oBtn_1_94.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_95.visible=!this.oPgFrm.Page1.oPag.oBtn_1_95.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_96.visible=!this.oPgFrm.Page1.oPag.oStr_1_96.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_101.visible=!this.oPgFrm.Page1.oPag.oBtn_1_101.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_104.visible=!this.oPgFrm.Page1.oPag.oBtn_1_104.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_105.visible=!this.oPgFrm.Page1.oPag.oBtn_1_105.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_106.visible=!this.oPgFrm.Page1.oPag.oBtn_1_106.mHide()
    this.oPgFrm.Page2.oPag.oANCONCAU_2_21.visible=!this.oPgFrm.Page2.oPag.oANCONCAU_2_21.mHide()
    this.oPgFrm.Page2.oPag.oANFLFIDO_2_49.visible=!this.oPgFrm.Page2.oPag.oANFLFIDO_2_49.mHide()
    this.oPgFrm.Page2.oPag.oANCODSTU_2_52.visible=!this.oPgFrm.Page2.oPag.oANCODSTU_2_52.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_53.visible=!this.oPgFrm.Page2.oPag.oBtn_2_53.mHide()
    this.oPgFrm.Page2.oPag.oANCODSOG_2_54.visible=!this.oPgFrm.Page2.oPag.oANCODSOG_2_54.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_57.visible=!this.oPgFrm.Page2.oPag.oBtn_2_57.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_59.visible=!this.oPgFrm.Page2.oPag.oBtn_2_59.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_60.visible=!this.oPgFrm.Page2.oPag.oStr_2_60.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_61.visible=!this.oPgFrm.Page2.oPag.oBtn_2_61.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_62.visible=!this.oPgFrm.Page2.oPag.oStr_2_62.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_63.visible=!this.oPgFrm.Page2.oPag.oStr_2_63.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_64.visible=!this.oPgFrm.Page2.oPag.oStr_2_64.mHide()
    this.oPgFrm.Page2.oPag.oMINCON_2_67.visible=!this.oPgFrm.Page2.oPag.oMINCON_2_67.mHide()
    this.oPgFrm.Page2.oPag.oMAXCON_2_68.visible=!this.oPgFrm.Page2.oPag.oMAXCON_2_68.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_70.visible=!this.oPgFrm.Page2.oPag.oStr_2_70.mHide()
    this.oPgFrm.Page2.oPag.oDESCAU_2_71.visible=!this.oPgFrm.Page2.oPag.oDESCAU_2_71.mHide()
    this.oPgFrm.Page3.oPag.oANFLCONA_3_5.visible=!this.oPgFrm.Page3.oPag.oANFLCONA_3_5.mHide()
    this.oPgFrm.Page3.oPag.oANCATSCM_3_6.visible=!this.oPgFrm.Page3.oPag.oANCATSCM_3_6.mHide()
    this.oPgFrm.Page3.oPag.oANFLGCAU_3_7.visible=!this.oPgFrm.Page3.oPag.oANFLGCAU_3_7.mHide()
    this.oPgFrm.Page3.oPag.oDESSCM_3_8.visible=!this.oPgFrm.Page3.oPag.oDESSCM_3_8.mHide()
    this.oPgFrm.Page3.oPag.oANTIPFAT_3_9.visible=!this.oPgFrm.Page3.oPag.oANTIPFAT_3_9.mHide()
    this.oPgFrm.Page3.oPag.oANGRUPRO_3_10.visible=!this.oPgFrm.Page3.oPag.oANGRUPRO_3_10.mHide()
    this.oPgFrm.Page3.oPag.oDESGPP_3_12.visible=!this.oPgFrm.Page3.oPag.oDESGPP_3_12.mHide()
    this.oPgFrm.Page3.oPag.oANCONCON_3_14.visible=!this.oPgFrm.Page3.oPag.oANCONCON_3_14.mHide()
    this.oPgFrm.Page3.oPag.oANCODCUC_3_22.visible=!this.oPgFrm.Page3.oPag.oANCODCUC_3_22.mHide()
    this.oPgFrm.Page3.oPag.oAN1SCONT_3_23.visible=!this.oPgFrm.Page3.oPag.oAN1SCONT_3_23.mHide()
    this.oPgFrm.Page3.oPag.oAN2SCONT_3_25.visible=!this.oPgFrm.Page3.oPag.oAN2SCONT_3_25.mHide()
    this.oPgFrm.Page3.oPag.oANCODAG1_3_27.visible=!this.oPgFrm.Page3.oPag.oANCODAG1_3_27.mHide()
    this.oPgFrm.Page3.oPag.oANTIPOCL_3_28.visible=!this.oPgFrm.Page3.oPag.oANTIPOCL_3_28.mHide()
    this.oPgFrm.Page3.oPag.oANMAGTER_3_29.visible=!this.oPgFrm.Page3.oPag.oANMAGTER_3_29.mHide()
    this.oPgFrm.Page3.oPag.oANCODPOR_3_31.visible=!this.oPgFrm.Page3.oPag.oANCODPOR_3_31.mHide()
    this.oPgFrm.Page3.oPag.oANMTDCLC_3_32.visible=!this.oPgFrm.Page3.oPag.oANMTDCLC_3_32.mHide()
    this.oPgFrm.Page3.oPag.oANMCALSI_3_33.visible=!this.oPgFrm.Page3.oPag.oANMCALSI_3_33.mHide()
    this.oPgFrm.Page3.oPag.oANMCALST_3_34.visible=!this.oPgFrm.Page3.oPag.oANMCALST_3_34.mHide()
    this.oPgFrm.Page3.oPag.oDESAGE1_3_39.visible=!this.oPgFrm.Page3.oPag.oDESAGE1_3_39.mHide()
    this.oPgFrm.Page3.oPag.oDESMAG_3_40.visible=!this.oPgFrm.Page3.oPag.oDESMAG_3_40.mHide()
    this.oPgFrm.Page3.oPag.oANBOLFAT_3_41.visible=!this.oPgFrm.Page3.oPag.oANBOLFAT_3_41.mHide()
    this.oPgFrm.Page3.oPag.oANPREBOL_3_42.visible=!this.oPgFrm.Page3.oPag.oANPREBOL_3_42.mHide()
    this.oPgFrm.Page3.oPag.oANFLAPCA_3_43.visible=!this.oPgFrm.Page3.oPag.oANFLAPCA_3_43.mHide()
    this.oPgFrm.Page3.oPag.oLinkPC_3_44.visible=!this.oPgFrm.Page3.oPag.oLinkPC_3_44.mHide()
    this.oPgFrm.Page3.oPag.oANSCORPO_3_45.visible=!this.oPgFrm.Page3.oPag.oANSCORPO_3_45.mHide()
    this.oPgFrm.Page3.oPag.oANFLCODI_3_46.visible=!this.oPgFrm.Page3.oPag.oANFLCODI_3_46.mHide()
    this.oPgFrm.Page3.oPag.oANCLIPOS_3_47.visible=!this.oPgFrm.Page3.oPag.oANCLIPOS_3_47.mHide()
    this.oPgFrm.Page3.oPag.oANFLIMBA_3_50.visible=!this.oPgFrm.Page3.oPag.oANFLIMBA_3_50.mHide()
    this.oPgFrm.Page3.oPag.oANCODESC_3_54.visible=!this.oPgFrm.Page3.oPag.oANCODESC_3_54.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_56.visible=!this.oPgFrm.Page3.oPag.oBtn_3_56.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_57.visible=!this.oPgFrm.Page3.oPag.oBtn_3_57.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_58.visible=!this.oPgFrm.Page3.oPag.oBtn_3_58.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_59.visible=!this.oPgFrm.Page3.oPag.oBtn_3_59.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_60.visible=!this.oPgFrm.Page3.oPag.oStr_3_60.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_61.visible=!this.oPgFrm.Page3.oPag.oBtn_3_61.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_63.visible=!this.oPgFrm.Page3.oPag.oBtn_3_63.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_65.visible=!this.oPgFrm.Page3.oPag.oBtn_3_65.mHide()
    this.oPgFrm.Page3.oPag.oBtn_3_67.visible=!this.oPgFrm.Page3.oPag.oBtn_3_67.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_69.visible=!this.oPgFrm.Page3.oPag.oStr_3_69.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_70.visible=!this.oPgFrm.Page3.oPag.oStr_3_70.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_72.visible=!this.oPgFrm.Page3.oPag.oStr_3_72.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_73.visible=!this.oPgFrm.Page3.oPag.oStr_3_73.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_74.visible=!this.oPgFrm.Page3.oPag.oStr_3_74.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_75.visible=!this.oPgFrm.Page3.oPag.oStr_3_75.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_76.visible=!this.oPgFrm.Page3.oPag.oStr_3_76.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_79.visible=!this.oPgFrm.Page3.oPag.oStr_3_79.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_81.visible=!this.oPgFrm.Page3.oPag.oStr_3_81.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_83.visible=!this.oPgFrm.Page3.oPag.oStr_3_83.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_85.visible=!this.oPgFrm.Page3.oPag.oStr_3_85.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_87.visible=!this.oPgFrm.Page3.oPag.oStr_3_87.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_89.visible=!this.oPgFrm.Page3.oPag.oStr_3_89.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_91.visible=!this.oPgFrm.Page3.oPag.oStr_3_91.mHide()
    this.oPgFrm.Page3.oPag.oDESCRI_3_94.visible=!this.oPgFrm.Page3.oPag.oDESCRI_3_94.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_95.visible=!this.oPgFrm.Page3.oPag.oStr_3_95.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_96.visible=!this.oPgFrm.Page3.oPag.oStr_3_96.mHide()
    this.oPgFrm.Page3.oPag.oANMCALSI_3_97.visible=!this.oPgFrm.Page3.oPag.oANMCALSI_3_97.mHide()
    this.oPgFrm.Page3.oPag.oANMCALST_3_98.visible=!this.oPgFrm.Page3.oPag.oANMCALST_3_98.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_100.visible=!this.oPgFrm.Page3.oPag.oStr_3_100.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_101.visible=!this.oPgFrm.Page3.oPag.oStr_3_101.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_102.visible=!this.oPgFrm.Page3.oPag.oStr_3_102.mHide()
    this.oPgFrm.Page3.oPag.oDESTIP_3_103.visible=!this.oPgFrm.Page3.oPag.oDESTIP_3_103.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_104.visible=!this.oPgFrm.Page3.oPag.oStr_3_104.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_15.visible=!this.oPgFrm.Page4.oPag.oBtn_4_15.mHide()
    this.oPgFrm.Page4.oPag.oBtn_4_16.visible=!this.oPgFrm.Page4.oPag.oBtn_4_16.mHide()
    this.oPgFrm.Page5.oPag.oANSAGINT_5_6.visible=!this.oPgFrm.Page5.oPag.oANSAGINT_5_6.mHide()
    this.oPgFrm.Page5.oPag.oANSPRINT_5_7.visible=!this.oPgFrm.Page5.oPag.oANSPRINT_5_7.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_12.visible=!this.oPgFrm.Page5.oPag.oStr_5_12.mHide()
    this.oPgFrm.Page6.oPag.oANMAXORD_6_5.visible=!this.oPgFrm.Page6.oPag.oANMAXORD_6_5.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_7.visible=!this.oPgFrm.Page6.oPag.oStr_6_7.mHide()
    this.oPgFrm.Page6.oPag.oFIDORD_6_15.visible=!this.oPgFrm.Page6.oPag.oFIDORD_6_15.mHide()
    this.oPgFrm.Page6.oPag.oFIDDDT_6_16.visible=!this.oPgFrm.Page6.oPag.oFIDDDT_6_16.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_23.visible=!this.oPgFrm.Page6.oPag.oStr_6_23.mHide()
    this.oPgFrm.Page6.oPag.oStr_6_24.visible=!this.oPgFrm.Page6.oPag.oStr_6_24.mHide()
    this.oPgFrm.Page6.oPag.oFIDRES1_6_29.visible=!this.oPgFrm.Page6.oPag.oFIDRES1_6_29.mHide()
    this.oPgFrm.Page6.oPag.oFIDRES2_6_30.visible=!this.oPgFrm.Page6.oPag.oFIDRES2_6_30.mHide()
    this.oPgFrm.Page9.oPag.oLinkPC_9_6.visible=!this.oPgFrm.Page9.oPag.oLinkPC_9_6.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_122.visible=!this.oPgFrm.Page1.oPag.oBtn_1_122.mHide()
    this.oPgFrm.Page2.oPag.oBtn_2_89.visible=!this.oPgFrm.Page2.oPag.oBtn_2_89.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_123.visible=!this.oPgFrm.Page1.oPag.oStr_1_123.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_128.visible=!this.oPgFrm.Page1.oPag.oStr_1_128.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsar_acl
    IF Upper(CEVENT)='ACTIVATEPAGE 6' And This.cFunction <> 'Query'
      this.NotifyEvent('Sedi')
    Endif
    
    *--- Cliente Caricato, se offerte attivo genero il nominativo
    if cEvent='Record Inserted' and not(bTrsErr) and (g_OFFE='S' OR g_AGEN='S')  AND (VARTYPE(THIS.w_OGGETTO)<>'O' AND !THIS.w_DANOM)
      * --- Carica Riferimenti Cliente sui Nominativi Offerte
      this.NotifyEvent('ClienteCaricato')
    endif
    
    IF cevent = "ForzaRicalcolaStudio"
    this.w_ANCODSTU=''
    this.notifyevent ("RicalcolaStudio")
    ENDIF
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_74.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_76.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_77.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_78.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_79.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_80.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_92.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_93.Event(cEvent)
        if lower(cEvent)==lower("AggiornaCF") or lower(cEvent)==lower("w_ANCODFIS LostFocus")
          .Calculate_XNLBETFNTC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Insert start")
          .Calculate_OLYGVFQXTR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Delete end")
          .Calculate_UIROVJBFQO()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load") or lower(cEvent)==lower("Record Inserted") or lower(cEvent)==lower("Record Updated")
          .Calculate_PGQNPFGCXZ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Init")
          .Calculate_GRJFLMWOLU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Record Deleted")
          .Calculate_NTGPIXRFKL()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Delete start")
          .Calculate_CZGHMRBITS()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CaricaDati") or lower(cEvent)==lower("RicalcolaStudio")
          .Calculate_QAXUOQUTMC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CaricaDati") or lower(cEvent)==lower("Edit Started") or lower(cEvent)==lower("Load")
          .Calculate_FIRXFFUGWY()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("CaricaDati") or lower(cEvent)==lower("Edit Started") or lower(cEvent)==lower("Load")
          .Calculate_NODBJNDQUR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ANSCORPO Changed")
          .Calculate_VISJNUHVVI()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_SIGJGJWAYX()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsar_acl
    If cevent='Insert start'
       this.cCODICEDEF=This.w_ANCODICE
    endif
    IF cevent='Edit Aborted'
           if  VARTYPE(g_GSAR_KCC)='O'
           g_GSAR_KCC.ecpquit()
        Endif
    endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AUTOAZI
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NUMAUT_M_IDX,3]
    i_lTable = "NUMAUT_M"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2], .t., this.NUMAUT_M_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AUTOAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AUTOAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODAZI,NATIPGES,NAACTIVE";
                   +" from "+i_cTable+" "+i_lTable+" where NACODAZI="+cp_ToStrODBC(this.w_AUTOAZI);
                   +" and NACODAZI="+cp_ToStrODBC(this.w_AUTOAZI);
                   +" and NATIPGES="+cp_ToStrODBC('CL');
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODAZI',this.w_AUTOAZI;
                       ,'NATIPGES','CL';
                       ,'NACODAZI',this.w_AUTOAZI)
            select NACODAZI,NATIPGES,NAACTIVE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AUTOAZI = NVL(_Link_.NACODAZI,space(5))
      this.w_AUTO = NVL(_Link_.NAACTIVE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AUTOAZI = space(5)
      endif
      this.w_AUTO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NUMAUT_M_IDX,2])+'\'+cp_ToStr(_Link_.NACODAZI,1)+'\'+cp_ToStr(_Link_.NATIPGES,1)+'\'+cp_ToStr(_Link_.NACODAZI,1)
      cp_ShowWarn(i_cKey,this.NUMAUT_M_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AUTOAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANNAZION
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.NAZIONI_IDX,3]
    i_lTable = "NAZIONI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2], .t., this.NAZIONI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNAZION) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ANZ',True,'NAZIONI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" NACODNAZ like "+cp_ToStrODBC(trim(this.w_ANNAZION)+"%");

          i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by NACODNAZ","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'NACODNAZ',trim(this.w_ANNAZION))
          select NACODNAZ,NADESNAZ,NACODISO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by NACODNAZ into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANNAZION)==trim(_Link_.NACODNAZ) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANNAZION) and !this.bDontReportError
            deferred_cp_zoom('NAZIONI','*','NACODNAZ',cp_AbsName(oSource.parent,'oANNAZION_1_16'),i_cWhere,'GSAR_ANZ',"Nazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                     +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',oSource.xKey(1))
            select NACODNAZ,NADESNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNAZION)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select NACODNAZ,NADESNAZ,NACODISO";
                   +" from "+i_cTable+" "+i_lTable+" where NACODNAZ="+cp_ToStrODBC(this.w_ANNAZION);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'NACODNAZ',this.w_ANNAZION)
            select NACODNAZ,NADESNAZ,NACODISO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNAZION = NVL(_Link_.NACODNAZ,space(3))
      this.w_DESNAZ = NVL(_Link_.NADESNAZ,space(35))
      this.w_CODISO = NVL(_Link_.NACODISO,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ANNAZION = space(3)
      endif
      this.w_DESNAZ = space(35)
      this.w_CODISO = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])+'\'+cp_ToStr(_Link_.NACODNAZ,1)
      cp_ShowWarn(i_cKey,this.NAZIONI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNAZION Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_16(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.NAZIONI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.NAZIONI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_16.NACODNAZ as NACODNAZ116"+ ",link_1_16.NADESNAZ as NADESNAZ116"+ ",link_1_16.NACODISO as NACODISO116"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_16 on CONTI.ANNAZION=link_1_16.NACODNAZ"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_16"
          i_cKey=i_cKey+'+" and CONTI.ANNAZION=link_1_16.NACODNAZ(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODSAL
  func Link_1_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SAL_NOMI_IDX,3]
    i_lTable = "SAL_NOMI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SAL_NOMI_IDX,2], .t., this.SAL_NOMI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SAL_NOMI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODSAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'SAL_NOMI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SACODICE like "+cp_ToStrODBC(trim(this.w_ANCODSAL)+"%");

          i_ret=cp_SQL(i_nConn,"select SACODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SACODICE',trim(this.w_ANCODSAL))
          select SACODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODSAL)==trim(_Link_.SACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODSAL) and !this.bDontReportError
            deferred_cp_zoom('SAL_NOMI','*','SACODICE',cp_AbsName(oSource.parent,'oANCODSAL_1_28'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SACODICE";
                     +" from "+i_cTable+" "+i_lTable+" where SACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SACODICE',oSource.xKey(1))
            select SACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODSAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SACODICE";
                   +" from "+i_cTable+" "+i_lTable+" where SACODICE="+cp_ToStrODBC(this.w_ANCODSAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SACODICE',this.w_ANCODSAL)
            select SACODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODSAL = NVL(_Link_.SACODICE,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODSAL = space(5)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SAL_NOMI_IDX,2])+'\'+cp_ToStr(_Link_.SACODICE,1)
      cp_ShowWarn(i_cKey,this.SAL_NOMI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODSAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI1
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_OFFE_IDX,3]
    i_lTable = "PAR_OFFE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2], .t., this.PAR_OFFE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select POCODAZI,POMASCOC,POCATCOC,POCODPAG";
                   +" from "+i_cTable+" "+i_lTable+" where POCODAZI="+cp_ToStrODBC(this.w_CODAZI1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'POCODAZI',this.w_CODAZI1)
            select POCODAZI,POMASCOC,POCATCOC,POCODPAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI1 = NVL(_Link_.POCODAZI,space(5))
      this.w_MASTRO = NVL(_Link_.POMASCOC,space(15))
      this.w_CATEGORIA = NVL(_Link_.POCATCOC,space(5))
      this.w_CODPAG = NVL(_Link_.POCODPAG,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI1 = space(5)
      endif
      this.w_MASTRO = space(15)
      this.w_CATEGORIA = space(5)
      this.w_CODPAG = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_OFFE_IDX,2])+'\'+cp_ToStr(_Link_.POCODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_OFFE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCONSUP
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MASTRI_IDX,3]
    i_lTable = "MASTRI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2], .t., this.MASTRI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCONSUP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMC',True,'MASTRI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MCCODICE like "+cp_ToStrODBC(trim(this.w_ANCONSUP)+"%");

          i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU,MCCONSUP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MCCODICE',trim(this.w_ANCONSUP))
          select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU,MCCONSUP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCONSUP)==trim(_Link_.MCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStrODBC(trim(this.w_ANCONSUP)+"%");

            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU,MCCONSUP";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MCDESCRI like "+cp_ToStr(trim(this.w_ANCONSUP)+"%");

            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU,MCCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCONSUP) and !this.bDontReportError
            deferred_cp_zoom('MASTRI','*','MCCODICE',cp_AbsName(oSource.parent,'oANCONSUP_2_7'),i_cWhere,'GSAR_AMC',"Mastri contabili (clienti)",'GSAR_ACL.MASTRI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU,MCCONSUP";
                     +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',oSource.xKey(1))
            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU,MCCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCONSUP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU,MCCONSUP";
                   +" from "+i_cTable+" "+i_lTable+" where MCCODICE="+cp_ToStrODBC(this.w_ANCONSUP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MCCODICE',this.w_ANCONSUP)
            select MCCODICE,MCDESCRI,MCNUMLIV,MCTIPMAS,MCCODSTU,MCPROSTU,MCCONSUP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCONSUP = NVL(_Link_.MCCODICE,space(15))
      this.w_DESSUP = NVL(_Link_.MCDESCRI,space(40))
      this.w_NULIV = NVL(_Link_.MCNUMLIV,0)
      this.w_TIPMAS = NVL(_Link_.MCTIPMAS,space(1))
      this.w_CODSTU = NVL(_Link_.MCCODSTU,space(3))
      this.w_PROSTU = NVL(_Link_.MCPROSTU,space(8))
      this.w_CONSUP = NVL(_Link_.MCCONSUP,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_ANCONSUP = space(15)
      endif
      this.w_DESSUP = space(40)
      this.w_NULIV = 0
      this.w_TIPMAS = space(1)
      this.w_CODSTU = space(3)
      this.w_PROSTU = space(8)
      this.w_CONSUP = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_NULIV=1 AND .w_TIPMAS='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ANCONSUP = space(15)
        this.w_DESSUP = space(40)
        this.w_NULIV = 0
        this.w_TIPMAS = space(1)
        this.w_CODSTU = space(3)
        this.w_PROSTU = space(8)
        this.w_CONSUP = space(15)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])+'\'+cp_ToStr(_Link_.MCCODICE,1)
      cp_ShowWarn(i_cKey,this.MASTRI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCONSUP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 7 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MASTRI_IDX,3] and i_nFlds+7<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MASTRI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_7.MCCODICE as MCCODICE207"+ ",link_2_7.MCDESCRI as MCDESCRI207"+ ",link_2_7.MCNUMLIV as MCNUMLIV207"+ ",link_2_7.MCTIPMAS as MCTIPMAS207"+ ",link_2_7.MCCODSTU as MCCODSTU207"+ ",link_2_7.MCPROSTU as MCPROSTU207"+ ",link_2_7.MCCONSUP as MCCONSUP207"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_7 on CONTI.ANCONSUP=link_2_7.MCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_7"
          i_cKey=i_cKey+'+" and CONTI.ANCONSUP=link_2_7.MCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+7
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCATCON
  func Link_2_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CACOCLFO_IDX,3]
    i_lTable = "CACOCLFO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2], .t., this.CACOCLFO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCATCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AC2',True,'CACOCLFO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" C2CODICE like "+cp_ToStrODBC(trim(this.w_ANCATCON)+"%");

          i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by C2CODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'C2CODICE',trim(this.w_ANCATCON))
          select C2CODICE,C2DESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by C2CODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCATCON)==trim(_Link_.C2CODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCATCON) and !this.bDontReportError
            deferred_cp_zoom('CACOCLFO','*','C2CODICE',cp_AbsName(oSource.parent,'oANCATCON_2_9'),i_cWhere,'GSAR_AC2',"Categorie contabili clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',oSource.xKey(1))
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCATCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select C2CODICE,C2DESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where C2CODICE="+cp_ToStrODBC(this.w_ANCATCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'C2CODICE',this.w_ANCATCON)
            select C2CODICE,C2DESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCATCON = NVL(_Link_.C2CODICE,space(5))
      this.w_DESCON = NVL(_Link_.C2DESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ANCATCON = space(5)
      endif
      this.w_DESCON = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])+'\'+cp_ToStr(_Link_.C2CODICE,1)
      cp_ShowWarn(i_cKey,this.CACOCLFO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCATCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CACOCLFO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CACOCLFO_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_9.C2CODICE as C2CODICE209"+ ",link_2_9.C2DESCRI as C2DESCRI209"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_9 on CONTI.ANCATCON=link_2_9.C2CODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_9"
          i_cKey=i_cKey+'+" and CONTI.ANCATCON=link_2_9.C2CODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCONRIF
  func Link_2_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCONRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_ANCONRIF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPRIF);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPRIF;
                     ,'ANCODICE',trim(this.w_ANCONRIF))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCONRIF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_ANCONRIF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPRIF);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_ANCONRIF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPRIF);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCONRIF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oANCONRIF_2_14'),i_cWhere,'GSAR_BZC',"Fornitore originario",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPRIF<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPRIF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCONRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_ANCONRIF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPRIF;
                       ,'ANCODICE',this.w_ANCONRIF)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCONRIF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESORI = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ANCONRIF = space(15)
      endif
      this.w_DESORI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice conto inesistente oppure obsoleto")
        endif
        this.w_ANCONRIF = space(15)
        this.w_DESORI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCONRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCODIVA
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOCIIVA_IDX,3]
    i_lTable = "VOCIIVA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2], .t., this.VOCIIVA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AIV',True,'VOCIIVA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" IVCODIVA like "+cp_ToStrODBC(trim(this.w_ANCODIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by IVCODIVA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'IVCODIVA',trim(this.w_ANCODIVA))
          select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by IVCODIVA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODIVA)==trim(_Link_.IVCODIVA) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStrODBC(trim(this.w_ANCODIVA)+"%");

            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" IVDESIVA like "+cp_ToStr(trim(this.w_ANCODIVA)+"%");

            select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCODIVA) and !this.bDontReportError
            deferred_cp_zoom('VOCIIVA','*','IVCODIVA',cp_AbsName(oSource.parent,'oANCODIVA_2_17'),i_cWhere,'GSAR_AIV',"Codici IVA",'GSVE0MDV.VOCIIVA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',oSource.xKey(1))
            select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where IVCODIVA="+cp_ToStrODBC(this.w_ANCODIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'IVCODIVA',this.w_ANCODIVA)
            select IVCODIVA,IVDESIVA,IVPERIVA,IVDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODIVA = NVL(_Link_.IVCODIVA,space(5))
      this.w_DESIVA = NVL(_Link_.IVDESIVA,space(35))
      this.w_PERIVA = NVL(_Link_.IVPERIVA,0)
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.IVDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODIVA = space(5)
      endif
      this.w_DESIVA = space(35)
      this.w_PERIVA = 0
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice IVA inesistente oppure obsoleto")
        endif
        this.w_ANCODIVA = space(5)
        this.w_DESIVA = space(35)
        this.w_PERIVA = 0
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])+'\'+cp_ToStr(_Link_.IVCODIVA,1)
      cp_ShowWarn(i_cKey,this.VOCIIVA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOCIIVA_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOCIIVA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_17.IVCODIVA as IVCODIVA217"+ ",link_2_17.IVDESIVA as IVDESIVA217"+ ",link_2_17.IVPERIVA as IVPERIVA217"+ ",link_2_17.IVDTOBSO as IVDTOBSO217"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_17 on CONTI.ANCODIVA=link_2_17.IVCODIVA"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_17"
          i_cKey=i_cKey+'+" and CONTI.ANCODIVA=link_2_17.IVCODIVA(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANTIPOPE
  func Link_2_20(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIPCODIV_IDX,3]
    i_lTable = "TIPCODIV"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2], .t., this.TIPCODIV_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANTIPOPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATO',True,'TIPCODIV')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TICODICE like "+cp_ToStrODBC(trim(this.w_ANTIPOPE)+"%");
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_ANCATOPE);

          i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TI__TIPO,TICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TI__TIPO',this.w_ANCATOPE;
                     ,'TICODICE',trim(this.w_ANTIPOPE))
          select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TI__TIPO,TICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANTIPOPE)==trim(_Link_.TICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANTIPOPE) and !this.bDontReportError
            deferred_cp_zoom('TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(oSource.parent,'oANTIPOPE_2_20'),i_cWhere,'GSAR_ATO',"Operazioni IVA",'GSVE_ZTI.TIPCODIV_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ANCATOPE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice incongruente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TI__TIPO="+cp_ToStrODBC(this.w_ANCATOPE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',oSource.xKey(1);
                       ,'TICODICE',oSource.xKey(2))
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANTIPOPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where TICODICE="+cp_ToStrODBC(this.w_ANTIPOPE);
                   +" and TI__TIPO="+cp_ToStrODBC(this.w_ANCATOPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TI__TIPO',this.w_ANCATOPE;
                       ,'TICODICE',this.w_ANTIPOPE)
            select TI__TIPO,TICODICE,TIDESCRI,TIDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANTIPOPE = NVL(_Link_.TICODICE,space(10))
      this.w_TIDESCRI = NVL(_Link_.TIDESCRI,space(30))
      this.w_TIDTOBSO = NVL(cp_ToDate(_Link_.TIDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ANTIPOPE = space(10)
      endif
      this.w_TIDESCRI = space(30)
      this.w_TIDTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_TIDTOBSO) OR .w_TIDTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice incongruente oppure obsoleto")
        endif
        this.w_ANTIPOPE = space(10)
        this.w_TIDESCRI = space(30)
        this.w_TIDTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIPCODIV_IDX,2])+'\'+cp_ToStr(_Link_.TI__TIPO,1)+'\'+cp_ToStr(_Link_.TICODICE,1)
      cp_ShowWarn(i_cKey,this.TIPCODIV_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANTIPOPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCONCAU
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCONCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_ANCONCAU)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCC);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCC;
                     ,'ANCODICE',trim(this.w_ANCONCAU))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCONCAU)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCONCAU) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oANCONCAU_2_21'),i_cWhere,'GSAR_BZC',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCC<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCC);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCONCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_ANCONCAU);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCC;
                       ,'ANCODICE',this.w_ANCONCAU)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCONCAU = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCAU = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ANCONCAU = space(15)
      endif
      this.w_DESCAU = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCONCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCODIRP
  func Link_2_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODIRP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_ANCODIRP)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_ANCODIRP))
          select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODIRP)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODIRP) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oANCODIRP_2_24'),i_cWhere,'GSAR_ATB',"Elenco tributi",'GSRI1AMR.TRI_BUTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODIRP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_ANCODIRP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_ANCODIRP)
            select TRCODTRI,TRDESTRI,TRFLACON,TRCAUPRE2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODIRP = NVL(_Link_.TRCODTRI,space(5))
      this.w_DESIRPEF = NVL(_Link_.TRDESTRI,space(60))
      this.w_FLIRPE = NVL(_Link_.TRFLACON,space(1))
      this.w_CAUPRE2 = NVL(_Link_.TRCAUPRE2,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODIRP = space(5)
      endif
      this.w_DESIRPEF = space(60)
      this.w_FLIRPE = space(1)
      this.w_CAUPRE2 = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLIRPE='R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire un codice tributo I.R.PE.F.")
        endif
        this.w_ANCODIRP = space(5)
        this.w_DESIRPEF = space(60)
        this.w_FLIRPE = space(1)
        this.w_CAUPRE2 = space(2)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODIRP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_24(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TRI_BUTI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_24.TRCODTRI as TRCODTRI224"+ ",link_2_24.TRDESTRI as TRDESTRI224"+ ",link_2_24.TRFLACON as TRFLACON224"+ ",link_2_24.TRCAUPRE2 as TRCAUPRE2224"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_24 on CONTI.ANCODIRP=link_2_24.TRCODTRI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_24"
          i_cKey=i_cKey+'+" and CONTI.ANCODIRP=link_2_24.TRCODTRI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODCAT
  func Link_2_55(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_CATA_IDX,3]
    i_lTable = "COD_CATA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_CATA_IDX,2], .t., this.COD_CATA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_CATA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACC',True,'COD_CATA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_ANCODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_ANCODCAT))
          select CCCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODCAT)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODCAT) and !this.bDontReportError
            deferred_cp_zoom('COD_CATA','*','CCCODICE',cp_AbsName(oSource.parent,'oANCODCAT_2_55'),i_cWhere,'GSAR_ACC',"Elenco codici catastali dei comuni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_ANCODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_ANCODCAT)
            select CCCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODCAT = NVL(_Link_.CCCODICE,space(4))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODCAT = space(4)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_CATA_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_CATA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCATCOM
  func Link_3_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCATCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_ANCATCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_ANCATCOM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCATCOM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCATCOM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oANCATCOM_3_3'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCATCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_ANCATCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_ANCATCOM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCATCOM = NVL(_Link_.CTCODICE,space(3))
      this.w_DESCAC = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ANCATCOM = space(3)
      endif
      this.w_DESCAC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCATCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CATECOMM_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_3.CTCODICE as CTCODICE303"+ ",link_3_3.CTDESCRI as CTDESCRI303"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_3 on CONTI.ANCATCOM=link_3_3.CTCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_3"
          i_cKey=i_cKey+'+" and CONTI.ANCATCOM=link_3_3.CTCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCATSCM
  func Link_3_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAT_SCMA_IDX,3]
    i_lTable = "CAT_SCMA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2], .t., this.CAT_SCMA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCATSCM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASM',True,'CAT_SCMA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CSCODICE like "+cp_ToStrODBC(trim(this.w_ANCATSCM)+"%");

          i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CSCODICE',trim(this.w_ANCATSCM))
          select CSCODICE,CSDESCRI,CSTIPCAT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCATSCM)==trim(_Link_.CSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCATSCM) and !this.bDontReportError
            deferred_cp_zoom('CAT_SCMA','*','CSCODICE',cp_AbsName(oSource.parent,'oANCATSCM_3_6'),i_cWhere,'GSAR_ASM',"Categorie sconti/maggiorazioni",'GSAR_ACL.CAT_SCMA_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                     +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',oSource.xKey(1))
            select CSCODICE,CSDESCRI,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCATSCM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CSCODICE,CSDESCRI,CSTIPCAT";
                   +" from "+i_cTable+" "+i_lTable+" where CSCODICE="+cp_ToStrODBC(this.w_ANCATSCM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CSCODICE',this.w_ANCATSCM)
            select CSCODICE,CSDESCRI,CSTIPCAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCATSCM = NVL(_Link_.CSCODICE,space(5))
      this.w_DESSCM = NVL(_Link_.CSDESCRI,space(35))
      this.w_FLSCM = NVL(_Link_.CSTIPCAT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANCATSCM = space(5)
      endif
      this.w_DESSCM = space(35)
      this.w_FLSCM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLSCM $ ' C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria sconti / maggiorazioni inesistente o non di tipo cliente")
        endif
        this.w_ANCATSCM = space(5)
        this.w_DESSCM = space(35)
        this.w_FLSCM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])+'\'+cp_ToStr(_Link_.CSCODICE,1)
      cp_ShowWarn(i_cKey,this.CAT_SCMA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCATSCM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAT_SCMA_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAT_SCMA_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_6.CSCODICE as CSCODICE306"+ ",link_3_6.CSDESCRI as CSDESCRI306"+ ",link_3_6.CSTIPCAT as CSTIPCAT306"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_6 on CONTI.ANCATSCM=link_3_6.CSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_6"
          i_cKey=i_cKey+'+" and CONTI.ANCATSCM=link_3_6.CSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANGRUPRO
  func Link_3_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUPRO_IDX,3]
    i_lTable = "GRUPRO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2], .t., this.GRUPRO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANGRUPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGP',True,'GRUPRO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GPCODICE like "+cp_ToStrODBC(trim(this.w_ANGRUPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GPCODICE',trim(this.w_ANGRUPRO))
          select GPCODICE,GPDESCRI,GPTIPPRO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANGRUPRO)==trim(_Link_.GPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANGRUPRO) and !this.bDontReportError
            deferred_cp_zoom('GRUPRO','*','GPCODICE',cp_AbsName(oSource.parent,'oANGRUPRO_3_10'),i_cWhere,'GSAR_AGP',"Categorie provvigioni",'GSAR_ACL.GRUPRO_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                     +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',oSource.xKey(1))
            select GPCODICE,GPDESCRI,GPTIPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANGRUPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GPCODICE,GPDESCRI,GPTIPPRO";
                   +" from "+i_cTable+" "+i_lTable+" where GPCODICE="+cp_ToStrODBC(this.w_ANGRUPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GPCODICE',this.w_ANGRUPRO)
            select GPCODICE,GPDESCRI,GPTIPPRO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANGRUPRO = NVL(_Link_.GPCODICE,space(5))
      this.w_DESGPP = NVL(_Link_.GPDESCRI,space(35))
      this.w_FLPRO = NVL(_Link_.GPTIPPRO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANGRUPRO = space(5)
      endif
      this.w_DESGPP = space(35)
      this.w_FLPRO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLPRO $ ' C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Categoria provvigioni inesistente o non di tipo cliente")
        endif
        this.w_ANGRUPRO = space(5)
        this.w_DESGPP = space(35)
        this.w_FLPRO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])+'\'+cp_ToStr(_Link_.GPCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUPRO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANGRUPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRUPRO_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRUPRO_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_10.GPCODICE as GPCODICE310"+ ",link_3_10.GPDESCRI as GPDESCRI310"+ ",link_3_10.GPTIPPRO as GPTIPPRO310"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_10 on CONTI.ANGRUPRO=link_3_10.GPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_10"
          i_cKey=i_cKey+'+" and CONTI.ANGRUPRO=link_3_10.GPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCONCON
  func Link_3_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_CONS_IDX,3]
    i_lTable = "CON_CONS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_CONS_IDX,2], .t., this.CON_CONS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_CONS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCONCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CON_CONS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" COCODICE like "+cp_ToStrODBC(trim(this.w_ANCONCON)+"%");
                   +" and CCCODISO="+cp_ToStrODBC(this.w_ANCODISO);

          i_ret=cp_SQL(i_nConn,"select CCCODISO,COCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODISO,COCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODISO',this.w_ANCODISO;
                     ,'COCODICE',trim(this.w_ANCONCON))
          select CCCODISO,COCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODISO,COCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCONCON)==trim(_Link_.COCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCONCON) and !this.bDontReportError
            deferred_cp_zoom('CON_CONS','*','CCCODISO,COCODICE',cp_AbsName(oSource.parent,'oANCONCON_3_14'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ANCODISO<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODISO,COCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CCCODISO,COCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODISO,COCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where COCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and CCCODISO="+cp_ToStrODBC(this.w_ANCODISO);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODISO',oSource.xKey(1);
                       ,'COCODICE',oSource.xKey(2))
            select CCCODISO,COCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCONCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODISO,COCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where COCODICE="+cp_ToStrODBC(this.w_ANCONCON);
                   +" and CCCODISO="+cp_ToStrODBC(this.w_ANCODISO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODISO',this.w_ANCODISO;
                       ,'COCODICE',this.w_ANCONCON)
            select CCCODISO,COCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCONCON = NVL(_Link_.COCODICE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANCONCON = space(1)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_CONS_IDX,2])+'\'+cp_ToStr(_Link_.CCCODISO,1)+'\'+cp_ToStr(_Link_.COCODICE,1)
      cp_ShowWarn(i_cKey,this.CON_CONS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCONCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCODLIN
  func Link_3_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODLIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALG',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_ANCODLIN)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_ANCODLIN))
          select LUCODICE,LUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODLIN)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODLIN) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'oANCODLIN_3_17'),i_cWhere,'GSAR_ALG',"Lingue",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODLIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_ANCODLIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_ANCODLIN)
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODLIN = NVL(_Link_.LUCODICE,space(3))
      this.w_DESLIN = NVL(_Link_.LUDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODLIN = space(3)
      endif
      this.w_DESLIN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODLIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LINGUE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_17.LUCODICE as LUCODICE317"+ ",link_3_17.LUDESCRI as LUDESCRI317"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_17 on CONTI.ANCODLIN=link_3_17.LUCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_17"
          i_cKey=i_cKey+'+" and CONTI.ANCODLIN=link_3_17.LUCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODVAL
  func Link_3_19(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_ANCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_ANCODVAL))
          select VACODVAL,VADESVAL,VADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_ANCODVAL)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_ANCODVAL)+"%");

            select VACODVAL,VADESVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oANCODVAL_3_19'),i_cWhere,'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_ANCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_ANCODVAL)
            select VACODVAL,VADESVAL,VADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKVLCF(.w_ANCODVAL, .w_VALLIS, "CLIENTE", 'X', ' ') AND CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .T.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ANCODVAL = space(3)
        this.w_DESVAL = space(35)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_19(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_19.VACODVAL as VACODVAL319"+ ",link_3_19.VADESVAL as VADESVAL319"+ ",link_3_19.VADTOBSO as VADTOBSO319"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_19 on CONTI.ANCODVAL=link_3_19.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_19"
          i_cKey=i_cKey+'+" and CONTI.ANCODVAL=link_3_19.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANNUMLIS
  func Link_3_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANNUMLIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_ANNUMLIS)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_ANNUMLIS))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANNUMLIS)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_ANNUMLIS)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_ANNUMLIS)+"%");

            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANNUMLIS) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oANNUMLIS_3_21'),i_cWhere,'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANNUMLIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_ANNUMLIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_ANNUMLIS)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANNUMLIS = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESLIS = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALLIS = NVL(_Link_.LSVALLIS,space(3))
      this.w_IVALIS = NVL(_Link_.LSIVALIS,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANNUMLIS = space(5)
      endif
      this.w_DESLIS = space(40)
      this.w_VALLIS = space(3)
      this.w_IVALIS = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKVLCF(.w_ANCODVAL, .w_VALLIS, "CLIENTE", .w_ANSCORPO, .w_IVALIS)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_ANNUMLIS = space(5)
        this.w_DESLIS = space(40)
        this.w_VALLIS = space(3)
        this.w_IVALIS = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANNUMLIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.LISTINI_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_21.LSCODLIS as LSCODLIS321"+ ",link_3_21.LSDESLIS as LSDESLIS321"+ ",link_3_21.LSVALLIS as LSVALLIS321"+ ",link_3_21.LSIVALIS as LSIVALIS321"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_21 on CONTI.ANNUMLIS=link_3_21.LSCODLIS"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_21"
          i_cKey=i_cKey+'+" and CONTI.ANNUMLIS=link_3_21.LSCODLIS(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANGRPDEF
  func Link_3_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRP_DEFA_IDX,3]
    i_lTable = "GRP_DEFA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRP_DEFA_IDX,2], .t., this.GRP_DEFA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRP_DEFA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANGRPDEF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gsar_agd',True,'GRP_DEFA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GDCODICE like "+cp_ToStrODBC(trim(this.w_ANGRPDEF)+"%");

          i_ret=cp_SQL(i_nConn,"select GDCODICE,GDDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GDCODICE',trim(this.w_ANGRPDEF))
          select GDCODICE,GDDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANGRPDEF)==trim(_Link_.GDCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANGRPDEF) and !this.bDontReportError
            deferred_cp_zoom('GRP_DEFA','*','GDCODICE',cp_AbsName(oSource.parent,'oANGRPDEF_3_24'),i_cWhere,'gsar_agd',"Gruppi output",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GDCODICE,GDDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GDCODICE',oSource.xKey(1))
            select GDCODICE,GDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANGRPDEF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GDCODICE,GDDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GDCODICE="+cp_ToStrODBC(this.w_ANGRPDEF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GDCODICE',this.w_ANGRPDEF)
            select GDCODICE,GDDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANGRPDEF = NVL(_Link_.GDCODICE,space(5))
      this.w_GDDESCRI = NVL(_Link_.GDDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ANGRPDEF = space(5)
      endif
      this.w_GDDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRP_DEFA_IDX,2])+'\'+cp_ToStr(_Link_.GDCODICE,1)
      cp_ShowWarn(i_cKey,this.GRP_DEFA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANGRPDEF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_24(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRP_DEFA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRP_DEFA_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_24.GDCODICE as GDCODICE324"+ ",link_3_24.GDDESCRI as GDDESCRI324"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_24 on CONTI.ANGRPDEF=link_3_24.GDCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_24"
          i_cKey=i_cKey+'+" and CONTI.ANGRPDEF=link_3_24.GDCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODZON
  func Link_3_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ZONE_IDX,3]
    i_lTable = "ZONE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2], .t., this.ZONE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODZON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AZO',True,'ZONE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ZOCODZON like "+cp_ToStrODBC(trim(this.w_ANCODZON)+"%");

          i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ZOCODZON","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ZOCODZON',trim(this.w_ANCODZON))
          select ZOCODZON,ZODESZON,ZODTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ZOCODZON into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODZON)==trim(_Link_.ZOCODZON) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODZON) and !this.bDontReportError
            deferred_cp_zoom('ZONE','*','ZOCODZON',cp_AbsName(oSource.parent,'oANCODZON_3_26'),i_cWhere,'GSAR_AZO',"Zone",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',oSource.xKey(1))
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODZON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ZOCODZON,ZODESZON,ZODTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ZOCODZON="+cp_ToStrODBC(this.w_ANCODZON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ZOCODZON',this.w_ANCODZON)
            select ZOCODZON,ZODESZON,ZODTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODZON = NVL(_Link_.ZOCODZON,space(3))
      this.w_DESZON = NVL(_Link_.ZODESZON,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ZODTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODZON = space(3)
      endif
      this.w_DESZON = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice zona inesistente oppure obsoleto")
        endif
        this.w_ANCODZON = space(3)
        this.w_DESZON = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])+'\'+cp_ToStr(_Link_.ZOCODZON,1)
      cp_ShowWarn(i_cKey,this.ZONE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODZON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_26(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ZONE_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ZONE_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_26.ZOCODZON as ZOCODZON326"+ ",link_3_26.ZODESZON as ZODESZON326"+ ",link_3_26.ZODTOBSO as ZODTOBSO326"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_26 on CONTI.ANCODZON=link_3_26.ZOCODZON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_26"
          i_cKey=i_cKey+'+" and CONTI.ANCODZON=link_3_26.ZOCODZON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODAG1
  func Link_3_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODAG1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_ANCODAG1)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_ANCODAG1))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODAG1)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStrODBC(trim(this.w_ANCODAG1)+"%");

            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" AGDESAGE like "+cp_ToStr(trim(this.w_ANCODAG1)+"%");

            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCODAG1) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oANCODAG1_3_27'),i_cWhere,'GSAR_AGE',"Agenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODAG1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_ANCODAG1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_ANCODAG1)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODAG1 = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE1 = NVL(_Link_.AGDESAGE,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODAG1 = space(5)
      endif
      this.w_DESAGE1 = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_ANCODAG1 = space(5)
        this.w_DESAGE1 = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODAG1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_27(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.AGENTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_27.AGCODAGE as AGCODAGE327"+ ",link_3_27.AGDESAGE as AGDESAGE327"+ ",link_3_27.AGDTOBSO as AGDTOBSO327"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_27 on CONTI.ANCODAG1=link_3_27.AGCODAGE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_27"
          i_cKey=i_cKey+'+" and CONTI.ANCODAG1=link_3_27.AGCODAGE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANTIPOCL
  func Link_3_28(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_CLIE_IDX,3]
    i_lTable = "TIP_CLIE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_CLIE_IDX,2], .t., this.TIP_CLIE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_CLIE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANTIPOCL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSPR_ATC',True,'TIP_CLIE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TCCODICE like "+cp_ToStrODBC(trim(this.w_ANTIPOCL)+"%");

          i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TCCODICE',trim(this.w_ANTIPOCL))
          select TCCODICE,TCDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANTIPOCL)==trim(_Link_.TCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANTIPOCL) and !this.bDontReportError
            deferred_cp_zoom('TIP_CLIE','*','TCCODICE',cp_AbsName(oSource.parent,'oANTIPOCL_3_28'),i_cWhere,'GSPR_ATC',"Studi di settore -tipologie clientela",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',oSource.xKey(1))
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANTIPOCL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TCCODICE,TCDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where TCCODICE="+cp_ToStrODBC(this.w_ANTIPOCL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TCCODICE',this.w_ANTIPOCL)
            select TCCODICE,TCDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANTIPOCL = NVL(_Link_.TCCODICE,space(5))
      this.w_DESTIP = NVL(_Link_.TCDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_ANTIPOCL = space(5)
      endif
      this.w_DESTIP = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_CLIE_IDX,2])+'\'+cp_ToStr(_Link_.TCCODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_CLIE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANTIPOCL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_28(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TIP_CLIE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TIP_CLIE_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_28.TCCODICE as TCCODICE328"+ ",link_3_28.TCDESCRI as TCDESCRI328"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_28 on CONTI.ANTIPOCL=link_3_28.TCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_28"
          i_cKey=i_cKey+'+" and CONTI.ANTIPOCL=link_3_28.TCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANMAGTER
  func Link_3_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANMAGTER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_ANMAGTER)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_ANMAGTER))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANMAGTER)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANMAGTER) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oANMAGTER_3_29'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANMAGTER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_ANMAGTER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_ANMAGTER)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANMAGTER = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ANMAGTER = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANMAGTER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_29(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_29.MGCODMAG as MGCODMAG329"+ ",link_3_29.MGDESMAG as MGDESMAG329"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_29 on CONTI.ANMAGTER=link_3_29.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_29"
          i_cKey=i_cKey+'+" and CONTI.ANMAGTER=link_3_29.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODORN
  func Link_3_30(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODORN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_ANCODORN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_ANTIPCON;
                     ,'ANCODICE',trim(this.w_ANCODORN))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODORN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODORN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oANCODORN_3_30'),i_cWhere,'GSAR_BZC',"Clienti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ANTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODORN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_ANCODORN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_ANTIPCON;
                       ,'ANCODICE',this.w_ANCODORN)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODORN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESORN = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODORN = space(15)
      endif
      this.w_DESORN = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODORN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_30(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_30.ANCODICE as ANCODICE330"+ ",link_3_30.ANDESCRI as ANDESCRI330"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_30 on CONTI.ANCODORN=link_3_30.ANCODICE"+" and CONTI.ANTIPCON=link_3_30.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_30"
          i_cKey=i_cKey+'+" and CONTI.ANCODORN=link_3_30.ANCODICE(+)"'+'+" and CONTI.ANTIPCON=link_3_30.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODPOR
  func Link_3_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COD_AREO_IDX,3]
    i_lTable = "COD_AREO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COD_AREO_IDX,2], .t., this.COD_AREO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COD_AREO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODPOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GLES_APP',True,'COD_AREO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PPCODICE like "+cp_ToStrODBC(trim(this.w_ANCODPOR)+"%");

          i_ret=cp_SQL(i_nConn,"select PPCODICE,PPDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PPCODICE',trim(this.w_ANCODPOR))
          select PPCODICE,PPDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODPOR)==trim(_Link_.PPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODPOR) and !this.bDontReportError
            deferred_cp_zoom('COD_AREO','*','PPCODICE',cp_AbsName(oSource.parent,'oANCODPOR_3_31'),i_cWhere,'GLES_APP',"Porti/aeroporti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',oSource.xKey(1))
            select PPCODICE,PPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODPOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PPCODICE,PPDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where PPCODICE="+cp_ToStrODBC(this.w_ANCODPOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PPCODICE',this.w_ANCODPOR)
            select PPCODICE,PPDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODPOR = NVL(_Link_.PPCODICE,space(10))
      this.w_DESCRI = NVL(_Link_.PPDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODPOR = space(10)
      endif
      this.w_DESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COD_AREO_IDX,2])+'\'+cp_ToStr(_Link_.PPCODICE,1)
      cp_ShowWarn(i_cKey,this.COD_AREO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODPOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_31(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COD_AREO_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COD_AREO_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_31.PPCODICE as PPCODICE331"+ ",link_3_31.PPDESCRI as PPDESCRI331"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_31 on CONTI.ANCODPOR=link_3_31.PPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_31"
          i_cKey=i_cKey+'+" and CONTI.ANCODPOR=link_3_31.PPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANMTDCLC
  func Link_3_32(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODCLDAT_IDX,3]
    i_lTable = "MODCLDAT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2], .t., this.MODCLDAT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANMTDCLC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'MODCLDAT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MDCODICE like "+cp_ToStrODBC(trim(this.w_ANMTDCLC)+"%");

          i_ret=cp_SQL(i_nConn,"select MDCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MDCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MDCODICE',trim(this.w_ANMTDCLC))
          select MDCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MDCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANMTDCLC)==trim(_Link_.MDCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANMTDCLC) and !this.bDontReportError
            deferred_cp_zoom('MODCLDAT','*','MDCODICE',cp_AbsName(oSource.parent,'oANMTDCLC_3_32'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',oSource.xKey(1))
            select MDCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANMTDCLC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MDCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where MDCODICE="+cp_ToStrODBC(this.w_ANMTDCLC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MDCODICE',this.w_ANMTDCLC)
            select MDCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANMTDCLC = NVL(_Link_.MDCODICE,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ANMTDCLC = space(3)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODCLDAT_IDX,2])+'\'+cp_ToStr(_Link_.MDCODICE,1)
      cp_ShowWarn(i_cKey,this.MODCLDAT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANMTDCLC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANMCALSI
  func Link_3_33(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANMCALSI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_ANMCALSI)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_ANMCALSI))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANMCALSI)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANMCALSI) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oANMCALSI_3_33'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANMCALSI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_ANMCALSI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_ANMCALSI)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANMCALSI = NVL(_Link_.MSCODICE,space(5))
      this.w_MSDESIMB = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ANMCALSI = space(5)
      endif
      this.w_MSDESIMB = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANMCALSI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_33(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.METCALSP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_33.MSCODICE as MSCODICE333"+ ",link_3_33.MSDESCRI as MSDESCRI333"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_33 on CONTI.ANMCALSI=link_3_33.MSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_33"
          i_cKey=i_cKey+'+" and CONTI.ANMCALSI=link_3_33.MSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANMCALST
  func Link_3_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANMCALST) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_ANMCALST)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_ANMCALST))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANMCALST)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANMCALST) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oANMCALST_3_34'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANMCALST)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_ANMCALST);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_ANMCALST)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANMCALST = NVL(_Link_.MSCODICE,space(5))
      this.w_MSDESTRA = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ANMCALST = space(5)
      endif
      this.w_MSDESTRA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANMCALST Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_34(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.METCALSP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_34.MSCODICE as MSCODICE334"+ ",link_3_34.MSDESCRI as MSDESCRI334"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_34 on CONTI.ANMCALST=link_3_34.MSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_34"
          i_cKey=i_cKey+'+" and CONTI.ANMCALST=link_3_34.MSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANMCALSI
  func Link_3_97(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANMCALSI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_ANMCALSI)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_ANMCALSI))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANMCALSI)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANMCALSI) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oANMCALSI_3_97'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANMCALSI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_ANMCALSI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_ANMCALSI)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANMCALSI = NVL(_Link_.MSCODICE,space(5))
      this.w_MSDESIMB = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ANMCALSI = space(5)
      endif
      this.w_MSDESIMB = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANMCALSI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_97(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.METCALSP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_97.MSCODICE as MSCODICE397"+ ",link_3_97.MSDESCRI as MSDESCRI397"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_97 on CONTI.ANMCALSI=link_3_97.MSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_97"
          i_cKey=i_cKey+'+" and CONTI.ANMCALSI=link_3_97.MSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANMCALST
  func Link_3_98(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.METCALSP_IDX,3]
    i_lTable = "METCALSP"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2], .t., this.METCALSP_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANMCALST) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMS',True,'METCALSP')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MSCODICE like "+cp_ToStrODBC(trim(this.w_ANMCALST)+"%");

          i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MSCODICE',trim(this.w_ANMCALST))
          select MSCODICE,MSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANMCALST)==trim(_Link_.MSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANMCALST) and !this.bDontReportError
            deferred_cp_zoom('METCALSP','*','MSCODICE',cp_AbsName(oSource.parent,'oANMCALST_3_98'),i_cWhere,'GSAR_AMS',"Metodi di calcolo",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',oSource.xKey(1))
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANMCALST)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MSCODICE,MSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MSCODICE="+cp_ToStrODBC(this.w_ANMCALST);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MSCODICE',this.w_ANMCALST)
            select MSCODICE,MSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANMCALST = NVL(_Link_.MSCODICE,space(5))
      this.w_MSDESTRA = NVL(_Link_.MSDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_ANMCALST = space(5)
      endif
      this.w_MSDESTRA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])+'\'+cp_ToStr(_Link_.MSCODICE,1)
      cp_ShowWarn(i_cKey,this.METCALSP_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANMCALST Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_3_98(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.METCALSP_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.METCALSP_IDX,2])
      i_cNewSel = i_cSel+ ",link_3_98.MSCODICE as MSCODICE398"+ ",link_3_98.MSDESCRI as MSDESCRI398"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_3_98 on CONTI.ANMCALST=link_3_98.MSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_3_98"
          i_cKey=i_cKey+'+" and CONTI.ANMCALST=link_3_98.MSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODPAG
  func Link_4_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_ANCODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_ANCODPAG))
          select PACODICE,PADESCRI,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODPAG)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_ANCODPAG)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_ANCODPAG))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_ANCODPAG)+"%");

            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCODPAG) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oANCODPAG_4_4'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_ANCODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_ANCODPAG)
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODPAG = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAG = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODPAG = space(5)
      endif
      this.w_DESPAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_ANCODPAG = space(5)
        this.w_DESPAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_4.PACODICE as PACODICE404"+ ","+cp_TransLinkFldName('link_4_4.PADESCRI')+" as PADESCRI404"+ ",link_4_4.PADTOBSO as PADTOBSO404"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_4 on CONTI.ANCODPAG=link_4_4.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_4"
          i_cKey=i_cKey+'+" and CONTI.ANCODPAG=link_4_4.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODBAN
  func Link_4_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABA',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_ANCODBAN)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_ANCODBAN))
          select BACODBAN,BADESBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODBAN)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODBAN) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oANCODBAN_4_17'),i_cWhere,'GSAR_ABA',"Elenco banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_ANCODBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_ANCODBAN)
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODBAN = NVL(_Link_.BACODBAN,space(10))
      this.w_DESBAN = NVL(_Link_.BADESBAN,space(42))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODBAN = space(10)
      endif
      this.w_DESBAN = space(42)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.BAN_CHE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_17.BACODBAN as BACODBAN417"+ ",link_4_17.BADESBAN as BADESBAN417"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_17 on CONTI.ANCODBAN=link_4_17.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_17"
          i_cKey=i_cKey+'+" and CONTI.ANCODBAN=link_4_17.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANCODBA2
  func Link_4_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODBA2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_ANCODBA2)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO,BACONSBF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_ANCODBA2))
          select BACODBAN,BADESCRI,BADTOBSO,BACONSBF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODBA2)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_ANCODBA2)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_ANCODBA2)+"%");

            select BACODBAN,BADESCRI,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANCODBA2) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oANCODBA2_4_23'),i_cWhere,'GSTE_ACB',"Elenco conti banche",'GSAR_ACL.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO,BACONSBF";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODBA2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_ANCODBA2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_ANCODBA2)
            select BACODBAN,BADESCRI,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODBA2 = NVL(_Link_.BACODBAN,space(15))
      this.w_DESBA2 = NVL(_Link_.BADESCRI,space(42))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
      this.w_CONSBF = NVL(_Link_.BACONSBF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODBA2 = space(15)
      endif
      this.w_DESBA2 = space(42)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_CONSBF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and .w_CONSBF='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente o obsoleto o di tipo salvo buon fine")
        endif
        this.w_ANCODBA2 = space(15)
        this.w_DESBA2 = space(42)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_CONSBF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODBA2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_23.BACODBAN as BACODBAN423"+ ",link_4_23.BADESCRI as BADESCRI423"+ ",link_4_23.BADTOBSO as BADTOBSO423"+ ",link_4_23.BACONSBF as BACONSBF423"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_23 on CONTI.ANCODBA2=link_4_23.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_23"
          i_cKey=i_cKey+'+" and CONTI.ANCODBA2=link_4_23.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANPAGFOR
  func Link_4_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANPAGFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_ANPAGFOR)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_ANPAGFOR))
          select PACODICE,PADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANPAGFOR)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_ANPAGFOR)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_ANPAGFOR))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_ANPAGFOR)+"%");

            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANPAGFOR) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oANPAGFOR_4_36'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANPAGFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_ANPAGFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_ANPAGFOR)
            select PACODICE,PADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANPAGFOR = NVL(_Link_.PACODICE,space(5))
      this.w_PADESCRI = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_ANPAGFOR = space(5)
      endif
      this.w_PADESCRI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANPAGFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_36(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_36.PACODICE as PACODICE436"+ ","+cp_TransLinkFldName('link_4_36.PADESCRI')+" as PADESCRI436"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_36 on CONTI.ANPAGFOR=link_4_36.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_36"
          i_cKey=i_cKey+'+" and CONTI.ANPAGFOR=link_4_36.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANPAGPAR
  func Link_5_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_lTable = "PAG_AMEN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2], .t., this.PAG_AMEN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANPAGPAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_APA',True,'PAG_AMEN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PACODICE like "+cp_ToStrODBC(trim(this.w_ANPAGPAR)+"%");

          i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PACODICE',trim(this.w_ANPAGPAR))
          select PACODICE,PADESCRI,PADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANPAGPAR)==trim(_Link_.PACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( PADESCRI like "+cp_ToStrODBC(trim(this.w_ANPAGPAR)+"%")+cp_TransWhereFldName('PADESCRI',trim(this.w_ANPAGPAR))+")";

            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" PADESCRI like "+cp_ToStr(trim(this.w_ANPAGPAR)+"%");

            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANPAGPAR) and !this.bDontReportError
            deferred_cp_zoom('PAG_AMEN','*','PACODICE',cp_AbsName(oSource.parent,'oANPAGPAR_5_3'),i_cWhere,'GSAR_APA',"Pagamenti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',oSource.xKey(1))
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANPAGPAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODICE,PADESCRI"+cp_TransInsFldName("PADESCRI")+",PADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where PACODICE="+cp_ToStrODBC(this.w_ANPAGPAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODICE',this.w_ANPAGPAR)
            select PACODICE,PADESCRI,PADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANPAGPAR = NVL(_Link_.PACODICE,space(5))
      this.w_DESPAR = NVL(cp_TransLoadField('_Link_.PADESCRI'),space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.PADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ANPAGPAR = space(5)
      endif
      this.w_DESPAR = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endif
        this.w_ANPAGPAR = space(5)
        this.w_DESPAR = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])+'\'+cp_ToStr(_Link_.PACODICE,1)
      cp_ShowWarn(i_cKey,this.PAG_AMEN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANPAGPAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_5_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.PAG_AMEN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_cNewSel = i_cSel+ ",link_5_3.PACODICE as PACODICE503"+ ","+cp_TransLinkFldName('link_5_3.PADESCRI')+" as PADESCRI503"+ ",link_5_3.PADTOBSO as PADTOBSO503"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_5_3 on CONTI.ANPAGPAR=link_5_3.PACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_5_3"
          i_cKey=i_cKey+'+" and CONTI.ANPAGPAR=link_5_3.PACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=READFID
  func Link_6_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SIT_FIDI_IDX,3]
    i_lTable = "SIT_FIDI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SIT_FIDI_IDX,2], .t., this.SIT_FIDI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SIT_FIDI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_READFID) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_READFID)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FICODCLI,FIDATELA,FIIMPPAP,FIIMPESC,FIIMPESO,FIIMPORD,FIIMPDDT,FIIMPFAT";
                   +" from "+i_cTable+" "+i_lTable+" where FICODCLI="+cp_ToStrODBC(this.w_READFID);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FICODCLI',this.w_READFID)
            select FICODCLI,FIDATELA,FIIMPPAP,FIIMPESC,FIIMPESO,FIIMPORD,FIIMPDDT,FIIMPFAT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_READFID = NVL(_Link_.FICODCLI,space(15))
      this.w_FIDDAT = NVL(cp_ToDate(_Link_.FIDATELA),ctod("  /  /  "))
      this.w_FIDPAP = NVL(_Link_.FIIMPPAP,0)
      this.w_FIDESC = NVL(_Link_.FIIMPESC,0)
      this.w_FIDESO = NVL(_Link_.FIIMPESO,0)
      this.w_FIDORD = NVL(_Link_.FIIMPORD,0)
      this.w_FIDDDT = NVL(_Link_.FIIMPDDT,0)
      this.w_FIDFAT = NVL(_Link_.FIIMPFAT,0)
    else
      if i_cCtrl<>'Load'
        this.w_READFID = space(15)
      endif
      this.w_FIDDAT = ctod("  /  /  ")
      this.w_FIDPAP = 0
      this.w_FIDESC = 0
      this.w_FIDESO = 0
      this.w_FIDORD = 0
      this.w_FIDDDT = 0
      this.w_FIDFAT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SIT_FIDI_IDX,2])+'\'+cp_ToStr(_Link_.FICODCLI,1)
      cp_ShowWarn(i_cKey,this.SIT_FIDI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_READFID Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ANCODGRU
  func Link_9_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRU_INTE_IDX,3]
    i_lTable = "GRU_INTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRU_INTE_IDX,2], .t., this.GRU_INTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRU_INTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANCODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGR',True,'GRU_INTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GRCODICE like "+cp_ToStrODBC(trim(this.w_ANCODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GRCODICE',trim(this.w_ANCODGRU))
          select GRCODICE,GRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANCODGRU)==trim(_Link_.GRCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANCODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRU_INTE','*','GRCODICE',cp_AbsName(oSource.parent,'oANCODGRU_9_4'),i_cWhere,'GSAR_AGR',"Gruppi intestatari EDI",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where GRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',oSource.xKey(1))
            select GRCODICE,GRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANCODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GRCODICE,GRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where GRCODICE="+cp_ToStrODBC(this.w_ANCODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GRCODICE',this.w_ANCODGRU)
            select GRCODICE,GRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANCODGRU = NVL(_Link_.GRCODICE,space(10))
      this.w_DESGRU = NVL(_Link_.GRDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_ANCODGRU = space(10)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRU_INTE_IDX,2])+'\'+cp_ToStr(_Link_.GRCODICE,1)
      cp_ShowWarn(i_cKey,this.GRU_INTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANCODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_9_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.GRU_INTE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.GRU_INTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_9_4.GRCODICE as GRCODICE904"+ ",link_9_4.GRDESCRI as GRDESCRI904"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_9_4 on CONTI.ANCODGRU=link_9_4.GRCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_9_4"
          i_cKey=i_cKey+'+" and CONTI.ANCODGRU=link_9_4.GRCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANRATING
  func Link_11_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DORATING_IDX,3]
    i_lTable = "DORATING"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2], .t., this.DORATING_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANRATING) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('Gsdf_Ara',True,'DORATING')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RACODICE like "+cp_ToStrODBC(trim(this.w_ANRATING)+"%");

          i_ret=cp_SQL(i_nConn,"select RACODICE,RADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RACODICE',trim(this.w_ANRATING))
          select RACODICE,RADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANRATING)==trim(_Link_.RACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ANRATING) and !this.bDontReportError
            deferred_cp_zoom('DORATING','*','RACODICE',cp_AbsName(oSource.parent,'oANRATING_11_2'),i_cWhere,'Gsdf_Ara',"Rating",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RACODICE,RADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where RACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RACODICE',oSource.xKey(1))
            select RACODICE,RADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANRATING)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RACODICE,RADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where RACODICE="+cp_ToStrODBC(this.w_ANRATING);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RACODICE',this.w_ANRATING)
            select RACODICE,RADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANRATING = NVL(_Link_.RACODICE,space(2))
      this.w_RADESCRI = NVL(_Link_.RADESCRI,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_ANRATING = space(2)
      endif
      this.w_RADESCRI = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])+'\'+cp_ToStr(_Link_.RACODICE,1)
      cp_ShowWarn(i_cKey,this.DORATING_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANRATING Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_11_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.DORATING_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.DORATING_IDX,2])
      i_cNewSel = i_cSel+ ",link_11_2.RACODICE as RACODICE1102"+ ",link_11_2.RADESCRI as RADESCRI1102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_11_2 on CONTI.ANRATING=link_11_2.RACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_11_2"
          i_cKey=i_cKey+'+" and CONTI.ANRATING=link_11_2.RACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=ANVOCFIN
  func Link_11_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VOC_FINZ_IDX,3]
    i_lTable = "VOC_FINZ"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2], .t., this.VOC_FINZ_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ANVOCFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSDF_AVF',True,'VOC_FINZ')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DFVOCFIN like "+cp_ToStrODBC(trim(this.w_ANVOCFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select DFVOCFIN,DFDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DFVOCFIN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DFVOCFIN',trim(this.w_ANVOCFIN))
          select DFVOCFIN,DFDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DFVOCFIN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ANVOCFIN)==trim(_Link_.DFVOCFIN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" DFDESCRI like "+cp_ToStrODBC(trim(this.w_ANVOCFIN)+"%");

            i_ret=cp_SQL(i_nConn,"select DFVOCFIN,DFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" DFDESCRI like "+cp_ToStr(trim(this.w_ANVOCFIN)+"%");

            select DFVOCFIN,DFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ANVOCFIN) and !this.bDontReportError
            deferred_cp_zoom('VOC_FINZ','*','DFVOCFIN',cp_AbsName(oSource.parent,'oANVOCFIN_11_7'),i_cWhere,'GSDF_AVF',"Voci finanziarie",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFVOCFIN,DFDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where DFVOCFIN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFVOCFIN',oSource.xKey(1))
            select DFVOCFIN,DFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ANVOCFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DFVOCFIN,DFDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where DFVOCFIN="+cp_ToStrODBC(this.w_ANVOCFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DFVOCFIN',this.w_ANVOCFIN)
            select DFVOCFIN,DFDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ANVOCFIN = NVL(_Link_.DFVOCFIN,space(6))
      this.w_DFDESCRI = NVL(_Link_.DFDESCRI,space(60))
    else
      if i_cCtrl<>'Load'
        this.w_ANVOCFIN = space(6)
      endif
      this.w_DFDESCRI = space(60)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])+'\'+cp_ToStr(_Link_.DFVOCFIN,1)
      cp_ShowWarn(i_cKey,this.VOC_FINZ_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ANVOCFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_11_7(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VOC_FINZ_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VOC_FINZ_IDX,2])
      i_cNewSel = i_cSel+ ",link_11_7.DFVOCFIN as DFVOCFIN1107"+ ",link_11_7.DFDESCRI as DFDESCRI1107"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_11_7 on CONTI.ANVOCFIN=link_11_7.DFVOCFIN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_11_7"
          i_cKey=i_cKey+'+" and CONTI.ANVOCFIN=link_11_7.DFVOCFIN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oANCODICE_1_5.value==this.w_ANCODICE)
      this.oPgFrm.Page1.oPag.oANCODICE_1_5.value=this.w_ANCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCRI_1_6.value==this.w_ANDESCRI)
      this.oPgFrm.Page1.oPag.oANDESCRI_1_6.value=this.w_ANDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oANDESCR2_1_7.value==this.w_ANDESCR2)
      this.oPgFrm.Page1.oPag.oANDESCR2_1_7.value=this.w_ANDESCR2
    endif
    if not(this.oPgFrm.Page1.oPag.oANINDIRI_1_11.value==this.w_ANINDIRI)
      this.oPgFrm.Page1.oPag.oANINDIRI_1_11.value=this.w_ANINDIRI
    endif
    if not(this.oPgFrm.Page1.oPag.oANINDIR2_1_12.value==this.w_ANINDIR2)
      this.oPgFrm.Page1.oPag.oANINDIR2_1_12.value=this.w_ANINDIR2
    endif
    if not(this.oPgFrm.Page1.oPag.oAN___CAP_1_13.value==this.w_AN___CAP)
      this.oPgFrm.Page1.oPag.oAN___CAP_1_13.value=this.w_AN___CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oANLOCALI_1_14.value==this.w_ANLOCALI)
      this.oPgFrm.Page1.oPag.oANLOCALI_1_14.value=this.w_ANLOCALI
    endif
    if not(this.oPgFrm.Page1.oPag.oANPROVIN_1_15.value==this.w_ANPROVIN)
      this.oPgFrm.Page1.oPag.oANPROVIN_1_15.value=this.w_ANPROVIN
    endif
    if not(this.oPgFrm.Page1.oPag.oANNAZION_1_16.value==this.w_ANNAZION)
      this.oPgFrm.Page1.oPag.oANNAZION_1_16.value=this.w_ANNAZION
    endif
    if not(this.oPgFrm.Page1.oPag.oDESNAZ_1_17.value==this.w_DESNAZ)
      this.oPgFrm.Page1.oPag.oDESNAZ_1_17.value=this.w_DESNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oCODISO_1_18.value==this.w_CODISO)
      this.oPgFrm.Page1.oPag.oCODISO_1_18.value=this.w_CODISO
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODFIS_1_19.value==this.w_ANCODFIS)
      this.oPgFrm.Page1.oPag.oANCODFIS_1_19.value=this.w_ANCODFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oANPARIVA_1_20.value==this.w_ANPARIVA)
      this.oPgFrm.Page1.oPag.oANPARIVA_1_20.value=this.w_ANPARIVA
    endif
    if not(this.oPgFrm.Page1.oPag.oANTELEFO_1_21.value==this.w_ANTELEFO)
      this.oPgFrm.Page1.oPag.oANTELEFO_1_21.value=this.w_ANTELEFO
    endif
    if not(this.oPgFrm.Page1.oPag.oANTELFAX_1_22.value==this.w_ANTELFAX)
      this.oPgFrm.Page1.oPag.oANTELFAX_1_22.value=this.w_ANTELFAX
    endif
    if not(this.oPgFrm.Page1.oPag.oANNUMCEL_1_23.value==this.w_ANNUMCEL)
      this.oPgFrm.Page1.oPag.oANNUMCEL_1_23.value=this.w_ANNUMCEL
    endif
    if not(this.oPgFrm.Page1.oPag.oANINDWEB_1_24.value==this.w_ANINDWEB)
      this.oPgFrm.Page1.oPag.oANINDWEB_1_24.value=this.w_ANINDWEB
    endif
    if not(this.oPgFrm.Page1.oPag.oAN_EMAIL_1_25.value==this.w_AN_EMAIL)
      this.oPgFrm.Page1.oPag.oAN_EMAIL_1_25.value=this.w_AN_EMAIL
    endif
    if not(this.oPgFrm.Page1.oPag.oAN_SKYPE_1_26.value==this.w_AN_SKYPE)
      this.oPgFrm.Page1.oPag.oAN_SKYPE_1_26.value=this.w_AN_SKYPE
    endif
    if not(this.oPgFrm.Page1.oPag.oAN_EMPEC_1_27.value==this.w_AN_EMPEC)
      this.oPgFrm.Page1.oPag.oAN_EMPEC_1_27.value=this.w_AN_EMPEC
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODSAL_1_28.RadioValue()==this.w_ANCODSAL)
      this.oPgFrm.Page1.oPag.oANCODSAL_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANPERFIS_1_29.RadioValue()==this.w_ANPERFIS)
      this.oPgFrm.Page1.oPag.oANPERFIS_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCOGNOM_1_30.value==this.w_ANCOGNOM)
      this.oPgFrm.Page1.oPag.oANCOGNOM_1_30.value=this.w_ANCOGNOM
    endif
    if not(this.oPgFrm.Page1.oPag.oAN__NOME_1_31.value==this.w_AN__NOME)
      this.oPgFrm.Page1.oPag.oAN__NOME_1_31.value=this.w_AN__NOME
    endif
    if not(this.oPgFrm.Page1.oPag.oAN_SESSO_1_32.RadioValue()==this.w_AN_SESSO)
      this.oPgFrm.Page1.oPag.oAN_SESSO_1_32.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANLOCNAS_1_33.value==this.w_ANLOCNAS)
      this.oPgFrm.Page1.oPag.oANLOCNAS_1_33.value=this.w_ANLOCNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oANPRONAS_1_34.value==this.w_ANPRONAS)
      this.oPgFrm.Page1.oPag.oANPRONAS_1_34.value=this.w_ANPRONAS
    endif
    if not(this.oPgFrm.Page1.oPag.oANDATNAS_1_35.value==this.w_ANDATNAS)
      this.oPgFrm.Page1.oPag.oANDATNAS_1_35.value=this.w_ANDATNAS
    endif
    if not(this.oPgFrm.Page1.oPag.oANNUMCAR_1_37.value==this.w_ANNUMCAR)
      this.oPgFrm.Page1.oPag.oANNUMCAR_1_37.value=this.w_ANNUMCAR
    endif
    if not(this.oPgFrm.Page1.oPag.oANCHKSTA_1_57.RadioValue()==this.w_ANCHKSTA)
      this.oPgFrm.Page1.oPag.oANCHKSTA_1_57.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCHKMAI_1_58.RadioValue()==this.w_ANCHKMAI)
      this.oPgFrm.Page1.oPag.oANCHKMAI_1_58.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCHKPEC_1_59.RadioValue()==this.w_ANCHKPEC)
      this.oPgFrm.Page1.oPag.oANCHKPEC_1_59.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCHKFAX_1_60.RadioValue()==this.w_ANCHKFAX)
      this.oPgFrm.Page1.oPag.oANCHKFAX_1_60.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCHKCPZ_1_61.RadioValue()==this.w_ANCHKCPZ)
      this.oPgFrm.Page1.oPag.oANCHKCPZ_1_61.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCHKWWP_1_62.RadioValue()==this.w_ANCHKWWP)
      this.oPgFrm.Page1.oPag.oANCHKWWP_1_62.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCHKPTL_1_63.RadioValue()==this.w_ANCHKPTL)
      this.oPgFrm.Page1.oPag.oANCHKPTL_1_63.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCHKFIR_1_64.RadioValue()==this.w_ANCHKFIR)
      this.oPgFrm.Page1.oPag.oANCHKFIR_1_64.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODEST_1_65.value==this.w_ANCODEST)
      this.oPgFrm.Page1.oPag.oANCODEST_1_65.value=this.w_ANCODEST
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODCLA_1_66.value==this.w_ANCODCLA)
      this.oPgFrm.Page1.oPag.oANCODCLA_1_66.value=this.w_ANCODCLA
    endif
    if not(this.oPgFrm.Page1.oPag.oANCODPEC_1_67.value==this.w_ANCODPEC)
      this.oPgFrm.Page1.oPag.oANCODPEC_1_67.value=this.w_ANCODPEC
    endif
    if not(this.oPgFrm.Page1.oPag.oANDTOBSO_1_68.value==this.w_ANDTOBSO)
      this.oPgFrm.Page1.oPag.oANDTOBSO_1_68.value=this.w_ANDTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oANDTINVA_1_72.value==this.w_ANDTINVA)
      this.oPgFrm.Page1.oPag.oANDTINVA_1_72.value=this.w_ANDTINVA
    endif
    if not(this.oPgFrm.Page2.oPag.oCODI_2_3.value==this.w_CODI)
      this.oPgFrm.Page2.oPag.oCODI_2_3.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page2.oPag.oRAG1_2_4.value==this.w_RAG1)
      this.oPgFrm.Page2.oPag.oRAG1_2_4.value=this.w_RAG1
    endif
    if not(this.oPgFrm.Page2.oPag.oANCONSUP_2_7.value==this.w_ANCONSUP)
      this.oPgFrm.Page2.oPag.oANCONSUP_2_7.value=this.w_ANCONSUP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESSUP_2_8.value==this.w_DESSUP)
      this.oPgFrm.Page2.oPag.oDESSUP_2_8.value=this.w_DESSUP
    endif
    if not(this.oPgFrm.Page2.oPag.oANCATCON_2_9.value==this.w_ANCATCON)
      this.oPgFrm.Page2.oPag.oANCATCON_2_9.value=this.w_ANCATCON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCON_2_10.value==this.w_DESCON)
      this.oPgFrm.Page2.oPag.oDESCON_2_10.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page2.oPag.oANCONRIF_2_14.value==this.w_ANCONRIF)
      this.oPgFrm.Page2.oPag.oANCONRIF_2_14.value=this.w_ANCONRIF
    endif
    if not(this.oPgFrm.Page2.oPag.oDESORI_2_15.value==this.w_DESORI)
      this.oPgFrm.Page2.oPag.oDESORI_2_15.value=this.w_DESORI
    endif
    if not(this.oPgFrm.Page2.oPag.oANCODIVA_2_17.value==this.w_ANCODIVA)
      this.oPgFrm.Page2.oPag.oANCODIVA_2_17.value=this.w_ANCODIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESIVA_2_19.value==this.w_DESIVA)
      this.oPgFrm.Page2.oPag.oDESIVA_2_19.value=this.w_DESIVA
    endif
    if not(this.oPgFrm.Page2.oPag.oANTIPOPE_2_20.value==this.w_ANTIPOPE)
      this.oPgFrm.Page2.oPag.oANTIPOPE_2_20.value=this.w_ANTIPOPE
    endif
    if not(this.oPgFrm.Page2.oPag.oANCONCAU_2_21.value==this.w_ANCONCAU)
      this.oPgFrm.Page2.oPag.oANCONCAU_2_21.value=this.w_ANCONCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oTIDESCRI_2_22.value==this.w_TIDESCRI)
      this.oPgFrm.Page2.oPag.oTIDESCRI_2_22.value=this.w_TIDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oANFLRITE_2_23.RadioValue()==this.w_ANFLRITE)
      this.oPgFrm.Page2.oPag.oANFLRITE_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANCODIRP_2_24.value==this.w_ANCODIRP)
      this.oPgFrm.Page2.oPag.oANCODIRP_2_24.value=this.w_ANCODIRP
    endif
    if not(this.oPgFrm.Page2.oPag.oANCAURIT_2_27.RadioValue()==this.w_ANCAURIT)
      this.oPgFrm.Page2.oPag.oANCAURIT_2_27.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANOPETRE_2_36.RadioValue()==this.w_ANOPETRE)
      this.oPgFrm.Page2.oPag.oANOPETRE_2_36.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANTIPPRE_2_38.RadioValue()==this.w_ANTIPPRE)
      this.oPgFrm.Page2.oPag.oANTIPPRE_2_38.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANFLAACC_2_40.RadioValue()==this.w_ANFLAACC)
      this.oPgFrm.Page2.oPag.oANFLAACC_2_40.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANFLESIG_2_41.RadioValue()==this.w_ANFLESIG)
      this.oPgFrm.Page2.oPag.oANFLESIG_2_41.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAFFLINTR_2_42.RadioValue()==this.w_AFFLINTR)
      this.oPgFrm.Page2.oPag.oAFFLINTR_2_42.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANIVASOS_2_43.RadioValue()==this.w_ANIVASOS)
      this.oPgFrm.Page2.oPag.oANIVASOS_2_43.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANFLBLLS_2_44.RadioValue()==this.w_ANFLBLLS)
      this.oPgFrm.Page2.oPag.oANFLBLLS_2_44.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANCOFISC_2_45.value==this.w_ANCOFISC)
      this.oPgFrm.Page2.oPag.oANCOFISC_2_45.value=this.w_ANCOFISC
    endif
    if not(this.oPgFrm.Page2.oPag.oANSCIPAG_2_46.RadioValue()==this.w_ANSCIPAG)
      this.oPgFrm.Page2.oPag.oANSCIPAG_2_46.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANFLSOAL_2_47.RadioValue()==this.w_ANFLSOAL)
      this.oPgFrm.Page2.oPag.oANFLSOAL_2_47.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANPARTSN_2_48.RadioValue()==this.w_ANPARTSN)
      this.oPgFrm.Page2.oPag.oANPARTSN_2_48.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANFLFIDO_2_49.RadioValue()==this.w_ANFLFIDO)
      this.oPgFrm.Page2.oPag.oANFLFIDO_2_49.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANFLSGRE_2_50.RadioValue()==this.w_ANFLSGRE)
      this.oPgFrm.Page2.oPag.oANFLSGRE_2_50.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANFLBODO_2_51.RadioValue()==this.w_ANFLBODO)
      this.oPgFrm.Page2.oPag.oANFLBODO_2_51.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANCODSTU_2_52.value==this.w_ANCODSTU)
      this.oPgFrm.Page2.oPag.oANCODSTU_2_52.value=this.w_ANCODSTU
    endif
    if not(this.oPgFrm.Page2.oPag.oANCODSOG_2_54.value==this.w_ANCODSOG)
      this.oPgFrm.Page2.oPag.oANCODSOG_2_54.value=this.w_ANCODSOG
    endif
    if not(this.oPgFrm.Page2.oPag.oANCODCAT_2_55.value==this.w_ANCODCAT)
      this.oPgFrm.Page2.oPag.oANCODCAT_2_55.value=this.w_ANCODCAT
    endif
    if not(this.oPgFrm.Page2.oPag.oMINCON_2_67.value==this.w_MINCON)
      this.oPgFrm.Page2.oPag.oMINCON_2_67.value=this.w_MINCON
    endif
    if not(this.oPgFrm.Page2.oPag.oMAXCON_2_68.value==this.w_MAXCON)
      this.oPgFrm.Page2.oPag.oMAXCON_2_68.value=this.w_MAXCON
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCAU_2_71.value==this.w_DESCAU)
      this.oPgFrm.Page2.oPag.oDESCAU_2_71.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page2.oPag.oDESIRPEF_2_75.value==this.w_DESIRPEF)
      this.oPgFrm.Page2.oPag.oDESIRPEF_2_75.value=this.w_DESIRPEF
    endif
    if not(this.oPgFrm.Page3.oPag.oCODI_3_1.value==this.w_CODI)
      this.oPgFrm.Page3.oPag.oCODI_3_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page3.oPag.oRAG1_3_2.value==this.w_RAG1)
      this.oPgFrm.Page3.oPag.oRAG1_3_2.value=this.w_RAG1
    endif
    if not(this.oPgFrm.Page3.oPag.oANCATCOM_3_3.value==this.w_ANCATCOM)
      this.oPgFrm.Page3.oPag.oANCATCOM_3_3.value=this.w_ANCATCOM
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCAC_3_4.value==this.w_DESCAC)
      this.oPgFrm.Page3.oPag.oDESCAC_3_4.value=this.w_DESCAC
    endif
    if not(this.oPgFrm.Page3.oPag.oANFLCONA_3_5.RadioValue()==this.w_ANFLCONA)
      this.oPgFrm.Page3.oPag.oANFLCONA_3_5.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANCATSCM_3_6.value==this.w_ANCATSCM)
      this.oPgFrm.Page3.oPag.oANCATSCM_3_6.value=this.w_ANCATSCM
    endif
    if not(this.oPgFrm.Page3.oPag.oANFLGCAU_3_7.RadioValue()==this.w_ANFLGCAU)
      this.oPgFrm.Page3.oPag.oANFLGCAU_3_7.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oDESSCM_3_8.value==this.w_DESSCM)
      this.oPgFrm.Page3.oPag.oDESSCM_3_8.value=this.w_DESSCM
    endif
    if not(this.oPgFrm.Page3.oPag.oANTIPFAT_3_9.RadioValue()==this.w_ANTIPFAT)
      this.oPgFrm.Page3.oPag.oANTIPFAT_3_9.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANGRUPRO_3_10.value==this.w_ANGRUPRO)
      this.oPgFrm.Page3.oPag.oANGRUPRO_3_10.value=this.w_ANGRUPRO
    endif
    if not(this.oPgFrm.Page3.oPag.oDESGPP_3_12.value==this.w_DESGPP)
      this.oPgFrm.Page3.oPag.oDESGPP_3_12.value=this.w_DESGPP
    endif
    if not(this.oPgFrm.Page3.oPag.oANCONCON_3_14.RadioValue()==this.w_ANCONCON)
      this.oPgFrm.Page3.oPag.oANCONCON_3_14.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANCODLIN_3_17.value==this.w_ANCODLIN)
      this.oPgFrm.Page3.oPag.oANCODLIN_3_17.value=this.w_ANCODLIN
    endif
    if not(this.oPgFrm.Page3.oPag.oDESLIN_3_18.value==this.w_DESLIN)
      this.oPgFrm.Page3.oPag.oDESLIN_3_18.value=this.w_DESLIN
    endif
    if not(this.oPgFrm.Page3.oPag.oANCODVAL_3_19.value==this.w_ANCODVAL)
      this.oPgFrm.Page3.oPag.oANCODVAL_3_19.value=this.w_ANCODVAL
    endif
    if not(this.oPgFrm.Page3.oPag.oDESVAL_3_20.value==this.w_DESVAL)
      this.oPgFrm.Page3.oPag.oDESVAL_3_20.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page3.oPag.oANNUMLIS_3_21.value==this.w_ANNUMLIS)
      this.oPgFrm.Page3.oPag.oANNUMLIS_3_21.value=this.w_ANNUMLIS
    endif
    if not(this.oPgFrm.Page3.oPag.oANCODCUC_3_22.RadioValue()==this.w_ANCODCUC)
      this.oPgFrm.Page3.oPag.oANCODCUC_3_22.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oAN1SCONT_3_23.value==this.w_AN1SCONT)
      this.oPgFrm.Page3.oPag.oAN1SCONT_3_23.value=this.w_AN1SCONT
    endif
    if not(this.oPgFrm.Page3.oPag.oANGRPDEF_3_24.value==this.w_ANGRPDEF)
      this.oPgFrm.Page3.oPag.oANGRPDEF_3_24.value=this.w_ANGRPDEF
    endif
    if not(this.oPgFrm.Page3.oPag.oAN2SCONT_3_25.value==this.w_AN2SCONT)
      this.oPgFrm.Page3.oPag.oAN2SCONT_3_25.value=this.w_AN2SCONT
    endif
    if not(this.oPgFrm.Page3.oPag.oANCODZON_3_26.value==this.w_ANCODZON)
      this.oPgFrm.Page3.oPag.oANCODZON_3_26.value=this.w_ANCODZON
    endif
    if not(this.oPgFrm.Page3.oPag.oANCODAG1_3_27.value==this.w_ANCODAG1)
      this.oPgFrm.Page3.oPag.oANCODAG1_3_27.value=this.w_ANCODAG1
    endif
    if not(this.oPgFrm.Page3.oPag.oANTIPOCL_3_28.value==this.w_ANTIPOCL)
      this.oPgFrm.Page3.oPag.oANTIPOCL_3_28.value=this.w_ANTIPOCL
    endif
    if not(this.oPgFrm.Page3.oPag.oANMAGTER_3_29.value==this.w_ANMAGTER)
      this.oPgFrm.Page3.oPag.oANMAGTER_3_29.value=this.w_ANMAGTER
    endif
    if not(this.oPgFrm.Page3.oPag.oANCODORN_3_30.value==this.w_ANCODORN)
      this.oPgFrm.Page3.oPag.oANCODORN_3_30.value=this.w_ANCODORN
    endif
    if not(this.oPgFrm.Page3.oPag.oANCODPOR_3_31.value==this.w_ANCODPOR)
      this.oPgFrm.Page3.oPag.oANCODPOR_3_31.value=this.w_ANCODPOR
    endif
    if not(this.oPgFrm.Page3.oPag.oANMTDCLC_3_32.RadioValue()==this.w_ANMTDCLC)
      this.oPgFrm.Page3.oPag.oANMTDCLC_3_32.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANMCALSI_3_33.value==this.w_ANMCALSI)
      this.oPgFrm.Page3.oPag.oANMCALSI_3_33.value=this.w_ANMCALSI
    endif
    if not(this.oPgFrm.Page3.oPag.oANMCALST_3_34.value==this.w_ANMCALST)
      this.oPgFrm.Page3.oPag.oANMCALST_3_34.value=this.w_ANMCALST
    endif
    if not(this.oPgFrm.Page3.oPag.oDESLIS_3_35.value==this.w_DESLIS)
      this.oPgFrm.Page3.oPag.oDESLIS_3_35.value=this.w_DESLIS
    endif
    if not(this.oPgFrm.Page3.oPag.oDESZON_3_38.value==this.w_DESZON)
      this.oPgFrm.Page3.oPag.oDESZON_3_38.value=this.w_DESZON
    endif
    if not(this.oPgFrm.Page3.oPag.oDESAGE1_3_39.value==this.w_DESAGE1)
      this.oPgFrm.Page3.oPag.oDESAGE1_3_39.value=this.w_DESAGE1
    endif
    if not(this.oPgFrm.Page3.oPag.oDESMAG_3_40.value==this.w_DESMAG)
      this.oPgFrm.Page3.oPag.oDESMAG_3_40.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page3.oPag.oANBOLFAT_3_41.RadioValue()==this.w_ANBOLFAT)
      this.oPgFrm.Page3.oPag.oANBOLFAT_3_41.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANPREBOL_3_42.RadioValue()==this.w_ANPREBOL)
      this.oPgFrm.Page3.oPag.oANPREBOL_3_42.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANFLAPCA_3_43.RadioValue()==this.w_ANFLAPCA)
      this.oPgFrm.Page3.oPag.oANFLAPCA_3_43.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANSCORPO_3_45.RadioValue()==this.w_ANSCORPO)
      this.oPgFrm.Page3.oPag.oANSCORPO_3_45.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANFLCODI_3_46.RadioValue()==this.w_ANFLCODI)
      this.oPgFrm.Page3.oPag.oANFLCODI_3_46.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANCLIPOS_3_47.RadioValue()==this.w_ANCLIPOS)
      this.oPgFrm.Page3.oPag.oANCLIPOS_3_47.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANFLGCPZ_3_48.RadioValue()==this.w_ANFLGCPZ)
      this.oPgFrm.Page3.oPag.oANFLGCPZ_3_48.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANFLIMBA_3_50.RadioValue()==this.w_ANFLIMBA)
      this.oPgFrm.Page3.oPag.oANFLIMBA_3_50.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANFLPRIV_3_51.RadioValue()==this.w_ANFLPRIV)
      this.oPgFrm.Page3.oPag.oANFLPRIV_3_51.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oANCODESC_3_54.value==this.w_ANCODESC)
      this.oPgFrm.Page3.oPag.oANCODESC_3_54.value=this.w_ANCODESC
    endif
    if not(this.oPgFrm.Page3.oPag.oMSDESIMB_3_82.value==this.w_MSDESIMB)
      this.oPgFrm.Page3.oPag.oMSDESIMB_3_82.value=this.w_MSDESIMB
    endif
    if not(this.oPgFrm.Page3.oPag.oMSDESTRA_3_84.value==this.w_MSDESTRA)
      this.oPgFrm.Page3.oPag.oMSDESTRA_3_84.value=this.w_MSDESTRA
    endif
    if not(this.oPgFrm.Page3.oPag.oDESORN_3_88.value==this.w_DESORN)
      this.oPgFrm.Page3.oPag.oDESORN_3_88.value=this.w_DESORN
    endif
    if not(this.oPgFrm.Page3.oPag.oGDDESCRI_3_93.value==this.w_GDDESCRI)
      this.oPgFrm.Page3.oPag.oGDDESCRI_3_93.value=this.w_GDDESCRI
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCRI_3_94.value==this.w_DESCRI)
      this.oPgFrm.Page3.oPag.oDESCRI_3_94.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page3.oPag.oANMCALSI_3_97.value==this.w_ANMCALSI)
      this.oPgFrm.Page3.oPag.oANMCALSI_3_97.value=this.w_ANMCALSI
    endif
    if not(this.oPgFrm.Page3.oPag.oANMCALST_3_98.value==this.w_ANMCALST)
      this.oPgFrm.Page3.oPag.oANMCALST_3_98.value=this.w_ANMCALST
    endif
    if not(this.oPgFrm.Page3.oPag.oDESTIP_3_103.value==this.w_DESTIP)
      this.oPgFrm.Page3.oPag.oDESTIP_3_103.value=this.w_DESTIP
    endif
    if not(this.oPgFrm.Page4.oPag.oCODI_4_1.value==this.w_CODI)
      this.oPgFrm.Page4.oPag.oCODI_4_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page4.oPag.oRAG1_4_2.value==this.w_RAG1)
      this.oPgFrm.Page4.oPag.oRAG1_4_2.value=this.w_RAG1
    endif
    if not(this.oPgFrm.Page4.oPag.oANCODPAG_4_4.value==this.w_ANCODPAG)
      this.oPgFrm.Page4.oPag.oANCODPAG_4_4.value=this.w_ANCODPAG
    endif
    if not(this.oPgFrm.Page4.oPag.oDESPAG_4_5.value==this.w_DESPAG)
      this.oPgFrm.Page4.oPag.oDESPAG_4_5.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page4.oPag.oANGIOFIS_4_6.value==this.w_ANGIOFIS)
      this.oPgFrm.Page4.oPag.oANGIOFIS_4_6.value=this.w_ANGIOFIS
    endif
    if not(this.oPgFrm.Page4.oPag.oANFLINCA_4_7.RadioValue()==this.w_ANFLINCA)
      this.oPgFrm.Page4.oPag.oANFLINCA_4_7.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oANSPEINC_4_8.value==this.w_ANSPEINC)
      this.oPgFrm.Page4.oPag.oANSPEINC_4_8.value=this.w_ANSPEINC
    endif
    if not(this.oPgFrm.Page4.oPag.oAN1MESCL_4_9.value==this.w_AN1MESCL)
      this.oPgFrm.Page4.oPag.oAN1MESCL_4_9.value=this.w_AN1MESCL
    endif
    if not(this.oPgFrm.Page4.oPag.oDESMES1_4_10.value==this.w_DESMES1)
      this.oPgFrm.Page4.oPag.oDESMES1_4_10.value=this.w_DESMES1
    endif
    if not(this.oPgFrm.Page4.oPag.oANGIOSC1_4_11.value==this.w_ANGIOSC1)
      this.oPgFrm.Page4.oPag.oANGIOSC1_4_11.value=this.w_ANGIOSC1
    endif
    if not(this.oPgFrm.Page4.oPag.oAN2MESCL_4_12.value==this.w_AN2MESCL)
      this.oPgFrm.Page4.oPag.oAN2MESCL_4_12.value=this.w_AN2MESCL
    endif
    if not(this.oPgFrm.Page4.oPag.oDESMES2_4_13.value==this.w_DESMES2)
      this.oPgFrm.Page4.oPag.oDESMES2_4_13.value=this.w_DESMES2
    endif
    if not(this.oPgFrm.Page4.oPag.oANGIOSC2_4_14.value==this.w_ANGIOSC2)
      this.oPgFrm.Page4.oPag.oANGIOSC2_4_14.value=this.w_ANGIOSC2
    endif
    if not(this.oPgFrm.Page4.oPag.oANCODBAN_4_17.value==this.w_ANCODBAN)
      this.oPgFrm.Page4.oPag.oANCODBAN_4_17.value=this.w_ANCODBAN
    endif
    if not(this.oPgFrm.Page4.oPag.oDESBAN_4_18.value==this.w_DESBAN)
      this.oPgFrm.Page4.oPag.oDESBAN_4_18.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page4.oPag.oDESBA2_4_21.value==this.w_DESBA2)
      this.oPgFrm.Page4.oPag.oDESBA2_4_21.value=this.w_DESBA2
    endif
    if not(this.oPgFrm.Page4.oPag.oANCODBA2_4_23.value==this.w_ANCODBA2)
      this.oPgFrm.Page4.oPag.oANCODBA2_4_23.value=this.w_ANCODBA2
    endif
    if not(this.oPgFrm.Page4.oPag.oANFLRAGG_4_27.RadioValue()==this.w_ANFLRAGG)
      this.oPgFrm.Page4.oPag.oANFLRAGG_4_27.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oANFLGAVV_4_28.RadioValue()==this.w_ANFLGAVV)
      this.oPgFrm.Page4.oPag.oANFLGAVV_4_28.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oANDATAVV_4_32.value==this.w_ANDATAVV)
      this.oPgFrm.Page4.oPag.oANDATAVV_4_32.value=this.w_ANDATAVV
    endif
    if not(this.oPgFrm.Page4.oPag.oANIDRIDY_4_33.RadioValue()==this.w_ANIDRIDY)
      this.oPgFrm.Page4.oPag.oANIDRIDY_4_33.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oANTIIDRI_4_34.RadioValue()==this.w_ANTIIDRI)
      this.oPgFrm.Page4.oPag.oANTIIDRI_4_34.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oANIBARID_4_35.value==this.w_ANIBARID)
      this.oPgFrm.Page4.oPag.oANIBARID_4_35.value=this.w_ANIBARID
    endif
    if not(this.oPgFrm.Page4.oPag.oANPAGFOR_4_36.value==this.w_ANPAGFOR)
      this.oPgFrm.Page4.oPag.oANPAGFOR_4_36.value=this.w_ANPAGFOR
    endif
    if not(this.oPgFrm.Page5.oPag.oANGESCON_5_1.RadioValue()==this.w_ANGESCON)
      this.oPgFrm.Page5.oPag.oANGESCON_5_1.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oANFLGCON_5_2.RadioValue()==this.w_ANFLGCON)
      this.oPgFrm.Page5.oPag.oANFLGCON_5_2.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oANPAGPAR_5_3.value==this.w_ANPAGPAR)
      this.oPgFrm.Page5.oPag.oANPAGPAR_5_3.value=this.w_ANPAGPAR
    endif
    if not(this.oPgFrm.Page5.oPag.oANDATMOR_5_4.value==this.w_ANDATMOR)
      this.oPgFrm.Page5.oPag.oANDATMOR_5_4.value=this.w_ANDATMOR
    endif
    if not(this.oPgFrm.Page5.oPag.oANFLESIM_5_5.RadioValue()==this.w_ANFLESIM)
      this.oPgFrm.Page5.oPag.oANFLESIM_5_5.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oANSAGINT_5_6.value==this.w_ANSAGINT)
      this.oPgFrm.Page5.oPag.oANSAGINT_5_6.value=this.w_ANSAGINT
    endif
    if not(this.oPgFrm.Page5.oPag.oANSPRINT_5_7.RadioValue()==this.w_ANSPRINT)
      this.oPgFrm.Page5.oPag.oANSPRINT_5_7.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oDESPAR_5_8.value==this.w_DESPAR)
      this.oPgFrm.Page5.oPag.oDESPAR_5_8.value=this.w_DESPAR
    endif
    if not(this.oPgFrm.Page5.oPag.oCODI_5_13.value==this.w_CODI)
      this.oPgFrm.Page5.oPag.oCODI_5_13.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page5.oPag.oRAG1_5_14.value==this.w_RAG1)
      this.oPgFrm.Page5.oPag.oRAG1_5_14.value=this.w_RAG1
    endif
    if not(this.oPgFrm.Page6.oPag.oCODI_6_1.value==this.w_CODI)
      this.oPgFrm.Page6.oPag.oCODI_6_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page6.oPag.oRAG1_6_2.value==this.w_RAG1)
      this.oPgFrm.Page6.oPag.oRAG1_6_2.value=this.w_RAG1
    endif
    if not(this.oPgFrm.Page6.oPag.oANFLBLVE_6_3.RadioValue()==this.w_ANFLBLVE)
      this.oPgFrm.Page6.oPag.oANFLBLVE_6_3.SetRadio()
    endif
    if not(this.oPgFrm.Page6.oPag.oANVALFID_6_4.value==this.w_ANVALFID)
      this.oPgFrm.Page6.oPag.oANVALFID_6_4.value=this.w_ANVALFID
    endif
    if not(this.oPgFrm.Page6.oPag.oANMAXORD_6_5.value==this.w_ANMAXORD)
      this.oPgFrm.Page6.oPag.oANMAXORD_6_5.value=this.w_ANMAXORD
    endif
    if not(this.oPgFrm.Page6.oPag.oFIDDAT_6_11.value==this.w_FIDDAT)
      this.oPgFrm.Page6.oPag.oFIDDAT_6_11.value=this.w_FIDDAT
    endif
    if not(this.oPgFrm.Page6.oPag.oFIDPAP_6_12.value==this.w_FIDPAP)
      this.oPgFrm.Page6.oPag.oFIDPAP_6_12.value=this.w_FIDPAP
    endif
    if not(this.oPgFrm.Page6.oPag.oFIDESO_6_13.value==this.w_FIDESO)
      this.oPgFrm.Page6.oPag.oFIDESO_6_13.value=this.w_FIDESO
    endif
    if not(this.oPgFrm.Page6.oPag.oFIDESC_6_14.value==this.w_FIDESC)
      this.oPgFrm.Page6.oPag.oFIDESC_6_14.value=this.w_FIDESC
    endif
    if not(this.oPgFrm.Page6.oPag.oFIDORD_6_15.value==this.w_FIDORD)
      this.oPgFrm.Page6.oPag.oFIDORD_6_15.value=this.w_FIDORD
    endif
    if not(this.oPgFrm.Page6.oPag.oFIDDDT_6_16.value==this.w_FIDDDT)
      this.oPgFrm.Page6.oPag.oFIDDDT_6_16.value=this.w_FIDDDT
    endif
    if not(this.oPgFrm.Page6.oPag.oFIDFAT_6_17.value==this.w_FIDFAT)
      this.oPgFrm.Page6.oPag.oFIDFAT_6_17.value=this.w_FIDFAT
    endif
    if not(this.oPgFrm.Page6.oPag.oFIDRES1_6_29.value==this.w_FIDRES1)
      this.oPgFrm.Page6.oPag.oFIDRES1_6_29.value=this.w_FIDRES1
    endif
    if not(this.oPgFrm.Page6.oPag.oFIDRES2_6_30.value==this.w_FIDRES2)
      this.oPgFrm.Page6.oPag.oFIDRES2_6_30.value=this.w_FIDRES2
    endif
    if not(this.oPgFrm.Page7.oPag.oCODI_7_1.value==this.w_CODI)
      this.oPgFrm.Page7.oPag.oCODI_7_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page7.oPag.oRAG1_7_2.value==this.w_RAG1)
      this.oPgFrm.Page7.oPag.oRAG1_7_2.value=this.w_RAG1
    endif
    if not(this.oPgFrm.Page8.oPag.oCODI_8_1.value==this.w_CODI)
      this.oPgFrm.Page8.oPag.oCODI_8_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page8.oPag.oRAG1_8_2.value==this.w_RAG1)
      this.oPgFrm.Page8.oPag.oRAG1_8_2.value=this.w_RAG1
    endif
    if not(this.oPgFrm.Page9.oPag.oCODI_9_1.value==this.w_CODI)
      this.oPgFrm.Page9.oPag.oCODI_9_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page9.oPag.oRAG1_9_2.value==this.w_RAG1)
      this.oPgFrm.Page9.oPag.oRAG1_9_2.value=this.w_RAG1
    endif
    if not(this.oPgFrm.Page9.oPag.oANCODGRU_9_4.value==this.w_ANCODGRU)
      this.oPgFrm.Page9.oPag.oANCODGRU_9_4.value=this.w_ANCODGRU
    endif
    if not(this.oPgFrm.Page9.oPag.oDESGRU_9_7.value==this.w_DESGRU)
      this.oPgFrm.Page9.oPag.oDESGRU_9_7.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page10.oPag.oCODI_10_1.value==this.w_CODI)
      this.oPgFrm.Page10.oPag.oCODI_10_1.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page10.oPag.oRAG1_10_2.value==this.w_RAG1)
      this.oPgFrm.Page10.oPag.oRAG1_10_2.value=this.w_RAG1
    endif
    if not(this.oPgFrm.Page10.oPag.oAN__NOTE_10_4.value==this.w_AN__NOTE)
      this.oPgFrm.Page10.oPag.oAN__NOTE_10_4.value=this.w_AN__NOTE
    endif
    if not(this.oPgFrm.Page11.oPag.oANRATING_11_2.value==this.w_ANRATING)
      this.oPgFrm.Page11.oPag.oANRATING_11_2.value=this.w_ANRATING
    endif
    if not(this.oPgFrm.Page11.oPag.oRADESCRI_11_3.value==this.w_RADESCRI)
      this.oPgFrm.Page11.oPag.oRADESCRI_11_3.value=this.w_RADESCRI
    endif
    if not(this.oPgFrm.Page11.oPag.oANGIORIT_11_6.value==this.w_ANGIORIT)
      this.oPgFrm.Page11.oPag.oANGIORIT_11_6.value=this.w_ANGIORIT
    endif
    if not(this.oPgFrm.Page11.oPag.oANVOCFIN_11_7.value==this.w_ANVOCFIN)
      this.oPgFrm.Page11.oPag.oANVOCFIN_11_7.value=this.w_ANVOCFIN
    endif
    if not(this.oPgFrm.Page11.oPag.oDFDESCRI_11_8.value==this.w_DFDESCRI)
      this.oPgFrm.Page11.oPag.oDFDESCRI_11_8.value=this.w_DFDESCRI
    endif
    if not(this.oPgFrm.Page11.oPag.oANESCDOF_11_9.RadioValue()==this.w_ANESCDOF)
      this.oPgFrm.Page11.oPag.oANESCDOF_11_9.SetRadio()
    endif
    if not(this.oPgFrm.Page11.oPag.oANDESPAR_11_10.RadioValue()==this.w_ANDESPAR)
      this.oPgFrm.Page11.oPag.oANDESPAR_11_10.SetRadio()
    endif
    if not(this.oPgFrm.Page11.oPag.oCODI_11_11.value==this.w_CODI)
      this.oPgFrm.Page11.oPag.oCODI_11_11.value=this.w_CODI
    endif
    if not(this.oPgFrm.Page11.oPag.oRAG1_11_12.value==this.w_RAG1)
      this.oPgFrm.Page11.oPag.oRAG1_11_12.value=this.w_RAG1
    endif
    if not(this.oPgFrm.Page4.oPag.oPADESCRI_4_59.value==this.w_PADESCRI)
      this.oPgFrm.Page4.oPag.oPADESCRI_4_59.value=this.w_PADESCRI
    endif
    cp_SetControlsValueExtFlds(this,'CONTI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not(IIF (.w_CODISO='IT' OR EMPTY (.w_CODISO) ,CHKCFP(.w_ANCODFIS, 'CFA', 'C',1,.w_ANCODICE,' ',.w_ANCOGNOM,.w_AN__NOME,.w_ANLOCNAS,.w_ANPRONAS,.w_ANDATNAS,.w_AN_SESSO,.w_ANPERFIS,,.w_CODCOM), .T.))
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ANCODICE))  and (.w_AUTO<>'S' AND .cFunction='Load')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANCODICE_1_5.SetFocus()
            i_bnoObbl = !empty(.w_ANCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ANDESCRI)) or not(CHKCFP(.w_ANDESCRI, "RA", "C",1, .w_ANCODICE)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANDESCRI_1_6.SetFocus()
            i_bnoObbl = !empty(.w_ANDESCRI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCFP(.w_ANCODFIS, 'CF', 'C',0,.w_ANCODICE, .w_ANNAZION))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANCODFIS_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKCFP(.w_ANPARIVA, "PI",'C',0,.w_ANCODICE, .w_ANNAZION))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANPARIVA_1_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ANCONSUP)) or not(.w_NULIV=1 AND .w_TIPMAS='C'))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oANCONSUP_2_7.SetFocus()
            i_bnoObbl = !empty(.w_ANCONSUP)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_ANCATCON))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oANCATCON_2_9.SetFocus()
            i_bnoObbl = !empty(.w_ANCATCON)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_ANCONRIF))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oANCONRIF_2_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice conto inesistente oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_ANCODIVA))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oANCODIVA_2_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice IVA inesistente oppure obsoleto")
          case   not(EMPTY(.w_TIDTOBSO) OR .w_TIDTOBSO>.w_OBTEST)  and not(empty(.w_ANTIPOPE))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oANTIPOPE_2_20.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice incongruente oppure obsoleto")
          case   not(.w_FLIRPE='R')  and (.w_ANFLRITE='S' AND (g_RITE='S' OR IsAlt()))  and not(empty(.w_ANCODIRP))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oANCODIRP_2_24.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Inserire un codice tributo I.R.PE.F.")
          case   not(.w_ANFLESIG $ 'SN')  and (g_PERPAR='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oANFLESIG_2_41.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Test soggetto pubblico non definito")
          case   not(.w_ANFLESIG<>'S' OR .w_ANPARTSN='S')  and (g_PERPAR='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oANPARTSN_2_48.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Test partite incongruente con test esigibilit�")
          case   not(chkconstucf(.w_ancodstu,.w_codstu))  and not(g_LEMC<>'S' OR g_TRAEXP='N')  and (g_LEMC='S' AND g_TRAEXP<>'N')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oANCODSTU_2_52.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice deve essere compreso negli intervalli del conto studio selezionato")
          case   not(.w_FLSCM $ ' C')  and not(IsAlt())  and not(empty(.w_ANCATSCM))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oANCATSCM_3_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria sconti / maggiorazioni inesistente o non di tipo cliente")
          case   not(.w_FLPRO $ ' C')  and not(IsAlt())  and not(empty(.w_ANGRUPRO))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oANGRUPRO_3_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Categoria provvigioni inesistente o non di tipo cliente")
          case   (empty(.w_ANCODLIN))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oANCODLIN_3_17.SetFocus()
            i_bnoObbl = !empty(.w_ANCODLIN)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKVLCF(.w_ANCODVAL, .w_VALLIS, "CLIENTE", 'X', ' ') AND CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .T.))  and not(empty(.w_ANCODVAL))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oANCODVAL_3_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(CHKVLCF(.w_ANCODVAL, .w_VALLIS, "CLIENTE", .w_ANSCORPO, .w_IVALIS))  and not(empty(.w_ANNUMLIS))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oANNUMLIS_3_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_ANCODZON))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oANCODZON_3_26.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice zona inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(IsAlt())  and not(empty(.w_ANCODAG1))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oANCODAG1_3_27.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_ANCODPAG))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oANCODPAG_4_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
          case   not(.w_ANGIOFIS <= 31)
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oANGIOFIS_4_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_AN1MESCL<13 AND (.w_AN1MESCL<.w_AN2MESCL OR .w_AN2MESCL=0))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oAN1MESCL_4_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ANGIOSC1)) or not(.w_ANGIOSC1 <= 31))  and (.w_AN1MESCL>0)
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oANGIOSC1_4_11.SetFocus()
            i_bnoObbl = !empty(.w_ANGIOSC1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_AN2MESCL<13 AND (.w_AN1MESCL<.w_AN2MESCL OR .w_AN2MESCL=0))  and (.w_AN1MESCL > 0)
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oAN2MESCL_4_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_ANGIOSC2)) or not(.w_ANGIOSC2 <= 31))  and (.w_AN2MESCL>0)
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oANGIOSC2_4_14.SetFocus()
            i_bnoObbl = !empty(.w_ANGIOSC2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and .w_CONSBF='C')  and not(empty(.w_ANCODBA2))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oANCODBA2_4_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente o obsoleto o di tipo salvo buon fine")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_ANGESCON='M')  and not(empty(.w_ANPAGPAR))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oANPAGPAR_5_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice pagamento inesistente oppure obsoleto")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAR_MSA.CheckForm()
      if i_bres
        i_bres=  .GSAR_MSA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MIN.CheckForm()
      if i_bres
        i_bres=  .GSAR_MIN.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=3
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MCC.CheckForm()
      if i_bres
        i_bres=  .GSAR_MCC.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=4
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MCO.CheckForm()
      if i_bres
        i_bres=  .GSAR_MCO.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=7
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MDD.CheckForm()
      if i_bres
        i_bres=  .GSAR_MDD.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=8
        endif
      endif
      *i_bRes = i_bRes .and. .GSAR_MSE.CheckForm()
      if i_bres
        i_bres=  .GSAR_MSE.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=9
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsar_acl
      IF (.w_AFFLINTR='S' AND EMPTY(.w_ANCODIVA))
           i_bnoChk = .f.
           i_bRes = .f.
           i_cErrorMsg =Ah_MsgFormat( "Codice IVA non Imponibile non definito (cliente INTRA)")
      ENDIF
      IF (this.w_ANIDRIDY='S' AND EMPTY (this.w_ANIBARID))
        AH_ERRORMSG ("Codice identificativo RID non inserito")
      ENDIF
      * Verifico se l'utente ha impostato il campo comunicazione fiscalit�
      * privilegiata oppure soggetto terzo e le combo "Operazione rilevanti IVA" oppure
      * "Tipologia prevalente "a 'beni/servizi' 
      if  i_bRes and (.w_ANFLBLLS='S' or .w_ANFLSOAL="S") and (.w_ANOPETRE $ 'PC' OR .w_ANTIPPRE $ 'BS')
         if ah_yesno ('Attenzione: le operazioni interessate dalla%0Comunicazione fiscalit� privilegiata%0sono normalmente escluse%0dalla Comunicazione operazioni superiori a 3000 euro.%0Confermi ugualmente?')
           * Confermo il salvataggio del record
         else
           i_bnoChk = .f.
           i_bRes = .f.
      		 i_cErrorMsg = Ah_MsgFormat("Riconsiderare i parametri relativi a 'Fiscalit� privilegiata'/'Soggetto terzo' e 'Comunicazione operazioni superiori a 3.000 euro'")
         endif
      endif
      bControllo=i_bRes
      
      * Se � attivo il modulo revi si esegue il controllo per verificare che i codici di pagamento tra le diverse aziende siano uguali
      If g_REVI='S' And .w_ANFLGCPZ='S' And Alltrim(i_CODAZI) $ AssocLst()
      	Local risultato
      	risultato = IAHR_CHK1BCP(This)
      	If Not risultato
      		i_bnoChk = .F.
      		i_bRes = .F.
      		i_cErrorMsg =Ah_MsgFormat("Operazione annullata")
      	Endif
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AUTO = this.w_AUTO
    this.o_ANTIPCON = this.w_ANTIPCON
    this.o_ANCODICE = this.w_ANCODICE
    this.o_ANCODFIS = this.w_ANCODFIS
    this.o_ANPERFIS = this.w_ANPERFIS
    this.o_ANCHKMAI = this.w_ANCHKMAI
    this.o_ANCHKPEC = this.w_ANCHKPEC
    this.o_ANCHKCPZ = this.w_ANCHKCPZ
    this.o_CONTA = this.w_CONTA
    this.o_PROSTU = this.w_PROSTU
    this.o_CODSTU = this.w_CODSTU
    this.o_ANFLRITE = this.w_ANFLRITE
    this.o_ANCODIRP = this.w_ANCODIRP
    this.o_ANFLESIG = this.w_ANFLESIG
    this.o_AFFLINTR = this.w_AFFLINTR
    this.o_ANFLBLLS = this.w_ANFLBLLS
    this.o_ANFLSOAL = this.w_ANFLSOAL
    this.o_ANFLSGRE = this.w_ANFLSGRE
    this.o_ANCODSTU = this.w_ANCODSTU
    this.o_AN1SCONT = this.w_AN1SCONT
    this.o_AN1MESCL = this.w_AN1MESCL
    this.o_AN2MESCL = this.w_AN2MESCL
    this.o_ANNUMCOR = this.w_ANNUMCOR
    this.o_ANCINABI = this.w_ANCINABI
    this.o_AN__BBAN = this.w_AN__BBAN
    this.o_ANIDRIDY = this.w_ANIDRIDY
    this.o_ANTIIDRI = this.w_ANTIIDRI
    this.o_ANIBARID = this.w_ANIBARID
    this.o_ANGESCON = this.w_ANGESCON
    this.o_ANPAGPAR = this.w_ANPAGPAR
    this.o_ANFLESIM = this.w_ANFLESIM
    this.o_ANSAGINT = this.w_ANSAGINT
    * --- GSAR_MSA : Depends On
    this.GSAR_MSA.SaveDependsOn()
    * --- GSAR_MIN : Depends On
    this.GSAR_MIN.SaveDependsOn()
    * --- GSAR_MCC : Depends On
    this.GSAR_MCC.SaveDependsOn()
    * --- GSAR_MCO : Depends On
    this.GSAR_MCO.SaveDependsOn()
    * --- GSAR_MDD : Depends On
    this.GSAR_MDD.SaveDependsOn()
    * --- GSAR_MSE : Depends On
    this.GSAR_MSE.SaveDependsOn()
    return

  func CanDelete()
    local i_res
    i_res=GSMA_BAE(this,'G')
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Operazione annullata"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsar_aclPag1 as StdContainer
  Width  = 769
  height = 554
  stdWidth  = 769
  stdheight = 554
  resizeXpos=323
  resizeYpos=311
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANCODICE_1_5 as StdField with uid="SXFRLRPZWL",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ANCODICE", cQueryName = "ANTIPCON,ANCODICE",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice alfanumerico cliente",;
    HelpContextID = 150336693,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=131, Top=8, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15)

  func oANCODICE_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AUTO<>'S' AND .cFunction='Load')
    endwith
   endif
  endfunc

  proc oANCODICE_1_5.mAfter
    with this.Parent.oContained
      .w_ANCODICE=CALCZER(.w_ANCODICE, 'CONTI')
    endwith
  endproc

  add object oANDESCRI_1_6 as StdField with uid="CMHDFQJCVM",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ANDESCRI", cQueryName = "ANDESCRI",;
    bObbl = .t. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Esatta ragione sociale del cliente",;
    HelpContextID = 235922609,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=282, Top=8, InputMask=replicate('X',60)

  proc oANDESCRI_1_6.mAfter
    with this.Parent.oContained
      .w_ANDESCRI=IIF(.cFunction<>'Query', Normragsoc(.w_ANDESCRI), .w_ANDESCRI)
    endwith
  endproc

  func oANDESCRI_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_ANDESCRI, "RA", "C",1, .w_ANCODICE))
    endwith
    return bRes
  endfunc

  add object oANDESCR2_1_7 as StdField with uid="BFMUQOCHSS",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ANDESCR2", cQueryName = "ANDESCR2",;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi alla ragione sociale",;
    HelpContextID = 235922632,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=282, Top=34, InputMask=replicate('X',60)

  proc oANDESCR2_1_7.mAfter
    with this.Parent.oContained
      .w_ANDESCR2=IIF(.cFunction<>'Query', Normragsoc(.w_ANDESCR2), .w_ANDESCR2)
    endwith
  endproc

  add object oANINDIRI_1_11 as StdField with uid="ZBVEMKDLNK",rtseq=11,rtrep=.f.,;
    cFormVar = "w_ANINDIRI", cQueryName = "ANINDIRI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo del cliente",;
    HelpContextID = 150377649,;
   bGlobalFont=.t.,;
    Height=21, Width=324, Left=132, Top=62, InputMask=replicate('X',35)

  add object oANINDIR2_1_12 as StdField with uid="ZXVXLVHBNO",rtseq=12,rtrep=.f.,;
    cFormVar = "w_ANINDIR2", cQueryName = "ANINDIR2",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Dati aggiuntivi all'indirizzo del cliente",;
    HelpContextID = 150377672,;
   bGlobalFont=.t.,;
    Height=21, Width=324, Left=132, Top=86, InputMask=replicate('X',35)

  add object oAN___CAP_1_13 as StdField with uid="ITMWFHHFBB",rtseq=13,rtrep=.f.,;
    cFormVar = "w_AN___CAP", cQueryName = "AN___CAP",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice di avviamento postale",;
    HelpContextID = 221525162,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=132, Top=110, InputMask=replicate('X',8), bHasZoom = .t. 

  proc oAN___CAP_1_13.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_AN___CAP",".w_ANLOCALI",".w_ANPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oANLOCALI_1_14 as StdField with uid="VJQOSUOYJH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_ANLOCALI", cQueryName = "ANLOCALI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di residenza",;
    HelpContextID = 251304783,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=202, Top=110, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oANLOCALI_1_14.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_AN___CAP",".w_ANLOCALI",".w_ANPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oANPROVIN_1_15 as StdField with uid="VEZHRVTQSK",rtseq=15,rtrep=.f.,;
    cFormVar = "w_ANPROVIN", cQueryName = "ANPROVIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza",;
    HelpContextID = 79551316,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=132, Top=134, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  proc oANPROVIN_1_15.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P"," "," ",".w_ANPROVIN")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oANNAZION_1_16 as StdField with uid="UYFCRMBYAN",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ANNAZION", cQueryName = "ANNAZION",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della nazione di appartenenza",;
    HelpContextID = 140294996,;
   bGlobalFont=.t.,;
    Height=21, Width=68, Left=132, Top=158, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="NAZIONI", cZoomOnZoom="GSAR_ANZ", oKey_1_1="NACODNAZ", oKey_1_2="this.w_ANNAZION"

  func oANNAZION_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oANNAZION_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANNAZION_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'NAZIONI','*','NACODNAZ',cp_AbsName(this.parent,'oANNAZION_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ANZ',"Nazioni",'',this.parent.oContained
  endproc
  proc oANNAZION_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ANZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_NACODNAZ=this.parent.oContained.w_ANNAZION
     i_obj.ecpSave()
  endproc

  add object oDESNAZ_1_17 as StdField with uid="RNBNUYYXDX",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESNAZ", cQueryName = "DESNAZ",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 136707530,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=202, Top=159, InputMask=replicate('X',35)

  add object oCODISO_1_18 as StdField with uid="XWWEYAMFAQ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_CODISO", cQueryName = "CODISO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 234101798,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=701, Top=159, InputMask=replicate('X',3)

  add object oANCODFIS_1_19 as StdField with uid="IMZUEJYJPI",rtseq=19,rtrep=.f.,;
    cFormVar = "w_ANCODFIS", cQueryName = "ANCODFIS",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale - Doppio click per eseguire il calcolo del codice fiscale di una persona fisica",;
    HelpContextID = 67767129,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=132, Top=184, cSayPict='REPL("!",16)', cGetPict='REPL("!",16)', InputMask=replicate('X',16), bHasZoom = .t. 

  func oANCODFIS_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_ANCODFIS, 'CF', 'C',0,.w_ANCODICE, .w_ANNAZION))
    endwith
    return bRes
  endfunc

  proc oANCODFIS_1_19.mZoom
    ZoomCF (  this.parent.oContained )
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oANPARIVA_1_20 as StdField with uid="BRJPVBLCII",rtseq=20,rtrep=.f.,;
    cFormVar = "w_ANPARIVA", cQueryName = "ANPARIVA",;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    ToolTipText = "Partita IVA",;
    HelpContextID = 136520889,;
   bGlobalFont=.t.,;
    Height=21, Width=107, Left=132, Top=209, InputMask=replicate('X',12)

  proc oANPARIVA_1_20.mBefore
    with this.Parent.oContained
      this.cQueryName = "ANPARIVA"
    endwith
  endproc

  func oANPARIVA_1_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (CHKCFP(.w_ANPARIVA, "PI",'C',0,.w_ANCODICE, .w_ANNAZION))
    endwith
    return bRes
  endfunc

  add object oANTELEFO_1_21 as StdField with uid="JLZYRXGMEO",rtseq=21,rtrep=.f.,;
    cFormVar = "w_ANTELEFO", cQueryName = "ANTELEFO",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero telefonico principale",;
    HelpContextID = 209642667,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=569, Top=187, InputMask=replicate('X',18)

  add object oANTELFAX_1_22 as StdField with uid="EYJTSVKBRC",rtseq=22,rtrep=.f.,;
    cFormVar = "w_ANTELFAX", cQueryName = "ANTELFAX",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero di TELEX o FAX",;
    HelpContextID = 192865442,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=569, Top=211, InputMask=replicate('X',18)

  add object oANNUMCEL_1_23 as StdField with uid="QMXPYEQQHX",rtseq=23,rtrep=.f.,;
    cFormVar = "w_ANNUMCEL", cQueryName = "ANNUMCEL",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero del telefono cellulare",;
    HelpContextID = 241124526,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=569, Top=235, InputMask=replicate('X',18)

  add object oANINDWEB_1_24 as StdField with uid="NJCOVFUBWC",rtseq=24,rtrep=.f.,;
    cFormVar = "w_ANINDWEB", cQueryName = "ANINDWEB",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo sito Internet",;
    HelpContextID = 183932088,;
   bGlobalFont=.t.,;
    Height=21, Width=324, Left=132, Top=235, InputMask=replicate('X',254)

  add object oAN_EMAIL_1_25 as StdField with uid="CPMLNBOBNM",rtseq=25,rtrep=.f.,;
    cFormVar = "w_AN_EMAIL", cQueryName = "AN_EMAIL",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo di posta elettronica",;
    HelpContextID = 7222446,;
   bGlobalFont=.t.,;
    Height=21, Width=324, Left=132, Top=261, InputMask=replicate('X',254)

  add object oAN_SKYPE_1_26 as StdField with uid="ZHYLYWTOOS",rtseq=26,rtrep=.f.,;
    cFormVar = "w_AN_SKYPE", cQueryName = "AN_SKYPE",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Codice utente Skype",;
    HelpContextID = 142619829,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=569, Top=260, InputMask=replicate('X',50)

  add object oAN_EMPEC_1_27 as StdField with uid="WJJJONEHWZ",rtseq=27,rtrep=.f.,;
    cFormVar = "w_AN_EMPEC", cQueryName = "AN_EMPEC",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo di posta elettronica certificata (PEC)",;
    HelpContextID = 23999671,;
   bGlobalFont=.t.,;
    Height=21, Width=324, Left=132, Top=287, InputMask=replicate('X',254)


  add object oANCODSAL_1_28 as StdTableCombo with uid="PZAGGDGVEP",rtseq=28,rtrep=.f.,left=132,top=315,width=277,height=21;
    , ToolTipText = "Formula di cortesia";
    , HelpContextID = 250999982;
    , cFormVar="w_ANCODSAL",tablefilter="", bObbl = .f. , nPag = 1;
    , cLinkFile="SAL_NOMI";
    , cTable='SAL_NOMI',cKey='SACODICE',cValue='SADESCRI',cOrderBy='SADESCRI',xDefault=space(5);
  , bGlobalFont=.t.


  func oANCODSAL_1_28.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  func oANCODSAL_1_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODSAL_1_28.ecpDrop(oSource)
    this.Parent.oContained.link_1_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oANPERFIS_1_29 as StdCheck with uid="KCLBTUIZYE",rtseq=29,rtrep=.f.,left=26, top=344, caption="Persona fisica",;
    ToolTipText = "Se attivo: il cliente � una persona fisica",;
    HelpContextID = 81845081,;
    cFormVar="w_ANPERFIS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANPERFIS_1_29.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANPERFIS_1_29.GetRadio()
    this.Parent.oContained.w_ANPERFIS = this.RadioValue()
    return .t.
  endfunc

  func oANPERFIS_1_29.SetRadio()
    this.Parent.oContained.w_ANPERFIS=trim(this.Parent.oContained.w_ANPERFIS)
    this.value = ;
      iif(this.Parent.oContained.w_ANPERFIS=='S',1,;
      0)
  endfunc

  add object oANCOGNOM_1_30 as StdField with uid="ZZNZUIPJAK",rtseq=30,rtrep=.f.,;
    cFormVar = "w_ANCOGNOM", cQueryName = "ANCOGNOM",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del cliente se persona fisica",;
    HelpContextID = 205130579,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=235, Top=344, InputMask=replicate('X',50)

  func oANCOGNOM_1_30.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANPERFIS = "S")
    endwith
   endif
  endfunc

  add object oAN__NOME_1_31 as StdField with uid="OBJSQJOBOP",rtseq=31,rtrep=.f.,;
    cFormVar = "w_AN__NOME", cQueryName = "AN__NOME",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Nome del cliente se persona fisica",;
    HelpContextID = 230411083,;
   bGlobalFont=.t.,;
    Height=21, Width=178, Left=548, Top=344, InputMask=replicate('X',50)

  func oAN__NOME_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANPERFIS = "S")
    endwith
   endif
  endfunc


  add object oAN_SESSO_1_32 as StdCombo with uid="MSRTRSDZTV",rtseq=32,rtrep=.f.,left=26,top=370,width=98,height=21;
    , ToolTipText = "Sesso";
    , HelpContextID = 249574571;
    , cFormVar="w_AN_SESSO",RowSource=""+"Maschio,"+"Femmina", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAN_SESSO_1_32.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oAN_SESSO_1_32.GetRadio()
    this.Parent.oContained.w_AN_SESSO = this.RadioValue()
    return .t.
  endfunc

  func oAN_SESSO_1_32.SetRadio()
    this.Parent.oContained.w_AN_SESSO=trim(this.Parent.oContained.w_AN_SESSO)
    this.value = ;
      iif(this.Parent.oContained.w_AN_SESSO=='M',1,;
      iif(this.Parent.oContained.w_AN_SESSO=='F',2,;
      0))
  endfunc

  func oAN_SESSO_1_32.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANPERFIS = "S")
    endwith
   endif
  endfunc

  add object oANLOCNAS_1_33 as StdField with uid="MSCIMZGJRW",rtseq=33,rtrep=.f.,;
    cFormVar = "w_ANLOCNAS", cQueryName = "ANLOCNAS",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Luogo di nascita del cliente",;
    HelpContextID = 67462311,;
   bGlobalFont=.t.,;
    Height=21, Width=221, Left=235, Top=370, InputMask=replicate('X',30), bHasZoom = .t. 

  func oANLOCNAS_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANPERFIS = "S")
    endwith
   endif
  endfunc

  proc oANLOCNAS_1_33.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C"," ",".w_ANLOCNAS",".w_ANPRONAS",,,".w_CODCOM")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oANPRONAS_1_34 as StdField with uid="XZXUTODSFJ",rtseq=34,rtrep=.f.,;
    cFormVar = "w_ANPRONAS", cQueryName = "ANPRONAS",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia del luogo di nascita",;
    HelpContextID = 54666407,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=548, Top=370, cSayPict='"!!"', cGetPict='"!!"', InputMask=replicate('X',2), bHasZoom = .t. 

  func oANPRONAS_1_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANPERFIS = "S")
    endwith
   endif
  endfunc

  proc oANPRONAS_1_34.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P"," "," ",".w_ANPRONAS")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oANDATNAS_1_35 as StdField with uid="IYXWCFDMRG",rtseq=35,rtrep=.f.,;
    cFormVar = "w_ANDATNAS", cQueryName = "ANDATNAS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del cliente",;
    HelpContextID = 50586791,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=646, Top=370

  func oANDATNAS_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANPERFIS = "S")
    endwith
   endif
  endfunc

  add object oANNUMCAR_1_37 as StdField with uid="IQPWZCTMJM",rtseq=36,rtrep=.f.,;
    cFormVar = "w_ANNUMCAR", cQueryName = "ANNUMCAR",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero carta di identit�",;
    HelpContextID = 241124520,;
   bGlobalFont=.t.,;
    Height=21, Width=149, Left=548, Top=396, InputMask=replicate('X',18)

  func oANNUMCAR_1_37.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANPERFIS = "S")
    endwith
   endif
  endfunc

  add object oANCHKSTA_1_57 as StdCheck with uid="HDBTXBPKLN",rtseq=41,rtrep=.f.,left=26, top=427, caption="Stampa",;
    HelpContextID = 244118713,;
    cFormVar="w_ANCHKSTA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANCHKSTA_1_57.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANCHKSTA_1_57.GetRadio()
    this.Parent.oContained.w_ANCHKSTA = this.RadioValue()
    return .t.
  endfunc

  func oANCHKSTA_1_57.SetRadio()
    this.Parent.oContained.w_ANCHKSTA=trim(this.Parent.oContained.w_ANCHKSTA)
    this.value = ;
      iif(this.Parent.oContained.w_ANCHKSTA=='S',1,;
      0)
  endfunc

  func oANCHKSTA_1_57.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oANCHKMAI_1_58 as StdCheck with uid="VNSLKUJWHZ",rtseq=42,rtrep=.f.,left=111, top=427, caption="E-mail",;
    HelpContextID = 76346545,;
    cFormVar="w_ANCHKMAI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANCHKMAI_1_58.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANCHKMAI_1_58.GetRadio()
    this.Parent.oContained.w_ANCHKMAI = this.RadioValue()
    return .t.
  endfunc

  func oANCHKMAI_1_58.SetRadio()
    this.Parent.oContained.w_ANCHKMAI=trim(this.Parent.oContained.w_ANCHKMAI)
    this.value = ;
      iif(this.Parent.oContained.w_ANCHKMAI=='S',1,;
      0)
  endfunc

  func oANCHKMAI_1_58.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oANCHKPEC_1_59 as StdCheck with uid="GQWOQUNDXG",rtseq=43,rtrep=.f.,left=187, top=427, caption="PEC",;
    HelpContextID = 26014903,;
    cFormVar="w_ANCHKPEC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANCHKPEC_1_59.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANCHKPEC_1_59.GetRadio()
    this.Parent.oContained.w_ANCHKPEC = this.RadioValue()
    return .t.
  endfunc

  func oANCHKPEC_1_59.SetRadio()
    this.Parent.oContained.w_ANCHKPEC=trim(this.Parent.oContained.w_ANCHKPEC)
    this.value = ;
      iif(this.Parent.oContained.w_ANCHKPEC=='S',1,;
      0)
  endfunc

  func oANCHKPEC_1_59.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oANCHKFAX_1_60 as StdCheck with uid="ZXITRVALRT",rtseq=44,rtrep=.f.,left=256, top=427, caption="FAX",;
    HelpContextID = 193787042,;
    cFormVar="w_ANCHKFAX", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANCHKFAX_1_60.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANCHKFAX_1_60.GetRadio()
    this.Parent.oContained.w_ANCHKFAX = this.RadioValue()
    return .t.
  endfunc

  func oANCHKFAX_1_60.SetRadio()
    this.Parent.oContained.w_ANCHKFAX=trim(this.Parent.oContained.w_ANCHKFAX)
    this.value = ;
      iif(this.Parent.oContained.w_ANCHKFAX=='S',1,;
      0)
  endfunc

  func oANCHKFAX_1_60.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oANCHKCPZ_1_61 as StdCheck with uid="SIMTIXJRRY",rtseq=45,rtrep=.f.,left=325, top=427, caption="Web Application",;
    ToolTipText = "Se attivo: consente invio documenti a cliente su Web Application",;
    HelpContextID = 24316768,;
    cFormVar="w_ANCHKCPZ", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANCHKCPZ_1_61.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANCHKCPZ_1_61.GetRadio()
    this.Parent.oContained.w_ANCHKCPZ = this.RadioValue()
    return .t.
  endfunc

  func oANCHKCPZ_1_61.SetRadio()
    this.Parent.oContained.w_ANCHKCPZ=trim(this.Parent.oContained.w_ANCHKCPZ)
    this.value = ;
      iif(this.Parent.oContained.w_ANCHKCPZ=='S',1,;
      0)
  endfunc

  func oANCHKCPZ_1_61.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((g_IZCP$'SA'  or g_CPIN='S') and g_DMIP<>'S' and .w_ANFLGCPZ='S')
    endwith
   endif
  endfunc

  func oANCHKCPZ_1_61.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oANCHKWWP_1_62 as StdCheck with uid="MATRZJJCUX",rtseq=46,rtrep=.f.,left=452, top=427, caption="Net-folder",;
    HelpContextID = 177009834,;
    cFormVar="w_ANCHKWWP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANCHKWWP_1_62.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANCHKWWP_1_62.GetRadio()
    this.Parent.oContained.w_ANCHKWWP = this.RadioValue()
    return .t.
  endfunc

  func oANCHKWWP_1_62.SetRadio()
    this.Parent.oContained.w_ANCHKWWP=trim(this.Parent.oContained.w_ANCHKWWP)
    this.value = ;
      iif(this.Parent.oContained.w_ANCHKWWP=='S',1,;
      0)
  endfunc

  func oANCHKWWP_1_62.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oANCHKPTL_1_63 as StdCheck with uid="RFDRJEBWSG",rtseq=47,rtrep=.f.,left=544, top=427, caption="PostaLite",;
    HelpContextID = 26014894,;
    cFormVar="w_ANCHKPTL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANCHKPTL_1_63.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANCHKPTL_1_63.GetRadio()
    this.Parent.oContained.w_ANCHKPTL = this.RadioValue()
    return .t.
  endfunc

  func oANCHKPTL_1_63.SetRadio()
    this.Parent.oContained.w_ANCHKPTL=trim(this.Parent.oContained.w_ANCHKPTL)
    this.value = ;
      iif(this.Parent.oContained.w_ANCHKPTL=='S',1,;
      0)
  endfunc

  func oANCHKPTL_1_63.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oANCHKFIR_1_64 as StdCheck with uid="FRVFQCMMTF",rtseq=48,rtrep=.f.,left=643, top=427, caption="Firma digitale",;
    ToolTipText = "Se attivo, consente invio al cliente del documento firmato",;
    HelpContextID = 74648408,;
    cFormVar="w_ANCHKFIR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANCHKFIR_1_64.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANCHKFIR_1_64.GetRadio()
    this.Parent.oContained.w_ANCHKFIR = this.RadioValue()
    return .t.
  endfunc

  func oANCHKFIR_1_64.SetRadio()
    this.Parent.oContained.w_ANCHKFIR=trim(this.Parent.oContained.w_ANCHKFIR)
    this.value = ;
      iif(this.Parent.oContained.w_ANCHKFIR=='S',1,;
      0)
  endfunc

  func oANCHKFIR_1_64.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANCHKMAI='S' or .w_ANCHKPEC='S' or .w_ANCHKCPZ = 'S')
    endwith
   endif
  endfunc

  func oANCHKFIR_1_64.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oANCODEST_1_65 as StdField with uid="BBBNJNPXYQ",rtseq=49,rtrep=.f.,;
    cFormVar = "w_ANCODEST", cQueryName = "ANCODEST",;
    bObbl = .f. , nPag = 1, value=space(7), bMultilanguage =  .f.,;
    ToolTipText = "Per le fatture PA contiene il codice, di 6 caratteri, dell'ufficio destinatario della fattura, riportato nella rubrica Indice PA,  per le fatture privati contiene il codice, di 7 caratteri, assegnato dal Sdi ai sogg. che hanno accreditato un canale",;
    HelpContextID = 217445542,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=132, Top=478, InputMask=replicate('X',7)

  add object oANCODCLA_1_66 as StdField with uid="NDPWHLKDYP",rtseq=50,rtrep=.f.,;
    cFormVar = "w_ANCODCLA", cQueryName = "ANCODCLA",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice di classificazione per fatturazione elettronica",;
    HelpContextID = 17435463,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=548, Top=478, InputMask=replicate('X',5)

  add object oANCODPEC_1_67 as StdMemo with uid="BVOZZMZVBA",rtseq=51,rtrep=.f.,;
    cFormVar = "w_ANCODPEC", cQueryName = "ANCODPEC",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo PEC al quale inviare il documento",;
    HelpContextID = 32896183,;
   bGlobalFont=.t.,;
    Height=21, Width=597, Left=132, Top=505

  add object oANDTOBSO_1_68 as StdField with uid="ZKZQUXBZQS",rtseq=52,rtrep=.f.,;
    cFormVar = "w_ANDTOBSO", cQueryName = "ANDTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di validit�",;
    HelpContextID = 255911083,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=643, Top=532, tabstop=.f.

  add object oANDTINVA_1_72 as StdField with uid="KUQMAKJFFV",rtseq=53,rtrep=.f.,;
    cFormVar = "w_ANDTINVA", cQueryName = "ANDTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit�",;
    HelpContextID = 60875961,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=389, Top=532, tabstop=.f.

  func oANDTINVA_1_72.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oObj_1_74 as cp_runprogram with uid="XRJBLACLJR",left=0, top=573, width=179,height=24,;
    caption='GSAR_BAU(A)',;
   bGlobalFont=.t.,;
    prg="GSAR_BAU('A')",;
    cEvent = "New record,CFGLoaded",;
    nPag=1;
    , HelpContextID = 239090629


  add object oObj_1_76 as cp_runprogram with uid="VMINIECNRR",left=447, top=604, width=201,height=24,;
    caption='GSAR_BOL(V)',;
   bGlobalFont=.t.,;
    prg="GSAR_BOL('V',w_ANCODICE)",;
    cEvent = "ClienteVariato",;
    nPag=1;
    , HelpContextID = 29350194


  add object oObj_1_77 as cp_runprogram with uid="ASLWPWYIPJ",left=0, top=604, width=179,height=24,;
    caption='GSAR_BPS',;
   bGlobalFont=.t.,;
    prg="GSAR_BPS",;
    cEvent = "Record Inserted",;
    nPag=1;
    , HelpContextID = 29159609


  add object oObj_1_78 as cp_runprogram with uid="UGICLFVUCP",left=185, top=604, width=260,height=24,;
    caption='GSPS_BCP(w_ANCODICE,E)',;
   bGlobalFont=.t.,;
    prg="GSPS_BCP(w_ANCODICE,'E')",;
    cEvent = "CliPosEliminato",;
    nPag=1;
    , HelpContextID = 124183050


  add object oObj_1_79 as cp_runprogram with uid="SQLYSNFJYB",left=185, top=573, width=260,height=24,;
    caption='GSPS_BCP(w_ANCODICE,V)',;
   bGlobalFont=.t.,;
    prg="GSPS_BCP(w_ANCODICE,'V')",;
    cEvent = "CliPosVariato",;
    nPag=1;
    , HelpContextID = 142008842


  add object oObj_1_80 as cp_runprogram with uid="UTMUKFCDVA",left=0, top=632, width=179,height=21,;
    caption='GSAR_BAN',;
   bGlobalFont=.t.,;
    prg="GSAR_BAN",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 239275852


  add object oBtn_1_83 as StdButton with uid="OJYMBPOMWQ",left=592, top=8, width=48,height=45,;
    CpPicture="bmp\cattura.bmp", caption="", nPag=1;
    , ToolTipText = "Cattura un allegato da unit� disco";
    , HelpContextID = 213766618;
    , tabstop=.f., Visible=IIF(g_FLARDO='S', .T., .F.), UID = 'CATTURA', disabledpicture = 'bmp\catturad.bmp',Caption='\<Cattura';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_83.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"CONTI",.w_ANTIPCON+ALLTRIM(.w_ANCODICE),"GSAR_ACL","C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_83.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_ANCODICE) And .cFunction ='Query')
      endwith
    endif
  endfunc

  func oBtn_1_83.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_FLARDO<>'S' or isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_84 as StdButton with uid="YOPFSNISFV",left=643, top=8, width=48,height=45,;
    CpPicture="BMP\visualicat.ico", caption="", nPag=1;
    , ToolTipText = "Visualizza allegati";
    , HelpContextID = 201560176;
    , Caption='\<Visualizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_84.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"CONTI",.w_ANTIPCON+ALLTRIM(.w_ANCODICE),"GSAR_ACL","V")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_84.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_ANCODICE) And .cFunction ='Query')
      endwith
    endif
  endfunc

  func oBtn_1_84.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_FLARDO<>'S' or isalt())
     endwith
    endif
  endfunc


  add object oBtn_1_85 as StdButton with uid="BJRFWTRDLX",left=694, top=8, width=48,height=45,;
    CpPicture="bmp\scanner.bmp", caption="", nPag=1;
    , ToolTipText = "Cattura un allegato da periferica di acquisizione immagini";
    , HelpContextID = 171245274;
    , tabstop=.f., Visible=IIF(g_FLARDO='S', .T., .F.), UID = 'SCANNER', disabledpicture = 'bmp\scannerd.bmp', Caption='\<Scanner';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_85.Click()
      with this.Parent.oContained
        GSUT_BCV(this.Parent.oContained,"CONTI",.w_ANTIPCON+ALLTRIM(.w_ANCODICE),"GSAR_ACL","S",MROW(),MCOL(),.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_85.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.not. empty(.w_ANCODICE) And .cFunction ='Query')
      endwith
    endif
  endfunc

  func oBtn_1_85.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_DOCM<>'S' OR g_FLARDO<>'S' or isalt())
     endwith
    endif
  endfunc


  add object oLinkPC_1_86 as StdButton with uid="FIYMUPNCST",left=592, top=55, width=48,height=45,;
    CpPicture="BMP\AnagStorico.bmp", caption="", nPag=1;
    , ToolTipText = "Dati anagrafici storicizzati";
    , HelpContextID = 188096936;
    , Caption='\<An. stor.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_1_86.Click()
      this.Parent.oContained.GSAR_MSA.LinkPCClick()
    endproc


  add object oBtn_1_87 as StdButton with uid="WQPDFLJUJA",left=643, top=55, width=48,height=45,;
    CpPicture="BMP\Storicizza.bmp", caption="", nPag=1;
    , ToolTipText = "Storicizza i dati anagrafici";
    , HelpContextID = 58743616;
    , Caption='S\<toricizza';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_87.Click()
      with this.Parent.oContained
        do GSAR_BSZ with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_87.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction ='Edit')
      endwith
    endif
  endfunc


  add object oBtn_1_89 as StdButton with uid="EAZNCZYAIS",left=694, top=102, width=48,height=45,;
    CpPicture="bmp\Visualiz.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai documenti della parcellazione";
    , HelpContextID = 204541787;
    , Caption='\<Parcelle';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_89.Click()
      with this.Parent.oContained
        GSPR_BAT(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_89.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (IsAlt())
      endwith
    endif
  endfunc

  func oBtn_1_89.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (!IsAlt() OR EMPTY(.w_ANCODICE) OR .cFunction='Load')
     endwith
    endif
  endfunc


  add object oObj_1_92 as cp_runprogram with uid="DXWKEYUCCR",left=446, top=632, width=201,height=24,;
    caption='GSCE_BGS(V)',;
   bGlobalFont=.t.,;
    prg="GSCE_BGS('V')",;
    cEvent = "CliCesEliminato",;
    nPag=1;
    , HelpContextID = 239929031


  add object oObj_1_93 as cp_runprogram with uid="UVTBWTYAVT",left=-1, top=656, width=179,height=24,;
    caption='GSAR_BAU(P)',;
   bGlobalFont=.t.,;
    prg="GSAR_BAU('P')",;
    cEvent = "w_ANPARIVA Changed",;
    nPag=1;
    , HelpContextID = 239086789


  add object oBtn_1_94 as StdButton with uid="PZKLYNNGQZ",left=461, top=235, width=22,height=22,;
    CpPicture="BMP\ROOT.bmp", caption="", nPag=1;
    , ToolTipText = "Apre il browser all'indirizzo web specificato";
    , HelpContextID = 159283046;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_94.Click()
      with this.Parent.oContained
        viewFile(.w_anindweb, .T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_94.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_ANINDWEB))
      endwith
    endif
  endfunc

  func oBtn_1_94.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_ANINDWEB))
     endwith
    endif
  endfunc


  add object oBtn_1_95 as StdButton with uid="XEUWBAPBCE",left=461, top=261, width=22,height=22,;
    CpPicture="BMP\btsend.bmp", caption="", nPag=1;
    , ToolTipText = "Manda una e-mail all'indirizzo specificato";
    , HelpContextID = 10881606;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_95.Click()
      with this.Parent.oContained
        MAILTO(.w_AN_EMAIL)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_95.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_AN_EMAIL))
      endwith
    endif
  endfunc

  func oBtn_1_95.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_AN_EMAIL))
     endwith
    endif
  endfunc


  add object oBtn_1_101 as StdButton with uid="YXOXMAGUJB",left=694, top=55, width=48,height=45,;
    CpPicture="bmp\ImportCF.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare i dati anagrafici dal codice fiscale";
    , HelpContextID = 183698740;
    , tabstop=.f., caption='Cod.\<Fisc';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_101.Click()
      with this.Parent.oContained
        iif(AH_yesno("Si vuole aggiornare sesso, luogo, provincia e data di nascita in base al codice fiscale?"), .notifyevent("AggiornaCF"),'')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_101.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_ANCODFIS)  AND (UPPER (.w_CODISO)='IT' OR  EMPTY (.w_CODISO)))
      endwith
    endif
  endfunc

  func oBtn_1_101.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_1_104 as StdButton with uid="GZBWLHZOEO",left=720, top=187, width=22,height=22,;
    CpPicture="BMP\phone.bmp", caption="", nPag=1;
    , ToolTipText = "Permette di effettuare chiamate al numero indicato";
    , HelpContextID = 4098854;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_104.Click()
      with this.Parent.oContained
        GSUT_BSK(this.Parent.oContained,.w_ANTELEFO, "MN", LookTab("OFF_NOMI", "NOCODICE", "NOCODCLI", .w_ANCODICE))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_104.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_ANTELEFO) And (g_SkypeService='C' Or !Empty(g_PhoneService)))
      endwith
    endif
  endfunc

  func oBtn_1_104.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_ANTELEFO) Or (g_SkypeService<>'C' And Empty(g_PhoneService)))
     endwith
    endif
  endfunc


  add object oBtn_1_105 as StdButton with uid="OUBHQIRZJT",left=720, top=260, width=22,height=22,;
    CpPicture="BMP\SKYPE.bmp", caption="", nPag=1;
    , ToolTipText = "Permette di effettuare chiamate via Skype al numero indicato";
    , HelpContextID = 4098854;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_105.Click()
      with this.Parent.oContained
        GSUT_BSK(this.Parent.oContained,.w_AN_SKYPE, "CC")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_105.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_AN_SKYPE) AND g_SkypeService<>'N')
      endwith
    endif
  endfunc

  func oBtn_1_105.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_AN_SKYPE) OR g_SkypeService='N')
     endwith
    endif
  endfunc


  add object oBtn_1_106 as StdButton with uid="CDFKSGVPAW",left=720, top=235, width=22,height=22,;
    CpPicture="BMP\Phone.bmp", caption="", nPag=1;
    , ToolTipText = "Permette di effettuare chiamate al numero indicato";
    , HelpContextID = 4098854;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_106.Click()
      with this.Parent.oContained
        GSUT_BSK(this.Parent.oContained,.w_ANNUMCEL, "MM", LookTab("OFF_NOMI", "NOCODICE", "NOCODCLI", .w_ANCODICE))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_106.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_ANNUMCEL) And (g_SkypeService='C' Or !Empty(g_PhoneService)))
      endwith
    endif
  endfunc

  func oBtn_1_106.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_ANNUMCEL) Or (g_SkypeService<>'C' And Empty(g_PhoneService)))
     endwith
    endif
  endfunc


  add object oBtn_1_115 as StdButton with uid="EBTWQHKHVR",left=643, top=102, width=48,height=45,;
    CpPicture="BMP\MASTER.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai dati aggiuntivi";
    , HelpContextID = 260003417;
    , Caption='\<Altri dati';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_115.Click()
      do gsar1kad with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_122 as StdButton with uid="IIGKUGLKHF",left=461, top=287, width=22,height=22,;
    CpPicture="BMP\fxmail_pec.bmp", caption="", nPag=1;
    , ToolTipText = "Manda una e-mail PEC all'indirizzo specificato";
    , HelpContextID = 159056630;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_122.Click()
      with this.Parent.oContained
        MAILTO(.w_AN_EMPEC,,,.T.)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_122.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (!empty(.w_AN_EMPEC))
      endwith
    endif
  endfunc

  func oBtn_1_122.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_AN_EMPEC))
     endwith
    endif
  endfunc

  add object oStr_1_36 as StdString with uid="PKMJSMVRIZ",Visible=.t., Left=6, Top=8,;
    Alignment=1, Width=122, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_38 as StdString with uid="CGQYEBLKPT",Visible=.t., Left=4, Top=62,;
    Alignment=1, Width=122, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="ALXKPMSZSJ",Visible=.t., Left=451, Top=187,;
    Alignment=1, Width=116, Height=15,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="VZCBHIXECX",Visible=.t., Left=451, Top=211,;
    Alignment=1, Width=116, Height=15,;
    Caption="Telefax:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="FWIDXVFDQF",Visible=.t., Left=4, Top=184,;
    Alignment=1, Width=122, Height=15,;
    Caption="Cod. fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="SBHJPCQHBJ",Visible=.t., Left=4, Top=209,;
    Alignment=1, Width=122, Height=15,;
    Caption="Partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="QLNHMTGLDP",Visible=.t., Left=4, Top=159,;
    Alignment=1, Width=122, Height=15,;
    Caption="Nazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="FVDDBDTASV",Visible=.t., Left=148, Top=370,;
    Alignment=1, Width=85, Height=15,;
    Caption="Nato a:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="TCJYQAWCWX",Visible=.t., Left=626, Top=371,;
    Alignment=1, Width=19, Height=15,;
    Caption="Il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="VPPPROQFVY",Visible=.t., Left=505, Top=235,;
    Alignment=1, Width=62, Height=15,;
    Caption="Cellulare:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="VAFJCXEMWE",Visible=.t., Left=4, Top=235,;
    Alignment=1, Width=122, Height=15,;
    Caption="Internet web:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="CVUWNUUQJG",Visible=.t., Left=4, Top=261,;
    Alignment=1, Width=122, Height=15,;
    Caption="E@mail addr.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="ODQXIZHDKU",Visible=.t., Left=4, Top=110,;
    Alignment=1, Width=122, Height=15,;
    Caption="CAP - localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="AQIIZHEBZI",Visible=.t., Left=83, Top=134,;
    Alignment=1, Width=43, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="UVKHTKBGEO",Visible=.t., Left=619, Top=159,;
    Alignment=1, Width=80, Height=15,;
    Caption="Cod.ISO:"  ;
  , bGlobalFont=.t.

  add object oStr_1_52 as StdString with uid="FOEHSUCJAQ",Visible=.t., Left=487, Top=371,;
    Alignment=1, Width=60, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_70 as StdString with uid="TVSOCPDHGW",Visible=.t., Left=294, Top=533,;
    Alignment=1, Width=92, Height=15,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_70.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_71 as StdString with uid="TOBEEPRVGN",Visible=.t., Left=518, Top=533,;
    Alignment=1, Width=123, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_81 as StdString with uid="NXZSESAJZM",Visible=.t., Left=148, Top=345,;
    Alignment=1, Width=85, Height=15,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_82 as StdString with uid="SPJAMZFENN",Visible=.t., Left=487, Top=345,;
    Alignment=1, Width=60, Height=15,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_1_90 as StdString with uid="RCGOPLUALE",Visible=.t., Left=4, Top=407,;
    Alignment=0, Width=147, Height=18,;
    Caption="Processi documentali"  ;
  , bGlobalFont=.t.

  func oStr_1_90.mHide()
    with this.Parent.oContained
      return (g_DOCM<>'S' OR IsAlt())
    endwith
  endfunc

  add object oStr_1_91 as StdString with uid="VWBJMPRGBK",Visible=.t., Left=420, Top=396,;
    Alignment=1, Width=127, Height=15,;
    Caption="Carta di identit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_96 as StdString with uid="CCBZPVWHAA",Visible=.t., Left=4, Top=314,;
    Alignment=1, Width=122, Height=18,;
    Caption="Formula di cortesia:"  ;
  , bGlobalFont=.t.

  func oStr_1_96.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oStr_1_103 as StdString with uid="ACUELWKBTT",Visible=.t., Left=505, Top=262,;
    Alignment=1, Width=62, Height=18,;
    Caption="Skype:"  ;
  , bGlobalFont=.t.

  add object oStr_1_121 as StdString with uid="UCJXPBXRTJ",Visible=.t., Left=4, Top=287,;
    Alignment=1, Width=122, Height=18,;
    Caption="PEC addr.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_123 as StdString with uid="WEDQLDHQOR",Visible=.t., Left=14, Top=478,;
    Alignment=1, Width=112, Height=18,;
    Caption="Codice destinatario:"  ;
  , bGlobalFont=.t.

  func oStr_1_123.mHide()
    with this.Parent.oContained
      return (.w_ANFLESIG='S')
    endwith
  endfunc

  add object oStr_1_124 as StdString with uid="BRQAAFGCCM",Visible=.t., Left=377, Top=480,;
    Alignment=1, Width=170, Height=18,;
    Caption="Classificazione FE:"  ;
  , bGlobalFont=.t.

  add object oStr_1_125 as StdString with uid="FPZKWYQMLG",Visible=.t., Left=9, Top=505,;
    Alignment=1, Width=117, Height=18,;
    Caption="PEC destinatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_127 as StdString with uid="XQRKIKRCPA",Visible=.t., Left=4, Top=456,;
    Alignment=0, Width=214, Height=18,;
    Caption="Informazioni per fatturazione elettronica"  ;
  , bGlobalFont=.t.

  add object oStr_1_128 as StdString with uid="OPNQOEDEYA",Visible=.t., Left=58, Top=478,;
    Alignment=1, Width=68, Height=18,;
    Caption="Codice IPA:"  ;
  , bGlobalFont=.t.

  func oStr_1_128.mHide()
    with this.Parent.oContained
      return (.w_ANFLESIG<>'S')
    endwith
  endfunc

  add object oBox_1_69 as StdBox with uid="ZRHVDXOOCV",left=1, top=340, width=726,height=1

  add object oBox_1_88 as StdBox with uid="LEBVCACMJU",left=1, top=426, width=744,height=1

  add object oBox_1_126 as StdBox with uid="WGFWMHQFGI",left=1, top=471, width=744,height=1
enddefine
define class tgsar_aclPag2 as StdContainer
  Width  = 769
  height = 554
  stdWidth  = 769
  stdheight = 554
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_2_3 as StdField with uid="JNCHVLOARA",rtseq=61,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 163847206,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=172, Top=8, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15)

  add object oRAG1_2_4 as StdField with uid="OKAVJIWEIO",rtseq=62,rtrep=.f.,;
    cFormVar = "w_RAG1", cQueryName = "RAG1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 162283286,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=314, Top=8, InputMask=replicate('X',40)

  add object oANCONSUP_2_7 as StdField with uid="EJFNOOMMNQ",rtseq=64,rtrep=.f.,;
    cFormVar = "w_ANCONSUP", cQueryName = "ANCONSUP",;
    bObbl = .t. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Mastro di raggruppamento del cliente",;
    HelpContextID = 240514218,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=172, Top=38, cSayPict="p_MAS", cGetPict="p_MAS", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MASTRI", cZoomOnZoom="GSAR_AMC", oKey_1_1="MCCODICE", oKey_1_2="this.w_ANCONSUP"

  func oANCONSUP_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCONSUP_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCONSUP_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MASTRI','*','MCCODICE',cp_AbsName(this.parent,'oANCONSUP_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMC',"Mastri contabili (clienti)",'GSAR_ACL.MASTRI_VZM',this.parent.oContained
  endproc
  proc oANCONSUP_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MCCODICE=this.parent.oContained.w_ANCONSUP
     i_obj.ecpSave()
  endproc

  add object oDESSUP_2_8 as StdField with uid="RWZBGOCFMA",rtseq=65,rtrep=.f.,;
    cFormVar = "w_DESSUP", cQueryName = "DESSUP",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 253690422,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=314, Top=38, InputMask=replicate('X',40)

  add object oANCATCON_2_9 as StdField with uid="LCBEADBASX",rtseq=66,rtrep=.f.,;
    cFormVar = "w_ANCATCON", cQueryName = "ANCATCON",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria contabile del cliente",;
    HelpContextID = 33295188,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=172, Top=68, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CACOCLFO", cZoomOnZoom="GSAR_AC2", oKey_1_1="C2CODICE", oKey_1_2="this.w_ANCATCON"

  func oANCATCON_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCATCON_2_9.ecpDrop(oSource)
    this.Parent.oContained.link_2_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCATCON_2_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CACOCLFO','*','C2CODICE',cp_AbsName(this.parent,'oANCATCON_2_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AC2',"Categorie contabili clienti",'',this.parent.oContained
  endproc
  proc oANCATCON_2_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AC2()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_C2CODICE=this.parent.oContained.w_ANCATCON
     i_obj.ecpSave()
  endproc

  add object oDESCON_2_10 as StdField with uid="JAMKNLZVQH",rtseq=67,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 212795958,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=236, Top=68, InputMask=replicate('X',35)

  add object oANCONRIF_2_14 as StdField with uid="ZPKXOLHAQA",rtseq=70,rtrep=.f.,;
    cFormVar = "w_ANCONRIF", cQueryName = "ANCONRIF",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice conto inesistente oppure obsoleto",;
    ToolTipText = "Codice originario fornitore corrispondente",;
    HelpContextID = 11144012,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=172, Top=98, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPRIF", oKey_2_1="ANCODICE", oKey_2_2="this.w_ANCONRIF"

  func oANCONRIF_2_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCONRIF_2_14.ecpDrop(oSource)
    this.Parent.oContained.link_2_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCONRIF_2_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPRIF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPRIF)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oANCONRIF_2_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Fornitore originario",'',this.parent.oContained
  endproc
  proc oANCONRIF_2_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPRIF
     i_obj.w_ANCODICE=this.parent.oContained.w_ANCONRIF
     i_obj.ecpSave()
  endproc

  add object oDESORI_2_15 as StdField with uid="NFYTSNTUVI",rtseq=71,rtrep=.f.,;
    cFormVar = "w_DESORI", cQueryName = "DESORI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 132842038,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=314, Top=98, InputMask=replicate('X',40)

  add object oANCODIVA_2_17 as StdField with uid="BZLGAESVTK",rtseq=73,rtrep=.f.,;
    cFormVar = "w_ANCODIVA", cQueryName = "ANCODIVA",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice IVA inesistente oppure obsoleto",;
    ToolTipText = "Eventuale codice IVA",;
    HelpContextID = 150336697,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=172, Top=128, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VOCIIVA", cZoomOnZoom="GSAR_AIV", oKey_1_1="IVCODIVA", oKey_1_2="this.w_ANCODIVA"

  func oANCODIVA_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODIVA_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODIVA_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOCIIVA','*','IVCODIVA',cp_AbsName(this.parent,'oANCODIVA_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AIV',"Codici IVA",'GSVE0MDV.VOCIIVA_VZM',this.parent.oContained
  endproc
  proc oANCODIVA_2_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AIV()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_IVCODIVA=this.parent.oContained.w_ANCODIVA
     i_obj.ecpSave()
  endproc

  add object oDESIVA_2_19 as StdField with uid="BTHUQDIOFB",rtseq=75,rtrep=.f.,;
    cFormVar = "w_DESIVA", cQueryName = "DESIVA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 2425398,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=236, Top=128, InputMask=replicate('X',35)

  add object oANTIPOPE_2_20 as StdField with uid="ATGWNZUHAX",rtseq=76,rtrep=.f.,;
    cFormVar = "w_ANTIPOPE", cQueryName = "ANTIPOPE",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Codice incongruente oppure obsoleto",;
    HelpContextID = 231021387,;
   bGlobalFont=.t.,;
    Height=21, Width=104, Left=172, Top=158, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="TIPCODIV", cZoomOnZoom="GSAR_ATO", oKey_1_1="TI__TIPO", oKey_1_2="this.w_ANCATOPE", oKey_2_1="TICODICE", oKey_2_2="this.w_ANTIPOPE"

  func oANTIPOPE_2_20.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_20('Part',this)
    endwith
    return bRes
  endfunc

  proc oANTIPOPE_2_20.ecpDrop(oSource)
    this.Parent.oContained.link_2_20('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANTIPOPE_2_20.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.TIPCODIV_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStrODBC(this.Parent.oContained.w_ANCATOPE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TI__TIPO="+cp_ToStr(this.Parent.oContained.w_ANCATOPE)
    endif
    do cp_zoom with 'TIPCODIV','*','TI__TIPO,TICODICE',cp_AbsName(this.parent,'oANTIPOPE_2_20'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATO',"Operazioni IVA",'GSVE_ZTI.TIPCODIV_VZM',this.parent.oContained
  endproc
  proc oANTIPOPE_2_20.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TI__TIPO=w_ANCATOPE
     i_obj.w_TICODICE=this.parent.oContained.w_ANTIPOPE
     i_obj.ecpSave()
  endproc

  add object oANCONCAU_2_21 as StdField with uid="DYUYXOSUGA",rtseq=77,rtrep=.f.,;
    cFormVar = "w_ANCONCAU", cQueryName = "ANCONCAU",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Contropartita per cauzioni imballi",;
    HelpContextID = 240514213,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=172, Top=188, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCC", oKey_2_1="ANCODICE", oKey_2_2="this.w_ANCONCAU"

  func oANCONCAU_2_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_VEFA='S' And .w_ANFLGCAU='S')
    endwith
   endif
  endfunc

  func oANCONCAU_2_21.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S' Or .w_ANFLGCAU<>'S')
    endwith
  endfunc

  func oANCONCAU_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCONCAU_2_21.ecpDrop(oSource)
    this.Parent.oContained.link_2_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCONCAU_2_21.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCC)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCC)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oANCONCAU_2_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"",'',this.parent.oContained
  endproc
  proc oANCONCAU_2_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCC
     i_obj.w_ANCODICE=this.parent.oContained.w_ANCONCAU
     i_obj.ecpSave()
  endproc

  add object oTIDESCRI_2_22 as StdField with uid="TNOOFYXTGO",rtseq=78,rtrep=.f.,;
    cFormVar = "w_TIDESCRI", cQueryName = "TIDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 235923585,;
   bGlobalFont=.t.,;
    Height=21, Width=323, Left=280, Top=158, InputMask=replicate('X',30)

  add object oANFLRITE_2_23 as StdCheck with uid="ZVLCZQNFBZ",rtseq=79,rtrep=.f.,left=15, top=240, caption="IRPEF/IRES",;
    ToolTipText = "Se attivo, il cliente gestisce le ritenute attive. Pu� valere solo IRPEF/IRES.",;
    HelpContextID = 135840949,;
    cFormVar="w_ANFLRITE", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oANFLRITE_2_23.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLRITE_2_23.GetRadio()
    this.Parent.oContained.w_ANFLRITE = this.RadioValue()
    return .t.
  endfunc

  func oANFLRITE_2_23.SetRadio()
    this.Parent.oContained.w_ANFLRITE=trim(this.Parent.oContained.w_ANFLRITE)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLRITE=='S',1,;
      0)
  endfunc

  func oANFLRITE_2_23.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_RITE='S' OR IsAlt())
    endwith
   endif
  endfunc

  add object oANCODIRP_2_24 as StdField with uid="LRDFFOSDTV",rtseq=80,rtrep=.f.,;
    cFormVar = "w_ANCODIRP", cQueryName = "ANCODIRP",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Inserire un codice tributo I.R.PE.F.",;
    ToolTipText = "Codice Tributo I.R.PE.F. di default",;
    HelpContextID = 150336682,;
   bGlobalFont=.t.,;
    Height=21, Width=67, Left=297, Top=238, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_ANCODIRP"

  func oANCODIRP_2_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLRITE='S' AND (g_RITE='S' OR IsAlt()))
    endwith
   endif
  endfunc

  func oANCODIRP_2_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODIRP_2_24.ecpDrop(oSource)
    this.Parent.oContained.link_2_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODIRP_2_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oANCODIRP_2_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"Elenco tributi",'GSRI1AMR.TRI_BUTI_VZM',this.parent.oContained
  endproc
  proc oANCODIRP_2_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_ANCODIRP
     i_obj.ecpSave()
  endproc


  add object oANCAURIT_2_27 as StdCombo with uid="WSDCCQWIEQ",rtseq=83,rtrep=.f.,left=297,top=264,width=76,height=21;
    , ToolTipText = "Valore predefinito causale prestazione";
    , HelpContextID = 17566554;
    , cFormVar="w_ANCAURIT",RowSource=""+"Tipo A,"+"Tipo B,"+"Tipo C,"+"Tipo D,"+"Tipo E,"+"Tipo F,"+"Tipo G,"+"Tipo H,"+"Tipo I,"+"Tipo J,"+"Tipo K,"+"Tipo L,"+"Tipo L1,"+"Tipo M,"+"Tipo M1,"+"Tipo M2,"+"Tipo N,"+"Tipo O,"+"Tipo O1,"+"Tipo P,"+"Tipo Q,"+"Tipo R,"+"Tipo S,"+"Tipo T,"+"Tipo U,"+"Tipo V,"+"Tipo V1,"+"Tipo V2,"+"Tipo W,"+"Tipo X,"+"Tipo Y,"+"Tipo Z,"+"Tipo ZO", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oANCAURIT_2_27.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'B',;
    iif(this.value =3,'C',;
    iif(this.value =4,'D',;
    iif(this.value =5,'E',;
    iif(this.value =6,'F',;
    iif(this.value =7,'G',;
    iif(this.value =8,'H',;
    iif(this.value =9,'I',;
    iif(this.value =10,'J',;
    iif(this.value =11,'K',;
    iif(this.value =12,'L',;
    iif(this.value =13,'L1',;
    iif(this.value =14,'M',;
    iif(this.value =15,'M1',;
    iif(this.value =16,'M2',;
    iif(this.value =17,'N',;
    iif(this.value =18,'O',;
    iif(this.value =19,'O1',;
    iif(this.value =20,'P',;
    iif(this.value =21,'Q',;
    iif(this.value =22,'R',;
    iif(this.value =23,'S',;
    iif(this.value =24,'T',;
    iif(this.value =25,'U',;
    iif(this.value =26,'V',;
    iif(this.value =27,'V1',;
    iif(this.value =28,'V2',;
    iif(this.value =29,'W',;
    iif(this.value =30,'X',;
    iif(this.value =31,'Y',;
    iif(this.value =32,'Z',;
    iif(this.value =33,'ZO',;
    space(2)))))))))))))))))))))))))))))))))))
  endfunc
  func oANCAURIT_2_27.GetRadio()
    this.Parent.oContained.w_ANCAURIT = this.RadioValue()
    return .t.
  endfunc

  func oANCAURIT_2_27.SetRadio()
    this.Parent.oContained.w_ANCAURIT=trim(this.Parent.oContained.w_ANCAURIT)
    this.value = ;
      iif(this.Parent.oContained.w_ANCAURIT=='A',1,;
      iif(this.Parent.oContained.w_ANCAURIT=='B',2,;
      iif(this.Parent.oContained.w_ANCAURIT=='C',3,;
      iif(this.Parent.oContained.w_ANCAURIT=='D',4,;
      iif(this.Parent.oContained.w_ANCAURIT=='E',5,;
      iif(this.Parent.oContained.w_ANCAURIT=='F',6,;
      iif(this.Parent.oContained.w_ANCAURIT=='G',7,;
      iif(this.Parent.oContained.w_ANCAURIT=='H',8,;
      iif(this.Parent.oContained.w_ANCAURIT=='I',9,;
      iif(this.Parent.oContained.w_ANCAURIT=='J',10,;
      iif(this.Parent.oContained.w_ANCAURIT=='K',11,;
      iif(this.Parent.oContained.w_ANCAURIT=='L',12,;
      iif(this.Parent.oContained.w_ANCAURIT=='L1',13,;
      iif(this.Parent.oContained.w_ANCAURIT=='M',14,;
      iif(this.Parent.oContained.w_ANCAURIT=='M1',15,;
      iif(this.Parent.oContained.w_ANCAURIT=='M2',16,;
      iif(this.Parent.oContained.w_ANCAURIT=='N',17,;
      iif(this.Parent.oContained.w_ANCAURIT=='O',18,;
      iif(this.Parent.oContained.w_ANCAURIT=='O1',19,;
      iif(this.Parent.oContained.w_ANCAURIT=='P',20,;
      iif(this.Parent.oContained.w_ANCAURIT=='Q',21,;
      iif(this.Parent.oContained.w_ANCAURIT=='R',22,;
      iif(this.Parent.oContained.w_ANCAURIT=='S',23,;
      iif(this.Parent.oContained.w_ANCAURIT=='T',24,;
      iif(this.Parent.oContained.w_ANCAURIT=='U',25,;
      iif(this.Parent.oContained.w_ANCAURIT=='V',26,;
      iif(this.Parent.oContained.w_ANCAURIT=='V1',27,;
      iif(this.Parent.oContained.w_ANCAURIT=='V2',28,;
      iif(this.Parent.oContained.w_ANCAURIT=='W',29,;
      iif(this.Parent.oContained.w_ANCAURIT=='X',30,;
      iif(this.Parent.oContained.w_ANCAURIT=='Y',31,;
      iif(this.Parent.oContained.w_ANCAURIT=='Z',32,;
      iif(this.Parent.oContained.w_ANCAURIT=='ZO',33,;
      0)))))))))))))))))))))))))))))))))
  endfunc

  func oANCAURIT_2_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLRITE='S' AND (g_RITE='S' OR IsAlt()))
    endwith
   endif
  endfunc


  add object oANOPETRE_2_36 as StdCombo with uid="RDWLAZXTUW",rtseq=87,rtrep=.f.,left=172,top=312,width=169,height=21;
    , ToolTipText = "Classificazione soggetto";
    , HelpContextID = 233059509;
    , cFormVar="w_ANOPETRE",RowSource=""+"Escludi,"+"Corrispettivi periodici,"+"Contratti collegati,"+"Non definibile", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oANOPETRE_2_36.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'P',;
    iif(this.value =3,'C',;
    iif(this.value =4,'N',;
    space(1))))))
  endfunc
  func oANOPETRE_2_36.GetRadio()
    this.Parent.oContained.w_ANOPETRE = this.RadioValue()
    return .t.
  endfunc

  func oANOPETRE_2_36.SetRadio()
    this.Parent.oContained.w_ANOPETRE=trim(this.Parent.oContained.w_ANOPETRE)
    this.value = ;
      iif(this.Parent.oContained.w_ANOPETRE=='E',1,;
      iif(this.Parent.oContained.w_ANOPETRE=='P',2,;
      iif(this.Parent.oContained.w_ANOPETRE=='C',3,;
      iif(this.Parent.oContained.w_ANOPETRE=='N',4,;
      0))))
  endfunc


  add object oANTIPPRE_2_38 as StdCombo with uid="OKEXLKPYZF",rtseq=88,rtrep=.f.,left=514,top=312,width=169,height=21;
    , ToolTipText = "Classificazione operazioni tra beni e servizi";
    , HelpContextID = 20636853;
    , cFormVar="w_ANTIPPRE",RowSource=""+"Non definibile,"+"Beni,"+"Servizi", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oANTIPPRE_2_38.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'B',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oANTIPPRE_2_38.GetRadio()
    this.Parent.oContained.w_ANTIPPRE = this.RadioValue()
    return .t.
  endfunc

  func oANTIPPRE_2_38.SetRadio()
    this.Parent.oContained.w_ANTIPPRE=trim(this.Parent.oContained.w_ANTIPPRE)
    this.value = ;
      iif(this.Parent.oContained.w_ANTIPPRE=='N',1,;
      iif(this.Parent.oContained.w_ANTIPPRE=='B',2,;
      iif(this.Parent.oContained.w_ANTIPPRE=='S',3,;
      0)))
  endfunc

  add object oANFLAACC_2_40 as StdCheck with uid="PEYPAFKENF",rtseq=89,rtrep=.f.,left=12, top=366, caption="Accorpa acconti",;
    ToolTipText = "Se attivo in fase di contabilizzazione documenti gli acconti gi� registrati verranno accorpati alla partita della fattura",;
    HelpContextID = 19449015,;
    cFormVar="w_ANFLAACC", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oANFLAACC_2_40.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLAACC_2_40.GetRadio()
    this.Parent.oContained.w_ANFLAACC = this.RadioValue()
    return .t.
  endfunc

  func oANFLAACC_2_40.SetRadio()
    this.Parent.oContained.w_ANFLAACC=trim(this.Parent.oContained.w_ANFLAACC)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLAACC=='S',1,;
      0)
  endfunc

  add object oANFLESIG_2_41 as StdCheck with uid="VZDBPATZSS",rtseq=90,rtrep=.f.,left=12, top=392, caption="Soggetto PA",;
    ToolTipText = "Se attivo: stampa su fatture esigibilit� (differita o immediata)",;
    HelpContextID = 18299725,;
    cFormVar="w_ANFLESIG", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Test soggetto pubblico non definito";
   , bGlobalFont=.t.


  func oANFLESIG_2_41.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLESIG_2_41.GetRadio()
    this.Parent.oContained.w_ANFLESIG = this.RadioValue()
    return .t.
  endfunc

  func oANFLESIG_2_41.SetRadio()
    this.Parent.oContained.w_ANFLESIG=trim(this.Parent.oContained.w_ANFLESIG)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLESIG=='S',1,;
      0)
  endfunc

  func oANFLESIG_2_41.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERPAR='S')
    endwith
   endif
  endfunc

  func oANFLESIG_2_41.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ANFLESIG $ 'SN')
    endwith
    return bRes
  endfunc

  add object oAFFLINTR_2_42 as StdCheck with uid="GSAYXUSTXF",rtseq=91,rtrep=.f.,left=12, top=418, caption="Cliente INTRA",;
    ToolTipText = "Se attivo: il cliente � un soggetto INTRA",;
    HelpContextID = 61394088,;
    cFormVar="w_AFFLINTR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAFFLINTR_2_42.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAFFLINTR_2_42.GetRadio()
    this.Parent.oContained.w_AFFLINTR = this.RadioValue()
    return .t.
  endfunc

  func oAFFLINTR_2_42.SetRadio()
    this.Parent.oContained.w_AFFLINTR=trim(this.Parent.oContained.w_AFFLINTR)
    this.value = ;
      iif(this.Parent.oContained.w_AFFLINTR=='S',1,;
      0)
  endfunc

  add object oANIVASOS_2_43 as StdCheck with uid="UXSDBOJDQM",rtseq=92,rtrep=.f.,left=12, top=444, caption="Maturazione temporale IVA in sospensione",;
    ToolTipText = "Maturazione temporale IVA in sospensione",;
    HelpContextID = 14773081,;
    cFormVar="w_ANIVASOS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oANIVASOS_2_43.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANIVASOS_2_43.GetRadio()
    this.Parent.oContained.w_ANIVASOS = this.RadioValue()
    return .t.
  endfunc

  func oANIVASOS_2_43.SetRadio()
    this.Parent.oContained.w_ANIVASOS=trim(this.Parent.oContained.w_ANIVASOS)
    this.value = ;
      iif(this.Parent.oContained.w_ANIVASOS=='S',1,;
      0)
  endfunc

  func oANIVASOS_2_43.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLESIG='N')
    endwith
   endif
  endfunc

  add object oANFLBLLS_2_44 as StdCheck with uid="WEEOKQPGPB",rtseq=93,rtrep=.f.,left=12, top=470, caption="Fiscalit� privilegiata",;
    ToolTipText = "Se attivato, il cliente � identificato come operatore economico con sede, residenza o domicilio negli Stati o territori a regime privilegiato",;
    HelpContextID = 166148953,;
    cFormVar="w_ANFLBLLS", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oANFLBLLS_2_44.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLBLLS_2_44.GetRadio()
    this.Parent.oContained.w_ANFLBLLS = this.RadioValue()
    return .t.
  endfunc

  func oANFLBLLS_2_44.SetRadio()
    this.Parent.oContained.w_ANFLBLLS=trim(this.Parent.oContained.w_ANFLBLLS)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLBLLS=='S',1,;
      0)
  endfunc

  add object oANCOFISC_2_45 as StdField with uid="HKPLVKXGXR",rtseq=94,rtrep=.f.,;
    cFormVar = "w_ANCOFISC", cQueryName = "ANCOFISC",;
    bObbl = .f. , nPag = 2, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Codice identificativo fiscale",;
    HelpContextID = 148239543,;
   bGlobalFont=.t.,;
    Height=21, Width=202, Left=360, Top=468, InputMask=replicate('X',25)

  func oANCOFISC_2_45.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLBLLS='S' )
    endwith
   endif
  endfunc

  add object oANSCIPAG_2_46 as StdCheck with uid="VUKBVHGHFD",rtseq=95,rtrep=.f.,left=12, top=496, caption="Soggetto a scissione pagamenti",;
    ToolTipText = "Pubblica amministrazione soggetta a c.d. scissione pagamenti",;
    HelpContextID = 28374195,;
    cFormVar="w_ANSCIPAG", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oANSCIPAG_2_46.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oANSCIPAG_2_46.GetRadio()
    this.Parent.oContained.w_ANSCIPAG = this.RadioValue()
    return .t.
  endfunc

  func oANSCIPAG_2_46.SetRadio()
    this.Parent.oContained.w_ANSCIPAG=trim(this.Parent.oContained.w_ANSCIPAG)
    this.value = ;
      iif(this.Parent.oContained.w_ANSCIPAG=='S',1,;
      0)
  endfunc

  add object oANFLSOAL_2_47 as StdCheck with uid="IRHRPVDJDH",rtseq=96,rtrep=.f.,left=12, top=522, caption="Soggetto terzo",;
    ToolTipText = "Se attivato, sar� possibile indicare l'intestatario effettivo della blacklist in primanota",;
    HelpContextID = 34129070,;
    cFormVar="w_ANFLSOAL", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oANFLSOAL_2_47.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLSOAL_2_47.GetRadio()
    this.Parent.oContained.w_ANFLSOAL = this.RadioValue()
    return .t.
  endfunc

  func oANFLSOAL_2_47.SetRadio()
    this.Parent.oContained.w_ANFLSOAL=trim(this.Parent.oContained.w_ANFLSOAL)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLSOAL=='S',1,;
      0)
  endfunc

  func oANFLSOAL_2_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLBLLS='N')
    endwith
   endif
  endfunc

  add object oANPARTSN_2_48 as StdCheck with uid="TAMKVJGKPV",rtseq=97,rtrep=.f.,left=173, top=366, caption="Gestione partite",;
    ToolTipText = "Se attivo: il cliente � gestito nelle partite",;
    HelpContextID = 220406956,;
    cFormVar="w_ANPARTSN", bObbl = .f. , nPag = 2;
    ,sErrorMsg = "Test partite incongruente con test esigibilit�";
   , bGlobalFont=.t.


  func oANPARTSN_2_48.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oANPARTSN_2_48.GetRadio()
    this.Parent.oContained.w_ANPARTSN = this.RadioValue()
    return .t.
  endfunc

  func oANPARTSN_2_48.SetRadio()
    this.Parent.oContained.w_ANPARTSN=trim(this.Parent.oContained.w_ANPARTSN)
    this.value = ;
      iif(this.Parent.oContained.w_ANPARTSN=='S',1,;
      0)
  endfunc

  func oANPARTSN_2_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERPAR='S')
    endwith
   endif
  endfunc

  func oANPARTSN_2_48.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ANFLESIG<>'S' OR .w_ANPARTSN='S')
    endwith
    return bRes
  endfunc

  add object oANFLFIDO_2_49 as StdCheck with uid="XGIAHWICNF",rtseq=98,rtrep=.f.,left=173, top=392, caption="Controllo fido",;
    ToolTipText = "Se attivo: abilita il controllo del fido all'emissione dei documenti",;
    HelpContextID = 148423851,;
    cFormVar="w_ANFLFIDO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oANFLFIDO_2_49.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oANFLFIDO_2_49.GetRadio()
    this.Parent.oContained.w_ANFLFIDO = this.RadioValue()
    return .t.
  endfunc

  func oANFLFIDO_2_49.SetRadio()
    this.Parent.oContained.w_ANFLFIDO=trim(this.Parent.oContained.w_ANFLFIDO)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLFIDO=='S',1,;
      0)
  endfunc

  func oANFLFIDO_2_49.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((NOT IsAlt() AND g_PERFID='S') OR (IsAlt() AND g_COGE='S' AND g_PERFID='S'))
    endwith
   endif
  endfunc

  func oANFLFIDO_2_49.mHide()
    with this.Parent.oContained
      return (IsAlt() AND (g_COGE<>'S' OR !g_PERFID='S'))
    endwith
  endfunc

  add object oANFLSGRE_2_50 as StdCheck with uid="USJFVQHHHP",rtseq=99,rtrep=.f.,left=173, top=418, caption="Soggetto non residente",;
    ToolTipText = "Se attivo: il soggetto � non residente",;
    HelpContextID = 168346805,;
    cFormVar="w_ANFLSGRE", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oANFLSGRE_2_50.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLSGRE_2_50.GetRadio()
    this.Parent.oContained.w_ANFLSGRE = this.RadioValue()
    return .t.
  endfunc

  func oANFLSGRE_2_50.SetRadio()
    this.Parent.oContained.w_ANFLSGRE=trim(this.Parent.oContained.w_ANFLSGRE)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLSGRE=='S',1,;
      0)
  endfunc

  add object oANFLBODO_2_51 as StdCheck with uid="VPSNNEGSZK",rtseq=100,rtrep=.f.,left=173, top=522, caption="Bolla doganale",;
    ToolTipText = "Se attivo, il cliente � soggetto a bolla doganale",;
    HelpContextID = 51954859,;
    cFormVar="w_ANFLBODO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oANFLBODO_2_51.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oANFLBODO_2_51.GetRadio()
    this.Parent.oContained.w_ANFLBODO = this.RadioValue()
    return .t.
  endfunc

  func oANFLBODO_2_51.SetRadio()
    this.Parent.oContained.w_ANFLBODO=trim(this.Parent.oContained.w_ANFLBODO)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLBODO=='S',1,;
      0)
  endfunc

  func oANFLBODO_2_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLSOAL # 'S' and g_TRAEXP # 'N' and .w_AFFLINTR#'S')
    endwith
   endif
  endfunc

  add object oANCODSTU_2_52 as StdField with uid="OPEHYXLOVF",rtseq=101,rtrep=.f.,;
    cFormVar = "w_ANCODSTU", cQueryName = "ANCODSTU",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice deve essere compreso negli intervalli del conto studio selezionato",;
    ToolTipText = "Codice associato al cliente nello studio",;
    HelpContextID = 250999973,;
   bGlobalFont=.t.,;
    Height=21, Width=75, Left=460, Top=366, cSayPict='"9999999999"', cGetPict='"9999999999"', InputMask=replicate('X',10)

  func oANCODSTU_2_52.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_LEMC='S' AND g_TRAEXP<>'N')
    endwith
   endif
  endfunc

  func oANCODSTU_2_52.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP='N')
    endwith
  endfunc

  func oANCODSTU_2_52.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (chkconstucf(.w_ancodstu,.w_codstu))
    endwith
    return bRes
  endfunc


  add object oBtn_2_53 as StdButton with uid="GAAUWJQKCM",left=540, top=366, width=22,height=22,;
    CpPicture="exe\bmp\auto.ico", caption="", nPag=2;
    , ToolTipText = "Ricalcola il sottoconto studio";
    , HelpContextID = 165719590;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_53.Click()
      this.parent.oContained.NotifyEvent("ForzaRicalcolaStudio")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_53.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_LEMC='S' AND g_TRAEXP<>'N' )
      endwith
    endif
  endfunc

  func oBtn_2_53.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP$'N-G' OR .cfunction<>'Load')
     endwith
    endif
  endfunc

  add object oANCODSOG_2_54 as StdField with uid="JJMFBIAWML",rtseq=102,rtrep=.f.,;
    cFormVar = "w_ANCODSOG", cQueryName = "ANCODSOG",;
    bObbl = .f. , nPag = 2, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "Codice soggetto (anagrafico unico)",;
    HelpContextID = 17435469,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=651, Top=366, cSayPict="repl('!',8)", cGetPict="repl('!',8)", InputMask=replicate('X',8)

  func oANCODSOG_2_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_LEMC='S' AND g_TRAEXP<>'N')
    endwith
   endif
  endfunc

  func oANCODSOG_2_54.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP<>'C')
    endwith
  endfunc

  add object oANCODCAT_2_55 as StdField with uid="DRDRMCQAXA",rtseq=103,rtrep=.f.,;
    cFormVar = "w_ANCODCAT", cQueryName = "ANCODCAT",;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Codice catastale del comune",;
    HelpContextID = 250999974,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=651, Top=392, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="COD_CATA", cZoomOnZoom="GSAR_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_ANCODCAT"

  func oANCODCAT_2_55.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_55('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODCAT_2_55.ecpDrop(oSource)
    this.Parent.oContained.link_2_55('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODCAT_2_55.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_CATA','*','CCCODICE',cp_AbsName(this.parent,'oANCODCAT_2_55'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACC',"Elenco codici catastali dei comuni",'',this.parent.oContained
  endproc
  proc oANCODCAT_2_55.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_ANCODCAT
     i_obj.ecpSave()
  endproc


  add object oBtn_2_57 as StdButton with uid="GBMHNPDTNI",left=571, top=457, width=48,height=45,;
    CpPicture="BMP\SCHEDE.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere alle schede contabili del cliente";
    , HelpContextID = 33678050;
    , Caption='\<Contabile';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_57.Click()
      with this.Parent.oContained
        GSAR_BKK(this.Parent.oContained,"A")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_57.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_2_57.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_COGE<>'S')
     endwith
    endif
  endfunc


  add object oBtn_2_59 as StdButton with uid="QPPQUXHZEB",left=623, top=457, width=48,height=45,;
    CpPicture="BMP\INTENTI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere alle dichiarazioni di intento";
    , HelpContextID = 29191813;
    , Caption='\<Dic.Intento';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_59.Click()
      do GSCG_KL2 with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_59.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND EMPTY(.w_ANCODIVA))
      endwith
    endif
  endfunc

  func oBtn_2_59.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_ANCODICE))
     endwith
    endif
  endfunc


  add object oBtn_2_61 as StdButton with uid="LMBSPCURIX",left=675, top=457, width=48,height=45,;
    CpPicture="BMP\SALDI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere ai saldi del cliente";
    , HelpContextID = 7450406;
    , Caption='\<Saldi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_61.Click()
      with this.Parent.oContained
        GSAR_BGS(this.Parent.oContained,"C", .w_ANCODICE)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_61.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_2_61.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_COGE<>'S')
     endwith
    endif
  endfunc

  add object oMINCON_2_67 as StdField with uid="WCGZZGXLIN",rtseq=105,rtrep=.f.,;
    cFormVar = "w_MINCON", cQueryName = "MINCON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Limite inferiore del conto clienti nello studio",;
    HelpContextID = 212776646,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=460, Top=392, InputMask=replicate('X',6)

  func oMINCON_2_67.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP$'N-G')
    endwith
  endfunc

  add object oMAXCON_2_68 as StdField with uid="OHVCTJHVLB",rtseq=106,rtrep=.f.,;
    cFormVar = "w_MAXCON", cQueryName = "MAXCON",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Limite superiore del conto clienti nello studio",;
    HelpContextID = 212815558,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=460, Top=418, InputMask=replicate('X',6)

  func oMAXCON_2_68.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP$'N-G')
    endwith
  endfunc

  add object oDESCAU_2_71 as StdField with uid="TPDVVFKXNE",rtseq=107,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 221314506,;
   bGlobalFont=.t.,;
    Height=21, Width=309, Left=294, Top=188, InputMask=replicate('X',40)

  func oDESCAU_2_71.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S' Or .w_ANFLGCAU<>'S')
    endwith
  endfunc

  add object oDESIRPEF_2_75 as StdField with uid="SWCQZMXUAI",rtseq=108,rtrep=.f.,;
    cFormVar = "w_DESIRPEF", cQueryName = "DESIRPEF",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 18546052,;
   bGlobalFont=.t.,;
    Height=21, Width=353, Left=368, Top=238, InputMask=replicate('X',60)


  add object oBtn_2_89 as StdButton with uid="YYONVGWQOM",left=540, top=366, width=22,height=22,;
    caption="...", nPag=2;
    , ToolTipText = "Premere per abbinare sottoconto studio";
    , HelpContextID = 158964182;
    , ImgSize = 16;
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_89.Click()
      do GSLMAKRS with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_89.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP<>'G' OR .cfunction<>'Load')
     endwith
    endif
  endfunc

  add object oStr_2_5 as StdString with uid="BSSPMTDPDP",Visible=.t., Left=43, Top=8,;
    Alignment=1, Width=126, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_2_11 as StdString with uid="OOHZOFUXBH",Visible=.t., Left=45, Top=68,;
    Alignment=1, Width=126, Height=15,;
    Caption="Categoria contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="HRJFCLLENE",Visible=.t., Left=45, Top=98,;
    Alignment=1, Width=126, Height=15,;
    Caption="Collegamento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="AMXLPVRLOY",Visible=.t., Left=45, Top=129,;
    Alignment=1, Width=126, Height=18,;
    Caption="Cod. IVA esenz./agev.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="CPJYCBZWNV",Visible=.t., Left=45, Top=38,;
    Alignment=1, Width=126, Height=15,;
    Caption="Mastro contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_2_35 as StdString with uid="ZVACPDOZTI",Visible=.t., Left=5, Top=291,;
    Alignment=0, Width=332, Height=18,;
    Caption="Comunicazione operazioni superiori a 3.000 euro"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="FCYVCLTNTI",Visible=.t., Left=5, Top=315,;
    Alignment=1, Width=166, Height=18,;
    Caption="Operazioni rilevanti IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_2_39 as StdString with uid="SUZRUPRUCS",Visible=.t., Left=361, Top=315,;
    Alignment=1, Width=147, Height=18,;
    Caption="Tipologia prevalente:"  ;
  , bGlobalFont=.t.

  add object oStr_2_58 as StdString with uid="NKBVNTCWRU",Visible=.t., Left=4, Top=345,;
    Alignment=0, Width=390, Height=15,;
    Caption="Altri dati"  ;
  , bGlobalFont=.t.

  add object oStr_2_60 as StdString with uid="YIVMNQFYHG",Visible=.t., Left=329, Top=366,;
    Alignment=1, Width=126, Height=17,;
    Caption="Sottoconto studio:"  ;
  , bGlobalFont=.t.

  func oStr_2_60.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP='N')
    endwith
  endfunc

  add object oStr_2_62 as StdString with uid="QUDILUZEVD",Visible=.t., Left=361, Top=392,;
    Alignment=1, Width=94, Height=17,;
    Caption="Limite inferiore:"  ;
  , bGlobalFont=.t.

  func oStr_2_62.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP$'N-G')
    endwith
  endfunc

  add object oStr_2_63 as StdString with uid="LAEQUILSJL",Visible=.t., Left=353, Top=418,;
    Alignment=1, Width=102, Height=17,;
    Caption="Limite superiore:"  ;
  , bGlobalFont=.t.

  func oStr_2_63.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP$'N-G')
    endwith
  endfunc

  add object oStr_2_64 as StdString with uid="PXOKWNNJPX",Visible=.t., Left=545, Top=366,;
    Alignment=1, Width=103, Height=18,;
    Caption="Cod. soggetto:"  ;
  , bGlobalFont=.t.

  func oStr_2_64.mHide()
    with this.Parent.oContained
      return (g_LEMC<>'S' OR g_TRAEXP<>'C')
    endwith
  endfunc

  add object oStr_2_65 as StdString with uid="GCCUOCCYZL",Visible=.t., Left=58, Top=159,;
    Alignment=1, Width=113, Height=18,;
    Caption="Tipo operazione IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_2_70 as StdString with uid="TFABSECPSF",Visible=.t., Left=5, Top=190,;
    Alignment=1, Width=166, Height=18,;
    Caption="Contropartita per cauzioni:"  ;
  , bGlobalFont=.t.

  func oStr_2_70.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S' Or .w_ANFLGCAU<>'S')
    endwith
  endfunc

  add object oStr_2_73 as StdString with uid="VKHKUTYGXE",Visible=.t., Left=5, Top=219,;
    Alignment=0, Width=390, Height=18,;
    Caption="Dati ritenute subite"  ;
  , bGlobalFont=.t.

  add object oStr_2_74 as StdString with uid="GVMYSYKYSK",Visible=.t., Left=163, Top=241,;
    Alignment=0, Width=138, Height=18,;
    Caption="Codice Tributo I.R.PE.F.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_76 as StdString with uid="RINPZJSKLJ",Visible=.t., Left=178, Top=263,;
    Alignment=0, Width=118, Height=18,;
    Caption="Causale Prestazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_79 as StdString with uid="DMHPXWFGXD",Visible=.t., Left=560, Top=392,;
    Alignment=1, Width=88, Height=18,;
    Caption="Cod. comune:"  ;
  , bGlobalFont=.t.

  add object oStr_2_82 as StdString with uid="LXULGJINJG",Visible=.t., Left=174, Top=470,;
    Alignment=1, Width=181, Height=18,;
    Caption="Codice identificativo fiscale:"  ;
  , bGlobalFont=.t.

  add object oBox_2_56 as StdBox with uid="HHBXEXIRIX",left=2, top=359, width=726,height=1

  add object oBox_2_72 as StdBox with uid="TRVQYGLKDL",left=3, top=236, width=726,height=1

  add object oBox_2_83 as StdBox with uid="USUYVAUTGO",left=3, top=308, width=726,height=2
enddefine
define class tgsar_aclPag3 as StdContainer
  Width  = 769
  height = 554
  stdWidth  = 769
  stdheight = 554
  resizeXpos=307
  resizeYpos=334
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_3_1 as StdField with uid="XTFRAYCWPB",rtseq=115,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 163847206,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=135, Top=8, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15)

  add object oRAG1_3_2 as StdField with uid="VWMWROWSZT",rtseq=116,rtrep=.f.,;
    cFormVar = "w_RAG1", cQueryName = "RAG1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 162283286,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=282, Top=8, InputMask=replicate('X',40)

  add object oANCATCOM_3_3 as StdField with uid="EABMHGYNOV",rtseq=117,rtrep=.f.,;
    cFormVar = "w_ANCATCOM", cQueryName = "ANCATCOM",;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categoria commerciale associata al cliente",;
    HelpContextID = 33295187,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=37, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_ANCATCOM"

  func oANCATCOM_3_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCATCOM_3_3.ecpDrop(oSource)
    this.Parent.oContained.link_3_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCATCOM_3_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oANCATCOM_3_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oANCATCOM_3_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_ANCATCOM
     i_obj.ecpSave()
  endproc

  add object oDESCAC_3_4 as StdField with uid="TFNKVTIPBP",rtseq=118,rtrep=.f.,;
    cFormVar = "w_DESCAC", cQueryName = "DESCAC",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 13566518,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=198, Top=37, InputMask=replicate('X',35)


  add object oANFLCONA_3_5 as StdCombo with uid="JWEFOYLSAR",rtseq=119,rtrep=.f.,left=619,top=37,width=122,height=21;
    , ToolTipText = "Tipologia cliente in funzione del contributo ambientale (CONAI)";
    , HelpContextID = 217529159;
    , cFormVar="w_ANFLCONA",RowSource=""+"Utilizzatore,"+"Esportatore,"+"Altro", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oANFLCONA_3_5.RadioValue()
    return(iif(this.value =1,'U',;
    iif(this.value =2,'E',;
    iif(this.value =3,'A',;
    space(1)))))
  endfunc
  func oANFLCONA_3_5.GetRadio()
    this.Parent.oContained.w_ANFLCONA = this.RadioValue()
    return .t.
  endfunc

  func oANFLCONA_3_5.SetRadio()
    this.Parent.oContained.w_ANFLCONA=trim(this.Parent.oContained.w_ANFLCONA)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLCONA=='U',1,;
      iif(this.Parent.oContained.w_ANFLCONA=='E',2,;
      iif(this.Parent.oContained.w_ANFLCONA=='A',3,;
      0)))
  endfunc

  func oANFLCONA_3_5.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oANCATSCM_3_6 as StdField with uid="YPVAFYDCXY",rtseq=120,rtrep=.f.,;
    cFormVar = "w_ANCATSCM", cQueryName = "ANCATSCM",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria sconti / maggiorazioni inesistente o non di tipo cliente",;
    ToolTipText = "Codice della categoria sconti / maggiorazioni associata al cliente",;
    HelpContextID = 235140269,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=64, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAT_SCMA", cZoomOnZoom="GSAR_ASM", oKey_1_1="CSCODICE", oKey_1_2="this.w_ANCATSCM"

  func oANCATSCM_3_6.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oANCATSCM_3_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCATSCM_3_6.ecpDrop(oSource)
    this.Parent.oContained.link_3_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCATSCM_3_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAT_SCMA','*','CSCODICE',cp_AbsName(this.parent,'oANCATSCM_3_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASM',"Categorie sconti/maggiorazioni",'GSAR_ACL.CAT_SCMA_VZM',this.parent.oContained
  endproc
  proc oANCATSCM_3_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CSCODICE=this.parent.oContained.w_ANCATSCM
     i_obj.ecpSave()
  endproc


  add object oANFLGCAU_3_7 as StdCombo with uid="SCYCWCDNAE",rtseq=121,rtrep=.f.,left=619,top=61,width=122,height=21;
    , ToolTipText = "Se attivo: sul documento viene gestita la cauzione relativa agli imballi";
    , HelpContextID = 248038565;
    , cFormVar="w_ANFLGCAU",RowSource=""+"Disattiva,"+"Attiva", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oANFLGCAU_3_7.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oANFLGCAU_3_7.GetRadio()
    this.Parent.oContained.w_ANFLGCAU = this.RadioValue()
    return .t.
  endfunc

  func oANFLGCAU_3_7.SetRadio()
    this.Parent.oContained.w_ANFLGCAU=trim(this.Parent.oContained.w_ANFLGCAU)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLGCAU=='N',1,;
      iif(this.Parent.oContained.w_ANFLGCAU=='S',2,;
      0))
  endfunc

  func oANFLGCAU_3_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_VEFA='S')
    endwith
   endif
  endfunc

  func oANFLGCAU_3_7.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S')
    endwith
  endfunc

  add object oDESSCM_3_8 as StdField with uid="EXFRMZOCSP",rtseq=122,rtrep=.f.,;
    cFormVar = "w_DESSCM", cQueryName = "DESSCM",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 184484406,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=198, Top=64, InputMask=replicate('X',35)

  func oDESSCM_3_8.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oANTIPFAT_3_9 as StdCombo with uid="ZVQFJAYXZJ",rtseq=123,rtrep=.f.,left=589,top=85,width=152,height=21;
    , ToolTipText = "Tipo raggruppamento DDT in fattura differita";
    , HelpContextID = 188408998;
    , cFormVar="w_ANTIPFAT",RowSource=""+"Riepilogativa,"+"Per destinazione,"+"Per singolo DDT,"+"Per ordine,"+"Per ordine+destinazione", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oANTIPFAT_3_9.RadioValue()
    return(iif(this.value =1,'R',;
    iif(this.value =2,'D',;
    iif(this.value =3,'S',;
    iif(this.value =4,'C',;
    iif(this.value =5,'E',;
    space(1)))))))
  endfunc
  func oANTIPFAT_3_9.GetRadio()
    this.Parent.oContained.w_ANTIPFAT = this.RadioValue()
    return .t.
  endfunc

  func oANTIPFAT_3_9.SetRadio()
    this.Parent.oContained.w_ANTIPFAT=trim(this.Parent.oContained.w_ANTIPFAT)
    this.value = ;
      iif(this.Parent.oContained.w_ANTIPFAT=='R',1,;
      iif(this.Parent.oContained.w_ANTIPFAT=='D',2,;
      iif(this.Parent.oContained.w_ANTIPFAT=='S',3,;
      iif(this.Parent.oContained.w_ANTIPFAT=='C',4,;
      iif(this.Parent.oContained.w_ANTIPFAT=='E',5,;
      0)))))
  endfunc

  func oANTIPFAT_3_9.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oANGRUPRO_3_10 as StdField with uid="YMPGGNTBZE",rtseq=124,rtrep=.f.,;
    cFormVar = "w_ANGRUPRO", cQueryName = "ANGRUPRO",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Categoria provvigioni inesistente o non di tipo cliente",;
    ToolTipText = "Codice della categoria provvigioni associata al cliente",;
    HelpContextID = 14857387,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=91, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUPRO", cZoomOnZoom="GSAR_AGP", oKey_1_1="GPCODICE", oKey_1_2="this.w_ANGRUPRO"

  func oANGRUPRO_3_10.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oANGRUPRO_3_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oANGRUPRO_3_10.ecpDrop(oSource)
    this.Parent.oContained.link_3_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANGRUPRO_3_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUPRO','*','GPCODICE',cp_AbsName(this.parent,'oANGRUPRO_3_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGP',"Categorie provvigioni",'GSAR_ACL.GRUPRO_VZM',this.parent.oContained
  endproc
  proc oANGRUPRO_3_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GPCODICE=this.parent.oContained.w_ANGRUPRO
     i_obj.ecpSave()
  endproc

  add object oDESGPP_3_12 as StdField with uid="COHMBSMALT",rtseq=126,rtrep=.f.,;
    cFormVar = "w_DESGPP", cQueryName = "DESGPP",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 247661110,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=198, Top=91, InputMask=replicate('X',35)

  func oDESGPP_3_12.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oANCONCON_3_14 as StdZTamTableCombo with uid="OUZRPVRXYH",rtseq=127,rtrep=.f.,left=589,top=107,width=152,height=21;
    , ToolTipText = "Condizione di consegna (utilizzata anche ai fini INTRA)";
    , HelpContextID = 27921236;
    , cFormVar="w_ANCONCON",tablefilter="", bObbl = .f. , nPag = 3;
    , cLinkFile="CON_CONS";
    , cTable='QUERY\GSARCACL.VQR',cKey='COCODICE',cValue='CODESCRI',cOrderBy='CODESCRI',xDefault=space(1);
  , bGlobalFont=.t.


  func oANCONCON_3_14.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oANCONCON_3_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCONCON_3_14.ecpDrop(oSource)
    this.Parent.oContained.link_3_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oANCODLIN_3_17 as StdField with uid="MZPQYYOENM",rtseq=130,rtrep=.f.,;
    cFormVar = "w_ANCODLIN", cQueryName = "ANCODLIN",;
    bObbl = .t. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Lingua in cui stampare i documenti",;
    HelpContextID = 168430420,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=118, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", cZoomOnZoom="GSAR_ALG", oKey_1_1="LUCODICE", oKey_1_2="this.w_ANCODLIN"

  func oANCODLIN_3_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODLIN_3_17.ecpDrop(oSource)
    this.Parent.oContained.link_3_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODLIN_3_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'oANCODLIN_3_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALG',"Lingue",'',this.parent.oContained
  endproc
  proc oANCODLIN_3_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALG()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LUCODICE=this.parent.oContained.w_ANCODLIN
     i_obj.ecpSave()
  endproc

  add object oDESLIN_3_18 as StdField with uid="CWTDJNOXMH",rtseq=131,rtrep=.f.,;
    cFormVar = "w_DESLIN", cQueryName = "DESLIN",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 207094326,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=198, Top=118, InputMask=replicate('X',30)

  add object oANCODVAL_3_19 as StdField with uid="CLDWQJXDDM",rtseq=132,rtrep=.f.,;
    cFormVar = "w_ANCODVAL", cQueryName = "ANCODVAL",;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice della valuta utilizzata in fatturazione",;
    HelpContextID = 200668334,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=145, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_ANCODVAL"

  func oANCODVAL_3_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_19('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODVAL_3_19.ecpDrop(oSource)
    this.Parent.oContained.link_3_19('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODVAL_3_19.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oANCODVAL_3_19'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oANCODVAL_3_19.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_ANCODVAL
     i_obj.ecpSave()
  endproc

  add object oDESVAL_3_20 as StdField with uid="KJKKWBVSOB",rtseq=133,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 165806646,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=198, Top=145, InputMask=replicate('X',35)

  add object oANNUMLIS_3_21 as StdField with uid="NEDKZWNAIU",rtseq=134,rtrep=.f.,;
    cFormVar = "w_ANNUMLIS", cQueryName = "ANNUMLIS",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del listino a cui fa riferimento il cliente",;
    HelpContextID = 178305881,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=172, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_ANNUMLIS"

  func oANNUMLIS_3_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oANNUMLIS_3_21.ecpDrop(oSource)
    this.Parent.oContained.link_3_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANNUMLIS_3_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oANNUMLIS_3_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSMA0KVL.LISTINI_VZM',this.parent.oContained
  endproc
  proc oANNUMLIS_3_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_ANNUMLIS
     i_obj.ecpSave()
  endproc


  add object oANCODCUC_3_22 as StdCombo with uid="ALBJAJIGVB",rtseq=135,rtrep=.f.,left=553,top=174,width=187,height=21;
    , ToolTipText = "Codice univoco CBI";
    , HelpContextID = 250999991;
    , cFormVar="w_ANCODCUC",RowSource=""+"No,"+"Solo testata,"+"Testata + corpo non pubblicato,"+"Testata + corpo pubblicato,"+"Testata + corpo 'white label'", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oANCODCUC_3_22.RadioValue()
    return(iif(this.value =1,'00',;
    iif(this.value =2,'01',;
    iif(this.value =3,'02',;
    iif(this.value =4,'03',;
    iif(this.value =5,'04',;
    space(2)))))))
  endfunc
  func oANCODCUC_3_22.GetRadio()
    this.Parent.oContained.w_ANCODCUC = this.RadioValue()
    return .t.
  endfunc

  func oANCODCUC_3_22.SetRadio()
    this.Parent.oContained.w_ANCODCUC=trim(this.Parent.oContained.w_ANCODCUC)
    this.value = ;
      iif(this.Parent.oContained.w_ANCODCUC=='00',1,;
      iif(this.Parent.oContained.w_ANCODCUC=='01',2,;
      iif(this.Parent.oContained.w_ANCODCUC=='02',3,;
      iif(this.Parent.oContained.w_ANCODCUC=='03',4,;
      iif(this.Parent.oContained.w_ANCODCUC=='04',5,;
      0)))))
  endfunc

  func oANCODCUC_3_22.mHide()
    with this.Parent.oContained
      return (g_FAEL<>'S')
    endwith
  endfunc

  add object oAN1SCONT_3_23 as StdField with uid="BGWIPDOGYK",rtseq=136,rtrep=.f.,;
    cFormVar = "w_AN1SCONT", cQueryName = "AN1SCONT",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Prima maggiorazione (se positiva) o sconto (se negativa) praticata",;
    HelpContextID = 217901914,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=135, Top=199, cSayPict='"999.99"', cGetPict='"999.99"'

  func oAN1SCONT_3_23.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oANGRPDEF_3_24 as StdField with uid="OKHGEEOSET",rtseq=137,rtrep=.f.,;
    cFormVar = "w_ANGRPDEF", cQueryName = "ANGRPDEF",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo output",;
    HelpContextID = 221426868,;
   bGlobalFont=.t.,;
    Height=21, Width=63, Left=534, Top=200, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRP_DEFA", cZoomOnZoom="gsar_agd", oKey_1_1="GDCODICE", oKey_1_2="this.w_ANGRPDEF"

  func oANGRPDEF_3_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oANGRPDEF_3_24.ecpDrop(oSource)
    this.Parent.oContained.link_3_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANGRPDEF_3_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRP_DEFA','*','GDCODICE',cp_AbsName(this.parent,'oANGRPDEF_3_24'),iif(empty(i_cWhere),.f.,i_cWhere),'gsar_agd',"Gruppi output",'',this.parent.oContained
  endproc
  proc oANGRPDEF_3_24.mZoomOnZoom
    local i_obj
    i_obj=gsar_agd()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GDCODICE=this.parent.oContained.w_ANGRPDEF
     i_obj.ecpSave()
  endproc

  add object oAN2SCONT_3_25 as StdField with uid="WJNUXHSSMG",rtseq=138,rtrep=.f.,;
    cFormVar = "w_AN2SCONT", cQueryName = "AN2SCONT",;
    bObbl = .f. , nPag = 3, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Seconda eventuale maggiorazione (se positiva) o sconto (se negativa) praticata",;
    HelpContextID = 217906010,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=198, Top=199, cSayPict='"999.99"', cGetPict='"999.99"'

  func oAN2SCONT_3_25.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AN1SCONT # 0)
    endwith
   endif
  endfunc

  func oAN2SCONT_3_25.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oANCODZON_3_26 as StdField with uid="USHUBMASZS",rtseq=139,rtrep=.f.,;
    cFormVar = "w_ANCODZON", cQueryName = "ANCODZON",;
    bObbl = .f. , nPag = 3, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Codice zona inesistente oppure obsoleto",;
    ToolTipText = "Codice della zona di appartenenza",;
    HelpContextID = 134875988,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=226, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="ZONE", cZoomOnZoom="GSAR_AZO", oKey_1_1="ZOCODZON", oKey_1_2="this.w_ANCODZON"

  func oANCODZON_3_26.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_26('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODZON_3_26.ecpDrop(oSource)
    this.Parent.oContained.link_3_26('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODZON_3_26.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ZONE','*','ZOCODZON',cp_AbsName(this.parent,'oANCODZON_3_26'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AZO',"Zone",'',this.parent.oContained
  endproc
  proc oANCODZON_3_26.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AZO()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ZOCODZON=this.parent.oContained.w_ANCODZON
     i_obj.ecpSave()
  endproc

  add object oANCODAG1_3_27 as StdField with uid="ABUWGLDDZP",rtseq=140,rtrep=.f.,;
    cFormVar = "w_ANCODAG1", cQueryName = "ANCODAG1",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice agente che cura i rapporti con il cliente",;
    HelpContextID = 16118985,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=253, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_ANCODAG1"

  func oANCODAG1_3_27.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oANCODAG1_3_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODAG1_3_27.ecpDrop(oSource)
    this.Parent.oContained.link_3_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODAG1_3_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oANCODAG1_3_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Agenti",'',this.parent.oContained
  endproc
  proc oANCODAG1_3_27.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_ANCODAG1
     i_obj.ecpSave()
  endproc

  add object oANTIPOCL_3_28 as StdField with uid="FTNOQSGZCZ",rtseq=141,rtrep=.f.,;
    cFormVar = "w_ANTIPOCL", cQueryName = "ANTIPOCL",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 37414062,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=135, Top=280, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_CLIE", cZoomOnZoom="GSPR_ATC", oKey_1_1="TCCODICE", oKey_1_2="this.w_ANTIPOCL"

  func oANTIPOCL_3_28.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  func oANTIPOCL_3_28.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_28('Part',this)
    endwith
    return bRes
  endfunc

  proc oANTIPOCL_3_28.ecpDrop(oSource)
    this.Parent.oContained.link_3_28('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANTIPOCL_3_28.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_CLIE','*','TCCODICE',cp_AbsName(this.parent,'oANTIPOCL_3_28'),iif(empty(i_cWhere),.f.,i_cWhere),'GSPR_ATC',"Studi di settore -tipologie clientela",'',this.parent.oContained
  endproc
  proc oANTIPOCL_3_28.mZoomOnZoom
    local i_obj
    i_obj=GSPR_ATC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TCCODICE=this.parent.oContained.w_ANTIPOCL
     i_obj.ecpSave()
  endproc

  add object oANMAGTER_3_29 as StdField with uid="NIWSZSBHAF",rtseq=142,rtrep=.f.,;
    cFormVar = "w_ANMAGTER", cQueryName = "ANMAGTER",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente",;
    ToolTipText = "Codice magazzino utilizzato per l'eventuale assegnazione dei documenti",;
    HelpContextID = 231953576,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=135, Top=280, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_ANMAGTER"

  func oANMAGTER_3_29.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oANMAGTER_3_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oANMAGTER_3_29.ecpDrop(oSource)
    this.Parent.oContained.link_3_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANMAGTER_3_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oANMAGTER_3_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oANMAGTER_3_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_ANMAGTER
     i_obj.ecpSave()
  endproc

  add object oANCODORN_3_30 as StdField with uid="YIRXRACULZ",rtseq=143,rtrep=.f.,;
    cFormVar = "w_ANCODORN", cQueryName = "ANCODORN",;
    bObbl = .f. , nPag = 3, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Per conto di.. Preimpostato sui documenti. Solo in caricamento manuale.",;
    HelpContextID = 49673388,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=135, Top=308, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_ANTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_ANCODORN"

  func oANCODORN_3_30.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_30('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODORN_3_30.ecpDrop(oSource)
    this.Parent.oContained.link_3_30('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODORN_3_30.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_ANTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_ANTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oANCODORN_3_30'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti",'',this.parent.oContained
  endproc
  proc oANCODORN_3_30.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_ANTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_ANCODORN
     i_obj.ecpSave()
  endproc

  add object oANCODPOR_3_31 as StdField with uid="CKOFXVLWSI",rtseq=144,rtrep=.f.,;
    cFormVar = "w_ANCODPOR", cQueryName = "ANCODPOR",;
    bObbl = .f. , nPag = 3, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice porto/aeroporto di destinazione della merce spedita a cliente",;
    HelpContextID = 235539288,;
   bGlobalFont=.t.,;
    Height=21, Width=121, Left=135, Top=336, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="COD_AREO", cZoomOnZoom="GLES_APP", oKey_1_1="PPCODICE", oKey_1_2="this.w_ANCODPOR"

  func oANCODPOR_3_31.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP')
    endwith
  endfunc

  func oANCODPOR_3_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODPOR_3_31.ecpDrop(oSource)
    this.Parent.oContained.link_3_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODPOR_3_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COD_AREO','*','PPCODICE',cp_AbsName(this.parent,'oANCODPOR_3_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GLES_APP',"Porti/aeroporti",'',this.parent.oContained
  endproc
  proc oANCODPOR_3_31.mZoomOnZoom
    local i_obj
    i_obj=GLES_APP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PPCODICE=this.parent.oContained.w_ANCODPOR
     i_obj.ecpSave()
  endproc


  add object oANMTDCLC_3_32 as StdTableCombo with uid="FECTWANVGQ",rtseq=145,rtrep=.f.,left=136,top=365,width=257,height=21;
    , ToolTipText = "Metodo di calcolo periodicit� di default";
    , HelpContextID = 17804105;
    , cFormVar="w_ANMTDCLC",tablefilter="", bObbl = .f. , nPag = 3;
    , cLinkFile="MODCLDAT";
    , cTable='MODCLDAT',cKey='MDCODICE',cValue='MDDESCRI',cOrderBy='MDDESCRI',xDefault=space(3);
  , bGlobalFont=.t.


  func oANMTDCLC_3_32.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oANMTDCLC_3_32.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_32('Part',this)
    endwith
    return bRes
  endfunc

  proc oANMTDCLC_3_32.ecpDrop(oSource)
    this.Parent.oContained.link_3_32('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oANMCALSI_3_33 as StdField with uid="AXAIXBZWZJ",rtseq=146,rtrep=.f.,;
    cFormVar = "w_ANMCALSI", cQueryName = "ANMCALSI",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo spese di imballo",;
    HelpContextID = 103896241,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=534, Top=253, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_ANMCALSI"

  func oANMCALSI_3_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
   endif
  endfunc

  func oANMCALSI_3_33.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oANMCALSI_3_33.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_33('Part',this)
    endwith
    return bRes
  endfunc

  proc oANMCALSI_3_33.ecpDrop(oSource)
    this.Parent.oContained.link_3_33('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANMCALSI_3_33.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oANMCALSI_3_33'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oANMCALSI_3_33.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_ANMCALSI
     i_obj.ecpSave()
  endproc

  add object oANMCALST_3_34 as StdField with uid="AWSYWHRWVU",rtseq=147,rtrep=.f.,;
    cFormVar = "w_ANMCALST", cQueryName = "ANMCALST",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Metodo di calcolo spese di trasporto",;
    HelpContextID = 103896230,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=534, Top=280, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_ANMCALST"

  func oANMCALST_3_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
   endif
  endfunc

  func oANMCALST_3_34.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  func oANMCALST_3_34.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_34('Part',this)
    endwith
    return bRes
  endfunc

  proc oANMCALST_3_34.ecpDrop(oSource)
    this.Parent.oContained.link_3_34('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANMCALST_3_34.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oANMCALST_3_34'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oANMCALST_3_34.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_ANMCALST
     i_obj.ecpSave()
  endproc

  add object oDESLIS_3_35 as StdField with uid="NHQIECGKWQ",rtseq=148,rtrep=.f.,;
    cFormVar = "w_DESLIS", cQueryName = "DESLIS",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 245890506,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=198, Top=172, InputMask=replicate('X',40)

  add object oDESZON_3_38 as StdField with uid="ENFHFGPEKI",rtseq=151,rtrep=.f.,;
    cFormVar = "w_DESZON", cQueryName = "DESZON",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 214303286,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=198, Top=226, InputMask=replicate('X',35)

  add object oDESAGE1_3_39 as StdField with uid="DPGOZUSHFG",rtseq=152,rtrep=.f.,;
    cFormVar = "w_DESAGE1", cQueryName = "DESAGE1",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 215154122,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=198, Top=253, InputMask=replicate('X',35)

  func oDESAGE1_3_39.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oDESMAG_3_40 as StdField with uid="NQXKEZAIBO",rtseq=153,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 81330742,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=198, Top=280, InputMask=replicate('X',30)

  func oDESMAG_3_40.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oANBOLFAT_3_41 as StdCheck with uid="PCISCTDPLX",rtseq=154,rtrep=.f.,left=6, top=413, caption="Bolli in fattura",;
    ToolTipText = "Se attivo: al cliente vengono addebitati in fattura i bolli su cambiale",;
    HelpContextID = 192283814,;
    cFormVar="w_ANBOLFAT", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oANBOLFAT_3_41.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANBOLFAT_3_41.GetRadio()
    this.Parent.oContained.w_ANBOLFAT = this.RadioValue()
    return .t.
  endfunc

  func oANBOLFAT_3_41.SetRadio()
    this.Parent.oContained.w_ANBOLFAT=trim(this.Parent.oContained.w_ANBOLFAT)
    this.value = ;
      iif(this.Parent.oContained.w_ANBOLFAT=='S',1,;
      0)
  endfunc

  func oANBOLFAT_3_41.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oANPREBOL_3_42 as StdCheck with uid="CCFAZCWNLS",rtseq=155,rtrep=.f.,left=6, top=435, caption="Prezzo in DDT/bolla",;
    ToolTipText = "Se attivo: stampa i prezzi sui DDT o sulle bolle",;
    HelpContextID = 1956690,;
    cFormVar="w_ANPREBOL", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oANPREBOL_3_42.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANPREBOL_3_42.GetRadio()
    this.Parent.oContained.w_ANPREBOL = this.RadioValue()
    return .t.
  endfunc

  func oANPREBOL_3_42.SetRadio()
    this.Parent.oContained.w_ANPREBOL=trim(this.Parent.oContained.w_ANPREBOL)
    this.value = ;
      iif(this.Parent.oContained.w_ANPREBOL=='S',1,;
      0)
  endfunc

  func oANPREBOL_3_42.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oANFLAPCA_3_43 as StdCheck with uid="EYKGDCBJDX",rtseq=156,rtrep=.f.,left=6, top=457, caption="Applica contributi accessori",;
    ToolTipText = "Se attivo per il cliente sar� applicata la combinazione dei contributi accessori indicata nella apposita tabella",;
    HelpContextID = 36226233,;
    cFormVar="w_ANFLAPCA", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oANFLAPCA_3_43.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLAPCA_3_43.GetRadio()
    this.Parent.oContained.w_ANFLAPCA = this.RadioValue()
    return .t.
  endfunc

  func oANFLAPCA_3_43.SetRadio()
    this.Parent.oContained.w_ANFLAPCA=trim(this.Parent.oContained.w_ANFLAPCA)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLAPCA=='S',1,;
      0)
  endfunc

  func oANFLAPCA_3_43.mHide()
    with this.Parent.oContained
      return (g_COAC='N')
    endwith
  endfunc


  add object oLinkPC_3_44 as StdButton with uid="OBYCBLOZAA",left=214, top=461, width=48,height=45,;
    CpPicture="BMP\CONTRACC.BMP", caption="", nPag=3;
    , ToolTipText = "Dettaglio contributo accessori generici";
    , HelpContextID = 99463973;
    , caption='\<Cont.acc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oLinkPC_3_44.Click()
      this.Parent.oContained.GSAR_MIN.LinkPCClick()
    endproc

  func oLinkPC_3_44.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_ANFLAPCA = 'S')
      endwith
    endif
  endfunc

  func oLinkPC_3_44.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_COAC='N')
     endwith
    endif
  endfunc

  add object oANSCORPO_3_45 as StdCheck with uid="ZROWAQOLUW",rtseq=157,rtrep=.f.,left=164, top=413, caption="Scorporo piede fatt.",;
    ToolTipText = "Se attivo: esegue lo scorporo sul totale fattura",;
    HelpContextID = 256963755,;
    cFormVar="w_ANSCORPO", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oANSCORPO_3_45.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANSCORPO_3_45.GetRadio()
    this.Parent.oContained.w_ANSCORPO = this.RadioValue()
    return .t.
  endfunc

  func oANSCORPO_3_45.SetRadio()
    this.Parent.oContained.w_ANSCORPO=trim(this.Parent.oContained.w_ANSCORPO)
    this.value = ;
      iif(this.Parent.oContained.w_ANSCORPO=='S',1,;
      0)
  endfunc

  func oANSCORPO_3_45.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oANFLCODI_3_46 as StdCheck with uid="CQJVPDRAII",rtseq=158,rtrep=.f.,left=164, top=435, caption="Codifica cliente",;
    ToolTipText = "Se attivo: stampa la codifica articoli del cliente nei documenti",;
    HelpContextID = 50906289,;
    cFormVar="w_ANFLCODI", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oANFLCODI_3_46.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oANFLCODI_3_46.GetRadio()
    this.Parent.oContained.w_ANFLCODI = this.RadioValue()
    return .t.
  endfunc

  func oANFLCODI_3_46.SetRadio()
    this.Parent.oContained.w_ANFLCODI=trim(this.Parent.oContained.w_ANFLCODI)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLCODI=='S',1,;
      0)
  endfunc

  func oANFLCODI_3_46.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oANCLIPOS_3_47 as StdCheck with uid="QSERGBLDCV",rtseq=159,rtrep=.f.,left=320, top=414, caption="Cliente P.O.S.",;
    ToolTipText = "Se attivo: cliente gestito nella vendita negozio",;
    HelpContextID = 240585561,;
    cFormVar="w_ANCLIPOS", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oANCLIPOS_3_47.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oANCLIPOS_3_47.GetRadio()
    this.Parent.oContained.w_ANCLIPOS = this.RadioValue()
    return .t.
  endfunc

  func oANCLIPOS_3_47.SetRadio()
    this.Parent.oContained.w_ANCLIPOS=trim(this.Parent.oContained.w_ANCLIPOS)
    this.value = ;
      iif(this.Parent.oContained.w_ANCLIPOS=='S',1,;
      0)
  endfunc

  func oANCLIPOS_3_47.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_GPOS='S')
    endwith
   endif
  endfunc

  func oANCLIPOS_3_47.mHide()
    with this.Parent.oContained
      return (g_GPOS<>'S' OR IsAlt())
    endwith
  endfunc

  add object oANFLGCPZ_3_48 as StdCheck with uid="IZEGSUZKEU",rtseq=160,rtrep=.f.,left=320, top=435, caption="Pubblica su web",;
    ToolTipText = "Se attivo: trasferisce anagrafica cliente a Web Application",;
    HelpContextID = 20396896,;
    cFormVar="w_ANFLGCPZ", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oANFLGCPZ_3_48.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLGCPZ_3_48.GetRadio()
    this.Parent.oContained.w_ANFLGCPZ = this.RadioValue()
    return .t.
  endfunc

  func oANFLGCPZ_3_48.SetRadio()
    this.Parent.oContained.w_ANFLGCPZ=trim(this.Parent.oContained.w_ANFLGCPZ)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLGCPZ=='S',1,;
      0)
  endfunc

  func oANFLGCPZ_3_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_IZCP$'SA' or g_CPIN='S' or g_REVI='S')
    endwith
   endif
  endfunc

  add object oANFLIMBA_3_50 as StdCheck with uid="PPHKDJSCPD",rtseq=162,rtrep=.f.,left=449, top=414, caption="Imballo",;
    ToolTipText = "Se attivo: utilizza codici di ricerca di tipo imballo nelle unit� logistiche",;
    HelpContextID = 78169273,;
    cFormVar="w_ANFLIMBA", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oANFLIMBA_3_50.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oANFLIMBA_3_50.GetRadio()
    this.Parent.oContained.w_ANFLIMBA = this.RadioValue()
    return .t.
  endfunc

  func oANFLIMBA_3_50.SetRadio()
    this.Parent.oContained.w_ANFLIMBA=trim(this.Parent.oContained.w_ANFLIMBA)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLIMBA=='S',1,;
      0)
  endfunc

  func oANFLIMBA_3_50.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_MADV='S')
    endwith
   endif
  endfunc

  func oANFLIMBA_3_50.mHide()
    with this.Parent.oContained
      return (g_MADV<>'S' OR IsAlt())
    endwith
  endfunc

  add object oANFLPRIV_3_51 as StdCheck with uid="SDHKBAVZNJ",rtseq=163,rtrep=.f.,left=449, top=435, caption="Cliente privato",;
    ToolTipText = "Se attivo identifica il cliente come privato (non Business)",;
    HelpContextID = 13056860,;
    cFormVar="w_ANFLPRIV", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oANFLPRIV_3_51.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLPRIV_3_51.GetRadio()
    this.Parent.oContained.w_ANFLPRIV = this.RadioValue()
    return .t.
  endfunc

  func oANFLPRIV_3_51.SetRadio()
    this.Parent.oContained.w_ANFLPRIV=trim(this.Parent.oContained.w_ANFLPRIV)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLPRIV=='S',1,;
      0)
  endfunc

  func oANFLPRIV_3_51.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_REVI<>'S' or .w_FLGCPZ<>'S' or .w_FLPRIV<>'S')
    endwith
   endif
  endfunc

  add object oANCODESC_3_54 as StdField with uid="IWDVZKXXIR",rtseq=165,rtrep=.f.,;
    cFormVar = "w_ANCODESC", cQueryName = "ANCODESC",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Suffisso codice di ricerca esclusivo per cli/for. viene utilizzato come filtro nel caricamento dei codici articoli sui documenti",;
    HelpContextID = 217445559,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=679, Top=414, InputMask=replicate('X',5)

  func oANCODESC_3_54.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_FLCESC='S')
    endwith
   endif
  endfunc

  func oANCODESC_3_54.mHide()
    with this.Parent.oContained
      return (g_FLCESC<>'S')
    endwith
  endfunc


  add object oBtn_3_56 as StdButton with uid="DNEESFCEGP",left=322, top=461, width=48,height=45,;
    CpPicture="BMP\MASTER.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per accedere al nominativo del cliente";
    , HelpContextID = 238600801;
    , Caption='\<Nominativi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_56.Click()
      with this.Parent.oContained
        GSAR_BOF(this.Parent.oContained,"A",.w_ANCODICE,.w_ANTIPCON)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_56.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((g_OFFE='S' OR g_AGEN='S') AND NOT EMPTY(.w_ANCODICE) AND NOT IsAlt())
      endwith
    endif
  endfunc

  func oBtn_3_56.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return ((g_OFFE<>'S' AND g_AGEN<>'S') OR IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_3_57 as StdButton with uid="MRZZRWSTOK",left=373, top=461, width=48,height=45,;
    CpPicture="BMP\VIEW.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per accedere alle offerte del cliente";
    , HelpContextID = 184396826;
    , Caption='\<Offerte';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_57.Click()
      with this.Parent.oContained
        GSAR_BOF(this.Parent.oContained,"B",.w_ANCODICE,.w_ANTIPCON)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_57.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_OFFE='S'  AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_3_57.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_OFFE<>'S')
     endwith
    endif
  endfunc


  add object oBtn_3_58 as StdButton with uid="OEVXAVUVZU",left=424, top=461, width=48,height=45,;
    CpPicture="BMP\DM_agenda.bmp", caption="", nPag=3;
    , ToolTipText = "Premere per accedere all'attivit�";
    , HelpContextID = 95993370;
    , Caption='\<Attivit�';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_58.Click()
      with this.Parent.oContained
        GSAR_BOF(this.Parent.oContained,"C",.w_ANCODICE,.w_ANTIPCON)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_58.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return ((g_AGEN = 'S' Or g_OFFE='S')   AND NOT EMPTY(.w_ANCODICE) AND NOT IsAlt())
      endwith
    endif
  endfunc

  func oBtn_3_58.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return ((g_OFFE<>'S'  And g_AGEN <> 'S') OR IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_3_59 as StdButton with uid="GDNPSNDKMP",left=475, top=461, width=48,height=45,;
    CpPicture="BMP\CONTRATT.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per accedere ai contratti del cliente";
    , HelpContextID = 233708758;
    , Caption='\<Contratti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_59.Click()
      with this.Parent.oContained
        do GSAR_BCL with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_59.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_GESCON='S' AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_3_59.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_GESCON<>'S')
     endwith
    endif
  endfunc


  add object oBtn_3_61 as StdButton with uid="BOJMNTCUPJ",left=526, top=461, width=48,height=45,;
    CpPicture="bmp\doc.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per accedere alle vendite relative al cliente";
    , HelpContextID = 193866922;
    , Caption='\<Vendite';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_61.Click()
      with this.Parent.oContained
        GSAR_BKK(this.Parent.oContained,"B")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_61.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_3_61.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_MAGA<>'S' OR IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_3_63 as StdButton with uid="VPGCDULAQO",left=577, top=461, width=48,height=45,;
    CpPicture="BMP\fattura.bmp", caption="", nPag=3;
    , ToolTipText = "Premere per accedere agli ordini del cliente";
    , HelpContextID = 104448026;
    , Caption='\<Ordini';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_63.Click()
      with this.Parent.oContained
        GSAR_BKK(this.Parent.oContained,"C")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_63.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_3_63.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_ORDI<>'S')
     endwith
    endif
  endfunc


  add object oBtn_3_65 as StdButton with uid="FUJYHNGTOC",left=628, top=461, width=48,height=45,;
    CpPicture="bmp\prospvend.bmp", caption="", nPag=3;
    , ToolTipText = "Premere per accedere al prospetto del venduto relativo al cliente";
    , HelpContextID = 17762501;
    , Caption='\<Prosp.Ven.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_65.Click()
      with this.Parent.oContained
        GSAR_BKK(this.Parent.oContained,"D")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_65.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_3_65.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_MAGA<>'S' OR IsAlt())
     endwith
    endif
  endfunc


  add object oBtn_3_67 as StdButton with uid="WBSJFQXSYB",left=679, top=461, width=48,height=45,;
    CpPicture="BMP\ULTVEN.BMP", caption="", nPag=3;
    , ToolTipText = "Premere per accedere agli ultimi articoli venduti al cliente";
    , HelpContextID = 101927610;
    , Caption='\<Art.Venduti';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_3_67.Click()
      do GSAR_KUV with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_3_67.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (g_MAGA='S' AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_3_67.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_MAGA<>'S' OR IsAlt())
     endwith
    endif
  endfunc

  add object oMSDESIMB_3_82 as StdField with uid="BTQZBCVNPG",rtseq=168,rtrep=.f.,;
    cFormVar = "w_MSDESIMB", cQueryName = "MSDESIMB",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 133177608,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=587, Top=253, InputMask=replicate('X',40)

  add object oMSDESTRA_3_84 as StdField with uid="SAQKMSVPYV",rtseq=169,rtrep=.f.,;
    cFormVar = "w_MSDESTRA", cQueryName = "MSDESTRA",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 219143929,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=587, Top=280, InputMask=replicate('X',40)

  add object oDESORN_3_88 as StdField with uid="BMURWMPBQL",rtseq=170,rtrep=.f.,;
    cFormVar = "w_DESORN", cQueryName = "DESORN",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 216728118,;
   bGlobalFont=.t.,;
    Height=21, Width=270, Left=256, Top=308, InputMask=replicate('X',40)

  add object oGDDESCRI_3_93 as StdField with uid="YMAIRCMCPA",rtseq=171,rtrep=.f.,;
    cFormVar = "w_GDDESCRI", cQueryName = "GDDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 235925073,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=598, Top=200, InputMask=replicate('X',40)

  add object oDESCRI_3_94 as StdField with uid="KHZVYDOWZB",rtseq=172,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 132055606,;
   bGlobalFont=.t.,;
    Height=21, Width=270, Left=256, Top=336, InputMask=replicate('X',40)

  func oDESCRI_3_94.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP')
    endwith
  endfunc

  add object oANMCALSI_3_97 as StdField with uid="KFHFUNVCEO",rtseq=173,rtrep=.f.,;
    cFormVar = "w_ANMCALSI", cQueryName = "ANMCALSI",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Spese generali",;
    HelpContextID = 103896241,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=534, Top=253, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_ANMCALSI"

  func oANMCALSI_3_97.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (isAlt())
    endwith
   endif
  endfunc

  func oANMCALSI_3_97.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  func oANMCALSI_3_97.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_97('Part',this)
    endwith
    return bRes
  endfunc

  proc oANMCALSI_3_97.ecpDrop(oSource)
    this.Parent.oContained.link_3_97('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANMCALSI_3_97.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oANMCALSI_3_97'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oANMCALSI_3_97.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_ANMCALSI
     i_obj.ecpSave()
  endproc

  add object oANMCALST_3_98 as StdField with uid="KLRMMZJJSK",rtseq=174,rtrep=.f.,;
    cFormVar = "w_ANMCALST", cQueryName = "ANMCALST",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Cassa di previdenza",;
    HelpContextID = 103896230,;
   bGlobalFont=.t.,;
    Height=21, Width=52, Left=534, Top=280, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="METCALSP", cZoomOnZoom="GSAR_AMS", oKey_1_1="MSCODICE", oKey_1_2="this.w_ANMCALST"

  func oANMCALST_3_98.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (IsAlt())
    endwith
   endif
  endfunc

  func oANMCALST_3_98.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  func oANMCALST_3_98.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_3_98('Part',this)
    endwith
    return bRes
  endfunc

  proc oANMCALST_3_98.ecpDrop(oSource)
    this.Parent.oContained.link_3_98('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANMCALST_3_98.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'METCALSP','*','MSCODICE',cp_AbsName(this.parent,'oANMCALST_3_98'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMS',"Metodi di calcolo",'',this.parent.oContained
  endproc
  proc oANMCALST_3_98.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MSCODICE=this.parent.oContained.w_ANMCALST
     i_obj.ecpSave()
  endproc

  add object oDESTIP_3_103 as StdField with uid="SEOSVPWHOI",rtseq=175,rtrep=.f.,;
    cFormVar = "w_DESTIP", cQueryName = "DESTIP",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 241173046,;
   bGlobalFont=.t.,;
    Height=21, Width=259, Left=198, Top=280, InputMask=replicate('X',60)

  func oDESTIP_3_103.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oStr_3_53 as StdString with uid="AABGXQEKMR",Visible=.t., Left=8, Top=226,;
    Alignment=1, Width=126, Height=15,;
    Caption="Cod. zona:"  ;
  , bGlobalFont=.t.

  add object oStr_3_55 as StdString with uid="XHIDLSLYVX",Visible=.t., Left=2, Top=8,;
    Alignment=1, Width=126, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_60 as StdString with uid="IYBNYQFLZW",Visible=.t., Left=8, Top=253,;
    Alignment=1, Width=126, Height=15,;
    Caption="Codice agente:"  ;
  , bGlobalFont=.t.

  func oStr_3_60.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_3_62 as StdString with uid="QEUDSZAUVL",Visible=.t., Left=5, Top=386,;
    Alignment=0, Width=129, Height=18,;
    Caption="Altri dati"  ;
  , bGlobalFont=.t.

  add object oStr_3_64 as StdString with uid="RBLORRHYWX",Visible=.t., Left=8, Top=37,;
    Alignment=1, Width=126, Height=15,;
    Caption="Cat.commerciale:"  ;
  , bGlobalFont=.t.

  add object oStr_3_66 as StdString with uid="MXIVGMPMJX",Visible=.t., Left=8, Top=172,;
    Alignment=1, Width=126, Height=15,;
    Caption="Cod. listino:"  ;
  , bGlobalFont=.t.

  add object oStr_3_68 as StdString with uid="BBUXWSMEHT",Visible=.t., Left=8, Top=145,;
    Alignment=1, Width=126, Height=15,;
    Caption="Cod. valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_3_69 as StdString with uid="ICTHTHKNDN",Visible=.t., Left=8, Top=199,;
    Alignment=1, Width=126, Height=15,;
    Caption="Sconti/maggiorazioni:"  ;
  , bGlobalFont=.t.

  func oStr_3_69.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_3_70 as StdString with uid="OHDDDTCIWJ",Visible=.t., Left=189, Top=199,;
    Alignment=0, Width=7, Height=15,;
    Caption="+"  ;
  , bGlobalFont=.t.

  func oStr_3_70.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_3_71 as StdString with uid="OFWOMMSVOQ",Visible=.t., Left=8, Top=118,;
    Alignment=1, Width=126, Height=15,;
    Caption="Cod. lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_3_72 as StdString with uid="EHKLCELNXS",Visible=.t., Left=1, Top=91,;
    Alignment=1, Width=133, Height=15,;
    Caption="Categoria provvigioni:"  ;
  , bGlobalFont=.t.

  func oStr_3_72.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_3_73 as StdString with uid="ATFNCMUKQP",Visible=.t., Left=8, Top=64,;
    Alignment=1, Width=126, Height=15,;
    Caption="Categoria sconti/mag.:"  ;
  , bGlobalFont=.t.

  func oStr_3_73.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_3_74 as StdString with uid="KJWYVOXOCH",Visible=.t., Left=511, Top=87,;
    Alignment=1, Width=74, Height=18,;
    Caption="Fatturazione:"  ;
  , bGlobalFont=.t.

  func oStr_3_74.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_3_75 as StdString with uid="CTRAQBCLGW",Visible=.t., Left=476, Top=110,;
    Alignment=1, Width=109, Height=15,;
    Caption="Cond.di consegna:"  ;
  , bGlobalFont=.t.

  func oStr_3_75.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_3_76 as StdString with uid="KQANJEYRLU",Visible=.t., Left=523, Top=38,;
    Alignment=1, Width=91, Height=15,;
    Caption="CONAI:"  ;
  , bGlobalFont=.t.

  func oStr_3_76.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_3_79 as StdString with uid="QJHESAJQJY",Visible=.t., Left=9, Top=280,;
    Alignment=1, Width=125, Height=18,;
    Caption="Mag. preferenziale:"  ;
  , bGlobalFont=.t.

  func oStr_3_79.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_3_81 as StdString with uid="ARKKOTMTWT",Visible=.t., Left=540, Top=414,;
    Alignment=1, Width=137, Height=18,;
    Caption="Suff. codici ricerca:"  ;
  , bGlobalFont=.t.

  func oStr_3_81.mHide()
    with this.Parent.oContained
      return (g_FLCESC<>'S')
    endwith
  endfunc

  add object oStr_3_83 as StdString with uid="NVGPYSMOVY",Visible=.t., Left=462, Top=255,;
    Alignment=1, Width=70, Height=18,;
    Caption="Imballo:"  ;
  , bGlobalFont=.t.

  func oStr_3_83.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_3_85 as StdString with uid="STWKWUGQOL",Visible=.t., Left=471, Top=282,;
    Alignment=1, Width=61, Height=18,;
    Caption="Trasporto:"  ;
  , bGlobalFont=.t.

  func oStr_3_85.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_3_86 as StdString with uid="AWEOCTREXM",Visible=.t., Left=9, Top=309,;
    Alignment=1, Width=125, Height=18,;
    Caption="Per conto di..:"  ;
  , bGlobalFont=.t.

  add object oStr_3_87 as StdString with uid="MZWGSEIWCL",Visible=.t., Left=489, Top=233,;
    Alignment=0, Width=149, Height=18,;
    Caption="Metodi di calcolo spese"  ;
  , bGlobalFont=.t.

  func oStr_3_87.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_3_89 as StdString with uid="OEWKKMPQBW",Visible=.t., Left=2, Top=336,;
    Alignment=1, Width=132, Height=18,;
    Caption="Codice porto/aeroporto:"  ;
  , bGlobalFont=.t.

  func oStr_3_89.mHide()
    with this.Parent.oContained
      return (g_ISONAZ<>'ESP')
    endwith
  endfunc

  add object oStr_3_91 as StdString with uid="PJIICSMGFY",Visible=.t., Left=489, Top=62,;
    Alignment=1, Width=127, Height=18,;
    Caption="Gestione cauzioni:"  ;
  , bGlobalFont=.t.

  func oStr_3_91.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S')
    endwith
  endfunc

  add object oStr_3_92 as StdString with uid="UEVQNRFMDX",Visible=.t., Left=452, Top=201,;
    Alignment=1, Width=81, Height=18,;
    Caption="Gr. output:"  ;
  , bGlobalFont=.t.

  add object oStr_3_95 as StdString with uid="QVMCPQMRTY",Visible=.t., Left=465, Top=255,;
    Alignment=1, Width=67, Height=18,;
    Caption="Generali:"  ;
  , bGlobalFont=.t.

  func oStr_3_95.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oStr_3_96 as StdString with uid="ZLPAEDIYIS",Visible=.t., Left=462, Top=282,;
    Alignment=1, Width=70, Height=18,;
    Caption="Cassa Pr.:"  ;
  , bGlobalFont=.t.

  func oStr_3_96.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oStr_3_100 as StdString with uid="SVPJDHOGOC",Visible=.t., Left=489, Top=233,;
    Alignment=0, Width=157, Height=18,;
    Caption="Spese gen. e Cassa Prev."  ;
  , bGlobalFont=.t.

  func oStr_3_100.mHide()
    with this.Parent.oContained
      return (NOT IsAlt())
    endwith
  endfunc

  add object oStr_3_101 as StdString with uid="SNRWBBVMJS",Visible=.t., Left=463, Top=175,;
    Alignment=1, Width=85, Height=18,;
    Caption="Fattura CBI:"  ;
  , bGlobalFont=.t.

  func oStr_3_101.mHide()
    with this.Parent.oContained
      return (g_FAEL<>'S')
    endwith
  endfunc

  add object oStr_3_102 as StdString with uid="TJSKLTAVOC",Visible=.t., Left=31, Top=280,;
    Alignment=1, Width=103, Height=18,;
    Caption="Tipologia clientela:"  ;
  , bGlobalFont=.t.

  func oStr_3_102.mHide()
    with this.Parent.oContained
      return (!Isalt())
    endwith
  endfunc

  add object oStr_3_104 as StdString with uid="HRWLFVUWUT",Visible=.t., Left=20, Top=365,;
    Alignment=1, Width=114, Height=18,;
    Caption="Periodicit�:"  ;
  , bGlobalFont=.t.

  func oStr_3_104.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oBox_3_80 as StdBox with uid="JVKDYQGSXW",left=2, top=404, width=726,height=1

  add object oBox_3_99 as StdBox with uid="KXKNPPIRBL",left=483, top=248, width=219,height=1
enddefine
define class tgsar_aclPag4 as StdContainer
  Width  = 769
  height = 554
  stdWidth  = 769
  stdheight = 554
  resizeXpos=296
  resizeYpos=305
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_4_1 as StdField with uid="ABHIYUGNCW",rtseq=176,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 163847206,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=114, Top=8, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15)

  add object oRAG1_4_2 as StdField with uid="ONBGYQXRTQ",rtseq=177,rtrep=.f.,;
    cFormVar = "w_RAG1", cQueryName = "RAG1",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 162283286,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=259, Top=8, InputMask=replicate('X',40)

  add object oANCODPAG_4_4 as StdField with uid="AKEPZSFPPD",rtseq=179,rtrep=.f.,;
    cFormVar = "w_ANCODPAG", cQueryName = "ANCODPAG",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Codice del pagamento praticato normalmente al cliente",;
    HelpContextID = 32896179,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=114, Top=34, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_ANCODPAG"

  func oANCODPAG_4_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODPAG_4_4.ecpDrop(oSource)
    this.Parent.oContained.link_4_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODPAG_4_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oANCODPAG_4_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oANCODPAG_4_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_ANCODPAG
     i_obj.ecpSave()
  endproc

  add object oDESPAG_4_5 as StdField with uid="LNHIRUXQRP",rtseq=180,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 81527350,;
   bGlobalFont=.t.,;
    Height=21, Width=226, Left=178, Top=34, InputMask=replicate('X',30)

  add object oANGIOFIS_4_6 as StdField with uid="FBHODWNPUZ",rtseq=181,rtrep=.f.,;
    cFormVar = "w_ANGIOFIS", cQueryName = "ANGIOFIS",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Eventuale giorno fisso di scadenza",;
    HelpContextID = 78924633,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=502, Top=34, cSayPict='"99"', cGetPict='"99"'

  func oANGIOFIS_4_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ANGIOFIS <= 31)
    endwith
    return bRes
  endfunc

  add object oANFLINCA_4_7 as StdCheck with uid="JRGSODICYN",rtseq=182,rtrep=.f.,left=595, top=33, caption="Escludi spese incasso",;
    ToolTipText = "Se attivo: non applica mai le spese incasso",;
    HelpContextID = 61392057,;
    cFormVar="w_ANFLINCA", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oANFLINCA_4_7.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oANFLINCA_4_7.GetRadio()
    this.Parent.oContained.w_ANFLINCA = this.RadioValue()
    return .t.
  endfunc

  func oANFLINCA_4_7.SetRadio()
    this.Parent.oContained.w_ANFLINCA=trim(this.Parent.oContained.w_ANFLINCA)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLINCA=='S',1,;
      0)
  endfunc

  add object oANSPEINC_4_8 as StdField with uid="LDMYZCPRQK",rtseq=183,rtrep=.f.,;
    cFormVar = "w_ANSPEINC", cQueryName = "ANSPEINC",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo delle spese di incasso riferite alla valuta e al codice pagamento del cliente, se zero � ignorato",;
    HelpContextID = 119278409,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=667, Top=61, cSayPict='"999999.99"', cGetPict='"999999.99"'

  func oANSPEINC_4_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ANCODVAL) AND NOT EMPTY(.w_ANCODPAG) AND .w_ANFLINCA<>'S')
    endwith
   endif
  endfunc

  add object oAN1MESCL_4_9 as StdField with uid="WRYLSJMMNX",rtseq=184,rtrep=.f.,;
    cFormVar = "w_AN1MESCL", cQueryName = "AN1MESCL",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Primo mese in cui le scadenze sono rinviate al mese successivo",;
    HelpContextID = 250156206,;
   bGlobalFont=.t.,;
    Height=21, Width=29, Left=114, Top=61, cSayPict='"99"', cGetPict='"99"'

  func oAN1MESCL_4_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_AN1MESCL<13 AND (.w_AN1MESCL<.w_AN2MESCL OR .w_AN2MESCL=0))
    endwith
    return bRes
  endfunc

  add object oDESMES1_4_10 as StdField with uid="PXGDKXZLSN",rtseq=185,rtrep=.f.,;
    cFormVar = "w_DESMES1", cQueryName = "DESMES1",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(12), bMultilanguage =  .f.,;
    HelpContextID = 250019274,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=154, Top=61, InputMask=replicate('X',12)

  add object oANGIOSC1_4_11 as StdField with uid="NNEIHYJDMS",rtseq=186,rtrep=.f.,;
    cFormVar = "w_ANGIOSC1", cQueryName = "ANGIOSC1",;
    bObbl = .t. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorno del mese successivo al 1^mese escluso a cui viene rinviata la scadenza",;
    HelpContextID = 239842505,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=374, Top=61, cSayPict='"99"', cGetPict='"99"'

  func oANGIOSC1_4_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AN1MESCL>0)
    endwith
   endif
  endfunc

  func oANGIOSC1_4_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ANGIOSC1 <= 31)
    endwith
    return bRes
  endfunc

  add object oAN2MESCL_4_12 as StdField with uid="MXRIIMLZQF",rtseq=187,rtrep=.f.,;
    cFormVar = "w_AN2MESCL", cQueryName = "AN2MESCL",;
    bObbl = .f. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Secondo mese in cui le scadenze sono rinviate al mese successivo",;
    HelpContextID = 250152110,;
   bGlobalFont=.t.,;
    Height=21, Width=29, Left=114, Top=89, cSayPict='"99"', cGetPict='"99"'

  func oAN2MESCL_4_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AN1MESCL > 0)
    endwith
   endif
  endfunc

  func oAN2MESCL_4_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_AN2MESCL<13 AND (.w_AN1MESCL<.w_AN2MESCL OR .w_AN2MESCL=0))
    endwith
    return bRes
  endfunc

  add object oDESMES2_4_13 as StdField with uid="PQQBQRPRKX",rtseq=188,rtrep=.f.,;
    cFormVar = "w_DESMES2", cQueryName = "DESMES2",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(12), bMultilanguage =  .f.,;
    HelpContextID = 250019274,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=154, Top=87, InputMask=replicate('X',12)

  add object oANGIOSC2_4_14 as StdField with uid="EXHZKYTQBV",rtseq=189,rtrep=.f.,;
    cFormVar = "w_ANGIOSC2", cQueryName = "ANGIOSC2",;
    bObbl = .t. , nPag = 4, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorno del mese successivo al 2^mese escluso a cui viene rinviata la scadenza",;
    HelpContextID = 239842504,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=374, Top=87, cSayPict='"99"', cGetPict='"99"'

  func oANGIOSC2_4_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AN2MESCL>0)
    endwith
   endif
  endfunc

  func oANGIOSC2_4_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_ANGIOSC2 <= 31)
    endwith
    return bRes
  endfunc


  add object oBtn_4_15 as StdButton with uid="HDTPBMCTVA",left=441, top=61, width=48,height=45,;
    CpPicture="bmp\SOLDIT.BMP", caption="", nPag=4;
    , ToolTipText = "Premere per accedere alle partite relative al cliente";
    , HelpContextID = 192803082;
    , Caption='\<Partite';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_15.Click()
      with this.Parent.oContained
        GSAR_BKK(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_15.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_4_15.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_COGE<>'S')
     endwith
    endif
  endfunc


  add object oBtn_4_16 as StdButton with uid="NKIXVEUYVE",left=493, top=61, width=51,height=45,;
    CpPicture="BMP\estratto.bmp", caption="", nPag=4;
    , ToolTipText = "Premere per accedere all'estratto conto relativo al cliente";
    , HelpContextID = 265336564;
    , Caption='\<Estr.Conto';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_4_16.Click()
      with this.Parent.oContained
        GSAR_BKK(this.Parent.oContained,"F")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_4_16.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.cFunction<>'Load' AND NOT EMPTY(.w_ANCODICE))
      endwith
    endif
  endfunc

  func oBtn_4_16.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_COGE<>'S')
     endwith
    endif
  endfunc

  add object oANCODBAN_4_17 as StdField with uid="SGACBLWUMN",rtseq=190,rtrep=.f.,;
    cFormVar = "w_ANCODBAN", cQueryName = "ANCODBAN",;
    bObbl = .f. , nPag = 4, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Banca di appoggio del cliente; riportata sui doc.se R.bancaria o tratta",;
    HelpContextID = 267777196,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=114, Top=114, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_ANCODBAN"

  func oANCODBAN_4_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODBAN_4_17.ecpDrop(oSource)
    this.Parent.oContained.link_4_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODBAN_4_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oANCODBAN_4_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABA',"Elenco banche",'',this.parent.oContained
  endproc
  proc oANCODBAN_4_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_ANCODBAN
     i_obj.ecpSave()
  endproc

  add object oDESBAN_4_18 as StdField with uid="YQQQWUYRUV",rtseq=191,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(42), bMultilanguage =  .f.,;
    HelpContextID = 198050358,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=247, Top=114, InputMask=replicate('X',42)

  add object oDESBA2_4_21 as StdField with uid="TJMOSEPKNW",rtseq=194,rtrep=.f.,;
    cFormVar = "w_DESBA2", cQueryName = "DESBA2",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(42), bMultilanguage =  .f.,;
    HelpContextID = 265159222,;
   bGlobalFont=.t.,;
    Height=21, Width=261, Left=247, Top=139, InputMask=replicate('X',42)

  add object oANCODBA2_4_23 as StdField with uid="UYWAYFXXOY",rtseq=196,rtrep=.f.,;
    cFormVar = "w_ANCODBA2", cQueryName = "ANCODBA2",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente o obsoleto o di tipo salvo buon fine",;
    ToolTipText = "Ns. C/Corrente, comunicato al cliente per incasso bonifici",;
    HelpContextID = 267777224,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=114, Top=139, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_ANCODBA2"

  func oANCODBA2_4_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODBA2_4_23.ecpDrop(oSource)
    this.Parent.oContained.link_4_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODBA2_4_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oANCODBA2_4_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Elenco conti banche",'GSAR_ACL.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oANCODBA2_4_23.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_ANCODBA2
     i_obj.ecpSave()
  endproc


  add object oLinkPC_4_26 as stdDynamicChildContainer with uid="JZLXSYFZPA",left=4, top=163, width=720, height=271, bOnScreen=.t.;



  add object oANFLRAGG_4_27 as StdCombo with uid="UPIENSEIQP",value=1,rtseq=199,rtrep=.f.,left=138,top=462,width=153,height=21;
    , ToolTipText = "Condiziona l'accorpamento di effetti nelle distinte";
    , HelpContextID = 1623219;
    , cFormVar="w_ANFLRAGG",RowSource=""+"No,"+"Solo fatture,"+"Fatture e note di credito", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oANFLRAGG_4_27.RadioValue()
    return(iif(this.value =1,' ',;
    iif(this.value =2,'S',;
    iif(this.value =3,'T',;
    ''))))
  endfunc
  func oANFLRAGG_4_27.GetRadio()
    this.Parent.oContained.w_ANFLRAGG = this.RadioValue()
    return .t.
  endfunc

  func oANFLRAGG_4_27.SetRadio()
    this.Parent.oContained.w_ANFLRAGG=trim(this.Parent.oContained.w_ANFLRAGG)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLRAGG=='',1,;
      iif(this.Parent.oContained.w_ANFLRAGG=='S',2,;
      iif(this.Parent.oContained.w_ANFLRAGG=='T',3,;
      0)))
  endfunc

  add object oANFLGAVV_4_28 as StdCheck with uid="ZPVJCGFOOU",rtseq=200,rtrep=.f.,left=337, top=462, caption="Invio avviso",;
    ToolTipText = "Se attivo seleziona in automatico il cliente nella stampa avvisi",;
    HelpContextID = 13157540,;
    cFormVar="w_ANFLGAVV", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oANFLGAVV_4_28.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANFLGAVV_4_28.GetRadio()
    this.Parent.oContained.w_ANFLGAVV = this.RadioValue()
    return .t.
  endfunc

  func oANFLGAVV_4_28.SetRadio()
    this.Parent.oContained.w_ANFLGAVV=trim(this.Parent.oContained.w_ANFLGAVV)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLGAVV=='S',1,;
      0)
  endfunc

  add object oANDATAVV_4_32 as StdField with uid="VPBDKGVUIF",rtseq=203,rtrep=.f.,;
    cFormVar = "w_ANDATAVV", cQueryName = "ANDATAVV",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di avviso",;
    HelpContextID = 255140,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=647, Top=462

  add object oANIDRIDY_4_33 as StdCheck with uid="BDWUZIRLUH",rtseq=204,rtrep=.f.,left=138, top=488, caption="Id. bancario RID",;
    ToolTipText = "Check id RID alternativo",;
    HelpContextID = 136352929,;
    cFormVar="w_ANIDRIDY", bObbl = .f. , nPag = 4;
   , bGlobalFont=.t.


  func oANIDRIDY_4_33.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANIDRIDY_4_33.GetRadio()
    this.Parent.oContained.w_ANIDRIDY = this.RadioValue()
    return .t.
  endfunc

  func oANIDRIDY_4_33.SetRadio()
    this.Parent.oContained.w_ANIDRIDY=trim(this.Parent.oContained.w_ANIDRIDY)
    this.value = ;
      iif(this.Parent.oContained.w_ANIDRIDY=='S',1,;
      0)
  endfunc


  add object oANTIIDRI_4_34 as StdCombo with uid="JNKAJEOQHZ",rtseq=205,rtrep=.f.,left=337,top=488,width=153,height=21;
    , HelpContextID = 229303473;
    , cFormVar="w_ANTIIDRI",RowSource=""+"Utenza,"+"Matricola,"+"Codice fiscale,"+"Cliente,"+"Portafoglio commerciale,"+"Altro", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oANTIIDRI_4_34.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,3,;
    iif(this.value =4,4,;
    iif(this.value =5,6,;
    iif(this.value =6,9,;
    0)))))))
  endfunc
  func oANTIIDRI_4_34.GetRadio()
    this.Parent.oContained.w_ANTIIDRI = this.RadioValue()
    return .t.
  endfunc

  func oANTIIDRI_4_34.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_ANTIIDRI==1,1,;
      iif(this.Parent.oContained.w_ANTIIDRI==2,2,;
      iif(this.Parent.oContained.w_ANTIIDRI==3,3,;
      iif(this.Parent.oContained.w_ANTIIDRI==4,4,;
      iif(this.Parent.oContained.w_ANTIIDRI==6,5,;
      iif(this.Parent.oContained.w_ANTIIDRI==9,6,;
      0))))))
  endfunc

  func oANTIIDRI_4_34.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANIDRIDY = 'S')
    endwith
   endif
  endfunc

  add object oANIBARID_4_35 as StdField with uid="SFSPLOEQHI",rtseq=206,rtrep=.f.,;
    cFormVar = "w_ANIBARID", cQueryName = "ANIBARID",;
    bObbl = .f. , nPag = 4, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Identificativo bancario RID",;
    HelpContextID = 265120586,;
   bGlobalFont=.t.,;
    Height=21, Width=125, Left=601, Top=486, InputMask=replicate('X',16)

  func oANIBARID_4_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANIDRIDY='S')
    endwith
   endif
  endfunc

  add object oANPAGFOR_4_36 as StdField with uid="NFRHTZDTOP",rtseq=207,rtrep=.f.,;
    cFormVar = "w_ANPAGFOR", cQueryName = "ANPAGFOR",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice pagamento da utilizzare nella generazione fatture differite per l'evasione dei documenti privi del flag scadenze confermate attivo",;
    HelpContextID = 70048600,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=138, Top=515, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_ANPAGFOR"

  func oANPAGFOR_4_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oANPAGFOR_4_36.ecpDrop(oSource)
    this.Parent.oContained.link_4_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANPAGFOR_4_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oANPAGFOR_4_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oANPAGFOR_4_36.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_ANPAGFOR
     i_obj.ecpSave()
  endproc

  add object oPADESCRI_4_59 as StdField with uid="YZFAMOBGBF",rtseq=256,rtrep=.f.,;
    cFormVar = "w_PADESCRI", cQueryName = "PADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 235925697,;
   bGlobalFont=.t.,;
    Height=21, Width=280, Left=210, Top=515, InputMask=replicate('X',30)

  add object oStr_4_31 as StdString with uid="VSOQYHTBWP",Visible=.t., Left=3, Top=8,;
    Alignment=1, Width=107, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_37 as StdString with uid="CSONFRHQOZ",Visible=.t., Left=3, Top=114,;
    Alignment=1, Width=107, Height=15,;
    Caption="Banca del cliente:"  ;
  , bGlobalFont=.t.

  add object oStr_4_38 as StdString with uid="MGNYTKXQCV",Visible=.t., Left=3, Top=34,;
    Alignment=1, Width=107, Height=15,;
    Caption="Cod. pagamento:"  ;
  , bGlobalFont=.t.

  add object oStr_4_39 as StdString with uid="VGENVVRSIJ",Visible=.t., Left=3, Top=61,;
    Alignment=1, Width=107, Height=15,;
    Caption="1^ Mese escluso:"  ;
  , bGlobalFont=.t.

  add object oStr_4_40 as StdString with uid="HSINJFPCSC",Visible=.t., Left=3, Top=87,;
    Alignment=1, Width=107, Height=15,;
    Caption="2^ Mese escluso:"  ;
  , bGlobalFont=.t.

  add object oStr_4_41 as StdString with uid="LEGCZIDJTX",Visible=.t., Left=272, Top=61,;
    Alignment=1, Width=98, Height=15,;
    Caption="Rinvio al:"  ;
  , bGlobalFont=.t.

  add object oStr_4_42 as StdString with uid="UMOZXAHNXX",Visible=.t., Left=274, Top=87,;
    Alignment=1, Width=96, Height=15,;
    Caption="Rinvio al:"  ;
  , bGlobalFont=.t.

  add object oStr_4_43 as StdString with uid="ORUCDKIURC",Visible=.t., Left=546, Top=462,;
    Alignment=1, Width=100, Height=15,;
    Caption="Ultimo avviso:"  ;
  , bGlobalFont=.t.

  add object oStr_4_44 as StdString with uid="GANIXFARCK",Visible=.t., Left=3, Top=139,;
    Alignment=1, Width=107, Height=15,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  add object oStr_4_45 as StdString with uid="CWESUEYPRB",Visible=.t., Left=409, Top=34,;
    Alignment=1, Width=92, Height=15,;
    Caption="Giorno fisso:"  ;
  , bGlobalFont=.t.

  add object oStr_4_47 as StdString with uid="RIMIXJDIRY",Visible=.t., Left=4, Top=438,;
    Alignment=0, Width=66, Height=15,;
    Caption="Altri dati"  ;
  , bGlobalFont=.t.

  add object oStr_4_49 as StdString with uid="SNOEMRRRXS",Visible=.t., Left=510, Top=489,;
    Alignment=1, Width=89, Height=18,;
    Caption="Id.bancario RID:"  ;
  , bGlobalFont=.t.

  add object oStr_4_51 as StdString with uid="WCNPAEOJHC",Visible=.t., Left=548, Top=61,;
    Alignment=1, Width=117, Height=18,;
    Caption="Spese incasso:"  ;
  , bGlobalFont=.t.

  add object oStr_4_52 as StdString with uid="RJKZATLUCC",Visible=.t., Left=4, Top=462,;
    Alignment=1, Width=124, Height=18,;
    Caption="Raggruppa scadenze:"  ;
  , bGlobalFont=.t.

  add object oStr_4_54 as StdString with uid="WTTPBZPEQP",Visible=.t., Left=302, Top=489,;
    Alignment=1, Width=30, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_4_58 as StdString with uid="SPUXBYISWM",Visible=.t., Left=-2, Top=518,;
    Alignment=1, Width=130, Height=15,;
    Caption="Pagamento forzato:"  ;
  , bGlobalFont=.t.

  add object oBox_4_46 as StdBox with uid="QKPUTGLFKC",left=1, top=453, width=725,height=3
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsar_mcc",lower(this.oContained.GSAR_MCC.class))=0
        this.oContained.GSAR_MCC.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsar_aclPag5 as StdContainer
  Width  = 769
  height = 554
  stdWidth  = 769
  stdheight = 554
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oANGESCON_5_1 as StdCombo with uid="NYQOTWVXPC",rtseq=211,rtrep=.f.,left=131,top=42,width=127,height=21;
    , HelpContextID = 32525140;
    , cFormVar="w_ANGESCON",RowSource=""+"Nessun blocco,"+"Moratoria attivabile,"+"Blocco attivabile", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oANGESCON_5_1.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'M',;
    iif(this.value =3,'B',;
    space(1)))))
  endfunc
  func oANGESCON_5_1.GetRadio()
    this.Parent.oContained.w_ANGESCON = this.RadioValue()
    return .t.
  endfunc

  func oANGESCON_5_1.SetRadio()
    this.Parent.oContained.w_ANGESCON=trim(this.Parent.oContained.w_ANGESCON)
    this.value = ;
      iif(this.Parent.oContained.w_ANGESCON=='N',1,;
      iif(this.Parent.oContained.w_ANGESCON=='M',2,;
      iif(this.Parent.oContained.w_ANGESCON=='B',3,;
      0)))
  endfunc

  add object oANFLGCON_5_2 as StdCheck with uid="FAAXTTOOAV",rtseq=212,rtrep=.f.,left=282, top=41, caption="Bloccato per contenzioso",;
    HelpContextID = 20396884,;
    cFormVar="w_ANFLGCON", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oANFLGCON_5_2.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oANFLGCON_5_2.GetRadio()
    this.Parent.oContained.w_ANFLGCON = this.RadioValue()
    return .t.
  endfunc

  func oANFLGCON_5_2.SetRadio()
    this.Parent.oContained.w_ANFLGCON=trim(this.Parent.oContained.w_ANFLGCON)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLGCON=='S',1,;
      0)
  endfunc

  func oANFLGCON_5_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANGESCON='B')
    endwith
   endif
  endfunc

  add object oANPAGPAR_5_3 as StdField with uid="WZJVBONSIB",rtseq=213,rtrep=.f.,;
    cFormVar = "w_ANPAGPAR", cQueryName = "ANPAGPAR",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice pagamento inesistente oppure obsoleto",;
    ToolTipText = "Codice pagamento praticato al cliente in particolari situazioni (es.moratoria fido ecc.)",;
    HelpContextID = 30614696,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=131, Top=80, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="PAG_AMEN", cZoomOnZoom="GSAR_APA", oKey_1_1="PACODICE", oKey_1_2="this.w_ANPAGPAR"

  func oANPAGPAR_5_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANGESCON='M')
    endwith
   endif
  endfunc

  func oANPAGPAR_5_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oANPAGPAR_5_3.ecpDrop(oSource)
    this.Parent.oContained.link_5_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANPAGPAR_5_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PAG_AMEN','*','PACODICE',cp_AbsName(this.parent,'oANPAGPAR_5_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_APA',"Pagamenti",'',this.parent.oContained
  endproc
  proc oANPAGPAR_5_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_APA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PACODICE=this.parent.oContained.w_ANPAGPAR
     i_obj.ecpSave()
  endproc

  add object oANDATMOR_5_4 as StdField with uid="AWLTHJZFZO",rtseq=214,rtrep=.f.,;
    cFormVar = "w_ANDATMOR", cQueryName = "ANDATMOR",;
    bObbl = .f. , nPag = 5, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data limite di applicazione della condizione di pagamento particolare",;
    HelpContextID = 201071448,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=531, Top=80

  func oANDATMOR_5_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_ANPAGPAR))
    endwith
   endif
  endfunc

  add object oANFLESIM_5_5 as StdCheck with uid="YWJNOPGXNM",rtseq=215,rtrep=.f.,left=131, top=114, caption="Escludi applicazione interessi di mora",;
    ToolTipText = "Se attivo il cliente viene escluso dall'applicazione degli interessi di mora",;
    HelpContextID = 18299731,;
    cFormVar="w_ANFLESIM", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oANFLESIM_5_5.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oANFLESIM_5_5.GetRadio()
    this.Parent.oContained.w_ANFLESIM = this.RadioValue()
    return .t.
  endfunc

  func oANFLESIM_5_5.SetRadio()
    this.Parent.oContained.w_ANFLESIM=trim(this.Parent.oContained.w_ANFLESIM)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLESIM=='S',1,;
      0)
  endfunc

  add object oANSAGINT_5_6 as StdField with uid="DXFDMRMDCA",rtseq=216,rtrep=.f.,;
    cFormVar = "w_ANSAGINT", cQueryName = "ANSAGINT",;
    bObbl = .f. , nPag = 5, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saggio di interesse concordato e applicato al posto di quello della tabella saggio interesse di mora. Se attivo il flag spread su saggio di mora, la percentuale rappresenta il tasso da aggiungere o togliere dal saggio di mora definito in tabella",;
    HelpContextID = 120392538,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=531, Top=115, cSayPict='"999.99"', cGetPict='"999.99"'

  func oANSAGINT_5_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLESIM<>'S')
    endwith
   endif
  endfunc

  func oANSAGINT_5_6.mHide()
    with this.Parent.oContained
      return (.w_ANFLESIM='S')
    endwith
  endfunc

  add object oANSPRINT_5_7 as StdCheck with uid="LXHJDEPXRD",rtseq=217,rtrep=.f.,left=600, top=114, caption="Spread su saggio di mora",;
    ToolTipText = "Se attivo la percentuale specificata nel campo interesse di mora rappresenta il tasso da aggiungere o togliere dal tasso di mora definito nella tabella",;
    HelpContextID = 132909914,;
    cFormVar="w_ANSPRINT", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oANSPRINT_5_7.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANSPRINT_5_7.GetRadio()
    this.Parent.oContained.w_ANSPRINT = this.RadioValue()
    return .t.
  endfunc

  func oANSPRINT_5_7.SetRadio()
    this.Parent.oContained.w_ANSPRINT=trim(this.Parent.oContained.w_ANSPRINT)
    this.value = ;
      iif(this.Parent.oContained.w_ANSPRINT=='S',1,;
      0)
  endfunc

  func oANSPRINT_5_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANSAGINT<>0)
    endwith
   endif
  endfunc

  func oANSPRINT_5_7.mHide()
    with this.Parent.oContained
      return (.w_ANFLESIM='S')
    endwith
  endfunc

  add object oDESPAR_5_8 as StdField with uid="BDMFKRSUFI",rtseq=218,rtrep=.f.,;
    cFormVar = "w_DESPAR", cQueryName = "DESPAR",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 266076726,;
   bGlobalFont=.t.,;
    Height=21, Width=219, Left=194, Top=80, InputMask=replicate('X',30)

  add object oCODI_5_13 as StdField with uid="UIZPFJBFFE",rtseq=219,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 163847206,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=131, Top=8, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15)

  add object oRAG1_5_14 as StdField with uid="FOMQJTAWKM",rtseq=220,rtrep=.f.,;
    cFormVar = "w_RAG1", cQueryName = "RAG1",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 162283286,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=282, Top=8, InputMask=replicate('X',40)

  add object oStr_5_9 as StdString with uid="RBLDDINGKL",Visible=.t., Left=6, Top=80,;
    Alignment=1, Width=122, Height=18,;
    Caption="Pagam.moratoria:"  ;
  , bGlobalFont=.t.

  add object oStr_5_10 as StdString with uid="YMKGIKGMBB",Visible=.t., Left=475, Top=81,;
    Alignment=1, Width=54, Height=15,;
    Caption="Fino a:"  ;
  , bGlobalFont=.t.

  add object oStr_5_11 as StdString with uid="QXBOSGKGHC",Visible=.t., Left=3, Top=43,;
    Alignment=1, Width=125, Height=18,;
    Caption="Gestione contenzioso:"  ;
  , bGlobalFont=.t.

  add object oStr_5_12 as StdString with uid="QTFJFLYOHN",Visible=.t., Left=416, Top=116,;
    Alignment=1, Width=113, Height=18,;
    Caption="Interesse di mora:"  ;
  , bGlobalFont=.t.

  func oStr_5_12.mHide()
    with this.Parent.oContained
      return (.w_ANFLESIM='S')
    endwith
  endfunc

  add object oStr_5_15 as StdString with uid="CZXFBRYBHH",Visible=.t., Left=2, Top=8,;
    Alignment=1, Width=126, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine
define class tgsar_aclPag6 as StdContainer
  Width  = 769
  height = 554
  stdWidth  = 769
  stdheight = 554
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_6_1 as StdField with uid="VVCPDBGJNM",rtseq=221,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 163847206,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=131, Top=8, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15)

  add object oRAG1_6_2 as StdField with uid="IILUFNDEIR",rtseq=222,rtrep=.f.,;
    cFormVar = "w_RAG1", cQueryName = "RAG1",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 162283286,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=282, Top=8, InputMask=replicate('X',40)

  add object oANFLBLVE_6_3 as StdCheck with uid="AVVGOLIDOR",rtseq=223,rtrep=.f.,left=84, top=73, caption="Blocco emissione documenti",;
    ToolTipText = "Se attivo: blocca l�emissione dei documenti al superamento del fido",;
    HelpContextID = 102286517,;
    cFormVar="w_ANFLBLVE", bObbl = .f. , nPag = 6;
   , bGlobalFont=.t.


  func oANFLBLVE_6_3.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oANFLBLVE_6_3.GetRadio()
    this.Parent.oContained.w_ANFLBLVE = this.RadioValue()
    return .t.
  endfunc

  func oANFLBLVE_6_3.SetRadio()
    this.Parent.oContained.w_ANFLBLVE=trim(this.Parent.oContained.w_ANFLBLVE)
    this.value = ;
      iif(this.Parent.oContained.w_ANFLBLVE=='S',1,;
      0)
  endfunc

  func oANFLBLVE_6_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERFID='S' AND .w_ANFLFIDO='S')
    endwith
   endif
  endfunc

  add object oANVALFID_6_4 as StdField with uid="QWLFZAIBKQ",rtseq=224,rtrep=.f.,;
    cFormVar = "w_ANVALFID", cQueryName = "ANVALFID",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo massimo del fido cliente",;
    HelpContextID = 75316042,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=487, Top=106, cSayPict="v_PV[20]", cGetPict="v_GV[20]"

  func oANVALFID_6_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLFIDO='S')
    endwith
   endif
  endfunc

  add object oANMAXORD_6_5 as StdField with uid="BADVFHKDYN",rtseq=225,rtrep=.f.,;
    cFormVar = "w_ANMAXORD", cQueryName = "ANMAXORD",;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo massimo ordinabile dal cliente",;
    HelpContextID = 29578422,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=87, Top=164, cSayPict="v_PV[20]", cGetPict="v_GV[20]"

  func oANMAXORD_6_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ANFLFIDO='S')
    endwith
   endif
  endfunc

  func oANMAXORD_6_5.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFIDDAT_6_11 as StdField with uid="TMJNVUGINC",rtseq=227,rtrep=.f.,;
    cFormVar = "w_FIDDAT", cQueryName = "FIDDAT",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dell'ultima elaborazione del fido",;
    HelpContextID = 238086570,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=87, Top=135

  add object oFIDPAP_6_12 as StdField with uid="OYYJGVWSDK",rtseq=228,rtrep=.f.,;
    cFormVar = "w_FIDPAP", cQueryName = "FIDPAP",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importi partite aperte esclusi effetti in distinta",;
    HelpContextID = 232461910,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=487, Top=135, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oFIDESO_6_13 as StdField with uid="CJJNQXBDRF",rtseq=229,rtrep=.f.,;
    cFormVar = "w_FIDESO", cQueryName = "FIDESO",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importi effetti in distinta in scadenza",;
    HelpContextID = 233838166,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=487, Top=164, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oFIDESC_6_14 as StdField with uid="SHAMSPCYYP",rtseq=230,rtrep=.f.,;
    cFormVar = "w_FIDESC", cQueryName = "FIDESC",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importi effetti scaduti (data elab. - n.gg.tolleranza)",;
    HelpContextID = 32511574,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=487, Top=193, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oFIDORD_6_15 as StdField with uid="INOZGZWKVY",rtseq=231,rtrep=.f.,;
    cFormVar = "w_FIDORD", cQueryName = "FIDORD",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importi ordini aperti, intestati al cliente",;
    HelpContextID = 48895574,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=487, Top=222, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oFIDORD_6_15.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFIDDDT_6_16 as StdField with uid="PBBZZQFQPJ",rtseq=232,rtrep=.f.,;
    cFormVar = "w_FIDDDT", cQueryName = "FIDDDT",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importi DDT non fatturati, intestati al cliente",;
    HelpContextID = 234940842,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=487, Top=251, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oFIDDDT_6_16.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oFIDFAT_6_17 as StdField with uid="DRHOOTVLUE",rtseq=233,rtrep=.f.,;
    cFormVar = "w_FIDFAT", cQueryName = "FIDFAT",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale importi fatture non contabilizzate, intestate al cliente",;
    HelpContextID = 237955498,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=487, Top=280, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  add object oFIDRES1_6_29 as StdField with uid="MXSEDUVIAZ",rtseq=234,rtrep=.f.,;
    cFormVar = "w_FIDRES1", cQueryName = "FIDRES1",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo fido cliente ancora disponibile",;
    HelpContextID = 249751978,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=487, Top=309, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oFIDRES1_6_29.mHide()
    with this.Parent.oContained
      return (.w_FIDRES1<0)
    endwith
  endfunc

  add object oFIDRES2_6_30 as StdField with uid="ERNGWCMYDF",rtseq=235,rtrep=.f.,;
    cFormVar = "w_FIDRES2", cQueryName = "FIDRES2",enabled=.f.,;
    bObbl = .f. , nPag = 6, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo fido cliente ancora disponibile",;
    HelpContextID = 249751978,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=487, Top=309, cSayPict="v_PV(20)", cGetPict="v_GV(20)"

  func oFIDRES2_6_30.mHide()
    with this.Parent.oContained
      return (.w_FIDRES2>=0)
    endwith
  endfunc

  add object oStr_6_6 as StdString with uid="QJTDTAHYFN",Visible=.t., Left=6, Top=8,;
    Alignment=1, Width=122, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_7 as StdString with uid="MGFQGOFABB",Visible=.t., Left=4, Top=164,;
    Alignment=1, Width=81, Height=15,;
    Caption="Max. ordine:"  ;
  , bGlobalFont=.t.

  func oStr_6_7.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_6_9 as StdString with uid="AZIAXQDNXP",Visible=.t., Left=3, Top=40,;
    Alignment=0, Width=170, Height=15,;
    Caption="Rischio cliente"  ;
  , bGlobalFont=.t.

  add object oStr_6_18 as StdString with uid="TRRCYEGLBT",Visible=.t., Left=3, Top=135,;
    Alignment=1, Width=83, Height=15,;
    Caption="Elaborato il:"  ;
  , bGlobalFont=.t.

  add object oStr_6_19 as StdString with uid="IZLNHKADRU",Visible=.t., Left=303, Top=106,;
    Alignment=1, Width=179, Height=15,;
    Caption="Importo fido:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_6_20 as StdString with uid="AQMPBJHWMS",Visible=.t., Left=303, Top=135,;
    Alignment=1, Width=179, Height=15,;
    Caption="Partite aperte (escl.effetti):"  ;
  , bGlobalFont=.t.

  add object oStr_6_21 as StdString with uid="UZULOSBHIW",Visible=.t., Left=303, Top=164,;
    Alignment=1, Width=179, Height=15,;
    Caption="Effetti in scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_6_22 as StdString with uid="QUJYTMCQZK",Visible=.t., Left=303, Top=193,;
    Alignment=1, Width=179, Height=15,;
    Caption="Effetti scaduti:"  ;
  , bGlobalFont=.t.

  add object oStr_6_23 as StdString with uid="WXSCAKIWOK",Visible=.t., Left=303, Top=222,;
    Alignment=1, Width=179, Height=15,;
    Caption="Ordini in essere:"  ;
  , bGlobalFont=.t.

  func oStr_6_23.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_6_24 as StdString with uid="EBNKXZVTKF",Visible=.t., Left=303, Top=251,;
    Alignment=1, Width=179, Height=15,;
    Caption="DDT non fatturati:"  ;
  , bGlobalFont=.t.

  func oStr_6_24.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_6_25 as StdString with uid="KLJXEUKGOD",Visible=.t., Left=303, Top=280,;
    Alignment=1, Width=179, Height=15,;
    Caption="Fatture non contabilizzate:"  ;
  , bGlobalFont=.t.

  add object oStr_6_27 as StdString with uid="GCSOLAMLRK",Visible=.t., Left=345, Top=74,;
    Alignment=0, Width=60, Height=15,;
    Caption="Totali"  ;
  , bGlobalFont=.t.

  add object oStr_6_28 as StdString with uid="HOSSDRHSVW",Visible=.t., Left=303, Top=309,;
    Alignment=1, Width=179, Height=15,;
    Caption="Fido disponibile:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_6_8 as StdBox with uid="FJUBMNQEBI",left=1, top=57, width=727,height=1

  add object oBox_6_26 as StdBox with uid="VXMPVWUJGC",left=345, top=92, width=284,height=1
enddefine
define class tgsar_aclPag7 as StdContainer
  Width  = 769
  height = 554
  stdWidth  = 769
  stdheight = 554
  resizeXpos=304
  resizeYpos=141
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_7_1 as StdField with uid="APGHWERXVH",rtseq=236,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 163847206,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=131, Top=8, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15)

  add object oRAG1_7_2 as StdField with uid="PRIXQQCNUR",rtseq=237,rtrep=.f.,;
    cFormVar = "w_RAG1", cQueryName = "RAG1",enabled=.f.,;
    bObbl = .f. , nPag = 7, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 162283286,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=282, Top=8, InputMask=replicate('X',40)


  add object oLinkPC_7_4 as stdDynamicChildContainer with uid="UMARVJJTPA",left=18, top=47, width=701, height=382, bOnScreen=.t.;


  add object oStr_7_3 as StdString with uid="XRNHBFEWME",Visible=.t., Left=6, Top=8,;
    Alignment=1, Width=122, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsar_mco",lower(this.oContained.GSAR_MCO.class))=0
        this.oContained.GSAR_MCO.createrealchild()
        this.oContained.WriteTo_GSAR_MCO()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsar_aclPag8 as StdContainer
  Width  = 769
  height = 554
  stdWidth  = 769
  stdheight = 554
  resizeXpos=331
  resizeYpos=130
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_8_1 as StdField with uid="VGIGGYTILV",rtseq=238,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 163847206,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=131, Top=8, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15)

  add object oRAG1_8_2 as StdField with uid="FMFRYRSNKG",rtseq=239,rtrep=.f.,;
    cFormVar = "w_RAG1", cQueryName = "RAG1",enabled=.f.,;
    bObbl = .f. , nPag = 8, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 162283286,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=282, Top=8, InputMask=replicate('X',40)


  add object oLinkPC_8_4 as stdDynamicChildContainer with uid="HBZYMLYDJJ",left=17, top=48, width=725, height=491, bOnScreen=.t.;


  add object oStr_8_3 as StdString with uid="OZPGTUWHYX",Visible=.t., Left=7, Top=8,;
    Alignment=1, Width=121, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine
define class tgsar_aclPag9 as StdContainer
  Width  = 769
  height = 554
  stdWidth  = 769
  stdheight = 554
  resizeXpos=451
  resizeYpos=361
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_9_1 as StdField with uid="CLACLNUGRU",rtseq=240,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 163847206,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=131, Top=8, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15)

  add object oRAG1_9_2 as StdField with uid="CYVYNEAKFR",rtseq=241,rtrep=.f.,;
    cFormVar = "w_RAG1", cQueryName = "RAG1",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 162283286,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=282, Top=8, InputMask=replicate('X',40)

  add object oANCODGRU_9_4 as StdField with uid="HKYGCCIADF",rtseq=242,rtrep=.f.,;
    cFormVar = "w_ANCODGRU", cQueryName = "ANCODGRU",;
    bObbl = .f. , nPag = 9, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 183891109,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=123, Top=71, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="GRU_INTE", cZoomOnZoom="GSAR_AGR", oKey_1_1="GRCODICE", oKey_1_2="this.w_ANCODGRU"

  func oANCODGRU_9_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_9_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oANCODGRU_9_4.ecpDrop(oSource)
    this.Parent.oContained.link_9_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANCODGRU_9_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRU_INTE','*','GRCODICE',cp_AbsName(this.parent,'oANCODGRU_9_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGR',"Gruppi intestatari EDI",'',this.parent.oContained
  endproc
  proc oANCODGRU_9_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GRCODICE=this.parent.oContained.w_ANCODGRU
     i_obj.ecpSave()
  endproc


  add object oLinkPC_9_6 as stdDynamicChildContainer with uid="HTFESTCBSO",left=120, top=103, width=467, height=262, bOnScreen=.t.;


  func oLinkPC_9_6.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_VEFA='N')
     endwith
    endif
  endfunc

  add object oDESGRU_9_7 as StdField with uid="MZLQBCCRMA",rtseq=243,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 9, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 203226570,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=209, Top=71, InputMask=replicate('X',35)

  add object oStr_9_3 as StdString with uid="XUWPLTKRZJ",Visible=.t., Left=56, Top=8,;
    Alignment=1, Width=72, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_9_5 as StdString with uid="JOYMDRCTFF",Visible=.t., Left=9, Top=73,;
    Alignment=1, Width=111, Height=18,;
    Caption="Gruppo intestari:"  ;
  , bGlobalFont=.t.

  add object oStr_9_9 as StdString with uid="SCBFEMWDNX",Visible=.t., Left=8, Top=45,;
    Alignment=0, Width=170, Height=18,;
    Caption="Dati EDI"  ;
  , bGlobalFont=.t.

  add object oBox_9_8 as StdBox with uid="EDLVUAHTEH",left=1, top=62, width=727,height=1
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gsar_mse",lower(this.oContained.GSAR_MSE.class))=0
        this.oContained.GSAR_MSE.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgsar_aclPag10 as StdContainer
  Width  = 769
  height = 554
  stdWidth  = 769
  stdheight = 554
  resizeXpos=525
  resizeYpos=379
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODI_10_1 as StdField with uid="PBACFJXBOX",rtseq=244,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 10, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 163847206,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=131, Top=8, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15)

  add object oRAG1_10_2 as StdField with uid="UJLGSIXMNL",rtseq=245,rtrep=.f.,;
    cFormVar = "w_RAG1", cQueryName = "RAG1",enabled=.f.,;
    bObbl = .f. , nPag = 10, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 162283286,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=282, Top=8, InputMask=replicate('X',40)

  add object oAN__NOTE_10_4 as StdMemo with uid="GLUJMEIDFY",rtseq=246,rtrep=.f.,;
    cFormVar = "w_AN__NOTE", cQueryName = "AN__NOTE",;
    bObbl = .f. , nPag = 10, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Eventuali annotazioni sul cliente",;
    HelpContextID = 38024373,;
   bGlobalFont=.t.,;
    Height=492, Width=750, Left=9, Top=40

  add object oStr_10_3 as StdString with uid="ANOKEVZAZQ",Visible=.t., Left=6, Top=8,;
    Alignment=1, Width=122, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine
define class tgsar_aclPag11 as StdContainer
  Width  = 769
  height = 554
  stdWidth  = 769
  stdheight = 554
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oANRATING_11_2 as StdField with uid="LNGADKNCDM",rtseq=247,rtrep=.f.,;
    cFormVar = "w_ANRATING", cQueryName = "ANRATING",;
    bObbl = .f. , nPag = 11, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Livello di esigibilit� per gli incassi",;
    HelpContextID = 134019917,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=131, Top=50, InputMask=replicate('X',2), bHasZoom = .t. , cLinkFile="DORATING", cZoomOnZoom="Gsdf_Ara", oKey_1_1="RACODICE", oKey_1_2="this.w_ANRATING"

  func oANRATING_11_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_11_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oANRATING_11_2.ecpDrop(oSource)
    this.Parent.oContained.link_11_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANRATING_11_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'DORATING','*','RACODICE',cp_AbsName(this.parent,'oANRATING_11_2'),iif(empty(i_cWhere),.f.,i_cWhere),'Gsdf_Ara',"Rating",'',this.parent.oContained
  endproc
  proc oANRATING_11_2.mZoomOnZoom
    local i_obj
    i_obj=Gsdf_Ara()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RACODICE=this.parent.oContained.w_ANRATING
     i_obj.ecpSave()
  endproc

  add object oRADESCRI_11_3 as StdField with uid="LKXITUNVVR",rtseq=248,rtrep=.f.,;
    cFormVar = "w_RADESCRI", cQueryName = "RADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 11, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 235925665,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=178, Top=50, InputMask=replicate('X',50)

  add object oANGIORIT_11_6 as StdField with uid="ACMTTAHCFQ",rtseq=249,rtrep=.f.,;
    cFormVar = "w_ANGIORIT", cQueryName = "ANGIORIT",;
    bObbl = .f. , nPag = 11, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Giorni di ritardo su incassi",;
    HelpContextID = 11815770,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=131, Top=89, cSayPict='"999"', cGetPict='"999"'

  add object oANVOCFIN_11_7 as StdField with uid="LSGWGQGNWC",rtseq=250,rtrep=.f.,;
    cFormVar = "w_ANVOCFIN", cQueryName = "ANVOCFIN",;
    bObbl = .f. , nPag = 11, value=space(6), bMultilanguage =  .f.,;
    ToolTipText = "Voce finanziaria",;
    HelpContextID = 66796372,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=131, Top=132, InputMask=replicate('X',6), bHasZoom = .t. , cLinkFile="VOC_FINZ", cZoomOnZoom="GSDF_AVF", oKey_1_1="DFVOCFIN", oKey_1_2="this.w_ANVOCFIN"

  func oANVOCFIN_11_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_11_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oANVOCFIN_11_7.ecpDrop(oSource)
    this.Parent.oContained.link_11_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oANVOCFIN_11_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VOC_FINZ','*','DFVOCFIN',cp_AbsName(this.parent,'oANVOCFIN_11_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSDF_AVF',"Voci finanziarie",'',this.parent.oContained
  endproc
  proc oANVOCFIN_11_7.mZoomOnZoom
    local i_obj
    i_obj=GSDF_AVF()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_DFVOCFIN=this.parent.oContained.w_ANVOCFIN
     i_obj.ecpSave()
  endproc

  add object oDFDESCRI_11_8 as StdField with uid="LIZWSGVYPA",rtseq=251,rtrep=.f.,;
    cFormVar = "w_DFDESCRI", cQueryName = "DFDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 11, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 235924609,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=204, Top=132, InputMask=replicate('X',60)

  add object oANESCDOF_11_9 as StdCheck with uid="JFHGGHJQQS",rtseq=252,rtrep=.f.,left=131, top=174, caption="Escludi nell'esportazione per DocFinance",;
    ToolTipText = "Escludi nell'esportazione per DocFinance",;
    HelpContextID = 33434444,;
    cFormVar="w_ANESCDOF", bObbl = .f. , nPag = 11;
   , bGlobalFont=.t.


  func oANESCDOF_11_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANESCDOF_11_9.GetRadio()
    this.Parent.oContained.w_ANESCDOF = this.RadioValue()
    return .t.
  endfunc

  func oANESCDOF_11_9.SetRadio()
    this.Parent.oContained.w_ANESCDOF=trim(this.Parent.oContained.w_ANESCDOF)
    this.value = ;
      iif(this.Parent.oContained.w_ANESCDOF=='S',1,;
      0)
  endfunc

  add object oANDESPAR_11_10 as StdCheck with uid="ECCTEFVCFP",rtseq=253,rtrep=.f.,left=131, top=215, caption="Riporta descrizione partita",;
    ToolTipText = "Riporta descrizione partita ",;
    HelpContextID = 17818792,;
    cFormVar="w_ANDESPAR", bObbl = .f. , nPag = 11;
   , bGlobalFont=.t.


  func oANDESPAR_11_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANDESPAR_11_10.GetRadio()
    this.Parent.oContained.w_ANDESPAR = this.RadioValue()
    return .t.
  endfunc

  func oANDESPAR_11_10.SetRadio()
    this.Parent.oContained.w_ANDESPAR=trim(this.Parent.oContained.w_ANDESPAR)
    this.value = ;
      iif(this.Parent.oContained.w_ANDESPAR=='S',1,;
      0)
  endfunc

  add object oCODI_11_11 as StdField with uid="IVFWHECLVT",rtseq=254,rtrep=.f.,;
    cFormVar = "w_CODI", cQueryName = "CODI",enabled=.f.,;
    bObbl = .f. , nPag = 11, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 163847206,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=135, Left=131, Top=8, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15)

  add object oRAG1_11_12 as StdField with uid="NFVUZQDKSE",rtseq=255,rtrep=.f.,;
    cFormVar = "w_RAG1", cQueryName = "RAG1",enabled=.f.,;
    bObbl = .f. , nPag = 11, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 162283286,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=282, Top=8, InputMask=replicate('X',40)

  add object oStr_11_1 as StdString with uid="ILIQAINTSU",Visible=.t., Left=51, Top=51,;
    Alignment=1, Width=77, Height=18,;
    Caption="Rating:"  ;
  , bGlobalFont=.t.

  add object oStr_11_4 as StdString with uid="FWWBRFHIQB",Visible=.t., Left=3, Top=91,;
    Alignment=1, Width=125, Height=18,;
    Caption="Giorni di ritardo:"  ;
  , bGlobalFont=.t.

  add object oStr_11_5 as StdString with uid="VPDDBFRBYL",Visible=.t., Left=3, Top=134,;
    Alignment=1, Width=125, Height=18,;
    Caption="Voce finanziaria:"  ;
  , bGlobalFont=.t.

  add object oStr_11_13 as StdString with uid="CZPUBIXKQL",Visible=.t., Left=6, Top=8,;
    Alignment=1, Width=122, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result="ANTIPCON='C'"
  if lower(right(result,4))='.vqr'
  i_cAliasName = "cp"+Right(SYS(2015),8)
    result=" exists (select 1 from ("+cp_GetSQLFromQuery(result)+") "+i_cAliasName+" where ";
  +" "+i_cAliasName+".ANTIPCON=CONTI.ANTIPCON";
  +" and "+i_cAliasName+".ANCODICE=CONTI.ANCODICE";
  +")"
  endif
  i_res=cp_AppQueryFilter('gsar_acl','CONTI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".ANTIPCON=CONTI.ANTIPCON";
  +" and "+i_cAliasName2+".ANCODICE=CONTI.ANCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsar_acl
* --- Classe per gestire la combo ANCONCON
* --- derivata dalla classe combo da tabella

define class StdZTamTableCombo as StdTableCombo

proc Init()
  This.backcolor=i_nEBackColor
  This.FontName=i_cCboxFontName
  This.FontSize=i_nCboxFontSize
  This.Fontitalic=i_bCboxFontItalic
endproc

  proc Popola()
    local i_nIdx,i_nConn,i_cTable,i_n1,i_n2,i_n3,i_curs,i_bCharKey,i_flt
    LOCAL i_fk,i_fd
    i_curs=sys(2015)
    IF LOWER(RIGHT(this.cTable,4))='.vqr'
      vq_exec(this.cTable,this.parent.ocontained,i_curs)
      i_fk=this.cKey
      i_fd=this.cValue
    else
      i_nIdx=cp_OpenTable(this.cTable)
      if i_nIdx<>0
        i_nConn=i_TableProp[i_nIdx,3]
        i_cTable=cp_SetAzi(i_TableProp[i_nIdx,2])
        i_n1=this.cKey
        i_n2=this.cValue
        IF !EMPTY(this.cOrderBy)
          i_n3=' order by '+this.cOrderBy
        ELSE
          i_n3=''
        ENDIF
        i_flt=IIF(EMPTY(this.tablefilter),'',' where '+this.tablefilter)
        if i_nConn<>0
          cp_sql(i_nConn,"select "+i_n1+" as combokey,"+i_n2+" as combodescr from "+i_cTable+i_flt+i_n3,i_curs)
        else
          select &i_n1 as combokey,&i_n2 as combodescr from (i_cTable) &i_flt &i_n3 into cursor (i_curs)
        ENDIF
        i_fk='combokey'
        i_fd='combodescr'
        cp_CloseTable(this.cTable)
      ENDIF
    ENDIF
    if used(i_curs)
      select (i_curs)
      this.nValues=reccount()
      dimension this.combovalues[MAX(1,this.nValues)]
      If this.nValues<1
       this.combovalues[1]=cp_NullValue(this.RadioValue())
      endif
      this.Clear
      i_bCharKey=type(i_fk)='C'
      do while !eof()
        this.AddItem(iif(type(i_fd)='C',ALLTRIM(&i_fd),ALLTRIM(str(&i_fd))))
        if i_bCharKey
          this.combovalues[recno()]=trim(&i_fk)
        else
          this.combovalues[recno()]=&i_fk
        endif
        skip
      enddo
      use
    endif

enddefine

*Lancia il messaggio quando cambia il flag "Scorporo a piede fattura"
proc MessFlgScorporo(pParent)
   if pParent.cFunction='Edit'
     ah_ErrorMsg("Attenzione: la modifica potrebbe avere influenza su tutti i documenti gi� caricati per il cliente",,'')
   endif
endproc

*--- Lancia stampa
*--- Sono costretto ad utilizzare una funzione perch� non funziona la User Program
proc PrintConto(pParent, cType)
   GSAR_SCL(cType)
endproc


func  ZoomCF
param maschera
*la variabile conta serve per disattivaer il ricalcolo della data di nascita 
maschera.w_conta = maschera.w_conta + 1 
return GSAR_BFC( maschera , 'w_ANCODFIS'  , maschera.w_ANCOGNOM,maschera.w_AN__NOME,maschera.w_ANLOCNAS,maschera.w_ANPRONAS,maschera.w_ANDATNAS,maschera.w_AN_SESSO, , maschera.w_CODISO,maschera.w_CODCOM )
endfunc
* --- Fine Area Manuale
