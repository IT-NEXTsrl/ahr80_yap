* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bdu                                                        *
*              DUPLICAZIONE FESTIVITA                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2008-05-07                                                      *
* Last revis.: 2009-09-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bdu",oParentObject)
return(i_retval)

define class tgsar_bdu as StdBatch
  * --- Local variables
  w_TMPDESCR = space(35)
  w_DATINI = ctod("  /  /  ")
  w_DATFIN = ctod("  /  /  ")
  w_CHKNUM = 0
  w_FEDATFES = ctod("  /  /  ")
  w_FEDESFES = space(35)
  * --- WorkFile variables
  FES_MAST_idx=0
  FES_DETT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_DATINI = cp_chartodate("01/01/"+ALLTRIM(STR(this.oParentObject.w_ANNO, 4,0) ) )
    this.w_DATFIN = cp_chartodate("31/12/"+ALLTRIM(STR(this.oParentObject.w_ANNO, 4,0) ) )
    * --- Read from FES_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.FES_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FES_MAST_idx,2],.t.,this.FES_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "FEDESCRI"+;
        " from "+i_cTable+" FES_MAST where ";
            +"FECODICE = "+cp_ToStrODBC(this.oParentObject.w_COD_DEST);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        FEDESCRI;
        from (i_cTable) where;
            FECODICE = this.oParentObject.w_COD_DEST;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TMPDESCR = NVL(cp_ToDate(_read_.FEDESCRI),cp_NullValue(_read_.FEDESCRI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_Rows>0
      ah_ErrorMsg("Esiste gi� un calendario festivit� con codice %1 avente descrizione uguale a %2", , , ALLTRIM(this.oParentObject.w_COD_DEST), ALLTRIM(this.w_TMPDESCR) )
      i_retcode = 'stop'
      return
    endif
    this.w_CHKNUM = 0
    * --- Select from FES_DETT
    i_nConn=i_TableProp[this.FES_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FES_DETT_idx,2],.t.,this.FES_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select COUNT(*) AS CONTA  from "+i_cTable+" FES_DETT ";
          +" where FEDATFES>="+cp_ToStrODBC(this.w_DATINI)+" AND FEDATFES<="+cp_ToStrODBC(this.w_DATFIN)+"";
           ,"_Curs_FES_DETT")
    else
      select COUNT(*) AS CONTA from (i_cTable);
       where FEDATFES>=this.w_DATINI AND FEDATFES<=this.w_DATFIN;
        into cursor _Curs_FES_DETT
    endif
    if used('_Curs_FES_DETT')
      select _Curs_FES_DETT
      locate for 1=1
      do while not(eof())
      this.w_CHKNUM = NVL(_Curs_FES_DETT.CONTA, 0 )
        select _Curs_FES_DETT
        continue
      enddo
      use
    endif
    if this.w_CHKNUM>0 AND !ah_YesNo("Esiste gi� almeno un calendario festivit� con date appartenenti all'anno solare impostato (%1). Procedere comunque?", , ALLTRIM(STR(this.oParentObject.w_ANNO,4,0)))
      ah_ErrorMsg("Operazione interrotta come richiesto", 64)
      i_retcode = 'stop'
      return
    endif
    * --- Try
    local bErr_03600708
    bErr_03600708=bTrsErr
    this.Try_03600708()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg("Errore durante la duplicazione del calendario festivit�.%0%1", , , ALLTRIM(MESSAGE()) )
    endif
    bTrsErr=bTrsErr or bErr_03600708
    * --- End
  endproc
  proc Try_03600708()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    ah_msg("Caricamento testata calendario festivit�")
    * --- Insert into FES_MAST
    i_nConn=i_TableProp[this.FES_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.FES_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.FES_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"FECODICE"+",FEDESCRI"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COD_DEST),'FES_MAST','FECODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DSDESCRI),'FES_MAST','FEDESCRI');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'FECODICE',this.oParentObject.w_COD_DEST,'FEDESCRI',this.oParentObject.w_DSDESCRI)
      insert into (i_cTable) (FECODICE,FEDESCRI &i_ccchkf. );
         values (;
           this.oParentObject.w_COD_DEST;
           ,this.oParentObject.w_DSDESCRI;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    ah_msg("Caricamento dettaglio calendario festivit�")
    * --- Select from gsar_kdf
    do vq_exec with 'gsar_kdf',this,'_Curs_gsar_kdf','',.f.,.t.
    if used('_Curs_gsar_kdf')
      select _Curs_gsar_kdf
      locate for 1=1
      do while not(eof())
      this.w_FEDATFES = ALLTRIM(DTOC( _Curs_gsar_kdf.FEDATFES))
      this.w_FEDATFES = cp_CharToDate(LEFT(this.w_FEDATFES,6)+ALLTRIM(STR(this.oParentObject.w_ANNO,4,0)))
      this.w_FEDESFES = _Curs_gsar_kdf.FEDESFES
      * --- Insert into FES_DETT
      i_nConn=i_TableProp[this.FES_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.FES_DETT_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.FES_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"FECODICE"+",FEDATFES"+",FEDESFES"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.oParentObject.w_COD_DEST),'FES_DETT','FECODICE');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FEDATFES),'FES_DETT','FEDATFES');
        +","+cp_NullLink(cp_ToStrODBC(this.w_FEDESFES),'FES_DETT','FEDESFES');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'FECODICE',this.oParentObject.w_COD_DEST,'FEDATFES',this.w_FEDATFES,'FEDESFES',this.w_FEDESFES)
        insert into (i_cTable) (FECODICE,FEDATFES,FEDESFES &i_ccchkf. );
           values (;
             this.oParentObject.w_COD_DEST;
             ,this.w_FEDATFES;
             ,this.w_FEDESFES;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
        select _Curs_gsar_kdf
        continue
      enddo
      use
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Caricamento calendario festivit� completato correttamente", 64)
    if TYPE("g_SINC") = "C"
      if g_SINC = "S"
        * --- Sincronizza il calendario appena creato
        GSSI_BUT(this, "GSAR_MFE" , "FES_MAST", "FECODICE=" + cp_ToStrODBC( this.oParentObject.w_COD_DEST ) )
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    this.oParentObject.w_COD_DEST = ""
    this.oParentObject.w_DSDESCRI = ""
    this.oParentObject.w_ANNO = this.oParentObject.w_ANNO + 1
    return


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='FES_MAST'
    this.cWorkTables[2]='FES_DETT'
    return(this.OpenAllTables(2))

  proc CloseCursors()
    if used('_Curs_FES_DETT')
      use in _Curs_FES_DETT
    endif
    if used('_Curs_gsar_kdf')
      use in _Curs_gsar_kdf
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
