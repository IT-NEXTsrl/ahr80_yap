* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bos                                                        *
*              CheckSupported OS/DB                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-01-18                                                      *
* Last revis.: 2017-12-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_ShowMsg
* --- Area Manuale = Header
* --- gsut_bos
#DEFINE VER_PLATFORM_WIN32S					0
#DEFINE VER_PLATFORM_WIN32_WINDOWS				1
#DEFINE VER_PLATFORM_WIN32_NT					2

#DEFINE VER_SERVER_NT                       0x80000000
#DEFINE VER_WORKSTATION_NT                  0x40000000

#DEFINE VER_NT_WORKSTATION		              0x00000001
#DEFINE VER_NT_DOMAIN_CONTROLLER	          0x00000002
#DEFINE VER_NT_SERVER               	      0x00000003

#DEFINE VER_SUITE_SMALLBUSINESS             0x00000001
#DEFINE VER_SUITE_ENTERPRISE                0x00000002
#DEFINE VER_SUITE_BACKOFFICE                0x00000004
#DEFINE VER_SUITE_COMMUNICATIONS            0x00000008
#DEFINE VER_SUITE_TERMINAL                  0x00000010
#DEFINE VER_SUITE_SMALLBUSINESS_RESTRICTED  0x00000020
#DEFINE VER_SUITE_EMBEDDEDNT                0x00000040
#DEFINE VER_SUITE_DATACENTER                0x00000080
#DEFINE VER_SUITE_SINGLEUSERTS              0x00000100
#DEFINE VER_SUITE_PERSONAL                  0x00000200
#DEFINE VER_SUITE_BLADE                     0x00000400
#DEFINE VER_SUITE_STORAGE_SERVER            0x00002000
#DEFINE FFFF 										  0x0000FFFF && 65535
#Define wbemFlagReturnImmediately  0x00000010
#Define wbemFlagForwardOnly  0x00000020
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bos",oParentObject,m.w_ShowMsg)
return(i_retval)

define class tgsut_bos as StdBatch
  * --- Local variables
  w_ShowMsg = .f.
  w_RetInfo = .NULL.
  w_Is64BitOS = .f.
  w_dwOSVersionInfoSize = 0
  w_dwMajorVersion = 0
  w_dwMinorVersion = 0
  w_dwBuildNumber = 0
  w_dwPlatformId = 0
  w_wServicePackMajor = 0
  w_wServicePackMinor = 0
  w_wSuiteMask = 0
  w_wProductType = 0
  w_wReserved = 0
  w_szCSDVersion = space(254)
  w_nResult = 0
  w_cProduct = space(254)
  w_Ret = 0
  w_DBVersion = space(0)
  w_DBInfo = space(254)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ritorna Sistema operativo e Database e se sono compatibili
    *     con la versione del gestionale
    *     Es:
    *     obj = CheckVersion()
    *     ?obj.OSInfo    &&WinXP
    *     ?obj.IsSupOS  &&True
    *     ?obj.DBInfo    &&SQL Server 2005
    *     ?obj.IsSupDB   &&True
    if !(VarType(g_bNoCheckSupported) ="L" And g_bNoCheckSupported And this.w_ShowMsg)
      this.w_RetInfo.AddProperty("PId", "(Unknown)")     
      this.w_RetInfo.AddProperty("OSInfo", "(Unknown)")     
      this.w_RetInfo.AddProperty("Is64BitOS", .F.)     
      this.w_RetInfo.AddProperty("MajorVersion", 0)     
      this.w_RetInfo.AddProperty("MinorVersion", 0)     
      this.w_RetInfo.AddProperty("BuildNumber", 0)     
      this.w_RetInfo.AddProperty("ServicePackMajor", 0)     
      this.w_RetInfo.AddProperty("ServicePackMinor", 0)     
      this.w_RetInfo.AddProperty("CSDVersion", "")     
      this.w_RetInfo.AddProperty("Product", "")     
      this.w_RetInfo.AddProperty("IsSupOS", .T.)     
      this.w_RetInfo.AddProperty("DBInfo", "(Unknown)")     
      this.w_RetInfo.AddProperty("DBVersion", "(Unknown)")     
      this.w_RetInfo.AddProperty("IsSupDB", .T.)     
      * --- Check OS
      * --- Se superiore o uguale a 5.1 esiste la funzione IsWow64Process
      this.w_Is64BitOS = .F.
      if (VAL(OS(3)) >= 6 ) OR (VAL(OS(3)) = 5 AND VAL(OS(4)) >= 1 )
        DECLARE Long GetCurrentProcess IN WIN32API
        DECLARE Long IsWow64Process IN WIN32API Long hProcess, Long @ Wow64Process
        lnIsWow64Process = 0
        if IsWow64Process( GetCurrentProcess(), @lnIsWow64Process) <> 0
          this.w_Is64BitOS = (lnIsWow64Process <> 0)
        endif
      endif
      this.w_RetInfo.Is64BitOS = this.w_Is64BitOS
      Declare LONG GetVersionEx in WIN32API STRING
      * --- Info
      if Val(Os(3))>4
        lczStructure = Chr(5*4+127+1+3*2+2*1) + Replicate(chr(0), 5*4-1)+ Space(127) + Chr(0) + Replicate(chr(0), 3*2+2*1)
      else
        lczStructure = Chr(5*4+127+1)+Replicate(Chr(0), 5*4-1)+Space(127)+Chr(0)
      endif
      this.w_nResult = GetVersionEx( @lczStructure )
      if this.w_nResult<>0
        this.w_dwOSVersionInfoSize = This.asc2BEint(lczStructure, 1, 4)
        this.w_dwMajorVersion = This.asc2BEint(lczStructure, 5, 4)
        this.w_dwMinorVersion = This.asc2BEint(lczStructure, 9, 4)
        this.w_dwBuildNumber = BITAND(This.asc2BEint(lczStructure, 13, 4), FFFF)
        this.w_dwPlatformId = This.asc2BEint(lczStructure, 17, 4)
        this.w_szCSDVersion = ALLTRIM(CHRTRAN(SUBSTR(lczStructure, 21, 128),CHR(0)+CHR(1),""))
        if Val(Os(3))>4 And this.w_dwOSVersionInfoSize > 148
          this.w_wServicePackMajor = This.asc2BEint(lczStructure, 149, 2)
          this.w_wServicePackMinor = This.asc2BEint(lczStructure, 151, 2)
          this.w_wSuiteMask = This.asc2BEint(lczStructure, 153, 2)
          this.w_wProductType = ASC(SUBSTR(lczStructure, 155, 1))
          this.w_wReserved = ASC(SUBSTR(lczStructure, 156, 1))
        endif
        * --- Save Info
        this.w_RetInfo.MajorVersion = this.w_dwMajorVersion
        this.w_RetInfo.MinorVersion = this.w_dwMinorVersion
        this.w_RetInfo.BuildNumber = this.w_dwBuildNumber
        this.w_RetInfo.ServicePackMajor = this.w_wServicePackMajor
        this.w_RetInfo.ServicePackMinor = this.w_wServicePackMinor
        this.w_RetInfo.CSDVersion = this.w_szCSDVersion
        do case
          case this.w_dwPlatformId = VER_PLATFORM_WIN32S
            * --- Windows 32s
            this.w_RetInfo.PId = "32s"
            this.w_RetInfo.IsSupOS = .F.
          case this.w_dwPlatformId = VER_PLATFORM_WIN32_WINDOWS
            * --- Windows 95/98
            this.w_RetInfo.PId = "95/98/ME"
            do case
              case this.w_dwMajorVersion = 4 and this.w_dwMinorVersion = 0
                * --- Windows 95
                this.w_RetInfo.PId = "95"
                if INLIST(UPPER(SUBSTR(this.w_szCSDVersion, 1, 1)), "B", "C")
                  this.w_RetInfo.PId = this.w_RetInfo.PId + " OSR2"
                endif
              case this.w_dwMajorVersion = 4 and this.w_dwMinorVersion = 10
                * --- Windows 95
                this.w_RetInfo.PId = "98"
                if UPPER(SUBSTR(this.w_szCSDVersion, 1, 1)) == "A"
                  this.w_RetInfo.PId = this.w_RetInfo.PId + " SE"
                endif
              case this.w_dwMajorVersion = 4 and this.w_dwMinorVersion = 90
                * --- Windows ME
                this.w_RetInfo.PId = "ME"
            endcase
            this.w_RetInfo.IsSupOS = .F.
          case this.w_dwPlatformId = VER_PLATFORM_WIN32_NT
            this.w_RetInfo.PId = "NT"
            do case
              case this.w_dwMajorVersion <= 4
                * --- Windows NT
                this.w_RetInfo.PId = "NT"
                this.w_RetInfo.IsSupOS = .F.
              case this.w_dwMajorVersion = 5
                do case
                  case this.w_dwMinorVersion = 0
                    * --- Windows 2000
                    this.w_RetInfo.PId = "2000"
                    do case
                      case BITAND(this.w_wSuiteMask, VER_SUITE_ENTERPRISE) <> 0
                        this.w_RetInfo.PId = this.w_RetInfo.PId + " Advanced Server"
                    endcase
                    this.w_RetInfo.IsSupOS = this.w_wServicePackMajor>3
                  case this.w_dwMinorVersion = 1
                    * --- Windows XP
                    this.w_RetInfo.PId = "XP "
                    if BITAND(this.w_wSuiteMask, VER_SUITE_PERSONAL) <> 0
                      this.w_RetInfo.PId = this.w_RetInfo.PId + "Home"
                      this.w_RetInfo.IsSupOS = IsAhr() Or IsAlt()
                    else
                      this.w_RetInfo.PId = this.w_RetInfo.PId + "Pro"
                      this.w_RetInfo.IsSupOS = this.w_wServicePackMajor>1
                    endif
                  case this.w_dwMinorVersion = 2
                    * --- Windows 2003
                    this.w_RetInfo.PId = "Server 2003"
                    do case
                      case BITAND(this.w_wSuiteMask, VER_SUITE_ENTERPRISE) <> 0
                        this.w_RetInfo.PId = this.w_RetInfo.PId + " Enterprise"
                        this.w_RetInfo.IsSupOS = .T.
                      case BITAND(this.w_wSuiteMask, VER_SUITE_DATACENTER) <> 0
                        this.w_RetInfo.PId = this.w_RetInfo.PId + " Datacenter"
                        this.w_RetInfo.IsSupOS = .T.
                      case BITAND(this.w_wSuiteMask, VER_SUITE_SMALLBUSINESS) <> 0 Or BITAND(this.w_wSuiteMask, VER_SUITE_SMALLBUSINESS_RESTRICTED) <> 0
                        this.w_RetInfo.PId = this.w_RetInfo.PId + " Small Business"
                        this.w_RetInfo.IsSupOS = .F.
                      case BITAND(this.w_wSuiteMask, VER_SUITE_BLADE) <> 0
                        this.w_RetInfo.PId = this.w_RetInfo.PId + " Web"
                        this.w_RetInfo.IsSupOS = .F.
                      case BITAND(this.w_wSuiteMask, VER_SUITE_STORAGE_SERVER) <> 0
                        this.w_RetInfo.PId = this.w_RetInfo.PId + " R2"
                        this.w_RetInfo.IsSupOS = .T.
                      otherwise
                        this.w_RetInfo.IsSupOS = .T.
                    endcase
                endcase
              case this.w_dwMajorVersion = 6
                * --- Leggo le informazioni del prodotto
                Declare INTEGER GetProductInfo in Kernel32 ; 
 LONG dwMajorVersion, ; 
 LONG dwMinorVersion, ; 
 LONG wServicePackMajor, ; 
 LONG wServicePackMinor, ; 
 LONG @pdwReturnedProductType
                pdwReturnedProductType = INT(0)
                this.w_nResult = GetProductInfo( this.w_dwMajorVersion, this.w_dwMinorVersion, this.w_wServicePackMajor, this.w_wServicePackMinor, @pdwReturnedProductType )
                if this.w_nResult = 1
                  this.w_cProduct = This.GetProduct(pdwReturnedProductType)
                  this.w_RetInfo.Product = this.w_cProduct
                endif
                do case
                  case this.w_dwMinorVersion = 0
                    if BITAND(this.w_wProductType, VER_NT_SERVER) = VER_NT_SERVER
                      * --- Windows 2008
                      this.w_RetInfo.PId = "Server 2008"
                      this.w_RetInfo.IsSupOS = .t.
                    else
                      * --- Windows Vista
                      this.w_RetInfo.PId = "Vista"
                      this.w_RetInfo.IsSupOS = INLIST(Upper(this.w_cProduct), "BUSINESS", "ENTERPRISE", "ULTIMATE") Or (IsAlt() And Upper(this.w_cProduct) = "HOME PREMIUM")
                    endif
                  case this.w_dwMinorVersion = 1
                    if BITAND(this.w_wProductType, VER_NT_SERVER) = VER_NT_SERVER
                      * --- Windows 2008 R2
                      this.w_RetInfo.PId = "Server 2008 R2"
                      this.w_RetInfo.IsSupOS = .t.
                    else
                      * --- Windows 7
                      this.w_RetInfo.PId = "Seven"
                      this.w_RetInfo.IsSupOS = INLIST(Upper(this.w_cProduct), "ULTIMATE", "PROFESSIONAL") Or (IsAlt() And Upper(this.w_cProduct) = "HOME PREMIUM")
                    endif
                  case this.w_dwMinorVersion =2
                    if BITAND(this.w_wProductType, VER_NT_SERVER) = VER_NT_SERVER
                      * --- Windows 2012
                      this.w_RetInfo.PId = "Server 2012"
                      this.w_RetInfo.IsSupOS = .t.
                    else
                      * --- Windows 8
                      this.w_RetInfo.IsSupOS = .T.
                      this.w_RetInfo.PId = "8"
                      * --- VErifica che la versione non sia Window RT ceh non � certificato
                      loWMI = getobject("winmgmts:\\") 
 colItems=loWMI .InstancesOf("Win32_OperatingSystem")
                      if TYPE ("colItems")="O"
                        For Each objItem In colItems
                        if TYPE ("objItem")="O" AND TYPE ("objItem.OperatingSystemSKU")="N"
                          * --- Windows RT non certificato
                          if objItem.OperatingSystemSKU=60
                            this.w_RetInfo.IsSupOS = .F.
                            this.w_RetInfo.PId = "RT"
                          endif
                        endif
                        NEXT
                      endif
                    endif
                  case this.w_dwMinorVersion > 2
                    this.w_RetInfo.PId = "sucessivo a windows 8"
                    this.w_RetInfo.IsSupOS = .f.
                endcase
              case this.w_dwMajorVersion > 6
                this.w_RetInfo.PId = "sucessivo a windows 8"
                this.w_RetInfo.IsSupOS = .F.
            endcase
        endcase
      endif
      this.w_RetInfo.OSInfo = "Microsoft Windows "+this.w_RetInfo.PId
      this.w_RetInfo.OSInfo = this.w_RetInfo.OSInfo + " " + IIF(!Empty(this.w_cProduct), this.w_cProduct + " ", "")
      this.w_RetInfo.OSInfo = this.w_RetInfo.OSInfo + AllTrim(Transform(this.w_dwMajorVersion,"@B 99999"))
      this.w_RetInfo.OSInfo = this.w_RetInfo.OSInfo + "." + AllTrim(Transform(this.w_dwMinorVersion,"@B 99999"))
      this.w_RetInfo.OSInfo = this.w_RetInfo.OSInfo + "." + AllTrim(Transform(this.w_dwBuildNumber,"@B 99999"))
      this.w_RetInfo.OSInfo = this.w_RetInfo.OSInfo + " " + IIF(EMPTY(this.w_szCSDVersion),"(No SP)", this.w_szCSDVersion)
      * --- CheckDB
      do case
        case Upper(CP_DBTYPE)="SQLSERVER"
          this.w_Ret = cp_SqlExec(i_ServerConn[1,2], "Select @@Version As Version", "__TMPVER__")
          if this.w_Ret>0
            this.w_DBVersion = STRCONV(__TMPVER__.Version, 6)
            this.w_DBInfo = Left(this.w_DBVersion, AT(CHR(10), this.w_DBVersion)-1)
            this.w_RetInfo.DBInfo = this.w_DBInfo
            this.w_Ret = cp_SqlExec(i_ServerConn[1,2], "Select SERVERPROPERTY('productversion') As Version, SERVERPROPERTY ('productlevel') As Sp, SERVERPROPERTY ('edition') As Edition", "__TMPVER__")
            if this.w_Ret>0
              * --- SQL2000/2005/2008
              this.w_DBVersion = __TMPVER__.Version
              this.w_RetInfo.DBInfo = this.w_RetInfo.DBInfo + " " + __TMPVER__.Sp + " " + __TMPVER__.Edition
            else
              * --- SQL7/6.5
              this.w_DBVersion = SUBSTR(this.w_DBInfo, AT("-", this.w_DBInfo)+2, AT("(", this.w_DBInfo)-AT("-", this.w_DBInfo)-2)
            endif
            * --- SQL Server 2000 Sp3/2005/2008/2012/2014
            this.w_RetInfo.IsSupDB = ("8." $ Left(this.w_DBVersion, 2) And Int(Val(SubStr(this.w_DBVersion, At(".", this.w_DBVersion, 2)+1)))>=760) Or "9."$Left(this.w_DBVersion,2) Or "10."$Left(this.w_DBVersion,3) Or "11."$Left(this.w_DBVersion,3) Or "12."$Left(this.w_DBVersion,3) Or "13."$Left(this.w_DBVersion,3) Or "14."$Left(this.w_DBVersion,3)
            this.w_RetInfo.DBVersion = this.w_DBVersion
          endif
          USE IN SELECT("__TMPVER__")
        case Upper(CP_DBTYPE)="ORACLE"
          this.w_Ret = cp_SqlExec(i_ServerConn[1,2], "Select  banner As Version from v$version where banner like 'Oracle%'", "__TMPVER__")
          if this.w_Ret>0
            this.w_DBInfo = AllTrim(__TMPVER__.Version)
            this.w_RetInfo.DBInfo = this.w_DBInfo
            this.w_Ret = cp_SqlExec(i_ServerConn[1,2], "Select  banner As Version from v$version where banner like 'CORE%'", "__TMPVER__")
            if this.w_Ret>0
              this.w_DBVersion = SUBSTR(__TMPVER__.Version, 6, AT(CHR(9), __TMPVER__.Version,2)-6)
              this.w_RetInfo.DBVersion = this.w_DBVersion
              * --- Oracle 10g/11g
              this.w_RetInfo.IsSupDB = "10.2" $ Left(this.w_DBVersion, 4) Or "11.2"$Left(this.w_DBVersion,4) Or "11.1"$Left(this.w_DBVersion,4)
            endif
          endif
          USE IN SELECT("__TMPVER__")
        case Upper(CP_DBTYPE)="DB2"
          this.w_RetInfo.DBInfo = "IBM DB2"
          this.w_RetInfo.IsSupDB = .F.
        case Upper(CP_DBTYPE)="POSTGRESQL"
          this.w_Ret = cp_SqlExec(i_ServerConn[1,2], "Select Version() As Version", "__TMPVER__")
          * --- Es.: "PostgreSQL 9.3.2, compiled by Visual C++ build 1600, 64-bit"
          if this.w_Ret>0
            this.w_DBInfo = AllTrim(__TMPVER__.Version)
            this.w_RetInfo.DBInfo = this.w_DBInfo
            this.w_DBVersion = STRTRAN(this.w_DBInfo,"postgresql ","",-1,-1,1)
            this.w_DBVersion = left(this.w_DBVersion, at(",", this.w_DBVersion)-1)
            this.w_RetInfo.DBVersion = this.w_DBVersion
            this.w_RetInfo.IsSupDB = this.w_DBVersion="9" and this.w_DBVersion>="9.3"
          endif
          USE IN SELECT("__TMPVER__")
      endcase
      if this.w_ShowMsg
        if !this.w_RetInfo.IsSupOS
          ah_ErrorMsg("Il sistema operativo %1 non � certificato per funzionare con l'applicativo%0Contattare l'amministratore", "!","", this.w_RetInfo.OSInfo)
        endif
        if !this.w_RetInfo.IsSupDB
          ah_ErrorMsg("Il database engine %1 non � certificato per funzionare con l'applicativo%0Contattare l'amministratore", "!","", this.w_RetInfo.DBInfo)
        endif
      endif
      i_retcode = 'stop'
      i_retval = this.w_RetInfo
      return
    endif
  endproc


  proc Init(oParentObject,w_ShowMsg)
    this.w_ShowMsg=w_ShowMsg
    this.w_RetInfo=createobject("Custom")
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- gsut_bos
  Function GetProduct
  	Lparameters znProductType
  
  	** pdwReturnedProductType
    #Define PRODUCT_BUSINESS	0x00000006  &&Business
    #Define PRODUCT_BUSINESS_N	0x00000010 &&Business N
    #Define PRODUCT_CLUSTER_SERVER	0x00000012	&&HPC Edition
    #Define PRODUCT_DATACENTER_SERVER	0x00000008	&&Server Datacenter (full installation)
    #Define PRODUCT_DATACENTER_SERVER_CORE	0x0000000C	&&Server Datacenter (core installation)
    #Define PRODUCT_DATACENTER_SERVER_CORE_V	0x00000027	&&Server Datacenter without Hyper-V (core installation)
    #Define PRODUCT_DATACENTER_SERVER_V	0x00000025	&&Server Datacenter without Hyper-V (full installation)
    #Define PRODUCT_ENTERPRISE	0x00000004	&&Enterprise
    #Define PRODUCT_ENTERPRISE_E	0x00000046	&&Enterprise E
    #Define PRODUCT_ENTERPRISE_N	0x0000001B	&&Enterprise N
    #Define PRODUCT_ENTERPRISE_SERVER	0x0000000A	&&Server Enterprise (full installation)
    #Define PRODUCT_ENTERPRISE_SERVER_CORE	0x0000000E	&&Server Enterprise (core installation)
    #Define PRODUCT_ENTERPRISE_SERVER_CORE_V	0x00000029	&&Server Enterprise without Hyper-V (core installation)
    #Define PRODUCT_ENTERPRISE_SERVER_IA64	0x0000000F	&&Server Enterprise for Itanium-based Systems
    #Define PRODUCT_ENTERPRISE_SERVER_V	0x00000026	&&Server Enterprise without Hyper-V (full installation)
    #Define PRODUCT_HOME_BASIC	0x00000002	&&Home Basic
    #Define PRODUCT_HOME_SERVER    0x00000013   &&Home Server
    #Define PRODUCT_HOME_BASIC_E	0x00000043	&&Home Basic E
    #Define PRODUCT_HOME_BASIC_N	0x00000005	&&Home Basic N
    #Define PRODUCT_HOME_PREMIUM	0x00000003	&&Home Premium
    #Define PRODUCT_HOME_PREMIUM_E	0x00000044	&&Home Premium E
    #Define PRODUCT_HOME_PREMIUM_N	0x0000001A	&&Home Premium N
    #Define PRODUCT_HYPERV	0x0000002A	&&Microsoft Hyper-V Server
    #Define PRODUCT_MEDIUMBUSINESS_SERVER_MANAGEMENT	0x0000001E	&&Windows Essential Business Server Management Server
    #Define PRODUCT_MEDIUMBUSINESS_SERVER_MESSAGING	0x00000020	&&Windows Essential Business Server Messaging Server
    #Define PRODUCT_MEDIUMBUSINESS_SERVER_SECURITY	0x0000001F	&&Windows Essential Business Server Security Server
    #Define PRODUCT_PROFESSIONAL	0x00000030	&&Professional
    #Define PRODUCT_PROFESSIONAL_E	0x00000045	&&Professional E
    #Define PRODUCT_PROFESSIONAL_N	0x00000031	&&Professional N
    #Define PRODUCT_SERVER_FOR_SMALLBUSINESS	0x00000018	&&Windows Server 2008 for Windows Essential Server Solutions
    #Define PRODUCT_SERVER_FOR_SMALLBUSINESS_V	0x00000023	&&Windows Server 2008 without Hyper-V for Windows Essential Server Solutions
    #Define PRODUCT_SERVER_FOUNDATION	0x00000021	&&Server Foundation
    #Define PRODUCT_SMALLBUSINESS_SERVER	0x00000009	&&Windows Small Business Server
    #Define PRODUCT_STANDARD_SERVER	0x00000007	&&Server Standard (full installation)
    #Define PRODUCT_STANDARD_SERVER_CORE	0x0000000D	&&Server Standard (core installation)
    #Define PRODUCT_STANDARD_SERVER_CORE_V	0x00000028	&&Server Standard without Hyper-V (core installation)
    #Define PRODUCT_STANDARD_SERVER_V	0x00000024	&&Server Standard without Hyper-V (full installation)
    #Define PRODUCT_STARTER	0x0000000B	&&Starter
    #Define PRODUCT_STARTER_E	0x00000042	&&Starter E
    #Define PRODUCT_STARTER_N	0x0000002F	&&Starter N
    #Define PRODUCT_STORAGE_ENTERPRISE_SERVER	0x00000017	&&Storage Server Enterprise
    #Define PRODUCT_STORAGE_EXPRESS_SERVER	0x00000014	&&Storage Server Express
    #Define PRODUCT_STORAGE_STANDARD_SERVER	0x00000015	&&Storage Server Standard
    #Define PRODUCT_STORAGE_WORKGROUP_SERVER	0x00000016	&&Storage Server Workgroup
    #Define PRODUCT_UNDEFINED	0x00000000	&&An unknown product
    #Define PRODUCT_ULTIMATE	0x00000001	&&Ultimate
    #Define PRODUCT_ULTIMATE_E	0x00000047	&&Ultimate E
    #Define PRODUCT_ULTIMATE_N	0x0000001C	&&Ultimate N
    #Define PRODUCT_WEB_SERVER	0x00000011	&&Web Server (full installation)
    #Define PRODUCT_WEB_SERVER_CORE	0x0000001D	&&Web Server (core installation)
    #Define PRODUCT_SMALLBUSINESS_SERVER_PREMIUM 0x00000019  &&Web Server Small Business Server Premium Edition
  	Local lcProductType
  
  	lcProductType = ''
    ** First the non-Servers
  	Do Case
  		Case znProductType = PRODUCT_ENTERPRISE Or znProductType = PRODUCT_ENTERPRISE_N Or znProductType = PRODUCT_ENTERPRISE_E
  			lcProductType = 'Enterprise' && Edition'
  		Case znProductType = PRODUCT_HOME_BASIC Or znProductType = PRODUCT_HOME_BASIC_N Or znProductType = PRODUCT_HOME_BASIC_E
  			lcProductType = 'Home Basic' && Edition'
  		Case znProductType = PRODUCT_HOME_PREMIUM Or znProductType = PRODUCT_HOME_PREMIUM_N Or znProductType = PRODUCT_HOME_PREMIUM_E
  			lcProductType = 'Home Premium' && Edition'
  		Case znProductType = PRODUCT_ULTIMATE Or znProductType = PRODUCT_ULTIMATE_N Or znProductType = PRODUCT_ULTIMATE_E
  			lcProductType = 'Ultimate' && Edition'
  		Case znProductType = PRODUCT_STARTER Or znProductType = PRODUCT_STARTER_N Or znProductType = PRODUCT_STARTER_E
  			lcProductType = 'Starter' && Edition'
  		Case znProductType = PRODUCT_PROFESSIONAL Or znProductType = PRODUCT_PROFESSIONAL_N Or znProductType = PRODUCT_PROFESSIONAL_E
  			lcProductType = 'Professional' && Edition'
      Case znProductType = PRODUCT_UNDEFINED
  			lcProductType = 'Unknown Product Type'
  			** Now the Servers
  		Case znProductType = PRODUCT_CLUSTER_SERVER
  			lcProductType = 'Cluster Server Edition'
  		Case znProductType = PRODUCT_DATACENTER_SERVER
  			lcProductType = 'Server Datacenter Edition (full installation)'
  		Case znProductType = PRODUCT_DATACENTER_SERVER_CORE
  			lcProductType = 'Server Datacenter Edition (core installation)'
  		Case znProductType = PRODUCT_DATACENTER_SERVER_CORE_V
  			lcProductType = 'Server Datacenter Edition without Hyper-V (core installation)'
  		Case znProductType = PRODUCT_DATACENTER_SERVER_V
  			lcProductType = 'Server Datacenter Edition without Hyper-V (full installation)'
  		Case znProductType = PRODUCT_ENTERPRISE_SERVER
  			lcProductType = 'Server Enterprise Edition (full installation)'
  		Case znProductType = PRODUCT_ENTERPRISE_SERVER_CORE
  			lcProductType = 'Server Enterprise Edition (core installation)'
  		Case znProductType = PRODUCT_ENTERPRISE_SERVER_CORE_V
  			lcProductType = 'Server Enterprise Edition without Hyper-V (core installation)'
  		Case znProductType = PRODUCT_ENTERPRISE_SERVER_IA64
  			lcProductType = 'Server Enterprise Edition for Itanium-based Systems'
  		Case znProductType = PRODUCT_ENTERPRISE_SERVER_V
  			lcProductType = 'Server Enterprise Edition without Hyper-V (full installation)'
  		Case znProductType = PRODUCT_HOME_SERVER
  			lcProductType = 'Home Server Edition'
  		Case znProductType = PRODUCT_MEDIUMBUSINESS_SERVER_MANAGEMENT
  			lcProductType = 'Windows Essential Business Server Management Server'
  		Case znProductType = PRODUCT_MEDIUMBUSINESS_SERVER_MESSAGING
  			lcProductType = 'Windows Essential Business Server Messaging Server'
  		Case znProductType = PRODUCT_MEDIUMBUSINESS_SERVER_SECURITY
  			lcProductType = 'Windows Essential Business Server Security Server'
  		Case znProductType = PRODUCT_SERVER_FOR_SMALLBUSINESS
  			lcProductType = 'Server for Small Business Edition'
  		Case znProductType = PRODUCT_SMALLBUSINESS_SERVER
  			lcProductType = 'Small Business Server'
  		Case znProductType = PRODUCT_SMALLBUSINESS_SERVER_PREMIUM
  			lcProductType = 'Small Business Server Premium Edition'
  		Case znProductType = PRODUCT_STANDARD_SERVER
  			lcProductType = 'Server Standard Edition (full installation)'
  		Case znProductType = PRODUCT_STANDARD_SERVER_CORE
  			lcProductType = 'Server Standard Edition (core installation)'
  		Case znProductType = PRODUCT_STANDARD_SERVER_CORE_V
  			lcProductType = 'Server Standard Edition without Hyper-V (core installation)'
  		Case znProductType = PRODUCT_STANDARD_SERVER_V
  			lcProductType = 'Server Standard Edition without Hyper-V (full installation)'
  		Case znProductType = PRODUCT_STORAGE_ENTERPRISE_SERVER
  			lcProductType = 'Storage Server Enterprise Edition'
  		Case znProductType = PRODUCT_STORAGE_EXPRESS_SERVER
  			lcProductType = 'Storage Server Express Edition'
  		Case znProductType = PRODUCT_STORAGE_STANDARD_SERVER
  			lcProductType = 'Storage Server Standard Edition'
  		Case znProductType = PRODUCT_STORAGE_WORKGROUP_SERVER
  			lcProductType = 'Storage Server Workgroup Edition'
  		Case znProductType = PRODUCT_WEB_SERVER
  			lcProductType = 'Web Server Edition (full installation)'
  		Case znProductType = PRODUCT_WEB_SERVER_CORE
  			lcProductType = 'Web Server Edition (core installation)'
  		Case znProductType = PRODUCT_BUSINESS Or znProductType = PRODUCT_BUSINESS_N
  			lcProductType = 'Business' && Edition'
  		Otherwise
  			lcProductType = ''
  	Endcase
  
  	Return lcProductType
  Endfunc
  
  Function asc2BEint
  	Lparameters p_cString, p_nStart, p_nLength, p_nAllowNegative
  	If Pcount() < 1 Or Vartype(p_cString) <> "C"
  		Return -1
  	Endif
  
  	If Pcount() < 2 Or Vartype(p_nStart) <> "N"
  		p_nStart = 1
  	Endif
  	If Pcount() < 3 Or Vartype(p_nLength) <> "N"
  		p_nLength = Len(p_cString)
  	Endif
  	If Pcount() < 4 Or Vartype(p_nAllowNegative) <> "L"
  		p_nAllowNegative = .F.
  	Endif
  
  	Local lnRet_val, llIsNegative, lcFixString, lnii, lcConvertString
  
  	lnRet_val = -1
  	llIsNegative = .F.
  
  	If !Between(p_nLength, 1, 4)
  		Return lnRet_val
  	Endif
  
  	lcConvertString = Substr(p_cString, p_nStart, p_nLength)
  	If p_nAllowNegative
  		If Asc(Substr(lcConvertString, (p_nLength-1), 1)) > 0x7F && It's Negative
  			lcFixString = lcConvertString
  			lcConvertString = ''
  			For lnii = 1 To p_nLength
  				lcConvertString = lcConvertString + Chr(255-Asc(Substr(lcFixString, lnii, 1))) && kludgy XOR
  			Endfor
  			llIsNegative = .T.
  		Endif
  	Endif
  
  	Do Case
  		Case p_nLength = 1
  			lnRet_val = Asc(Substr(lcConvertString, 1, 1))
  
  		Case p_nLength = 2
  			lnRet_val = Asc(Substr(lcConvertString, 1, 1));
  				+ Asc(Substr(lcConvertString, 2, 1))*256
  
  		Case p_nLength = 3
  			lnRet_val = Asc(Substr(lcConvertString, 1, 1));
  				+ Asc(Substr(lcConvertString, 2, 1))*256;
  				+ Asc(Substr(lcConvertString, 3, 1))*256^2
  
  		Case p_nLength = 4
  			lnRet_val = Asc(Substr(lcConvertString, 1, 1));
  				+ Asc(Substr(lcConvertString, 2, 1))*256;
  				+ Asc(Substr(lcConvertString, 3, 1))*256^2;
  				+ Asc(Substr(lcConvertString, 4, 1))*256^3
  
  		Otherwise
  			lnRet_val = -1
  	Endcase
  	If llIsNegative	&& Two's Complement Adj
  		lnRet_val = -(lnRet_val + 1)
  	Endif
  
  	Return Int(lnRet_val)
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_ShowMsg"
endproc
