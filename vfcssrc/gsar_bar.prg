* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bar                                                        *
*              Associazione attributi                                          *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][4]                                                       *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-13                                                      *
* Last revis.: 2008-02-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bar",oParentObject)
return(i_retval)

define class tgsar_bar as StdBatch
  * --- Local variables
  w_GESTIONE = .NULL.
  w_GSFUNZIO = space(10)
  w_TIMESTAMP = space(10)
  * --- WorkFile variables
  ASS_ATTR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_GESTIONE = i_CURFORM
    this.w_GSFUNZIO = this.w_GESTIONE.cfunction
    * --- Se archivio in interroga e non ha attributi chiedo all'tutente se vuole immediatamente caricarne di nuovi
    if this.w_GSFUNZIO="Query"
      this.w_TIMESTAMP = this.w_GESTIONE.w_GESTGUID
      * --- Read from ASS_ATTR
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ASS_ATTR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ASS_ATTR_idx,2],.t.,this.ASS_ATTR_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" ASS_ATTR where ";
              +"GUID = "+cp_ToStrODBC(this.w_TIMESTAMP);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              GUID = this.w_TIMESTAMP;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        use
        if i_Rows=0
          if ah_YesNo("Alla gestione non � stato ancora associato nessun attributo %1Si desidera effettuare tale operazione?","",chr(13))
            * --- Metto in automatico la gestione in modofica per poter caricare gli attributi
            this.w_GESTIONE.ecpEdit()     
            * --- Se la gestione � immodificabile non posso proseguire
            if this.w_GESTIONE.cFunction<>"Edit"
              i_retcode = 'stop'
              return
            endif
          else
            i_retcode = 'stop'
            return
          endif
        endif
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    * --- Lancio la maschera passando come padre la gestione sottostante
    DO GSAR_KRA with this.w_GESTIONE
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='ASS_ATTR'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
