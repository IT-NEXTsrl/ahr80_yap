* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kso                                                        *
*              Dati del soggetto delegato                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2018-01-29                                                      *
* Last revis.: 2018-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kso",oParentObject))

* --- Class definition
define class tgsar_kso as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 613
  Height = 256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2018-03-02"
  HelpContextID=169248919
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsar_kso"
  cComment = "Dati del soggetto delegato"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_AZCODNAZ = space(3)
  w_AZAICFPI = space(16)
  w_AZAIRAGS = space(40)
  w_AZAIPREF = space(5)
  w_AZAITELE = space(18)
  w_AZAIINDI = space(35)
  w_AZAI_CAP = space(8)
  w_AZAICITT = space(30)
  w_AZAIPROV = space(2)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_ksoPag1","gsar_kso",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAZAICFPI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_AZCODNAZ=space(3)
      .w_AZAICFPI=space(16)
      .w_AZAIRAGS=space(40)
      .w_AZAIPREF=space(5)
      .w_AZAITELE=space(18)
      .w_AZAIINDI=space(35)
      .w_AZAI_CAP=space(8)
      .w_AZAICITT=space(30)
      .w_AZAIPROV=space(2)
      .w_AZCODNAZ=oParentObject.w_AZCODNAZ
      .w_AZAICFPI=oParentObject.w_AZAICFPI
      .w_AZAIRAGS=oParentObject.w_AZAIRAGS
      .w_AZAIPREF=oParentObject.w_AZAIPREF
      .w_AZAITELE=oParentObject.w_AZAITELE
      .w_AZAIINDI=oParentObject.w_AZAIINDI
      .w_AZAI_CAP=oParentObject.w_AZAI_CAP
      .w_AZAICITT=oParentObject.w_AZAICITT
      .w_AZAIPROV=oParentObject.w_AZAIPROV
    endwith
    this.DoRTCalc(1,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_9.enabled = this.oPgFrm.Page1.oPag.oBtn_1_9.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_AZCODNAZ=.w_AZCODNAZ
      .oParentObject.w_AZAICFPI=.w_AZAICFPI
      .oParentObject.w_AZAIRAGS=.w_AZAIRAGS
      .oParentObject.w_AZAIPREF=.w_AZAIPREF
      .oParentObject.w_AZAITELE=.w_AZAITELE
      .oParentObject.w_AZAIINDI=.w_AZAIINDI
      .oParentObject.w_AZAI_CAP=.w_AZAI_CAP
      .oParentObject.w_AZAICITT=.w_AZAICITT
      .oParentObject.w_AZAIPROV=.w_AZAIPROV
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAZAICFPI_1_2.value==this.w_AZAICFPI)
      this.oPgFrm.Page1.oPag.oAZAICFPI_1_2.value=this.w_AZAICFPI
    endif
    if not(this.oPgFrm.Page1.oPag.oAZAIRAGS_1_10.value==this.w_AZAIRAGS)
      this.oPgFrm.Page1.oPag.oAZAIRAGS_1_10.value=this.w_AZAIRAGS
    endif
    if not(this.oPgFrm.Page1.oPag.oAZAIPREF_1_11.value==this.w_AZAIPREF)
      this.oPgFrm.Page1.oPag.oAZAIPREF_1_11.value=this.w_AZAIPREF
    endif
    if not(this.oPgFrm.Page1.oPag.oAZAITELE_1_13.value==this.w_AZAITELE)
      this.oPgFrm.Page1.oPag.oAZAITELE_1_13.value=this.w_AZAITELE
    endif
    if not(this.oPgFrm.Page1.oPag.oAZAIINDI_1_14.value==this.w_AZAIINDI)
      this.oPgFrm.Page1.oPag.oAZAIINDI_1_14.value=this.w_AZAIINDI
    endif
    if not(this.oPgFrm.Page1.oPag.oAZAI_CAP_1_15.value==this.w_AZAI_CAP)
      this.oPgFrm.Page1.oPag.oAZAI_CAP_1_15.value=this.w_AZAI_CAP
    endif
    if not(this.oPgFrm.Page1.oPag.oAZAICITT_1_16.value==this.w_AZAICITT)
      this.oPgFrm.Page1.oPag.oAZAICITT_1_16.value=this.w_AZAICITT
    endif
    if not(this.oPgFrm.Page1.oPag.oAZAIPROV_1_17.value==this.w_AZAIPROV)
      this.oPgFrm.Page1.oPag.oAZAIPROV_1_17.value=this.w_AZAIPROV
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(EMPTY(.w_AZAICFPI) OR CHKCFP(.w_AZAICFPI,"PI"))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAZAICFPI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsar_ksoPag1 as StdContainer
  Width  = 609
  height = 256
  stdWidth  = 609
  stdheight = 256
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAZAICFPI_1_2 as StdField with uid="GIXSOGPFXX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_AZAICFPI", cQueryName = "AZAICFPI",;
    bObbl = .f. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale o partita IVA del soggetto delegato",;
    HelpContextID = 191629489,;
   bGlobalFont=.t.,;
    Height=21, Width=146, Left=175, Top=13, InputMask=replicate('X',16)

  func oAZAICFPI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_AZAICFPI) OR CHKCFP(.w_AZAICFPI,"PI"))
    endwith
    return bRes
  endfunc


  add object oBtn_1_9 as StdButton with uid="YYYWRVTVKV",left=547, top=199, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare le scelte impostate";
    , HelpContextID = 44031466;
    , caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_9.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oAZAIRAGS_1_10 as StdField with uid="XLUINZNODK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_AZAIRAGS", cQueryName = "AZAIRAGS",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Ragione sociale del soggetto delegato",;
    HelpContextID = 8648537,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=175, Top=52, InputMask=replicate('X',40)

  add object oAZAIPREF_1_11 as StdField with uid="FWUHHCSJSY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_AZAIPREF", cQueryName = "AZAIPREF",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso telefonico del soggetto delegato",;
    HelpContextID = 23328588,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=175, Top=88, InputMask=replicate('X',5)

  add object oAZAITELE_1_13 as StdField with uid="GPGWBFXRXC",rtseq=5,rtrep=.f.,;
    cFormVar = "w_AZAITELE", cQueryName = "AZAITELE",;
    bObbl = .f. , nPag = 1, value=space(18), bMultilanguage =  .f.,;
    ToolTipText = "Numero telefonico del soggetto delegato",;
    HelpContextID = 77854539,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=242, Top=88, InputMask=replicate('X',18)

  add object oAZAIINDI_1_14 as StdField with uid="CARMRAKKBO",rtseq=6,rtrep=.f.,;
    cFormVar = "w_AZAIINDI", cQueryName = "AZAIINDI",;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo del soggetto delegato",;
    HelpContextID = 51120305,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=175, Top=125, InputMask=replicate('X',35)

  add object oAZAI_CAP_1_15 as StdField with uid="WGIIJHRERL",rtseq=7,rtrep=.f.,;
    cFormVar = "w_AZAI_CAP", cQueryName = "AZAI_CAP",;
    bObbl = .f. , nPag = 1, value=space(8), bMultilanguage =  .f.,;
    ToolTipText = "CAP di residenza del soggetto delegato",;
    HelpContextID = 212601002,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=175, Top=161, InputMask=replicate('X',8), bHasZoom = .t. 

  proc oAZAI_CAP_1_15.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_AZAI_CAP",".w_AZAICITT",".w_AZAIPROV")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAZAICITT_1_16 as StdField with uid="OXPTYSXXFL",rtseq=8,rtrep=.f.,;
    cFormVar = "w_AZAICITT", cQueryName = "AZAICITT",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    ToolTipText = "Localit� di residenza del soggetto delegato",;
    HelpContextID = 141297830,;
   bGlobalFont=.t.,;
    Height=21, Width=237, Left=270, Top=161, InputMask=replicate('X',30), bHasZoom = .t. 

  proc oAZAICITT_1_16.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"C",".w_AZAI_CAP",".w_AZAICITT",".w_AZAIPROV")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oAZAIPROV_1_17 as StdField with uid="WOWSOIBZHG",rtseq=9,rtrep=.f.,;
    cFormVar = "w_AZAIPROV", cQueryName = "AZAIPROV",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Sigla della provincia di residenza del soggetto delegato",;
    HelpContextID = 245106852,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=549, Top=161, cSayPict="'!!'", cGetPict="'!!'", InputMask=replicate('X',2), bHasZoom = .t. 

  proc oAZAIPROV_1_17.mZoom
      with this.Parent.oContained
        GSAR_BXC(this.Parent.oContained,"P"," "," ",".w_AZAIPROV")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oStr_1_3 as StdString with uid="DNNRCQWJYQ",Visible=.t., Left=9, Top=16,;
    Alignment=1, Width=159, Height=15,;
    Caption="Codice fiscale o partita IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_4 as StdString with uid="VPCHCAPHAX",Visible=.t., Left=54, Top=53,;
    Alignment=1, Width=114, Height=15,;
    Caption="Ragione sociale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="REUSSFFKCS",Visible=.t., Left=104, Top=90,;
    Alignment=1, Width=64, Height=15,;
    Caption="Telefono:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="DCNBODRJCL",Visible=.t., Left=111, Top=127,;
    Alignment=1, Width=57, Height=15,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="RIYXUFMSOS",Visible=.t., Left=76, Top=163,;
    Alignment=1, Width=92, Height=15,;
    Caption="CAP - localit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="BTITSFHEOJ",Visible=.t., Left=514, Top=163,;
    Alignment=1, Width=30, Height=15,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="HFFJFMZIVP",Visible=.t., Left=232, Top=92,;
    Alignment=0, Width=12, Height=15,;
    Caption="-"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kso','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
