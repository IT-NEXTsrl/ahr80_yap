* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscf_kcr                                                        *
*              Caricamento righe                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-10-07                                                      *
* Last revis.: 2013-06-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscf_kcr",oParentObject))

* --- Class definition
define class tgscf_kcr as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 788
  Height = 560
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-06-07"
  HelpContextID=99235177
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscf_kcr"
  cComment = "Caricamento righe"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_RETABMST = space(15)
  w_TBCOMMENT = space(60)
  w_RETABDTL = space(15)
  w_TBCOMMDTL = space(60)
  w_FILRECSE = space(1)
  o_FILRECSE = space(1)
  w_FILMST = space(1)
  o_FILMST = space(1)
  w_FILDTL = space(1)
  o_FILDTL = space(1)
  w_FLNAME = space(10)
  o_FLNAME = space(10)
  w_FLMONINS = 0
  o_FLMONINS = 0
  w_FLMONDEL = 0
  w_FLMONMOD = space(1)
  w_FLBLKINS = 0
  o_FLBLKINS = 0
  w_FLOBBINS = 0
  o_FLOBBINS = 0
  w_VOID = space(10)
  w_ZOOMFLDS = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscf_kcrPag1","gscf_kcr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oFILRECSE_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMFLDS = this.oPgFrm.Pages(1).oPag.ZOOMFLDS
    DoDefault()
    proc Destroy()
      this.w_ZOOMFLDS = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        gscf_bcr(this,"INSERTROWS")
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_RETABMST=space(15)
      .w_TBCOMMENT=space(60)
      .w_RETABDTL=space(15)
      .w_TBCOMMDTL=space(60)
      .w_FILRECSE=space(1)
      .w_FILMST=space(1)
      .w_FILDTL=space(1)
      .w_FLNAME=space(10)
      .w_FLMONINS=0
      .w_FLMONDEL=0
      .w_FLMONMOD=space(1)
      .w_FLBLKINS=0
      .w_FLOBBINS=0
      .w_VOID=space(10)
      .w_RETABMST=oParentObject.w_RETABMST
      .w_TBCOMMENT=oParentObject.w_TBCOMMENT
      .w_RETABDTL=oParentObject.w_RETABDTL
      .w_TBCOMMDTL=oParentObject.w_TBCOMMDTL
          .DoRTCalc(1,4,.f.)
        .w_FILRECSE = 'T'
        .w_FILMST = 'S'
        .w_FILDTL = 'S'
      .oPgFrm.Page1.oPag.ZOOMFLDS.Calculate()
        .w_FLNAME = .w_ZOOMFLDS.getvar("FLNAME")
        .w_FLMONINS = 2
        .w_FLMONDEL = 2
        .w_FLMONMOD = 'X'
        .w_FLBLKINS = 2
        .w_FLOBBINS = 2
      .oPgFrm.Page1.oPag.oObj_1_33.Calculate(.w_FLBLKINS)
      .oPgFrm.Page1.oPag.oObj_1_34.Calculate(.w_FLOBBINS)
      .oPgFrm.Page1.oPag.oObj_1_35.Calculate(.w_FLMONINS)
    endwith
    this.DoRTCalc(14,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_13.enabled = this.oPgFrm.Page1.oPag.oBtn_1_13.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_RETABMST=.w_RETABMST
      .oParentObject.w_TBCOMMENT=.w_TBCOMMENT
      .oParentObject.w_RETABDTL=.w_RETABDTL
      .oParentObject.w_TBCOMMDTL=.w_TBCOMMDTL
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMFLDS.Calculate()
        .DoRTCalc(1,7,.t.)
            .w_FLNAME = .w_ZOOMFLDS.getvar("FLNAME")
        if .o_FLBLKINS<>.w_FLBLKINS
          .Calculate_ECCVSOTZGW()
        endif
        if .o_FLOBBINS<>.w_FLOBBINS.or. .o_FLMONINS<>.w_FLMONINS
          .Calculate_WKZVUUVRAQ()
        endif
        if .o_FLBLKINS<>.w_FLBLKINS.or. .o_FLMONINS<>.w_FLMONINS.or. .o_FLOBBINS<>.w_FLOBBINS
          .Calculate_DBQIBIDODI()
        endif
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(.w_FLBLKINS)
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate(.w_FLOBBINS)
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate(.w_FLMONINS)
        if .o_FILDTL<>.w_FILDTL.or. .o_FILMST<>.w_FILMST.or. .o_FILRECSE<>.w_FILRECSE
          .Calculate_PHNUPVFIVI()
        endif
        if .o_FLNAME<>.w_FLNAME
          .Calculate_CTKOKVHKQL()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(9,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMFLDS.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate(.w_FLBLKINS)
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate(.w_FLOBBINS)
        .oPgFrm.Page1.oPag.oObj_1_35.Calculate(.w_FLMONINS)
    endwith
  return

  proc Calculate_PIWKTPPQSR()
    with this
          * --- Aggiunge checkbox secondario per gestione chiave primaria
          AddPrimKeyCheckBox(this;
              ,.w_ZOOMFLDS;
             )
          AddRefRecCheckBox(this;
              ,.w_ZOOMFLDS;
             )
    endwith
  endproc
  proc Calculate_ECCVSOTZGW()
    with this
          * --- FLBLKINS cha
          .w_FLOBBINS = icase(.w_FLBLKINS=1 OR .w_FLMONINS=1, 0, .w_FLBLKINS=2 AND .w_FLOBBINS=1, 2, .w_FLOBBINS)
          .o_FLOBBINS = .w_FLOBBINS
          .w_FLMONINS = icase(.w_FLBLKINS=1, 0, .w_FLBLKINS=2 AND .w_FLMONINS=1, 2, .w_FLMONINS)
          .o_FLMONINS = .w_FLMONINS
    endwith
  endproc
  proc Calculate_WKZVUUVRAQ()
    with this
          * --- FLOBBINS-FLMONINS cha
          .w_FLBLKINS = icase(.w_FLOBBINS=1 OR .w_FLMONINS=1, 0, .w_FLBLKINS=1 AND (.w_FLBLKINS=2 OR .w_FLMONINS=2), 2, .w_FLBLKINS)
    endwith
  endproc
  proc Calculate_DBQIBIDODI()
    with this
          * --- SetControlsValue
          .w_VOID = .SetControlsValue()
    endwith
  endproc
  proc Calculate_BBPWAKLELV()
    with this
          * --- Sincronizza
          gscf_bcr(this;
              ,"SINCRO";
             )
    endwith
  endproc
  proc Calculate_PHNUPVFIVI()
    with this
          * --- FILTRO
          gscf_bcr(this;
              ,"FILTRO";
             )
    endwith
  endproc
  proc Calculate_CTKOKVHKQL()
    with this
          * --- refresh
     if .w_FILRECSE<>"T"
          .w_VOID = .w_ZOOMFLDS.Refresh()
     endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFILMST_1_8.enabled = this.oPgFrm.Page1.oPag.oFILMST_1_8.mCond()
    this.oPgFrm.Page1.oPag.oFILDTL_1_9.enabled = this.oPgFrm.Page1.oPag.oFILDTL_1_9.mCond()
    this.oPgFrm.Page1.oPag.oFLMONINS_1_16.enabled = this.oPgFrm.Page1.oPag.oFLMONINS_1_16.mCond()
    this.oPgFrm.Page1.oPag.oFLBLKINS_1_19.enabled = this.oPgFrm.Page1.oPag.oFLBLKINS_1_19.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFILMST_1_8.visible=!this.oPgFrm.Page1.oPag.oFILMST_1_8.mHide()
    this.oPgFrm.Page1.oPag.oFILDTL_1_9.visible=!this.oPgFrm.Page1.oPag.oFILDTL_1_9.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMFLDS.Event(cEvent)
        if lower(cEvent)==lower("w_ZOOMFLDS after query")
          .Calculate_PIWKTPPQSR()
          bRefresh=.t.
        endif
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_35.Event(cEvent)
        if lower(cEvent)==lower("Init")
          .Calculate_BBPWAKLELV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_ZOOMFLDS row checked") or lower(cEvent)==lower("w_ZOOMFLDS row unchecked")
          .Calculate_CTKOKVHKQL()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oRETABMST_1_1.value==this.w_RETABMST)
      this.oPgFrm.Page1.oPag.oRETABMST_1_1.value=this.w_RETABMST
    endif
    if not(this.oPgFrm.Page1.oPag.oTBCOMMENT_1_3.value==this.w_TBCOMMENT)
      this.oPgFrm.Page1.oPag.oTBCOMMENT_1_3.value=this.w_TBCOMMENT
    endif
    if not(this.oPgFrm.Page1.oPag.oRETABDTL_1_4.value==this.w_RETABDTL)
      this.oPgFrm.Page1.oPag.oRETABDTL_1_4.value=this.w_RETABDTL
    endif
    if not(this.oPgFrm.Page1.oPag.oTBCOMMDTL_1_6.value==this.w_TBCOMMDTL)
      this.oPgFrm.Page1.oPag.oTBCOMMDTL_1_6.value=this.w_TBCOMMDTL
    endif
    if not(this.oPgFrm.Page1.oPag.oFILRECSE_1_7.RadioValue()==this.w_FILRECSE)
      this.oPgFrm.Page1.oPag.oFILRECSE_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFILMST_1_8.RadioValue()==this.w_FILMST)
      this.oPgFrm.Page1.oPag.oFILMST_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFILDTL_1_9.RadioValue()==this.w_FILDTL)
      this.oPgFrm.Page1.oPag.oFILDTL_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLMONINS_1_16.RadioValue()==this.w_FLMONINS)
      this.oPgFrm.Page1.oPag.oFLMONINS_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLMONDEL_1_17.RadioValue()==this.w_FLMONDEL)
      this.oPgFrm.Page1.oPag.oFLMONDEL_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLMONMOD_1_18.RadioValue()==this.w_FLMONMOD)
      this.oPgFrm.Page1.oPag.oFLMONMOD_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLBLKINS_1_19.RadioValue()==this.w_FLBLKINS)
      this.oPgFrm.Page1.oPag.oFLBLKINS_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLOBBINS_1_20.RadioValue()==this.w_FLOBBINS)
      this.oPgFrm.Page1.oPag.oFLOBBINS_1_20.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_FILRECSE = this.w_FILRECSE
    this.o_FILMST = this.w_FILMST
    this.o_FILDTL = this.w_FILDTL
    this.o_FLNAME = this.w_FLNAME
    this.o_FLMONINS = this.w_FLMONINS
    this.o_FLBLKINS = this.w_FLBLKINS
    this.o_FLOBBINS = this.w_FLOBBINS
    return

enddefine

* --- Define pages as container
define class tgscf_kcrPag1 as StdContainer
  Width  = 784
  height = 560
  stdWidth  = 784
  stdheight = 560
  resizeXpos=635
  resizeYpos=400
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oRETABMST_1_1 as StdField with uid="JPELUJDYJN",rtseq=1,rtrep=.f.,;
    cFormVar = "w_RETABMST", cQueryName = "RETABMST",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 192697706,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=257, Top=9, InputMask=replicate('X',15)

  add object oTBCOMMENT_1_3 as StdField with uid="JHTIMRNHKF",rtseq=2,rtrep=.f.,;
    cFormVar = "w_TBCOMMENT", cQueryName = "TBCOMMENT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 205080516,;
   bGlobalFont=.t.,;
    Height=21, Width=370, Left=379, Top=9, InputMask=replicate('X',60)

  add object oRETABDTL_1_4 as StdField with uid="WHQFZGDVZU",rtseq=3,rtrep=.f.,;
    cFormVar = "w_RETABDTL", cQueryName = "RETABDTL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 41702754,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=257, Top=33, InputMask=replicate('X',15)

  add object oTBCOMMDTL_1_6 as StdField with uid="ZFHQFONKXR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_TBCOMMDTL", cQueryName = "TBCOMMDTL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    HelpContextID = 205080394,;
   bGlobalFont=.t.,;
    Height=21, Width=370, Left=379, Top=33, InputMask=replicate('X',60)


  add object oFILRECSE_1_7 as StdCombo with uid="APMIRUZGNI",rtseq=5,rtrep=.f.,left=57,top=9,width=93,height=22;
    , ToolTipText = "Filtro su selezione righe";
    , HelpContextID = 29153435;
    , cFormVar="w_FILRECSE",RowSource=""+"Tutte,"+"Selezionate,"+"Non selez.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFILRECSE_1_7.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'N',;
    space(1)))))
  endfunc
  func oFILRECSE_1_7.GetRadio()
    this.Parent.oContained.w_FILRECSE = this.RadioValue()
    return .t.
  endfunc

  func oFILRECSE_1_7.SetRadio()
    this.Parent.oContained.w_FILRECSE=trim(this.Parent.oContained.w_FILRECSE)
    this.value = ;
      iif(this.Parent.oContained.w_FILRECSE=='T',1,;
      iif(this.Parent.oContained.w_FILRECSE=='S',2,;
      iif(this.Parent.oContained.w_FILRECSE=='N',3,;
      0)))
  endfunc

  add object oFILMST_1_8 as StdCheck with uid="ZHHKFTCLKM",rtseq=6,rtrep=.f.,left=756, top=9, caption="",;
    ToolTipText = "Se attivo visualizza i campi della tabella selezionata nello zoom",;
    HelpContextID = 208152490,;
    cFormVar="w_FILMST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFILMST_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILMST_1_8.GetRadio()
    this.Parent.oContained.w_FILMST = this.RadioValue()
    return .t.
  endfunc

  func oFILMST_1_8.SetRadio()
    this.Parent.oContained.w_FILMST=trim(this.Parent.oContained.w_FILMST)
    this.value = ;
      iif(this.Parent.oContained.w_FILMST=='S',1,;
      0)
  endfunc

  func oFILMST_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FILDTL='S')
    endwith
   endif
  endfunc

  func oFILMST_1_8.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_RETABMST) OR EMPTY(.w_RETABDTL))
    endwith
  endfunc

  add object oFILDTL_1_9 as StdCheck with uid="URTVPLZJJO",rtseq=7,rtrep=.f.,left=756, top=31, caption="",;
    ToolTipText = "Se attivo visualizza i campi della tabella dettaglio selezionata nello zoom",;
    HelpContextID = 73476010,;
    cFormVar="w_FILDTL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFILDTL_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFILDTL_1_9.GetRadio()
    this.Parent.oContained.w_FILDTL = this.RadioValue()
    return .t.
  endfunc

  func oFILDTL_1_9.SetRadio()
    this.Parent.oContained.w_FILDTL=trim(this.Parent.oContained.w_FILDTL)
    this.value = ;
      iif(this.Parent.oContained.w_FILDTL=='S',1,;
      0)
  endfunc

  func oFILDTL_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FILMST='S')
    endwith
   endif
  endfunc

  func oFILDTL_1_9.mHide()
    with this.Parent.oContained
      return (EMPTY(.w_RETABMST) OR EMPTY(.w_RETABDTL))
    endwith
  endfunc


  add object ZOOMFLDS as cp_szoombox with uid="GWVVIYCEIB",left=11, top=58, width=765,height=432,;
    caption='ZOOMFLDS',;
   bGlobalFont=.t.,;
    bQueryOnLoad=.t.,bReadOnly=.f.,cMenuFile="",cTable="XDC_FIELDS",cZoomFile="GSCF_KCR",bOptions=.t.,bAdvOptions=.f.,cZoomOnZoom="",bQueryOnDblClick=.t.,bNoZoomGridShape=.f.,;
    nPag=1;
    , HelpContextID = 180883433


  add object oBtn_1_12 as StdButton with uid="WOOVXXVTWQ",left=11, top=506, width=48,height=45,;
    CpPicture="BMP\Check.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare tutti i campi";
    , HelpContextID = 4941274;
    , Caption='\<Sel. tutti';
  , bGlobalFont=.t.

    proc oBtn_1_12.Click()
      with this.Parent.oContained
        gscf_bcr(this.Parent.oContained,"SELEZ")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_13 as StdButton with uid="NWXMVNCBXX",left=63, top=506, width=48,height=45,;
    CpPicture="BMP\UnCheck.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per deselezionare tutti i campi";
    , HelpContextID = 5038086;
    , Caption='\<Desel. tutti';
  , bGlobalFont=.t.

    proc oBtn_1_13.Click()
      with this.Parent.oContained
        gscf_bcr(this.Parent.oContained,"DESELEZ")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_14 as StdButton with uid="ZPJFCAWGCO",left=115, top=506, width=48,height=45,;
    CpPicture="BMP\InvCheck.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per invertire la selezione dei campi";
    , HelpContextID = 256835138;
    , Caption='\<Inv. selez.';
  , bGlobalFont=.t.

    proc oBtn_1_14.Click()
      with this.Parent.oContained
        gscf_bcr(this.Parent.oContained,"INVSELEZ")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="VQZOPPMTLN",left=167, top=506, width=48,height=45,;
    CpPicture="bmp\IMP_LIC.ico", caption="", nPag=1;
    , ToolTipText = "Premere per selezionare i campi della chiave primaria e settare il relativo check";
    , HelpContextID = 104104488;
    , Caption='Sel. \<ch.';
  , bGlobalFont=.t.

    proc oBtn_1_15.Click()
      with this.Parent.oContained
        gscf_bcr(this.Parent.oContained,"SELPRIMKEY")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oFLMONINS_1_16 as StdCheck with uid="IVQTWDUKYU",rtseq=9,rtrep=.f.,left=234, top=515, caption="Inserimento",;
    ToolTipText = "Monitorizza le operazioni di inserimento di nuovi record",;
    HelpContextID = 129373271,;
    cFormVar="w_FLMONINS", bObbl = .f. , nPag = 1;
    , oldvalue=2;
   , bGlobalFont=.t.


  func oFLMONINS_1_16.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    0)))
  endfunc
  func oFLMONINS_1_16.GetRadio()
    this.Parent.oContained.w_FLMONINS = this.RadioValue()
    return .t.
  endfunc

  func oFLMONINS_1_16.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLMONINS==1,1,;
      iif(this.Parent.oContained.w_FLMONINS==2,2,;
      0))
  endfunc

  func oFLMONINS_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLBLKINS<>1)
    endwith
   endif
  endfunc

  proc oFLMONINS_1_16.mAfter
      with this.Parent.oContained
        threestate(this.Parent.oContained,this)
      endwith
  endproc

  add object oFLMONDEL_1_17 as StdCheck with uid="VFENKHIEET",rtseq=10,rtrep=.f.,left=234, top=533, caption="Cancellazione",;
    ToolTipText = "Monitorizza le operazioni di cancellazione dei record",;
    HelpContextID = 55176098,;
    cFormVar="w_FLMONDEL", bObbl = .f. , nPag = 1;
    , oldvalue=2;
   , bGlobalFont=.t.


  func oFLMONDEL_1_17.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    0)))
  endfunc
  func oFLMONDEL_1_17.GetRadio()
    this.Parent.oContained.w_FLMONDEL = this.RadioValue()
    return .t.
  endfunc

  func oFLMONDEL_1_17.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLMONDEL==1,1,;
      iif(this.Parent.oContained.w_FLMONDEL==2,2,;
      0))
  endfunc

  proc oFLMONDEL_1_17.mAfter
      with this.Parent.oContained
        threestate(this.Parent.oContained,this)
      endwith
  endproc


  add object oFLMONMOD_1_18 as StdCombo with uid="KKEPELKXYN",rtseq=11,rtrep=.f.,left=407,top=518,width=105,height=22;
    , ToolTipText = "Monitorizza/impedisce le operazioni di modifica dei record";
    , HelpContextID = 62264422;
    , cFormVar="w_FLMONMOD",RowSource=""+"Ignora,"+"Rileva,"+"Blocca,"+"Obbligatorio,"+"Invariato", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLMONMOD_1_18.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'I',;
    iif(this.value =4,'O',;
    iif(this.value =5,'X',;
    space(1)))))))
  endfunc
  func oFLMONMOD_1_18.GetRadio()
    this.Parent.oContained.w_FLMONMOD = this.RadioValue()
    return .t.
  endfunc

  func oFLMONMOD_1_18.SetRadio()
    this.Parent.oContained.w_FLMONMOD=trim(this.Parent.oContained.w_FLMONMOD)
    this.value = ;
      iif(this.Parent.oContained.w_FLMONMOD=='N',1,;
      iif(this.Parent.oContained.w_FLMONMOD=='S',2,;
      iif(this.Parent.oContained.w_FLMONMOD=='I',3,;
      iif(this.Parent.oContained.w_FLMONMOD=='O',4,;
      iif(this.Parent.oContained.w_FLMONMOD=='X',5,;
      0)))))
  endfunc

  add object oFLBLKINS_1_19 as StdCheck with uid="FARSGGLXWK",rtseq=12,rtrep=.f.,left=527, top=515, caption="Blocca inserimento",;
    ToolTipText = "Dato bloccato in caricamento: il campo deve restare vuoto",;
    HelpContextID = 132760663,;
    cFormVar="w_FLBLKINS", bObbl = .f. , nPag = 1;
    , oldvalue=2;
   , bGlobalFont=.t.


  func oFLBLKINS_1_19.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    0)))
  endfunc
  func oFLBLKINS_1_19.GetRadio()
    this.Parent.oContained.w_FLBLKINS = this.RadioValue()
    return .t.
  endfunc

  func oFLBLKINS_1_19.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLBLKINS==1,1,;
      iif(this.Parent.oContained.w_FLBLKINS==2,2,;
      0))
  endfunc

  func oFLBLKINS_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FLMONINS<>1)
    endwith
   endif
  endfunc

  proc oFLBLKINS_1_19.mAfter
      with this.Parent.oContained
        threestate(this.Parent.oContained,this)
      endwith
  endproc

  add object oFLOBBINS_1_20 as StdCheck with uid="FIWAWSPMLT",rtseq=13,rtrep=.f.,left=527, top=533, caption="inser. obbligatorio",;
    ToolTipText = "Dato obbligatorio in caricamento",;
    HelpContextID = 142799959,;
    cFormVar="w_FLOBBINS", bObbl = .f. , nPag = 1;
    , oldvalue=2;
   , bGlobalFont=.t.


  func oFLOBBINS_1_20.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    0)))
  endfunc
  func oFLOBBINS_1_20.GetRadio()
    this.Parent.oContained.w_FLOBBINS = this.RadioValue()
    return .t.
  endfunc

  func oFLOBBINS_1_20.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_FLOBBINS==1,1,;
      iif(this.Parent.oContained.w_FLOBBINS==2,2,;
      0))
  endfunc

  proc oFLOBBINS_1_20.mAfter
      with this.Parent.oContained
        threestate(this.Parent.oContained,this)
      endwith
  endproc


  add object oBtn_1_22 as StdButton with uid="VMPFRPKBFV",left=676, top=506, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare l'inserimento";
    , HelpContextID = 99206426;
    , Caption='\<Ok';
  , bGlobalFont=.t.

    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_FILRECSE<>'N')
      endwith
    endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="MTXNONHMHL",left=728, top=506, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 91917754;
    , Caption='\<Esci';
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_33 as cp_setobjprop with uid="HQZWMMGBCZ",left=676, top=619, width=38,height=38,;
    caption='Object',;
   bGlobalFont=.t.,;
    cObj="w_FLBLKINS",cProp="oldvalue",;
    cEvent = "w_FLOBBINS Changed, w_FLMONINS Changed",;
    nPag=1;
    , HelpContextID = 78762470


  add object oObj_1_34 as cp_setobjprop with uid="AKQZELYWQW",left=714, top=619, width=38,height=38,;
    caption='Object',;
   bGlobalFont=.t.,;
    cObj="w_FLOBBINS",cProp="oldvalue",;
    cEvent = "w_FLBLKINS Changed, w_FLMONINS Changed",;
    nPag=1;
    , HelpContextID = 78762470


  add object oObj_1_35 as cp_setobjprop with uid="NRYIPVFKMH",left=752, top=619, width=38,height=38,;
    caption='Object',;
   bGlobalFont=.t.,;
    cObj="w_FLMONINS",cProp="oldvalue",;
    cEvent = "w_FLOBBINS Changed, w_FLBLKINS Changed",;
    nPag=1;
    , HelpContextID = 78762470

  add object oStr_1_2 as StdString with uid="FKGDNOCCKY",Visible=.t., Left=180, Top=11,;
    Alignment=1, Width=71, Height=18,;
    Caption="Tabella:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="IFXUKFKLYF",Visible=.t., Left=137, Top=35,;
    Alignment=1, Width=114, Height=18,;
    Caption="Tabella dettaglio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="BIFBVSCBWR",Visible=.t., Left=229, Top=500,;
    Alignment=2, Width=295, Height=18,;
    Caption="Operazioni da monitorare per i campi selezionati"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="QSEDKFDFAD",Visible=.t., Left=342, Top=525,;
    Alignment=1, Width=64, Height=18,;
    Caption="Modifica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="STRFNIVDXK",Visible=.t., Left=522, Top=500,;
    Alignment=2, Width=149, Height=18,;
    Caption="Controllo dati"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="GGXFCHPNLZ",Visible=.t., Left=11, Top=11,;
    Alignment=1, Width=44, Height=18,;
    Caption="Righe:"  ;
  , bGlobalFont=.t.

  add object oBox_1_25 as StdBox with uid="ICZFNEMMGU",left=229, top=496, width=295,height=60

  add object oBox_1_28 as StdBox with uid="LKXQZWNTQN",left=522, top=496, width=149,height=60
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscf_kcr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscf_kcr
proc AddPrimKeyCheckBox(objmask,obj)
  * Aggiungo il checkbox per indicare che il campo deve essere trattato come chiave del record
  local i
  IF obj.grd.columncount>0
	  for i=1 to obj.grd.columncount
	    n=alltrim(str(i))
	    if UPPER( obj.grd.Columns(i).ControlSource )=='PRIMKEY'
	      exit
	    endif
	  next
	  if !PEMSTATUS(obj.grd.Columns(i), 'PrimKey', 5)
	    obj.grd.Columns(i).AddObject('PrimKey','zzboxcheckbox')
	    obj.grd.Columns(i).CurrentControl='PRIMKEY'
	    obj.grd.Columns(i).PrimKey.visible=.t.
	    obj.grd.Columns(i).PrimKey.caption=''
	    obj.grd.Columns(i).sparse=.f.
	    obj.grd.Columns(i).Alignment=2 &&Allineamento centrato del checkbox
	  ENDIF
  endif
endproc

proc AddRefRecCheckBox(objmask,obj)
  * Aggiungo il checkbox per indicare che il campo deve essere trattato come chiave del record
  local i
  IF obj.grd.columncount>0
	  for i=1 to obj.grd.columncount
	    n=alltrim(str(i))
	    if UPPER( obj.grd.Columns(i).ControlSource )=='REFREC'
	      exit
	    endif
	  next
	  if !PEMSTATUS(obj.grd.Columns(i), 'RefRec', 5)
	    obj.grd.Columns(i).AddObject('RefRec','zzboxcheckbox')
	    obj.grd.Columns(i).CurrentControl='REFREC'
	    obj.grd.Columns(i).RefRec.visible=.t.
	    obj.grd.Columns(i).RefRec.caption=''
	    obj.grd.Columns(i).sparse=.f.
	    obj.grd.Columns(i).Alignment=2 &&Allineamento centrato del checkbox
	  ENDIF
  ENDIF
endproc

procedure threestate (oParent,oControl)
  IF oControl.oldvalue=1
  	oControl.value=2
  	oControl.GetRadio()
  Endif
  oControl.oldvalue=oControl.value
endproc
* --- Fine Area Manuale
