* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_be2                                                        *
*              CONTROLLI CONTRIBUTI                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-23                                                      *
* Last revis.: 2008-01-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo,pCodice
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_be2",oParentObject,m.pTipo,m.pCodice)
return(i_retval)

define class tgsar_be2 as StdBatch
  * --- Local variables
  pTipo = space(1)
  pCodice = space(15)
  w_MOVEMENT = .NULL.
  w_CURS = space(10)
  w_WORKAREA = 0
  w_FIELDS = space(250)
  w_WHERE = space(250)
  w_ORDERBY = space(250)
  w_TERNAPRECEDENTE = space(35)
  w_NUMERORIGAESAMINATA = 0
  w_DATAFINALEPRECEDENTE = ctod("  /  /  ")
  * --- WorkFile variables
  CONTRINT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- CONTROLLA CHE PER LA STESSA TERNA CONTRIBUTO-SISTEMA-CATEGORIA
    *     NON CI SIANO PERIODI SOVRAPPOSTI
    *     Se pTipo valorizzato allora chiamato dalla rotuine di inserimento
    *     massivo, non deve leggere dal transitorio ma direttamente dal database
    *     (tipo e codice contengono l'intestatario sul quale eseguire il check)
    this.w_WORKAREA = SELECT()
    this.w_TERNAPRECEDENTE = ""
    this.w_NUMERORIGAESAMINATA = 0
    this.oParentObject.w_ERRORE = ""
    * --- La sovrapposizione delle date deve essere controllata per ogni combinazione
    *     TIPO CONTRIBUTO - SISTEMA - CATEGORIA CONTRIBUTO
    if Vartype(this.pTipo)="C"
      * --- Select from CONTRINT
      i_nConn=i_TableProp[this.CONTRINT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTRINT_idx,2],.t.,this.CONTRINT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MCTIPCAT,MCCODSIS,MCCODCAT,MCDATATT,MCDATSCA  from "+i_cTable+" CONTRINT ";
            +" where MCTIPINT="+cp_ToStrODBC(this.pTipo)+" And MCCODINT="+cp_ToStrODBC(this.pCodice)+"";
            +" order by MCTIPCAT,MCCODSIS,MCCODCAT,MCDATATT,MCDATSCA";
             ,"_Curs_CONTRINT")
      else
        select MCTIPCAT,MCCODSIS,MCCODCAT,MCDATATT,MCDATSCA from (i_cTable);
         where MCTIPINT=this.pTipo And MCCODINT=this.pCodice;
         order by MCTIPCAT,MCCODSIS,MCCODCAT,MCDATATT,MCDATSCA;
          into cursor _Curs_CONTRINT
      endif
      if used('_Curs_CONTRINT')
        select _Curs_CONTRINT
        locate for 1=1
        do while not(eof())
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if NOT EMPTY( this.oParentObject.w_ERRORE )
          exit
        endif
          select _Curs_CONTRINT
          continue
        enddo
        use
      endif
    else
      this.w_MOVEMENT = this.oParentObject
      this.w_CURS = SYS( 2015 )
      this.w_FIELDS = "t_MCTIPCAT As MCTIPCAT, NVL(t_MCCODSIS,Space(15)) AS MCCODSIS, NVL(t_MCCODCAT,Space(15)) AS MCCODCAT, t_MCDATATT as MCDATATT , t_MCDATSCA as MCDATSCA"
      this.w_WHERE = "NOT DELETED()"
      this.w_ORDERBY = "MCTIPCAT ,MCCODSIS, MCCODCAT, MCDATATT, MCDATSCA"
      * --- INTERROGA IL TEMPORANEO
      this.w_MOVEMENT.EXEC_SELECT(this.w_CURS, this.w_FIELDS, this.w_WHERE, this.w_ORDERBY, "", "")     
      SELECT( this.w_CURS )
      GO TOP
      do while NOT EOF()
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if EMPTY( this.oParentObject.w_ERRORE )
          SKIP
        else
          exit
        endif
      enddo
      if USED( this.w_CURS )
        SELECT( this.w_CURS )
        USE
      endif
    endif
    SELECT( this.w_WORKAREA )
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if this.w_TERNAPRECEDENTE <> MCTIPCAT + Nvl( MCCODSIS, Space(15)) + Nvl( MCCODCAT , Space(15))
      * --- SE SI TRATTA DI UNA NUOVA TERNA IL CONTROLLO RICOMINCIA
      this.w_NUMERORIGAESAMINATA = 1
    endif
    if this.w_NUMERORIGAESAMINATA = 1
      * --- SE SIAMO SULLA PRIMA RIGA DELLA TERNA SI CONTROLLA SOLTANTO CHE LA DATA INIZIALE SIA INFERIORE
      *     A QUELLA FINALE
      if  MCDATSCA < MCDATATT
        this.oParentObject.w_ERRORE = "1"
      endif
    else
      * --- SE NON SIAMO SULLA PRIMA RIGA DELLA TERNA SI CONTROLLA CHE LA DATA INIZIALE SIA INFERIORE
      *     A QUELLA FINALE E CHE LA DATA FINALE DEL RECORD PRECEDENTE SIA INFERIORE ALLA DATA INIZIALE DEL
      *     RECORD SUCCESSIVO
      if  MCDATSCA < MCDATATT
        this.oParentObject.w_ERRORE = "1"
      endif
      if  MCDATATT <= this.w_DATAFINALEPRECEDENTE
        this.oParentObject.w_ERRORE = "2"
      endif
    endif
    this.w_NUMERORIGAESAMINATA = this.w_NUMERORIGAESAMINATA + 1
    this.w_DATAFINALEPRECEDENTE = NVL( MCDATSCA , CP_CHARTODATE("") )
    this.w_TERNAPRECEDENTE = MCTIPCAT + Nvl( MCCODSIS, Space(15)) + Nvl( MCCODCAT , Space(15))
  endproc


  proc Init(oParentObject,pTipo,pCodice)
    this.pTipo=pTipo
    this.pCodice=pCodice
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONTRINT'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_CONTRINT')
      use in _Curs_CONTRINT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo,pCodice"
endproc
