* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bgp                                                        *
*              Apertura pratica selezionata                                    *
*                                                                              *
*      Author: Gianluca B.                                                     *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2009-06-08                                                      *
* Last revis.: 2009-06-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bgp",oParentObject)
return(i_retval)

define class tgsut_bgp as StdBatch
  * --- Local variables
  w_OBJECT = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch apertura praticaeseguito da GSUT_KVT
    if ! empty(nvl(this.oParentObject.w_PRATICA," ")) and this.oParentObject.w_PRATICA # "@@@"
      * --- Apro la gestione con i dati rlativi alla pratica selezionata caricati
      this.w_OBJECT = GSPR_ACN()
      if !(this.w_OBJECT.bSec1)
        i_retcode = 'stop'
        return
      endif
      this.w_OBJECT.EcpFilter()     
      * --- Valorizzo il campo chiave
      this.w_OBJECT.w_CNCODCAN = this.oParentObject.w_PRATICA
      * --- Carico il record richiesto
      this.w_OBJECT.EcpSave()     
    else
      Ah_Errormsg("Selezionare un nodo di tipo pratica",48,"")
    endif
    this.w_OBJECT = .NULL.
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
