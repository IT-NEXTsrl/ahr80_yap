* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bst                                                        *
*              Calcolo spese trasporto/imballo                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_15]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-12-17                                                      *
* Last revis.: 2015-07-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pNoAsk,pNomeCursore
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bst",oParentObject,m.pNoAsk,m.pNomeCursore)
return(i_retval)

define class tgsar_bst as StdBatch
  * --- Local variables
  pTIPO = .f.
  pNoAsk = .f.
  pNomeCursore = space(10)
  w_PADRE = .NULL.
  w_MESIMB = .f.
  w_MESTRA = .f.
  w_MESCOLIMB = .f.
  w_MESCOLTRA = .f.
  w_MESTOTIMB = .f.
  w_MESTOTTRA = .f.
  w_MESPESIMB = .f.
  w_MESPESTRA = .f.
  w_ASKYETIMB = .f.
  w_ASKYETTRA = .f.
  w_SPEVET1 = 0
  w_SPEVET2 = 0
  w_SPEVET3 = 0
  w_TEST = .f.
  w_TEST = .f.
  pTIPO = .f.
  pASK = .f.
  w_PREZZO = 0
  w_OLDSPE = 0
  w_BASCAL = space(1)
  w_MSIMPFIS = 0
  w_MSIMPMIN = 0
  w_MSIMPMAX = 0
  w_CODICE = space(5)
  w_VALUTA = space(3)
  w_CURSOR = space(20)
  w_QTACOL = 0
  w_QTALOR = 0
  w_IMPFIS = 0
  w_IMPUNI = 0
  w_QTAIMP = 0
  w_OK = .f.
  w_MESS = space(254)
  w_CICLO = .f.
  w_TEST = .f.
  w_RICSPEIMB = .f.
  w_RICSPETRA = .f.
  w_TEST = .f.
  pASK = .f.
  w_PREZZO = 0
  w_OLDSPE = 0
  w_BASCAL = space(1)
  w_MSIMPFIS = 0
  w_MSIMPMIN = 0
  w_MSIMPMAX = 0
  w_CODICE = space(5)
  w_VALUTA = space(3)
  w_CURSOR = space(20)
  w_QTACOL = 0
  w_QTALOR = 0
  w_IMPFIS = 0
  w_IMPUNI = 0
  w_QTAIMP = 0
  w_OK = .f.
  w_MESS = space(254)
  w_DECTOT = 0
  w_oMESS = .NULL.
  * --- WorkFile variables
  METCALSP_idx=0
  MCALSCAP_idx=0
  VALUTE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola spese Imballo e Trasporto (da GSVE_BCC, GSVE_MDV, GSAC_MDV, GSOR_MDV,GSAL_BCI)
    * --- Specifichiamo il nome del cursore nel caso in cui non ci sia un transitorio da elaborare
    this.w_PADRE = This.oParentObject
    if ! VARTYPE(this.pNomeCursore)="C"
      * --- Attivit� sul transitorio solo se non c'� un cursore come parametro
      if lower(this.w_PADRE.CLASS)="tgsof_aof"
        this.w_PADRE.GSOF_MDO.MARKPOS()     
      else
        this.w_PADRE.MARKPOS()     
      endif
    endif
    if (this.w_PADRE.w_FLSPIM = "S" Or this.w_PADRE.w_FLSPTR = "S") 
      if IsAlt() 
        this.Page_3()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        if lower(this.w_PADRE.CLASS)="tgsof_aof"
          this.w_PADRE.GSOF_MDO.Exec_Select("_TMP_BCC","t_CPROWORD , CPROWNUM,  0 AS t_MVNUMCOL, t_ODTIPRIG AS t_MVTIPRIG , t_ODCODICE AS t_MVCODICE , t_ODVALRIG AS t_MVVALRIG, t_ODFLOMAG AS t_MVFLOMAG","t_CPROWORD<>0 AND NOT EMPTY( t_ODCODICE ) AND NOT DELETED() ","","","")     
          this.w_VALUTA = this.w_PADRE.w_OFCODVAL
          this.w_QTACOL = 0
          this.w_QTALOR = 0
          this.w_CURSOR = "_TMP_BCC"
          if this.w_PADRE.w_FLSPIM = "S" And (this.w_PADRE.w_CHKSPEIMB Or this.w_PADRE.cFunction<>"Load")
            * --- Calcolo Spese Imballo
            this.w_OLDSPE = this.w_PADRE.w_OFSPEIMB
            this.pTIPO = .T.
            this.w_CODICE = this.w_PADRE.w_MCALSI
            if !empty(this.w_CODICE)
              this.pASK = .T.
              this.w_MESIMB = .F.
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_PADRE.w_OFSPEIMB = this.w_PREZZO
            endif
          endif
          this.w_PREZZO = 0
          if this.w_PADRE.w_FLSPTR = "S" And (this.w_PADRE.w_CHKSPETRA Or this.w_PADRE.cFunction<>"Load")
            * --- Calcolo Spese Trasporto
            this.w_OLDSPE = this.w_PADRE.w_OFSPETRA
            this.pTIPO = .F.
            this.w_CODICE = this.w_PADRE.w_MCALST
            if !empty(this.w_CODICE)
              this.pASK = .T.
              this.w_MESTRA = .F.
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_PADRE.w_OFSPETRA = this.w_PREZZO
            endif
          endif
        else
          if ! VARTYPE(this.pNomeCursore)="C"
            if UPPER( g_APPLICATION ) <> "AD HOC ENTERPRISE"
              this.w_PADRE.Exec_Select("_TMP_BCC","t_CPROWORD , CPROWNUM,  t_MVNUMCOL, t_MVTIPRIG , t_MVCODICE , t_MVVALRIG, t_MVFLOMAG","t_CPROWORD<>0 AND NOT EMPTY( t_MVCODICE ) AND NOT DELETED() ","","","")     
            else
              this.w_PADRE.Exec_Select("_TMP_BCC","t_CPROWORD , CPROWNUM, t_MVRIFKIT, t_MVNUMCOL, t_MVTIPRIG , t_MVCODICE , t_MVVALRIG, t_MVFLOMAG","t_CPROWORD<>0 AND NOT EMPTY( t_MVCODICE ) AND NOT DELETED() ","","","")     
            endif
            this.w_CURSOR = "_TMP_BCC"
          endif
          this.w_VALUTA = this.w_PADRE.w_MVCODVAL
          if TYPE("this.w_PADRE.oparentobject")=="O" AND lower(this.w_PADRE.oparentobject.class)=="tgsar_bgd"
            this.w_QTACOL = this.w_PADRE.oParentObject.w_MVQTACOL
            this.w_QTALOR = this.w_PADRE.oParentObject.w_MVQTALOR
          else
            this.w_QTACOL = this.w_PADRE.w_MVQTACOL
            this.w_QTALOR = this.w_PADRE.w_MVQTALOR
          endif
          this.w_MESCOLIMB = .F.
          this.w_MESCOLTRA = .F.
          this.w_MESPESIMB = .F.
          this.w_MESPESTRA = .F.
          this.w_ASKYETIMB = .F.
          this.w_ASKYETTRA = .F.
          this.w_MESTOTIMB = .F.
          this.w_MESTOTTRA = .F.
          if this.w_PADRE.w_FLSPIM = "S" And (this.w_PADRE.w_CHKSPEIMB Or this.w_PADRE.cFunction<>"Load")
            * --- Calcolo Spese Imballo
            this.w_OLDSPE = this.w_PADRE.w_MVSPEIMB
            this.pTIPO = .T.
            if !(this.w_OLDSPE<>0 and VARTYPE(this.pNomeCursore)="C")
              if Empty(this.w_PADRE.w_MCSIVT1) And Empty(this.w_PADRE.w_MCSIVT2) And Empty(this.w_PADRE.w_MCSIVT3)
                this.w_CODICE = this.w_PADRE.w_MCALSI
                if !empty(this.w_CODICE)
                  this.pASK = .T.
                  if ! VARTYPE(this.pNomeCursore)="C"
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  else
                    this.Page_5()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                  this.w_PADRE.w_MVSPEIMB = this.w_PREZZO
                endif
              else
                this.pASK = .F.
                this.w_CODICE = this.w_PADRE.w_MCSIVT1
                if !empty(this.w_CODICE)
                  if ! VARTYPE(this.pNomeCursore)="C"
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  else
                    this.Page_5()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                  this.w_SPEVET1 = this.w_PREZZO
                endif
                this.w_CODICE = this.w_PADRE.w_MCSIVT2
                if !empty(this.w_CODICE)
                  if ! VARTYPE(this.pNomeCursore)="C"
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  else
                    this.Page_5()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                  this.w_SPEVET2 = this.w_PREZZO
                endif
                this.w_CODICE = this.w_PADRE.w_MCSIVT3
                if !empty(this.w_CODICE)
                  if ! VARTYPE(this.pNomeCursore)="C"
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  else
                    this.Page_5()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                  this.w_SPEVET3 = this.w_PREZZO
                endif
                if (this.w_SPEVET1+this.w_SPEVET2+this.w_SPEVET3)<>0 AND this.w_PADRE.w_MVSPEIMB<>(this.w_SPEVET1+this.w_SPEVET2+this.w_SPEVET3) And Not this.pNoAsk
                  if !this.w_ASKYETIMB
                    this.w_TEST = ah_YesNo("Attenzione, spese di imballo gi� valorizzate%0Si desidera ricalcolarle?")
                    if this.w_TEST
                      this.w_PADRE.w_MVSPEIMB = this.w_SPEVET1+this.w_SPEVET2+this.w_SPEVET3
                    else
                      this.w_ASKYETIMB = .T.
                    endif
                  endif
                else
                  if (this.w_SPEVET1+this.w_SPEVET2+this.w_SPEVET3)<>0
                    this.w_PADRE.w_MVSPEIMB = this.w_SPEVET1+this.w_SPEVET2+this.w_SPEVET3
                  endif
                endif
              endif
            endif
          endif
          this.w_QTAIMP = 0
          this.w_PREZZO = 0
          if this.w_PADRE.w_FLSPTR = "S" And (this.w_PADRE.w_CHKSPETRA Or this.w_PADRE.cFunction<>"Load")
            * --- Calcolo Spese Trasporto
            this.w_OLDSPE = this.w_PADRE.w_MVSPETRA
            this.pTIPO = .F.
            if !(this.w_OLDSPE<>0 and VARTYPE(this.pNomeCursore)="C")
              if Empty(this.w_PADRE.w_MCSTVT1) And Empty(this.w_PADRE.w_MCSTVT2) And Empty(this.w_PADRE.w_MCSTVT3)
                this.w_CODICE = this.w_PADRE.w_MCALST
                if !EMPTY(this.w_CODICE)
                  this.pASK = .T.
                  if ! VARTYPE(this.pNomeCursore)="C"
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  else
                    this.Page_5()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                  this.w_PADRE.w_MVSPETRA = this.w_PREZZO
                endif
              else
                this.pASK = .F.
                this.w_CODICE = this.w_PADRE.w_MCSTVT1
                if !EMPTY(this.w_CODICE)
                  if ! VARTYPE(this.pNomeCursore)="C"
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  else
                    this.Page_5()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                  this.w_SPEVET1 = this.w_PREZZO
                endif
                this.w_CODICE = this.w_PADRE.w_MCSTVT2
                if !EMPTY(this.w_CODICE)
                  if ! VARTYPE(this.pNomeCursore)="C"
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  else
                    this.Page_5()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                  this.w_SPEVET2 = this.w_PREZZO
                endif
                this.w_CODICE = this.w_PADRE.w_MCSTVT3
                if !EMPTY(this.w_CODICE)
                  if ! VARTYPE(this.pNomeCursore)="C"
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  else
                    this.Page_5()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                  this.w_SPEVET3 = this.w_PREZZO
                endif
                if (this.w_SPEVET1+this.w_SPEVET2+this.w_SPEVET3)<>0 AND this.w_PADRE.w_MVSPETRA<>(this.w_SPEVET1+this.w_SPEVET2+this.w_SPEVET3) And Not this.pNoAsk
                  if !this.w_ASKYETTRA
                    this.w_TEST = ah_YesNo("Attenzione, spese di trasporto gi� valorizzate%0Si desidera ricalcolarle?")
                    if this.w_TEST
                      this.w_PADRE.w_MVSPETRA = this.w_SPEVET1+this.w_SPEVET2+this.w_SPEVET3
                    else
                      this.w_ASKYETTRA = .T.
                    endif
                  endif
                else
                  if (this.w_SPEVET1+this.w_SPEVET2+this.w_SPEVET3)<>0
                    this.w_PADRE.w_MVSPETRA = this.w_SPEVET1+this.w_SPEVET2+this.w_SPEVET3
                  endif
                endif
              endif
            endif
          endif
        endif
        if ! VARTYPE(this.pNomeCursore)="C"
          Use in _TMP_BCC
        else
          USE IN SELECT(this.pNomeCursore)
        endif
      endif
    endif
    if ! VARTYPE(this.pNomeCursore)="C"
      * --- Attivit� sul transitorio solo se non c'� un cursore come parametro
      if lower(this.w_PADRE.CLASS)="tgsof_aof"
        this.w_PADRE.GSOF_MDO.REPOS()     
      else
        this.w_PADRE.REPOS()     
      endif
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo automatico spese trasporto / imballo
    *     pCODICE  = Metodo di calcolo
    *     pVALUTA  = Valuta del documento
    *     pCURSOR = Transitorio documenti
    *     pQTACOL  = Numero Colli
    *     pQTALOR  = Peso Lordo
    *     pOLDSPE = Vecchio valore della spesa
    *     pPADRE = Oggetto padre (GSVE_MDV, GSAC_MDV, GSOR_MDV)
    *     pTIPO = Imballo/Trasporto = .T./.F.
    *     pASK = Richiesta domanda di aggiornamento = .T.
    * --- Leggo la base di calcolo,l'importo minimo,l'importo massimo e l'importo fisso.
    *     Se la valuta del documento � diversa da quella del metodo non faccio nulla
    this.w_OK = .F.
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_VALUTA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_VALUTA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from METCALSP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.METCALSP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.METCALSP_idx,2],.t.,this.METCALSP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MSBASCAL,MSIMPFIS,MSIMPMIN,MSIMPMAX"+;
        " from "+i_cTable+" METCALSP where ";
            +"MSCODICE = "+cp_ToStrODBC(this.w_CODICE);
            +" and MSVALUTA = "+cp_ToStrODBC(this.w_VALUTA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MSBASCAL,MSIMPFIS,MSIMPMIN,MSIMPMAX;
        from (i_cTable) where;
            MSCODICE = this.w_CODICE;
            and MSVALUTA = this.w_VALUTA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_BASCAL = NVL(cp_ToDate(_read_.MSBASCAL),cp_NullValue(_read_.MSBASCAL))
      this.w_MSIMPFIS = NVL(cp_ToDate(_read_.MSIMPFIS),cp_NullValue(_read_.MSIMPFIS))
      this.w_MSIMPMIN = NVL(cp_ToDate(_read_.MSIMPMIN),cp_NullValue(_read_.MSIMPMIN))
      this.w_MSIMPMAX = NVL(cp_ToDate(_read_.MSIMPMAX),cp_NullValue(_read_.MSIMPMAX))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if lower(this.w_PADRE.CLASS)="tgsof_aof"
      * --- Se la base di calcolo non � totale righe documento o totale righe esclusi servizi
      *     annullo la ricerca perch� gli altri calcoli non sono applicabili alle offerte
      this.w_BASCAL = IIF( !INLIST( this.w_BASCAL,"T","S"), "Z", this.w_BASCAL)
      if this.w_BASCAL="Z"
        if this.pTIPO
          if !this.w_MESIMB
            this.w_MESS = ah_Msgformat("Il metodo di calcolo utilizzato non � applicabile alle offerte: Impossibile calcolare le spese di imballo")
            this.w_MESIMB = .T.
          endif
        else
          if !this.w_MESTRA
            this.w_MESS = ah_Msgformat("Il metodo di calcolo utilizzato non � applicabile alle offerte: Impossibile calcolare le spese di trasporto")
            this.w_MESTRA = .T.
          endif
        endif
      endif
    endif
    if i_Rows > 0
      * --- Analizzando la base di calcolo decido come operare
      do case
        case INLIST( this.w_BASCAL,"T","S")
          * --- Totale righe documento o totale righe documento esclusi i servizi
          *     Considero solo le righe Normali
          if this.w_BASCAL = "T"
            if UPPER( g_APPLICATION ) <> "AD HOC ENTERPRISE"
              SELECT SUM( t_MVVALRIG ) AS VALRIG FROM ( this.w_CURSOR ) WHERE INLIST(t_MVTIPRIG,"R", "F", "M") AND t_MVFLOMAG = 1 INTO CURSOR TmpCurs
            else
              * --- -considero anche gli articoli kit
              SELECT SUM( t_MVVALRIG ) AS VALRIG, CPROWNUM FROM ( this.w_CURSOR ) WHERE INLIST(t_MVTIPRIG,"A") AND t_MVFLOMAG = "X" GROUP BY CPROWNUM INTO CURSOR TmpCurs_KIT
              if RECCOUNT("TmpCurs_KIT")>0
                * --- se ci sono gli articoli kit differenzio i cursori
                SELECT SUM( t_MVVALRIG ) AS VALRIG, t_MVRIFKIT AS MVRIFKIT FROM ( this.w_CURSOR ) WHERE INLIST(t_MVTIPRIG,"R", "F", "M") AND t_MVFLOMAG = "X" GROUP BY MVRIFKIT INTO CURSOR TmpCurs_ARTI
                SELECT SUM(IIF(TmpCurs_ARTI.VALRIG>0, TmpCurs_ARTI.VALRIG, TmpCurs_KIT.VALRIG)) AS VALRIG FROM TmpCurs_ARTI LEFT JOIN TmpCurs_KIT ON TmpCurs_ARTI.MVRIFKIT=TmpCurs_KIT.CPROWNUM INTO CURSOR TmpCurs
              else
                SELECT SUM( t_MVVALRIG ) AS VALRIG FROM ( this.w_CURSOR ) WHERE INLIST(t_MVTIPRIG,"R", "F", "M") AND t_MVFLOMAG = "X" INTO CURSOR TmpCurs
              endif
            endif
          else
            * --- Escludo i servizi
            if UPPER( g_APPLICATION ) <> "AD HOC ENTERPRISE"
              SELECT SUM( t_MVVALRIG ) AS VALRIG FROM ( this.w_CURSOR ) WHERE t_MVTIPRIG = "R" AND t_MVFLOMAG = iif(UPPER( g_APPLICATION ) <> "AD HOC ENTERPRISE",1,"X") INTO CURSOR TmpCurs
            else
              * --- -considero anche gli articoli kit
              SELECT SUM( t_MVVALRIG ) AS VALRIG, CPROWNUM FROM ( this.w_CURSOR ) WHERE INLIST(t_MVTIPRIG,"A") AND t_MVFLOMAG = "X" GROUP BY CPROWNUM INTO CURSOR TmpCurs_KIT
              if RECCOUNT("TmpCurs_KIT")>0
                * --- se ci sono gli articoli kit differenzio i cursori
                SELECT SUM( t_MVVALRIG ) AS VALRIG, t_MVRIFKIT AS MVRIFKIT FROM ( this.w_CURSOR ) WHERE INLIST(t_MVTIPRIG,"R") AND t_MVFLOMAG = "X" GROUP BY MVRIFKIT INTO CURSOR TmpCurs_ARTI
                SELECT SUM(IIF(TmpCurs_ARTI.VALRIG>0, TmpCurs_ARTI.VALRIG, TmpCurs_KIT.VALRIG)) AS VALRIG FROM TmpCurs_ARTI LEFT JOIN TmpCurs_KIT ON TmpCurs_ARTI.MVRIFKIT=TmpCurs_KIT.CPROWNUM INTO CURSOR TmpCurs
              else
                SELECT SUM( t_MVVALRIG ) AS VALRIG FROM ( this.w_CURSOR ) WHERE INLIST(t_MVTIPRIG,"R") AND t_MVFLOMAG = "X" INTO CURSOR TmpCurs
              endif
            endif
          endif
          SELECT TmpCurs
          * --- Sottraggo al totale delle righe l'eventuale sconto di piede
          if lower(this.w_PADRE.CLASS)="tgsof_aof"
            this.w_QTAIMP = VALRIG + this.w_PADRE.w_OFSCONTI
          else
            this.w_QTAIMP = VALRIG + this.w_PADRE.w_MVSCONTI
          endif
          * --- Se QTAIMP � diversa da zero individuo lo scaglione, altrimenti non valorizzo
          *     w_IMPFIS e w_IMPUNI per cui w_PREZZO sar� zero
          if this.w_QTAIMP <> 0
            * --- Select from QUERY\GSAR1MSI
            do vq_exec with 'QUERY\GSAR1MSI',this,'_Curs_QUERY_GSAR1MSI','',.f.,.t.
            if used('_Curs_QUERY_GSAR1MSI')
              select _Curs_QUERY_GSAR1MSI
              locate for 1=1
              do while not(eof())
              this.w_IMPFIS = SIIMPFIS
              this.w_IMPUNI = SIPERCEN / 100
                select _Curs_QUERY_GSAR1MSI
                continue
              enddo
              use
            endif
            this.w_OK = .T.
          else
            * --- Totale documento non valorizzato, non sono presenti righe nel documento
            if this.pTIPO
              if !this.w_MESTOTIMB
                this.w_MESS = ah_Msgformat("Totale righe documento uguale a zero: Impossibile calcolare le spese di imballo")
                this.w_MESTOTIMB = .T.
              endif
            else
              if !this.w_MESTOTTRA
                this.w_MESS = ah_Msgformat("Totale righe documento uguale a zero: Impossibile calcolare le spese di trasporto")
                this.w_MESTOTTRA = .T.
              endif
            endif
          endif
        case this.w_BASCAL = "N"
          * --- Numero colli
          this.w_QTAIMP = this.w_QTACOL
          * --- Se QTAIMP � diversa da zero individuo lo scaglione, altrimenti non valorizzo
          *     w_IMPFIS e w_IMPUNI per cui w_PREZZO sar� zero
          if this.w_QTAIMP <> 0
            * --- Select from QUERY\GSAR1MSC
            do vq_exec with 'QUERY\GSAR1MSC',this,'_Curs_QUERY_GSAR1MSC','',.f.,.t.
            if used('_Curs_QUERY_GSAR1MSC')
              select _Curs_QUERY_GSAR1MSC
              locate for 1=1
              do while not(eof())
              this.w_IMPFIS = SCIMPFIS
              this.w_IMPUNI = SCIMPUNI
                select _Curs_QUERY_GSAR1MSC
                continue
              enddo
              use
            endif
            this.w_OK = .T.
          else
            * --- Numero colli non valorizzato, non � stato premuto il bottone calcola colli
            if this.pTIPO
              if !this.w_MESCOLIMB
                if lower(this.w_PADRE.CLASS)="tgsor_mdv"
                  this.w_MESS = ah_Msgformat("Il metodo di calcolo utilizzato non � applicabile agli ordini: Impossibile calcolare le spese di imballo")
                else
                  this.w_MESS = ah_Msgformat("Numero colli non valorizzato%0Impossibile calcolare le spese di imballo")
                endif
                this.w_MESCOLIMB = .T.
              else
                this.w_MESS = ""
              endif
            else
              if !this.w_MESCOLTRA
                if lower(this.w_PADRE.CLASS)="tgsor_mdv"
                  this.w_MESS = ah_Msgformat("Il metodo di calcolo utilizzato non � applicabile agli ordini: Impossibile calcolare le spese di trasporto")
                else
                  this.w_MESS = ah_Msgformat("Numero colli non valorizzato%0Impossibile calcolare le spese di trasporto")
                endif
                this.w_MESCOLTRA = .T.
              else
                this.w_MESS = ""
              endif
            endif
          endif
        case this.w_BASCAL = "Q"
          * --- Quantit� confezioni
          * --- Lancio il ricalcolo del numero di confezioni
          this.w_PADRE.NotifyEvent("CalcolaConfezioni")     
          this.w_PADRE.Exec_Select("_TMP_BCC","t_CPROWORD , t_MVNUMCOL, t_MVTIPRIG , t_MVCODICE , t_MVVALRIG, t_MVFLOMAG","t_CPROWORD<>0 AND NOT EMPTY( t_MVCODICE ) AND NOT DELETED() ","","","")     
          * --- Sommo le confezioni delle singole righe, controllando se MVNUMCOL �
          *     valorizzato
          this.w_CICLO = .F.
          SELECT t_CPROWORD, t_MVNUMCOL FROM ( this.w_CURSOR ) WHERE t_CPROWORD<>0 AND ; 
 t_MVTIPRIG = "R" AND NOT EMPTY( t_MVCODICE ) AND NOT DELETED() INTO CURSOR TmpCurs
           
 SELECT TmpCurs 
 GO TOP 
 SCAN
          this.w_CICLO = .T.
          if t_MVNUMCOL <> 0
            this.w_QTAIMP = this.w_QTAIMP + t_MVNUMCOL
          else
            * --- Esistono righe con numero confezione a zero
            if this.pTIPO
              if lower(this.w_PADRE.CLASS)="tgsor_mdv"
                this.w_MESS = ah_Msgformat("Il metodo di calcolo utilizzato non � applicabile agli ordini: Impossibile calcolare le spese di imballo")
              else
                this.w_MESS = ah_Msgformat("Riga documento %1: numero confezioni uguale a zero%0Impossibile calcolare le spese di imballo", ALLTRIM(STR(t_CPROWORD)) )
              endif
            else
              if lower(this.w_PADRE.CLASS)="tgsor_mdv"
                this.w_MESS = ah_Msgformat("Il metodo di calcolo utilizzato non � applicabile agli ordini: Impossibile calcolare le spese di trasporto")
              else
                this.w_MESS = ah_Msgformat("Riga documento %1: numero confezioni uguale a zero%0Impossibile calcolare le spese di trasporto", ALLTRIM(STR(t_CPROWORD)) )
              endif
            endif
            this.w_QTAIMP = 0
            * --- Esco dalla scan
            EXIT
          endif
           
 SELECT TmpCurs 
 ENDSCAN
          if !this.w_CICLO
            this.w_MESS = ah_Msgformat("Deve essere presente almeno una riga nel documento")
          endif
          * --- Se QTAIMP � diversa da zero individuo lo scaglione, altrimenti non valorizzo
          *     w_IMPFIS e w_IMPUNI per cui w_PREZZO sar� zero
          if this.w_QTAIMP <> 0
            * --- Select from QUERY\GSAR1MSC
            do vq_exec with 'QUERY\GSAR1MSC',this,'_Curs_QUERY_GSAR1MSC','',.f.,.t.
            if used('_Curs_QUERY_GSAR1MSC')
              select _Curs_QUERY_GSAR1MSC
              locate for 1=1
              do while not(eof())
              this.w_IMPFIS = SCIMPFIS
              this.w_IMPUNI = SCIMPUNI
                select _Curs_QUERY_GSAR1MSC
                continue
              enddo
              use
            endif
            this.w_OK = .T.
          endif
        case this.w_BASCAL = "P"
          * --- Peso lordo
          this.w_QTAIMP = this.w_QTALOR
          * --- Se QTAIMP � diversa da zero individuo lo scaglione, altrimenti non valorizzo
          *     w_IMPFIS e w_IMPUNI per cui w_PREZZO sar� zero
          if this.w_QTAIMP <> 0
            * --- Select from QUERY\GSAR1MSP
            do vq_exec with 'QUERY\GSAR1MSP',this,'_Curs_QUERY_GSAR1MSP','',.f.,.t.
            if used('_Curs_QUERY_GSAR1MSP')
              select _Curs_QUERY_GSAR1MSP
              locate for 1=1
              do while not(eof())
              this.w_IMPFIS = SPIMPFIS
              this.w_IMPUNI = SPIMPUNI
                select _Curs_QUERY_GSAR1MSP
                continue
              enddo
              use
            endif
            this.w_OK = .T.
          else
            if this.pTIPO
              if !this.w_MESPESIMB
                if lower(this.w_PADRE.CLASS)="tgsor_mdv"
                  this.w_MESS = ah_Msgformat("Il metodo di calcolo utilizzato non � applicabile agli ordini: Impossibile calcolare le spese di imballo")
                else
                  this.w_MESS = ah_Msgformat("Peso lordo non valorizzato%0Impossibile calcolare le spese di imballo")
                endif
                this.w_MESPESIMB = .T.
              else
                this.w_MESS = ""
              endif
            else
              if !this.w_MESPESTRA
                if lower(this.w_PADRE.CLASS)="tgsor_mdv"
                  this.w_MESS = ah_Msgformat("Il metodo di calcolo utilizzato non � applicabile agli ordini: Impossibile calcolare le spese di trasporto")
                else
                  this.w_MESS = ah_Msgformat("Peso lordo non valorizzato%0Impossibile calcolare le spese di trasporto")
                endif
                this.w_MESPESTRA = .T.
              else
                this.w_MESS = ""
              endif
            endif
          endif
      endcase
      * --- Calcolo il prezzo
      *     Se w_OK=.t. significa che w_QTAIMP � <> 0
      if this.w_OK
        do case
          case this.w_IMPUNI <> 0
            * --- Importo unitario o Percentuale
            this.w_PREZZO = this.w_QTAIMP * this.w_IMPUNI
          otherwise
            * --- Importo fisso dello scaglione
            this.w_PREZZO = this.w_IMPFIS
        endcase
        * --- Aggiungo l'importo fisso
        this.w_PREZZO = this.w_PREZZO + this.w_MSIMPFIS
        * --- Verifico se il prezzo da attribuire � inferiore all'importo minimo
        if this.w_PREZZO < this.w_MSIMPMIN
          this.w_PREZZO = this.w_MSIMPMIN
        endif
        * --- Verifico se il prezzo da attribuire � superiore all'importo massimo
        *     se l'importo massimo � zero significa che non ho limite
        if this.w_MSIMPMAX <> 0 AND this.w_PREZZO > this.w_MSIMPMAX
          this.w_PREZZO = this.w_MSIMPMAX
        endif
      else
        if (this.w_BASCAL<>"Z" or lower(this.w_PADRE.CLASS)="tgsof_aof") and not empty(this.w_MESS)
          * --- Il calcolo non � andato a buon fine e quindi segnalo l'errore
          ah_ErrorMsg("%1",,"", this.w_MESS)
        endif
      endif
    else
      this.w_PREZZO = 0
    endif
    * --- Chiudo i cursori
    USE IN SELECT("TmpCurs")
    USE IN SELECT("TmpCurs_KIT")
    USE IN SELECT("TmpCurs_ARTI")
    * --- Restituisco il prezzo
    this.w_PREZZO = cp_Round(this.w_PREZZO,this.w_DECTOT)
    if lower(this.w_PADRE.CLASS)="tgsof_aof" AND this.w_PREZZO=0 
      if this.pTIPO
        this.w_PREZZO = cp_Round(this.w_PADRE.w_MOSPEIMB,this.w_DECTOT)
      else
        this.w_PREZZO = cp_Round(this.w_PADRE.w_MOSPETRA,this.w_DECTOT)
      endif
    endif
    if (this.w_OLDSPE<>0 Or this.w_PADRE.cFunction="Edit") and this.w_OLDSPE<>this.w_PREZZO AND this.pASK And Not this.pNoAsk 
      if this.pTIPO
        if !this.w_ASKYETIMB
          this.w_TEST = ah_YesNo("Attenzione, spese di imballo gi� valorizzate%0Si desidera ricalcolarle?")
          this.w_ASKYETIMB = .T.
        endif
      else
        if !this.w_ASKYETTRA
          this.w_TEST = ah_YesNo("Attenzione, spese di trasporto gi� valorizzate%0Si desidera ricalcolarle?")
          this.w_ASKYETTRA = .T.
        endif
      endif
      if Not this.w_TEST
        this.w_PREZZO = this.w_OLDSPE
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Spese AlterEgo
    if ! VARTYPE(this.pNomeCursore)="C"
      * --- Attivit� sul transitorio solo se non c'� un cursore come parametro
      this.w_PADRE.Exec_Select("_TMP_BCC","t_CPROWORD , t_MVNUMCOL, t_MVTIPRIG , t_MVCODICE , t_MVVALRIG, t_MVFLOMAG, t_PRESTA, t_MVRIFORD, t_SPEGEN,t_FLGDSPE","t_CPROWORD<>0 AND NOT EMPTY( t_MVCODICE ) AND NOT DELETED() ","","","")     
      this.w_CURSOR = "_TMP_BCC"
    else
      * --- Proveniamo da GSAL_BIA ed abbiamo gi� il cursore pronto
      this.w_CURSOR = this.pNomeCursore
    endif
    this.w_VALUTA = this.w_PADRE.w_MVCODVAL
    this.w_RICSPEIMB = .F.
    if this.w_PADRE.w_FLSPIM = "S" And (this.w_PADRE.w_CHKSPEIMB Or (VARTYPE(this.w_PADRE.cFunction)="C" AND this.w_PADRE.cFunction<>"Load")) And this.w_PADRE.w_Da_Ritenute<>"S"
      * --- Calcolo Spese Generali
      this.w_OLDSPE = this.w_PADRE.w_MVSPEIMB
      this.pTIPO = .T.
      this.w_CODICE = this.w_PADRE.w_MCALSI
      this.pASK = .T.
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_PADRE.w_MVSPEIMB = this.w_PREZZO
      this.w_RICSPEIMB = .T.
    endif
    this.w_RICSPETRA = .F.
    if this.w_PADRE.w_FLSPTR = "S" And (this.w_PADRE.w_CHKSPETRA Or this.w_PADRE.cFunction<>"Load") And this.w_PADRE.w_Da_Ritenute<>"S"
      * --- Calcolo CPA
      this.w_OLDSPE = this.w_PADRE.w_MVSPETRA
      this.pTIPO = .F.
      this.w_CODICE = this.w_PADRE.w_MCALST
      this.pASK = .T.
      this.Page_4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_PADRE.w_MVSPETRA = this.w_PREZZO
      this.w_RICSPETRA = .T.
    endif
    if this.w_PADRE.w_FLGMSPE="S"
      this.w_PADRE.w_MVSPEIMB = this.w_PADRE.w_MVSPEIMB + IIF(this.w_RICSPEIMB, this.w_PADRE.w_Impon_Gen,0)
      this.w_PADRE.w_MVSPETRA = this.w_PADRE.w_MVSPETRA + IIF(this.w_RICSPETRA,this.w_PADRE.w_Impon_Cpa,0)
    endif
    if ! VARTYPE(this.pNomeCursore)="C"
      Use in _TMP_BCC
    else
      USE IN SELECT(this.pNomeCursore)
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Spese generali e CPA
    * --- Leggo la percentuale di aumento.
    *     Se la valuta del documento � diversa da quella del metodo non faccio nulla
    this.w_OK = .F.
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_VALUTA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_VALUTA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from METCALSP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.METCALSP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.METCALSP_idx,2],.t.,this.METCALSP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MSIMPFIS"+;
        " from "+i_cTable+" METCALSP where ";
            +"MSCODICE = "+cp_ToStrODBC(this.w_CODICE);
            +" and MSVALUTA = "+cp_ToStrODBC(this.w_VALUTA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MSIMPFIS;
        from (i_cTable) where;
            MSCODICE = this.w_CODICE;
            and MSVALUTA = this.w_VALUTA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_MSIMPFIS = NVL(cp_ToDate(_read_.MSIMPFIS),cp_NullValue(_read_.MSIMPFIS))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_Rows > 0
      if this.pTIPO
        * --- Nel caso di Spese Generali
        * --- Considero tutte le righe di servizio ad eccezione di quelle di tipo spese e anticipazioni (e generiche)
        * --- t_MVTIPRIG deve valere 'F' o 'M'
        *     Inoltre t_PRESTA (valorizzato con ART_ICOL.ARPRESTA) deve essere
        *     in ('I','E','T','R','C','P','F', 'G')
        *     non in ('S','A')
        SELECT SUM( t_MVVALRIG ) AS VALRIG FROM ( this.w_CURSOR ) WHERE INLIST(t_MVTIPRIG, "F", "M") AND INLIST(t_PRESTA,"I","E","T","R","C","P","F","G") AND t_MVFLOMAG = 1 AND NOT (NOT EMPTY(t_MVRIFORD) AND t_SPEGEN=0) and t_FLGDSPE<>"S" INTO CURSOR TmpCurs
        SELECT TmpCurs
        * --- Sottraggo al totale delle righe l'eventuale sconto di piede
        if VALRIG<>0
          this.w_QTAIMP = VALRIG + this.w_PADRE.w_MVSCONTI
        endif
      else
        * --- Nel caso di CPA
        * --- Considero tutte le righe di servizio ad eccezione di quelle di tipo anticipazioni (e generiche)
        * --- t_MVTIPRIG deve valere 'F' o 'M'
        *     Inoltre t_PRESTA (valorizzato con ART_ICOL.ARPRESTA) deve essere
        *     in ('I','E','T','R','C','P','F','S','G')
        *     non in ('A')
        SELECT SUM( t_MVVALRIG ) AS VALRIG FROM ( this.w_CURSOR ) WHERE INLIST(t_MVTIPRIG, "F", "M") AND INLIST(t_PRESTA,"I","E","T","R","C","P","F","S", "G") AND t_MVFLOMAG = 1 and t_FLGDSPE<>"S" INTO CURSOR TmpCurs
        SELECT TmpCurs
        if VALRIG<>0
          * --- Sottraggo al totale delle righe l'eventuale sconto di piede
          this.w_QTAIMP = VALRIG + this.w_PADRE.w_MVSCONTI
          * --- Aggiungo al totale le eventuali spese di incasso
          this.w_QTAIMP = this.w_QTAIMP + this.w_PADRE.w_MVSPEINC
          * --- Aggiungo al totale le eventuali spese generali
          this.w_QTAIMP = this.w_QTAIMP + this.w_PADRE.w_MVSPEIMB
        endif
      endif
      * --- Se QTAIMP � diversa da zero applico la % di aumento, altrimenti non valorizzo
      *     w_IMPFIS e w_IMPUNI per cui w_PREZZO sar� zero
      if this.w_QTAIMP <> 0
        * --- Percentuale
        this.w_PREZZO = this.w_QTAIMP * this.w_MSIMPFIS / 100
      endif
    else
      this.w_PREZZO = 0
    endif
    * --- Chiudo il cursore
    USE IN SELECT("TmpCurs")
    * --- Restituisco il prezzo
    this.w_PREZZO = cp_Round(this.w_PREZZO,this.w_DECTOT)
    if VARTYPE(this.pNomeCursore)="C"
      * --- Se proveniamo da GSAL_BIA  non deve chiedere nulla ma deve avvalorare sul chiamante l'imponibile
      if this.pTIPO
        this.w_PADRE.w_Impon_Gen = this.w_QTAIMP
      else
        this.w_PADRE.w_Impon_CPA = this.w_QTAIMP
      endif
    else
      if this.w_OLDSPE<>this.w_PREZZO AND this.w_PADRE.cFunction<>"Load" AND this.pASK And Not this.pNoAsk
        if this.pTIPO
          this.w_TEST = ah_YesNo("Attenzione, spese generali gi� valorizzate%0Si desidera ricalcolarle?")
        else
          this.w_TEST = ah_YesNo("Attenzione, CPA gi� valorizzato%0Si desidera ricalcolarlo?")
        endif
        if Not this.w_TEST
          this.w_PREZZO = this.w_OLDSPE
        endif
      endif
    endif
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Leggo la base di calcolo,l'importo minimo,l'importo massimo e l'importo fisso.
    *     Se la valuta del documento � diversa da quella del metodo non faccio nulla
    this.w_OK = .F.
    this.w_CURSOR = this.pNomeCursore
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECTOT"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_VALUTA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECTOT;
        from (i_cTable) where;
            VACODVAL = this.w_VALUTA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from METCALSP
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.METCALSP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.METCALSP_idx,2],.t.,this.METCALSP_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MSBASCAL,MSIMPFIS,MSIMPMIN,MSIMPMAX"+;
        " from "+i_cTable+" METCALSP where ";
            +"MSCODICE = "+cp_ToStrODBC(this.w_CODICE);
            +" and MSVALUTA = "+cp_ToStrODBC(this.w_VALUTA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MSBASCAL,MSIMPFIS,MSIMPMIN,MSIMPMAX;
        from (i_cTable) where;
            MSCODICE = this.w_CODICE;
            and MSVALUTA = this.w_VALUTA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_BASCAL = NVL(cp_ToDate(_read_.MSBASCAL),cp_NullValue(_read_.MSBASCAL))
      this.w_MSIMPFIS = NVL(cp_ToDate(_read_.MSIMPFIS),cp_NullValue(_read_.MSIMPFIS))
      this.w_MSIMPMIN = NVL(cp_ToDate(_read_.MSIMPMIN),cp_NullValue(_read_.MSIMPMIN))
      this.w_MSIMPMAX = NVL(cp_ToDate(_read_.MSIMPMAX),cp_NullValue(_read_.MSIMPMAX))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if i_Rows > 0
      * --- Analizzando la base di calcolo decido come operare
      do case
        case INLIST( this.w_BASCAL,"T","S")
          * --- Totale righe documento o totale righe documento esclusi i servizi
          *     Considero solo le righe Normali
          if this.w_BASCAL = "T"
            if UPPER( g_APPLICATION ) <> "AD HOC ENTERPRISE"
              SELECT SUM( t_MVVALRIG ) AS VALRIG FROM ( this.w_CURSOR ) WHERE INLIST(t_MVTIPRIG,"R", "F", "M") AND t_MVFLOMAG = 1 INTO CURSOR TmpCurs
            else
              * --- -considero anche gli articoli kit
              SELECT SUM( t_MVVALRIG ) AS VALRIG, t_CPROWNUM AS CPROWNUM FROM ( this.w_CURSOR ) WHERE INLIST(t_MVTIPRIG,"A") AND t_MVFLOMAG =1 GROUP BY CPROWNUM INTO CURSOR TmpCurs_KIT
              if RECCOUNT("TmpCurs_KIT")>0
                * --- se ci sono gli articoli kit differenzio i cursori
                SELECT SUM( t_MVVALRIG ) AS VALRIG, t_MVRIFKIT AS MVRIFKIT FROM ( this.w_CURSOR ) WHERE INLIST(t_MVTIPRIG,"R", "F", "M") AND t_MVFLOMAG =1 GROUP BY MVRIFKIT INTO CURSOR TmpCurs_ARTI
                SELECT SUM(IIF(TmpCurs_ARTI.VALRIG>0, TmpCurs_ARTI.VALRIG, TmpCurs_KIT.VALRIG)) AS VALRIG FROM TmpCurs_ARTI LEFT JOIN TmpCurs_KIT ON TmpCurs_ARTI.MVRIFKIT=TmpCurs_KIT.CPROWNUM INTO CURSOR TmpCurs
              else
                SELECT SUM( t_MVVALRIG ) AS VALRIG FROM ( this.w_CURSOR ) WHERE INLIST(t_MVTIPRIG,"R", "F", "M") AND t_MVFLOMAG =1 INTO CURSOR TmpCurs
              endif
            endif
          else
            * --- Escludo i servizi
            if UPPER( g_APPLICATION ) <> "AD HOC ENTERPRISE"
              SELECT SUM( t_MVVALRIG ) AS VALRIG FROM ( this.w_CURSOR ) WHERE t_MVTIPRIG = "R" AND t_MVFLOMAG = 1 INTO CURSOR TmpCurs
            else
              * --- -considero anche gli articoli kit
              SELECT SUM( t_MVVALRIG ) AS VALRIG, t_CPROWNUM AS CPROWNUM FROM ( this.w_CURSOR ) WHERE INLIST(t_MVTIPRIG,"A") AND t_MVFLOMAG =1 GROUP BY CPROWNUM INTO CURSOR TmpCurs_KIT
              if RECCOUNT("TmpCurs_KIT")>0
                * --- se ci sono gli articoli kit differenzio i cursori
                SELECT SUM( t_MVVALRIG ) AS VALRIG, t_MVRIFKIT AS MVRIFKIT FROM ( this.w_CURSOR ) WHERE INLIST(t_MVTIPRIG,"R") AND t_MVFLOMAG =1 GROUP BY MVRIFKIT INTO CURSOR TmpCurs_ARTI
                SELECT SUM(IIF(TmpCurs_ARTI.VALRIG>0, TmpCurs_ARTI.VALRIG, TmpCurs_KIT.VALRIG)) AS VALRIG FROM TmpCurs_ARTI LEFT JOIN TmpCurs_KIT ON TmpCurs_ARTI.MVRIFKIT=TmpCurs_KIT.CPROWNUM INTO CURSOR TmpCurs
              else
                SELECT SUM( t_MVVALRIG ) AS VALRIG FROM ( this.w_CURSOR ) WHERE INLIST(t_MVTIPRIG,"R") AND t_MVFLOMAG = 1 INTO CURSOR TmpCurs
              endif
            endif
          endif
          SELECT TmpCurs
          * --- Sottraggo al totale delle righe l'eventuale sconto di piede
          this.w_QTAIMP = VALRIG + this.w_PADRE.w_MVSCONTI
          * --- Se QTAIMP � diversa da zero individuo lo scaglione, altrimenti non valorizzo
          *     w_IMPFIS e w_IMPUNI per cui w_PREZZO sar� zero
          if this.w_QTAIMP <> 0
            * --- Select from QUERY\GSAR1MSI
            do vq_exec with 'QUERY\GSAR1MSI',this,'_Curs_QUERY_GSAR1MSI','',.f.,.t.
            if used('_Curs_QUERY_GSAR1MSI')
              select _Curs_QUERY_GSAR1MSI
              locate for 1=1
              do while not(eof())
              this.w_IMPFIS = SIIMPFIS
              this.w_IMPUNI = SIPERCEN / 100
                select _Curs_QUERY_GSAR1MSI
                continue
              enddo
              use
            endif
            this.w_OK = .T.
          else
            * --- Totale documento non valorizzato, non sono presenti righe nel documento
            if this.pTIPO
              if !this.w_MESTOTIMB
                this.w_MESS = ah_Msgformat("Totale righe documento uguale a zero: Impossibile calcolare le spese di imballo")
                this.w_MESTOTIMB = .T.
              endif
            else
              if !this.w_MESTOTTRA
                this.w_MESS = ah_Msgformat("Totale righe documento uguale a zero: Impossibile calcolare le spese di trasporto")
                this.w_MESTOTTRA = .T.
              endif
            endif
          endif
        case this.w_BASCAL = "N"
          * --- Numero colli
          this.w_QTAIMP = this.w_QTACOL
          * --- Se QTAIMP � diversa da zero individuo lo scaglione, altrimenti non valorizzo
          *     w_IMPFIS e w_IMPUNI per cui w_PREZZO sar� zero
          if this.w_QTAIMP <> 0
            * --- Select from QUERY\GSAR1MSC
            do vq_exec with 'QUERY\GSAR1MSC',this,'_Curs_QUERY_GSAR1MSC','',.f.,.t.
            if used('_Curs_QUERY_GSAR1MSC')
              select _Curs_QUERY_GSAR1MSC
              locate for 1=1
              do while not(eof())
              this.w_IMPFIS = SCIMPFIS
              this.w_IMPUNI = SCIMPUNI
                select _Curs_QUERY_GSAR1MSC
                continue
              enddo
              use
            endif
            this.w_OK = .T.
          else
            * --- Numero colli non valorizzato, non � stato premuto il bottone calcola colli
            if this.pTIPO
              if !this.w_MESCOLIMB
                this.w_MESS = ah_Msgformat("Numero colli non valorizzato%0Impossibile calcolare le spese di imballo")
                this.w_MESCOLIMB = .T.
              else
                this.w_MESS = ""
              endif
            else
              if !this.w_MESCOLTRA
                this.w_MESS = ah_Msgformat("Numero colli non valorizzato%0Impossibile calcolare le spese di trasporto")
              else
                this.w_MESS = ""
              endif
            endif
          endif
        case this.w_BASCAL = "P" OR (TYPE("this.w_PADRE.oparentobject")=="O" AND lower(this.w_PADRE.oparentobject.class)=="tgsar_bgd" AND this.w_BASCAL = "Q" )
          * --- Peso lordo
          if this.w_BASCAL = "Q"
            this.w_QTAIMP = this.w_QTAIMP + NVL(tmp_bst.t_MVNUMCOL, 0)
          else
            this.w_QTAIMP = this.w_QTALOR
          endif
          * --- Se QTAIMP � diversa da zero individuo lo scaglione, altrimenti non valorizzo
          *     w_IMPFIS e w_IMPUNI per cui w_PREZZO sar� zero
          if this.w_QTAIMP <> 0
            if this.w_BASCAL = "Q"
              * --- Select from QUERY\GSAR1MSC
              do vq_exec with 'QUERY\GSAR1MSC',this,'_Curs_QUERY_GSAR1MSC','',.f.,.t.
              if used('_Curs_QUERY_GSAR1MSC')
                select _Curs_QUERY_GSAR1MSC
                locate for 1=1
                do while not(eof())
                this.w_IMPFIS = SCIMPFIS
                this.w_IMPUNI = SCIMPUNI
                  select _Curs_QUERY_GSAR1MSC
                  continue
                enddo
                use
              endif
            else
              * --- Select from QUERY\GSAR1MSP
              do vq_exec with 'QUERY\GSAR1MSP',this,'_Curs_QUERY_GSAR1MSP','',.f.,.t.
              if used('_Curs_QUERY_GSAR1MSP')
                select _Curs_QUERY_GSAR1MSP
                locate for 1=1
                do while not(eof())
                this.w_IMPFIS = SPIMPFIS
                this.w_IMPUNI = SPIMPUNI
                  select _Curs_QUERY_GSAR1MSP
                  continue
                enddo
                use
              endif
            endif
            this.w_OK = .T.
          else
            if this.pTIPO
              if !this.w_MESPESIMB
                if this.w_BASCAL = "Q"
                  this.w_MESS = ah_Msgformat("Riga documento %1: numero confezioni uguale a zero%0Impossibile calcolare le spese di imballo", ALLTRIM(STR(t_CPROWORD)) )
                else
                  this.w_MESS = ah_Msgformat("Peso lordo non valorizzato: Impossibile calcolare le spese di imballo")
                endif
                this.w_MESPESIMB = .T.
              else
                this.w_MESS = ""
              endif
            else
              if !this.w_MESPESTRA
                if this.w_BASCAL = "Q"
                  this.w_MESS = ah_Msgformat("Riga documento %1: numero confezioni uguale a zero%0Impossibile calcolare le spese di imballo", ALLTRIM(STR(t_CPROWORD)) )
                else
                  this.w_MESS = ah_Msgformat("Peso lordo non valorizzato: Impossibile calcolare le spese di trasporto")
                endif
                this.w_MESPESTRA = .T.
              else
                this.w_MESS = ""
              endif
            endif
          endif
      endcase
      * --- Calcolo il prezzo
      *     Se w_OK=.t. significa che w_QTAIMP � <> 0
      if this.w_OK
        do case
          case this.w_IMPUNI <> 0
            * --- Importo unitario o Percentuale
            this.w_PREZZO = this.w_QTAIMP * this.w_IMPUNI
          otherwise
            * --- Importo fisso dello scaglione
            this.w_PREZZO = this.w_IMPFIS
        endcase
        * --- Aggiungo l'importo fisso
        this.w_PREZZO = this.w_PREZZO + this.w_MSIMPFIS
        * --- Verifico se il prezzo da attribuire � inferiore all'importo minimo
        if this.w_PREZZO < this.w_MSIMPMIN
          this.w_PREZZO = this.w_MSIMPMIN
        endif
        * --- Verifico se il prezzo da attribuire � superiore all'importo massimo
        *     se l'importo massimo � zero significa che non ho limite
        if this.w_MSIMPMAX <> 0 AND this.w_PREZZO > this.w_MSIMPMAX
          this.w_PREZZO = this.w_MSIMPMAX
        endif
      else
        if this.w_BASCAL<>"Z" and not empty(this.w_MESS)
          this.w_oMESS=createobject("AH_Message")
          if this.w_MESTOTIMB OR this.w_MESTOTTRA
            if EMPTY(this.oparentobject.w_MSGSPE) OR !this.w_MESTOTIMB
              this.w_oMESS.AddMsgPart("Totale righe documento uguale a zero: ")     
              if this.w_MESTOTIMB 
                this.w_oMESS.AddMsgPart("Impossibile calcolare le spese di imballo ")     
              endif
            endif
            if this.w_MESTOTTRA
              this.w_oMESS.AddMsgPart("Impossibile calcolare le spese di trasporto ")     
            endif
          endif
          if this.w_MESCOLIMB OR this.w_MESCOLTRA
            if EMPTY(this.oparentobject.w_MSGSPE) OR !this.w_MESCOLIMB
              this.w_oMESS.AddMsgPart("Numero colli non valorizzato: ")     
              if this.w_MESCOLIMB 
                this.w_oMESS.AddMsgPart("Impossibile calcolare le spese di imballo ")     
              endif
            endif
            if this.w_MESCOLTRA
              this.w_oMESS.AddMsgPart("Impossibile calcolare le spese di trasporto ")     
            endif
          endif
          if this.w_MESPESIMB OR this.w_MESPESTRA
            if EMPTY(this.oparentobject.w_MSGSPE) OR !this.w_MESPESIMB
              if this.w_BASCAL = "Q"
                this.w_oMESS.AddMsgPart("Numero confezioni uguale a zero: ")     
              else
                this.w_oMESS.AddMsgPart("Peso lordo non valorizzato: ")     
              endif
              if this.w_MESPESIMB 
                this.w_oMESS.AddMsgPart("Impossibile calcolare le spese di imballo ")     
              endif
            endif
            if this.w_MESPESTRA
              this.w_oMESS.AddMsgPart("Impossibile calcolare le spese di trasporto ")     
            endif
          endif
          if TYPE("this.w_PADRE.oparentobject")=="O" AND lower(this.w_PADRE.oparentobject.class)=="tgsar_bgd"
            this.w_PADRE.oParentObject.w_PADRE.AddLogMsg("W", this.w_oMESS.ComposeMessage() )
          else
            this.oparentobject.w_MSGSPE=this.oparentobject.w_MSGSPE + this.w_oMESS.ComposeMessage()
          endif
        endif
      endif
    else
      this.w_PREZZO = 0
    endif
    * --- Chiudo i cursori
    USE IN SELECT("TmpCurs")
    USE IN SELECT("TmpCurs_KIT")
    USE IN SELECT("TmpCurs_ARTI")
    * --- Restituisco il prezzo
    this.w_PREZZO = cp_Round(this.w_PREZZO,this.w_DECTOT)
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pNoAsk,pNomeCursore)
    this.pNoAsk=pNoAsk
    this.pNomeCursore=pNomeCursore
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='METCALSP'
    this.cWorkTables[2]='MCALSCAP'
    this.cWorkTables[3]='VALUTE'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_QUERY_GSAR1MSI')
      use in _Curs_QUERY_GSAR1MSI
    endif
    if used('_Curs_QUERY_GSAR1MSC')
      use in _Curs_QUERY_GSAR1MSC
    endif
    if used('_Curs_QUERY_GSAR1MSC')
      use in _Curs_QUERY_GSAR1MSC
    endif
    if used('_Curs_QUERY_GSAR1MSP')
      use in _Curs_QUERY_GSAR1MSP
    endif
    if used('_Curs_QUERY_GSAR1MSI')
      use in _Curs_QUERY_GSAR1MSI
    endif
    if used('_Curs_QUERY_GSAR1MSC')
      use in _Curs_QUERY_GSAR1MSC
    endif
    if used('_Curs_QUERY_GSAR1MSC')
      use in _Curs_QUERY_GSAR1MSC
    endif
    if used('_Curs_QUERY_GSAR1MSP')
      use in _Curs_QUERY_GSAR1MSP
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pNoAsk,pNomeCursore"
endproc
