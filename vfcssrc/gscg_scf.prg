* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_scf                                                        *
*              Dettaglio clienti fornitori                                     *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-06-16                                                      *
* Last revis.: 2013-07-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_scf",oParentObject))

* --- Class definition
define class tgscg_scf as StdForm
  Top    = 15
  Left   = 4

  * --- Standard Properties
  Width  = 693
  Height = 249
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-07-09"
  HelpContextID=90842473
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=55

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  VALUTE_IDX = 0
  BUSIUNIT_IDX = 0
  AZIENDA_IDX = 0
  SB_MAST_IDX = 0
  cPrg = "gscg_scf"
  cComment = "Dettaglio clienti fornitori"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODAZ1 = space(5)
  w_MASABI = space(1)
  w_AZESSTCO = space(4)
  w_FINSTO = ctod('  /  /  ')
  w_UTENTE = 0
  w_GESTBU = space(1)
  w_ESE = space(4)
  o_ESE = space(4)
  w_ESSALDI = space(1)
  o_ESSALDI = space(1)
  w_VALAPP = space(3)
  o_VALAPP = space(3)
  w_TOTDAT = space(1)
  o_TOTDAT = space(1)
  w_DATA1 = ctod('  /  /  ')
  o_DATA1 = ctod('  /  /  ')
  w_DATA2 = ctod('  /  /  ')
  w_DATINIESE = ctod('  /  /  ')
  w_DATFINESE = ctod('  /  /  ')
  w_MONETE = space(1)
  o_MONETE = space(1)
  w_ESPREC = space(1)
  o_ESPREC = space(1)
  w_DIVISA = space(3)
  o_DIVISA = space(3)
  w_DATEMU = ctod('  /  /  ')
  w_CAMBGIOR = 0
  w_SIMVAL = space(5)
  w_CODBUN = space(3)
  w_SUPERBU = space(15)
  w_SBDESCRI = space(35)
  w_PROVVI = space(1)
  o_PROVVI = space(1)
  w_TIPSTA = space(1)
  o_TIPSTA = space(1)
  w_TIPORD = space(1)
  w_TUTTI = space(1)
  w_TUTTI_Z = space(1)
  o_TUTTI_Z = space(1)
  w_PERIODO = space(1)
  o_PERIODO = space(1)
  w_EXCEL = space(1)
  o_EXCEL = space(1)
  w_DECIMI = 0
  w_DESCRI = space(30)
  w_BUDESCRI = space(35)
  w_EXTRAEUR = 0
  o_EXTRAEUR = 0
  w_CAMVAL = 0
  o_CAMVAL = 0
  w_esepre = space(4)
  w_PRVALNAZ = space(3)
  w_PRCAOVAL = 0
  w_attivita = space(1)
  w_passivi = space(1)
  w_Costi = space(1)
  w_Ricavi = space(1)
  w_Ordine = space(1)
  w_transi = space(1)
  w_MODSTA = space(1)
  o_MODSTA = space(1)
  w_LIVELLI = space(1)
  o_LIVELLI = space(1)
  w_SEZVAR = space(1)
  o_SEZVAR = space(1)
  w_DECNAZ = 0
  w_CAONAZ = 0
  w_CALCSTR = space(1)
  w_CONPRO = space(1)
  w_SEZVAR1 = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_INFRAAN = space(1)
  w_EXCEL = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_scfPag1","gscg_scf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oESE_1_7
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='ESERCIZI'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='BUSIUNIT'
    this.cWorkTables[4]='AZIENDA'
    this.cWorkTables[5]='SB_MAST'
    return(this.OpenAllTables(5))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCG_BPC with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODAZ1=space(5)
      .w_MASABI=space(1)
      .w_AZESSTCO=space(4)
      .w_FINSTO=ctod("  /  /  ")
      .w_UTENTE=0
      .w_GESTBU=space(1)
      .w_ESE=space(4)
      .w_ESSALDI=space(1)
      .w_VALAPP=space(3)
      .w_TOTDAT=space(1)
      .w_DATA1=ctod("  /  /  ")
      .w_DATA2=ctod("  /  /  ")
      .w_DATINIESE=ctod("  /  /  ")
      .w_DATFINESE=ctod("  /  /  ")
      .w_MONETE=space(1)
      .w_ESPREC=space(1)
      .w_DIVISA=space(3)
      .w_DATEMU=ctod("  /  /  ")
      .w_CAMBGIOR=0
      .w_SIMVAL=space(5)
      .w_CODBUN=space(3)
      .w_SUPERBU=space(15)
      .w_SBDESCRI=space(35)
      .w_PROVVI=space(1)
      .w_TIPSTA=space(1)
      .w_TIPORD=space(1)
      .w_TUTTI=space(1)
      .w_TUTTI_Z=space(1)
      .w_PERIODO=space(1)
      .w_EXCEL=space(1)
      .w_DECIMI=0
      .w_DESCRI=space(30)
      .w_BUDESCRI=space(35)
      .w_EXTRAEUR=0
      .w_CAMVAL=0
      .w_esepre=space(4)
      .w_PRVALNAZ=space(3)
      .w_PRCAOVAL=0
      .w_attivita=space(1)
      .w_passivi=space(1)
      .w_Costi=space(1)
      .w_Ricavi=space(1)
      .w_Ordine=space(1)
      .w_transi=space(1)
      .w_MODSTA=space(1)
      .w_LIVELLI=space(1)
      .w_SEZVAR=space(1)
      .w_DECNAZ=0
      .w_CAONAZ=0
      .w_CALCSTR=space(1)
      .w_CONPRO=space(1)
      .w_SEZVAR1=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_INFRAAN=space(1)
      .w_EXCEL=space(1)
        .w_CODAZ1 = i_codazi
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODAZ1))
          .link_1_1('Full')
        endif
        .w_MASABI = IIF(g_APPLICATION="ADHOC REVOLUTION" OR g_APPLICATION="ad hoc ENTERPRISE",' ',LOOKTAB("AZIENDA","AZMASABI","AZCODAZI",.w_CODAZ1))
        .w_AZESSTCO = IIF(g_APPLICATION="ADHOC REVOLUTION",'    ',NVL(LOOKTAB("AZIENDA","AZESSTCO","AZCODAZI",.w_CODAZ1),'    '))
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_AZESSTCO))
          .link_1_3('Full')
        endif
          .DoRTCalc(4,4,.f.)
        .w_UTENTE = g_Codute
          .DoRTCalc(6,6,.f.)
        .w_ESE = g_CODESE
        .DoRTCalc(7,7,.f.)
        if not(empty(.w_ESE))
          .link_1_7('Full')
        endif
        .w_ESSALDI = iif(g_LRON='N',IIF(.w_MASABI='S',' ','S'),' ')
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_VALAPP))
          .link_1_9('Full')
        endif
        .w_TOTDAT = IIF(.w_ESSALDI='S','T',IIF(EMPTY(NVL(.w_totdat,' ')),'T',.w_totdat))
        .w_DATA1 = iif(.w_totdat='T',cp_CharToDate('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATINIESE))
        .w_DATA2 = iif(.w_totdat='T',cp_CharToDate('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATFINESE))
          .DoRTCalc(13,14,.f.)
        .w_MONETE = 'c'
        .w_ESPREC = IIF(.w_TOTDAT='T','S',IIF(.w_DATA1=.w_DATINIESE,'S',' '))
        .w_DIVISA = IIF(.w_monete='c',.w_VALAPP,.w_DIVISA)
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_DIVISA))
          .link_1_17('Full')
        endif
          .DoRTCalc(18,18,.f.)
        .w_CAMBGIOR = GETCAM(.w_DIVISA, i_datsys, 7)
        .w_SIMVAL = .w_SIMVAL
        .w_CODBUN = IIF(.w_ESSALDI='S','   ',IIF(EMPTY(NVL(.w_CODBUN,'   ')),'   ',.w_CODBUN))
        .DoRTCalc(21,21,.f.)
        if not(empty(.w_CODBUN))
          .link_1_21('Full')
        endif
        .w_SUPERBU = IIF(.w_ESSALDI='S','',IIF(NOT EMPTY(.w_CODBUN),'',IIF(EMPTY(NVL(.w_SUPERBU,'')),'',.w_SUPERBU)))
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_SUPERBU))
          .link_1_22('Full')
        endif
          .DoRTCalc(23,23,.f.)
        .w_PROVVI = IIF(.w_ESSALDI='S','N',IIF(.w_TOTDAT='T','N',.w_PROVVI))
        .w_TIPSTA = 'F'
        .w_TIPORD = 'C'
        .w_TUTTI = IIF(.w_TUTTI_Z='S','S',.w_TUTTI)
        .w_TUTTI_Z = IIF(.w_PERIODO='S',' ',.w_TUTTI_Z)
          .DoRTCalc(29,34,.f.)
        .w_CAMVAL = iif(.w_EXTRAEUR=0,GETCAM(.w_DIVISA, .w_DATFINESE, -1), .w_EXTRAEUR)
        .w_esepre = CALCESPR(i_CODAZI,.w_DATINIESE,.T.)
        .DoRTCalc(36,36,.f.)
        if not(empty(.w_esepre))
          .link_1_49('Full')
        endif
        .DoRTCalc(37,37,.f.)
        if not(empty(.w_PRVALNAZ))
          .link_1_50('Full')
        endif
          .DoRTCalc(38,38,.f.)
        .w_attivita = 'A'
        .w_passivi = 'P'
        .w_Costi = ' '
        .w_Ricavi = ' '
          .DoRTCalc(43,44,.f.)
        .w_MODSTA = 'U'
        .w_LIVELLI = ' '
          .DoRTCalc(47,49,.f.)
        .w_CALCSTR = 'S'
        .w_CONPRO = ' '
        .w_SEZVAR1 = 'N'
        .w_OBTEST = i_DATSYS
        .w_INFRAAN = 'N'
    endwith
    this.DoRTCalc(55,55,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_35.enabled = this.oPgFrm.Page1.oPag.oBtn_1_35.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
          .link_1_1('Full')
        .DoRTCalc(2,2,.t.)
          .link_1_3('Full')
        .DoRTCalc(4,8,.t.)
          .link_1_9('Full')
        if .o_ESSALDI<>.w_ESSALDI
            .w_TOTDAT = IIF(.w_ESSALDI='S','T',IIF(EMPTY(NVL(.w_totdat,' ')),'T',.w_totdat))
        endif
        if .o_ESE<>.w_ESE.or. .o_TOTDAT<>.w_TOTDAT
            .w_DATA1 = iif(.w_totdat='T',cp_CharToDate('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATINIESE))
        endif
        if .o_ESE<>.w_ESE.or. .o_TOTDAT<>.w_TOTDAT
            .w_DATA2 = iif(.w_totdat='T',cp_CharToDate('  -  -    '),iif(empty(.w_ESE),i_datsys,.w_DATFINESE))
        endif
        .DoRTCalc(13,15,.t.)
        if .o_data1<>.w_data1.or. .o_PROVVI<>.w_PROVVI.or. .o_totdat<>.w_totdat
            .w_ESPREC = IIF(.w_TOTDAT='T','S',IIF(.w_DATA1=.w_DATINIESE,'S',' '))
        endif
        if .o_monete<>.w_monete.or. .o_VALAPP<>.w_VALAPP
            .w_DIVISA = IIF(.w_monete='c',.w_VALAPP,.w_DIVISA)
          .link_1_17('Full')
        endif
        .DoRTCalc(18,18,.t.)
        if .o_DIVISA<>.w_DIVISA
            .w_CAMBGIOR = GETCAM(.w_DIVISA, i_datsys, 7)
        endif
            .w_SIMVAL = .w_SIMVAL
        if .o_ESSALDI<>.w_ESSALDI
            .w_CODBUN = IIF(.w_ESSALDI='S','   ',IIF(EMPTY(NVL(.w_CODBUN,'   ')),'   ',.w_CODBUN))
          .link_1_21('Full')
        endif
        if .o_totdat<>.w_totdat.or. .o_ESPREC<>.w_ESPREC.or. .o_data1<>.w_data1.or. .o_ESSALDI<>.w_ESSALDI
            .w_SUPERBU = IIF(.w_ESSALDI='S','',IIF(NOT EMPTY(.w_CODBUN),'',IIF(EMPTY(NVL(.w_SUPERBU,'')),'',.w_SUPERBU)))
          .link_1_22('Full')
        endif
        .DoRTCalc(23,23,.t.)
        if .o_ESSALDI<>.w_ESSALDI
            .w_PROVVI = IIF(.w_ESSALDI='S','N',IIF(.w_TOTDAT='T','N',.w_PROVVI))
        endif
        .DoRTCalc(25,26,.t.)
        if .o_TUTTI_Z<>.w_TUTTI_Z
            .w_TUTTI = IIF(.w_TUTTI_Z='S','S',.w_TUTTI)
        endif
        if .o_PERIODO<>.w_PERIODO
            .w_TUTTI_Z = IIF(.w_PERIODO='S',' ',.w_TUTTI_Z)
        endif
        .DoRTCalc(29,34,.t.)
        if .o_EXTRAEUR<>.w_EXTRAEUR.or. .o_DIVISA<>.w_DIVISA
            .w_CAMVAL = iif(.w_EXTRAEUR=0,GETCAM(.w_DIVISA, .w_DATFINESE, -1), .w_EXTRAEUR)
        endif
        if .o_ese<>.w_ese
            .w_esepre = CALCESPR(i_CODAZI,.w_DATINIESE,.T.)
          .link_1_49('Full')
        endif
          .link_1_50('Full')
        if .o_CAMVAL<>.w_CAMVAL
          .Calculate_ZJCGXHOAJG()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(38,55,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_ZJCGXHOAJG()
    with this
          * --- Inserimento cambio giornaliero
     if not empty(.w_DIVISA) and .w_DIVISA<>g_perval and .w_CAMBGIOR<>.w_CAMVAL
          GSAR_BIC(this;
              ,.w_DIVISA;
              ,.w_CAMVAL;
              ,i_datsys;
             )
     endif
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTOTDAT_1_10.enabled = this.oPgFrm.Page1.oPag.oTOTDAT_1_10.mCond()
    this.oPgFrm.Page1.oPag.oDATA1_1_11.enabled = this.oPgFrm.Page1.oPag.oDATA1_1_11.mCond()
    this.oPgFrm.Page1.oPag.oDATA2_1_12.enabled = this.oPgFrm.Page1.oPag.oDATA2_1_12.mCond()
    this.oPgFrm.Page1.oPag.oESPREC_1_16.enabled = this.oPgFrm.Page1.oPag.oESPREC_1_16.mCond()
    this.oPgFrm.Page1.oPag.oDIVISA_1_17.enabled = this.oPgFrm.Page1.oPag.oDIVISA_1_17.mCond()
    this.oPgFrm.Page1.oPag.oCODBUN_1_21.enabled = this.oPgFrm.Page1.oPag.oCODBUN_1_21.mCond()
    this.oPgFrm.Page1.oPag.oSUPERBU_1_22.enabled = this.oPgFrm.Page1.oPag.oSUPERBU_1_22.mCond()
    this.oPgFrm.Page1.oPag.oPROVVI_1_24.enabled = this.oPgFrm.Page1.oPag.oPROVVI_1_24.mCond()
    this.oPgFrm.Page1.oPag.oTUTTI_1_27.enabled = this.oPgFrm.Page1.oPag.oTUTTI_1_27.mCond()
    this.oPgFrm.Page1.oPag.oCAMVAL_1_48.enabled = this.oPgFrm.Page1.oPag.oCAMVAL_1_48.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_33.enabled = this.oPgFrm.Page1.oPag.oBtn_1_33.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oESSALDI_1_8.visible=!this.oPgFrm.Page1.oPag.oESSALDI_1_8.mHide()
    this.oPgFrm.Page1.oPag.oESPREC_1_16.visible=!this.oPgFrm.Page1.oPag.oESPREC_1_16.mHide()
    this.oPgFrm.Page1.oPag.oCODBUN_1_21.visible=!this.oPgFrm.Page1.oPag.oCODBUN_1_21.mHide()
    this.oPgFrm.Page1.oPag.oSUPERBU_1_22.visible=!this.oPgFrm.Page1.oPag.oSUPERBU_1_22.mHide()
    this.oPgFrm.Page1.oPag.oSBDESCRI_1_23.visible=!this.oPgFrm.Page1.oPag.oSBDESCRI_1_23.mHide()
    this.oPgFrm.Page1.oPag.oEXCEL_1_30.visible=!this.oPgFrm.Page1.oPag.oEXCEL_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_40.visible=!this.oPgFrm.Page1.oPag.oStr_1_40.mHide()
    this.oPgFrm.Page1.oPag.oBUDESCRI_1_43.visible=!this.oPgFrm.Page1.oPag.oBUDESCRI_1_43.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_67.visible=!this.oPgFrm.Page1.oPag.oStr_1_67.mHide()
    this.oPgFrm.Page1.oPag.oEXCEL_1_70.visible=!this.oPgFrm.Page1.oPag.oEXCEL_1_70.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODAZ1
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZ1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZ1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLBUNI";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZ1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZ1)
            select AZCODAZI,AZFLBUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZ1 = NVL(_Link_.AZCODAZI,space(5))
      this.w_gestbu = NVL(_Link_.AZFLBUNI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZ1 = space(5)
      endif
      this.w_gestbu = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZ1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZESSTCO
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZESSTCO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZESSTCO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_AZESSTCO);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZ1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZ1;
                       ,'ESCODESE',this.w_AZESSTCO)
            select ESCODAZI,ESCODESE,ESINIESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZESSTCO = NVL(_Link_.ESCODESE,space(4))
      this.w_FINSTO = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AZESSTCO = space(4)
      endif
      this.w_FINSTO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZESSTCO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ESE
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESE)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_codaz1;
                     ,'ESCODESE',trim(this.w_ESE))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESE)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESE) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESE_1_7'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_codaz1<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Esercizio storicizzato")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESE);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_codaz1;
                       ,'ESCODESE',this.w_ESE)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESE = NVL(_Link_.ESCODESE,space(4))
      this.w_DATINIESE = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_DATFINESE = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
      this.w_VALAPP = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_ESE = space(4)
      endif
      this.w_DATINIESE = ctod("  /  /  ")
      this.w_DATFINESE = ctod("  /  /  ")
      this.w_VALAPP = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DATINIESE>.w_FINSTO
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Esercizio storicizzato")
        endif
        this.w_ESE = space(4)
        this.w_DATINIESE = ctod("  /  /  ")
        this.w_DATFINESE = ctod("  /  /  ")
        this.w_VALAPP = space(3)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALAPP
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALAPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALAPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALAPP)
            select VACODVAL,VACAOVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALAPP = NVL(_Link_.VACODVAL,space(3))
      this.w_CAONAZ = NVL(_Link_.VACAOVAL,0)
      this.w_DECNAZ = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_VALAPP = space(3)
      endif
      this.w_CAONAZ = 0
      this.w_DECNAZ = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALAPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DIVISA
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DIVISA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_DIVISA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADATEUR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_DIVISA))
          select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADATEUR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DIVISA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_DIVISA)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADATEUR";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_DIVISA)+"%");

            select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADATEUR;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_DIVISA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oDIVISA_1_17'),i_cWhere,'GSAR_AVL',"Divise",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADATEUR";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADATEUR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DIVISA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADATEUR";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_DIVISA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_DIVISA)
            select VACODVAL,VADESVAL,VADECTOT,VASIMVAL,VACAOVAL,VADATEUR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DIVISA = NVL(_Link_.VACODVAL,space(3))
      this.w_descri = NVL(_Link_.VADESVAL,space(30))
      this.w_decimi = NVL(_Link_.VADECTOT,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_EXTRAEUR = NVL(_Link_.VACAOVAL,0)
      this.w_DATEMU = NVL(cp_ToDate(_Link_.VADATEUR),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DIVISA = space(3)
      endif
      this.w_descri = space(30)
      this.w_decimi = 0
      this.w_SIMVAL = space(5)
      this.w_EXTRAEUR = 0
      this.w_DATEMU = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DIVISA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODBUN
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BUSIUNIT_IDX,3]
    i_lTable = "BUSIUNIT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2], .t., this.BUSIUNIT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODBUN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_KBU',True,'BUSIUNIT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BUCODICE like "+cp_ToStrODBC(trim(this.w_CODBUN)+"%");
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_codaz1);

          i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BUCODAZI,BUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BUCODAZI',this.w_codaz1;
                     ,'BUCODICE',trim(this.w_CODBUN))
          select BUCODAZI,BUCODICE,BUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BUCODAZI,BUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODBUN)==trim(_Link_.BUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODBUN) and !this.bDontReportError
            deferred_cp_zoom('BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(oSource.parent,'oCODBUN_1_21'),i_cWhere,'GSAR_KBU',"Business Unit",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_codaz1<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and BUCODAZI="+cp_ToStrODBC(this.w_codaz1);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',oSource.xKey(1);
                       ,'BUCODICE',oSource.xKey(2))
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODBUN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BUCODAZI,BUCODICE,BUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BUCODICE="+cp_ToStrODBC(this.w_CODBUN);
                   +" and BUCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BUCODAZI',this.w_codaz1;
                       ,'BUCODICE',this.w_CODBUN)
            select BUCODAZI,BUCODICE,BUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODBUN = NVL(_Link_.BUCODICE,space(3))
      this.w_BUDESCRI = NVL(_Link_.BUDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODBUN = space(3)
      endif
      this.w_BUDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BUSIUNIT_IDX,2])+'\'+cp_ToStr(_Link_.BUCODAZI,1)+'\'+cp_ToStr(_Link_.BUCODICE,1)
      cp_ShowWarn(i_cKey,this.BUSIUNIT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODBUN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SUPERBU
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SB_MAST_IDX,3]
    i_lTable = "SB_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2], .t., this.SB_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SUPERBU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MSB',True,'SB_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SBCODICE like "+cp_ToStrODBC(trim(this.w_SUPERBU)+"%");

          i_ret=cp_SQL(i_nConn,"select SBCODICE,SBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SBCODICE',trim(this.w_SUPERBU))
          select SBCODICE,SBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SUPERBU)==trim(_Link_.SBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SUPERBU) and !this.bDontReportError
            deferred_cp_zoom('SB_MAST','*','SBCODICE',cp_AbsName(oSource.parent,'oSUPERBU_1_22'),i_cWhere,'GSAR_MSB',"Gruppi Business Unit",'Gruppobunit.SB_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SBCODICE,SBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SBCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SBCODICE',oSource.xKey(1))
            select SBCODICE,SBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SUPERBU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SBCODICE,SBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SBCODICE="+cp_ToStrODBC(this.w_SUPERBU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SBCODICE',this.w_SUPERBU)
            select SBCODICE,SBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SUPERBU = NVL(_Link_.SBCODICE,space(15))
      this.w_SBDESCRI = NVL(_Link_.SBDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_SUPERBU = space(15)
      endif
      this.w_SBDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SB_MAST_IDX,2])+'\'+cp_ToStr(_Link_.SBCODICE,1)
      cp_ShowWarn(i_cKey,this.SB_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SUPERBU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=esepre
  func Link_1_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_esepre) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_esepre)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESVALNAZ";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_esepre);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_codaz1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_codaz1;
                       ,'ESCODESE',this.w_esepre)
            select ESCODAZI,ESCODESE,ESVALNAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_esepre = NVL(_Link_.ESCODESE,space(4))
      this.w_PRVALNAZ = NVL(_Link_.ESVALNAZ,space(3))
    else
      if i_cCtrl<>'Load'
        this.w_esepre = space(4)
      endif
      this.w_PRVALNAZ = space(3)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_esepre Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PRVALNAZ
  func Link_1_50(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PRVALNAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PRVALNAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VACAOVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PRVALNAZ);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PRVALNAZ)
            select VACODVAL,VACAOVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PRVALNAZ = NVL(_Link_.VACODVAL,space(3))
      this.w_PRCAOVAL = NVL(_Link_.VACAOVAL,0)
    else
      if i_cCtrl<>'Load'
        this.w_PRVALNAZ = space(3)
      endif
      this.w_PRCAOVAL = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PRVALNAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oESE_1_7.value==this.w_ESE)
      this.oPgFrm.Page1.oPag.oESE_1_7.value=this.w_ESE
    endif
    if not(this.oPgFrm.Page1.oPag.oESSALDI_1_8.RadioValue()==this.w_ESSALDI)
      this.oPgFrm.Page1.oPag.oESSALDI_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTDAT_1_10.RadioValue()==this.w_TOTDAT)
      this.oPgFrm.Page1.oPag.oTOTDAT_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_11.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_11.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_12.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_12.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.oMONETE_1_15.RadioValue()==this.w_MONETE)
      this.oPgFrm.Page1.oPag.oMONETE_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESPREC_1_16.RadioValue()==this.w_ESPREC)
      this.oPgFrm.Page1.oPag.oESPREC_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDIVISA_1_17.value==this.w_DIVISA)
      this.oPgFrm.Page1.oPag.oDIVISA_1_17.value=this.w_DIVISA
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_20.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_20.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCODBUN_1_21.value==this.w_CODBUN)
      this.oPgFrm.Page1.oPag.oCODBUN_1_21.value=this.w_CODBUN
    endif
    if not(this.oPgFrm.Page1.oPag.oSUPERBU_1_22.value==this.w_SUPERBU)
      this.oPgFrm.Page1.oPag.oSUPERBU_1_22.value=this.w_SUPERBU
    endif
    if not(this.oPgFrm.Page1.oPag.oSBDESCRI_1_23.value==this.w_SBDESCRI)
      this.oPgFrm.Page1.oPag.oSBDESCRI_1_23.value=this.w_SBDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oPROVVI_1_24.RadioValue()==this.w_PROVVI)
      this.oPgFrm.Page1.oPag.oPROVVI_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPSTA_1_25.RadioValue()==this.w_TIPSTA)
      this.oPgFrm.Page1.oPag.oTIPSTA_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPORD_1_26.RadioValue()==this.w_TIPORD)
      this.oPgFrm.Page1.oPag.oTIPORD_1_26.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTUTTI_1_27.RadioValue()==this.w_TUTTI)
      this.oPgFrm.Page1.oPag.oTUTTI_1_27.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTUTTI_Z_1_28.RadioValue()==this.w_TUTTI_Z)
      this.oPgFrm.Page1.oPag.oTUTTI_Z_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPERIODO_1_29.RadioValue()==this.w_PERIODO)
      this.oPgFrm.Page1.oPag.oPERIODO_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oEXCEL_1_30.RadioValue()==this.w_EXCEL)
      this.oPgFrm.Page1.oPag.oEXCEL_1_30.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBUDESCRI_1_43.value==this.w_BUDESCRI)
      this.oPgFrm.Page1.oPag.oBUDESCRI_1_43.value=this.w_BUDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMVAL_1_48.value==this.w_CAMVAL)
      this.oPgFrm.Page1.oPag.oCAMVAL_1_48.value=this.w_CAMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oEXCEL_1_70.RadioValue()==this.w_EXCEL)
      this.oPgFrm.Page1.oPag.oEXCEL_1_70.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_ESE)) or not(.w_DATINIESE>.w_FINSTO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oESE_1_7.SetFocus()
            i_bnoObbl = !empty(.w_ESE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Esercizio storicizzato")
          case   not(empty(.w_data2) or .w_data2>=.w_data1)  and (.w_totdat='D')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA1_1_11.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo date non valido")
          case   not(empty(.w_data1) or .w_data2>=.w_data1)  and (.w_totdat='D')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA2_1_12.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Intervallo di date non valido")
          case   (empty(.w_DIVISA))  and (.w_monete='a')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDIVISA_1_17.SetFocus()
            i_bnoObbl = !empty(.w_DIVISA)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAMVAL))  and (i_datsys<.w_DATEMU or .w_EXTRAEUR=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMVAL_1_48.SetFocus()
            i_bnoObbl = !empty(.w_CAMVAL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_esepre))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oesepre_1_49.SetFocus()
            i_bnoObbl = !empty(.w_esepre)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ESE = this.w_ESE
    this.o_ESSALDI = this.w_ESSALDI
    this.o_VALAPP = this.w_VALAPP
    this.o_TOTDAT = this.w_TOTDAT
    this.o_DATA1 = this.w_DATA1
    this.o_MONETE = this.w_MONETE
    this.o_ESPREC = this.w_ESPREC
    this.o_DIVISA = this.w_DIVISA
    this.o_PROVVI = this.w_PROVVI
    this.o_TIPSTA = this.w_TIPSTA
    this.o_TUTTI_Z = this.w_TUTTI_Z
    this.o_PERIODO = this.w_PERIODO
    this.o_EXCEL = this.w_EXCEL
    this.o_EXTRAEUR = this.w_EXTRAEUR
    this.o_CAMVAL = this.w_CAMVAL
    this.o_MODSTA = this.w_MODSTA
    this.o_LIVELLI = this.w_LIVELLI
    this.o_SEZVAR = this.w_SEZVAR
    return

enddefine

* --- Define pages as container
define class tgscg_scfPag1 as StdContainer
  Width  = 689
  height = 249
  stdWidth  = 689
  stdheight = 249
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oESE_1_7 as StdField with uid="UZALAOQFQO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ESE", cQueryName = "ESE",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Esercizio storicizzato",;
    ToolTipText = "Esercizio di competenza dei movimenti da stampare",;
    HelpContextID = 90537402,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=131, Top=7, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_codaz1", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESE"

  func oESE_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oESE_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESE_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_codaz1)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_codaz1)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESE_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc

  add object oESSALDI_1_8 as StdCheck with uid="LHILIIGAEZ",rtseq=8,rtrep=.f.,left=303, top=7, caption="Lettura saldi eser. attuale",;
    ToolTipText = "I dati estratti verranno presi dall'archivio saldi",;
    HelpContextID = 207855034,;
    cFormVar="w_ESSALDI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESSALDI_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    ''))
  endfunc
  func oESSALDI_1_8.GetRadio()
    this.Parent.oContained.w_ESSALDI = this.RadioValue()
    return .t.
  endfunc

  func oESSALDI_1_8.SetRadio()
    this.Parent.oContained.w_ESSALDI=trim(this.Parent.oContained.w_ESSALDI)
    this.value = ;
      iif(this.Parent.oContained.w_ESSALDI=='S',1,;
      0)
  endfunc

  func oESSALDI_1_8.mHide()
    with this.Parent.oContained
      return (.w_MASABI='S')
    endwith
  endfunc


  add object oTOTDAT_1_10 as StdCombo with uid="MTEKJTIFMR",rtseq=10,rtrep=.f.,left=131,top=33,width=127,height=21;
    , ToolTipText = "Tipo bilancio selezionato";
    , HelpContextID = 49246006;
    , cFormVar="w_TOTDAT",RowSource=""+"Totale esercizio,"+"Da data a data", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTOTDAT_1_10.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oTOTDAT_1_10.GetRadio()
    this.Parent.oContained.w_TOTDAT = this.RadioValue()
    return .t.
  endfunc

  func oTOTDAT_1_10.SetRadio()
    this.Parent.oContained.w_TOTDAT=trim(this.Parent.oContained.w_TOTDAT)
    this.value = ;
      iif(this.Parent.oContained.w_TOTDAT=='T',1,;
      iif(this.Parent.oContained.w_TOTDAT=='D',2,;
      0))
  endfunc

  func oTOTDAT_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ESSALDI<>'S')
    endwith
   endif
  endfunc

  add object oDATA1_1_11 as StdField with uid="UJEHLUCHGB",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo date non valido",;
    ToolTipText = "Data di registrazione di inizio stampa",;
    HelpContextID = 34840522,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=317, Top=33

  func oDATA1_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_totdat='D')
    endwith
   endif
  endfunc

  func oDATA1_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data2) or .w_data2>=.w_data1)
    endwith
    return bRes
  endfunc

  add object oDATA2_1_12 as StdField with uid="ILCEOMKHKO",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Intervallo di date non valido",;
    ToolTipText = "Data di registrazione di fine stampa",;
    HelpContextID = 33791946,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=420, Top=33

  func oDATA2_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_totdat='D')
    endwith
   endif
  endfunc

  func oDATA2_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_data1) or .w_data2>=.w_data1)
    endwith
    return bRes
  endfunc


  add object oMONETE_1_15 as StdCombo with uid="RHPABNLZIJ",rtseq=15,rtrep=.f.,left=131,top=59,width=127,height=21;
    , ToolTipText = "Stampa in valuta di conto o in altra valuta";
    , HelpContextID = 85987014;
    , cFormVar="w_MONETE",RowSource=""+"Valuta di conto,"+"Altra valuta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oMONETE_1_15.RadioValue()
    return(iif(this.value =1,'c',;
    iif(this.value =2,'a',;
    space(1))))
  endfunc
  func oMONETE_1_15.GetRadio()
    this.Parent.oContained.w_MONETE = this.RadioValue()
    return .t.
  endfunc

  func oMONETE_1_15.SetRadio()
    this.Parent.oContained.w_MONETE=trim(this.Parent.oContained.w_MONETE)
    this.value = ;
      iif(this.Parent.oContained.w_MONETE=='c',1,;
      iif(this.Parent.oContained.w_MONETE=='a',2,;
      0))
  endfunc

  add object oESPREC_1_16 as StdCheck with uid="ZAYQYBVIOT",rtseq=16,rtrep=.f.,left=501, top=38, caption="Saldi es. precedente",;
    ToolTipText = "Considera i saldi dell'esercizio precedente",;
    HelpContextID = 37564998,;
    cFormVar="w_ESPREC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESPREC_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    ''))
  endfunc
  func oESPREC_1_16.GetRadio()
    this.Parent.oContained.w_ESPREC = this.RadioValue()
    return .t.
  endfunc

  func oESPREC_1_16.SetRadio()
    this.Parent.oContained.w_ESPREC=trim(this.Parent.oContained.w_ESPREC)
    this.value = ;
      iif(this.Parent.oContained.w_ESPREC=='S',1,;
      0)
  endfunc

  func oESPREC_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TOTDAT='D' AND .w_DATA1=.w_DATINIESE)
    endwith
   endif
  endfunc

  func oESPREC_1_16.mHide()
    with this.Parent.oContained
      return (.w_PROVVI='S')
    endwith
  endfunc

  add object oDIVISA_1_17 as StdField with uid="QTREFSMJOJ",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DIVISA", cQueryName = "DIVISA",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta selezionata",;
    HelpContextID = 18122806,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=345, Top=59, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_DIVISA"

  func oDIVISA_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_monete='a')
    endwith
   endif
  endfunc

  func oDIVISA_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oDIVISA_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDIVISA_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oDIVISA_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Divise",'',this.parent.oContained
  endproc
  proc oDIVISA_1_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_DIVISA
     i_obj.ecpSave()
  endproc

  add object oSIMVAL_1_20 as StdField with uid="WPICBVQXAL",rtseq=20,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Simbolo della valuta",;
    HelpContextID = 184613158,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=400, Top=59, InputMask=replicate('X',5)

  add object oCODBUN_1_21 as StdField with uid="RBUQNXVLZD",rtseq=21,rtrep=.f.,;
    cFormVar = "w_CODBUN", cQueryName = "CODBUN",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Business Unit selezionata",;
    HelpContextID = 237792806,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=131, Top=90, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="BUSIUNIT", cZoomOnZoom="GSAR_KBU", oKey_1_1="BUCODAZI", oKey_1_2="this.w_codaz1", oKey_2_1="BUCODICE", oKey_2_2="this.w_CODBUN"

  func oCODBUN_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" AND (.w_gestbu $ 'ET') and EMPTY(.w_SUPERBU) and .w_ESSALDI<>'S')
    endwith
   endif
  endfunc

  func oCODBUN_1_21.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  func oCODBUN_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODBUN_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODBUN_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BUSIUNIT_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_codaz1)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"BUCODAZI="+cp_ToStr(this.Parent.oContained.w_codaz1)
    endif
    do cp_zoom with 'BUSIUNIT','*','BUCODAZI,BUCODICE',cp_AbsName(this.parent,'oCODBUN_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_KBU',"Business Unit",'',this.parent.oContained
  endproc
  proc oCODBUN_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_KBU()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.BUCODAZI=w_codaz1
     i_obj.w_BUCODICE=this.parent.oContained.w_CODBUN
     i_obj.ecpSave()
  endproc

  add object oSUPERBU_1_22 as StdField with uid="UFPYRJKQEW",rtseq=22,rtrep=.f.,;
    cFormVar = "w_SUPERBU", cQueryName = "SUPERBU",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Gruppo Business Unit selezionato",;
    HelpContextID = 33568038,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=131, Top=118, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="SB_MAST", cZoomOnZoom="GSAR_MSB", oKey_1_1="SBCODICE", oKey_1_2="this.w_SUPERBU"

  func oSUPERBU_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_APPLICATION = "ad hoc ENTERPRISE" AND (.w_gestbu $ 'ET') AND EMPTY(.w_CODBUN) and .w_ESSALDI<>'S')
    endwith
   endif
  endfunc

  func oSUPERBU_1_22.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  func oSUPERBU_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oSUPERBU_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSUPERBU_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SB_MAST','*','SBCODICE',cp_AbsName(this.parent,'oSUPERBU_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MSB',"Gruppi Business Unit",'Gruppobunit.SB_MAST_VZM',this.parent.oContained
  endproc
  proc oSUPERBU_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MSB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SBCODICE=this.parent.oContained.w_SUPERBU
     i_obj.ecpSave()
  endproc

  add object oSBDESCRI_1_23 as StdField with uid="BAWNZUAVRD",rtseq=23,rtrep=.f.,;
    cFormVar = "w_SBDESCRI", cQueryName = "SBDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 51339887,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=268, Top=118, InputMask=replicate('X',35)

  func oSBDESCRI_1_23.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc


  add object oPROVVI_1_24 as StdCombo with uid="WHBVZTLTRA",value=3,rtseq=24,rtrep=.f.,left=131,top=146,width=136,height=21;
    , ToolTipText = "Selezione dello stato dei movimenti da stampare";
    , HelpContextID = 156312054;
    , cFormVar="w_PROVVI",RowSource=""+"Confermati,"+"Provvisori,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oPROVVI_1_24.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    iif(this.value =3,'',;
    space(1)))))
  endfunc
  func oPROVVI_1_24.GetRadio()
    this.Parent.oContained.w_PROVVI = this.RadioValue()
    return .t.
  endfunc

  func oPROVVI_1_24.SetRadio()
    this.Parent.oContained.w_PROVVI=trim(this.Parent.oContained.w_PROVVI)
    this.value = ;
      iif(this.Parent.oContained.w_PROVVI=='N',1,;
      iif(this.Parent.oContained.w_PROVVI=='S',2,;
      iif(this.Parent.oContained.w_PROVVI=='',3,;
      0)))
  endfunc

  func oPROVVI_1_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ESSALDI<>'S')
    endwith
   endif
  endfunc


  add object oTIPSTA_1_25 as StdCombo with uid="JHYKJDBDOH",rtseq=25,rtrep=.f.,left=132,top=173,width=111,height=21;
    , ToolTipText = "Selezioni di stampa";
    , HelpContextID = 19802422;
    , cFormVar="w_TIPSTA",RowSource=""+"Fornitori,"+"Clienti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPSTA_1_25.RadioValue()
    return(iif(this.value =1,'F',;
    iif(this.value =2,'C',;
    space(1))))
  endfunc
  func oTIPSTA_1_25.GetRadio()
    this.Parent.oContained.w_TIPSTA = this.RadioValue()
    return .t.
  endfunc

  func oTIPSTA_1_25.SetRadio()
    this.Parent.oContained.w_TIPSTA=trim(this.Parent.oContained.w_TIPSTA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPSTA=='F',1,;
      iif(this.Parent.oContained.w_TIPSTA=='C',2,;
      0))
  endfunc


  add object oTIPORD_1_26 as StdCombo with uid="JWCBVLUOQS",rtseq=26,rtrep=.f.,left=132,top=202,width=111,height=21;
    , ToolTipText = "Tipo di ordinamento";
    , HelpContextID = 67774774;
    , cFormVar="w_TIPORD",RowSource=""+"Codice,"+"Descrizione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPORD_1_26.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oTIPORD_1_26.GetRadio()
    this.Parent.oContained.w_TIPORD = this.RadioValue()
    return .t.
  endfunc

  func oTIPORD_1_26.SetRadio()
    this.Parent.oContained.w_TIPORD=trim(this.Parent.oContained.w_TIPORD)
    this.value = ;
      iif(this.Parent.oContained.w_TIPORD=='C',1,;
      iif(this.Parent.oContained.w_TIPORD=='D',2,;
      0))
  endfunc

  add object oTUTTI_1_27 as StdCheck with uid="TDJCVSTXKQ",rtseq=27,rtrep=.f.,left=349, top=147, caption="Escludi conti non movimentati",;
    ToolTipText = "Stampa solo i conti movimentati nel periodo",;
    HelpContextID = 8424138,;
    cFormVar="w_TUTTI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTUTTI_1_27.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTUTTI_1_27.GetRadio()
    this.Parent.oContained.w_TUTTI = this.RadioValue()
    return .t.
  endfunc

  func oTUTTI_1_27.SetRadio()
    this.Parent.oContained.w_TUTTI=trim(this.Parent.oContained.w_TUTTI)
    this.value = ;
      iif(this.Parent.oContained.w_TUTTI=='S',1,;
      0)
  endfunc

  func oTUTTI_1_27.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TUTTI_Z<>'S')
    endwith
   endif
  endfunc

  add object oTUTTI_Z_1_28 as StdCheck with uid="FSKLWQCHUI",rtseq=28,rtrep=.f.,left=349, top=167, caption="Escludi importi a zero",;
    ToolTipText = "Stampa solo i conti con saldo diverso da 0",;
    HelpContextID = 25201354,;
    cFormVar="w_TUTTI_Z", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTUTTI_Z_1_28.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oTUTTI_Z_1_28.GetRadio()
    this.Parent.oContained.w_TUTTI_Z = this.RadioValue()
    return .t.
  endfunc

  func oTUTTI_Z_1_28.SetRadio()
    this.Parent.oContained.w_TUTTI_Z=trim(this.Parent.oContained.w_TUTTI_Z)
    this.value = ;
      iif(this.Parent.oContained.w_TUTTI_Z=='S',1,;
      0)
  endfunc

  add object oPERIODO_1_29 as StdCheck with uid="YOCDHBUIFA",rtseq=29,rtrep=.f.,left=349, top=187, caption="Saldi periodo",;
    ToolTipText = "Bilancio con sezioni dare / avere",;
    HelpContextID = 64242934,;
    cFormVar="w_PERIODO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPERIODO_1_29.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPERIODO_1_29.GetRadio()
    this.Parent.oContained.w_PERIODO = this.RadioValue()
    return .t.
  endfunc

  func oPERIODO_1_29.SetRadio()
    this.Parent.oContained.w_PERIODO=trim(this.Parent.oContained.w_PERIODO)
    this.value = ;
      iif(this.Parent.oContained.w_PERIODO=='S',1,;
      0)
  endfunc

  add object oEXCEL_1_30 as StdCheck with uid="DRRRMIWZWM",rtseq=30,rtrep=.f.,left=349, top=207, caption="Stampa su Excel",;
    ToolTipText = "Invia i dati su foglio Excel",;
    HelpContextID = 6330554,;
    cFormVar="w_EXCEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEXCEL_1_30.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oEXCEL_1_30.GetRadio()
    this.Parent.oContained.w_EXCEL = this.RadioValue()
    return .t.
  endfunc

  func oEXCEL_1_30.SetRadio()
    this.Parent.oContained.w_EXCEL=trim(this.Parent.oContained.w_EXCEL)
    this.value = ;
      iif(this.Parent.oContained.w_EXCEL=='S',1,;
      0)
  endfunc

  func oEXCEL_1_30.mHide()
    with this.Parent.oContained
      return (g_office<>'M')
    endwith
  endfunc


  add object oBtn_1_33 as StdButton with uid="FDGMRNFPIR",left=552, top=191, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la stampa";
    , HelpContextID = 103446;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_33.Click()
      with this.Parent.oContained
        do GSCG_BPC with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_33.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_monete='c' or ((not empty(.w_DIVISA)) and (not empty(.w_CAMVAL))))
      endwith
    endif
  endfunc


  add object oBtn_1_35 as StdButton with uid="DFQQCRXSSP",left=607, top=191, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 103446;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_35.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oBUDESCRI_1_43 as StdField with uid="MGNDNZNVWZ",rtseq=33,rtrep=.f.,;
    cFormVar = "w_BUDESCRI", cQueryName = "BUDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 51344479,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=182, Top=90, InputMask=replicate('X',35)

  func oBUDESCRI_1_43.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oCAMVAL_1_48 as StdField with uid="LNHAHYTQDV",rtseq=35,rtrep=.f.,;
    cFormVar = "w_CAMVAL", cQueryName = "CAMVAL",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio in uscita nei confronti della moneta di conto",;
    HelpContextID = 184610854,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=529, Top=59, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  func oCAMVAL_1_48.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (i_datsys<.w_DATEMU or .w_EXTRAEUR=0)
    endwith
   endif
  endfunc

  add object oEXCEL_1_70 as StdCheck with uid="QBGQCEYDDU",rtseq=55,rtrep=.f.,left=349, top=207, caption="Stampa su Calc",;
    ToolTipText = "Invia i dati su foglio Calc",;
    HelpContextID = 6330554,;
    cFormVar="w_EXCEL", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oEXCEL_1_70.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oEXCEL_1_70.GetRadio()
    this.Parent.oContained.w_EXCEL = this.RadioValue()
    return .t.
  endfunc

  func oEXCEL_1_70.SetRadio()
    this.Parent.oContained.w_EXCEL=trim(this.Parent.oContained.w_EXCEL)
    this.value = ;
      iif(this.Parent.oContained.w_EXCEL=='S',1,;
      0)
  endfunc

  func oEXCEL_1_70.mHide()
    with this.Parent.oContained
      return (g_office='M')
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="ISRXIQVIND",Visible=.t., Left=395, Top=33,;
    Alignment=1, Width=22, Height=15,;
    Caption="al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="ELHQNMUUDG",Visible=.t., Left=8, Top=59,;
    Alignment=1, Width=122, Height=15,;
    Caption="Valuta di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="SJSVTBTYDO",Visible=.t., Left=285, Top=59,;
    Alignment=1, Width=55, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="BZVKYHBRZL",Visible=.t., Left=48, Top=33,;
    Alignment=1, Width=82, Height=15,;
    Caption="Tipo bilancio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="OWSKXDGPRJ",Visible=.t., Left=58, Top=7,;
    Alignment=1, Width=72, Height=15,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="ARDWDELRML",Visible=.t., Left=17, Top=118,;
    Alignment=1, Width=113, Height=15,;
    Caption="Gruppo B.Unit:"  ;
  , bGlobalFont=.t.

  func oStr_1_40.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="FJAGMCDOIX",Visible=.t., Left=260, Top=33,;
    Alignment=1, Width=55, Height=18,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="QUNFKDYXUK",Visible=.t., Left=461, Top=59,;
    Alignment=1, Width=66, Height=15,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="DIRJXERMBY",Visible=.t., Left=75, Top=90,;
    Alignment=1, Width=55, Height=15,;
    Caption="B. Unit:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="EKFGTNCDXW",Visible=.t., Left=86, Top=146,;
    Alignment=1, Width=44, Height=15,;
    Caption="Stato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_47 as StdString with uid="HPBRGNGKIX",Visible=.t., Left=7, Top=173,;
    Alignment=1, Width=122, Height=15,;
    Caption="Selezione stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_65 as StdString with uid="IOGNIYXZAE",Visible=.t., Left=27, Top=203,;
    Alignment=1, Width=102, Height=18,;
    Caption="Tipo ordinamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="TRQCFYUXYJ",Visible=.t., Left=133, Top=286,;
    Alignment=0, Width=416, Height=19,;
    Caption="Attenzione aggiungere eventuali variabili anche a gscg_spc"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_67.mHide()
    with this.Parent.oContained
      return (.T.)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_scf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
