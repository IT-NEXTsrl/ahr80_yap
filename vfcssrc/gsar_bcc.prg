* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bcc                                                        *
*              Cancellazione collo/confezione                                  *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_12]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-12-10                                                      *
* Last revis.: 2008-06-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bcc",oParentObject)
return(i_retval)

define class tgsar_bcc as StdBatch
  * --- Local variables
  w_MESS = space(50)
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch di controllo /avviso cancellazione Collo lanciato all'evento Delete start di GSAR_MTO 
    this.w_MESS = "Attenzione: la cancellazione/modifica della tipologia collo,%0provocherÓ la cancellazione/modifica della stessa%0all'interno dell'anagrafica articoli se presente%0Vuoi cancellare ugualmente?"
    if ah_YesNo(this.w_MESS)
      * --- OK cancellazione
    else
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_MsgFormat("Cancellazione annullata")
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
