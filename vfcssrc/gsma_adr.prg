* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_adr                                                        *
*              Dati rilevati                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_233]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-05-14                                                      *
* Last revis.: 2014-10-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_adr"))

* --- Class definition
define class tgsma_adr as StdForm
  Top    = 2
  Left   = 8

  * --- Standard Properties
  Width  = 643
  Height = 327+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-28"
  HelpContextID=92961641
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=63

  * --- Constant Properties
  RILEVAZI_IDX = 0
  ADDERILE_IDX = 0
  MAGAZZIN_IDX = 0
  UBICAZIO_IDX = 0
  KEY_ARTI_IDX = 0
  LOTTIART_IDX = 0
  UNIMIS_IDX = 0
  ART_ICOL_IDX = 0
  CODIRILE_IDX = 0
  MVM_MAST_IDX = 0
  MATRICOL_IDX = 0
  CAN_TIER_IDX = 0
  cFile = "RILEVAZI"
  cKeySelect = "DRSERIAL"
  cKeyWhere  = "DRSERIAL=this.w_DRSERIAL"
  cKeyWhereODBC = '"DRSERIAL="+cp_ToStrODBC(this.w_DRSERIAL)';

  cKeyWhereODBCqualified = '"RILEVAZI.DRSERIAL="+cp_ToStrODBC(this.w_DRSERIAL)';

  cPrg = "gsma_adr"
  cComment = "Dati rilevati"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DRSERIAL = space(15)
  w_DRCODRIL = space(10)
  w_DATRIL = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DRNUMDOC = 0
  w_DRCODUTE = 0
  w_DRSERMOV = space(10)
  w_APPO = space(10)
  w_DRADDRIL = space(5)
  w_DESADD = space(40)
  w_DRCODMAG = space(5)
  o_DRCODMAG = space(5)
  w_DESMAG = space(30)
  w_DRUBICAZ = space(20)
  o_DRUBICAZ = space(20)
  w_COARTVAR = space(41)
  o_COARTVAR = space(41)
  w_UBIDESCRI = space(40)
  w_DRCODRIC = space(41)
  w_DRCODRIC = space(20)
  w_DRCODART = space(20)
  o_DRCODART = space(20)
  w_DRCODVAR = space(20)
  o_DRCODVAR = space(20)
  w_FLLOTT = space(1)
  w_UNMIS1 = space(3)
  o_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_MOLTIP = 0
  w_OPERAT = space(1)
  w_DESART = space(40)
  w_DR_LOTTO = space(20)
  o_DR_LOTTO = space(20)
  w_DATCRE = ctod('  /  /  ')
  w_DRKEYSAL = space(40)
  w_DATSCA = ctod('  /  /  ')
  w_DRCODMAT = space(40)
  o_DRCODMAT = space(40)
  w_DRCODCOM = space(15)
  w_DRQTAES1 = 0
  w_DRUNIMIS = space(3)
  o_DRUNIMIS = space(3)
  w_DRQTAESI = 0
  o_DRQTAESI = 0
  w_MGUBIC = space(1)
  w_UNMIS3 = space(3)
  o_UNMIS3 = space(3)
  w_MOLTI3 = 0
  o_MOLTI3 = 0
  w_OPERAT3 = space(1)
  w_FLFRAZ = space(1)
  w_UTCC = 0
  w_DATOBSO = ctod('  /  /  ')
  w_UTDC = ctot('')
  w_UTCV = 0
  w_UTDV = ctot('')
  w_LCODART = space(20)
  w_LCODVAR = space(20)
  w_DRKEYULO = space(40)
  w_TIPOART = space(1)
  w_DRCONFER = space(1)
  w_RESCHK = 0
  w_DRDATMOV = ctod('  /  /  ')
  w_DRUTEMOV = 0
  w_GESMAT = space(1)
  w_DTOBS1 = ctod('  /  /  ')
  w_DRREGMOV = 0
  w_DRREGMOVAPP = 0
  w_DRDATMOVAPP = ctod('  /  /  ')
  w_DESART = space(40)
  w_MODUM2 = space(1)
  w_FLUSEP = space(1)
  w_NOFRAZ1 = space(1)
  w_DRDESCOM = space(40)
  w_FLCOMM = space(1)

  * --- Autonumbered Variables
  w_codazi = i_codazi
  op_CODAZI = this.W_CODAZI
  op_DRSERIAL = this.W_DRSERIAL
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'RILEVAZI','gsma_adr')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_adrPag1","gsma_adr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dato rilevato")
      .Pages(1).HelpContextID = 162089230
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[12]
    this.cWorkTables[1]='ADDERILE'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='UBICAZIO'
    this.cWorkTables[4]='KEY_ARTI'
    this.cWorkTables[5]='LOTTIART'
    this.cWorkTables[6]='UNIMIS'
    this.cWorkTables[7]='ART_ICOL'
    this.cWorkTables[8]='CODIRILE'
    this.cWorkTables[9]='MVM_MAST'
    this.cWorkTables[10]='MATRICOL'
    this.cWorkTables[11]='CAN_TIER'
    this.cWorkTables[12]='RILEVAZI'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(12))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.RILEVAZI_IDX,5],7]
    this.nPostItConn=i_TableProp[this.RILEVAZI_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_DRSERIAL = NVL(DRSERIAL,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_2_joined
    link_1_2_joined=.f.
    local link_1_10_joined
    link_1_10_joined=.f.
    local link_1_13_joined
    link_1_13_joined=.f.
    local link_1_15_joined
    link_1_15_joined=.f.
    local link_1_21_joined
    link_1_21_joined=.f.
    local link_1_22_joined
    link_1_22_joined=.f.
    local link_1_23_joined
    link_1_23_joined=.f.
    local link_1_36_joined
    link_1_36_joined=.f.
    local link_1_38_joined
    link_1_38_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from RILEVAZI where DRSERIAL=KeySet.DRSERIAL
    *
    i_nConn = i_TableProp[this.RILEVAZI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RILEVAZI_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('RILEVAZI')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "RILEVAZI.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' RILEVAZI '
      link_1_2_joined=this.AddJoinedLink_1_2(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_10_joined=this.AddJoinedLink_1_10(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_13_joined=this.AddJoinedLink_1_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_15_joined=this.AddJoinedLink_1_15(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_21_joined=this.AddJoinedLink_1_21(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_22_joined=this.AddJoinedLink_1_22(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_23_joined=this.AddJoinedLink_1_23(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_36_joined=this.AddJoinedLink_1_36(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_38_joined=this.AddJoinedLink_1_38(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DRSERIAL',this.w_DRSERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DATRIL = ctod("  /  /  ")
        .w_DESADD = space(40)
        .w_DESMAG = space(30)
        .w_UBIDESCRI = space(40)
        .w_FLLOTT = space(1)
        .w_UNMIS1 = space(3)
        .w_UNMIS2 = space(3)
        .w_MOLTIP = 0
        .w_OPERAT = space(1)
        .w_DESART = space(40)
        .w_DATCRE = ctod("  /  /  ")
        .w_DATSCA = ctod("  /  /  ")
        .w_MGUBIC = space(1)
        .w_UNMIS3 = space(3)
        .w_MOLTI3 = 0
        .w_OPERAT3 = space(1)
        .w_FLFRAZ = space(1)
        .w_DATOBSO = ctod("  /  /  ")
        .w_LCODART = space(20)
        .w_LCODVAR = space(20)
        .w_TIPOART = space(1)
        .w_RESCHK = 0
        .w_GESMAT = space(1)
        .w_DTOBS1 = ctod("  /  /  ")
        .w_DRREGMOVAPP = 0
        .w_DRDATMOVAPP = ctod("  /  /  ")
        .w_DESART = space(40)
        .w_MODUM2 = space(1)
        .w_FLUSEP = space(1)
        .w_NOFRAZ1 = space(1)
        .w_DRDESCOM = space(40)
        .w_FLCOMM = space(1)
        .w_DRSERIAL = NVL(DRSERIAL,space(15))
        .op_DRSERIAL = .w_DRSERIAL
        .w_DRCODRIL = NVL(DRCODRIL,space(10))
          if link_1_2_joined
            this.w_DRCODRIL = NVL(RICODICE102,NVL(this.w_DRCODRIL,space(10)))
            this.w_DATRIL = NVL(cp_ToDate(RIDATRIL102),ctod("  /  /  "))
          else
          .link_1_2('Load')
          endif
        .w_OBTEST = .w_DATRIL
        .w_DRNUMDOC = NVL(DRNUMDOC,0)
        .w_DRCODUTE = NVL(DRCODUTE,0)
        .w_DRSERMOV = NVL(DRSERMOV,space(10))
          if link_1_10_joined
            this.w_DRSERMOV = NVL(MMSERIAL110,NVL(this.w_DRSERMOV,space(10)))
            this.w_DRREGMOVAPP = NVL(MMNUMREG110,0)
            this.w_DRDATMOVAPP = NVL(cp_ToDate(MMDATREG110),ctod("  /  /  "))
            this.w_DRUTEMOV = NVL(MMCODUTE110,0)
          else
          .link_1_10('Load')
          endif
        .w_APPO = .w_DRSERMOV
        .w_DRADDRIL = NVL(DRADDRIL,space(5))
          if link_1_13_joined
            this.w_DRADDRIL = NVL(ARCODICE113,NVL(this.w_DRADDRIL,space(5)))
            this.w_DESADD = NVL(ARDESCRI113,space(40))
          else
          .link_1_13('Load')
          endif
        .w_DRCODMAG = NVL(DRCODMAG,space(5))
          if link_1_15_joined
            this.w_DRCODMAG = NVL(MGCODMAG115,NVL(this.w_DRCODMAG,space(5)))
            this.w_DESMAG = NVL(MGDESMAG115,space(30))
            this.w_MGUBIC = NVL(MGFLUBIC115,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(MGDTOBSO115),ctod("  /  /  "))
          else
          .link_1_15('Load')
          endif
        .w_DRUBICAZ = NVL(DRUBICAZ,space(20))
          .link_1_18('Load')
        .w_COARTVAR = iif(empty(.w_drcodart),space(iif( g_APPLICATION="ADHOC REVOLUTION" ,20,41 )),iif( g_APPLICATION="ADHOC REVOLUTION" ,.w_drcodart,rtrim(.w_drcodart)+iif(empty(nvl(.w_drcodvar,'')),'','#'+alltrim(.w_drcodvar)) ))
        .w_DRCODRIC = NVL(DRCODRIC,space(41))
          if link_1_21_joined
            this.w_DRCODRIC = NVL(CACODICE121,NVL(this.w_DRCODRIC,space(41)))
            this.w_DRCODART = NVL(CACODART121,space(20))
            this.w_DRCODVAR = NVL(CACODVAR121,space(20))
            this.w_UNMIS3 = NVL(CAUNIMIS121,space(3))
            this.w_OPERAT3 = NVL(CAOPERAT121,space(1))
            this.w_MOLTI3 = NVL(CAMOLTIP121,0)
            this.w_DESART = NVL(CADESART121,space(40))
            this.w_DTOBS1 = NVL(cp_ToDate(CADTOBSO121),ctod("  /  /  "))
            this.w_TIPOART = NVL(CA__TIPO121,space(1))
          else
          .link_1_21('Load')
          endif
        .w_DRCODRIC = NVL(DRCODRIC,space(20))
          if link_1_22_joined
            this.w_DRCODRIC = NVL(CACODICE122,NVL(this.w_DRCODRIC,space(20)))
            this.w_DRCODART = NVL(CACODART122,space(20))
            this.w_DRCODVAR = NVL(CACODVAR122,space(20))
            this.w_UNMIS3 = NVL(CAUNIMIS122,space(3))
            this.w_OPERAT3 = NVL(CAOPERAT122,space(1))
            this.w_MOLTI3 = NVL(CAMOLTIP122,0)
            this.w_DESART = NVL(CADESART122,space(40))
            this.w_DTOBS1 = NVL(cp_ToDate(CADTOBSO122),ctod("  /  /  "))
            this.w_TIPOART = NVL(CA__TIPO122,space(1))
          else
          .link_1_22('Load')
          endif
        .w_DRCODART = NVL(DRCODART,space(20))
          if link_1_23_joined
            this.w_DRCODART = NVL(ARCODART123,NVL(this.w_DRCODART,space(20)))
            this.w_FLLOTT = NVL(ARFLLOTT123,space(1))
            this.w_UNMIS1 = NVL(ARUNMIS1123,space(3))
            this.w_MOLTIP = NVL(ARMOLTIP123,0)
            this.w_UNMIS2 = NVL(ARUNMIS2123,space(3))
            this.w_OPERAT = NVL(AROPERAT123,space(1))
            this.w_GESMAT = NVL(ARGESMAT123,space(1))
            this.w_FLUSEP = NVL(ARFLUSEP123,space(1))
            this.w_FLCOMM = NVL(ARSALCOM123,space(1))
          else
          .link_1_23('Load')
          endif
        .w_DRCODVAR = NVL(DRCODVAR,space(20))
          .link_1_26('Load')
        .w_DR_LOTTO = NVL(DR_LOTTO,space(20))
          .link_1_31('Load')
        .w_DRKEYSAL = NVL(DRKEYSAL,space(40))
        .w_DRCODMAT = NVL(DRCODMAT,space(40))
          * evitabile
          *.link_1_35('Load')
        .w_DRCODCOM = NVL(DRCODCOM,space(15))
          if link_1_36_joined
            this.w_DRCODCOM = NVL(CNCODCAN136,NVL(this.w_DRCODCOM,space(15)))
            this.w_DRDESCOM = NVL(CNDESCAN136,space(40))
          else
          .link_1_36('Load')
          endif
        .w_DRQTAES1 = NVL(DRQTAES1,0)
        .w_DRUNIMIS = NVL(DRUNIMIS,space(3))
          if link_1_38_joined
            this.w_DRUNIMIS = NVL(UMCODICE138,NVL(this.w_DRUNIMIS,space(3)))
            this.w_FLFRAZ = NVL(UMFLFRAZ138,space(1))
          else
          .link_1_38('Load')
          endif
        .w_DRQTAESI = NVL(DRQTAESI,0)
        .w_UTCC = NVL(UTCC,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTCV = NVL(UTCV,0)
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_DRKEYULO = NVL(DRKEYULO,space(40))
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
        .w_DRCONFER = NVL(DRCONFER,space(1))
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .w_DRDATMOV = NVL(cp_ToDate(DRDATMOV),ctod("  /  /  "))
        .w_DRUTEMOV = NVL(DRUTEMOV,0)
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        .w_DRREGMOV = NVL(DRREGMOV,0)
        .op_codazi = .w_codazi
        cp_LoadRecExtFlds(this,'RILEVAZI')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_71.enabled = this.oPgFrm.Page1.oPag.oBtn_1_71.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DRSERIAL = space(15)
      .w_DRCODRIL = space(10)
      .w_DATRIL = ctod("  /  /  ")
      .w_OBTEST = ctod("  /  /  ")
      .w_DRNUMDOC = 0
      .w_DRCODUTE = 0
      .w_DRSERMOV = space(10)
      .w_APPO = space(10)
      .w_DRADDRIL = space(5)
      .w_DESADD = space(40)
      .w_DRCODMAG = space(5)
      .w_DESMAG = space(30)
      .w_DRUBICAZ = space(20)
      .w_COARTVAR = space(41)
      .w_UBIDESCRI = space(40)
      .w_DRCODRIC = space(41)
      .w_DRCODRIC = space(20)
      .w_DRCODART = space(20)
      .w_DRCODVAR = space(20)
      .w_FLLOTT = space(1)
      .w_UNMIS1 = space(3)
      .w_UNMIS2 = space(3)
      .w_MOLTIP = 0
      .w_OPERAT = space(1)
      .w_DESART = space(40)
      .w_DR_LOTTO = space(20)
      .w_DATCRE = ctod("  /  /  ")
      .w_DRKEYSAL = space(40)
      .w_DATSCA = ctod("  /  /  ")
      .w_DRCODMAT = space(40)
      .w_DRCODCOM = space(15)
      .w_DRQTAES1 = 0
      .w_DRUNIMIS = space(3)
      .w_DRQTAESI = 0
      .w_MGUBIC = space(1)
      .w_UNMIS3 = space(3)
      .w_MOLTI3 = 0
      .w_OPERAT3 = space(1)
      .w_FLFRAZ = space(1)
      .w_UTCC = 0
      .w_DATOBSO = ctod("  /  /  ")
      .w_UTDC = ctot("")
      .w_UTCV = 0
      .w_UTDV = ctot("")
      .w_LCODART = space(20)
      .w_LCODVAR = space(20)
      .w_DRKEYULO = space(40)
      .w_TIPOART = space(1)
      .w_DRCONFER = space(1)
      .w_RESCHK = 0
      .w_DRDATMOV = ctod("  /  /  ")
      .w_DRUTEMOV = 0
      .w_GESMAT = space(1)
      .w_DTOBS1 = ctod("  /  /  ")
      .w_DRREGMOV = 0
      .w_DRREGMOVAPP = 0
      .w_DRDATMOVAPP = ctod("  /  /  ")
      .w_DESART = space(40)
      .w_MODUM2 = space(1)
      .w_FLUSEP = space(1)
      .w_NOFRAZ1 = space(1)
      .w_DRDESCOM = space(40)
      .w_FLCOMM = space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
          if not(empty(.w_DRCODRIL))
          .link_1_2('Full')
          endif
          .DoRTCalc(3,3,.f.)
        .w_OBTEST = .w_DATRIL
          .DoRTCalc(5,5,.f.)
        .w_DRCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
        .DoRTCalc(7,7,.f.)
          if not(empty(.w_DRSERMOV))
          .link_1_10('Full')
          endif
        .w_APPO = .w_DRSERMOV
        .DoRTCalc(9,9,.f.)
          if not(empty(.w_DRADDRIL))
          .link_1_13('Full')
          endif
        .DoRTCalc(10,11,.f.)
          if not(empty(.w_DRCODMAG))
          .link_1_15('Full')
          endif
          .DoRTCalc(12,12,.f.)
        .w_DRUBICAZ = IIF(g_PERUBI='S' AND .w_MGUBIC='S', .w_DRUBICAZ, SPACE(20))
        .DoRTCalc(13,13,.f.)
          if not(empty(.w_DRUBICAZ))
          .link_1_18('Full')
          endif
        .w_COARTVAR = iif(empty(.w_drcodart),space(iif( g_APPLICATION="ADHOC REVOLUTION" ,20,41 )),iif( g_APPLICATION="ADHOC REVOLUTION" ,.w_drcodart,rtrim(.w_drcodart)+iif(empty(nvl(.w_drcodvar,'')),'','#'+alltrim(.w_drcodvar)) ))
          .DoRTCalc(15,15,.f.)
        .w_DRCODRIC = .w_COARTVAR
        .DoRTCalc(16,16,.f.)
          if not(empty(.w_DRCODRIC))
          .link_1_21('Full')
          endif
        .w_DRCODRIC = .w_COARTVAR
        .DoRTCalc(17,17,.f.)
          if not(empty(.w_DRCODRIC))
          .link_1_22('Full')
          endif
        .DoRTCalc(18,18,.f.)
          if not(empty(.w_DRCODART))
          .link_1_23('Full')
          endif
        .DoRTCalc(19,21,.f.)
          if not(empty(.w_UNMIS1))
          .link_1_26('Full')
          endif
          .DoRTCalc(22,25,.f.)
        .w_DR_LOTTO = IIF(g_PERLOT='S' AND ((.w_FLLOTT = 'S'  And g_APPLICATION <> "ADHOC REVOLUTION") Or (.w_FLLOTT $ 'S-C'  And g_APPLICATION = "ADHOC REVOLUTION") ) ,.w_DR_LOTTO,SPACE(20))
        .DoRTCalc(26,26,.f.)
          if not(empty(.w_DR_LOTTO))
          .link_1_31('Full')
          endif
          .DoRTCalc(27,27,.f.)
        .w_DRKEYSAL = iif( g_APPLICATION="ADHOC REVOLUTION" ,iif(empty(.w_DRCODART),SPACE(20),.w_DRCODART),iif(empty(.w_DRCODART),SPACE(40),.w_DRCODART+ iif(empty(.w_DRCODVAR),repl('#',20),.w_DRCODVAR)) )
        .DoRTCalc(29,30,.f.)
          if not(empty(.w_DRCODMAT))
          .link_1_35('Full')
          endif
        .DoRTCalc(31,31,.f.)
          if not(empty(.w_DRCODCOM))
          .link_1_36('Full')
          endif
        .w_DRQTAES1 = CALQTA(.w_DRQTAESI, .w_DRUNIMIS ,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_NOFRAZ1, .w_MODUM2,  '', .w_UNMIS3, .w_OPERAT3, .w_MOLTI3)
        .w_DRUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
        .DoRTCalc(33,33,.f.)
          if not(empty(.w_DRUNIMIS))
          .link_1_38('Full')
          endif
        .w_DRQTAESI = 0
          .DoRTCalc(35,46,.f.)
        .w_DRKEYULO = iif(empty(.w_DRUBICAZ),repl('#',20),.w_DRUBICAZ)+iif(empty(.w_DR_LOTTO),repl('#',20),.w_DR_LOTTO)
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
          .DoRTCalc(48,48,.f.)
        .w_DRCONFER = ' '
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .w_RESCHK = 0
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'RILEVAZI')
    this.DoRTCalc(51,63,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_71.enabled = this.oPgFrm.Page1.oPag.oBtn_1_71.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  procedure InitAutonumber()
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RILEVAZI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RILEVAZI_IDX,2])
    with this	
      cp_AskTableProg(this,i_nConn,"RILSER","i_CODAZI,w_DRSERIAL")
      .op_CODAZI = .w_CODAZI
      .op_DRSERIAL = .w_DRSERIAL
    endwith
    this.SetControlsValue()
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oDRCODRIL_1_2.enabled = i_bVal
      .Page1.oPag.oDRNUMDOC_1_5.enabled = i_bVal
      .Page1.oPag.oDRADDRIL_1_13.enabled = i_bVal
      .Page1.oPag.oDRCODMAG_1_15.enabled = i_bVal
      .Page1.oPag.oDRUBICAZ_1_18.enabled = i_bVal
      .Page1.oPag.oDRCODRIC_1_21.enabled = i_bVal
      .Page1.oPag.oDRCODRIC_1_22.enabled = i_bVal
      .Page1.oPag.oDR_LOTTO_1_31.enabled = i_bVal
      .Page1.oPag.oDRCODMAT_1_35.enabled = i_bVal
      .Page1.oPag.oDRCODCOM_1_36.enabled = i_bVal
      .Page1.oPag.oDRUNIMIS_1_38.enabled = i_bVal
      .Page1.oPag.oDRQTAESI_1_39.enabled = i_bVal
      .Page1.oPag.oBtn_1_71.enabled = .Page1.oPag.oBtn_1_71.mCond()
      .Page1.oPag.oObj_1_61.enabled = i_bVal
      .Page1.oPag.oObj_1_63.enabled = i_bVal
      .Page1.oPag.oObj_1_64.enabled = i_bVal
      .Page1.oPag.oObj_1_74.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oDRCODRIL_1_2.enabled = .t.
        .Page1.oPag.oDRCODMAG_1_15.enabled = .t.
        .Page1.oPag.oDRCODRIC_1_21.enabled = .t.
        .Page1.oPag.oDRCODCOM_1_36.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'RILEVAZI',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.RILEVAZI_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRSERIAL,"DRSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRCODRIL,"DRCODRIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRNUMDOC,"DRNUMDOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRCODUTE,"DRCODUTE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRSERMOV,"DRSERMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRADDRIL,"DRADDRIL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRCODMAG,"DRCODMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRUBICAZ,"DRUBICAZ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRCODRIC,"DRCODRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRCODRIC,"DRCODRIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRCODART,"DRCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRCODVAR,"DRCODVAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DR_LOTTO,"DR_LOTTO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRKEYSAL,"DRKEYSAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRCODMAT,"DRCODMAT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRCODCOM,"DRCODCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRQTAES1,"DRQTAES1",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRUNIMIS,"DRUNIMIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRQTAESI,"DRQTAESI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRKEYULO,"DRKEYULO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRCONFER,"DRCONFER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRDATMOV,"DRDATMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRUTEMOV,"DRUTEMOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DRREGMOV,"DRREGMOV",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.RILEVAZI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RILEVAZI_IDX,2])
    i_lTable = "RILEVAZI"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.RILEVAZI_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.RILEVAZI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RILEVAZI_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.RILEVAZI_IDX,i_nConn)
      with this
          cp_NextTableProg(this,i_nConn,"RILSER","i_CODAZI,w_DRSERIAL")
      endwith
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into RILEVAZI
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'RILEVAZI')
        i_extval=cp_InsertValODBCExtFlds(this,'RILEVAZI')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DRSERIAL,DRCODRIL,DRNUMDOC,DRCODUTE,DRSERMOV"+;
                  ",DRADDRIL,DRCODMAG,DRUBICAZ,DRCODRIC,DRCODART"+;
                  ",DRCODVAR,DR_LOTTO,DRKEYSAL,DRCODMAT,DRCODCOM"+;
                  ",DRQTAES1,DRUNIMIS,DRQTAESI,UTCC,UTDC"+;
                  ",UTCV,UTDV,DRKEYULO,DRCONFER,DRDATMOV"+;
                  ",DRUTEMOV,DRREGMOV "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DRSERIAL)+;
                  ","+cp_ToStrODBCNull(this.w_DRCODRIL)+;
                  ","+cp_ToStrODBC(this.w_DRNUMDOC)+;
                  ","+cp_ToStrODBC(this.w_DRCODUTE)+;
                  ","+cp_ToStrODBCNull(this.w_DRSERMOV)+;
                  ","+cp_ToStrODBCNull(this.w_DRADDRIL)+;
                  ","+cp_ToStrODBCNull(this.w_DRCODMAG)+;
                  ","+cp_ToStrODBCNull(this.w_DRUBICAZ)+;
                  ","+cp_ToStrODBCNull(this.w_DRCODRIC)+;
                  ","+cp_ToStrODBCNull(this.w_DRCODART)+;
                  ","+cp_ToStrODBC(this.w_DRCODVAR)+;
                  ","+cp_ToStrODBCNull(this.w_DR_LOTTO)+;
                  ","+cp_ToStrODBC(this.w_DRKEYSAL)+;
                  ","+cp_ToStrODBCNull(this.w_DRCODMAT)+;
                  ","+cp_ToStrODBCNull(this.w_DRCODCOM)+;
                  ","+cp_ToStrODBC(this.w_DRQTAES1)+;
                  ","+cp_ToStrODBCNull(this.w_DRUNIMIS)+;
                  ","+cp_ToStrODBC(this.w_DRQTAESI)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_DRKEYULO)+;
                  ","+cp_ToStrODBC(this.w_DRCONFER)+;
                  ","+cp_ToStrODBC(this.w_DRDATMOV)+;
                  ","+cp_ToStrODBC(this.w_DRUTEMOV)+;
                  ","+cp_ToStrODBC(this.w_DRREGMOV)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'RILEVAZI')
        i_extval=cp_InsertValVFPExtFlds(this,'RILEVAZI')
        cp_CheckDeletedKey(i_cTable,0,'DRSERIAL',this.w_DRSERIAL)
        INSERT INTO (i_cTable);
              (DRSERIAL,DRCODRIL,DRNUMDOC,DRCODUTE,DRSERMOV,DRADDRIL,DRCODMAG,DRUBICAZ,DRCODRIC,DRCODART,DRCODVAR,DR_LOTTO,DRKEYSAL,DRCODMAT,DRCODCOM,DRQTAES1,DRUNIMIS,DRQTAESI,UTCC,UTDC,UTCV,UTDV,DRKEYULO,DRCONFER,DRDATMOV,DRUTEMOV,DRREGMOV  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DRSERIAL;
                  ,this.w_DRCODRIL;
                  ,this.w_DRNUMDOC;
                  ,this.w_DRCODUTE;
                  ,this.w_DRSERMOV;
                  ,this.w_DRADDRIL;
                  ,this.w_DRCODMAG;
                  ,this.w_DRUBICAZ;
                  ,this.w_DRCODRIC;
                  ,this.w_DRCODART;
                  ,this.w_DRCODVAR;
                  ,this.w_DR_LOTTO;
                  ,this.w_DRKEYSAL;
                  ,this.w_DRCODMAT;
                  ,this.w_DRCODCOM;
                  ,this.w_DRQTAES1;
                  ,this.w_DRUNIMIS;
                  ,this.w_DRQTAESI;
                  ,this.w_UTCC;
                  ,this.w_UTDC;
                  ,this.w_UTCV;
                  ,this.w_UTDV;
                  ,this.w_DRKEYULO;
                  ,this.w_DRCONFER;
                  ,this.w_DRDATMOV;
                  ,this.w_DRUTEMOV;
                  ,this.w_DRREGMOV;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.RILEVAZI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RILEVAZI_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.RILEVAZI_IDX,i_nConn)
      *
      * update RILEVAZI
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'RILEVAZI')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " DRCODRIL="+cp_ToStrODBCNull(this.w_DRCODRIL)+;
             ",DRNUMDOC="+cp_ToStrODBC(this.w_DRNUMDOC)+;
             ",DRCODUTE="+cp_ToStrODBC(this.w_DRCODUTE)+;
             ",DRSERMOV="+cp_ToStrODBCNull(this.w_DRSERMOV)+;
             ",DRADDRIL="+cp_ToStrODBCNull(this.w_DRADDRIL)+;
             ",DRCODMAG="+cp_ToStrODBCNull(this.w_DRCODMAG)+;
             ",DRUBICAZ="+cp_ToStrODBCNull(this.w_DRUBICAZ)+;
             ",DRCODRIC="+cp_ToStrODBCNull(this.w_DRCODRIC)+;
             ",DRCODART="+cp_ToStrODBCNull(this.w_DRCODART)+;
             ",DRCODVAR="+cp_ToStrODBC(this.w_DRCODVAR)+;
             ",DR_LOTTO="+cp_ToStrODBCNull(this.w_DR_LOTTO)+;
             ",DRKEYSAL="+cp_ToStrODBC(this.w_DRKEYSAL)+;
             ",DRCODMAT="+cp_ToStrODBCNull(this.w_DRCODMAT)+;
             ",DRCODCOM="+cp_ToStrODBCNull(this.w_DRCODCOM)+;
             ",DRQTAES1="+cp_ToStrODBC(this.w_DRQTAES1)+;
             ",DRUNIMIS="+cp_ToStrODBCNull(this.w_DRUNIMIS)+;
             ",DRQTAESI="+cp_ToStrODBC(this.w_DRQTAESI)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",DRKEYULO="+cp_ToStrODBC(this.w_DRKEYULO)+;
             ",DRCONFER="+cp_ToStrODBC(this.w_DRCONFER)+;
             ",DRDATMOV="+cp_ToStrODBC(this.w_DRDATMOV)+;
             ",DRUTEMOV="+cp_ToStrODBC(this.w_DRUTEMOV)+;
             ",DRREGMOV="+cp_ToStrODBC(this.w_DRREGMOV)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'RILEVAZI')
        i_cWhere = cp_PKFox(i_cTable  ,'DRSERIAL',this.w_DRSERIAL  )
        UPDATE (i_cTable) SET;
              DRCODRIL=this.w_DRCODRIL;
             ,DRNUMDOC=this.w_DRNUMDOC;
             ,DRCODUTE=this.w_DRCODUTE;
             ,DRSERMOV=this.w_DRSERMOV;
             ,DRADDRIL=this.w_DRADDRIL;
             ,DRCODMAG=this.w_DRCODMAG;
             ,DRUBICAZ=this.w_DRUBICAZ;
             ,DRCODRIC=this.w_DRCODRIC;
             ,DRCODART=this.w_DRCODART;
             ,DRCODVAR=this.w_DRCODVAR;
             ,DR_LOTTO=this.w_DR_LOTTO;
             ,DRKEYSAL=this.w_DRKEYSAL;
             ,DRCODMAT=this.w_DRCODMAT;
             ,DRCODCOM=this.w_DRCODCOM;
             ,DRQTAES1=this.w_DRQTAES1;
             ,DRUNIMIS=this.w_DRUNIMIS;
             ,DRQTAESI=this.w_DRQTAESI;
             ,UTCC=this.w_UTCC;
             ,UTDC=this.w_UTDC;
             ,UTCV=this.w_UTCV;
             ,UTDV=this.w_UTDV;
             ,DRKEYULO=this.w_DRKEYULO;
             ,DRCONFER=this.w_DRCONFER;
             ,DRDATMOV=this.w_DRDATMOV;
             ,DRUTEMOV=this.w_DRUTEMOV;
             ,DRREGMOV=this.w_DRREGMOV;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.RILEVAZI_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.RILEVAZI_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.RILEVAZI_IDX,i_nConn)
      *
      * delete RILEVAZI
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DRSERIAL',this.w_DRSERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.RILEVAZI_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.RILEVAZI_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
            .w_OBTEST = .w_DATRIL
        .DoRTCalc(5,5,.t.)
            .w_DRCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
          .link_1_10('Full')
            .w_APPO = .w_DRSERMOV
        .DoRTCalc(9,12,.t.)
        if .o_DRCODMAG<>.w_DRCODMAG
            .w_DRUBICAZ = IIF(g_PERUBI='S' AND .w_MGUBIC='S', .w_DRUBICAZ, SPACE(20))
          .link_1_18('Full')
        endif
            .w_COARTVAR = iif(empty(.w_drcodart),space(iif( g_APPLICATION="ADHOC REVOLUTION" ,20,41 )),iif( g_APPLICATION="ADHOC REVOLUTION" ,.w_drcodart,rtrim(.w_drcodart)+iif(empty(nvl(.w_drcodvar,'')),'','#'+alltrim(.w_drcodvar)) ))
        .DoRTCalc(15,15,.t.)
        if .o_COARTVAR<>.w_COARTVAR.or. .o_DRCODMAT<>.w_DRCODMAT
            .w_DRCODRIC = .w_COARTVAR
          .link_1_21('Full')
        endif
        if .o_COARTVAR<>.w_COARTVAR.or. .o_DRCODMAT<>.w_DRCODMAT
            .w_DRCODRIC = .w_COARTVAR
          .link_1_22('Full')
        endif
          .link_1_23('Full')
        .DoRTCalc(19,20,.t.)
          .link_1_26('Full')
        .DoRTCalc(22,25,.t.)
        if .o_DRCODART<>.w_DRCODART
            .w_DR_LOTTO = IIF(g_PERLOT='S' AND ((.w_FLLOTT = 'S'  And g_APPLICATION <> "ADHOC REVOLUTION") Or (.w_FLLOTT $ 'S-C'  And g_APPLICATION = "ADHOC REVOLUTION") ) ,.w_DR_LOTTO,SPACE(20))
          .link_1_31('Full')
        endif
        .DoRTCalc(27,27,.t.)
        if .o_DRCODART<>.w_DRCODART.or. .o_DRCODVAR<>.w_DRCODVAR
            .w_DRKEYSAL = iif( g_APPLICATION="ADHOC REVOLUTION" ,iif(empty(.w_DRCODART),SPACE(20),.w_DRCODART),iif(empty(.w_DRCODART),SPACE(40),.w_DRCODART+ iif(empty(.w_DRCODVAR),repl('#',20),.w_DRCODVAR)) )
        endif
        .DoRTCalc(29,31,.t.)
            .w_DRQTAES1 = CALQTA(.w_DRQTAESI, .w_DRUNIMIS ,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_NOFRAZ1, .w_MODUM2,  '', .w_UNMIS3, .w_OPERAT3, .w_MOLTI3)
        if .o_UNMIS3<>.w_UNMIS3.or. .o_UNMIS1<>.w_UNMIS1.or. .o_MOLTI3<>.w_MOLTI3
            .w_DRUNIMIS = IIF(NOT EMPTY(.w_UNMIS3) AND .w_MOLTI3<>0, .w_UNMIS3, .w_UNMIS1)
          .link_1_38('Full')
        endif
        if .o_DRCODMAT<>.w_DRCODMAT
            .w_DRQTAESI = 0
        endif
        .DoRTCalc(35,46,.t.)
        if .o_DRUBICAZ<>.w_DRUBICAZ.or. .o_DR_LOTTO<>.w_DR_LOTTO
            .w_DRKEYULO = iif(empty(.w_DRUBICAZ),repl('#',20),.w_DRUBICAZ)+iif(empty(.w_DR_LOTTO),repl('#',20),.w_DR_LOTTO)
        endif
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
        if .o_DRQTAESI<>.w_DRQTAESI.or. .o_DRUNIMIS<>.w_DRUNIMIS
          .Calculate_DJIBSZQZKL()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
        if .op_CODAZI<>.w_CODAZI
           cp_AskTableProg(this,i_nConn,"RILSER","i_CODAZI,w_DRSERIAL")
          .op_DRSERIAL = .w_DRSERIAL
        endif
        .op_CODAZI = .w_CODAZI
      endwith
      this.DoRTCalc(48,63,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_61.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_64.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_74.Calculate()
    endwith
  return

  proc Calculate_DJIBSZQZKL()
    with this
          * --- Gsma_brl(Q)
          gsma_brl(this;
              ,'Q';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDRCODRIL_1_2.enabled = this.oPgFrm.Page1.oPag.oDRCODRIL_1_2.mCond()
    this.oPgFrm.Page1.oPag.oDRNUMDOC_1_5.enabled = this.oPgFrm.Page1.oPag.oDRNUMDOC_1_5.mCond()
    this.oPgFrm.Page1.oPag.oDRADDRIL_1_13.enabled = this.oPgFrm.Page1.oPag.oDRADDRIL_1_13.mCond()
    this.oPgFrm.Page1.oPag.oDRCODMAG_1_15.enabled = this.oPgFrm.Page1.oPag.oDRCODMAG_1_15.mCond()
    this.oPgFrm.Page1.oPag.oDRUBICAZ_1_18.enabled = this.oPgFrm.Page1.oPag.oDRUBICAZ_1_18.mCond()
    this.oPgFrm.Page1.oPag.oDRCODRIC_1_21.enabled = this.oPgFrm.Page1.oPag.oDRCODRIC_1_21.mCond()
    this.oPgFrm.Page1.oPag.oDRCODRIC_1_22.enabled = this.oPgFrm.Page1.oPag.oDRCODRIC_1_22.mCond()
    this.oPgFrm.Page1.oPag.oDR_LOTTO_1_31.enabled = this.oPgFrm.Page1.oPag.oDR_LOTTO_1_31.mCond()
    this.oPgFrm.Page1.oPag.oDRCODMAT_1_35.enabled = this.oPgFrm.Page1.oPag.oDRCODMAT_1_35.mCond()
    this.oPgFrm.Page1.oPag.oDRCODCOM_1_36.enabled = this.oPgFrm.Page1.oPag.oDRCODCOM_1_36.mCond()
    this.oPgFrm.Page1.oPag.oDRUNIMIS_1_38.enabled = this.oPgFrm.Page1.oPag.oDRUNIMIS_1_38.mCond()
    this.oPgFrm.Page1.oPag.oDRQTAESI_1_39.enabled = this.oPgFrm.Page1.oPag.oDRQTAESI_1_39.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_71.enabled = this.oPgFrm.Page1.oPag.oBtn_1_71.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oDRCODRIC_1_21.visible=!this.oPgFrm.Page1.oPag.oDRCODRIC_1_21.mHide()
    this.oPgFrm.Page1.oPag.oDRCODRIC_1_22.visible=!this.oPgFrm.Page1.oPag.oDRCODRIC_1_22.mHide()
    this.oPgFrm.Page1.oPag.oDESART_1_30.visible=!this.oPgFrm.Page1.oPag.oDESART_1_30.mHide()
    this.oPgFrm.Page1.oPag.oDRCODMAT_1_35.visible=!this.oPgFrm.Page1.oPag.oDRCODMAT_1_35.mHide()
    this.oPgFrm.Page1.oPag.oDRDATMOV_1_66.visible=!this.oPgFrm.Page1.oPag.oDRDATMOV_1_66.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_67.visible=!this.oPgFrm.Page1.oPag.oStr_1_67.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_69.visible=!this.oPgFrm.Page1.oPag.oStr_1_69.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_70.visible=!this.oPgFrm.Page1.oPag.oStr_1_70.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_71.visible=!this.oPgFrm.Page1.oPag.oBtn_1_71.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_72.visible=!this.oPgFrm.Page1.oPag.oStr_1_72.mHide()
    this.oPgFrm.Page1.oPag.oDRREGMOV_1_76.visible=!this.oPgFrm.Page1.oPag.oDRREGMOV_1_76.mHide()
    this.oPgFrm.Page1.oPag.oDRREGMOVAPP_1_77.visible=!this.oPgFrm.Page1.oPag.oDRREGMOVAPP_1_77.mHide()
    this.oPgFrm.Page1.oPag.oDRDATMOVAPP_1_78.visible=!this.oPgFrm.Page1.oPag.oDRDATMOVAPP_1_78.mHide()
    this.oPgFrm.Page1.oPag.oDESART_1_79.visible=!this.oPgFrm.Page1.oPag.oDESART_1_79.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_61.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_63.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_64.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_74.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=DRCODRIL
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CODIRILE_IDX,3]
    i_lTable = "CODIRILE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CODIRILE_IDX,2], .t., this.CODIRILE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CODIRILE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRCODRIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACR',True,'CODIRILE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" RICODICE like "+cp_ToStrODBC(trim(this.w_DRCODRIL)+"%");

          i_ret=cp_SQL(i_nConn,"select RICODICE,RIDATRIL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by RICODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'RICODICE',trim(this.w_DRCODRIL))
          select RICODICE,RIDATRIL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by RICODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DRCODRIL)==trim(_Link_.RICODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DRCODRIL) and !this.bDontReportError
            deferred_cp_zoom('CODIRILE','*','RICODICE',cp_AbsName(oSource.parent,'oDRCODRIL_1_2'),i_cWhere,'GSMA_ACR',"Rilevazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RICODICE,RIDATRIL";
                     +" from "+i_cTable+" "+i_lTable+" where RICODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RICODICE',oSource.xKey(1))
            select RICODICE,RIDATRIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRCODRIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select RICODICE,RIDATRIL";
                   +" from "+i_cTable+" "+i_lTable+" where RICODICE="+cp_ToStrODBC(this.w_DRCODRIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'RICODICE',this.w_DRCODRIL)
            select RICODICE,RIDATRIL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRCODRIL = NVL(_Link_.RICODICE,space(10))
      this.w_DATRIL = NVL(cp_ToDate(_Link_.RIDATRIL),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DRCODRIL = space(10)
      endif
      this.w_DATRIL = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CODIRILE_IDX,2])+'\'+cp_ToStr(_Link_.RICODICE,1)
      cp_ShowWarn(i_cKey,this.CODIRILE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRCODRIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_2(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CODIRILE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CODIRILE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_2.RICODICE as RICODICE102"+ ",link_1_2.RIDATRIL as RIDATRIL102"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_2 on RILEVAZI.DRCODRIL=link_1_2.RICODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_2"
          i_cKey=i_cKey+'+" and RILEVAZI.DRCODRIL=link_1_2.RICODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DRSERMOV
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MVM_MAST_IDX,3]
    i_lTable = "MVM_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MVM_MAST_IDX,2], .t., this.MVM_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MVM_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRSERMOV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRSERMOV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MMSERIAL,MMNUMREG,MMDATREG,MMCODUTE";
                   +" from "+i_cTable+" "+i_lTable+" where MMSERIAL="+cp_ToStrODBC(this.w_DRSERMOV);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MMSERIAL',this.w_DRSERMOV)
            select MMSERIAL,MMNUMREG,MMDATREG,MMCODUTE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRSERMOV = NVL(_Link_.MMSERIAL,space(10))
      this.w_DRREGMOVAPP = NVL(_Link_.MMNUMREG,0)
      this.w_DRDATMOVAPP = NVL(cp_ToDate(_Link_.MMDATREG),ctod("  /  /  "))
      this.w_DRUTEMOV = NVL(_Link_.MMCODUTE,0)
    else
      if i_cCtrl<>'Load'
        this.w_DRSERMOV = space(10)
      endif
      this.w_DRREGMOVAPP = 0
      this.w_DRDATMOVAPP = ctod("  /  /  ")
      this.w_DRUTEMOV = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MVM_MAST_IDX,2])+'\'+cp_ToStr(_Link_.MMSERIAL,1)
      cp_ShowWarn(i_cKey,this.MVM_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRSERMOV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_10(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MVM_MAST_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MVM_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_10.MMSERIAL as MMSERIAL110"+ ",link_1_10.MMNUMREG as MMNUMREG110"+ ",link_1_10.MMDATREG as MMDATREG110"+ ",link_1_10.MMCODUTE as MMCODUTE110"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_10 on RILEVAZI.DRSERMOV=link_1_10.MMSERIAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_10"
          i_cKey=i_cKey+'+" and RILEVAZI.DRSERMOV=link_1_10.MMSERIAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DRADDRIL
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ADDERILE_IDX,3]
    i_lTable = "ADDERILE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ADDERILE_IDX,2], .t., this.ADDERILE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ADDERILE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRADDRIL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ADD',True,'ADDERILE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODICE like "+cp_ToStrODBC(trim(this.w_DRADDRIL)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODICE',trim(this.w_DRADDRIL))
          select ARCODICE,ARDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DRADDRIL)==trim(_Link_.ARCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DRADDRIL) and !this.bDontReportError
            deferred_cp_zoom('ADDERILE','*','ARCODICE',cp_AbsName(oSource.parent,'oDRADDRIL_1_13'),i_cWhere,'GSMA_ADD',"Addetti rilevazione",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',oSource.xKey(1))
            select ARCODICE,ARDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRADDRIL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODICE,ARDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODICE="+cp_ToStrODBC(this.w_DRADDRIL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODICE',this.w_DRADDRIL)
            select ARCODICE,ARDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRADDRIL = NVL(_Link_.ARCODICE,space(5))
      this.w_DESADD = NVL(_Link_.ARDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DRADDRIL = space(5)
      endif
      this.w_DESADD = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ADDERILE_IDX,2])+'\'+cp_ToStr(_Link_.ARCODICE,1)
      cp_ShowWarn(i_cKey,this.ADDERILE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRADDRIL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ADDERILE_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ADDERILE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_13.ARCODICE as ARCODICE113"+ ",link_1_13.ARDESCRI as ARDESCRI113"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_13 on RILEVAZI.DRADDRIL=link_1_13.ARCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_13"
          i_cKey=i_cKey+'+" and RILEVAZI.DRADDRIL=link_1_13.ARCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DRCODMAG
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_DRCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_DRCODMAG))
          select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DRCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DRCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oDRCODMAG_1_15'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_DRCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_DRCODMAG)
            select MGCODMAG,MGDESMAG,MGFLUBIC,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_MGUBIC = NVL(_Link_.MGFLUBIC,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_DRCODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_MGUBIC = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DRCODMAG = space(5)
        this.w_DESMAG = space(30)
        this.w_MGUBIC = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_15(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_15.MGCODMAG as MGCODMAG115"+ ",link_1_15.MGDESMAG as MGDESMAG115"+ ",link_1_15.MGFLUBIC as MGFLUBIC115"+ ",link_1_15.MGDTOBSO as MGDTOBSO115"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_15 on RILEVAZI.DRCODMAG=link_1_15.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_15"
          i_cKey=i_cKey+'+" and RILEVAZI.DRCODMAG=link_1_15.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DRUBICAZ
  func Link_1_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UBICAZIO_IDX,3]
    i_lTable = "UBICAZIO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2], .t., this.UBICAZIO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRUBICAZ) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'UBICAZIO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UBCODICE like "+cp_ToStrODBC(trim(this.w_DRUBICAZ)+"%");
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_DRCODMAG);

          i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UBCODMAG,UBCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UBCODMAG',this.w_DRCODMAG;
                     ,'UBCODICE',trim(this.w_DRUBICAZ))
          select UBCODMAG,UBCODICE,UBDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UBCODMAG,UBCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DRUBICAZ)==trim(_Link_.UBCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DRUBICAZ) and !this.bDontReportError
            deferred_cp_zoom('UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(oSource.parent,'oDRUBICAZ_1_18'),i_cWhere,'',"Ubicazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DRCODMAG<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and UBCODMAG="+cp_ToStrODBC(this.w_DRCODMAG);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',oSource.xKey(1);
                       ,'UBCODICE',oSource.xKey(2))
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRUBICAZ)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UBCODMAG,UBCODICE,UBDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where UBCODICE="+cp_ToStrODBC(this.w_DRUBICAZ);
                   +" and UBCODMAG="+cp_ToStrODBC(this.w_DRCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UBCODMAG',this.w_DRCODMAG;
                       ,'UBCODICE',this.w_DRUBICAZ)
            select UBCODMAG,UBCODICE,UBDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRUBICAZ = NVL(_Link_.UBCODICE,space(20))
      this.w_UBIDESCRI = NVL(_Link_.UBDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DRUBICAZ = space(20)
      endif
      this.w_UBIDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UBICAZIO_IDX,2])+'\'+cp_ToStr(_Link_.UBCODMAG,1)+'\'+cp_ToStr(_Link_.UBCODICE,1)
      cp_ShowWarn(i_cKey,this.UBICAZIO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRUBICAZ Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DRCODRIC
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRCODRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DRCODRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CACODVAR,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CADTOBSO,CA__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DRCODRIC))
          select CACODICE,CACODART,CACODVAR,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CADTOBSO,CA__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DRCODRIC)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DRCODRIC) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDRCODRIC_1_21'),i_cWhere,'GSMA_ACA',"Codici di ricerca",'GSMA_ZRE.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CACODVAR,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CADTOBSO,CA__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CACODART,CACODVAR,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CADTOBSO,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRCODRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CACODVAR,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CADTOBSO,CA__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DRCODRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DRCODRIC)
            select CACODICE,CACODART,CACODVAR,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CADTOBSO,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRCODRIC = NVL(_Link_.CACODICE,space(41))
      this.w_DRCODART = NVL(_Link_.CACODART,space(20))
      this.w_DRCODVAR = NVL(_Link_.CACODVAR,space(20))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_OPERAT3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_DESART = NVL(_Link_.CADESART,space(40))
      this.w_DTOBS1 = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_TIPOART = NVL(_Link_.CA__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DRCODRIC = space(41)
      endif
      this.w_DRCODART = space(20)
      this.w_DRCODVAR = space(20)
      this.w_UNMIS3 = space(3)
      this.w_OPERAT3 = space(1)
      this.w_MOLTI3 = 0
      this.w_DESART = space(40)
      this.w_DTOBS1 = ctod("  /  /  ")
      this.w_TIPOART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes= .w_TIPOART='R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DRCODRIC = space(41)
        this.w_DRCODART = space(20)
        this.w_DRCODVAR = space(20)
        this.w_UNMIS3 = space(3)
        this.w_OPERAT3 = space(1)
        this.w_MOLTI3 = 0
        this.w_DESART = space(40)
        this.w_DTOBS1 = ctod("  /  /  ")
        this.w_TIPOART = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRCODRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_21(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_21.CACODICE as CACODICE121"+ ",link_1_21.CACODART as CACODART121"+ ",link_1_21.CACODVAR as CACODVAR121"+ ",link_1_21.CAUNIMIS as CAUNIMIS121"+ ",link_1_21.CAOPERAT as CAOPERAT121"+ ",link_1_21.CAMOLTIP as CAMOLTIP121"+ ",link_1_21.CADESART as CADESART121"+ ",link_1_21.CADTOBSO as CADTOBSO121"+ ",link_1_21.CA__TIPO as CA__TIPO121"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_21 on RILEVAZI.DRCODRIC=link_1_21.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_21"
          i_cKey=i_cKey+'+" and RILEVAZI.DRCODRIC=link_1_21.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DRCODRIC
  func Link_1_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.KEY_ARTI_IDX,3]
    i_lTable = "KEY_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2], .t., this.KEY_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRCODRIC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_ACA',True,'KEY_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CACODICE like "+cp_ToStrODBC(trim(this.w_DRCODRIC)+"%");

          i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CACODVAR,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CADTOBSO,CA__TIPO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CACODICE',trim(this.w_DRCODRIC))
          select CACODICE,CACODART,CACODVAR,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CADTOBSO,CA__TIPO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DRCODRIC)==trim(_Link_.CACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DRCODRIC) and !this.bDontReportError
            deferred_cp_zoom('KEY_ARTI','*','CACODICE',cp_AbsName(oSource.parent,'oDRCODRIC_1_22'),i_cWhere,'GSMA_ACA',"Chiavi articoli",'GSMA_ZRE.KEY_ARTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CACODVAR,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CADTOBSO,CA__TIPO";
                     +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',oSource.xKey(1))
            select CACODICE,CACODART,CACODVAR,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CADTOBSO,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRCODRIC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CACODICE,CACODART,CACODVAR,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CADTOBSO,CA__TIPO";
                   +" from "+i_cTable+" "+i_lTable+" where CACODICE="+cp_ToStrODBC(this.w_DRCODRIC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CACODICE',this.w_DRCODRIC)
            select CACODICE,CACODART,CACODVAR,CAUNIMIS,CAOPERAT,CAMOLTIP,CADESART,CADTOBSO,CA__TIPO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRCODRIC = NVL(_Link_.CACODICE,space(20))
      this.w_DRCODART = NVL(_Link_.CACODART,space(20))
      this.w_DRCODVAR = NVL(_Link_.CACODVAR,space(20))
      this.w_UNMIS3 = NVL(_Link_.CAUNIMIS,space(3))
      this.w_OPERAT3 = NVL(_Link_.CAOPERAT,space(1))
      this.w_MOLTI3 = NVL(_Link_.CAMOLTIP,0)
      this.w_DESART = NVL(_Link_.CADESART,space(40))
      this.w_DTOBS1 = NVL(cp_ToDate(_Link_.CADTOBSO),ctod("  /  /  "))
      this.w_TIPOART = NVL(_Link_.CA__TIPO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DRCODRIC = space(20)
      endif
      this.w_DRCODART = space(20)
      this.w_DRCODVAR = space(20)
      this.w_UNMIS3 = space(3)
      this.w_OPERAT3 = space(1)
      this.w_MOLTI3 = 0
      this.w_DESART = space(40)
      this.w_DTOBS1 = ctod("  /  /  ")
      this.w_TIPOART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes= .w_TIPOART='R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DRCODRIC = space(20)
        this.w_DRCODART = space(20)
        this.w_DRCODVAR = space(20)
        this.w_UNMIS3 = space(3)
        this.w_OPERAT3 = space(1)
        this.w_MOLTI3 = 0
        this.w_DESART = space(40)
        this.w_DTOBS1 = ctod("  /  /  ")
        this.w_TIPOART = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.CACODICE,1)
      cp_ShowWarn(i_cKey,this.KEY_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRCODRIC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_22(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.KEY_ARTI_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.KEY_ARTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_22.CACODICE as CACODICE122"+ ",link_1_22.CACODART as CACODART122"+ ",link_1_22.CACODVAR as CACODVAR122"+ ",link_1_22.CAUNIMIS as CAUNIMIS122"+ ",link_1_22.CAOPERAT as CAOPERAT122"+ ",link_1_22.CAMOLTIP as CAMOLTIP122"+ ",link_1_22.CADESART as CADESART122"+ ",link_1_22.CADTOBSO as CADTOBSO122"+ ",link_1_22.CA__TIPO as CA__TIPO122"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_22 on RILEVAZI.DRCODRIC=link_1_22.CACODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_22"
          i_cKey=i_cKey+'+" and RILEVAZI.DRCODRIC=link_1_22.CACODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DRCODART
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARFLLOTT,ARUNMIS1,ARMOLTIP,ARUNMIS2,AROPERAT,ARGESMAT,ARFLUSEP,ARSALCOM";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_DRCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_DRCODART)
            select ARCODART,ARFLLOTT,ARUNMIS1,ARMOLTIP,ARUNMIS2,AROPERAT,ARGESMAT,ARFLUSEP,ARSALCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRCODART = NVL(_Link_.ARCODART,space(20))
      this.w_FLLOTT = NVL(_Link_.ARFLLOTT,space(1))
      this.w_UNMIS1 = NVL(_Link_.ARUNMIS1,space(3))
      this.w_MOLTIP = NVL(_Link_.ARMOLTIP,0)
      this.w_UNMIS2 = NVL(_Link_.ARUNMIS2,space(3))
      this.w_OPERAT = NVL(_Link_.AROPERAT,space(1))
      this.w_GESMAT = NVL(_Link_.ARGESMAT,space(1))
      this.w_FLUSEP = NVL(_Link_.ARFLUSEP,space(1))
      this.w_FLCOMM = NVL(_Link_.ARSALCOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DRCODART = space(20)
      endif
      this.w_FLLOTT = space(1)
      this.w_UNMIS1 = space(3)
      this.w_MOLTIP = 0
      this.w_UNMIS2 = space(3)
      this.w_OPERAT = space(1)
      this.w_GESMAT = space(1)
      this.w_FLUSEP = space(1)
      this.w_FLCOMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_23(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 9 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+9<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_23.ARCODART as ARCODART123"+ ",link_1_23.ARFLLOTT as ARFLLOTT123"+ ",link_1_23.ARUNMIS1 as ARUNMIS1123"+ ",link_1_23.ARMOLTIP as ARMOLTIP123"+ ",link_1_23.ARUNMIS2 as ARUNMIS2123"+ ",link_1_23.AROPERAT as AROPERAT123"+ ",link_1_23.ARGESMAT as ARGESMAT123"+ ",link_1_23.ARFLUSEP as ARFLUSEP123"+ ",link_1_23.ARSALCOM as ARSALCOM123"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_23 on RILEVAZI.DRCODART=link_1_23.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_23"
          i_cKey=i_cKey+'+" and RILEVAZI.DRCODART=link_1_23.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+9
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=UNMIS1
  func Link_1_26(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_UNMIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_UNMIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ,UMMODUM2";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_UNMIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_UNMIS1)
            select UMCODICE,UMFLFRAZ,UMMODUM2;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_UNMIS1 = NVL(_Link_.UMCODICE,space(3))
      this.w_NOFRAZ1 = NVL(_Link_.UMFLFRAZ,space(1))
      this.w_MODUM2 = NVL(_Link_.UMMODUM2,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_UNMIS1 = space(3)
      endif
      this.w_NOFRAZ1 = space(1)
      this.w_MODUM2 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_UNMIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DR_LOTTO
  func Link_1_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LOTTIART_IDX,3]
    i_lTable = "LOTTIART"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2], .t., this.LOTTIART_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DR_LOTTO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LOTTIART')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LOCODICE like "+cp_ToStrODBC(trim(this.w_DR_LOTTO)+"%");
                   +" and LOCODART="+cp_ToStrODBC(this.w_DRCODART);

          i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATCRE,LODATSCA,LOCODVAR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LOCODART,LOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LOCODART',this.w_DRCODART;
                     ,'LOCODICE',trim(this.w_DR_LOTTO))
          select LOCODART,LOCODICE,LODATCRE,LODATSCA,LOCODVAR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LOCODART,LOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DR_LOTTO)==trim(_Link_.LOCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DR_LOTTO) and !this.bDontReportError
            deferred_cp_zoom('LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(oSource.parent,'oDR_LOTTO_1_31'),i_cWhere,'',"Lotti",'GSMA_ZLA.LOTTIART_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DRCODART<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATCRE,LODATSCA,LOCODVAR";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select LOCODART,LOCODICE,LODATCRE,LODATSCA,LOCODVAR;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATCRE,LODATSCA,LOCODVAR";
                     +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and LOCODART="+cp_ToStrODBC(this.w_DRCODART);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',oSource.xKey(1);
                       ,'LOCODICE',oSource.xKey(2))
            select LOCODART,LOCODICE,LODATCRE,LODATSCA,LOCODVAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DR_LOTTO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LOCODART,LOCODICE,LODATCRE,LODATSCA,LOCODVAR";
                   +" from "+i_cTable+" "+i_lTable+" where LOCODICE="+cp_ToStrODBC(this.w_DR_LOTTO);
                   +" and LOCODART="+cp_ToStrODBC(this.w_DRCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LOCODART',this.w_DRCODART;
                       ,'LOCODICE',this.w_DR_LOTTO)
            select LOCODART,LOCODICE,LODATCRE,LODATSCA,LOCODVAR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DR_LOTTO = NVL(_Link_.LOCODICE,space(20))
      this.w_DATCRE = NVL(cp_ToDate(_Link_.LODATCRE),ctod("  /  /  "))
      this.w_DATSCA = NVL(cp_ToDate(_Link_.LODATSCA),ctod("  /  /  "))
      this.w_LCODART = NVL(_Link_.LOCODART,space(20))
      this.w_LCODVAR = NVL(_Link_.LOCODVAR,space(20))
    else
      if i_cCtrl<>'Load'
        this.w_DR_LOTTO = space(20)
      endif
      this.w_DATCRE = ctod("  /  /  ")
      this.w_DATSCA = ctod("  /  /  ")
      this.w_LCODART = space(20)
      this.w_LCODVAR = space(20)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DRCODART=.w_LCODART and iif( g_APPLICATION="ADHOC REVOLUTION" ,.t., .w_DRCODVAR=.w_LCODVAR  )
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_DR_LOTTO = space(20)
        this.w_DATCRE = ctod("  /  /  ")
        this.w_DATSCA = ctod("  /  /  ")
        this.w_LCODART = space(20)
        this.w_LCODVAR = space(20)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LOTTIART_IDX,2])+'\'+cp_ToStr(_Link_.LOCODART,1)+'\'+cp_ToStr(_Link_.LOCODICE,1)
      cp_ShowWarn(i_cKey,this.LOTTIART_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DR_LOTTO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DRCODMAT
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MATRICOL_IDX,3]
    i_lTable = "MATRICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2], .t., this.MATRICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRCODMAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSVE_AMT',True,'MATRICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AMCODICE like "+cp_ToStrODBC(trim(this.w_DRCODMAT)+"%");
                   +" and AMKEYSAL="+cp_ToStrODBC(this.w_DRKEYSAL);

          i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AMKEYSAL,AMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AMKEYSAL',this.w_DRKEYSAL;
                     ,'AMCODICE',trim(this.w_DRCODMAT))
          select AMKEYSAL,AMCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AMKEYSAL,AMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DRCODMAT)==trim(_Link_.AMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DRCODMAT) and !this.bDontReportError
            deferred_cp_zoom('MATRICOL','*','AMKEYSAL,AMCODICE',cp_AbsName(oSource.parent,'oDRCODMAT_1_35'),i_cWhere,'GSVE_AMT',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_DRKEYSAL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select AMKEYSAL,AMCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and AMKEYSAL="+cp_ToStrODBC(this.w_DRKEYSAL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMKEYSAL',oSource.xKey(1);
                       ,'AMCODICE',oSource.xKey(2))
            select AMKEYSAL,AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRCODMAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AMKEYSAL,AMCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where AMCODICE="+cp_ToStrODBC(this.w_DRCODMAT);
                   +" and AMKEYSAL="+cp_ToStrODBC(this.w_DRKEYSAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AMKEYSAL',this.w_DRKEYSAL;
                       ,'AMCODICE',this.w_DRCODMAT)
            select AMKEYSAL,AMCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRCODMAT = NVL(_Link_.AMCODICE,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DRCODMAT = space(40)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MATRICOL_IDX,2])+'\'+cp_ToStr(_Link_.AMKEYSAL,1)+'\'+cp_ToStr(_Link_.AMCODICE,1)
      cp_ShowWarn(i_cKey,this.MATRICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRCODMAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DRCODCOM
  func Link_1_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRCODCOM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_DRCODCOM)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_DRCODCOM))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DRCODCOM)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DRCODCOM) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oDRCODCOM_1_36'),i_cWhere,'GSAR_ACN',"Commessa",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRCODCOM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_DRCODCOM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_DRCODCOM)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRCODCOM = NVL(_Link_.CNCODCAN,space(15))
      this.w_DRDESCOM = NVL(_Link_.CNDESCAN,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_DRCODCOM = space(15)
      endif
      this.w_DRDESCOM = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRCODCOM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_36(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_36.CNCODCAN as CNCODCAN136"+ ",link_1_36.CNDESCAN as CNDESCAN136"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_36 on RILEVAZI.DRCODCOM=link_1_36.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_36"
          i_cKey=i_cKey+'+" and RILEVAZI.DRCODCOM=link_1_36.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=DRUNIMIS
  func Link_1_38(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.UNIMIS_IDX,3]
    i_lTable = "UNIMIS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2], .t., this.UNIMIS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DRUNIMIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AUM',True,'UNIMIS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" UMCODICE like "+cp_ToStrODBC(trim(this.w_DRUNIMIS)+"%");

          i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by UMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'UMCODICE',trim(this.w_DRUNIMIS))
          select UMCODICE,UMFLFRAZ;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by UMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_DRUNIMIS)==trim(_Link_.UMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_DRUNIMIS) and !this.bDontReportError
            deferred_cp_zoom('UNIMIS','*','UMCODICE',cp_AbsName(oSource.parent,'oDRUNIMIS_1_38'),i_cWhere,'GSAR_AUM',"U.M.",'GSMAUMVM.UNIMIS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                     +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',oSource.xKey(1))
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DRUNIMIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select UMCODICE,UMFLFRAZ";
                   +" from "+i_cTable+" "+i_lTable+" where UMCODICE="+cp_ToStrODBC(this.w_DRUNIMIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'UMCODICE',this.w_DRUNIMIS)
            select UMCODICE,UMFLFRAZ;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DRUNIMIS = NVL(_Link_.UMCODICE,space(3))
      this.w_FLFRAZ = NVL(_Link_.UMFLFRAZ,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_DRUNIMIS = space(3)
      endif
      this.w_FLFRAZ = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKUNIMI(.w_DRUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Unit� di misura inesistente o incongruente")
        endif
        this.w_DRUNIMIS = space(3)
        this.w_FLFRAZ = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])+'\'+cp_ToStr(_Link_.UMCODICE,1)
      cp_ShowWarn(i_cKey,this.UNIMIS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DRUNIMIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_38(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.UNIMIS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.UNIMIS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_38.UMCODICE as UMCODICE138"+ ",link_1_38.UMFLFRAZ as UMFLFRAZ138"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_38 on RILEVAZI.DRUNIMIS=link_1_38.UMCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_38"
          i_cKey=i_cKey+'+" and RILEVAZI.DRUNIMIS=link_1_38.UMCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDRCODRIL_1_2.value==this.w_DRCODRIL)
      this.oPgFrm.Page1.oPag.oDRCODRIL_1_2.value=this.w_DRCODRIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDATRIL_1_3.value==this.w_DATRIL)
      this.oPgFrm.Page1.oPag.oDATRIL_1_3.value=this.w_DATRIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDRNUMDOC_1_5.value==this.w_DRNUMDOC)
      this.oPgFrm.Page1.oPag.oDRNUMDOC_1_5.value=this.w_DRNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDRADDRIL_1_13.value==this.w_DRADDRIL)
      this.oPgFrm.Page1.oPag.oDRADDRIL_1_13.value=this.w_DRADDRIL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESADD_1_14.value==this.w_DESADD)
      this.oPgFrm.Page1.oPag.oDESADD_1_14.value=this.w_DESADD
    endif
    if not(this.oPgFrm.Page1.oPag.oDRCODMAG_1_15.value==this.w_DRCODMAG)
      this.oPgFrm.Page1.oPag.oDRCODMAG_1_15.value=this.w_DRCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_16.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_16.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDRUBICAZ_1_18.value==this.w_DRUBICAZ)
      this.oPgFrm.Page1.oPag.oDRUBICAZ_1_18.value=this.w_DRUBICAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oUBIDESCRI_1_20.value==this.w_UBIDESCRI)
      this.oPgFrm.Page1.oPag.oUBIDESCRI_1_20.value=this.w_UBIDESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oDRCODRIC_1_21.value==this.w_DRCODRIC)
      this.oPgFrm.Page1.oPag.oDRCODRIC_1_21.value=this.w_DRCODRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDRCODRIC_1_22.value==this.w_DRCODRIC)
      this.oPgFrm.Page1.oPag.oDRCODRIC_1_22.value=this.w_DRCODRIC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_30.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_30.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oDR_LOTTO_1_31.value==this.w_DR_LOTTO)
      this.oPgFrm.Page1.oPag.oDR_LOTTO_1_31.value=this.w_DR_LOTTO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATCRE_1_32.value==this.w_DATCRE)
      this.oPgFrm.Page1.oPag.oDATCRE_1_32.value=this.w_DATCRE
    endif
    if not(this.oPgFrm.Page1.oPag.oDATSCA_1_34.value==this.w_DATSCA)
      this.oPgFrm.Page1.oPag.oDATSCA_1_34.value=this.w_DATSCA
    endif
    if not(this.oPgFrm.Page1.oPag.oDRCODMAT_1_35.value==this.w_DRCODMAT)
      this.oPgFrm.Page1.oPag.oDRCODMAT_1_35.value=this.w_DRCODMAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDRCODCOM_1_36.value==this.w_DRCODCOM)
      this.oPgFrm.Page1.oPag.oDRCODCOM_1_36.value=this.w_DRCODCOM
    endif
    if not(this.oPgFrm.Page1.oPag.oDRUNIMIS_1_38.value==this.w_DRUNIMIS)
      this.oPgFrm.Page1.oPag.oDRUNIMIS_1_38.value=this.w_DRUNIMIS
    endif
    if not(this.oPgFrm.Page1.oPag.oDRQTAESI_1_39.value==this.w_DRQTAESI)
      this.oPgFrm.Page1.oPag.oDRQTAESI_1_39.value=this.w_DRQTAESI
    endif
    if not(this.oPgFrm.Page1.oPag.oDRDATMOV_1_66.value==this.w_DRDATMOV)
      this.oPgFrm.Page1.oPag.oDRDATMOV_1_66.value=this.w_DRDATMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oDRREGMOV_1_76.value==this.w_DRREGMOV)
      this.oPgFrm.Page1.oPag.oDRREGMOV_1_76.value=this.w_DRREGMOV
    endif
    if not(this.oPgFrm.Page1.oPag.oDRREGMOVAPP_1_77.value==this.w_DRREGMOVAPP)
      this.oPgFrm.Page1.oPag.oDRREGMOVAPP_1_77.value=this.w_DRREGMOVAPP
    endif
    if not(this.oPgFrm.Page1.oPag.oDRDATMOVAPP_1_78.value==this.w_DRDATMOVAPP)
      this.oPgFrm.Page1.oPag.oDRDATMOVAPP_1_78.value=this.w_DRDATMOVAPP
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_79.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_79.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oDRDESCOM_1_85.value==this.w_DRDESCOM)
      this.oPgFrm.Page1.oPag.oDRDESCOM_1_85.value=this.w_DRDESCOM
    endif
    cp_SetControlsValueExtFlds(this,'RILEVAZI')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DRCODRIL))  and (.w_DRCONFER<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDRCODRIL_1_2.SetFocus()
            i_bnoObbl = !empty(.w_DRCODRIL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DRADDRIL))  and (.w_DRCONFER<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDRADDRIL_1_13.SetFocus()
            i_bnoObbl = !empty(.w_DRADDRIL)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and (.w_DRCONFER<>'S')  and not(empty(.w_DRCODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDRCODMAG_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not( .w_TIPOART='R')  and not(g_APPLICATION="ADHOC REVOLUTION")  and (.w_DRCONFER<>'S' and  Empty(.w_DRCODMAT))  and not(empty(.w_DRCODRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDRCODRIC_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not( .w_TIPOART='R')  and not(g_APPLICATION="ad hoc ENTERPRISE")  and (.w_DRCONFER<>'S' and  Empty(.w_DRCODMAT))  and not(empty(.w_DRCODRIC))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDRCODRIC_1_22.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_DRCODART=.w_LCODART and iif( g_APPLICATION="ADHOC REVOLUTION" ,.t., .w_DRCODVAR=.w_LCODVAR  ))  and (g_PERLOT='S' AND((.w_FLLOTT = 'S'  And g_APPLICATION <> "ADHOC REVOLUTION") Or (.w_FLLOTT $ 'S-C'  And g_APPLICATION = "ADHOC REVOLUTION") )  AND .w_DRCONFER<>'S' AND NOT EMPTY(.w_DRCODRIC))  and not(empty(.w_DR_LOTTO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDR_LOTTO_1_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_DRUNIMIS)) or not(CHKUNIMI(.w_DRUNIMIS, .w_UNMIS1, .w_UNMIS2, .w_UNMIS3)))  and (NOT EMPTY(.w_DRCODRIC)  AND .w_DRCONFER<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDRUNIMIS_1_38.SetFocus()
            i_bnoObbl = !empty(.w_DRUNIMIS)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Unit� di misura inesistente o incongruente")
          case   not((Empty(.w_DRCODMAT) AND .w_DRQTAESI >=0  AND (.w_FLFRAZ<>'S' OR .w_DRQTAESI=INT(.w_DRQTAESI)) OR !Empty(.w_DRCODMAT) AND (.w_DRQTAESI =1 OR .w_DRQTAESI=0)) AND CALQTA(.w_DRQTAESI, .w_DRUNIMIS ,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_NOFRAZ1, .w_MODUM2,  '', .w_UNMIS3, .w_OPERAT3, .w_MOLTI3)>=0)  and (.w_DRCONFER<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDRQTAESI_1_39.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Quantit� rilevata negativa o articolo gestito a matricole e quantit� maggiore di uno")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsma_adr
      if i_bRes=.t.
         * --- Controlli Univocita' Documento
         .w_RESCHK=0
          Ah_Msg('Controllo univocit�...',.T.)
           .NotifyEvent('Controllo')
           WAIT CLEAR
           if .w_RESCHK<>0
              i_bRes=.f.
           endif
      endif
      
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DRCODMAG = this.w_DRCODMAG
    this.o_DRUBICAZ = this.w_DRUBICAZ
    this.o_COARTVAR = this.w_COARTVAR
    this.o_DRCODART = this.w_DRCODART
    this.o_DRCODVAR = this.w_DRCODVAR
    this.o_UNMIS1 = this.w_UNMIS1
    this.o_DR_LOTTO = this.w_DR_LOTTO
    this.o_DRCODMAT = this.w_DRCODMAT
    this.o_DRUNIMIS = this.w_DRUNIMIS
    this.o_DRQTAESI = this.w_DRQTAESI
    this.o_UNMIS3 = this.w_UNMIS3
    this.o_MOLTI3 = this.w_MOLTI3
    return

  func CanEdit()
    local i_res
    i_res=this.w_DRCONFER<>'S'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile modificare. Dichiarazione gi� processata"))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=this.w_DRCONFER<>'S'
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Impossibile cancellare. Dichiarazione gi� processata"))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgsma_adrPag1 as StdContainer
  Width  = 639
  height = 327
  stdWidth  = 639
  stdheight = 327
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDRCODRIL_1_2 as StdField with uid="MCDNGXONFK",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DRCODRIL", cQueryName = "DRCODRIL",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice della rilevazione",;
    HelpContextID = 17369986,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=101, Top=15, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="CODIRILE", cZoomOnZoom="GSMA_ACR", oKey_1_1="RICODICE", oKey_1_2="this.w_DRCODRIL"

  func oDRCODRIL_1_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DRCONFER<>'S')
    endwith
   endif
  endfunc

  func oDRCODRIL_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oDRCODRIL_1_2.ecpDrop(oSource)
    this.Parent.oContained.link_1_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDRCODRIL_1_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CODIRILE','*','RICODICE',cp_AbsName(this.parent,'oDRCODRIL_1_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACR',"Rilevazioni",'',this.parent.oContained
  endproc
  proc oDRCODRIL_1_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_RICODICE=this.parent.oContained.w_DRCODRIL
     i_obj.ecpSave()
  endproc

  add object oDATRIL_1_3 as StdField with uid="RGHTAVIHLB",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATRIL", cQueryName = "DATRIL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 77788618,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=224, Top=15

  add object oDRNUMDOC_1_5 as StdField with uid="FODCKMJNYF",rtseq=5,rtrep=.f.,;
    cFormVar = "w_DRNUMDOC", cQueryName = "DRNUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero del documento",;
    HelpContextID = 207635591,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=413, Top=15, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  func oDRNUMDOC_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DRCONFER<>'S')
    endwith
   endif
  endfunc

  add object oDRADDRIL_1_13 as StdField with uid="DJUEZLTMHZ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DRADDRIL", cQueryName = "DRADDRIL",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Utente che ha effettuato la rilevazione",;
    HelpContextID = 16640898,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=155, Top=63, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ADDERILE", cZoomOnZoom="GSMA_ADD", oKey_1_1="ARCODICE", oKey_1_2="this.w_DRADDRIL"

  func oDRADDRIL_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DRCONFER<>'S')
    endwith
   endif
  endfunc

  func oDRADDRIL_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oDRADDRIL_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDRADDRIL_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ADDERILE','*','ARCODICE',cp_AbsName(this.parent,'oDRADDRIL_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ADD',"Addetti rilevazione",'',this.parent.oContained
  endproc
  proc oDRADDRIL_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ADD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODICE=this.parent.oContained.w_DRADDRIL
     i_obj.ecpSave()
  endproc

  add object oDESADD_1_14 as StdField with uid="QLWZOARRIY",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESADD", cQueryName = "DESADD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 218366410,;
   bGlobalFont=.t.,;
    Height=21, Width=279, Left=219, Top=63, InputMask=replicate('X',40)

  add object oDRCODMAG_1_15 as StdField with uid="ARRXSEYAAW",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DRCODMAG", cQueryName = "DRCODRIL,DRCODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino da considerare",;
    HelpContextID = 66516099,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=155, Top=96, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_DRCODMAG"

  func oDRCODMAG_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DRCONFER<>'S')
    endwith
   endif
  endfunc

  func oDRCODMAG_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
      if .not. empty(.w_DRUBICAZ)
        bRes2=.link_1_18('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oDRCODMAG_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDRCODMAG_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oDRCODMAG_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oDRCODMAG_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_DRCODMAG
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_16 as StdField with uid="YHBTKYGEPO",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 170394058,;
   bGlobalFont=.t.,;
    Height=21, Width=223, Left=219, Top=96, InputMask=replicate('X',30)

  add object oDRUBICAZ_1_18 as StdField with uid="BUYWLNZMIZ",rtseq=13,rtrep=.t.,;
    cFormVar = "w_DRUBICAZ", cQueryName = "DRUBICAZ",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Ubicazione da considerare",;
    HelpContextID = 229823600,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=155, Top=129, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="UBICAZIO", oKey_1_1="UBCODMAG", oKey_1_2="this.w_DRCODMAG", oKey_2_1="UBCODICE", oKey_2_2="this.w_DRUBICAZ"

  func oDRUBICAZ_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERUBI='S' AND .w_MGUBIC='S'  AND .w_DRCONFER<>'S')
    endwith
   endif
  endfunc

  func oDRUBICAZ_1_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oDRUBICAZ_1_18.ecpDrop(oSource)
    this.Parent.oContained.link_1_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDRUBICAZ_1_18.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.UBICAZIO_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStrODBC(this.Parent.oContained.w_DRCODMAG)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"UBCODMAG="+cp_ToStr(this.Parent.oContained.w_DRCODMAG)
    endif
    do cp_zoom with 'UBICAZIO','*','UBCODMAG,UBCODICE',cp_AbsName(this.parent,'oDRUBICAZ_1_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Ubicazioni",'',this.parent.oContained
  endproc

  add object oUBIDESCRI_1_20 as StdField with uid="LJUMIDECTY",rtseq=15,rtrep=.f.,;
    cFormVar = "w_UBIDESCRI", cQueryName = "UBIDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 34496808,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=323, Top=129, InputMask=replicate('X',40)

  add object oDRCODRIC_1_21 as StdField with uid="VBHPCGPVIB",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DRCODRIC", cQueryName = "DRCODRIC",;
    bObbl = .f. , nPag = 1, value=space(41), bMultilanguage =  .f.,;
    ToolTipText = "Articolo da considerare",;
    HelpContextID = 17369977,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=155, Top=163, InputMask=replicate('X',41), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_DRCODRIC"

  func oDRCODRIC_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DRCONFER<>'S' and  Empty(.w_DRCODMAT))
    endwith
   endif
  endfunc

  func oDRCODRIC_1_21.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc

  func oDRCODRIC_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oDRCODRIC_1_21.ecpDrop(oSource)
    this.Parent.oContained.link_1_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDRCODRIC_1_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDRCODRIC_1_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Codici di ricerca",'GSMA_ZRE.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oDRCODRIC_1_21.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_DRCODRIC
     i_obj.ecpSave()
  endproc

  add object oDRCODRIC_1_22 as StdField with uid="ZBCTFSOZMX",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DRCODRIC", cQueryName = "DRCODRIC",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Articolo da considerare",;
    HelpContextID = 17369977,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=155, Top=163, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="KEY_ARTI", cZoomOnZoom="GSMA_ACA", oKey_1_1="CACODICE", oKey_1_2="this.w_DRCODRIC"

  func oDRCODRIC_1_22.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DRCONFER<>'S' and  Empty(.w_DRCODMAT))
    endwith
   endif
  endfunc

  func oDRCODRIC_1_22.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ad hoc ENTERPRISE")
    endwith
  endfunc

  func oDRCODRIC_1_22.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_22('Part',this)
    endwith
    return bRes
  endfunc

  proc oDRCODRIC_1_22.ecpDrop(oSource)
    this.Parent.oContained.link_1_22('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDRCODRIC_1_22.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'KEY_ARTI','*','CACODICE',cp_AbsName(this.parent,'oDRCODRIC_1_22'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_ACA',"Chiavi articoli",'GSMA_ZRE.KEY_ARTI_VZM',this.parent.oContained
  endproc
  proc oDRCODRIC_1_22.mZoomOnZoom
    local i_obj
    i_obj=GSMA_ACA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CACODICE=this.parent.oContained.w_DRCODRIC
     i_obj.ecpSave()
  endproc

  add object oDESART_1_30 as StdField with uid="USNJFNNUQJ",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 203686346,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=323, Top=163, InputMask=replicate('X',40)

  func oDESART_1_30.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ADHOC REVOLUTION")
    endwith
  endfunc

  add object oDR_LOTTO_1_31 as StdField with uid="BRUTZBOTDU",rtseq=26,rtrep=.t.,;
    cFormVar = "w_DR_LOTTO", cQueryName = "DR_LOTTO",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Lotto da considerare",;
    HelpContextID = 62376837,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=155, Top=197, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="LOTTIART", oKey_1_1="LOCODART", oKey_1_2="this.w_DRCODART", oKey_2_1="LOCODICE", oKey_2_2="this.w_DR_LOTTO"

  func oDR_LOTTO_1_31.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (g_PERLOT='S' AND((.w_FLLOTT = 'S'  And g_APPLICATION <> "ADHOC REVOLUTION") Or (.w_FLLOTT $ 'S-C'  And g_APPLICATION = "ADHOC REVOLUTION") )  AND .w_DRCONFER<>'S' AND NOT EMPTY(.w_DRCODRIC))
    endwith
   endif
  endfunc

  func oDR_LOTTO_1_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oDR_LOTTO_1_31.ecpDrop(oSource)
    this.Parent.oContained.link_1_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDR_LOTTO_1_31.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.LOTTIART_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStrODBC(this.Parent.oContained.w_DRCODART)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"LOCODART="+cp_ToStr(this.Parent.oContained.w_DRCODART)
    endif
    do cp_zoom with 'LOTTIART','*','LOCODART,LOCODICE',cp_AbsName(this.parent,'oDR_LOTTO_1_31'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Lotti",'GSMA_ZLA.LOTTIART_VZM',this.parent.oContained
  endproc

  add object oDATCRE_1_32 as StdField with uid="ZBYEZPOFCF",rtseq=27,rtrep=.f.,;
    cFormVar = "w_DATCRE", cQueryName = "DATCRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di creazione del lotto",;
    HelpContextID = 186774986,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=382, Top=197

  add object oDATSCA_1_34 as StdField with uid="CDREFMGEJN",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DATSCA", cQueryName = "DATSCA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di scadenza del lotto",;
    HelpContextID = 128458,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=541, Top=197

  add object oDRCODMAT_1_35 as StdField with uid="CDJWXWVUNF",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DRCODMAT", cQueryName = "DRCODMAT",;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 66516086,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=155, Top=232, InputMask=replicate('X',40), bHasZoom = .t. , cLinkFile="MATRICOL", cZoomOnZoom="GSVE_AMT", oKey_1_1="AMKEYSAL", oKey_1_2="this.w_DRKEYSAL", oKey_2_1="AMCODICE", oKey_2_2="this.w_DRCODMAT"

  func oDRCODMAT_1_35.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((Empty(.w_DRCODRIC) or .w_GESMAT='S') and g_MATR='S')
    endwith
   endif
  endfunc

  func oDRCODMAT_1_35.mHide()
    with this.Parent.oContained
      return (g_MATR<>'S' Or .w_DATRIL<Nvl(g_DATMAT,cp_CharToDate('  /  /    ')))
    endwith
  endfunc

  func oDRCODMAT_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_35('Part',this)
    endwith
    return bRes
  endfunc

  proc oDRCODMAT_1_35.ecpDrop(oSource)
    this.Parent.oContained.link_1_35('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDRCODMAT_1_35.mZoom
      with this.Parent.oContained
        GSMA_BRL(this.Parent.oContained,"O")
      endwith
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc
  proc oDRCODMAT_1_35.mZoomOnZoom
    local i_obj
    i_obj=GSVE_AMT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.AMKEYSAL=w_DRKEYSAL
     i_obj.w_AMCODICE=this.parent.oContained.w_DRCODMAT
     i_obj.ecpSave()
  endproc

  add object oDRCODCOM_1_36 as StdField with uid="PQDMRFARSN",rtseq=31,rtrep=.f.,;
    cFormVar = "w_DRCODCOM", cQueryName = "DRCODCOM",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Commessa da considerare",;
    HelpContextID = 234288253,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=155, Top=267, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_DRCODCOM"

  func oDRCODCOM_1_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return ((Empty(.w_DRCODRIC) or .w_FLCOMM='S'))
    endwith
   endif
  endfunc

  func oDRCODCOM_1_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_36('Part',this)
    endwith
    return bRes
  endfunc

  proc oDRCODCOM_1_36.ecpDrop(oSource)
    this.Parent.oContained.link_1_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDRCODCOM_1_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oDRCODCOM_1_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commessa",'',this.parent.oContained
  endproc
  proc oDRCODCOM_1_36.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_DRCODCOM
     i_obj.ecpSave()
  endproc

  add object oDRUNIMIS_1_38 as StdField with uid="VAWWEIQYKW",rtseq=33,rtrep=.t.,;
    cFormVar = "w_DRUNIMIS", cQueryName = "DRUNIMIS",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Unit� di misura inesistente o incongruente",;
    ToolTipText = "Unit� di misura",;
    HelpContextID = 207170441,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=155, Top=302, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="UNIMIS", cZoomOnZoom="GSAR_AUM", oKey_1_1="UMCODICE", oKey_1_2="this.w_DRUNIMIS"

  func oDRUNIMIS_1_38.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_DRCODRIC)  AND .w_DRCONFER<>'S')
    endwith
   endif
  endfunc

  func oDRUNIMIS_1_38.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_38('Part',this)
    endwith
    return bRes
  endfunc

  proc oDRUNIMIS_1_38.ecpDrop(oSource)
    this.Parent.oContained.link_1_38('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oDRUNIMIS_1_38.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'UNIMIS','*','UMCODICE',cp_AbsName(this.parent,'oDRUNIMIS_1_38'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AUM',"U.M.",'GSMAUMVM.UNIMIS_VZM',this.parent.oContained
  endproc
  proc oDRUNIMIS_1_38.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AUM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_UMCODICE=this.parent.oContained.w_DRUNIMIS
     i_obj.ecpSave()
  endproc

  add object oDRQTAESI_1_39 as StdField with uid="ZAVVNHTCZU",rtseq=34,rtrep=.t.,;
    cFormVar = "w_DRQTAESI", cQueryName = "DRQTAESI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Quantit� rilevata negativa o articolo gestito a matricole e quantit� maggiore di uno",;
    ToolTipText = "Quantit� rilevata",;
    HelpContextID = 64940927,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=307, Top=302, cSayPict="v_PQ(12)", cGetPict="v_GQ(12)"

  func oDRQTAESI_1_39.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DRCONFER<>'S')
    endwith
   endif
  endfunc

  func oDRQTAESI_1_39.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((Empty(.w_DRCODMAT) AND .w_DRQTAESI >=0  AND (.w_FLFRAZ<>'S' OR .w_DRQTAESI=INT(.w_DRQTAESI)) OR !Empty(.w_DRCODMAT) AND (.w_DRQTAESI =1 OR .w_DRQTAESI=0)) AND CALQTA(.w_DRQTAESI, .w_DRUNIMIS ,.w_UNMIS2,.w_OPERAT, .w_MOLTIP, .w_FLUSEP, .w_NOFRAZ1, .w_MODUM2,  '', .w_UNMIS3, .w_OPERAT3, .w_MOLTI3)>=0)
    endwith
    return bRes
  endfunc


  add object oObj_1_61 as cp_runprogram with uid="LUZDKCVJLL",left=3, top=342, width=185,height=19,;
    caption='GSMA_BRL(A)',;
   bGlobalFont=.t.,;
    prg="GSMA_BRL('A')",;
    cEvent = "New record",;
    nPag=1;
    , HelpContextID = 223444942


  add object oObj_1_63 as cp_runprogram with uid="XJLVLXFNVA",left=193, top=342, width=185,height=19,;
    caption='GSMA_BRL(U)',;
   bGlobalFont=.t.,;
    prg="GSMA_BRL('U')",;
    cEvent = "w_DRADDRIL Changed",;
    nPag=1;
    , HelpContextID = 223439822


  add object oObj_1_64 as cp_runprogram with uid="WEWQHTBIVW",left=3, top=362, width=185,height=19,;
    caption='GSMA_BRL(N)',;
   bGlobalFont=.t.,;
    prg="GSMA_BRL('N')",;
    cEvent = "Controllo",;
    nPag=1;
    , HelpContextID = 223441614

  add object oDRDATMOV_1_66 as StdField with uid="YYVLTZOZWC",rtseq=51,rtrep=.f.,;
    cFormVar = "w_DRDATMOV", cQueryName = "DRDATMOV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del movimento di magazzino collegato",;
    HelpContextID = 50652276,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=542, Top=96

  func oDRDATMOV_1_66.mHide()
    with this.Parent.oContained
      return (Empty(.w_DRSERMOV))
    endwith
  endfunc


  add object oBtn_1_71 as StdButton with uid="SPNYJFPGNH",left=610, top=67, width=20,height=18,;
    caption="...", nPag=1;
    , ToolTipText = "Premere per aprire la gestione";
    , HelpContextID = 92760618;
  , bGlobalFont=.t.

    proc oBtn_1_71.Click()
      with this.Parent.oContained
        GSMA_BRL(this.Parent.oContained,"M")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_71.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty(.w_DRSERMOV))
      endwith
    endif
  endfunc

  func oBtn_1_71.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (Empty(.w_DRSERMOV))
     endwith
    endif
  endfunc


  add object oObj_1_74 as cp_runprogram with uid="JPKVDZHHAR",left=193, top=362, width=185,height=19,;
    caption='GSMA_BRL(S)',;
   bGlobalFont=.t.,;
    prg="GSMA_BRL('S')",;
    cEvent = "w_DRCODRIC Changed",;
    nPag=1;
    , HelpContextID = 223440334

  add object oDRREGMOV_1_76 as StdField with uid="TKDMCMAOGN",rtseq=55,rtrep=.f.,;
    cFormVar = "w_DRREGMOV", cQueryName = "DRREGMOV",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione movimento di magazzino collegato",;
    HelpContextID = 63964276,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=542, Top=63

  func oDRREGMOV_1_76.mHide()
    with this.Parent.oContained
      return (Empty(.w_DRSERMOV))
    endwith
  endfunc

  add object oDRREGMOVAPP_1_77 as StdField with uid="QVTMKFZAKM",rtseq=56,rtrep=.f.,;
    cFormVar = "w_DRREGMOVAPP", cQueryName = "DRREGMOVAPP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione movimento di magazzino collegato",;
    HelpContextID = 63615076,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=542, Top=63

  func oDRREGMOVAPP_1_77.mHide()
    with this.Parent.oContained
      return (Empty(.w_DRSERMOV))
    endwith
  endfunc

  add object oDRDATMOVAPP_1_78 as StdField with uid="NCOJWAXKOA",rtseq=57,rtrep=.f.,;
    cFormVar = "w_DRDATMOVAPP", cQueryName = "DRDATMOVAPP",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data del movimento di magazzino collegato",;
    HelpContextID = 50303076,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=542, Top=96

  func oDRDATMOVAPP_1_78.mHide()
    with this.Parent.oContained
      return (Empty(.w_DRSERMOV))
    endwith
  endfunc

  add object oDESART_1_79 as StdField with uid="HURVBRHLGD",rtseq=58,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 203686346,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=323, Top=163, InputMask=replicate('X',40)

  func oDESART_1_79.mHide()
    with this.Parent.oContained
      return (g_APPLICATION="ad hoc ENTERPRISE")
    endwith
  endfunc

  add object oDRDESCOM_1_85 as StdField with uid="SEOFJRQSEN",rtseq=62,rtrep=.f.,;
    cFormVar = "w_DRDESCOM", cQueryName = "DRDESCOM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 219210877,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=277, Top=267, InputMask=replicate('X',40)

  add object oStr_1_6 as StdString with uid="ZAQIFHCIAM",Visible=.t., Left=313, Top=15,;
    Alignment=1, Width=97, Height=18,;
    Caption="Num.documento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="WKKMJRAYGU",Visible=.t., Left=7, Top=15,;
    Alignment=1, Width=89, Height=18,;
    Caption="Rilevazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="FJDCHWSXTA",Visible=.t., Left=189, Top=15,;
    Alignment=1, Width=32, Height=18,;
    Caption="del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="DCEPLXOSLV",Visible=.t., Left=12, Top=62,;
    Alignment=1, Width=140, Height=18,;
    Caption="Addetto alla rilev.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_17 as StdString with uid="FCORWFSLUF",Visible=.t., Left=58, Top=96,;
    Alignment=1, Width=94, Height=18,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="CKLMZLIXYE",Visible=.t., Left=58, Top=129,;
    Alignment=1, Width=94, Height=18,;
    Caption="Ubicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="IFBWUVDRDW",Visible=.t., Left=58, Top=163,;
    Alignment=1, Width=94, Height=18,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="ETSUJPLUZD",Visible=.t., Left=58, Top=197,;
    Alignment=1, Width=94, Height=18,;
    Caption="Lotto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="JOQIEYVZMU",Visible=.t., Left=314, Top=197,;
    Alignment=1, Width=66, Height=18,;
    Caption="Creato il:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="RMDOWAQIGO",Visible=.t., Left=464, Top=197,;
    Alignment=1, Width=73, Height=18,;
    Caption="Scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_45 as StdString with uid="NMAGDUFDAJ",Visible=.t., Left=58, Top=302,;
    Alignment=1, Width=94, Height=18,;
    Caption="U.M.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="MUJJEGMTET",Visible=.t., Left=241, Top=302,;
    Alignment=1, Width=61, Height=18,;
    Caption="Esistenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_67 as StdString with uid="GKPSWZOOYJ",Visible=.t., Left=499, Top=99,;
    Alignment=1, Width=39, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  func oStr_1_67.mHide()
    with this.Parent.oContained
      return (Empty(.w_DRSERMOV))
    endwith
  endfunc

  add object oStr_1_69 as StdString with uid="GTVIHGOPIP",Visible=.t., Left=505, Top=63,;
    Alignment=1, Width=33, Height=18,;
    Caption="Num.:"  ;
  , bGlobalFont=.t.

  func oStr_1_69.mHide()
    with this.Parent.oContained
      return (Empty(.w_DRSERMOV))
    endwith
  endfunc

  add object oStr_1_70 as StdString with uid="EACETXQLDL",Visible=.t., Left=487, Top=41,;
    Alignment=0, Width=147, Height=18,;
    Caption="Movimento di magazzino"  ;
  , bGlobalFont=.t.

  func oStr_1_70.mHide()
    with this.Parent.oContained
      return (Empty(.w_DRSERMOV))
    endwith
  endfunc

  add object oStr_1_72 as StdString with uid="GMYREUXYXQ",Visible=.t., Left=58, Top=232,;
    Alignment=1, Width=94, Height=18,;
    Caption="Matricola:"  ;
  , bGlobalFont=.t.

  func oStr_1_72.mHide()
    with this.Parent.oContained
      return (g_MATR<>'S' Or .w_DATRIL<Nvl(g_DATMAT,cp_CharToDate('  /  /    ')))
    endwith
  endfunc

  add object oStr_1_86 as StdString with uid="LUXTWIOVVZ",Visible=.t., Left=12, Top=267,;
    Alignment=1, Width=140, Height=18,;
    Caption="Commessa:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_adr','RILEVAZI','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DRSERIAL=RILEVAZI.DRSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
