* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kid                                                        *
*              Import da sede                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_32]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-19                                                      *
* Last revis.: 2007-07-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- gsut_kid
* --- Funzionalit� disponibile sono
* --- se blocco sistema attivo..
if not cp_IsAdministrator(.F.)
   Ah_ErrorMsg("Procedura utilizzabile solo da utenti con privilegi di amministratore%0Rivolgersi al proprio amministratore di sistema",'!')
   return
endif
if g_LOCKALL=.F.
   Ah_ErrorMsg("L'importazione pu� avvenire solo con il sistema in manutenzione%0Portare il sistema in manutenzione per svolgere l'operazione")
   return
endif
* --- Fine Area Manuale
return(createobject("tgsut_kid",oParentObject))

* --- Class definition
define class tgsut_kid as StdForm
  Top    = 27
  Left   = 53

  * --- Standard Properties
  Width  = 586
  Height = 403
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-09"
  HelpContextID=266945385
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=6

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_kid"
  cComment = "Import da sede"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PATH = space(254)
  w_PWD = space(254)
  w_UPDMODE = space(1)
  w_VERBOSE = .F.
  w_REMALLINT = space(1)
  w_Msg = space(0)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kidPag1","gsut_kid",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Log elaborazione")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPATH_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSUT_BID with this
      endwith
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PATH=space(254)
      .w_PWD=space(254)
      .w_UPDMODE=space(1)
      .w_VERBOSE=.f.
      .w_REMALLINT=space(1)
      .w_Msg=space(0)
          .DoRTCalc(1,2,.f.)
        .w_UPDMODE = 'T'
          .DoRTCalc(4,4,.f.)
        .w_REMALLINT = "S"
    endwith
    this.DoRTCalc(6,6,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,6,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_11.enabled = this.oPgFrm.Page1.oPag.oBtn_1_11.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oBtn_1_12.visible=!this.oPgFrm.Page1.oPag.oBtn_1_12.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPATH_1_1.value==this.w_PATH)
      this.oPgFrm.Page1.oPag.oPATH_1_1.value=this.w_PATH
    endif
    if not(this.oPgFrm.Page1.oPag.oPWD_1_2.value==this.w_PWD)
      this.oPgFrm.Page1.oPag.oPWD_1_2.value=this.w_PWD
    endif
    if not(this.oPgFrm.Page1.oPag.oUPDMODE_1_3.RadioValue()==this.w_UPDMODE)
      this.oPgFrm.Page1.oPag.oUPDMODE_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oVERBOSE_1_4.RadioValue()==this.w_VERBOSE)
      this.oPgFrm.Page1.oPag.oVERBOSE_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oREMALLINT_1_5.RadioValue()==this.w_REMALLINT)
      this.oPgFrm.Page1.oPag.oREMALLINT_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oMsg_1_10.value==this.w_Msg)
      this.oPgFrm.Page1.oPag.oMsg_1_10.value=this.w_Msg
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(Upper(Right(Alltrim(.w_PATH),4))='.AHZ')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPATH_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Indicare estensione '.Ahz'")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsut_kidPag1 as StdContainer
  Width  = 582
  height = 403
  stdWidth  = 582
  stdheight = 403
  resizeXpos=279
  resizeYpos=170
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPATH_1_1 as StdField with uid="IIKXZJUYWL",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PATH", cQueryName = "PATH",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    sErrorMsg = "Indicare estensione '.Ahz'",;
    ToolTipText = "Path di creazione/posizione del file compresso contenente i DBF con i dati delle tabelle da esportare/importare",;
    HelpContextID = 261864714,;
   bGlobalFont=.t.,;
    Height=21, Width=378, Left=140, Top=7, InputMask=replicate('X',254), bHasZoom = .t. 

  func oPATH_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Upper(Right(Alltrim(.w_PATH),4))='.AHZ')
    endwith
    return bRes
  endfunc

  proc oPATH_1_1.mZoom
    this.parent.oContained.w_PATH=GetFile( "AHZ" )
    if !isnull(this.parent.oContained)
      this.parent.oContained.mCalc(.t.)
    endif
  endproc

  add object oPWD_1_2 as StdField with uid="FPCABYYWEI",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PWD", cQueryName = "PWD",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Eventuale password per criptare/decriptare il file zip",;
    HelpContextID = 266643210,;
   bGlobalFont=.t.,;
    Height=21, Width=378, Left=140, Top=39, InputMask=replicate('X',254), PasswordChar="*"


  add object oUPDMODE_1_3 as StdCombo with uid="ELOSBXEZIT",rtseq=3,rtrep=.f.,left=140,top=68,width=173,height=21;
    , ToolTipText = "Criterio di aggiornamento, se tutto l'archivio prima cancella tutto e poi importa, altrimenti aggiorna l'archivio con le registrazioni da importare";
    , HelpContextID = 156783174;
    , cFormVar="w_UPDMODE",RowSource=""+"Tutto l'archivio,"+"Solo importati", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oUPDMODE_1_3.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'I',;
    space(1))))
  endfunc
  func oUPDMODE_1_3.GetRadio()
    this.Parent.oContained.w_UPDMODE = this.RadioValue()
    return .t.
  endfunc

  func oUPDMODE_1_3.SetRadio()
    this.Parent.oContained.w_UPDMODE=trim(this.Parent.oContained.w_UPDMODE)
    this.value = ;
      iif(this.Parent.oContained.w_UPDMODE=='T',1,;
      iif(this.Parent.oContained.w_UPDMODE=='I',2,;
      0))
  endfunc

  add object oVERBOSE_1_4 as StdCheck with uid="FVKZVFABRD",rtseq=4,rtrep=.f.,left=448, top=68, caption="Verbose",;
    ToolTipText = "Se attivo il log mostra tutte le frasi svolte sul database",;
    HelpContextID = 139339606,;
    cFormVar="w_VERBOSE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oVERBOSE_1_4.RadioValue()
    return(iif(this.value =1,.T.,;
    .f.))
  endfunc
  func oVERBOSE_1_4.GetRadio()
    this.Parent.oContained.w_VERBOSE = this.RadioValue()
    return .t.
  endfunc

  func oVERBOSE_1_4.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_VERBOSE==.T.,1,;
      0)
  endfunc


  add object oREMALLINT_1_5 as StdCombo with uid="ZKWMGXCDOM",rtseq=5,rtrep=.f.,left=140,top=98,width=173,height=21;
    , ToolTipText = "Per garantire che nessun dato sia rifiutato dal database, � bene rimuovo le integrit� referenziali";
    , HelpContextID = 249766748;
    , cFormVar="w_REMALLINT",RowSource=""+"Tutti gli archivi,"+"Solo archivi nel file", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oREMALLINT_1_5.RadioValue()
    return(iif(this.value =1,"S",;
    iif(this.value =2,"N",;
    space(1))))
  endfunc
  func oREMALLINT_1_5.GetRadio()
    this.Parent.oContained.w_REMALLINT = this.RadioValue()
    return .t.
  endfunc

  func oREMALLINT_1_5.SetRadio()
    this.Parent.oContained.w_REMALLINT=trim(this.Parent.oContained.w_REMALLINT)
    this.value = ;
      iif(this.Parent.oContained.w_REMALLINT=="S",1,;
      iif(this.Parent.oContained.w_REMALLINT=="N",2,;
      0))
  endfunc


  add object oBtn_1_7 as StdButton with uid="CZRHGAVNYF",left=465, top=353, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 266916634;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        do GSUT_BID with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_PATH))
      endwith
    endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="NYQDDFCUPJ",left=522, top=353, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Esci";
    , HelpContextID = 259627962;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMsg_1_10 as StdMemo with uid="QYBPPIIQMH",rtseq=6,rtrep=.f.,;
    cFormVar = "w_Msg", cQueryName = "Msg",;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    HelpContextID = 266492730,;
    FontName = "Courier New", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=226, Width=563, Left=9, Top=122, tabstop = .f., readonly = .t.


  add object oBtn_1_11 as StdButton with uid="QKNERQNDZN",left=9, top=353, width=48,height=45,;
    CpPicture="BMP\SAVE.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per salvare il log di elaborazione";
    , HelpContextID = 157031642;
    , Caption='\<Salva';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_11.Click()
      with this.Parent.oContained
        STRTOFILE(.w_MSG, PUTFILE('',cp_MsgFormat("Log_Elaborazione"),"TXT"))
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (Not Empty( .w_Msg ))
      endwith
    endif
  endfunc


  add object oBtn_1_12 as StdButton with uid="ENNJJPWFDP",left=522, top=5, width=48,height=45,;
    CpPicture="BMP\visualizza.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 81926026;
    , Caption='\<Contenuto';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_12.Click()
      with this.Parent.oContained
        gslr_bap(this.Parent.oContained,.w_PATH, .w_PWD)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_PATH))
      endwith
    endif
  endfunc

  func oBtn_1_12.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_LORE<>'S')
     endwith
    endif
  endfunc

  add object oStr_1_6 as StdString with uid="ANZCMLXCSI",Visible=.t., Left=16, Top=7,;
    Alignment=1, Width=122, Height=18,;
    Caption="File:"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="DQFWWAPUQY",Visible=.t., Left=14, Top=39,;
    Alignment=1, Width=124, Height=18,;
    Caption="Password:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="EMXSALBFSU",Visible=.t., Left=5, Top=70,;
    Alignment=1, Width=133, Height=18,;
    Caption="Aggiornamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="CTUAIYGXOZ",Visible=.t., Left=5, Top=97,;
    Alignment=1, Width=133, Height=18,;
    Caption="Rimuovi integrit� di:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kid','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
