* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_kdk                                                        *
*              Visualizza storico singolo documento                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2017-11-10                                                      *
* Last revis.: 2017-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_kdk",oParentObject))

* --- Class definition
define class tgscg_kdk as StdForm
  Top    = 3
  Left   = 7

  * --- Standard Properties
  Width  = 792
  Height = 516
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-11-13"
  HelpContextID=185981591
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  _IDX = 0
  cPrg = "gscg_kdk"
  cComment = "Visualizza storico singolo documento"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_SERIALE_DOC = space(10)
  w_DISERIAL = space(15)
  w_MVNUMDOC = 0
  w_MVALFDOC = space(10)
  w_MVDATDOC = ctod('  /  /  ')
  w_Vis_Storico = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_kdkPag1","gscg_kdk",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Vis_Storico = this.oPgFrm.Pages(1).oPag.Vis_Storico
    DoDefault()
    proc Destroy()
      this.w_Vis_Storico = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_SERIALE_DOC=space(10)
      .w_DISERIAL=space(15)
      .w_MVNUMDOC=0
      .w_MVALFDOC=space(10)
      .w_MVDATDOC=ctod("  /  /  ")
      .w_SERIALE_DOC=oParentObject.w_SERIALE_DOC
      .oPgFrm.Page1.oPag.Vis_Storico.Calculate()
          .DoRTCalc(1,1,.f.)
        .w_DISERIAL = This.oParentObject.oParentObject .w_DISERIAL
        .w_MVNUMDOC = .w_Vis_Storico.getVar('MVNUMDOC')
        .w_MVALFDOC = .w_Vis_Storico.getVar('MVALFDOC')
        .w_MVDATDOC = .w_Vis_Storico.getVar('MVDATDOC')
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_4.enabled = this.oPgFrm.Page1.oPag.oBtn_1_4.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_SERIALE_DOC=.w_SERIALE_DOC
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.Vis_Storico.Calculate()
        .DoRTCalc(1,2,.t.)
            .w_MVNUMDOC = .w_Vis_Storico.getVar('MVNUMDOC')
            .w_MVALFDOC = .w_Vis_Storico.getVar('MVALFDOC')
            .w_MVDATDOC = .w_Vis_Storico.getVar('MVDATDOC')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Vis_Storico.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Vis_Storico.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oMVNUMDOC_1_6.value==this.w_MVNUMDOC)
      this.oPgFrm.Page1.oPag.oMVNUMDOC_1_6.value=this.w_MVNUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVALFDOC_1_8.value==this.w_MVALFDOC)
      this.oPgFrm.Page1.oPag.oMVALFDOC_1_8.value=this.w_MVALFDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oMVDATDOC_1_10.value==this.w_MVDATDOC)
      this.oPgFrm.Page1.oPag.oMVDATDOC_1_10.value=this.w_MVDATDOC
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_kdkPag1 as StdContainer
  Width  = 788
  height = 516
  stdWidth  = 788
  stdheight = 516
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object Vis_Storico as cp_zoombox with uid="LDALKFUMLB",left=7, top=49, width=769,height=413,;
    caption='Vis_Storico',;
   bGlobalFont=.t.,;
    cZoomFile="GSCG_KDK",bOptions=.t.,bAdvOptions=.t.,bQueryOnDblClick=.t.,bQueryOnLoad=.t.,bReadOnly=.t.,cMenuFile="",cTable="DICDINTE",bNoZoomGridShape=.f.,cZoomOnZoom="",bRetriveAllRows=.f.,;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 189541544


  add object oBtn_1_4 as StdButton with uid="ZJIUAZYUNY",left=728, top=466, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 193299014;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_4.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oMVNUMDOC_1_6 as StdField with uid="UDIAGXLKWA",rtseq=3,rtrep=.f.,;
    cFormVar = "w_MVNUMDOC", cQueryName = "MVNUMDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 71308809,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=77, Top=13

  add object oMVALFDOC_1_8 as StdField with uid="JSVDVHRLGP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_MVALFDOC", cQueryName = "MVALFDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    HelpContextID = 63325705,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=215, Top=13, InputMask=replicate('X',10)

  add object oMVDATDOC_1_10 as StdField with uid="ORRGTHBDES",rtseq=5,rtrep=.f.,;
    cFormVar = "w_MVDATDOC", cQueryName = "MVDATDOC",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 77297161,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=338, Top=13

  add object oStr_1_5 as StdString with uid="UVEPAGYCFS",Visible=.t., Left=15, Top=14,;
    Alignment=1, Width=60, Height=18,;
    Caption="Doc. N.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="ZDOSADOMUS",Visible=.t., Left=197, Top=15,;
    Alignment=2, Width=18, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_9 as StdString with uid="XANXZYHJPN",Visible=.t., Left=303, Top=14,;
    Alignment=1, Width=29, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_kdk','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
