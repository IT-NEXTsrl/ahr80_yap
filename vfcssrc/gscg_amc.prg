* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_amc                                                        *
*              Modelli contabili                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [116] [VRS_14]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-17                                                      *
* Last revis.: 2010-12-06                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_amc"))

* --- Class definition
define class tgscg_amc as StdForm
  Top    = 9
  Left   = 13

  * --- Standard Properties
  Width  = 820
  Height = 411+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2010-12-06"
  HelpContextID=210380137
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  MOD_CONT_IDX = 0
  CAU_CONT_IDX = 0
  CONTI_IDX = 0
  CAUIVA_IDX = 0
  CAUPRI_IDX = 0
  cFile = "MOD_CONT"
  cKeySelect = "CCCODICE,CCTIPCON,CCCODCON"
  cKeyWhere  = "CCCODICE=this.w_CCCODICE and CCTIPCON=this.w_CCTIPCON and CCCODCON=this.w_CCCODCON"
  cKeyWhereODBC = '"CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';
      +'+" and CCTIPCON="+cp_ToStrODBC(this.w_CCTIPCON)';
      +'+" and CCCODCON="+cp_ToStrODBC(this.w_CCCODCON)';

  cKeyWhereODBCqualified = '"MOD_CONT.CCCODICE="+cp_ToStrODBC(this.w_CCCODICE)';
      +'+" and MOD_CONT.CCTIPCON="+cp_ToStrODBC(this.w_CCTIPCON)';
      +'+" and MOD_CONT.CCCODCON="+cp_ToStrODBC(this.w_CCCODCON)';

  cPrg = "gscg_amc"
  cComment = "Modelli contabili"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CCCODICE = space(5)
  w_CCTIPCON = space(1)
  w_CCCODCON = space(15)
  w_CNUMREG = 0
  w_CFLPART = space(1)
  w_DESCAU = space(35)
  w_FLRIFE = space(1)
  w_CTIPREG = space(1)
  w_DESCON = space(40)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_FLMOVC = space(1)

  * --- Children pointers
  GSCG_MAI = .NULL.
  GSCG_MAP = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'MOD_CONT','gscg_amc')
    stdPageFrame::Init()
    *set procedure to GSCG_MAI additive
    *set procedure to GSCG_MAP additive
    with this
      .Pages(1).addobject("oPag","tgscg_amcPag1","gscg_amc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Modello contabile")
      .Pages(1).HelpContextID = 13833518
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCCCODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCG_MAI
    *release procedure GSCG_MAP
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[5]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='CAUIVA'
    this.cWorkTables[4]='CAUPRI'
    this.cWorkTables[5]='MOD_CONT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(5))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.MOD_CONT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.MOD_CONT_IDX,3]
  return

  function CreateChildren()
    this.GSCG_MAI = CREATEOBJECT('stdDynamicChild',this,'GSCG_MAI',this.oPgFrm.Page1.oPag.oLinkPC_1_11)
    this.GSCG_MAI.createrealchild()
    this.GSCG_MAP = CREATEOBJECT('stdDynamicChild',this,'GSCG_MAP',this.oPgFrm.Page1.oPag.oLinkPC_1_12)
    this.GSCG_MAP.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCG_MAI)
      this.GSCG_MAI.DestroyChildrenChain()
      this.GSCG_MAI=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_11')
    if !ISNULL(this.GSCG_MAP)
      this.GSCG_MAP.DestroyChildrenChain()
      this.GSCG_MAP=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_12')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCG_MAI.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_MAP.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCG_MAI.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_MAP.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCG_MAI.NewDocument()
    this.GSCG_MAP.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCG_MAI.SetKey(;
            .w_CCCODICE,"AICODCAU";
            ,.w_CCTIPCON,"AITIPCLF";
            ,.w_CCCODCON,"AICODCLF";
            )
      this.GSCG_MAP.SetKey(;
            .w_CCCODICE,"APCODCAU";
            ,.w_CCTIPCON,"APTIPCLF";
            ,.w_CCCODCON,"APCODCLF";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCG_MAI.ChangeRow(this.cRowID+'      1',1;
             ,.w_CCCODICE,"AICODCAU";
             ,.w_CCTIPCON,"AITIPCLF";
             ,.w_CCCODCON,"AICODCLF";
             )
      .GSCG_MAP.ChangeRow(this.cRowID+'      1',1;
             ,.w_CCCODICE,"APCODCAU";
             ,.w_CCTIPCON,"APTIPCLF";
             ,.w_CCCODCON,"APCODCLF";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCG_MAI)
        i_f=.GSCG_MAI.BuildFilter()
        if !(i_f==.GSCG_MAI.cQueryFilter)
          i_fnidx=.GSCG_MAI.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_MAI.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_MAI.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_MAI.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_MAI.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCG_MAP)
        i_f=.GSCG_MAP.BuildFilter()
        if !(i_f==.GSCG_MAP.cQueryFilter)
          i_fnidx=.GSCG_MAP.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_MAP.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_MAP.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_MAP.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_MAP.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_CCCODICE = NVL(CCCODICE,space(5))
      .w_CCTIPCON = NVL(CCTIPCON,space(1))
      .w_CCCODCON = NVL(CCCODCON,space(15))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_1_joined
    link_1_1_joined=.f.
    local link_1_3_joined
    link_1_3_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from MOD_CONT where CCCODICE=KeySet.CCCODICE
    *                            and CCTIPCON=KeySet.CCTIPCON
    *                            and CCCODCON=KeySet.CCCODCON
    *
    i_nConn = i_TableProp[this.MOD_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_CONT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('MOD_CONT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "MOD_CONT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' MOD_CONT '
      link_1_1_joined=this.AddJoinedLink_1_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  ,'CCTIPCON',this.w_CCTIPCON  ,'CCCODCON',this.w_CCCODCON  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CNUMREG = 0
        .w_CFLPART = space(1)
        .w_DESCAU = space(35)
        .w_FLRIFE = space(1)
        .w_CTIPREG = space(1)
        .w_DESCON = space(40)
        .w_OBTEST = i_datsys
        .w_DATOBSO = ctod("  /  /  ")
        .w_FLMOVC = space(1)
        .w_CCCODICE = NVL(CCCODICE,space(5))
          if link_1_1_joined
            this.w_CCCODICE = NVL(CCCODICE101,NVL(this.w_CCCODICE,space(5)))
            this.w_DESCAU = NVL(CCDESCRI101,space(35))
            this.w_FLRIFE = NVL(CCFLRIFE101,space(1))
            this.w_CTIPREG = NVL(CCTIPREG101,space(1))
            this.w_CNUMREG = NVL(CCNUMREG101,0)
            this.w_CFLPART = NVL(CCFLPART101,space(1))
            this.w_DATOBSO = NVL(cp_ToDate(CCDTOBSO101),ctod("  /  /  "))
            this.w_FLMOVC = NVL(CCFLMOVC101,space(1))
          else
          .link_1_1('Load')
          endif
        .w_CCTIPCON = NVL(CCTIPCON,space(1))
        .w_CCCODCON = NVL(CCCODCON,space(15))
          if link_1_3_joined
            this.w_CCCODCON = NVL(ANCODICE103,NVL(this.w_CCCODCON,space(15)))
            this.w_DESCON = NVL(ANDESCRI103,space(40))
            this.w_DATOBSO = NVL(cp_ToDate(ANDTOBSO103),ctod("  /  /  "))
          else
          .link_1_3('Load')
          endif
        cp_LoadRecExtFlds(this,'MOD_CONT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CCCODICE = space(5)
      .w_CCTIPCON = space(1)
      .w_CCCODCON = space(15)
      .w_CNUMREG = 0
      .w_CFLPART = space(1)
      .w_DESCAU = space(35)
      .w_FLRIFE = space(1)
      .w_CTIPREG = space(1)
      .w_DESCON = space(40)
      .w_OBTEST = ctod("  /  /  ")
      .w_DATOBSO = ctod("  /  /  ")
      .w_FLMOVC = space(1)
      if .cFunction<>"Filter"
        .DoRTCalc(1,1,.f.)
          if not(empty(.w_CCCODICE))
          .link_1_1('Full')
          endif
        .w_CCTIPCON = .w_FLRIFE
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_CCCODCON))
          .link_1_3('Full')
          endif
          .DoRTCalc(4,9,.f.)
        .w_OBTEST = i_datsys
      endif
    endwith
    cp_BlankRecExtFlds(this,'MOD_CONT')
    this.DoRTCalc(11,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oCCCODICE_1_1.enabled = i_bVal
      .Page1.oPag.oCCCODCON_1_3.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oCCCODICE_1_1.enabled = .f.
        .Page1.oPag.oCCCODCON_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oCCCODICE_1_1.enabled = .t.
        .Page1.oPag.oCCCODCON_1_3.enabled = .t.
      endif
    endwith
    this.GSCG_MAI.SetStatus(i_cOp)
    this.GSCG_MAP.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'MOD_CONT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCG_MAI.SetChildrenStatus(i_cOp)
  *  this.GSCG_MAP.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.MOD_CONT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCODICE,"CCCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCTIPCON,"CCTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CCCODCON,"CCCODCON",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.MOD_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_CONT_IDX,2])
    i_lTable = "MOD_CONT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.MOD_CONT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSCG_SCM with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.MOD_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_CONT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.MOD_CONT_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into MOD_CONT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'MOD_CONT')
        i_extval=cp_InsertValODBCExtFlds(this,'MOD_CONT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(CCCODICE,CCTIPCON,CCCODCON "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_CCCODICE)+;
                  ","+cp_ToStrODBC(this.w_CCTIPCON)+;
                  ","+cp_ToStrODBCNull(this.w_CCCODCON)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'MOD_CONT')
        i_extval=cp_InsertValVFPExtFlds(this,'MOD_CONT')
        cp_CheckDeletedKey(i_cTable,0,'CCCODICE',this.w_CCCODICE,'CCTIPCON',this.w_CCTIPCON,'CCCODCON',this.w_CCCODCON)
        INSERT INTO (i_cTable);
              (CCCODICE,CCTIPCON,CCCODCON  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_CCCODICE;
                  ,this.w_CCTIPCON;
                  ,this.w_CCCODCON;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.MOD_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_CONT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.MOD_CONT_IDX,i_nConn)
      *
      * update MOD_CONT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'MOD_CONT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'MOD_CONT')
        i_cWhere = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  ,'CCTIPCON',this.w_CCTIPCON  ,'CCCODCON',this.w_CCCODCON  )
        UPDATE (i_cTable) SET;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCG_MAI : Saving
      this.GSCG_MAI.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CCCODICE,"AICODCAU";
             ,this.w_CCTIPCON,"AITIPCLF";
             ,this.w_CCCODCON,"AICODCLF";
             )
      this.GSCG_MAI.mReplace()
      * --- GSCG_MAP : Saving
      this.GSCG_MAP.ChangeRow(this.cRowID+'      1',0;
             ,this.w_CCCODICE,"APCODCAU";
             ,this.w_CCTIPCON,"APTIPCLF";
             ,this.w_CCCODCON,"APCODCLF";
             )
      this.GSCG_MAP.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCG_MAI : Deleting
    this.GSCG_MAI.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CCCODICE,"AICODCAU";
           ,this.w_CCTIPCON,"AITIPCLF";
           ,this.w_CCCODCON,"AICODCLF";
           )
    this.GSCG_MAI.mDelete()
    * --- GSCG_MAP : Deleting
    this.GSCG_MAP.ChangeRow(this.cRowID+'      1',0;
           ,this.w_CCCODICE,"APCODCAU";
           ,this.w_CCTIPCON,"APTIPCLF";
           ,this.w_CCCODCON,"APCODCLF";
           )
    this.GSCG_MAP.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.MOD_CONT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_CONT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.MOD_CONT_IDX,i_nConn)
      *
      * delete MOD_CONT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'CCCODICE',this.w_CCCODICE  ,'CCTIPCON',this.w_CCTIPCON  ,'CCCODCON',this.w_CCCODCON  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.MOD_CONT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_CONT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
            .w_CCTIPCON = .w_FLRIFE
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(3,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.GSCG_MAI.enabled = this.oPgFrm.Page1.oPag.oLinkPC_1_11.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_16.visible=!this.oPgFrm.Page1.oPag.oStr_1_16.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CCCODICE
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCODICE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CCCODICE)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLRIFE,CCTIPREG,CCNUMREG,CCFLPART,CCDTOBSO,CCFLMOVC";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CCCODICE))
          select CCCODICE,CCDESCRI,CCFLRIFE,CCTIPREG,CCNUMREG,CCFLPART,CCDTOBSO,CCFLMOVC;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCODICE)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CCCODICE) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCCCODICE_1_1'),i_cWhere,'GSCG_ACC',"Causali contabili",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLRIFE,CCTIPREG,CCNUMREG,CCFLPART,CCDTOBSO,CCFLMOVC";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLRIFE,CCTIPREG,CCNUMREG,CCFLPART,CCDTOBSO,CCFLMOVC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCODICE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLRIFE,CCTIPREG,CCNUMREG,CCFLPART,CCDTOBSO,CCFLMOVC";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CCCODICE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CCCODICE)
            select CCCODICE,CCDESCRI,CCFLRIFE,CCTIPREG,CCNUMREG,CCFLPART,CCDTOBSO,CCFLMOVC;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCODICE = NVL(_Link_.CCCODICE,space(5))
      this.w_DESCAU = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLRIFE = NVL(_Link_.CCFLRIFE,space(1))
      this.w_CTIPREG = NVL(_Link_.CCTIPREG,space(1))
      this.w_CNUMREG = NVL(_Link_.CCNUMREG,0)
      this.w_CFLPART = NVL(_Link_.CCFLPART,space(1))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.CCDTOBSO),ctod("  /  /  "))
      this.w_FLMOVC = NVL(_Link_.CCFLMOVC,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CCCODICE = space(5)
      endif
      this.w_DESCAU = space(35)
      this.w_FLRIFE = space(1)
      this.w_CTIPREG = space(1)
      this.w_CNUMREG = 0
      this.w_CFLPART = space(1)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLMOVC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(IIF(UPPER(this.cFunction)='LOAD', .w_FLRIFE='C' OR .w_FLRIFE='F' , .T.)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente, non riferita a clienti/fornitori oppure obsoleta")
        endif
        this.w_CCCODICE = space(5)
        this.w_DESCAU = space(35)
        this.w_FLRIFE = space(1)
        this.w_CTIPREG = space(1)
        this.w_CNUMREG = 0
        this.w_CFLPART = space(1)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLMOVC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCODICE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 8 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+8<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_1.CCCODICE as CCCODICE101"+ ",link_1_1.CCDESCRI as CCDESCRI101"+ ",link_1_1.CCFLRIFE as CCFLRIFE101"+ ",link_1_1.CCTIPREG as CCTIPREG101"+ ",link_1_1.CCNUMREG as CCNUMREG101"+ ",link_1_1.CCFLPART as CCFLPART101"+ ",link_1_1.CCDTOBSO as CCDTOBSO101"+ ",link_1_1.CCFLMOVC as CCFLMOVC101"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_1 on MOD_CONT.CCCODICE=link_1_1.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_1"
          i_cKey=i_cKey+'+" and MOD_CONT.CCCODICE=link_1_1.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+8
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CCCODCON
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CCCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CCCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CCTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CCTIPCON;
                     ,'ANCODICE',trim(this.w_CCCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CCCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CCCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CCTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CCCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_CCTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CCCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCCCODCON_1_3'),i_cWhere,'',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CCTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Modello contabile gi� inserito o cliente/fornitore inesistente/obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CCTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CCCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CCCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CCTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CCTIPCON;
                       ,'ANCODICE',this.w_CCCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CCCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CCCODCON = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=IIF(UPPER(this.cFunction)='LOAD', CHKAMC(.w_CCCODICE, .w_CCTIPCON, .w_CCCODCON), .T.) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Modello contabile gi� inserito o cliente/fornitore inesistente/obsoleto")
        endif
        this.w_CCCODCON = space(15)
        this.w_DESCON = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CCCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.ANCODICE as ANCODICE103"+ ",link_1_3.ANDESCRI as ANDESCRI103"+ ",link_1_3.ANDTOBSO as ANDTOBSO103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on MOD_CONT.CCCODCON=link_1_3.ANCODICE"+" and MOD_CONT.CCTIPCON=link_1_3.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and MOD_CONT.CCCODCON=link_1_3.ANCODICE(+)"'+'+" and MOD_CONT.CCTIPCON=link_1_3.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCCCODICE_1_1.value==this.w_CCCODICE)
      this.oPgFrm.Page1.oPag.oCCCODICE_1_1.value=this.w_CCCODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oCCCODCON_1_3.value==this.w_CCCODCON)
      this.oPgFrm.Page1.oPag.oCCCODCON_1_3.value=this.w_CCCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAU_1_7.value==this.w_DESCAU)
      this.oPgFrm.Page1.oPag.oDESCAU_1_7.value=this.w_DESCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_10.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_10.value=this.w_DESCON
    endif
    cp_SetControlsValueExtFlds(this,'MOD_CONT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CCCODICE)) or not((IIF(UPPER(this.cFunction)='LOAD', .w_FLRIFE='C' OR .w_FLRIFE='F' , .T.)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCCODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CCCODICE)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente, non riferita a clienti/fornitori oppure obsoleta")
          case   ((empty(.w_CCCODCON)) or not(IIF(UPPER(this.cFunction)='LOAD', CHKAMC(.w_CCCODICE, .w_CCTIPCON, .w_CCCODCON), .T.) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCCCODCON_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CCCODCON)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Modello contabile gi� inserito o cliente/fornitore inesistente/obsoleto")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCG_MAI.CheckForm()
      if i_bres
        i_bres=  .GSCG_MAI.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_MAP.CheckForm()
      if i_bres
        i_bres=  .GSCG_MAP.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- GSCG_MAI : Depends On
    this.GSCG_MAI.SaveDependsOn()
    * --- GSCG_MAP : Depends On
    this.GSCG_MAP.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscg_amcPag1 as StdContainer
  Width  = 816
  height = 411
  stdWidth  = 816
  stdheight = 411
  resizeXpos=243
  resizeYpos=342
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCCCODICE_1_1 as StdField with uid="GVNPEADEZC",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CCCODICE", cQueryName = "CCCODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente, non riferita a clienti/fornitori oppure obsoleta",;
    ToolTipText = "Codice causale contabile",;
    HelpContextID = 17388139,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=93, Top=11, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CCCODICE"

  func oCCCODICE_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCODICE_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCCODICE_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCCCODICE_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'',this.parent.oContained
  endproc
  proc oCCCODICE_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CCCODICE
     i_obj.ecpSave()
  endproc

  add object oCCCODCON_1_3 as StdField with uid="YYQIAUROTW",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CCCODCON", cQueryName = "CCCODICE,CCTIPCON,CCCODCON",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Modello contabile gi� inserito o cliente/fornitore inesistente/obsoleto",;
    ToolTipText = "Codice cliente/fornitore associato al modello contabile",;
    HelpContextID = 83275148,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=93, Top=38, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CCTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CCCODCON"

  func oCCCODCON_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCCCODCON_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCCCODCON_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CCTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CCTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCCCODCON_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti/fornitori",'',this.parent.oContained
  endproc

  add object oDESCAU_1_7 as StdField with uid="BHILYDEJGY",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCAU", cQueryName = "DESCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 53586890,;
   bGlobalFont=.t.,;
    Height=21, Width=254, Left=159, Top=11, InputMask=replicate('X',35)

  add object oDESCON_1_10 as StdField with uid="TFTSATAFZD",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 156347338,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=236, Top=38, InputMask=replicate('X',40)


  add object oLinkPC_1_11 as stdDynamicChildContainer with uid="FGXMQRCVBU",left=6, top=88, width=810, height=124, bOnScreen=.t.;


  func oLinkPC_1_11.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_CTIPREG<>'N')
      endwith
    endif
  endfunc


  add object oLinkPC_1_12 as stdDynamicChildContainer with uid="MXNOESGXWD",left=6, top=240, width=663, height=165, bOnScreen=.t.;


  add object oStr_1_4 as StdString with uid="XXTUGIOGXX",Visible=.t., Left=1, Top=11,;
    Alignment=1, Width=92, Height=15,;
    Caption="Cod.causale:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_15 as StdString with uid="DVFDAPBEGK",Visible=.t., Left=25, Top=38,;
    Alignment=1, Width=61, Height=15,;
    Caption="Cliente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (.w_CCTIPCON<>'C')
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="RDMETNCSEM",Visible=.t., Left=1, Top=38,;
    Alignment=1, Width=92, Height=15,;
    Caption="Fornitore:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_16.mHide()
    with this.Parent.oContained
      return (.w_CCTIPCON<>'F')
    endwith
  endfunc

  add object oStr_1_17 as StdString with uid="DISEGLCWSA",Visible=.t., Left=7, Top=68,;
    Alignment=0, Width=624, Height=15,;
    Caption="Righe IVA"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_18 as StdString with uid="GBIEFHUAVG",Visible=.t., Left=7, Top=220,;
    Alignment=0, Width=625, Height=15,;
    Caption="Righe contabili"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_19 as StdBox with uid="IECEDFWSWE",left=2, top=84, width=812,height=1

  add object oBox_1_20 as StdBox with uid="QCYQDABAIA",left=2, top=236, width=812,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_amc','MOD_CONT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CCCODICE=MOD_CONT.CCCODICE";
  +" and "+i_cAliasName2+".CCTIPCON=MOD_CONT.CCTIPCON";
  +" and "+i_cAliasName2+".CCCODCON=MOD_CONT.CCCODCON";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
