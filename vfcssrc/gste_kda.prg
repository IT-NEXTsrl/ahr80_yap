* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_kda                                                        *
*              Situazione partita                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_15]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-31                                                      *
* Last revis.: 2008-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_kda",oParentObject))

* --- Class definition
define class tgste_kda as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 418
  Height = 87
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-07-08"
  HelpContextID=185977751
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=4

  * --- Constant Properties
  _IDX = 0
  cPrg = "gste_kda"
  cComment = "Situazione partita"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_PTBANNOS = space(15)
  w_PTRIFIND = space(10)
  w_PTDATVAL = ctod('  /  /  ')
  w_PTDESRIG = space(50)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_kdaPag1","gste_kda",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPTDATVAL_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PTBANNOS=space(15)
      .w_PTRIFIND=space(10)
      .w_PTDATVAL=ctod("  /  /  ")
      .w_PTDESRIG=space(50)
      .w_PTBANNOS=oParentObject.w_PTBANNOS
      .w_PTRIFIND=oParentObject.w_PTRIFIND
      .w_PTDATVAL=oParentObject.w_PTDATVAL
      .w_PTDESRIG=oParentObject.w_PTDESRIG
    endwith
    this.DoRTCalc(1,4,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_PTBANNOS=.w_PTBANNOS
      .oParentObject.w_PTRIFIND=.w_PTRIFIND
      .oParentObject.w_PTDATVAL=.w_PTDATVAL
      .oParentObject.w_PTDESRIG=.w_PTDESRIG
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,4,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_KKRSZHHVNN()
    with this
          * --- Aggiorna data valuta di partite dello stesso raggruppamento
          Aggdata(this;
              ,'AggDat';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPTDATVAL_1_4.enabled = this.oPgFrm.Page1.oPag.oPTDATVAL_1_4.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Update end")
          .Calculate_KKRSZHHVNN()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPTBANNOS_1_1.value==this.w_PTBANNOS)
      this.oPgFrm.Page1.oPag.oPTBANNOS_1_1.value=this.w_PTBANNOS
    endif
    if not(this.oPgFrm.Page1.oPag.oPTDATVAL_1_4.value==this.w_PTDATVAL)
      this.oPgFrm.Page1.oPag.oPTDATVAL_1_4.value=this.w_PTDATVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oPTDESRIG_1_6.value==this.w_PTDESRIG)
      this.oPgFrm.Page1.oPag.oPTDESRIG_1_6.value=this.w_PTDESRIG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgste_kdaPag1 as StdContainer
  Width  = 414
  height = 87
  stdWidth  = 414
  stdheight = 87
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPTBANNOS_1_1 as StdField with uid="HQJAPJCIVA",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PTBANNOS", cQueryName = "PTBANNOS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 29670071,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=108, Top=32, InputMask=replicate('X',15)

  add object oPTDATVAL_1_4 as StdField with uid="TOPTYOVKOZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PTDATVAL", cQueryName = "PTDATVAL",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 157588158,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=108, Top=5

  func oPTDATVAL_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_PTRIFIND))
    endwith
   endif
  endfunc

  add object oPTDESRIG_1_6 as StdField with uid="WSQWWLVMBB",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PTDESRIG", cQueryName = "PTDESRIG",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    ToolTipText = "Riferimento libero da indicare sul record 50 del tracciato CBI",;
    HelpContextID = 42951997,;
   bGlobalFont=.t.,;
    Height=20, Width=300, Left=108, Top=59, InputMask=replicate('X',50)

  add object oStr_1_2 as StdString with uid="VCUEEFTDYB",Visible=.t., Left=8, Top=32,;
    Alignment=1, Width=97, Height=18,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="JITAMEHYEQ",Visible=.t., Left=8, Top=5,;
    Alignment=1, Width=97, Height=18,;
    Caption="Data valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="KICPEYNDFS",Visible=.t., Left=8, Top=59,;
    Alignment=1, Width=97, Height=18,;
    Caption="Dicitura CBI:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_kda','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gste_kda
proc Aggdata(oParent,cevent)
oparent.oparentobject.oparentobject.Notifyevent(cevent)
endproc

* --- Fine Area Manuale
