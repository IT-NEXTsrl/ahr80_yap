* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_api                                                        *
*              Parametri IVA                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [116] [VRS_312]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-06-29                                                      *
* Last revis.: 2014-01-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_api")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_api")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_api")
  return

* --- Class definition
define class tgscg_api as StdPCForm
  Width  = 638
  Height = 450+35
  Top    = 2
  Left   = 19
  cComment = "Parametri IVA"
  cPrg = "gscg_api"
  HelpContextID=108386967
  add object cnt as tcgscg_api
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_api as PCContext
  w_AZCODAZI = space(5)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = space(8)
  w_UTDV = space(8)
  w_OBTEST = space(8)
  w_AZIVAAZI = space(25)
  w_AZIVACOF = space(16)
  w_AZIVACON = space(3)
  w_AZIVACAR = space(2)
  w_AZCATAZI = space(5)
  w_DESATT = space(35)
  w_AZATTIVI = space(1)
  w_AZTIPDIC = space(1)
  w_AZTIPDEN = space(1)
  w_AZDENMAG = 0
  w_AZSTAREG = space(1)
  w_AZTIPINT = space(1)
  w_AZPAGINT = 0
  w_AZPREFIS = space(20)
  w_AZCONBAN = space(10)
  w_CONBAN = space(10)
  w_BATIPCON = space(1)
  w_DATOBSO1 = space(8)
  w_AZBANPRO = space(15)
  w_BACONSBF = space(1)
  w_BATIPCON_B = space(1)
  w_DATOBSO2 = space(8)
  w_BACONSBF_1 = space(1)
  w_DATOBSO = space(8)
  w_DIFINC = space(1)
  w_REGINC = space(1)
  w_DIFPAG = space(1)
  w_REGPAG = space(1)
  w_TIPCON = space(10)
  w_FLPART = space(1)
  w_FLRIFE = space(1)
  w_TIPREG = space(1)
  w_DATAOB = space(8)
  w_TIPSOT = space(1)
  w_AZTIPFOR = space(2)
  w_AZFISFOR = space(1)
  w_AZCOFFOR = space(16)
  w_AZNUMCAF = space(5)
  w_AZCOGFOR = space(24)
  w_AZNOMFOR = space(20)
  w_AZSEXFOR = space(1)
  w_AZDATFOR = space(8)
  w_AZCONFOR = space(40)
  w_AZPRNFOR = space(2)
  w_AZDENFOR = space(60)
  w_AZCOMFOR = space(40)
  w_AZPROFOR = space(2)
  w_AZINDFOR = space(35)
  w_AZCAPFOR = space(5)
  w_AZCOLFOR = space(40)
  w_AZPRLFOR = space(2)
  w_AZINLFOR = space(35)
  w_AZCALFOR = space(5)
  w_AZPLNODO = space(1)
  w_AZPLADOC = space(1)
  w_AZGIRIVA = space(2)
  w_AZCAUIVA = space(5)
  w_AZCONIVA = space(15)
  w_AZCODVEN = space(15)
  w_DESCRI = space(35)
  w_AZNEWIVA = space(1)
  w_DESCRIZ = space(40)
  w_AZCAUGIR = space(5)
  w_AZIVCONT = space(15)
  w_AZCRERIP = space(15)
  w_AZCRESPE = space(15)
  w_AZINTDOV = space(15)
  w_AZCONIND = space(15)
  w_DESGIRIVA = space(35)
  w_IVCONTDES = space(40)
  w_DESCRERIP = space(40)
  w_DESCRESPE = space(40)
  w_DESINTDOV = space(40)
  w_TIPOREG = space(1)
  w_FLANAL = space(1)
  w_TIPSOT1 = space(1)
  w_TIPSOTC = space(1)
  w_DESPIA = space(40)
  w_TIPVEN = space(1)
  w_PARVEN = space(1)
  w_DESIND = space(15)
  w_CAUINC = space(5)
  w_AZCAUPAG = space(5)
  w_AZDATCON = space(1)
  w_AZCAUMTA = space(5)
  w_AZCAUMTP = space(5)
  w_AZCAUAUT = space(5)
  w_AZIVAAUT = space(15)
  w_AZDATAUT = space(8)
  w_DESINC = space(35)
  w_DESPAG = space(35)
  w_DESAUT = space(35)
  w_DESCON = space(40)
  w_AZCAUINC = space(5)
  w_AZDESBAN = space(50)
  w_ABIBAN = space(5)
  w_CABBAN = space(5)
  w_CONCOR = space(12)
  w_BANPROABI = space(5)
  w_BANPROCAB = space(5)
  w_BANPROSIA = space(5)
  w_BADESCRI = space(35)
  w_BANPRO = space(15)
  w_DESINCAT = space(35)
  w_DESPAGPT = space(35)
  w_DIFINCAT = space(1)
  w_REGINCAT = space(1)
  w_DIFPAGPT = space(1)
  w_REGPAGPT = space(1)
  w_AZRAGCOR = space(1)
  w_AZCONPRO = space(15)
  w_DESPRO = space(15)
  w_AZFLGIND = space(1)
  w_TIPSOTRC = space(1)
  w_FLANAL2 = space(1)
  proc Save(oFrom)
    this.w_AZCODAZI = oFrom.w_AZCODAZI
    this.w_UTCC = oFrom.w_UTCC
    this.w_UTCV = oFrom.w_UTCV
    this.w_UTDC = oFrom.w_UTDC
    this.w_UTDV = oFrom.w_UTDV
    this.w_OBTEST = oFrom.w_OBTEST
    this.w_AZIVAAZI = oFrom.w_AZIVAAZI
    this.w_AZIVACOF = oFrom.w_AZIVACOF
    this.w_AZIVACON = oFrom.w_AZIVACON
    this.w_AZIVACAR = oFrom.w_AZIVACAR
    this.w_AZCATAZI = oFrom.w_AZCATAZI
    this.w_DESATT = oFrom.w_DESATT
    this.w_AZATTIVI = oFrom.w_AZATTIVI
    this.w_AZTIPDIC = oFrom.w_AZTIPDIC
    this.w_AZTIPDEN = oFrom.w_AZTIPDEN
    this.w_AZDENMAG = oFrom.w_AZDENMAG
    this.w_AZSTAREG = oFrom.w_AZSTAREG
    this.w_AZTIPINT = oFrom.w_AZTIPINT
    this.w_AZPAGINT = oFrom.w_AZPAGINT
    this.w_AZPREFIS = oFrom.w_AZPREFIS
    this.w_AZCONBAN = oFrom.w_AZCONBAN
    this.w_CONBAN = oFrom.w_CONBAN
    this.w_BATIPCON = oFrom.w_BATIPCON
    this.w_DATOBSO1 = oFrom.w_DATOBSO1
    this.w_AZBANPRO = oFrom.w_AZBANPRO
    this.w_BACONSBF = oFrom.w_BACONSBF
    this.w_BATIPCON_B = oFrom.w_BATIPCON_B
    this.w_DATOBSO2 = oFrom.w_DATOBSO2
    this.w_BACONSBF_1 = oFrom.w_BACONSBF_1
    this.w_DATOBSO = oFrom.w_DATOBSO
    this.w_DIFINC = oFrom.w_DIFINC
    this.w_REGINC = oFrom.w_REGINC
    this.w_DIFPAG = oFrom.w_DIFPAG
    this.w_REGPAG = oFrom.w_REGPAG
    this.w_TIPCON = oFrom.w_TIPCON
    this.w_FLPART = oFrom.w_FLPART
    this.w_FLRIFE = oFrom.w_FLRIFE
    this.w_TIPREG = oFrom.w_TIPREG
    this.w_DATAOB = oFrom.w_DATAOB
    this.w_TIPSOT = oFrom.w_TIPSOT
    this.w_AZTIPFOR = oFrom.w_AZTIPFOR
    this.w_AZFISFOR = oFrom.w_AZFISFOR
    this.w_AZCOFFOR = oFrom.w_AZCOFFOR
    this.w_AZNUMCAF = oFrom.w_AZNUMCAF
    this.w_AZCOGFOR = oFrom.w_AZCOGFOR
    this.w_AZNOMFOR = oFrom.w_AZNOMFOR
    this.w_AZSEXFOR = oFrom.w_AZSEXFOR
    this.w_AZDATFOR = oFrom.w_AZDATFOR
    this.w_AZCONFOR = oFrom.w_AZCONFOR
    this.w_AZPRNFOR = oFrom.w_AZPRNFOR
    this.w_AZDENFOR = oFrom.w_AZDENFOR
    this.w_AZCOMFOR = oFrom.w_AZCOMFOR
    this.w_AZPROFOR = oFrom.w_AZPROFOR
    this.w_AZINDFOR = oFrom.w_AZINDFOR
    this.w_AZCAPFOR = oFrom.w_AZCAPFOR
    this.w_AZCOLFOR = oFrom.w_AZCOLFOR
    this.w_AZPRLFOR = oFrom.w_AZPRLFOR
    this.w_AZINLFOR = oFrom.w_AZINLFOR
    this.w_AZCALFOR = oFrom.w_AZCALFOR
    this.w_AZPLNODO = oFrom.w_AZPLNODO
    this.w_AZPLADOC = oFrom.w_AZPLADOC
    this.w_AZGIRIVA = oFrom.w_AZGIRIVA
    this.w_AZCAUIVA = oFrom.w_AZCAUIVA
    this.w_AZCONIVA = oFrom.w_AZCONIVA
    this.w_AZCODVEN = oFrom.w_AZCODVEN
    this.w_DESCRI = oFrom.w_DESCRI
    this.w_AZNEWIVA = oFrom.w_AZNEWIVA
    this.w_DESCRIZ = oFrom.w_DESCRIZ
    this.w_AZCAUGIR = oFrom.w_AZCAUGIR
    this.w_AZIVCONT = oFrom.w_AZIVCONT
    this.w_AZCRERIP = oFrom.w_AZCRERIP
    this.w_AZCRESPE = oFrom.w_AZCRESPE
    this.w_AZINTDOV = oFrom.w_AZINTDOV
    this.w_AZCONIND = oFrom.w_AZCONIND
    this.w_DESGIRIVA = oFrom.w_DESGIRIVA
    this.w_IVCONTDES = oFrom.w_IVCONTDES
    this.w_DESCRERIP = oFrom.w_DESCRERIP
    this.w_DESCRESPE = oFrom.w_DESCRESPE
    this.w_DESINTDOV = oFrom.w_DESINTDOV
    this.w_TIPOREG = oFrom.w_TIPOREG
    this.w_FLANAL = oFrom.w_FLANAL
    this.w_TIPSOT1 = oFrom.w_TIPSOT1
    this.w_TIPSOTC = oFrom.w_TIPSOTC
    this.w_DESPIA = oFrom.w_DESPIA
    this.w_TIPVEN = oFrom.w_TIPVEN
    this.w_PARVEN = oFrom.w_PARVEN
    this.w_DESIND = oFrom.w_DESIND
    this.w_CAUINC = oFrom.w_CAUINC
    this.w_AZCAUPAG = oFrom.w_AZCAUPAG
    this.w_AZDATCON = oFrom.w_AZDATCON
    this.w_AZCAUMTA = oFrom.w_AZCAUMTA
    this.w_AZCAUMTP = oFrom.w_AZCAUMTP
    this.w_AZCAUAUT = oFrom.w_AZCAUAUT
    this.w_AZIVAAUT = oFrom.w_AZIVAAUT
    this.w_AZDATAUT = oFrom.w_AZDATAUT
    this.w_DESINC = oFrom.w_DESINC
    this.w_DESPAG = oFrom.w_DESPAG
    this.w_DESAUT = oFrom.w_DESAUT
    this.w_DESCON = oFrom.w_DESCON
    this.w_AZCAUINC = oFrom.w_AZCAUINC
    this.w_AZDESBAN = oFrom.w_AZDESBAN
    this.w_ABIBAN = oFrom.w_ABIBAN
    this.w_CABBAN = oFrom.w_CABBAN
    this.w_CONCOR = oFrom.w_CONCOR
    this.w_BANPROABI = oFrom.w_BANPROABI
    this.w_BANPROCAB = oFrom.w_BANPROCAB
    this.w_BANPROSIA = oFrom.w_BANPROSIA
    this.w_BADESCRI = oFrom.w_BADESCRI
    this.w_BANPRO = oFrom.w_BANPRO
    this.w_DESINCAT = oFrom.w_DESINCAT
    this.w_DESPAGPT = oFrom.w_DESPAGPT
    this.w_DIFINCAT = oFrom.w_DIFINCAT
    this.w_REGINCAT = oFrom.w_REGINCAT
    this.w_DIFPAGPT = oFrom.w_DIFPAGPT
    this.w_REGPAGPT = oFrom.w_REGPAGPT
    this.w_AZRAGCOR = oFrom.w_AZRAGCOR
    this.w_AZCONPRO = oFrom.w_AZCONPRO
    this.w_DESPRO = oFrom.w_DESPRO
    this.w_AZFLGIND = oFrom.w_AZFLGIND
    this.w_TIPSOTRC = oFrom.w_TIPSOTRC
    this.w_FLANAL2 = oFrom.w_FLANAL2
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_AZCODAZI = this.w_AZCODAZI
    oTo.w_UTCC = this.w_UTCC
    oTo.w_UTCV = this.w_UTCV
    oTo.w_UTDC = this.w_UTDC
    oTo.w_UTDV = this.w_UTDV
    oTo.w_OBTEST = this.w_OBTEST
    oTo.w_AZIVAAZI = this.w_AZIVAAZI
    oTo.w_AZIVACOF = this.w_AZIVACOF
    oTo.w_AZIVACON = this.w_AZIVACON
    oTo.w_AZIVACAR = this.w_AZIVACAR
    oTo.w_AZCATAZI = this.w_AZCATAZI
    oTo.w_DESATT = this.w_DESATT
    oTo.w_AZATTIVI = this.w_AZATTIVI
    oTo.w_AZTIPDIC = this.w_AZTIPDIC
    oTo.w_AZTIPDEN = this.w_AZTIPDEN
    oTo.w_AZDENMAG = this.w_AZDENMAG
    oTo.w_AZSTAREG = this.w_AZSTAREG
    oTo.w_AZTIPINT = this.w_AZTIPINT
    oTo.w_AZPAGINT = this.w_AZPAGINT
    oTo.w_AZPREFIS = this.w_AZPREFIS
    oTo.w_AZCONBAN = this.w_AZCONBAN
    oTo.w_CONBAN = this.w_CONBAN
    oTo.w_BATIPCON = this.w_BATIPCON
    oTo.w_DATOBSO1 = this.w_DATOBSO1
    oTo.w_AZBANPRO = this.w_AZBANPRO
    oTo.w_BACONSBF = this.w_BACONSBF
    oTo.w_BATIPCON_B = this.w_BATIPCON_B
    oTo.w_DATOBSO2 = this.w_DATOBSO2
    oTo.w_BACONSBF_1 = this.w_BACONSBF_1
    oTo.w_DATOBSO = this.w_DATOBSO
    oTo.w_DIFINC = this.w_DIFINC
    oTo.w_REGINC = this.w_REGINC
    oTo.w_DIFPAG = this.w_DIFPAG
    oTo.w_REGPAG = this.w_REGPAG
    oTo.w_TIPCON = this.w_TIPCON
    oTo.w_FLPART = this.w_FLPART
    oTo.w_FLRIFE = this.w_FLRIFE
    oTo.w_TIPREG = this.w_TIPREG
    oTo.w_DATAOB = this.w_DATAOB
    oTo.w_TIPSOT = this.w_TIPSOT
    oTo.w_AZTIPFOR = this.w_AZTIPFOR
    oTo.w_AZFISFOR = this.w_AZFISFOR
    oTo.w_AZCOFFOR = this.w_AZCOFFOR
    oTo.w_AZNUMCAF = this.w_AZNUMCAF
    oTo.w_AZCOGFOR = this.w_AZCOGFOR
    oTo.w_AZNOMFOR = this.w_AZNOMFOR
    oTo.w_AZSEXFOR = this.w_AZSEXFOR
    oTo.w_AZDATFOR = this.w_AZDATFOR
    oTo.w_AZCONFOR = this.w_AZCONFOR
    oTo.w_AZPRNFOR = this.w_AZPRNFOR
    oTo.w_AZDENFOR = this.w_AZDENFOR
    oTo.w_AZCOMFOR = this.w_AZCOMFOR
    oTo.w_AZPROFOR = this.w_AZPROFOR
    oTo.w_AZINDFOR = this.w_AZINDFOR
    oTo.w_AZCAPFOR = this.w_AZCAPFOR
    oTo.w_AZCOLFOR = this.w_AZCOLFOR
    oTo.w_AZPRLFOR = this.w_AZPRLFOR
    oTo.w_AZINLFOR = this.w_AZINLFOR
    oTo.w_AZCALFOR = this.w_AZCALFOR
    oTo.w_AZPLNODO = this.w_AZPLNODO
    oTo.w_AZPLADOC = this.w_AZPLADOC
    oTo.w_AZGIRIVA = this.w_AZGIRIVA
    oTo.w_AZCAUIVA = this.w_AZCAUIVA
    oTo.w_AZCONIVA = this.w_AZCONIVA
    oTo.w_AZCODVEN = this.w_AZCODVEN
    oTo.w_DESCRI = this.w_DESCRI
    oTo.w_AZNEWIVA = this.w_AZNEWIVA
    oTo.w_DESCRIZ = this.w_DESCRIZ
    oTo.w_AZCAUGIR = this.w_AZCAUGIR
    oTo.w_AZIVCONT = this.w_AZIVCONT
    oTo.w_AZCRERIP = this.w_AZCRERIP
    oTo.w_AZCRESPE = this.w_AZCRESPE
    oTo.w_AZINTDOV = this.w_AZINTDOV
    oTo.w_AZCONIND = this.w_AZCONIND
    oTo.w_DESGIRIVA = this.w_DESGIRIVA
    oTo.w_IVCONTDES = this.w_IVCONTDES
    oTo.w_DESCRERIP = this.w_DESCRERIP
    oTo.w_DESCRESPE = this.w_DESCRESPE
    oTo.w_DESINTDOV = this.w_DESINTDOV
    oTo.w_TIPOREG = this.w_TIPOREG
    oTo.w_FLANAL = this.w_FLANAL
    oTo.w_TIPSOT1 = this.w_TIPSOT1
    oTo.w_TIPSOTC = this.w_TIPSOTC
    oTo.w_DESPIA = this.w_DESPIA
    oTo.w_TIPVEN = this.w_TIPVEN
    oTo.w_PARVEN = this.w_PARVEN
    oTo.w_DESIND = this.w_DESIND
    oTo.w_CAUINC = this.w_CAUINC
    oTo.w_AZCAUPAG = this.w_AZCAUPAG
    oTo.w_AZDATCON = this.w_AZDATCON
    oTo.w_AZCAUMTA = this.w_AZCAUMTA
    oTo.w_AZCAUMTP = this.w_AZCAUMTP
    oTo.w_AZCAUAUT = this.w_AZCAUAUT
    oTo.w_AZIVAAUT = this.w_AZIVAAUT
    oTo.w_AZDATAUT = this.w_AZDATAUT
    oTo.w_DESINC = this.w_DESINC
    oTo.w_DESPAG = this.w_DESPAG
    oTo.w_DESAUT = this.w_DESAUT
    oTo.w_DESCON = this.w_DESCON
    oTo.w_AZCAUINC = this.w_AZCAUINC
    oTo.w_AZDESBAN = this.w_AZDESBAN
    oTo.w_ABIBAN = this.w_ABIBAN
    oTo.w_CABBAN = this.w_CABBAN
    oTo.w_CONCOR = this.w_CONCOR
    oTo.w_BANPROABI = this.w_BANPROABI
    oTo.w_BANPROCAB = this.w_BANPROCAB
    oTo.w_BANPROSIA = this.w_BANPROSIA
    oTo.w_BADESCRI = this.w_BADESCRI
    oTo.w_BANPRO = this.w_BANPRO
    oTo.w_DESINCAT = this.w_DESINCAT
    oTo.w_DESPAGPT = this.w_DESPAGPT
    oTo.w_DIFINCAT = this.w_DIFINCAT
    oTo.w_REGINCAT = this.w_REGINCAT
    oTo.w_DIFPAGPT = this.w_DIFPAGPT
    oTo.w_REGPAGPT = this.w_REGPAGPT
    oTo.w_AZRAGCOR = this.w_AZRAGCOR
    oTo.w_AZCONPRO = this.w_AZCONPRO
    oTo.w_DESPRO = this.w_DESPRO
    oTo.w_AZFLGIND = this.w_AZFLGIND
    oTo.w_TIPSOTRC = this.w_TIPSOTRC
    oTo.w_FLANAL2 = this.w_FLANAL2
    PCContext::Load(oTo)
enddefine

define class tcgscg_api as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 638
  Height = 450+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-01-16"
  HelpContextID=108386967
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=121

  * --- Constant Properties
  AZIENDA_IDX = 0
  ATTIMAST_IDX = 0
  COC_MAST_IDX = 0
  VALUTE_IDX = 0
  CAU_CONT_IDX = 0
  CONTI_IDX = 0
  cFile = "AZIENDA"
  cKeySelect = "AZCODAZI"
  cKeyWhere  = "AZCODAZI=this.w_AZCODAZI"
  cKeyWhereODBC = '"AZCODAZI="+cp_ToStrODBC(this.w_AZCODAZI)';

  cKeyWhereODBCqualified = '"AZIENDA.AZCODAZI="+cp_ToStrODBC(this.w_AZCODAZI)';

  cPrg = "gscg_api"
  cComment = "Parametri IVA"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_AZCODAZI = space(5)
  o_AZCODAZI = space(5)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_OBTEST = ctod('  /  /  ')
  w_AZIVAAZI = space(25)
  w_AZIVACOF = space(16)
  w_AZIVACON = space(3)
  w_AZIVACAR = space(2)
  w_AZCATAZI = space(5)
  w_DESATT = space(35)
  w_AZATTIVI = space(1)
  w_AZTIPDIC = space(1)
  w_AZTIPDEN = space(1)
  o_AZTIPDEN = space(1)
  w_AZDENMAG = 0
  w_AZSTAREG = space(1)
  w_AZTIPINT = space(1)
  o_AZTIPINT = space(1)
  w_AZPAGINT = 0
  w_AZPREFIS = space(20)
  w_AZCONBAN = space(10)
  w_CONBAN = space(10)
  w_BATIPCON = space(1)
  w_DATOBSO1 = ctod('  /  /  ')
  w_AZBANPRO = space(15)
  w_BACONSBF = space(1)
  w_BATIPCON_B = space(1)
  w_DATOBSO2 = ctod('  /  /  ')
  w_BACONSBF_1 = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_DIFINC = space(1)
  w_REGINC = space(1)
  w_DIFPAG = space(1)
  w_REGPAG = space(1)
  w_TIPCON = space(10)
  w_FLPART = space(1)
  w_FLRIFE = space(1)
  w_TIPREG = space(1)
  w_DATAOB = ctod('  /  /  ')
  w_TIPSOT = space(1)
  w_AZTIPFOR = space(2)
  w_AZFISFOR = space(1)
  w_AZCOFFOR = space(16)
  w_AZNUMCAF = space(5)
  w_AZCOGFOR = space(24)
  w_AZNOMFOR = space(20)
  w_AZSEXFOR = space(1)
  w_AZDATFOR = ctod('  /  /  ')
  w_AZCONFOR = space(40)
  w_AZPRNFOR = space(2)
  w_AZDENFOR = space(60)
  w_AZCOMFOR = space(40)
  w_AZPROFOR = space(2)
  w_AZINDFOR = space(35)
  w_AZCAPFOR = space(5)
  w_AZCOLFOR = space(40)
  w_AZPRLFOR = space(2)
  w_AZINLFOR = space(35)
  w_AZCALFOR = space(5)
  w_AZPLNODO = space(1)
  o_AZPLNODO = space(1)
  w_AZPLADOC = space(1)
  w_AZGIRIVA = space(2)
  w_AZCAUIVA = space(5)
  o_AZCAUIVA = space(5)
  w_AZCONIVA = space(15)
  w_AZCODVEN = space(15)
  w_DESCRI = space(35)
  w_AZNEWIVA = space(1)
  o_AZNEWIVA = space(1)
  w_DESCRIZ = space(40)
  w_AZCAUGIR = space(5)
  w_AZIVCONT = space(15)
  w_AZCRERIP = space(15)
  w_AZCRESPE = space(15)
  w_AZINTDOV = space(15)
  w_AZCONIND = space(15)
  w_DESGIRIVA = space(35)
  w_IVCONTDES = space(40)
  w_DESCRERIP = space(40)
  w_DESCRESPE = space(40)
  w_DESINTDOV = space(40)
  w_TIPOREG = space(1)
  w_FLANAL = space(1)
  w_TIPSOT1 = space(1)
  w_TIPSOTC = space(1)
  w_DESPIA = space(40)
  w_TIPVEN = space(1)
  w_PARVEN = space(1)
  w_DESIND = space(15)
  w_CAUINC = space(5)
  w_AZCAUPAG = space(5)
  w_AZDATCON = space(1)
  w_AZCAUMTA = space(5)
  w_AZCAUMTP = space(5)
  w_AZCAUAUT = space(5)
  w_AZIVAAUT = space(15)
  w_AZDATAUT = ctod('  /  /  ')
  w_DESINC = space(35)
  w_DESPAG = space(35)
  w_DESAUT = space(35)
  w_DESCON = space(40)
  w_AZCAUINC = space(5)
  w_AZDESBAN = space(50)
  w_ABIBAN = space(5)
  w_CABBAN = space(5)
  w_CONCOR = space(12)
  w_BANPROABI = space(5)
  w_BANPROCAB = space(5)
  w_BANPROSIA = space(5)
  w_BADESCRI = space(35)
  w_BANPRO = space(15)
  w_DESINCAT = space(35)
  w_DESPAGPT = space(35)
  w_DIFINCAT = space(1)
  w_REGINCAT = space(1)
  w_DIFPAGPT = space(1)
  w_REGPAGPT = space(1)
  w_AZRAGCOR = space(1)
  w_AZCONPRO = space(15)
  w_DESPRO = space(15)
  w_AZFLGIND = space(1)
  w_TIPSOTRC = space(1)
  w_FLANAL2 = space(1)

  * --- Children pointers
  GSCG_MDI = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=5, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_apiPag1","gscg_api",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Principale")
      .Pages(1).HelpContextID = 102452759
      .Pages(2).addobject("oPag","tgscg_apiPag2","gscg_api",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dati annuali")
      .Pages(2).HelpContextID = 173451508
      .Pages(3).addobject("oPag","tgscg_apiPag3","gscg_api",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Dati fornitore")
      .Pages(3).HelpContextID = 75681656
      .Pages(4).addobject("oPag","tgscg_apiPag4","gscg_api",4)
      .Pages(4).oPag.Visible=.t.
      .Pages(4).Caption=cp_Translate("IVA sospesa")
      .Pages(4).HelpContextID = 53424314
      .Pages(5).addobject("oPag","tgscg_apiPag5","gscg_api",5)
      .Pages(5).oPag.Visible=.t.
      .Pages(5).Caption=cp_Translate("Giroconti")
      .Pages(5).HelpContextID = 203190122
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oAZCODAZI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='ATTIMAST'
    this.cWorkTables[2]='COC_MAST'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='CAU_CONT'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='AZIENDA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.AZIENDA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.AZIENDA_IDX,3]
  return

  function CreateChildren()
    this.GSCG_MDI = CREATEOBJECT('stdDynamicChild',this,'GSCG_MDI',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    return

  procedure NewContext()
    return(createobject('tsgscg_api'))

  function DestroyChildrenChain()
    this.oParentObject=.NULL.
    if !ISNULL(this.GSCG_MDI)
      this.GSCG_MDI.DestroyChildrenChain()
    endif
    return

  function HideChildrenChain()
    *this.Hide()
    this.bOnScreen = .f.
    this.GSCG_MDI.HideChildrenChain()
    return

  function ShowChildrenChain()
    this.bOnScreen=.t.
    this.GSCG_MDI.ShowChildrenChain()
    DoDefault()
    return
  procedure DestroyChildren()
    if !ISNULL(this.GSCG_MDI)
      this.GSCG_MDI.DestroyChildrenChain()
      this.GSCG_MDI=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCG_MDI.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCG_MDI.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCG_MDI.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCG_MDI.SetKey(;
            .w_AZCODAZI,"IACODAZI";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCG_MDI.ChangeRow(this.cRowID+'      1',1;
             ,.w_AZCODAZI,"IACODAZI";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCG_MDI)
        i_f=.GSCG_MDI.BuildFilter()
        if !(i_f==.GSCG_MDI.cQueryFilter)
          i_fnidx=.GSCG_MDI.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_MDI.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_MDI.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_MDI.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_MDI.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_11_joined
    link_1_11_joined=.f.
    local link_5_3_joined
    link_5_3_joined=.f.
    local link_5_13_joined
    link_5_13_joined=.f.
    local link_4_6_joined
    link_4_6_joined=.f.
    local link_4_8_joined
    link_4_8_joined=.f.
    local link_4_9_joined
    link_4_9_joined=.f.
    local link_4_13_joined
    link_4_13_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from AZIENDA where AZCODAZI=KeySet.AZCODAZI
    *
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('AZIENDA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "AZIENDA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' AZIENDA '
      link_1_11_joined=this.AddJoinedLink_1_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_5_3_joined=this.AddJoinedLink_5_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_5_13_joined=this.AddJoinedLink_5_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_6_joined=this.AddJoinedLink_4_6(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_8_joined=this.AddJoinedLink_4_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_9_joined=this.AddJoinedLink_4_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_4_13_joined=this.AddJoinedLink_4_13(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'AZCODAZI',this.w_AZCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = i_datsys
        .w_DESATT = space(35)
        .w_CONBAN = space(10)
        .w_BATIPCON = space(1)
        .w_DATOBSO1 = ctod("  /  /  ")
        .w_BACONSBF = space(1)
        .w_BATIPCON_B = space(1)
        .w_DATOBSO2 = ctod("  /  /  ")
        .w_BACONSBF_1 = space(1)
        .w_DATOBSO = ctod("  /  /  ")
        .w_DIFINC = space(1)
        .w_REGINC = space(1)
        .w_DIFPAG = space(1)
        .w_REGPAG = space(1)
        .w_TIPCON = 'G'
        .w_FLPART = space(1)
        .w_FLRIFE = space(1)
        .w_TIPREG = space(1)
        .w_DATAOB = ctod("  /  /  ")
        .w_TIPSOT = space(1)
        .w_DESCRIZ = space(40)
        .w_DESGIRIVA = space(35)
        .w_IVCONTDES = space(40)
        .w_DESCRERIP = space(40)
        .w_DESCRESPE = space(40)
        .w_DESINTDOV = space(40)
        .w_TIPOREG = space(1)
        .w_FLANAL = space(1)
        .w_TIPSOT1 = space(1)
        .w_TIPSOTC = space(1)
        .w_DESPIA = space(40)
        .w_TIPVEN = space(1)
        .w_PARVEN = space(1)
        .w_DESIND = space(15)
        .w_CAUINC = space(5)
        .w_DESINC = space(35)
        .w_DESPAG = space(35)
        .w_DESAUT = space(35)
        .w_DESCON = space(40)
        .w_ABIBAN = space(5)
        .w_CABBAN = space(5)
        .w_CONCOR = space(12)
        .w_BANPROABI = space(5)
        .w_BANPROCAB = space(5)
        .w_BANPROSIA = space(5)
        .w_BADESCRI = space(35)
        .w_BANPRO = space(15)
        .w_DESINCAT = space(35)
        .w_DESPAGPT = space(35)
        .w_DIFINCAT = space(1)
        .w_REGINCAT = space(1)
        .w_DIFPAGPT = space(1)
        .w_REGPAGPT = space(1)
        .w_DESPRO = space(15)
        .w_TIPSOTRC = space(1)
        .w_FLANAL2 = space(1)
        .w_AZCODAZI = NVL(AZCODAZI,space(5))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_AZIVAAZI = NVL(AZIVAAZI,space(25))
        .w_AZIVACOF = NVL(AZIVACOF,space(16))
        .w_AZIVACON = NVL(AZIVACON,space(3))
        .w_AZIVACAR = NVL(AZIVACAR,space(2))
        .w_AZCATAZI = NVL(AZCATAZI,space(5))
          if link_1_11_joined
            this.w_AZCATAZI = NVL(ATCODATT111,NVL(this.w_AZCATAZI,space(5)))
            this.w_DESATT = NVL(ATDESATT111,space(35))
          else
          .link_1_11('Load')
          endif
        .w_AZATTIVI = NVL(AZATTIVI,space(1))
        .w_AZTIPDIC = NVL(AZTIPDIC,space(1))
        .w_AZTIPDEN = NVL(AZTIPDEN,space(1))
        .w_AZDENMAG = NVL(AZDENMAG,0)
        .w_AZSTAREG = NVL(AZSTAREG,space(1))
        .w_AZTIPINT = NVL(AZTIPINT,space(1))
        .w_AZPAGINT = NVL(AZPAGINT,0)
        .w_AZPREFIS = NVL(AZPREFIS,space(20))
        .w_AZCONBAN = NVL(AZCONBAN,space(10))
          .link_1_23('Load')
        .w_AZBANPRO = NVL(AZBANPRO,space(15))
        .w_AZTIPFOR = NVL(AZTIPFOR,space(2))
        .w_AZFISFOR = NVL(AZFISFOR,space(1))
        .w_AZCOFFOR = NVL(AZCOFFOR,space(16))
        .w_AZNUMCAF = NVL(AZNUMCAF,space(5))
        .w_AZCOGFOR = NVL(AZCOGFOR,space(24))
        .w_AZNOMFOR = NVL(AZNOMFOR,space(20))
        .w_AZSEXFOR = NVL(AZSEXFOR,space(1))
        .w_AZDATFOR = NVL(cp_ToDate(AZDATFOR),ctod("  /  /  "))
        .w_AZCONFOR = NVL(AZCONFOR,space(40))
        .w_AZPRNFOR = NVL(AZPRNFOR,space(2))
        .w_AZDENFOR = NVL(AZDENFOR,space(60))
        .w_AZCOMFOR = NVL(AZCOMFOR,space(40))
        .w_AZPROFOR = NVL(AZPROFOR,space(2))
        .w_AZINDFOR = NVL(AZINDFOR,space(35))
        .w_AZCAPFOR = NVL(AZCAPFOR,space(5))
        .w_AZCOLFOR = NVL(AZCOLFOR,space(40))
        .w_AZPRLFOR = NVL(AZPRLFOR,space(2))
        .w_AZINLFOR = NVL(AZINLFOR,space(35))
        .w_AZCALFOR = NVL(AZCALFOR,space(5))
        .w_AZPLNODO = NVL(AZPLNODO,space(1))
        .w_AZPLADOC = NVL(AZPLADOC,space(1))
        .w_AZGIRIVA = NVL(AZGIRIVA,space(2))
        .w_AZCAUIVA = NVL(AZCAUIVA,space(5))
          if link_5_3_joined
            this.w_AZCAUIVA = NVL(CCCODICE503,NVL(this.w_AZCAUIVA,space(5)))
            this.w_FLANAL2 = NVL(CCFLANAL503,space(1))
            this.w_TIPOREG = NVL(CCTIPREG503,space(1))
          else
          .link_5_3('Load')
          endif
        .w_AZCONIVA = NVL(AZCONIVA,space(15))
          .link_5_4('Load')
        .w_AZCODVEN = NVL(AZCODVEN,space(15))
          .link_5_5('Load')
        .w_DESCRI = looktab('CAU_CONT','CCDESCRI','CCCODICE',.w_AZCAUIVA)
        .w_AZNEWIVA = NVL(AZNEWIVA,space(1))
        .w_AZCAUGIR = NVL(AZCAUGIR,space(5))
          if link_5_13_joined
            this.w_AZCAUGIR = NVL(CCCODICE513,NVL(this.w_AZCAUGIR,space(5)))
            this.w_DESGIRIVA = NVL(CCDESCRI513,space(35))
            this.w_TIPOREG = NVL(CCTIPREG513,space(1))
            this.w_FLANAL = NVL(CCFLANAL513,space(1))
          else
          .link_5_13('Load')
          endif
        .w_AZIVCONT = NVL(AZIVCONT,space(15))
          .link_5_14('Load')
        .w_AZCRERIP = NVL(AZCRERIP,space(15))
          .link_5_15('Load')
        .w_AZCRESPE = NVL(AZCRESPE,space(15))
          .link_5_16('Load')
        .w_AZINTDOV = NVL(AZINTDOV,space(15))
          .link_5_17('Load')
        .w_AZCONIND = NVL(AZCONIND,space(15))
          .link_5_18('Load')
          .link_4_4('Load')
        .w_AZCAUPAG = NVL(AZCAUPAG,space(5))
          if link_4_6_joined
            this.w_AZCAUPAG = NVL(CCCODICE406,NVL(this.w_AZCAUPAG,space(5)))
            this.w_DESPAG = NVL(CCDESCRI406,space(35))
            this.w_DIFPAG = NVL(CCFLPDIF406,space(1))
            this.w_REGPAG = NVL(CCTIPREG406,space(1))
          else
          .link_4_6('Load')
          endif
        .w_AZDATCON = NVL(AZDATCON,space(1))
        .w_AZCAUMTA = NVL(AZCAUMTA,space(5))
          if link_4_8_joined
            this.w_AZCAUMTA = NVL(CCCODICE408,NVL(this.w_AZCAUMTA,space(5)))
            this.w_DESINCAT = NVL(CCDESCRI408,space(35))
            this.w_DIFINCAT = NVL(CCFLPDIF408,space(1))
            this.w_REGINCAT = NVL(CCTIPREG408,space(1))
          else
          .link_4_8('Load')
          endif
        .w_AZCAUMTP = NVL(AZCAUMTP,space(5))
          if link_4_9_joined
            this.w_AZCAUMTP = NVL(CCCODICE409,NVL(this.w_AZCAUMTP,space(5)))
            this.w_DESPAGPT = NVL(CCDESCRI409,space(35))
            this.w_DIFPAGPT = NVL(CCFLPDIF409,space(1))
            this.w_REGPAGPT = NVL(CCTIPREG409,space(1))
          else
          .link_4_9('Load')
          endif
        .w_AZCAUAUT = NVL(AZCAUAUT,space(5))
          if link_4_13_joined
            this.w_AZCAUAUT = NVL(CCCODICE413,NVL(this.w_AZCAUAUT,space(5)))
            this.w_DESAUT = NVL(CCDESCRI413,space(35))
            this.w_FLPART = NVL(CCFLPART413,space(1))
            this.w_FLRIFE = NVL(CCFLRIFE413,space(1))
            this.w_TIPREG = NVL(CCTIPREG413,space(1))
          else
          .link_4_13('Load')
          endif
        .w_AZIVAAUT = NVL(AZIVAAUT,space(15))
          .link_4_14('Load')
        .w_AZDATAUT = NVL(cp_ToDate(AZDATAUT),ctod("  /  /  "))
        .w_AZCAUINC = NVL(AZCAUINC,space(5))
        .w_AZDESBAN = NVL(AZDESBAN,space(50))
          .link_1_65('Load')
        .w_AZRAGCOR = NVL(AZRAGCOR,space(1))
        .w_AZCONPRO = NVL(AZCONPRO,space(15))
          .link_5_45('Load')
        .w_AZFLGIND = NVL(AZFLGIND,space(1))
        cp_LoadRecExtFlds(this,'AZIENDA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_AZCODAZI = space(5)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_OBTEST = ctod("  /  /  ")
      .w_AZIVAAZI = space(25)
      .w_AZIVACOF = space(16)
      .w_AZIVACON = space(3)
      .w_AZIVACAR = space(2)
      .w_AZCATAZI = space(5)
      .w_DESATT = space(35)
      .w_AZATTIVI = space(1)
      .w_AZTIPDIC = space(1)
      .w_AZTIPDEN = space(1)
      .w_AZDENMAG = 0
      .w_AZSTAREG = space(1)
      .w_AZTIPINT = space(1)
      .w_AZPAGINT = 0
      .w_AZPREFIS = space(20)
      .w_AZCONBAN = space(10)
      .w_CONBAN = space(10)
      .w_BATIPCON = space(1)
      .w_DATOBSO1 = ctod("  /  /  ")
      .w_AZBANPRO = space(15)
      .w_BACONSBF = space(1)
      .w_BATIPCON_B = space(1)
      .w_DATOBSO2 = ctod("  /  /  ")
      .w_BACONSBF_1 = space(1)
      .w_DATOBSO = ctod("  /  /  ")
      .w_DIFINC = space(1)
      .w_REGINC = space(1)
      .w_DIFPAG = space(1)
      .w_REGPAG = space(1)
      .w_TIPCON = space(10)
      .w_FLPART = space(1)
      .w_FLRIFE = space(1)
      .w_TIPREG = space(1)
      .w_DATAOB = ctod("  /  /  ")
      .w_TIPSOT = space(1)
      .w_AZTIPFOR = space(2)
      .w_AZFISFOR = space(1)
      .w_AZCOFFOR = space(16)
      .w_AZNUMCAF = space(5)
      .w_AZCOGFOR = space(24)
      .w_AZNOMFOR = space(20)
      .w_AZSEXFOR = space(1)
      .w_AZDATFOR = ctod("  /  /  ")
      .w_AZCONFOR = space(40)
      .w_AZPRNFOR = space(2)
      .w_AZDENFOR = space(60)
      .w_AZCOMFOR = space(40)
      .w_AZPROFOR = space(2)
      .w_AZINDFOR = space(35)
      .w_AZCAPFOR = space(5)
      .w_AZCOLFOR = space(40)
      .w_AZPRLFOR = space(2)
      .w_AZINLFOR = space(35)
      .w_AZCALFOR = space(5)
      .w_AZPLNODO = space(1)
      .w_AZPLADOC = space(1)
      .w_AZGIRIVA = space(2)
      .w_AZCAUIVA = space(5)
      .w_AZCONIVA = space(15)
      .w_AZCODVEN = space(15)
      .w_DESCRI = space(35)
      .w_AZNEWIVA = space(1)
      .w_DESCRIZ = space(40)
      .w_AZCAUGIR = space(5)
      .w_AZIVCONT = space(15)
      .w_AZCRERIP = space(15)
      .w_AZCRESPE = space(15)
      .w_AZINTDOV = space(15)
      .w_AZCONIND = space(15)
      .w_DESGIRIVA = space(35)
      .w_IVCONTDES = space(40)
      .w_DESCRERIP = space(40)
      .w_DESCRESPE = space(40)
      .w_DESINTDOV = space(40)
      .w_TIPOREG = space(1)
      .w_FLANAL = space(1)
      .w_TIPSOT1 = space(1)
      .w_TIPSOTC = space(1)
      .w_DESPIA = space(40)
      .w_TIPVEN = space(1)
      .w_PARVEN = space(1)
      .w_DESIND = space(15)
      .w_CAUINC = space(5)
      .w_AZCAUPAG = space(5)
      .w_AZDATCON = space(1)
      .w_AZCAUMTA = space(5)
      .w_AZCAUMTP = space(5)
      .w_AZCAUAUT = space(5)
      .w_AZIVAAUT = space(15)
      .w_AZDATAUT = ctod("  /  /  ")
      .w_DESINC = space(35)
      .w_DESPAG = space(35)
      .w_DESAUT = space(35)
      .w_DESCON = space(40)
      .w_AZCAUINC = space(5)
      .w_AZDESBAN = space(50)
      .w_ABIBAN = space(5)
      .w_CABBAN = space(5)
      .w_CONCOR = space(12)
      .w_BANPROABI = space(5)
      .w_BANPROCAB = space(5)
      .w_BANPROSIA = space(5)
      .w_BADESCRI = space(35)
      .w_BANPRO = space(15)
      .w_DESINCAT = space(35)
      .w_DESPAGPT = space(35)
      .w_DIFINCAT = space(1)
      .w_REGINCAT = space(1)
      .w_DIFPAGPT = space(1)
      .w_REGPAGPT = space(1)
      .w_AZRAGCOR = space(1)
      .w_AZCONPRO = space(15)
      .w_DESPRO = space(15)
      .w_AZFLGIND = space(1)
      .w_TIPSOTRC = space(1)
      .w_FLANAL2 = space(1)
      if .cFunction<>"Filter"
          .DoRTCalc(1,1,.f.)
        .w_UTCC = i_CODUTE
        .w_UTCV = i_CODUTE
        .w_UTDC = i_DATSYS
        .w_UTDV = i_DATSYS
        .w_OBTEST = i_datsys
        .DoRTCalc(7,11,.f.)
          if not(empty(.w_AZCATAZI))
          .link_1_11('Full')
          endif
          .DoRTCalc(12,13,.f.)
        .w_AZTIPDIC = 'N'
        .w_AZTIPDEN = 'T'
        .w_AZDENMAG = 0
        .w_AZSTAREG = 'V'
        .w_AZTIPINT = 'S'
        .w_AZPAGINT = 0
        .w_AZPREFIS = iif(.w_AZTIPINT='S',.w_AZPREFIS,' ')
        .DoRTCalc(21,22,.f.)
          if not(empty(.w_CONBAN))
          .link_1_23('Full')
          endif
          .DoRTCalc(23,34,.f.)
        .w_TIPCON = 'G'
          .DoRTCalc(36,59,.f.)
        .w_AZPLNODO = ' '
        .w_AZPLADOC = IIF(.w_AZPLNODO='S',' ',.w_AZPLADOC)
        .w_AZGIRIVA = 'NO'
        .DoRTCalc(63,63,.f.)
          if not(empty(.w_AZCAUIVA))
          .link_5_3('Full')
          endif
        .DoRTCalc(64,64,.f.)
          if not(empty(.w_AZCONIVA))
          .link_5_4('Full')
          endif
        .DoRTCalc(65,65,.f.)
          if not(empty(.w_AZCODVEN))
          .link_5_5('Full')
          endif
        .w_DESCRI = looktab('CAU_CONT','CCDESCRI','CCCODICE',.w_AZCAUIVA)
        .w_AZNEWIVA = 'N'
        .DoRTCalc(68,69,.f.)
          if not(empty(.w_AZCAUGIR))
          .link_5_13('Full')
          endif
        .DoRTCalc(70,70,.f.)
          if not(empty(.w_AZIVCONT))
          .link_5_14('Full')
          endif
        .DoRTCalc(71,71,.f.)
          if not(empty(.w_AZCRERIP))
          .link_5_15('Full')
          endif
        .DoRTCalc(72,72,.f.)
          if not(empty(.w_AZCRESPE))
          .link_5_16('Full')
          endif
        .DoRTCalc(73,73,.f.)
          if not(empty(.w_AZINTDOV))
          .link_5_17('Full')
          endif
        .DoRTCalc(74,74,.f.)
          if not(empty(.w_AZCONIND))
          .link_5_18('Full')
          endif
        .DoRTCalc(75,88,.f.)
          if not(empty(.w_CAUINC))
          .link_4_4('Full')
          endif
        .DoRTCalc(89,89,.f.)
          if not(empty(.w_AZCAUPAG))
          .link_4_6('Full')
          endif
        .w_AZDATCON = 'I'
        .DoRTCalc(91,91,.f.)
          if not(empty(.w_AZCAUMTA))
          .link_4_8('Full')
          endif
        .DoRTCalc(92,92,.f.)
          if not(empty(.w_AZCAUMTP))
          .link_4_9('Full')
          endif
        .DoRTCalc(93,93,.f.)
          if not(empty(.w_AZCAUAUT))
          .link_4_13('Full')
          endif
        .DoRTCalc(94,94,.f.)
          if not(empty(.w_AZIVAAUT))
          .link_4_14('Full')
          endif
        .DoRTCalc(95,109,.f.)
          if not(empty(.w_BANPRO))
          .link_1_65('Full')
          endif
        .DoRTCalc(110,117,.f.)
          if not(empty(.w_AZCONPRO))
          .link_5_45('Full')
          endif
          .DoRTCalc(118,118,.f.)
        .w_AZFLGIND = IIF(.w_AZNEWIVA='S','S',' ')
      endif
    endwith
    cp_BlankRecExtFlds(this,'AZIENDA')
    this.DoRTCalc(120,121,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_42.enabled = this.oPgFrm.Page1.oPag.oBtn_1_42.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oAZCODAZI_1_1.enabled = i_bVal
      .Page1.oPag.oAZIVAAZI_1_7.enabled = i_bVal
      .Page1.oPag.oAZIVACOF_1_8.enabled = i_bVal
      .Page1.oPag.oAZIVACON_1_9.enabled = i_bVal
      .Page1.oPag.oAZIVACAR_1_10.enabled = i_bVal
      .Page1.oPag.oAZCATAZI_1_11.enabled = i_bVal
      .Page1.oPag.oAZATTIVI_1_13.enabled = i_bVal
      .Page1.oPag.oAZTIPDIC_1_14.enabled = i_bVal
      .Page1.oPag.oAZTIPDEN_1_15.enabled = i_bVal
      .Page1.oPag.oAZDENMAG_1_16.enabled = i_bVal
      .Page1.oPag.oAZSTAREG_1_17.enabled = i_bVal
      .Page1.oPag.oAZTIPINT_1_18.enabled = i_bVal
      .Page1.oPag.oAZPAGINT_1_19.enabled = i_bVal
      .Page1.oPag.oAZPREFIS_1_20.enabled = i_bVal
      .Page1.oPag.oCONBAN_1_23.enabled = i_bVal
      .Page3.oPag.oAZTIPFOR_3_11.enabled = i_bVal
      .Page3.oPag.oAZFISFOR_3_13.enabled = i_bVal
      .Page3.oPag.oAZCOFFOR_3_14.enabled = i_bVal
      .Page3.oPag.oAZNUMCAF_3_15.enabled = i_bVal
      .Page3.oPag.oAZCOGFOR_3_16.enabled = i_bVal
      .Page3.oPag.oAZNOMFOR_3_17.enabled = i_bVal
      .Page3.oPag.oAZSEXFOR_3_18.enabled_(i_bVal)
      .Page3.oPag.oAZDATFOR_3_19.enabled = i_bVal
      .Page3.oPag.oAZCONFOR_3_20.enabled = i_bVal
      .Page3.oPag.oAZPRNFOR_3_21.enabled = i_bVal
      .Page3.oPag.oAZDENFOR_3_29.enabled = i_bVal
      .Page3.oPag.oAZCOMFOR_3_30.enabled = i_bVal
      .Page3.oPag.oAZPROFOR_3_31.enabled = i_bVal
      .Page3.oPag.oAZINDFOR_3_32.enabled = i_bVal
      .Page3.oPag.oAZCAPFOR_3_33.enabled = i_bVal
      .Page3.oPag.oAZCOLFOR_3_38.enabled = i_bVal
      .Page3.oPag.oAZPRLFOR_3_39.enabled = i_bVal
      .Page3.oPag.oAZINLFOR_3_40.enabled = i_bVal
      .Page3.oPag.oAZCALFOR_3_41.enabled = i_bVal
      .Page2.oPag.oAZPLNODO_2_2.enabled = i_bVal
      .Page2.oPag.oAZPLADOC_2_3.enabled = i_bVal
      .Page5.oPag.oAZGIRIVA_5_1.enabled = i_bVal
      .Page5.oPag.oAZCAUIVA_5_3.enabled = i_bVal
      .Page5.oPag.oAZCONIVA_5_4.enabled = i_bVal
      .Page5.oPag.oAZCODVEN_5_5.enabled = i_bVal
      .Page5.oPag.oAZNEWIVA_5_7.enabled = i_bVal
      .Page5.oPag.oAZCAUGIR_5_13.enabled = i_bVal
      .Page5.oPag.oAZIVCONT_5_14.enabled = i_bVal
      .Page5.oPag.oAZCRERIP_5_15.enabled = i_bVal
      .Page5.oPag.oAZCRESPE_5_16.enabled = i_bVal
      .Page5.oPag.oAZINTDOV_5_17.enabled = i_bVal
      .Page5.oPag.oAZCONIND_5_18.enabled = i_bVal
      .Page4.oPag.oCAUINC_4_4.enabled = i_bVal
      .Page4.oPag.oAZCAUPAG_4_6.enabled = i_bVal
      .Page4.oPag.oAZDATCON_4_7.enabled = i_bVal
      .Page4.oPag.oAZCAUMTA_4_8.enabled = i_bVal
      .Page4.oPag.oAZCAUMTP_4_9.enabled = i_bVal
      .Page4.oPag.oAZCAUAUT_4_13.enabled = i_bVal
      .Page4.oPag.oAZIVAAUT_4_14.enabled = i_bVal
      .Page4.oPag.oAZDATAUT_4_16.enabled = i_bVal
      .Page1.oPag.oBANPRO_1_65.enabled = i_bVal
      .Page1.oPag.oAZRAGCOR_1_71.enabled = i_bVal
      .Page5.oPag.oAZCONPRO_5_45.enabled = i_bVal
      .Page5.oPag.oAZFLGIND_5_48.enabled = i_bVal
      .Page1.oPag.oBtn_1_41.enabled = i_bVal
      .Page1.oPag.oBtn_1_42.enabled = .Page1.oPag.oBtn_1_42.mCond()
      if i_cOp = "Edit"
        .Page1.oPag.oAZCODAZI_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oAZCODAZI_1_1.enabled = .t.
      endif
    endwith
    this.GSCG_MDI.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'AZIENDA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCG_MDI.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCODAZI,"AZCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZIVAAZI,"AZIVAAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZIVACOF,"AZIVACOF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZIVACON,"AZIVACON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZIVACAR,"AZIVACAR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCATAZI,"AZCATAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZATTIVI,"AZATTIVI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZTIPDIC,"AZTIPDIC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZTIPDEN,"AZTIPDEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZDENMAG,"AZDENMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZSTAREG,"AZSTAREG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZTIPINT,"AZTIPINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPAGINT,"AZPAGINT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPREFIS,"AZPREFIS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCONBAN,"AZCONBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZBANPRO,"AZBANPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZTIPFOR,"AZTIPFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZFISFOR,"AZFISFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCOFFOR,"AZCOFFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZNUMCAF,"AZNUMCAF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCOGFOR,"AZCOGFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZNOMFOR,"AZNOMFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZSEXFOR,"AZSEXFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZDATFOR,"AZDATFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCONFOR,"AZCONFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPRNFOR,"AZPRNFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZDENFOR,"AZDENFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCOMFOR,"AZCOMFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPROFOR,"AZPROFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZINDFOR,"AZINDFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCAPFOR,"AZCAPFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCOLFOR,"AZCOLFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPRLFOR,"AZPRLFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZINLFOR,"AZINLFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCALFOR,"AZCALFOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPLNODO,"AZPLNODO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZPLADOC,"AZPLADOC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZGIRIVA,"AZGIRIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCAUIVA,"AZCAUIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCONIVA,"AZCONIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCODVEN,"AZCODVEN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZNEWIVA,"AZNEWIVA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCAUGIR,"AZCAUGIR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZIVCONT,"AZIVCONT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCRERIP,"AZCRERIP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCRESPE,"AZCRESPE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZINTDOV,"AZINTDOV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCONIND,"AZCONIND",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCAUPAG,"AZCAUPAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZDATCON,"AZDATCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCAUMTA,"AZCAUMTA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCAUMTP,"AZCAUMTP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCAUAUT,"AZCAUAUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZIVAAUT,"AZIVAAUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZDATAUT,"AZDATAUT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCAUINC,"AZCAUINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZDESBAN,"AZDESBAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZRAGCOR,"AZRAGCOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZCONPRO,"AZCONPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_AZFLGIND,"AZFLGIND",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.AZIENDA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.AZIENDA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into AZIENDA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'AZIENDA')
        i_extval=cp_InsertValODBCExtFlds(this,'AZIENDA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(AZCODAZI,UTCC,UTCV,UTDC,UTDV"+;
                  ",AZIVAAZI,AZIVACOF,AZIVACON,AZIVACAR,AZCATAZI"+;
                  ",AZATTIVI,AZTIPDIC,AZTIPDEN,AZDENMAG,AZSTAREG"+;
                  ",AZTIPINT,AZPAGINT,AZPREFIS,AZCONBAN,AZBANPRO"+;
                  ",AZTIPFOR,AZFISFOR,AZCOFFOR,AZNUMCAF,AZCOGFOR"+;
                  ",AZNOMFOR,AZSEXFOR,AZDATFOR,AZCONFOR,AZPRNFOR"+;
                  ",AZDENFOR,AZCOMFOR,AZPROFOR,AZINDFOR,AZCAPFOR"+;
                  ",AZCOLFOR,AZPRLFOR,AZINLFOR,AZCALFOR,AZPLNODO"+;
                  ",AZPLADOC,AZGIRIVA,AZCAUIVA,AZCONIVA,AZCODVEN"+;
                  ",AZNEWIVA,AZCAUGIR,AZIVCONT,AZCRERIP,AZCRESPE"+;
                  ",AZINTDOV,AZCONIND,AZCAUPAG,AZDATCON,AZCAUMTA"+;
                  ",AZCAUMTP,AZCAUAUT,AZIVAAUT,AZDATAUT,AZCAUINC"+;
                  ",AZDESBAN,AZRAGCOR,AZCONPRO,AZFLGIND "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_AZCODAZI)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_AZIVAAZI)+;
                  ","+cp_ToStrODBC(this.w_AZIVACOF)+;
                  ","+cp_ToStrODBC(this.w_AZIVACON)+;
                  ","+cp_ToStrODBC(this.w_AZIVACAR)+;
                  ","+cp_ToStrODBCNull(this.w_AZCATAZI)+;
                  ","+cp_ToStrODBC(this.w_AZATTIVI)+;
                  ","+cp_ToStrODBC(this.w_AZTIPDIC)+;
                  ","+cp_ToStrODBC(this.w_AZTIPDEN)+;
                  ","+cp_ToStrODBC(this.w_AZDENMAG)+;
                  ","+cp_ToStrODBC(this.w_AZSTAREG)+;
                  ","+cp_ToStrODBC(this.w_AZTIPINT)+;
                  ","+cp_ToStrODBC(this.w_AZPAGINT)+;
                  ","+cp_ToStrODBC(this.w_AZPREFIS)+;
                  ","+cp_ToStrODBC(this.w_AZCONBAN)+;
                  ","+cp_ToStrODBC(this.w_AZBANPRO)+;
                  ","+cp_ToStrODBC(this.w_AZTIPFOR)+;
                  ","+cp_ToStrODBC(this.w_AZFISFOR)+;
                  ","+cp_ToStrODBC(this.w_AZCOFFOR)+;
                  ","+cp_ToStrODBC(this.w_AZNUMCAF)+;
                  ","+cp_ToStrODBC(this.w_AZCOGFOR)+;
                  ","+cp_ToStrODBC(this.w_AZNOMFOR)+;
                  ","+cp_ToStrODBC(this.w_AZSEXFOR)+;
                  ","+cp_ToStrODBC(this.w_AZDATFOR)+;
                  ","+cp_ToStrODBC(this.w_AZCONFOR)+;
                  ","+cp_ToStrODBC(this.w_AZPRNFOR)+;
                  ","+cp_ToStrODBC(this.w_AZDENFOR)+;
                  ","+cp_ToStrODBC(this.w_AZCOMFOR)+;
                  ","+cp_ToStrODBC(this.w_AZPROFOR)+;
                  ","+cp_ToStrODBC(this.w_AZINDFOR)+;
                  ","+cp_ToStrODBC(this.w_AZCAPFOR)+;
                  ","+cp_ToStrODBC(this.w_AZCOLFOR)+;
                  ","+cp_ToStrODBC(this.w_AZPRLFOR)+;
                  ","+cp_ToStrODBC(this.w_AZINLFOR)+;
                  ","+cp_ToStrODBC(this.w_AZCALFOR)+;
                  ","+cp_ToStrODBC(this.w_AZPLNODO)+;
                  ","+cp_ToStrODBC(this.w_AZPLADOC)+;
                  ","+cp_ToStrODBC(this.w_AZGIRIVA)+;
                  ","+cp_ToStrODBCNull(this.w_AZCAUIVA)+;
                  ","+cp_ToStrODBCNull(this.w_AZCONIVA)+;
                  ","+cp_ToStrODBCNull(this.w_AZCODVEN)+;
                  ","+cp_ToStrODBC(this.w_AZNEWIVA)+;
                  ","+cp_ToStrODBCNull(this.w_AZCAUGIR)+;
                  ","+cp_ToStrODBCNull(this.w_AZIVCONT)+;
                  ","+cp_ToStrODBCNull(this.w_AZCRERIP)+;
                  ","+cp_ToStrODBCNull(this.w_AZCRESPE)+;
                  ","+cp_ToStrODBCNull(this.w_AZINTDOV)+;
                  ","+cp_ToStrODBCNull(this.w_AZCONIND)+;
                  ","+cp_ToStrODBCNull(this.w_AZCAUPAG)+;
                  ","+cp_ToStrODBC(this.w_AZDATCON)+;
                  ","+cp_ToStrODBCNull(this.w_AZCAUMTA)+;
                  ","+cp_ToStrODBCNull(this.w_AZCAUMTP)+;
                  ","+cp_ToStrODBCNull(this.w_AZCAUAUT)+;
                  ","+cp_ToStrODBCNull(this.w_AZIVAAUT)+;
                  ","+cp_ToStrODBC(this.w_AZDATAUT)+;
                  ","+cp_ToStrODBC(this.w_AZCAUINC)+;
                  ","+cp_ToStrODBC(this.w_AZDESBAN)+;
                  ","+cp_ToStrODBC(this.w_AZRAGCOR)+;
                  ","+cp_ToStrODBCNull(this.w_AZCONPRO)+;
                  ","+cp_ToStrODBC(this.w_AZFLGIND)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'AZIENDA')
        i_extval=cp_InsertValVFPExtFlds(this,'AZIENDA')
        cp_CheckDeletedKey(i_cTable,0,'AZCODAZI',this.w_AZCODAZI)
        INSERT INTO (i_cTable);
              (AZCODAZI,UTCC,UTCV,UTDC,UTDV,AZIVAAZI,AZIVACOF,AZIVACON,AZIVACAR,AZCATAZI,AZATTIVI,AZTIPDIC,AZTIPDEN,AZDENMAG,AZSTAREG,AZTIPINT,AZPAGINT,AZPREFIS,AZCONBAN,AZBANPRO,AZTIPFOR,AZFISFOR,AZCOFFOR,AZNUMCAF,AZCOGFOR,AZNOMFOR,AZSEXFOR,AZDATFOR,AZCONFOR,AZPRNFOR,AZDENFOR,AZCOMFOR,AZPROFOR,AZINDFOR,AZCAPFOR,AZCOLFOR,AZPRLFOR,AZINLFOR,AZCALFOR,AZPLNODO,AZPLADOC,AZGIRIVA,AZCAUIVA,AZCONIVA,AZCODVEN,AZNEWIVA,AZCAUGIR,AZIVCONT,AZCRERIP,AZCRESPE,AZINTDOV,AZCONIND,AZCAUPAG,AZDATCON,AZCAUMTA,AZCAUMTP,AZCAUAUT,AZIVAAUT,AZDATAUT,AZCAUINC,AZDESBAN,AZRAGCOR,AZCONPRO,AZFLGIND  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_AZCODAZI;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_AZIVAAZI;
                  ,this.w_AZIVACOF;
                  ,this.w_AZIVACON;
                  ,this.w_AZIVACAR;
                  ,this.w_AZCATAZI;
                  ,this.w_AZATTIVI;
                  ,this.w_AZTIPDIC;
                  ,this.w_AZTIPDEN;
                  ,this.w_AZDENMAG;
                  ,this.w_AZSTAREG;
                  ,this.w_AZTIPINT;
                  ,this.w_AZPAGINT;
                  ,this.w_AZPREFIS;
                  ,this.w_AZCONBAN;
                  ,this.w_AZBANPRO;
                  ,this.w_AZTIPFOR;
                  ,this.w_AZFISFOR;
                  ,this.w_AZCOFFOR;
                  ,this.w_AZNUMCAF;
                  ,this.w_AZCOGFOR;
                  ,this.w_AZNOMFOR;
                  ,this.w_AZSEXFOR;
                  ,this.w_AZDATFOR;
                  ,this.w_AZCONFOR;
                  ,this.w_AZPRNFOR;
                  ,this.w_AZDENFOR;
                  ,this.w_AZCOMFOR;
                  ,this.w_AZPROFOR;
                  ,this.w_AZINDFOR;
                  ,this.w_AZCAPFOR;
                  ,this.w_AZCOLFOR;
                  ,this.w_AZPRLFOR;
                  ,this.w_AZINLFOR;
                  ,this.w_AZCALFOR;
                  ,this.w_AZPLNODO;
                  ,this.w_AZPLADOC;
                  ,this.w_AZGIRIVA;
                  ,this.w_AZCAUIVA;
                  ,this.w_AZCONIVA;
                  ,this.w_AZCODVEN;
                  ,this.w_AZNEWIVA;
                  ,this.w_AZCAUGIR;
                  ,this.w_AZIVCONT;
                  ,this.w_AZCRERIP;
                  ,this.w_AZCRESPE;
                  ,this.w_AZINTDOV;
                  ,this.w_AZCONIND;
                  ,this.w_AZCAUPAG;
                  ,this.w_AZDATCON;
                  ,this.w_AZCAUMTA;
                  ,this.w_AZCAUMTP;
                  ,this.w_AZCAUAUT;
                  ,this.w_AZIVAAUT;
                  ,this.w_AZDATAUT;
                  ,this.w_AZCAUINC;
                  ,this.w_AZDESBAN;
                  ,this.w_AZRAGCOR;
                  ,this.w_AZCONPRO;
                  ,this.w_AZFLGIND;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.AZIENDA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.AZIENDA_IDX,i_nConn)
      *
      * update AZIENDA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'AZIENDA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",AZIVAAZI="+cp_ToStrODBC(this.w_AZIVAAZI)+;
             ",AZIVACOF="+cp_ToStrODBC(this.w_AZIVACOF)+;
             ",AZIVACON="+cp_ToStrODBC(this.w_AZIVACON)+;
             ",AZIVACAR="+cp_ToStrODBC(this.w_AZIVACAR)+;
             ",AZCATAZI="+cp_ToStrODBCNull(this.w_AZCATAZI)+;
             ",AZATTIVI="+cp_ToStrODBC(this.w_AZATTIVI)+;
             ",AZTIPDIC="+cp_ToStrODBC(this.w_AZTIPDIC)+;
             ",AZTIPDEN="+cp_ToStrODBC(this.w_AZTIPDEN)+;
             ",AZDENMAG="+cp_ToStrODBC(this.w_AZDENMAG)+;
             ",AZSTAREG="+cp_ToStrODBC(this.w_AZSTAREG)+;
             ",AZTIPINT="+cp_ToStrODBC(this.w_AZTIPINT)+;
             ",AZPAGINT="+cp_ToStrODBC(this.w_AZPAGINT)+;
             ",AZPREFIS="+cp_ToStrODBC(this.w_AZPREFIS)+;
             ",AZCONBAN="+cp_ToStrODBC(this.w_AZCONBAN)+;
             ",AZBANPRO="+cp_ToStrODBC(this.w_AZBANPRO)+;
             ",AZTIPFOR="+cp_ToStrODBC(this.w_AZTIPFOR)+;
             ",AZFISFOR="+cp_ToStrODBC(this.w_AZFISFOR)+;
             ",AZCOFFOR="+cp_ToStrODBC(this.w_AZCOFFOR)+;
             ",AZNUMCAF="+cp_ToStrODBC(this.w_AZNUMCAF)+;
             ",AZCOGFOR="+cp_ToStrODBC(this.w_AZCOGFOR)+;
             ",AZNOMFOR="+cp_ToStrODBC(this.w_AZNOMFOR)+;
             ",AZSEXFOR="+cp_ToStrODBC(this.w_AZSEXFOR)+;
             ",AZDATFOR="+cp_ToStrODBC(this.w_AZDATFOR)+;
             ",AZCONFOR="+cp_ToStrODBC(this.w_AZCONFOR)+;
             ",AZPRNFOR="+cp_ToStrODBC(this.w_AZPRNFOR)+;
             ",AZDENFOR="+cp_ToStrODBC(this.w_AZDENFOR)+;
             ",AZCOMFOR="+cp_ToStrODBC(this.w_AZCOMFOR)+;
             ",AZPROFOR="+cp_ToStrODBC(this.w_AZPROFOR)+;
             ",AZINDFOR="+cp_ToStrODBC(this.w_AZINDFOR)+;
             ",AZCAPFOR="+cp_ToStrODBC(this.w_AZCAPFOR)+;
             ",AZCOLFOR="+cp_ToStrODBC(this.w_AZCOLFOR)+;
             ",AZPRLFOR="+cp_ToStrODBC(this.w_AZPRLFOR)+;
             ",AZINLFOR="+cp_ToStrODBC(this.w_AZINLFOR)+;
             ",AZCALFOR="+cp_ToStrODBC(this.w_AZCALFOR)+;
             ",AZPLNODO="+cp_ToStrODBC(this.w_AZPLNODO)+;
             ",AZPLADOC="+cp_ToStrODBC(this.w_AZPLADOC)+;
             ",AZGIRIVA="+cp_ToStrODBC(this.w_AZGIRIVA)+;
             ",AZCAUIVA="+cp_ToStrODBCNull(this.w_AZCAUIVA)+;
             ",AZCONIVA="+cp_ToStrODBCNull(this.w_AZCONIVA)+;
             ",AZCODVEN="+cp_ToStrODBCNull(this.w_AZCODVEN)+;
             ",AZNEWIVA="+cp_ToStrODBC(this.w_AZNEWIVA)+;
             ",AZCAUGIR="+cp_ToStrODBCNull(this.w_AZCAUGIR)+;
             ",AZIVCONT="+cp_ToStrODBCNull(this.w_AZIVCONT)+;
             ",AZCRERIP="+cp_ToStrODBCNull(this.w_AZCRERIP)+;
             ",AZCRESPE="+cp_ToStrODBCNull(this.w_AZCRESPE)+;
             ",AZINTDOV="+cp_ToStrODBCNull(this.w_AZINTDOV)+;
             ",AZCONIND="+cp_ToStrODBCNull(this.w_AZCONIND)+;
             ",AZCAUPAG="+cp_ToStrODBCNull(this.w_AZCAUPAG)+;
             ",AZDATCON="+cp_ToStrODBC(this.w_AZDATCON)+;
             ",AZCAUMTA="+cp_ToStrODBCNull(this.w_AZCAUMTA)+;
             ",AZCAUMTP="+cp_ToStrODBCNull(this.w_AZCAUMTP)+;
             ",AZCAUAUT="+cp_ToStrODBCNull(this.w_AZCAUAUT)+;
             ",AZIVAAUT="+cp_ToStrODBCNull(this.w_AZIVAAUT)+;
             ",AZDATAUT="+cp_ToStrODBC(this.w_AZDATAUT)+;
             ",AZCAUINC="+cp_ToStrODBC(this.w_AZCAUINC)+;
             ",AZDESBAN="+cp_ToStrODBC(this.w_AZDESBAN)+;
             ",AZRAGCOR="+cp_ToStrODBC(this.w_AZRAGCOR)+;
             ",AZCONPRO="+cp_ToStrODBCNull(this.w_AZCONPRO)+;
             ",AZFLGIND="+cp_ToStrODBC(this.w_AZFLGIND)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'AZIENDA')
        i_cWhere = cp_PKFox(i_cTable  ,'AZCODAZI',this.w_AZCODAZI  )
        UPDATE (i_cTable) SET;
              UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,AZIVAAZI=this.w_AZIVAAZI;
             ,AZIVACOF=this.w_AZIVACOF;
             ,AZIVACON=this.w_AZIVACON;
             ,AZIVACAR=this.w_AZIVACAR;
             ,AZCATAZI=this.w_AZCATAZI;
             ,AZATTIVI=this.w_AZATTIVI;
             ,AZTIPDIC=this.w_AZTIPDIC;
             ,AZTIPDEN=this.w_AZTIPDEN;
             ,AZDENMAG=this.w_AZDENMAG;
             ,AZSTAREG=this.w_AZSTAREG;
             ,AZTIPINT=this.w_AZTIPINT;
             ,AZPAGINT=this.w_AZPAGINT;
             ,AZPREFIS=this.w_AZPREFIS;
             ,AZCONBAN=this.w_AZCONBAN;
             ,AZBANPRO=this.w_AZBANPRO;
             ,AZTIPFOR=this.w_AZTIPFOR;
             ,AZFISFOR=this.w_AZFISFOR;
             ,AZCOFFOR=this.w_AZCOFFOR;
             ,AZNUMCAF=this.w_AZNUMCAF;
             ,AZCOGFOR=this.w_AZCOGFOR;
             ,AZNOMFOR=this.w_AZNOMFOR;
             ,AZSEXFOR=this.w_AZSEXFOR;
             ,AZDATFOR=this.w_AZDATFOR;
             ,AZCONFOR=this.w_AZCONFOR;
             ,AZPRNFOR=this.w_AZPRNFOR;
             ,AZDENFOR=this.w_AZDENFOR;
             ,AZCOMFOR=this.w_AZCOMFOR;
             ,AZPROFOR=this.w_AZPROFOR;
             ,AZINDFOR=this.w_AZINDFOR;
             ,AZCAPFOR=this.w_AZCAPFOR;
             ,AZCOLFOR=this.w_AZCOLFOR;
             ,AZPRLFOR=this.w_AZPRLFOR;
             ,AZINLFOR=this.w_AZINLFOR;
             ,AZCALFOR=this.w_AZCALFOR;
             ,AZPLNODO=this.w_AZPLNODO;
             ,AZPLADOC=this.w_AZPLADOC;
             ,AZGIRIVA=this.w_AZGIRIVA;
             ,AZCAUIVA=this.w_AZCAUIVA;
             ,AZCONIVA=this.w_AZCONIVA;
             ,AZCODVEN=this.w_AZCODVEN;
             ,AZNEWIVA=this.w_AZNEWIVA;
             ,AZCAUGIR=this.w_AZCAUGIR;
             ,AZIVCONT=this.w_AZIVCONT;
             ,AZCRERIP=this.w_AZCRERIP;
             ,AZCRESPE=this.w_AZCRESPE;
             ,AZINTDOV=this.w_AZINTDOV;
             ,AZCONIND=this.w_AZCONIND;
             ,AZCAUPAG=this.w_AZCAUPAG;
             ,AZDATCON=this.w_AZDATCON;
             ,AZCAUMTA=this.w_AZCAUMTA;
             ,AZCAUMTP=this.w_AZCAUMTP;
             ,AZCAUAUT=this.w_AZCAUAUT;
             ,AZIVAAUT=this.w_AZIVAAUT;
             ,AZDATAUT=this.w_AZDATAUT;
             ,AZCAUINC=this.w_AZCAUINC;
             ,AZDESBAN=this.w_AZDESBAN;
             ,AZRAGCOR=this.w_AZRAGCOR;
             ,AZCONPRO=this.w_AZCONPRO;
             ,AZFLGIND=this.w_AZFLGIND;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCG_MDI : Saving
      this.GSCG_MDI.ChangeRow(this.cRowID+'      1',0;
             ,this.w_AZCODAZI,"IACODAZI";
             )
      this.GSCG_MDI.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    * --- GSCG_MDI : Deleting
    this.GSCG_MDI.ChangeRow(this.cRowID+'      1',0;
           ,this.w_AZCODAZI,"IACODAZI";
           )
    this.GSCG_MDI.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.AZIENDA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.AZIENDA_IDX,i_nConn)
      *
      * delete AZIENDA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'AZCODAZI',this.w_AZCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
            .w_UTCV = i_CODUTE
        .DoRTCalc(4,4,.t.)
            .w_UTDV = i_DATSYS
        .DoRTCalc(6,15,.t.)
        if .o_AZTIPDEN<>.w_AZTIPDEN
            .w_AZDENMAG = 0
        endif
        .DoRTCalc(17,18,.t.)
        if .o_AZTIPINT<>.w_AZTIPINT
            .w_AZPAGINT = 0
        endif
        if .o_AZTIPINT<>.w_AZTIPINT
            .w_AZPREFIS = iif(.w_AZTIPINT='S',.w_AZPREFIS,' ')
        endif
        .DoRTCalc(21,60,.t.)
        if .o_AZPLNODO<>.w_AZPLNODO
            .w_AZPLADOC = IIF(.w_AZPLNODO='S',' ',.w_AZPLADOC)
        endif
        .DoRTCalc(62,65,.t.)
        if .o_AZCAUIVA<>.w_AZCAUIVA
            .w_DESCRI = looktab('CAU_CONT','CCDESCRI','CCCODICE',.w_AZCAUIVA)
        endif
        .DoRTCalc(67,118,.t.)
        if .o_AZNEWIVA<>.w_AZNEWIVA
            .w_AZFLGIND = IIF(.w_AZNEWIVA='S','S',' ')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(120,121,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_HQDKOTOTEU()
    with this
          * --- Valorizzo azcauinc
          .w_AZCAUINC = .w_CAUINC
    endwith
  endproc
  proc Calculate_IFTNYZTKEV()
    with this
          * --- Valorizzo cauinc
          .w_CAUINC = .w_AZCAUINC
          .link_4_4('Full')
    endwith
  endproc
  proc Calculate_JERRVWLUEM()
    with this
          * --- Valorizzo azconban
          .w_AZCONBAN = .w_CONBAN
    endwith
  endproc
  proc Calculate_FJVBSKTGOC()
    with this
          * --- Valorizzo conban
          .w_CONBAN = .w_AZCONBAN
          .link_1_23('Full')
    endwith
  endproc
  proc Calculate_OTPSWALBLJ()
    with this
          * --- Valorizzo azbanpro
          .w_AZBANPRO = .w_BANPRO
    endwith
  endproc
  proc Calculate_RUININESPD()
    with this
          * --- Valorizzo banpro
          .w_BANPRO = .w_AZBANPRO
          .link_1_65('Full')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oAZDENMAG_1_16.enabled = this.oPgFrm.Page1.oPag.oAZDENMAG_1_16.mCond()
    this.oPgFrm.Page1.oPag.oAZPAGINT_1_19.enabled = this.oPgFrm.Page1.oPag.oAZPAGINT_1_19.mCond()
    this.oPgFrm.Page3.oPag.oAZCOGFOR_3_16.enabled = this.oPgFrm.Page3.oPag.oAZCOGFOR_3_16.mCond()
    this.oPgFrm.Page3.oPag.oAZNOMFOR_3_17.enabled = this.oPgFrm.Page3.oPag.oAZNOMFOR_3_17.mCond()
    this.oPgFrm.Page3.oPag.oAZSEXFOR_3_18.enabled_(this.oPgFrm.Page3.oPag.oAZSEXFOR_3_18.mCond())
    this.oPgFrm.Page3.oPag.oAZDATFOR_3_19.enabled = this.oPgFrm.Page3.oPag.oAZDATFOR_3_19.mCond()
    this.oPgFrm.Page3.oPag.oAZCONFOR_3_20.enabled = this.oPgFrm.Page3.oPag.oAZCONFOR_3_20.mCond()
    this.oPgFrm.Page3.oPag.oAZPRNFOR_3_21.enabled = this.oPgFrm.Page3.oPag.oAZPRNFOR_3_21.mCond()
    this.oPgFrm.Page5.oPag.oAZINTDOV_5_17.enabled = this.oPgFrm.Page5.oPag.oAZINTDOV_5_17.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oAZPAGINT_1_19.visible=!this.oPgFrm.Page1.oPag.oAZPAGINT_1_19.mHide()
    this.oPgFrm.Page1.oPag.oAZPREFIS_1_20.visible=!this.oPgFrm.Page1.oPag.oAZPREFIS_1_20.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page2.oPag.oAZPLADOC_2_3.visible=!this.oPgFrm.Page2.oPag.oAZPLADOC_2_3.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page5.oPag.oAZCODVEN_5_5.visible=!this.oPgFrm.Page5.oPag.oAZCODVEN_5_5.mHide()
    this.oPgFrm.Page5.oPag.oAZCAUGIR_5_13.visible=!this.oPgFrm.Page5.oPag.oAZCAUGIR_5_13.mHide()
    this.oPgFrm.Page5.oPag.oAZIVCONT_5_14.visible=!this.oPgFrm.Page5.oPag.oAZIVCONT_5_14.mHide()
    this.oPgFrm.Page5.oPag.oAZCRERIP_5_15.visible=!this.oPgFrm.Page5.oPag.oAZCRERIP_5_15.mHide()
    this.oPgFrm.Page5.oPag.oAZCRESPE_5_16.visible=!this.oPgFrm.Page5.oPag.oAZCRESPE_5_16.mHide()
    this.oPgFrm.Page5.oPag.oAZINTDOV_5_17.visible=!this.oPgFrm.Page5.oPag.oAZINTDOV_5_17.mHide()
    this.oPgFrm.Page5.oPag.oAZCONIND_5_18.visible=!this.oPgFrm.Page5.oPag.oAZCONIND_5_18.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_19.visible=!this.oPgFrm.Page5.oPag.oStr_5_19.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_20.visible=!this.oPgFrm.Page5.oPag.oStr_5_20.mHide()
    this.oPgFrm.Page5.oPag.oDESGIRIVA_5_21.visible=!this.oPgFrm.Page5.oPag.oDESGIRIVA_5_21.mHide()
    this.oPgFrm.Page5.oPag.oIVCONTDES_5_22.visible=!this.oPgFrm.Page5.oPag.oIVCONTDES_5_22.mHide()
    this.oPgFrm.Page5.oPag.oDESCRERIP_5_23.visible=!this.oPgFrm.Page5.oPag.oDESCRERIP_5_23.mHide()
    this.oPgFrm.Page5.oPag.oDESCRESPE_5_24.visible=!this.oPgFrm.Page5.oPag.oDESCRESPE_5_24.mHide()
    this.oPgFrm.Page5.oPag.oDESINTDOV_5_25.visible=!this.oPgFrm.Page5.oPag.oDESINTDOV_5_25.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_26.visible=!this.oPgFrm.Page5.oPag.oStr_5_26.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_27.visible=!this.oPgFrm.Page5.oPag.oStr_5_27.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_28.visible=!this.oPgFrm.Page5.oPag.oStr_5_28.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_30.visible=!this.oPgFrm.Page5.oPag.oStr_5_30.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_36.visible=!this.oPgFrm.Page5.oPag.oStr_5_36.mHide()
    this.oPgFrm.Page5.oPag.oDESPIA_5_37.visible=!this.oPgFrm.Page5.oPag.oDESPIA_5_37.mHide()
    this.oPgFrm.Page5.oPag.oDESIND_5_40.visible=!this.oPgFrm.Page5.oPag.oDESIND_5_40.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_41.visible=!this.oPgFrm.Page5.oPag.oStr_5_41.mHide()
    this.oPgFrm.Page5.oPag.oStr_5_42.visible=!this.oPgFrm.Page5.oPag.oStr_5_42.mHide()
    this.oPgFrm.Page1.oPag.oAZDESBAN_1_47.visible=!this.oPgFrm.Page1.oPag.oAZDESBAN_1_47.mHide()
    this.oPgFrm.Page1.oPag.oABIBAN_1_51.visible=!this.oPgFrm.Page1.oPag.oABIBAN_1_51.mHide()
    this.oPgFrm.Page1.oPag.oCABBAN_1_52.visible=!this.oPgFrm.Page1.oPag.oCABBAN_1_52.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_68.visible=!this.oPgFrm.Page1.oPag.oStr_1_68.mHide()
    this.oPgFrm.Page5.oPag.oAZFLGIND_5_48.visible=!this.oPgFrm.Page5.oPag.oAZFLGIND_5_48.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("w_CAUINC Changed")
          .Calculate_HQDKOTOTEU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_IFTNYZTKEV()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_CONBAN Changed")
          .Calculate_JERRVWLUEM()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_FJVBSKTGOC()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("w_BANPRO Changed")
          .Calculate_OTPSWALBLJ()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Load")
          .Calculate_RUININESPD()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=AZCATAZI
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ATTIMAST_IDX,3]
    i_lTable = "ATTIMAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2], .t., this.ATTIMAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCATAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_MAT',True,'ATTIMAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ATCODATT like "+cp_ToStrODBC(trim(this.w_AZCATAZI)+"%");

          i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ATCODATT","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ATCODATT',trim(this.w_AZCATAZI))
          select ATCODATT,ATDESATT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ATCODATT into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZCATAZI)==trim(_Link_.ATCODATT) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZCATAZI) and !this.bDontReportError
            deferred_cp_zoom('ATTIMAST','*','ATCODATT',cp_AbsName(oSource.parent,'oAZCATAZI_1_11'),i_cWhere,'GSAR_MAT',"Attivit�",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                     +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',oSource.xKey(1))
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCATAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ATCODATT,ATDESATT";
                   +" from "+i_cTable+" "+i_lTable+" where ATCODATT="+cp_ToStrODBC(this.w_AZCATAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ATCODATT',this.w_AZCATAZI)
            select ATCODATT,ATDESATT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCATAZI = NVL(_Link_.ATCODATT,space(5))
      this.w_DESATT = NVL(_Link_.ATDESATT,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_AZCATAZI = space(5)
      endif
      this.w_DESATT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])+'\'+cp_ToStr(_Link_.ATCODATT,1)
      cp_ShowWarn(i_cKey,this.ATTIMAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCATAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ATTIMAST_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ATTIMAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_11.ATCODATT as ATCODATT111"+ ",link_1_11.ATDESATT as ATDESATT111"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_11 on AZIENDA.AZCATAZI=link_1_11.ATCODATT"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_11"
          i_cKey=i_cKey+'+" and AZIENDA.AZCATAZI=link_1_11.ATCODATT(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CONBAN
  func Link_1_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CONBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_CONBAN)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB,BATIPCON,BACONCOR,BADTOBSO,BACONSBF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_CONBAN))
          select BACODBAN,BADESCRI,BACODABI,BACODCAB,BATIPCON,BACONCOR,BADTOBSO,BACONSBF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CONBAN)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CONBAN) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oCONBAN_1_23'),i_cWhere,'GSTE_ACB',"Conti banche",'GSCG_API.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB,BATIPCON,BACONCOR,BADTOBSO,BACONSBF";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BACODABI,BACODCAB,BATIPCON,BACONCOR,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CONBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACODABI,BACODCAB,BATIPCON,BACONCOR,BADTOBSO,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CONBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CONBAN)
            select BACODBAN,BADESCRI,BACODABI,BACODCAB,BATIPCON,BACONCOR,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CONBAN = NVL(_Link_.BACODBAN,space(10))
      this.w_AZDESBAN = NVL(_Link_.BADESCRI,space(50))
      this.w_ABIBAN = NVL(_Link_.BACODABI,space(5))
      this.w_CABBAN = NVL(_Link_.BACODCAB,space(5))
      this.w_BATIPCON = NVL(_Link_.BATIPCON,space(1))
      this.w_CONCOR = NVL(_Link_.BACONCOR,space(12))
      this.w_DATOBSO1 = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
      this.w_BACONSBF = NVL(_Link_.BACONSBF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CONBAN = space(10)
      endif
      this.w_AZDESBAN = space(50)
      this.w_ABIBAN = space(5)
      this.w_CABBAN = space(5)
      this.w_BATIPCON = space(1)
      this.w_CONCOR = space(12)
      this.w_DATOBSO1 = ctod("  /  /  ")
      this.w_BACONSBF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_BATIPCON<>'S' AND (EMPTY(.w_DATOBSO1) OR .w_DATOBSO1>.w_OBTEST) AND .w_BACONSBF='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice banca inesistente, obsoleta, di compensazione o salvo buon fine")
        endif
        this.w_CONBAN = space(10)
        this.w_AZDESBAN = space(50)
        this.w_ABIBAN = space(5)
        this.w_CABBAN = space(5)
        this.w_BATIPCON = space(1)
        this.w_CONCOR = space(12)
        this.w_DATOBSO1 = ctod("  /  /  ")
        this.w_BACONSBF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CONBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZCAUIVA
  func Link_5_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCAUIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_AZCAUIVA)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCFLANAL,CCTIPREG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_AZCAUIVA))
          select CCCODICE,CCFLANAL,CCTIPREG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZCAUIVA)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZCAUIVA) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oAZCAUIVA_5_3'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSCG0KCA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCFLANAL,CCTIPREG";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCFLANAL,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCAUIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCFLANAL,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_AZCAUIVA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_AZCAUIVA)
            select CCCODICE,CCFLANAL,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCAUIVA = NVL(_Link_.CCCODICE,space(5))
      this.w_FLANAL2 = NVL(_Link_.CCFLANAL,space(1))
      this.w_TIPOREG = NVL(_Link_.CCTIPREG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZCAUIVA = space(5)
      endif
      this.w_FLANAL2 = space(1)
      this.w_TIPOREG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=( .w_TIPOREG='N' OR .w_TIPOREG=' ' ) AND .w_FLANAL2<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_AZCAUIVA = space(5)
        this.w_FLANAL2 = space(1)
        this.w_TIPOREG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCAUIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_5_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_5_3.CCCODICE as CCCODICE503"+ ",link_5_3.CCFLANAL as CCFLANAL503"+ ",link_5_3.CCTIPREG as CCTIPREG503"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_5_3 on AZIENDA.AZCAUIVA=link_5_3.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_5_3"
          i_cKey=i_cKey+'+" and AZIENDA.AZCAUIVA=link_5_3.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AZCONIVA
  func Link_5_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCONIVA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_AZCONIVA)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_AZCONIVA))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZCONIVA)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZCONIVA) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oAZCONIVA_5_4'),i_cWhere,'GSAR_BZC',"Conti",'GSCG0KC1.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, di tipo non IVA oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCONIVA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_AZCONIVA);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_AZCONIVA)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCONIVA = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRIZ = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPSOTC = NVL(_Link_.ANTIPSOT,space(1))
      this.w_DATAOB = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AZCONIVA = space(15)
      endif
      this.w_DESCRIZ = space(40)
      this.w_TIPSOTC = space(1)
      this.w_DATAOB = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPSOTC='I' and (EMPTY(.w_DATAOB) OR .w_DATAOB>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, di tipo non IVA oppure obsoleto")
        endif
        this.w_AZCONIVA = space(15)
        this.w_DESCRIZ = space(40)
        this.w_TIPSOTC = space(1)
        this.w_DATAOB = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCONIVA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZCODVEN
  func Link_5_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCODVEN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_AZCODVEN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_AZCODVEN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZCODVEN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZCODVEN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oAZCODVEN_5_5'),i_cWhere,'GSAR_BZC',"Conti",'GSCG0KCO.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, conto non di tipo contropartite vendite")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCODVEN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_AZCODVEN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_AZCODVEN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCODVEN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESPIA = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPVEN = NVL(_Link_.ANTIPSOT,space(1))
      this.w_PARVEN = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZCODVEN = space(15)
      endif
      this.w_DESPIA = space(40)
      this.w_TIPVEN = space(1)
      this.w_PARVEN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPVEN='V'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Attenzione, conto non di tipo contropartite vendite")
        endif
        this.w_AZCODVEN = space(15)
        this.w_DESPIA = space(40)
        this.w_TIPVEN = space(1)
        this.w_PARVEN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCODVEN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZCAUGIR
  func Link_5_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCAUGIR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_AZCAUGIR)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCFLANAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_AZCAUGIR))
          select CCCODICE,CCDESCRI,CCTIPREG,CCFLANAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZCAUGIR)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZCAUGIR) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oAZCAUGIR_5_13'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSCG0KCA.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCFLANAL";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCTIPREG,CCFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCAUGIR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCTIPREG,CCFLANAL";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_AZCAUGIR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_AZCAUGIR)
            select CCCODICE,CCDESCRI,CCTIPREG,CCFLANAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCAUGIR = NVL(_Link_.CCCODICE,space(5))
      this.w_DESGIRIVA = NVL(_Link_.CCDESCRI,space(35))
      this.w_TIPOREG = NVL(_Link_.CCTIPREG,space(1))
      this.w_FLANAL = NVL(_Link_.CCFLANAL,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZCAUGIR = space(5)
      endif
      this.w_DESGIRIVA = space(35)
      this.w_TIPOREG = space(1)
      this.w_FLANAL = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=( .w_TIPOREG='N' OR .w_TIPOREG=' ' ) AND .w_FLANAL<>'S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_AZCAUGIR = space(5)
        this.w_DESGIRIVA = space(35)
        this.w_TIPOREG = space(1)
        this.w_FLANAL = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCAUGIR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_5_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_5_13.CCCODICE as CCCODICE513"+ ",link_5_13.CCDESCRI as CCDESCRI513"+ ",link_5_13.CCTIPREG as CCTIPREG513"+ ",link_5_13.CCFLANAL as CCFLANAL513"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_5_13 on AZIENDA.AZCAUGIR=link_5_13.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_5_13"
          i_cKey=i_cKey+'+" and AZIENDA.AZCAUGIR=link_5_13.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AZIVCONT
  func Link_5_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIVCONT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_AZIVCONT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_AZIVCONT))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZIVCONT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZIVCONT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oAZIVCONT_5_14'),i_cWhere,'GSAR_BZC',"Conti",'GSCG0KC1.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, di tipo non IVA oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIVCONT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_AZIVCONT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_AZIVCONT)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIVCONT = NVL(_Link_.ANCODICE,space(15))
      this.w_IVCONTDES = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPSOT1 = NVL(_Link_.ANTIPSOT,space(1))
      this.w_DATAOB = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AZIVCONT = space(15)
      endif
      this.w_IVCONTDES = space(40)
      this.w_TIPSOT1 = space(1)
      this.w_DATAOB = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPSOT1='I' and (EMPTY(.w_DATAOB) OR .w_DATAOB>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, di tipo non IVA oppure obsoleto")
        endif
        this.w_AZIVCONT = space(15)
        this.w_IVCONTDES = space(40)
        this.w_TIPSOT1 = space(1)
        this.w_DATAOB = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIVCONT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZCRERIP
  func Link_5_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCRERIP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_AZCRERIP)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_AZCRERIP))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZCRERIP)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZCRERIP) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oAZCRERIP_5_15'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCRERIP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_AZCRERIP);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_AZCRERIP)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCRERIP = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRERIP = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_AZCRERIP = space(15)
      endif
      this.w_DESCRERIP = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCRERIP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZCRESPE
  func Link_5_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCRESPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_AZCRESPE)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_AZCRESPE))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZCRESPE)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZCRESPE) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oAZCRESPE_5_16'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCRESPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_AZCRESPE);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_AZCRESPE)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCRESPE = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCRESPE = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_AZCRESPE = space(15)
      endif
      this.w_DESCRESPE = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCRESPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZINTDOV
  func Link_5_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZINTDOV) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_AZINTDOV)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_AZINTDOV))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZINTDOV)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZINTDOV) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oAZINTDOV_5_17'),i_cWhere,'GSAR_BZC',"Conti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZINTDOV)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_AZINTDOV);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_AZINTDOV)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZINTDOV = NVL(_Link_.ANCODICE,space(15))
      this.w_DESINTDOV = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_AZINTDOV = space(15)
      endif
      this.w_DESINTDOV = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZINTDOV Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZCONIND
  func Link_5_18(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCONIND) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_AZCONIND)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_AZCONIND))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZCONIND)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZCONIND) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oAZCONIND_5_18'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCONIND)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_AZCONIND);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_AZCONIND)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCONIND = NVL(_Link_.ANCODICE,space(15))
      this.w_DESIND = NVL(_Link_.ANDESCRI,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_AZCONIND = space(15)
      endif
      this.w_DESIND = space(15)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCONIND Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUINC
  func Link_4_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUINC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_CAUINC)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_CAUINC))
          select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUINC)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_CAUINC)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_CAUINC)+"%");

            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CAUINC) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oCAUINC_4_4'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSCGIKGD.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUINC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUINC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUINC)
            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUINC = NVL(_Link_.CCCODICE,space(5))
      this.w_DESINC = NVL(_Link_.CCDESCRI,space(35))
      this.w_DIFINC = NVL(_Link_.CCFLPDIF,space(1))
      this.w_REGINC = NVL(_Link_.CCTIPREG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUINC = space(5)
      endif
      this.w_DESINC = space(35)
      this.w_DIFINC = space(1)
      this.w_REGINC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DIFINC='S' AND .w_REGINC='V'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale rilevazione incassi incongruente o inesistente")
        endif
        this.w_CAUINC = space(5)
        this.w_DESINC = space(35)
        this.w_DIFINC = space(1)
        this.w_REGINC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUINC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZCAUPAG
  func Link_4_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCAUPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_AZCAUPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_AZCAUPAG))
          select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZCAUPAG)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_AZCAUPAG)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_AZCAUPAG)+"%");

            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AZCAUPAG) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oAZCAUPAG_4_6'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSCGPKGD.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCAUPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_AZCAUPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_AZCAUPAG)
            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCAUPAG = NVL(_Link_.CCCODICE,space(5))
      this.w_DESPAG = NVL(_Link_.CCDESCRI,space(35))
      this.w_DIFPAG = NVL(_Link_.CCFLPDIF,space(1))
      this.w_REGPAG = NVL(_Link_.CCTIPREG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZCAUPAG = space(5)
      endif
      this.w_DESPAG = space(35)
      this.w_DIFPAG = space(1)
      this.w_REGPAG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DIFPAG='S' AND .w_REGPAG='A'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale rilevazione pagamenti incongruente o inesistente")
        endif
        this.w_AZCAUPAG = space(5)
        this.w_DESPAG = space(35)
        this.w_DIFPAG = space(1)
        this.w_REGPAG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCAUPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_6(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_6.CCCODICE as CCCODICE406"+ ",link_4_6.CCDESCRI as CCDESCRI406"+ ",link_4_6.CCFLPDIF as CCFLPDIF406"+ ",link_4_6.CCTIPREG as CCTIPREG406"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_6 on AZIENDA.AZCAUPAG=link_4_6.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_6"
          i_cKey=i_cKey+'+" and AZIENDA.AZCAUPAG=link_4_6.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AZCAUMTA
  func Link_4_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCAUMTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_AZCAUMTA)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_AZCAUMTA))
          select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZCAUMTA)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_AZCAUMTA)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_AZCAUMTA)+"%");

            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AZCAUMTA) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oAZCAUMTA_4_8'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSCGIKGD.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCAUMTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_AZCAUMTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_AZCAUMTA)
            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCAUMTA = NVL(_Link_.CCCODICE,space(5))
      this.w_DESINCAT = NVL(_Link_.CCDESCRI,space(35))
      this.w_DIFINCAT = NVL(_Link_.CCFLPDIF,space(1))
      this.w_REGINCAT = NVL(_Link_.CCTIPREG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZCAUMTA = space(5)
      endif
      this.w_DESINCAT = space(35)
      this.w_DIFINCAT = space(1)
      this.w_REGINCAT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DIFINCAT='S' AND .w_REGINCAT='V'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale per maturazione temporale incongruente o inesistente")
        endif
        this.w_AZCAUMTA = space(5)
        this.w_DESINCAT = space(35)
        this.w_DIFINCAT = space(1)
        this.w_REGINCAT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCAUMTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_8.CCCODICE as CCCODICE408"+ ",link_4_8.CCDESCRI as CCDESCRI408"+ ",link_4_8.CCFLPDIF as CCFLPDIF408"+ ",link_4_8.CCTIPREG as CCTIPREG408"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_8 on AZIENDA.AZCAUMTA=link_4_8.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_8"
          i_cKey=i_cKey+'+" and AZIENDA.AZCAUMTA=link_4_8.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AZCAUMTP
  func Link_4_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCAUMTP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_AZCAUMTP)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_AZCAUMTP))
          select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZCAUMTP)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_AZCAUMTP)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_AZCAUMTP)+"%");

            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AZCAUMTP) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oAZCAUMTP_4_9'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSCGPKGD.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCAUMTP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_AZCAUMTP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_AZCAUMTP)
            select CCCODICE,CCDESCRI,CCFLPDIF,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCAUMTP = NVL(_Link_.CCCODICE,space(5))
      this.w_DESPAGPT = NVL(_Link_.CCDESCRI,space(35))
      this.w_DIFPAGPT = NVL(_Link_.CCFLPDIF,space(1))
      this.w_REGPAGPT = NVL(_Link_.CCTIPREG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZCAUMTP = space(5)
      endif
      this.w_DESPAGPT = space(35)
      this.w_DIFPAGPT = space(1)
      this.w_REGPAGPT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_DIFPAGPT='S' AND .w_REGPAGPT='A'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale per maturazione temporale incongruente o inesistente")
        endif
        this.w_AZCAUMTP = space(5)
        this.w_DESPAGPT = space(35)
        this.w_DIFPAGPT = space(1)
        this.w_REGPAGPT = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCAUMTP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_9.CCCODICE as CCCODICE409"+ ",link_4_9.CCDESCRI as CCDESCRI409"+ ",link_4_9.CCFLPDIF as CCFLPDIF409"+ ",link_4_9.CCTIPREG as CCTIPREG409"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_9 on AZIENDA.AZCAUMTP=link_4_9.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_9"
          i_cKey=i_cKey+'+" and AZIENDA.AZCAUMTP=link_4_9.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AZCAUAUT
  func Link_4_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCAUAUT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_ACC',True,'CAU_CONT')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCODICE like "+cp_ToStrODBC(trim(this.w_AZCAUAUT)+"%");

          i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPART,CCFLRIFE,CCTIPREG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCCODICE',trim(this.w_AZCAUAUT))
          select CCCODICE,CCDESCRI,CCFLPART,CCFLRIFE,CCTIPREG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZCAUAUT)==trim(_Link_.CCCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStrODBC(trim(this.w_AZCAUAUT)+"%");

            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPART,CCFLRIFE,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CCDESCRI like "+cp_ToStr(trim(this.w_AZCAUAUT)+"%");

            select CCCODICE,CCDESCRI,CCFLPART,CCFLRIFE,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AZCAUAUT) and !this.bDontReportError
            deferred_cp_zoom('CAU_CONT','*','CCCODICE',cp_AbsName(oSource.parent,'oAZCAUAUT_4_13'),i_cWhere,'GSCG_ACC',"Causali contabili",'GSCG_AUT.CAU_CONT_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPART,CCFLRIFE,CCTIPREG";
                     +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',oSource.xKey(1))
            select CCCODICE,CCDESCRI,CCFLPART,CCFLRIFE,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCAUAUT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCDESCRI,CCFLPART,CCFLRIFE,CCTIPREG";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_AZCAUAUT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_AZCAUAUT)
            select CCCODICE,CCDESCRI,CCFLPART,CCFLRIFE,CCTIPREG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCAUAUT = NVL(_Link_.CCCODICE,space(5))
      this.w_DESAUT = NVL(_Link_.CCDESCRI,space(35))
      this.w_FLPART = NVL(_Link_.CCFLPART,space(1))
      this.w_FLRIFE = NVL(_Link_.CCFLRIFE,space(1))
      this.w_TIPREG = NVL(_Link_.CCTIPREG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZCAUAUT = space(5)
      endif
      this.w_DESAUT = space(35)
      this.w_FLPART = space(1)
      this.w_FLRIFE = space(1)
      this.w_TIPREG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLPART='N' AND .w_FLRIFE='N' AND .w_TIPREG='N'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale storno IVA autotrasportatori incongruente o inesistente")
        endif
        this.w_AZCAUAUT = space(5)
        this.w_DESAUT = space(35)
        this.w_FLPART = space(1)
        this.w_FLRIFE = space(1)
        this.w_TIPREG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCAUAUT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_4_13(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAU_CONT_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
      i_cNewSel = i_cSel+ ",link_4_13.CCCODICE as CCCODICE413"+ ",link_4_13.CCDESCRI as CCDESCRI413"+ ",link_4_13.CCFLPART as CCFLPART413"+ ",link_4_13.CCFLRIFE as CCFLRIFE413"+ ",link_4_13.CCTIPREG as CCTIPREG413"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_4_13 on AZIENDA.AZCAUAUT=link_4_13.CCCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_4_13"
          i_cKey=i_cKey+'+" and AZIENDA.AZCAUAUT=link_4_13.CCCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=AZIVAAUT
  func Link_4_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZIVAAUT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_AZIVAAUT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_AZIVAAUT))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZIVAAUT)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_AZIVAAUT)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_AZIVAAUT)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_AZIVAAUT) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oAZIVAAUT_4_14'),i_cWhere,'GSAR_BZC',"Conti IVA",'GSCG_ACC.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, di tipo non IVA oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZIVAAUT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_AZIVAAUT);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_AZIVAAUT)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZIVAAUT = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_TIPSOT = NVL(_Link_.ANTIPSOT,space(1))
      this.w_DATAOB = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_AZIVAAUT = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_TIPSOT = space(1)
      this.w_DATAOB = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPSOT='I' and (EMPTY(.w_DATAOB) OR .w_DATAOB>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, di tipo non IVA oppure obsoleto")
        endif
        this.w_AZIVAAUT = space(15)
        this.w_DESCON = space(40)
        this.w_TIPSOT = space(1)
        this.w_DATAOB = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZIVAAUT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BANPRO
  func Link_1_65(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BANPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_BANPRO)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB,BACODSIA,BATIPCON,BADESCRI,BADTOBSO,BACONSBF";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_BANPRO))
          select BACODBAN,BACODABI,BACODCAB,BACODSIA,BATIPCON,BADESCRI,BADTOBSO,BACONSBF;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BANPRO)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_BANPRO) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oBANPRO_1_65'),i_cWhere,'GSTE_ACB',"Conti banche",'GSCG_API.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB,BACODSIA,BATIPCON,BADESCRI,BADTOBSO,BACONSBF";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BACODABI,BACODCAB,BACODSIA,BATIPCON,BADESCRI,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BANPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BACODABI,BACODCAB,BACODSIA,BATIPCON,BADESCRI,BADTOBSO,BACONSBF";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_BANPRO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_BANPRO)
            select BACODBAN,BACODABI,BACODCAB,BACODSIA,BATIPCON,BADESCRI,BADTOBSO,BACONSBF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BANPRO = NVL(_Link_.BACODBAN,space(15))
      this.w_BANPROABI = NVL(_Link_.BACODABI,space(5))
      this.w_BANPROCAB = NVL(_Link_.BACODCAB,space(5))
      this.w_BANPROSIA = NVL(_Link_.BACODSIA,space(5))
      this.w_BATIPCON_B = NVL(_Link_.BATIPCON,space(1))
      this.w_BADESCRI = NVL(_Link_.BADESCRI,space(35))
      this.w_DATOBSO2 = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
      this.w_BACONSBF_1 = NVL(_Link_.BACONSBF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_BANPRO = space(15)
      endif
      this.w_BANPROABI = space(5)
      this.w_BANPROCAB = space(5)
      this.w_BANPROSIA = space(5)
      this.w_BATIPCON_B = space(1)
      this.w_BADESCRI = space(35)
      this.w_DATOBSO2 = ctod("  /  /  ")
      this.w_BACONSBF_1 = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_BATIPCON_B<>'S' AND (EMPTY(.w_DATOBSO2) OR .w_DATOBSO2>.w_OBTEST) AND .w_BACONSBF_1='C'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice banca inesistente, obsoleta, di compensazione o salvo buon fine")
        endif
        this.w_BANPRO = space(15)
        this.w_BANPROABI = space(5)
        this.w_BANPROCAB = space(5)
        this.w_BANPROSIA = space(5)
        this.w_BATIPCON_B = space(1)
        this.w_BADESCRI = space(35)
        this.w_DATOBSO2 = ctod("  /  /  ")
        this.w_BACONSBF_1 = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BANPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=AZCONPRO
  func Link_5_45(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_AZCONPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_AZCONPRO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_AZCONPRO))
          select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_AZCONPRO)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_AZCONPRO) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oAZCONPRO_5_45'),i_cWhere,'GSAR_BZC',"Conti",'GSCG0KC1.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, di tipo non IVA oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_AZCONPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_AZCONPRO);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_AZCONPRO)
            select ANTIPCON,ANCODICE,ANDESCRI,ANTIPSOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_AZCONPRO = NVL(_Link_.ANCODICE,space(15))
      this.w_DESPRO = NVL(_Link_.ANDESCRI,space(15))
      this.w_TIPSOTRC = NVL(_Link_.ANTIPSOT,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_AZCONPRO = space(15)
      endif
      this.w_DESPRO = space(15)
      this.w_TIPSOTRC = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_TIPSOTRC='I' and (EMPTY(.w_DATAOB) OR .w_DATAOB>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente, di tipo non IVA oppure obsoleto")
        endif
        this.w_AZCONPRO = space(15)
        this.w_DESPRO = space(15)
        this.w_TIPSOTRC = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_AZCONPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oAZCODAZI_1_1.value==this.w_AZCODAZI)
      this.oPgFrm.Page1.oPag.oAZCODAZI_1_1.value=this.w_AZCODAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oAZIVAAZI_1_7.value==this.w_AZIVAAZI)
      this.oPgFrm.Page1.oPag.oAZIVAAZI_1_7.value=this.w_AZIVAAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oAZIVACOF_1_8.value==this.w_AZIVACOF)
      this.oPgFrm.Page1.oPag.oAZIVACOF_1_8.value=this.w_AZIVACOF
    endif
    if not(this.oPgFrm.Page1.oPag.oAZIVACON_1_9.value==this.w_AZIVACON)
      this.oPgFrm.Page1.oPag.oAZIVACON_1_9.value=this.w_AZIVACON
    endif
    if not(this.oPgFrm.Page1.oPag.oAZIVACAR_1_10.RadioValue()==this.w_AZIVACAR)
      this.oPgFrm.Page1.oPag.oAZIVACAR_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAZCATAZI_1_11.value==this.w_AZCATAZI)
      this.oPgFrm.Page1.oPag.oAZCATAZI_1_11.value=this.w_AZCATAZI
    endif
    if not(this.oPgFrm.Page1.oPag.oDESATT_1_12.value==this.w_DESATT)
      this.oPgFrm.Page1.oPag.oDESATT_1_12.value=this.w_DESATT
    endif
    if not(this.oPgFrm.Page1.oPag.oAZATTIVI_1_13.RadioValue()==this.w_AZATTIVI)
      this.oPgFrm.Page1.oPag.oAZATTIVI_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAZTIPDIC_1_14.RadioValue()==this.w_AZTIPDIC)
      this.oPgFrm.Page1.oPag.oAZTIPDIC_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAZTIPDEN_1_15.RadioValue()==this.w_AZTIPDEN)
      this.oPgFrm.Page1.oPag.oAZTIPDEN_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAZDENMAG_1_16.value==this.w_AZDENMAG)
      this.oPgFrm.Page1.oPag.oAZDENMAG_1_16.value=this.w_AZDENMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oAZSTAREG_1_17.RadioValue()==this.w_AZSTAREG)
      this.oPgFrm.Page1.oPag.oAZSTAREG_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAZTIPINT_1_18.RadioValue()==this.w_AZTIPINT)
      this.oPgFrm.Page1.oPag.oAZTIPINT_1_18.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oAZPAGINT_1_19.value==this.w_AZPAGINT)
      this.oPgFrm.Page1.oPag.oAZPAGINT_1_19.value=this.w_AZPAGINT
    endif
    if not(this.oPgFrm.Page1.oPag.oAZPREFIS_1_20.value==this.w_AZPREFIS)
      this.oPgFrm.Page1.oPag.oAZPREFIS_1_20.value=this.w_AZPREFIS
    endif
    if not(this.oPgFrm.Page1.oPag.oCONBAN_1_23.value==this.w_CONBAN)
      this.oPgFrm.Page1.oPag.oCONBAN_1_23.value=this.w_CONBAN
    endif
    if not(this.oPgFrm.Page3.oPag.oAZTIPFOR_3_11.RadioValue()==this.w_AZTIPFOR)
      this.oPgFrm.Page3.oPag.oAZTIPFOR_3_11.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oAZFISFOR_3_13.RadioValue()==this.w_AZFISFOR)
      this.oPgFrm.Page3.oPag.oAZFISFOR_3_13.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oAZCOFFOR_3_14.value==this.w_AZCOFFOR)
      this.oPgFrm.Page3.oPag.oAZCOFFOR_3_14.value=this.w_AZCOFFOR
    endif
    if not(this.oPgFrm.Page3.oPag.oAZNUMCAF_3_15.value==this.w_AZNUMCAF)
      this.oPgFrm.Page3.oPag.oAZNUMCAF_3_15.value=this.w_AZNUMCAF
    endif
    if not(this.oPgFrm.Page3.oPag.oAZCOGFOR_3_16.value==this.w_AZCOGFOR)
      this.oPgFrm.Page3.oPag.oAZCOGFOR_3_16.value=this.w_AZCOGFOR
    endif
    if not(this.oPgFrm.Page3.oPag.oAZNOMFOR_3_17.value==this.w_AZNOMFOR)
      this.oPgFrm.Page3.oPag.oAZNOMFOR_3_17.value=this.w_AZNOMFOR
    endif
    if not(this.oPgFrm.Page3.oPag.oAZSEXFOR_3_18.RadioValue()==this.w_AZSEXFOR)
      this.oPgFrm.Page3.oPag.oAZSEXFOR_3_18.SetRadio()
    endif
    if not(this.oPgFrm.Page3.oPag.oAZDATFOR_3_19.value==this.w_AZDATFOR)
      this.oPgFrm.Page3.oPag.oAZDATFOR_3_19.value=this.w_AZDATFOR
    endif
    if not(this.oPgFrm.Page3.oPag.oAZCONFOR_3_20.value==this.w_AZCONFOR)
      this.oPgFrm.Page3.oPag.oAZCONFOR_3_20.value=this.w_AZCONFOR
    endif
    if not(this.oPgFrm.Page3.oPag.oAZPRNFOR_3_21.value==this.w_AZPRNFOR)
      this.oPgFrm.Page3.oPag.oAZPRNFOR_3_21.value=this.w_AZPRNFOR
    endif
    if not(this.oPgFrm.Page3.oPag.oAZDENFOR_3_29.value==this.w_AZDENFOR)
      this.oPgFrm.Page3.oPag.oAZDENFOR_3_29.value=this.w_AZDENFOR
    endif
    if not(this.oPgFrm.Page3.oPag.oAZCOMFOR_3_30.value==this.w_AZCOMFOR)
      this.oPgFrm.Page3.oPag.oAZCOMFOR_3_30.value=this.w_AZCOMFOR
    endif
    if not(this.oPgFrm.Page3.oPag.oAZPROFOR_3_31.value==this.w_AZPROFOR)
      this.oPgFrm.Page3.oPag.oAZPROFOR_3_31.value=this.w_AZPROFOR
    endif
    if not(this.oPgFrm.Page3.oPag.oAZINDFOR_3_32.value==this.w_AZINDFOR)
      this.oPgFrm.Page3.oPag.oAZINDFOR_3_32.value=this.w_AZINDFOR
    endif
    if not(this.oPgFrm.Page3.oPag.oAZCAPFOR_3_33.value==this.w_AZCAPFOR)
      this.oPgFrm.Page3.oPag.oAZCAPFOR_3_33.value=this.w_AZCAPFOR
    endif
    if not(this.oPgFrm.Page3.oPag.oAZCOLFOR_3_38.value==this.w_AZCOLFOR)
      this.oPgFrm.Page3.oPag.oAZCOLFOR_3_38.value=this.w_AZCOLFOR
    endif
    if not(this.oPgFrm.Page3.oPag.oAZPRLFOR_3_39.value==this.w_AZPRLFOR)
      this.oPgFrm.Page3.oPag.oAZPRLFOR_3_39.value=this.w_AZPRLFOR
    endif
    if not(this.oPgFrm.Page3.oPag.oAZINLFOR_3_40.value==this.w_AZINLFOR)
      this.oPgFrm.Page3.oPag.oAZINLFOR_3_40.value=this.w_AZINLFOR
    endif
    if not(this.oPgFrm.Page3.oPag.oAZCALFOR_3_41.value==this.w_AZCALFOR)
      this.oPgFrm.Page3.oPag.oAZCALFOR_3_41.value=this.w_AZCALFOR
    endif
    if not(this.oPgFrm.Page2.oPag.oAZPLNODO_2_2.RadioValue()==this.w_AZPLNODO)
      this.oPgFrm.Page2.oPag.oAZPLNODO_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oAZPLADOC_2_3.RadioValue()==this.w_AZPLADOC)
      this.oPgFrm.Page2.oPag.oAZPLADOC_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oAZGIRIVA_5_1.RadioValue()==this.w_AZGIRIVA)
      this.oPgFrm.Page5.oPag.oAZGIRIVA_5_1.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oAZCAUIVA_5_3.value==this.w_AZCAUIVA)
      this.oPgFrm.Page5.oPag.oAZCAUIVA_5_3.value=this.w_AZCAUIVA
    endif
    if not(this.oPgFrm.Page5.oPag.oAZCONIVA_5_4.value==this.w_AZCONIVA)
      this.oPgFrm.Page5.oPag.oAZCONIVA_5_4.value=this.w_AZCONIVA
    endif
    if not(this.oPgFrm.Page5.oPag.oAZCODVEN_5_5.value==this.w_AZCODVEN)
      this.oPgFrm.Page5.oPag.oAZCODVEN_5_5.value=this.w_AZCODVEN
    endif
    if not(this.oPgFrm.Page5.oPag.oDESCRI_5_6.value==this.w_DESCRI)
      this.oPgFrm.Page5.oPag.oDESCRI_5_6.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page5.oPag.oAZNEWIVA_5_7.RadioValue()==this.w_AZNEWIVA)
      this.oPgFrm.Page5.oPag.oAZNEWIVA_5_7.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oDESCRIZ_5_9.value==this.w_DESCRIZ)
      this.oPgFrm.Page5.oPag.oDESCRIZ_5_9.value=this.w_DESCRIZ
    endif
    if not(this.oPgFrm.Page5.oPag.oAZCAUGIR_5_13.value==this.w_AZCAUGIR)
      this.oPgFrm.Page5.oPag.oAZCAUGIR_5_13.value=this.w_AZCAUGIR
    endif
    if not(this.oPgFrm.Page5.oPag.oAZIVCONT_5_14.value==this.w_AZIVCONT)
      this.oPgFrm.Page5.oPag.oAZIVCONT_5_14.value=this.w_AZIVCONT
    endif
    if not(this.oPgFrm.Page5.oPag.oAZCRERIP_5_15.value==this.w_AZCRERIP)
      this.oPgFrm.Page5.oPag.oAZCRERIP_5_15.value=this.w_AZCRERIP
    endif
    if not(this.oPgFrm.Page5.oPag.oAZCRESPE_5_16.value==this.w_AZCRESPE)
      this.oPgFrm.Page5.oPag.oAZCRESPE_5_16.value=this.w_AZCRESPE
    endif
    if not(this.oPgFrm.Page5.oPag.oAZINTDOV_5_17.value==this.w_AZINTDOV)
      this.oPgFrm.Page5.oPag.oAZINTDOV_5_17.value=this.w_AZINTDOV
    endif
    if not(this.oPgFrm.Page5.oPag.oAZCONIND_5_18.value==this.w_AZCONIND)
      this.oPgFrm.Page5.oPag.oAZCONIND_5_18.value=this.w_AZCONIND
    endif
    if not(this.oPgFrm.Page5.oPag.oDESGIRIVA_5_21.value==this.w_DESGIRIVA)
      this.oPgFrm.Page5.oPag.oDESGIRIVA_5_21.value=this.w_DESGIRIVA
    endif
    if not(this.oPgFrm.Page5.oPag.oIVCONTDES_5_22.value==this.w_IVCONTDES)
      this.oPgFrm.Page5.oPag.oIVCONTDES_5_22.value=this.w_IVCONTDES
    endif
    if not(this.oPgFrm.Page5.oPag.oDESCRERIP_5_23.value==this.w_DESCRERIP)
      this.oPgFrm.Page5.oPag.oDESCRERIP_5_23.value=this.w_DESCRERIP
    endif
    if not(this.oPgFrm.Page5.oPag.oDESCRESPE_5_24.value==this.w_DESCRESPE)
      this.oPgFrm.Page5.oPag.oDESCRESPE_5_24.value=this.w_DESCRESPE
    endif
    if not(this.oPgFrm.Page5.oPag.oDESINTDOV_5_25.value==this.w_DESINTDOV)
      this.oPgFrm.Page5.oPag.oDESINTDOV_5_25.value=this.w_DESINTDOV
    endif
    if not(this.oPgFrm.Page5.oPag.oDESPIA_5_37.value==this.w_DESPIA)
      this.oPgFrm.Page5.oPag.oDESPIA_5_37.value=this.w_DESPIA
    endif
    if not(this.oPgFrm.Page5.oPag.oDESIND_5_40.value==this.w_DESIND)
      this.oPgFrm.Page5.oPag.oDESIND_5_40.value=this.w_DESIND
    endif
    if not(this.oPgFrm.Page4.oPag.oCAUINC_4_4.value==this.w_CAUINC)
      this.oPgFrm.Page4.oPag.oCAUINC_4_4.value=this.w_CAUINC
    endif
    if not(this.oPgFrm.Page4.oPag.oAZCAUPAG_4_6.value==this.w_AZCAUPAG)
      this.oPgFrm.Page4.oPag.oAZCAUPAG_4_6.value=this.w_AZCAUPAG
    endif
    if not(this.oPgFrm.Page4.oPag.oAZDATCON_4_7.RadioValue()==this.w_AZDATCON)
      this.oPgFrm.Page4.oPag.oAZDATCON_4_7.SetRadio()
    endif
    if not(this.oPgFrm.Page4.oPag.oAZCAUMTA_4_8.value==this.w_AZCAUMTA)
      this.oPgFrm.Page4.oPag.oAZCAUMTA_4_8.value=this.w_AZCAUMTA
    endif
    if not(this.oPgFrm.Page4.oPag.oAZCAUMTP_4_9.value==this.w_AZCAUMTP)
      this.oPgFrm.Page4.oPag.oAZCAUMTP_4_9.value=this.w_AZCAUMTP
    endif
    if not(this.oPgFrm.Page4.oPag.oAZCAUAUT_4_13.value==this.w_AZCAUAUT)
      this.oPgFrm.Page4.oPag.oAZCAUAUT_4_13.value=this.w_AZCAUAUT
    endif
    if not(this.oPgFrm.Page4.oPag.oAZIVAAUT_4_14.value==this.w_AZIVAAUT)
      this.oPgFrm.Page4.oPag.oAZIVAAUT_4_14.value=this.w_AZIVAAUT
    endif
    if not(this.oPgFrm.Page4.oPag.oAZDATAUT_4_16.value==this.w_AZDATAUT)
      this.oPgFrm.Page4.oPag.oAZDATAUT_4_16.value=this.w_AZDATAUT
    endif
    if not(this.oPgFrm.Page4.oPag.oDESINC_4_17.value==this.w_DESINC)
      this.oPgFrm.Page4.oPag.oDESINC_4_17.value=this.w_DESINC
    endif
    if not(this.oPgFrm.Page4.oPag.oDESPAG_4_18.value==this.w_DESPAG)
      this.oPgFrm.Page4.oPag.oDESPAG_4_18.value=this.w_DESPAG
    endif
    if not(this.oPgFrm.Page4.oPag.oDESAUT_4_19.value==this.w_DESAUT)
      this.oPgFrm.Page4.oPag.oDESAUT_4_19.value=this.w_DESAUT
    endif
    if not(this.oPgFrm.Page4.oPag.oDESCON_4_21.value==this.w_DESCON)
      this.oPgFrm.Page4.oPag.oDESCON_4_21.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oAZDESBAN_1_47.value==this.w_AZDESBAN)
      this.oPgFrm.Page1.oPag.oAZDESBAN_1_47.value=this.w_AZDESBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oABIBAN_1_51.value==this.w_ABIBAN)
      this.oPgFrm.Page1.oPag.oABIBAN_1_51.value=this.w_ABIBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oCABBAN_1_52.value==this.w_CABBAN)
      this.oPgFrm.Page1.oPag.oCABBAN_1_52.value=this.w_CABBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oCONCOR_1_53.value==this.w_CONCOR)
      this.oPgFrm.Page1.oPag.oCONCOR_1_53.value=this.w_CONCOR
    endif
    if not(this.oPgFrm.Page1.oPag.oBANPROABI_1_56.value==this.w_BANPROABI)
      this.oPgFrm.Page1.oPag.oBANPROABI_1_56.value=this.w_BANPROABI
    endif
    if not(this.oPgFrm.Page1.oPag.oBANPROCAB_1_57.value==this.w_BANPROCAB)
      this.oPgFrm.Page1.oPag.oBANPROCAB_1_57.value=this.w_BANPROCAB
    endif
    if not(this.oPgFrm.Page1.oPag.oBANPROSIA_1_58.value==this.w_BANPROSIA)
      this.oPgFrm.Page1.oPag.oBANPROSIA_1_58.value=this.w_BANPROSIA
    endif
    if not(this.oPgFrm.Page1.oPag.oBADESCRI_1_62.value==this.w_BADESCRI)
      this.oPgFrm.Page1.oPag.oBADESCRI_1_62.value=this.w_BADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oBANPRO_1_65.value==this.w_BANPRO)
      this.oPgFrm.Page1.oPag.oBANPRO_1_65.value=this.w_BANPRO
    endif
    if not(this.oPgFrm.Page4.oPag.oDESINCAT_4_28.value==this.w_DESINCAT)
      this.oPgFrm.Page4.oPag.oDESINCAT_4_28.value=this.w_DESINCAT
    endif
    if not(this.oPgFrm.Page4.oPag.oDESPAGPT_4_29.value==this.w_DESPAGPT)
      this.oPgFrm.Page4.oPag.oDESPAGPT_4_29.value=this.w_DESPAGPT
    endif
    if not(this.oPgFrm.Page1.oPag.oAZRAGCOR_1_71.RadioValue()==this.w_AZRAGCOR)
      this.oPgFrm.Page1.oPag.oAZRAGCOR_1_71.SetRadio()
    endif
    if not(this.oPgFrm.Page5.oPag.oAZCONPRO_5_45.value==this.w_AZCONPRO)
      this.oPgFrm.Page5.oPag.oAZCONPRO_5_45.value=this.w_AZCONPRO
    endif
    if not(this.oPgFrm.Page5.oPag.oDESPRO_5_47.value==this.w_DESPRO)
      this.oPgFrm.Page5.oPag.oDESPRO_5_47.value=this.w_DESPRO
    endif
    if not(this.oPgFrm.Page5.oPag.oAZFLGIND_5_48.RadioValue()==this.w_AZFLGIND)
      this.oPgFrm.Page5.oPag.oAZFLGIND_5_48.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'AZIENDA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_AZIVACOF)) or not(EMPTY(.w_AZIVACOF) OR CHKCFP(.w_AZIVACOF,"CF")))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAZIVACOF_1_8.SetFocus()
            i_bnoObbl = !empty(.w_AZIVACOF)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AZCATAZI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAZCATAZI_1_11.SetFocus()
            i_bnoObbl = !empty(.w_AZCATAZI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore non ammesso o attivit� principale con associati registri (se multi att.)")
          case   (empty(.w_AZSTAREG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAZSTAREG_1_17.SetFocus()
            i_bnoObbl = !empty(.w_AZSTAREG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_AZTIPINT))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oAZTIPINT_1_18.SetFocus()
            i_bnoObbl = !empty(.w_AZTIPINT)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_BATIPCON<>'S' AND (EMPTY(.w_DATOBSO1) OR .w_DATOBSO1>.w_OBTEST) AND .w_BACONSBF='C')  and not(empty(.w_CONBAN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCONBAN_1_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice banca inesistente, obsoleta, di compensazione o salvo buon fine")
          case   not(EMPTY(.w_AZCOFFOR) OR CHKCFP(.w_AZCOFFOR,"CF"))
            .oPgFrm.ActivePage = 3
            .oPgFrm.Page3.oPag.oAZCOFFOR_3_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(( .w_TIPOREG='N' OR .w_TIPOREG=' ' ) AND .w_FLANAL2<>'S')  and not(empty(.w_AZCAUIVA))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oAZCAUIVA_5_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPSOTC='I' and (EMPTY(.w_DATAOB) OR .w_DATAOB>.w_OBTEST))  and not(empty(.w_AZCONIVA))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oAZCONIVA_5_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente, di tipo non IVA oppure obsoleto")
          case   not(.w_TIPVEN='V')  and not(.w_AZGIRIVA='SI')  and not(empty(.w_AZCODVEN))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oAZCODVEN_5_5.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Attenzione, conto non di tipo contropartite vendite")
          case   not(( .w_TIPOREG='N' OR .w_TIPOREG=' ' ) AND .w_FLANAL<>'S')  and not(.w_AZNEWIVA='N')  and not(empty(.w_AZCAUGIR))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oAZCAUGIR_5_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_TIPSOT1='I' and (EMPTY(.w_DATAOB) OR .w_DATAOB>.w_OBTEST))  and not(.w_AZNEWIVA='N')  and not(empty(.w_AZIVCONT))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oAZIVCONT_5_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente, di tipo non IVA oppure obsoleto")
          case   not(.w_DIFINC='S' AND .w_REGINC='V')  and not(empty(.w_CAUINC))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oCAUINC_4_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale rilevazione incassi incongruente o inesistente")
          case   not(.w_DIFPAG='S' AND .w_REGPAG='A')  and not(empty(.w_AZCAUPAG))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oAZCAUPAG_4_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale rilevazione pagamenti incongruente o inesistente")
          case   not(.w_DIFINCAT='S' AND .w_REGINCAT='V')  and not(empty(.w_AZCAUMTA))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oAZCAUMTA_4_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale per maturazione temporale incongruente o inesistente")
          case   not(.w_DIFPAGPT='S' AND .w_REGPAGPT='A')  and not(empty(.w_AZCAUMTP))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oAZCAUMTP_4_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale per maturazione temporale incongruente o inesistente")
          case   not(.w_FLPART='N' AND .w_FLRIFE='N' AND .w_TIPREG='N')  and not(empty(.w_AZCAUAUT))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oAZCAUAUT_4_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale storno IVA autotrasportatori incongruente o inesistente")
          case   not(.w_TIPSOT='I' and (EMPTY(.w_DATAOB) OR .w_DATAOB>.w_OBTEST))  and not(empty(.w_AZIVAAUT))
            .oPgFrm.ActivePage = 4
            .oPgFrm.Page4.oPag.oAZIVAAUT_4_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente, di tipo non IVA oppure obsoleto")
          case   not(.w_BATIPCON_B<>'S' AND (EMPTY(.w_DATOBSO2) OR .w_DATOBSO2>.w_OBTEST) AND .w_BACONSBF_1='C')  and not(empty(.w_BANPRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oBANPRO_1_65.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice banca inesistente, obsoleta, di compensazione o salvo buon fine")
          case   not(.w_TIPSOTRC='I' and (EMPTY(.w_DATAOB) OR .w_DATAOB>.w_OBTEST))  and not(empty(.w_AZCONPRO))
            .oPgFrm.ActivePage = 5
            .oPgFrm.Page5.oPag.oAZCONPRO_5_45.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente, di tipo non IVA oppure obsoleto")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCG_MDI.CheckForm()
      if i_bres
        i_bres=  .GSCG_MDI.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_AZCODAZI = this.w_AZCODAZI
    this.o_AZTIPDEN = this.w_AZTIPDEN
    this.o_AZTIPINT = this.w_AZTIPINT
    this.o_AZPLNODO = this.w_AZPLNODO
    this.o_AZCAUIVA = this.w_AZCAUIVA
    this.o_AZNEWIVA = this.w_AZNEWIVA
    * --- GSCG_MDI : Depends On
    this.GSCG_MDI.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscg_apiPag1 as StdContainer
  Width  = 637
  height = 450
  stdWidth  = 637
  stdheight = 450
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oAZCODAZI_1_1 as StdField with uid="KUTERUTFWA",rtseq=1,rtrep=.f.,;
    cFormVar = "w_AZCODAZI", cQueryName = "AZCODAZI",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Funzione non consentita",;
    ToolTipText = "Codice azienda selezionata",;
    HelpContextID = 66492081,;
   bGlobalFont=.t.,;
    Height=21, Width=13, Left=302, Top=11, InputMask=replicate('X',5)

  add object oAZIVAAZI_1_7 as StdField with uid="UNJJPYPAEV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_AZIVAAZI", cQueryName = "AZIVAAZI",;
    bObbl = .f. , nPag = 1, value=space(25), bMultilanguage =  .f.,;
    ToolTipText = "Ufficio IVA competente per territorio",;
    HelpContextID = 69154481,;
   bGlobalFont=.t.,;
    Height=21, Width=208, Left=156, Top=11, InputMask=replicate('X',25)

  add object oAZIVACOF_1_8 as StdField with uid="HHCZQVIGJS",rtseq=8,rtrep=.f.,;
    cFormVar = "w_AZIVACOF", cQueryName = "AZIVACOF",;
    bObbl = .t. , nPag = 1, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del dichiarante usato per la liquidazione IVA",;
    HelpContextID = 232835404,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=156, Top=40, cSayPict="REPL('!', 16)", cGetPict="REPL('!', 16)", InputMask=replicate('X',16)

  func oAZIVACOF_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_AZIVACOF) OR CHKCFP(.w_AZIVACOF,"CF"))
    endwith
    return bRes
  endfunc

  add object oAZIVACON_1_9 as StdField with uid="GMNJNZBDRU",rtseq=9,rtrep=.f.,;
    cFormVar = "w_AZIVACON", cQueryName = "AZIVACON",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice concessione",;
    HelpContextID = 232835412,;
   bGlobalFont=.t.,;
    Height=21, Width=30, Left=156, Top=69, cSayPict="'999'", cGetPict="'999'", InputMask=replicate('X',3)


  add object oAZIVACAR_1_10 as StdCombo with uid="YYMESQRZRO",rtseq=10,rtrep=.f.,left=255,top=69,width=322,height=21;
    , ToolTipText = "Codice carica";
    , HelpContextID = 232835416;
    , cFormVar="w_AZIVACAR",RowSource=""+"1) Rappresentante legale o negoziale,"+"2) Rappresentante minore - curatore eredit�,"+"3) Curatore fallimentare,"+"4) Commissario liquidatore,"+"5) Commissario giudiziale,"+"6) Rappresentante fiscale soggetto non residente,"+"7) Erede dichiarante,"+"8) Liquidazione volontaria,"+"11) Amministratore di condominio,"+"12) Condominio,"+"13) Per conto pubblica Amministrazione,"+"14) Commissario liquidatore pubblica Amministrazione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAZIVACAR_1_10.RadioValue()
    return(iif(this.value =1,'1',;
    iif(this.value =2,'2',;
    iif(this.value =3,'3',;
    iif(this.value =4,'4',;
    iif(this.value =5,'5',;
    iif(this.value =6,'6',;
    iif(this.value =7,'7',;
    iif(this.value =8,'8',;
    iif(this.value =9,'11',;
    iif(this.value =10,'12',;
    iif(this.value =11,'13',;
    iif(this.value =12,'14',;
    space(2))))))))))))))
  endfunc
  func oAZIVACAR_1_10.GetRadio()
    this.Parent.oContained.w_AZIVACAR = this.RadioValue()
    return .t.
  endfunc

  func oAZIVACAR_1_10.SetRadio()
    this.Parent.oContained.w_AZIVACAR=trim(this.Parent.oContained.w_AZIVACAR)
    this.value = ;
      iif(this.Parent.oContained.w_AZIVACAR=='1',1,;
      iif(this.Parent.oContained.w_AZIVACAR=='2',2,;
      iif(this.Parent.oContained.w_AZIVACAR=='3',3,;
      iif(this.Parent.oContained.w_AZIVACAR=='4',4,;
      iif(this.Parent.oContained.w_AZIVACAR=='5',5,;
      iif(this.Parent.oContained.w_AZIVACAR=='6',6,;
      iif(this.Parent.oContained.w_AZIVACAR=='7',7,;
      iif(this.Parent.oContained.w_AZIVACAR=='8',8,;
      iif(this.Parent.oContained.w_AZIVACAR=='11',9,;
      iif(this.Parent.oContained.w_AZIVACAR=='12',10,;
      iif(this.Parent.oContained.w_AZIVACAR=='13',11,;
      iif(this.Parent.oContained.w_AZIVACAR=='14',12,;
      0))))))))))))
  endfunc

  proc oAZIVACAR_1_10.mDefault
    with this.Parent.oContained
      if empty(.w_AZIVACAR)
        .w_AZIVACAR = '2'
      endif
    endwith
  endproc

  add object oAZCATAZI_1_11 as StdField with uid="PLAJMROALI",rtseq=11,rtrep=.f.,;
    cFormVar = "w_AZCATAZI", cQueryName = "AZCATAZI",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso o attivit� principale con associati registri (se multi att.)",;
    ToolTipText = "Codice attivit� principale",;
    HelpContextID = 50632369,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=156, Top=98, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="ATTIMAST", cZoomOnZoom="GSAR_MAT", oKey_1_1="ATCODATT", oKey_1_2="this.w_AZCATAZI"

  func oAZCATAZI_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZCATAZI_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZCATAZI_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ATTIMAST','*','ATCODATT',cp_AbsName(this.parent,'oAZCATAZI_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_MAT',"Attivit�",'',this.parent.oContained
  endproc
  proc oAZCATAZI_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_MAT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ATCODATT=this.parent.oContained.w_AZCATAZI
     i_obj.ecpSave()
  endproc

  add object oDESATT_1_12 as StdField with uid="UHYIBUXJTB",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESATT", cQueryName = "DESATT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 268194870,;
   bGlobalFont=.t.,;
    Height=21, Width=247, Left=236, Top=98, InputMask=replicate('X',35)

  add object oAZATTIVI_1_13 as StdCheck with uid="HLJBZIHGIN",rtseq=13,rtrep=.f.,left=488, top=98, caption="Pi� attivit�",;
    ToolTipText = "Se attivo: l'azienda esercita pi� attivit�",;
    HelpContextID = 183613105,;
    cFormVar="w_AZATTIVI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAZATTIVI_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZATTIVI_1_13.GetRadio()
    this.Parent.oContained.w_AZATTIVI = this.RadioValue()
    return .t.
  endfunc

  func oAZATTIVI_1_13.SetRadio()
    this.Parent.oContained.w_AZATTIVI=trim(this.Parent.oContained.w_AZATTIVI)
    this.value = ;
      iif(this.Parent.oContained.w_AZATTIVI=='S',1,;
      0)
  endfunc


  add object oAZTIPDIC_1_14 as StdCombo with uid="MYIIOKWEBD",rtseq=14,rtrep=.f.,left=236,top=129,width=190,height=21;
    , ToolTipText = "Tipo dichiarazione";
    , HelpContextID = 3901111;
    , cFormVar="w_AZTIPDIC",RowSource=""+"Normale,"+"Societ� controllata dal gruppo", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAZTIPDIC_1_14.RadioValue()
    return(iif(this.value =1,'N',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oAZTIPDIC_1_14.GetRadio()
    this.Parent.oContained.w_AZTIPDIC = this.RadioValue()
    return .t.
  endfunc

  func oAZTIPDIC_1_14.SetRadio()
    this.Parent.oContained.w_AZTIPDIC=trim(this.Parent.oContained.w_AZTIPDIC)
    this.value = ;
      iif(this.Parent.oContained.w_AZTIPDIC=='N',1,;
      iif(this.Parent.oContained.w_AZTIPDIC=='S',2,;
      0))
  endfunc


  add object oAZTIPDEN_1_15 as StdCombo with uid="PLCGHAEPJY",rtseq=15,rtrep=.f.,left=156,top=159,width=98,height=21;
    , ToolTipText = "Se attivo: denuncia IVA trimenstrale, altrimenti mensile";
    , HelpContextID = 3901100;
    , cFormVar="w_AZTIPDEN",RowSource=""+"Mensile,"+"Trimestrale", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oAZTIPDEN_1_15.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'T',;
    space(1))))
  endfunc
  func oAZTIPDEN_1_15.GetRadio()
    this.Parent.oContained.w_AZTIPDEN = this.RadioValue()
    return .t.
  endfunc

  func oAZTIPDEN_1_15.SetRadio()
    this.Parent.oContained.w_AZTIPDEN=trim(this.Parent.oContained.w_AZTIPDEN)
    this.value = ;
      iif(this.Parent.oContained.w_AZTIPDEN=='M',1,;
      iif(this.Parent.oContained.w_AZTIPDEN=='T',2,;
      0))
  endfunc

  add object oAZDENMAG_1_16 as StdField with uid="LUBHHCOOFS",rtseq=16,rtrep=.f.,;
    cFormVar = "w_AZDENMAG", cQueryName = "AZDENMAG",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di maggiorazione IVA per le denunce trimestrali",;
    HelpContextID = 144669005,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=450, Top=159, cSayPict='"99.99"', cGetPict='"99.99"'

  func oAZDENMAG_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZTIPDEN='T')
    endwith
   endif
  endfunc


  add object oAZSTAREG_1_17 as StdCombo with uid="WCPSUYHQHJ",rtseq=17,rtrep=.f.,left=156,top=188,width=98,height=21;
    , ToolTipText = "Tipo di orientamento moduli di stampa registri IVA (influenza la num.pagine)";
    , HelpContextID = 52467379;
    , cFormVar="w_AZSTAREG",RowSource=""+"Orizzontale,"+"Verticale", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oAZSTAREG_1_17.RadioValue()
    return(iif(this.value =1,'O',;
    iif(this.value =2,'V',;
    space(1))))
  endfunc
  func oAZSTAREG_1_17.GetRadio()
    this.Parent.oContained.w_AZSTAREG = this.RadioValue()
    return .t.
  endfunc

  func oAZSTAREG_1_17.SetRadio()
    this.Parent.oContained.w_AZSTAREG=trim(this.Parent.oContained.w_AZSTAREG)
    this.value = ;
      iif(this.Parent.oContained.w_AZSTAREG=='O',1,;
      iif(this.Parent.oContained.w_AZSTAREG=='V',2,;
      0))
  endfunc


  add object oAZTIPINT_1_18 as StdCombo with uid="LCQSRXWTBM",rtseq=18,rtrep=.f.,left=156,top=217,width=137,height=21;
    , ToolTipText = "Tipo num.pagine lettere intento. se reg. vend. verr� preso in considerazione il reg.princ. dell'attivit� principale.";
    , HelpContextID = 79984986;
    , cFormVar="w_AZTIPINT",RowSource=""+"Separata,"+"Reg.vendite princ.", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oAZTIPINT_1_18.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'V',;
    space(1))))
  endfunc
  func oAZTIPINT_1_18.GetRadio()
    this.Parent.oContained.w_AZTIPINT = this.RadioValue()
    return .t.
  endfunc

  func oAZTIPINT_1_18.SetRadio()
    this.Parent.oContained.w_AZTIPINT=trim(this.Parent.oContained.w_AZTIPINT)
    this.value = ;
      iif(this.Parent.oContained.w_AZTIPINT=='S',1,;
      iif(this.Parent.oContained.w_AZTIPINT=='V',2,;
      0))
  endfunc

  add object oAZPAGINT_1_19 as StdField with uid="RLKKPZQRIJ",rtseq=19,rtrep=.f.,;
    cFormVar = "w_AZPAGINT", cQueryName = "AZPAGINT",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Progressivo pagine lettere di intento",;
    HelpContextID = 70007130,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=439, Top=217, cSayPict='"9999999"', cGetPict='"9999999"'

  func oAZPAGINT_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZTIPINT='S')
    endwith
   endif
  endfunc

  func oAZPAGINT_1_19.mHide()
    with this.Parent.oContained
      return (.w_AZTIPINT<>'S')
    endwith
  endfunc

  add object oAZPREFIS_1_20 as StdField with uid="BOXBPVORLZ",rtseq=20,rtrep=.f.,;
    cFormVar = "w_AZPREFIS", cQueryName = "AZPREFIS",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Prefisso per numerazione pagine",;
    HelpContextID = 249743015,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=156, Top=245, InputMask=replicate('X',20)

  func oAZPREFIS_1_20.mHide()
    with this.Parent.oContained
      return (.w_AZTIPINT<>'S')
    endwith
  endfunc

  add object oCONBAN_1_23 as StdField with uid="BTWPSZFYUB",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CONBAN", cQueryName = "CONBAN",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Codice banca inesistente, obsoleta, di compensazione o salvo buon fine",;
    ToolTipText = "Codice banca",;
    HelpContextID = 147656230,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=161, Top=297, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_CONBAN"

  func oCONBAN_1_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oCONBAN_1_23.ecpDrop(oSource)
    this.Parent.oContained.link_1_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCONBAN_1_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oCONBAN_1_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banche",'GSCG_API.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oCONBAN_1_23.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_CONBAN
     i_obj.ecpSave()
  endproc


  add object oBtn_1_41 as StdButton with uid="HQYNTFYXBV",left=532, top=403, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare";
    , HelpContextID = 108415718;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_41.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_42 as StdButton with uid="ENQXUUYHRU",left=583, top=403, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per abbandonare";
    , HelpContextID = 115704390;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_42.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oAZDESBAN_1_47 as StdField with uid="NRCOLPTNWU",rtseq=101,rtrep=.f.,;
    cFormVar = "w_AZDESBAN", cQueryName = "AZDESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 233797972,;
   bGlobalFont=.t.,;
    Height=21, Width=332, Left=298, Top=297, InputMask=replicate('X',50)

  func oAZDESBAN_1_47.mHide()
    with this.Parent.oContained
      return (g_TESO='S')
    endwith
  endfunc

  add object oABIBAN_1_51 as StdField with uid="VHBVRTXYVH",rtseq=102,rtrep=.f.,;
    cFormVar = "w_ABIBAN", cQueryName = "ABIBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 147632390,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=162, Top=324, InputMask=replicate('X',5)

  func oABIBAN_1_51.mHide()
    with this.Parent.oContained
      return (g_TESO='S')
    endwith
  endfunc

  add object oCABBAN_1_52 as StdField with uid="CBNSVDFSGT",rtseq=103,rtrep=.f.,;
    cFormVar = "w_CABBAN", cQueryName = "CABBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 147603494,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=298, Top=324, InputMask=replicate('X',5)

  func oCABBAN_1_52.mHide()
    with this.Parent.oContained
      return (g_TESO='S')
    endwith
  endfunc

  add object oCONCOR_1_53 as StdField with uid="HOFOWRYSPS",rtseq=104,rtrep=.f.,;
    cFormVar = "w_CONCOR", cQueryName = "CONCOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(12), bMultilanguage =  .f.,;
    ToolTipText = "Conto Corrente",;
    HelpContextID = 229510694,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=470, Top=324, InputMask=replicate('X',12)

  add object oBANPROABI_1_56 as StdField with uid="FQQQNXGIIW",rtseq=105,rtrep=.f.,;
    cFormVar = "w_BANPROABI", cQueryName = "BANPROABI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice ABI banca proponente",;
    HelpContextID = 183174376,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=162, Top=387, InputMask=replicate('X',5)

  add object oBANPROCAB_1_57 as StdField with uid="UIMUAZCRVN",rtseq=106,rtrep=.f.,;
    cFormVar = "w_BANPROCAB", cQueryName = "BANPROCAB",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice CAB banca proponente",;
    HelpContextID = 85261193,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=298, Top=387, InputMask=replicate('X',5)

  add object oBANPROSIA_1_58 as StdField with uid="DQZRJNNZIZ",rtseq=107,rtrep=.f.,;
    cFormVar = "w_BANPROSIA", cQueryName = "BANPROSIA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice SIA banca proponente",;
    HelpContextID = 85261201,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=470, Top=387, InputMask=replicate('X',5)

  add object oBADESCRI_1_62 as StdField with uid="VGHBPVAHAD",rtseq=108,rtrep=.f.,;
    cFormVar = "w_BADESCRI", cQueryName = "BADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 250568799,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=298, Top=354, InputMask=replicate('X',35)

  add object oBANPRO_1_65 as StdField with uid="HJEDYNYTNC",rtseq=109,rtrep=.f.,;
    cFormVar = "w_BANPRO", cQueryName = "BANPRO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice banca inesistente, obsoleta, di compensazione o salvo buon fine",;
    HelpContextID = 183173142,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=161, Top=354, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_BANPRO"

  func oBANPRO_1_65.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_65('Part',this)
    endwith
    return bRes
  endfunc

  proc oBANPRO_1_65.ecpDrop(oSource)
    this.Parent.oContained.link_1_65('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBANPRO_1_65.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oBANPRO_1_65'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banche",'GSCG_API.COC_MAST_VZM',this.parent.oContained
  endproc
  proc oBANPRO_1_65.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_BANPRO
     i_obj.ecpSave()
  endproc

  add object oAZRAGCOR_1_71 as StdCheck with uid="KABRHOPCHO",rtseq=116,rtrep=.f.,left=439, top=245, caption="Raggruppa corrispettivi",;
    ToolTipText = "Se attivo, effettua il raggruppamento dei corrispettivi giornalieri in fase di stampa registri IVA",;
    HelpContextID = 237787480,;
    cFormVar="w_AZRAGCOR", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oAZRAGCOR_1_71.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oAZRAGCOR_1_71.GetRadio()
    this.Parent.oContained.w_AZRAGCOR = this.RadioValue()
    return .t.
  endfunc

  func oAZRAGCOR_1_71.SetRadio()
    this.Parent.oContained.w_AZRAGCOR=trim(this.Parent.oContained.w_AZRAGCOR)
    this.value = ;
      iif(this.Parent.oContained.w_AZRAGCOR=='S',1,;
      0)
  endfunc

  add object oStr_1_21 as StdString with uid="IMCHKXCKZI",Visible=.t., Left=75, Top=11,;
    Alignment=1, Width=78, Height=15,;
    Caption="Ufficio IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="CRJJGLJJTR",Visible=.t., Left=100, Top=98,;
    Alignment=1, Width=53, Height=15,;
    Caption="Attivit�:"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (.w_AZATTIVI='S')
    endwith
  endfunc

  add object oStr_1_32 as StdString with uid="GVWIETCIKL",Visible=.t., Left=301, Top=161,;
    Alignment=1, Width=146, Height=15,;
    Caption="% Maggiorazione IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="GTEZIBOMJQ",Visible=.t., Left=53, Top=188,;
    Alignment=1, Width=99, Height=15,;
    Caption="Orientamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="BFLNHWFYPX",Visible=.t., Left=49, Top=40,;
    Alignment=1, Width=104, Height=15,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="RDXGOLJHQQ",Visible=.t., Left=50, Top=69,;
    Alignment=1, Width=103, Height=15,;
    Caption="Cod.concessione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="RTNYKQQRRV",Visible=.t., Left=199, Top=69,;
    Alignment=1, Width=51, Height=15,;
    Caption="Carica:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="WBVFJEQVLO",Visible=.t., Left=38, Top=98,;
    Alignment=1, Width=115, Height=15,;
    Caption="Attivit� principale:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (.w_AZATTIVI<>'S')
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="AWVIPJVDGK",Visible=.t., Left=53, Top=159,;
    Alignment=1, Width=100, Height=18,;
    Caption="Periodicit� IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="MKZDJFBGJZ",Visible=.t., Left=4, Top=129,;
    Alignment=1, Width=230, Height=18,;
    Caption="Tipo liquidazione IVA di gruppo (art. 73):"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="EZAVHUCUBY",Visible=.t., Left=48, Top=216,;
    Alignment=1, Width=104, Height=18,;
    Caption="N.pag.lett.intento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="TVUVLXQMKA",Visible=.t., Left=312, Top=216,;
    Alignment=1, Width=123, Height=18,;
    Caption="Progressivo pagina:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (.w_AZTIPINT<>'S')
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="TCWAIJDHZN",Visible=.t., Left=3, Top=276,;
    Alignment=0, Width=207, Height=15,;
    Caption="Dati per la gestione del modello F24"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="QHZVIYRQAD",Visible=.t., Left=2, Top=297,;
    Alignment=1, Width=157, Height=18,;
    Caption="Conto Corrente Versamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="KWKJBRACQK",Visible=.t., Left=88, Top=324,;
    Alignment=1, Width=71, Height=15,;
    Caption="Codice ABI:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="UFYMKANPEY",Visible=.t., Left=264, Top=324,;
    Alignment=1, Width=30, Height=15,;
    Caption="CAB:"  ;
  , bGlobalFont=.t.

  add object oStr_1_54 as StdString with uid="CHLUVQNXPP",Visible=.t., Left=350, Top=324,;
    Alignment=1, Width=117, Height=18,;
    Caption="Num.C/Corrente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="ZBRTWVBUCK",Visible=.t., Left=33, Top=355,;
    Alignment=1, Width=126, Height=18,;
    Caption="Banca proponente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="MNBNDRRUYS",Visible=.t., Left=96, Top=390,;
    Alignment=0, Width=63, Height=15,;
    Caption="Codice ABI:"  ;
  , bGlobalFont=.t.

  add object oStr_1_60 as StdString with uid="DLBRTBFBSC",Visible=.t., Left=267, Top=390,;
    Alignment=0, Width=27, Height=15,;
    Caption="CAB:"  ;
  , bGlobalFont=.t.

  add object oStr_1_61 as StdString with uid="NFKBXGDDCZ",Visible=.t., Left=404, Top=390,;
    Alignment=0, Width=63, Height=18,;
    Caption="Codice SIA:"  ;
  , bGlobalFont=.t.

  add object oStr_1_68 as StdString with uid="VUSBEGTHJF",Visible=.t., Left=11, Top=248,;
    Alignment=1, Width=141, Height=18,;
    Caption="Prefisso per num. pag.:"  ;
  , bGlobalFont=.t.

  func oStr_1_68.mHide()
    with this.Parent.oContained
      return (.w_AZTIPINT<>'S')
    endwith
  endfunc

  add object oStr_1_69 as StdString with uid="EWWFZEZKOA",Visible=.t., Left=82, Top=529,;
    Alignment=0, Width=300, Height=22,;
    Caption="ATTENZIONE da eliminare se BUILD 55"  ;
    , FontName = "Arial", FontSize = 12, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_70 as StdString with uid="BNPFUPJAJG",Visible=.t., Left=82, Top=554,;
    Alignment=0, Width=517, Height=22,;
    Caption="IN CASO DI GENERAZIONE AGGIORNARE NEL PRG  i_nFlds = 200"  ;
    , FontName = "Arial", FontSize = 12, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oBox_1_48 as StdBox with uid="YAXYPOIOLN",left=3, top=291, width=629,height=1
enddefine
define class tgscg_apiPag2 as StdContainer
  Width  = 637
  height = 450
  stdWidth  = 637
  stdheight = 450
  resizeYpos=122
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="CRMTFTMRAT",left=6, top=14, width=619, height=379, bOnScreen=.t.;


  add object oAZPLNODO_2_2 as StdCheck with uid="VLDXCPYXWD",rtseq=60,rtrep=.f.,left=20, top=399, caption="Escludi doc. da stampa plafond",;
    ToolTipText = "Se attivo: la stampa plafond non considera il ciclo documentale",;
    HelpContextID = 89704107,;
    cFormVar="w_AZPLNODO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAZPLNODO_2_2.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZPLNODO_2_2.GetRadio()
    this.Parent.oContained.w_AZPLNODO = this.RadioValue()
    return .t.
  endfunc

  func oAZPLNODO_2_2.SetRadio()
    this.Parent.oContained.w_AZPLNODO=trim(this.Parent.oContained.w_AZPLNODO)
    this.value = ;
      iif(this.Parent.oContained.w_AZPLNODO=='S',1,;
      0)
  endfunc

  add object oAZPLADOC_2_3 as StdCheck with uid="GCYISPXFAY",rtseq=61,rtrep=.f.,left=20, top=421, caption="Consumo plafond su data documento",;
    ToolTipText = "Se attivo: la data consumo plafond nei documenti viene inizializzata sulla data documento. Altrimenti sulla data registrazione",;
    HelpContextID = 248985929,;
    cFormVar="w_AZPLADOC", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oAZPLADOC_2_3.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZPLADOC_2_3.GetRadio()
    this.Parent.oContained.w_AZPLADOC = this.RadioValue()
    return .t.
  endfunc

  func oAZPLADOC_2_3.SetRadio()
    this.Parent.oContained.w_AZPLADOC=trim(this.Parent.oContained.w_AZPLADOC)
    this.value = ;
      iif(this.Parent.oContained.w_AZPLADOC=='S',1,;
      0)
  endfunc

  func oAZPLADOC_2_3.mHide()
    with this.Parent.oContained
      return (.w_AZPLNODO='S')
    endwith
  endfunc
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscg_mdi",lower(this.oContained.GSCG_MDI.class))=0
        this.oContained.GSCG_MDI.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine
define class tgscg_apiPag3 as StdContainer
  Width  = 637
  height = 450
  stdWidth  = 637
  stdheight = 450
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oAZTIPFOR_3_11 as StdCombo with uid="DCBUYNZOBW",rtseq=41,rtrep=.f.,left=118,top=20,width=250,height=21;
    , ToolTipText = "Tipo fornitore associato alla dichiarazione IVA";
    , HelpContextID = 29653336;
    , cFormVar="w_AZTIPFOR",RowSource=""+"01: Soggetti che utilizzano il canale Internet,"+"02: Soggetti che utilizzano il canale Entratel,"+"03: C.A.F. dipendenti e pensionati,"+"05: C.A.F: imprese,"+"09: Art 3 comma 2,"+"10: Altri intermediari", bObbl = .f. , nPag = 3;
  , bGlobalFont=.t.


  func oAZTIPFOR_3_11.RadioValue()
    return(iif(this.value =1,'01',;
    iif(this.value =2,'02',;
    iif(this.value =3,'03',;
    iif(this.value =4,'05',;
    iif(this.value =5,'09',;
    iif(this.value =6,'10',;
    space(2))))))))
  endfunc
  func oAZTIPFOR_3_11.GetRadio()
    this.Parent.oContained.w_AZTIPFOR = this.RadioValue()
    return .t.
  endfunc

  func oAZTIPFOR_3_11.SetRadio()
    this.Parent.oContained.w_AZTIPFOR=trim(this.Parent.oContained.w_AZTIPFOR)
    this.value = ;
      iif(this.Parent.oContained.w_AZTIPFOR=='01',1,;
      iif(this.Parent.oContained.w_AZTIPFOR=='02',2,;
      iif(this.Parent.oContained.w_AZTIPFOR=='03',3,;
      iif(this.Parent.oContained.w_AZTIPFOR=='05',4,;
      iif(this.Parent.oContained.w_AZTIPFOR=='09',5,;
      iif(this.Parent.oContained.w_AZTIPFOR=='10',6,;
      0))))))
  endfunc

  proc oAZTIPFOR_3_11.mDefault
    with this.Parent.oContained
      if empty(.w_AZTIPFOR)
        .w_AZTIPFOR = '01'
      endif
    endwith
  endproc

  add object oAZFISFOR_3_13 as StdCheck with uid="FNIFDPFVNP",rtseq=42,rtrep=.f.,left=462, top=20, caption="Persona fisica",;
    ToolTipText = "Se attivo: fornitore persona fisica",;
    HelpContextID = 32741720,;
    cFormVar="w_AZFISFOR", bObbl = .f. , nPag = 3;
   , bGlobalFont=.t.


  func oAZFISFOR_3_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oAZFISFOR_3_13.GetRadio()
    this.Parent.oContained.w_AZFISFOR = this.RadioValue()
    return .t.
  endfunc

  func oAZFISFOR_3_13.SetRadio()
    this.Parent.oContained.w_AZFISFOR=trim(this.Parent.oContained.w_AZFISFOR)
    this.value = ;
      iif(this.Parent.oContained.w_AZFISFOR=='S',1,;
      0)
  endfunc

  add object oAZCOFFOR_3_14 as StdField with uid="LGRDCXDZXF",rtseq=43,rtrep=.f.,;
    cFormVar = "w_AZCOFFOR", cQueryName = "AZCOFFOR",;
    bObbl = .f. , nPag = 3, value=space(16), bMultilanguage =  .f.,;
    ToolTipText = "Codice fiscale del fornitore",;
    HelpContextID = 19491160,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=118, Top=46, InputMask=replicate('X',16)

  func oAZCOFFOR_3_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (EMPTY(.w_AZCOFFOR) OR CHKCFP(.w_AZCOFFOR,"CF"))
    endwith
    return bRes
  endfunc

  add object oAZNUMCAF_3_15 as StdField with uid="WISVZXZDSS",rtseq=44,rtrep=.f.,;
    cFormVar = "w_AZNUMCAF", cQueryName = "AZNUMCAF",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Numero di iscrizione all'albo dei C.A.F.",;
    HelpContextID = 245373260,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=462, Top=46, cSayPict="'99999'", cGetPict="'99999'", InputMask=replicate('X',5)

  add object oAZCOGFOR_3_16 as StdField with uid="GEZQVMOLDP",rtseq=45,rtrep=.f.,;
    cFormVar = "w_AZCOGFOR", cQueryName = "AZCOGFOR",;
    bObbl = .f. , nPag = 3, value=space(24), bMultilanguage =  .f.,;
    ToolTipText = "Cognome del fornitore",;
    HelpContextID = 20539736,;
   bGlobalFont=.t.,;
    Height=21, Width=181, Left=118, Top=74, InputMask=replicate('X',24)

  func oAZCOGFOR_3_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZFISFOR='S')
    endwith
   endif
  endfunc

  add object oAZNOMFOR_3_17 as StdField with uid="TNIUKXBZPP",rtseq=46,rtrep=.f.,;
    cFormVar = "w_AZNOMFOR", cQueryName = "AZNOMFOR",;
    bObbl = .f. , nPag = 3, value=space(20), bMultilanguage =  .f.,;
    ToolTipText = "Nome del fornitore",;
    HelpContextID = 26876248,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=462, Top=74, InputMask=replicate('X',20)

  func oAZNOMFOR_3_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZFISFOR='S')
    endwith
   endif
  endfunc

  add object oAZSEXFOR_3_18 as StdRadio with uid="TOHEBFKBOC",rtseq=47,rtrep=.f.,left=118, top=102, width=163,height=23;
    , cFormVar="w_AZSEXFOR", ButtonCount=2, bObbl=.f., nPag=3;
  , bGlobalFont=.t.

    proc oAZSEXFOR_3_18.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Maschio"
      this.Buttons(1).HelpContextID = 37775704
      this.Buttons(1).Left=i_coord
      this.Buttons(1).Width=(TxtWidth("Maschio","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(1).Width
      this.Buttons(2).Caption="Femmina"
      this.Buttons(2).HelpContextID = 37775704
      this.Buttons(2).Left=i_coord
      this.Buttons(2).Width=(TxtWidth("Femmina","Arial",9,"")*FontMetric(6,"Arial",9,""))+21
      i_coord=i_coord+this.Buttons(2).Width
      this.SetAll("Height",23)
      this.SetAll("Top",0)
      this.SetAll("BackStyle",0)
      StdRadio::init()
    endproc

  func oAZSEXFOR_3_18.RadioValue()
    return(iif(this.value =1,'M',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oAZSEXFOR_3_18.GetRadio()
    this.Parent.oContained.w_AZSEXFOR = this.RadioValue()
    return .t.
  endfunc

  func oAZSEXFOR_3_18.SetRadio()
    this.Parent.oContained.w_AZSEXFOR=trim(this.Parent.oContained.w_AZSEXFOR)
    this.value = ;
      iif(this.Parent.oContained.w_AZSEXFOR=='M',1,;
      iif(this.Parent.oContained.w_AZSEXFOR=='F',2,;
      0))
  endfunc

  func oAZSEXFOR_3_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZFISFOR='S')
    endwith
   endif
  endfunc

  add object oAZDATFOR_3_19 as StdField with uid="WMPERYACIO",rtseq=48,rtrep=.f.,;
    cFormVar = "w_AZDATFOR", cQueryName = "AZDATFOR",;
    bObbl = .f. , nPag = 3, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di nascita del fornitore",;
    HelpContextID = 33257816,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=462, Top=102

  func oAZDATFOR_3_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZFISFOR='S')
    endwith
   endif
  endfunc

  add object oAZCONFOR_3_20 as StdField with uid="MEFYCGJBAL",rtseq=49,rtrep=.f.,;
    cFormVar = "w_AZCONFOR", cQueryName = "AZCONFOR",;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune o stato di nascita del fornitore",;
    HelpContextID = 27879768,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=118, Top=129, InputMask=replicate('X',40)

  func oAZCONFOR_3_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZFISFOR='S')
    endwith
   endif
  endfunc

  add object oAZPRNFOR_3_21 as StdField with uid="QSYIWCLYVZ",rtseq=50,rtrep=.f.,;
    cFormVar = "w_AZPRNFOR", cQueryName = "AZPRNFOR",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia di nascita del fornitore",;
    HelpContextID = 28129624,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=462, Top=129, InputMask=replicate('X',2)

  func oAZPRNFOR_3_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZFISFOR='S')
    endwith
   endif
  endfunc

  add object oAZDENFOR_3_29 as StdField with uid="FYHZEQEDMB",rtseq=51,rtrep=.f.,;
    cFormVar = "w_AZDENFOR", cQueryName = "AZDENFOR",;
    bObbl = .f. , nPag = 3, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Denominazione fornitore",;
    HelpContextID = 27228504,;
   bGlobalFont=.t.,;
    Height=21, Width=433, Left=118, Top=180, InputMask=replicate('X',60)

  add object oAZCOMFOR_3_30 as StdField with uid="DOMJMLZMGP",rtseq=52,rtrep=.f.,;
    cFormVar = "w_AZCOMFOR", cQueryName = "AZCOMFOR",;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune residenza anagrafica o domicilio fiscale del fornitore",;
    HelpContextID = 26831192,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=118, Top=206, InputMask=replicate('X',40)

  add object oAZPROFOR_3_31 as StdField with uid="MRRVCZZHAY",rtseq=53,rtrep=.f.,;
    cFormVar = "w_AZPROFOR", cQueryName = "AZPROFOR",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provincia residenza anagrafica o domicilio fiscale del fornitore",;
    HelpContextID = 29178200,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=462, Top=206, InputMask=replicate('X',2)

  add object oAZINDFOR_3_32 as StdField with uid="JFAUPCVSMD",rtseq=54,rtrep=.f.,;
    cFormVar = "w_AZINDFOR", cQueryName = "AZINDFOR",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo residenza anagrafica o domicilio fiscale del fornitore",;
    HelpContextID = 17353048,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=118, Top=232, InputMask=replicate('X',35)

  add object oAZCAPFOR_3_33 as StdField with uid="NOKTKDPADT",rtseq=55,rtrep=.f.,;
    cFormVar = "w_AZCAPFOR", cQueryName = "AZCAPFOR",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "CAP residenza anagrafica o domicilio fiscale del fornitore",;
    HelpContextID = 29059416,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=462, Top=232, InputMask=replicate('X',5)

  add object oAZCOLFOR_3_38 as StdField with uid="TQRAFUDQGV",rtseq=56,rtrep=.f.,;
    cFormVar = "w_AZCOLFOR", cQueryName = "AZCOLFOR",;
    bObbl = .f. , nPag = 3, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Comune sede legale del fornitore",;
    HelpContextID = 25782616,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=118, Top=283, InputMask=replicate('X',40)

  add object oAZPRLFOR_3_39 as StdField with uid="BWDKMNOUAI",rtseq=57,rtrep=.f.,;
    cFormVar = "w_AZPRLFOR", cQueryName = "AZPRLFOR",;
    bObbl = .f. , nPag = 3, value=space(2), bMultilanguage =  .f.,;
    ToolTipText = "Provicia sede legale del fornitore",;
    HelpContextID = 26032472,;
   bGlobalFont=.t.,;
    Height=21, Width=27, Left=462, Top=283, InputMask=replicate('X',2)

  add object oAZINLFOR_3_40 as StdField with uid="TSBGOYVCRX",rtseq=58,rtrep=.f.,;
    cFormVar = "w_AZINLFOR", cQueryName = "AZINLFOR",;
    bObbl = .f. , nPag = 3, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Indirizzo sede legale del fornitore",;
    HelpContextID = 25741656,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=118, Top=310, InputMask=replicate('X',35)

  add object oAZCALFOR_3_41 as StdField with uid="HRBAQFODAQ",rtseq=59,rtrep=.f.,;
    cFormVar = "w_AZCALFOR", cQueryName = "AZCALFOR",;
    bObbl = .f. , nPag = 3, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "CAP sede legale del fornitore",;
    HelpContextID = 24865112,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=462, Top=310, InputMask=replicate('X',5)

  add object oStr_3_12 as StdString with uid="WJQQLAGRNB",Visible=.t., Left=11, Top=20,;
    Alignment=1, Width=103, Height=18,;
    Caption="Tipo fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_3_22 as StdString with uid="IXIGZORHTQ",Visible=.t., Left=11, Top=75,;
    Alignment=1, Width=103, Height=18,;
    Caption="Cognome:"  ;
  , bGlobalFont=.t.

  add object oStr_3_23 as StdString with uid="FXNCZNIFLH",Visible=.t., Left=401, Top=74,;
    Alignment=1, Width=59, Height=18,;
    Caption="Nome:"  ;
  , bGlobalFont=.t.

  add object oStr_3_24 as StdString with uid="WYNIIEAKLY",Visible=.t., Left=11, Top=100,;
    Alignment=1, Width=103, Height=18,;
    Caption="Sesso:"  ;
  , bGlobalFont=.t.

  add object oStr_3_25 as StdString with uid="RCDDXTKFJI",Visible=.t., Left=11, Top=129,;
    Alignment=1, Width=103, Height=18,;
    Caption="Nato a:"  ;
  , bGlobalFont=.t.

  add object oStr_3_26 as StdString with uid="DGKXRZCMMH",Visible=.t., Left=421, Top=129,;
    Alignment=1, Width=39, Height=18,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_3_27 as StdString with uid="YWEQFXHRAI",Visible=.t., Left=386, Top=102,;
    Alignment=1, Width=74, Height=18,;
    Caption="Nato il:"  ;
  , bGlobalFont=.t.

  add object oStr_3_28 as StdString with uid="ONNNCHCLCY",Visible=.t., Left=8, Top=159,;
    Alignment=0, Width=321, Height=18,;
    Caption="Residenza anagrafica o domicilio fiscale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_34 as StdString with uid="THVBXUWBSZ",Visible=.t., Left=11, Top=206,;
    Alignment=1, Width=103, Height=18,;
    Caption="Comune:"  ;
  , bGlobalFont=.t.

  add object oStr_3_35 as StdString with uid="OWNBVVSMHL",Visible=.t., Left=421, Top=206,;
    Alignment=1, Width=39, Height=18,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_3_36 as StdString with uid="YSQYWUMWEQ",Visible=.t., Left=11, Top=232,;
    Alignment=1, Width=103, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_3_37 as StdString with uid="KNZHEWYJFI",Visible=.t., Left=394, Top=232,;
    Alignment=1, Width=66, Height=18,;
    Caption="CAP:"  ;
  , bGlobalFont=.t.

  add object oStr_3_42 as StdString with uid="QIFGXLVHLE",Visible=.t., Left=11, Top=283,;
    Alignment=1, Width=103, Height=18,;
    Caption="Comune:"  ;
  , bGlobalFont=.t.

  add object oStr_3_43 as StdString with uid="IFCNRIYWMJ",Visible=.t., Left=421, Top=283,;
    Alignment=1, Width=39, Height=18,;
    Caption="Prov.:"  ;
  , bGlobalFont=.t.

  add object oStr_3_44 as StdString with uid="ZTPJEURECY",Visible=.t., Left=11, Top=310,;
    Alignment=1, Width=103, Height=18,;
    Caption="Indirizzo:"  ;
  , bGlobalFont=.t.

  add object oStr_3_45 as StdString with uid="QKNNJNEEBC",Visible=.t., Left=394, Top=310,;
    Alignment=1, Width=66, Height=18,;
    Caption="CAP:"  ;
  , bGlobalFont=.t.

  add object oStr_3_46 as StdString with uid="YBPPPWXGUK",Visible=.t., Left=11, Top=180,;
    Alignment=1, Width=103, Height=18,;
    Caption="Denominazione:"  ;
  , bGlobalFont=.t.

  add object oStr_3_47 as StdString with uid="ZZODPWAMKX",Visible=.t., Left=8, Top=263,;
    Alignment=0, Width=180, Height=18,;
    Caption="Sede legale"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_3_48 as StdString with uid="TXLKOGSZFE",Visible=.t., Left=11, Top=46,;
    Alignment=1, Width=103, Height=18,;
    Caption="Codice fiscale:"  ;
  , bGlobalFont=.t.

  add object oStr_3_49 as StdString with uid="TOKOVMBJJC",Visible=.t., Left=319, Top=46,;
    Alignment=1, Width=141, Height=18,;
    Caption="N.iscrizione al C.A.F.:"  ;
  , bGlobalFont=.t.

  add object oBox_3_50 as StdBox with uid="YMAJYHJFFX",left=3, top=261, width=634,height=2

  add object oBox_3_51 as StdBox with uid="RUETOKQQIC",left=3, top=156, width=634,height=2
enddefine
define class tgscg_apiPag4 as StdContainer
  Width  = 637
  height = 450
  stdWidth  = 637
  stdheight = 450
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCAUINC_4_4 as StdField with uid="QZBJTPPOPN",rtseq=88,rtrep=.f.,;
    cFormVar = "w_CAUINC", cQueryName = "CAUINC",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale rilevazione incassi incongruente o inesistente",;
    ToolTipText = "Causale contabile dei movimenti di rilevazione incassi ad esigib.differita",;
    HelpContextID = 245657638,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=276, Top=32, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_CAUINC"

  func oCAUINC_4_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUINC_4_4.ecpDrop(oSource)
    this.Parent.oContained.link_4_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUINC_4_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oCAUINC_4_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSCGIKGD.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oCAUINC_4_4.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_CAUINC
     i_obj.ecpSave()
  endproc

  add object oAZCAUPAG_4_6 as StdField with uid="EFPMFLMQQL",rtseq=89,rtrep=.f.,;
    cFormVar = "w_AZCAUPAG", cQueryName = "AZCAUPAG",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale rilevazione pagamenti incongruente o inesistente",;
    ToolTipText = "Causale contabile dei movimenti di rilevazione pagamenti ad esigib.differita",;
    HelpContextID = 202074445,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=276, Top=75, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_AZCAUPAG"

  func oAZCAUPAG_4_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZCAUPAG_4_6.ecpDrop(oSource)
    this.Parent.oContained.link_4_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZCAUPAG_4_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oAZCAUPAG_4_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSCGPKGD.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oAZCAUPAG_4_6.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_AZCAUPAG
     i_obj.ecpSave()
  endproc


  add object oAZDATCON_4_7 as StdCombo with uid="QQVRBOBWKN",rtseq=90,rtrep=.f.,left=276,top=118,width=143,height=22;
    , ToolTipText = "Data sulla quale verr� effettuato  il controllo per la decorrenza temporale dell'esigibilit� IVA in sospensione";
    , HelpContextID = 251361620;
    , cFormVar="w_AZDATCON",RowSource=""+"Data competenza IVA,"+"Data registrazione,"+"Data documento", bObbl = .f. , nPag = 4;
  , bGlobalFont=.t.


  func oAZDATCON_4_7.RadioValue()
    return(iif(this.value =1,'I',;
    iif(this.value =2,'R',;
    iif(this.value =3,'D',;
    space(1)))))
  endfunc
  func oAZDATCON_4_7.GetRadio()
    this.Parent.oContained.w_AZDATCON = this.RadioValue()
    return .t.
  endfunc

  func oAZDATCON_4_7.SetRadio()
    this.Parent.oContained.w_AZDATCON=trim(this.Parent.oContained.w_AZDATCON)
    this.value = ;
      iif(this.Parent.oContained.w_AZDATCON=='I',1,;
      iif(this.Parent.oContained.w_AZDATCON=='R',2,;
      iif(this.Parent.oContained.w_AZDATCON=='D',3,;
      0)))
  endfunc

  add object oAZCAUMTA_4_8 as StdField with uid="DQWDCLXDML",rtseq=91,rtrep=.f.,;
    cFormVar = "w_AZCAUMTA", cQueryName = "AZCAUMTA",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale per maturazione temporale incongruente o inesistente",;
    ToolTipText = "Causale contabile attiva per maturazione temporale IVA in sospensione",;
    HelpContextID = 116692665,;
   bGlobalFont=.t.,;
    Height=19, Width=62, Left=276, Top=161, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_AZCAUMTA"

  func oAZCAUMTA_4_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZCAUMTA_4_8.ecpDrop(oSource)
    this.Parent.oContained.link_4_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZCAUMTA_4_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oAZCAUMTA_4_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSCGIKGD.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oAZCAUMTA_4_8.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_AZCAUMTA
     i_obj.ecpSave()
  endproc

  add object oAZCAUMTP_4_9 as StdField with uid="WTKYNQYXWJ",rtseq=92,rtrep=.f.,;
    cFormVar = "w_AZCAUMTP", cQueryName = "AZCAUMTP",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale per maturazione temporale incongruente o inesistente",;
    ToolTipText = "Causale contabile passiva per maturazione temporale IVA in sospensione",;
    HelpContextID = 116692650,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=276, Top=202, cSayPict="'!!!!!'", cGetPict="'!!!!!'", InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_AZCAUMTP"

  func oAZCAUMTP_4_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZCAUMTP_4_9.ecpDrop(oSource)
    this.Parent.oContained.link_4_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZCAUMTP_4_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oAZCAUMTP_4_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSCGPKGD.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oAZCAUMTP_4_9.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_AZCAUMTP
     i_obj.ecpSave()
  endproc

  add object oAZCAUAUT_4_13 as StdField with uid="GGFJIGRMDV",rtseq=93,rtrep=.f.,;
    cFormVar = "w_AZCAUAUT", cQueryName = "AZCAUAUT",;
    bObbl = .f. , nPag = 4, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale storno IVA autotrasportatori incongruente o inesistente",;
    ToolTipText = "Causale contabile di giroconto IVA sospesa autotrasportatori",;
    HelpContextID = 49583782,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=147, Top=281, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_AZCAUAUT"

  func oAZCAUAUT_4_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZCAUAUT_4_13.ecpDrop(oSource)
    this.Parent.oContained.link_4_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZCAUAUT_4_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oAZCAUAUT_4_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSCG_AUT.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oAZCAUAUT_4_13.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_AZCAUAUT
     i_obj.ecpSave()
  endproc

  add object oAZIVAAUT_4_14 as StdField with uid="VLEDXKDXQV",rtseq=94,rtrep=.f.,;
    cFormVar = "w_AZIVAAUT", cQueryName = "AZIVAAUT",;
    bObbl = .f. , nPag = 4, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente, di tipo non IVA oppure obsoleto",;
    ToolTipText = "Conto IVA da utilizzarsi nel giroconto IVA autotrasportatori",;
    HelpContextID = 69154470,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=147, Top=324, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_AZIVAAUT"

  func oAZIVAAUT_4_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_4_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZIVAAUT_4_14.ecpDrop(oSource)
    this.Parent.oContained.link_4_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZIVAAUT_4_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oAZIVAAUT_4_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti IVA",'GSCG_ACC.CONTI_VZM',this.parent.oContained
  endproc
  proc oAZIVAAUT_4_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_AZIVAAUT
     i_obj.ecpSave()
  endproc

  add object oAZDATAUT_4_16 as StdField with uid="WRRVFOKBKP",rtseq=95,rtrep=.f.,;
    cFormVar = "w_AZDATAUT", cQueryName = "AZDATAUT",;
    bObbl = .f. , nPag = 4, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data ultimo giroconto IVA sospesa autotrasportatori",;
    HelpContextID = 50628262,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=147, Top=367

  add object oDESINC_4_17 as StdField with uid="EMFEOGKNDH",rtseq=96,rtrep=.f.,;
    cFormVar = "w_DESINC", cQueryName = "DESINC",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 245650486,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=354, Top=32, InputMask=replicate('X',35)

  add object oDESPAG_4_18 as StdField with uid="UZLKOWPRWX",rtseq=97,rtrep=.f.,;
    cFormVar = "w_DESPAG", cQueryName = "DESPAG",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 31151158,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=354, Top=75, InputMask=replicate('X',35)

  add object oDESAUT_4_19 as StdField with uid="DXINEOZHPJ",rtseq=98,rtrep=.f.,;
    cFormVar = "w_DESAUT", cQueryName = "DESAUT",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 807990,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=213, Top=281, InputMask=replicate('X',35)

  add object oDESCON_4_21 as StdField with uid="ZCFGUMOZDC",rtseq=99,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 162419766,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=267, Top=324, InputMask=replicate('X',40)

  add object oDESINCAT_4_28 as StdField with uid="JGIKVBZZZH",rtseq=110,rtrep=.f.,;
    cFormVar = "w_DESINCAT", cQueryName = "DESINCAT",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 245650570,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=354, Top=161, InputMask=replicate('X',35)

  add object oDESPAGPT_4_29 as StdField with uid="KIRRRPGQDX",rtseq=111,rtrep=.f.,;
    cFormVar = "w_DESPAGPT", cQueryName = "DESPAGPT",enabled=.f.,;
    bObbl = .f. , nPag = 4, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 31151242,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=354, Top=202, InputMask=replicate('X',35)

  add object oStr_4_1 as StdString with uid="MKHMQMCYPZ",Visible=.t., Left=5, Top=7,;
    Alignment=0, Width=180, Height=18,;
    Caption="IVA esigibilit� differita"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_2 as StdString with uid="VXVMSKBGRZ",Visible=.t., Left=5, Top=243,;
    Alignment=0, Width=219, Height=18,;
    Caption="IVA autotrasportatori"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_4_3 as StdString with uid="PZPPLRINKT",Visible=.t., Left=130, Top=35,;
    Alignment=1, Width=140, Height=18,;
    Caption="Causale per incassi:"  ;
  , bGlobalFont=.t.

  add object oStr_4_5 as StdString with uid="DBOGLGDYAH",Visible=.t., Left=130, Top=77,;
    Alignment=1, Width=140, Height=18,;
    Caption="Causale per pagamenti:"  ;
  , bGlobalFont=.t.

  add object oStr_4_12 as StdString with uid="RDUMWLZWML",Visible=.t., Left=5, Top=284,;
    Alignment=1, Width=140, Height=18,;
    Caption="Causale per storni:"  ;
  , bGlobalFont=.t.

  add object oStr_4_15 as StdString with uid="ELHISNQGGA",Visible=.t., Left=5, Top=368,;
    Alignment=1, Width=140, Height=18,;
    Caption="Data ultimo storno:"  ;
  , bGlobalFont=.t.

  add object oStr_4_20 as StdString with uid="WDGXZNNQPY",Visible=.t., Left=5, Top=324,;
    Alignment=1, Width=140, Height=18,;
    Caption="Conto IVA per storni:"  ;
  , bGlobalFont=.t.

  add object oStr_4_25 as StdString with uid="CGYBMLZYOY",Visible=.t., Left=28, Top=120,;
    Alignment=1, Width=242, Height=18,;
    Caption="Controllo maturazione IVA sospesa su:"  ;
  , bGlobalFont=.t.

  add object oStr_4_26 as StdString with uid="FLJKCUZAFS",Visible=.t., Left=22, Top=163,;
    Alignment=1, Width=248, Height=18,;
    Caption="Causale attiva per maturazione temporale:"  ;
  , bGlobalFont=.t.

  add object oStr_4_27 as StdString with uid="INHBZTAJTR",Visible=.t., Left=2, Top=204,;
    Alignment=1, Width=268, Height=18,;
    Caption="Causale passiva per maturazione temporale:"  ;
  , bGlobalFont=.t.

  add object oBox_4_10 as StdBox with uid="EBYIBUGZEI",left=0, top=25, width=634,height=2

  add object oBox_4_11 as StdBox with uid="JDMPUZLDGE",left=0, top=262, width=634,height=2
enddefine
define class tgscg_apiPag5 as StdContainer
  Width  = 637
  height = 450
  stdWidth  = 637
  stdheight = 450
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oAZGIRIVA_5_1 as StdCombo with uid="JFEEBGGMFM",rtseq=62,rtrep=.f.,left=178,top=16,width=227,height=21;
    , ToolTipText = "Se automatico, la procedura al momento della stampa liquidazione genera il movimento di giroconto IVA in ventilazione";
    , HelpContextID = 186406585;
    , cFormVar="w_AZGIRIVA",RowSource=""+"Manuale,"+"Automatico", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oAZGIRIVA_5_1.RadioValue()
    return(iif(this.value =1,'NO',;
    iif(this.value =2,'SI',;
    space(2))))
  endfunc
  func oAZGIRIVA_5_1.GetRadio()
    this.Parent.oContained.w_AZGIRIVA = this.RadioValue()
    return .t.
  endfunc

  func oAZGIRIVA_5_1.SetRadio()
    this.Parent.oContained.w_AZGIRIVA=trim(this.Parent.oContained.w_AZGIRIVA)
    this.value = ;
      iif(this.Parent.oContained.w_AZGIRIVA=='NO',1,;
      iif(this.Parent.oContained.w_AZGIRIVA=='SI',2,;
      0))
  endfunc

  add object oAZCAUIVA_5_3 as StdField with uid="VOLAWMLYMV",rtseq=63,rtrep=.f.,;
    cFormVar = "w_AZCAUIVA", cQueryName = "AZCAUIVA",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale giroconto IVA ventilata impostata di default",;
    HelpContextID = 183801529,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=178, Top=48, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_AZCAUIVA"

  func oAZCAUIVA_5_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZCAUIVA_5_3.ecpDrop(oSource)
    this.Parent.oContained.link_5_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZCAUIVA_5_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oAZCAUIVA_5_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSCG0KCA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oAZCAUIVA_5_3.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_AZCAUIVA
     i_obj.ecpSave()
  endproc

  add object oAZCONIVA_5_4 as StdField with uid="VJPWQFLPOJ",rtseq=64,rtrep=.f.,;
    cFormVar = "w_AZCONIVA", cQueryName = "AZCONIVA",;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente, di tipo non IVA oppure obsoleto",;
    ToolTipText = "Conto IVA su corrispettivi impostato di default",;
    HelpContextID = 190224057,;
   bGlobalFont=.t.,;
    Height=21, Width=119, Left=178, Top=76, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_AZCONIVA"

  func oAZCONIVA_5_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZCONIVA_5_4.ecpDrop(oSource)
    this.Parent.oContained.link_5_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZCONIVA_5_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oAZCONIVA_5_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'GSCG0KC1.CONTI_VZM',this.parent.oContained
  endproc
  proc oAZCONIVA_5_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_AZCONIVA
     i_obj.ecpSave()
  endproc

  add object oAZCODVEN_5_5 as StdField with uid="OKDDZTYCYE",rtseq=65,rtrep=.f.,;
    cFormVar = "w_AZCODVEN", cQueryName = "AZCODVEN",;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, conto non di tipo contropartite vendite",;
    ToolTipText = "Conto vendite corrispettivi",;
    HelpContextID = 251041452,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=178, Top=104, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_AZCODVEN"

  func oAZCODVEN_5_5.mHide()
    with this.Parent.oContained
      return (.w_AZGIRIVA='SI')
    endwith
  endfunc

  func oAZCODVEN_5_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZCODVEN_5_5.ecpDrop(oSource)
    this.Parent.oContained.link_5_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZCODVEN_5_5.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oAZCODVEN_5_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'GSCG0KCO.CONTI_VZM',this.parent.oContained
  endproc
  proc oAZCODVEN_5_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_AZCODVEN
     i_obj.ecpSave()
  endproc

  add object oDESCRI_5_6 as StdField with uid="AQLCYBSCDM",rtseq=66,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 81679414,;
   bGlobalFont=.t.,;
    Height=21, Width=345, Left=247, Top=48, InputMask=replicate('X',35)


  add object oAZNEWIVA_5_7 as StdCombo with uid="ZGDZPOBKQU",rtseq=67,rtrep=.f.,left=178,top=147,width=209,height=21;
    , ToolTipText = "Se automatico al momento della stampa liquidazione � possibile generare una registrazione contabile di compesnazione dei conti IVA";
    , HelpContextID = 181397177;
    , cFormVar="w_AZNEWIVA",RowSource=""+"Automatico,"+"Manuale", bObbl = .f. , nPag = 5;
  , bGlobalFont=.t.


  func oAZNEWIVA_5_7.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'N',;
    space(1))))
  endfunc
  func oAZNEWIVA_5_7.GetRadio()
    this.Parent.oContained.w_AZNEWIVA = this.RadioValue()
    return .t.
  endfunc

  func oAZNEWIVA_5_7.SetRadio()
    this.Parent.oContained.w_AZNEWIVA=trim(this.Parent.oContained.w_AZNEWIVA)
    this.value = ;
      iif(this.Parent.oContained.w_AZNEWIVA=='S',1,;
      iif(this.Parent.oContained.w_AZNEWIVA=='N',2,;
      0))
  endfunc

  add object oDESCRIZ_5_9 as StdField with uid="EEJOOZMHXV",rtseq=68,rtrep=.f.,;
    cFormVar = "w_DESCRIZ", cQueryName = "DESCRIZ",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 186756042,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=298, Top=76, InputMask=replicate('X',40)

  add object oAZCAUGIR_5_13 as StdField with uid="YRZVOHFJZE",rtseq=69,rtrep=.f.,;
    cFormVar = "w_AZCAUGIR", cQueryName = "AZCAUGIR",;
    bObbl = .f. , nPag = 5, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale giroconto IVA impostata di default",;
    HelpContextID = 217355944,;
   bGlobalFont=.t.,;
    Height=21, Width=70, Left=178, Top=181, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CAU_CONT", cZoomOnZoom="GSCG_ACC", oKey_1_1="CCCODICE", oKey_1_2="this.w_AZCAUGIR"

  func oAZCAUGIR_5_13.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  func oAZCAUGIR_5_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZCAUGIR_5_13.ecpDrop(oSource)
    this.Parent.oContained.link_5_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZCAUGIR_5_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAU_CONT','*','CCCODICE',cp_AbsName(this.parent,'oAZCAUGIR_5_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_ACC',"Causali contabili",'GSCG0KCA.CAU_CONT_VZM',this.parent.oContained
  endproc
  proc oAZCAUGIR_5_13.mZoomOnZoom
    local i_obj
    i_obj=GSCG_ACC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CCCODICE=this.parent.oContained.w_AZCAUGIR
     i_obj.ecpSave()
  endproc

  add object oAZIVCONT_5_14 as StdField with uid="RFYUTEBVOR",rtseq=70,rtrep=.f.,;
    cFormVar = "w_AZIVCONT", cQueryName = "AZIVCONT",;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente, di tipo non IVA oppure obsoleto",;
    ToolTipText = "Conto IVA impostato di default",;
    HelpContextID = 167823706,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=178, Top=208, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_AZIVCONT"

  func oAZIVCONT_5_14.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  func oAZIVCONT_5_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZIVCONT_5_14.ecpDrop(oSource)
    this.Parent.oContained.link_5_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZIVCONT_5_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oAZIVCONT_5_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'GSCG0KC1.CONTI_VZM',this.parent.oContained
  endproc
  proc oAZIVCONT_5_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_AZIVCONT
     i_obj.ecpSave()
  endproc

  add object oAZCRERIP_5_15 as StdField with uid="HNDPUZNHDC",rtseq=71,rtrep=.f.,;
    cFormVar = "w_AZCRERIP", cQueryName = "AZCRERIP",;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 48469674,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=178, Top=261, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_AZCRERIP"

  func oAZCRERIP_5_15.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  func oAZCRERIP_5_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZCRERIP_5_15.ecpDrop(oSource)
    this.Parent.oContained.link_5_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZCRERIP_5_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oAZCRERIP_5_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oAZCRERIP_5_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_AZCRERIP
     i_obj.ecpSave()
  endproc

  add object oAZCRESPE_5_16 as StdField with uid="MLYMETPJDQ",rtseq=72,rtrep=.f.,;
    cFormVar = "w_AZCRESPE", cQueryName = "AZCRESPE",;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 236742987,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=178, Top=286, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_AZCRESPE"

  func oAZCRESPE_5_16.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  func oAZCRESPE_5_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZCRESPE_5_16.ecpDrop(oSource)
    this.Parent.oContained.link_5_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZCRESPE_5_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oAZCRESPE_5_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oAZCRESPE_5_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_AZCRESPE
     i_obj.ecpSave()
  endproc

  add object oAZINTDOV_5_17 as StdField with uid="DCXHMYVIQY",rtseq=73,rtrep=.f.,;
    cFormVar = "w_AZINTDOV", cQueryName = "AZINTDOV",;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 575836,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=178, Top=311, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_AZINTDOV"

  func oAZINTDOV_5_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_AZTIPDEN='T')
    endwith
   endif
  endfunc

  func oAZINTDOV_5_17.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  func oAZINTDOV_5_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZINTDOV_5_17.ecpDrop(oSource)
    this.Parent.oContained.link_5_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZINTDOV_5_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oAZINTDOV_5_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'',this.parent.oContained
  endproc
  proc oAZINTDOV_5_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_AZINTDOV
     i_obj.ecpSave()
  endproc

  add object oAZCONIND_5_18 as StdField with uid="GIFBIGNCUG",rtseq=74,rtrep=.f.,;
    cFormVar = "w_AZCONIND", cQueryName = "AZCONIND",;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Contropartia IVA indetraibile",;
    HelpContextID = 78211402,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=178, Top=336, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_AZCONIND"

  func oAZCONIND_5_18.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  func oAZCONIND_5_18.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_18('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZCONIND_5_18.ecpDrop(oSource)
    this.Parent.oContained.link_5_18('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZCONIND_5_18.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oAZCONIND_5_18'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oDESGIRIVA_5_21 as StdField with uid="TQDJKQIRUG",rtseq=75,rtrep=.f.,;
    cFormVar = "w_DESGIRIVA", cQueryName = "DESGIRIVA",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 44935012,;
   bGlobalFont=.t.,;
    Height=21, Width=344, Left=247, Top=181, InputMask=replicate('X',35)

  func oDESGIRIVA_5_21.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  add object oIVCONTDES_5_22 as StdField with uid="OCBIBVIBKR",rtseq=76,rtrep=.f.,;
    cFormVar = "w_IVCONTDES", cQueryName = "IVCONTDES",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 5674245,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=298, Top=208, InputMask=replicate('X',40)

  func oIVCONTDES_5_22.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  add object oDESCRERIP_5_23 as StdField with uid="YKVQIBHSTA",rtseq=77,rtrep=.f.,;
    cFormVar = "w_DESCRERIP", cQueryName = "DESCRERIP",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 14571903,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=298, Top=261, InputMask=replicate('X',40)

  func oDESCRERIP_5_23.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  add object oDESCRESPE_5_24 as StdField with uid="ELSNSLEHIG",rtseq=78,rtrep=.f.,;
    cFormVar = "w_DESCRESPE", cQueryName = "DESCRESPE",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 253863722,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=298, Top=286, InputMask=replicate('X',40)

  func oDESCRESPE_5_24.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  add object oDESINTDOV_5_25 as StdField with uid="DWHVCNFWVP",rtseq=79,rtrep=.f.,;
    cFormVar = "w_DESINTDOV", cQueryName = "DESINTDOV",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 6006299,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=298, Top=311, InputMask=replicate('X',40)

  func oDESINTDOV_5_25.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  add object oDESPIA_5_37 as StdField with uid="DADUZESGNW",rtseq=84,rtrep=.f.,;
    cFormVar = "w_DESPIA", cQueryName = "DESPIA",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 207311926,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=298, Top=104, InputMask=replicate('X',40)

  func oDESPIA_5_37.mHide()
    with this.Parent.oContained
      return (.w_AZGIRIVA='SI')
    endwith
  endfunc

  add object oDESIND_5_40 as StdField with uid="NHIXZFUGMY",rtseq=87,rtrep=.f.,;
    cFormVar = "w_DESIND", cQueryName = "DESIND",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 262427702,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=298, Top=336, InputMask=replicate('X',15)

  func oDESIND_5_40.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  add object oAZCONPRO_5_45 as StdField with uid="BPAEIOIRJK",rtseq=117,rtrep=.f.,;
    cFormVar = "w_AZCONPRO", cQueryName = "AZCONPRO",;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente, di tipo non IVA oppure obsoleto",;
    ToolTipText = "Inserire il conto iva acquisti utilizzato nelle FA/NC reverse charge in presenza di registrazioni caratterizzate nella primanota contabile da conti iva acquisti e iva vendite",;
    HelpContextID = 195651925,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=178, Top=391, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_AZCONPRO"

  func oAZCONPRO_5_45.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_5_45('Part',this)
    endwith
    return bRes
  endfunc

  proc oAZCONPRO_5_45.ecpDrop(oSource)
    this.Parent.oContained.link_5_45('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oAZCONPRO_5_45.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oAZCONPRO_5_45'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Conti",'GSCG0KC1.CONTI_VZM',this.parent.oContained
  endproc
  proc oAZCONPRO_5_45.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_AZCONPRO
     i_obj.ecpSave()
  endproc

  add object oDESPRO_5_47 as StdField with uid="OZBYCJRTJK",rtseq=118,rtrep=.f.,;
    cFormVar = "w_DESPRO", cQueryName = "DESPRO",enabled=.f.,;
    bObbl = .f. , nPag = 5, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 183194678,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=298, Top=391, InputMask=replicate('X',15)

  add object oAZFLGIND_5_48 as StdCheck with uid="RQULNVMDWJ",rtseq=119,rtrep=.f.,left=178, top=421, caption="Utilizza conto iva indetraibile",;
    ToolTipText = "Se attivo viene effettuato il giroconto utilizzando non il conto iva acquisti della registrazione ma il conto iva indetraibile definito nel corrispondente campo.",;
    HelpContextID = 70687050,;
    cFormVar="w_AZFLGIND", bObbl = .f. , nPag = 5;
   , bGlobalFont=.t.


  func oAZFLGIND_5_48.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oAZFLGIND_5_48.GetRadio()
    this.Parent.oContained.w_AZFLGIND = this.RadioValue()
    return .t.
  endfunc

  func oAZFLGIND_5_48.SetRadio()
    this.Parent.oContained.w_AZFLGIND=trim(this.Parent.oContained.w_AZFLGIND)
    this.value = ;
      iif(this.Parent.oContained.w_AZFLGIND=='S',1,;
      0)
  endfunc

  func oAZFLGIND_5_48.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA<>'S')
    endwith
  endfunc

  add object oStr_5_2 as StdString with uid="FFYCQMOQXM",Visible=.t., Left=66, Top=16,;
    Alignment=1, Width=108, Height=18,;
    Caption="IVA ventilata:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_8 as StdString with uid="QNZVIDVSQS",Visible=.t., Left=98, Top=48,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  add object oStr_5_10 as StdString with uid="JAKSFVGFEW",Visible=.t., Left=104, Top=77,;
    Alignment=1, Width=70, Height=18,;
    Caption="Conto IVA:"  ;
  , bGlobalFont=.t.

  add object oStr_5_19 as StdString with uid="NWVVHYAVWK",Visible=.t., Left=98, Top=181,;
    Alignment=1, Width=76, Height=18,;
    Caption="Causale:"  ;
  , bGlobalFont=.t.

  func oStr_5_19.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  add object oStr_5_20 as StdString with uid="IJIGYACELA",Visible=.t., Left=104, Top=208,;
    Alignment=1, Width=70, Height=18,;
    Caption="Conto IVA:"  ;
  , bGlobalFont=.t.

  func oStr_5_20.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  add object oStr_5_26 as StdString with uid="ZSTSHPWJYS",Visible=.t., Left=22, Top=261,;
    Alignment=1, Width=152, Height=18,;
    Caption="Debito/credito riportato:"  ;
  , bGlobalFont=.t.

  func oStr_5_26.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  add object oStr_5_27 as StdString with uid="HYFFKIUOHV",Visible=.t., Left=79, Top=286,;
    Alignment=1, Width=95, Height=18,;
    Caption="Crediti speciali:"  ;
  , bGlobalFont=.t.

  func oStr_5_27.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  add object oStr_5_28 as StdString with uid="VAGALIZBCM",Visible=.t., Left=62, Top=311,;
    Alignment=1, Width=112, Height=18,;
    Caption="Interessi dovuti:"  ;
  , bGlobalFont=.t.

  func oStr_5_28.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  add object oStr_5_30 as StdString with uid="XZYFRINVJK",Visible=.t., Left=113, Top=236,;
    Alignment=0, Width=74, Height=18,;
    Caption="Contropartite"  ;
  , bGlobalFont=.t.

  func oStr_5_30.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  add object oStr_5_31 as StdString with uid="XRVVFLXIKN",Visible=.t., Left=50, Top=147,;
    Alignment=1, Width=124, Height=18,;
    Caption="Conti IVA:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_5_36 as StdString with uid="KNLIMYKJQU",Visible=.t., Left=41, Top=107,;
    Alignment=1, Width=135, Height=18,;
    Caption="Conto vendite corr.:"  ;
  , bGlobalFont=.t.

  func oStr_5_36.mHide()
    with this.Parent.oContained
      return (.w_AZGIRIVA='SI')
    endwith
  endfunc

  add object oStr_5_41 as StdString with uid="VFLALOCRSQ",Visible=.t., Left=47, Top=336,;
    Alignment=1, Width=127, Height=18,;
    Caption="IVA indetraibile:"  ;
  , bGlobalFont=.t.

  func oStr_5_41.mHide()
    with this.Parent.oContained
      return (.w_AZNEWIVA='N')
    endwith
  endfunc

  add object oStr_5_42 as StdString with uid="PSRVEOSLQY",Visible=.t., Left=9, Top=472,;
    Alignment=0, Width=553, Height=19,;
    Caption="Attenzione non aggiungere campi direttamente nella tabella azienda utilizzare figli PC"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_5_42.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oStr_5_44 as StdString with uid="WSEMZSFYKL",Visible=.t., Left=5, Top=366,;
    Alignment=0, Width=184, Height=18,;
    Caption="Giroconto iva indetraibile pro-rata"  ;
  , bGlobalFont=.t.

  add object oStr_5_46 as StdString with uid="HULOWJZIBG",Visible=.t., Left=-11, Top=392,;
    Alignment=1, Width=185, Height=18,;
    Caption="Conto iva acquisti RC:"  ;
  , bGlobalFont=.t.

  add object oBox_5_11 as StdBox with uid="YKMAZOYYNZ",left=2, top=45, width=633,height=2

  add object oBox_5_12 as StdBox with uid="EXRHZZMQTA",left=2, top=177, width=633,height=2

  add object oBox_5_29 as StdBox with uid="VRGIERSQKP",left=3, top=252, width=633,height=2

  add object oBox_5_43 as StdBox with uid="SLIKSNKVEU",left=3, top=384, width=633,height=2
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_api','AZIENDA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".AZCODAZI=AZIENDA.AZCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
