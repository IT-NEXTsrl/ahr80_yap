* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bap                                                        *
*              Aggior. FIRR prov.liquidate                                     *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_6]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-16                                                      *
* Last revis.: 2000-03-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bap",oParentObject,m.pTipo)
return(i_retval)

define class tgsve_bap as StdBatch
  * --- Local variables
  pTipo = 0
  w_NUMREG = 0
  w_SERIAL = space(10)
  w_ORIGINE = .f.
  w_SERRIF = space(10)
  w_AGENTE = space(5)
  w_ANNO = space(4)
  * --- WorkFile variables
  PRO_LIQU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Parametro utilizzato per poter distinguere l'aggiornamento delle provvigioni liquidate dalla cancellazione
    * --- Se 1 Cancellazione, 0 Aggiornamento
    * --- Aggiorno il serial delle provvigioni liquidate
    this.w_SERRIF = iif(this.pTipo=0,space(10),this.oParentObject.w_VFSERIAL)
    this.w_SERIAL = iif(this.pTipo=0,this.oParentObject.w_VFSERIAL,space(10))
    this.w_ORIGINE = .F.
    * --- Mi posiziono sul dettaglio
    SELECT (this.oParentObject.cTrsName)
    GO TOP
    SCAN FOR t_VFFLORMI=1
    if this.pTipo=0
      ah_Msg("Aggiornamento serial provvigioni liquidate")
    else
      ah_Msg("Cancellazione serial provvigioni liquidate")
    endif
    this.w_AGENTE = t_VFCODAGE
    if this.w_ORIGINE=.F.
      * --- A seconda del valore del parametro pTipo lancio due differenti query
      * --- Ognuna recupera i dati dalle provvigioni liquidate
      if this.pTipo=0
        vq_exec("query\GSVE_BAP.VQR",this,"TEMP")
      else
        vq_exec("query\GSVE0BAP.VQR",this,"TEMP")
      endif
      this.w_ORIGINE = .t.
    endif
    SELECT TEMP
    GO TOP
    SCAN FOR PLCODAGE=this.w_AGENTE
    this.w_ANNO = TEMP.PL__ANNO
    this.w_NUMREG = TEMP.PLNUMREG
    * --- Write into PRO_LIQU
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.PRO_LIQU_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRO_LIQU_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_LIQU_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"PLSERFIR ="+cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'PRO_LIQU','PLSERFIR');
          +i_ccchkf ;
      +" where ";
          +"PLNUMREG = "+cp_ToStrODBC(this.w_NUMREG);
          +" and PL__ANNO = "+cp_ToStrODBC(this.w_ANNO);
             )
    else
      update (i_cTable) set;
          PLSERFIR = this.w_SERIAL;
          &i_ccchkf. ;
       where;
          PLNUMREG = this.w_NUMREG;
          and PL__ANNO = this.w_ANNO;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ENDSCAN
    SELECT (this.oParentObject.cTrsName)
    ENDSCAN
    if used("TEMP")
      select Temp
      use
    endif
  endproc


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='PRO_LIQU'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
