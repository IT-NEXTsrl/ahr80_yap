* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_kli                                                        *
*              Stampa listino                                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_83]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-17                                                      *
* Last revis.: 2011-07-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_kli",oParentObject))

* --- Class definition
define class tgsma_kli as StdForm
  Top    = 1
  Left   = 16

  * --- Standard Properties
  Width  = 673
  Height = 419+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-07-14"
  HelpContextID=51741847
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=50

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  UNIMIS_IDX = 0
  CONTI_IDX = 0
  LISTINI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  TRADARTI_IDX = 0
  LINGUE_IDX = 0
  FAM_ARTI_IDX = 0
  MARCHI_IDX = 0
  CLA_RICA_IDX = 0
  MAGAZZIN_IDX = 0
  cPrg = "gsma_kli"
  cComment = "Stampa listino"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CODLIS1 = space(5)
  w_DESL1 = space(40)
  w_VALU1 = space(3)
  w_bcodi = space(20)
  w_desi = space(40)
  w_ecodi = space(20)
  w_desi1 = space(40)
  w_grumer1 = space(5)
  w_desg1 = space(30)
  w_grumer2 = space(5)
  w_desg2 = space(30)
  w_CATOMO = space(5)
  w_DESCAT = space(30)
  w_produ1 = space(15)
  w_DESF = space(40)
  w_produ2 = space(15)
  w_DESF2 = space(40)
  w_TIPCONTO = space(1)
  w_CODAZ1 = space(10)
  w_TIPO1 = space(1)
  w_DATOBSO = ctod('  /  /  ')
  w_ONUME = 0
  w_tipolingua = space(3)
  w_Desclingua = space(30)
  w_DATA2 = ctod('  /  /  ')
  o_DATA2 = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_LSDTOBSO = ctod('  /  /  ')
  w_TIPUMI = 0
  w_CODLIS1 = space(5)
  w_DESL1 = space(40)
  w_VALU1 = space(3)
  w_tipolingua = space(3)
  w_Desclingua = space(30)
  w_DATA2 = ctod('  /  /  ')
  w_TIPUMI = 0
  w_FAM1 = space(15)
  w_DESFA = space(40)
  w_FAM2 = space(15)
  w_DESFA2 = space(40)
  w_MAR1 = space(15)
  w_DESMA = space(40)
  w_MAR2 = space(15)
  w_DESMA2 = space(40)
  w_RICA1 = space(15)
  w_DESRICA1 = space(40)
  w_RICA2 = space(15)
  w_DESCRICA2 = space(40)
  w_GIALIST = space(1)
  o_GIALIST = space(1)
  w_CODMAG = space(5)
  w_DESMAG = space(30)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_kliPag1","gsma_kli",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Principale")
      .Pages(2).addobject("oPag","tgsma_kliPag2","gsma_kli",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCODLIS1_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[12]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='UNIMIS'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='LISTINI'
    this.cWorkTables[5]='GRUMERC'
    this.cWorkTables[6]='CATEGOMO'
    this.cWorkTables[7]='TRADARTI'
    this.cWorkTables[8]='LINGUE'
    this.cWorkTables[9]='FAM_ARTI'
    this.cWorkTables[10]='MARCHI'
    this.cWorkTables[11]='CLA_RICA'
    this.cWorkTables[12]='MAGAZZIN'
    return(this.OpenAllTables(12))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CODLIS1=space(5)
      .w_DESL1=space(40)
      .w_VALU1=space(3)
      .w_bcodi=space(20)
      .w_desi=space(40)
      .w_ecodi=space(20)
      .w_desi1=space(40)
      .w_grumer1=space(5)
      .w_desg1=space(30)
      .w_grumer2=space(5)
      .w_desg2=space(30)
      .w_CATOMO=space(5)
      .w_DESCAT=space(30)
      .w_produ1=space(15)
      .w_DESF=space(40)
      .w_produ2=space(15)
      .w_DESF2=space(40)
      .w_TIPCONTO=space(1)
      .w_CODAZ1=space(10)
      .w_TIPO1=space(1)
      .w_DATOBSO=ctod("  /  /  ")
      .w_ONUME=0
      .w_tipolingua=space(3)
      .w_Desclingua=space(30)
      .w_DATA2=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_LSDTOBSO=ctod("  /  /  ")
      .w_TIPUMI=0
      .w_CODLIS1=space(5)
      .w_DESL1=space(40)
      .w_VALU1=space(3)
      .w_tipolingua=space(3)
      .w_Desclingua=space(30)
      .w_DATA2=ctod("  /  /  ")
      .w_TIPUMI=0
      .w_FAM1=space(15)
      .w_DESFA=space(40)
      .w_FAM2=space(15)
      .w_DESFA2=space(40)
      .w_MAR1=space(15)
      .w_DESMA=space(40)
      .w_MAR2=space(15)
      .w_DESMA2=space(40)
      .w_RICA1=space(15)
      .w_DESRICA1=space(40)
      .w_RICA2=space(15)
      .w_DESCRICA2=space(40)
      .w_GIALIST=space(1)
      .w_CODMAG=space(5)
      .w_DESMAG=space(30)
        .w_CODLIS1 = space(5)
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_CODLIS1))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,4,.f.)
        if not(empty(.w_bcodi))
          .link_1_4('Full')
        endif
        .DoRTCalc(5,6,.f.)
        if not(empty(.w_ecodi))
          .link_1_6('Full')
        endif
        .DoRTCalc(7,8,.f.)
        if not(empty(.w_grumer1))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_grumer2))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,12,.f.)
        if not(empty(.w_CATOMO))
          .link_1_12('Full')
        endif
        .DoRTCalc(13,14,.f.)
        if not(empty(.w_produ1))
          .link_1_14('Full')
        endif
        .DoRTCalc(15,16,.f.)
        if not(empty(.w_produ2))
          .link_1_16('Full')
        endif
          .DoRTCalc(17,17,.f.)
        .w_TIPCONTO = 'F'
        .w_CODAZ1 = I_CODAZI
      .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
          .DoRTCalc(20,22,.f.)
        .w_tipolingua = G_CODLIN
        .DoRTCalc(23,23,.f.)
        if not(empty(.w_tipolingua))
          .link_1_40('Full')
        endif
          .DoRTCalc(24,24,.f.)
        .w_DATA2 = i_datsys
        .w_OBTEST = IIF(EMPTY(.w_DATA2),i_INIDAT,.w_DATA2)
          .DoRTCalc(27,27,.f.)
        .w_TIPUMI = 0
        .w_CODLIS1 = space(5)
        .DoRTCalc(29,29,.f.)
        if not(empty(.w_CODLIS1))
          .link_2_1('Full')
        endif
          .DoRTCalc(30,31,.f.)
        .w_tipolingua = G_CODLIN
        .DoRTCalc(32,32,.f.)
        if not(empty(.w_tipolingua))
          .link_2_8('Full')
        endif
          .DoRTCalc(33,33,.f.)
        .w_DATA2 = i_datsys
        .w_TIPUMI = 0
        .DoRTCalc(36,36,.f.)
        if not(empty(.w_FAM1))
          .link_2_15('Full')
        endif
        .DoRTCalc(37,38,.f.)
        if not(empty(.w_FAM2))
          .link_2_17('Full')
        endif
        .DoRTCalc(39,40,.f.)
        if not(empty(.w_MAR1))
          .link_2_23('Full')
        endif
        .DoRTCalc(41,42,.f.)
        if not(empty(.w_MAR2))
          .link_2_25('Full')
        endif
        .DoRTCalc(43,44,.f.)
        if not(empty(.w_RICA1))
          .link_2_29('Full')
        endif
        .DoRTCalc(45,46,.f.)
        if not(empty(.w_RICA2))
          .link_2_31('Full')
        endif
          .DoRTCalc(47,47,.f.)
        .w_GIALIST = 'N'
        .w_CODMAG = IIF(.w_GIALIST='S',.w_CODMAG,SPACE(5))
        .DoRTCalc(49,49,.f.)
        if not(empty(.w_CODMAG))
          .link_2_36('Full')
        endif
    endwith
    this.DoRTCalc(50,50,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
        .DoRTCalc(1,25,.t.)
        if .o_DATA2<>.w_DATA2
            .w_OBTEST = IIF(EMPTY(.w_DATA2),i_INIDAT,.w_DATA2)
        endif
        .DoRTCalc(27,28,.t.)
          .link_2_1('Full')
        .DoRTCalc(30,31,.t.)
          .link_2_8('Full')
        .DoRTCalc(33,48,.t.)
        if .o_GIALIST<>.w_GIALIST
            .w_CODMAG = IIF(.w_GIALIST='S',.w_CODMAG,SPACE(5))
          .link_2_36('Full')
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(50,50,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_22.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oCODMAG_2_36.enabled = this.oPgFrm.Page2.oPag.oCODMAG_2_36.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    oField= this.oPgFrm.Page2.oPag.oCODMAG_2_36
    cCShape='TTxOb'+m.oField.Name
    If PemStatus(oField.Parent, cCShape, 5) 
      oField.Parent.&cCShape..visible= oField.visible And oField.enabled And oField.CondObbl()
    EndIf
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_22.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODLIS1
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODLIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ALI',True,'LISTINI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LSCODLIS like "+cp_ToStrODBC(trim(this.w_CODLIS1)+"%");

          i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LSCODLIS","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LSCODLIS',trim(this.w_CODLIS1))
          select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LSCODLIS into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODLIS1)==trim(_Link_.LSCODLIS) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStrODBC(trim(this.w_CODLIS1)+"%");

            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" LSDESLIS like "+cp_ToStr(trim(this.w_CODLIS1)+"%");

            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODLIS1) and !this.bDontReportError
            deferred_cp_zoom('LISTINI','*','LSCODLIS',cp_AbsName(oSource.parent,'oCODLIS1_1_1'),i_cWhere,'GSAR_ALI',"Listini",'GSAR_SLO.LISTINI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',oSource.xKey(1))
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODLIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_CODLIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_CODLIS1)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODLIS1 = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESL1 = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALU1 = NVL(_Link_.LSVALLIS,space(3))
      this.w_TIPO1 = NVL(_Link_.LSIVALIS,space(1))
      this.w_LSDTOBSO = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODLIS1 = space(5)
      endif
      this.w_DESL1 = space(40)
      this.w_VALU1 = space(3)
      this.w_TIPO1 = space(1)
      this.w_LSDTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_LSDTOBSO) OR .w_LSDTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Listino gi� selezionato obsoleto o non valido")
        endif
        this.w_CODLIS1 = space(5)
        this.w_DESL1 = space(40)
        this.w_VALU1 = space(3)
        this.w_TIPO1 = space(1)
        this.w_LSDTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODLIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=bcodi
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_bcodi) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_bcodi)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_bcodi))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_bcodi)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_bcodi)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_bcodi)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_bcodi) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'obcodi_1_4'),i_cWhere,'GSMA_BZA',"Articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_bcodi)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_bcodi);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_bcodi)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_bcodi = NVL(_Link_.ARCODART,space(20))
      this.w_desi = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_bcodi = space(20)
      endif
      this.w_desi = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_ECODi)) OR  (upper(.w_BCODI)<=upper(.w_ECODI))) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
        endif
        this.w_bcodi = space(20)
        this.w_desi = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_bcodi Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ecodi
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ecodi) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ecodi)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ecodi))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ecodi)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_ecodi)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_ecodi)+"%");

            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_ecodi) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oecodi_1_6'),i_cWhere,'GSMA_BZA',"Articoli",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ecodi)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ecodi);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ecodi)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ecodi = NVL(_Link_.ARCODART,space(20))
      this.w_desi1 = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ecodi = space(20)
      endif
      this.w_desi1 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((upper(.w_ECODI)>=upper(.w_BCODI)) or (empty(.w_Bcodi))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endif
        this.w_ecodi = space(20)
        this.w_desi1 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ecodi Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=grumer1
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_grumer1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_grumer1)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_grumer1))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_grumer1)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_grumer1) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'ogrumer1_1_8'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_grumer1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_grumer1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_grumer1)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_grumer1 = NVL(_Link_.GMCODICE,space(5))
      this.W_desg1 = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_grumer1 = space(5)
      endif
      this.W_desg1 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(empty(.w_grumer2)) OR  (.w_grumer1<=.w_grumer2)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale superiore al codice finale oppure errato")
        endif
        this.w_grumer1 = space(5)
        this.W_desg1 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_grumer1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=grumer2
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_grumer2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_grumer2)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_grumer2))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_grumer2)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_grumer2) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'ogrumer2_1_10'),i_cWhere,'GSAR_AGM',"Gruppi merceologici",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_grumer2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_grumer2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_grumer2)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_grumer2 = NVL(_Link_.GMCODICE,space(5))
      this.W_desg2 = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_grumer2 = space(5)
      endif
      this.W_desg2 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_grumer2>=.w_grumer1) or (empty(.w_grumer1))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale superiore al codice finale oppure errato")
        endif
        this.w_grumer2 = space(5)
        this.W_desg2 = space(30)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_grumer2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATOMO
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATOMO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CATOMO)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CATOMO))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATOMO)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATOMO) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCATOMO_1_12'),i_cWhere,'GSAR_AOM',"Categorie omogenee",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATOMO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CATOMO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CATOMO)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATOMO = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCAT = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CATOMO = space(5)
      endif
      this.w_DESCAT = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATOMO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=produ1
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_produ1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_produ1)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TipConto);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TipConto;
                     ,'ANCODICE',trim(this.w_produ1))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_produ1)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_produ1)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TipConto);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_produ1)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TipConto);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_produ1) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oprodu1_1_14'),i_cWhere,'GSAR_AFR',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipConto<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TipConto);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_produ1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_produ1);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TipConto);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TipConto;
                       ,'ANCODICE',this.w_produ1)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_produ1 = NVL(_Link_.ANCODICE,space(15))
      this.W_DESF = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_produ1 = space(15)
      endif
      this.W_DESF = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_produ2)) OR  (.w_produ1<=.w_produ2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endif
        this.w_produ1 = space(15)
        this.W_DESF = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_produ1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=produ2
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_produ2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_produ2)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TipConto);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TipConto;
                     ,'ANCODICE',trim(this.w_produ2))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_produ2)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_produ2)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TipConto);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_produ2)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TipConto);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_produ2) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oprodu2_1_16'),i_cWhere,'GSAR_AFR',"Fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TipConto<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TipConto);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_produ2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_produ2);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TipConto);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TipConto;
                       ,'ANCODICE',this.w_produ2)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_produ2 = NVL(_Link_.ANCODICE,space(15))
      this.W_DESF2 = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_produ2 = space(15)
      endif
      this.W_DESF2 = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_produ1)) OR  (.w_produ1<=.w_produ2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endif
        this.w_produ2 = space(15)
        this.W_DESF2 = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_produ2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=tipolingua
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_tipolingua) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'LINGUE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" LUCODICE like "+cp_ToStrODBC(trim(this.w_tipolingua)+"%");

          i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by LUCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'LUCODICE',trim(this.w_tipolingua))
          select LUCODICE,LUDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by LUCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_tipolingua)==trim(_Link_.LUCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_tipolingua) and !this.bDontReportError
            deferred_cp_zoom('LINGUE','*','LUCODICE',cp_AbsName(oSource.parent,'otipolingua_1_40'),i_cWhere,'',"",'LINGUE.LINGUE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',oSource.xKey(1))
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_tipolingua)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_tipolingua);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_tipolingua)
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_tipolingua = NVL(_Link_.LUCODICE,space(3))
      this.w_Desclingua = NVL(_Link_.LUDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_tipolingua = space(3)
      endif
      this.w_Desclingua = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_tipolingua Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODLIS1
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LISTINI_IDX,3]
    i_lTable = "LISTINI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2], .t., this.LISTINI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODLIS1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODLIS1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where LSCODLIS="+cp_ToStrODBC(this.w_CODLIS1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LSCODLIS',this.w_CODLIS1)
            select LSCODLIS,LSDESLIS,LSVALLIS,LSIVALIS,LSDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODLIS1 = NVL(_Link_.LSCODLIS,space(5))
      this.w_DESL1 = NVL(_Link_.LSDESLIS,space(40))
      this.w_VALU1 = NVL(_Link_.LSVALLIS,space(3))
      this.w_TIPO1 = NVL(_Link_.LSIVALIS,space(1))
      this.w_LSDTOBSO = NVL(cp_ToDate(_Link_.LSDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODLIS1 = space(5)
      endif
      this.w_DESL1 = space(40)
      this.w_VALU1 = space(3)
      this.w_TIPO1 = space(1)
      this.w_LSDTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_LSDTOBSO) OR .w_LSDTOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Listino gi� selezionato obsoleto o non valido")
        endif
        this.w_CODLIS1 = space(5)
        this.w_DESL1 = space(40)
        this.w_VALU1 = space(3)
        this.w_TIPO1 = space(1)
        this.w_LSDTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LISTINI_IDX,2])+'\'+cp_ToStr(_Link_.LSCODLIS,1)
      cp_ShowWarn(i_cKey,this.LISTINI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODLIS1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=tipolingua
  func Link_2_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.LINGUE_IDX,3]
    i_lTable = "LINGUE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2], .t., this.LINGUE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_tipolingua) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_tipolingua)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select LUCODICE,LUDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where LUCODICE="+cp_ToStrODBC(this.w_tipolingua);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'LUCODICE',this.w_tipolingua)
            select LUCODICE,LUDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_tipolingua = NVL(_Link_.LUCODICE,space(3))
      this.w_Desclingua = NVL(_Link_.LUDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_tipolingua = space(3)
      endif
      this.w_Desclingua = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.LINGUE_IDX,2])+'\'+cp_ToStr(_Link_.LUCODICE,1)
      cp_ShowWarn(i_cKey,this.LINGUE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_tipolingua Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAM1
  func Link_2_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAM1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAM1)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAM1))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAM1)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" FADESCRI like "+cp_ToStrODBC(trim(this.w_FAM1)+"%");

            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FADESCRI like "+cp_ToStr(trim(this.w_FAM1)+"%");

            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FAM1) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAM1_2_15'),i_cWhere,'GSAR_AFA',"Famiglia",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAM1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAM1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAM1)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAM1 = NVL(_Link_.FACODICE,space(15))
      this.W_DESFA = NVL(_Link_.FADESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FAM1 = space(15)
      endif
      this.W_DESFA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_FAM2)) OR  (.w_FAM1<=.w_FAM2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endif
        this.w_FAM1 = space(15)
        this.W_DESFA = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAM1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=FAM2
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_FAM2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_FAM2)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_FAM2))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_FAM2)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" FADESCRI like "+cp_ToStrODBC(trim(this.w_FAM2)+"%");

            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FADESCRI like "+cp_ToStr(trim(this.w_FAM2)+"%");

            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_FAM2) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oFAM2_2_17'),i_cWhere,'GSAR_AFA',"Famiglia",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_FAM2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_FAM2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_FAM2)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_FAM2 = NVL(_Link_.FACODICE,space(15))
      this.W_DESFA2 = NVL(_Link_.FADESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_FAM2 = space(15)
      endif
      this.W_DESFA2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_FAM1)) OR  (.w_FAM1<=.w_FAM2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endif
        this.w_FAM2 = space(15)
        this.W_DESFA2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_FAM2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAR1
  func Link_2_23(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAR1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_MAR1)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_MAR1))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAR1)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStrODBC(trim(this.w_MAR1)+"%");

            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStr(trim(this.w_MAR1)+"%");

            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MAR1) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oMAR1_2_23'),i_cWhere,'GSAR_AMH',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAR1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_MAR1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_MAR1)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAR1 = NVL(_Link_.MACODICE,space(15))
      this.W_DESMA = NVL(_Link_.MADESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MAR1 = space(15)
      endif
      this.W_DESMA = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_MAR2)) OR  (.w_MAR1<=.w_MAR2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endif
        this.w_MAR1 = space(15)
        this.W_DESMA = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAR1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=MAR2
  func Link_2_25(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_MAR2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_MAR2)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_MAR2))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_MAR2)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStrODBC(trim(this.w_MAR2)+"%");

            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStr(trim(this.w_MAR2)+"%");

            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_MAR2) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oMAR2_2_25'),i_cWhere,'GSAR_AMH',"Marchi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_MAR2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_MAR2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_MAR2)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_MAR2 = NVL(_Link_.MACODICE,space(15))
      this.W_DESMA2 = NVL(_Link_.MADESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_MAR2 = space(15)
      endif
      this.W_DESMA2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_MAR1)) OR  (.w_MAR1<=.w_MAR2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endif
        this.w_MAR2 = space(15)
        this.W_DESMA2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_MAR2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RICA1
  func Link_2_29(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RICA_IDX,3]
    i_lTable = "CLA_RICA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2], .t., this.CLA_RICA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RICA1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACR',True,'CLA_RICA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CRCODICE like "+cp_ToStrODBC(trim(this.w_RICA1)+"%");

          i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CRCODICE',trim(this.w_RICA1))
          select CRCODICE,CRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RICA1)==trim(_Link_.CRCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CRDESCRI like "+cp_ToStrODBC(trim(this.w_RICA1)+"%");

            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CRDESCRI like "+cp_ToStr(trim(this.w_RICA1)+"%");

            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RICA1) and !this.bDontReportError
            deferred_cp_zoom('CLA_RICA','*','CRCODICE',cp_AbsName(oSource.parent,'oRICA1_2_29'),i_cWhere,'GSAR_ACR',"Clasdsi di ricarico",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODICE',oSource.xKey(1))
            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RICA1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CRCODICE="+cp_ToStrODBC(this.w_RICA1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODICE',this.w_RICA1)
            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RICA1 = NVL(_Link_.CRCODICE,space(15))
      this.W_DESRICA1 = NVL(_Link_.CRDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RICA1 = space(15)
      endif
      this.W_DESRICA1 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_RICA2)) OR  (.w_RICA1<=.w_RICA2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endif
        this.w_RICA1 = space(15)
        this.W_DESRICA1 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])+'\'+cp_ToStr(_Link_.CRCODICE,1)
      cp_ShowWarn(i_cKey,this.CLA_RICA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RICA1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=RICA2
  func Link_2_31(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_RICA_IDX,3]
    i_lTable = "CLA_RICA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2], .t., this.CLA_RICA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_RICA2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACR',True,'CLA_RICA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CRCODICE like "+cp_ToStrODBC(trim(this.w_RICA2)+"%");

          i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CRCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CRCODICE',trim(this.w_RICA2))
          select CRCODICE,CRDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CRCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_RICA2)==trim(_Link_.CRCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CRDESCRI like "+cp_ToStrODBC(trim(this.w_RICA2)+"%");

            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CRDESCRI like "+cp_ToStr(trim(this.w_RICA2)+"%");

            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_RICA2) and !this.bDontReportError
            deferred_cp_zoom('CLA_RICA','*','CRCODICE',cp_AbsName(oSource.parent,'oRICA2_2_31'),i_cWhere,'GSAR_ACR',"Classi di ricarico",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CRCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODICE',oSource.xKey(1))
            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_RICA2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CRCODICE,CRDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CRCODICE="+cp_ToStrODBC(this.w_RICA2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CRCODICE',this.w_RICA2)
            select CRCODICE,CRDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_RICA2 = NVL(_Link_.CRCODICE,space(15))
      this.W_DESCRICA2 = NVL(_Link_.CRDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_RICA2 = space(15)
      endif
      this.W_DESCRICA2 = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_RICA1)) OR  (.w_RICA1<=.w_RICA2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
        endif
        this.w_RICA2 = space(15)
        this.W_DESCRICA2 = space(40)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_RICA_IDX,2])+'\'+cp_ToStr(_Link_.CRCODICE,1)
      cp_ShowWarn(i_cKey,this.CLA_RICA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_RICA2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAG
  func Link_2_36(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_CODMAG)+"%");

            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_2_36'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCODLIS1_1_1.value==this.w_CODLIS1)
      this.oPgFrm.Page1.oPag.oCODLIS1_1_1.value=this.w_CODLIS1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESL1_1_2.value==this.w_DESL1)
      this.oPgFrm.Page1.oPag.oDESL1_1_2.value=this.w_DESL1
    endif
    if not(this.oPgFrm.Page1.oPag.oVALU1_1_3.value==this.w_VALU1)
      this.oPgFrm.Page1.oPag.oVALU1_1_3.value=this.w_VALU1
    endif
    if not(this.oPgFrm.Page1.oPag.obcodi_1_4.value==this.w_bcodi)
      this.oPgFrm.Page1.oPag.obcodi_1_4.value=this.w_bcodi
    endif
    if not(this.oPgFrm.Page1.oPag.odesi_1_5.value==this.w_desi)
      this.oPgFrm.Page1.oPag.odesi_1_5.value=this.w_desi
    endif
    if not(this.oPgFrm.Page1.oPag.oecodi_1_6.value==this.w_ecodi)
      this.oPgFrm.Page1.oPag.oecodi_1_6.value=this.w_ecodi
    endif
    if not(this.oPgFrm.Page1.oPag.odesi1_1_7.value==this.w_desi1)
      this.oPgFrm.Page1.oPag.odesi1_1_7.value=this.w_desi1
    endif
    if not(this.oPgFrm.Page1.oPag.ogrumer1_1_8.value==this.w_grumer1)
      this.oPgFrm.Page1.oPag.ogrumer1_1_8.value=this.w_grumer1
    endif
    if not(this.oPgFrm.Page1.oPag.odesg1_1_9.value==this.w_desg1)
      this.oPgFrm.Page1.oPag.odesg1_1_9.value=this.w_desg1
    endif
    if not(this.oPgFrm.Page1.oPag.ogrumer2_1_10.value==this.w_grumer2)
      this.oPgFrm.Page1.oPag.ogrumer2_1_10.value=this.w_grumer2
    endif
    if not(this.oPgFrm.Page1.oPag.odesg2_1_11.value==this.w_desg2)
      this.oPgFrm.Page1.oPag.odesg2_1_11.value=this.w_desg2
    endif
    if not(this.oPgFrm.Page1.oPag.oCATOMO_1_12.value==this.w_CATOMO)
      this.oPgFrm.Page1.oPag.oCATOMO_1_12.value=this.w_CATOMO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_13.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_13.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oprodu1_1_14.value==this.w_produ1)
      this.oPgFrm.Page1.oPag.oprodu1_1_14.value=this.w_produ1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESF_1_15.value==this.w_DESF)
      this.oPgFrm.Page1.oPag.oDESF_1_15.value=this.w_DESF
    endif
    if not(this.oPgFrm.Page1.oPag.oprodu2_1_16.value==this.w_produ2)
      this.oPgFrm.Page1.oPag.oprodu2_1_16.value=this.w_produ2
    endif
    if not(this.oPgFrm.Page1.oPag.oDESF2_1_17.value==this.w_DESF2)
      this.oPgFrm.Page1.oPag.oDESF2_1_17.value=this.w_DESF2
    endif
    if not(this.oPgFrm.Page1.oPag.otipolingua_1_40.value==this.w_tipolingua)
      this.oPgFrm.Page1.oPag.otipolingua_1_40.value=this.w_tipolingua
    endif
    if not(this.oPgFrm.Page1.oPag.oDesclingua_1_41.value==this.w_Desclingua)
      this.oPgFrm.Page1.oPag.oDesclingua_1_41.value=this.w_Desclingua
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA2_1_42.value==this.w_DATA2)
      this.oPgFrm.Page1.oPag.oDATA2_1_42.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPUMI_1_46.RadioValue()==this.w_TIPUMI)
      this.oPgFrm.Page1.oPag.oTIPUMI_1_46.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODLIS1_2_1.value==this.w_CODLIS1)
      this.oPgFrm.Page2.oPag.oCODLIS1_2_1.value=this.w_CODLIS1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESL1_2_2.value==this.w_DESL1)
      this.oPgFrm.Page2.oPag.oDESL1_2_2.value=this.w_DESL1
    endif
    if not(this.oPgFrm.Page2.oPag.oVALU1_2_3.value==this.w_VALU1)
      this.oPgFrm.Page2.oPag.oVALU1_2_3.value=this.w_VALU1
    endif
    if not(this.oPgFrm.Page2.oPag.otipolingua_2_8.value==this.w_tipolingua)
      this.oPgFrm.Page2.oPag.otipolingua_2_8.value=this.w_tipolingua
    endif
    if not(this.oPgFrm.Page2.oPag.oDesclingua_2_9.value==this.w_Desclingua)
      this.oPgFrm.Page2.oPag.oDesclingua_2_9.value=this.w_Desclingua
    endif
    if not(this.oPgFrm.Page2.oPag.oDATA2_2_10.value==this.w_DATA2)
      this.oPgFrm.Page2.oPag.oDATA2_2_10.value=this.w_DATA2
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPUMI_2_12.RadioValue()==this.w_TIPUMI)
      this.oPgFrm.Page2.oPag.oTIPUMI_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oFAM1_2_15.value==this.w_FAM1)
      this.oPgFrm.Page2.oPag.oFAM1_2_15.value=this.w_FAM1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFA_2_16.value==this.w_DESFA)
      this.oPgFrm.Page2.oPag.oDESFA_2_16.value=this.w_DESFA
    endif
    if not(this.oPgFrm.Page2.oPag.oFAM2_2_17.value==this.w_FAM2)
      this.oPgFrm.Page2.oPag.oFAM2_2_17.value=this.w_FAM2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFA2_2_18.value==this.w_DESFA2)
      this.oPgFrm.Page2.oPag.oDESFA2_2_18.value=this.w_DESFA2
    endif
    if not(this.oPgFrm.Page2.oPag.oMAR1_2_23.value==this.w_MAR1)
      this.oPgFrm.Page2.oPag.oMAR1_2_23.value=this.w_MAR1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMA_2_24.value==this.w_DESMA)
      this.oPgFrm.Page2.oPag.oDESMA_2_24.value=this.w_DESMA
    endif
    if not(this.oPgFrm.Page2.oPag.oMAR2_2_25.value==this.w_MAR2)
      this.oPgFrm.Page2.oPag.oMAR2_2_25.value=this.w_MAR2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMA2_2_26.value==this.w_DESMA2)
      this.oPgFrm.Page2.oPag.oDESMA2_2_26.value=this.w_DESMA2
    endif
    if not(this.oPgFrm.Page2.oPag.oRICA1_2_29.value==this.w_RICA1)
      this.oPgFrm.Page2.oPag.oRICA1_2_29.value=this.w_RICA1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESRICA1_2_30.value==this.w_DESRICA1)
      this.oPgFrm.Page2.oPag.oDESRICA1_2_30.value=this.w_DESRICA1
    endif
    if not(this.oPgFrm.Page2.oPag.oRICA2_2_31.value==this.w_RICA2)
      this.oPgFrm.Page2.oPag.oRICA2_2_31.value=this.w_RICA2
    endif
    if not(this.oPgFrm.Page2.oPag.oDESCRICA2_2_32.value==this.w_DESCRICA2)
      this.oPgFrm.Page2.oPag.oDESCRICA2_2_32.value=this.w_DESCRICA2
    endif
    if not(this.oPgFrm.Page2.oPag.oGIALIST_2_35.RadioValue()==this.w_GIALIST)
      this.oPgFrm.Page2.oPag.oGIALIST_2_35.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODMAG_2_36.value==this.w_CODMAG)
      this.oPgFrm.Page2.oPag.oCODMAG_2_36.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page2.oPag.oDESMAG_2_37.value==this.w_DESMAG)
      this.oPgFrm.Page2.oPag.oDESMAG_2_37.value=this.w_DESMAG
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CODLIS1)) or not(EMPTY(.w_LSDTOBSO) OR .w_LSDTOBSO>.w_OBTEST))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODLIS1_1_1.SetFocus()
            i_bnoObbl = !empty(.w_CODLIS1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Listino gi� selezionato obsoleto o non valido")
          case   not(((empty(.w_ECODi)) OR  (upper(.w_BCODI)<=upper(.w_ECODI))) AND (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_bcodi))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.obcodi_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � pi� grande del codice finale oppure obsoleto")
          case   not(((upper(.w_ECODI)>=upper(.w_BCODI)) or (empty(.w_Bcodi))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_ecodi))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oecodi_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
          case   not((empty(.w_grumer2)) OR  (.w_grumer1<=.w_grumer2))  and not(empty(.w_grumer1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.ogrumer1_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale superiore al codice finale oppure errato")
          case   not((.w_grumer2>=.w_grumer1) or (empty(.w_grumer1)))  and not(empty(.w_grumer2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.ogrumer2_1_10.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale superiore al codice finale oppure errato")
          case   not(((empty(.w_produ2)) OR  (.w_produ1<=.w_produ2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_produ1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oprodu1_1_14.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
          case   not(((empty(.w_produ1)) OR  (.w_produ1<=.w_produ2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_produ2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oprodu2_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
          case   (empty(.w_tipolingua))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.otipolingua_1_40.SetFocus()
            i_bnoObbl = !empty(.w_tipolingua)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATA2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATA2_1_42.SetFocus()
            i_bnoObbl = !empty(.w_DATA2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CODLIS1)) or not(EMPTY(.w_LSDTOBSO) OR .w_LSDTOBSO>.w_OBTEST))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODLIS1_2_1.SetFocus()
            i_bnoObbl = !empty(.w_CODLIS1)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Listino gi� selezionato obsoleto o non valido")
          case   (empty(.w_tipolingua))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.otipolingua_2_8.SetFocus()
            i_bnoObbl = !empty(.w_tipolingua)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_DATA2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oDATA2_2_10.SetFocus()
            i_bnoObbl = !empty(.w_DATA2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((empty(.w_FAM2)) OR  (.w_FAM1<=.w_FAM2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_FAM1))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFAM1_2_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
          case   not(((empty(.w_FAM1)) OR  (.w_FAM1<=.w_FAM2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_FAM2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oFAM2_2_17.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
          case   not(((empty(.w_MAR2)) OR  (.w_MAR1<=.w_MAR2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_MAR1))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMAR1_2_23.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
          case   not(((empty(.w_MAR1)) OR  (.w_MAR1<=.w_MAR2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_MAR2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oMAR2_2_25.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
          case   not(((empty(.w_RICA2)) OR  (.w_RICA1<=.w_RICA2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_RICA1))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRICA1_2_29.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
          case   not(((empty(.w_RICA1)) OR  (.w_RICA1<=.w_RICA2)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_RICA2))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oRICA2_2_31.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il codice iniziale � maggiore del codice finale oppure obsoleto")
          case   (empty(.w_CODMAG) and (.w_GIALIST='S'))  and (.w_GIALIST='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oCODMAG_2_36.SetFocus()
            i_bnoObbl = !empty(.w_CODMAG) or !(.w_GIALIST='S')
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATA2 = this.w_DATA2
    this.o_GIALIST = this.w_GIALIST
    return

enddefine

* --- Define pages as container
define class tgsma_kliPag1 as StdContainer
  Width  = 669
  height = 419
  stdWidth  = 669
  stdheight = 419
  resizeXpos=381
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODLIS1_1_1 as StdField with uid="VRGQGRHOTK",rtseq=1,rtrep=.f.,;
    cFormVar = "w_CODLIS1", cQueryName = "CODLIS1",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Listino gi� selezionato obsoleto o non valido",;
    ToolTipText = "Seleziona il listino",;
    HelpContextID = 183900198,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=106, Top=46, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_CODLIS1"

  func oCODLIS1_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODLIS1_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODLIS1_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LISTINI','*','LSCODLIS',cp_AbsName(this.parent,'oCODLIS1_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ALI',"Listini",'GSAR_SLO.LISTINI_VZM',this.parent.oContained
  endproc
  proc oCODLIS1_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ALI()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_LSCODLIS=this.parent.oContained.w_CODLIS1
     i_obj.ecpSave()
  endproc

  add object oDESL1_1_2 as StdField with uid="YZKGPUAFTX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DESL1", cQueryName = "DESL1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 108461622,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=167, Top=46, InputMask=replicate('X',40)

  add object oVALU1_1_3 as StdField with uid="CFAXHEHVIO",rtseq=3,rtrep=.f.,;
    cFormVar = "w_VALU1", cQueryName = "VALU1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 109022038,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=461, Top=46, InputMask=replicate('X',3)

  add object obcodi_1_4 as StdField with uid="XZDXJWBIFX",rtseq=4,rtrep=.f.,;
    cFormVar = "w_bcodi", cQueryName = "bcodi",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � pi� grande del codice finale oppure obsoleto",;
    ToolTipText = "Codice primo articolo da stampare",;
    HelpContextID = 168877590,;
   bGlobalFont=.t.,;
    Height=21, Width=198, Left=106, Top=148, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_bcodi"

  func obcodi_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc obcodi_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc obcodi_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'obcodi_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'',this.parent.oContained
  endproc
  proc obcodi_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_bcodi
     i_obj.ecpSave()
  endproc

  add object odesi_1_5 as StdField with uid="TNBAJQEERN",rtseq=5,rtrep=.f.,;
    cFormVar = "w_desi", cQueryName = "desi",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 59121718,;
   bGlobalFont=.t.,;
    Height=21, Width=327, Left=308, Top=148, InputMask=replicate('X',40)

  add object oecodi_1_6 as StdField with uid="GUTIXXHEEG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ecodi", cQueryName = "ecodi",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure obsoleto",;
    ToolTipText = "Codice ultimo articolo da stampare",;
    HelpContextID = 168877638,;
   bGlobalFont=.t.,;
    Height=21, Width=198, Left=106, Top=173, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_ecodi"

  func oecodi_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oecodi_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oecodi_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oecodi_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Articoli",'',this.parent.oContained
  endproc
  proc oecodi_1_6.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_ecodi
     i_obj.ecpSave()
  endproc

  add object odesi1_1_7 as StdField with uid="XHKZYXJYCK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_desi1", cQueryName = "desi1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 110501942,;
   bGlobalFont=.t.,;
    Height=21, Width=327, Left=308, Top=173, InputMask=replicate('X',40)

  add object ogrumer1_1_8 as StdField with uid="HEGTRGWLVX",rtseq=8,rtrep=.f.,;
    cFormVar = "w_grumer1", cQueryName = "grumer1",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale superiore al codice finale oppure errato",;
    ToolTipText = "Filtro sul gruppo merceologico",;
    HelpContextID = 69579418,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=106, Top=203, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_grumer1"

  func ogrumer1_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc ogrumer1_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ogrumer1_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'ogrumer1_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc ogrumer1_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_grumer1
     i_obj.ecpSave()
  endproc

  add object odesg1_1_9 as StdField with uid="ZBFUFQLFPT",rtseq=9,rtrep=.f.,;
    cFormVar = "w_desg1", cQueryName = "desg1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 110370870,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=166, Top=203, InputMask=replicate('X',30)

  add object ogrumer2_1_10 as StdField with uid="CWWNGHWNRL",rtseq=10,rtrep=.f.,;
    cFormVar = "w_grumer2", cQueryName = "grumer2",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale superiore al codice finale oppure errato",;
    ToolTipText = "Filtro sul gruppo merceologico",;
    HelpContextID = 69579418,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=106, Top=228, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_grumer2"

  func ogrumer2_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc ogrumer2_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc ogrumer2_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'ogrumer2_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"Gruppi merceologici",'',this.parent.oContained
  endproc
  proc ogrumer2_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_grumer2
     i_obj.ecpSave()
  endproc

  add object odesg2_1_11 as StdField with uid="XXNJPSEPTQ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_desg2", cQueryName = "desg2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 111419446,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=166, Top=228, InputMask=replicate('X',30)

  add object oCATOMO_1_12 as StdField with uid="HNRRIJGNHH",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CATOMO", cQueryName = "CATOMO",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Stampa solo gli articoli con questa categoria omogenea",;
    HelpContextID = 121244198,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=106, Top=257, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CATOMO"

  func oCATOMO_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATOMO_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATOMO_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCATOMO_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"Categorie omogenee",'',this.parent.oContained
  endproc
  proc oCATOMO_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CATOMO
     i_obj.ecpSave()
  endproc

  add object oDESCAT_1_13 as StdField with uid="GIHTPCSAOV",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 191757878,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=166, Top=257, InputMask=replicate('X',30)

  add object oprodu1_1_14 as StdField with uid="SWFWWDFOSE",rtseq=14,rtrep=.f.,;
    cFormVar = "w_produ1", cQueryName = "produ1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure obsoleto",;
    ToolTipText = "Filtro sui fornitori",;
    HelpContextID = 198241782,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=106, Top=289, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TipConto", oKey_2_1="ANCODICE", oKey_2_2="this.w_produ1"

  func oprodu1_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oprodu1_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oprodu1_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TipConto)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TipConto)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oprodu1_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Fornitori",'',this.parent.oContained
  endproc
  proc oprodu1_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TipConto
     i_obj.w_ANCODICE=this.parent.oContained.w_produ1
     i_obj.ecpSave()
  endproc

  add object oDESF_1_15 as StdField with uid="IYEVIQYDIL",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DESF", cQueryName = "DESF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 56688182,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=251, Top=289, InputMask=replicate('X',40)

  add object oprodu2_1_16 as StdField with uid="MCIOWHVXSU",rtseq=16,rtrep=.f.,;
    cFormVar = "w_produ2", cQueryName = "produ2",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure obsoleto",;
    ToolTipText = "Filtro sui fornitori",;
    HelpContextID = 215018998,;
   bGlobalFont=.t.,;
    Height=21, Width=142, Left=106, Top=313, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TipConto", oKey_2_1="ANCODICE", oKey_2_2="this.w_produ2"

  func oprodu2_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oprodu2_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oprodu2_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TipConto)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TipConto)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oprodu2_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Fornitori",'',this.parent.oContained
  endproc
  proc oprodu2_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TipConto
     i_obj.w_ANCODICE=this.parent.oContained.w_produ2
     i_obj.ecpSave()
  endproc

  add object oDESF2_1_17 as StdField with uid="LMPTMMETGK",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESF2", cQueryName = "DESF2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 109116982,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=251, Top=313, InputMask=replicate('X',40)


  add object oObj_1_22 as cp_outputCombo with uid="ZLERXIVPWT",left=106, top=351, width=385,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 229739494


  add object oBtn_1_23 as StdButton with uid="WKCHZJVTVN",left=560, top=371, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 193531430;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_codlis1))
      endwith
    endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="VRNTVMNYGF",left=613, top=371, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 59059270;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object otipolingua_1_40 as StdField with uid="VQCGRTTKAY",rtseq=23,rtrep=.f.,;
    cFormVar = "w_tipolingua", cQueryName = "tipolingua",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Stampa descrizioni articoli nella lingua selezionata (se esistente)",;
    HelpContextID = 55336429,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=106, Top=75, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="LINGUE", oKey_1_1="LUCODICE", oKey_1_2="this.w_tipolingua"

  func otipolingua_1_40.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_40('Part',this)
    endwith
    return bRes
  endfunc

  proc otipolingua_1_40.ecpDrop(oSource)
    this.Parent.oContained.link_1_40('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc otipolingua_1_40.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'LINGUE','*','LUCODICE',cp_AbsName(this.parent,'otipolingua_1_40'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'LINGUE.LINGUE_VZM',this.parent.oContained
  endproc

  add object oDesclingua_1_41 as StdField with uid="UEHQVPMQTC",rtseq=24,rtrep=.f.,;
    cFormVar = "w_Desclingua", cQueryName = "Desclingua",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 54560493,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=167, Top=75, InputMask=replicate('X',30)

  add object oDATA2_1_42 as StdField with uid="WKJPZGJBNP",rtseq=25,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data validit� di riferimento",;
    HelpContextID = 108792374,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=561, Top=46


  add object oTIPUMI_1_46 as StdCombo with uid="ZLVISPSMQV",value=3,rtseq=28,rtrep=.f.,left=539,top=71,width=99,height=21;
    , ToolTipText = "Cerca il listino nell'unit� di misura indicata (prima, seconda o tutte)";
    , HelpContextID = 20960054;
    , cFormVar="w_TIPUMI",RowSource=""+"Nella 1a U.m.,"+"Nella 2a U.m.,"+"In tutte le U.m.", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPUMI_1_46.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,0,;
    0))))
  endfunc
  func oTIPUMI_1_46.GetRadio()
    this.Parent.oContained.w_TIPUMI = this.RadioValue()
    return .t.
  endfunc

  func oTIPUMI_1_46.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TIPUMI==1,1,;
      iif(this.Parent.oContained.w_TIPUMI==2,2,;
      iif(this.Parent.oContained.w_TIPUMI==0,3,;
      0)))
  endfunc

  add object oStr_1_20 as StdString with uid="FMNBYBSJKX",Visible=.t., Left=16, Top=49,;
    Alignment=1, Width=87, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="MIVYDSUKOY",Visible=.t., Left=30, Top=9,;
    Alignment=0, Width=415, Height=18,;
    Caption="Selezione listino"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="FXPYFUJAPC",Visible=.t., Left=18, Top=258,;
    Alignment=1, Width=86, Height=18,;
    Caption="Categoria:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="GRWTWHHZWS",Visible=.t., Left=18, Top=203,;
    Alignment=1, Width=86, Height=15,;
    Caption="Da gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="GZEPDBXNDD",Visible=.t., Left=9, Top=228,;
    Alignment=1, Width=95, Height=15,;
    Caption="A gr. merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="MLQERLPBEA",Visible=.t., Left=18, Top=289,;
    Alignment=1, Width=86, Height=15,;
    Caption="Da fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="EBOGQPAKHB",Visible=.t., Left=18, Top=313,;
    Alignment=1, Width=86, Height=15,;
    Caption="A fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="YMDOPMDMDV",Visible=.t., Left=18, Top=148,;
    Alignment=1, Width=86, Height=15,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="ATZJURTAPF",Visible=.t., Left=18, Top=173,;
    Alignment=1, Width=86, Height=15,;
    Caption="A articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="OLPBBPGUKF",Visible=.t., Left=29, Top=127,;
    Alignment=0, Width=626, Height=15,;
    Caption="Selezione articoli"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="FJAAVJVYHC",Visible=.t., Left=18, Top=351,;
    Alignment=1, Width=86, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="JRUHHAUNUP",Visible=.t., Left=16, Top=75,;
    Alignment=1, Width=87, Height=15,;
    Caption="Lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="VXYNNIQDMV",Visible=.t., Left=498, Top=48,;
    Alignment=1, Width=60, Height=15,;
    Caption="Alla data:"  ;
  , bGlobalFont=.t.

  add object oBox_1_32 as StdBox with uid="QEWZAIZMGY",left=27, top=26, width=420,height=1

  add object oBox_1_34 as StdBox with uid="HERMSVEUJU",left=27, top=143, width=630,height=1
enddefine
define class tgsma_kliPag2 as StdContainer
  Width  = 669
  height = 419
  stdWidth  = 669
  stdheight = 419
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCODLIS1_2_1 as StdField with uid="WQQBBYGDYX",rtseq=29,rtrep=.f.,;
    cFormVar = "w_CODLIS1", cQueryName = "CODLIS1",enabled=.f.,;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Listino gi� selezionato obsoleto o non valido",;
    ToolTipText = "Seleziona il listino",;
    HelpContextID = 183900198,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=106, Top=46, InputMask=replicate('X',5), cLinkFile="LISTINI", cZoomOnZoom="GSAR_ALI", oKey_1_1="LSCODLIS", oKey_1_2="this.w_CODLIS1"

  func oCODLIS1_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESL1_2_2 as StdField with uid="PIXVTQWGLG",rtseq=30,rtrep=.f.,;
    cFormVar = "w_DESL1", cQueryName = "DESL1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 108461622,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=167, Top=46, InputMask=replicate('X',40)

  add object oVALU1_2_3 as StdField with uid="CHAEMYFGGJ",rtseq=31,rtrep=.f.,;
    cFormVar = "w_VALU1", cQueryName = "VALU1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    HelpContextID = 109022038,;
   bGlobalFont=.t.,;
    Height=21, Width=37, Left=461, Top=46, InputMask=replicate('X',3)

  add object otipolingua_2_8 as StdField with uid="RZXSJLAEDG",rtseq=32,rtrep=.f.,;
    cFormVar = "w_tipolingua", cQueryName = "tipolingua",enabled=.f.,;
    bObbl = .t. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Stampa descrizioni articoli nella lingua selezionata (se esistente)",;
    HelpContextID = 55336429,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=106, Top=75, InputMask=replicate('X',3), cLinkFile="LINGUE", oKey_1_1="LUCODICE", oKey_1_2="this.w_tipolingua"

  func otipolingua_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDesclingua_2_9 as StdField with uid="ISPIDDSFNR",rtseq=33,rtrep=.f.,;
    cFormVar = "w_Desclingua", cQueryName = "Desclingua",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 54560493,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=167, Top=75, InputMask=replicate('X',30)

  add object oDATA2_2_10 as StdField with uid="OMNODHAKKN",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DATA2", cQueryName = "DATA2",enabled=.f.,;
    bObbl = .t. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data validit� di riferimento",;
    HelpContextID = 108792374,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=561, Top=46


  add object oTIPUMI_2_12 as StdCombo with uid="BSVMPKNTTF",value=3,rtseq=35,rtrep=.f.,left=539,top=71,width=99,height=21, enabled=.f.;
    , ToolTipText = "Cerca il listino nell'unit� di misura indicata (prima, seconda o tutte)";
    , HelpContextID = 20960054;
    , cFormVar="w_TIPUMI",RowSource=""+"Nella 1a U.m.,"+"Nella 2a U.m.,"+"In tutte le U.m.", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPUMI_2_12.RadioValue()
    return(iif(this.value =1,1,;
    iif(this.value =2,2,;
    iif(this.value =3,0,;
    0))))
  endfunc
  func oTIPUMI_2_12.GetRadio()
    this.Parent.oContained.w_TIPUMI = this.RadioValue()
    return .t.
  endfunc

  func oTIPUMI_2_12.SetRadio()
    
    this.value = ;
      iif(this.Parent.oContained.w_TIPUMI==1,1,;
      iif(this.Parent.oContained.w_TIPUMI==2,2,;
      iif(this.Parent.oContained.w_TIPUMI==0,3,;
      0)))
  endfunc

  add object oFAM1_2_15 as StdField with uid="CGIESAYSFQ",rtseq=36,rtrep=.f.,;
    cFormVar = "w_FAM1", cQueryName = "FAM1",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure obsoleto",;
    ToolTipText = "Filtro sulla famiglia",;
    HelpContextID = 55286358,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=140, Top=148, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_FAM1"

  func oFAM1_2_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAM1_2_15.ecpDrop(oSource)
    this.Parent.oContained.link_2_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAM1_2_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAM1_2_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Famiglia",'',this.parent.oContained
  endproc
  proc oFAM1_2_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_FAM1
     i_obj.ecpSave()
  endproc

  add object oDESFA_2_16 as StdField with uid="ITPICAQOAZ",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DESFA", cQueryName = "DESFA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 124845622,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=199, Top=148, InputMask=replicate('X',40)

  add object oFAM2_2_17 as StdField with uid="CLBAGUJPQU",rtseq=38,rtrep=.f.,;
    cFormVar = "w_FAM2", cQueryName = "FAM2",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure obsoleto",;
    ToolTipText = "Filtro sulla famiglia",;
    HelpContextID = 55351894,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=140, Top=172, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_FAM2"

  func oFAM2_2_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oFAM2_2_17.ecpDrop(oSource)
    this.Parent.oContained.link_2_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oFAM2_2_17.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oFAM2_2_17'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"Famiglia",'',this.parent.oContained
  endproc
  proc oFAM2_2_17.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_FAM2
     i_obj.ecpSave()
  endproc

  add object oDESFA2_2_18 as StdField with uid="PXVHFQYKXX",rtseq=39,rtrep=.f.,;
    cFormVar = "w_DESFA2", cQueryName = "DESFA2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 158400054,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=199, Top=172, InputMask=replicate('X',40)

  add object oMAR1_2_23 as StdField with uid="KAGQCNZHRW",rtseq=40,rtrep=.f.,;
    cFormVar = "w_MAR1", cQueryName = "MAR1",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure obsoleto",;
    ToolTipText = "Filtro sulla marca",;
    HelpContextID = 55306950,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=140, Top=204, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_MAR1"

  func oMAR1_2_23.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_23('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAR1_2_23.ecpDrop(oSource)
    this.Parent.oContained.link_2_23('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAR1_2_23.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oMAR1_2_23'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Marchi",'',this.parent.oContained
  endproc
  proc oMAR1_2_23.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_MAR1
     i_obj.ecpSave()
  endproc

  add object oDESMA_2_24 as StdField with uid="VXVLCPVNYA",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESMA", cQueryName = "DESMA",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 125304374,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=199, Top=204, InputMask=replicate('X',40)

  add object oMAR2_2_25 as StdField with uid="EDDTPCZKID",rtseq=42,rtrep=.f.,;
    cFormVar = "w_MAR2", cQueryName = "MAR2",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure obsoleto",;
    ToolTipText = "Filtro sulla marca",;
    HelpContextID = 55372486,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=140, Top=228, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_MAR2"

  func oMAR2_2_25.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_25('Part',this)
    endwith
    return bRes
  endfunc

  proc oMAR2_2_25.ecpDrop(oSource)
    this.Parent.oContained.link_2_25('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oMAR2_2_25.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oMAR2_2_25'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"Marchi",'',this.parent.oContained
  endproc
  proc oMAR2_2_25.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_MAR2
     i_obj.ecpSave()
  endproc

  add object oDESMA2_2_26 as StdField with uid="JXWQNFWRDJ",rtseq=43,rtrep=.f.,;
    cFormVar = "w_DESMA2", cQueryName = "DESMA2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 158858806,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=199, Top=228, InputMask=replicate('X',40)

  add object oRICA1_2_29 as StdField with uid="NRKMCAZMAV",rtseq=44,rtrep=.f.,;
    cFormVar = "w_RICA1", cQueryName = "RICA1",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure obsoleto",;
    ToolTipText = "Filtro sulla classe di ricarico",;
    HelpContextID = 107676438,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=140, Top=259, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CLA_RICA", cZoomOnZoom="GSAR_ACR", oKey_1_1="CRCODICE", oKey_1_2="this.w_RICA1"

  func oRICA1_2_29.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_29('Part',this)
    endwith
    return bRes
  endfunc

  proc oRICA1_2_29.ecpDrop(oSource)
    this.Parent.oContained.link_2_29('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRICA1_2_29.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RICA','*','CRCODICE',cp_AbsName(this.parent,'oRICA1_2_29'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACR',"Clasdsi di ricarico",'',this.parent.oContained
  endproc
  proc oRICA1_2_29.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CRCODICE=this.parent.oContained.w_RICA1
     i_obj.ecpSave()
  endproc

  add object oDESRICA1_2_30 as StdField with uid="XGDRWOROMR",rtseq=45,rtrep=.f.,;
    cFormVar = "w_DESRICA1", cQueryName = "DESRICA1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 184352359,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=199, Top=259, InputMask=replicate('X',40)

  add object oRICA2_2_31 as StdField with uid="WMCAJDYPRO",rtseq=46,rtrep=.f.,;
    cFormVar = "w_RICA2", cQueryName = "RICA2",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Il codice iniziale � maggiore del codice finale oppure obsoleto",;
    ToolTipText = "Filtro sulla classe di ricarico",;
    HelpContextID = 108725014,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=140, Top=283, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CLA_RICA", cZoomOnZoom="GSAR_ACR", oKey_1_1="CRCODICE", oKey_1_2="this.w_RICA2"

  func oRICA2_2_31.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_31('Part',this)
    endwith
    return bRes
  endfunc

  proc oRICA2_2_31.ecpDrop(oSource)
    this.Parent.oContained.link_2_31('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oRICA2_2_31.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CLA_RICA','*','CRCODICE',cp_AbsName(this.parent,'oRICA2_2_31'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACR',"Classi di ricarico",'',this.parent.oContained
  endproc
  proc oRICA2_2_31.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CRCODICE=this.parent.oContained.w_RICA2
     i_obj.ecpSave()
  endproc

  add object oDESCRICA2_2_32 as StdField with uid="YZMRGDLUJK",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESCRICA2", cQueryName = "DESCRICA2",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 243400297,;
   bGlobalFont=.t.,;
    Height=21, Width=317, Left=199, Top=283, InputMask=replicate('X',40)

  add object oGIALIST_2_35 as StdCheck with uid="TADZVIQAOD",rtseq=48,rtrep=.f.,left=481, top=314, caption="Articoli con disponibilit� >0",;
    ToolTipText = "Articoli con disponibilit� >0",;
    HelpContextID = 84549018,;
    cFormVar="w_GIALIST", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oGIALIST_2_35.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oGIALIST_2_35.GetRadio()
    this.Parent.oContained.w_GIALIST = this.RadioValue()
    return .t.
  endfunc

  func oGIALIST_2_35.SetRadio()
    this.Parent.oContained.w_GIALIST=trim(this.Parent.oContained.w_GIALIST)
    this.value = ;
      iif(this.Parent.oContained.w_GIALIST=='S',1,;
      0)
  endfunc

  add object oCODMAG_2_36 as StdField with uid="LWWLJUWSIC",rtseq=49,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .t. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Magazzino selezionato",;
    HelpContextID = 242685990,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=140, Top=313, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_2_36.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_GIALIST='S')
    endwith
   endif
  endfunc

  func oCODMAG_2_36.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_36('Part',this)
    endwith
    return bRes
  endfunc

  func oCODMAG_2_36.CondObbl()
    local i_bRes
    i_bRes = .t.
    with this.Parent.oContained
      i_bres=.w_GIALIST='S'
    endwith
    return i_bres
  endfunc

  proc oCODMAG_2_36.ecpDrop(oSource)
    this.Parent.oContained.link_2_36('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_2_36.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_2_36'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oCODMAG_2_36.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oDESMAG_2_37 as StdField with uid="FFZLWMSBUI",rtseq=50,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 242744886,;
   bGlobalFont=.t.,;
    Height=21, Width=275, Left=199, Top=313, InputMask=replicate('X',30)

  add object oStr_2_4 as StdString with uid="RNPPZUUIUS",Visible=.t., Left=16, Top=49,;
    Alignment=1, Width=87, Height=18,;
    Caption="Listino:"  ;
  , bGlobalFont=.t.

  add object oStr_2_5 as StdString with uid="CKYBVRYZSQ",Visible=.t., Left=30, Top=9,;
    Alignment=0, Width=415, Height=18,;
    Caption="Listino selezionato"  ;
  , bGlobalFont=.t.

  add object oStr_2_7 as StdString with uid="KPTVDMXVAW",Visible=.t., Left=16, Top=75,;
    Alignment=1, Width=87, Height=15,;
    Caption="Lingua:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="SEKPOGQJIN",Visible=.t., Left=498, Top=48,;
    Alignment=1, Width=60, Height=15,;
    Caption="Alla data:"  ;
  , bGlobalFont=.t.

  add object oStr_2_13 as StdString with uid="JNQRMLRYAE",Visible=.t., Left=26, Top=-73,;
    Alignment=0, Width=626, Height=15,;
    Caption="Selezione articoli"  ;
  , bGlobalFont=.t.

  add object oStr_2_19 as StdString with uid="RQKJSYIXMT",Visible=.t., Left=54, Top=148,;
    Alignment=1, Width=86, Height=18,;
    Caption="Da famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_20 as StdString with uid="DJDIMQYZBX",Visible=.t., Left=54, Top=172,;
    Alignment=1, Width=86, Height=18,;
    Caption="A famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_2_21 as StdString with uid="JOVHQRZFNK",Visible=.t., Left=29, Top=127,;
    Alignment=0, Width=626, Height=15,;
    Caption="Selezione articoli"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="CWATPPXAPX",Visible=.t., Left=49, Top=205,;
    Alignment=1, Width=86, Height=18,;
    Caption="Da marca:"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="PKSCZLUSIZ",Visible=.t., Left=49, Top=229,;
    Alignment=1, Width=86, Height=18,;
    Caption="A marca:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="RCSASCYHQK",Visible=.t., Left=23, Top=260,;
    Alignment=1, Width=115, Height=18,;
    Caption="Da classe di ricarico:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="LSWEHFPUNV",Visible=.t., Left=32, Top=284,;
    Alignment=1, Width=106, Height=18,;
    Caption="A classe di ricarico:"  ;
  , bGlobalFont=.t.

  add object oStr_2_38 as StdString with uid="NXCISOCKHU",Visible=.t., Left=38, Top=314,;
    Alignment=1, Width=100, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oBox_2_6 as StdBox with uid="QBSHBFNYQN",left=27, top=26, width=420,height=1

  add object oBox_2_14 as StdBox with uid="KOHYVKDAVA",left=24, top=-57, width=630,height=1

  add object oBox_2_22 as StdBox with uid="UFCCDAMACP",left=27, top=143, width=630,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_kli','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
