* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_ssc                                                        *
*              Controllo scorte magazzino                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_78]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-24                                                      *
* Last revis.: 2007-07-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_ssc",oParentObject))

* --- Class definition
define class tgsma_ssc as StdForm
  Top    = 9
  Left   = 10

  * --- Standard Properties
  Width  = 621
  Height = 413
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-13"
  HelpContextID=90864489
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  _IDX = 0
  ART_ICOL_IDX = 0
  MAGAZZIN_IDX = 0
  FAM_ARTI_IDX = 0
  GRUMERC_IDX = 0
  CATEGOMO_IDX = 0
  MARCHI_IDX = 0
  CONTI_IDX = 0
  ESERCIZI_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gsma_ssc"
  cComment = "Controllo scorte magazzino"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DATASTAM = ctod('  /  /  ')
  o_DATASTAM = ctod('  /  /  ')
  w_ANTIPCON = space(10)
  w_CODMAG = space(5)
  w_DESMAG = space(30)
  w_CODFAM = space(5)
  w_DESFAM = space(35)
  w_CODGRU = space(5)
  w_DESGRU = space(35)
  w_CODCAT = space(5)
  w_DESCAT = space(35)
  w_CODMAR = space(5)
  w_DESMAR = space(35)
  w_CODPRO = space(15)
  w_DESPRO = space(40)
  w_CODFOR1 = space(15)
  w_CODART = space(20)
  w_DESFOR = space(40)
  w_ZEROSCO = space(1)
  w_NETTIF = space(1)
  w_TIPO = space(2)
  w_DESINI = space(40)
  w_SELSCO = space(10)
  w_SELART = space(10)
  o_SELART = space(10)
  w_SELORD = space(10)
  w_ELABSC = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_ONUME = 0
  o_ONUME = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_sscPag1","gsma_ssc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATASTAM_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[9]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='MAGAZZIN'
    this.cWorkTables[3]='FAM_ARTI'
    this.cWorkTables[4]='GRUMERC'
    this.cWorkTables[5]='CATEGOMO'
    this.cWorkTables[6]='MARCHI'
    this.cWorkTables[7]='CONTI'
    this.cWorkTables[8]='ESERCIZI'
    this.cWorkTables[9]='AZIENDA'
    return(this.OpenAllTables(9))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATASTAM=ctod("  /  /  ")
      .w_ANTIPCON=space(10)
      .w_CODMAG=space(5)
      .w_DESMAG=space(30)
      .w_CODFAM=space(5)
      .w_DESFAM=space(35)
      .w_CODGRU=space(5)
      .w_DESGRU=space(35)
      .w_CODCAT=space(5)
      .w_DESCAT=space(35)
      .w_CODMAR=space(5)
      .w_DESMAR=space(35)
      .w_CODPRO=space(15)
      .w_DESPRO=space(40)
      .w_CODFOR1=space(15)
      .w_CODART=space(20)
      .w_DESFOR=space(40)
      .w_ZEROSCO=space(1)
      .w_NETTIF=space(1)
      .w_TIPO=space(2)
      .w_DESINI=space(40)
      .w_SELSCO=space(10)
      .w_SELART=space(10)
      .w_SELORD=space(10)
      .w_ELABSC=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_ONUME=0
        .w_DATASTAM = i_datsys
        .w_ANTIPCON = 'F'
        .w_CODMAG = IIF( .w_ONUME<>3 ,.w_CODMAG , Space(5) )
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_CODMAG))
          .link_1_3('Full')
        endif
        .DoRTCalc(4,5,.f.)
        if not(empty(.w_CODFAM))
          .link_1_5('Full')
        endif
        .DoRTCalc(6,7,.f.)
        if not(empty(.w_CODGRU))
          .link_1_7('Full')
        endif
        .DoRTCalc(8,9,.f.)
        if not(empty(.w_CODCAT))
          .link_1_9('Full')
        endif
        .DoRTCalc(10,11,.f.)
        if not(empty(.w_CODMAR))
          .link_1_11('Full')
        endif
        .DoRTCalc(12,13,.f.)
        if not(empty(.w_CODPRO))
          .link_1_13('Full')
        endif
        .DoRTCalc(14,15,.f.)
        if not(empty(.w_CODFOR1))
          .link_1_15('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CODART))
          .link_1_16('Full')
        endif
          .DoRTCalc(17,17,.f.)
        .w_ZEROSCO = IIF( .w_ONUME = 3 , ' ' , .w_ZEROSCO )
        .w_NETTIF = ' '
          .DoRTCalc(20,21,.f.)
        .w_SELSCO = 'MIN'
        .w_SELART = 'T'
        .w_SELORD = 'A'
        .w_ELABSC = 'E'
      .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        .w_OBTEST = .w_DATASTAM
    endwith
    this.DoRTCalc(27,28,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_ONUME<>.w_ONUME
            .w_CODMAG = IIF( .w_ONUME<>3 ,.w_CODMAG , Space(5) )
          .link_1_3('Full')
        endif
        .DoRTCalc(4,17,.t.)
        if .o_ONUME<>.w_ONUME
            .w_ZEROSCO = IIF( .w_ONUME = 3 , ' ' , .w_ZEROSCO )
        endif
        .DoRTCalc(19,24,.t.)
        if .o_SELART<>.w_SELART
            .w_ELABSC = 'E'
        endif
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
        if .o_DATASTAM<>.w_DATASTAM
            .w_OBTEST = .w_DATASTAM
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(27,28,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_27.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODMAG_1_3.enabled = this.oPgFrm.Page1.oPag.oCODMAG_1_3.mCond()
    this.oPgFrm.Page1.oPag.oZEROSCO_1_19.enabled = this.oPgFrm.Page1.oPag.oZEROSCO_1_19.mCond()
    this.oPgFrm.Page1.oPag.oNETTIF_1_20.enabled = this.oPgFrm.Page1.oPag.oNETTIF_1_20.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oELABSC_1_26.visible=!this.oPgFrm.Page1.oPag.oELABSC_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_27.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODMAG
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGDISMAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_CODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO,MGDISMAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStrODBC(trim(this.w_CODMAG)+"%");

            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGDISMAG";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MGDESMAG like "+cp_ToStr(trim(this.w_CODMAG)+"%");

            select MGCODMAG,MGDESMAG,MGDTOBSO,MGDISMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oCODMAG_1_3'),i_cWhere,'GSAR_AMA',"Magazzino",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGDISMAG";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGDISMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO,MGDISMAG";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_CODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_CODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO,MGDISMAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
      this.w_NETTIF = NVL(_Link_.MGDISMAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_NETTIF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Magazzino inesistente oppure obsoleto")
        endif
        this.w_CODMAG = space(5)
        this.w_DESMAG = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_NETTIF = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFAM
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.FAM_ARTI_IDX,3]
    i_lTable = "FAM_ARTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2], .t., this.FAM_ARTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFAM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFA',True,'FAM_ARTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" FACODICE like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%");

          i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by FACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'FACODICE',trim(this.w_CODFAM))
          select FACODICE,FADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by FACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFAM)==trim(_Link_.FACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( FADESCRI like "+cp_ToStrODBC(trim(this.w_CODFAM)+"%")+cp_TransWhereFldName('FADESCRI',trim(this.w_CODFAM))+")";

            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" FADESCRI like "+cp_ToStr(trim(this.w_CODFAM)+"%");

            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFAM) and !this.bDontReportError
            deferred_cp_zoom('FAM_ARTI','*','FACODICE',cp_AbsName(oSource.parent,'oCODFAM_1_5'),i_cWhere,'GSAR_AFA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',oSource.xKey(1))
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFAM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select FACODICE,FADESCRI"+cp_TransInsFldName("FADESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where FACODICE="+cp_ToStrODBC(this.w_CODFAM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'FACODICE',this.w_CODFAM)
            select FACODICE,FADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFAM = NVL(_Link_.FACODICE,space(5))
      this.w_DESFAM = NVL(cp_TransLoadField('_Link_.FADESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODFAM = space(5)
      endif
      this.w_DESFAM = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.FAM_ARTI_IDX,2])+'\'+cp_ToStr(_Link_.FACODICE,1)
      cp_ShowWarn(i_cKey,this.FAM_ARTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFAM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODGRU
  func Link_1_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODGRU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_CODGRU)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_CODGRU))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODGRU)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( GMDESCRI like "+cp_ToStrODBC(trim(this.w_CODGRU)+"%")+cp_TransWhereFldName('GMDESCRI',trim(this.w_CODGRU))+")";

            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" GMDESCRI like "+cp_ToStr(trim(this.w_CODGRU)+"%");

            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODGRU) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oCODGRU_1_7'),i_cWhere,'GSAR_AGM',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODGRU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_CODGRU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_CODGRU)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODGRU = NVL(_Link_.GMCODICE,space(5))
      this.w_DESGRU = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODGRU = space(5)
      endif
      this.w_DESGRU = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODGRU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODCAT
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATEGOMO_IDX,3]
    i_lTable = "CATEGOMO"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2], .t., this.CATEGOMO_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AOM',True,'CATEGOMO')
        if i_nConn<>0
          i_cWhere = i_cFlt+" OMCODICE like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%");

          i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by OMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'OMCODICE',trim(this.w_CODCAT))
          select OMCODICE,OMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by OMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCAT)==trim(_Link_.OMCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+"( OMDESCRI like "+cp_ToStrODBC(trim(this.w_CODCAT)+"%")+cp_TransWhereFldName('OMDESCRI',trim(this.w_CODCAT))+")";

            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" OMDESCRI like "+cp_ToStr(trim(this.w_CODCAT)+"%");

            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCAT) and !this.bDontReportError
            deferred_cp_zoom('CATEGOMO','*','OMCODICE',cp_AbsName(oSource.parent,'oCODCAT_1_9'),i_cWhere,'GSAR_AOM',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',oSource.xKey(1))
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select OMCODICE,OMDESCRI"+cp_TransInsFldName("OMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where OMCODICE="+cp_ToStrODBC(this.w_CODCAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'OMCODICE',this.w_CODCAT)
            select OMCODICE,OMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAT = NVL(_Link_.OMCODICE,space(5))
      this.w_DESCAT = NVL(cp_TransLoadField('_Link_.OMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAT = space(5)
      endif
      this.w_DESCAT = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATEGOMO_IDX,2])+'\'+cp_ToStr(_Link_.OMCODICE,1)
      cp_ShowWarn(i_cKey,this.CATEGOMO_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMAR
  func Link_1_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MARCHI_IDX,3]
    i_lTable = "MARCHI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2], .t., this.MARCHI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMAR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMH',True,'MARCHI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MACODICE like "+cp_ToStrODBC(trim(this.w_CODMAR)+"%");

          i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MACODICE',trim(this.w_CODMAR))
          select MACODICE,MADESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMAR)==trim(_Link_.MACODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStrODBC(trim(this.w_CODMAR)+"%");

            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MADESCRI like "+cp_ToStr(trim(this.w_CODMAR)+"%");

            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMAR) and !this.bDontReportError
            deferred_cp_zoom('MARCHI','*','MACODICE',cp_AbsName(oSource.parent,'oCODMAR_1_11'),i_cWhere,'GSAR_AMH',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',oSource.xKey(1))
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMAR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MACODICE,MADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where MACODICE="+cp_ToStrODBC(this.w_CODMAR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MACODICE',this.w_CODMAR)
            select MACODICE,MADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMAR = NVL(_Link_.MACODICE,space(5))
      this.w_DESMAR = NVL(_Link_.MADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODMAR = space(5)
      endif
      this.w_DESMAR = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MARCHI_IDX,2])+'\'+cp_ToStr(_Link_.MACODICE,1)
      cp_ShowWarn(i_cKey,this.MARCHI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMAR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRO
  func Link_1_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODPRO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_ANTIPCON;
                     ,'ANCODICE',trim(this.w_CODPRO))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPRO)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODPRO)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODPRO)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_ANTIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPRO) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODPRO_1_13'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ANTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODPRO);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_ANTIPCON;
                       ,'ANCODICE',this.w_CODPRO)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRO = NVL(_Link_.ANCODICE,space(15))
      this.w_DESPRO = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRO = space(15)
      endif
      this.w_DESPRO = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_CODPRO = space(15)
        this.w_DESPRO = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFOR1
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFOR1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODFOR1)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_ANTIPCON;
                     ,'ANCODICE',trim(this.w_CODFOR1))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFOR1)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFOR1) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODFOR1_1_15'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_ANTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFOR1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODFOR1);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_ANTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_ANTIPCON;
                       ,'ANCODICE',this.w_CODFOR1)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFOR1 = NVL(_Link_.ANCODICE,space(15))
      this.w_DESFOR = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODFOR1 = space(15)
      endif
      this.w_DESFOR = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice inesistente oppure obsoleto")
        endif
        this.w_CODFOR1 = space(15)
        this.w_DESFOR = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFOR1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODART
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_CODART))
          select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStrODBC(trim(this.w_CODART)+"%");

            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ARDESART like "+cp_ToStr(trim(this.w_CODART)+"%");

            select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oCODART_1_16'),i_cWhere,'GSMA_BZA',"Elenco articoli",'GSMA0ADI.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_CODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_CODART)
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
      this.w_TIPO = NVL(_Link_.ARTIPART,space(2))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODART = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_TIPO = space(2)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPO$'PF-SE-MP-PH-MC-MA-IM-FS'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice di tipo non articolo oppure codice obsoleto o inesistente")
        endif
        this.w_CODART = space(20)
        this.w_DESINI = space(40)
        this.w_TIPO = space(2)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATASTAM_1_1.value==this.w_DATASTAM)
      this.oPgFrm.Page1.oPag.oDATASTAM_1_1.value=this.w_DATASTAM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAG_1_3.value==this.w_CODMAG)
      this.oPgFrm.Page1.oPag.oCODMAG_1_3.value=this.w_CODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_4.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_4.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFAM_1_5.value==this.w_CODFAM)
      this.oPgFrm.Page1.oPag.oCODFAM_1_5.value=this.w_CODFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFAM_1_6.value==this.w_DESFAM)
      this.oPgFrm.Page1.oPag.oDESFAM_1_6.value=this.w_DESFAM
    endif
    if not(this.oPgFrm.Page1.oPag.oCODGRU_1_7.value==this.w_CODGRU)
      this.oPgFrm.Page1.oPag.oCODGRU_1_7.value=this.w_CODGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oDESGRU_1_8.value==this.w_DESGRU)
      this.oPgFrm.Page1.oPag.oDESGRU_1_8.value=this.w_DESGRU
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAT_1_9.value==this.w_CODCAT)
      this.oPgFrm.Page1.oPag.oCODCAT_1_9.value=this.w_CODCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAT_1_10.value==this.w_DESCAT)
      this.oPgFrm.Page1.oPag.oDESCAT_1_10.value=this.w_DESCAT
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMAR_1_11.value==this.w_CODMAR)
      this.oPgFrm.Page1.oPag.oCODMAR_1_11.value=this.w_CODMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAR_1_12.value==this.w_DESMAR)
      this.oPgFrm.Page1.oPag.oDESMAR_1_12.value=this.w_DESMAR
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPRO_1_13.value==this.w_CODPRO)
      this.oPgFrm.Page1.oPag.oCODPRO_1_13.value=this.w_CODPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRO_1_14.value==this.w_DESPRO)
      this.oPgFrm.Page1.oPag.oDESPRO_1_14.value=this.w_DESPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFOR1_1_15.value==this.w_CODFOR1)
      this.oPgFrm.Page1.oPag.oCODFOR1_1_15.value=this.w_CODFOR1
    endif
    if not(this.oPgFrm.Page1.oPag.oCODART_1_16.value==this.w_CODART)
      this.oPgFrm.Page1.oPag.oCODART_1_16.value=this.w_CODART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESFOR_1_17.value==this.w_DESFOR)
      this.oPgFrm.Page1.oPag.oDESFOR_1_17.value=this.w_DESFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oZEROSCO_1_19.RadioValue()==this.w_ZEROSCO)
      this.oPgFrm.Page1.oPag.oZEROSCO_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNETTIF_1_20.RadioValue()==this.w_NETTIF)
      this.oPgFrm.Page1.oPag.oNETTIF_1_20.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESINI_1_22.value==this.w_DESINI)
      this.oPgFrm.Page1.oPag.oDESINI_1_22.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSELSCO_1_23.RadioValue()==this.w_SELSCO)
      this.oPgFrm.Page1.oPag.oSELSCO_1_23.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELART_1_24.RadioValue()==this.w_SELART)
      this.oPgFrm.Page1.oPag.oSELART_1_24.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oSELORD_1_25.RadioValue()==this.w_SELORD)
      this.oPgFrm.Page1.oPag.oSELORD_1_25.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oELABSC_1_26.RadioValue()==this.w_ELABSC)
      this.oPgFrm.Page1.oPag.oELABSC_1_26.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_DATASTAM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATASTAM_1_1.SetFocus()
            i_bnoObbl = !empty(.w_DATASTAM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and (.w_ONUME<>3)  and not(empty(.w_CODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMAG_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Magazzino inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CODPRO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODPRO_1_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)  and not(empty(.w_CODFOR1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFOR1_1_15.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice inesistente oppure obsoleto")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) AND .w_TIPO$'PF-SE-MP-PH-MC-MA-IM-FS')  and not(empty(.w_CODART))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODART_1_16.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice di tipo non articolo oppure codice obsoleto o inesistente")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATASTAM = this.w_DATASTAM
    this.o_SELART = this.w_SELART
    this.o_ONUME = this.w_ONUME
    return

enddefine

* --- Define pages as container
define class tgsma_sscPag1 as StdContainer
  Width  = 617
  height = 413
  stdWidth  = 617
  stdheight = 413
  resizeXpos=405
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATASTAM_1_1 as StdField with uid="OZFWSSELLY",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATASTAM", cQueryName = "DATASTAM",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di stampa",;
    HelpContextID = 67897987,;
   bGlobalFont=.t.,;
    Height=21, Width=72, Left=120, Top=8

  add object oCODMAG_1_3 as StdField with uid="OPCQYSZQRT",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CODMAG", cQueryName = "CODMAG",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Magazzino inesistente oppure obsoleto",;
    ToolTipText = "Codice del magazzino da considerare",;
    HelpContextID = 168355802,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=120, Top=32, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_CODMAG"

  func oCODMAG_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ONUME<>3)
    endwith
   endif
  endfunc

  func oCODMAG_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAG_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAG_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oCODMAG_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzino",'',this.parent.oContained
  endproc
  proc oCODMAG_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_CODMAG
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_4 as StdField with uid="HMXLWZFAKI",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 168296906,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=184, Top=32, InputMask=replicate('X',30)

  add object oCODFAM_1_5 as StdField with uid="CAZYAQAKNG",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODFAM", cQueryName = "CODFAM",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della famiglia da considerare",;
    HelpContextID = 68151258,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=120, Top=56, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="FAM_ARTI", cZoomOnZoom="GSAR_AFA", oKey_1_1="FACODICE", oKey_1_2="this.w_CODFAM"

  func oCODFAM_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFAM_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFAM_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'FAM_ARTI','*','FACODICE',cp_AbsName(this.parent,'oCODFAM_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFA',"",'',this.parent.oContained
  endproc
  proc oCODFAM_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_FACODICE=this.parent.oContained.w_CODFAM
     i_obj.ecpSave()
  endproc

  add object oDESFAM_1_6 as StdField with uid="SKXLGQCZIL",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESFAM", cQueryName = "DESFAM",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 68092362,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=184, Top=56, InputMask=replicate('X',35)

  add object oCODGRU_1_7 as StdField with uid="NFSMTFLWOP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODGRU", cQueryName = "CODGRU",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice del gruppo merceologico da considerare",;
    HelpContextID = 83957798,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=120, Top=80, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_CODGRU"

  func oCODGRU_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODGRU_1_7.ecpDrop(oSource)
    this.Parent.oContained.link_1_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODGRU_1_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oCODGRU_1_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"",'',this.parent.oContained
  endproc
  proc oCODGRU_1_7.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_CODGRU
     i_obj.ecpSave()
  endproc

  add object oDESGRU_1_8 as StdField with uid="QHMLHWIKGG",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESGRU", cQueryName = "DESGRU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 84016694,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=184, Top=80, InputMask=replicate('X',35)

  add object oCODCAT_1_9 as StdField with uid="YJKKTHAHNY",rtseq=9,rtrep=.f.,;
    cFormVar = "w_CODCAT", cQueryName = "CODCAT",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della categoria omogenea da considerare",;
    HelpContextID = 49092646,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=120, Top=104, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CATEGOMO", cZoomOnZoom="GSAR_AOM", oKey_1_1="OMCODICE", oKey_1_2="this.w_CODCAT"

  func oCODCAT_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCAT_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCAT_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATEGOMO','*','OMCODICE',cp_AbsName(this.parent,'oCODCAT_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AOM',"",'',this.parent.oContained
  endproc
  proc oCODCAT_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AOM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_OMCODICE=this.parent.oContained.w_CODCAT
     i_obj.ecpSave()
  endproc

  add object oDESCAT_1_10 as StdField with uid="YUTGXVVCVO",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCAT", cQueryName = "DESCAT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 49151542,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=184, Top=104, InputMask=replicate('X',35)

  add object oCODMAR_1_11 as StdField with uid="VELVVYXNNT",rtseq=11,rtrep=.f.,;
    cFormVar = "w_CODMAR", cQueryName = "CODMAR",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice della marca da considerare",;
    HelpContextID = 16193574,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=120, Top=128, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MARCHI", cZoomOnZoom="GSAR_AMH", oKey_1_1="MACODICE", oKey_1_2="this.w_CODMAR"

  func oCODMAR_1_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMAR_1_11.ecpDrop(oSource)
    this.Parent.oContained.link_1_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMAR_1_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MARCHI','*','MACODICE',cp_AbsName(this.parent,'oCODMAR_1_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMH',"",'',this.parent.oContained
  endproc
  proc oCODMAR_1_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMH()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MACODICE=this.parent.oContained.w_CODMAR
     i_obj.ecpSave()
  endproc

  add object oDESMAR_1_12 as StdField with uid="RBQZZJAURK",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DESMAR", cQueryName = "DESMAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 16252470,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=184, Top=128, InputMask=replicate('X',35)

  add object oCODPRO_1_13 as StdField with uid="FPGCDQLCXY",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CODPRO", cQueryName = "CODPRO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice del produttore da considerare",;
    HelpContextID = 16115674,;
   bGlobalFont=.t.,;
    Height=21, Width=163, Left=120, Top=152, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_ANTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODPRO"

  func oCODPRO_1_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPRO_1_13.ecpDrop(oSource)
    this.Parent.oContained.link_1_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPRO_1_13.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_ANTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_ANTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODPRO_1_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oCODPRO_1_13.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_ANTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODPRO
     i_obj.ecpSave()
  endproc

  add object oDESPRO_1_14 as StdField with uid="NIWPPARKTO",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DESPRO", cQueryName = "DESPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 16056778,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=292, Top=152, InputMask=replicate('X',40)

  add object oCODFOR1_1_15 as StdField with uid="EGROJSSEOB",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CODFOR1", cQueryName = "CODFOR1",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Codice inesistente oppure obsoleto",;
    ToolTipText = "Codice del fornitore abituale da considerare",;
    HelpContextID = 30414886,;
   bGlobalFont=.t.,;
    Height=21, Width=163, Left=120, Top=176, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_ANTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODFOR1"

  func oCODFOR1_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFOR1_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFOR1_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_ANTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_ANTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODFOR1_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oCODFOR1_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_ANTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODFOR1
     i_obj.ecpSave()
  endproc

  add object oCODART_1_16 as StdField with uid="GGKMJKWYQQ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CODART", cQueryName = "CODART",;
    bObbl = .f. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice di tipo non articolo oppure codice obsoleto o inesistente",;
    ToolTipText = "Codice dell'articolo da considerare",;
    HelpContextID = 66787366,;
   bGlobalFont=.t.,;
    Height=21, Width=163, Left=120, Top=200, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_CODART"

  func oCODART_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODART_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODART_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oCODART_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"Elenco articoli",'GSMA0ADI.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oCODART_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_CODART
     i_obj.ecpSave()
  endproc

  add object oDESFOR_1_17 as StdField with uid="SYCBOVRNIY",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DESFOR", cQueryName = "DESFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 30473782,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=292, Top=176, InputMask=replicate('X',40)

  add object oZEROSCO_1_19 as StdCheck with uid="QDTYYGHSVX",rtseq=18,rtrep=.f.,left=292, top=235, caption="Scorta a zero",;
    ToolTipText = "Esamina anche articoli privi di scorta minima / massima (impostata a 0)",;
    HelpContextID = 216404074,;
    cFormVar="w_ZEROSCO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oZEROSCO_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oZEROSCO_1_19.GetRadio()
    this.Parent.oContained.w_ZEROSCO = this.RadioValue()
    return .t.
  endfunc

  func oZEROSCO_1_19.SetRadio()
    this.Parent.oContained.w_ZEROSCO=trim(this.Parent.oContained.w_ZEROSCO)
    this.value = ;
      iif(this.Parent.oContained.w_ZEROSCO=='S',1,;
      0)
  endfunc

  func oZEROSCO_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ONUME<>3)
    endwith
   endif
  endfunc

  add object oNETTIF_1_20 as StdCheck with uid="CXBHTSIYZD",rtseq=19,rtrep=.f.,left=444, top=235, caption="Solo mag.nettificabili",;
    ToolTipText = "Se attivo, considera solo i magazzini nettificabili",;
    HelpContextID = 176222506,;
    cFormVar="w_NETTIF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oNETTIF_1_20.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oNETTIF_1_20.GetRadio()
    this.Parent.oContained.w_NETTIF = this.RadioValue()
    return .t.
  endfunc

  func oNETTIF_1_20.SetRadio()
    this.Parent.oContained.w_NETTIF=trim(this.Parent.oContained.w_NETTIF)
    this.value = ;
      iif(this.Parent.oContained.w_NETTIF=='S',1,;
      0)
  endfunc

  func oNETTIF_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty( .w_CODMAG ))
    endwith
   endif
  endfunc

  add object oDESINI_1_22 as StdField with uid="JTLSUJDAZC",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 121373130,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=292, Top=200, InputMask=replicate('X',40)


  add object oSELSCO_1_23 as StdCombo with uid="MPEZRCLSVF",rtseq=22,rtrep=.f.,left=100,top=278,width=108,height=21;
    , ToolTipText = "Consente di selezionare il tipo di scorta da controllare";
    , HelpContextID = 31617242;
    , cFormVar="w_SELSCO",RowSource=""+"Scorta minima,"+"Scorta massima", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELSCO_1_23.RadioValue()
    return(iif(this.value =1,'MIN',;
    iif(this.value =2,'MAX',;
    space(10))))
  endfunc
  func oSELSCO_1_23.GetRadio()
    this.Parent.oContained.w_SELSCO = this.RadioValue()
    return .t.
  endfunc

  func oSELSCO_1_23.SetRadio()
    this.Parent.oContained.w_SELSCO=trim(this.Parent.oContained.w_SELSCO)
    this.value = ;
      iif(this.Parent.oContained.w_SELSCO=='MIN',1,;
      iif(this.Parent.oContained.w_SELSCO=='MAX',2,;
      0))
  endfunc


  add object oSELART_1_24 as StdCombo with uid="HKWGLUQEZU",rtseq=23,rtrep=.f.,left=294,top=278,width=108,height=21;
    , ToolTipText = "Consente di stampare tutti gli articoli o soltanto quelli fuori scorta";
    , HelpContextID = 66817830;
    , cFormVar="w_SELART",RowSource=""+"Fuori scorta,"+"Tutti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELART_1_24.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'T',;
    space(10))))
  endfunc
  func oSELART_1_24.GetRadio()
    this.Parent.oContained.w_SELART = this.RadioValue()
    return .t.
  endfunc

  func oSELART_1_24.SetRadio()
    this.Parent.oContained.w_SELART=trim(this.Parent.oContained.w_SELART)
    this.value = ;
      iif(this.Parent.oContained.w_SELART=='E',1,;
      iif(this.Parent.oContained.w_SELART=='T',2,;
      0))
  endfunc


  add object oSELORD_1_25 as StdCombo with uid="BVIUHAXIAY",rtseq=24,rtrep=.f.,left=493,top=277,width=108,height=21;
    , ToolTipText = "Consente di selezionare l'ordinamento di stampa";
    , HelpContextID = 200700122;
    , cFormVar="w_SELORD",RowSource=""+"Per articolo,"+"Per descrizione", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSELORD_1_25.RadioValue()
    return(iif(this.value =1,'A',;
    iif(this.value =2,'D',;
    space(10))))
  endfunc
  func oSELORD_1_25.GetRadio()
    this.Parent.oContained.w_SELORD = this.RadioValue()
    return .t.
  endfunc

  func oSELORD_1_25.SetRadio()
    this.Parent.oContained.w_SELORD=trim(this.Parent.oContained.w_SELORD)
    this.value = ;
      iif(this.Parent.oContained.w_SELORD=='A',1,;
      iif(this.Parent.oContained.w_SELORD=='D',2,;
      0))
  endfunc


  add object oELABSC_1_26 as StdCombo with uid="AHPTZNGLES",rtseq=25,rtrep=.f.,left=100,top=305,width=108,height=21;
    , ToolTipText = "Scorta elaborata su esistenza o disponibilitÓ";
    , HelpContextID = 217324218;
    , cFormVar="w_ELABSC",RowSource=""+"Esistenza,"+"DisponibilitÓ", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oELABSC_1_26.RadioValue()
    return(iif(this.value =1,'E',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oELABSC_1_26.GetRadio()
    this.Parent.oContained.w_ELABSC = this.RadioValue()
    return .t.
  endfunc

  func oELABSC_1_26.SetRadio()
    this.Parent.oContained.w_ELABSC=trim(this.Parent.oContained.w_ELABSC)
    this.value = ;
      iif(this.Parent.oContained.w_ELABSC=='E',1,;
      iif(this.Parent.oContained.w_ELABSC=='D',2,;
      0))
  endfunc

  func oELABSC_1_26.mHide()
    with this.Parent.oContained
      return (.w_SELART='T')
    endwith
  endfunc


  add object oObj_1_27 as cp_outputCombo with uid="ZLERXIVPWT",left=120, top=335, width=481,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 87133158


  add object oBtn_1_28 as StdButton with uid="INEKHPBPKL",left=503, top=363, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 50925094;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_28.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_29 as StdButton with uid="FSUHPUAONE",left=553, top=363, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 83547066;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_18 as StdString with uid="XFTVRKFFZQ",Visible=.t., Left=18, Top=200,;
    Alignment=1, Width=100, Height=15,;
    Caption="Articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="ZRYWDVFUNP",Visible=.t., Left=18, Top=32,;
    Alignment=1, Width=100, Height=15,;
    Caption="Magazzino:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="YITRMLQGNR",Visible=.t., Left=18, Top=56,;
    Alignment=1, Width=100, Height=15,;
    Caption="Famiglia:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="TCCQFNGLGY",Visible=.t., Left=9, Top=80,;
    Alignment=1, Width=109, Height=15,;
    Caption="Gr. merceologico:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="IUPTVYRJPO",Visible=.t., Left=18, Top=8,;
    Alignment=1, Width=100, Height=15,;
    Caption="Data di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="EVOPVZITOG",Visible=.t., Left=8, Top=104,;
    Alignment=1, Width=110, Height=15,;
    Caption="Cat. omogenea:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="VZMAUGNOHW",Visible=.t., Left=18, Top=128,;
    Alignment=1, Width=100, Height=15,;
    Caption="Marca:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="NIAQWDERWS",Visible=.t., Left=18, Top=152,;
    Alignment=1, Width=100, Height=15,;
    Caption="Produttore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="GQCPNTWIOH",Visible=.t., Left=23, Top=241,;
    Alignment=0, Width=128, Height=15,;
    Caption="Opzioni di stampa"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="EQIOCMYFIS",Visible=.t., Left=27, Top=278,;
    Alignment=1, Width=71, Height=15,;
    Caption="Tipo scorta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="YRFOUQKKZF",Visible=.t., Left=208, Top=278,;
    Alignment=1, Width=83, Height=15,;
    Caption="Stato scorta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="VDECHSQKPI",Visible=.t., Left=407, Top=278,;
    Alignment=1, Width=82, Height=15,;
    Caption="Ordinamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="EGESVLXIMC",Visible=.t., Left=17, Top=306,;
    Alignment=1, Width=81, Height=18,;
    Caption="Calcolata su:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (.w_SELART='T')
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="AHGQQDQCGU",Visible=.t., Left=9, Top=176,;
    Alignment=1, Width=108, Height=18,;
    Caption="Fornitore abituale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="HRTFFGQQPR",Visible=.t., Left=25, Top=335,;
    Alignment=1, Width=93, Height=15,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oBox_1_37 as StdBox with uid="XXDIWRFUXD",left=19, top=259, width=587,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_ssc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
