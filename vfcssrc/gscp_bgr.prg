* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_bgr                                                        *
*              Dichiarazione di intento                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_70]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-26                                                      *
* Last revis.: 2002-03-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_OPERAZ
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscp_bgr",oParentObject,m.w_OPERAZ)
return(i_retval)

define class tgscp_bgr as StdBatch
  * --- Local variables
  w_OPERAZ = space(30)
  w_TIPOPE = space(1)
  w_CODIVE = space(5)
  w_RIFDIC = space(10)
  w_IMPDIC = 0
  w_IMPUTI = 0
  w_FLANAL = space(1)
  w_FLGCOM = space(1)
  w_VOCRIC = space(15)
  w_OK_LET = .f.
  * --- WorkFile variables
  ART_ICOL_idx=0
  TIP_DOCU_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tipo operazione
    * --- Variabili caller
    * --- Variabili locali
    do case
      case this.w_OPERAZ = "DICHIARAZIONE INTENTO"
        * --- Se Cliente/Fornitore e no Codice Iva Non Imponibile
        * --- La procedura prende sempre quella con data lettera pi� alta (Esclude dichiarazioni ad operazione specifica)
        * --- Lettura lettera di intento valida
        DECLARE ARRDIC (14,1)
        * --- Azzero l'Array che verr� riempito dalla Funzione
        ARRDIC(1)=0
        this.w_OK_LET = CAL_LETT(this.oParentObject.w_MVDATDOC,this.oParentObject.w_MVTIPCON,this.oParentObject.w_MVCODCON, @ArrDic)
        if this.w_OK_LET
          * --- Parametri
          *     pDatRif : Data di Riferimento per filtro su Lettere di intento
          *     pTipCon : Tipo Conto : 'C' Clienti, 'F' : Fornitori
          *     pCodCon : Codice Cliente/Fornitore
          *     pArrDic : Array passato per riferimento: conterr� anche i dati letti dalla lettera di intento
          *     
          *     pArrDic[ 1 ]   = Numero Dichiarazione
          *     pArrDic[ 2 ]   = Tipo Operazione
          *     pArrDic[ 3 ]   = Anno Dichiarazione
          *     pArrDic[ 4 ]   = Importo Dichiarazione
          *     pArrDic[ 5 ]   = Data Dichiarazione
          *     pArrDic[ 6 ]   = Importo Utilizzato
          *     pArrDic[ 7 ]   = Tipo conto: Cliente/Fornitore
          *     pArrDic[ 8 ]   = Codice Iva Agevolata
          *     pArrDic[ 9 ]   = Tipo Iva (Agevolata o senza)
          *     pArrDic[ 10 ] = Data Inizio Validit�
          *     pArrDic[ 11 ] = Codice Dichiarazione (Se a clienti = codice Cliente, Se Fornitori= codice progressivo)
          *     pArrDic[ 12 ] = Data Obsolescenza
          this.w_TIPOPE = ArrDic(2)
          this.w_RIFDIC = ArrDic(11)
          this.w_CODIVE = ArrDic(8)
          this.w_IMPDIC = ArrDic(4)
          this.w_IMPUTI = ArrDic(6)
        endif
        * --- Nel caso sia importo definito
        if this.w_TIPOPE = "I" and not(Empty(this.w_CODIVE))
          if this.w_IMPDIC > this.w_IMPUTI
            * --- Importo Definito - Applico dichiarazione di intento specificata
            this.oParentObject.w_MVRIFDIC = this.w_RIFDIC
            this.oParentObject.w_MVCODIVE = this.w_CODIVE
            * --- Assegno w_codice all'imballo, trasporto e incasso 
            this.oParentObject.w_MVIVAINC = this.w_CODIVE
            this.oParentObject.w_MVIVAIMB = this.w_CODIVE
            this.oParentObject.w_MVIVATRA = this.w_CODIVE
          else
            * --- Importo Disponibile della Dichiarazione di Esenzione Esaurito! - Non applico dichiarazione di intento
            this.oParentObject.w_MVRIFDIC = this.w_RIFDIC
            this.oParentObject.w_MVCODIVE = ""
            this.oParentObject.w_MVIVAINC = ""
            this.oParentObject.w_MVIVAIMB = ""
            this.oParentObject.w_MVIVATRA = ""
          endif
        endif
        * --- Nel caso sia operazione pianificata o definizione periodo
        if this.w_TIPOPE = "O" and not(Empty(this.w_CODIVE)) or this.w_TIPOPE = "D" and not(Empty(this.w_CODIVE))
          * --- Nel caso in cui la dichiarazione di intento � presente la passo a MVCODICE 
          this.oParentObject.w_MVRIFDIC = this.w_RIFDIC
          this.oParentObject.w_MVCODIVE = this.w_CODIVE
          * --- Assegno w_codice all'imballo, trasporto e incasso 
          this.oParentObject.w_MVIVAINC = this.w_CODIVE
          this.oParentObject.w_MVIVAIMB = this.w_CODIVE
          this.oParentObject.w_MVIVATRA = this.w_CODIVE
        else
          * --- Nel caso in cui la dichiarazione di intento � vuota passo MVCODICE vuoto
          if Empty(this.w_CODIVE)
            this.oParentObject.w_MVRIFDIC = this.w_RIFDIC
            this.oParentObject.w_MVCODIVE = ""
            this.oParentObject.w_MVIVAINC = ""
            this.oParentObject.w_MVIVAIMB = ""
            this.oParentObject.w_MVIVATRA = ""
          endif
        endif
      case this.w_OPERAZ = "VOCI ANALITICA"
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDFLANAL,TDFLCOMM"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_TIPDOC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDFLANAL,TDFLCOMM;
            from (i_cTable) where;
                TDTIPDOC = this.oParentObject.w_TIPDOC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLANAL = NVL(cp_ToDate(_read_.TDFLANAL),cp_NullValue(_read_.TDFLANAL))
          this.w_FLGCOM = NVL(cp_ToDate(_read_.TDFLCOMM),cp_NullValue(_read_.TDFLCOMM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARVOCRIC"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARVOCRIC;
            from (i_cTable) where;
                ARCODART = this.oParentObject.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_VOCRIC = NVL(cp_ToDate(_read_.ARVOCRIC),cp_NullValue(_read_.ARVOCRIC))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.oParentObject.w_MVVOCCEN = IIF((g_PERCCR="S" AND this.w_FLANAL="S") OR (g_COMM="S" AND this.w_FLGCOM="S"), this.w_VOCRIC, SPACE(15))
        if g_PERCCR="S" AND this.w_FLANAL="S" AND this.oParentObject.w_MVTIPRIG<>"D"
          this.oParentObject.w_MVCODCEN = NVL(this.oParentObject.w_LOCENCOS,SPACE(15))
        endif
    endcase
  endproc


  proc Init(oParentObject,w_OPERAZ)
    this.w_OPERAZ=w_OPERAZ
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='TIP_DOCU'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_OPERAZ"
endproc
