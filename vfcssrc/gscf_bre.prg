* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscf_bre                                                        *
*              Gestione regole controllo flussi                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-10-12                                                      *
* Last revis.: 2013-05-31                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER,pVAR
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscf_bre",oParentObject,m.pOPER,m.pVAR)
return(i_retval)

define class tgscf_bre as StdBatch
  * --- Local variables
  pOPER = space(10)
  pVAR = space(10)
  w_GSCF_MRE = .NULL.
  w_GSCF_MRU = .NULL.
  w_TMPCURS = space(10)
  w_FLNAME = space(30)
  w_TBNAME = space(15)
  w_FLTYPE = space(1)
  w_FLCOMMEN = space(80)
  w_FLLENGHT = 0
  w_FLDECIMA = 0
  w_VAR = space(10)
  w_FILT_EXP = space(254)
  w_CICLA = .f.
  w_KEY_MAST = space(254)
  w_KEY_DETT = space(254)
  w_OK = .f.
  w_LOSTAMP = space(10)
  w_KEYSEL = space(10)
  w_KEYSEL2 = space(10)
  w_NEWSERIAL = space(10)
  w_POSVIR = 0
  w_KEYLIST = space(254)
  * --- WorkFile variables
  XDC_FIELDS_idx=0
  TMPZ_idx=0
  ASSREGUT_idx=0
  LOGCNTFL_idx=0
  REF_MAST_idx=0
  REF_DETT_idx=0
  OPAGGREG_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_GSCF_MRE = this.oParentObject
    do case
      case this.pOPER=="ZOOMFILTER" AND NOT EMPTY(this.pVAR)
        this.w_TMPCURS = SYS(2015)
        this.w_VAR = "w_" + this.pVAR
        if PEMSTATUS(this.w_GSCF_MRE, this.w_VAR, 5)
          * --- Select from XDC_FIELDS
          i_nConn=i_TableProp[this.XDC_FIELDS_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.XDC_FIELDS_idx,2],.t.,this.XDC_FIELDS_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN  from "+i_cTable+" XDC_FIELDS ";
                +" where TBNAME="+cp_ToStrODBC(this.oParentObject.w_RETABMST)+" OR TBNAME="+cp_ToStrODBC(this.oParentObject.w_RETABDTL)+"";
                 ,"_Curs_XDC_FIELDS")
          else
            select FLNAME,FLTYPE,FLLENGHT,FLDECIMA,FLCOMMEN from (i_cTable);
             where TBNAME=this.oParentObject.w_RETABMST OR TBNAME=this.oParentObject.w_RETABDTL;
              into cursor _Curs_XDC_FIELDS
          endif
          if used('_Curs_XDC_FIELDS')
            select _Curs_XDC_FIELDS
            locate for 1=1
            do while not(eof())
            GO BOTTOM
            SELECT "w_"+ALLTRIM(_Curs_XDC_FIELDS.FLNAME) AS FLNAME, _Curs_XDC_FIELDS.FLTYPE, _Curs_XDC_FIELDS.FLLENGHT, _Curs_XDC_FIELDS.FLDECIMA, _Curs_XDC_FIELDS.FLCOMMEN FROM ALIAS() INTO CURSOR (this.w_TMPCURS)
              select _Curs_XDC_FIELDS
              continue
            enddo
            use
          endif
          if used(this.w_TMPCURS) and RecCount(this.w_TMPCURS)>0
            this.w_FILT_EXP = EVAL("This.oParentObject." + this.w_VAR)
            do CP_ADDFILTER WITH this.w_TMPCURS, this.w_FILT_EXP, this.w_VAR, this.w_GSCF_MRE
            this.w_GSCF_MRE.bUpdated  = .t.
            if this.w_VAR = "w_REFLTEXP"
              select (this.w_GSCF_MRE.ctrsname) 
 replace i_srv with iif(i_srv="A", "A", "U")
            else
              this.w_GSCF_MRE.bHeaderUpdated = .t.
            endif
          else
            ah_errormsg("Tabella non definita oppure non trovata nel dizionario.")
          endif
        endif
      case this.pOPER=="ZOOMFIELDS"
        this.w_TBNAME = ""
        this.w_FLNAME = ""
        this.w_FLTYPE = ""
        this.w_FLCOMMEN = ""
        this.w_FLLENGHT = 0
        this.w_FLDECIMA = 0
        * --- Create temporary table TMPZ
        i_nIdx=cp_AddTableDef('TMPZ') && aggiunge la definizione nella lista delle tabelle
        i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
        i_nConn=i_TableProp[this.XDC_FIELDS_idx,3] && recupera la connessione
        i_cTable=cp_SetAzi(i_TableProp[this.XDC_FIELDS_idx,2])
        cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
              +" where TBNAME="+cp_ToStrODBC(this.oParentObject.w_RETABMST)+" OR TBNAME="+cp_ToStrODBC(this.oParentObject.w_RETABDTL)+"";
              )
        this.TMPZ_idx=i_nIdx
        i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
        vx_exec("STD\GSCF_BRE.VZM",this)
        if !EMPTY(NVL(this.w_FLNAME, " "))
          this.oParentObject.w_RE_TABLE = this.w_TBNAME
          this.oParentObject.w_REFLDNAM = this.w_FLNAME
          this.oParentObject.w_RECOMMEN = this.w_FLCOMMEN
        endif
        * --- Drop temporary table TMPZ
        i_nIdx=cp_GetTableDefIdx('TMPZ')
        if i_nIdx<>0
          cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
          cp_RemoveTableDef('TMPZ')
        endif
      case this.pOPER=="UTE_GRU"
        * --- Elimina utenti/gruppi duplicati
        this.w_CICLA = True
        do while this.w_CICLA
          * --- Elimina utenti duplicati
          * --- Delete from ASSREGUT
          i_nConn=i_TableProp[this.ASSREGUT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ASSREGUT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          local _hZBESSKDARL
          _hZBESSKDARL=createobject('prm_container')
          addproperty(_hZBESSKDARL,'w_RUSERIAL',this.oParentObject.w_RESERIAL)
          if i_nConn<>0
            local i_cQueryTable,i_cWhere
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_cWhere=i_cTable+".RUSERIAL = "+i_cQueryTable+".RUSERIAL";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
          
            do vq_exec with 'gscf_but',_hZBESSKDARL,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          this.w_CICLA = not bTrsErr and i_rows>0
        enddo
        this.w_CICLA = True
        do while this.w_CICLA
          * --- Elimina gruppi duplicati
          * --- Delete from ASSREGUT
          i_nConn=i_TableProp[this.ASSREGUT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ASSREGUT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          local _hGWIYSEBUQQ
          _hGWIYSEBUQQ=createobject('prm_container')
          addproperty(_hGWIYSEBUQQ,'w_RUSERIAL',this.oParentObject.w_RESERIAL)
          if i_nConn<>0
            local i_cQueryTable,i_cWhere
            i_cQueryTable=cp_getTempTableName(i_nConn)
            i_cWhere=i_cTable+".RUSERIAL = "+i_cQueryTable+".RUSERIAL";
                  +" and "+i_cTable+".CPROWNUM = "+i_cQueryTable+".CPROWNUM";
          
            do vq_exec with 'gscf_bgr',_hGWIYSEBUQQ,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable
            i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable;
                  +" where exists( select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
            cp_DropTempTable(i_nConn,i_cQueryTable)
          else
            error "not yet implemented!"
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            * --- Error: delete not accepted
            i_Error=MSG_DELETE_ERROR
            return
          endif
          this.w_CICLA = not bTrsErr and i_rows>0
        enddo
      case this.pOPER=="CHKFORM"
        * --- Verifica se il flag chiave corrisponde alla PK della tabella
        this.w_OK = True
        this.w_GSCF_MRU = this.w_GSCF_MRE.GSCF_MRU
        do case
          case this.w_GSCF_MRU.NumRow()=0
            ah_ErrorMsg("Nessun utente o gruppo associato alla regola.")
            this.w_OK = false
          case not empty(this.oParentObject.w_RETABMST) and this.oParentObject.w_TOTPKM=0
            ah_ErrorMsg("Nessuna chiave impostata per la tabella %1.",,,alltrim(this.oParentObject.w_RETABMST))
            this.w_OK = false
          case not empty(this.oParentObject.w_RETABDTL)and this.oParentObject.w_TOTPKD=0
            ah_ErrorMsg("Nessuna chiave impostata per la tabella %1.",,,alltrim(this.oParentObject.w_RETABDTL))
            this.w_OK = false
          case not empty(this.oParentObject.w_RETABMST) or not empty(this.oParentObject.w_RETABDTL)
            * --- Verifica coerenza campo sequenza PK
            this.w_KEY_MAST = IIF(empty(this.oParentObject.w_RETABMST) , "", cp_KeyToSQL(i_dcx.getidxdef( this.oParentObject.w_RETABMST ,1)))
            this.w_KEY_DETT = IIF(empty(this.oParentObject.w_RETABDTL), "", cp_KeyToSQL(i_dcx.getidxdef( this.oParentObject.w_RETABDTL ,1)))
            this.w_TBNAME = alltrim(IIF(empty(this.oParentObject.w_RETABMST), this.oParentObject.w_RETABDTL, this.oParentObject.w_RETABMST))
            this.w_GSCF_MRE.MarkPos()     
            this.w_GSCF_MRE.FirstRow()     
            do while this.w_OK and not this.w_GSCF_MRE.eof_trs()
              this.w_GSCF_MRE.SetRow()     
              if this.oParentObject.w_REKEYREC="S"
                this.w_FLNAME = alltrim(this.oParentObject.w_REFLDNAM)
                if alltrim(this.oParentObject.w_RE_TABLE) == alltrim(this.oParentObject.w_RETABMST)
                  if this.w_FLNAME $ this.w_KEY_MAST
                    this.w_KEY_MAST = StrTran(this.w_KEY_MAST, this.w_FLNAME, "")
                  else
                    this.w_OK = False
                  endif
                else
                  if this.w_FLNAME $ this.w_KEY_DETT
                    this.w_KEY_DETT = StrTran(this.w_KEY_DETT, this.w_FLNAME , "")
                  else
                    this.w_OK = False
                  endif
                endif
                * --- Sequenza campi per bottone postit
                if this.w_TBNAME=alltrim(this.oParentObject.w_RE_TABLE)
                  if empty(this.w_KEYSEL)
                    this.w_KEYSEL = this.w_FLNAME
                  else
                    this.w_KEYSEL = this.w_KEYSEL + "," + this.w_FLNAME
                  endif
                endif
              endif
              this.w_GSCF_MRE.NextRow()     
            enddo
            this.w_GSCF_MRE.RePos()     
            this.w_KEY_MAST = STRTRAN(this.w_KEY_MAST,",", "")
            this.w_KEY_DETT = STRTRAN(this.w_KEY_DETT,",", "")
            this.w_OK = this.w_OK and empty(this.w_KEY_MAST) and empty(this.w_KEY_DETT)
            if this.w_OK
              if this.oParentObject.w_REPSTBTN="S"
                if "," $ this.w_KEYSEL
                  if Empty(this.oParentObject.w_REKEYSEL)
                    ah_ErrorMsg("Campo Sequenza PK obbligatorio se flag Bottone su post-IN attivo")
                    this.w_OK = false
                  else
                    * --- Se w_REKEYGES="S" il campo Sequenza PK � impostato dalla gestione, quindi � corretto
                    if NVL(this.oParentObject.w_REKEYGES," ")<>"S"
                      this.w_KEYSEL2 = alltrim(this.oParentObject.w_REKEYSEL)
                      if LEN(this.w_KEYSEL)<>LEN(this.w_KEYSEL2)
                        ah_ErrorMsg("Valore su campo <Sequenza PK> non valido")
                        this.w_OK = false
                      else
                        do while not empty(this.w_KEYSEL2)
                          this.w_FLLENGHT = at(",", this.w_KEYSEL2)
                          if this.w_FLLENGHT=0
                            this.w_VAR = this.w_KEYSEL2
                            this.w_KEYSEL2 = ""
                          else
                            this.w_VAR = left(this.w_KEYSEL2, this.w_FLLENGHT-1)
                            this.w_KEYSEL2 = substr(this.w_KEYSEL2, this.w_FLLENGHT+1)
                          endif
                          this.w_KEYSEL = strtran(this.w_KEYSEL, this.w_VAR, "")
                        enddo
                        this.w_KEYSEL = strtran(this.w_KEYSEL, ",", "")
                        if NOT EMPTY(this.w_KEYSEL)
                          ah_ErrorMsg("Valore su campo <Sequenza PK> non valido")
                          this.w_OK = false
                        endif
                      endif
                    endif
                  endif
                else
                  if Empty(this.oParentObject.w_REKEYSEL)
                    this.oParentObject.w_REKEYSEL = this.w_KEYSEL
                    this.w_GSCF_MRE.bHeaderUpdated = .t.
                  else
                    if this.oParentObject.w_REKEYSEL<>this.w_KEYSEL
                      ah_ErrorMsg("Valore su campo <Sequenza PK> non valido")
                      this.w_OK = false
                    endif
                  endif
                endif
              endif
            else
              if ah_Yesno("Flag chiave differente da chiave primaria tabella. Si vuole continuare?")
                if this.oParentObject.w_REPSTBTN="S"
                  ah_ErrorMsg("Flag <Bottone su post-IN> disattivato")
                  this.oParentObject.w_REPSTBTN = "N"
                  this.w_GSCF_MRE.bHeaderUpdated = .t.
                endif
                this.w_OK = True
              endif
            endif
            i_retcode = 'stop'
            i_retval = this.w_OK
            return
        endcase
        i_retcode = 'stop'
        i_retval = this.w_OK
        return
      case this.pOPER=="CHIAVE"
        * --- Imposta il flag chiave in relazione alla PK della tabella
        if not empty(this.oParentObject.w_RETABMST) or not empty(this.oParentObject.w_RETABDTL)
          this.w_KEY_MAST = IIF(empty(this.oParentObject.w_RETABMST) , "", cp_KeyToSQL(i_dcx.getidxdef( this.oParentObject.w_RETABMST ,1)))
          this.w_KEY_DETT = IIF(empty(this.oParentObject.w_RETABDTL), "", cp_KeyToSQL(i_dcx.getidxdef( this.oParentObject.w_RETABDTL ,1)))
          this.w_GSCF_MRE.MarkPos()     
          this.w_GSCF_MRE.FirstRow()     
          do while not this.w_GSCF_MRE.eof_trs()
            this.w_GSCF_MRE.SetRow()     
            this.w_GSCF_MRE.SaveDependsOn()     
            this.w_FLNAME = alltrim(this.oParentObject.w_REFLDNAM)
            if alltrim(this.oParentObject.w_RE_TABLE) = alltrim(this.oParentObject.w_RETABMST) and this.w_FLNAME $ this.w_KEY_MAST or ; 
 alltrim(this.oParentObject.w_RE_TABLE) = alltrim(this.oParentObject.w_RETABDTL) and this.w_FLNAME $ this.w_KEY_DETT
              if this.oParentObject.w_REKEYREC="N"
                this.w_GSCF_MRE.w_REKEYREC = "S"
                this.w_GSCF_MRE.w_REMONMOD = "S"
              endif
              if alltrim(this.oParentObject.w_RE_TABLE) = alltrim(this.oParentObject.w_RETABMST) and this.w_FLNAME $ this.w_KEY_MAST
                this.w_KEY_MAST = StrTran(this.w_KEY_MAST, this.w_FLNAME, "")
              endif
              if alltrim(this.oParentObject.w_RE_TABLE) = alltrim(this.oParentObject.w_RETABDTL) and this.w_FLNAME $ this.w_KEY_DETT
                this.w_KEY_DETT = StrTran(this.w_KEY_DETT, this.w_FLNAME, "")
              endif
            else
              if this.oParentObject.w_REKEYREC="S"
                this.w_GSCF_MRE.w_REKEYREC = "N"
              endif
            endif
            this.w_GSCF_MRE.mCalc(.T.)     
            this.w_GSCF_MRE.SaveRow()     
            this.w_GSCF_MRE.NextRow()     
          enddo
          if not empty(StrTran(this.w_KEY_MAST, ",", ""))
            * --- Aggiungo i campi chiave mancanti sulle righe
            this.w_TBNAME = this.oParentObject.w_RETABMST
            this.w_KEYLIST = this.w_KEY_MAST+","
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          if not empty(StrTran(this.w_KEY_DETT, ",", ""))
            * --- Aggiungo i campi chiave mancanti sulle righe
            this.w_TBNAME = this.oParentObject.w_RETABDTL
            this.w_KEYLIST = this.w_KEY_DETT+","
            this.Pag2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          Select (this.w_GSCF_MRE.cTrsName)
          * --- TOTALIZZATORI
          SUM(T_NPKM) TO this.w_GSCF_MRE.w_TOTPKM
          SUM(T_NPKD) TO this.w_GSCF_MRE.w_TOTPKD
          this.w_GSCF_MRE.RePos()     
        endif
      case this.pOPER=="DELRST" OR this.pOPER=="DELST"
        * --- Cancella regola sotto transazione
        if this.pOPER=="DELST"
          * --- Read from LOGCNTFL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.LOGCNTFL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LOGCNTFL_idx,2],.t.,this.LOGCNTFL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LOSTAMP"+;
              " from "+i_cTable+" LOGCNTFL where ";
                  +"LOSERIAL = "+cp_ToStrODBC(this.oParentObject.w_RESERIAL);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LOSTAMP;
              from (i_cTable) where;
                  LOSERIAL = this.oParentObject.w_RESERIAL;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LOSTAMP = NVL(cp_ToDate(_read_.LOSTAMP),cp_NullValue(_read_.LOSTAMP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        else
          * --- Read from LOGCNTFL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.LOGCNTFL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.LOGCNTFL_idx,2],.t.,this.LOGCNTFL_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "LOSTAMP"+;
              " from "+i_cTable+" LOGCNTFL where ";
                  +"LOSERIAL = "+cp_ToStrODBC(this.oParentObject.w_RESERIAL);
                  +" and LOROWLOG = "+cp_ToStrODBC(this.oParentObject.w_CPROWNUM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              LOSTAMP;
              from (i_cTable) where;
                  LOSERIAL = this.oParentObject.w_RESERIAL;
                  and LOROWLOG = this.oParentObject.w_CPROWNUM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LOSTAMP = NVL(cp_ToDate(_read_.LOSTAMP),cp_NullValue(_read_.LOSTAMP))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        if not empty(this.w_LOSTAMP)
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg="Impossibile eliminare/modificare la regola perch� utilizzata nel LOG"
        endif
      case this.pOPER=="TABMST" OR this.pOPER=="TABDTL"
        * --- Elimina le righe associate
        if this.pOPER=="TABMST"
          this.w_TBNAME = this.w_GSCF_MRE.o_RETABMST
        else
          this.w_TBNAME = this.w_GSCF_MRE.o_RETABDTL
        endif
        if not empty(this.w_TBNAME)
          this.w_GSCF_MRE.FirstRow()     
          do while not this.w_GSCF_MRE.eof_trs()
            this.w_GSCF_MRE.SetRow()     
            this.w_FLNAME = alltrim(this.oParentObject.w_REFLDNAM)
            if alltrim(this.oParentObject.w_RE_TABLE) = alltrim(this.w_TBNAME)
              this.w_GSCF_MRE.DeleteRow()     
            endif
            this.w_GSCF_MRE.NextRow()     
          enddo
          this.w_GSCF_MRE.FirstRow()     
          this.w_GSCF_MRE.SetRow()     
          this.w_GSCF_MRE.Refresh()     
        endif
      case this.pOPER=="KEYSEL"
        * --- Sequenza campi per bottone postit
        this.oParentObject.w_REKEYSEL = ""
        this.w_TBNAME = alltrim(IIF(empty(this.oParentObject.w_RETABMST), this.oParentObject.w_RETABDTL, this.oParentObject.w_RETABMST))
        this.w_GSCF_MRE.MarkPos()     
        this.w_GSCF_MRE.Exec_select("ckey", "t_REFLDNAM", "alltrim(t_RE_TABLE)=='"+this.w_TBNAME+"' AND t_REKEYREC=1", "t_CPROWORD")     
        this.w_GSCF_MRE.RePos()     
        Select ckey
        scan
        this.w_FLNAME = alltrim(t_REFLDNAM)
        if empty(this.oParentObject.w_REKEYSEL)
          this.oParentObject.w_REKEYSEL = this.w_FLNAME
        else
          this.oParentObject.w_REKEYSEL = this.oParentObject.w_REKEYSEL + "," + this.w_FLNAME
        endif
        endscan
        use in ckey
        if "," $ this.oParentObject.w_REKEYSEL
          ah_errormsg("La sequenza � stata generata rispettando l'ordinamento delle righe, verifcare che sia coerente con la relativa gestione.")
        endif
      case this.pOPER=="DUPLICA"
        * --- Duplica la regola selezionata
        if not empty(this.oParentObject.w_RESERIAL)
          * --- Try
          local bErr_053A7F40
          bErr_053A7F40=bTrsErr
          this.Try_053A7F40()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_errormsg("Operazione non eseguita: %1",,, i_trserr)
          endif
          bTrsErr=bTrsErr or bErr_053A7F40
          * --- End
          * --- Try
          local bErr_053A7AF0
          bErr_053A7AF0=bTrsErr
          this.Try_053A7AF0()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
          endif
          bTrsErr=bTrsErr or bErr_053A7AF0
          * --- End
        endif
    endcase
  endproc
  proc Try_053A7F40()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Try
    local bErr_05399F30
    bErr_05399F30=bTrsErr
    this.Try_05399F30()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error="Impossibile duplicare dati di testata"
      return
    endif
    bTrsErr=bTrsErr or bErr_05399F30
    * --- End
    * --- Try
    local bErr_05399690
    bErr_05399690=bTrsErr
    this.Try_05399690()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error="Impossibile duplicare righe di dettaglio"
      return
    endif
    bTrsErr=bTrsErr or bErr_05399690
    * --- End
    * --- Try
    local bErr_0539A4D0
    bErr_0539A4D0=bTrsErr
    this.Try_0539A4D0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error="Impossibile duplicare righe dettaglio utenti/gruppi"
      return
    endif
    bTrsErr=bTrsErr or bErr_0539A4D0
    * --- End
    * --- Try
    local bErr_04FFBE60
    bErr_04FFBE60=bTrsErr
    this.Try_04FFBE60()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Raise
      i_Error="Impossibile duplicare righe notifica utenti"
      return
    endif
    bTrsErr=bTrsErr or bErr_04FFBE60
    * --- End
    * --- commit
    cp_EndTrs(.t.)
    ah_errormsg("Operazione completata con successo")
    this.w_GSCF_MRE.ecpFilter()     
    this.oParentObject.w_RESERIAL = this.w_NEWSERIAL
    this.w_GSCF_MRE.ecpSave()     
    return
  proc Try_053A7AF0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_05399F30()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Create temporary table TMPZ
    i_nIdx=cp_AddTableDef('TMPZ') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.REF_MAST_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.REF_MAST_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          +" where RESERIAL="+cp_ToStrODBC(this.oParentObject.w_RESERIAL)+"";
          )
    this.TMPZ_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    this.w_NEWSERIAL = CP_GETPROG("REF_MAST","SEREG", this.oParentObject.w_RESERIAL, i_codazi)
    * --- Write into TMPZ
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZ_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"RESERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_NEWSERIAL),'TMPZ','RESERIAL');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          RESERIAL = this.w_NEWSERIAL;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Insert into REF_MAST
    i_nConn=i_TableProp[this.REF_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.REF_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.REF_MAST_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMPZ
    i_nIdx=cp_GetTableDefIdx('TMPZ')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPZ')
    endif
    return
  proc Try_05399690()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Create temporary table TMPZ
    i_nIdx=cp_AddTableDef('TMPZ') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.REF_DETT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.REF_DETT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          +" where RESERIAL="+cp_ToStrODBC(this.oParentObject.w_RESERIAL)+"";
          )
    this.TMPZ_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMPZ
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZ_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"RESERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_NEWSERIAL),'TMPZ','RESERIAL');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          RESERIAL = this.w_NEWSERIAL;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Insert into REF_DETT
    i_nConn=i_TableProp[this.REF_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.REF_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.REF_DETT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMPZ
    i_nIdx=cp_GetTableDefIdx('TMPZ')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPZ')
    endif
    return
  proc Try_0539A4D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Create temporary table TMPZ
    i_nIdx=cp_AddTableDef('TMPZ') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.ASSREGUT_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.ASSREGUT_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          +" where RUSERIAL="+cp_ToStrODBC(this.oParentObject.w_RESERIAL)+"";
          )
    this.TMPZ_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMPZ
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZ_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"RUSERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_NEWSERIAL),'TMPZ','RUSERIAL');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          RUSERIAL = this.w_NEWSERIAL;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Insert into ASSREGUT
    i_nConn=i_TableProp[this.ASSREGUT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ASSREGUT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.ASSREGUT_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMPZ
    i_nIdx=cp_GetTableDefIdx('TMPZ')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPZ')
    endif
    return
  proc Try_04FFBE60()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Create temporary table TMPZ
    i_nIdx=cp_AddTableDef('TMPZ') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    i_nConn=i_TableProp[this.OPAGGREG_idx,3] && recupera la connessione
    i_cTable=cp_SetAzi(i_TableProp[this.OPAGGREG_idx,2])
    cp_CreateTempTable(i_nConn,i_cTempTable,"*"," from "+i_cTable;
          +" where OASERIAL="+cp_ToStrODBC(this.oParentObject.w_RESERIAL)+"";
          )
    this.TMPZ_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMPZ
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPZ_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"OASERIAL ="+cp_NullLink(cp_ToStrODBC(this.w_NEWSERIAL),'TMPZ','OASERIAL');
          +i_ccchkf ;
      +" where ";
          +"1 = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          OASERIAL = this.w_NEWSERIAL;
          &i_ccchkf. ;
       where;
          1 = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Insert into OPAGGREG
    i_nConn=i_TableProp[this.OPAGGREG_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.OPAGGREG_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_cTempTable=cp_SetAzi(i_TableProp[this.TMPZ_idx,2])
      i_Rows=cp_InsertIntoSQL(i_nConn,i_cTable,"*"," from "+i_cTempTable,this.OPAGGREG_idx)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Drop temporary table TMPZ
    i_nIdx=cp_GetTableDefIdx('TMPZ')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPZ')
    endif
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Carico le righe della chiave
    do while "," $ this.w_KEYLIST
      this.w_POSVIR = AT(",", this.w_KEYLIST)
      this.w_FLNAME = LEFT(this.w_KEYLIST, this.w_POSVIR-1)
      this.w_KEYLIST = SUBSTR(this.w_KEYLIST, this.w_POSVIR+1)
      if not empty(this.w_FLNAME)
        this.w_GSCF_MRE.AddRow()     
        this.w_GSCF_MRE.w_RE_TABLE = this.w_TBNAME
        this.w_GSCF_MRE.w_MSTDET = IIF(this.w_TBNAME=this.oParentObject.w_RETABDTL, "D", "M")
        this.w_GSCF_MRE.o_MSTDET = this.w_GSCF_MRE.w_MSTDET
        this.w_GSCF_MRE.w_REFLDNAM = this.w_FLNAME
        * --- Read from XDC_FIELDS
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.XDC_FIELDS_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.XDC_FIELDS_idx,2],.t.,this.XDC_FIELDS_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "FLCOMMEN"+;
            " from "+i_cTable+" XDC_FIELDS where ";
                +"TBNAME = "+cp_ToStrODBC(this.w_TBNAME);
                +" and FLNAME = "+cp_ToStrODBC(this.w_FLNAME);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            FLCOMMEN;
            from (i_cTable) where;
                TBNAME = this.w_TBNAME;
                and FLNAME = this.w_FLNAME;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_GSCF_MRE.w_REFLDCOM = NVL(cp_ToDate(_read_.FLCOMMEN),cp_NullValue(_read_.FLCOMMEN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_GSCF_MRE.w_REKEYREC = "S"
        this.w_GSCF_MRE.w_RERIFREC = "N"
        this.w_GSCF_MRE.w_REMONINS = "S"
        this.w_GSCF_MRE.w_REMONMOD = "N"
        this.w_GSCF_MRE.w_REMONDEL = "S"
        this.w_GSCF_MRE.w_REBLKINS = "N"
        this.w_GSCF_MRE.w_REOBBINS = "N"
        this.w_GSCF_MRE.w_NPKM = IIF(ALLTRIM(this.w_TBNAME)==ALLTRIM(this.w_GSCF_MRE.w_RETABMST), 1, 0)
        this.w_GSCF_MRE.w_NPKD = IIF(ALLTRIM(this.w_TBNAME)==ALLTRIM(this.w_GSCF_MRE.w_RETABDTL), 1, 0)
        this.w_GSCF_MRE.mCalc(.t.)     
        this.w_GSCF_MRE.SaveRow()     
      endif
    enddo
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOPER,pVAR)
    this.pOPER=pOPER
    this.pVAR=pVAR
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,7)]
    this.cWorkTables[1]='XDC_FIELDS'
    this.cWorkTables[2]='*TMPZ'
    this.cWorkTables[3]='ASSREGUT'
    this.cWorkTables[4]='LOGCNTFL'
    this.cWorkTables[5]='REF_MAST'
    this.cWorkTables[6]='REF_DETT'
    this.cWorkTables[7]='OPAGGREG'
    return(this.OpenAllTables(7))

  proc CloseCursors()
    if used('_Curs_XDC_FIELDS')
      use in _Curs_XDC_FIELDS
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER,pVAR"
endproc
