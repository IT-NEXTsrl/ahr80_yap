* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bcu                                                        *
*              Contabilizzazione incassi corr                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_396]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-11                                                      *
* Last revis.: 2013-12-12                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bcu",oParentObject)
return(i_retval)

define class tgscg_bcu as StdBatch
  * --- Local variables
  w_oERRORLOG = .NULL.
  w_OSERIAL = space(10)
  w_OCODCON = space(15)
  w_OTIPDOC = space(5)
  w_ORIDOC = space(10)
  w_ROWINC = 0
  w_PNSERIAL = space(10)
  w_PNCODCAU = space(5)
  w_PNVALNAZ = space(3)
  w_PNCOMIVA = ctod("  /  /  ")
  w_CCNUMREG = 0
  w_PNNUMRER = 0
  w_PNTIPREG = space(1)
  w_PNCODVAL = space(3)
  w_PNDESSUP = space(50)
  w_DATBLO = ctod("  /  /  ")
  w_PNNUMPRO = 0
  w_PNNUMREG = 0
  w_PNCAOVAL = 0
  w_PNCODUTE = 0
  w_PNPRD = space(2)
  w_PNTOTDOC = 0
  w_AINUMREG = 0
  w_PNTIPCLF = space(1)
  w_PNDATREG = ctod("  /  /  ")
  w_PNPRP = space(2)
  w_CPROWNUM = 0
  w_PNTIPDOC = space(2)
  w_PNCODCLF = space(15)
  w_PNALFDOC = space(10)
  w_APPOK = .f.
  w_CPROWORD = 0
  w_PNALFPRO = space(10)
  w_PNTIPCLF = space(1)
  w_PNDATDOC = ctod("  /  /  ")
  w_PNPRG = space(8)
  w_PNCODESE = space(4)
  w_PNFLPROV = space(1)
  w_PNFLREGI = space(1)
  w_ARRSUP = 0
  w_PNCOMPET = space(4)
  w_PNNUMDOC = 0
  w_PNCODPAG = space(5)
  w_FLOMAG = space(1)
  w_SEZCLF = space(1)
  w_FLPDOC = space(1)
  w_APPO = space(10)
  w_PNFLPART = space(1)
  w_CONVEA = space(15)
  w_APPSEZ = space(1)
  w_FLPPRO = space(1)
  w_APPO1 = 0
  w_CODIVA = space(5)
  w_CONOMA = space(15)
  w_CODVAC = space(3)
  w_PNIMPAVE = 0
  w_APPO2 = 0
  w_PERIVA = 0
  w_CONOMI = space(15)
  w_TOTDAR = 0
  w_PNIMPDAR = 0
  w_CONNAC = space(15)
  w_TOTAVE = 0
  w_PNTIPCON = space(1)
  w_PARTSN = space(1)
  w_VALMAG = 0
  w_DECTOT = 0
  w_APPVAL = 0
  w_PNCODCON = space(15)
  w_PNANNDOC = space(4)
  w_CONTRO = space(15)
  w_DECVAC = 0
  w_CCCONINT = space(15)
  w_CCCONIVA = space(15)
  w_PNANNPRO = space(4)
  w_CONIND = space(15)
  w_CAONAZ = 0
  w_DECTOP = 0
  w_ATTIVI = space(5)
  w_IVFLOMAG = space(1)
  w_PTNUMPAR = space(31)
  w_PNIMPIND = 0
  w_DATIVA = ctod("  /  /  ")
  AI = space(10)
  w_IVCODIVA = space(5)
  w_PTDATSCA = ctod("  /  /  ")
  w_FLIND = .f.
  w_MAXCOMIVA = ctod("  /  /  ")
  MES = space(10)
  w_PTFLSOSP = space(1)
  w_TROV = .f.
  w_PNINICOM = ctod("  /  /  ")
  w_CODAZI = space(5)
  w_IVIMPONI = 0
  w_PTMODPAG = space(10)
  w_INICOMD = ctod("  /  /  ")
  w_PNFINCOM = ctod("  /  /  ")
  w_ASSEG = .f.
  w_IVIMPIVA = 0
  w_PTTOTIMP = 0
  w_FINCOMD = ctod("  /  /  ")
  w_ABDIFF = 0
  w_REGOK = .f.
  w_FLERR = .f.
  w_PTBANNOS = space(15)
  w_NUDOC = 0
  w_ROWNUM = 0
  w_INICOM = ctod("  /  /  ")
  w_CONTA = 0
  w_PNFLABAN = space(6)
  w_OKDOC = 0
  w_RESTO = 0
  w_FINCOM = ctod("  /  /  ")
  w_STALIG = ctod("  /  /  ")
  w_MAXDAT = ctod("  /  /  ")
  w_ROWINI = 0
  w_PNFLZERO = space(1)
  w_MESS = space(90)
  w_MESS1 = space(10)
  w_IVADET = 0
  w_SALINI = space(1)
  w_TROPAR = .f.
  w_DATOBSO = ctod("  /  /  ")
  w_PTBANAPP = space(10)
  w_IMPCAF = 0
  w_PARFOR = 0
  w_TESVAL = 0
  w_TIPO = space(1)
  w_IVTIPREG = space(1)
  w_IMPABF = 0
  w_PTCODVAL = space(3)
  w_PTNUMDOC = 0
  w_STATUS = space(1)
  w_VABENE = .f.
  w_CODIVE = space(5)
  w_PTCAOAPE = 0
  w_PTALFDOC = space(10)
  w_ROWACC = 0
  w_FLBESE = space(1)
  w_IMPCLI = 0
  w_PTDATAPE = ctod("  /  /  ")
  w_PTDATDOC = ctod("  /  /  ")
  w_PTCAOVAL = 0
  w_TOTCAS = 0
  w_TOTPAR = 0
  w_MVDATDOC = ctod("  /  /  ")
  w_MVCAOVAL = 0
  w_MVCODVAL = space(3)
  w_TOTVAL = 0
  w_VADTOBSO = ctod("  /  /  ")
  w_CODVAP = space(3)
  w_CAOVAP = 0
  w_DIFCON = 0
  w_APPO3 = 0
  w_CONCLI1 = space(15)
  w_CONCAF1 = space(15)
  w_CONABF1 = space(15)
  w_CONIVF1 = space(15)
  w_CATCLG1 = space(5)
  w_PAGRIC1 = space(5)
  w_APPTIPDOC = space(5)
  w_CONCAFC = space(15)
  w_CONABFC = space(15)
  w_CONIVFC = space(15)
  w_CONCLIC = space(15)
  w_IMPABB = 0
  w_MVTIPDOC = space(5)
  w_MVTIPDOC = space(5)
  w_TIPDOCU = space(5)
  w_MESS_ERR = space(200)
  w_CODESE = space(4)
  w_NUMDOC = 0
  w_ALFDOC = space(10)
  w_CCDESRIG = space(254)
  w_CCDESSUP = space(254)
  w_LNUMDOC = 0
  w_LALFDOC = space(10)
  w_LDATDOC = ctod("  /  /  ")
  w_PNDESRIG = space(50)
  w_IMPON = 0
  w_IMPIV = 0
  w_IVASOSP = 0
  * --- WorkFile variables
  CAU_CONT_idx=0
  PNT_MAST_idx=0
  INC_CORR_idx=0
  VALUTE_idx=0
  PNT_DETT_idx=0
  SALDICON_idx=0
  VOCIIVA_idx=0
  PNT_IVA_idx=0
  CONTI_idx=0
  PAR_TITE_idx=0
  AZIENDA_idx=0
  ATTIDETT_idx=0
  ESERCIZI_idx=0
  RIFI_CADO_idx=0
  DOC_RATE_idx=0
  INCDCORR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Contabilizzazione Incassi Corrispettivi (da GSCG_KCR)
    * --- Parametri dalla Maschera
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- AI[] - Vettore Castelletto IVA : 1^Indice Num.Aliquota
    * --- 2^Indice : 1 =Flag Omaggio
    * --- 2 =Codice IVA
    * --- 3 =Imponibile
    * --- 4 =Imposta
    * --- 5 =%IVA
    * --- 6 =%Indetraibile
    DIMENSION AI[6, 5], MES[19],ARPARAM[12,2]
    * --- Carica i Documenti da Contabilizzare
    ah_Msg("Ricerca incassi da contabilizzare...",.T.)
    vq_exec("query\GSVE_QCZ.VQR",this,"CONTADOC")
    if USED("CONTADOC")
      * --- Inizio Aggiornamento vero e proprio
      this.w_NUDOC = 0
      this.w_OKDOC = 0
      if RECCOUNT("CONTADOC") > 0
        ah_Msg("Inizio fase di contabilizzazione...",.T.)
        * --- Fase di Blocco
        * --- 1 - Blocco la Prima Nota con data = Max (data reg.)
        * --- 2 - Blocco tutti i Registri Iva con data = Max( Data reg)
        this.w_CODAZI = i_CODAZI
        * --- Calcolo il Max Data reg.
        if this.oParentObject.w_FLDATC="U"
          this.w_MAXDAT = this.oParentObject.w_DATCON
        else
          CALCULATE MAX(INDATDOC) FOR NOT EMPTY(NVL(INSERIAL," ")) AND ;
          NOT EMPTY(NVL(INCODCAU," ")) TO this.w_MAXDAT
        endif
        this.w_MAXCOMIVA = this.w_MAXDAT
        this.w_VABENE = .T.
        this.w_ASSEG = .F.
        * --- Try
        local bErr_03EC5300
        bErr_03EC5300=bTrsErr
        this.Try_03EC5300()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("%1",,"",i_errmsg)
          this.Pag6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_03EC5300
        * --- End
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Rimuovo i blocchi
        * --- Stampa Libro giornale e Registri Iva
        * --- Try
        local bErr_03D71540
        bErr_03D71540=bTrsErr
        this.Try_03D71540()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
          ah_ErrorMsg("Impossibile rimuovere i blocchi. Rimuoverli dai dati azienda",,"")
        endif
        bTrsErr=bTrsErr or bErr_03D71540
        * --- End
        ah_ErrorMsg("Operazione completata%0n. %1 documenti contabilizzati su %2 documenti da contabilizzare",,"",ALLTRIM(STR(this.w_OKDOC)),ALLTRIM(STR(this.w_NUDOC)))
        * --- LOG Errori
        if this.w_NUDOC>0 AND this.w_OKDOC<>this.w_NUDOC
          this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati")     
        endif
      else
        ah_ErrorMsg("Per l'intervallo selezionato%0non esistono documenti da contabilizzare",,"")
      endif
      * --- Chiudo la Maschera
      This.bUpdateParentObject=.f.
      This.oParentObject.bUpdated=.f.
      This.oParentObject.ecpQuit()
      this.Pag6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc
  proc Try_03EC5300()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Leggo la data dei blocco e la data stampa del libro giornale
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO,AZSTALIG"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO,AZSTALIG;
        from (i_cTable) where;
            AZCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if empty(NVL(this.w_datblo,cp_CharToDate("  -  -  "))) 
      * --- Inserisce <Blocco> per Primanota
      * --- Write into AZIENDA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_MAXDAT),'AZIENDA','AZDATBLO');
            +i_ccchkf ;
        +" where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               )
      else
        update (i_cTable) set;
            AZDATBLO = this.w_MAXDAT;
            &i_ccchkf. ;
         where;
            AZCODAZI = this.w_CODAZI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- UN altro utente ha impostato il blocco - controllo concorrenza
      this.w_VABENE = .F.
      * --- Raise
      i_Error=ah_MsgFormat("Prima nota bloccata, verificare semaforo bollati in dati azienda. Impossibile contabilizzare")
      return
    endif
    * --- Tento il blocco dei Registri IVA
    * --- se g_catazi vuoto esci e segnala nessuna attivit�
    if empty(nvl(g_catazi,"")) AND this.w_VABENE
      this.w_VABENE = .F.
      * --- Raise
      i_Error="Nessuna attivit� presente. Inserirne una per proseguire"
      return
    endif
    * --- Se il numero e il tipo del registro non ha attivit� significa che non � stato ancora stampato
    * --- infatti la stampa IVA crea in automatico una voce nell'attivit�, questo non esclude che qualche utentedecida di stampare i  regi
    * --- durante la contabilizzaione => associo il registro all'attivit� di default
    if this.w_VABENE
      select INDATDOC AS MVDATREG, CCTIPREG, CCNUMREG FROM ;
      CONTADOC GROUP BY CCTIPREG,CCNUMREG ;
      WHERE NOT EMPTY(NVL(INSERIAL," ")) AND NOT EMPTY(NVL(INCODCAU," ")) ;
      into cursor reg_iva
      SELECT reg_iva
      go top
      scan
      this.w_MAXDAT = MVDATREG
      this.w_PNNUMREG = NVL(CCNUMREG, 1)
      this.w_PNTIPREG = NVL(CCTIPREG, "N")
      * --- Se ha un tipo IVA buono
      if this.w_PNTIPREG<>"N"
        this.w_CONTA = 0
        * --- Select from ATTIDETT
        i_nConn=i_TableProp[this.ATTIDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select ATDATBLO,ATCODATT  from "+i_cTable+" ATTIDETT ";
              +" where ATNUMREG="+cp_ToStrODBC(this.w_PNNUMREG)+" AND ATTIPREG="+cp_ToStrODBC(this.w_PNTIPREG)+"";
               ,"_Curs_ATTIDETT")
        else
          select ATDATBLO,ATCODATT from (i_cTable);
           where ATNUMREG=this.w_PNNUMREG AND ATTIPREG=this.w_PNTIPREG;
            into cursor _Curs_ATTIDETT
        endif
        if used('_Curs_ATTIDETT')
          select _Curs_ATTIDETT
          locate for 1=1
          do while not(eof())
          this.w_DATBLO = NVL(_Curs_ATTIDETT.ATDATBLO, cp_CharToDate("  -  -  "))
          this.w_ATTIVI = NVL(_Curs_ATTIDETT.ATCODATT, cp_CharToDate("  -  -  "))
          this.w_CONTA = this.w_CONTA+1
            select _Curs_ATTIDETT
            continue
          enddo
          use
        endif
        if this.w_conta>1
          * --- Raise
          i_Error=ah_MsgFormat("L'attivit� %1 ha due registri IVA del solito tipo e numero",this.w_ATTIVI)
          return
          this.w_VABENE = .F.
        endif
        this.w_CONTA = 0
        if empty(NVL(this.w_datblo,cp_CharToDate("  -  -  "))) and this.w_VABENE
          if EMPTY(NVL(this.w_ATTIVI,""))
            * --- Se l'attivit� non la trovo per default � quella principale
            this.w_ATTIVI = g_CATAZI
            * --- Insert into ATTIDETT
            i_nConn=i_TableProp[this.ATTIDETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_ccchkf=''
            i_ccchkv=''
            this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ATTIDETT_idx,i_nConn)
            if i_nConn<>0
              i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                          " ("+"ATCODATT"+",ATNUMREG"+",ATTIPREG"+i_ccchkf+") values ("+;
              cp_NullLink(cp_ToStrODBC(this.w_ATTIVI),'ATTIDETT','ATCODATT');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'ATTIDETT','ATNUMREG');
              +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'ATTIDETT','ATTIPREG');
                   +i_ccchkv+")")
            else
              cp_CheckDeletedKey(i_cTable,0,'ATCODATT',this.w_ATTIVI,'ATNUMREG',this.w_PNNUMREG,'ATTIPREG',this.w_PNTIPREG)
              insert into (i_cTable) (ATCODATT,ATNUMREG,ATTIPREG &i_ccchkf. );
                 values (;
                   this.w_ATTIVI;
                   ,this.w_PNNUMREG;
                   ,this.w_PNTIPREG;
                   &i_ccchkv. )
              i_Rows=iif(bTrsErr,0,1)
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if i_Rows<0 or bTrsErr
              * --- Error: insert not accepted
              i_Error=MSG_INSERT_ERROR
              return
            endif
            * --- sotto tranzazione � meglio non mettere messaggi per avvisare l'utente di questa impostazione
            this.w_ASSEG = .T.
          endif
          * --- Inserisce <Blocco> per Primanota
          * --- Write into ATTIDETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_MAXDAT),'ATTIDETT','ATDATBLO');
                +i_ccchkf ;
            +" where ";
                +"ATCODATT = "+cp_ToStrODBC(this.w_ATTIVI);
                +" and ATNUMREG = "+cp_ToStrODBC(this.w_PNNUMREG);
                +" and ATTIPREG = "+cp_ToStrODBC(this.w_PNTIPREG);
                   )
          else
            update (i_cTable) set;
                ATDATBLO = this.w_MAXDAT;
                &i_ccchkf. ;
             where;
                ATCODATT = this.w_ATTIVI;
                and ATNUMREG = this.w_PNNUMREG;
                and ATTIPREG = this.w_PNTIPREG;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- UN ALTRO STA STAMPANDO REGISTRI IVA - controllo concorrenza
          this.w_VABENE = .F.
          * --- Raise
          i_Error=ah_MsgFormat("Contabilizzazione annullata. Blocco stampa registri IVA")
          return
        endif
      endif
      endscan
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_03D71540()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = this.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    select reg_iva
    scan for NVL(cctipreg,"")<>"N"
    this.w_PNNUMREG = NVL(CCNUMREG, 1)
    this.w_PNTIPREG = NVL(CCTIPREG, "N")
    * --- Write into ATTIDETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ATTIDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'ATTIDETT','ATDATBLO');
          +i_ccchkf ;
      +" where ";
          +"ATCODATT = "+cp_ToStrODBC(this.w_ATTIVI);
          +" and ATNUMREG = "+cp_ToStrODBC(this.w_PNNUMREG);
          +" and ATTIPREG = "+cp_ToStrODBC(this.w_PNTIPREG);
             )
    else
      update (i_cTable) set;
          ATDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          ATCODATT = this.w_ATTIVI;
          and ATNUMREG = this.w_PNNUMREG;
          and ATTIPREG = this.w_PNTIPREG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ENDSCAN
    return


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Genera i Documenti da Contabilizzare
    * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    * --- Testa il Cambio di Documento
    this.w_OSERIAL = REPL("#", 10)
    this.w_OCODCON = REPL("#", 15)
    SELECT CONTADOC
    GO TOP
    SCAN FOR NOT EMPTY(NVL(INSERIAL," ")) AND NOT EMPTY(NVL(INCODCAU," "))
    this.w_APPTIPDOC = MVTIPDOC
    * --- Testa Cambio Documento
    if this.w_OSERIAL<>INSERIAL or this.w_OCODCON<> Nvl(MVCODCON," ")
      this.Pag4()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      SELECT CONTADOC
      * --- Inizializza i dati di Testata della Nuova Registrazione P.N.
      this.w_PNDATREG = IIF(this.oParentObject.w_FLDATC="U", this.oParentObject.w_DATCON, INDATDOC)
      this.w_PNCODUTE = IIF(g_UNIUTE $ "UE", i_CODUTE, 0)
      this.w_PNPRG = IIF(g_UNIUTE $ "GE", DTOS(this.w_PNDATREG), SPACE(8))
      this.w_PNCODESE = NVL(INCODESE, SPACE(4))
      this.w_PNCOMPET = NVL(INCODESE, SPACE(4))
      this.w_ORIDOC = NVL(INORIDOC,SPACE(10))
      this.w_INICOM = cp_CharToDate("  -  -  ")
      this.w_FINCOM = cp_CharToDate("  -  -  ")
      this.w_PNCODCAU = INCODCAU
      * --- Read from CAU_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAU_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCFLANAL,CCDESSUP,CCDESRIG"+;
          " from "+i_cTable+" CAU_CONT where ";
              +"CCCODICE = "+cp_ToStrODBC(this.w_PNCODCAU);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCFLANAL,CCDESSUP,CCDESRIG;
          from (i_cTable) where;
              CCCODICE = this.w_PNCODCAU;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_CCFLANAL = NVL(cp_ToDate(_read_.CCFLANAL),cp_NullValue(_read_.CCFLANAL))
        this.w_CCDESSUP = NVL(cp_ToDate(_read_.CCDESSUP),cp_NullValue(_read_.CCDESSUP))
        this.w_CCDESRIG = NVL(cp_ToDate(_read_.CCDESRIG),cp_NullValue(_read_.CCDESRIG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_PNTIPREG = NVL(CCTIPREG, "N")
      this.w_PNNUMREG = NVL(CCNUMREG, 1)
      this.w_PNNUMDOC = NVL(INNUMDOC, 0)
      this.w_PNALFDOC = NVL(INALFDOC, Space(10))
      this.w_PNDATDOC = CP_TODATE(INDATDOC)
      this.w_PNCOMIVA = this.w_PNDATREG
      this.w_PTBANNOS = SPACE(15)
      this.w_CODESE = NVL(MVCODESE, SPACE(4))
      this.w_NUMDOC = NVL(MVNUMDOC, 0)
      this.w_ALFDOC = NVL(MVALFDOC, Space(10))
      this.w_PNTIPCLF = Nvl(MVTIPCON, "N")
      this.w_PNCODCLF = NVL(MVCODCON, SPACE(15))
      * --- Controllo Integrit� Prima Nota
      * --- Controllo che il documento possa essere contabilizzato
      * --- 1 - Controllo che la data reg. sia maggiore ultima stampa libro giornale e data blocco prima Nota
      * --- 2 - Data reg. maggiore Data Stampa Registri Iva
      * --- All'inizio della procedura vengono bloccati tutti i registri coinvolti e messa la data di blocco prima nota
      * --- Controllo se posso registrarla - rileggo i dati ogni volta la contabilizzazione potrebbe protrarsi a lungo
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZSTALIG"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZSTALIG;
          from (i_cTable) where;
              AZCODAZI = this.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_STALIG = NVL(cp_ToDate(_read_.AZSTALIG),cp_NullValue(_read_.AZSTALIG))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_REGOK = .T.
      do case
        case this.w_PNDATREG<=this.w_STALIG
          this.w_REGOK = .F.
          this.w_MESS = ah_MsgFormat("La data reg. � inferiore alla data ultima stampa L.G. e/o data blocco primanota")
        case Not Empty(CHKCONS("P",this.w_PNDATREG,"B","N"))
          this.w_REGOK = .F.
          this.w_MESS = substr(CHKCONS("P",this.w_PNDATREG,"B","N")+"!",12)
        otherwise
          * --- Controllo anche x i Registri IVA
          this.w_DATIVA = cp_CharToDate("  -  -  ")
          * --- Select from ATTIDETT
          i_nConn=i_TableProp[this.ATTIDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select ATDATSTA  from "+i_cTable+" ATTIDETT ";
                +" where ATNUMREG="+cp_ToStrODBC(this.w_PNNUMREG)+" AND ATTIPREG="+cp_ToStrODBC(this.w_PNTIPREG)+"";
                 ,"_Curs_ATTIDETT")
          else
            select ATDATSTA from (i_cTable);
             where ATNUMREG=this.w_PNNUMREG AND ATTIPREG=this.w_PNTIPREG;
              into cursor _Curs_ATTIDETT
          endif
          if used('_Curs_ATTIDETT')
            select _Curs_ATTIDETT
            locate for 1=1
            do while not(eof())
            this.w_DATIVA = NVL(CP_TODATE(_Curs_ATTIDETT.ATDATSTA),cp_CharToDate("  -  -  "))
              select _Curs_ATTIDETT
              continue
            enddo
            use
          endif
          * --- Normalmente la competenza IVA � sempre inferiore alla data di registrazione (fatt. differite)
          * --- Non capita ma se capita � meglio controllare anche la data registrazione
          this.w_REGOK = MIN(this.w_PNDATREG,this.w_PNCOMIVA)>this.w_DATIVA
          * --- Il messaggio lo inizializzo sempre - se serve lo prendo altrimenti no
          this.w_MESS = ah_MsgFormat("Data reg. � inferiore all'ultima stampa dei registri IVA")
      endcase
      if this.w_REGOK=.T.
        SELECT CONTADOC
        this.w_PNTIPDOC = NVL(CCTIPDOC, "  ")
        this.w_FLPDOC = NVL(CCFLPDOC, "N")
        this.w_FLPPRO = NVL(CCFLPPRO, "N")
        this.w_PNPRD = "NN"
        this.w_PNPRP = "NN"
        this.w_PNANNPRO = "    "
        this.w_PNALFPRO = Space(10)
        this.w_PNNUMPRO = 0
        this.w_PNVALNAZ = NVL(INVALNAZ, g_PERVAL)
        * --- Valuta del Documento di Origine
        this.w_PNCODVAL = NVL(INCODVAL, g_PERVAL)
        this.w_PNCAOVAL = NVL(INCAOVAL, 1)
        this.w_VADTOBSO = CP_TODATE(VADTOBSO)
        this.w_PNTIPCLF = "N"
        this.w_PNCODCLF = SPACE(15)
        this.w_PTBANAPP = SPACE(10)
        this.w_CCCONIVA = NVL(CCCONIVA, SPACE(15))
        this.w_PNFLREGI = " "
        this.w_PNFLPROV = "N"
        this.w_PNDESSUP = SPACE(50)
        this.w_FLBESE = NVL(CCFLBESE, " ")
        this.w_PARTSN = "N"
        if g_PERPAR="S" AND this.oParentObject.w_PARCLI="S"
          this.w_PARTSN = IIF(NVL(CCFLPART, " ") $ "ACS", CCFLPART, "N")
        endif
        this.w_CAONAZ = GETCAM(g_PERVAL, I_DATSYS)
        this.w_DECTOP = g_PERPVL
        * --- Se altra Valuta di Esercizio
        if this.w_PNVALNAZ<>g_PERVAL
          * --- Read from VALUTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VADECTOT"+;
              " from "+i_cTable+" VALUTE where ";
                  +"VACODVAL = "+cp_ToStrODBC(this.w_PNVALNAZ);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VADECTOT;
              from (i_cTable) where;
                  VACODVAL = this.w_PNVALNAZ;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DECTOP = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_CAONAZ = GETCAM(this.w_PNVALNAZ, I_DATSYS)
          SELECT CONTADOC
        endif
        this.w_PNTOTDOC = 0
        this.w_SEZCLF = "A"
      else
        * --- Segnalo l'errore
        this.w_oERRORLOG.AddMsgLog("Verificare doc. n.: %1 del.: %2", ALLTRIM(STR(this.w_PNNUMDOC,15))+IIF(EMPTY(this.w_PNALFDOC),"","/"+Alltrim(this.w_PNALFDOC)),DTOC(this.w_PNDATDOC))     
        this.w_oERRORLOG.AddMsgLogNoTranslate(left(SPACE(20)+this.w_MESS,120))     
        this.w_NUDOC = this.w_NUDOC + 1
      endif
    endif
    SELECT CONTADOC
    this.w_OSERIAL = INSERIAL
    this.w_OCODCON = MVCODCON
    this.w_INICOMD = cp_CharToDate("  -  -  ")
    this.w_FINCOMD = cp_CharToDate("  -  -  ")
    this.w_ROWINC = CPROWNUM
    if this.w_REGOK=.T.
      * --- Scrive nuova Riga sul Temporaneo di Appoggio
      this.w_PTCODVAL = NVL(INCODVAL, g_PERVAL)
      this.w_PTCAOVAL = NVL(INCAOVAL, g_CAOVAL)
      this.w_PTCAOAPE = NVL(INCAOAPE, g_CAOVAL)
      this.w_PTDATAPE = CP_TODATE(INDATAPE)
      this.w_APPO1 = NVL(INIMPSAL, 0)
      this.w_APPO2 = NVL(INIMPABB, 0)
      this.w_PTNUMDOC = NVL(MVNUMDOC, 0)
      this.w_PTALFDOC = NVL(MVALFDOC, Space(10))
      this.w_PTDATDOC = CP_TODATE(MVDATDOC)
      this.w_CONCLI1 = NVL(RFCONCLI, SPACE(15))
      if NOT EMPTY(this.w_CONCLI1)
        this.w_CONCLIC = this.w_CONCLI1
      else
        this.w_CONCLIC = this.oParentObject.w_CONCLI
      endif
      this.w_MVTIPDOC = NVL(MVTIPDOC, SPACE(5))
      INSERT INTO DettDocu (CODVAL, CAOAPE, DATAPE, CAOVAL, ;
      IMPSAL, IMPABB, NUMDOC, ALFDOC, DATDOC,CODCON,ROWINC,TIPDOC) ;
      VALUES (this.w_PTCODVAL, this.w_PTCAOAPE, this.w_PTDATAPE, this.w_PTCAOVAL, ;
      this.w_APPO1, this.w_APPO2, this.w_PTNUMDOC, this.w_PTALFDOC, this.w_PTDATDOC, this.w_CONCLIC,this.w_ROWINC,this.w_MVTIPDOC)
      this.w_APPO3 = this.w_APPO1+this.w_APPO2
      if this.w_PTCODVAL<>this.w_PNVALNAZ
        * --- Converte in Moneta di Conto
        this.w_APPO1 = VAL2MON(this.w_APPO1,this.w_PTCAOVAL,this.w_CAONAZ,this.w_PNDATDOC,this.w_PNVALNAZ, this.w_DECTOP)
        this.w_APPO2 = VAL2MON(this.w_APPO2,this.w_PTCAOVAL,this.w_CAONAZ,this.w_PNDATDOC,this.w_PNVALNAZ, this.w_DECTOP)
        this.w_APPO3 = VAL2MON(this.w_APPO3,this.w_PTCAOAPE,this.w_CAONAZ,this.w_PNDATDOC,this.w_PNVALNAZ, this.w_DECTOP)
      endif
      * --- Se Corrispettivi su Servizi calcola L'IVA sull'effettivo Incassato
      this.w_IVASOSP = 0
      if this.w_FLBESE="S"
        SELECT CONTADOC
        this.w_MVDATDOC = CP_TODATE(MVDATDOC)
        this.w_MVCODVAL = NVL(MVCODVAL, g_PERVAL)
        this.w_MVCAOVAL = NVL(MVCAOVAL, g_CAOVAL)
        this.w_CODIVE = NVL(MVCODIVE, SPACE(5))
        this.w_TOTCAS = this.w_APPO1+this.w_APPO2
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.w_IMPCLI = this.w_IMPCLI + this.w_APPO3
      this.w_CONCLI1 = NVL(RFCONCLI, SPACE(15))
      if NOT EMPTY(this.w_CONCLI1)
        this.w_CONCLIC = this.w_CONCLI1
      else
        this.w_CONCLIC = this.oParentObject.w_CONCLI
      endif
      * --- Inserisce nel cursore d'appoggio i conti di tipo Crediti v.Clienti (tipo B)
      INSERT INTO DETCONT( TIPO, CONTO, IMPORTO,TIPDOC) VALUES("B", this.w_CONCLIC,this.w_APPO3, this.w_MVTIPDOC)
      this.w_IMPCAF = this.w_IMPCAF + this.w_APPO1
      this.w_CONCAF1 = NVL(RFCONCAF, SPACE(15))
      if NOT EMPTY(this.w_CONCAF1)
        this.w_CONCAFC = this.w_CONCAF1
      else
        this.w_CONCAFC = this.oParentObject.w_CONCAF
      endif
      * --- Inserisce nel cursore d'appoggio i conti di tipo Incasso Corrispettivi (tipo A)
      INSERT INTO DETCONT( TIPO, CONTO, IMPORTO,TIPDOC) VALUES("A", this.w_CONCAFC,this.w_APPO1, this.w_MVTIPDOC)
      this.w_IMPABF = this.w_IMPABF + this.w_APPO2
      this.w_CONABF1 = NVL(RFCONABF,SPACE(15))
      if NOT EMPTY(this.w_CONABF1)
        this.w_CONABFC = this.w_CONABF1
      else
        this.w_CONABFC = this.oParentObject.w_CONABF
      endif
      * --- Inserisce nel cursore d'appoggio i conti di tipo Abbuoni (tipo C)
      INSERT INTO DETCONT( TIPO, CONTO, IMPORTO, TIPDOC) VALUES("C", this.w_CONABFC, this.w_APPO2, this.w_MVTIPDOC)
      this.w_CONIVF1 = NVL(RFCONIVF, SPACE(15))
      if NOT EMPTY(this.w_CONIVF1)
        this.w_CONIVFC = this.w_CONIVF1
      else
        this.w_CONIVFC = this.oParentObject.w_CONIVF
      endif
      INSERT INTO DETCONT( TIPO, CONTO, IMPORTO, TIPDOC) VALUES("D", this.w_CONIVFC, this.w_IVASOSP, this.w_MVTIPDOC)
    endif
    SELECT CONTADOC
    ENDSCAN
    * --- Testa l'Ultima Uscita
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili Locali
    this.w_TIPO = "G"
    if NOT EMPTY(this.oParentObject.w_CONDIC)
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ANDTOBSO"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_TIPO);
              +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_CONDIC);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ANDTOBSO;
          from (i_cTable) where;
              ANTIPCON = this.w_TIPO;
              and ANCODICE = this.oParentObject.w_CONDIC;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if this.w_DATOBSO<this.oParentObject.w_DATFIN AND NOT EMPTY(this.w_DATOBSO)
        ah_ErrorMsg("Contropartita diff. di conversione obsoleta",,"")
        i_retcode = 'stop'
        return
      endif
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se non e' il Primo Ingresso, Scrive la Nuova Registrazione Contabile
    if this.w_OSERIAL <> REPL("#", 10) AND RECCOUNT("DettDocu") > 0
      * --- Inizializza Messaggistica di Errore
      this.w_FLERR = .F.
      FOR L_i = 1 TO 19
      MES[l_i] = ""
      ENDFOR
      if (Not Empty(this.w_CCDESSUP) OR Not Empty(this.w_CCDESRIG))
        * --- Array elenco parametri per descrizioni di riga e testata
         
 ARPARAM[1,1]="NUMDOC" 
 ARPARAM[1,2]=ALLTRIM(STR(this.w_PNNUMDOC,15)) 
 ARPARAM[2,1]="ALFDOC" 
 ARPARAM[2,2]=this.w_PNALFDOC 
 ARPARAM[3,1]="DATDOC" 
 ARPARAM[3,2]=DTOC(this.w_PNDATDOC) 
 ARPARAM[4,1]="ALFPRO" 
 ARPARAM[4,2]="" 
 ARPARAM[5,1]="DATREG" 
 ARPARAM[5,2]=DTOC(this.w_PNDATREG) 
 ARPARAM[6,1]="CODCON" 
 ARPARAM[6,2]="" 
 ARPARAM[7,1]="ELEDOC" 
 ARPARAM[7,2]="" 
 ARPARAM[8,1]="CODAGE" 
 ARPARAM[8,2]="" 
 ARPARAM[9,1]="NUMPRO" 
 ARPARAM[9,2]=ALLTRIM(STR(this.w_PNNUMPRO,15)) 
 ARPARAM[10,1]="NUMPAR" 
 ARPARAM[10,2]="" 
 ARPARAM[11,1]="DATSCA" 
 ARPARAM[11,2]="" 
 ARPARAM[12,1]="TIPCON" 
 ARPARAM[12,2]=""
        this.w_PNDESSUP = CALDESPA(this.w_CCDESSUP,@ARPARAM)
      endif
      * --- Calcola MVSERIAL, MVNUMREG, MVNUMDOC
      this.w_PNSERIAL = SPACE(10)
      this.w_PNNUMRER = 0
      this.w_PNANNDOC = CALPRO(IIF(EMPTY(this.w_PNDATDOC), this.w_PNDATREG, this.w_PNDATDOC), this.w_PNCOMPET, this.w_FLPDOC)
      this.w_PNINICOM = cp_CharToDate("  -  -  ")
      this.w_PNFINCOM = cp_CharToDate("  -  -  ")
      this.w_PNFLABAN = SPACE(6)
      this.w_CPROWNUM = 0
      this.w_CPROWORD = 0
      this.w_TOTDAR = 0
      this.w_TOTAVE = 0
      this.w_PARFOR = 0
      this.w_LNUMDOC = iif(this.oParentObject.w_FLNODOC="N",this.w_PNNUMDOC,0)
      this.w_LALFDOC = iif(this.oParentObject.w_FLNODOC="N",this.w_PNALFDOC,SPACE(10))
      this.w_LDATDOC = iif(this.oParentObject.w_FLNODOC="N",this.w_PNDATDOC,cp_CharToDate("  -  -  "))
      this.w_NUDOC = this.w_NUDOC + 1
      * --- Try
      local bErr_03B3EF98
      bErr_03B3EF98=bTrsErr
      this.Try_03B3EF98()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        if this.w_FLERR=.F.
          * --- Errore non Compreso nei Controlli
          this.w_oERRORLOG.AddMsgLog("Verificare doc. n.: %1 del.: %2", ALLTRIM(STR(this.w_PNNUMDOC,15))+IIF(EMPTY(this.w_PNALFDOC),"","/"+Alltrim(this.w_PNALFDOC)),DTOC(this.w_PNDATDOC))     
          this.w_oERRORLOG.AddMsgLogNoTranslate(SPACE(50))     
        endif
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_03B3EF98
      * --- End
    endif
    * --- Azzera il Temporaneo di Appoggio
    CREATE CURSOR DettDocu ;
    (CODVAL C(3), CAOAPE N(12,6), DATAPE D(8), CAOVAL N(12,6), ;
    IMPSAL N(18,4), IMPABB N(18,4), NUMDOC N(15,0), ALFDOC C(10), DATDOC D(8),CODCON C(15),ROWINC N(5) , TIPDOC CHAR(5))
    CREATE CURSOR DettIva ;
    (FLOMAG C(1), CODIVA C(5), TOTIMP N(18,4), TOTIVA N(18,4), PERIVA N(5,2))
    * --- Cursore di appoggio Parametri fiscali per caus. doc.
    CREATE CURSOR Detcont ;
    (TIPO C(1), CONTO C(15), IMPORTO N(18,4), TIPDOC C(5))
    this.w_IVADET = 0
    this.w_IMPCLI = 0
    this.w_TOTDAR = 0
    this.w_IMPCAF = 0
    this.w_TOTAVE = 0
    this.w_IMPABF = 0
  endproc
  proc Try_03B3EF98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    i_Conn=i_TableProp[this.PNT_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEPNT", "i_codazi,w_PNSERIAL")
    cp_NextTableProg(this, i_Conn, "PRPNT", "i_codazi,w_PNCODESE,w_PNCODUTE,w_PNPRG,w_PNNUMRER")
    this.w_APPO = ah_MsgFormat("%1 del.: %2",ALLTRIM(STR(this.w_PNNUMRER)),DTOC(this.w_PNDATREG))
    ah_Msg("Scrivo reg. n.: %1",.T.,.F.,.F.,this.w_APPO)
    * --- Scrive la Testata
    this.w_APPO = IIF(EMPTY(this.w_PNDATDOC), this.w_PNDATREG, this.w_PNDATDOC)
    this.w_CODVAP = this.w_PNCODVAL
    this.w_CAOVAP = this.w_PNCAOVAL
    if NOT EMPTY(this.w_VADTOBSO) AND this.w_VADTOBSO<=this.w_APPO
      * --- Vauta EMU Obsoleta ; Converte in EURO
      this.w_CODVAP = g_CODEUR
      this.w_CAOVAP = 1
    endif
    * --- Insert into PNT_MAST
    i_nConn=i_TableProp[this.PNT_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNALFDOC"+",PNALFPRO"+",PNANNDOC"+",PNANNPRO"+",PNCAOVAL"+",PNCODCAU"+",PNCODCLF"+",PNCODESE"+",PNCODUTE"+",PNCODVAL"+",PNCOMIVA"+",PNCOMPET"+",PNDATDOC"+",PNDATREG"+",PNDESSUP"+",PNFLIVDF"+",PNFLPROV"+",PNFLREGI"+",PNNUMDOC"+",PNNUMPRO"+",PNNUMREG"+",PNNUMRER"+",PNPRD"+",PNPRG"+",PNPRP"+",PNRIFINC"+",PNSERIAL"+",PNTIPCLF"+",PNTIPDOC"+",PNTIPREG"+",PNTOTDOC"+",PNVALNAZ"+",UTCC"+",UTDC"+",UTCV"+",UTDV"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNALFDOC),'PNT_MAST','PNALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNALFPRO),'PNT_MAST','PNALFPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNANNDOC),'PNT_MAST','PNANNDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNANNPRO),'PNT_MAST','PNANNPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CAOVAP),'PNT_MAST','PNCAOVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_MAST','PNCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCLF),'PNT_MAST','PNCODCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'PNT_MAST','PNCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODUTE),'PNT_MAST','PNCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODVAP),'PNT_MAST','PNCODVAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMIVA),'PNT_MAST','PNCOMIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCOMPET),'PNT_MAST','PNCOMPET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATDOC),'PNT_MAST','PNDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PNT_MAST','PNDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESSUP),'PNT_MAST','PNDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_MAST','PNFLIVDF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPROV),'PNT_MAST','PNFLPROV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLREGI),'PNT_MAST','PNFLREGI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMDOC),'PNT_MAST','PNNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMPRO),'PNT_MAST','PNNUMPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'PNT_MAST','PNNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMRER),'PNT_MAST','PNNUMRER');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRD),'PNT_MAST','PNPRD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRG),'PNT_MAST','PNPRG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNPRP),'PNT_MAST','PNPRP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_OSERIAL),'PNT_MAST','PNRIFINC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_MAST','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCLF),'PNT_MAST','PNTIPCLF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPDOC),'PNT_MAST','PNTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'PNT_MAST','PNTIPREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTOTDOC),'PNT_MAST','PNTOTDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNVALNAZ),'PNT_MAST','PNVALNAZ');
      +","+cp_NullLink(cp_ToStrODBC(i_CODUTE),'PNT_MAST','UTCC');
      +","+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'PNT_MAST','UTDC');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_MAST','UTCV');
      +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'PNT_MAST','UTDV');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNALFDOC',this.w_PNALFDOC,'PNALFPRO',this.w_PNALFPRO,'PNANNDOC',this.w_PNANNDOC,'PNANNPRO',this.w_PNANNPRO,'PNCAOVAL',this.w_CAOVAP,'PNCODCAU',this.w_PNCODCAU,'PNCODCLF',this.w_PNCODCLF,'PNCODESE',this.w_PNCODESE,'PNCODUTE',this.w_PNCODUTE,'PNCODVAL',this.w_CODVAP,'PNCOMIVA',this.w_PNCOMIVA,'PNCOMPET',this.w_PNCOMPET)
      insert into (i_cTable) (PNALFDOC,PNALFPRO,PNANNDOC,PNANNPRO,PNCAOVAL,PNCODCAU,PNCODCLF,PNCODESE,PNCODUTE,PNCODVAL,PNCOMIVA,PNCOMPET,PNDATDOC,PNDATREG,PNDESSUP,PNFLIVDF,PNFLPROV,PNFLREGI,PNNUMDOC,PNNUMPRO,PNNUMREG,PNNUMRER,PNPRD,PNPRG,PNPRP,PNRIFINC,PNSERIAL,PNTIPCLF,PNTIPDOC,PNTIPREG,PNTOTDOC,PNVALNAZ,UTCC,UTDC,UTCV,UTDV &i_ccchkf. );
         values (;
           this.w_PNALFDOC;
           ,this.w_PNALFPRO;
           ,this.w_PNANNDOC;
           ,this.w_PNANNPRO;
           ,this.w_CAOVAP;
           ,this.w_PNCODCAU;
           ,this.w_PNCODCLF;
           ,this.w_PNCODESE;
           ,this.w_PNCODUTE;
           ,this.w_CODVAP;
           ,this.w_PNCOMIVA;
           ,this.w_PNCOMPET;
           ,this.w_PNDATDOC;
           ,this.w_PNDATREG;
           ,this.w_PNDESSUP;
           ," ";
           ,this.w_PNFLPROV;
           ,this.w_PNFLREGI;
           ,this.w_PNNUMDOC;
           ,this.w_PNNUMPRO;
           ,this.w_PNNUMREG;
           ,this.w_PNNUMRER;
           ,this.w_PNPRD;
           ,this.w_PNPRG;
           ,this.w_PNPRP;
           ,this.w_OSERIAL;
           ,this.w_PNSERIAL;
           ,this.w_PNTIPCLF;
           ,this.w_PNTIPDOC;
           ,this.w_PNTIPREG;
           ,this.w_PNTOTDOC;
           ,this.w_PNVALNAZ;
           ,i_CODUTE;
           ,SetInfoDate( g_CALUTD );
           ,0;
           ,cp_CharToDate("  -  -    ");
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_PNTIPCON = "G"
    * --- Raggruppa per tipo e conto
    SELECT DETCONT
    GO TOP
    SELECT TIPO, CONTO,SUM(IMPORTO) AS IMPORTO,TIPDOC;
    FROM DETCONT INTO CURSOR DETCONT GROUP BY TIPO, CONTO,TIPDOC
    * --- Scrive il Dettaglio: Riga Clienti c.Vendite
    if this.w_IMPCLI<>0
      SELECT DETCONT 
      GO TOP
      SCAN FOR TIPO = "B"
      this.w_PNCODCON = CONTO
      this.w_APPVAL = IMPORTO
      this.w_APPSEZ = this.w_SEZCLF
      this.w_PNFLPART = this.w_PARTSN
      this.w_TIPDOCU = TIPDOC
      * --- Read from RIFI_CADO
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.RIFI_CADO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIFI_CADO_idx,2],.t.,this.RIFI_CADO_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "RFPAGRIC"+;
          " from "+i_cTable+" RIFI_CADO where ";
              +"RFCAUDOC = "+cp_ToStrODBC(this.w_TIPDOCU);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          RFPAGRIC;
          from (i_cTable) where;
              RFCAUDOC = this.w_TIPDOCU;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PAGRIC1 = NVL(cp_ToDate(_read_.RFPAGRIC),cp_NullValue(_read_.RFPAGRIC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT EMPTY (this.w_PAGRIC1)
        this.w_PNCODPAG = this.w_PAGRIC1
      else
        this.w_PNCODPAG = this.oParentObject.w_SAPAGA
      endif
      this.w_PNINICOM = cp_CharToDate("  -  -  ")
      this.w_PNFINCOM = cp_CharToDate("  -  -  ")
      this.w_PNIMPIND = 0
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Aggiorna le Partite
      if this.w_PARTSN $ "ACS"
        this.w_ROWNUM = 0
        this.w_PTFLSOSP = " "
        this.w_PTMODPAG = this.oParentObject.w_TIPPAB
        SELECT DETTDOCU
        SELECT * FROM DETTDOCU WHERE CODCON = this.w_PNCODCON AND TIPDOC=this.w_TIPDOCU INTO CURSOR APPCUR
        SELECT AppCur
        GO TOP
        SCAN FOR IMPSAL<>0 OR IMPABB<>0
        this.w_ROWNUM = this.w_ROWNUM + 1
        this.w_PTTOTIMP = IMPSAL+IMPABB
        this.w_PTDATSCA = DATDOC
        this.w_PTCODVAL = CODVAL
        this.w_PTCAOVAL = CAOVAL
        this.w_PTCAOAPE = CAOAPE
        this.w_PTDATAPE = DATAPE
        this.w_PTNUMDOC = NUMDOC
        this.w_PTALFDOC = ALFDOC
        this.w_PTDATDOC = DATDOC
        * --- Select from DOC_RATE
        i_nConn=i_TableProp[this.DOC_RATE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2],.t.,this.DOC_RATE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select RSDATRAT  from "+i_cTable+" DOC_RATE ";
              +" where RSSERIAL="+cp_ToStrODBC(this.w_ORIDOC)+"";
               ,"_Curs_DOC_RATE")
        else
          select RSDATRAT from (i_cTable);
           where RSSERIAL=this.w_ORIDOC;
            into cursor _Curs_DOC_RATE
        endif
        if used('_Curs_DOC_RATE')
          select _Curs_DOC_RATE
          locate for 1=1
          do while not(eof())
          this.w_PTDATSCA = _Curs_DOC_RATE.RSDATRAT
            select _Curs_DOC_RATE
            continue
          enddo
          use
        endif
        * --- I dati necessari al calcolo del numero partita li prendo dal documento e non dall'incasso corrispettivo
        this.w_PTNUMPAR = CANUMPAR("N", this.w_CODESE, this.w_NUMDOC, this.w_ALFDOC)
        * --- Insert into PAR_TITE
        i_nConn=i_TableProp[this.PAR_TITE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_TITE_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PAR_TITE_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PTSERIAL"+",PTROWORD"+",CPROWNUM"+",PTNUMPAR"+",PTDATSCA"+",PTTIPCON"+",PTCODCON"+",PT_SEGNO"+",PTTOTIMP"+",PTCODVAL"+",PTCAOVAL"+",PTCAOAPE"+",PTDATAPE"+",PTNUMDOC"+",PTALFDOC"+",PTDATDOC"+",PTIMPDOC"+",PTMODPAG"+",PTFLSOSP"+",PTBANAPP"+",PTNUMDIS"+",PTFLINDI"+",PTBANNOS"+",PTFLRAGG"+",PTFLCRSA"+",PTFLIMPE"+",PTTOTABB"+",PTDATREG"+",PTNUMCOR"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PAR_TITE','PTSERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PAR_TITE','PTROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PAR_TITE','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMPAR),'PAR_TITE','PTNUMPAR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATSCA),'PAR_TITE','PTDATSCA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PAR_TITE','PTTIPCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PAR_TITE','PTCODCON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SEZCLF),'PAR_TITE','PT_SEGNO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTTOTIMP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTCODVAL),'PAR_TITE','PTCODVAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOVAL),'PAR_TITE','PTCAOVAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTCAOAPE),'PAR_TITE','PTCAOAPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATAPE),'PAR_TITE','PTDATAPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTNUMDOC),'PAR_TITE','PTNUMDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTALFDOC),'PAR_TITE','PTALFDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTDATDOC),'PAR_TITE','PTDATDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTTOTIMP),'PAR_TITE','PTIMPDOC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTMODPAG),'PAR_TITE','PTMODPAG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTFLSOSP),'PAR_TITE','PTFLSOSP');
          +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'PAR_TITE','PTBANAPP');
          +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTNUMDIS');
          +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLINDI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PTBANNOS),'PAR_TITE','PTBANNOS');
          +","+cp_NullLink(cp_ToStrODBC(" "),'PAR_TITE','PTFLRAGG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PARTSN),'PAR_TITE','PTFLCRSA');
          +","+cp_NullLink(cp_ToStrODBC("  "),'PAR_TITE','PTFLIMPE');
          +","+cp_NullLink(cp_ToStrODBC(0),'PAR_TITE','PTTOTABB');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PNDATREG),'PAR_TITE','PTDATREG');
          +","+cp_NullLink(cp_ToStrODBC(SPACE(25)),'PAR_TITE','PTNUMCOR');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PTSERIAL',this.w_PNSERIAL,'PTROWORD',this.w_CPROWNUM,'CPROWNUM',this.w_ROWNUM,'PTNUMPAR',this.w_PTNUMPAR,'PTDATSCA',this.w_PTDATSCA,'PTTIPCON',this.w_PNTIPCON,'PTCODCON',this.w_PNCODCON,'PT_SEGNO',this.w_SEZCLF,'PTTOTIMP',this.w_PTTOTIMP,'PTCODVAL',this.w_PTCODVAL,'PTCAOVAL',this.w_PTCAOVAL,'PTCAOAPE',this.w_PTCAOAPE)
          insert into (i_cTable) (PTSERIAL,PTROWORD,CPROWNUM,PTNUMPAR,PTDATSCA,PTTIPCON,PTCODCON,PT_SEGNO,PTTOTIMP,PTCODVAL,PTCAOVAL,PTCAOAPE,PTDATAPE,PTNUMDOC,PTALFDOC,PTDATDOC,PTIMPDOC,PTMODPAG,PTFLSOSP,PTBANAPP,PTNUMDIS,PTFLINDI,PTBANNOS,PTFLRAGG,PTFLCRSA,PTFLIMPE,PTTOTABB,PTDATREG,PTNUMCOR &i_ccchkf. );
             values (;
               this.w_PNSERIAL;
               ,this.w_CPROWNUM;
               ,this.w_ROWNUM;
               ,this.w_PTNUMPAR;
               ,this.w_PTDATSCA;
               ,this.w_PNTIPCON;
               ,this.w_PNCODCON;
               ,this.w_SEZCLF;
               ,this.w_PTTOTIMP;
               ,this.w_PTCODVAL;
               ,this.w_PTCAOVAL;
               ,this.w_PTCAOAPE;
               ,this.w_PTDATAPE;
               ,this.w_PTNUMDOC;
               ,this.w_PTALFDOC;
               ,this.w_PTDATDOC;
               ,this.w_PTTOTIMP;
               ,this.w_PTMODPAG;
               ,this.w_PTFLSOSP;
               ,SPACE(10);
               ," ";
               ," ";
               ,this.w_PTBANNOS;
               ," ";
               ,this.w_PARTSN;
               ,"  ";
               ,0;
               ,this.w_PNDATREG;
               ,SPACE(25);
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        SELECT appcur
        ENDSCAN
      endif
      ENDSCAN
    endif
    this.w_PNFLPART = "N"
    this.w_PNCODPAG = SPACE(5)
    * --- Scrive il Dettaglio: Riga Incasso
    if this.w_IMPCAF<>0
      SELECT DETCONT 
      GO TOP
      SCAN FOR TIPO = "A"
      this.w_PNCODCON = CONTO
      this.w_APPVAL = IMPORTO
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      this.w_PNINICOM = cp_CharToDate("  -  -  ")
      this.w_PNFINCOM = cp_CharToDate("  -  -  ")
      this.w_PNIMPIND = 0
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ENDSCAN
    endif
    * --- Scrive il Dettaglio: Riga Abbuono a Saldo
    if this.w_IMPABF<>0
      SELECT DETCONT 
      GO TOP
      SCAN FOR TIPO = "C"
      this.w_PNCODCON = CONTO
      this.w_APPVAL = IMPORTO
      this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
      this.w_PNINICOM = cp_CharToDate("  -  -  ")
      this.w_PNFINCOM = cp_CharToDate("  -  -  ")
      this.w_PNIMPIND = 0
      this.Pag5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      ENDSCAN
    endif
    if this.w_FLBESE="S" AND this.w_IVADET<>0
      * --- Se Incasso Corrispettivi Servizi Scrive anche l'IVA per l'Incassato e Storna l'IVA Differita
      * --- Scrive il Dettaglio: Conto IVA Detraibile
      if EMPTY(this.w_CCCONIVA)
        MES[2] = ah_Msgformat("Conto IVA detraibile non definito")
        this.w_FLERR = .T.
      else
        this.w_PNCODCON = this.w_CCCONIVA
        this.w_APPVAL = this.w_IVADET
        this.w_APPSEZ = this.w_SEZCLF
        this.w_PNINICOM = cp_CharToDate("  -  -  ")
        this.w_PNFINCOM = cp_CharToDate("  -  -  ")
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        * --- Scrive il Dettaglio: Riga IVA Differita
        SELECT DETCONT 
        GO TOP
        SCAN FOR TIPO = "D"
        this.w_PNCODCON = CONTO
        this.w_APPVAL = IMPORTO
        this.w_APPSEZ = IIF(this.w_SEZCLF="D", "A", "D")
        this.w_PNINICOM = cp_CharToDate("  -  -  ")
        this.w_PNFINCOM = cp_CharToDate("  -  -  ")
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        ENDSCAN
      endif
      * --- Scrive Castelletto IVA
      if this.w_FLERR=.F.
        * --- Scrive il Castelletto IVA
        this.w_ROWNUM = 0
        if USED("DettIva")
          SELECT FLOMAG, CODIVA, SUM(TOTIMP) AS TOTIMP, SUM(TOTIVA) AS TOTIVA, PERIVA FROM DettIva ;
          WHERE NOT EMPTY(NVL(CODIVA,"")) AND (TOTIMP<>0 OR TOTIVA<>0 OR FLOMAG="Z") ;
          INTO CURSOR DettIva GROUP BY FLOMAG, CODIVA, PERIVA
          SELECT DettIva
          GO TOP
          SCAN FOR NOT EMPTY(CODIVA) AND (TOTIMP<>0 OR TOTIVA<>0 OR FLOMAG="Z")
          this.w_ROWNUM = this.w_ROWNUM + 1
          this.w_IVFLOMAG = FLOMAG
          this.w_IVCODIVA = CODIVA
          this.w_IVIMPONI = TOTIMP
          this.w_IVIMPIVA = TOTIVA
          this.w_IVTIPREG = this.w_PNTIPREG
          * --- Insert into PNT_IVA
          i_nConn=i_TableProp[this.PNT_IVA_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_IVA_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"IVSERIAL"+",CPROWNUM"+",IVCODIVA"+",IVTIPCON"+",IVCODCON"+",IVPERIND"+",IVTIPREG"+",IVNUMREG"+",IVIMPONI"+",IVIMPIVA"+",IVFLOMAG"+",IVCFLOMA"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_IVA','IVSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PNT_IVA','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IVCODIVA),'PNT_IVA','IVCODIVA');
            +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_IVA','IVTIPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_CCCONIVA),'PNT_IVA','IVCODCON');
            +","+cp_NullLink(cp_ToStrODBC(0),'PNT_IVA','IVPERIND');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IVTIPREG),'PNT_IVA','IVTIPREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'PNT_IVA','IVNUMREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IVIMPONI),'PNT_IVA','IVIMPONI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IVIMPIVA),'PNT_IVA','IVIMPIVA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IVFLOMAG),'PNT_IVA','IVFLOMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_IVFLOMAG),'PNT_IVA','IVCFLOMA');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'IVSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_ROWNUM,'IVCODIVA',this.w_IVCODIVA,'IVTIPCON',"G",'IVCODCON',this.w_CCCONIVA,'IVPERIND',0,'IVTIPREG',this.w_IVTIPREG,'IVNUMREG',this.w_PNNUMREG,'IVIMPONI',this.w_IVIMPONI,'IVIMPIVA',this.w_IVIMPIVA,'IVFLOMAG',this.w_IVFLOMAG,'IVCFLOMA',this.w_IVFLOMAG)
            insert into (i_cTable) (IVSERIAL,CPROWNUM,IVCODIVA,IVTIPCON,IVCODCON,IVPERIND,IVTIPREG,IVNUMREG,IVIMPONI,IVIMPIVA,IVFLOMAG,IVCFLOMA &i_ccchkf. );
               values (;
                 this.w_PNSERIAL;
                 ,this.w_ROWNUM;
                 ,this.w_IVCODIVA;
                 ,"G";
                 ,this.w_CCCONIVA;
                 ,0;
                 ,this.w_IVTIPREG;
                 ,this.w_PNNUMREG;
                 ,this.w_IVIMPONI;
                 ,this.w_IVIMPIVA;
                 ,this.w_IVFLOMAG;
                 ,this.w_IVFLOMAG;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          SELECT DettIva
          ENDSCAN
        endif
      endif
    endif
    * --- Scrive il Dettaglio: Eventuali Differenze di Conversione (Solo alla Fine)
    this.w_DIFCON = 0
    if this.w_TOTDAR<>this.w_TOTAVE
      if this.w_PNCODVAL<>this.w_PNVALNAZ
        this.w_DIFCON = this.w_TOTDAR-this.w_TOTAVE
        if EMPTY(this.oParentObject.w_CONDIC)
          MES[12]=ah_MsgFormat("Conto differenze di conversione non definito")
          this.w_FLERR = .T.
        else
          this.w_PNCODCON = this.oParentObject.w_CONDIC
        endif
      else
        * --- Se valuta corrispettivo uguale a valuta di conto
        *     aggiungo lo sbilancio solo nel castelletto contabile
        if EMPTY(this.oParentObject.w_CONARR)
          MES[19]=ah_MsgFormat("Conto arrotondamenti non definito")
          this.w_FLERR = .T.
        else
          this.w_PNCODCON = this.oParentObject.w_CONARR
        endif
      endif
      if Not this.w_FLERR
        this.w_APPSEZ = IIF(this.w_TOTDAR-this.w_TOTAVE>0, "A", "D")
        this.w_APPVAL = ABS( this.w_TOTDAR-this.w_TOTAVE )
        this.w_PNINICOM = this.w_INICOM
        this.w_PNFINCOM = this.w_FINCOM
        this.Pag5()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    endif
    if this.w_FLERR
      this.w_oERRORLOG.AddMsgLog("Verificare doc. n.: %1 del.: %2", ALLTRIM(STR(this.w_PNNUMDOC,15))+IIF(EMPTY(this.w_PNALFDOC),"","/"+Alltrim(this.w_PNALFDOC)),DTOC(this.w_PNDATDOC))     
      FOR L_i = 1 TO 19
      if NOT EMPTY(MES[L_i])
        this.w_oERRORLOG.AddMsgLogNoTranslate(SPACE(10)+MES[L_i])     
      endif
      ENDFOR
      this.w_oERRORLOG.AddMsgLogNoTranslate(SPACE(50))     
      * --- Abbandona la Registrazione
      * --- Raise
      i_Error="Errore"
      return
    else
      * --- commit
      cp_EndTrs(.t.)
      this.w_OKDOC = this.w_OKDOC + 1
    endif
    if this.w_FLERR=.F. and Not Empty(this.w_PNSERIAL)
      * --- Scrive Flag Contabilizzato sul Documento Origine
      Select dettdocu 
 Go top 
 Scan
      this.w_ROWINC = ROWINC
      * --- Write into INCDCORR
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.INCDCORR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INCDCORR_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.INCDCORR_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"IN_FLCON ="+cp_NullLink(cp_ToStrODBC("S"),'INCDCORR','IN_FLCON');
        +",IN_RIFCO ="+cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'INCDCORR','IN_RIFCO');
            +i_ccchkf ;
        +" where ";
            +"INSERIAL = "+cp_ToStrODBC(this.w_OSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWINC);
               )
      else
        update (i_cTable) set;
            IN_FLCON = "S";
            ,IN_RIFCO = this.w_PNSERIAL;
            &i_ccchkf. ;
         where;
            INSERIAL = this.w_OSERIAL;
            and CPROWNUM = this.w_ROWINC;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      Endscan
    endif
    return


  procedure Pag5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Scrive Dettaglio P.N. e Saldi
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    * --- Inverte la sezione se Negativo..
    this.w_APPSEZ = IIF(this.w_APPVAL<0, IIF(this.w_APPSEZ="D", "A", "D"), this.w_APPSEZ)
    if this.w_APPSEZ="D"
      this.w_PNIMPDAR = ABS(this.w_APPVAL)
      this.w_PNIMPAVE = 0
    else
      this.w_PNIMPDAR = 0
      this.w_PNIMPAVE = ABS(this.w_APPVAL)
    endif
    * --- Eventuali Righe non Valorizzate
    this.w_PNFLZERO = IIF(this.w_PNIMPDAR=0 AND this.w_PNIMPAVE=0, "S", " ")
    this.w_TOTDAR = this.w_TOTDAR + this.w_PNIMPDAR
    this.w_TOTAVE = this.w_TOTAVE + this.w_PNIMPAVE
    if Not EMPTY(this.w_CCDESRIG)
      this.w_PNDESRIG = CALDESPA(this.w_CCDESRIG,@ARPARAM)
    endif
    * --- Insert into PNT_DETT
    i_nConn=i_TableProp[this.PNT_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PNT_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"PNSERIAL"+",CPROWNUM"+",CPROWORD"+",PNTIPCON"+",PNCODCON"+",PNIMPDAR"+",PNIMPAVE"+",PNFLPART"+",PNFLZERO"+",PNDESRIG"+",PNCAURIG"+",PNCODPAG"+",PNFLSALD"+",PNFLSALI"+",PNFLSALF"+",PNINICOM"+",PNFINCOM"+",PNIMPIND"+",PNFLABAN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_DETT','PNSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'PNT_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'PNT_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'PNT_DETT','PNTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'PNT_DETT','PNCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPDAR),'PNT_DETT','PNIMPDAR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNIMPAVE),'PNT_DETT','PNIMPAVE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLPART),'PNT_DETT','PNFLPART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLZERO),'PNT_DETT','PNFLZERO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNDESRIG),'PNT_DETT','PNDESRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCAU),'PNT_DETT','PNCAURIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODPAG),'PNT_DETT','PNCODPAG');
      +","+cp_NullLink(cp_ToStrODBC("+"),'PNT_DETT','PNFLSALD');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALI');
      +","+cp_NullLink(cp_ToStrODBC(" "),'PNT_DETT','PNFLSALF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNINICOM),'PNT_DETT','PNINICOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFINCOM),'PNT_DETT','PNFINCOM');
      +","+cp_NullLink(cp_ToStrODBC(0),'PNT_DETT','PNIMPIND');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNFLABAN),'PNT_DETT','PNFLABAN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'PNSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PNTIPCON',this.w_PNTIPCON,'PNCODCON',this.w_PNCODCON,'PNIMPDAR',this.w_PNIMPDAR,'PNIMPAVE',this.w_PNIMPAVE,'PNFLPART',this.w_PNFLPART,'PNFLZERO',this.w_PNFLZERO,'PNDESRIG',this.w_PNDESRIG,'PNCAURIG',this.w_PNCODCAU,'PNCODPAG',this.w_PNCODPAG)
      insert into (i_cTable) (PNSERIAL,CPROWNUM,CPROWORD,PNTIPCON,PNCODCON,PNIMPDAR,PNIMPAVE,PNFLPART,PNFLZERO,PNDESRIG,PNCAURIG,PNCODPAG,PNFLSALD,PNFLSALI,PNFLSALF,PNINICOM,PNFINCOM,PNIMPIND,PNFLABAN &i_ccchkf. );
         values (;
           this.w_PNSERIAL;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNIMPDAR;
           ,this.w_PNIMPAVE;
           ,this.w_PNFLPART;
           ,this.w_PNFLZERO;
           ,this.w_PNDESRIG;
           ,this.w_PNCODCAU;
           ,this.w_PNCODPAG;
           ,"+";
           ," ";
           ," ";
           ,this.w_PNINICOM;
           ,this.w_PNFINCOM;
           ,0;
           ,this.w_PNFLABAN;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    this.w_PNFLABAN = SPACE(6)
    * --- Aggiorna Saldo
    * --- Try
    local bErr_03AECA48
    bErr_03AECA48=bTrsErr
    this.Try_03AECA48()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- accept error
      bTrsErr=.f.
    endif
    bTrsErr=bTrsErr or bErr_03AECA48
    * --- End
    * --- Write into SALDICON
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"SLDARPER =SLDARPER+ "+cp_ToStrODBC(this.w_PNIMPDAR);
      +",SLAVEPER =SLAVEPER+ "+cp_ToStrODBC(this.w_PNIMPAVE);
          +i_ccchkf ;
      +" where ";
          +"SLTIPCON = "+cp_ToStrODBC(this.w_PNTIPCON);
          +" and SLCODICE = "+cp_ToStrODBC(this.w_PNCODCON);
          +" and SLCODESE = "+cp_ToStrODBC(this.w_PNCODESE);
             )
    else
      update (i_cTable) set;
          SLDARPER = SLDARPER + this.w_PNIMPDAR;
          ,SLAVEPER = SLAVEPER + this.w_PNIMPAVE;
          &i_ccchkf. ;
       where;
          SLTIPCON = this.w_PNTIPCON;
          and SLCODICE = this.w_PNCODCON;
          and SLCODESE = this.w_PNCODESE;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Gestione Analitica
    this.w_MESS_ERR = SPACE(200)
    if w_CCFLANAL="S" And this.w_PNTIPCON="G" And g_PERCCR="S"
      this.w_MESS_ERR = GSAR_BRA(This,this.w_PNSERIAL, this.w_CPROWNUM, this.w_PNTIPCON, this.w_PNCODCON, this.w_APPVAL)
      if Not EMPTY(this.w_MESS_ERR)
        ah_ErrorMsg("%1",,"",this.w_MESS_ERR)
      endif
    endif
  endproc
  proc Try_03AECA48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICON
    i_nConn=i_TableProp[this.SALDICON_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICON_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICON_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLTIPCON"+",SLCODICE"+",SLCODESE"+",SLDARPRO"+",SLAVEPRO"+",SLDARPER"+",SLAVEPER"+",SLDARINI"+",SLAVEINI"+",SLDARFIN"+",SLAVEFIN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_PNTIPCON),'SALDICON','SLTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODCON),'SALDICON','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_PNCODESE),'SALDICON','SLCODESE');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPRO');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEPER');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEINI');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLDARFIN');
      +","+cp_NullLink(cp_ToStrODBC(0),'SALDICON','SLAVEFIN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLTIPCON',this.w_PNTIPCON,'SLCODICE',this.w_PNCODCON,'SLCODESE',this.w_PNCODESE,'SLDARPRO',0,'SLAVEPRO',0,'SLDARPER',0,'SLAVEPER',0,'SLDARINI',0,'SLAVEINI',0,'SLDARFIN',0,'SLAVEFIN',0)
      insert into (i_cTable) (SLTIPCON,SLCODICE,SLCODESE,SLDARPRO,SLAVEPRO,SLDARPER,SLAVEPER,SLDARINI,SLAVEINI,SLDARFIN,SLAVEFIN &i_ccchkf. );
         values (;
           this.w_PNTIPCON;
           ,this.w_PNCODCON;
           ,this.w_PNCODESE;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           ,0;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elimina Cursori di Appoggio
    if USED("CONTADOC")
      SELECT CONTADOC
      USE
    endif
    if USED("DettDocu")
      SELECT DettDocu
      USE
    endif
    if USED("DettIva")
      SELECT DettIva
      USE
    endif
    if USED("AppCur")
      SELECT AppCur
      USE
    endif
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
    if USED("reg_iva")
      SELECT reg_iva
      USE
    endif
    i_retcode = 'stop'
    return
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola castelletto IVA
    this.w_TOTVAL = 0
    FOR L_i = 1 TO 6
    y = ALLTRIM(STR(L_i))
    AI[L_i, 1] = NVL(MVAFLOM&y, " ")
    AI[L_i, 2] = NVL(MVACIVA&y, SPACE(5))
    AI[L_i, 3] = NVL(MVAIMPN&y, 0)
    AI[L_i, 4] = NVL(MVAIMPS&y, 0)
    * --- Rivalorizzo AI[L_i, 1] (se imponibile e iva = 0 per righe zero) ="Z"
    if AI[L_i, 3] = 0 AND AI[L_i, 4]=0
      AI[L_i, 1] = "Z"
    endif
    * --- Legge %Iva e %Indetraibile
    this.w_PERIVA = 0
    if EMPTY(this.w_CODIVE) AND NOT EMPTY(AI[L_i, 2])
      this.w_APPO = AI[L_i, 2]
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIVA"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_APPO);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIVA;
          from (i_cTable) where;
              IVCODIVA = this.w_APPO;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      SELECT CONTADOC
    endif
    AI[L_i, 5] = this.w_PERIVA
    * --- Totale Documento (in Valuta)  (Le righe Sconto Merce non vengono memorizzate)
    this.w_TOTVAL = this.w_TOTVAL + (IIF(AI[L_i, 1] $ "IE", 0, AI[L_i, 3]) + IIF(AI[L_i, 1] = "E", 0, AI[L_i, 4]))
    if this.w_MVCODVAL<>this.w_PNVALNAZ
      * --- Converte in Moneta di Conto
      AI[L_i, 3] = VAL2MON(AI[L_i, 3],this.w_MVCAOVAL,this.w_CAONAZ,this.w_MVDATDOC,this.w_PNVALNAZ, this.w_DECTOP)
      AI[L_i, 4] = VAL2MON(AI[L_i, 4],this.w_MVCAOVAL,this.w_CAONAZ,this.w_MVDATDOC,this.w_PNVALNAZ, this.w_DECTOP)
      SELECT CONTADOC
    endif
    ENDFOR
    * --- Totale Documento in Moneta di Conto
    if this.w_MVCODVAL<>this.w_PNVALNAZ
      this.w_TOTVAL = VAL2MON(this.w_TOTVAL,this.w_MVCAOVAL,this.w_CAONAZ,this.w_MVDATDOC,this.w_PNVALNAZ, this.w_DECTOP)
    endif
    * --- Calcola Totali IVA
    FOR L_i = 1 TO 6
    * --- L'IVA Effettiva e' calcolata solo per la Parte Effettivamente Incassata
    if AI[L_i, 5] >0
      this.w_IMPIV = (AI[L_i, 4]*this.w_TOTCAS)/this.w_TOTVAL
      this.w_IMPON = (AI[L_i, 3]*this.w_TOTCAS)/this.w_TOTVAL
       
 AI[L_i, 4] = cp_ROUND((this.w_IMPON*AI[L_i, 5])/100, this.w_DECTOP) 
 AI[L_i, 3] = cp_ROUND((this.w_IMPON+this.w_IMPIV)-AI[L_i, 4],this.w_DECTOP)
    else
       
 AI[L_i, 3] = cp_ROUND((AI[L_i, 3]*this.w_TOTCAS)/this.w_TOTVAL, this.w_DECTOP) 
 AI[L_i, 4] = cp_ROUND((AI[L_i, 4]*this.w_TOTCAS)/this.w_TOTVAL, this.w_DECTOP)
    endif
    this.w_IMPON = this.w_IMPON-AI[L_i, 4] -AI[L_i, 3]
    * --- Totale IVA Detraibile effettivamente Incassata
    this.w_IVADET = this.w_IVADET + AI[L_i, 4]
    this.w_IVASOSP = this.w_IVASOSP + AI[L_i, 4]
    INSERT INTO DettIva (FLOMAG, CODIVA, TOTIMP, TOTIVA, PERIVA) ;
    VALUES (AI[L_i, 1] , AI[L_i, 2] , AI[L_i, 3] , AI[L_i, 4] , AI[L_i, 5])
    ENDFOR
    if this.w_DIFCON<>0 AND NOT EMPTY(g_COIDIF)
      * --- Se presente Differenza di Conversione aggiunge una riga nel castelletto Iva
      this.w_APPO = g_COIDIF
      * --- Read from VOCIIVA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.VOCIIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2],.t.,this.VOCIIVA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "IVPERIND"+;
          " from "+i_cTable+" VOCIIVA where ";
              +"IVCODIVA = "+cp_ToStrODBC(this.w_APPO);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          IVPERIND;
          from (i_cTable) where;
              IVCODIVA = this.w_APPO;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_PERIND = NVL(cp_ToDate(_read_.IVPERIND),cp_NullValue(_read_.IVPERIND))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_IVCONTRO = IIF(g_DETCON="S", this.oParentObject.w_CONDIC, SPACE(15))
      this.w_ROWNUM = this.w_ROWNUM + 1
      * --- Insert into PNT_IVA
      i_nConn=i_TableProp[this.PNT_IVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PNT_IVA_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PNT_IVA_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"IVSERIAL"+",CPROWNUM"+",IVCODIVA"+",IVTIPCON"+",IVCODCON"+",IVPERIND"+",IVTIPREG"+",IVNUMREG"+",IVIMPONI"+",IVIMPIVA"+",IVFLOMAG"+",IVCFLOMA"+",IVCONTRO"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_PNSERIAL),'PNT_IVA','IVSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(this.w_ROWNUM),'PNT_IVA','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_APPO),'PNT_IVA','IVCODIVA');
        +","+cp_NullLink(cp_ToStrODBC("G"),'PNT_IVA','IVTIPCON');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CCCONIVA),'PNT_IVA','IVCODCON');
        +","+cp_NullLink(cp_ToStrODBC(w_PERIND),'PNT_IVA','IVPERIND');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNTIPREG),'PNT_IVA','IVTIPREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_PNNUMREG),'PNT_IVA','IVNUMREG');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DIFCON),'PNT_IVA','IVIMPONI');
        +","+cp_NullLink(cp_ToStrODBC(0),'PNT_IVA','IVIMPIVA');
        +","+cp_NullLink(cp_ToStrODBC("X"),'PNT_IVA','IVFLOMAG');
        +","+cp_NullLink(cp_ToStrODBC("X"),'PNT_IVA','IVCFLOMA');
        +","+cp_NullLink(cp_ToStrODBC(this.w_IVCONTRO),'PNT_IVA','IVCONTRO');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'IVSERIAL',this.w_PNSERIAL,'CPROWNUM',this.w_ROWNUM,'IVCODIVA',this.w_APPO,'IVTIPCON',"G",'IVCODCON',this.w_CCCONIVA,'IVPERIND',w_PERIND,'IVTIPREG',this.w_PNTIPREG,'IVNUMREG',this.w_PNNUMREG,'IVIMPONI',this.w_DIFCON,'IVIMPIVA',0,'IVFLOMAG',"X",'IVCFLOMA',"X")
        insert into (i_cTable) (IVSERIAL,CPROWNUM,IVCODIVA,IVTIPCON,IVCODCON,IVPERIND,IVTIPREG,IVNUMREG,IVIMPONI,IVIMPIVA,IVFLOMAG,IVCFLOMA,IVCONTRO &i_ccchkf. );
           values (;
             this.w_PNSERIAL;
             ,this.w_ROWNUM;
             ,this.w_APPO;
             ,"G";
             ,this.w_CCCONIVA;
             ,w_PERIND;
             ,this.w_PNTIPREG;
             ,this.w_PNNUMREG;
             ,this.w_DIFCON;
             ,0;
             ,"X";
             ,"X";
             ,this.w_IVCONTRO;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,16)]
    this.cWorkTables[1]='CAU_CONT'
    this.cWorkTables[2]='PNT_MAST'
    this.cWorkTables[3]='INC_CORR'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='PNT_DETT'
    this.cWorkTables[6]='SALDICON'
    this.cWorkTables[7]='VOCIIVA'
    this.cWorkTables[8]='PNT_IVA'
    this.cWorkTables[9]='CONTI'
    this.cWorkTables[10]='PAR_TITE'
    this.cWorkTables[11]='AZIENDA'
    this.cWorkTables[12]='ATTIDETT'
    this.cWorkTables[13]='ESERCIZI'
    this.cWorkTables[14]='RIFI_CADO'
    this.cWorkTables[15]='DOC_RATE'
    this.cWorkTables[16]='INCDCORR'
    return(this.OpenAllTables(16))

  proc CloseCursors()
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    if used('_Curs_DOC_RATE')
      use in _Curs_DOC_RATE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
