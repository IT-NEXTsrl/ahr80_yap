* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_mbc                                                        *
*              Castelletto banche                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_68]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-10-08                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgste_mbc")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgste_mbc")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgste_mbc")
  return

* --- Class definition
define class tgste_mbc as StdPCForm
  Width  = 450
  Height = 207
  Top    = 10
  Left   = 10
  cComment = "Castelletto banche"
  cPrg = "gste_mbc"
  HelpContextID=113914985
  add object cnt as tcgste_mbc
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgste_mbc as PCContext
  w_CBCAUDIS = space(5)
  w_CBCODBAN = space(15)
  w_DECTOT = 0
  w_CALCPICT = 0
  w_CBIMPPRO = 0
  w_CBCOMEFF = 0
  w_DESCRI = space(35)
  w_CONSBF = space(1)
  w_CONSBF1 = space(1)
  w_OBTEST = space(8)
  w_DTOBSO = space(8)
  proc Save(i_oFrom)
    this.w_CBCAUDIS = i_oFrom.w_CBCAUDIS
    this.w_CBCODBAN = i_oFrom.w_CBCODBAN
    this.w_DECTOT = i_oFrom.w_DECTOT
    this.w_CALCPICT = i_oFrom.w_CALCPICT
    this.w_CBIMPPRO = i_oFrom.w_CBIMPPRO
    this.w_CBCOMEFF = i_oFrom.w_CBCOMEFF
    this.w_DESCRI = i_oFrom.w_DESCRI
    this.w_CONSBF = i_oFrom.w_CONSBF
    this.w_CONSBF1 = i_oFrom.w_CONSBF1
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_DTOBSO = i_oFrom.w_DTOBSO
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_CBCAUDIS = this.w_CBCAUDIS
    i_oTo.w_CBCODBAN = this.w_CBCODBAN
    i_oTo.w_DECTOT = this.w_DECTOT
    i_oTo.w_CALCPICT = this.w_CALCPICT
    i_oTo.w_CBIMPPRO = this.w_CBIMPPRO
    i_oTo.w_CBCOMEFF = this.w_CBCOMEFF
    i_oTo.w_DESCRI = this.w_DESCRI
    i_oTo.w_CONSBF = this.w_CONSBF
    i_oTo.w_CONSBF1 = this.w_CONSBF1
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_DTOBSO = this.w_DTOBSO
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgste_mbc as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 450
  Height = 207
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=113914985
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=11

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  BAN_CAST_IDX = 0
  COC_MAST_IDX = 0
  CAU_DIST_IDX = 0
  cFile = "BAN_CAST"
  cKeySelect = "CBCAUDIS"
  cKeyWhere  = "CBCAUDIS=this.w_CBCAUDIS"
  cKeyDetail  = "CBCAUDIS=this.w_CBCAUDIS and CBCODBAN=this.w_CBCODBAN"
  cKeyWhereODBC = '"CBCAUDIS="+cp_ToStrODBC(this.w_CBCAUDIS)';

  cKeyDetailWhereODBC = '"CBCAUDIS="+cp_ToStrODBC(this.w_CBCAUDIS)';
      +'+" and CBCODBAN="+cp_ToStrODBC(this.w_CBCODBAN)';

  cKeyWhereODBCqualified = '"BAN_CAST.CBCAUDIS="+cp_ToStrODBC(this.w_CBCAUDIS)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cPrg = "gste_mbc"
  cComment = "Castelletto banche"
  i_nRowNum = 0
  i_nRowPerPage = 8
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_CBCAUDIS = space(5)
  w_CBCODBAN = space(15)
  w_DECTOT = 0
  w_CALCPICT = 0
  w_CBIMPPRO = 0
  w_CBCOMEFF = 0
  w_DESCRI = space(35)
  w_CONSBF = space(1)
  w_CONSBF1 = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_DTOBSO = ctod('  /  /  ')
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_mbcPag1","gste_mbc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='CAU_DIST'
    this.cWorkTables[3]='BAN_CAST'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.BAN_CAST_IDX,5],7]
    this.nPostItConn=i_TableProp[this.BAN_CAST_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgste_mbc'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_1_joined
    link_2_1_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from BAN_CAST where CBCAUDIS=KeySet.CBCAUDIS
    *                            and CBCODBAN=KeySet.CBCODBAN
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.BAN_CAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CAST_IDX,2],this.bLoadRecFilter,this.BAN_CAST_IDX,"gste_mbc")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('BAN_CAST')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "BAN_CAST.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' BAN_CAST '
      link_2_1_joined=this.AddJoinedLink_2_1(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'CBCAUDIS',this.w_CBCAUDIS  )
      select * from (i_cTable) BAN_CAST where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_CONSBF1 = space(1)
        .w_DTOBSO = ctod("  /  /  ")
        .w_CBCAUDIS = NVL(CBCAUDIS,space(5))
        .w_CONSBF = this.oparentobject .w_CACONSBF
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .w_OBTEST = THIS.OPARENTOBJECT.w_OBTEST
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'BAN_CAST')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DESCRI = space(35)
          .w_CBCODBAN = NVL(CBCODBAN,space(15))
          if link_2_1_joined
            this.w_CBCODBAN = NVL(BACODBAN201,NVL(this.w_CBCODBAN,space(15)))
            this.w_DESCRI = NVL(BADESCRI201,space(35))
            this.w_CONSBF1 = NVL(BACONSBF201,space(1))
            this.w_DTOBSO = NVL(cp_ToDate(BADTOBSO201),ctod("  /  /  "))
          else
          .link_2_1('Load')
          endif
        .w_DECTOT = This.OparentObject .w_DECTOT
        .w_CALCPICT = DEFPIC(.w_DECTOT)
          .w_CBIMPPRO = NVL(CBIMPPRO,0)
          .w_CBCOMEFF = NVL(CBCOMEFF,0)
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CBCODBAN with .w_CBCODBAN
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_CONSBF = this.oparentobject .w_CACONSBF
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .w_OBTEST = THIS.OPARENTOBJECT.w_OBTEST
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    replace I_SRV    with "A"
    with this
      .w_CBCAUDIS=space(5)
      .w_CBCODBAN=space(15)
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_CBIMPPRO=0
      .w_CBCOMEFF=0
      .w_DESCRI=space(35)
      .w_CONSBF=space(1)
      .w_CONSBF1=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_DTOBSO=ctod("  /  /  ")
      if .cFunction<>"Filter"
        .DoRTCalc(1,2,.f.)
        if not(empty(.w_CBCODBAN))
         .link_2_1('Full')
        endif
        .w_DECTOT = This.OparentObject .w_DECTOT
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .DoRTCalc(5,7,.f.)
        .w_CONSBF = this.oparentobject .w_CACONSBF
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .DoRTCalc(9,9,.f.)
        .w_OBTEST = THIS.OPARENTOBJECT.w_OBTEST
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'BAN_CAST')
    this.DoRTCalc(11,11,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oObj_1_5.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'BAN_CAST',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.BAN_CAST_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CBCAUDIS,"CBCAUDIS",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CBCODBAN C(15);
      ,t_CBIMPPRO N(18,4);
      ,t_CBCOMEFF N(18,4);
      ,t_DESCRI C(35);
      ,CBCODBAN C(15);
      ,t_DECTOT N(1);
      ,t_CALCPICT N(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgste_mbcbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCBCODBAN_2_1.controlsource=this.cTrsName+'.t_CBCODBAN'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCBIMPPRO_2_4.controlsource=this.cTrsName+'.t_CBIMPPRO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCBCOMEFF_2_5.controlsource=this.cTrsName+'.t_CBCOMEFF'
    this.oPgFRm.Page1.oPag.oDESCRI_2_6.controlsource=this.cTrsName+'.t_DESCRI'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(131)
    this.AddVLine(273)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCBCODBAN_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.BAN_CAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CAST_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.BAN_CAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CAST_IDX,2])
      *
      * insert into BAN_CAST
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'BAN_CAST')
        i_extval=cp_InsertValODBCExtFlds(this,'BAN_CAST')
        i_cFldBody=" "+;
                  "(CBCAUDIS,CBCODBAN,CBIMPPRO,CBCOMEFF,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_CBCAUDIS)+","+cp_ToStrODBCNull(this.w_CBCODBAN)+","+cp_ToStrODBC(this.w_CBIMPPRO)+","+cp_ToStrODBC(this.w_CBCOMEFF)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'BAN_CAST')
        i_extval=cp_InsertValVFPExtFlds(this,'BAN_CAST')
        cp_CheckDeletedKey(i_cTable,0,'CBCAUDIS',this.w_CBCAUDIS,'CBCODBAN',this.w_CBCODBAN)
        INSERT INTO (i_cTable) (;
                   CBCAUDIS;
                  ,CBCODBAN;
                  ,CBIMPPRO;
                  ,CBCOMEFF;
                  ,CPCCCHK &i_extfld.) VALUES (;
                  this.w_CBCAUDIS;
                  ,this.w_CBCODBAN;
                  ,this.w_CBIMPPRO;
                  ,this.w_CBCOMEFF;
                  ,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.BAN_CAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CAST_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (not(Empty(t_CBCODBAN))) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'BAN_CAST')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CBCODBAN="+cp_ToStrODBC(&i_TN.->CBCODBAN)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'BAN_CAST')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CBCODBAN=&i_TN.->CBCODBAN;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (not(Empty(t_CBCODBAN))) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+;
                            " and CBCODBAN="+cp_ToStrODBC(&i_TN.->CBCODBAN)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere;
                            and CBCODBAN=&i_TN.->CBCODBAN;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = 0
              replace CBCODBAN with this.w_CBCODBAN
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update BAN_CAST
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'BAN_CAST')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CBIMPPRO="+cp_ToStrODBC(this.w_CBIMPPRO)+;
                     ",CBCOMEFF="+cp_ToStrODBC(this.w_CBCOMEFF)+;
                     ",CBCODBAN="+cp_ToStrODBC(this.w_CBCODBAN)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+;
                             " and CBCODBAN="+cp_ToStrODBC(CBCODBAN)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'BAN_CAST')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CBIMPPRO=this.w_CBIMPPRO;
                     ,CBCOMEFF=this.w_CBCOMEFF;
                     ,CBCODBAN=this.w_CBCODBAN;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                                      and CBCODBAN=&i_TN.->CBCODBAN;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- gste_mbc
    * --- Controlli Finali
    this.NotifyEvent('ControlliFinali')
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.BAN_CAST_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CAST_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (not(Empty(t_CBCODBAN))) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete BAN_CAST
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+;
                            " and CBCODBAN="+cp_ToStrODBC(&i_TN.->CBCODBAN)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere;
                              and CBCODBAN=&i_TN.->CBCODBAN;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (not(Empty(t_CBCODBAN))) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.BAN_CAST_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CAST_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
          .w_DECTOT = This.OparentObject .w_DECTOT
          .w_CALCPICT = DEFPIC(.w_DECTOT)
        .DoRTCalc(5,7,.t.)
          .w_CONSBF = this.oparentobject .w_CACONSBF
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .DoRTCalc(9,9,.t.)
          .w_OBTEST = THIS.OPARENTOBJECT.w_OBTEST
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(11,11,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DECTOT with this.w_DECTOT
      replace t_CALCPICT with this.w_CALCPICT
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_5.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_5.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CBCODBAN
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CBCODBAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_CBCODBAN)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACONSBF,BADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_CBCODBAN))
          select BACODBAN,BADESCRI,BACONSBF,BADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CBCODBAN)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_CBCODBAN)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACONSBF,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_CBCODBAN)+"%");

            select BACODBAN,BADESCRI,BACONSBF,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CBCODBAN) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oCBCODBAN_2_1'),i_cWhere,'GSTE_ACB',"Elenchi banche",'GSTE_MCB.COC_MAST_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACONSBF,BADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BACONSBF,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CBCODBAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BACONSBF,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_CBCODBAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_CBCODBAN)
            select BACODBAN,BADESCRI,BACONSBF,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CBCODBAN = NVL(_Link_.BACODBAN,space(15))
      this.w_DESCRI = NVL(_Link_.BADESCRI,space(35))
      this.w_CONSBF1 = NVL(_Link_.BACONSBF,space(1))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CBCODBAN = space(15)
      endif
      this.w_DESCRI = space(35)
      this.w_CONSBF1 = space(1)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST) AND (this.oparentobject .w_CACONSBF=.w_CONSBF1) OR this.oparentobject .w_CACONSBF='E'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice banca incongruente o obsoleta")
        endif
        this.w_CBCODBAN = space(15)
        this.w_DESCRI = space(35)
        this.w_CONSBF1 = space(1)
        this.w_DTOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CBCODBAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_1(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.COC_MAST_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_1.BACODBAN as BACODBAN201"+ ",link_2_1.BADESCRI as BADESCRI201"+ ",link_2_1.BACONSBF as BACONSBF201"+ ",link_2_1.BADTOBSO as BADTOBSO201"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_1 on BAN_CAST.CBCODBAN=link_2_1.BACODBAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_1"
          i_cKey=i_cKey+'+" and BAN_CAST.CBCODBAN=link_2_1.BACODBAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDESCRI_2_6.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_2_6.value=this.w_DESCRI
      replace t_DESCRI with this.oPgFrm.Page1.oPag.oDESCRI_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oDTOBSO_1_7.value==this.w_DTOBSO)
      this.oPgFrm.Page1.oPag.oDTOBSO_1_7.value=this.w_DTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCBCODBAN_2_1.value==this.w_CBCODBAN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCBCODBAN_2_1.value=this.w_CBCODBAN
      replace t_CBCODBAN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCBCODBAN_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCBIMPPRO_2_4.value==this.w_CBIMPPRO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCBIMPPRO_2_4.value=this.w_CBIMPPRO
      replace t_CBIMPPRO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCBIMPPRO_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCBCOMEFF_2_5.value==this.w_CBCOMEFF)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCBCOMEFF_2_5.value=this.w_CBCOMEFF
      replace t_CBCOMEFF with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCBCOMEFF_2_5.value
    endif
    cp_SetControlsValueExtFlds(this,'BAN_CAST')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   not((EMPTY(.w_DTOBSO) OR .w_DTOBSO>.w_OBTEST) AND (this.oparentobject .w_CACONSBF=.w_CONSBF1) OR this.oparentobject .w_CACONSBF='E') and not(empty(.w_CBCODBAN)) and (not(Empty(.w_CBCODBAN)))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCBCODBAN_2_1
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Codice banca incongruente o obsoleta")
      endcase
      if not(Empty(.w_CBCODBAN))
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(not(Empty(t_CBCODBAN)))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CBCODBAN=space(15)
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_CBIMPPRO=0
      .w_CBCOMEFF=0
      .w_DESCRI=space(35)
      .DoRTCalc(1,2,.f.)
      if not(empty(.w_CBCODBAN))
        .link_2_1('Full')
      endif
        .w_DECTOT = This.OparentObject .w_DECTOT
        .w_CALCPICT = DEFPIC(.w_DECTOT)
    endwith
    this.DoRTCalc(5,11,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CBCODBAN = t_CBCODBAN
    this.w_DECTOT = t_DECTOT
    this.w_CALCPICT = t_CALCPICT
    this.w_CBIMPPRO = t_CBIMPPRO
    this.w_CBCOMEFF = t_CBCOMEFF
    this.w_DESCRI = t_DESCRI
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CBCODBAN with this.w_CBCODBAN
    replace t_DECTOT with this.w_DECTOT
    replace t_CALCPICT with this.w_CALCPICT
    replace t_CBIMPPRO with this.w_CBIMPPRO
    replace t_CBCOMEFF with this.w_CBCOMEFF
    replace t_DESCRI with this.w_DESCRI
    if i_srv='A'
      replace CBCODBAN with this.w_CBCODBAN
    endif
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgste_mbcPag1 as StdContainer
  Width  = 446
  height = 207
  stdWidth  = 446
  stdheight = 207
  resizeYpos=149
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oObj_1_5 as cp_runprogram with uid="ACNJUZMLYT",left=13, top=209, width=160,height=20,;
    caption='GSTE_BBC',;
   bGlobalFont=.t.,;
    prg="GSTE_BBC",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 24142761

  add object oDTOBSO_1_7 as StdField with uid="MPEQKOCIXX",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DTOBSO", cQueryName = "DTOBSO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    HelpContextID = 38988746,;
   bGlobalFont=.t.,;
    Height=21, Width=74, Left=367, Top=181


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=5, top=3, width=425,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=3,Field1="CBCODBAN",Label1="Codice banca",Field2="CBIMPPRO",Label2="Importo proposto",Field3="CBCOMEFF",Label3="Commissioni per effetto",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 202175866

  add object oStr_1_2 as StdString with uid="UVBAGATYID",Visible=.t., Left=11, Top=181,;
    Alignment=1, Width=94, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-5,top=22,;
    width=421+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-4,top=23,width=420+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*8*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='COC_MAST|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDESCRI_2_6.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='COC_MAST'
        oDropInto=this.oBodyCol.oRow.oCBCODBAN_2_1
    endcase
    return(oDropInto)
  EndFunc


  add object oDESCRI_2_6 as StdTrsField with uid="EKAXUBOTXB",rtseq=7,rtrep=.t.,;
    cFormVar="w_DESCRI",value=space(35),enabled=.f.,;
    HelpContextID = 140622538,;
    cTotal="", bFixedPos=.t., cQueryName = "DESCRI",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=109, Top=181, InputMask=replicate('X',35)

  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgste_mbcBodyRow as CPBodyRowCnt
  Width=411
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCBCODBAN_2_1 as StdTrsField with uid="TSFNKOBMDX",rtseq=2,rtrep=.t.,;
    cFormVar="w_CBCODBAN",value=space(15),isprimarykey=.t.,;
    HelpContextID = 3587468,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Codice banca incongruente o obsoleta",;
   bGlobalFont=.t.,;
    Height=17, Width=124, Left=-2, Top=0, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_CBCODBAN"

  func oCBCODBAN_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oCBCODBAN_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oCBCODBAN_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    if not(this.parent.oCBCODBAN_2_1.readonly and this.parent.oCBCODBAN_2_1.isprimarykey)
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oCBCODBAN_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Elenchi banche",'GSTE_MCB.COC_MAST_VZM',this.parent.oContained
   endif
  endproc
  proc oCBCODBAN_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_CBCODBAN
    i_obj.ecpSave()
  endproc

  add object oCBIMPPRO_2_4 as StdTrsField with uid="ZWYIWVMQSM",rtseq=5,rtrep=.t.,;
    cFormVar="w_CBIMPPRO",value=0,;
    HelpContextID = 243769973,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=126, Top=0, cSayPict=[v_PV(32+VVL)], cGetPict=[v_GV(32+VVL)]

  add object oCBCOMEFF_2_5 as StdTrsField with uid="LZDUWOWENZ",rtseq=6,rtrep=.t.,;
    cFormVar="w_CBCOMEFF",value=0,;
    HelpContextID = 56181356,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=139, Left=267, Top=0, cSayPict=[v_PV(32+VVL)], cGetPict=[v_GV(32+VVL)]
  add object oLast as LastKeyMover
  * ---
  func oCBCODBAN_2_1.When()
    return(.t.)
  proc oCBCODBAN_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCBCODBAN_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=7
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_mbc','BAN_CAST','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".CBCAUDIS=BAN_CAST.CBCAUDIS";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
