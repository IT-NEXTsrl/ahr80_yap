* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bea                                                        *
*              Evasione componenti da documenti                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_545]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-01-25                                                      *
* Last revis.: 2016-05-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bea",oParentObject)
return(i_retval)

define class tgsar_bea as StdBatch
  * --- Local variables
  w_oMessLog = .NULL.
  w_oPartLog = .NULL.
  w_oMessLog1 = .NULL.
  w_oPartLog1 = .NULL.
  w_oERRORLOG = .NULL.
  w_PROG = .NULL.
  w_RITENTA = .f.
  w_OKPFI = .f.
  w_OKCOD = .f.
  w_DOCEVA = space(10)
  w_TOTCOD = 0
  w_RIGCOD = 0
  w_TROV = .f.
  w_RECINS = .f.
  w_DBRK = 0
  w_ROWPAD = 0
  w_SERIAL = space(10)
  w_DBCODINI = space(20)
  w_DBCODFIN = space(20)
  w_MAXLEVEL = 0
  w_VERIFICA = space(1)
  w_FILSTAT = space(1)
  w_DATFIL = ctod("  /  /  ")
  w_NUMLEV = 0
  w_UNIMIS = space(3)
  w_QTAMOV = 0
  w_QTAUM1 = 0
  w_COEIMP = space(15)
  w_TDFLESPL = space(1)
  w_VALCOM = space(1)
  w_FLESPL = space(1)
  w_ARTCOM = space(20)
  w_CODCOM = space(20)
  w_DITIPVAL = space(1)
  w_DINUMINV = space(6)
  w_DICODESE = space(4)
  w_MMTIPCON = space(1)
  w_MMCODCON = space(15)
  w_MVNUMERO = 0
  w_MMCODICE = space(20)
  w_MMCODART = space(20)
  w_MMUNIMIS = space(3)
  w_MMQTAMOV = 0
  w_MMQTAUM1 = 0
  w_CONFERMA = .f.
  w_MMCAUCOD = space(5)
  w_MMCAUPFI = space(5)
  w_MMCAMPFI = space(5)
  w_MMCAMCOD = space(5)
  w_MMDATREG = ctod("  /  /  ")
  w_MMDATEVA = ctod("  /  /  ")
  w_MMTCOLIS = space(5)
  w_PDCRIVAL = space(1)
  w_PDESEINV = space(4)
  w_PDNUMINV = space(6)
  w_PDCODLIS = space(5)
  w_QTAOR1 = 0
  w_QTAORI = 0
  w_CODAZI = space(5)
  w_OKDOC = 0
  w_NUDOC = 0
  w_FLEXPL = space(1)
  w_TDFLEXPL = space(1)
  w_EXPAUT = space(1)
  w_MVRIFESC = space(10)
  w_ORIGIN = space(1)
  w_CAMCOC = space(5)
  w_CAMCOP = space(5)
  w_ACCEDE = .f.
  w_FLCOSINV = space(1)
  w_TIPCOS = space(1)
  QTC = 0
  w_LOTDIF = space(1)
  w_LOTDIF2 = space(1)
  DR = space(10)
  w_LOOP = 0
  w_RIFESP = space(10)
  w_FLCOM1 = space(1)
  w_FLCOM2 = space(1)
  w_MAGORI = space(5)
  w_MATORI = space(5)
  w_MAGTER = space(5)
  w_MAGPRE = space(5)
  w_MAGDES_P = space(5)
  w_MATDES_P = space(5)
  w_MAGDES_C = space(5)
  w_MATDES_C = space(5)
  w_FLMGPR_P = space(1)
  w_FLMTPR_P = space(1)
  w_FLMGPR_C = space(1)
  w_FLMTPR_C = space(1)
  w_MVFLELAN = space(1)
  w_MV_SEGNO = space(1)
  w_VOCECR = space(1)
  w_CODTLIS = space(5)
  w_OLDTES = space(35)
  w_TDCOSEPL = space(1)
  w_PADRE = .NULL.
  w_GSAR_MIR = .NULL.
  w_GSAR_MIP = .NULL.
  w_TIPIMB_R = space(1)
  w_TIPIMB_P = space(1)
  w_CODLIN = space(3)
  w_MAXROW = 0
  w_CODICE = space(20)
  w_CODART = space(20)
  w_DESART = space(40)
  w_IUNIMIS = space(3)
  w_IQTAMOV = 0
  w_RECNUM = 0
  w_TESTART = space(20)
  w_DISPAD = space(20)
  w_NUMRIG = 0
  w_QTANET = 0
  w_ARTPAD = space(20)
  w_PFLANAL = space(1)
  w_OLD_STOPFK = .f.
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_SERORI = space(10)
  w_ROWORI = 0
  w_DOCRIG = .f.
  w_ANNULLA = .f.
  w_RESOCON1 = space(0)
  w_MESBLOK = space(0)
  w_DAIM = .f.
  w_OK = .f.
  w_Imballo = space(20)
  w_QtaSal = 0
  w_OLD_STOPFK = .f.
  w_ROWCOM = 0
  w_FLGCOM = space(1)
  w_ARTPAD = space(20)
  w_DESCOD = space(40)
  w_DESSUP = space(40)
  w_CREAESP = .f.
  w_AGGSAL = space(10)
  w_AGGSAL1 = space(10)
  w_TIPREC = space(1)
  w_ARTPAD = space(20)
  w_QUANTI = 0
  w_TESTANAL = .f.
  w_ARTPAD = space(20)
  w_MVFLSCOM = space(1)
  w_DATCOM = ctod("  /  /  ")
  w_OK = .f.
  w_ROWDISP = 0
  w_PRODOC = space(2)
  w_FLPDOC = space(1)
  w_QTAIMP = 0
  w_QTAIM1 = 0
  w_ORQTAEVA = 0
  w_ORQTAMOV = 0
  w_ORQTAUM1 = 0
  w_ORQTAEV1 = 0
  w_ORFLEVAS = space(1)
  w_MVFLULCA = space(1)
  w_MVFLULPV = space(1)
  w_MVVALULT = 0
  w_RET = .NULL.
  pDOCINFO = .NULL.
  w_MVTIPDOC = space(5)
  w_MVTIPCON = space(1)
  w_MVNUMREG = 0
  w_MVFLVEAC = space(1)
  w_MVEMERIC = space(1)
  w_MVCLADOC = space(2)
  w_MVALFDOC = space(10)
  w_MVANNDOC = space(4)
  w_MVNUMEST = 0
  w_MVALFEST = space(10)
  w_MVFLPROV = space(1)
  w_MVPRP = space(2)
  w_MVANNPRO = space(4)
  w_MVDATCIV = ctod("  /  /  ")
  w_MVPRD = space(2)
  w_MVFLACCO = space(1)
  w_MVFLINTE = space(1)
  w_MVCAUCON = space(5)
  w_MVTFRAGG = space(1)
  w_MVNUMDOC = 0
  w_MVSERIAL = space(10)
  w_MVDATREG = ctod("  /  /  ")
  w_MVCODUTE = 0
  w_MVCODESE = space(4)
  w_CPROWNUM = 0
  w_MVNUMRIF = 0
  w_CPROWORD = 0
  w_MVCONTRA = space(15)
  w_MVIVAINC = space(5)
  w_MVSPEINC = 0
  w_MVSERRIF = space(10)
  w_MVROWRIF = 0
  w_MVFLARIF = space(1)
  w_MVFLERIF = space(1)
  w_MVFLRINC = space(1)
  w_MVFLSALD = space(1)
  w_FLGVAL = space(1)
  w_MVTIPRIG = space(1)
  w_MVCODLIS = space(5)
  w_MVSPEIMB = 0
  w_MVFLORDI = space(1)
  w_MVCODICE = space(20)
  w_MVQTAMOV = 0
  w_MVTINCOM = ctod("  /  /  ")
  w_MVIVAIMB = space(5)
  w_MVFLIMPE = space(1)
  w_MVCODART = space(20)
  w_MVQTAUM1 = 0
  w_MVACCONT = 0
  w_MVFLRIMB = space(1)
  w_MVCODMAG = space(5)
  w_MFLCASC = space(1)
  w_MFLORDI = space(1)
  w_MFLIMPE = space(1)
  w_MFLRISE = space(1)
  w_MVCODMAT = space(5)
  w_MCAUCOL = space(5)
  w_MF2CASC = space(1)
  w_MF2ORDI = space(1)
  w_MF2IMPE = space(1)
  w_MF2RISE = space(1)
  w_MVPREZZO = 0
  w_MVSCOCL1 = 0
  w_MVSPETRA = 0
  w_MVDESART = space(40)
  w_MVSCONT1 = 0
  w_MVSCOCL2 = 0
  w_MVIVATRA = space(5)
  w_MVDESSUP = space(0)
  w_MVSCONT2 = 0
  w_MVSCOPAG = 0
  w_MVFLRTRA = space(1)
  w_MVUNIMIS = space(3)
  w_MVSCONT3 = 0
  w_MVCAUMAG = space(5)
  w_MVSPEBOL = 0
  w_MVCATCON = space(5)
  w_MVSCONT4 = 0
  w_MVMOLSUP = 0
  w_MVIVABOL = space(5)
  w_MVFLSCOR = space(1)
  w_MVCODCLA = space(3)
  w_MVFLOMAG = space(1)
  w_MVSCONTI = 0
  w_MVDATDIV = ctod("  /  /  ")
  w_MVCODCON = space(15)
  w_MVCODIVA = space(5)
  w_MVDATDOC = ctod("  /  /  ")
  w_MVCODIVE = space(5)
  w_MVCODPAG = space(5)
  w_MVKEYSAL = space(20)
  w_MVVALNAZ = space(3)
  w_MFLAVAL = space(1)
  w_MVCODAGE = space(5)
  w_MVCODBAN = space(10)
  w_MVCODPOR = space(1)
  w_MVCAOVAL = 0
  w_MVTCONTR = space(15)
  w_MVCODVAL = space(3)
  w_MVCODVET = space(5)
  w_MVVALRIG = 0
  w_MVFLELGM = space(1)
  w_MVPESNET = 0
  w_MVCODSPE = space(3)
  w_MVIMPSCO = 0
  w_MVRIFDIC = space(10)
  w_MVFLTRAS = space(1)
  w_MVTIPATT = space(1)
  w_MVVALMAG = 0
  w_MVPESNET = 0
  w_MVTCOLIS = space(5)
  w_MVNOMENC = space(8)
  w_MVIMPNAZ = 0
  w_MVACIVA1 = space(5)
  w_MVAIMPN1 = 0
  w_MVAFLOM1 = space(1)
  w_MVAIMPS1 = 0
  w_MVUMSUPP = space(3)
  w_MVACIVA2 = space(5)
  w_MVAIMPN2 = 0
  w_MVAFLOM2 = space(1)
  w_MVAIMPS2 = 0
  w_MVACIVA3 = space(5)
  w_MVAIMPN3 = 0
  w_MVAFLOM3 = space(1)
  w_MVAIMPS3 = 0
  w_MVCODDES = space(5)
  w_MVACIVA4 = space(5)
  w_MVAIMPN4 = 0
  w_MVAFLOM4 = space(1)
  w_MVAIMPS4 = 0
  w_MVIMPARR = 0
  w_MVACIVA5 = space(5)
  w_MVAIMPN5 = 0
  w_MVAFLOM5 = space(1)
  w_MVAIMPS5 = 0
  w_MVDATEVA = ctod("  /  /  ")
  w_MVACIVA6 = space(5)
  w_MVAIMPN6 = 0
  w_MVAFLOM6 = space(1)
  w_MVAIMPS6 = 0
  w_MVACCPRE = 0
  w_MVTOTRIT = 0
  w_MVTOTENA = 0
  w_MVIMPACC = 0
  w_MVRIFESP = space(10)
  w_MVQTASAL = 0
  w_CAOVAL = 0
  w_BOLARR = 0
  w_PERIVA = 0
  w_PEIINC = 0
  w_IMPARR = 0
  w_BOLMIN = 0
  w_BOLIVA = space(1)
  w_BOLINC = space(1)
  w_RSIMPRAT = 0
  w_TOTMERCE = 0
  w_MESE1 = 0
  w_PEIIMB = 0
  w_DECTOT = 0
  w_TOTALE = 0
  w_MESE2 = 0
  w_BOLIMB = space(1)
  w_BOLESE = 0
  w_TOTIMPON = 0
  w_GIORN1 = 0
  w_PEITRA = 0
  w_MFLELGM = space(1)
  w_BOLSUP = 0
  w_TOTIMPOS = 0
  w_GIORN2 = 0
  w_BOLTRA = space(1)
  w_PERIVE = 0
  w_BOLCAM = 0
  w_TOTFATTU = 0
  w_GIOFIS = 0
  w_BOLBOL = space(1)
  w_MCAUCOL = space(5)
  w_CAONAZ = 0
  w_CLBOLFAT = space(1)
  w_BOLIVE = space(1)
  w_MVDATEVA = ctod("  /  /  ")
  w_MVTCAMAG = space(5)
  w_RSMODPAG = space(10)
  w_RSNUMRAT = 0
  w_ACCPRE = 0
  w_RSDATRAT = ctod("  /  /  ")
  w_DETNUM = 0
  w_ORDNUM = 0
  w_CODNAZ = space(3)
  w_APPO = space(10)
  w_APPO1 = 0
  w_NUCON = 0
  w_FLFOBO = space(1)
  w_ACQINT = space(1)
  w_LIPREZZO = 0
  w_DECUNI = 0
  w_DATREG = ctod("  /  /  ")
  w_QUAN = 0
  w_CODVAL = space(3)
  w_CODLIS = space(5)
  w_MAGCOD = space(5)
  w_MAGPFI = space(5)
  w_ARRSUP = 0
  w_CODGRU = space(5)
  w_TIPOLN = space(1)
  w_FLAGG = space(1)
  w_GIOAPP = 0
  w_EVQTASAL = 0
  w_EVKEYSAL = space(20)
  w_EVCODMAG = space(5)
  w_EVFLORDI = space(1)
  w_EVFLIMPE = space(1)
  w_EVFLRISE = space(1)
  w_MVFLLOTT = space(1)
  w_MVF2LOTT = space(1)
  w_FLLOTT = space(1)
  w_FLUBIC = space(1)
  w_F2UBIC = space(1)
  w_CODMAG = space(5)
  w_MVIMPCOM = 0
  w_MVCODCOM = space(15)
  w_MVCODCEN = space(15)
  w_MVCODATT = space(15)
  w_MVVOCCEN = space(15)
  w_FLANAL = space(1)
  w_MFLCOMM = space(0)
  w_MVFLCOCO = space(1)
  w_MVFLORCO = space(1)
  w_MVCODCOS = space(5)
  w_OK_COMM = .f.
  w_PRZFIN = 0
  w_VALRIG = 0
  w_VALMAG = 0
  w_CODRIS = space(15)
  w_IMPNAZ = 0
  w_KEYSAL = space(20)
  w_MAGSAL = space(5)
  w_RICTOT = .f.
  w_COCODVAL = space(5)
  w_DECCOM = 0
  w_ONLYAGG = .f.
  w_EVAKEY = space(20)
  w_EVAMAG = space(5)
  w_EVAMAT = space(5)
  w_EVAQTA = 0
  w_EVAQT1 = 0
  w_EVACAR = space(1)
  w_EVAORD = space(1)
  w_EVAIMP = space(1)
  w_EVARIS = space(1)
  w_EV2CAR = space(1)
  w_EV2ORD = space(1)
  w_EV2IMP = space(1)
  w_EV2RIS = space(1)
  w_EVASER = space(10)
  w_EVAROW = 0
  w_EVAFLA = space(1)
  w_EVAKEY2 = space(20)
  w_EVAMAG2 = space(5)
  w_EVQIMP = 0
  w_EVQIM1 = 0
  w_FLORDI = space(1)
  w_FLIMPE = space(1)
  w_FLRISE = space(1)
  w_OQTAEVA = 0
  w_OQTAEV1 = 0
  w_OQTAMOV = 0
  w_OQTAUM1 = 0
  w_OQTASAL = 0
  w_NQTAEVA = 0
  w_NQTAEV1 = 0
  w_NQTASAL = 0
  w_NUMRIF = 0
  * --- WorkFile variables
  CAM_AGAZ_idx=0
  CAN_TIER_idx=0
  CONTI_idx=0
  DISMBASE_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  DOC_RATE_idx=0
  ESERCIZI_idx=0
  EVA_ARTI_idx=0
  EVA_COMP_idx=0
  LISTINI_idx=0
  MAGAZZIN_idx=0
  PAR_DISB_idx=0
  PAR_RIOR_idx=0
  SALDIART_idx=0
  TIP_DOCU_idx=0
  VALUTE_idx=0
  VOCIIVA_idx=0
  ART_ICOL_idx=0
  VOC_COST_idx=0
  MA_COSTI_idx=0
  SALDICOM_idx=0
  SALOTCOM_idx=0
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Evasione dei Componenti Distinta Base da Documenti (da GSVE_MDV, GSOR_MDV, GSAC_MDV)
    this.w_oMessLog=createobject("Ah_Message")
    this.w_oMessLog1=createobject("Ah_Message")
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    DIMENSION QTC[99]
    DIMENSION DR[51, 9]
    this.w_LOOP = 1
    do while this.w_LOOP <= 51
      DR[ this.w_LOOP , 1] = cp_CharToDate("  -  -  ")
      DR[ this.w_LOOP , 2] = 0
      DR[ this.w_LOOP , 3] = "  "
      DR[ this.w_LOOP , 4] = "  "
      DR[ this.w_LOOP , 5] = "  "
      DR[ this.w_LOOP , 6] = "  "
      DR[ this.w_LOOP , 7] = " "
      DR[ this.w_LOOP , 8] = " "
      DR[ this.w_LOOP , 9] = " "
      this.w_LOOP = this.w_LOOP + 1
    enddo
    * --- Magazzini gestibili (Doc.Origine, Preferenziale, Intestatario, Doc.Destinazione)
    * --- Flag Tipo Magazzino
    * --- Gestione Analitica
    this.w_PADRE = this.oParentObject
    this.w_TROV = .F.
    this.w_RITENTA = .F.
    this.w_SERIAL = this.w_PADRE.w_MVSERIAL
    this.w_MAXLEVEL = iif(this.oParentObject.w_MAXLEV=0,99,this.oParentObject.w_MAXLEV)
    this.w_VERIFICA = "S"
    this.w_FILSTAT = " "
    this.w_FLFOBO = " "
    this.w_CODAZI = i_CODAZI
    this.w_ACQINT = "N"
    this.w_MVCAUCON = SPACE(5)
    this.w_MVTIPCON = " "
    this.w_MVCODCON = SPACE(15)
    this.w_MMCAUPFI = this.oParentObject.w_CAUPFI
    this.w_MMCAUCOD = this.oParentObject.w_CAUCOD
    this.w_MMTIPCON = this.w_PADRE.w_MVTIPCON
    this.w_MMCODCON = this.w_PADRE.w_MVCODCON
    this.w_MVCODMAG = this.w_PADRE.w_MVCODMAG
    this.w_MVDATREG = CP_TODATE(this.w_PADRE.w_MVDATREG)
    this.w_MVDATDOC = CP_TODATE(this.w_PADRE.w_MVDATDOC)
    this.w_MMDATREG = IIF(EMPTY(this.w_MVDATDOC), this.w_MVDATREG, this.w_MVDATDOC)
    this.w_CODTLIS = this.w_PADRE.w_MVTCOLIS
    this.w_MVDATCIV = this.w_MVDATREG
    this.w_MVNUMEST = this.w_PADRE.w_MVNUMDOC
    this.w_MVALFEST = this.w_PADRE.w_MVALFDOC
    this.w_MVFLPROV = "N"
    this.w_MVPRP = "NN"
    this.w_MVANNPRO = SPACE(4)
    this.w_MVDATDIV = cp_CharToDate("  -  -  ")
    this.w_MVCODUTE = IIF(g_MAGUTE="S", 0, i_CODUTE)
    * --- Diversifico il calcolo in base al chiamante. Diversificazione resa necessaria dalla generazione automatica dagli ordini
    *     con articoli composti da CPZ.
    if upper(this.w_PADRE.class) == "TGSCP_BGO"
      this.w_MVCODESE = this.w_PADRE.w_CODESE
      this.w_OLDTES = Alltrim(this.w_MMCODCON)+Alltrim(this.w_CODTLIS)+Alltrim(DTOC(this.w_MVDATDOC))
    else
      this.w_MVCODESE = this.w_PADRE.w_MVCODESE
      this.w_OLDTES = this.w_PADRE.w_OLDTES
      this.w_RIFESP = this.w_PADRE.w_MVRIFESP
    endif
    this.w_MVVALNAZ = g_PERVAL
    this.w_CAONAZ = GETCAM(this.w_MVVALNAZ, this.w_MVDATDOC, 0)
    this.w_CODMAG = this.w_MVCODMAG
    this.w_DITIPVAL = "N"
    this.w_DINUMINV = SPACE(6)
    this.w_DICODESE = SPACE(4)
    this.w_MMTCOLIS = SPACE(5)
    this.w_TDCOSEPL = this.w_PADRE.w_TDCOSEPL
    this.w_TDFLEXPL = this.w_PADRE.w_TDFLEXPL
    this.w_VALCOM = this.w_PADRE.w_VALCOM
    this.w_EXPAUT = " "
    this.w_FLMGPR_P = "D"
    this.w_FLMTPR_P = "D"
    this.w_FLMGPR_C = "D"
    this.w_FLMTPR_C = "D"
    this.w_MAGTER = SPACE(5)
    this.w_MAGDES_P = SPACE(5)
    this.w_MATDES_P = SPACE(5)
    this.w_MAGDES_C = SPACE(5)
    this.w_MATDES_C = SPACE(5)
    this.w_CAMCOC = SPACE(5)
    this.w_CAMCOP = SPACE(5)
    this.w_MVFLELAN = " "
    this.w_MV_SEGNO = "D"
    this.w_FLCOSINV = "N"
    this.w_VOCECR = "R"
    this.w_LOTDIF = " "
    this.w_LOTDIF2 = " "
    this.w_TIPIMB_R = "N"
    this.w_GSAR_MIR = .Null.
    this.w_GSAR_MIP = .Null.
    * --- Controllo se esiste ed � instanziato il figlio relativo agli imballi a rendere
    if Type("this.w_PADRE.GSAR_MIR.cnt")="O"
      this.w_GSAR_MIR = this.w_PADRE.GSAR_MIR.cnt
      * --- Solo se nDeferredFillRec � = 0 significa che il figlio � stato modificato e deve 
      *     essere salvato. Infatti se instanzio il figlio in interroga su un documento che ha presente imballi a rendere
      *     e poi senza chiudere la gestione documentale carico un nuovo documento che non ha imballi a rendere
      *     il figlio rimane instanziato e i suoi cursori pieni. Quindi genererebbe di nuovo gli imballi del documento aperto 
      *     precedentemente in interroga
      if this.w_GSAR_MIR.nDeferredFillRec = 0
        this.w_TIPIMB_R = "S"
      endif
    endif
    if Type("this.w_PADRE.GSAR_MIP.cnt")="O"
      this.w_GSAR_MIP = this.w_PADRE.GSAR_MIP.cnt
      * --- Per il child legato alle righe nDeferredfillrec non � sufficente.
      *     La propriet� CONTROLLO contiene chiave univoca Sys(2015) opportunamente definita.
      if this.w_GSAR_MIP.w_CONTROLLO =this.w_PADRE.w_KEYCHILD
        this.w_TIPIMB_P = "S"
      endif
    endif
    this.w_ACCEDE = .F.
    if NOT EMPTY(this.w_SERIAL)
      if this.w_MMTIPCON $ "CF" AND NOT EMPTY(this.w_MMCODCON)
        * --- Legge Eventuale Magazzino Intestatario
        * --- Read from CONTI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CONTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ANMAGTER,ANCODLIN"+;
            " from "+i_cTable+" CONTI where ";
                +"ANTIPCON = "+cp_ToStrODBC(this.w_MMTIPCON);
                +" and ANCODICE = "+cp_ToStrODBC(this.w_MMCODCON);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ANMAGTER,ANCODLIN;
            from (i_cTable) where;
                ANTIPCON = this.w_MMTIPCON;
                and ANCODICE = this.w_MMCODCON;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MAGTER = NVL(cp_ToDate(_read_.ANMAGTER),cp_NullValue(_read_.ANMAGTER))
          this.w_CODLIN = NVL(cp_ToDate(_read_.ANCODLIN),cp_NullValue(_read_.ANCODLIN))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      * --- Legge Dati Articoli x Valorizzazioni
      ah_Msg("Lettura dati articoli per valorizzazioni...",.T.)
      vq_exec("QUERY\GSAR_UMI.VQR", this,"UMNOFR")
      * --- Carica Temporaneo Evasioni
      vq_exec("QUERY\GSAR_BEA.VQR", this,"APPOEVA")
      if this.w_TIPIMB_R= "S"
        * --- Controllo se nel cursore degli imballi a rendere ci sono record
        *     Poich� GSAR_MIR � linkata al documento di esplosione prodotti finiti, in modifica contiene
        *     anche questo documento nel caso di distinta base. Con il filtro t_KITIMB = 'R' vengono prese
        *     le sole righe di imballi a perdere
        this.w_GSAR_MIR.Exec_Select("IMBALLI","Distinct t_MVCODICE AS CODICE","Not Empty( Nvl(t_MVCODICE,' ') ) And t_MVQTAUM1 <>0 And Nvl(t_KITIMB,'X') = 'R' ","","","")     
        if Reccount("IMBALLI")>0
          this.w_MAXROW = 0
          * --- Devo calcolare il primo CPROWNUM utile per inserire la riga fittizia relativa agli imballi a rendere
          this.w_PADRE.Exec_Select("RIGAMAX","Max(CPROWNUM) AS RIGA","t_CPROWORD<>0 AND t_MVTIPRIG<>' ' AND Not Empty(Nvl(t_MVCODICE,' '))","","","")     
          if Used("RIGAMAX")
            this.w_MAXROW = RIGAMAX.RIGA+1
            USE IN Select("RIGAMAX")
          endif
          * --- Inserisco la riga fittizia per gli imballi a rendere in EVA_ARTI poich� per gli 
          *     imballi a rendere non ho nessun articolo che simula il prodotto finito.
          INSERT INTO APPOEVA ; 
 (MVSERIAL, MVNUMDOC, MVALFDOC , MVDATDOC, MVDATREG, CPROWNUM, CPROWORD, MVTIPRIG, ; 
 MVCODICE, MVCODART, MVDESART, MVUNIMIS, MVCODMAG, MVQTAMOV, MVQTAUM1, MVRIFESC, ; 
 MVDATEVA, MVFLERIF, QTAORI, QTAOR1, MVDESSUP, MVCODMAT, MVCODCOM, MVIMPCOM , TIPO, DISKIT) ; 
 VALUES(this.w_SERIAL, this.w_MVNUMEST, this.w_MVALFEST, this.w_MVDATDOC, this.w_MVDATREG, this.w_MAXROW, this.w_MAXROW*10, "D", g_ARTDES,; 
 g_ARTDES, " ", "", this.w_MVCODMAG, 0, 0, "", this.w_MVDATREG, "", 0, 0, "", "", "", 0, "B","X")
          SELECT * FROM APPOEVA INTO CURSOR APPOEVA ORDER BY TIPO, CPROWORD
        endif
      endif
      if USED("APPOEVA") Or USED("IMBALLI")
        * --- EVA_ARTI e EVA_COMP sono tabelle fisiche ma utilizzate come tabelle temporanee di appoggio
        *     Posso cancellare solo quelli con il solito seriale per il problema della multiutenza
        *     Un'altro utente potrebbe aver lanciato la stessa operazione.
        * --- Delete from EVA_COMP
        i_nConn=i_TableProp[this.EVA_COMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.EVA_COMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          delete from (i_cTable) where;
                MVSERIAL = this.w_SERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Delete from EVA_ARTI
        i_nConn=i_TableProp[this.EVA_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.EVA_ARTI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          delete from (i_cTable) where;
                MVSERIAL = this.w_SERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Legge ev.li Criteri di valorizzazione di Default
        this.w_PDCRIVAL = " "
        this.w_PDESEINV = SPACE(4)
        this.w_PDNUMINV = SPACE(6)
        this.w_PDCODLIS = SPACE(5)
        * --- Read from PAR_DISB
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PAR_DISB_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PAR_DISB_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "PDCRIVAL,PDESEINV,PDNUMINV,PDCODLIS,PDCOSPAR"+;
            " from "+i_cTable+" PAR_DISB where ";
                +"PDCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            PDCRIVAL,PDESEINV,PDNUMINV,PDCODLIS,PDCOSPAR;
            from (i_cTable) where;
                PDCODAZI = this.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PDCRIVAL = NVL(cp_ToDate(_read_.PDCRIVAL),cp_NullValue(_read_.PDCRIVAL))
          this.w_PDESEINV = NVL(cp_ToDate(_read_.PDESEINV),cp_NullValue(_read_.PDESEINV))
          this.w_PDNUMINV = NVL(cp_ToDate(_read_.PDNUMINV),cp_NullValue(_read_.PDNUMINV))
          this.w_PDCODLIS = NVL(cp_ToDate(_read_.PDCODLIS),cp_NullValue(_read_.PDCODLIS))
          this.w_TIPCOS = NVL(cp_ToDate(_read_.PDCOSPAR),cp_NullValue(_read_.PDCOSPAR))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_PDCRIVAL = IIF(EMPTY(this.w_PDCRIVAL), "S", this.w_PDCRIVAL)
        this.w_PDCODLIS = IIF(this.w_PDCRIVAL="L", this.w_PDCODLIS, SPACE(5))
        this.w_PDESEINV = IIF(this.w_PDCRIVAL $ "SMUP", IIF(EMPTY(this.w_PDESEINV), g_CODESE, this.w_PDESEINV), SPACE(4))
        this.w_PDNUMINV = IIF(this.w_PDCRIVAL $ "SMUP", this.w_PDNUMINV, SPACE(6))
        this.w_MMCAMCOD = SPACE(5)
        this.w_MAGCOD = SPACE(5)
        if NOT EMPTY(this.w_MMCAUCOD)
          * --- Legge Causale Magazzino e Magazzino di Default  (e Flag Intestazione per eventuale Magazzino C/Lavoro su Fornitore)
          this.w_APPO = "N"
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDCAUMAG,TDCODMAG,TDCODMAT,TDFLINTE,TDFLMGPR,TDFLMTPR,TDLOTDIF"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.w_MMCAUCOD);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDCAUMAG,TDCODMAG,TDCODMAT,TDFLINTE,TDFLMGPR,TDFLMTPR,TDLOTDIF;
              from (i_cTable) where;
                  TDTIPDOC = this.w_MMCAUCOD;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MMCAMCOD = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
            this.w_MAGDES_C = NVL(cp_ToDate(_read_.TDCODMAG),cp_NullValue(_read_.TDCODMAG))
            this.w_MATDES_C = NVL(cp_ToDate(_read_.TDCODMAT),cp_NullValue(_read_.TDCODMAT))
            this.w_APPO = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
            this.w_FLMGPR_C = NVL(cp_ToDate(_read_.TDFLMGPR),cp_NullValue(_read_.TDFLMGPR))
            this.w_FLMTPR_C = NVL(cp_ToDate(_read_.TDFLMTPR),cp_NullValue(_read_.TDFLMTPR))
            this.w_LOTDIF2 = NVL(cp_ToDate(_read_.TDLOTDIF),cp_NullValue(_read_.TDLOTDIF))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_FLMGPR_C = IIF(Empty(this.w_FLMGPR_C), "D", this.w_FLMGPR_C)
          this.w_FLMTPR_C = IIF(Empty(this.w_FLMTPR_C), "D", this.w_FLMTPR_C)
          this.w_MAGCOD = IIF(EMPTY(this.w_MMCAMCOD), SPACE(5), IIF(EMPTY(this.w_MAGDES_C), this.w_CODMAG, this.w_MAGDES_C))
          if NOT EMPTY(this.w_MMCAMCOD)
            * --- Verifica Presenza causale Collegata
            * --- Read from CAM_AGAZ
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CMCAUCOL"+;
                " from "+i_cTable+" CAM_AGAZ where ";
                    +"CMCODICE = "+cp_ToStrODBC(this.w_MMCAMCOD);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CMCAUCOL;
                from (i_cTable) where;
                    CMCODICE = this.w_MMCAMCOD;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_CAMCOC = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          if this.w_MMTIPCON="F" AND NOT EMPTY(this.w_MMCODCON)
            * --- Legge Eventuale Magazzino C/Lavoro sul Fornitore
            this.w_MAGCOD = IIF(EMPTY(this.w_MAGTER), this.w_MAGCOD, this.w_MAGTER)
          endif
        endif
        SELECT APPOEVA
        if RECCOUNT()>0
          GO TOP
          SCAN
          this.w_CPROWNUM = CPROWNUM
          this.w_CPROWORD = CPROWORD
          this.w_MVCODICE = IIF(TIPO="A",NVL(MVCODICE," "),g_ARTDES)
          this.w_MVCODART = IIF(TIPO="A",NVL(MVCODART," "),g_ARTDES)
          this.w_MVDESART = IIF(TIPO="A",NVL(MVDESART," "),"Imballi a rendere")
          this.w_MVUNIMIS = IIF(TIPO="A",NVL(MVUNIMIS," "), "")
          this.w_MVQTAMOV = IIF(TIPO="A",NVL(MVQTAMOV, 0),1)
          this.w_MVQTAUM1 = IIF(TIPO="A",NVL(MVQTAUM1, 0),1)
          this.w_MVDATREG = CP_TODATE(MVDATREG)
          this.w_MVRIFESC = NVL(MVRIFESC, " ")
          this.w_MVDESSUP = NVL(MVDESSUP, " ")
          if this.w_TDFLEXPL="S"
            * --- Flag Entrambi: Se esiste Doc.Origine esplode Documento altrimenti Distinta
            this.w_FLEXPL = IIF(NOT EMPTY(this.w_MVRIFESC), "S", "N")
          else
            * --- Esplode solo Doc.Origine (solo Se esiste) oppure solo Distinta
            this.w_FLEXPL = IIF(this.w_TDFLEXPL="D", "S", "N")
          endif
          this.w_MVFLERIF = IIF(this.w_FLEXPL="S", NVL(MVFLERIF, " "), " ")
          this.w_MVDATEVA = MAX(IIF(EMPTY(CP_TODATE(MVDATEVA)), this.w_MVDATDOC, CP_TODATE(MVDATEVA)), this.w_MVDATDOC)
          this.w_QTAORI = NVL(QTAORI, 0)
          this.w_QTAOR1 = NVL(QTAOR1, 0)
          this.w_MAGORI = NVL(MVCODMAG, SPACE(5))
          this.w_MATORI = NVL(MVCODMAT, SPACE(5))
          this.w_MVCODCOM = Nvl(MVCODCOM,Space(15))
          this.w_MVCODCEN = Nvl(MVCODCEN,Space(15))
          this.w_MVCODATT = Nvl(MVCODATT,Space(15))
          if NOT EMPTY(this.w_MVCODICE) AND NOT EMPTY(this.w_MVCODART) AND this.w_MVQTAUM1>0
            this.w_RECINS = .F.
            * --- Try
            local bErr_041A8C28
            bErr_041A8C28=bTrsErr
            this.Try_041A8C28()
            * --- Catch
            if !empty(i_Error)
              i_ErrMsg=i_Error
              i_Error=''
              * --- accept error
              bTrsErr=.f.
            endif
            bTrsErr=bTrsErr or bErr_041A8C28
            * --- End
            SELECT APPOEVA
            * --- Inserisce Riga Prodotto FInito in EVA_COMP
            if this.w_RECINS
              this.w_MVNUMERO = 0
              this.w_CPROWORD = 0
              * --- Se Esiste Causale per Prodotti Finiti
              *     ==========================================================
              if NOT EMPTY(this.w_MMCAUPFI)
                this.w_MVNUMERO = this.w_MVNUMERO + 1
                this.w_CPROWORD = this.w_CPROWORD + 10
                this.w_MMCODICE = this.w_MVCODICE
                this.w_MMCODART = this.w_MVCODART
                this.w_MMUNIMIS = this.w_MVUNIMIS
                this.w_MMQTAMOV = this.w_MVQTAMOV
                this.w_MMQTAUM1 = this.w_MVQTAUM1
                if TIPO = "A" And DISKIT <> "I"
                  * --- Per gli articoli di tipo imballo non devo creare il documento di esplosione del 
                  *     prodotto finito
                  ah_Msg("Scrittura movimento componenti; articolo: %1",.T.,.F.,.F., ALLTRIM(this.w_MMCODART) )
                  * --- Insert into EVA_COMP
                  i_nConn=i_TableProp[this.EVA_COMP_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.EVA_COMP_idx,2])
                  i_commit = .f.
                  if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                    cp_BeginTrs()
                    i_commit = .t.
                  endif
                  i_ccchkf=''
                  i_ccchkv=''
                  this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.EVA_COMP_idx,i_nConn)
                  if i_nConn<>0
                    i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                " ("+"MVSERIAL"+",MVROWNUM"+",CPROWNUM"+",MVTIPREC"+",CPROWORD"+",MVTIPRIG"+",MVCODICE"+",MVCODART"+",MVUNIMIS"+",MVQTAMOV"+",MVQTAUM1"+",MVCODCAU"+",MVDATEVA"+",MVSERRIF"+",MVROWRIF"+",MVQTASAL"+",MVCODMAG"+",MVKEYSAL"+",MVFLORDI"+",MVFLIMPE"+",MVFLRISE"+",MVDESSUP"+",MVMAGORI"+",MVMATORI"+",MVCODCEN"+",MVCODCOM"+",MVCODATT"+i_ccchkf+") values ("+;
                    cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'EVA_COMP','MVSERIAL');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'EVA_COMP','MVROWNUM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMERO),'EVA_COMP','CPROWNUM');
                    +","+cp_NullLink(cp_ToStrODBC("1"),'EVA_COMP','MVTIPREC');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'EVA_COMP','CPROWORD');
                    +","+cp_NullLink(cp_ToStrODBC("R"),'EVA_COMP','MVTIPRIG');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODICE),'EVA_COMP','MVCODICE');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODART),'EVA_COMP','MVCODART');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_MMUNIMIS),'EVA_COMP','MVUNIMIS');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_MMQTAMOV),'EVA_COMP','MVQTAMOV');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_MMQTAUM1),'EVA_COMP','MVQTAUM1');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAUPFI),'EVA_COMP','MVCODCAU');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATEVA),'EVA_COMP','MVDATEVA');
                    +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'EVA_COMP','MVSERRIF');
                    +","+cp_NullLink(cp_ToStrODBC(0),'EVA_COMP','MVROWRIF');
                    +","+cp_NullLink(cp_ToStrODBC(0),'EVA_COMP','MVQTASAL');
                    +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'EVA_COMP','MVCODMAG');
                    +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'EVA_COMP','MVKEYSAL');
                    +","+cp_NullLink(cp_ToStrODBC(" "),'EVA_COMP','MVFLORDI');
                    +","+cp_NullLink(cp_ToStrODBC(" "),'EVA_COMP','MVFLIMPE');
                    +","+cp_NullLink(cp_ToStrODBC(" "),'EVA_COMP','MVFLRISE');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESSUP),'EVA_COMP','MVDESSUP');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_MAGORI),'EVA_COMP','MVMAGORI');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_MATORI),'EVA_COMP','MVMATORI');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCEN),'EVA_COMP','MVCODCEN');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'EVA_COMP','MVCODCOM');
                    +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODATT),'EVA_COMP','MVCODATT');
                         +i_ccchkv+")")
                  else
                    cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_SERIAL,'MVROWNUM',this.w_CPROWNUM,'CPROWNUM',this.w_MVNUMERO,'MVTIPREC',"1",'CPROWORD',this.w_CPROWORD,'MVTIPRIG',"R",'MVCODICE',this.w_MMCODICE,'MVCODART',this.w_MMCODART,'MVUNIMIS',this.w_MMUNIMIS,'MVQTAMOV',this.w_MMQTAMOV,'MVQTAUM1',this.w_MMQTAUM1,'MVCODCAU',this.w_MMCAUPFI)
                    insert into (i_cTable) (MVSERIAL,MVROWNUM,CPROWNUM,MVTIPREC,CPROWORD,MVTIPRIG,MVCODICE,MVCODART,MVUNIMIS,MVQTAMOV,MVQTAUM1,MVCODCAU,MVDATEVA,MVSERRIF,MVROWRIF,MVQTASAL,MVCODMAG,MVKEYSAL,MVFLORDI,MVFLIMPE,MVFLRISE,MVDESSUP,MVMAGORI,MVMATORI,MVCODCEN,MVCODCOM,MVCODATT &i_ccchkf. );
                       values (;
                         this.w_SERIAL;
                         ,this.w_CPROWNUM;
                         ,this.w_MVNUMERO;
                         ,"1";
                         ,this.w_CPROWORD;
                         ,"R";
                         ,this.w_MMCODICE;
                         ,this.w_MMCODART;
                         ,this.w_MMUNIMIS;
                         ,this.w_MMQTAMOV;
                         ,this.w_MMQTAUM1;
                         ,this.w_MMCAUPFI;
                         ,this.w_MVDATEVA;
                         ,SPACE(10);
                         ,0;
                         ,0;
                         ,SPACE(5);
                         ,SPACE(20);
                         ," ";
                         ," ";
                         ," ";
                         ,this.w_MVDESSUP;
                         ,this.w_MAGORI;
                         ,this.w_MATORI;
                         ,this.w_MVCODCEN;
                         ,this.w_MVCODCOM;
                         ,this.w_MVCODATT;
                         &i_ccchkv. )
                    i_Rows=iif(bTrsErr,0,1)
                  endif
                  if i_commit
                    cp_EndTrs(.t.)
                  endif
                  if i_Rows<0 or bTrsErr
                    * --- Error: insert not accepted
                    i_Error='Errore in inserimento evasioni componenti'
                    return
                  endif
                endif
                * --- Legge Causale Magazzino e Magazzino di Default
                this.w_MMCAMPFI = SPACE(5)
                * --- Read from TIP_DOCU
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "TDCAUMAG,TDCODMAG,TDCODMAT,TDFLMGPR,TDFLMTPR,TDLOTDIF"+;
                    " from "+i_cTable+" TIP_DOCU where ";
                        +"TDTIPDOC = "+cp_ToStrODBC(this.w_MMCAUPFI);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    TDCAUMAG,TDCODMAG,TDCODMAT,TDFLMGPR,TDFLMTPR,TDLOTDIF;
                    from (i_cTable) where;
                        TDTIPDOC = this.w_MMCAUPFI;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_MMCAMPFI = NVL(cp_ToDate(_read_.TDCAUMAG),cp_NullValue(_read_.TDCAUMAG))
                  this.w_MAGDES_P = NVL(cp_ToDate(_read_.TDCODMAG),cp_NullValue(_read_.TDCODMAG))
                  this.w_MATDES_P = NVL(cp_ToDate(_read_.TDCODMAT),cp_NullValue(_read_.TDCODMAT))
                  this.w_FLMGPR_P = NVL(cp_ToDate(_read_.TDFLMGPR),cp_NullValue(_read_.TDFLMGPR))
                  this.w_FLMTPR_P = NVL(cp_ToDate(_read_.TDFLMTPR),cp_NullValue(_read_.TDFLMTPR))
                  this.w_LOTDIF = NVL(cp_ToDate(_read_.TDLOTDIF),cp_NullValue(_read_.TDLOTDIF))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_FLMGPR_P = IIF(Empty(this.w_FLMGPR_P), "D", this.w_FLMGPR_P)
                this.w_FLMTPR_P = IIF(Empty(this.w_FLMTPR_P), "D", this.w_FLMTPR_P)
                this.w_MAGPFI = IIF(EMPTY(this.w_MMCAMPFI), SPACE(5), IIF(EMPTY( this.w_MAGDES_P), this.w_CODMAG, this.w_MAGDES_P))
                if NOT EMPTY(this.w_MMCAMPFI)
                  * --- Verifica Presenza causale Collegata
                  * --- Read from CAM_AGAZ
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "CMCAUCOL"+;
                      " from "+i_cTable+" CAM_AGAZ where ";
                          +"CMCODICE = "+cp_ToStrODBC(this.w_MMCAMPFI);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      CMCAUCOL;
                      from (i_cTable) where;
                          CMCODICE = this.w_MMCAMPFI;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_CAMCOP = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                endif
                if TIPO = "B"
                  * --- Inserisco in EVA_COMP le righe relative agli imballi a rendere
                  this.w_GSAR_MIR.FirstRow()     
                  do while Not this.w_GSAR_MIR.Eof_Trs()
                    this.w_CODICE = NVL( this.w_GSAR_MIR.Get("w_MVCODICE") , " ")
                    this.w_CODART = NVL( this.w_GSAR_MIR.Get("w_MVCODART") , " ")
                    this.w_DESART = NVL( this.w_GSAR_MIR.Get("w_MVDESART") , " ")
                    this.w_IUNIMIS = NVL( this.w_GSAR_MIR.Get("w_MVUNIMIS") , " ")
                    this.w_IQTAMOV = NVL( this.w_GSAR_MIR.Get("w_MVQTAUM1") , 0)
                    this.w_MVNUMERO = this.w_MVNUMERO + 1
                    this.w_CPROWORD = this.w_CPROWORD + 10
                    * --- Insert into EVA_COMP
                    i_nConn=i_TableProp[this.EVA_COMP_idx,3]
                    i_cTable=cp_SetAzi(i_TableProp[this.EVA_COMP_idx,2])
                    i_commit = .f.
                    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                      cp_BeginTrs()
                      i_commit = .t.
                    endif
                    i_ccchkf=''
                    i_ccchkv=''
                    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.EVA_COMP_idx,i_nConn)
                    if i_nConn<>0
                      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                                  " ("+"MVSERIAL"+",MVROWNUM"+",CPROWNUM"+",MVTIPREC"+",CPROWORD"+",MVTIPRIG"+",MVCODICE"+",MVCODART"+",MVUNIMIS"+",MVQTAMOV"+",MVQTAUM1"+",MVCODCAU"+",MVDATEVA"+",MVSERRIF"+",MVROWRIF"+",MVQTASAL"+",MVCODMAG"+",MVKEYSAL"+",MVFLORDI"+",MVFLIMPE"+",MVFLRISE"+",MVDESSUP"+",MVMAGORI"+",MVMATORI"+i_ccchkf+") values ("+;
                      cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'EVA_COMP','MVSERIAL');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'EVA_COMP','MVROWNUM');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMERO),'EVA_COMP','CPROWNUM');
                      +","+cp_NullLink(cp_ToStrODBC("1"),'EVA_COMP','MVTIPREC');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'EVA_COMP','CPROWORD');
                      +","+cp_NullLink(cp_ToStrODBC("R"),'EVA_COMP','MVTIPRIG');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'EVA_COMP','MVCODICE');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_CODART),'EVA_COMP','MVCODART');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_IUNIMIS),'EVA_COMP','MVUNIMIS');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_IQTAMOV),'EVA_COMP','MVQTAMOV');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_IQTAMOV),'EVA_COMP','MVQTAUM1');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAUPFI),'EVA_COMP','MVCODCAU');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATEVA),'EVA_COMP','MVDATEVA');
                      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'EVA_COMP','MVSERRIF');
                      +","+cp_NullLink(cp_ToStrODBC(0),'EVA_COMP','MVROWRIF');
                      +","+cp_NullLink(cp_ToStrODBC(0),'EVA_COMP','MVQTASAL');
                      +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'EVA_COMP','MVCODMAG');
                      +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'EVA_COMP','MVKEYSAL');
                      +","+cp_NullLink(cp_ToStrODBC(" "),'EVA_COMP','MVFLORDI');
                      +","+cp_NullLink(cp_ToStrODBC(" "),'EVA_COMP','MVFLIMPE');
                      +","+cp_NullLink(cp_ToStrODBC(" "),'EVA_COMP','MVFLRISE');
                      +","+cp_NullLink(cp_ToStrODBC(" "),'EVA_COMP','MVDESSUP');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_MAGORI),'EVA_COMP','MVMAGORI');
                      +","+cp_NullLink(cp_ToStrODBC(this.w_MATORI),'EVA_COMP','MVMATORI');
                           +i_ccchkv+")")
                    else
                      cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_SERIAL,'MVROWNUM',this.w_CPROWNUM,'CPROWNUM',this.w_MVNUMERO,'MVTIPREC',"1",'CPROWORD',this.w_CPROWORD,'MVTIPRIG',"R",'MVCODICE',this.w_CODICE,'MVCODART',this.w_CODART,'MVUNIMIS',this.w_IUNIMIS,'MVQTAMOV',this.w_IQTAMOV,'MVQTAUM1',this.w_IQTAMOV,'MVCODCAU',this.w_MMCAUPFI)
                      insert into (i_cTable) (MVSERIAL,MVROWNUM,CPROWNUM,MVTIPREC,CPROWORD,MVTIPRIG,MVCODICE,MVCODART,MVUNIMIS,MVQTAMOV,MVQTAUM1,MVCODCAU,MVDATEVA,MVSERRIF,MVROWRIF,MVQTASAL,MVCODMAG,MVKEYSAL,MVFLORDI,MVFLIMPE,MVFLRISE,MVDESSUP,MVMAGORI,MVMATORI &i_ccchkf. );
                         values (;
                           this.w_SERIAL;
                           ,this.w_CPROWNUM;
                           ,this.w_MVNUMERO;
                           ,"1";
                           ,this.w_CPROWORD;
                           ,"R";
                           ,this.w_CODICE;
                           ,this.w_CODART;
                           ,this.w_IUNIMIS;
                           ,this.w_IQTAMOV;
                           ,this.w_IQTAMOV;
                           ,this.w_MMCAUPFI;
                           ,this.w_MVDATEVA;
                           ,SPACE(10);
                           ,0;
                           ,0;
                           ,SPACE(5);
                           ,SPACE(20);
                           ," ";
                           ," ";
                           ," ";
                           ," ";
                           ,this.w_MAGORI;
                           ,this.w_MATORI;
                           &i_ccchkv. )
                      i_Rows=iif(bTrsErr,0,1)
                    endif
                    if i_commit
                      cp_EndTrs(.t.)
                    endif
                    if i_Rows<0 or bTrsErr
                      * --- Error: insert not accepted
                      i_Error='Errore in Inserimento Evasioni Componenti'
                      return
                    endif
                    this.w_GSAR_MIR.NextRow()     
                  enddo
                endif
              endif
              SELECT APPOEVA
              * --- Se Inserito Articolo Composto, esplode Distinta Componenti 
              *     ==========================================================
              if NOT EMPTY(this.w_MMCAUCOD)
                * --- Parametri per Batch di Esplosione
                this.w_DBCODINI = this.w_MVCODART
                this.w_DBCODFIN = this.w_MVCODART
                this.w_DATFIL = IIF(EMPTY(this.w_MVDATREG), i_DATSYS, this.w_MVDATREG)
                USE IN SELECT("TES_PLOS")
                USE IN SELECT("TMP_SCAR")
                * --- L'esplosione puo' avvenire da distinta o direttamente dal riferimento al Documento associato alla Riga di Origine
                if this.w_FLEXPL="S"
                  if NOT EMPTY(this.w_MVRIFESC)
                    * --- Legge Documento Interno di Riferimento ai Componenti
                    vq_exec("QUERY\GSAR1BEA.VQR", this,"TES_PLOS")
                  endif
                else
                  * --- Nel caso di valorizzazione Prodotto finito lancio GSAR_BDE con parametro
                  *     S per evitare di calcolare due volte eventuali coefficenti
                  gsar_bde(this,"S")
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                  if this.w_TIPIMB_P= "S" And this.w_CPROWNUM <> this.w_MAXROW
                    * --- Marco la riga del temporaneo dei documenti
                    this.w_PADRE.MarkPos()     
                    * --- Mi posiziono sulla riga del temporaneo dei documenti
                    this.w_RECNUM = this.w_PADRE.Search( "CPROWNUM="+ Str(this.w_CPROWNUM,5,0) , 0 )
                    if this.w_RECNUM>0
                      this.w_PADRE.SetRow(this.w_RECNUM)     
                      this.w_PADRE.ChildrenChangeRow()     
                      * --- Devo simulare la pressione del bottone Imballi di riga altrimenti il cursore temporaneo non si aggiorna
                      this.w_PADRE.GSAR_MIP.LinkPCClick(.T.)     
                      this.w_PADRE.GSAR_MIP.HideChildrenChain()     
                      * --- Metto bUpdated a F perch� non deve salvare niente e perch� altrimenti la
                      *     ecpquit mi farebbe la domanda se voglio abbandonare le modifiche
                      this.w_GSAR_MIP.bUpdated = .F.
                      this.w_GSAR_MIP.FirstRow()     
                      this.w_TESTART = this.w_GSAR_MIP.Get("t_MVCODART")
                      if Not empty(this.w_TESTART)
                        do while Not this.w_GSAR_MIP.Eof_Trs()
                          this.w_GSAR_MIP.SetRow()     
                          this.w_DISPAD = this.w_PADRE.w_CODDIS
                          * --- Read from DISMBASE
                          i_nOldArea=select()
                          if used('_read_')
                            select _read_
                            use
                          endif
                          i_nConn=i_TableProp[this.DISMBASE_idx,3]
                          i_cTable=cp_SetAzi(i_TableProp[this.DISMBASE_idx,2])
                          if i_nConn<>0
                            cp_sqlexec(i_nConn,"select "+;
                              "DBDESCRI"+;
                              " from "+i_cTable+" DISMBASE where ";
                                  +"DBCODICE = "+cp_ToStrODBC(this.w_DISPAD);
                                   ,"_read_")
                            i_Rows=iif(used('_read_'),reccount(),0)
                          else
                            select;
                              DBDESCRI;
                              from (i_cTable) where;
                                  DBCODICE = this.w_DISPAD;
                               into cursor _read_
                            i_Rows=_tally
                          endif
                          if used('_read_')
                            locate for 1=1
                            w_DESCOM = NVL(cp_ToDate(_read_.DBDESCRI),cp_NullValue(_read_.DBDESCRI))
                            use
                          else
                            * --- Error: sql sentence error.
                            i_Error = MSG_READ_ERROR
                            return
                          endif
                          select (i_nOldArea)
                          this.w_CODART = this.w_GSAR_MIP.w_MVCODART
                          this.w_NUMRIG = this.w_GSAR_MIP.w_CPROWNUM
                          this.w_CODCOM = this.w_GSAR_MIP.w_MVCODICE
                          this.w_UNIMIS = this.w_GSAR_MIP.w_MVUNIMIS
                          this.w_QTANET = this.w_GSAR_MIP.w_MVQTAUM1
                          this.w_ARTPAD = this.w_CODART
                          INSERT INTO TES_PLOS ;
                          (CODART, NUMRIG, DISPAD, ARTPAD, FLVARI, CODCOM, NUMLEV, CODRIS, DESCOM, UNIMIS, QTANET, QTANE1, QTAMOV, QTAUM1, QTAREC, QTARE1, COEIMP, ;
                          PERSCA, RECSCA, PERSFR, RECSFR, PERRIC, FLESPL, ARTCOM, CODDIS, FLSTAT, PROPRE, TIPGES, MAGPRE, FLPREV, RIFFAS, UNMIS0, MVDESSUP) ;
                          VALUES (this.w_CODART, this.w_NUMRIG, this.w_DISPAD, this.w_ARTPAD, " ", this.w_CODCOM, "0001", SPACE(15), w_DESCOM, ;
                          this.w_UNIMIS, 0, 0, this.w_QTANET, this.w_QTANET, 0, 0, Space(15), 0,0, 0, 0, 0, ;
                          " ", this.w_CODART, this.w_DISPAD, " ", "E", "@", Space(5), " ", 0, this.w_UNIMIS, "")
                          * --- TIPGES = '@' capisco che in TES_PLOS sono record derivanti da Kit Imballo
                          *     Altrimenti contiene ARTIPGES dell'articolo
                          this.w_GSAR_MIP.NextRow()     
                        enddo
                      endif
                      * --- Mi riposiziono sul transitorio principale dei documenti senza effettuare la savedependson
                    endif
                    this.w_PADRE.Repos(.T.)     
                  endif
                endif
                if used("TES_PLOS")
                  select TES_PLOS
                  LOCATE FOR NVL(FLSTAT, " ")="*"
                  if FOUND()
                    ah_ErrorMsg("Nel piano elaborato sono presenti distinte provvisorie; impossibile proseguire",,"")
                    this.Page_7()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                    i_retcode = 'stop'
                    return
                  else
                    * --- Aggiorna Cursore TMP_SCAR
                    this.Page_2()
                    if i_retcode='stop' or !empty(i_Error)
                      return
                    endif
                  endif
                endif
                * --- Inserisce  Righe componenti in EVA_COMP
                if USED("TMP_SCAR")
                  * --- Se esiste tmp
                  SELECT TMP_SCAR
                  if RECCOUNT()>0
                    * --- Considera solo le 'foglie'
                    GO TOP
                    DELETE FROM TMP_SCAR WHERE FLESPL="S"
                    GO TOP
                    * --- Raggruppa per Distinta+Componente
                    SELECT CODCOM, MVSERRIF, MVROWRIF, UNIMIS, ORIGIN, ;
                    MAX(ARTCOM) AS ARTCOM, SUM(QTAMOV) AS QTAMOV, SUM(QTAUM1) AS QTAUM1, ;
                    SUM(MVQTASAL) AS MVQTASAL, MAX(MVCODMAG) AS MVCODMAG, MAX(MVKEYSAL) AS MVKEYSAL, ;
                    MAX(MVFLORDI) AS MVFLORDI, MAX(MVFLIMPE) AS MVFLIMPE, MAX(MVFLRISE) AS MVFLRISE, MVDESSUP ;
                    FROM TMP_SCAR GROUP BY 1,2,3,4,5 ORDER BY 1,2,3 INTO CURSOR TMP_SCAR
                    USE IN SELECT("TES_PLOS")
                    if USED("TMP_SCAR") 
                      * --- Lettura Giorni per Approvvigionamento (del prodotto finito)
                      *     L'ottimo sarebbe creare una query che dati gli articoli recuperasse per ognuno
                      *     questa informazione onde evitare letture "doppie" sul database. Si potrebbe
                      *     pensare di utilizzare la scrittura del dettaglio della gestione esplosione mettendola in
                      *     Join con PAR_RIOR. Ad oggi la read non ha evidenziato problemi di lentezza.
                      * --- Read from PAR_RIOR
                      i_nOldArea=select()
                      if used('_read_')
                        select _read_
                        use
                      endif
                      i_nConn=i_TableProp[this.PAR_RIOR_idx,3]
                      i_cTable=cp_SetAzi(i_TableProp[this.PAR_RIOR_idx,2])
                      if i_nConn<>0
                        cp_sqlexec(i_nConn,"select "+;
                          "PRGIOAPP"+;
                          " from "+i_cTable+" PAR_RIOR where ";
                              +"PRCODART = "+cp_ToStrODBC(this.w_MVCODART);
                               ,"_read_")
                        i_Rows=iif(used('_read_'),reccount(),0)
                      else
                        select;
                          PRGIOAPP;
                          from (i_cTable) where;
                              PRCODART = this.w_MVCODART;
                           into cursor _read_
                        i_Rows=_tally
                      endif
                      if used('_read_')
                        locate for 1=1
                        this.w_GIOAPP = NVL(cp_ToDate(_read_.PRGIOAPP),cp_NullValue(_read_.PRGIOAPP))
                        use
                      else
                        * --- Error: sql sentence error.
                        i_Error = MSG_READ_ERROR
                        return
                      endif
                      select (i_nOldArea)
                      if i_Rows=0
                        this.w_GIOAPP = 0
                      endif
                      * --- Aggiorna Componenti (se Previsto)
                      select TMP_SCAR
                      if RECCOUNT()>0
                        this.w_CPROWORD = 0
                        GO TOP
                        SCAN FOR NOT EMPTY(CODCOM) AND QTAMOV>0
                        * --- Try
                        local bErr_041A3648
                        bErr_041A3648=bTrsErr
                        this.Try_041A3648()
                        * --- Catch
                        if !empty(i_Error)
                          i_ErrMsg=i_Error
                          i_Error=''
                          * --- accept error
                          bTrsErr=.f.
                        endif
                        bTrsErr=bTrsErr or bErr_041A3648
                        * --- End
                        select TMP_SCAR
                        ENDSCAN 
                      endif
                    endif
                  endif
                endif
              endif
            endif
          endif
          * --- Accumulo cursore Coefficenti
          if Used("APPCOE")
             
 UPDATE APPCOE SET CPROWNUM=this.w_CPROWNUM WHERE 1=1
            if Used("TOTCOE")
              * --- Devo valorizzare riferimento riga in TOTCOE contenuto 
              *     in w_CPROWNUM
               
 INSERT INTO TOTCOE SELECT * FROM APPCOE 
 
            else
               
 Select * from APPCOE into cursor TOTCOE 
 Wrcursor("TOTCOE")
            endif
            USE IN Select("APPCOE")
          endif
          SELECT APPOEVA
          ENDSCAN
        endif
      endif
      if this.w_TROV
        * --- Lancia la Manutenzione
        this.w_CONFERMA = .F.
        this.w_TIPOLN = " "
        * --- Legge flag esplosione Automatica e movimento di analitica
        this.w_APPO = this.w_PADRE.w_MVTIPDOC
        * --- Read from TIP_DOCU
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDEXPAUT,TDFLANAL"+;
            " from "+i_cTable+" TIP_DOCU where ";
                +"TDTIPDOC = "+cp_ToStrODBC(this.w_APPO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDEXPAUT,TDFLANAL;
            from (i_cTable) where;
                TDTIPDOC = this.w_APPO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_EXPAUT = NVL(cp_ToDate(_read_.TDEXPAUT),cp_NullValue(_read_.TDEXPAUT))
          this.w_PFLANAL = NVL(cp_ToDate(_read_.TDFLANAL),cp_NullValue(_read_.TDFLANAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if this.w_EXPAUT="S" OR lower(this.w_PADRE.class)=="tgsar_bgd"
          * --- Automatico
          do case
            case this.w_PDCRIVAL="L"
              this.w_DITIPVAL = this.w_PDCRIVAL
              if NOT EMPTY(this.w_PDCODLIS)
                this.w_MMTCOLIS = this.w_PDCODLIS
                this.w_MVCODVAL = g_PERVAL
                this.w_MVCAOVAL = g_CAOVAL
                * --- Read from LISTINI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.LISTINI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "LSIVALIS,LSVALLIS"+;
                    " from "+i_cTable+" LISTINI where ";
                        +"LSCODLIS = "+cp_ToStrODBC(this.w_MMTCOLIS);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    LSIVALIS,LSVALLIS;
                    from (i_cTable) where;
                        LSCODLIS = this.w_MMTCOLIS;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_TIPOLN = NVL(cp_ToDate(_read_.LSIVALIS),cp_NullValue(_read_.LSIVALIS))
                  this.w_MVCODVAL = NVL(cp_ToDate(_read_.LSVALLIS),cp_NullValue(_read_.LSVALLIS))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                if this.w_MVCODVAL=g_CODEUR OR this.w_MVCODVAL=g_CODLIR
                  this.w_MVCAOVAL = IIF(this.w_MVCODVAL=g_CODLIR, g_CAOEUR, 1)
                else
                  * --- Read from VALUTE
                  i_nOldArea=select()
                  if used('_read_')
                    select _read_
                    use
                  endif
                  i_nConn=i_TableProp[this.VALUTE_idx,3]
                  i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
                  if i_nConn<>0
                    cp_sqlexec(i_nConn,"select "+;
                      "VACAOVAL"+;
                      " from "+i_cTable+" VALUTE where ";
                          +"VACODVAL = "+cp_ToStrODBC(this.w_MVCODVAL);
                           ,"_read_")
                    i_Rows=iif(used('_read_'),reccount(),0)
                  else
                    select;
                      VACAOVAL;
                      from (i_cTable) where;
                          VACODVAL = this.w_MVCODVAL;
                       into cursor _read_
                    i_Rows=_tally
                  endif
                  if used('_read_')
                    locate for 1=1
                    this.w_MVCAOVAL = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
                    use
                  else
                    * --- Error: sql sentence error.
                    i_Error = MSG_READ_ERROR
                    return
                  endif
                  select (i_nOldArea)
                endif
                if this.w_MVCAOVAL<>0
                  this.w_CONFERMA = .T.
                endif
              endif
            case this.w_PDCRIVAL $ "SMUP"
              this.w_DITIPVAL = this.w_PDCRIVAL
              if NOT EMPTY(this.w_PDESEINV) AND NOT EMPTY(this.w_PDNUMINV)
                this.w_DICODESE = this.w_PDESEINV
                this.w_DINUMINV = this.w_PDNUMINV
                this.w_MVCODVAL = g_PERVAL
                * --- Read from ESERCIZI
                i_nOldArea=select()
                if used('_read_')
                  select _read_
                  use
                endif
                i_nConn=i_TableProp[this.ESERCIZI_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2])
                if i_nConn<>0
                  cp_sqlexec(i_nConn,"select "+;
                    "ESVALNAZ"+;
                    " from "+i_cTable+" ESERCIZI where ";
                        +"ESCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
                        +" and ESCODESE = "+cp_ToStrODBC(this.w_DICODESE);
                         ,"_read_")
                  i_Rows=iif(used('_read_'),reccount(),0)
                else
                  select;
                    ESVALNAZ;
                    from (i_cTable) where;
                        ESCODAZI = this.w_CODAZI;
                        and ESCODESE = this.w_DICODESE;
                     into cursor _read_
                  i_Rows=_tally
                endif
                if used('_read_')
                  locate for 1=1
                  this.w_MVCODVAL = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
                  use
                else
                  * --- Error: sql sentence error.
                  i_Error = MSG_READ_ERROR
                  return
                endif
                select (i_nOldArea)
                this.w_MVCAOVAL = IIF(this.w_MVCODVAL=g_CODLIR, g_CAOEUR, 1)
                this.w_CONFERMA = .T.
              endif
            case this.w_PDCRIVAL $ "XNA"
              this.w_DITIPVAL = this.w_PDCRIVAL
              this.w_CONFERMA = .T.
          endcase
        endif
        if Not this.w_CONFERMA
          * --- Manuale
          * --- L'HasEvent  � lanciata all'interno della Cp_DOaction che mette a .t.
          *     i_stopFk per impedire successive pressioni di tasto. Per poter
          *     gestire Esc e F10 sulla maschera la valorizzo a .F.
          if type("i_stopFK")="U"
            public i_stopFK
          endif
          this.w_OLD_STOPFK = i_stopFK
          i_stopFK=.f.
          do GSAR_KEA with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_stopFK=this.w_OLD_STOPFK
        endif
        * --- Controlli Finali sulla Valuta Documento (potrebbe non essere valorizzata in caso di nessun criterio di valorizzazione)
        if EMPTY(this.w_MVCODVAL) OR this.w_MVCAOVAL=0
          this.w_MVCODVAL = g_PERVAL
          this.w_MVCAOVAL = g_CAOVAL
          if NOT EMPTY(this.w_MMTCOLIS)
            * --- Verifica Congruita' Listini con la Valuta (se incongruente azzera)
            this.w_APPO = SPACE(3)
            * --- Read from LISTINI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.LISTINI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.LISTINI_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "LSVALLIS"+;
                " from "+i_cTable+" LISTINI where ";
                    +"LSCODLIS = "+cp_ToStrODBC(this.w_MMTCOLIS);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                LSVALLIS;
                from (i_cTable) where;
                    LSCODLIS = this.w_MMTCOLIS;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_APPO = NVL(cp_ToDate(_read_.LSVALLIS),cp_NullValue(_read_.LSVALLIS))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.w_MMTCOLIS = IIF(this.w_APPO=this.w_MVCODVAL, this.w_MMTCOLIS, SPACE(5))
          endif
        endif
        * --- Chiude i Cursori che non servono piu'
        USE IN SELECT("TES_PLOS")
        USE IN SELECT("APPOEVA")
        if this.w_CONFERMA
          * --- Se Evasione Confermata
          * --- Cambio e valuta calcolati in GSAR_BEV
          if NOT EMPTY(this.w_MMCAUCOD) OR NOT EMPTY(this.w_MMCAUPFI)
            * --- Legge Dati Associati alla Valuta
            this.w_DECTOT = 0
            this.w_DECUNI = 0
            this.w_BOLESE = 0
            this.w_BOLSUP = 0
            this.w_BOLCAM = 0
            this.w_BOLARR = 0
            this.w_BOLMIN = 0
            * --- Read from VALUTE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VALUTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM,VADECUNI"+;
                " from "+i_cTable+" VALUTE where ";
                    +"VACODVAL = "+cp_ToStrODBC(this.w_MVCODVAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                VADECTOT,VABOLESE,VABOLSUP,VABOLCAM,VABOLARR,VABOLMIM,VADECUNI;
                from (i_cTable) where;
                    VACODVAL = this.w_MVCODVAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DECTOT = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
              this.w_BOLESE = NVL(cp_ToDate(_read_.VABOLESE),cp_NullValue(_read_.VABOLESE))
              this.w_BOLSUP = NVL(cp_ToDate(_read_.VABOLSUP),cp_NullValue(_read_.VABOLSUP))
              this.w_BOLCAM = NVL(cp_ToDate(_read_.VABOLCAM),cp_NullValue(_read_.VABOLCAM))
              this.w_BOLARR = NVL(cp_ToDate(_read_.VABOLARR),cp_NullValue(_read_.VABOLARR))
              this.w_BOLMIN = NVL(cp_ToDate(_read_.VABOLMIM),cp_NullValue(_read_.VABOLMIM))
              this.w_DECUNI = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            this.Page_3()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
        endif
        * --- Ripulisce le tabelle temporanee
        * --- EVA_ARTI e EVA_COMP sono tabelle fisiche ma utilizzate come tabelle temporanee di appoggio
        *     Posso cancellare solo quelli con il solito seriale per il problema della multiutenza
        *     Un'altro utente potrebbe aver lanciato la stessa operazione.
        * --- Delete from EVA_COMP
        i_nConn=i_TableProp[this.EVA_COMP_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.EVA_COMP_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          delete from (i_cTable) where;
                MVSERIAL = this.w_SERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error='Errore cancellazione tabella EVA_COMP'
          return
        endif
        * --- Delete from EVA_ARTI
        i_nConn=i_TableProp[this.EVA_ARTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.EVA_ARTI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          delete from (i_cTable) where;
                MVSERIAL = this.w_SERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error='Errore cancellazione tabella EVA_ARTI'
          return
        endif
      endif
    endif
    this.Page_7()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    this.w_RITENTA = .F.
    this.w_OKPFI = .T.
    this.w_OKCOD = .T.
    if EMPTY(this.w_SERIAL)
      this.w_APPO = "Attenzione: per il documento registrato,%0non � stato generato nessun documento di evasione"
      ah_ErrorMsg(this.w_APPO,,"")
    else
      * --- La query prende solo le righe del documento con articoli che hanno associato 
      *     una distinta base o un articolo kit. Non considera quindi i kit imballi
      vq_exec("QUERY\GSARVBEA.VQR", this,"APPOTEST")
      if USED("APPOTEST")
        SELECT APPOTEST
        if RECCOUNT("APPOTEST")>0
          GO TOP
          LOCATE FOR NOT EMPTY(NVL(CAUPFI, "")) AND EMPTY(NVL(SERPFI, ""))
          if FOUND()
            this.w_OKPFI = .F.
            this.w_oMess.AddMsgPartNL("Attenzione: per il documento registrato,%0non � stato generato il documento di evasione prodotti finiti")     
          endif
          GO TOP
          this.w_TOTCOD = 0
          this.w_RIGCOD = 0
          SCAN FOR NOT EMPTY(NVL(CAUCOD, "")) AND EMPTY(NVL(SERCOD, ""))
          if this.w_TDFLEXPL<>"D" OR (this.w_TDFLEXPL="D" AND NOT EMPTY(NVL(SERRIF, " ")))
            * --- Il controllo non va fatto se Importo SOLO dai Documenti e l'articolo non ha riferimento a Importazione
            * --- (vale a dire e' stato caricato direttamente)
            this.w_TOTCOD = this.w_TOTCOD + 1
            if this.w_TOTCOD=1
              this.w_RIGCOD = NVL(CPROWORD, 0)
            endif
          endif
          ENDSCAN
          if this.w_TOTCOD>0
            this.w_OKCOD = .F.
            if this.w_OKPFI=.F.
              if this.w_TOTCOD>1
                this.w_oPart = this.w_oMess.AddMsgPartNL("Inoltre, non sono stati generati %1 documenti di evasione componenti (ad iniziare dalla riga: %2)") 
              else
                this.w_oPart = this.w_oMess.AddMsgPartNL("Inoltre, non sono stati generati %1 documenti di evasione componenti (riga: %2)") 
              endif
            else
              if this.w_TOTCOD>1
                this.w_oPart = this.w_oMess.AddMsgPartNL("Attenzione: per il documento registrato,%0non sono stati generati %1 documenti di evasione componenti (ad iniziare dalla riga: %2)") 
              else
                this.w_oPart = this.w_oMess.AddMsgPartNL("Attenzione: per il documento registrato,%0non sono stati generati %1 documenti di evasione componenti (riga: %2)") 
              endif
            endif
            this.w_oPart.AddParam(ALLTR(STR(this.w_TOTCOD)))     
            this.w_oPart.AddParam(ALLTRIM(STR(this.w_RIGCOD)))     
          endif
        endif
      endif
      if this.w_OKPFI=.F. OR this.w_OKCOD=.F.
        this.w_oMess.AddMsgPart("Si desidera ritentare la generazione dei documenti?")     
        this.w_RITENTA = this.w_oMESS.ah_YesNo()
      endif
    endif
    this.w_DOCRIG = .F.
    if this.w_RITENTA
      if USED("APPOTEST")
        SELECT APPOTEST
        if RECCOUNT("APPOTEST")>0
          * --- Storna Eventuali Documenti Collegati
          this.w_DOCEVA = SPACE(10)
          GO TOP
          LOCATE FOR NOT EMPTY(NVL(CAUPFI, "")) AND NOT EMPTY(NVL(SERPFI, ""))
          if FOUND()
            this.w_DOCEVA = SERPFI
          endif
          if NOT EMPTY(this.w_DOCEVA)
            this.w_SERORI = MVSERIAL
            this.Page_8()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          SELECT APPOTEST
          GO TOP
          SCAN FOR NOT EMPTY(NVL(CAUCOD, "")) AND NOT EMPTY(NVL(SERCOD, ""))
          this.w_DOCEVA = SERCOD
          if NOT EMPTY(this.w_DOCEVA)
            this.w_DOCRIG = .T.
            this.w_SERORI = MVSERIAL
            this.w_ROWORI = CPROWNUM
            this.Page_8()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          SELECT APPOTEST
          ENDSCAN
        endif
        USE IN SELECT("APPOTEST")
      endif
      this.w_PADRE.NotifyEvent("EsplodeDistinta")     
    endif
    if this.w_OKPFI Or this.w_OKCOD
      if g_VEFA="S" And this.oParentObject.w_MVTIPIMB$"RC"
        * --- Try
        local bErr_041F5DE8
        bErr_041F5DE8=bTrsErr
        this.Try_041F5DE8()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          if lower(this.w_PADRE.class)=="tgsar_bgd"
            this.w_PADRE.w_PADRE.AddLogMsg("W", ah_MsgFormat( "Si sono verificati errori nell'aggiornamento saldi imballi.%0� opportuno eseguire una ricostruzione saldi imballi per avere la situazione aggiornata" ))     
          else
            ah_ErrorMsg("Attenzione: si sono verificati errori nell'aggiornamento saldi imballi.%0� opportuno eseguire una ricostruzione saldi imballi per avere la situazione aggiornata")
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
          endif
        endif
        bTrsErr=bTrsErr or bErr_041F5DE8
        * --- End
        * --- Select from gsva_qcs
        do vq_exec with 'gsva_qcs',this,'_Curs_gsva_qcs','',.f.,.t.
        if used('_Curs_gsva_qcs')
          select _Curs_gsva_qcs
          locate for 1=1
          do while not(eof())
          * --- Recupero i saldi negativi dellintestatario e visualizzo il log Warning
          this.w_Imballo = _Curs_gsva_qcs.SICODART
          this.w_QtaSal = _Curs_gsva_qcs.SIQTAESI
          if lower(this.w_PADRE.class)=="tgsar_bgd"
            this.w_PADRE.w_PADRE.AddLogMsg("W", ah_MsgFormat("Intestatario: %1%0Imballo: %2%0Totale imballo reso superiore a quello preso%0Saldo: %3 ", ALLTRIM(this.w_MVCODCON), ALLTRIM(this.w_IMBALLO), Alltrim(Str(this.w_QtaSal,12,3)) ))     
          endif
          this.w_oPartLog = this.w_oMessLog.AddMsgPartNL("Intestatario: %1%0Imballo: %2%0Totale imballo reso superiore a quello preso%0Saldo: %3 ")
          this.w_oPartLog.AddParam(Alltrim(this.w_MVCODCON))     
          this.w_oPartLog.AddParam(Alltrim(this.w_Imballo))     
          this.w_oPartLog.AddParam(Alltrim(Str(this.w_QtaSal,12,3)))     
            select _Curs_gsva_qcs
            continue
          enddo
          use
        endif
      endif
      this.w_RESOCON1 = this.w_RESOCON1 +this.w_oMessLog.ComposeMessage()
      * --- Messaggio di log
      if ! Empty (this.w_MESBLOK) Or !Empty(this.w_RESOCON1)
        if lower(this.w_PADRE.class)=="tgsar_bgd"
          if !Empty (this.w_MESBLOK)
            this.w_PADRE.w_PADRE.AddLogMsg("E", ah_MsgFormat(this.w_MESBLOK))     
          endif
          if !Empty (this.w_RESOCON1)
            this.w_PADRE.w_PADRE.AddLogMsg("W", ah_MsgFormat(this.w_RESOCON1))     
          endif
        else
          * --- Visualizzo la maschera con all'interno tutti i messagi di errore
          this.w_ANNULLA = False
          this.w_DAIM = False
          * --- L'HasEvent  � lanciata all'interno della Cp_DOaction che mette a .t.
          *     i_stopFk per impedire successive pressioni di tasto. Per poter
          *     gestire Esc e F10 sulla maschera la valorizzo a .F.
          if type("i_stopFK")="U"
            public i_stopFK
          endif
          this.w_OLD_STOPFK = i_stopFK
          i_stopFK=.f.
          do GSVE_KLG with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_OK = this.w_ANNULLA
          i_stopFK=this.w_OLD_STOPFK
        endif
      endif
    endif
    USE IN SELECT("APPOTEST")
  endproc
  proc Try_041A8C28()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into EVA_ARTI
    i_nConn=i_TableProp[this.EVA_ARTI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.EVA_ARTI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.EVA_ARTI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVSERIAL"+",CPROWNUM"+",CPROWORD"+",MVCODICE"+",MVDESART"+",MVUNIMIS"+",MVQTAMOV"+",MVCODART"+",MVQTAUM1"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'EVA_ARTI','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'EVA_ARTI','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'EVA_ARTI','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'EVA_ARTI','MVCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'EVA_ARTI','MVDESART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'EVA_ARTI','MVUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'EVA_ARTI','MVQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'EVA_ARTI','MVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'EVA_ARTI','MVQTAUM1');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_SERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'MVCODICE',this.w_MVCODICE,'MVDESART',this.w_MVDESART,'MVUNIMIS',this.w_MVUNIMIS,'MVQTAMOV',this.w_MVQTAMOV,'MVCODART',this.w_MVCODART,'MVQTAUM1',this.w_MVQTAUM1)
      insert into (i_cTable) (MVSERIAL,CPROWNUM,CPROWORD,MVCODICE,MVDESART,MVUNIMIS,MVQTAMOV,MVCODART,MVQTAUM1 &i_ccchkf. );
         values (;
           this.w_SERIAL;
           ,this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_MVCODICE;
           ,this.w_MVDESART;
           ,this.w_MVUNIMIS;
           ,this.w_MVQTAMOV;
           ,this.w_MVCODART;
           ,this.w_MVQTAUM1;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento evasioni articoli composti'
      return
    endif
    this.w_TROV = .T.
    this.w_RECINS = .T.
    return
  proc Try_041A3648()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    this.w_MVNUMERO = this.w_MVNUMERO + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    this.w_MMCODICE = CODCOM
    this.w_MMCODART = ARTCOM
    this.w_MMUNIMIS = UNIMIS
    this.w_MMQTAMOV = QTAMOV
    this.w_MMQTAUM1 = QTAUM1
    this.w_ORIGIN = ORIGIN
    this.w_MVSERRIF = SPACE(10)
    this.w_MVROWRIF = 0
    this.w_EVQTASAL = 0
    this.w_EVCODMAG = SPACE(5)
    this.w_EVKEYSAL = SPACE(20)
    this.w_EVFLORDI = " "
    this.w_EVFLIMPE = " "
    this.w_EVFLRISE = " "
    this.w_MVDESSUP = NVL(MVDESSUP, " ")
    if this.w_FLEXPL="S"
      this.w_MVSERRIF = MVSERRIF
      this.w_MVROWRIF = MVROWRIF
      this.w_EVQTASAL = MVQTASAL
      this.w_EVCODMAG = MVCODMAG
      this.w_EVKEYSAL = MVKEYSAL
      this.w_EVFLORDI = MVFLORDI
      this.w_EVFLIMPE = MVFLIMPE
      this.w_EVFLRISE = MVFLRISE
    endif
    * --- Verifica le UM Frazionabili
    if this.w_ORIGIN="E" AND this.w_MMQTAMOV>0
      * --- Solo se Scarico Componenti da Esplosione Distinta Base
      this.w_QTAMOV = this.w_MMQTAMOV
      this.w_QTAUM1 = this.w_MMQTAUM1
      if used("UMNOFR")
        SELECT UMNOFR
        GO TOP
        LOCATE FOR UMCODICE=this.w_MMUNIMIS
        if FOUND()
          * --- Se U.M. Non Frazionabile aggiorna la Qta all'Intero Superiore
          this.w_MMQTAMOV = cp_ROUND(this.w_MMQTAMOV+.499, 0)
        endif
      endif
      this.w_MMQTAMOV = cp_ROUND(this.w_MMQTAMOV, g_PERPQT)
      * --- Se Variata Qta
      if this.w_QTAMOV<>this.w_MMQTAMOV
        this.w_MMQTAUM1 = (this.w_MMQTAMOV * this.w_QTAUM1) / this.w_QTAMOV
      endif
    endif
    * --- Sottrae i GG per Approvvigionamento alla data Evasione
    this.w_MMDATEVA = MAX(this.w_MVDATEVA - this.w_GIOAPP, this.w_MVDATDOC)
    ah_Msg("Scrittura movimento componenti; articolo: %1",.T.,.F.,.F., ALLTRIM(this.w_MMCODART) )
    * --- I dati dell'analitica sono ripetuti uguali per tutti i componenti.
    *     Sono uguali al prodotto finito indicato sulla riga del documento principale
    * --- Insert into EVA_COMP
    i_nConn=i_TableProp[this.EVA_COMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.EVA_COMP_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.EVA_COMP_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVSERIAL"+",MVROWNUM"+",CPROWNUM"+",MVTIPREC"+",CPROWORD"+",MVTIPRIG"+",MVCODICE"+",MVCODART"+",MVUNIMIS"+",MVQTAMOV"+",MVQTAUM1"+",MVCODCAU"+",MVDATEVA"+",MVSERRIF"+",MVROWRIF"+",MVQTASAL"+",MVCODMAG"+",MVKEYSAL"+",MVFLORDI"+",MVFLIMPE"+",MVFLRISE"+",MVFLERIF"+",MVDESSUP"+",MVMAGORI"+",MVMATORI"+",MVCODCEN"+",MVCODCOM"+",MVCODATT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_SERIAL),'EVA_COMP','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'EVA_COMP','MVROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMERO),'EVA_COMP','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC("2"),'EVA_COMP','MVTIPREC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'EVA_COMP','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC("R"),'EVA_COMP','MVTIPRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODICE),'EVA_COMP','MVCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCODART),'EVA_COMP','MVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMUNIMIS),'EVA_COMP','MVUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMQTAMOV),'EVA_COMP','MVQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMQTAUM1),'EVA_COMP','MVQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMCAUCOD),'EVA_COMP','MVCODCAU');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MMDATEVA),'EVA_COMP','MVDATEVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSERRIF),'EVA_COMP','MVSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVROWRIF),'EVA_COMP','MVROWRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EVQTASAL),'EVA_COMP','MVQTASAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EVCODMAG),'EVA_COMP','MVCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EVKEYSAL),'EVA_COMP','MVKEYSAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EVFLORDI),'EVA_COMP','MVFLORDI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EVFLIMPE),'EVA_COMP','MVFLIMPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_EVFLRISE),'EVA_COMP','MVFLRISE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLERIF),'EVA_COMP','MVFLERIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESSUP),'EVA_COMP','MVDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MAGORI),'EVA_COMP','MVMAGORI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MATORI),'EVA_COMP','MVMATORI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCEN),'EVA_COMP','MVCODCEN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'EVA_COMP','MVCODCOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODATT),'EVA_COMP','MVCODATT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.w_SERIAL,'MVROWNUM',this.w_CPROWNUM,'CPROWNUM',this.w_MVNUMERO,'MVTIPREC',"2",'CPROWORD',this.w_CPROWORD,'MVTIPRIG',"R",'MVCODICE',this.w_MMCODICE,'MVCODART',this.w_MMCODART,'MVUNIMIS',this.w_MMUNIMIS,'MVQTAMOV',this.w_MMQTAMOV,'MVQTAUM1',this.w_MMQTAUM1,'MVCODCAU',this.w_MMCAUCOD)
      insert into (i_cTable) (MVSERIAL,MVROWNUM,CPROWNUM,MVTIPREC,CPROWORD,MVTIPRIG,MVCODICE,MVCODART,MVUNIMIS,MVQTAMOV,MVQTAUM1,MVCODCAU,MVDATEVA,MVSERRIF,MVROWRIF,MVQTASAL,MVCODMAG,MVKEYSAL,MVFLORDI,MVFLIMPE,MVFLRISE,MVFLERIF,MVDESSUP,MVMAGORI,MVMATORI,MVCODCEN,MVCODCOM,MVCODATT &i_ccchkf. );
         values (;
           this.w_SERIAL;
           ,this.w_CPROWNUM;
           ,this.w_MVNUMERO;
           ,"2";
           ,this.w_CPROWORD;
           ,"R";
           ,this.w_MMCODICE;
           ,this.w_MMCODART;
           ,this.w_MMUNIMIS;
           ,this.w_MMQTAMOV;
           ,this.w_MMQTAUM1;
           ,this.w_MMCAUCOD;
           ,this.w_MMDATEVA;
           ,this.w_MVSERRIF;
           ,this.w_MVROWRIF;
           ,this.w_EVQTASAL;
           ,this.w_EVCODMAG;
           ,this.w_EVKEYSAL;
           ,this.w_EVFLORDI;
           ,this.w_EVFLIMPE;
           ,this.w_EVFLRISE;
           ,this.w_MVFLERIF;
           ,this.w_MVDESSUP;
           ,this.w_MAGORI;
           ,this.w_MATORI;
           ,this.w_MVCODCEN;
           ,this.w_MVCODCOM;
           ,this.w_MVCODATT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error='Errore in inserimento evasioni componenti'
      return
    endif
    this.w_TROV = .T.
    this.w_RECINS = .T.
    return
  proc Try_041F5DE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if lower(this.w_PADRE.class)<>"tgsar_bgd"
      * --- begin transaction
      cp_BeginTrs()
    endif
    * --- Aggiorno i saldi Imballi
    GSVA_BRS(this,this.w_SERIAL,"C")
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if lower(this.w_PADRE.class)<>"tgsar_bgd"
      * --- commit
      cp_EndTrs(.t.)
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna il temporaneo di Elaborazione
    if used("TES_PLOS")
      * --- Crea Cursore di Appoggio Ordine Prodotti
      USE IN SELECT("TMP_SCAR")
      CREATE CURSOR TMP_SCAR ;
      (CODCOM C(20), ARTCOM C(20), UNIMIS C(3), QTAMOV N(15,6), QTAUM1 N(15,6), COEIMP C(15), FLESPL C(1), ORIGIN C(1), ;
      MVSERRIF C(10), MVROWRIF N(4,0), MVQTASAL N(12,3), MVCODMAG C(5), MVKEYSAL C(20), MVFLORDI C(1), MVFLIMPE C(1), MVFLRISE C(1), MVDESSUP M(10))
      SELECT TES_PLOS
      GO TOP
      SCAN FOR NOT EMPTY(DISPAD)
      this.w_NUMLEV = NUMLEV
      this.w_UNIMIS = UNMIS0
      this.w_QTAMOV = QTAMOV
      this.w_QTAUM1 = IIF(QTAUM1=0, QTAMOV, QTAUM1)
      this.w_COEIMP = COEIMP
      this.w_FLESPL = FLESPL
      this.w_ARTCOM = ARTCOM
      this.w_MVSERRIF = SPACE(10)
      this.w_MVROWRIF = 0
      this.w_EVQTASAL = 0
      this.w_EVCODMAG = SPACE(5)
      this.w_EVKEYSAL = SPACE(20)
      this.w_EVFLORDI = " "
      this.w_EVFLIMPE = " "
      this.w_EVFLRISE = " "
      this.w_ORIGIN = "E"
      if this.w_FLEXPL="S" 
        this.w_ORIGIN = "D"
        this.w_MVSERRIF = NVL(DISPAD, SPACE(10))
        this.w_MVROWRIF = NVL(CPROWNUM, 0)
        this.w_EVQTASAL = NVL(MVQTASAL, 0)
        this.w_EVCODMAG = NVL(MVCODMAG, SPACE(5))
        this.w_EVKEYSAL = NVL(MVKEYSAL, SPACE(20))
        this.w_EVFLORDI = NVL(MVFLORDI, " ")
        this.w_EVFLIMPE = NVL(MVFLIMPE, " ")
        this.w_EVFLRISE = NVL(MVFLRISE, " ")
      else
        this.w_MVDESSUP = NVL(MVDESSUP, " ")
      endif
      if VAL(this.w_NUMLEV)=0
        this.w_QTAMOV = IIF(this.w_FLEXPL="S" , 1, this.w_MVQTAUM1)
        this.w_QTAUM1 = IIF(this.w_FLEXPL="S" , 1, this.w_MVQTAUM1)
        FOR L_i = 1 TO 99
        QTC[L_i] = 0
        ENDFOR
      else
        * --- Sommarizza le Quantita moltiplicandole per i Livelli Inferiori
        QTC[VAL(this.w_NUMLEV)] = this.w_QTAUM1
        * --- Parte dalle Quantita' da Produrre (Distinta=liv. 0000)
        this.w_QTAMOV = IIF(this.w_FLEXPL="S" , 1, this.w_MVQTAUM1)
        this.w_QTAUM1 = IIF(this.w_FLEXPL="S", 1, this.w_MVQTAUM1)
        FOR L_i = 1 TO VAL(this.w_NUMLEV)
        * --- Solo la riga del componente viene moltiplicato per l'effettivo valore, gli altri calcoli vanno riportati alla 1^UM
        *     Se TES_PLOS � costruito tramite query GSAR1BEA il campo TIPGES non esiste
        if Type("TIPGES")="C" And Nvl(TIPGES,"")="@"
          * --- Nel caso di Kit Imballi prendo le quantit� che sono state inserite nel dettaglio imballi 
          this.w_QTAMOV = QTAMOV
          this.w_QTAUM1 = QTAUM1
        else
          this.w_QTAMOV = this.w_QTAMOV * IIF(L_i=VAL(this.w_NUMLEV), QTAMOV, QTC[L_i])
          this.w_QTAUM1 = this.w_QTAUM1 * QTC[L_i]
        endif
        ENDFOR
        this.w_CODCOM = CODCOM
        if this.w_FLEXPL="S" AND this.w_MVQTAUM1<>this.w_QTAOR1 AND this.w_QTAOR1<>0
          * --- Se proviene dai Documenti ed evado una qta diversa calcola anche i componenti in proporzione
          this.w_QTAMOV = (this.w_QTAMOV * this.w_MVQTAMOV) / this.w_QTAORI
          this.w_QTAUM1 = (this.w_QTAUM1 * this.w_MVQTAUM1) / this.w_QTAOR1
        endif
        INSERT INTO TMP_SCAR (CODCOM, ARTCOM, UNIMIS, QTAMOV, QTAUM1, COEIMP, FLESPL, ORIGIN, ;
        MVSERRIF, MVROWRIF, MVQTASAL, MVCODMAG, MVKEYSAL, MVFLORDI, MVFLIMPE, MVFLRISE, MVDESSUP) ;
        VALUES (this.w_CODCOM, this.w_ARTCOM, this.w_UNIMIS, this.w_QTAMOV, this.w_QTAUM1, this.w_COEIMP, this.w_FLESPL, this.w_ORIGIN, ;
        this.w_MVSERRIF, this.w_MVROWRIF, this.w_EVQTASAL, this.w_EVCODMAG, this.w_EVKEYSAL, this.w_EVFLORDI, this.w_EVFLIMPE, this.w_EVFLRISE, this.w_MVDESSUP)
      endif
      SELECT TES_PLOS
      ENDSCAN
      select TES_PLOS
      use
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna Documenti di Evasione
    * --- Carica Temporaneo Evasioni
    vq_exec("QUERY\GSAR1QEC.VQR", this,"APPOEVA")
    * --- Se il batch viene lanciato per esplosione Kit, e quindi con modulo Vendite funzioni avanzate attivo,
    *     sulle causali non � impostabile la valorizzazione che quindi � fissa a N
    *     Il batch GSDS_BDC pu� quindi essere lasciato in Disb
    if USED("APPOEVA")
      SELECT APPOEVA
      if RECCOUNT()>0
        * --- Calcola i Costi Unitari e Globali, In base al Tipo di valorizzazione ...
        this.w_MVTCOLIS = this.w_MMTCOLIS
        do case
          case this.w_DITIPVAL="L"
            * --- Calcola su Listino
            ah_Msg("Lettura dati listini associati...",.T.)
            vq_exec("QUERY\GSARLQEC.VQR", this,"APPOVALO")
          case this.w_DITIPVAL="X"
            * --- Calcola su Ultimo Costo Standard Articolo
            vq_exec("QUERY\GSARXQEC.VQR", this,"APPOVALO")
          case this.w_DITIPVAL $ "SMUP"
            * --- Calcola su Inventario
            ah_Msg("Lettura dati inventario di riferimento...",.T.)
            if this.w_TDCOSEPL="S"
              * --- Calcola su Inventario nel caso di  opzione Valorizza da esplosione
              vq_exec("QUERY\GSARIQEC.VQR", this,"APPOVALO")
            else
              this.w_ACCEDE = .T.
            endif
          case this.w_DITIPVAL="A"
            * --- Calcola su Ultimo Costo dei Saldi (Articolo)
            this.w_ACCEDE = .T.
        endcase
        * --- Inizio Aggiornamento
        this.w_OKDOC = 0
        this.w_NUDOC = 0
        * --- Controllo analitica in pag6
        this.w_TESTANAL = .T.
        ah_Msg("Inizio generazione documenti collegati...",.T.)
        * --- Aggiorna Componenti (tiprec='2')
        this.w_FLAGG = "C"
        this.w_ROWPAD = APPOEVA.MVROWNUM
        if NOT EMPTY(this.w_MMCAUCOD) AND NOT EMPTY(this.w_MMCAMCOD) 
          * --- Testa Cambio Documento
          this.w_DBRK = -999
          * --- Legge Informazioni di Riga di Default
          this.w_MVTIPDOC = this.w_MMCAUCOD
          this.w_MVFLINTE = "N"
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDFLVEAC,TDEMERIC,TDALFDOC,TDCATDOC,TDPRODOC,TDFLACCO,TDFLINTE,TFFLRAGG,TDFLELAN,TD_SEGNO,TDVOCECR,TDFLANAL,TDFLCOMM,TDFLPDOC"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDFLVEAC,TDEMERIC,TDALFDOC,TDCATDOC,TDPRODOC,TDFLACCO,TDFLINTE,TFFLRAGG,TDFLELAN,TD_SEGNO,TDVOCECR,TDFLANAL,TDFLCOMM,TDFLPDOC;
              from (i_cTable) where;
                  TDTIPDOC = this.w_MVTIPDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MVFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
            this.w_MVEMERIC = NVL(cp_ToDate(_read_.TDEMERIC),cp_NullValue(_read_.TDEMERIC))
            this.w_MVALFDOC = NVL(cp_ToDate(_read_.TDALFDOC),cp_NullValue(_read_.TDALFDOC))
            this.w_MVCLADOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
            this.w_PRODOC = NVL(cp_ToDate(_read_.TDPRODOC),cp_NullValue(_read_.TDPRODOC))
            this.w_MVFLACCO = NVL(cp_ToDate(_read_.TDFLACCO),cp_NullValue(_read_.TDFLACCO))
            this.w_MVFLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
            this.w_MVTFRAGG = NVL(cp_ToDate(_read_.TFFLRAGG),cp_NullValue(_read_.TFFLRAGG))
            this.w_MVFLELAN = NVL(cp_ToDate(_read_.TDFLELAN),cp_NullValue(_read_.TDFLELAN))
            this.w_MV_SEGNO = NVL(cp_ToDate(_read_.TD_SEGNO),cp_NullValue(_read_.TD_SEGNO))
            this.w_VOCECR = NVL(cp_ToDate(_read_.TDVOCECR),cp_NullValue(_read_.TDVOCECR))
            this.w_FLANAL = NVL(cp_ToDate(_read_.TDFLANAL),cp_NullValue(_read_.TDFLANAL))
            this.w_FLGCOM = NVL(cp_ToDate(_read_.TDFLCOMM),cp_NullValue(_read_.TDFLCOMM))
            this.w_FLPDOC = NVL(cp_ToDate(_read_.TDFLPDOC),cp_NullValue(_read_.TDFLPDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_MVTIPCON = IIF(EMPTY(this.w_MVFLINTE) OR this.w_MVFLINTE="N", " ", this.w_MMTIPCON)
          this.w_MVCODCON = IIF(EMPTY(this.w_MVFLINTE) OR this.w_MVFLINTE="N", SPACE(15), this.w_MMCODCON)
          this.w_MVTCAMAG = this.w_MMCAMCOD
          this.w_MVCAUMAG = this.w_MVTCAMAG
          this.w_MFLCASC = " "
          this.w_MFLORDI = " "
          this.w_MFLIMPE = " "
          this.w_MFLRISE = " "
          this.w_MFLELGM = " "
          this.w_MFLAVAL = " "
          this.w_MCAUCOL = SPACE(5)
          this.w_MVCODMAT = SPACE(5)
          this.w_MF2CASC = " "
          this.w_MFLCOMM = " "
          this.w_MF2ORDI = " "
          this.w_MF2IMPE = " "
          this.w_MF2RISE = " "
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM,CMCAUCOL,CMFLCOMM"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.w_MVTCAMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM,CMCAUCOL,CMFLCOMM;
              from (i_cTable) where;
                  CMCODICE = this.w_MVTCAMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            this.w_MFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            this.w_MFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
            this.w_MFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
            this.w_MFLAVAL = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
            this.w_MFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
            this.w_MCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
            this.w_MFLCOMM = NVL(cp_ToDate(_read_.CMFLCOMM),cp_NullValue(_read_.CMFLCOMM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Documento di Trasferimento: legge dati Causale Collegata
          if NOT EMPTY(this.w_MCAUCOL)
            * --- Read from CAM_AGAZ
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE"+;
                " from "+i_cTable+" CAM_AGAZ where ";
                    +"CMCODICE = "+cp_ToStrODBC(this.w_MCAUCOL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
                from (i_cTable) where;
                    CMCODICE = this.w_MCAUCOL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MF2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
              this.w_MF2ORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
              this.w_MF2IMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
              this.w_MF2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.w_AGGSAL = ALLTRIM(this.w_MFLCASC+this.w_MFLRISE+this.w_MFLORDI+this.w_MFLIMPE)
          this.w_AGGSAL1 = ALLTRIM(this.w_MF2CASC+this.w_MF2RISE+this.w_MF2ORDI+this.w_MF2IMPE)
          SELECT APPOEVA
          GO TOP
          SCAN FOR NVL(MVTIPREC, " ")="2" AND NVL(MVROWNUM, 0)<>0
          this.w_TIPREC = "2"
          this.w_ROWPAD = APPOEVA.MVROWNUM
          this.w_ROWDISP = APPOEVA.ROWDISP
          this.w_ROWCOM = APPOEVA.CPROWNUM
          if this.w_DBRK<>MVROWNUM
            * --- Inizializza i dati di Testata del Nuovo Documento
            this.w_ARTPAD = APPOEVA.CODART
            this.w_QUANTI = NVL(MVQTAUM1, 0)
            if this.w_DITIPVAL $ "SMUP"
              * --- Lancio il batch valorizzazioni per calcolare 
              *     il valore da inventario dei componenti
              if Used("TOTCOE")
                Select * from TOTCOE where CPROWNUM=this.w_ROWPAD into Cursor APPCOE
              endif
              do GSDS_BDC with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            SELECT APPOEVA
            * --- Attenzione: w_DBRK viene usato anche per puntare al Documento di Origine
            this.w_DBRK = MVROWNUM
          endif
          * --- Scrive nuova Riga sul Temporaneo di Appoggio
          this.w_MVTIPRIG = IIF(EMPTY(NVL(MVTIPRIG,"")), "R", MVTIPRIG)
          this.w_MVCODICE = NVL(MVCODICE, SPACE(20))
          this.w_MVCODART = NVL(MVCODART, SPACE(20))
          this.w_MVDESART = NVL(MVDESART, SPACE(40))
          this.w_MVDESSUP = NVL(MVDESSUP, SPACE(10))
          * --- Traduzione descrizione articoli...
          if ! (EMPTY(this.w_MVCODICE) OR EMPTY(this.w_CODLIN) OR this.w_CODLIN=g_CODLIN)
            * --- Legge la Traduzione
            DECLARE ARRRIS (2) 
 a=Tradlin(this.w_MVCODICE,this.w_CODLIN,"TRADARTI",@ARRRIS,"B") 
 this.w_DESCOD=ARRRIS(1) 
 this.w_DESSUP=ARRRIS(2)
            if Not Empty(this.w_DESCOD)
              this.w_MVDESART = this.w_DESCOD
              this.w_MVDESSUP = this.w_DESSUP
            endif
          endif
          this.w_MVUNIMIS = NVL(MVUNIMIS, SPACE(3))
          this.w_MVCATCON = NVL(ARCATCON, SPACE(5))
          this.w_MVCODCLA = NVL(ARCODCLA, SPACE(3))
          this.w_FLLOTT = NVL(ARFLLOTT, " ")
          this.w_MVCONTRA = this.w_MVTCONTR
          this.w_MVCODLIS = this.w_MVTCOLIS
          this.w_MVQTAMOV = NVL(MVQTAMOV , 0)
          this.w_MVQTAUM1 = NVL(MVQTAUM1, 0)
          this.w_MVDATEVA = CP_TODATE(MVDATEVA)
          this.w_MVSERRIF = SPACE(10)
          this.w_MVROWRIF = 0
          this.w_EVQTASAL = 0
          this.w_EVKEYSAL = SPACE(20)
          this.w_EVCODMAG = SPACE(5)
          this.w_EVFLORDI = " "
          this.w_EVFLIMPE = " "
          this.w_EVFLRISE = " "
          this.w_MVFLERIF = NVL(MVFLERIF, " ")
          * --- Mag. Doc.Origine / Preferenziale Articolo
          this.w_MAGORI = NVL(MVMAGORI, SPACE(5))
          this.w_MATORI = NVL(MVMATORI, SPACE(5))
          this.w_MAGPRE = NVL(ARMAGPRE, SPACE(5))
          * --- aggiorna i riferimenti per evasione Documento di Origine
          if this.w_TDFLEXPL $ "SD" AND EMPTY(this.w_MCAUCOL)
            * --- Se Importa Componenti da Documento di Origine e la Causale del Doc Destinazione no trasferimento
            if NOT EMPTY(NVL(MVKEYSAL, "")) AND NVL(MVKEYSAL, "")=this.w_MVCODART AND NOT EMPTY(NVL(MVCODMAG, ""))
              * --- Verifica anche la Congruita tra l'articolo del Documento e quello da evadere nonche' la presenza del Magazzino
              this.w_MVSERRIF = NVL(MVSERRIF, SPACE(10))
              this.w_MVROWRIF = NVL(MVROWRIF, 0)
              this.w_EVQTASAL = NVL(MVQTASAL, 0)
              this.w_EVKEYSAL = NVL(MVKEYSAL, SPACE(20))
              this.w_EVCODMAG = NVL(MVCODMAG, SPACE(5))
              this.w_EVFLORDI = NVL(MVFLORDI, " ")
              this.w_EVFLIMPE = NVL(MVFLIMPE, " ")
              this.w_EVFLRISE = NVL(MVFLRISE, " ")
              this.w_MVDESSUP = NVL(MVDESSUP, SPACE(10))
              * --- Considera come magazzino Origine quello sul documento Importato (tranne caso Default)
              if this.w_FLMGPR_C $ "OFPI"
                this.w_MAGORI = IIF(NOT EMPTY(this.w_EVCODMAG) AND NOT EMPTY(this.w_MVSERRIF), this.w_EVCODMAG, this.w_MAGORI)
              endif
            endif
          endif
          * --- Calcola magazzino in base alle priorita'
          this.w_MVCODMAG = CALCMAG(3, this.w_FLMGPR_C, this.w_MAGORI, this.w_MAGDES_C, this.w_MAGDES_C, this.w_MAGPRE, this.w_MAGTER)
          this.w_MVCODMAT = CALCMAG(3, this.w_FLMTPR_C, this.w_MATORI, this.w_MATDES_C, this.w_MATDES_C, this.w_MAGPRE, this.w_MAGTER)
          * --- Azzera Magazzino se la Causale non lo Gestisce
          this.w_MVCODMAG = IIF(EMPTY(this.w_MFLCASC+this.w_MFLRISE+this.w_MFLORDI+this.w_MFLIMPE), SPACE(5), this.w_MVCODMAG)
          this.w_MVCODMAT = IIF(EMPTY(this.w_MF2CASC+this.w_MF2RISE+this.w_MF2ORDI+this.w_MF2IMPE) OR EMPTY(this.w_MCAUCOL), SPACE(5), this.w_MVCODMAT)
          this.w_FLUBIC = " "
          this.w_F2UBIC = " "
          if g_PERUBI="S"
            * --- Se gestite Ubicazioni
            if NOT EMPTY(this.w_MVCODMAG)
              * --- Read from MAGAZZIN
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MGFLUBIC"+;
                  " from "+i_cTable+" MAGAZZIN where ";
                      +"MGCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MGFLUBIC;
                  from (i_cTable) where;
                      MGCODMAG = this.w_MVCODMAG;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_FLUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if NOT EMPTY(this.w_MVCODMAT)
              * --- Read from MAGAZZIN
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MGFLUBIC"+;
                  " from "+i_cTable+" MAGAZZIN where ";
                      +"MGCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MGFLUBIC;
                  from (i_cTable) where;
                      MGCODMAG = this.w_MVCODMAT;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_F2UBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          endif
          * --- Verifica presenza magazzino
          if not EMPTY(this.w_MFLCASC+this.w_MFLRISE+this.w_MFLORDI+this.w_MFLIMPE) and empty(this.w_MVCODMAG)
            this.w_oPartLog1 = this.w_oMessLog1.AddMsgPartNL("Documento da generare: %1; articolo: %2%0Magazzino mancante%0")
            this.w_oPartLog1.AddParam(Alltrim(this.w_MVTIPDOC))     
            this.w_oPartLog1.AddParam(Alltrim(this.w_MVCODART))     
          endif
          this.w_ONLYAGG = .F.
          this.Page_6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if this.w_TDCOSEPL="S"
            * --- Nel caso di valorizzazione dei prodotti finiti attraverso i dati della maschera di esplosione
            *     memorizzo prezzo dei componenti calcolato attraverso la valorizzazione pervista nella
            *     tabella di appoggio EVA_COMP per riprenderlo successivamente in pag 9
            * --- Write into EVA_COMP
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.EVA_COMP_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.EVA_COMP_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.EVA_COMP_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"MVPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_MVPREZZO),'EVA_COMP','MVPREZZO');
                  +i_ccchkf ;
              +" where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                  +" and MVROWNUM = "+cp_ToStrODBC(this.w_ROWPAD);
                  +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWCOM);
                     )
            else
              update (i_cTable) set;
                  MVPREZZO = this.w_MVPREZZO;
                  &i_ccchkf. ;
               where;
                  MVSERIAL = this.w_SERIAL;
                  and MVROWNUM = this.w_ROWPAD;
                  and CPROWNUM = this.w_ROWCOM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
          SELECT APPOEVA
          ENDSCAN
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        * --- Aggiorna Prodotti Finiti (tiprec='1')
        this.w_FLAGG = "P"
        this.w_CREAESP = (this.w_OLDTES <> Alltrim(this.w_MMCODCON)+Alltrim(this.w_CODTLIS)+Alltrim(DTOC(this.w_MVDATDOC))) OR Empty(this.w_RIFESP) OR this.oParentObject.w_MVTIPIMB$"RC"
        if NOT EMPTY(this.w_MMCAUPFI) AND NOT EMPTY(this.w_MMCAMPFI) and this.w_CREAESP
          * --- Testa Cambio Documento
          this.w_DBRK = -999
          * --- Legge Informazioni di Riga di Default
          this.w_MVTIPDOC = this.w_MMCAUPFI
          this.w_MVFLINTE = "N"
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDFLVEAC,TDEMERIC,TDALFDOC,TDCATDOC,TDPRODOC,TDFLACCO,TDFLINTE,TFFLRAGG,TDFLELAN,TD_SEGNO,TDVOCECR,TDFLANAL,TDFLCOMM,TDFLPDOC"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.w_MVTIPDOC);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDFLVEAC,TDEMERIC,TDALFDOC,TDCATDOC,TDPRODOC,TDFLACCO,TDFLINTE,TFFLRAGG,TDFLELAN,TD_SEGNO,TDVOCECR,TDFLANAL,TDFLCOMM,TDFLPDOC;
              from (i_cTable) where;
                  TDTIPDOC = this.w_MVTIPDOC;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MVFLVEAC = NVL(cp_ToDate(_read_.TDFLVEAC),cp_NullValue(_read_.TDFLVEAC))
            this.w_MVEMERIC = NVL(cp_ToDate(_read_.TDEMERIC),cp_NullValue(_read_.TDEMERIC))
            this.w_MVALFDOC = NVL(cp_ToDate(_read_.TDALFDOC),cp_NullValue(_read_.TDALFDOC))
            this.w_MVCLADOC = NVL(cp_ToDate(_read_.TDCATDOC),cp_NullValue(_read_.TDCATDOC))
            this.w_PRODOC = NVL(cp_ToDate(_read_.TDPRODOC),cp_NullValue(_read_.TDPRODOC))
            this.w_MVFLACCO = NVL(cp_ToDate(_read_.TDFLACCO),cp_NullValue(_read_.TDFLACCO))
            this.w_MVFLINTE = NVL(cp_ToDate(_read_.TDFLINTE),cp_NullValue(_read_.TDFLINTE))
            this.w_MVTFRAGG = NVL(cp_ToDate(_read_.TFFLRAGG),cp_NullValue(_read_.TFFLRAGG))
            this.w_MVFLELAN = NVL(cp_ToDate(_read_.TDFLELAN),cp_NullValue(_read_.TDFLELAN))
            this.w_MV_SEGNO = NVL(cp_ToDate(_read_.TD_SEGNO),cp_NullValue(_read_.TD_SEGNO))
            this.w_VOCECR = NVL(cp_ToDate(_read_.TDVOCECR),cp_NullValue(_read_.TDVOCECR))
            this.w_FLANAL = NVL(cp_ToDate(_read_.TDFLANAL),cp_NullValue(_read_.TDFLANAL))
            this.w_FLGCOM = NVL(cp_ToDate(_read_.TDFLCOMM),cp_NullValue(_read_.TDFLCOMM))
            this.w_FLPDOC = NVL(cp_ToDate(_read_.TDFLPDOC),cp_NullValue(_read_.TDFLPDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_MVTIPCON = IIF(EMPTY(this.w_MVFLINTE) OR this.w_MVFLINTE="N", " ", this.w_MMTIPCON)
          this.w_MVCODCON = IIF(EMPTY(this.w_MVFLINTE) OR this.w_MVFLINTE="N", SPACE(15), this.w_MMCODCON)
          this.w_MVTCAMAG = this.w_MMCAMPFI
          this.w_MVCAUMAG = this.w_MVTCAMAG
          this.w_MFLCASC = " "
          this.w_MFLORDI = " "
          this.w_MFLIMPE = " "
          this.w_MFLRISE = " "
          this.w_MFLELGM = " "
          this.w_MFLAVAL = " "
          this.w_MCAUCOL = SPACE(5)
          this.w_MVCODMAT = SPACE(5)
          this.w_MF2CASC = " "
          this.w_MF2ORDI = " "
          this.w_MF2IMPE = " "
          this.w_MF2RISE = " "
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM,CMCAUCOL"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.w_MVTCAMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE,CMFLAVAL,CMFLELGM,CMCAUCOL;
              from (i_cTable) where;
                  CMCODICE = this.w_MVTCAMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            this.w_MFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            this.w_MFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
            this.w_MFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
            this.w_MFLAVAL = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
            this.w_MFLELGM = NVL(cp_ToDate(_read_.CMFLELGM),cp_NullValue(_read_.CMFLELGM))
            this.w_MCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Documento di Trasferimento: legge dati Causale Collegata
          if NOT EMPTY(this.w_MCAUCOL)
            * --- Read from CAM_AGAZ
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE"+;
                " from "+i_cTable+" CAM_AGAZ where ";
                    +"CMCODICE = "+cp_ToStrODBC(this.w_MCAUCOL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
                from (i_cTable) where;
                    CMCODICE = this.w_MCAUCOL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MF2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
              this.w_MF2ORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
              this.w_MF2IMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
              this.w_MF2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.w_AGGSAL = ALLTRIM(this.w_MFLCASC+this.w_MFLRISE+this.w_MFLORDI+this.w_MFLIMPE)
          this.w_AGGSAL1 = ALLTRIM(this.w_MF2CASC+this.w_MF2RISE+this.w_MF2ORDI+this.w_MF2IMPE)
          SELECT APPOEVA
          GO TOP
          SCAN FOR NVL(MVTIPREC, " ")="1"
          this.w_TIPREC = "1"
          this.w_ROWPAD = APPOEVA.MVROWNUM
          this.w_ROWDISP = APPOEVA.ROWDISP
          if this.w_DBRK=-999
            * --- Serve solo per inizializzare i dati di testata! , (l' aggiornamento vero e proprio avviene solo alla fine)
            * --- Inizializza i dati di Testata del Nuovo Documento (una volta sola)
            this.Page_4()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            SELECT APPOEVA
            this.w_DBRK = MVROWNUM
          endif
          this.w_ARTPAD = NVL(APPOEVA.MVCODART, SPACE(20))
          this.w_QUANTI = NVL(MVQTAUM1, 0)
          if this.w_TDCOSEPL="N"
            if this.w_VALCOM <>"N" or this.w_DITIPVAL $ "SMUP"
              * --- Lancio il batch valorizzazioni al cambiare del prodotto finito
              *     devo creare opportunamente appocoe in base a totce filtrato 
              *     per w_ROWPAD
              if Used("TOTCOE")
                Select * from TOTCOE where CPROWNUM=this.w_ROWPAD into Cursor APPCOE
              endif
              do GSDS_BDC with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
          endif
          * --- Scrive nuova Riga sul Temporaneo di Appoggio
          Select APPOEVA
          this.w_MVTIPRIG = IIF(EMPTY(NVL(MVTIPRIG,"")), "R", MVTIPRIG)
          this.w_MVCODICE = NVL(MVCODICE, SPACE(20))
          this.w_MVCODART = NVL(MVCODART, SPACE(20))
          this.w_MVDESART = NVL(MVDESART, SPACE(40))
          this.w_MVDESSUP = NVL(MVDESSUP, SPACE(10))
          this.w_MVUNIMIS = NVL(MVUNIMIS, SPACE(3))
          this.w_MVCATCON = NVL(ARCATCON, SPACE(5))
          this.w_MVCODCLA = NVL(ARCODCLA, SPACE(3))
          this.w_FLLOTT = NVL(ARFLLOTT, " ")
          this.w_MVCONTRA = this.w_MVTCONTR
          this.w_MVCODLIS = this.w_MVTCOLIS
          this.w_MVQTAMOV = NVL(MVQTAMOV , 0)
          this.w_MVQTAUM1 = NVL(MVQTAUM1, 0)
          this.w_MVDATEVA = CP_TODATE(MVDATEVA)
          this.w_MVSERRIF = SPACE(10)
          this.w_MVROWRIF = 0
          this.w_EVQTASAL = 0
          this.w_EVKEYSAL = SPACE(20)
          this.w_EVCODMAG = SPACE(5)
          this.w_EVFLORDI = " "
          this.w_EVFLIMPE = " "
          this.w_EVFLRISE = " "
          this.w_MVFLERIF = NVL(MVFLERIF, " ")
          * --- Mag. Doc.Origine / Preferenziale Articolo
          this.w_MAGORI = NVL(MVMAGORI, SPACE(5))
          this.w_MATORI = NVL(MVMATORI, SPACE(5))
          this.w_MAGPRE = NVL(ARMAGPRE, SPACE(5))
          * --- Calcola magazzino in base alle priorita'
          this.w_MVCODMAG = CALCMAG(3, this.w_FLMGPR_P, this.w_MAGORI, this.w_MAGDES_P, this.w_MAGDES_P, this.w_MAGPRE, this.w_MAGTER)
          this.w_MVCODMAT = CALCMAG(3, this.w_FLMTPR_P, this.w_MATORI, this.w_MATDES_P, this.w_MATDES_P, this.w_MAGPRE, this.w_MAGTER)
          * --- Azzera Magazzino se la Causale non lo Gestisce
          this.w_MVCODMAG = IIF(EMPTY(this.w_MFLCASC+this.w_MFLRISE+this.w_MFLORDI+this.w_MFLIMPE), SPACE(5), this.w_MVCODMAG)
          this.w_MVCODMAT = IIF(EMPTY(this.w_MF2CASC+this.w_MF2RISE+this.w_MF2ORDI+this.w_MF2IMPE) OR EMPTY(this.w_MCAUCOL), SPACE(5), this.w_MVCODMAT)
          this.w_FLUBIC = " "
          this.w_F2UBIC = " "
          if g_PERUBI="S"
            * --- Se gestite Ubicazioni
            if NOT EMPTY(this.w_MVCODMAG)
              * --- Read from MAGAZZIN
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MGFLUBIC"+;
                  " from "+i_cTable+" MAGAZZIN where ";
                      +"MGCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MGFLUBIC;
                  from (i_cTable) where;
                      MGCODMAG = this.w_MVCODMAG;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_FLUBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
            if NOT EMPTY(this.w_MVCODMAT)
              * --- Read from MAGAZZIN
              i_nOldArea=select()
              if used('_read_')
                select _read_
                use
              endif
              i_nConn=i_TableProp[this.MAGAZZIN_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.MAGAZZIN_idx,2])
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select "+;
                  "MGFLUBIC"+;
                  " from "+i_cTable+" MAGAZZIN where ";
                      +"MGCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
                       ,"_read_")
                i_Rows=iif(used('_read_'),reccount(),0)
              else
                select;
                  MGFLUBIC;
                  from (i_cTable) where;
                      MGCODMAG = this.w_MVCODMAT;
                   into cursor _read_
                i_Rows=_tally
              endif
              if used('_read_')
                locate for 1=1
                this.w_F2UBIC = NVL(cp_ToDate(_read_.MGFLUBIC),cp_NullValue(_read_.MGFLUBIC))
                use
              else
                * --- Error: sql sentence error.
                i_Error = MSG_READ_ERROR
                return
              endif
              select (i_nOldArea)
            endif
          endif
          * --- aggiorna i riferimenti per evasione Documento di Origine
          this.w_ONLYAGG = .F.
          this.Page_6()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          SELECT APPOEVA
          ENDSCAN
          * --- Per i Prodotti finiti genera un solo Documento
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          if NOT EMPTY(this.w_MMCAUCOD) AND NOT EMPTY(this.w_MMCAMCOD) 
            * --- Non devo fare i controlli analitica in pag6
            this.w_TESTANAL = .F.
            * --- Nel caso in cui non devo generare sottodocumento per prodotto finito
            *     Devo comunque aggiornare prezzo riga principale dai componenti
            *     in base all'impostazione nella causale documento
            this.w_TIPREC = "1"
            * --- Utilizzo il filtro MVSERIAL = w_SERIAL per la multiutenza .
            *     Un'alro utente potrebbe aver lanciato la stessa operazione con la possibilit� quindi
            *     di avere all'interno di EVA_ARTI anche gli articoli di un'altro documento
            * --- Select from EVA_ARTI
            i_nConn=i_TableProp[this.EVA_ARTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.EVA_ARTI_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select CPROWNUM,MVCODICE,MVQTAMOV,MVCODART,MVQTAUM1  from "+i_cTable+" EVA_ARTI ";
                  +" where MVSERIAL="+cp_ToStrODBC(this.w_SERIAL)+"";
                   ,"_Curs_EVA_ARTI")
            else
              select CPROWNUM,MVCODICE,MVQTAMOV,MVCODART,MVQTAUM1 from (i_cTable);
               where MVSERIAL=this.w_SERIAL;
                into cursor _Curs_EVA_ARTI
            endif
            if used('_Curs_EVA_ARTI')
              select _Curs_EVA_ARTI
              locate for 1=1
              do while not(eof())
              this.w_ROWPAD = _Curs_EVA_ARTI.CPROWNUM
              this.w_MVCODART = _Curs_EVA_ARTI.MVCODART
              this.w_MVQTAMOV = Nvl(_Curs_EVA_ARTI.MVQTAMOV,0)
              this.w_MVQTAUM1 = Nvl(_Curs_EVA_ARTI.MVQTAUM1,0)
              if this.w_VALCOM <>"N" 
                this.w_ARTPAD = this.w_MVCODART
                this.w_QUANTI = this.w_MVQTAUM1
                if this.w_TDCOSEPL="N"
                  * --- Lancio il batch valorizzazioni al cambiare del prodotto finito
                  *     devo creare opportunamente appocoe in base a totce filtrato 
                  *     per w_ROWPAD
                  if Used("TOTCOE")
                    Select * from TOTCOE where CPROWNUM=this.w_ROWPAD into Cursor APPCOE
                  endif
                  do GSDS_BDC with this
                  if i_retcode='stop' or !empty(i_Error)
                    return
                  endif
                endif
              endif
              this.w_ONLYAGG = .T.
              this.Page_6()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
                select _Curs_EVA_ARTI
                continue
              enddo
              use
            endif
          endif
        endif
        if this.w_VALCOM $ "P-E-D" AND this.w_RICTOT
          * --- Aggiorno totali documento principale se Modificato MVPREZZO
          *     in Pagina 6 attraverso la Write in Doc_Dett
          GSAR_BRD(this,this.w_SERIAL)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        if this.w_NUDOC>0 AND this.w_NUDOC<>this.w_OKDOC
          if lower(this.w_PADRE.class)=="tgsar_bgd"
            this.w_PADRE.w_PADRE.AddLogMsg("W", ah_MsgFormat( "Esplosione kit completata%0N.%1 documenti generati%0Su %2 documenti da generare", ALLTRIM(STR(this.w_OKDOC)), ALLTRIM(STR(this.w_NUDOC)) ))     
          else
            ah_ErrorMsg("Operazione completata%0N.%1 documenti generati%0Su %2 documenti da generare","","", ALLTRIM(STR(this.w_OKDOC)), ALLTRIM(STR(this.w_NUDOC)) )
            this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati")     
          endif
        endif
      endif
    endif
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se non e' il Primo Ingresso, Scrive il Nuovo Documento di Destinazione
    if this.w_DBRK <> -999 AND RECCOUNT("GeneApp") > 0
      * --- Calcola Sconti su Omaggio
      this.w_MVFLSCOM = IIF(g_FLSCOM="S" AND this.w_MVDATDOC<g_DTSCOM AND !empty(nvl(g_DTSCOM,cp_CharToDate("  /  /    ")))," ",g_FLSCOM)
      * --- Aggiorna le Spese Accessorie e gli Sconti Finali
      this.w_MVSCONTI = Calsco(this.w_TOTMERCE, this.w_MVSCOCL1, this.w_MVSCOCL2, this.w_MVSCOPAG, this.w_DECTOT)
      * --- Lancia Batch per ripartizione spese accessorie
      *     Il 5� e l'ultimo parametro sono le spese accessorie in questo caso non esistono non esistono
      this.w_DATCOM = IIF(Empty(this.w_MVDATDOC),this.w_MVDATREG, this.w_MVDATDOC)
      GSAR_BRS(this,"B", "GeneApp", this.w_MVFLVEAC, this.w_MVFLSCOR, 0, this.w_MVSCONTI, this.w_TOTMERCE, this.w_MVCODIVE, this.w_MVCAOVAL, this.w_DATCOM, this.w_CAONAZ, this.w_MVVALNAZ, this.w_MVCODVAL, 0)
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      * --- Calcoli Finali
      this.w_MVFLSALD = " "
      * --- Calcola MVSERIAL, MVNUMREG, MVNUMDOC
      this.w_MVSERIAL = SPACE(10)
      this.w_MVNUMREG = 0
      this.w_MVANNDOC = STR(YEAR(this.w_MVDATDOC), 4, 0)
      this.w_CPROWNUM = 0
      this.w_CPROWORD = 0
      this.w_MVNUMRIF = -20
      this.w_NUDOC = this.w_NUDOC + 1
      this.w_RESOCON1 = this.w_oMessLog1.ComposeMessage()
      if this.w_MFLCASC="-" Or this.w_MFLRISE="+"
        * --- Creazione del cursore da passare al controllo disponibilit� per magazzino principale
        *     Solo diminuzione esistenza o aumento di riservato
         
 Select ROWDISP as Riga, Sum(t_MVQTAUM1*IIF( this.w_MFLCASC="-",-1, IIF( this.w_MFLCASC="+",1, 0)) - t_MVQTAUM1*IIF( this.w_MFLRISE="-",-1, IIF( this.w_MFLRISE="+",1, 0))) As Disp , ; 
 Sum(t_MVQTAUM1*IIF( this.w_MFLCASC="-",-1, IIF( this.w_MFLCASC="+",1, 0)) - t_MVQTAUM1*IIF( this.w_MFLRISE="-",-1, IIF( this.w_MFLRISE="+",1, 0)) +t_MVQTAUM1*IIF( this.w_MFLORDI="-",-1, IIF( this.w_MFLORDI="+",1, 0)) - t_MVQTAUM1*IIF( this.w_MFLIMPE="-",-1, IIF( this.w_MFLIMPE="+",1, 0))) As DispC , ; 
 Sum(t_MVQTAUM1*IIF( this.w_MFLRISE="-",-1, IIF( this.w_MFLRISE="+",1, 0))) As DispR , ; 
 t_MVCODMAG As CodMag, t_MVCODART As KEYSAL,Alltrim(this.w_MFLCASC+this.w_MFLRISE) As Mov_Disp, Alltrim(this.w_MFLORDI+this.w_MFLIMPE) As Mov_Cont,this.w_MFLRISE As Mov_Rise ; 
 From GeneApp Where t_MVTIPRIG="R" Group By KEYSAL,CODMAG into cursor DISPCONTROL
      endif
      if this.w_MF2CASC="-" Or this.w_MF2RISE="+"
        * --- Creazione del cursore da passare al controllo disponibilit� per magazzino collegato
        *     Solo diminuzione esistenza o aumento di riservato
         
 Select ROWDISP as Riga, Sum(t_MVQTAUM1*IIF( this.w_MF2CASC="-",-1, IIF( this.w_MF2CASC="+",1, 0)) - t_MVQTAUM1*IIF( this.w_MF2RISE="-",-1, IIF( this.w_MF2RISE="+",1, 0))) As Disp , ; 
 Sum(t_MVQTAUM1*IIF( this.w_MF2CASC="-",-1, IIF( this.w_MF2CASC="+",1, 0)) - t_MVQTAUM1*IIF( this.w_MF2RISE="-",-1, IIF( this.w_MF2RISE="+",1, 0)) +t_MVQTAUM1*IIF( this.w_MF2ORDI="-",-1, IIF( this.w_MF2ORDI="+",1, 0)) - t_MVQTAUM1*IIF( this.w_MF2IMPE="-",-1, IIF( this.w_MF2IMPE="+",1, 0))) As DispC , ; 
 Sum(t_MVQTAUM1*IIF( this.w_MF2RISE="-",-1, IIF( this.w_MF2RISE="+",1, 0))) As DispR , ; 
 t_MVCODMAT As CodMag, t_MVCODART As KEYSAL,Alltrim(this.w_MF2CASC+this.w_MF2RISE) As Mov_Disp, Alltrim(this.w_MF2ORDI+this.w_MF2IMPE) As Mov_Cont,this.w_MF2RISE As Mov_Rise ; 
 From GeneApp Where t_MVTIPRIG="R" Group By KEYSAL,CODMAG into cursor DISPCONTROL1
      endif
      if Used("DISPCONTROL") And Used("DISPCONTROL1")
        * --- Creo un cursore unico
        Select * From DISPCONTROL union all Select * From DISPCONTROL1 Order By KEYSAL, CODMAG Into cursor Trs_Add
      else
        if Used("DISPCONTROL")
          Select * From DISPCONTROL Order By KEYSAL, CODMAG Into cursor Trs_Add
        endif
        if Used("DISPCONTROL1")
          Select * From DISPCONTROL1 Order By KEYSAL, CODMAG Into cursor Trs_Add
        endif
      endif
      USE IN SELECT("DISPCONTROL")
      USE IN SELECT("DISPCONTROL1")
      this.w_OK = .T.
      if g_PERDIS="S" And (this.w_MFLCASC="-" Or this.w_MFLRISE="+" Or this.w_MF2CASC="-" Or this.w_MF2RISE="+")
        * --- Lancio il check disponibilit� articoli
        this.w_OK = GSAR_BDA( This , "S", This )
      endif
      if this.w_OK
        * --- Try
        local bErr_044472F0
        bErr_044472F0=bTrsErr
        this.Try_044472F0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          if lower(this.w_PADRE.class)<>"tgsar_bgd"
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
          endif
          this.w_OK = .F.
        endif
        bTrsErr=bTrsErr or bErr_044472F0
        * --- End
      endif
    endif
    * --- Azzera il Temporaneo di Appoggio
    CREATE CURSOR GeneApp ;
    (t_MVTIPRIG C(1), t_MVCODICE C(20), t_MVCODART C(20), ;
    t_MVDESART C(40), t_MVDESSUP M(10), t_MVUNIMIS C(3), t_MVCATCON C(5), ;
    t_MVCODCLA C(3), t_MVCONTRA C(15), t_MVCODLIS C(5), ;
    t_MVQTAMOV N(12,3), t_MVQTAUM1 N(12,3), t_MVPREZZO N(18,5), ;
    t_MVSCONT1 N(6,2), t_MVSCONT2 N(6,2), t_MVSCONT3 N(6,2), t_MVSCONT4 N(6,2), ;
    t_MVFLOMAG C(1), t_MVCODIVA C(5), t_PERIVA N(5,2), t_BOLIVA C(1), t_MVVALRIG N(18,4), ;
    t_MVPESNET N(9,3), t_MVNOMENC C(8), t_MVUMSUPP C(3), t_MVMOLSUP N(8,3), ;
    t_MVIMPACC N(18,4), t_MVIMPSCO N(18,4), t_MVVALMAG N(18,4), t_MVIMPNAZ N(18,4), ;
    t_MVCODMAG C(5), t_MVCODMAT C(5), t_MVTIPATT C(1), t_MVDATEVA D(8), t_FLSERA C(1), ;
    t_MVSERRIF C(10), t_MVROWRIF N(4,0), t_EVQTASAL N(12,3), t_EVCODMAG C(5), t_EVKEYSAL C(20), ;
    t_EVFLORDI C(1), t_EVFLIMPE C(1), t_EVFLRISE C(1), t_MVFLERIF C(1), t_FLLOTT C(1), t_FLUBIC C(1), t_MVFLCOCO C(1), t_MVFLORCO C(1), t_MVCODCOS C(5), ;
    t_F2UBIC C(1), t_MVIMPCOM N(18,4), t_MVCODCOM C(15), t_MVIMPAC2 N(18,4), t_MVCODCEN C(15), t_MVVOCCEN C(15),t_MVCODATT C(15), ROWDISP N(4), t_MVFLTRAS C(1))
    * --- Inizializza i Dati i testata
    SELECT APPOEVA
    * --- Inizializza i dati di Testata del Nuovo Documento
    this.w_MVCODIVE = SPACE(5)
    this.w_MVCODPAG = SPACE(5)
    this.w_MVCODBAN = SPACE(10)
    this.w_MVSCOCL1 = 0
    this.w_MVSCOCL2 = 0
    this.w_MVSCOPAG = 0
    this.w_MVFLSCOR = "N"
    * --- Reinizializza le Variabili di Lavoro
    this.w_MVSPEINC = 0
    this.w_MVSPEIMB = 0
    this.w_MVSPETRA = 0
    this.w_MVSPEBOL = 0
    this.w_MVIMPARR = 0
    this.w_MVACCPRE = 0
    this.w_MVIVAINC = g_COIINC
    this.w_MVIVAIMB = g_COIIMB
    this.w_MVIVABOL = g_COIBOL
    this.w_MVCODVET = SPACE(5)
    this.w_MVCODSPE = SPACE(3)
    this.w_MVCODDES = SPACE(5)
    this.w_MVCODPOR = " "
    this.w_GIORN1 = 0
    this.w_GIORN2 = 0
    this.w_MESE1 = 0
    this.w_MESE2 = 0
    this.w_GIOFIS = 0
    this.w_CLBOLFAT = SPACE(1)
    this.w_CODNAZ = g_CODNAZ
    this.w_BOLINC = " "
    this.w_BOLIMB = " "
    this.w_BOLTRA = " "
    this.w_BOLBOL = " "
    this.w_BOLIVE = " "
    this.w_PEIINC = 0
    this.w_PEIIMB = 0
    this.w_PEITRA = 0
    this.w_PERIVE = 0
    this.w_TOTMERCE = 0
    this.w_MVACIVA1 = SPACE(5)
    this.w_MVAIMPS1 = 0
    this.w_TOTALE = 0
    this.w_MVTCONTR = SPACE(15)
    this.w_MVACIVA2 = SPACE(5)
    this.w_MVAIMPS2 = 0
    this.w_MVACIVA3 = SPACE(5)
    this.w_MVAIMPS3 = 0
    this.w_MVACIVA4 = SPACE(5)
    this.w_MVAIMPS4 = 0
    this.w_MVFLRINC = " "
    this.w_MVACIVA5 = SPACE(5)
    this.w_MVAIMPS5 = 0
    this.w_MVFLRIMB = " "
    this.w_MVACIVA6 = SPACE(5)
    this.w_MVAIMPS6 = 0
    this.w_MVACCONT = 0
    this.w_MVFLRTRA = " "
    this.w_MVAFLOM1 = SPACE(1)
    this.w_MVAIMPN1 = 0
    this.w_TOTIMPON = 0
    this.w_MVAFLOM2 = SPACE(1)
    this.w_MVAIMPN2 = 0
    this.w_TOTIMPOS = 0
    this.w_MVAFLOM3 = SPACE(1)
    this.w_MVAIMPN3 = 0
    this.w_TOTFATTU = 0
    this.w_MVAFLOM4 = SPACE(1)
    this.w_MVAIMPN4 = 0
    this.w_MVAFLOM5 = SPACE(1)
    this.w_MVAIMPN5 = 0
    this.w_MVAFLOM6 = SPACE(1)
    this.w_MVAIMPN6 = 0
    this.w_MVTOTRIT = 0
    this.w_MVTOTENA = 0
  endproc
  proc Try_044472F0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    if lower(this.w_PADRE.class)<>"tgsar_bgd"
      * --- begin transaction
      cp_BeginTrs()
    endif
    i_Conn=i_TableProp[this.DOC_MAST_IDX, 3]
    cp_NextTableProg(this, i_Conn, "SEDOC", "i_codazi,w_MVSERIAL")
    this.w_MVPRD = iif(this.w_FLPDOC="S" AND this.w_MVFLVEAC="A",iif(this.w_MVCLADOC="DT","DV",iif(this.w_MVCLADOC="DI","IV",this.w_PRODOC)),this.w_PRODOC)
    if this.w_MVFLVEAC="V" OR (this.w_MVFLVEAC="A" AND (this.w_MVPRD="DV" OR this.w_MVPRD="IV"))
      cp_NextTableProg(this, i_Conn, "PRDOC", "i_codazi,w_MVANNDOC,w_MVPRD,w_MVALFDOC,w_MVNUMDOC")
    else
      this.w_MVNUMDOC = this.w_MVNUMEST
      this.w_MVALFDOC = this.w_MVALFEST
    endif
    this.w_MVNUMREG = this.w_MVNUMDOC
    * --- Scrive la Testata
    * --- Insert into DOC_MAST
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"MVALFDOC"+",MVANNDOC"+",MVCAUCON"+",MVCLADOC"+",MVCODAGE"+",MVCODCON"+",MVCODDES"+",MVCODESE"+",MVCODPOR"+",MVCODSPE"+",MVCODUTE"+",MVCODVET"+",MVCONCON"+",MVDATCIV"+",MVDATDOC"+",MVDATREG"+",MVFLACCO"+",MVFLCONT"+",MVFLGIOM"+",MVFLINTE"+",MVFLPROV"+",MVFLSALD"+",MVFLSCOR"+",MVFLVEAC"+",MVEMERIC"+",MVGENEFF"+",MVGENPRO"+",MVNUMDOC"+",MVNUMREG"+",MVPRD"+",MVRIFDIC"+",MVRIFESP"+",MVSERIAL"+",MVTCAMAG"+",MVTCOLIS"+",MVTCONTR"+",MVTFRAGG"+",MVTIPCON"+",MVTIPDOC"+",MVTIPORN"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVALFDOC),'DOC_MAST','MVALFDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVANNDOC),'DOC_MAST','MVANNDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCAUCON),'DOC_MAST','MVCAUCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCLADOC),'DOC_MAST','MVCLADOC');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_MAST','MVCODAGE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCON),'DOC_MAST','MVCODCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODDES),'DOC_MAST','MVCODDES');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODESE),'DOC_MAST','MVCODESE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODPOR),'DOC_MAST','MVCODPOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODSPE),'DOC_MAST','MVCODSPE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODUTE),'DOC_MAST','MVCODUTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODVET),'DOC_MAST','MVCODVET');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVCONCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATCIV),'DOC_MAST','MVDATCIV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATDOC),'DOC_MAST','MVDATDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDATREG),'DOC_MAST','MVDATREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLACCO),'DOC_MAST','MVFLACCO');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLCONT');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLGIOM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLINTE),'DOC_MAST','MVFLINTE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLPROV),'DOC_MAST','MVFLPROV');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLSALD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOR),'DOC_MAST','MVFLSCOR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLVEAC),'DOC_MAST','MVFLVEAC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVEMERIC),'DOC_MAST','MVEMERIC');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVGENEFF');
      +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVGENPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMDOC),'DOC_MAST','MVNUMDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMREG),'DOC_MAST','MVNUMREG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVPRD),'DOC_MAST','MVPRD');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_MAST','MVRIFDIC');
      +","+cp_NullLink(cp_ToStrODBC(Space(10)),'DOC_MAST','MVRIFESP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_MAST','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCAMAG),'DOC_MAST','MVTCAMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCOLIS),'DOC_MAST','MVTCOLIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCONTR),'DOC_MAST','MVTCONTR');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTFRAGG),'DOC_MAST','MVTFRAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPCON),'DOC_MAST','MVTIPCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPDOC),'DOC_MAST','MVTIPDOC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPCON),'DOC_MAST','MVTIPORN');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'MVALFDOC',this.w_MVALFDOC,'MVANNDOC',this.w_MVANNDOC,'MVCAUCON',this.w_MVCAUCON,'MVCLADOC',this.w_MVCLADOC,'MVCODAGE',SPACE(5),'MVCODCON',this.w_MVCODCON,'MVCODDES',this.w_MVCODDES,'MVCODESE',this.w_MVCODESE,'MVCODPOR',this.w_MVCODPOR,'MVCODSPE',this.w_MVCODSPE,'MVCODUTE',this.w_MVCODUTE,'MVCODVET',this.w_MVCODVET)
      insert into (i_cTable) (MVALFDOC,MVANNDOC,MVCAUCON,MVCLADOC,MVCODAGE,MVCODCON,MVCODDES,MVCODESE,MVCODPOR,MVCODSPE,MVCODUTE,MVCODVET,MVCONCON,MVDATCIV,MVDATDOC,MVDATREG,MVFLACCO,MVFLCONT,MVFLGIOM,MVFLINTE,MVFLPROV,MVFLSALD,MVFLSCOR,MVFLVEAC,MVEMERIC,MVGENEFF,MVGENPRO,MVNUMDOC,MVNUMREG,MVPRD,MVRIFDIC,MVRIFESP,MVSERIAL,MVTCAMAG,MVTCOLIS,MVTCONTR,MVTFRAGG,MVTIPCON,MVTIPDOC,MVTIPORN &i_ccchkf. );
         values (;
           this.w_MVALFDOC;
           ,this.w_MVANNDOC;
           ,this.w_MVCAUCON;
           ,this.w_MVCLADOC;
           ,SPACE(5);
           ,this.w_MVCODCON;
           ,this.w_MVCODDES;
           ,this.w_MVCODESE;
           ,this.w_MVCODPOR;
           ,this.w_MVCODSPE;
           ,this.w_MVCODUTE;
           ,this.w_MVCODVET;
           ," ";
           ,this.w_MVDATCIV;
           ,this.w_MVDATDOC;
           ,this.w_MVDATREG;
           ,this.w_MVFLACCO;
           ," ";
           ," ";
           ,this.w_MVFLINTE;
           ,this.w_MVFLPROV;
           ," ";
           ,this.w_MVFLSCOR;
           ,this.w_MVFLVEAC;
           ,this.w_MVEMERIC;
           ," ";
           ," ";
           ,this.w_MVNUMDOC;
           ,this.w_MVNUMREG;
           ,this.w_MVPRD;
           ,SPACE(10);
           ,Space(10);
           ,this.w_MVSERIAL;
           ,this.w_MVTCAMAG;
           ,this.w_MVTCOLIS;
           ,this.w_MVTCONTR;
           ,this.w_MVTFRAGG;
           ,this.w_MVTIPCON;
           ,this.w_MVTIPDOC;
           ,this.w_MVTIPCON;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Occorrono 3 frasi per problema lunghezza stringa INSERT
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVCODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODPAG),'DOC_MAST','MVCODPAG');
      +",MVCODIVE ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVE),'DOC_MAST','MVCODIVE');
      +",MVSCONTI ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCONTI),'DOC_MAST','MVSCONTI');
      +",MVSPEINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEINC),'DOC_MAST','MVSPEINC');
      +",MVIVAINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAINC),'DOC_MAST','MVIVAINC');
      +",MVFLRINC ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRINC),'DOC_MAST','MVFLRINC');
      +",MVSPETRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPETRA),'DOC_MAST','MVSPETRA');
      +",MVIVATRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVATRA),'DOC_MAST','MVIVATRA');
      +",MVFLRTRA ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRTRA),'DOC_MAST','MVFLRTRA');
      +",MVSPEIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEIMB),'DOC_MAST','MVSPEIMB');
      +",MVIVAIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVAIMB),'DOC_MAST','MVIVAIMB');
      +",MVFLRIMB ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLRIMB),'DOC_MAST','MVFLRIMB');
      +",MVSPEBOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVSPEBOL),'DOC_MAST','MVSPEBOL');
      +",MVDATDIV ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATDIV),'DOC_MAST','MVDATDIV');
      +",MVCODBAN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODBAN),'DOC_MAST','MVCODBAN');
      +",MVVALNAZ ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALNAZ),'DOC_MAST','MVVALNAZ');
      +",MVCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODVAL),'DOC_MAST','MVCODVAL');
      +",MVCAOVAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAOVAL),'DOC_MAST','MVCAOVAL');
      +",MVSCOCL1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOCL1),'DOC_MAST','MVSCOCL1');
      +",MVSCOCL2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOCL2),'DOC_MAST','MVSCOCL2');
      +",MVSCOPAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVSCOPAG),'DOC_MAST','MVSCOPAG');
      +",MVALFEST ="+cp_NullLink(cp_ToStrODBC(this.w_MVALFEST),'DOC_MAST','MVALFEST');
      +",MVPRP ="+cp_NullLink(cp_ToStrODBC(this.w_MVPRP),'DOC_MAST','MVPRP');
      +",MVANNPRO ="+cp_NullLink(cp_ToStrODBC(this.w_MVANNPRO),'DOC_MAST','MVANNPRO');
      +",MVNUMEST ="+cp_NullLink(cp_ToStrODBC(this.w_MVNUMEST),'DOC_MAST','MVNUMEST');
      +",MVFLFOSC ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVFLFOSC');
      +",MVNOTAGG ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_MAST','MVNOTAGG');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
             )
    else
      update (i_cTable) set;
          MVCODPAG = this.w_MVCODPAG;
          ,MVCODIVE = this.w_MVCODIVE;
          ,MVSCONTI = this.w_MVSCONTI;
          ,MVSPEINC = this.w_MVSPEINC;
          ,MVIVAINC = this.w_MVIVAINC;
          ,MVFLRINC = this.w_MVFLRINC;
          ,MVSPETRA = this.w_MVSPETRA;
          ,MVIVATRA = this.w_MVIVATRA;
          ,MVFLRTRA = this.w_MVFLRTRA;
          ,MVSPEIMB = this.w_MVSPEIMB;
          ,MVIVAIMB = this.w_MVIVAIMB;
          ,MVFLRIMB = this.w_MVFLRIMB;
          ,MVSPEBOL = this.w_MVSPEBOL;
          ,MVDATDIV = this.w_MVDATDIV;
          ,MVCODBAN = this.w_MVCODBAN;
          ,MVVALNAZ = this.w_MVVALNAZ;
          ,MVCODVAL = this.w_MVCODVAL;
          ,MVCAOVAL = this.w_MVCAOVAL;
          ,MVSCOCL1 = this.w_MVSCOCL1;
          ,MVSCOCL2 = this.w_MVSCOCL2;
          ,MVSCOPAG = this.w_MVSCOPAG;
          ,MVALFEST = this.w_MVALFEST;
          ,MVPRP = this.w_MVPRP;
          ,MVANNPRO = this.w_MVANNPRO;
          ,MVNUMEST = this.w_MVNUMEST;
          ,MVFLFOSC = " ";
          ,MVNOTAGG = " ";
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_MVSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Write into DOC_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MVIMPARR ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPARR),'DOC_MAST','MVIMPARR');
      +",MVIVABOL ="+cp_NullLink(cp_ToStrODBC(this.w_MVIVABOL),'DOC_MAST','MVIVABOL');
      +",MVACCONT ="+cp_NullLink(cp_ToStrODBC(this.w_MVACCONT),'DOC_MAST','MVACCONT');
      +",UTCC ="+cp_NullLink(cp_ToStrODBC(i_CODUTE),'DOC_MAST','UTCC');
      +",UTCV ="+cp_NullLink(cp_ToStrODBC(0),'DOC_MAST','UTCV');
      +",UTDC ="+cp_NullLink(cp_ToStrODBC(SetInfoDate( g_CALUTD )),'DOC_MAST','UTDC');
      +",UTDV ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -    ")),'DOC_MAST','UTDV');
      +",MVACIVA1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA1),'DOC_MAST','MVACIVA1');
      +",MVACIVA2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA2),'DOC_MAST','MVACIVA2');
      +",MVACIVA3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA3),'DOC_MAST','MVACIVA3');
      +",MVACIVA4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA4),'DOC_MAST','MVACIVA4');
      +",MVACIVA5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA5),'DOC_MAST','MVACIVA5');
      +",MVACIVA6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVACIVA6),'DOC_MAST','MVACIVA6');
      +",MVAIMPN1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN1),'DOC_MAST','MVAIMPN1');
      +",MVAIMPN2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN2),'DOC_MAST','MVAIMPN2');
      +",MVAIMPN3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN3),'DOC_MAST','MVAIMPN3');
      +",MVAIMPN4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN4),'DOC_MAST','MVAIMPN4');
      +",MVAIMPN5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN5),'DOC_MAST','MVAIMPN5');
      +",MVAIMPN6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPN6),'DOC_MAST','MVAIMPN6');
      +",MVAIMPS1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS1),'DOC_MAST','MVAIMPS1');
      +",MVAIMPS2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS2),'DOC_MAST','MVAIMPS2');
      +",MVAIMPS3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS3),'DOC_MAST','MVAIMPS3');
      +",MVAIMPS4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS4),'DOC_MAST','MVAIMPS4');
      +",MVAIMPS5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS5),'DOC_MAST','MVAIMPS5');
      +",MVAIMPS6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAIMPS6),'DOC_MAST','MVAIMPS6');
      +",MVAFLOM1 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM1),'DOC_MAST','MVAFLOM1');
      +",MVAFLOM2 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM2),'DOC_MAST','MVAFLOM2');
      +",MVAFLOM3 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM3),'DOC_MAST','MVAFLOM3');
      +",MVAFLOM4 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM4),'DOC_MAST','MVAFLOM4');
      +",MVAFLOM5 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM5),'DOC_MAST','MVAFLOM5');
      +",MVAFLOM6 ="+cp_NullLink(cp_ToStrODBC(this.w_MVAFLOM6),'DOC_MAST','MVAFLOM6');
      +",MVACCPRE ="+cp_NullLink(cp_ToStrODBC(this.w_MVACCPRE),'DOC_MAST','MVACCPRE');
      +",MVFLSCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLSCOM),'DOC_MAST','MVFLSCOM');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
             )
    else
      update (i_cTable) set;
          MVIMPARR = this.w_MVIMPARR;
          ,MVIVABOL = this.w_MVIVABOL;
          ,MVACCONT = this.w_MVACCONT;
          ,UTCC = i_CODUTE;
          ,UTCV = 0;
          ,UTDC = SetInfoDate( g_CALUTD );
          ,UTDV = cp_CharToDate("  -  -    ");
          ,MVACIVA1 = this.w_MVACIVA1;
          ,MVACIVA2 = this.w_MVACIVA2;
          ,MVACIVA3 = this.w_MVACIVA3;
          ,MVACIVA4 = this.w_MVACIVA4;
          ,MVACIVA5 = this.w_MVACIVA5;
          ,MVACIVA6 = this.w_MVACIVA6;
          ,MVAIMPN1 = this.w_MVAIMPN1;
          ,MVAIMPN2 = this.w_MVAIMPN2;
          ,MVAIMPN3 = this.w_MVAIMPN3;
          ,MVAIMPN4 = this.w_MVAIMPN4;
          ,MVAIMPN5 = this.w_MVAIMPN5;
          ,MVAIMPN6 = this.w_MVAIMPN6;
          ,MVAIMPS1 = this.w_MVAIMPS1;
          ,MVAIMPS2 = this.w_MVAIMPS2;
          ,MVAIMPS3 = this.w_MVAIMPS3;
          ,MVAIMPS4 = this.w_MVAIMPS4;
          ,MVAIMPS5 = this.w_MVAIMPS5;
          ,MVAIMPS6 = this.w_MVAIMPS6;
          ,MVAFLOM1 = this.w_MVAFLOM1;
          ,MVAFLOM2 = this.w_MVAFLOM2;
          ,MVAFLOM3 = this.w_MVAFLOM3;
          ,MVAFLOM4 = this.w_MVAFLOM4;
          ,MVAFLOM5 = this.w_MVAFLOM5;
          ,MVAFLOM6 = this.w_MVAFLOM6;
          ,MVACCPRE = this.w_MVACCPRE;
          ,MVFLSCOM = this.w_MVFLSCOM;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_MVSERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Cicla sulle Righe Documento
    ah_Msg("Generazione documento: %1",.T.,.F.,.F., ALLTRIM(STR(this.w_MVNUMDOC,15)) )
    SELECT GeneApp
    GO TOP
    SCAN FOR t_MVTIPRIG<>" " AND NOT EMPTY(t_MVCODICE)
    * --- Scrive il Dettaglio
    this.w_MVTIPRIG = t_MVTIPRIG
    this.w_MVCODICE = t_MVCODICE
    this.w_MVCODART = t_MVCODART
    this.w_MVDESART = t_MVDESART
    this.w_MVDESSUP = t_MVDESSUP
    this.w_MVUNIMIS = t_MVUNIMIS
    this.w_MVCATCON = t_MVCATCON
    this.w_MVCODCLA = t_MVCODCLA
    this.w_MVCONTRA = t_MVCONTRA
    this.w_MVCODLIS = t_MVCODLIS
    this.w_MVQTAMOV = t_MVQTAMOV
    this.w_MVQTAUM1 = t_MVQTAUM1
    this.w_MVPREZZO = t_MVPREZZO
    this.w_MVIMPACC = t_MVIMPACC
    this.w_MVIMPSCO = t_MVIMPSCO
    this.w_MVVALMAG = t_MVVALMAG
    this.w_MVIMPNAZ = t_MVIMPNAZ
    this.w_MVCODCOM = t_MVCODCOM
    this.w_MVIMPCOM = t_MVIMPCOM
    this.w_MVVOCCEN = t_MVVOCCEN
    this.w_MVCODCEN = t_MVCODCEN
    this.w_MVSCONT1 = t_MVSCONT1
    this.w_MVSCONT2 = t_MVSCONT2
    this.w_MVSCONT3 = t_MVSCONT3
    this.w_MVSCONT4 = t_MVSCONT4
    this.w_MVFLOMAG = t_MVFLOMAG
    this.w_MVCODIVA = t_MVCODIVA
    this.w_MVVALRIG = t_MVVALRIG
    this.w_MVPESNET = t_MVPESNET
    this.w_MVNOMENC = t_MVNOMENC
    this.w_MVFLTRAS = t_MVFLTRAS
    this.w_MVUMSUPP = t_MVUMSUPP
    this.w_MVMOLSUP = t_MVMOLSUP
    this.w_MVCODMAG = t_MVCODMAG
    this.w_MVCODMAT = t_MVCODMAT
    this.w_MVTIPATT = t_MVTIPATT
    this.w_MVCODATT = t_MVCODATT
    this.w_MVDATEVA = t_MVDATEVA
    this.w_MVSERRIF = t_MVSERRIF
    this.w_MVROWRIF = t_MVROWRIF
    this.w_EVQTASAL = t_EVQTASAL
    this.w_EVKEYSAL = t_EVKEYSAL
    this.w_EVCODMAG = t_EVCODMAG
    this.w_EVFLORDI = t_EVFLORDI
    this.w_EVFLIMPE = t_EVFLIMPE
    this.w_EVFLRISE = t_EVFLRISE
    this.w_FLLOTT = t_FLLOTT
    this.w_FLUBIC = t_FLUBIC
    this.w_F2UBIC = t_F2UBIC
    this.w_MVCAUMAG = this.w_MVTCAMAG
    this.w_MVFLORDI = this.w_MFLORDI
    this.w_MVFLIMPE = this.w_MFLIMPE
    this.w_MVFLELGM = this.w_MFLELGM
    this.w_MVKEYSAL = IIF(this.w_MVTIPRIG="R" AND (NOT EMPTY(this.w_AGGSAL) OR NOT EMPTY(this.w_AGGSAL1) ), this.w_MVCODART, SPACE(20))
    this.w_MVCODMAG = IIF(EMPTY(this.w_MVKEYSAL) OR EMPTY(this.w_AGGSAL), SPACE(5), this.w_MVCODMAG)
    this.w_MVCODMAT = IIF(EMPTY(this.w_MVKEYSAL) OR EMPTY(this.w_AGGSAL1), SPACE(5), this.w_MVCODMAT)
    this.w_MVFLLOTT = " "
    this.w_MVF2LOTT = " "
    if ((g_PERLOT="S" AND this.w_FLLOTT$ "SC") OR (g_PERUBI="S" AND this.w_FLUBIC="S")) AND NOT EMPTY(this.w_MVCODMAG)
      this.w_MVFLLOTT = LEFT(ALLTRIM(this.w_MFLCASC)+IIF(this.w_MFLRISE="+", "-", IIF(this.w_MFLRISE="-", "+", " ")), 1)
      * --- Deve essere una causale di magazzino che movimenta esistenza o riservato
    endif
    if ((g_PERLOT="S" AND this.w_FLLOTT$ "SC") OR (g_PERUBI="S" AND this.w_F2UBIC="S")) AND NOT EMPTY(this.w_MVCODMAT)
      this.w_MVF2LOTT = LEFT(ALLTRIM(this.w_MF2CASC)+IIF(this.w_MF2RISE="+", "-", IIF(this.w_MF2RISE="-", "+", " ")), 1)
      * --- Deve essere una causale di magazzino che movimenta esistenza o riservato
    endif
    this.w_MVFLARIF = " "
    this.w_MVFLERIF = " "
    this.w_QTAIMP = 0
    this.w_QTAIM1 = 0
    if NOT EMPTY(this.w_MVSERRIF) AND this.w_MVROWRIF<>0
      this.w_MVFLARIF = "+"
      this.w_MVFLERIF = IIF(this.w_EVQTASAL > this.w_MVQTAUM1, NVL(t_MVFLERIF, " "), "S")
      * --- Read from DOC_DETT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "MVQTAEVA,MVQTAMOV,MVQTAUM1,MVQTAEV1,MVFLEVAS"+;
          " from "+i_cTable+" DOC_DETT where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVROWRIF);
              +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          MVQTAEVA,MVQTAMOV,MVQTAUM1,MVQTAEV1,MVFLEVAS;
          from (i_cTable) where;
              MVSERIAL = this.w_MVSERRIF;
              and CPROWNUM = this.w_MVROWRIF;
              and MVNUMRIF = this.w_MVNUMRIF;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ORQTAEVA = NVL(cp_ToDate(_read_.MVQTAEVA),cp_NullValue(_read_.MVQTAEVA))
        this.w_ORQTAMOV = NVL(cp_ToDate(_read_.MVQTAMOV),cp_NullValue(_read_.MVQTAMOV))
        this.w_ORQTAUM1 = NVL(cp_ToDate(_read_.MVQTAUM1),cp_NullValue(_read_.MVQTAUM1))
        this.w_ORQTAEV1 = NVL(cp_ToDate(_read_.MVQTAEV1),cp_NullValue(_read_.MVQTAEV1))
        this.w_ORFLEVAS = NVL(cp_ToDate(_read_.MVFLEVAS),cp_NullValue(_read_.MVFLEVAS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Per determinare la parte impegno evasione calcolo il minimo tra la qta del
      *     documento creato e la parte da evadere sul documento di
      *     origine
      this.w_QTAIMP = Min ( this.w_MVQTAMOV , this.w_ORQTAMOV- this.w_ORQTAEVA )
      this.w_QTAIM1 = Min ( this.w_MVQTAUM1 , this.w_ORQTAUM1- this.w_ORQTAEV1 )
    endif
    if this.w_MFLAVAL $ "A-V"
      * --- Read from SALDIART
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SLDATUCA,SLDATUPV"+;
          " from "+i_cTable+" SALDIART where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_MVCODART);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SLDATUCA,SLDATUPV;
          from (i_cTable) where;
              SLCODICE = this.w_MVCODART;
              and SLCODMAG = this.w_MVCODMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        w_ULTCAR = NVL(cp_ToDate(_read_.SLDATUCA),cp_NullValue(_read_.SLDATUCA))
        w_ULTSCA = NVL(cp_ToDate(_read_.SLDATUPV),cp_NullValue(_read_.SLDATUPV))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_MVVALULT = IIF(this.w_MVQTAUM1=0, 0, cp_ROUND(this.w_MVIMPNAZ/this.w_MVQTAUM1, g_PERPUL))
    this.w_MVFLULCA = IIF(this.w_MFLAVAL="A" AND this.w_MFLCASC="+" AND this.w_MVDATDOC>=w_ULTCAR AND this.w_MVFLOMAG<>"S", "=", " ")
    this.w_MVFLULPV = IIF(this.w_MFLAVAL="V" AND this.w_MFLCASC="-" AND this.w_MVDATDOC>=w_ULTSCA AND this.w_MVFLOMAG<>"S" AND (this.w_MVCLADOC<>"DT" OR this.w_MVPREZZO<>0), "=", " ")
    * --- Se quantit� impegnata � 0 o la riga � evasa, allora il documento � gia stato completamente
    *     evaso,  lego i due documenti esplosi ma non storno l'evasione / saldi
    if this.w_QTAIMP=0 Or this.w_ORFLEVAS="S"
      this.w_MVFLARIF = " "
      this.w_MVFLERIF = " "
    endif
    this.w_CPROWNUM = this.w_CPROWNUM + 1
    this.w_CPROWORD = this.w_CPROWORD + 10
    * --- Insert into DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CPROWNUM"+",CPROWORD"+",MVCATCON"+",MVCAUMAG"+",MVCODART"+",MVCODCLA"+",MVCODICE"+",MVCODIVA"+",MVCODLIS"+",MVCONIND"+",MVCONTRA"+",MVCONTRO"+",MVDESART"+",MVDESSUP"+",MVFLOMAG"+",MVFLRAGG"+",MVFLTRAS"+",MVIMPACC"+",MVIMPNAZ"+",MVIMPPRO"+",MVIMPSCO"+",MVMOLSUP"+",MVNOMENC"+",MVNUMCOL"+",MVNUMRIF"+",MVPERPRO"+",MVPESNET"+",MVPREZZO"+",MVQTAMOV"+",MVQTASAL"+",MVQTAUM1"+",MVROWRIF"+",MVSCONT1"+",MVSCONT2"+",MVSCONT3"+",MVSCONT4"+",MVSERIAL"+",MVSERRIF"+",MVTIPRIG"+",MVUNIMIS"+",MVVALMAG"+",MVVALRIG"+",MVCODMAG"+",MVCODMAT"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'DOC_DETT','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'DOC_DETT','CPROWORD');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCATCON),'DOC_DETT','MVCATCON');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'DOC_DETT','MVCODART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCLA),'DOC_DETT','MVCODCLA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODICE),'DOC_DETT','MVCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODIVA),'DOC_DETT','MVCODIVA');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODLIS),'DOC_DETT','MVCODLIS');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCONIND');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCONTRA),'DOC_DETT','MVCONTRA');
      +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'DOC_DETT','MVCONTRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESART),'DOC_DETT','MVDESART');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVDESSUP),'DOC_DETT','MVDESSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLOMAG),'DOC_DETT','MVFLOMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTFRAGG),'DOC_DETT','MVFLRAGG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVFLTRAS),'DOC_DETT','MVFLTRAS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPACC),'DOC_DETT','MVIMPACC');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPNAZ),'DOC_DETT','MVIMPNAZ');
      +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVIMPSCO),'DOC_DETT','MVIMPSCO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVMOLSUP),'DOC_DETT','MVMOLSUP');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNOMENC),'DOC_DETT','MVNOMENC');
      +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVNUMCOL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVNUMRIF),'DOC_DETT','MVNUMRIF');
      +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVPERPRO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVPESNET),'DOC_DETT','MVPESNET');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVPREZZO),'DOC_DETT','MVPREZZO');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAMOV),'DOC_DETT','MVQTAMOV');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTASAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVQTAUM1),'DOC_DETT','MVQTAUM1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVROWRIF),'DOC_DETT','MVROWRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT1),'DOC_DETT','MVSCONT1');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT2),'DOC_DETT','MVSCONT2');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT3),'DOC_DETT','MVSCONT3');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSCONT4),'DOC_DETT','MVSCONT4');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_DETT','MVSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVSERRIF),'DOC_DETT','MVSERRIF');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVTIPRIG),'DOC_DETT','MVTIPRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVUNIMIS),'DOC_DETT','MVUNIMIS');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALMAG),'DOC_DETT','MVVALMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVVALRIG),'DOC_DETT','MVVALRIG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'DOC_DETT','MVCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAT),'DOC_DETT','MVCODMAT');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'MVCATCON',this.w_MVCATCON,'MVCAUMAG',this.w_MVCAUMAG,'MVCODART',this.w_MVCODART,'MVCODCLA',this.w_MVCODCLA,'MVCODICE',this.w_MVCODICE,'MVCODIVA',this.w_MVCODIVA,'MVCODLIS',this.w_MVCODLIS,'MVCONIND',SPACE(5),'MVCONTRA',this.w_MVCONTRA,'MVCONTRO',SPACE(15))
      insert into (i_cTable) (CPROWNUM,CPROWORD,MVCATCON,MVCAUMAG,MVCODART,MVCODCLA,MVCODICE,MVCODIVA,MVCODLIS,MVCONIND,MVCONTRA,MVCONTRO,MVDESART,MVDESSUP,MVFLOMAG,MVFLRAGG,MVFLTRAS,MVIMPACC,MVIMPNAZ,MVIMPPRO,MVIMPSCO,MVMOLSUP,MVNOMENC,MVNUMCOL,MVNUMRIF,MVPERPRO,MVPESNET,MVPREZZO,MVQTAMOV,MVQTASAL,MVQTAUM1,MVROWRIF,MVSCONT1,MVSCONT2,MVSCONT3,MVSCONT4,MVSERIAL,MVSERRIF,MVTIPRIG,MVUNIMIS,MVVALMAG,MVVALRIG,MVCODMAG,MVCODMAT &i_ccchkf. );
         values (;
           this.w_CPROWNUM;
           ,this.w_CPROWORD;
           ,this.w_MVCATCON;
           ,this.w_MVCAUMAG;
           ,this.w_MVCODART;
           ,this.w_MVCODCLA;
           ,this.w_MVCODICE;
           ,this.w_MVCODIVA;
           ,this.w_MVCODLIS;
           ,SPACE(5);
           ,this.w_MVCONTRA;
           ,SPACE(15);
           ,this.w_MVDESART;
           ,this.w_MVDESSUP;
           ,this.w_MVFLOMAG;
           ,this.w_MVTFRAGG;
           ,this.w_MVFLTRAS;
           ,this.w_MVIMPACC;
           ,this.w_MVIMPNAZ;
           ,0;
           ,this.w_MVIMPSCO;
           ,this.w_MVMOLSUP;
           ,this.w_MVNOMENC;
           ,0;
           ,this.w_MVNUMRIF;
           ,0;
           ,this.w_MVPESNET;
           ,this.w_MVPREZZO;
           ,this.w_MVQTAMOV;
           ,this.w_MVQTAUM1;
           ,this.w_MVQTAUM1;
           ,this.w_MVROWRIF;
           ,this.w_MVSCONT1;
           ,this.w_MVSCONT2;
           ,this.w_MVSCONT3;
           ,this.w_MVSCONT4;
           ,this.w_MVSERIAL;
           ,this.w_MVSERRIF;
           ,this.w_MVTIPRIG;
           ,this.w_MVUNIMIS;
           ,this.w_MVVALMAG;
           ,this.w_MVVALRIG;
           ,this.w_MVCODMAG;
           ,this.w_MVCODMAT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- Write into DOC_DETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MV_SEGNO ="+cp_NullLink(cp_ToStrODBC(this.w_MV_SEGNO),'DOC_DETT','MV_SEGNO');
      +",MVCAUCOL ="+cp_NullLink(cp_ToStrODBC(this.w_MCAUCOL),'DOC_DETT','MVCAUCOL');
      +",MVCAUMAG ="+cp_NullLink(cp_ToStrODBC(this.w_MVCAUMAG),'DOC_DETT','MVCAUMAG');
      +",MVCODATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODATT),'DOC_DETT','MVCODATT');
      +",MVCODCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCEN),'DOC_DETT','MVCODCEN');
      +",MVCODCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'DOC_DETT','MVCODCOM');
      +",MVDATEVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATEVA),'DOC_DETT','MVDATEVA');
      +",MVF2CASC ="+cp_NullLink(cp_ToStrODBC(this.w_MF2CASC),'DOC_DETT','MVF2CASC');
      +",MVF2IMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MF2IMPE),'DOC_DETT','MVF2IMPE');
      +",MVF2LOTT ="+cp_NullLink(cp_ToStrODBC(this.w_MVF2LOTT),'DOC_DETT','MVF2LOTT');
      +",MVF2ORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MF2ORDI),'DOC_DETT','MVF2ORDI');
      +",MVF2RISE ="+cp_NullLink(cp_ToStrODBC(this.w_MF2RISE),'DOC_DETT','MVF2RISE');
      +",MVFLARIF ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLARIF),'DOC_DETT','MVFLARIF');
      +",MVFLCASC ="+cp_NullLink(cp_ToStrODBC(this.w_MFLCASC),'DOC_DETT','MVFLCASC');
      +",MVFLELAN ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLELAN),'DOC_DETT','MVFLELAN');
      +",MVFLELGM ="+cp_NullLink(cp_ToStrODBC(this.w_MFLELGM),'DOC_DETT','MVFLELGM');
      +",MVFLERIF ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLERIF),'DOC_DETT','MVFLERIF');
      +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
      +",MVFLIMPE ="+cp_NullLink(cp_ToStrODBC(this.w_MFLIMPE),'DOC_DETT','MVFLIMPE');
      +",MVFLLOTT ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLLOTT),'DOC_DETT','MVFLLOTT');
      +",MVFLORDI ="+cp_NullLink(cp_ToStrODBC(this.w_MFLORDI),'DOC_DETT','MVFLORDI');
      +",MVFLRIPA ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLRIPA');
      +",MVFLRISE ="+cp_NullLink(cp_ToStrODBC(this.w_MFLRISE),'DOC_DETT','MVFLRISE');
      +",MVIMPCOM ="+cp_NullLink(cp_ToStrODBC(this.w_MVIMPCOM),'DOC_DETT','MVIMPCOM');
      +",MVIMPEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPEVA');
      +",MVKEYSAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'DOC_DETT','MVKEYSAL');
      +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEV1');
      +",MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAEVA');
      +",MVQTAIM1 ="+cp_NullLink(cp_ToStrODBC(this.w_QTAIM1),'DOC_DETT','MVQTAIM1');
      +",MVQTAIMP ="+cp_NullLink(cp_ToStrODBC(this.w_QTAIMP),'DOC_DETT','MVQTAIMP');
      +",MVTIPATT ="+cp_NullLink(cp_ToStrODBC(this.w_MVTIPATT),'DOC_DETT','MVTIPATT');
      +",MVUMSUPP ="+cp_NullLink(cp_ToStrODBC(this.w_MVUMSUPP),'DOC_DETT','MVUMSUPP');
      +",MVVOCCEN ="+cp_NullLink(cp_ToStrODBC(this.w_MVVOCCEN),'DOC_DETT','MVVOCCEN');
      +",MVFLCOCO ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLCOCO),'DOC_DETT','MVFLCOCO');
      +",MVFLORCO ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLORCO),'DOC_DETT','MVFLORCO');
      +",MVCODCOS ="+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOS),'DOC_DETT','MVCODCOS');
          +i_ccchkf ;
      +" where ";
          +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(this.w_CPROWNUM);
          +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
             )
    else
      update (i_cTable) set;
          MV_SEGNO = this.w_MV_SEGNO;
          ,MVCAUCOL = this.w_MCAUCOL;
          ,MVCAUMAG = this.w_MVCAUMAG;
          ,MVCODATT = this.w_MVCODATT;
          ,MVCODCEN = this.w_MVCODCEN;
          ,MVCODCOM = this.w_MVCODCOM;
          ,MVDATEVA = this.w_MVDATEVA;
          ,MVF2CASC = this.w_MF2CASC;
          ,MVF2IMPE = this.w_MF2IMPE;
          ,MVF2LOTT = this.w_MVF2LOTT;
          ,MVF2ORDI = this.w_MF2ORDI;
          ,MVF2RISE = this.w_MF2RISE;
          ,MVFLARIF = this.w_MVFLARIF;
          ,MVFLCASC = this.w_MFLCASC;
          ,MVFLELAN = this.w_MVFLELAN;
          ,MVFLELGM = this.w_MFLELGM;
          ,MVFLERIF = this.w_MVFLERIF;
          ,MVFLEVAS = " ";
          ,MVFLIMPE = this.w_MFLIMPE;
          ,MVFLLOTT = this.w_MVFLLOTT;
          ,MVFLORDI = this.w_MFLORDI;
          ,MVFLRIPA = " ";
          ,MVFLRISE = this.w_MFLRISE;
          ,MVIMPCOM = this.w_MVIMPCOM;
          ,MVIMPEVA = 0;
          ,MVKEYSAL = this.w_MVKEYSAL;
          ,MVQTAEV1 = 0;
          ,MVQTAEVA = 0;
          ,MVQTAIM1 = this.w_QTAIM1;
          ,MVQTAIMP = this.w_QTAIMP;
          ,MVTIPATT = this.w_MVTIPATT;
          ,MVUMSUPP = this.w_MVUMSUPP;
          ,MVVOCCEN = this.w_MVVOCCEN;
          ,MVFLCOCO = this.w_MVFLCOCO;
          ,MVFLORCO = this.w_MVFLORCO;
          ,MVCODCOS = this.w_MVCODCOS;
          &i_ccchkf. ;
       where;
          MVSERIAL = this.w_MVSERIAL;
          and CPROWNUM = this.w_CPROWNUM;
          and MVNUMRIF = this.w_MVNUMRIF;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    * --- Read from ART_ICOL
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.ART_ICOL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "ARSALCOM"+;
        " from "+i_cTable+" ART_ICOL where ";
            +"ARCODART = "+cp_ToStrODBC(this.w_MVCODART);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        ARSALCOM;
        from (i_cTable) where;
            ARCODART = this.w_MVCODART;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLCOM1 = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Aggiorna Saldi
    if this.w_MVQTAUM1<>0 AND NOT EMPTY(this.w_MVKEYSAL) AND NOT EMPTY(this.w_MVCODMAG)
      if NOT EMPTY(this.w_MFLCASC+this.w_MFLRISE+this.w_MFLORDI+this.w_MFLIMPE)
        * --- Try
        local bErr_044C8D70
        bErr_044C8D70=bTrsErr
        this.Try_044C8D70()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_044C8D70
        * --- End
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_MFLCASC,'SLQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_MFLRISE,'SLQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_MFLORDI,'SLQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_MFLIMPE,'SLQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp5=cp_SetTrsOp(this.w_MVFLULCA,'SLVALUCA','this.w_MVVALULT ',this.w_MVVALULT ,'update',i_nConn)
          i_cOp6=cp_SetTrsOp(this.w_MVFLULCA,'SLDATUCA','this.w_MVDATDOC',this.w_MVDATDOC,'update',i_nConn)
          i_cOp7=cp_SetTrsOp(this.w_MVFLULPV,'SLDATUPV','this.w_MVDATDOC',this.w_MVDATDOC,'update',i_nConn)
          i_cOp8=cp_SetTrsOp(this.w_MVFLULPV,'SLVALUPV','this.w_MVVALULT ',this.w_MVVALULT ,'update',i_nConn)
          i_cOp9=cp_SetTrsOp(this.w_MVFLULCA,'SLCODVAA','this.w_MVVALNAZ',this.w_MVVALNAZ,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
          +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
          +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
          +",SLVALUCA ="+cp_NullLink(i_cOp5,'SALDIART','SLVALUCA');
          +",SLDATUCA ="+cp_NullLink(i_cOp6,'SALDIART','SLDATUCA');
          +",SLDATUPV ="+cp_NullLink(i_cOp7,'SALDIART','SLDATUPV');
          +",SLVALUPV ="+cp_NullLink(i_cOp8,'SALDIART','SLVALUPV');
          +",SLCODVAA ="+cp_NullLink(i_cOp9,'SALDIART','SLCODVAA');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTAPER = &i_cOp1.;
              ,SLQTRPER = &i_cOp2.;
              ,SLQTOPER = &i_cOp3.;
              ,SLQTIPER = &i_cOp4.;
              ,SLVALUCA = &i_cOp5.;
              ,SLDATUCA = &i_cOp6.;
              ,SLDATUPV = &i_cOp7.;
              ,SLVALUPV = &i_cOp8.;
              ,SLCODVAA = &i_cOp9.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_MVKEYSAL;
              and SLCODMAG = this.w_MVCODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if NVL(this.w_FLCOM1,"N")="S" 
          * --- Try
          local bErr_044CA840
          bErr_044CA840=bTrsErr
          this.Try_044CA840()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_044CA840
          * --- End
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_MFLCASC,'SCQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_MFLRISE,'SCQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_MFLORDI,'SCQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_MFLIMPE,'SCQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
            +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
            +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_MVCODCOM);
                   )
          else
            update (i_cTable) set;
                SCQTAPER = &i_cOp1.;
                ,SCQTRPER = &i_cOp2.;
                ,SCQTOPER = &i_cOp3.;
                ,SCQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_MVKEYSAL;
                and SCCODMAG = this.w_MVCODMAG;
                and SCCODCAN = this.w_MVCODCOM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore Aggiornamento Saldi Commessa'
            return
          endif
        endif
      endif
      * --- Eventuale Magazzino Collegato
      if NOT EMPTY(this.w_MVCODMAT) AND NOT EMPTY(this.w_MF2CASC+this.w_MF2RISE+this.w_MF2ORDI+this.w_MF2IMPE)
        * --- Try
        local bErr_044C3E50
        bErr_044C3E50=bTrsErr
        this.Try_044C3E50()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- accept error
          bTrsErr=.f.
        endif
        bTrsErr=bTrsErr or bErr_044C3E50
        * --- End
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_MF2CASC,'SLQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_MF2RISE,'SLQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_MF2ORDI,'SLQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_MF2IMPE,'SLQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
          +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
          +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
                 )
        else
          update (i_cTable) set;
              SLQTAPER = &i_cOp1.;
              ,SLQTRPER = &i_cOp2.;
              ,SLQTOPER = &i_cOp3.;
              ,SLQTIPER = &i_cOp4.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_MVKEYSAL;
              and SLCODMAG = this.w_MVCODMAT;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if NVL(this.w_FLCOM1,"N")="S" 
          * --- Try
          local bErr_044C5A40
          bErr_044C5A40=bTrsErr
          this.Try_044C5A40()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- accept error
            bTrsErr=.f.
          endif
          bTrsErr=bTrsErr or bErr_044C5A40
          * --- End
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_MF2CASC,'SCQTAPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_MF2RISE,'SCQTRPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_MF2ORDI,'SCQTOPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_MF2IMPE,'SCQTIPER','this.w_MVQTAUM1',this.w_MVQTAUM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
            +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
            +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_MVKEYSAL);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_MVCODMAT);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_MVCODCOM);
                   )
          else
            update (i_cTable) set;
                SCQTAPER = &i_cOp1.;
                ,SCQTRPER = &i_cOp2.;
                ,SCQTOPER = &i_cOp3.;
                ,SCQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_MVKEYSAL;
                and SCCODMAG = this.w_MVCODMAT;
                and SCCODCAN = this.w_MVCODCOM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore Aggiornamento Saldi Commessa'
            return
          endif
        endif
      endif
    endif
    if NOT EMPTY(this.w_MVSERRIF) AND this.w_MVROWRIF<>0 And Not Empty( this.w_MVFLARIF )
      * --- Aggiorna Documento di Evasione
      this.w_MVQTASAL = IIF(this.w_MVFLERIF="S", 0, MAX(this.w_EVQTASAL - this.w_MVQTAUM1, 0))
      this.w_EVKEYSAL = t_EVKEYSAL
      this.w_EVCODMAG = t_EVCODMAG
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_cOp2=cp_SetTrsOp(this.w_MVFLARIF,'MVQTAEVA','this.w_QTAIMP',this.w_QTAIMP,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_MVFLARIF,'MVQTAEV1','this.w_QTAIM1',this.w_QTAIM1,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_MVQTASAL),'DOC_DETT','MVQTASAL');
        +",MVQTAEVA ="+cp_NullLink(i_cOp2,'DOC_DETT','MVQTAEVA');
        +",MVQTAEV1 ="+cp_NullLink(i_cOp3,'DOC_DETT','MVQTAEV1');
        +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(this.w_MVFLERIF),'DOC_DETT','MVFLEVAS');
        +",MVEFFEVA ="+cp_NullLink(cp_ToStrODBC(this.w_MVDATREG),'DOC_DETT','MVEFFEVA');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_MVSERRIF);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_MVROWRIF);
            +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
               )
      else
        update (i_cTable) set;
            MVQTASAL = this.w_MVQTASAL;
            ,MVQTAEVA = &i_cOp2.;
            ,MVQTAEV1 = &i_cOp3.;
            ,MVFLEVAS = this.w_MVFLERIF;
            ,MVEFFEVA = this.w_MVDATREG;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_MVSERRIF;
            and CPROWNUM = this.w_MVROWRIF;
            and MVNUMRIF = this.w_MVNUMRIF;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_EVQTASAL = this.w_MVQTASAL - this.w_EVQTASAL
      if NOT EMPTY(this.w_EVKEYSAL) AND NOT EMPTY(this.w_EVCODMAG)
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_EVFLRISE,'SLQTRPER','this.w_EVQTASAL',this.w_EVQTASAL,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_EVFLORDI,'SLQTOPER','this.w_EVQTASAL',this.w_EVQTASAL,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_EVFLIMPE,'SLQTIPER','this.w_EVQTASAL',this.w_EVQTASAL,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTRPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTRPER');
          +",SLQTOPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_EVKEYSAL);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_EVCODMAG);
                 )
        else
          update (i_cTable) set;
              SLQTRPER = &i_cOp1.;
              ,SLQTOPER = &i_cOp2.;
              ,SLQTIPER = &i_cOp3.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_EVKEYSAL;
              and SLCODMAG = this.w_EVCODMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_EVKEYSAL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.w_EVKEYSAL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLCOM2 = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NVL(this.w_FLCOM2,"N")="S" 
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_EVFLRISE,'SCQTRPER','this.w_EVQTASAL',this.w_EVQTASAL,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_EVFLORDI,'SCQTOPER','this.w_EVQTASAL',this.w_EVQTASAL,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_EVFLIMPE,'SCQTIPER','this.w_EVQTASAL',this.w_EVQTASAL,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTRPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTRPER');
            +",SCQTOPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_EVKEYSAL);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_EVCODMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_MVCODCOM);
                   )
          else
            update (i_cTable) set;
                SCQTRPER = &i_cOp1.;
                ,SCQTOPER = &i_cOp2.;
                ,SCQTIPER = &i_cOp3.;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_EVKEYSAL;
                and SCCODMAG = this.w_EVCODMAG;
                and SCCODCAN = this.w_MVCODCOM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
    endif
    SELECT GeneApp
    ENDSCAN
    * --- Attenzione: Non essedoci  Pagamento non vi sono Rate
    * --- Aggiorna Riferimento sul Documento di Origine
    if this.w_FLAGG="P"
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVRIFESP ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_MAST','MVRIFESP');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
               )
      else
        update (i_cTable) set;
            MVRIFESP = this.w_MVSERIAL;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_SERIAL;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVRIFESC ="+cp_NullLink(cp_ToStrODBC(this.w_MVSERIAL),'DOC_DETT','MVRIFESC');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_DBRK);
            +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
               )
      else
        update (i_cTable) set;
            MVRIFESC = this.w_MVSERIAL;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_SERIAL;
            and CPROWNUM = this.w_DBRK;
            and MVNUMRIF = this.w_MVNUMRIF;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- Calcoli finali
    this.pDOCINFO=createobject("DOCOBJ",this)
    this.pDOCINFO.cMVSERIAL = this.w_MVSERIAL
    * --- Rileggo il documento dal database...
    this.pDOCINFO.cRead = "A"
    this.w_RET = CASTIVARATE(.null., this.pDOCINFO)
    if Empty(this.w_RET.nErrorLog)
      GSUT_BAT(this, this.w_MVSERIAL, this.w_RET )
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      * --- Scrivo errore nel campo di log
      this.w_oERRORLOG.AddMsgLog(this.w_RET.nErrorLog)     
      * --- Raise
      i_Error=this.w_RET.nErrorLog
      return
    endif
    if lower(this.w_PADRE.class)<>"tgsar_bgd"
      * --- commit
      cp_EndTrs(.t.)
    endif
    this.w_OKDOC = this.w_OKDOC + 1
    * --- NUMDOC azzerato per evitare che i documenti successivi vengano 'Forzati'
    this.w_MVNUMDOC = 0
    return
  proc Try_044C8D70()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_MVKEYSAL,'SLCODMAG',this.w_MVCODMAG)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_MVKEYSAL;
           ,this.w_MVCODMAG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_044CA840()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAG),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_MVKEYSAL,'SCCODMAG',this.w_MVCODMAG,'SCCODCAN',this.w_MVCODCOM,'SCCODART',this.w_MVCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.w_MVKEYSAL;
           ,this.w_MVCODMAG;
           ,this.w_MVCODCOM;
           ,this.w_MVCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_044C3E50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDIART
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDIART_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SLCODICE"+",SLCODMAG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'SALDIART','SLCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAT),'SALDIART','SLCODMAG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SLCODICE',this.w_MVKEYSAL,'SLCODMAG',this.w_MVCODMAT)
      insert into (i_cTable) (SLCODICE,SLCODMAG &i_ccchkf. );
         values (;
           this.w_MVKEYSAL;
           ,this.w_MVCODMAT;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_044C5A40()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into SALDICOM
    i_nConn=i_TableProp[this.SALDICOM_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"SCCODICE"+",SCCODMAG"+",SCCODCAN"+",SCCODART"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_MVKEYSAL),'SALDICOM','SCCODICE');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODMAT),'SALDICOM','SCCODMAG');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODCOM),'SALDICOM','SCCODCAN');
      +","+cp_NullLink(cp_ToStrODBC(this.w_MVCODART),'SALDICOM','SCCODART');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'SCCODICE',this.w_MVKEYSAL,'SCCODMAG',this.w_MVCODMAT,'SCCODCAN',this.w_MVCODCOM,'SCCODART',this.w_MVCODART)
      insert into (i_cTable) (SCCODICE,SCCODMAG,SCCODCAN,SCCODART &i_ccchkf. );
         values (;
           this.w_MVKEYSAL;
           ,this.w_MVCODMAT;
           ,this.w_MVCODCOM;
           ,this.w_MVCODART;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Variabili Locali
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_MVPREZZO = 0
    * --- Num. Max. Sconti Consentiti
    this.w_MVSCONT1 = 0
    this.w_MVSCONT2 = 0
    this.w_MVSCONT3 = 0
    this.w_MVSCONT4 = 0
    this.w_MVFLOMAG = "X"
    this.w_MVCODIVA = CALCODIV(this.w_MVCODART, this.w_MVTIPCON, this.w_MVCODCON , Space(10), iif( this.w_MVFLVEAC="V", this.w_MVDATREG , this.w_MVDATDOC ))
    * --- Read from VOCIIVA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VOCIIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VOCIIVA_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "IVPERIVA,IVBOLIVA"+;
        " from "+i_cTable+" VOCIIVA where ";
            +"IVCODIVA = "+cp_ToStrODBC(this.w_MVCODIVA);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        IVPERIVA,IVBOLIVA;
        from (i_cTable) where;
            IVCODIVA = this.w_MVCODIVA;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_PERIVA = NVL(cp_ToDate(_read_.IVPERIVA),cp_NullValue(_read_.IVPERIVA))
      this.w_BOLIVA = NVL(cp_ToDate(_read_.IVBOLIVA),cp_NullValue(_read_.IVBOLIVA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_MVVALRIG = 0
    this.w_MVIMPACC = 0
    this.w_MVIMPSCO = 0
    this.w_MVVALMAG = 0
    this.w_MVIMPNAZ = 0
    this.w_LIPREZZO = 0
    * --- Calcola Prezzo
    this.w_MVPREZZO = 0
    this.w_RICTOT = .F.
    if USED("APPOVALO") 
      SELECT APPOVALO
      if RECCOUNT()>0
        GO TOP
        this.w_ACCEDE = .T.
      endif
    endif
    * --- -Sto caricando record documenti componenti
    *     -Non devo valorizzare attraverso i componenti il prodotto finito
    if this.w_TIPREC="2" OR this.w_VALCOM="N"
      if this.w_ACCEDE
        do case
          case this.w_DITIPVAL="L"
            * --- Leggo il Listino Qta Scaglione e Data di Attivaziione piu' Prossimi
            LOCATE FOR NVL(LICODART,"  ")=this.w_MVCODART AND NVL(LIQUANTI,0)>=this.w_MVQTAUM1
            if FOUND()
              this.w_MVPREZZO = NVL(LIPREZZO, 0)
              if this.w_MVPREZZO<>0 AND this.w_TIPOLN="L" AND NVL(IVPERIVA,0)<>0
                * --- Se Listino al Lordo , Nettifica
                this.w_APPO1 = cp_ROUND(this.w_MVPREZZO - (this.w_MVPREZZO / (1 + (IVPERIVA / 100))), this.w_DECUNI)
                this.w_MVPREZZO = this.w_MVPREZZO - this.w_APPO1
              endif
            endif
          case this.w_DITIPVAL="X"
            * --- Cerco il Costo dell'Articolo nei Dati Articoli (Prima sul Magazzino)
            LOCATE FOR NVL(PRCODART,"  ")=this.w_MVCODART AND NVL(PRCODMAG, SPACE(5))=this.w_MVCODMAG
            if FOUND()
              this.w_MVPREZZO = NVL(PRCOSSTA, 0)
            else
              * --- ...Se non esiste sul Magazzino nei Dati generici
              GO TOP
              LOCATE FOR NVL(PRCODART,"  ")=this.w_MVCODART AND NVL(PRCODMAG, SPACE(5))="#####"
              if FOUND()
                this.w_MVPREZZO = NVL(PRCOSSTA, 0)
              endif
            endif
          case this.w_DITIPVAL $ "SMUP"
            if this.w_TDCOSEPL="S"
              if used("APPOVALO")
                * --- Cerco il Costo dell'Articolo nel Cursore calcolato sull'Inventario
                 
 LOCATE FOR NVL(DICODICE,"  ")=this.w_MVCODART
                if FOUND()
                  this.w_MVPREZZO = NVL(Icase(this.w_DITIPVAL="S", DICOSSTA, this.w_DITIPVAL="M", DICOSMPA, this.w_DITIPVAL="P",DICOSMPP,DICOSULT), 0)
                endif
              endif
            else
              * --- Cerco il Costo dell'Articolo nel Cursore calcolato sull'Inventario
              if used("tempValo")
                select tempValo
                if this.w_TIPREC="2"
                  locate for artcom=this.w_MVCODART AND Nvl(UNIMIS,"   ")=this.w_MVUNIMIS
                else
                  locate for codart=this.w_MVCODART 
                endif
                if found()
                  this.w_MVPREZZO = NVL(COSUNI, 0)
                endif
              endif
            endif
          case this.w_DITIPVAL="A"
            * --- Read from SALDIART
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.SALDIART_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "SLVALUCA"+;
                " from "+i_cTable+" SALDIART where ";
                    +"SLCODICE = "+cp_ToStrODBC(this.w_MVCODART);
                    +" and SLCODMAG = "+cp_ToStrODBC(this.w_MVCODMAG);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                SLVALUCA;
                from (i_cTable) where;
                    SLCODICE = this.w_MVCODART;
                    and SLCODMAG = this.w_MVCODMAG;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MVPREZZO = NVL(cp_ToDate(_read_.SLVALUCA),cp_NullValue(_read_.SLVALUCA))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
        endcase
        if this.w_MVQTAUM1<>this.w_MVQTAMOV AND this.w_MVPREZZO<>0 and Not (this.w_DITIPVAL $ "SMUP" and this.w_TDCOSEPL<>"S")
          * --- Se La Quantita' componente espressa in altra UM, riporto il Prezzo Unitario alla UM presente in Distinta.
          *     tranne nel caso di criteri inventariali dove viene gi� eseguita nel GSDS_BDC
          this.w_MVPREZZO = cp_ROUND((this.w_MVPREZZO * this.w_MVQTAUM1)/this.w_MVQTAMOV, this.w_DECUNI)
        endif
      endif
    else
      * --- Solo Se :
      *     -Documento Principale 
      *     -Documento di Evasione Prodotti Finiti
      *     -Documento Principale e Documento di Evasione Prodotti Finiti
      this.w_PRZFIN = 0
      this.w_MVNUMRIF = -20
      * --- Aggiorno Riga documento principale
      if this.w_VALCOM $ "E-D-P" or Not(NOT EMPTY(this.w_MMCAUPFI) AND NOT EMPTY(this.w_MMCAMPFI) AND this.w_CREAESP)
        * --- Rileggo dati documento principale per eseguire costificazione
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVQTAMOV,MVQTAUM1,MVVALULT,MVFLULCA,MVFLULPV,MVCODMAG,MVKEYSAL,MVUNIMIS"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWPAD);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVQTAMOV,MVQTAUM1,MVVALULT,MVFLULCA,MVFLULPV,MVCODMAG,MVKEYSAL,MVUNIMIS;
            from (i_cTable) where;
                MVSERIAL = this.w_SERIAL;
                and CPROWNUM = this.w_ROWPAD;
                and MVNUMRIF = this.w_MVNUMRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_MVQTAMOV = NVL(cp_ToDate(_read_.MVQTAMOV),cp_NullValue(_read_.MVQTAMOV))
          this.w_MVQTAUM1 = NVL(cp_ToDate(_read_.MVQTAUM1),cp_NullValue(_read_.MVQTAUM1))
          this.w_MVVALULT = NVL(cp_ToDate(_read_.MVVALULT),cp_NullValue(_read_.MVVALULT))
          this.w_MVFLULCA = NVL(cp_ToDate(_read_.MVFLULCA),cp_NullValue(_read_.MVFLULCA))
          this.w_MVFLULPV = NVL(cp_ToDate(_read_.MVFLULPV),cp_NullValue(_read_.MVFLULPV))
          this.w_MAGSAL = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
          this.w_KEYSAL = NVL(cp_ToDate(_read_.MVKEYSAL),cp_NullValue(_read_.MVKEYSAL))
          this.w_MVUNIMIS = NVL(cp_ToDate(_read_.MVUNIMIS),cp_NullValue(_read_.MVUNIMIS))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
      endif
      if this.w_TDCOSEPL="N"
        if used("tempValo")
          select tempValo
          locate for artcom=this.w_MVCODART AND (Nvl(UNIMIS,"   ")=this.w_MVUNIMIS OR this.w_DITIPVAL<>"L")
          if found()
            this.w_PRZFIN = NVL(COSUNI, 0)
          endif
        endif
      else
        this.Page_9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      if this.w_TIPREC="1" AND this.w_VALCOM<>"N" AND this.w_TDCOSEPL="N"
        if this.w_MVQTAUM1<>this.w_MVQTAMOV AND this.w_PRZFIN<>0
          * --- Se La Quantita' componente espressa in altra UM, riporto il Prezzo Unitario alla UM presente in Distinta.
          this.w_PRZFIN = cp_ROUND((this.w_PRZFIN * this.w_MVQTAUM1)/this.w_MVQTAMOV, this.w_DECUNI)
        endif
      else
        if this.w_MVQTAUM1<>this.w_MVQTAMOV AND this.w_PRZFIN<>0 and Not (this.w_DITIPVAL $ "SMUP" )
          * --- Se La Quantita' componente espressa in altra UM, riporto il Prezzo Unitario alla UM presente in Distinta.
          this.w_PRZFIN = cp_ROUND((this.w_PRZFIN * this.w_MVQTAUM1)/this.w_MVQTAMOV, this.w_DECUNI)
        endif
      endif
      if this.w_VALCOM $ "P-E"
        this.w_MVPREZZO = this.w_PRZFIN
      endif
      if this.w_VALCOM $ "E-D"
        this.w_RICTOT = .T.
        this.w_VALRIG = cp_ROUND(this.w_MVQTAMOV * this.w_PRZFIN, this.w_DECTOT)
        this.w_VALMAG = CAVALMAG("N", this.w_VALRIG, this.w_MVIMPSCO, this.w_MVIMPACC, this.w_PERIVA, this.w_DECTOT, Space(5), 0 )
        this.w_IMPNAZ = CAIMPNAZ(this.w_MVFLVEAC, this.w_VALMAG, this.w_MVCAOVAL, this.w_CAONAZ, this.w_MVDATDOC, this.w_MVVALNAZ, this.w_MVCODVAL, Space(5), 0, 0, 0, 0 )
        this.w_MVVALULT = IIF(this.w_MVQTAUM1=0, 0, cp_ROUND(this.w_IMPNAZ/this.w_MVQTAUM1, g_PERPUL))
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVPREZZO ="+cp_NullLink(cp_ToStrODBC(this.w_PRZFIN),'DOC_DETT','MVPREZZO');
          +",MVVALRIG ="+cp_NullLink(cp_ToStrODBC(this.w_VALRIG),'DOC_DETT','MVVALRIG');
          +",MVVALULT ="+cp_NullLink(cp_ToStrODBC(this.w_MVVALULT),'DOC_DETT','MVVALULT');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWPAD);
              +" and MVNUMRIF = "+cp_ToStrODBC(this.w_MVNUMRIF);
                 )
        else
          update (i_cTable) set;
              MVPREZZO = this.w_PRZFIN;
              ,MVVALRIG = this.w_VALRIG;
              ,MVVALULT = this.w_MVVALULT;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_SERIAL;
              and CPROWNUM = this.w_ROWPAD;
              and MVNUMRIF = this.w_MVNUMRIF;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if this.w_MVFLULPV="=" or this.w_MVFLULCA="="
          * --- Se previsto aggiorno ultimo costo\prezzo nei saldi
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_MVFLULCA,'SLVALUCA','this.w_MVVALULT ',this.w_MVVALULT ,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_MVFLULCA,'SLDATUCA','this.w_MVDATDOC',this.w_MVDATDOC,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_MVFLULPV,'SLVALUPV','this.w_MVVALULT ',this.w_MVVALULT ,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_MVFLULCA,'SLCODVAA','this.w_MVVALNAZ',this.w_MVVALNAZ,'update',i_nConn)
            i_cOp5=cp_SetTrsOp(this.w_MVFLULPV,'SLDATUPV','this.w_MVDATDOC',this.w_MVDATDOC,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLVALUCA ="+cp_NullLink(i_cOp1,'SALDIART','SLVALUCA');
            +",SLDATUCA ="+cp_NullLink(i_cOp2,'SALDIART','SLDATUCA');
            +",SLVALUPV ="+cp_NullLink(i_cOp3,'SALDIART','SLVALUPV');
            +",SLCODVAA ="+cp_NullLink(i_cOp4,'SALDIART','SLCODVAA');
            +",SLDATUPV ="+cp_NullLink(i_cOp5,'SALDIART','SLDATUPV');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_MAGSAL);
                   )
          else
            update (i_cTable) set;
                SLVALUCA = &i_cOp1.;
                ,SLDATUCA = &i_cOp2.;
                ,SLVALUPV = &i_cOp3.;
                ,SLCODVAA = &i_cOp4.;
                ,SLDATUPV = &i_cOp5.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_KEYSAL;
                and SLCODMAG = this.w_MAGSAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
    endif
    this.w_MVVALRIG = CAVALRIG(this.w_MVPREZZO,this.w_MVQTAMOV, this.w_MVSCONT1,this.w_MVSCONT2,this.w_MVSCONT3,this.w_MVSCONT4,this.w_DECTOT)
    * --- Funzione per calcolo valore di magazzino: considera vuoto il campo MVCODIVE (codice Iva agevolata)
    *     Gli ultimi due parametri Vuoti sono il codice Iva Agevolata e la ua percentuale
    this.w_MVVALMAG = CAVALMAG("N", this.w_MVVALRIG, this.w_MVIMPSCO, this.w_MVIMPACC, this.w_PERIVA, this.w_DECTOT, Space(5), 0 )
    * --- Importo in valuta di conto: considero sempre documenti di vendita:
    *     Gli ultimi 5 parametri vuoti servono per l'Iva indetraibile gestita solo negli Acquisti
    this.w_MVIMPNAZ = CAIMPNAZ(this.w_MVFLVEAC, this.w_MVVALMAG, this.w_MVCAOVAL, this.w_CAONAZ, this.w_MVDATDOC, this.w_MVVALNAZ, this.w_MVCODVAL, Space(5), 0, 0, 0, 0 )
    this.w_MVCODCEN = Space(15)
    this.w_MVCODCOM = Space(15)
    this.w_MVCODATT = Space(15)
    this.w_MVVOCCEN = Space(15)
    this.w_MVIMPCOM = 0
    SELECT APPOEVA
    if this.w_MVTIPRIG<>"D" And this.w_TESTANAL
      if g_PERCCR="S" And this.w_FLANAL = "S"
        * --- Assegno centro di costo del documento padre
        *     Se non c'� lo vado a leggere dall'anagrafica articolo
        this.w_MVCODCEN = Nvl(MVCODCEN,Space(15))
        if Empty(this.w_MVCODCEN) Or this.w_PFLANAL<>"S"
          this.w_MVCODCEN = Nvl(ARCODCEN,Space(15))
        endif
        * --- Assegno la voce di costo ricavo
        if this.w_VOCECR="R"
          this.w_MVVOCCEN = Nvl(ARVOCRIC,Space(15))
        else
          this.w_MVVOCCEN = Nvl(ARVOCCEN,Space(15))
        endif
        * --- Se vuoto il centro di costo o la voce di costo/ricavo visualizzo il log
        if Empty(this.w_MVCODCEN)
          if lower(this.w_PADRE.class)=="tgsar_bgd"
            this.w_PADRE.w_PADRE.AddLogMsg("W", ah_MsgFormat("Riga documento di origine: %1%0Documento generato: %2 articolo: %3%0Centro di costo\ricavo mancante: controllare l'anagrafica articolo", Alltrim(Str(this.w_ROWDISP,4,0)), Alltrim(this.w_MVTIPDOC), Alltrim(this.w_MVCODART) ))     
          endif
          this.w_oPartLog = this.w_oMessLog.AddMsgPartNL("Riga documento di origine: %1%0Documento generato: %2 articolo: %3%0Centro di costo\ricavo mancante: controllare l'anagrafica articolo%0")
          this.w_oPartLog.AddParam(Alltrim(Str(this.w_ROWDISP,4,0)))     
          this.w_oPartLog.AddParam(Alltrim(this.w_MVTIPDOC))     
          this.w_oPartLog.AddParam(Alltrim(this.w_MVCODART))     
        endif
      endif
      if (g_PERCCR="S" And this.w_FLANAL = "S") OR ((g_PERCAN="S" AND this.w_FLANAL="S") Or (g_COMM="S" AND this.w_FLGCOM="S"))
        * --- Assegno la voce di costo ricavo
        if this.w_VOCECR="R"
          this.w_MVVOCCEN = Nvl(ARVOCRIC,Space(15))
        else
          this.w_MVVOCCEN = Nvl(ARVOCCEN,Space(15))
        endif
        if Empty(this.w_MVVOCCEN)
          if this.w_VOCECR="R"
            if lower(this.w_PADRE.class)=="tgsar_bgd"
              this.w_PADRE.w_PADRE.AddLogMsg("W", ah_MsgFormat("Riga documento di origine: %1%0Documento generato: %2 articolo: %3%0Voce di ricavo mancante: controllare l'anagrafica articolo", Alltrim(Str(this.w_ROWDISP,4,0)), Alltrim(this.w_MVTIPDOC), Alltrim(this.w_MVCODART) ))     
            endif
            this.w_oPartLog = this.w_oMessLog.AddMsgPartNL("Riga documento di origine: %1%0Documento generato: %2 articolo: %3%0Voce di ricavo mancante: controllare l'anagrafica articolo%0")
            this.w_oPartLog.AddParam(Alltrim(Str(this.w_ROWDISP,4,0)))     
            this.w_oPartLog.AddParam(Alltrim(this.w_MVTIPDOC))     
            this.w_oPartLog.AddParam(Alltrim(this.w_MVCODART))     
          else
            if lower(this.w_PADRE.class)=="tgsar_bgd"
              this.w_PADRE.w_PADRE.AddLogMsg("W", ah_MsgFormat("Riga documento di origine: %1%0Documento generato: %2 articolo: %3%0Voce di costo mancante: controllare l'anagrafica articolo", Alltrim(Str(this.w_ROWDISP,4,0)), Alltrim(this.w_MVTIPDOC), Alltrim(this.w_MVCODART) ))     
            endif
            this.w_oPartLog = this.w_oMessLog.AddMsgPartNL("Riga documento di origine: %1%0Documento generato: %2 articolo: %3%0Voce di costo mancante: controllare l'anagrafica articolo%0")
            this.w_oPartLog.AddParam(Alltrim(Str(this.w_ROWDISP,4,0)))     
            this.w_oPartLog.AddParam(Alltrim(this.w_MVTIPDOC))     
            this.w_oPartLog.AddParam(Alltrim(this.w_MVCODART))     
          endif
        endif
      endif
      if (g_PERCAN="S" AND this.w_FLANAL="S") Or (g_COMM="S" AND this.w_FLGCOM="S")
        this.w_MVCODCOM = Nvl(MVCODCOM,Space(15))
        if NOT EMPTY(this.w_MVCODCOM) AND g_COMM="S" AND this.w_FLGCOM="S" 
          this.w_MVCODATT = Nvl(MVCODATT,Space(15))
        endif
        if Not Empty(this.w_MVCODCOM)
          * --- Read from CAN_TIER
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAN_TIER_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAN_TIER_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CNCODVAL"+;
              " from "+i_cTable+" CAN_TIER where ";
                  +"CNCODCAN = "+cp_ToStrODBC(this.w_MVCODCOM);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CNCODVAL;
              from (i_cTable) where;
                  CNCODCAN = this.w_MVCODCOM;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_COCODVAL = NVL(cp_ToDate(_read_.CNCODVAL),cp_NullValue(_read_.CNCODVAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if Not Empty(this.w_COCODVAL)
            * --- Read from VALUTE
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.VALUTE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2])
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "VADECTOT"+;
                " from "+i_cTable+" VALUTE where ";
                    +"VACODVAL = "+cp_ToStrODBC(this.w_COCODVAL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                VADECTOT;
                from (i_cTable) where;
                    VACODVAL = this.w_COCODVAL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_DECCOM = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
        else
          if lower(this.w_PADRE.class)=="tgsar_bgd"
            this.w_PADRE.w_PADRE.AddLogMsg("W", ah_MsgFormat("Riga documento di origine: %1%0Documento generato: %2 articolo: %3%0Commessa mancante: aggiornare il documento maualmente%0o tramite 'Visualizza schede di analitica'", Alltrim(Str(this.w_ROWDISP,4,0)), Alltrim(this.w_MVTIPDOC), Alltrim(this.w_MVCODART) ))     
          endif
          this.w_oPartLog = this.w_oMessLog.AddMsgPartNL("Riga documento di origine: %1%0Documento generato: %2 articolo: %3%0Commessa mancante: aggiornare il documento maualmente%0o tramite 'Visualizza schede di analitica'%0")
          this.w_oPartLog.AddParam(Alltrim(Str(this.w_ROWDISP,4,0)))     
          this.w_oPartLog.AddParam(Alltrim(this.w_MVTIPDOC))     
          this.w_oPartLog.AddParam(Alltrim(this.w_MVCODART))     
        endif
        this.w_MVFLORCO = ""
        this.w_MVFLCOCO = ""
        this.w_MVFLORCO = IIF(g_COMM="S" AND this.w_MVFLVEAC$ "AV" ,iif(this.w_MFLCOMM="I","+",IIF(this.w_MFLCOMM="D","-"," "))," ")
        this.w_MVFLCOCO = IIF(g_COMM="S" AND this.w_MVFLVEAC $ "AV" ,iif(this.w_MFLCOMM="C","+",IIF(this.w_MFLCOMM="S","-"," "))," ")
        this.w_MVIMPCOM = CAIMPCOM( IIF(Empty(this.w_MVCODCOM),"S", "N" ), this.w_MVVALNAZ, this.w_COCODVAL, this.w_MVIMPNAZ, this.w_CAONAZ, IIF(EMPTY(this.w_MVDATDOC), this.w_MVDATREG, this.w_MVDATDOC), this.w_DECCOM )
        if NOT EMPTY(this.w_MVVOCCEN)
          * --- Read from VOC_COST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VOC_COST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VOC_COST_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VCTIPCOS"+;
              " from "+i_cTable+" VOC_COST where ";
                  +"VCCODICE = "+cp_ToStrODBC(this.w_MVVOCCEN);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VCTIPCOS;
              from (i_cTable) where;
                  VCCODICE = this.w_MVVOCCEN;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            w_CODCOS = NVL(cp_ToDate(_read_.VCTIPCOS),cp_NullValue(_read_.VCTIPCOS))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_MVCODCOS = w_CODCOS
        endif
        if this.w_MVIMPCOM<>0
          * --- Write into MA_COSTI
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.MA_COSTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.MA_COSTI_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_MVFLCOCO,'CSCONSUN','this.w_MVIMPCOM',this.w_MVIMPCOM,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_MVFLORCO,'CSORDIN','this.w_MVIMPCOM',this.w_MVIMPCOM,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.MA_COSTI_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"CSCONSUN ="+cp_NullLink(i_cOp1,'MA_COSTI','CSCONSUN');
            +",CSORDIN ="+cp_NullLink(i_cOp2,'MA_COSTI','CSORDIN');
                +i_ccchkf ;
            +" where ";
                +"CSCODCOM = "+cp_ToStrODBC(this.w_MVCODCOM);
                +" and CSTIPSTR = "+cp_ToStrODBC(this.w_MVTIPATT);
                +" and CSCODMAT = "+cp_ToStrODBC(this.w_MVCODATT);
                   )
          else
            update (i_cTable) set;
                CSCONSUN = &i_cOp1.;
                ,CSORDIN = &i_cOp2.;
                &i_ccchkf. ;
             where;
                CSCODCOM = this.w_MVCODCOM;
                and CSTIPSTR = this.w_MVTIPATT;
                and CSCODMAT = this.w_MVCODATT;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
    endif
    this.w_MVPESNET = NVL(ARPESNET, 0)
    this.w_MVNOMENC = NVL(ARNOMENC, SPACE(8))
    this.w_MVFLTRAS = IIF(NVL(ARDATINT,"F")="S","Z",IIF( NVL(ARDATINT,"F")="F"," ",IIF( NVL(ARDATINT,"F")="I","I","S")))
    this.w_MVUMSUPP = NVL(ARUMSUPP, SPACE(3))
    this.w_MVMOLSUP = NVL(ARMOLSUP, 0)
    this.w_MVTIPATT = "A"
    * --- Totalizzatori
    this.w_TOTALE = this.w_TOTALE + this.w_MVVALRIG
    this.w_TOTMERCE = this.w_TOTMERCE + IIF(this.w_MVFLOMAG="X", this.w_MVVALRIG, 0)
    if !this.w_ONLYAGG
      INSERT INTO GeneApp ;
      (t_MVTIPRIG, t_MVCODICE, t_MVCODART, ;
      t_MVDESART, t_MVDESSUP, t_MVUNIMIS, t_MVCATCON, ;
      t_MVCODCLA, t_MVCONTRA, t_MVCODLIS, ;
      t_MVQTAMOV, t_MVQTAUM1, t_MVPREZZO, ;
      t_MVIMPACC, t_MVIMPSCO, t_MVVALMAG, t_MVIMPNAZ, ;
      t_MVSCONT1, t_MVSCONT2, t_MVSCONT3, t_MVSCONT4, ;
      t_MVFLOMAG, t_MVCODIVA, t_PERIVA, t_BOLIVA, t_MVVALRIG, ;
      t_MVPESNET, t_MVNOMENC, t_MVUMSUPP, t_MVMOLSUP, ;
      t_MVCODMAG, t_MVCODMAT, t_MVTIPATT, t_MVDATEVA, t_FLSERA, ;
      t_MVSERRIF, t_MVROWRIF, t_EVQTASAL, t_EVCODMAG, t_EVKEYSAL, ;
      t_EVFLORDI, t_EVFLIMPE, t_EVFLRISE, t_MVFLERIF, t_FLLOTT, t_FLUBIC, ;
      t_MVFLCOCO, t_MVFLORCO, t_MVCODCOS, ;
      t_F2UBIC, t_MVIMPCOM, t_MVCODCOM, t_MVIMPAC2, t_MVCODCEN, t_MVVOCCEN,t_MVCODATT, ROWDISP, t_MVFLTRAS) ;
      VALUES (this.w_MVTIPRIG, this.w_MVCODICE, this.w_MVCODART, ;
      this.w_MVDESART, this.w_MVDESSUP, this.w_MVUNIMIS, this.w_MVCATCON, ;
      this.w_MVCODCLA, this.w_MVCONTRA, this.w_MVCODLIS, ;
      this.w_MVQTAMOV, this.w_MVQTAUM1, this.w_MVPREZZO, ;
      this.w_MVIMPACC, this.w_MVIMPSCO, this.w_MVVALMAG, this.w_MVIMPNAZ, ;
      this.w_MVSCONT1, this.w_MVSCONT2, this.w_MVSCONT3, this.w_MVSCONT4, ;
      this.w_MVFLOMAG, this.w_MVCODIVA, this.w_PERIVA, this.w_BOLIVA, this.w_MVVALRIG, ;
      this.w_MVPESNET, this.w_MVNOMENC, this.w_MVUMSUPP, this.w_MVMOLSUP, ;
      this.w_MVCODMAG, this.w_MVCODMAT, this.w_MVTIPATT, this.w_MVDATEVA, " ", ;
      this.w_MVSERRIF, this.w_MVROWRIF, this.w_EVQTASAL, this.w_EVCODMAG, this.w_EVKEYSAL, ;
      this.w_EVFLORDI, this.w_EVFLIMPE, this.w_EVFLRISE, this.w_MVFLERIF, this.w_FLLOTT, this.w_FLUBIC, ;
      this.w_MVFLCOCO, this.w_MVFLORCO, this.w_MVCODCOS, ;
      this.w_F2UBIC, this.w_MVIMPCOM, this.w_MVCODCOM, 0, this.w_MVCODCEN, this.w_MVVOCCEN,this.w_MVCODATT, this.w_ROWDISP, this.w_MVFLTRAS)
    endif
  endproc


  procedure Page_7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Rimuovo i cursori
    USE IN SELECT("TMP_SCAR")
    USE IN SELECT("TES_PLOS")
    USE IN SELECT("APPOEVA")
    USE IN SELECT("APPOVALO")
    USE IN SELECT("UMNOFR")
    USE IN SELECT("GeneApp")
    USE IN SELECT("ELABREC")
    USE IN SELECT("Tempvalo")
    USE IN SELECT("APPCOE")
    USE IN SELECT("IMBALLI")
    USE IN SELECT("TOTCOE")
    * --- Per non rieseguire i calcoli sui documenti
    this.bUpdateParentObject=.F.
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eliminazione Documento di Evasione Componenti/Prodotti Finiti
    * --- Aggiorna prima i saldi - Poi cancella dettaglio e testata
    * --- Select from DOC_DETT
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_DETT ";
          +" where MVSERIAL="+cp_ToStrODBC(this.w_DOCEVA)+"";
           ,"_Curs_DOC_DETT")
    else
      select * from (i_cTable);
       where MVSERIAL=this.w_DOCEVA;
        into cursor _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      select _Curs_DOC_DETT
      locate for 1=1
      do while not(eof())
      this.w_EVAKEY = NVL(_Curs_DOC_DETT.MVKEYSAL, "")
      this.w_EVAMAG = NVL(_Curs_DOC_DETT.MVCODMAG,"")
      this.w_EVAMAT = NVL(_Curs_DOC_DETT.MVCODMAT,"")
      this.w_EVAQTA = NVL(_Curs_DOC_DETT.MVQTAMOV, 0)
      this.w_EVAQT1 = NVL(_Curs_DOC_DETT.MVQTAUM1, 0)
      this.w_EVQIMP = NVL(_Curs_DOC_DETT.MVQTAIMP, 0)
      this.w_EVQIM1 = NVL(_Curs_DOC_DETT.MVQTAIM1, 0)
      this.w_EVASER = NVL(_Curs_DOC_DETT.MVSERRIF,"")
      this.w_EVAROW = NVL(_Curs_DOC_DETT.MVROWRIF, 0)
      this.w_EVAFLA = NVL(_Curs_DOC_DETT.MVFLARIF, " ")
      this.w_EVACAR = IIF(_Curs_DOC_DETT.MVFLCASC="-","+",IIF(_Curs_DOC_DETT.MVFLCASC="+","-"," "))
      this.w_EVAORD = IIF(_Curs_DOC_DETT.MVFLORDI="-","+",IIF(_Curs_DOC_DETT.MVFLORDI="+","-"," "))
      this.w_EVAIMP = IIF(_Curs_DOC_DETT.MVFLIMPE="-","+",IIF(_Curs_DOC_DETT.MVFLIMPE="+","-"," "))
      this.w_EVARIS = IIF(_Curs_DOC_DETT.MVFLRISE="-","+",IIF(_Curs_DOC_DETT.MVFLRISE="+","-"," "))
      this.w_EV2CAR = IIF(_Curs_DOC_DETT.MVF2CASC="-","+",IIF(_Curs_DOC_DETT.MVF2CASC="+","-"," "))
      this.w_EV2ORD = IIF(_Curs_DOC_DETT.MVF2ORDI="-","+",IIF(_Curs_DOC_DETT.MVF2ORDI="+","-"," "))
      this.w_EV2IMP = IIF(_Curs_DOC_DETT.MVF2IMPE="-","+",IIF(_Curs_DOC_DETT.MVF2IMPE="+","-"," "))
      this.w_EV2RIS = IIF(_Curs_DOC_DETT.MVF2RISE="-","+",IIF(_Curs_DOC_DETT.MVF2RISE="+","-"," "))
      if NOT EMPTY(this.w_EVAKEY) AND NOT EMPTY(this.w_EVAMAG)
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARSALCOM"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.w_EVAKEY);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARSALCOM;
            from (i_cTable) where;
                ARCODART = this.w_EVAKEY;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLCOM1 = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Write into SALDIART
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.SALDIART_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
          i_cOp1=cp_SetTrsOp(this.w_EVACAR,'SLQTAPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
          i_cOp2=cp_SetTrsOp(this.w_EVARIS,'SLQTRPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
          i_cOp3=cp_SetTrsOp(this.w_EVAORD,'SLQTOPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
          i_cOp4=cp_SetTrsOp(this.w_EVAIMP,'SLQTIPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
          +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
          +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
          +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
              +i_ccchkf ;
          +" where ";
              +"SLCODICE = "+cp_ToStrODBC(this.w_EVAKEY);
              +" and SLCODMAG = "+cp_ToStrODBC(this.w_EVAMAG);
                 )
        else
          update (i_cTable) set;
              SLQTAPER = &i_cOp1.;
              ,SLQTRPER = &i_cOp2.;
              ,SLQTOPER = &i_cOp3.;
              ,SLQTIPER = &i_cOp4.;
              &i_ccchkf. ;
           where;
              SLCODICE = this.w_EVAKEY;
              and SLCODMAG = this.w_EVAMAG;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if NVL(this.w_FLCOM1,"N")="S" 
          * --- Write into SALDICOM
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDICOM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_EVACAR,'SCQTAPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_EVARIS,'SCQTRPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_EVAORD,'SCQTOPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_EVAIMP,'SCQTIPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
            +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
            +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
            +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SCCODICE = "+cp_ToStrODBC(this.w_EVAKEY);
                +" and SCCODMAG = "+cp_ToStrODBC(this.w_EVAMAG);
                +" and SCCODCAN = "+cp_ToStrODBC(this.w_MVCODCOM);
                   )
          else
            update (i_cTable) set;
                SCQTAPER = &i_cOp1.;
                ,SCQTRPER = &i_cOp2.;
                ,SCQTOPER = &i_cOp3.;
                ,SCQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SCCODICE = this.w_EVAKEY;
                and SCCODMAG = this.w_EVAMAG;
                and SCCODCAN = this.w_MVCODCOM;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error='Errore Aggiornamento Saldi Commessa'
            return
          endif
        endif
        if NOT EMPTY(this.w_EVAMAT) AND NOT EMPTY(this.w_EV2CAR+this.w_EV2ORD+this.w_EV2IMP+this.w_EV2RIS)
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_EV2CAR,'SLQTAPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_EV2RIS,'SLQTRPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_EV2ORD,'SLQTOPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
            i_cOp4=cp_SetTrsOp(this.w_EV2IMP,'SLQTIPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTAPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTAPER');
            +",SLQTRPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTRPER');
            +",SLQTOPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTOPER');
            +",SLQTIPER ="+cp_NullLink(i_cOp4,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_EVAKEY);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_EVAMAT);
                   )
          else
            update (i_cTable) set;
                SLQTAPER = &i_cOp1.;
                ,SLQTRPER = &i_cOp2.;
                ,SLQTOPER = &i_cOp3.;
                ,SLQTIPER = &i_cOp4.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_EVAKEY;
                and SLCODMAG = this.w_EVAMAT;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if NVL(this.w_FLCOM1,"N")="S" 
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_EV2CAR,'SCQTAPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_EV2RIS,'SCQTRPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_EV2ORD,'SCQTOPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
              i_cOp4=cp_SetTrsOp(this.w_EV2IMP,'SCQTIPER','this.w_EVAQT1',this.w_EVAQT1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTAPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTAPER');
              +",SCQTRPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTRPER');
              +",SCQTOPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTOPER');
              +",SCQTIPER ="+cp_NullLink(i_cOp4,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_EVAKEY);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_EVAMAT);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_MVCODCOM);
                     )
            else
              update (i_cTable) set;
                  SCQTAPER = &i_cOp1.;
                  ,SCQTRPER = &i_cOp2.;
                  ,SCQTOPER = &i_cOp3.;
                  ,SCQTIPER = &i_cOp4.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_EVAKEY;
                  and SCCODMAG = this.w_EVAMAT;
                  and SCCODCAN = this.w_MVCODCOM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore Aggiornamento Saldi Commessa'
              return
            endif
          endif
        endif
      endif
      if NOT EMPTY(this.w_EVASER) AND this.w_EVAROW<>0 AND this.w_EVAFLA="+"
        * --- Storna Evasione Documento di Origine
        this.w_FLORDI = " "
        this.w_FLIMPE = " "
        this.w_FLRISE = " "
        this.w_EVAKEY2 = " "
        this.w_EVAMAG2 = " "
        this.w_OQTAEVA = 0
        this.w_OQTAEV1 = 0
        this.w_OQTAMOV = 0
        this.w_OQTAUM1 = 0
        this.w_OQTASAL = 0
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVFLORDI,MVFLIMPE,MVFLRISE,MVKEYSAL,MVCODMAG,MVQTAEV1,MVQTAEVA,MVQTAMOV,MVQTAUM1,MVQTASAL"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_EVASER);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_EVAROW);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVFLORDI,MVFLIMPE,MVFLRISE,MVKEYSAL,MVCODMAG,MVQTAEV1,MVQTAEVA,MVQTAMOV,MVQTAUM1,MVQTASAL;
            from (i_cTable) where;
                MVSERIAL = this.w_EVASER;
                and CPROWNUM = this.w_EVAROW;
                and MVNUMRIF = this.w_NUMRIF;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_FLORDI = NVL(cp_ToDate(_read_.MVFLORDI),cp_NullValue(_read_.MVFLORDI))
          this.w_FLIMPE = NVL(cp_ToDate(_read_.MVFLIMPE),cp_NullValue(_read_.MVFLIMPE))
          this.w_FLRISE = NVL(cp_ToDate(_read_.MVFLRISE),cp_NullValue(_read_.MVFLRISE))
          this.w_EVAKEY2 = NVL(cp_ToDate(_read_.MVKEYSAL),cp_NullValue(_read_.MVKEYSAL))
          this.w_EVAMAG2 = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
          this.w_OQTAEV1 = NVL(cp_ToDate(_read_.MVQTAEV1),cp_NullValue(_read_.MVQTAEV1))
          this.w_OQTAEVA = NVL(cp_ToDate(_read_.MVQTAEVA),cp_NullValue(_read_.MVQTAEVA))
          this.w_OQTAMOV = NVL(cp_ToDate(_read_.MVQTAMOV),cp_NullValue(_read_.MVQTAMOV))
          this.w_OQTAUM1 = NVL(cp_ToDate(_read_.MVQTAUM1),cp_NullValue(_read_.MVQTAUM1))
          this.w_OQTASAL = NVL(cp_ToDate(_read_.MVQTASAL),cp_NullValue(_read_.MVQTASAL))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_NQTAEVA = this.w_OQTAEVA - this.w_EVQIMP
        this.w_NQTAEV1 = this.w_OQTAEV1 - this.w_EVQIM1
        this.w_NQTASAL = IIF(this.w_NQTAEV1>this.w_OQTAUM1, 0, this.w_OQTAUM1-this.w_NQTAEV1)
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVQTAEVA ="+cp_NullLink(cp_ToStrODBC(this.w_NQTAEVA),'DOC_DETT','MVQTAEVA');
          +",MVQTAEV1 ="+cp_NullLink(cp_ToStrODBC(this.w_NQTAEV1),'DOC_DETT','MVQTAEV1');
          +",MVQTASAL ="+cp_NullLink(cp_ToStrODBC(this.w_NQTASAL),'DOC_DETT','MVQTASAL');
          +",MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_EVASER);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_EVAROW);
              +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                 )
        else
          update (i_cTable) set;
              MVQTAEVA = this.w_NQTAEVA;
              ,MVQTAEV1 = this.w_NQTAEV1;
              ,MVQTASAL = this.w_NQTASAL;
              ,MVFLEVAS = " ";
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_EVASER;
              and CPROWNUM = this.w_EVAROW;
              and MVNUMRIF = this.w_NUMRIF;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if NOT EMPTY(this.w_EVAKEY2) AND NOT EMPTY(this.w_EVAMAG2) AND NOT EMPTY(this.w_FLORDI+this.w_FLIMPE+this.w_FLRISE)
          * --- Read from ART_ICOL
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ART_ICOL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ARSALCOM"+;
              " from "+i_cTable+" ART_ICOL where ";
                  +"ARCODART = "+cp_ToStrODBC(this.w_EVAKEY2);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ARSALCOM;
              from (i_cTable) where;
                  ARCODART = this.w_EVAKEY2;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_FLCOM2 = NVL(cp_ToDate(_read_.ARSALCOM),cp_NullValue(_read_.ARSALCOM))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_NQTASAL = this.w_NQTASAL - this.w_OQTASAL
          * --- Write into SALDIART
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.SALDIART_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
            i_cOp1=cp_SetTrsOp(this.w_FLRISE,'SLQTRPER','this.w_EVQIM1',this.w_EVQIM1,'update',i_nConn)
            i_cOp2=cp_SetTrsOp(this.w_FLORDI,'SLQTOPER','this.w_EVQIM1',this.w_EVQIM1,'update',i_nConn)
            i_cOp3=cp_SetTrsOp(this.w_FLIMPE,'SLQTIPER','this.w_EVQIM1',this.w_EVQIM1,'update',i_nConn)
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"SLQTRPER ="+cp_NullLink(i_cOp1,'SALDIART','SLQTRPER');
            +",SLQTOPER ="+cp_NullLink(i_cOp2,'SALDIART','SLQTOPER');
            +",SLQTIPER ="+cp_NullLink(i_cOp3,'SALDIART','SLQTIPER');
                +i_ccchkf ;
            +" where ";
                +"SLCODICE = "+cp_ToStrODBC(this.w_EVAKEY2);
                +" and SLCODMAG = "+cp_ToStrODBC(this.w_EVAMAG2);
                   )
          else
            update (i_cTable) set;
                SLQTRPER = &i_cOp1.;
                ,SLQTOPER = &i_cOp2.;
                ,SLQTIPER = &i_cOp3.;
                &i_ccchkf. ;
             where;
                SLCODICE = this.w_EVAKEY2;
                and SLCODMAG = this.w_EVAMAG2;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          if NVL(this.w_FLCOM2,"N")="S" 
            * --- Write into SALDICOM
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.SALDICOM_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.SALDICOM_idx,2])
              i_cOp1=cp_SetTrsOp(this.w_FLRISE,'SCQTRPER','this.w_EVQIM1',this.w_EVQIM1,'update',i_nConn)
              i_cOp2=cp_SetTrsOp(this.w_FLORDI,'SCQTOPER','this.w_EVQIM1',this.w_EVQIM1,'update',i_nConn)
              i_cOp3=cp_SetTrsOp(this.w_FLIMPE,'SCQTIPER','this.w_EVQIM1',this.w_EVQIM1,'update',i_nConn)
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDICOM_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"SCQTRPER ="+cp_NullLink(i_cOp1,'SALDICOM','SCQTRPER');
              +",SCQTOPER ="+cp_NullLink(i_cOp2,'SALDICOM','SCQTOPER');
              +",SCQTIPER ="+cp_NullLink(i_cOp3,'SALDICOM','SCQTIPER');
                  +i_ccchkf ;
              +" where ";
                  +"SCCODICE = "+cp_ToStrODBC(this.w_EVAKEY2);
                  +" and SCCODMAG = "+cp_ToStrODBC(this.w_EVAMAG2);
                  +" and SCCODCAN = "+cp_ToStrODBC(this.w_MVCODCOM);
                     )
            else
              update (i_cTable) set;
                  SCQTRPER = &i_cOp1.;
                  ,SCQTOPER = &i_cOp2.;
                  ,SCQTIPER = &i_cOp3.;
                  &i_ccchkf. ;
               where;
                  SCCODICE = this.w_EVAKEY2;
                  and SCCODMAG = this.w_EVAMAG2;
                  and SCCODCAN = this.w_MVCODCOM;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error='Errore Aggiornamento Saldi Commessa'
              return
            endif
          endif
        endif
      endif
        select _Curs_DOC_DETT
        continue
      enddo
      use
    endif
    * --- Elimina Documento
    GSAR_BED(this,this.w_DOCEVA, this.w_MVNUMRIF)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    if this.w_DOCRIG
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVRIFESC ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_DETT','MVRIFESC');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERORI);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWORI);
               )
      else
        update (i_cTable) set;
            MVRIFESC = SPACE(10);
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_SERORI;
            and CPROWNUM = this.w_ROWORI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVRIFESP ="+cp_NullLink(cp_ToStrODBC(SPACE(10)),'DOC_MAST','MVRIFESP');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERORI);
               )
      else
        update (i_cTable) set;
            MVRIFESP = SPACE(10);
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_SERORI;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Select from EVA_COMP
    i_nConn=i_TableProp[this.EVA_COMP_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.EVA_COMP_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select MVQTAMOV,MVROWNUM,MVSERIAL,MVTIPREC,MVQTAMOV,MVPREZZO  from "+i_cTable+" EVA_COMP ";
          +" where "+cp_ToStrODBC(this.w_ROWPAD)+"=MVROWNUM AND "+cp_ToStrODBC(this.w_SERIAL)+"=MVSERIAL AND MVTIPREC='2'";
           ,"_Curs_EVA_COMP")
    else
      select MVQTAMOV,MVROWNUM,MVSERIAL,MVTIPREC,MVQTAMOV,MVPREZZO from (i_cTable);
       where this.w_ROWPAD=MVROWNUM AND this.w_SERIAL=MVSERIAL AND MVTIPREC="2";
        into cursor _Curs_EVA_COMP
    endif
    if used('_Curs_EVA_COMP')
      select _Curs_EVA_COMP
      locate for 1=1
      do while not(eof())
      this.w_PRZFIN = this.w_PRZFIN + cp_ROUND((Nvl(_Curs_EVA_COMP.MVQTAMOV,0) * Nvl(_Curs_EVA_COMP.MVPREZZO,0)), 6)
        select _Curs_EVA_COMP
        continue
      enddo
      use
    endif
    this.w_PRZFIN = this.w_PRZFIN/this.w_MVQTAUM1
    if this.w_MVQTAUM1<>this.w_MVQTAMOV AND this.w_PRZFIN<>0
      * --- Se La Quantita' componente espressa in altra UM, riporto il Prezzo Unitario alla UM presente in Distinta.
      this.w_PRZFIN = cp_ROUND((this.w_PRZFIN * this.w_MVQTAUM1)/this.w_MVQTAMOV, this.w_DECUNI)
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,23)]
    this.cWorkTables[1]='CAM_AGAZ'
    this.cWorkTables[2]='CAN_TIER'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='DISMBASE'
    this.cWorkTables[5]='DOC_DETT'
    this.cWorkTables[6]='DOC_MAST'
    this.cWorkTables[7]='DOC_RATE'
    this.cWorkTables[8]='ESERCIZI'
    this.cWorkTables[9]='EVA_ARTI'
    this.cWorkTables[10]='EVA_COMP'
    this.cWorkTables[11]='LISTINI'
    this.cWorkTables[12]='MAGAZZIN'
    this.cWorkTables[13]='PAR_DISB'
    this.cWorkTables[14]='PAR_RIOR'
    this.cWorkTables[15]='SALDIART'
    this.cWorkTables[16]='TIP_DOCU'
    this.cWorkTables[17]='VALUTE'
    this.cWorkTables[18]='VOCIIVA'
    this.cWorkTables[19]='ART_ICOL'
    this.cWorkTables[20]='VOC_COST'
    this.cWorkTables[21]='MA_COSTI'
    this.cWorkTables[22]='SALDICOM'
    this.cWorkTables[23]='SALOTCOM'
    return(this.OpenAllTables(23))

  proc CloseCursors()
    if used('_Curs_EVA_COMP')
      use in _Curs_EVA_COMP
    endif
    if used('_Curs_gsva_qcs')
      use in _Curs_gsva_qcs
    endif
    if used('_Curs_EVA_ARTI')
      use in _Curs_EVA_ARTI
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
