* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gstevkcf                                                        *
*              Visualizza cash flow                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_182]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-08-29                                                      *
* Last revis.: 2015-10-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgstevkcf",oParentObject))

* --- Class definition
define class tgstevkcf as StdForm
  Top    = 1
  Left   = 11

  * --- Standard Properties
  Width  = 763
  Height = 541+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-27"
  HelpContextID=97727593
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=60

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  PNT_MAST_IDX = 0
  VALUTE_IDX = 0
  SMOBSCAM_IDX = 0
  CATECOMM_IDX = 0
  AZIENDA_IDX = 0
  cPrg = "gstevkcf"
  cComment = "Visualizza cash flow"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_TIPOVISUA = space(1)
  o_TIPOVISUA = space(1)
  w_PAGRD = space(2)
  o_PAGRD = space(2)
  w_PAGRI = space(2)
  o_PAGRI = space(2)
  w_PAGBO = space(2)
  o_PAGBO = space(2)
  w_PAGCA = space(2)
  o_PAGCA = space(2)
  w_PAGRB = space(2)
  o_PAGRB = space(2)
  w_PAGMA = space(2)
  o_PAGMA = space(2)
  w_TIPCER = space(1)
  o_TIPCER = space(1)
  w_TIPEFF = space(1)
  o_TIPEFF = space(1)
  w_FLSCAD = space(1)
  o_FLSCAD = space(1)
  w_TIPATT = space(1)
  o_TIPATT = space(1)
  w_SALIVAC = space(1)
  o_SALIVAC = space(1)
  w_SALIVAT = space(1)
  o_SALIVAT = space(1)
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_TIPPER = space(1)
  w_DATCAM = ctod('  /  /  ')
  w_SELEZI = space(1)
  o_SELEZI = space(1)
  w_TIPCLI = space(1)
  o_TIPCLI = space(1)
  w_CODCLI = space(15)
  w_TIPFOR = space(1)
  o_TIPFOR = space(1)
  w_CODFOR = space(15)
  w_TIPGEN = space(1)
  w_CATECOMM = space(3)
  w_SMOSCA = space(10)
  w_PERIMP = 0
  w_PRIPAGRD = space(2)
  w_PRIPAGRI = space(2)
  w_PRIPAGBO = space(2)
  w_PRIPAGRB = space(2)
  w_PRIPAGCA = space(2)
  w_PRIPAGMA = space(2)
  w_NDOCIN = 0
  w_ADOCIN = space(10)
  w_DDOCIN = ctod('  /  /  ')
  w_NDOCFI = 0
  w_ADOCFI = space(10)
  w_DDOCFI = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_CODVAL = space(3)
  w_VADESCRI = space(35)
  w_SSDESCRI = space(254)
  w_CDESCRI = space(40)
  w_FDESCRI = space(40)
  w_CTDESCRI = space(35)
  w_TIPOPAG = space(2)
  o_TIPOPAG = space(2)
  w_DESCPAGA = space(25)
  w_NUMCOR = space(15)
  w_ROWSEL = 0
  w_COLSEL = 0
  w_TIPODATO = space(15)
  w_CONTINUM = 0
  w_TIPOCONTI = space(15)
  w_DETTPAGAMENTI = space(1)
  w_TIPOPAGAMENTO = space(2)
  w_PERIODI_ELABORATI = 0
  w_CFDATCRE = ctod('  /  /  ')
  w_ABILITAZOOM = .F.
  w_FLSEVA = space(1)
  w_CODAZI = space(10)
  w_Zoomcf = .NULL.
  w_Zoomdetcf = .NULL.
  w_Zoomdetpa = .NULL.
  w_Zoomdetcb = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gstevkcf
  *---------------------------
  *DICHIARAZIONE ARRAY GLOBALI
  *---------------------------
  DIMENSION PERIODO(1)
  DIMENSION CERTI(1,1)
  DIMENSION EFFETTIVI(1,1)
  DIMENSION ATTESI(1,1)
  DIMENSION CODCONTI(1,1)
  DIMENSION SALIVAC(1,1)
  DIMENSION SALIVAT(1,1)
  DIMENSION TIPOCONTI(1)
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgstevkcfPag1","gstevkcf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgstevkcfPag2","gstevkcf",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Parametri aggiuntivi")
      .Pages(3).addobject("oPag","tgstevkcfPag3","gstevkcf",3)
      .Pages(3).oPag.Visible=.t.
      .Pages(3).Caption=cp_Translate("Dettaglio cash flow")
      .Pages(3).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTIPOVISUA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_Zoomcf = this.oPgFrm.Pages(1).oPag.Zoomcf
    this.w_Zoomdetcf = this.oPgFrm.Pages(3).oPag.Zoomdetcf
    this.w_Zoomdetpa = this.oPgFrm.Pages(3).oPag.Zoomdetpa
    this.w_Zoomdetcb = this.oPgFrm.Pages(3).oPag.Zoomdetcb
    DoDefault()
    proc Destroy()
      this.w_Zoomcf = .NULL.
      this.w_Zoomdetcf = .NULL.
      this.w_Zoomdetpa = .NULL.
      this.w_Zoomdetcb = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='PNT_MAST'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='SMOBSCAM'
    this.cWorkTables[5]='CATECOMM'
    this.cWorkTables[6]='AZIENDA'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOVISUA=space(1)
      .w_PAGRD=space(2)
      .w_PAGRI=space(2)
      .w_PAGBO=space(2)
      .w_PAGCA=space(2)
      .w_PAGRB=space(2)
      .w_PAGMA=space(2)
      .w_TIPCER=space(1)
      .w_TIPEFF=space(1)
      .w_FLSCAD=space(1)
      .w_TIPATT=space(1)
      .w_SALIVAC=space(1)
      .w_SALIVAT=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_TIPPER=space(1)
      .w_DATCAM=ctod("  /  /  ")
      .w_SELEZI=space(1)
      .w_TIPCLI=space(1)
      .w_CODCLI=space(15)
      .w_TIPFOR=space(1)
      .w_CODFOR=space(15)
      .w_TIPGEN=space(1)
      .w_CATECOMM=space(3)
      .w_SMOSCA=space(10)
      .w_PERIMP=0
      .w_PRIPAGRD=space(2)
      .w_PRIPAGRI=space(2)
      .w_PRIPAGBO=space(2)
      .w_PRIPAGRB=space(2)
      .w_PRIPAGCA=space(2)
      .w_PRIPAGMA=space(2)
      .w_NDOCIN=0
      .w_ADOCIN=space(10)
      .w_DDOCIN=ctod("  /  /  ")
      .w_NDOCFI=0
      .w_ADOCFI=space(10)
      .w_DDOCFI=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_CODVAL=space(3)
      .w_VADESCRI=space(35)
      .w_SSDESCRI=space(254)
      .w_CDESCRI=space(40)
      .w_FDESCRI=space(40)
      .w_CTDESCRI=space(35)
      .w_TIPOPAG=space(2)
      .w_DESCPAGA=space(25)
      .w_NUMCOR=space(15)
      .w_ROWSEL=0
      .w_COLSEL=0
      .w_TIPODATO=space(15)
      .w_CONTINUM=0
      .w_TIPOCONTI=space(15)
      .w_DETTPAGAMENTI=space(1)
      .w_TIPOPAGAMENTO=space(2)
      .w_PERIODI_ELABORATI=0
      .w_CFDATCRE=ctod("  /  /  ")
      .w_ABILITAZOOM=.f.
      .w_FLSEVA=space(1)
      .w_CODAZI=space(10)
        .w_TIPOVISUA = 'C'
        .w_PAGRD = 'RD'
        .w_PAGRI = 'RI'
        .w_PAGBO = 'BO'
        .w_PAGCA = 'CA'
        .w_PAGRB = 'RB'
        .w_PAGMA = 'MA'
        .w_TIPCER = 'S'
          .DoRTCalc(9,9,.f.)
        .w_FLSCAD = 'N'
          .DoRTCalc(11,11,.f.)
        .w_SALIVAC = ' '
        .w_SALIVAT = ' '
        .w_DATINI = i_datsys
        .w_DATFIN = IIF(Not Empty(.w_Datini),.w_Datini+30,i_datsys+30)
        .w_TIPPER = 'S'
        .w_DATCAM = i_DATSYS
        .w_SELEZI = 'S'
        .w_TIPCLI = 'C'
        .w_CODCLI = ' '
        .DoRTCalc(20,20,.f.)
        if not(empty(.w_CODCLI))
          .link_2_2('Full')
        endif
        .w_TIPFOR = 'F'
        .w_CODFOR = ' '
        .DoRTCalc(22,22,.f.)
        if not(empty(.w_CODFOR))
          .link_2_4('Full')
        endif
        .w_TIPGEN = 'G'
        .DoRTCalc(24,24,.f.)
        if not(empty(.w_CATECOMM))
          .link_2_6('Full')
        endif
        .w_SMOSCA = Space(10) 
        .DoRTCalc(25,25,.f.)
        if not(empty(.w_SMOSCA))
          .link_2_7('Full')
        endif
        .w_PERIMP = 0
        .w_PRIPAGRD = ' '
        .w_PRIPAGRI = ' '
        .w_PRIPAGBO = ' '
        .w_PRIPAGRB = ' '
        .w_PRIPAGCA = ' '
        .w_PRIPAGMA = ' '
      .oPgFrm.Page1.oPag.Zoomcf.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
          .DoRTCalc(33,38,.f.)
        .w_OBTEST = i_DATSYS
        .DoRTCalc(40,40,.f.)
        if not(empty(.w_CODVAL))
          .link_1_27('Full')
        endif
      .oPgFrm.Page3.oPag.Zoomdetcf.Calculate()
      .oPgFrm.Page3.oPag.Zoomdetpa.Calculate()
          .DoRTCalc(41,45,.f.)
        .w_TIPOPAG = .w_zoomdetpa.getVar('NUMCOR')
        .w_DESCPAGA = .w_zoomdetpa.getVar('TIPOCONTI')
      .oPgFrm.Page3.oPag.Zoomdetcb.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .w_NUMCOR = iif(.w_TipoVisua='C',.w_zoomdetcf.getVar('NUMCOR'),.w_zoomdetcb.getVar('NUMCOR'))
      .oPgFrm.Page3.oPag.oObj_3_8.Calculate()
        .w_ROWSEL = iif(.w_TipoVisua='C',iif(.w_Zoomdetcf.GRD.ActiveRow=0,.w_ROWSEL,.w_Zoomdetcf.GRD.ActiveRow),iif(.w_Zoomdetcb.GRD.ActiveRow=0,.w_ROWSEL,.w_Zoomdetcb.GRD.ActiveRow))
        .w_COLSEL = iif(.w_TipoVisua='C',iif(.w_Zoomdetcf.GRD.ActiveColumn=0,.w_COLSEL,.w_Zoomdetcf.GRD.ActiveColumn)-1,iif(.w_Zoomdetcb.GRD.ActiveColumn=0,.w_COLSEL,.w_Zoomdetcb.GRD.ActiveColumn)-1)
        .w_TIPODATO = iif(.w_TipoVisua='C',.w_zoomdetcf.getVar('TIPODATO'),.w_zoomdetcb.getVar('TIPODATO'))
          .DoRTCalc(52,52,.f.)
        .w_TIPOCONTI = .w_zoomdetcf.getVar('TIPOCONTI')
      .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .w_DETTPAGAMENTI = 'N'
      .oPgFrm.Page3.oPag.oObj_3_18.Calculate()
          .DoRTCalc(55,56,.f.)
        .w_CFDATCRE = cp_todate(DataStoricoCF())
        .w_ABILITAZOOM = .T.
          .DoRTCalc(59,59,.f.)
        .w_CODAZI = i_CODAZI
        .DoRTCalc(60,60,.f.)
        if not(empty(.w_CODAZI))
          .link_1_40('Full')
        endif
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,14,.t.)
        if .o_DATINI<>.w_DATINI
            .w_DATFIN = IIF(Not Empty(.w_Datini),.w_Datini+30,i_datsys+30)
        endif
        .DoRTCalc(16,19,.t.)
        if .o_TIPCLI<>.w_TIPCLI
            .w_CODCLI = ' '
          .link_2_2('Full')
        endif
        .DoRTCalc(21,21,.t.)
        if .o_TIPFOR<>.w_TIPFOR
            .w_CODFOR = ' '
          .link_2_4('Full')
        endif
        .DoRTCalc(23,24,.t.)
        if .o_TIPEFF<>.w_TIPEFF.or. .o_FLSCAD<>.w_FLSCAD
            .w_SMOSCA = Space(10) 
          .link_2_7('Full')
        endif
        if .o_TIPEFF<>.w_TIPEFF
            .w_PERIMP = 0
        endif
        .oPgFrm.Page1.oPag.Zoomcf.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page3.oPag.Zoomdetcf.Calculate()
        .oPgFrm.Page3.oPag.Zoomdetpa.Calculate()
        .DoRTCalc(27,45,.t.)
            .w_TIPOPAG = .w_zoomdetpa.getVar('NUMCOR')
        if .o_TipoPag<>.w_TipoPag
            .w_DESCPAGA = .w_zoomdetpa.getVar('TIPOCONTI')
        endif
        .oPgFrm.Page3.oPag.Zoomdetcb.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
            .w_NUMCOR = iif(.w_TipoVisua='C',.w_zoomdetcf.getVar('NUMCOR'),.w_zoomdetcb.getVar('NUMCOR'))
        .oPgFrm.Page3.oPag.oObj_3_8.Calculate()
            .w_ROWSEL = iif(.w_TipoVisua='C',iif(.w_Zoomdetcf.GRD.ActiveRow=0,.w_ROWSEL,.w_Zoomdetcf.GRD.ActiveRow),iif(.w_Zoomdetcb.GRD.ActiveRow=0,.w_ROWSEL,.w_Zoomdetcb.GRD.ActiveRow))
            .w_COLSEL = iif(.w_TipoVisua='C',iif(.w_Zoomdetcf.GRD.ActiveColumn=0,.w_COLSEL,.w_Zoomdetcf.GRD.ActiveColumn)-1,iif(.w_Zoomdetcb.GRD.ActiveColumn=0,.w_COLSEL,.w_Zoomdetcb.GRD.ActiveColumn)-1)
            .w_TIPODATO = iif(.w_TipoVisua='C',.w_zoomdetcf.getVar('TIPODATO'),.w_zoomdetcb.getVar('TIPODATO'))
        .DoRTCalc(52,52,.t.)
            .w_TIPOCONTI = .w_zoomdetcf.getVar('TIPOCONTI')
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_18.Calculate()
        if .o_SELEZI<>.w_SELEZI
          .Calculate_FNIQLMDPKA()
        endif
        Local l_Dep1,l_Dep2,l_Dep3
        l_Dep1= .o_TIPCER<>.w_TIPCER .or. .o_TIPATT<>.w_TIPATT .or. .o_TIPEFF<>.w_TIPEFF .or. .o_TIPOVISUA<>.w_TIPOVISUA .or. .o_PAGBO<>.w_PAGBO
        l_Dep2= .o_PAGCA<>.w_PAGCA .or. .o_PAGMA<>.w_PAGMA .or. .o_PAGRB<>.w_PAGRB .or. .o_PAGRD<>.w_PAGRD .or. .o_PAGRI<>.w_PAGRI
        l_Dep3= .o_SALIVAC<>.w_SALIVAC .or. .o_SALIVAT<>.w_SALIVAT
        if m.l_Dep1 .or. m.l_Dep2 .or. m.l_Dep3
          .Calculate_LUUJYSAMCK()
        endif
        if .o_TIPATT<>.w_TIPATT
          .Calculate_QJZWYTXQUB()
        endif
        .DoRTCalc(54,59,.t.)
          .link_1_40('Full')
        * --- Area Manuale = Calculate
        * --- gstevkcf
        if this.w_Dettpagamenti='S' And Alltrim(This.w_Tipopag)<>Alltrim(This.w_Tipopagamento)
            This.NotifyEvent('ZoomDett')
            this.w_Tipopagamento=Alltrim(This.w_Tipopag )
        endif
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.Zoomcf.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_24.Calculate()
        .oPgFrm.Page3.oPag.Zoomdetcf.Calculate()
        .oPgFrm.Page3.oPag.Zoomdetpa.Calculate()
        .oPgFrm.Page3.oPag.Zoomdetcb.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_30.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_8.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_31.Calculate()
        .oPgFrm.Page3.oPag.oObj_3_18.Calculate()
    endwith
  return

  proc Calculate_EZARSDSYTT()
    with this
          * --- ZoomDett
          Gste_Bvf(this;
              ,'ZoomDett';
             )
    endwith
  endproc
  proc Calculate_FNIQLMDPKA()
    with this
          * --- Disabilitazione pagina 3 da zoom
          Gste_bvf(this;
              ,'ZoomDisable';
             )
    endwith
  endproc
  proc Calculate_LUUJYSAMCK()
    with this
          * --- Disabilitazione pagina 3
          Gste_bvf(this;
              ,'Disabilita';
             )
    endwith
  endproc
  proc Calculate_QJZWYTXQUB()
    with this
          * --- Cash Flow Atteso
          Gste_bvf(this;
              ,'Atteso';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page2.oPag.oCODCLI_2_2.enabled = this.oPgFrm.Page2.oPag.oCODCLI_2_2.mCond()
    this.oPgFrm.Page2.oPag.oCODFOR_2_4.enabled = this.oPgFrm.Page2.oPag.oCODFOR_2_4.mCond()
    this.oPgFrm.Page2.oPag.oSMOSCA_2_7.enabled = this.oPgFrm.Page2.oPag.oSMOSCA_2_7.mCond()
    this.oPgFrm.Page2.oPag.oPERIMP_2_8.enabled = this.oPgFrm.Page2.oPag.oPERIMP_2_8.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLSCAD_1_10.visible=!this.oPgFrm.Page1.oPag.oFLSCAD_1_10.mHide()
    this.oPgFrm.Page3.oPag.oStr_3_4.visible=!this.oPgFrm.Page3.oPag.oStr_3_4.mHide()
    this.oPgFrm.Page3.oPag.oDESCPAGA_3_5.visible=!this.oPgFrm.Page3.oPag.oDESCPAGA_3_5.mHide()
    this.oPgFrm.Page1.oPag.oFLSEVA_1_39.visible=!this.oPgFrm.Page1.oPag.oFLSEVA_1_39.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.Zoomcf.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_24.Event(cEvent)
      .oPgFrm.Page3.oPag.Zoomdetcf.Event(cEvent)
      .oPgFrm.Page3.oPag.Zoomdetpa.Event(cEvent)
      .oPgFrm.Page3.oPag.Zoomdetcb.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_30.Event(cEvent)
      .oPgFrm.Page3.oPag.oObj_3_8.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_31.Event(cEvent)
        if lower(cEvent)==lower("ZoomDett")
          .Calculate_EZARSDSYTT()
          bRefresh=.t.
        endif
      .oPgFrm.Page3.oPag.oObj_3_18.Event(cEvent)
        if lower(cEvent)==lower("w_zoomcf row checked") or lower(cEvent)==lower("w_zoomcf row unchecked") or lower(cEvent)==lower("zoomcf menucheck")
          .Calculate_FNIQLMDPKA()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCLI
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCLI;
                     ,'ANCODICE',trim(this.w_CODCLI))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCLI)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCLI)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCLI);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCLI) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCLI_2_2'),i_cWhere,'GSAR_BZC',"Elenco clienti",'gsar_scl.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLI);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCLI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCLI;
                       ,'ANCODICE',this.w_CODCLI)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLI = NVL(_Link_.ANCODICE,space(15))
      this.w_CDESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLI = space(15)
      endif
      this.w_CDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFOR
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPFOR;
                     ,'ANCODICE',trim(this.w_CODFOR))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFOR)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODFOR)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPFOR);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODFOR) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODFOR_2_4'),i_cWhere,'GSAR_BZC',"Elenco fornitori",'gsar_sfr.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPFOR<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore non ammesso")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODFOR);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPFOR);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPFOR;
                       ,'ANCODICE',this.w_CODFOR)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFOR = NVL(_Link_.ANCODICE,space(15))
      this.w_FDESCRI = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODFOR = space(15)
      endif
      this.w_FDESCRI = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CATECOMM
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CATECOMM_IDX,3]
    i_lTable = "CATECOMM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2], .t., this.CATECOMM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CATECOMM) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACT',True,'CATECOMM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CTCODICE like "+cp_ToStrODBC(trim(this.w_CATECOMM)+"%");

          i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CTCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CTCODICE',trim(this.w_CATECOMM))
          select CTCODICE,CTDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CTCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CATECOMM)==trim(_Link_.CTCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CATECOMM) and !this.bDontReportError
            deferred_cp_zoom('CATECOMM','*','CTCODICE',cp_AbsName(oSource.parent,'oCATECOMM_2_6'),i_cWhere,'GSAR_ACT',"Categorie commerciali",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',oSource.xKey(1))
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CATECOMM)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CTCODICE,CTDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where CTCODICE="+cp_ToStrODBC(this.w_CATECOMM);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CTCODICE',this.w_CATECOMM)
            select CTCODICE,CTDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CATECOMM = NVL(_Link_.CTCODICE,space(3))
      this.w_CTDESCRI = NVL(_Link_.CTDESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CATECOMM = space(3)
      endif
      this.w_CTDESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CATECOMM_IDX,2])+'\'+cp_ToStr(_Link_.CTCODICE,1)
      cp_ShowWarn(i_cKey,this.CATECOMM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CATECOMM Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=SMOSCA
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SMOBSCAM_IDX,3]
    i_lTable = "SMOBSCAM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SMOBSCAM_IDX,2], .t., this.SMOBSCAM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SMOBSCAM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SMOSCA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_MSS',True,'SMOBSCAM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SSCODICE like "+cp_ToStrODBC(trim(this.w_SMOSCA)+"%");

          i_ret=cp_SQL(i_nConn,"select SSCODICE,SSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SSCODICE',trim(this.w_SMOSCA))
          select SSCODICE,SSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SMOSCA)==trim(_Link_.SSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SMOSCA) and !this.bDontReportError
            deferred_cp_zoom('SMOBSCAM','*','SSCODICE',cp_AbsName(oSource.parent,'oSMOSCA_2_7'),i_cWhere,'GSTE_MSS',"Smobilizzo scaduto",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SSCODICE,SSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where SSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SSCODICE',oSource.xKey(1))
            select SSCODICE,SSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SMOSCA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SSCODICE,SSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where SSCODICE="+cp_ToStrODBC(this.w_SMOSCA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SSCODICE',this.w_SMOSCA)
            select SSCODICE,SSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SMOSCA = NVL(_Link_.SSCODICE,space(10))
      this.w_SSDESCRI = NVL(_Link_.SSDESCRI,space(254))
    else
      if i_cCtrl<>'Load'
        this.w_SMOSCA = space(10)
      endif
      this.w_SSDESCRI = space(254)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SMOBSCAM_IDX,2])+'\'+cp_ToStr(_Link_.SSCODICE,1)
      cp_ShowWarn(i_cKey,this.SMOBSCAM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SMOSCA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVAL
  func Link_1_27(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_CODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_CODVAL))
          select VACODVAL,VADESVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCODVAL_1_27'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_CODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_CODVAL)
            select VACODVAL,VADESVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_VADESCRI = NVL(_Link_.VADESVAL,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODVAL = space(3)
      endif
      this.w_VADESCRI = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAZI
  func Link_1_40(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AZIENDA_IDX,3]
    i_lTable = "AZIENDA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2], .t., this.AZIENDA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AZCODAZI,AZFLSEVA";
                   +" from "+i_cTable+" "+i_lTable+" where AZCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AZCODAZI',this.w_CODAZI)
            select AZCODAZI,AZFLSEVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAZI = NVL(_Link_.AZCODAZI,space(10))
      this.w_FLSEVA = NVL(_Link_.AZFLSEVA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODAZI = space(10)
      endif
      this.w_FLSEVA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AZIENDA_IDX,2])+'\'+cp_ToStr(_Link_.AZCODAZI,1)
      cp_ShowWarn(i_cKey,this.AZIENDA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oTIPOVISUA_1_1.RadioValue()==this.w_TIPOVISUA)
      this.oPgFrm.Page1.oPag.oTIPOVISUA_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRD_1_2.RadioValue()==this.w_PAGRD)
      this.oPgFrm.Page1.oPag.oPAGRD_1_2.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRI_1_3.RadioValue()==this.w_PAGRI)
      this.oPgFrm.Page1.oPag.oPAGRI_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGBO_1_4.RadioValue()==this.w_PAGBO)
      this.oPgFrm.Page1.oPag.oPAGBO_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGCA_1_5.RadioValue()==this.w_PAGCA)
      this.oPgFrm.Page1.oPag.oPAGCA_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGRB_1_6.RadioValue()==this.w_PAGRB)
      this.oPgFrm.Page1.oPag.oPAGRB_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAGMA_1_7.RadioValue()==this.w_PAGMA)
      this.oPgFrm.Page1.oPag.oPAGMA_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCER_1_8.RadioValue()==this.w_TIPCER)
      this.oPgFrm.Page1.oPag.oTIPCER_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPEFF_1_9.RadioValue()==this.w_TIPEFF)
      this.oPgFrm.Page1.oPag.oTIPEFF_1_9.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSCAD_1_10.RadioValue()==this.w_FLSCAD)
      this.oPgFrm.Page1.oPag.oFLSCAD_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPATT_1_11.RadioValue()==this.w_TIPATT)
      this.oPgFrm.Page1.oPag.oTIPATT_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_15.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_15.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_16.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_16.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPPER_1_17.RadioValue()==this.w_TIPPER)
      this.oPgFrm.Page1.oPag.oTIPPER_1_17.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATCAM_1_18.value==this.w_DATCAM)
      this.oPgFrm.Page1.oPag.oDATCAM_1_18.value=this.w_DATCAM
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_22.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_22.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPCLI_2_1.RadioValue()==this.w_TIPCLI)
      this.oPgFrm.Page2.oPag.oTIPCLI_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODCLI_2_2.value==this.w_CODCLI)
      this.oPgFrm.Page2.oPag.oCODCLI_2_2.value=this.w_CODCLI
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPFOR_2_3.RadioValue()==this.w_TIPFOR)
      this.oPgFrm.Page2.oPag.oTIPFOR_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODFOR_2_4.value==this.w_CODFOR)
      this.oPgFrm.Page2.oPag.oCODFOR_2_4.value=this.w_CODFOR
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPGEN_2_5.RadioValue()==this.w_TIPGEN)
      this.oPgFrm.Page2.oPag.oTIPGEN_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCATECOMM_2_6.value==this.w_CATECOMM)
      this.oPgFrm.Page2.oPag.oCATECOMM_2_6.value=this.w_CATECOMM
    endif
    if not(this.oPgFrm.Page2.oPag.oSMOSCA_2_7.value==this.w_SMOSCA)
      this.oPgFrm.Page2.oPag.oSMOSCA_2_7.value=this.w_SMOSCA
    endif
    if not(this.oPgFrm.Page2.oPag.oPERIMP_2_8.value==this.w_PERIMP)
      this.oPgFrm.Page2.oPag.oPERIMP_2_8.value=this.w_PERIMP
    endif
    if not(this.oPgFrm.Page2.oPag.oPRIPAGRD_2_9.RadioValue()==this.w_PRIPAGRD)
      this.oPgFrm.Page2.oPag.oPRIPAGRD_2_9.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRIPAGRI_2_10.RadioValue()==this.w_PRIPAGRI)
      this.oPgFrm.Page2.oPag.oPRIPAGRI_2_10.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRIPAGBO_2_11.RadioValue()==this.w_PRIPAGBO)
      this.oPgFrm.Page2.oPag.oPRIPAGBO_2_11.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRIPAGRB_2_12.RadioValue()==this.w_PRIPAGRB)
      this.oPgFrm.Page2.oPag.oPRIPAGRB_2_12.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRIPAGCA_2_13.RadioValue()==this.w_PRIPAGCA)
      this.oPgFrm.Page2.oPag.oPRIPAGCA_2_13.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPRIPAGMA_2_14.RadioValue()==this.w_PRIPAGMA)
      this.oPgFrm.Page2.oPag.oPRIPAGMA_2_14.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oNDOCIN_2_15.value==this.w_NDOCIN)
      this.oPgFrm.Page2.oPag.oNDOCIN_2_15.value=this.w_NDOCIN
    endif
    if not(this.oPgFrm.Page2.oPag.oADOCIN_2_16.value==this.w_ADOCIN)
      this.oPgFrm.Page2.oPag.oADOCIN_2_16.value=this.w_ADOCIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDDOCIN_2_17.value==this.w_DDOCIN)
      this.oPgFrm.Page2.oPag.oDDOCIN_2_17.value=this.w_DDOCIN
    endif
    if not(this.oPgFrm.Page2.oPag.oNDOCFI_2_18.value==this.w_NDOCFI)
      this.oPgFrm.Page2.oPag.oNDOCFI_2_18.value=this.w_NDOCFI
    endif
    if not(this.oPgFrm.Page2.oPag.oADOCFI_2_19.value==this.w_ADOCFI)
      this.oPgFrm.Page2.oPag.oADOCFI_2_19.value=this.w_ADOCFI
    endif
    if not(this.oPgFrm.Page2.oPag.oDDOCFI_2_20.value==this.w_DDOCFI)
      this.oPgFrm.Page2.oPag.oDDOCFI_2_20.value=this.w_DDOCFI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVAL_1_27.value==this.w_CODVAL)
      this.oPgFrm.Page1.oPag.oCODVAL_1_27.value=this.w_CODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oVADESCRI_1_28.value==this.w_VADESCRI)
      this.oPgFrm.Page1.oPag.oVADESCRI_1_28.value=this.w_VADESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oSSDESCRI_2_30.value==this.w_SSDESCRI)
      this.oPgFrm.Page2.oPag.oSSDESCRI_2_30.value=this.w_SSDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oCDESCRI_2_31.value==this.w_CDESCRI)
      this.oPgFrm.Page2.oPag.oCDESCRI_2_31.value=this.w_CDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oFDESCRI_2_32.value==this.w_FDESCRI)
      this.oPgFrm.Page2.oPag.oFDESCRI_2_32.value=this.w_FDESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oCTDESCRI_2_35.value==this.w_CTDESCRI)
      this.oPgFrm.Page2.oPag.oCTDESCRI_2_35.value=this.w_CTDESCRI
    endif
    if not(this.oPgFrm.Page3.oPag.oDESCPAGA_3_5.value==this.w_DESCPAGA)
      this.oPgFrm.Page3.oPag.oDESCPAGA_3_5.value=this.w_DESCPAGA
    endif
    if not(this.oPgFrm.Page1.oPag.oCFDATCRE_1_33.value==this.w_CFDATCRE)
      this.oPgFrm.Page1.oPag.oCFDATCRE_1_33.value=this.w_CFDATCRE
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSEVA_1_39.RadioValue()==this.w_FLSEVA)
      this.oPgFrm.Page1.oPag.oFLSEVA_1_39.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_DATINI)) or not(.w_Datini >= i_datsys))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_15.SetFocus()
            i_bnoObbl = !empty(.w_DATINI)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale maggiore di data finale oppure data iniziale inferiore alla data di sistema")
          case   ((empty(.w_DATFIN)) or not(.w_Datini<=.w_Datfin))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_16.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data iniziale maggiore della data finale")
          case   (empty(.w_DATCAM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATCAM_1_18.SetFocus()
            i_bnoObbl = !empty(.w_DATCAM)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PERIMP<=100 AND .w_PERIMP>=0)  and (.w_TIPEFF='S')
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oPERIMP_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_TIPOVISUA = this.w_TIPOVISUA
    this.o_PAGRD = this.w_PAGRD
    this.o_PAGRI = this.w_PAGRI
    this.o_PAGBO = this.w_PAGBO
    this.o_PAGCA = this.w_PAGCA
    this.o_PAGRB = this.w_PAGRB
    this.o_PAGMA = this.w_PAGMA
    this.o_TIPCER = this.w_TIPCER
    this.o_TIPEFF = this.w_TIPEFF
    this.o_FLSCAD = this.w_FLSCAD
    this.o_TIPATT = this.w_TIPATT
    this.o_SALIVAC = this.w_SALIVAC
    this.o_SALIVAT = this.w_SALIVAT
    this.o_DATINI = this.w_DATINI
    this.o_SELEZI = this.w_SELEZI
    this.o_TIPCLI = this.w_TIPCLI
    this.o_TIPFOR = this.w_TIPFOR
    this.o_TIPOPAG = this.w_TIPOPAG
    return

enddefine

* --- Define pages as container
define class tgstevkcfPag1 as StdContainer
  Width  = 759
  height = 541
  stdWidth  = 759
  stdheight = 541
  resizeXpos=401
  resizeYpos=353
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oTIPOVISUA_1_1 as StdCombo with uid="SOKKIXRVYA",rtseq=1,rtrep=.f.,left=143,top=16,width=125,height=22;
    , HelpContextID = 148971163;
    , cFormVar="w_TIPOVISUA",RowSource=""+"Conto banca/cassa,"+"Pagamento", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPOVISUA_1_1.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'P',;
    space(1))))
  endfunc
  func oTIPOVISUA_1_1.GetRadio()
    this.Parent.oContained.w_TIPOVISUA = this.RadioValue()
    return .t.
  endfunc

  func oTIPOVISUA_1_1.SetRadio()
    this.Parent.oContained.w_TIPOVISUA=trim(this.Parent.oContained.w_TIPOVISUA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPOVISUA=='C',1,;
      iif(this.Parent.oContained.w_TIPOVISUA=='P',2,;
      0))
  endfunc

  add object oPAGRD_1_2 as StdCheck with uid="YXPZLKEHVK",rtseq=2,rtrep=.f.,left=74, top=49, caption="Rimessa diretta",;
    ToolTipText = "Se attivo filtra pagamenti rimessa diretta",;
    HelpContextID = 20741642,;
    cFormVar="w_PAGRD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRD_1_2.RadioValue()
    return(iif(this.value =1,'RD',;
    ' '))
  endfunc
  func oPAGRD_1_2.GetRadio()
    this.Parent.oContained.w_PAGRD = this.RadioValue()
    return .t.
  endfunc

  func oPAGRD_1_2.SetRadio()
    this.Parent.oContained.w_PAGRD=trim(this.Parent.oContained.w_PAGRD)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRD=='RD',1,;
      0)
  endfunc

  add object oPAGRI_1_3 as StdCheck with uid="QOFHPAWNSJ",rtseq=3,rtrep=.f.,left=245, top=49, caption="R.I.D.",;
    ToolTipText = "Se attivo filtra pagamenti R.I.D.",;
    HelpContextID = 15498762,;
    cFormVar="w_PAGRI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRI_1_3.RadioValue()
    return(iif(this.value =1,'RI',;
    ' '))
  endfunc
  func oPAGRI_1_3.GetRadio()
    this.Parent.oContained.w_PAGRI = this.RadioValue()
    return .t.
  endfunc

  func oPAGRI_1_3.SetRadio()
    this.Parent.oContained.w_PAGRI=trim(this.Parent.oContained.w_PAGRI)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRI=='RI',1,;
      0)
  endfunc

  add object oPAGBO_1_4 as StdCheck with uid="LNFVRFNFDE",rtseq=4,rtrep=.f.,left=74, top=71, caption="Bonifico",;
    ToolTipText = "Se attivo filtra pagamenti bonifico",;
    HelpContextID = 10255882,;
    cFormVar="w_PAGBO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGBO_1_4.RadioValue()
    return(iif(this.value =1,'BO',;
    ' '))
  endfunc
  func oPAGBO_1_4.GetRadio()
    this.Parent.oContained.w_PAGBO = this.RadioValue()
    return .t.
  endfunc

  func oPAGBO_1_4.SetRadio()
    this.Parent.oContained.w_PAGBO=trim(this.Parent.oContained.w_PAGBO)
    this.value = ;
      iif(this.Parent.oContained.w_PAGBO=='BO',1,;
      0)
  endfunc

  add object oPAGCA_1_5 as StdCheck with uid="FXFINXWFBL",rtseq=5,rtrep=.f.,left=245, top=71, caption="Cambiale",;
    ToolTipText = "Se attivo filtra pagamenti cambiale",;
    HelpContextID = 24870410,;
    cFormVar="w_PAGCA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGCA_1_5.RadioValue()
    return(iif(this.value =1,'CA',;
    ' '))
  endfunc
  func oPAGCA_1_5.GetRadio()
    this.Parent.oContained.w_PAGCA = this.RadioValue()
    return .t.
  endfunc

  func oPAGCA_1_5.SetRadio()
    this.Parent.oContained.w_PAGCA=trim(this.Parent.oContained.w_PAGCA)
    this.value = ;
      iif(this.Parent.oContained.w_PAGCA=='CA',1,;
      0)
  endfunc

  add object oPAGRB_1_6 as StdCheck with uid="KQBHYVCHXH",rtseq=6,rtrep=.f.,left=74, top=93, caption="Ric.bancaria/Ri.Ba.",;
    ToolTipText = "Se attivo filtra pagamenti ric.bancaria",;
    HelpContextID = 22838794,;
    cFormVar="w_PAGRB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGRB_1_6.RadioValue()
    return(iif(this.value =1,'RB',;
    ' '))
  endfunc
  func oPAGRB_1_6.GetRadio()
    this.Parent.oContained.w_PAGRB = this.RadioValue()
    return .t.
  endfunc

  func oPAGRB_1_6.SetRadio()
    this.Parent.oContained.w_PAGRB=trim(this.Parent.oContained.w_PAGRB)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRB=='RB',1,;
      0)
  endfunc

  add object oPAGMA_1_7 as StdCheck with uid="JERCZZLYHZ",rtseq=7,rtrep=.f.,left=245, top=93, caption="M.AV.",;
    ToolTipText = "Se attivo filtra pagamenti M.AV.",;
    HelpContextID = 24215050,;
    cFormVar="w_PAGMA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAGMA_1_7.RadioValue()
    return(iif(this.value =1,'MA',;
    ' '))
  endfunc
  func oPAGMA_1_7.GetRadio()
    this.Parent.oContained.w_PAGMA = this.RadioValue()
    return .t.
  endfunc

  func oPAGMA_1_7.SetRadio()
    this.Parent.oContained.w_PAGMA=trim(this.Parent.oContained.w_PAGMA)
    this.value = ;
      iif(this.Parent.oContained.w_PAGMA=='MA',1,;
      0)
  endfunc

  add object oTIPCER_1_8 as StdCheck with uid="WAVDVSMHDV",rtseq=8,rtrep=.f.,left=74, top=120, caption="Certo",;
    ToolTipText = "Dati derivanti da distinte effetti contabilizzabili non ancora contabilizzate",;
    HelpContextID = 12917302,;
    cFormVar="w_TIPCER", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTIPCER_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTIPCER_1_8.GetRadio()
    this.Parent.oContained.w_TIPCER = this.RadioValue()
    return .t.
  endfunc

  func oTIPCER_1_8.SetRadio()
    this.Parent.oContained.w_TIPCER=trim(this.Parent.oContained.w_TIPCER)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCER=='S',1,;
      0)
  endfunc

  add object oTIPEFF_1_9 as StdCheck with uid="BVXSSKGXKD",rtseq=9,rtrep=.f.,left=74, top=142, caption="Effettivo",;
    ToolTipText = "Dati derivanti da partite associate a primanota e a scadenze diverse",;
    HelpContextID = 81205814,;
    cFormVar="w_TIPEFF", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTIPEFF_1_9.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTIPEFF_1_9.GetRadio()
    this.Parent.oContained.w_TIPEFF = this.RadioValue()
    return .t.
  endfunc

  func oTIPEFF_1_9.SetRadio()
    this.Parent.oContained.w_TIPEFF=trim(this.Parent.oContained.w_TIPEFF)
    this.value = ;
      iif(this.Parent.oContained.w_TIPEFF=='S',1,;
      0)
  endfunc

  add object oFLSCAD_1_10 as StdCheck with uid="OBDFZACRKB",rtseq=10,rtrep=.f.,left=245, top=142, caption="Considera scaduto",;
    ToolTipText = "Se attivo verranno considerate anche le partite scadute",;
    HelpContextID = 42290262,;
    cFormVar="w_FLSCAD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSCAD_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLSCAD_1_10.GetRadio()
    this.Parent.oContained.w_FLSCAD = this.RadioValue()
    return .t.
  endfunc

  func oFLSCAD_1_10.SetRadio()
    this.Parent.oContained.w_FLSCAD=trim(this.Parent.oContained.w_FLSCAD)
    this.value = ;
      iif(this.Parent.oContained.w_FLSCAD=='S',1,;
      0)
  endfunc

  func oFLSCAD_1_10.mHide()
    with this.Parent.oContained
      return (.w_TIPEFF<>'S')
    endwith
  endfunc

  add object oTIPATT_1_11 as StdCheck with uid="ZNARTWEVGE",rtseq=11,rtrep=.f.,left=74, top=165, caption="Atteso",;
    ToolTipText = "Dati derivanti da rate scadenze associate a documenti",;
    HelpContextID = 62069302,;
    cFormVar="w_TIPATT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTIPATT_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oTIPATT_1_11.GetRadio()
    this.Parent.oContained.w_TIPATT = this.RadioValue()
    return .t.
  endfunc

  func oTIPATT_1_11.SetRadio()
    this.Parent.oContained.w_TIPATT=trim(this.Parent.oContained.w_TIPATT)
    this.value = ;
      iif(this.Parent.oContained.w_TIPATT=='S',1,;
      0)
  endfunc

  add object oDATINI_1_15 as StdField with uid="PMTTMHPITH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale maggiore di data finale oppure data iniziale inferiore alla data di sistema",;
    ToolTipText = "Data inizio selezione",;
    HelpContextID = 140202294,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=576, Top=49

  func oDATINI_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Datini >= i_datsys)
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_16 as StdField with uid="OTCGWMGFYS",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data iniziale maggiore della data finale",;
    ToolTipText = "Data fine selezione",;
    HelpContextID = 218648886,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=679, Top=49

  func oDATFIN_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_Datini<=.w_Datfin)
    endwith
    return bRes
  endfunc


  add object oTIPPER_1_17 as StdCombo with uid="LXNCDCDAHL",rtseq=16,rtrep=.f.,left=576,top=84,width=147,height=21;
    , ToolTipText = "Periodo di raggruppamento selezionato";
    , HelpContextID = 13769270;
    , cFormVar="w_TIPPER",RowSource=""+"Giornaliero,"+"Settimanale,"+"Quindicinale,"+"Mensile", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPPER_1_17.RadioValue()
    return(iif(this.value =1,'G',;
    iif(this.value =2,'S',;
    iif(this.value =3,'Q',;
    iif(this.value =4,'M',;
    space(1))))))
  endfunc
  func oTIPPER_1_17.GetRadio()
    this.Parent.oContained.w_TIPPER = this.RadioValue()
    return .t.
  endfunc

  func oTIPPER_1_17.SetRadio()
    this.Parent.oContained.w_TIPPER=trim(this.Parent.oContained.w_TIPPER)
    this.value = ;
      iif(this.Parent.oContained.w_TIPPER=='G',1,;
      iif(this.Parent.oContained.w_TIPPER=='S',2,;
      iif(this.Parent.oContained.w_TIPPER=='Q',3,;
      iif(this.Parent.oContained.w_TIPPER=='M',4,;
      0))))
  endfunc

  add object oDATCAM_1_18 as StdField with uid="DWFDXSPGRM",rtseq=17,rtrep=.f.,;
    cFormVar = "w_DATCAM", cQueryName = "DATCAM",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data dalla quale inizia la ricerca dei cambi giornalieri (viene preso il cambio pi� recente a partire dalla data impostata)",;
    HelpContextID = 193286454,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=576, Top=115

  add object oSELEZI_1_22 as StdRadio with uid="RDFUYVXOXW",rtseq=18,rtrep=.f.,left=34, top=508, width=140,height=35;
    , ToolTipText = "Seleziona/deseleziona tutti i conti banca";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_22.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutto"
      this.Buttons(1).HelpContextID = 152491558
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutto"
      this.Buttons(2).HelpContextID = 152491558
      this.Buttons(2).Top=16
      this.SetAll("Width",138)
      this.SetAll("Height",18)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona tutti i conti banca")
      StdRadio::init()
    endproc

  func oSELEZI_1_22.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_22.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_22.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object Zoomcf as cp_szoombox with uid="IWTIQPTJGQ",left=25, top=240, width=728,height=264,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSTE_KCF",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.t.,cTable="COC_MAST",cMenuFile="",cZoomOnZoom="",bQueryOnDblClick=.f.,bNoZoomGridShape=.f.,;
    cEvent = "Blank",;
    nPag=1;
    , HelpContextID = 80270054


  add object oObj_1_24 as cp_runprogram with uid="HWXSNDJZQK",left=-2, top=577, width=237,height=18,;
    caption='GSTE_BSB',;
   bGlobalFont=.t.,;
    prg="GSTE_BSB",;
    cEvent = "w_SELEZI Changed,Blank",;
    nPag=1;
    , HelpContextID = 40330152

  add object oCODVAL_1_27 as StdField with uid="TMMMDEZAEX",rtseq=40,rtrep=.f.,;
    cFormVar = "w_CODVAL", cQueryName = "CODVAL",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta, se vuota tutte",;
    HelpContextID = 177692454,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=441, Top=179, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_CODVAL"

  func oCODVAL_1_27.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_27('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVAL_1_27.ecpDrop(oSource)
    this.Parent.oContained.link_1_27('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVAL_1_27.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCODVAL_1_27'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oCODVAL_1_27.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_CODVAL
     i_obj.ecpSave()
  endproc

  add object oVADESCRI_1_28 as StdField with uid="GFEMTIBHLK",rtseq=41,rtrep=.f.,;
    cFormVar = "w_VADESCRI", cQueryName = "VADESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione valuta",;
    HelpContextID = 44454559,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=498, Top=179, InputMask=replicate('X',35)


  add object oObj_1_30 as cp_runprogram with uid="CYGGSXSLII",left=-2, top=598, width=237,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSTE_BVF('Elabora')",;
    cEvent = "ActivatePage 3",;
    nPag=1;
    , HelpContextID = 80270054


  add object oObj_1_31 as cp_runprogram with uid="DSBENUYXUW",left=-2, top=619, width=237,height=18,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSTE_BVF('Drop')",;
    cEvent = "ActivatePage 1,ActivatePage 2,Done",;
    nPag=1;
    , HelpContextID = 80270054

  add object oCFDATCRE_1_33 as StdField with uid="XBGXKLRWXX",rtseq=57,rtrep=.f.,;
    cFormVar = "w_CFDATCRE", cQueryName = "CFDATCRE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di aggiornamento storico",;
    HelpContextID = 45241963,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=441, Top=209

  add object oFLSEVA_1_39 as StdCheck with uid="UWFTWYUGVD",rtseq=59,rtrep=.f.,left=539, top=214, caption="Evasione a data scadenza", enabled=.f.,;
    ToolTipText = "Se attivo verranno evase le rate in ordine di scadenza invece che proporzionalmente per il cash flow atteso",;
    HelpContextID = 14109782,;
    cFormVar="w_FLSEVA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLSEVA_1_39.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLSEVA_1_39.GetRadio()
    this.Parent.oContained.w_FLSEVA = this.RadioValue()
    return .t.
  endfunc

  func oFLSEVA_1_39.SetRadio()
    this.Parent.oContained.w_FLSEVA=trim(this.Parent.oContained.w_FLSEVA)
    this.value = ;
      iif(this.Parent.oContained.w_FLSEVA=='S',1,;
      0)
  endfunc

  func oFLSEVA_1_39.mHide()
    with this.Parent.oContained
      return (.w_TIPATT<>'S')
    endwith
  endfunc

  add object oStr_1_14 as StdString with uid="SCBVFRZZEG",Visible=.t., Left=29, Top=120,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="SMODERLOEY",Visible=.t., Left=535, Top=49,;
    Alignment=1, Width=34, Height=18,;
    Caption="Dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="WSESJLGWYK",Visible=.t., Left=653, Top=49,;
    Alignment=1, Width=23, Height=18,;
    Caption="Al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="QPXBZBNSLQ",Visible=.t., Left=513, Top=84,;
    Alignment=1, Width=56, Height=18,;
    Caption="Periodo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="STVTDNLYIV",Visible=.t., Left=1, Top=49,;
    Alignment=1, Width=67, Height=18,;
    Caption="Pagamenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="HCEVWHRVQD",Visible=.t., Left=342, Top=180,;
    Alignment=1, Width=96, Height=18,;
    Caption="Codice valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="WWNIOHONPG",Visible=.t., Left=3, Top=19,;
    Alignment=1, Width=133, Height=18,;
    Caption="Visualizzazione per:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="WZQCIXDFOL",Visible=.t., Left=444, Top=117,;
    Alignment=1, Width=125, Height=15,;
    Caption="Applica cambi del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="TRIBKYHAXY",Visible=.t., Left=231, Top=213,;
    Alignment=1, Width=207, Height=18,;
    Caption="Ultima data elaborazione storico:"  ;
  , bGlobalFont=.t.
enddefine
define class tgstevkcfPag2 as StdContainer
  Width  = 759
  height = 541
  stdWidth  = 759
  stdheight = 541
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTIPCLI_2_1 as StdCheck with uid="IRJWPVXOBO",rtseq=19,rtrep=.f.,left=104, top=38, caption="Clienti",;
    ToolTipText = "Se attivo filtra gli intestatari clienti",;
    HelpContextID = 137697846,;
    cFormVar="w_TIPCLI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTIPCLI_2_1.RadioValue()
    return(iif(this.value =1,'C',;
    ' '))
  endfunc
  func oTIPCLI_2_1.GetRadio()
    this.Parent.oContained.w_TIPCLI = this.RadioValue()
    return .t.
  endfunc

  func oTIPCLI_2_1.SetRadio()
    this.Parent.oContained.w_TIPCLI=trim(this.Parent.oContained.w_TIPCLI)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCLI=='C',1,;
      0)
  endfunc

  func oTIPCLI_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCLI)
        bRes2=.link_2_2('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODCLI_2_2 as StdField with uid="DRRQATAJPY",rtseq=20,rtrep=.f.,;
    cFormVar = "w_CODCLI", cQueryName = "CODCLI",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso",;
    ToolTipText = "Codice cliente",;
    HelpContextID = 137649958,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=191, Top=38, cSayPict="p_CLI", cGetPict="p_CLI", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCLI", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCLI"

  func oCODCLI_2_2.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCLI='C')
    endwith
   endif
  endfunc

  func oCODCLI_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCLI_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCLI_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCLI)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCLI_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco clienti",'gsar_scl.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCLI_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCLI
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCLI
     i_obj.ecpSave()
  endproc

  add object oTIPFOR_2_3 as StdCheck with uid="ETIYEZTRXP",rtseq=21,rtrep=.f.,left=104, top=70, caption="Fornitori",;
    ToolTipText = "Se attivo filtra gli intestatari fornitori",;
    HelpContextID = 23599670,;
    cFormVar="w_TIPFOR", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTIPFOR_2_3.RadioValue()
    return(iif(this.value =1,'F',;
    ' '))
  endfunc
  func oTIPFOR_2_3.GetRadio()
    this.Parent.oContained.w_TIPFOR = this.RadioValue()
    return .t.
  endfunc

  func oTIPFOR_2_3.SetRadio()
    this.Parent.oContained.w_TIPFOR=trim(this.Parent.oContained.w_TIPFOR)
    this.value = ;
      iif(this.Parent.oContained.w_TIPFOR=='F',1,;
      0)
  endfunc

  func oTIPFOR_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODFOR)
        bRes2=.link_2_4('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODFOR_2_4 as StdField with uid="JFSPSQZVIK",rtseq=22,rtrep=.f.,;
    cFormVar = "w_CODFOR", cQueryName = "CODFOR",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore non ammesso",;
    ToolTipText = "Codice fornitore",;
    HelpContextID = 23551782,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=191, Top=70, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPFOR", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODFOR"

  func oCODFOR_2_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPFOR='F')
    endwith
   endif
  endfunc

  func oCODFOR_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFOR_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFOR_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPFOR)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPFOR)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODFOR_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Elenco fornitori",'gsar_sfr.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODFOR_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPFOR
     i_obj.w_ANCODICE=this.parent.oContained.w_CODFOR
     i_obj.ecpSave()
  endproc

  add object oTIPGEN_2_5 as StdCheck with uid="KFTABRHETU",rtseq=23,rtrep=.f.,left=104, top=98, caption="Altro",;
    ToolTipText = "Se attivo filtra i conti generici",;
    HelpContextID = 214506038,;
    cFormVar="w_TIPGEN", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oTIPGEN_2_5.RadioValue()
    return(iif(this.value =1,'G',;
    ' '))
  endfunc
  func oTIPGEN_2_5.GetRadio()
    this.Parent.oContained.w_TIPGEN = this.RadioValue()
    return .t.
  endfunc

  func oTIPGEN_2_5.SetRadio()
    this.Parent.oContained.w_TIPGEN=trim(this.Parent.oContained.w_TIPGEN)
    this.value = ;
      iif(this.Parent.oContained.w_TIPGEN=='G',1,;
      0)
  endfunc

  add object oCATECOMM_2_6 as StdField with uid="FNNUKUNBXH",rtseq=24,rtrep=.f.,;
    cFormVar = "w_CATECOMM", cQueryName = "CATECOMM",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Categoria commerciale selezionata",;
    HelpContextID = 39366285,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=191, Top=141, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="CATECOMM", cZoomOnZoom="GSAR_ACT", oKey_1_1="CTCODICE", oKey_1_2="this.w_CATECOMM"

  func oCATECOMM_2_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCATECOMM_2_6.ecpDrop(oSource)
    this.Parent.oContained.link_2_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCATECOMM_2_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CATECOMM','*','CTCODICE',cp_AbsName(this.parent,'oCATECOMM_2_6'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACT',"Categorie commerciali",'',this.parent.oContained
  endproc
  proc oCATECOMM_2_6.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CTCODICE=this.parent.oContained.w_CATECOMM
     i_obj.ecpSave()
  endproc

  add object oSMOSCA_2_7 as StdField with uid="JNDJMONUUV",rtseq=25,rtrep=.f.,;
    cFormVar = "w_SMOSCA", cQueryName = "SMOSCA",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Smobilizzo scaduto",;
    HelpContextID = 263523878,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=191, Top=185, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="SMOBSCAM", cZoomOnZoom="GSTE_MSS", oKey_1_1="SSCODICE", oKey_1_2="this.w_SMOSCA"

  func oSMOSCA_2_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPEFF='S' And .w_FLSCAD='S')
    endwith
   endif
  endfunc

  func oSMOSCA_2_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_7('Part',this)
    endwith
    return bRes
  endfunc

  proc oSMOSCA_2_7.ecpDrop(oSource)
    this.Parent.oContained.link_2_7('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSMOSCA_2_7.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SMOBSCAM','*','SSCODICE',cp_AbsName(this.parent,'oSMOSCA_2_7'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_MSS',"Smobilizzo scaduto",'',this.parent.oContained
  endproc
  proc oSMOSCA_2_7.mZoomOnZoom
    local i_obj
    i_obj=GSTE_MSS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SSCODICE=this.parent.oContained.w_SMOSCA
     i_obj.ecpSave()
  endproc

  add object oPERIMP_2_8 as StdField with uid="LPBWZWVPHS",rtseq=26,rtrep=.f.,;
    cFormVar = "w_PERIMP", cQueryName = "PERIMP",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    HelpContextID = 256587254,;
   bGlobalFont=.t.,;
    Height=21, Width=55, Left=191, Top=242, cSayPict='"999.99"', cGetPict='"999.99"'

  func oPERIMP_2_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPEFF='S')
    endwith
   endif
  endfunc

  func oPERIMP_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PERIMP<=100 AND .w_PERIMP>=0)
    endwith
    return bRes
  endfunc

  add object oPRIPAGRD_2_9 as StdCheck with uid="EDJFVWBCPG",rtseq=27,rtrep=.f.,left=286, top=229, caption="Rimessa diretta",;
    ToolTipText = "Se attivo, la percentuale di riduzione importo � applicata al pagamento",;
    HelpContextID = 93434682,;
    cFormVar="w_PRIPAGRD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPRIPAGRD_2_9.RadioValue()
    return(iif(this.value =1,'RD',;
    ' '))
  endfunc
  func oPRIPAGRD_2_9.GetRadio()
    this.Parent.oContained.w_PRIPAGRD = this.RadioValue()
    return .t.
  endfunc

  func oPRIPAGRD_2_9.SetRadio()
    this.Parent.oContained.w_PRIPAGRD=trim(this.Parent.oContained.w_PRIPAGRD)
    this.value = ;
      iif(this.Parent.oContained.w_PRIPAGRD=='RD',1,;
      0)
  endfunc

  add object oPRIPAGRI_2_10 as StdCheck with uid="PVKAPIHWOZ",rtseq=28,rtrep=.f.,left=459, top=229, caption="R.I.D.",;
    ToolTipText = "Se attivo, la percentuale di riduzione importo � applicata al pagamento",;
    HelpContextID = 93434687,;
    cFormVar="w_PRIPAGRI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPRIPAGRI_2_10.RadioValue()
    return(iif(this.value =1,'RI',;
    ' '))
  endfunc
  func oPRIPAGRI_2_10.GetRadio()
    this.Parent.oContained.w_PRIPAGRI = this.RadioValue()
    return .t.
  endfunc

  func oPRIPAGRI_2_10.SetRadio()
    this.Parent.oContained.w_PRIPAGRI=trim(this.Parent.oContained.w_PRIPAGRI)
    this.value = ;
      iif(this.Parent.oContained.w_PRIPAGRI=='RI',1,;
      0)
  endfunc

  add object oPRIPAGBO_2_11 as StdCheck with uid="EXWUWIJVKP",rtseq=29,rtrep=.f.,left=573, top=229, caption="Bonifico",;
    ToolTipText = "Se attivo, la percentuale di riduzione importo � applicata al pagamento",;
    HelpContextID = 93434693,;
    cFormVar="w_PRIPAGBO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPRIPAGBO_2_11.RadioValue()
    return(iif(this.value =1,'BO',;
    ' '))
  endfunc
  func oPRIPAGBO_2_11.GetRadio()
    this.Parent.oContained.w_PRIPAGBO = this.RadioValue()
    return .t.
  endfunc

  func oPRIPAGBO_2_11.SetRadio()
    this.Parent.oContained.w_PRIPAGBO=trim(this.Parent.oContained.w_PRIPAGBO)
    this.value = ;
      iif(this.Parent.oContained.w_PRIPAGBO=='BO',1,;
      0)
  endfunc

  add object oPRIPAGRB_2_12 as StdCheck with uid="FQHWGHPVEM",rtseq=30,rtrep=.f.,left=286, top=256, caption="Ric.bancaria/Ri.Ba.",;
    ToolTipText = "Se attivo, la percentuale di riduzione importo � applicata al pagamento",;
    HelpContextID = 93434680,;
    cFormVar="w_PRIPAGRB", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPRIPAGRB_2_12.RadioValue()
    return(iif(this.value =1,'RB',;
    ' '))
  endfunc
  func oPRIPAGRB_2_12.GetRadio()
    this.Parent.oContained.w_PRIPAGRB = this.RadioValue()
    return .t.
  endfunc

  func oPRIPAGRB_2_12.SetRadio()
    this.Parent.oContained.w_PRIPAGRB=trim(this.Parent.oContained.w_PRIPAGRB)
    this.value = ;
      iif(this.Parent.oContained.w_PRIPAGRB=='RB',1,;
      0)
  endfunc

  add object oPRIPAGCA_2_13 as StdCheck with uid="CILGOOTHRN",rtseq=31,rtrep=.f.,left=459, top=256, caption="Cambiale",;
    ToolTipText = "Se attivo, la percentuale di riduzione importo � applicata al pagamento",;
    HelpContextID = 93434679,;
    cFormVar="w_PRIPAGCA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPRIPAGCA_2_13.RadioValue()
    return(iif(this.value =1,'CA',;
    ' '))
  endfunc
  func oPRIPAGCA_2_13.GetRadio()
    this.Parent.oContained.w_PRIPAGCA = this.RadioValue()
    return .t.
  endfunc

  func oPRIPAGCA_2_13.SetRadio()
    this.Parent.oContained.w_PRIPAGCA=trim(this.Parent.oContained.w_PRIPAGCA)
    this.value = ;
      iif(this.Parent.oContained.w_PRIPAGCA=='CA',1,;
      0)
  endfunc

  add object oPRIPAGMA_2_14 as StdCheck with uid="VAPUJMETCG",rtseq=32,rtrep=.f.,left=573, top=256, caption="M.AV.",;
    ToolTipText = "Se attivo, la percentuale di riduzione importo � applicata al pagamento",;
    HelpContextID = 175000777,;
    cFormVar="w_PRIPAGMA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPRIPAGMA_2_14.RadioValue()
    return(iif(this.value =1,'MA',;
    ' '))
  endfunc
  func oPRIPAGMA_2_14.GetRadio()
    this.Parent.oContained.w_PRIPAGMA = this.RadioValue()
    return .t.
  endfunc

  func oPRIPAGMA_2_14.SetRadio()
    this.Parent.oContained.w_PRIPAGMA=trim(this.Parent.oContained.w_PRIPAGMA)
    this.value = ;
      iif(this.Parent.oContained.w_PRIPAGMA=='MA',1,;
      0)
  endfunc

  add object oNDOCIN_2_15 as StdField with uid="ZBNZTFUKTZ",rtseq=33,rtrep=.f.,;
    cFormVar = "w_NDOCIN", cQueryName = "",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Num. documento iniziale",;
    HelpContextID = 218432726,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=191, Top=307, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oADOCIN_2_16 as StdField with uid="IQDHLHPBVF",rtseq=34,rtrep=.f.,;
    cFormVar = "w_ADOCIN", cQueryName = "ADOCIN",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento iniziale",;
    HelpContextID = 218432518,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=348, Top=307, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oDDOCIN_2_17 as StdField with uid="KTUFDNNKXZ",rtseq=35,rtrep=.f.,;
    cFormVar = "w_DDOCIN", cQueryName = "",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data emissione del documento iniziale",;
    HelpContextID = 218432566,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=471, Top=312

  add object oNDOCFI_2_18 as StdField with uid="GDBIXIPPHU",rtseq=36,rtrep=.f.,;
    cFormVar = "w_NDOCFI", cQueryName = "",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Num.documento finale",;
    HelpContextID = 131400918,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=191, Top=340, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oADOCFI_2_19 as StdField with uid="HFHATSWFWM",rtseq=37,rtrep=.f.,;
    cFormVar = "w_ADOCFI", cQueryName = "ADOCFI",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento finale",;
    HelpContextID = 131400710,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=348, Top=340, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oDDOCFI_2_20 as StdField with uid="VNSTMBLDUE",rtseq=38,rtrep=.f.,;
    cFormVar = "w_DDOCFI", cQueryName = "",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data emissione del documento finale",;
    HelpContextID = 131400758,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=471, Top=340

  add object oSSDESCRI_2_30 as StdField with uid="TIBTPTARDZ",rtseq=42,rtrep=.f.,;
    cFormVar = "w_SSDESCRI", cQueryName = "SSDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(254), bMultilanguage =  .f.,;
    HelpContextID = 44459119,;
   bGlobalFont=.t.,;
    Height=21, Width=383, Left=328, Top=185, InputMask=replicate('X',254)

  add object oCDESCRI_2_31 as StdField with uid="PNEILFOEAK",rtseq=43,rtrep=.f.,;
    cFormVar = "w_CDESCRI", cQueryName = "CDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione cliente",;
    HelpContextID = 256613338,;
   bGlobalFont=.t.,;
    Height=21, Width=338, Left=328, Top=38, InputMask=replicate('X',40)

  add object oFDESCRI_2_32 as StdField with uid="PRILNFCFVV",rtseq=44,rtrep=.f.,;
    cFormVar = "w_FDESCRI", cQueryName = "FDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione fornitore",;
    HelpContextID = 256613290,;
   bGlobalFont=.t.,;
    Height=21, Width=338, Left=328, Top=70, InputMask=replicate('X',40)

  add object oCTDESCRI_2_35 as StdField with uid="GLFMHNDYEI",rtseq=45,rtrep=.f.,;
    cFormVar = "w_CTDESCRI", cQueryName = "CTDESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 44459119,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=328, Top=141, InputMask=replicate('X',35)

  add object oStr_2_21 as StdString with uid="DKPVJOYCGC",Visible=.t., Left=67, Top=307,;
    Alignment=1, Width=112, Height=18,;
    Caption="Da n.documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_22 as StdString with uid="XCVVRPDOFL",Visible=.t., Left=82, Top=340,;
    Alignment=1, Width=97, Height=18,;
    Caption="A n. documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_23 as StdString with uid="DLGWUZZOTK",Visible=.t., Left=438, Top=307,;
    Alignment=1, Width=26, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_2_24 as StdString with uid="JGWMBFEVVL",Visible=.t., Left=438, Top=340,;
    Alignment=1, Width=26, Height=18,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_2_25 as StdString with uid="YAAATJHSEK",Visible=.t., Left=330, Top=307,;
    Alignment=2, Width=14, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="BJWEUSWIMR",Visible=.t., Left=330, Top=340,;
    Alignment=2, Width=14, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="QTLQPGDOXA",Visible=.t., Left=17, Top=39,;
    Alignment=1, Width=79, Height=18,;
    Caption="Intestatari:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="NWQZDYMAMR",Visible=.t., Left=53, Top=186,;
    Alignment=1, Width=133, Height=18,;
    Caption="Smobilizzo scaduto:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="LFDQNEWAJD",Visible=.t., Left=0, Top=244,;
    Alignment=1, Width=186, Height=18,;
    Caption="Percentuale riduzione importo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="UAPXMUMKXR",Visible=.t., Left=29, Top=144,;
    Alignment=1, Width=157, Height=18,;
    Caption="Categoria commerciale:"  ;
  , bGlobalFont=.t.
enddefine
define class tgstevkcfPag3 as StdContainer
  Width  = 759
  height = 541
  stdWidth  = 759
  stdheight = 541
  resizeXpos=282
  resizeYpos=368
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object Zoomdetcf as cp_zoombox with uid="SXKANRLHCS",left=0, top=5, width=711,height=517,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSTEVKCF",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="COC_MAST",bQueryOnDblClick=.f.,cZoomOnZoom="",bRetriveAllRows=.f.,cMenuFile="",bNoZoomGridShape=.f.,;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 80270054


  add object Zoomdetpa as cp_zoombox with uid="GVYQRDDEJM",left=0, top=5, width=711,height=251,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSTEVKCF1",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="COC_MAST",bQueryOnDblClick=.f.,cZoomOnZoom="",bRetriveAllRows=.f.,cMenuFile="",bNoZoomGridShape=.f.,;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 80270054

  add object oDESCPAGA_3_5 as StdField with uid="UXAIPQURPU",rtseq=47,rtrep=.f.,;
    cFormVar = "w_DESCPAGA", cQueryName = "DESCPAGA",enabled=.f.,;
    bObbl = .f. , nPag = 3, value=space(25), bMultilanguage =  .f.,;
    HelpContextID = 260749961,;
   bGlobalFont=.t.,;
    Height=21, Width=187, Left=274, Top=263, InputMask=replicate('X',25)

  func oDESCPAGA_3_5.mHide()
    with this.Parent.oContained
      return (.w_Tipovisua='C' Or .w_TipoPag='TO')
    endwith
  endfunc


  add object Zoomdetcb as cp_zoombox with uid="PGGMJHVANB",left=0, top=290, width=711,height=253,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="GSTEVKCF2",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,cTable="COC_MAST",bQueryOnDblClick=.f.,cZoomOnZoom="",bRetriveAllRows=.f.,cMenuFile="",bNoZoomGridShape=.f.,;
    cEvent = "Init",;
    nPag=3;
    , HelpContextID = 80270054


  add object oObj_3_8 as cp_runprogram with uid="VIMSISVUVS",left=0, top=588, width=177,height=27,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSTE_BLD",;
    cEvent = "w_zoomdetcf selected",;
    nPag=3;
    , HelpContextID = 80270054


  add object oObj_3_18 as cp_runprogram with uid="GABLUVLOCH",left=0, top=617, width=177,height=27,;
    caption='Object',;
   bGlobalFont=.t.,;
    prg="GSTE_BLD",;
    cEvent = "w_zoomdetcb selected",;
    nPag=3;
    , HelpContextID = 80270054

  add object oStr_3_4 as StdString with uid="QKGGSLQWMJ",Visible=.t., Left=7, Top=264,;
    Alignment=1, Width=262, Height=19,;
    Caption="Di seguito il dettaglio del pagamento:"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_3_4.mHide()
    with this.Parent.oContained
      return (.w_Tipovisua='C' Or .w_TipoPag='TO')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gstevkcf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gstevkcf
func DataStoricoCF()
  local w_CFDATCRE
  vq_exec('query\gste_kvx',,'CursDataStorico')
  if used('CursDataStorico')
    select CursDataStorico
    w_CFDATCRE=CFDATCRE
    select CursDataStorico
    use
  endif
  w_CFDATCRE=nvl(w_CFDATCRE,cp_chartodate('  -  -    '))
return(w_CFDATCRE)
* --- Fine Area Manuale
