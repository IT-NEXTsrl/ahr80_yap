* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gszm_bin                                                        *
*              Men� contestuale inventari                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2006-10-04                                                      *
* Last revis.: 2006-10-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgszm_bin",oParentObject)
return(i_retval)

define class tgszm_bin as StdBatch
  * --- Local variables
  w_CODINV = space(6)
  w_CODESE = space(4)
  w_OBJECT = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione tasto destro Inventari
    this.w_CODINV = g_oMenu.getValue("INNUMINV")
    this.w_CODESE = g_oMenu.getValue("INCODESE")
    * --- Visualizza schede contabili
    this.w_OBJECT = GSMA_SZI(this)
    if !this.w_OBJECT.bSec1
      i_retcode = 'stop'
      return
    endif
    if Not Empty( this.w_CODESE ) And Not Empty( this.w_CODINV )
      this.w_OBJECT.w_INCODESE = this.w_CODESE
      this.w_OBJECT.w_INNUMINV = this.w_CODINV
      * --- Visualizzo la chiave caricata
      this.w_OBJECT.mCalc(.T.)     
      this.w_OBJECT.mEnableControls()     
      * --- Non avvio la ricerca in modo automatico, l'inventario potrebbe avere
      *     molti articoli...
    endif
    this.w_OBJECT = .NULL.
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
