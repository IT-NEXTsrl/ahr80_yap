* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscf_bcr                                                        *
*              Gestione caricamento righe                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2010-10-08                                                      *
* Last revis.: 2013-09-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscf_bcr",oParentObject,m.pOPER)
return(i_retval)

define class tgscf_bcr as StdBatch
  * --- Local variables
  pOPER = space(10)
  w_PRIM_KEY = space(254)
  w_OBJ_GEST = .NULL.
  w_PADRE = .NULL.
  w_NUMREC = 0
  w_SRC = space(30)
  w_FILTRO = space(10)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione  maschera GSCF_KCR
    this.w_PADRE = this.oParentObject
    do case
      case this.pOPER=="INSERTROWS"
        * --- Inserimento righe nella gestione padre
        this.w_OBJ_GEST = this.w_PADRE.oParentObject
        this.w_OBJ_GEST.w_SHOWALERT = False
        this.w_OBJ_GEST.MarkPos()     
        SELECT ( this.oParentObject.w_ZOOMFLDS.cCursor )
        GO TOP
        SCAN FOR XCHK=1
        * --- Cerco nelle righe se il campo non � gi� stato inserito
        this.w_SRC = "NVL(t_REFLDNAM, SPACE(30) ) = " + cp_ToStrODBC(FLNAME)
        this.w_SRC = this.w_SRC + " AND NVL(t_RE_TABLE, SPACE(30) ) = " + cp_ToStrODBC(TBNAME)
        this.w_SRC = this.w_SRC + " AND NOT DELETED()"
        this.w_NUMREC = this.w_OBJ_GEST.Search(this.w_SRC)
        if this.w_NUMREC<>-1
          this.w_OBJ_GEST.SetRow(this.w_NUMREC)     
          this.w_OBJ_GEST.SaveDependsOn()     
        else
          this.w_OBJ_GEST.AddRow()     
          this.w_OBJ_GEST.SaveDependsOn()     
          this.w_OBJ_GEST.w_RE_TABLE = TBNAME
          this.w_OBJ_GEST.w_MSTDET = IIF(TBNAME=this.w_OBJ_GEST.w_RETABDTL, "D", "M")
          this.w_OBJ_GEST.o_MSTDET = this.w_OBJ_GEST.w_MSTDET
          this.w_OBJ_GEST.w_REFLDNAM = FLNAME
          this.w_OBJ_GEST.w_REFLDCOM = FLCOMMEN
        endif
        this.w_OBJ_GEST.w_REKEYREC = IIF(PrimKey=1, "S", "N" )
        this.w_OBJ_GEST.w_RERIFREC = IIF(RefRec=1, "S", "N" )
        if this.oParentObject.w_FLMONINS<>2
          this.w_OBJ_GEST.w_REMONINS = iif(this.oParentObject.w_FLMONINS=1, "S", "N" )
        endif
        if this.oParentObject.w_FLMONMOD<>"X" and (PrimKey<>1 or this.w_OBJ_GEST.w_MSTDET="D")
          this.w_OBJ_GEST.w_REMONMOD = this.oParentObject.w_FLMONMOD
        endif
        if this.oParentObject.w_FLMONDEL<>2
          this.w_OBJ_GEST.w_REMONDEL = iif(this.oParentObject.w_FLMONDEL=1, "S", "N")
        endif
        if PrimKey=1
          this.w_OBJ_GEST.w_REBLKINS = "N"
        else
          if this.oParentObject.w_FLBLKINS<>2
            this.w_OBJ_GEST.w_REBLKINS = iif(this.oParentObject.w_FLBLKINS=1, "S", "N")
          endif
        endif
        if this.oParentObject.w_FLOBBINS<>2
          this.w_OBJ_GEST.w_REOBBINS = iif(this.oParentObject.w_FLOBBINS=1, "S", "N")
        endif
        if this.w_OBJ_GEST.w_REOBBINS="S" AND (this.w_OBJ_GEST.w_REOBBINS="S" OR this.w_OBJ_GEST.w_REMONINS="S")
          this.w_OBJ_GEST.w_REBLKINS = "N"
        endif
        this.w_OBJ_GEST.w_NPKM = IIF(PrimKey=1 and ALLTRIM(TBNAME)==ALLTRIM(this.oParentObject.w_RETABMST), 1, 0)
        this.w_OBJ_GEST.w_NPKD = IIF(PrimKey=1 and ALLTRIM(TBNAME)==ALLTRIM(this.oParentObject.w_RETABDTL), 1, 0)
        this.w_OBJ_GEST.mCalc(.t.)     
        this.w_OBJ_GEST.SaveRow()     
        SELECT ( this.oParentObject.w_ZOOMFLDS.cCursor )
        ENDSCAN
        Select (this.w_OBJ_GEST.cTrsName)
        SUM(T_NPKM) TO this.w_OBJ_GEST.w_TOTPKM
        SUM(T_NPKD) TO this.w_OBJ_GEST.w_TOTPKD
        * --- TOTALIZZATORI
        this.w_OBJ_GEST.RePos()     
        this.w_OBJ_GEST.mCalc(.T.)     
        this.w_OBJ_GEST.w_SHOWALERT = True
        if this.w_OBJ_GEST.w_TOTROW=>min(this.w_OBJ_GEST.w_WARNROWS,255)
          ah_errormsg(this.w_OBJ_GEST.w_MSGERR)
        endif
      case this.pOPER=="SELEZ" OR this.pOPER=="DESELEZ" OR this.pOPER=="INVSELEZ"
        * --- Seleziona/Deseleziona o inverti selezione
        if USED(this.oParentObject.w_ZOOMFLDS.cCursor)
          SELECT ( this.oParentObject.w_ZOOMFLDS.cCursor )
          UPDATE ( this.oParentObject.w_ZOOMFLDS.cCursor ) SET XCHK=ICASE( this.pOPER=="SELEZ", 1, this.pOPER=="DESELEZ", 0, IIF( XCHK=1, 0 , 1 ) )
          GO TOP
          this.oParentObject.w_ZOOMFLDS.Refresh()     
        endif
      case this.pOPER=="SELPRIMKEY"
        * --- Selezione automatica chiave primaria
        if USED(this.oParentObject.w_ZOOMFLDS.cCursor)
          this.w_PRIM_KEY = i_dcx.getidxdef( this.oParentObject.w_RETABMST ,1)
          SELECT ( this.oParentObject.w_ZOOMFLDS.cCursor )
          UPDATE ( this.oParentObject.w_ZOOMFLDS.cCursor ) SET XCHK=1, PRIMKEY=1 WHERE UPPER(ALLTRIM(FLNAME)) $ this.w_PRIM_KEY
          this.w_PRIM_KEY = i_dcx.getidxdef( this.oParentObject.w_RETABDTL ,1)
          UPDATE ( this.oParentObject.w_ZOOMFLDS.cCursor ) SET XCHK=1, PRIMKEY=1 WHERE UPPER(ALLTRIM(FLNAME)) $ this.w_PRIM_KEY
          GO TOP
        endif
      case this.pOPER=="SINCRO"
        * --- Sincronizza i valori di chiave e riferimento e seleziona i campi gi� inseriti
        if RecCount(this.oParentObject.w_ZOOMFLDS.cCursor)>0
          this.w_OBJ_GEST = this.w_PADRE.oParentObject
          this.w_OBJ_GEST.MarkPos()     
          this.w_OBJ_GEST.FirstRow()     
          do while not this.w_OBJ_GEST.Eof_trs()
            this.w_OBJ_GEST.SetRow()     
            SELECT ( this.oParentObject.w_ZOOMFLDS.cCursor )
            GO TOP
            LOCATE FOR upper(alltrim(TBNAME))==upper(alltrim(this.w_OBJ_GEST.w_RE_TABLE)) AND upper(alltrim(FLNAME))==upper(alltrim(this.w_OBJ_GEST.w_REFLDNAM))
            if FOUND()
              REPLACE XCHK WITH 1
              if this.w_OBJ_GEST.w_REKEYREC="S"
                REPLACE PrimKey WITH 1
              endif
              if this.w_OBJ_GEST.w_RERIFREC="S"
                REPLACE RefRec WITH 1
              endif
            endif
            this.w_OBJ_GEST.NextRow()     
          enddo
          this.w_OBJ_GEST.RePos()     
        endif
        SELECT ( this.oParentObject.w_ZOOMFLDS.cCursor )
        GO TOP
      case this.pOPER=="FILTRO"
        * --- Imposta le condizioni di filtro sullo zoom
        SELECT ( this.oParentObject.w_ZOOMFLDS.cCursor )
        this.w_FILTRO = ""
        if this.oParentObject.w_FILDTL="N" AND NOT EMPTY(this.oParentObject.w_RETABDTL) OR this.oParentObject.w_FILMST="N" AND NOT EMPTY(this.oParentObject.w_RETABMST)
          if this.oParentObject.w_FILDTL="N"
            this.w_FILTRO = "TBNAME='" + this.oParentObject.w_RETABMST + "'"
          else
            this.w_FILTRO = "TBNAME='" + this.oParentObject.w_RETABDTL + "'"
          endif
        endif
        if this.oParentObject.w_FILRECSE<>"T"
          if not empty(this.w_FILTRO)
            this.w_FILTRO = this.w_FILTRO + " AND "
          endif
          this.w_FILTRO = this.w_FILTRO + iif(this.oParentObject.w_FILRECSE="S", "XCHK=1", "XCHK=0")
        endif
        if empty(this.w_FILTRO)
          SET FILTER TO IN (this.oParentObject.w_ZOOMFLDS.cCursor)
        else
          FILTRO=this.w_FILTRO
          SET FILTER TO &FILTRO IN (this.oParentObject.w_ZOOMFLDS.cCursor)
        endif
        GO TOP
        this.oParentObject.w_ZOOMFLDS.Refresh()     
    endcase
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
