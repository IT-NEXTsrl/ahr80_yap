* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bvc                                                        *
*              Controllo variazione commessa                                   *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-12                                                      *
* Last revis.: 2011-06-10                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bvc",oParentObject)
return(i_retval)

define class tgsma_bvc as StdBatch
  * --- Local variables
  w_ERRMSG = space(10)
  w_ERRORE = .f.
  w_GSMA_ADP = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlla la possibilita' di variazione del flag 'Commessa' (da GSMA_AAR)
    * --- Esegue operazione richiesta
    this.w_ERRMSG = ""
    if g_PROD="S"
      this.w_GSMA_ADP = this.oParentObject.GSMA_APR
    endif
    if this.oParentObject.w_ARFLCOMM="S"
      do case
        case this.oParentObject.w_ARTIPGES = "S"
          this.w_ERRMSG = "Impossibile variare il flag:%0%0Gli articoli gestiti <a scorta> non possono essere gestiti <a commessa>"
          this.w_ERRORE = .T.
        case g_PROD="S"
          * --- Controlla se esistono padri non gestiti a commessa
          * --- Select from GSMA1BVC
          do vq_exec with 'GSMA1BVC',this,'_Curs_GSMA1BVC','',.f.,.t.
          if used('_Curs_GSMA1BVC')
            select _Curs_GSMA1BVC
            locate for 1=1
            do while not(eof())
            this.w_ERRORE = this.w_ERRORE or (_Curs_GSMA1BVC.ARFLCOMM<>"S") 
              select _Curs_GSMA1BVC
              continue
            enddo
            use
          endif
          if this.w_ERRORE
            this.w_ERRMSG = "Impossibile variare il flag:%0%0Esistono delle distinte non gestite a commessa che contengono <%1>"
          else
            this.w_GSMA_ADP.w_PRQTAMIN = 0
            this.w_GSMA_ADP.w_PRLOTRIO = 0
            this.w_GSMA_ADP.w_PRPUNRIO = 0
            this.w_GSMA_ADP.w_PRSCOMIN = 0
            this.w_GSMA_ADP.w_PRPERSCA = 0
            this.w_GSMA_ADP.mCalc(true)     
          endif
      endcase
    else
      if g_PROD="S"
        * --- Controlla se esistono figli gestiti a commessa
        * --- Select from GSMA3BVC
        do vq_exec with 'GSMA3BVC',this,'_Curs_GSMA3BVC','',.f.,.t.
        if used('_Curs_GSMA3BVC')
          select _Curs_GSMA3BVC
          locate for 1=1
          do while not(eof())
          this.w_ERRORE = this.w_ERRORE or (_Curs_GSMA3BVC.ARFLCOMM="S")
            select _Curs_GSMA3BVC
            continue
          enddo
          use
        endif
        if this.w_ERRORE
          this.w_ERRMSG = "Impossibile variare il flag:%0%0Nella distinta <%1> esistono dei componenti gestiti a commessa"
        endif
      endif
    endif
    if this.w_ERRORE
      ah_ErrorMsg(this.w_ERRMSG, 16,"",alltrim(this.oParentObject.w_ARCODART))
      * --- Riassegna vecchio valore
      this.oParentObject.w_ARFLCOMM = iif(this.oParentObject.w_ARFLCOMM="S","N","S")
    else
      if g_PROD="S"
        this.w_GSMA_ADP.mCalc(true)     
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_GSMA1BVC')
      use in _Curs_GSMA1BVC
    endif
    if used('_Curs_GSMA3BVC')
      use in _Curs_GSMA3BVC
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
