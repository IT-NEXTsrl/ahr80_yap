* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_kat                                                        *
*              Dettaglio cash flow atteso                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-14                                                      *
* Last revis.: 2015-10-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_kat",oParentObject))

* --- Class definition
define class tgste_kat as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 750
  Height = 369
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-10-27"
  HelpContextID=132789353
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Constant Properties
  _IDX = 0
  COC_MAST_IDX = 0
  VALUTE_IDX = 0
  cPrg = "gste_kat"
  cComment = "Dettaglio cash flow atteso"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_NUMCOR = space(15)
  w_FINE = ctod('  /  /  ')
  w_FINE_MSK = ctod('  /  /  ')
  w_INIZIO = ctod('  /  /  ')
  w_INIZIO_MSK = ctod('  /  /  ')
  w_DESCON = space(35)
  w_NONDOMIC = .F.
  w_SERIALE = space(10)
  w_TIPOCASHFLOW = space(1)
  w_TIPOLOGIACONTO = space(1)
  w_PARAME = space(3)
  w_TIPOPAG = space(2)
  w_ZOOMDDE = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_katPag1","gste_kat",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZOOMDDE = this.oPgFrm.Pages(1).oPag.ZOOMDDE
    DoDefault()
    proc Destroy()
      this.w_ZOOMDDE = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='COC_MAST'
    this.cWorkTables[2]='VALUTE'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NUMCOR=space(15)
      .w_FINE=ctod("  /  /  ")
      .w_FINE_MSK=ctod("  /  /  ")
      .w_INIZIO=ctod("  /  /  ")
      .w_INIZIO_MSK=ctod("  /  /  ")
      .w_DESCON=space(35)
      .w_NONDOMIC=.f.
      .w_SERIALE=space(10)
      .w_TIPOCASHFLOW=space(1)
      .w_TIPOLOGIACONTO=space(1)
      .w_PARAME=space(3)
      .w_TIPOPAG=space(2)
      .w_NUMCOR=oParentObject.w_NUMCOR
      .w_FINE=oParentObject.w_FINE
      .w_FINE_MSK=oParentObject.w_FINE_MSK
      .w_INIZIO=oParentObject.w_INIZIO
      .w_INIZIO_MSK=oParentObject.w_INIZIO_MSK
      .w_DESCON=oParentObject.w_DESCON
      .w_NONDOMIC=oParentObject.w_NONDOMIC
      .w_TIPOCASHFLOW=oParentObject.w_TIPOCASHFLOW
      .w_TIPOLOGIACONTO=oParentObject.w_TIPOLOGIACONTO
      .w_TIPOPAG=oParentObject.w_TIPOPAG
      .oPgFrm.Page1.oPag.ZOOMDDE.Calculate()
          .DoRTCalc(1,7,.f.)
        .w_SERIALE = .w_zoomDDE.getVar('MVSERIAL')
          .DoRTCalc(9,10,.f.)
        .w_PARAME = .w_zoomDDE.getVar('MVFLVEAC')+.w_zoomDDE.getVar('MVCLADOC')
    endwith
    this.DoRTCalc(12,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_NUMCOR=.w_NUMCOR
      .oParentObject.w_FINE=.w_FINE
      .oParentObject.w_FINE_MSK=.w_FINE_MSK
      .oParentObject.w_INIZIO=.w_INIZIO
      .oParentObject.w_INIZIO_MSK=.w_INIZIO_MSK
      .oParentObject.w_DESCON=.w_DESCON
      .oParentObject.w_NONDOMIC=.w_NONDOMIC
      .oParentObject.w_TIPOCASHFLOW=.w_TIPOCASHFLOW
      .oParentObject.w_TIPOLOGIACONTO=.w_TIPOLOGIACONTO
      .oParentObject.w_TIPOPAG=.w_TIPOPAG
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZOOMDDE.Calculate()
        .DoRTCalc(1,7,.t.)
            .w_SERIALE = .w_zoomDDE.getVar('MVSERIAL')
        .DoRTCalc(9,10,.t.)
            .w_PARAME = .w_zoomDDE.getVar('MVFLVEAC')+.w_zoomDDE.getVar('MVCLADOC')
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(12,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZOOMDDE.Calculate()
    endwith
  return

  proc Calculate_MDCVJZPDLW()
    with this
          * --- CREA
          GSTE_BD5(this;
              ,'A';
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_1.visible=!this.oPgFrm.Page1.oPag.oStr_1_1.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_2.visible=!this.oPgFrm.Page1.oPag.oStr_1_2.mHide()
    this.oPgFrm.Page1.oPag.oNUMCOR_1_3.visible=!this.oPgFrm.Page1.oPag.oNUMCOR_1_3.mHide()
    this.oPgFrm.Page1.oPag.oDESCON_1_9.visible=!this.oPgFrm.Page1.oPag.oDESCON_1_9.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_13.visible=!this.oPgFrm.Page1.oPag.oStr_1_13.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZOOMDDE.Event(cEvent)
        if lower(cEvent)==lower("Blank")
          .Calculate_MDCVJZPDLW()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNUMCOR_1_3.value==this.w_NUMCOR)
      this.oPgFrm.Page1.oPag.oNUMCOR_1_3.value=this.w_NUMCOR
    endif
    if not(this.oPgFrm.Page1.oPag.oFINE_MSK_1_6.value==this.w_FINE_MSK)
      this.oPgFrm.Page1.oPag.oFINE_MSK_1_6.value=this.w_FINE_MSK
    endif
    if not(this.oPgFrm.Page1.oPag.oINIZIO_MSK_1_8.value==this.w_INIZIO_MSK)
      this.oPgFrm.Page1.oPag.oINIZIO_MSK_1_8.value=this.w_INIZIO_MSK
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_9.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_9.value=this.w_DESCON
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgste_katPag1 as StdContainer
  Width  = 746
  height = 369
  stdWidth  = 746
  stdheight = 369
  resizeXpos=443
  resizeYpos=198
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNUMCOR_1_3 as StdField with uid="CHABXZVGWR",rtseq=1,rtrep=.f.,;
    cFormVar = "w_NUMCOR", cQueryName = "NUMCOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Conto selezionato per cash flow",;
    HelpContextID = 11668010,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=104, Top=4, InputMask=replicate('X',15)

  func oNUMCOR_1_3.mHide()
    with this.Parent.oContained
      return (.w_NONDOMIC)
    endwith
  endfunc


  add object ZOOMDDE as cp_zoombox with uid="FMSRBJXCFE",left=7, top=30, width=730,height=288,;
    caption='ZOOMDDE',;
   bGlobalFont=.t.,;
    cTable="DOC_MAST",cZoomFile="GSTE_KAT",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.t.,bQueryOnLoad=.f.,bNoZoomGridShape=.f.,cZoomOnZoom="",cMenuFile="",bRetriveAllRows=.f.,bQueryOnDblClick=.t.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 11014294

  add object oFINE_MSK_1_6 as StdField with uid="WRBJIAIOKH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_FINE_MSK", cQueryName = "FINE_MSK",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data fine movimenti",;
    HelpContextID = 189790625,;
   bGlobalFont=.t.,;
    Height=20, Width=76, Left=629, Top=6

  add object oINIZIO_MSK_1_8 as StdField with uid="SWINJLVOOA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_INIZIO_MSK", cQueryName = "INIZIO_MSK",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data inizio movimenti",;
    HelpContextID = 66781437,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=527, Top=6

  add object oDESCON_1_9 as StdField with uid="GJYJDIYVGW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 78756554,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=226, Top=4, InputMask=replicate('X',35)

  func oDESCON_1_9.mHide()
    with this.Parent.oContained
      return (.w_NONDOMIC)
    endwith
  endfunc


  add object oBtn_1_14 as StdButton with uid="MBXAIGGBZE",left=639, top=319, width=48,height=45,;
    CpPicture="bmp\ORIGINE.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza il documento associato alla riga selezionata";
    , HelpContextID = 219402982;
    , caption='\<Origine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      with this.Parent.oContained
        GSVE_BZM(this.Parent.oContained,.w_SERIALE, .w_PARAME)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc


  add object oBtn_1_16 as StdButton with uid="GHXPFFDRPI",left=690, top=319, width=48,height=45,;
    CpPicture="bmp\esc.bmp", caption="", nPag=1;
    , ToolTipText = "Annulla";
    , HelpContextID = 125471930;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_1_1 as StdString with uid="RXGHAOBBMG",Visible=.t., Left=16, Top=7,;
    Alignment=1, Width=85, Height=18,;
    Caption="Conto cassa:"  ;
  , bGlobalFont=.t.

  func oStr_1_1.mHide()
    with this.Parent.oContained
      return (.w_TIPOLOGIACONTO <> 'G')
    endwith
  endfunc

  add object oStr_1_2 as StdString with uid="AGHABLKMIA",Visible=.t., Left=8, Top=7,;
    Alignment=1, Width=93, Height=18,;
    Caption="Conto banca:"  ;
  , bGlobalFont=.t.

  func oStr_1_2.mHide()
    with this.Parent.oContained
      return (.w_TIPOLOGIACONTO <> 'C')
    endwith
  endfunc

  add object oStr_1_10 as StdString with uid="IENRQNJDTA",Visible=.t., Left=490, Top=6,;
    Alignment=1, Width=32, Height=18,;
    Caption="dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="OOEHRSQLGY",Visible=.t., Left=605, Top=6,;
    Alignment=1, Width=22, Height=18,;
    Caption="al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="CJPUSVTCKD",Visible=.t., Left=179, Top=6,;
    Alignment=0, Width=207, Height=21,;
    Caption="Scadenze non domiciliate"    , forecolor= rgb(255,0,0);
  ;
    , FontName = "Arial", FontSize = 12, FontBold = .t., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_13.mHide()
    with this.Parent.oContained
      return (!.w_NONDOMIC)
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_kat','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
