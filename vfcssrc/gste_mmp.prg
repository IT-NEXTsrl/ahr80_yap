* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_mmp                                                        *
*              Manutenzione partite                                            *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_306]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-21                                                      *
* Last revis.: 2016-11-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_mmp"))

* --- Class definition
define class tgste_mmp as StdTrsForm
  Top    = 63
  Left   = 10

  * --- Standard Properties
  Width  = 787
  Height = 296+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2016-11-22"
  HelpContextID=70634391
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=100

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  PAR_TITE_IDX = 0
  VALUTE_IDX = 0
  BAN_CHE_IDX = 0
  CONTI_IDX = 0
  MOD_PAGA_IDX = 0
  PNT_DETT_IDX = 0
  DIS_TINT_IDX = 0
  COC_MAST_IDX = 0
  BAN_CONTI_IDX = 0
  CAU_CONT_IDX = 0
  DATI_AGG_IDX = 0
  PNT_MAST_IDX = 0
  cFile = "PAR_TITE"
  cKeySelect = "PTSERIAL,PTROWORD"
  cKeyWhere  = "PTSERIAL=this.w_PTSERIAL and PTROWORD=this.w_PTROWORD"
  cKeyDetail  = "PTSERIAL=this.w_PTSERIAL and PTROWORD=this.w_PTROWORD"
  cKeyWhereODBC = '"PTSERIAL="+cp_ToStrODBC(this.w_PTSERIAL)';
      +'+" and PTROWORD="+cp_ToStrODBC(this.w_PTROWORD)';

  cKeyDetailWhereODBC = '"PTSERIAL="+cp_ToStrODBC(this.w_PTSERIAL)';
      +'+" and PTROWORD="+cp_ToStrODBC(this.w_PTROWORD)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"PAR_TITE.PTSERIAL="+cp_ToStrODBC(this.w_PTSERIAL)';
      +'+" and PAR_TITE.PTROWORD="+cp_ToStrODBC(this.w_PTROWORD)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'PAR_TITE.CPROWNUM '
  cPrg = "gste_mmp"
  cComment = "Manutenzione partite"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 6
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PTSERIAL = space(10)
  w_PTROWORD = 0
  w_PTTIPCON = space(1)
  w_PTCODCON = space(15)
  w_OBTEST = ctod('  /  /  ')
  w_DESCON = space(40)
  w_IMPRIG = 0
  w_RIFNUM = 0
  w_RIFALF = space(10)
  w_DESCRI = space(37)
  w_DESRIG = space(50)
  w_CODICE = space(10)
  w_NUMRER = 0
  w_DATREG = ctod('  /  /  ')
  w_CODCAU = space(5)
  w_FLDAVE = space(1)
  w_VALNAZ = space(3)
  w_CAONAZ = 0
  w_DECTOP = 0
  w_CODVAL = space(3)
  w_CAOVAL = 0
  w_BANRIF = space(15)
  w_DESBAN = space(35)
  w_NUMIND = 0
  w_ESEIND = space(4)
  w_DATIND = ctod('  /  /  ')
  w_DATDOC = ctod('  /  /  ')
  w_TOTDOC = 0
  w_PTDATSCA = ctod('  /  /  ')
  w_SEGNO = space(1)
  o_SEGNO = space(1)
  w_SIMBVA = space(5)
  w_PTNUMPAR = space(31)
  w_PT_SEGNO = space(1)
  o_PT_SEGNO = space(1)
  w_PTSERRIF = space(10)
  w_CAUSALE = space(5)
  w_ESIDETT = space(1)
  w_PTTOTIMP = 0
  o_PTTOTIMP = 0
  w_PTFLSOSP = space(1)
  w_PTMODPAG = space(10)
  w_PTBANAPP = space(10)
  o_PTBANAPP = space(10)
  w_PTNUMDOC = 0
  w_PTALFDOC = space(10)
  w_PTDATDOC = ctod('  /  /  ')
  w_PTIMPDOC = 0
  w_PTCODVAL = space(3)
  w_DESVAL = space(35)
  w_PTCAOVAL = 0
  w_PTDATAPE = ctod('  /  /  ')
  w_PTCAOAPE = 0
  w_PTBANNOS = space(15)
  w_PTNUMEFF = 0
  w_PTFLRAGG = space(1)
  w_PTTOTABB = 0
  w_PTFLCRSA = space(1)
  w_DECTOT = 0
  w_IMPDAR = 0
  w_IMPAVE = 0
  w_DECUNI = 0
  w_PTNUMDIS = space(10)
  w_CODDIS = space(10)
  w_NUMDIS = 0
  w_ANNDIS = space(4)
  w_DATDIS = ctod('  /  /  ')
  w_TIPDIS = space(2)
  w_PTDESRIG = space(50)
  w_PTORDRIF = 0
  w_PTNUMRIF = 0
  w_PTCODAGE = space(5)
  w_PTDATINT = ctod('  /  /  ')
  w_PTFLVABD = space(1)
  w_RIG = 0
  w_NUMEFF = 0
  w_FLDEFI = space(1)
  w_PTDATREG = ctod('  /  /  ')
  w_PTNUMCOR = space(25)
  w_DATSCA = ctod('  /  /  ')
  w_TOTIMP = 0
  w_OTOTIMP = 0
  w_TIPPAG = space(2)
  w_DVSERIAL = space(10)
  w_TOTALE = 0
  w_SEG = space(1)
  w_SIMNAZ = space(3)
  w_TOTRIG = 0
  o_TOTRIG = 0
  w_FLINSO = space(1)
  w_FLIVDF = space(1)
  w_FLPART = space(1)
  w_PNAGG_01 = space(15)
  w_PNAGG_02 = space(15)
  w_PNAGG_03 = space(15)
  w_PNAGG_04 = space(15)
  w_PNAGG_05 = ctod('  /  /  ')
  w_PNAGG_06 = ctod('  /  /  ')
  w_DASERIAL = space(10)
  w_DACAM_01 = space(30)
  w_DACAM_02 = space(30)
  w_DACAM_03 = space(30)
  w_DACAM_04 = space(30)
  w_DACAM_05 = space(30)
  w_DACAM_06 = space(30)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gste_mmp
   i_Batch =.f.
   w_ROWNUM=0
   w_RESIDUO=0
  * --- Disabilita il Caricamento e la Cancellazione sulla Toolbar
  * --- Salva
  proc ecpSave()
         DoDefault()
         if this.i_Batch = .f.
            Keyboard "{ESC}"
          endif
  endproc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PAR_TITE','gste_mmp')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_mmpPag1","gste_mmp",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Partite")
      .Pages(1).HelpContextID = 255939062
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- gste_mmp
      * Nascondo le tabs in vecchia configurazione interfaccia
    *non � stato messo in blanck Record End perch� in modifica i campi sono editabili
    if i_cMenuTab<>"T" or i_VisualTheme=-1
      this.Tabs=.f.
    endif
    * --- Fine Area Manuale
    this.parent.resize()
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[12]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='BAN_CHE'
    this.cWorkTables[3]='CONTI'
    this.cWorkTables[4]='MOD_PAGA'
    this.cWorkTables[5]='PNT_DETT'
    this.cWorkTables[6]='DIS_TINT'
    this.cWorkTables[7]='COC_MAST'
    this.cWorkTables[8]='BAN_CONTI'
    this.cWorkTables[9]='CAU_CONT'
    this.cWorkTables[10]='DATI_AGG'
    this.cWorkTables[11]='PNT_MAST'
    this.cWorkTables[12]='PAR_TITE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(12))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAR_TITE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAR_TITE_IDX,3]
  return

  procedure SetWorkFromKeySet
    * --- Initializing work variables from KeySet. They will be used to load the record.
    select (this.cKeySet)
    with this
      .w_PTSERIAL = NVL(PTSERIAL,space(10))
      .w_PTROWORD = NVL(PTROWORD,0)
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_4_joined
    link_1_4_joined=.f.
    local link_2_11_joined
    link_2_11_joined=.f.
    local link_2_17_joined
    link_2_17_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from PAR_TITE where PTSERIAL=KeySet.PTSERIAL
    *                            and PTROWORD=KeySet.PTROWORD
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- gste_mmp
      * --- Setta Ordine per Data Scadenza
      i_cOrder = 'order by PTDATSCA '
      IF this.w_ROWNUM>0
      this.cKeyWhereODBCqualified=;
            '"PAR_TITE.PTSERIAL="+cp_ToStrODBC(this.w_PTSERIAL)';
            +'+" and PAR_TITE.PTROWORD="+cp_ToStrODBC(this.w_PTROWORD)';
            + iif(Not Isalt() or g_COGE='S' ,'+" and PAR_TITE.CPROWNUM="+cp_ToStrODBC(this.w_ROWNUM)','')
      ENDIF
      
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2],this.bLoadRecFilter,this.PAR_TITE_IDX,"gste_mmp")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAR_TITE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAR_TITE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAR_TITE '
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_11_joined=this.AddJoinedLink_2_11(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_2_17_joined=this.AddJoinedLink_2_17(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PTSERIAL',this.w_PTSERIAL  ,'PTROWORD',this.w_PTROWORD  )
      select * from (i_cTable) PAR_TITE where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESCON = space(40)
        .w_IMPRIG = 0
        .w_RIFNUM = 0
        .w_RIFALF = space(10)
        .w_DESCRI = space(37)
        .w_DESRIG = space(50)
        .w_CODICE = space(10)
        .w_NUMRER = 0
        .w_DATREG = ctod("  /  /  ")
        .w_CODCAU = space(5)
        .w_FLDAVE = space(1)
        .w_VALNAZ = space(3)
        .w_CAONAZ = 0
        .w_DECTOP = 0
        .w_CODVAL = space(3)
        .w_CAOVAL = 0
        .w_BANRIF = space(15)
        .w_DESBAN = space(35)
        .w_NUMIND = 0
        .w_ESEIND = space(4)
        .w_DATIND = ctod("  /  /  ")
        .w_DATDOC = ctod("  /  /  ")
        .w_TOTDOC = 0
        .w_TOTALE = 0
        .w_SEG = space(1)
        .w_SIMNAZ = space(3)
        .w_TOTRIG = 0
        .w_FLINSO = space(1)
        .w_FLIVDF = space(1)
        .w_FLPART = space(1)
        .w_PNAGG_01 = space(15)
        .w_PNAGG_02 = space(15)
        .w_PNAGG_03 = space(15)
        .w_PNAGG_04 = space(15)
        .w_PNAGG_05 = ctod("  /  /  ")
        .w_PNAGG_06 = ctod("  /  /  ")
        .w_DASERIAL = '0000000001'
        .w_DACAM_01 = space(30)
        .w_DACAM_02 = space(30)
        .w_DACAM_03 = space(30)
        .w_DACAM_04 = space(30)
        .w_DACAM_05 = space(30)
        .w_DACAM_06 = space(30)
        .w_PTSERIAL = NVL(PTSERIAL,space(10))
        .w_PTROWORD = NVL(PTROWORD,0)
        .w_PTTIPCON = NVL(PTTIPCON,space(1))
        .w_PTCODCON = NVL(PTCODCON,space(15))
          if link_1_4_joined
            this.w_PTCODCON = NVL(ANCODICE104,NVL(this.w_PTCODCON,space(15)))
            this.w_DESCON = NVL(ANDESCRI104,space(40))
          else
          .link_1_4('Load')
          endif
        .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
          .link_1_21('Load')
          .link_1_35('Load')
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
          .link_2_61('Load')
        cp_LoadRecExtFlds(this,'PAR_TITE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      this.w_TOTRIG = 0
      scan
        with this
          .w_SEGNO = space(1)
          .w_SIMBVA = space(5)
          .w_CAUSALE = space(5)
          .w_ESIDETT = space(1)
          .w_DESVAL = space(35)
          .w_DECTOT = 0
          .w_DECUNI = 0
          .w_NUMDIS = 0
          .w_ANNDIS = space(4)
          .w_DATDIS = ctod("  /  /  ")
          .w_TIPDIS = space(2)
          .w_FLDEFI = space(1)
          .w_DATSCA = ctod("  /  /  ")
          .w_TOTIMP = 0
        .w_OTOTIMP = .w_PTTOTIMP
          .w_TIPPAG = space(2)
          .w_DVSERIAL = space(10)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_PTDATSCA = NVL(cp_ToDate(PTDATSCA),ctod("  /  /  "))
          .w_PTNUMPAR = NVL(PTNUMPAR,space(31))
          .w_PT_SEGNO = NVL(PT_SEGNO,space(1))
          .w_PTSERRIF = NVL(PTSERRIF,space(10))
          .link_2_6('Load')
          .link_2_7('Load')
          .w_PTTOTIMP = NVL(PTTOTIMP,0)
          .w_PTFLSOSP = NVL(PTFLSOSP,space(1))
          .w_PTMODPAG = NVL(PTMODPAG,space(10))
          if link_2_11_joined
            this.w_PTMODPAG = NVL(MPCODICE211,NVL(this.w_PTMODPAG,space(10)))
            this.w_TIPPAG = NVL(MPTIPPAG211,space(2))
          else
          .link_2_11('Load')
          endif
          .w_PTBANAPP = NVL(PTBANAPP,space(10))
          * evitabile
          *.link_2_12('Load')
          .w_PTNUMDOC = NVL(PTNUMDOC,0)
          .w_PTALFDOC = NVL(PTALFDOC,space(10))
          .w_PTDATDOC = NVL(cp_ToDate(PTDATDOC),ctod("  /  /  "))
          .w_PTIMPDOC = NVL(PTIMPDOC,0)
          .w_PTCODVAL = NVL(PTCODVAL,space(3))
          if link_2_17_joined
            this.w_PTCODVAL = NVL(VACODVAL217,NVL(this.w_PTCODVAL,space(3)))
            this.w_DESVAL = NVL(VADESVAL217,space(35))
            this.w_SIMBVA = NVL(VASIMVAL217,space(5))
            this.w_DECTOT = NVL(VADECTOT217,0)
            this.w_DECUNI = NVL(VADECUNI217,0)
          else
          .link_2_17('Load')
          endif
          .w_PTCAOVAL = NVL(PTCAOVAL,0)
          .w_PTDATAPE = NVL(cp_ToDate(PTDATAPE),ctod("  /  /  "))
          .w_PTCAOAPE = NVL(PTCAOAPE,0)
          .w_PTBANNOS = NVL(PTBANNOS,space(15))
          * evitabile
          *.link_2_22('Load')
          .w_PTNUMEFF = NVL(PTNUMEFF,0)
          .w_PTFLRAGG = NVL(PTFLRAGG,space(1))
          .w_PTTOTABB = NVL(PTTOTABB,0)
          .w_PTFLCRSA = NVL(PTFLCRSA,space(1))
        .w_IMPDAR = IIF(.w_PT_SEGNO='D', .w_PTTOTIMP,0)
        .w_IMPAVE = IIF(.w_PT_SEGNO='A', .w_PTTOTIMP,0)
          .w_PTNUMDIS = NVL(PTNUMDIS,space(10))
        .w_CODDIS = IIF(.w_PTROWORD=-2, .w_PTSERIAL, .w_PTNUMDIS)
          .link_2_34('Load')
          .w_PTDESRIG = NVL(PTDESRIG,space(50))
          .w_PTORDRIF = NVL(PTORDRIF,0)
          .w_PTNUMRIF = NVL(PTNUMRIF,0)
          .w_PTCODAGE = NVL(PTCODAGE,space(5))
          .w_PTDATINT = NVL(cp_ToDate(PTDATINT),ctod("  /  /  "))
          .w_PTFLVABD = NVL(PTFLVABD,space(1))
        .w_RIG = IIF(.w_PT_SEGNO='A',1,-1 )*IIF( .w_PTCODVAL=.w_VALNAZ , .w_PTTOTIMP , cp_ROUND(VAL2MON(.w_PTTOTIMP, .w_PTCAOVAL, .w_CAONAZ, .w_PTDATSCA, .w_VALNAZ), .w_DECTOP))
        .w_NUMEFF = IIF(.w_PTNUMEFF<>0,.w_PTNUMEFF,.w_NUMEFF)
          .w_PTDATREG = NVL(cp_ToDate(PTDATREG),ctod("  /  /  "))
          .w_PTNUMCOR = NVL(PTNUMCOR,space(25))
          * evitabile
          *.link_2_49('Load')
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          .w_TOTRIG = .w_TOTRIG+.w_RIG
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .oPgFrm.Page1.oPag.oBtn_2_27.enabled = .oPgFrm.Page1.oPag.oBtn_2_27.mCond()
        .oPgFrm.Page1.oPag.oBtn_2_31.enabled = .oPgFrm.Page1.oPag.oBtn_2_31.mCond()
        .oPgFrm.Page1.oPag.oBtn_1_53.enabled = .oPgFrm.Page1.oPag.oBtn_1_53.mCond()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_PTSERIAL=space(10)
      .w_PTROWORD=0
      .w_PTTIPCON=space(1)
      .w_PTCODCON=space(15)
      .w_OBTEST=ctod("  /  /  ")
      .w_DESCON=space(40)
      .w_IMPRIG=0
      .w_RIFNUM=0
      .w_RIFALF=space(10)
      .w_DESCRI=space(37)
      .w_DESRIG=space(50)
      .w_CODICE=space(10)
      .w_NUMRER=0
      .w_DATREG=ctod("  /  /  ")
      .w_CODCAU=space(5)
      .w_FLDAVE=space(1)
      .w_VALNAZ=space(3)
      .w_CAONAZ=0
      .w_DECTOP=0
      .w_CODVAL=space(3)
      .w_CAOVAL=0
      .w_BANRIF=space(15)
      .w_DESBAN=space(35)
      .w_NUMIND=0
      .w_ESEIND=space(4)
      .w_DATIND=ctod("  /  /  ")
      .w_DATDOC=ctod("  /  /  ")
      .w_TOTDOC=0
      .w_PTDATSCA=ctod("  /  /  ")
      .w_SEGNO=space(1)
      .w_SIMBVA=space(5)
      .w_PTNUMPAR=space(31)
      .w_PT_SEGNO=space(1)
      .w_PTSERRIF=space(10)
      .w_CAUSALE=space(5)
      .w_ESIDETT=space(1)
      .w_PTTOTIMP=0
      .w_PTFLSOSP=space(1)
      .w_PTMODPAG=space(10)
      .w_PTBANAPP=space(10)
      .w_PTNUMDOC=0
      .w_PTALFDOC=space(10)
      .w_PTDATDOC=ctod("  /  /  ")
      .w_PTIMPDOC=0
      .w_PTCODVAL=space(3)
      .w_DESVAL=space(35)
      .w_PTCAOVAL=0
      .w_PTDATAPE=ctod("  /  /  ")
      .w_PTCAOAPE=0
      .w_PTBANNOS=space(15)
      .w_PTNUMEFF=0
      .w_PTFLRAGG=space(1)
      .w_PTTOTABB=0
      .w_PTFLCRSA=space(1)
      .w_DECTOT=0
      .w_IMPDAR=0
      .w_IMPAVE=0
      .w_DECUNI=0
      .w_PTNUMDIS=space(10)
      .w_CODDIS=space(10)
      .w_NUMDIS=0
      .w_ANNDIS=space(4)
      .w_DATDIS=ctod("  /  /  ")
      .w_TIPDIS=space(2)
      .w_PTDESRIG=space(50)
      .w_PTORDRIF=0
      .w_PTNUMRIF=0
      .w_PTCODAGE=space(5)
      .w_PTDATINT=ctod("  /  /  ")
      .w_PTFLVABD=space(1)
      .w_RIG=0
      .w_NUMEFF=0
      .w_FLDEFI=space(1)
      .w_PTDATREG=ctod("  /  /  ")
      .w_PTNUMCOR=space(25)
      .w_DATSCA=ctod("  /  /  ")
      .w_TOTIMP=0
      .w_OTOTIMP=0
      .w_TIPPAG=space(2)
      .w_DVSERIAL=space(10)
      .w_TOTALE=0
      .w_SEG=space(1)
      .w_SIMNAZ=space(3)
      .w_TOTRIG=0
      .w_FLINSO=space(1)
      .w_FLIVDF=space(1)
      .w_FLPART=space(1)
      .w_PNAGG_01=space(15)
      .w_PNAGG_02=space(15)
      .w_PNAGG_03=space(15)
      .w_PNAGG_04=space(15)
      .w_PNAGG_05=ctod("  /  /  ")
      .w_PNAGG_06=ctod("  /  /  ")
      .w_DASERIAL=space(10)
      .w_DACAM_01=space(30)
      .w_DACAM_02=space(30)
      .w_DACAM_03=space(30)
      .w_DACAM_04=space(30)
      .w_DACAM_05=space(30)
      .w_DACAM_06=space(30)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        if not(empty(.w_PTCODCON))
         .link_1_4('Full')
        endif
        .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .DoRTCalc(6,15,.f.)
        if not(empty(.w_CODCAU))
         .link_1_21('Full')
        endif
        .DoRTCalc(16,22,.f.)
        if not(empty(.w_BANRIF))
         .link_1_35('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(23,34,.f.)
        if not(empty(.w_PTSERRIF))
         .link_2_6('Full')
        endif
        .DoRTCalc(35,35,.f.)
        if not(empty(.w_CAUSALE))
         .link_2_7('Full')
        endif
        .DoRTCalc(36,37,.f.)
        .w_PTFLSOSP = ' '
        .DoRTCalc(39,39,.f.)
        if not(empty(.w_PTMODPAG))
         .link_2_11('Full')
        endif
        .DoRTCalc(40,40,.f.)
        if not(empty(.w_PTBANAPP))
         .link_2_12('Full')
        endif
        .DoRTCalc(41,44,.f.)
        .w_PTCODVAL = .w_CODVAL
        .DoRTCalc(45,45,.f.)
        if not(empty(.w_PTCODVAL))
         .link_2_17('Full')
        endif
        .DoRTCalc(46,46,.f.)
        .w_PTCAOVAL = .w_CAOVAL
        .DoRTCalc(48,50,.f.)
        if not(empty(.w_PTBANNOS))
         .link_2_22('Full')
        endif
        .DoRTCalc(51,53,.f.)
        .w_PTFLCRSA = IIF(.w_PTROWORD<1,'C',.w_FLPART)
        .DoRTCalc(55,55,.f.)
        .w_IMPDAR = IIF(.w_PT_SEGNO='D', .w_PTTOTIMP,0)
        .w_IMPAVE = IIF(.w_PT_SEGNO='A', .w_PTTOTIMP,0)
        .DoRTCalc(58,59,.f.)
        .w_CODDIS = IIF(.w_PTROWORD=-2, .w_PTSERIAL, .w_PTNUMDIS)
        .DoRTCalc(60,60,.f.)
        if not(empty(.w_CODDIS))
         .link_2_34('Full')
        endif
        .DoRTCalc(61,64,.f.)
        .w_PTDESRIG = .w_DESRIG
        .DoRTCalc(66,70,.f.)
        .w_RIG = IIF(.w_PT_SEGNO='A',1,-1 )*IIF( .w_PTCODVAL=.w_VALNAZ , .w_PTTOTIMP , cp_ROUND(VAL2MON(.w_PTTOTIMP, .w_PTCAOVAL, .w_CAONAZ, .w_PTDATSCA, .w_VALNAZ), .w_DECTOP))
        .w_NUMEFF = IIF(.w_PTNUMEFF<>0,.w_PTNUMEFF,.w_NUMEFF)
        .DoRTCalc(73,73,.f.)
        .w_PTDATREG = .w_DATREG
        .DoRTCalc(75,75,.f.)
        if not(empty(.w_PTNUMCOR))
         .link_2_49('Full')
        endif
        .DoRTCalc(76,77,.f.)
        .w_OTOTIMP = .w_PTTOTIMP
        .DoRTCalc(79,93,.f.)
        .w_DASERIAL = '0000000001'
        .DoRTCalc(94,94,.f.)
        if not(empty(.w_DASERIAL))
         .link_2_61('Full')
        endif
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAR_TITE')
    this.DoRTCalc(95,100,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.oPgFrm.Page1.oPag.oBtn_2_27.enabled = this.oPgFrm.Page1.oPag.oBtn_2_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_31.enabled = this.oPgFrm.Page1.oPag.oBtn_2_31.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_53.enabled = this.oPgFrm.Page1.oPag.oBtn_1_53.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- gste_mmp
    * Nascondo le tabs in nuova configurazione interfaccia
     if i_cMenuTab="T" and i_VisualTheme<>-1 and Type('This.oTabMenu')='O'
       This.oTabMenu.Visible=.F.
     endif
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disabling List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPTCODCON_1_4.enabled = i_bVal
      .Page1.oPag.oPTDESRIG_2_39.enabled = i_bVal
      .Page1.oPag.oPTNUMCOR_2_49.enabled = i_bVal
      .Page1.oPag.oBtn_2_27.enabled = .Page1.oPag.oBtn_2_27.mCond()
      .Page1.oPag.oBtn_2_31.enabled = .Page1.oPag.oBtn_2_31.mCond()
      .Page1.oPag.oBtn_1_53.enabled = .Page1.oPag.oBtn_1_53.mCond()
      .Page1.oPag.oObj_1_13.enabled = i_bVal
      .Page1.oPag.oObj_1_41.enabled = i_bVal
      .Page1.oPag.oObj_1_42.enabled = i_bVal
      .Page1.oPag.oObj_1_44.enabled = i_bVal
      .Page1.oPag.oObj_1_47.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
      if i_cOp = "Query"
        .Page1.oPag.oPTCODCON_1_4.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'PAR_TITE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PTSERIAL,"PTSERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PTROWORD,"PTROWORD",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PTTIPCON,"PTTIPCON",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PTCODCON,"PTCODCON",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Creating cKeySet cursor with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
    i_lTable = "PAR_TITE"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PAR_TITE_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select distinct "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select distinct &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && because cursors reuse main file
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_PTDATSCA D(8);
      ,t_SIMBVA C(5);
      ,t_PTNUMPAR C(31);
      ,t_PT_SEGNO C(1);
      ,t_PTTOTIMP N(18,4);
      ,t_PTFLSOSP N(3);
      ,t_PTMODPAG C(10);
      ,t_PTBANAPP C(10);
      ,t_PTDESRIG C(50);
      ,t_PTNUMCOR C(25);
      ,CPROWNUM N(10);
      ,t_SEGNO C(1);
      ,t_PTSERRIF C(10);
      ,t_CAUSALE C(5);
      ,t_ESIDETT C(1);
      ,t_PTNUMDOC N(15);
      ,t_PTALFDOC C(10);
      ,t_PTDATDOC D(8);
      ,t_PTIMPDOC N(18,4);
      ,t_PTCODVAL C(3);
      ,t_DESVAL C(35);
      ,t_PTCAOVAL N(12,7);
      ,t_PTDATAPE D(8);
      ,t_PTCAOAPE N(12,6);
      ,t_PTBANNOS C(15);
      ,t_PTNUMEFF N(6);
      ,t_PTFLRAGG C(1);
      ,t_PTTOTABB N(18,4);
      ,t_PTFLCRSA C(1);
      ,t_DECTOT N(1);
      ,t_IMPDAR N(18,4);
      ,t_IMPAVE N(18,4);
      ,t_DECUNI N(1);
      ,t_PTNUMDIS C(10);
      ,t_CODDIS C(10);
      ,t_NUMDIS N(6);
      ,t_ANNDIS C(4);
      ,t_DATDIS D(8);
      ,t_TIPDIS C(2);
      ,t_PTORDRIF N(4);
      ,t_PTNUMRIF N(3);
      ,t_PTCODAGE C(5);
      ,t_PTDATINT D(8);
      ,t_PTFLVABD C(1);
      ,t_RIG N(18,4);
      ,t_NUMEFF N(6);
      ,t_FLDEFI C(1);
      ,t_PTDATREG D(8);
      ,t_DATSCA D(8);
      ,t_TOTIMP N(18,4);
      ,t_OTOTIMP N(18,4);
      ,t_TIPPAG C(2);
      ,t_DVSERIAL C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgste_mmpbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTDATSCA_2_1.controlsource=this.cTrsName+'.t_PTDATSCA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oSIMBVA_2_3.controlsource=this.cTrsName+'.t_SIMBVA'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTNUMPAR_2_4.controlsource=this.cTrsName+'.t_PTNUMPAR'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPT_SEGNO_2_5.controlsource=this.cTrsName+'.t_PT_SEGNO'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTTOTIMP_2_9.controlsource=this.cTrsName+'.t_PTTOTIMP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTFLSOSP_2_10.controlsource=this.cTrsName+'.t_PTFLSOSP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTMODPAG_2_11.controlsource=this.cTrsName+'.t_PTMODPAG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANAPP_2_12.controlsource=this.cTrsName+'.t_PTBANAPP'
    this.oPgFRm.Page1.oPag.oPTDESRIG_2_39.controlsource=this.cTrsName+'.t_PTDESRIG'
    this.oPgFRm.Page1.oPag.oPTNUMCOR_2_49.controlsource=this.cTrsName+'.t_PTNUMCOR'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(85)
    this.AddVLine(319)
    this.AddVLine(346)
    this.AddVLine(478)
    this.AddVLine(536)
    this.AddVLine(566)
    this.AddVLine(664)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTDATSCA_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
      *
      * insert into PAR_TITE
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAR_TITE')
        i_extval=cp_InsertValODBCExtFlds(this,'PAR_TITE')
        i_cFldBody=" "+;
                  "(PTSERIAL,PTROWORD,PTTIPCON,PTCODCON,PTDATSCA"+;
                  ",PTNUMPAR,PT_SEGNO,PTSERRIF,PTTOTIMP,PTFLSOSP"+;
                  ",PTMODPAG,PTBANAPP,PTNUMDOC,PTALFDOC,PTDATDOC"+;
                  ",PTIMPDOC,PTCODVAL,PTCAOVAL,PTDATAPE,PTCAOAPE"+;
                  ",PTBANNOS,PTNUMEFF,PTFLRAGG,PTTOTABB,PTFLCRSA"+;
                  ",PTNUMDIS,PTDESRIG,PTORDRIF,PTNUMRIF,PTCODAGE"+;
                  ",PTDATINT,PTFLVABD,PTDATREG,PTNUMCOR,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_PTSERIAL)+","+cp_ToStrODBC(this.w_PTROWORD)+","+cp_ToStrODBC(this.w_PTTIPCON)+","+cp_ToStrODBCNull(this.w_PTCODCON)+","+cp_ToStrODBC(this.w_PTDATSCA)+;
             ","+cp_ToStrODBC(this.w_PTNUMPAR)+","+cp_ToStrODBC(this.w_PT_SEGNO)+","+cp_ToStrODBCNull(this.w_PTSERRIF)+","+cp_ToStrODBC(this.w_PTTOTIMP)+","+cp_ToStrODBC(this.w_PTFLSOSP)+;
             ","+cp_ToStrODBCNull(this.w_PTMODPAG)+","+cp_ToStrODBCNull(this.w_PTBANAPP)+","+cp_ToStrODBC(this.w_PTNUMDOC)+","+cp_ToStrODBC(this.w_PTALFDOC)+","+cp_ToStrODBC(this.w_PTDATDOC)+;
             ","+cp_ToStrODBC(this.w_PTIMPDOC)+","+cp_ToStrODBCNull(this.w_PTCODVAL)+","+cp_ToStrODBC(this.w_PTCAOVAL)+","+cp_ToStrODBC(this.w_PTDATAPE)+","+cp_ToStrODBC(this.w_PTCAOAPE)+;
             ","+cp_ToStrODBCNull(this.w_PTBANNOS)+","+cp_ToStrODBC(this.w_PTNUMEFF)+","+cp_ToStrODBC(this.w_PTFLRAGG)+","+cp_ToStrODBC(this.w_PTTOTABB)+","+cp_ToStrODBC(this.w_PTFLCRSA)+;
             ","+cp_ToStrODBC(this.w_PTNUMDIS)+","+cp_ToStrODBC(this.w_PTDESRIG)+","+cp_ToStrODBC(this.w_PTORDRIF)+","+cp_ToStrODBC(this.w_PTNUMRIF)+","+cp_ToStrODBC(this.w_PTCODAGE)+;
             ","+cp_ToStrODBC(this.w_PTDATINT)+","+cp_ToStrODBC(this.w_PTFLVABD)+","+cp_ToStrODBC(this.w_PTDATREG)+","+cp_ToStrODBCNull(this.w_PTNUMCOR)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAR_TITE')
        i_extval=cp_InsertValVFPExtFlds(this,'PAR_TITE')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'PTSERIAL',this.w_PTSERIAL,'PTROWORD',this.w_PTROWORD)
        INSERT INTO (i_cTable) (;
                   PTSERIAL;
                  ,PTROWORD;
                  ,PTTIPCON;
                  ,PTCODCON;
                  ,PTDATSCA;
                  ,PTNUMPAR;
                  ,PT_SEGNO;
                  ,PTSERRIF;
                  ,PTTOTIMP;
                  ,PTFLSOSP;
                  ,PTMODPAG;
                  ,PTBANAPP;
                  ,PTNUMDOC;
                  ,PTALFDOC;
                  ,PTDATDOC;
                  ,PTIMPDOC;
                  ,PTCODVAL;
                  ,PTCAOVAL;
                  ,PTDATAPE;
                  ,PTCAOAPE;
                  ,PTBANNOS;
                  ,PTNUMEFF;
                  ,PTFLRAGG;
                  ,PTTOTABB;
                  ,PTFLCRSA;
                  ,PTNUMDIS;
                  ,PTDESRIG;
                  ,PTORDRIF;
                  ,PTNUMRIF;
                  ,PTCODAGE;
                  ,PTDATINT;
                  ,PTFLVABD;
                  ,PTDATREG;
                  ,PTNUMCOR;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_PTSERIAL;
                  ,this.w_PTROWORD;
                  ,this.w_PTTIPCON;
                  ,this.w_PTCODCON;
                  ,this.w_PTDATSCA;
                  ,this.w_PTNUMPAR;
                  ,this.w_PT_SEGNO;
                  ,this.w_PTSERRIF;
                  ,this.w_PTTOTIMP;
                  ,this.w_PTFLSOSP;
                  ,this.w_PTMODPAG;
                  ,this.w_PTBANAPP;
                  ,this.w_PTNUMDOC;
                  ,this.w_PTALFDOC;
                  ,this.w_PTDATDOC;
                  ,this.w_PTIMPDOC;
                  ,this.w_PTCODVAL;
                  ,this.w_PTCAOVAL;
                  ,this.w_PTDATAPE;
                  ,this.w_PTCAOAPE;
                  ,this.w_PTBANNOS;
                  ,this.w_PTNUMEFF;
                  ,this.w_PTFLRAGG;
                  ,this.w_PTTOTABB;
                  ,this.w_PTFLCRSA;
                  ,this.w_PTNUMDIS;
                  ,this.w_PTDESRIG;
                  ,this.w_PTORDRIF;
                  ,this.w_PTNUMRIF;
                  ,this.w_PTCODAGE;
                  ,this.w_PTDATINT;
                  ,this.w_PTFLVABD;
                  ,this.w_PTDATREG;
                  ,this.w_PTNUMCOR;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- gste_mmp
    if not(bTrsErr)
       this.NotifyEvent('ControlliFinali')
    endif
    if not(bTrsErr)
       this.NotifyEvent('ChkFinali')
    endif
    * --- Fine Area Manuale
    if this.bUpdated
      i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_PTDATSCA) AND t_PTTOTIMP<>0 AND NOT EMPTY(t_PTNUMPAR)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_TITE')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " PTTIPCON="+cp_ToStrODBC(this.w_PTTIPCON)+;
                 ",PTCODCON="+cp_ToStrODBCNull(this.w_PTCODCON)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_TITE')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  PTTIPCON=this.w_PTTIPCON;
                 ,PTCODCON=this.w_PTCODCON;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_PTDATSCA) AND t_PTTOTIMP<>0 AND NOT EMPTY(t_PTNUMPAR)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update PAR_TITE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'PAR_TITE')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " PTTIPCON="+cp_ToStrODBC(this.w_PTTIPCON)+;
                     ",PTCODCON="+cp_ToStrODBCNull(this.w_PTCODCON)+;
                     ",PTDATSCA="+cp_ToStrODBC(this.w_PTDATSCA)+;
                     ",PTNUMPAR="+cp_ToStrODBC(this.w_PTNUMPAR)+;
                     ",PT_SEGNO="+cp_ToStrODBC(this.w_PT_SEGNO)+;
                     ",PTSERRIF="+cp_ToStrODBCNull(this.w_PTSERRIF)+;
                     ",PTTOTIMP="+cp_ToStrODBC(this.w_PTTOTIMP)+;
                     ",PTFLSOSP="+cp_ToStrODBC(this.w_PTFLSOSP)+;
                     ",PTMODPAG="+cp_ToStrODBCNull(this.w_PTMODPAG)+;
                     ",PTBANAPP="+cp_ToStrODBCNull(this.w_PTBANAPP)+;
                     ",PTNUMDOC="+cp_ToStrODBC(this.w_PTNUMDOC)+;
                     ",PTALFDOC="+cp_ToStrODBC(this.w_PTALFDOC)+;
                     ",PTDATDOC="+cp_ToStrODBC(this.w_PTDATDOC)+;
                     ",PTIMPDOC="+cp_ToStrODBC(this.w_PTIMPDOC)+;
                     ",PTCODVAL="+cp_ToStrODBCNull(this.w_PTCODVAL)+;
                     ",PTCAOVAL="+cp_ToStrODBC(this.w_PTCAOVAL)+;
                     ",PTDATAPE="+cp_ToStrODBC(this.w_PTDATAPE)+;
                     ",PTCAOAPE="+cp_ToStrODBC(this.w_PTCAOAPE)+;
                     ",PTBANNOS="+cp_ToStrODBCNull(this.w_PTBANNOS)+;
                     ",PTNUMEFF="+cp_ToStrODBC(this.w_PTNUMEFF)+;
                     ",PTFLRAGG="+cp_ToStrODBC(this.w_PTFLRAGG)+;
                     ",PTTOTABB="+cp_ToStrODBC(this.w_PTTOTABB)+;
                     ",PTFLCRSA="+cp_ToStrODBC(this.w_PTFLCRSA)+;
                     ",PTNUMDIS="+cp_ToStrODBC(this.w_PTNUMDIS)+;
                     ",PTDESRIG="+cp_ToStrODBC(this.w_PTDESRIG)+;
                     ",PTORDRIF="+cp_ToStrODBC(this.w_PTORDRIF)+;
                     ",PTNUMRIF="+cp_ToStrODBC(this.w_PTNUMRIF)+;
                     ",PTCODAGE="+cp_ToStrODBC(this.w_PTCODAGE)+;
                     ",PTDATINT="+cp_ToStrODBC(this.w_PTDATINT)+;
                     ",PTFLVABD="+cp_ToStrODBC(this.w_PTFLVABD)+;
                     ",PTDATREG="+cp_ToStrODBC(this.w_PTDATREG)+;
                     ",PTNUMCOR="+cp_ToStrODBCNull(this.w_PTNUMCOR)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'PAR_TITE')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      PTTIPCON=this.w_PTTIPCON;
                     ,PTCODCON=this.w_PTCODCON;
                     ,PTDATSCA=this.w_PTDATSCA;
                     ,PTNUMPAR=this.w_PTNUMPAR;
                     ,PT_SEGNO=this.w_PT_SEGNO;
                     ,PTSERRIF=this.w_PTSERRIF;
                     ,PTTOTIMP=this.w_PTTOTIMP;
                     ,PTFLSOSP=this.w_PTFLSOSP;
                     ,PTMODPAG=this.w_PTMODPAG;
                     ,PTBANAPP=this.w_PTBANAPP;
                     ,PTNUMDOC=this.w_PTNUMDOC;
                     ,PTALFDOC=this.w_PTALFDOC;
                     ,PTDATDOC=this.w_PTDATDOC;
                     ,PTIMPDOC=this.w_PTIMPDOC;
                     ,PTCODVAL=this.w_PTCODVAL;
                     ,PTCAOVAL=this.w_PTCAOVAL;
                     ,PTDATAPE=this.w_PTDATAPE;
                     ,PTCAOAPE=this.w_PTCAOAPE;
                     ,PTBANNOS=this.w_PTBANNOS;
                     ,PTNUMEFF=this.w_PTNUMEFF;
                     ,PTFLRAGG=this.w_PTFLRAGG;
                     ,PTTOTABB=this.w_PTTOTABB;
                     ,PTFLCRSA=this.w_PTFLCRSA;
                     ,PTNUMDIS=this.w_PTNUMDIS;
                     ,PTDESRIG=this.w_PTDESRIG;
                     ,PTORDRIF=this.w_PTORDRIF;
                     ,PTNUMRIF=this.w_PTNUMRIF;
                     ,PTCODAGE=this.w_PTCODAGE;
                     ,PTDATINT=this.w_PTDATINT;
                     ,PTFLVABD=this.w_PTFLVABD;
                     ,PTDATREG=this.w_PTDATREG;
                     ,PTNUMCOR=this.w_PTNUMCOR;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_PTDATSCA) AND t_PTTOTIMP<>0 AND NOT EMPTY(t_PTNUMPAR)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete PAR_TITE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_PTDATSCA) AND t_PTTOTIMP<>0 AND NOT EMPTY(t_PTNUMPAR)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAR_TITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_TITE_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
          .w_OBTEST = i_DATSYS
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .DoRTCalc(6,14,.t.)
          .link_1_21('Full')
        .DoRTCalc(16,21,.t.)
          .link_1_35('Full')
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        .DoRTCalc(23,33,.t.)
          .link_2_6('Full')
          .link_2_7('Full')
        .DoRTCalc(36,44,.t.)
          .link_2_17('Full')
        .DoRTCalc(46,49,.t.)
          .link_2_22('Full')
        .DoRTCalc(51,55,.t.)
          .w_IMPDAR = IIF(.w_PT_SEGNO='D', .w_PTTOTIMP,0)
          .w_IMPAVE = IIF(.w_PT_SEGNO='A', .w_PTTOTIMP,0)
        .DoRTCalc(58,59,.t.)
          .w_CODDIS = IIF(.w_PTROWORD=-2, .w_PTSERIAL, .w_PTNUMDIS)
          .link_2_34('Full')
        .DoRTCalc(61,70,.t.)
        if .o_PTTOTIMP<>.w_PTTOTIMP.or. .o_PT_SEGNO<>.w_PT_SEGNO
          .w_TOTRIG = .w_TOTRIG-.w_rig
          .w_RIG = IIF(.w_PT_SEGNO='A',1,-1 )*IIF( .w_PTCODVAL=.w_VALNAZ , .w_PTTOTIMP , cp_ROUND(VAL2MON(.w_PTTOTIMP, .w_PTCAOVAL, .w_CAONAZ, .w_PTDATSCA, .w_VALNAZ), .w_DECTOP))
          .w_TOTRIG = .w_TOTRIG+.w_rig
        endif
          .w_NUMEFF = IIF(.w_PTNUMEFF<>0,.w_PTNUMEFF,.w_NUMEFF)
        .DoRTCalc(73,74,.t.)
        if .o_PTBANAPP<>.w_PTBANAPP
          .link_2_49('Full')
        endif
        .DoRTCalc(76,93,.t.)
          .link_2_61('Full')
        * --- Area Manuale = Calculate
        * --- gste_mmp
        * Non posso eseguire la totalize su una funzione abs
        * per cui eseguo qui la ABS del totale del campo visualizzato
        this.w_TOTALE= Abs( this.w_TOTRIG )
        
        * Determino il segno in base al totale
        this.w_SEG=IIF( this.w_TOTRIG>0 , 'A', 'D')
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(95,100,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_SEGNO with this.w_SEGNO
      replace t_PTSERRIF with this.w_PTSERRIF
      replace t_CAUSALE with this.w_CAUSALE
      replace t_ESIDETT with this.w_ESIDETT
      replace t_PTNUMDOC with this.w_PTNUMDOC
      replace t_PTALFDOC with this.w_PTALFDOC
      replace t_PTDATDOC with this.w_PTDATDOC
      replace t_PTIMPDOC with this.w_PTIMPDOC
      replace t_PTCODVAL with this.w_PTCODVAL
      replace t_DESVAL with this.w_DESVAL
      replace t_PTCAOVAL with this.w_PTCAOVAL
      replace t_PTDATAPE with this.w_PTDATAPE
      replace t_PTCAOAPE with this.w_PTCAOAPE
      replace t_PTBANNOS with this.w_PTBANNOS
      replace t_PTNUMEFF with this.w_PTNUMEFF
      replace t_PTFLRAGG with this.w_PTFLRAGG
      replace t_PTTOTABB with this.w_PTTOTABB
      replace t_PTFLCRSA with this.w_PTFLCRSA
      replace t_DECTOT with this.w_DECTOT
      replace t_IMPDAR with this.w_IMPDAR
      replace t_IMPAVE with this.w_IMPAVE
      replace t_DECUNI with this.w_DECUNI
      replace t_PTNUMDIS with this.w_PTNUMDIS
      replace t_CODDIS with this.w_CODDIS
      replace t_NUMDIS with this.w_NUMDIS
      replace t_ANNDIS with this.w_ANNDIS
      replace t_DATDIS with this.w_DATDIS
      replace t_TIPDIS with this.w_TIPDIS
      replace t_PTORDRIF with this.w_PTORDRIF
      replace t_PTNUMRIF with this.w_PTNUMRIF
      replace t_PTCODAGE with this.w_PTCODAGE
      replace t_PTDATINT with this.w_PTDATINT
      replace t_PTFLVABD with this.w_PTFLVABD
      replace t_RIG with this.w_RIG
      replace t_NUMEFF with this.w_NUMEFF
      replace t_FLDEFI with this.w_FLDEFI
      replace t_PTDATREG with this.w_PTDATREG
      replace t_DATSCA with this.w_DATSCA
      replace t_TOTIMP with this.w_TOTIMP
      replace t_OTOTIMP with this.w_OTOTIMP
      replace t_TIPPAG with this.w_TIPPAG
      replace t_DVSERIAL with this.w_DVSERIAL
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_13.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_41.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_42.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_44.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPTCODCON_1_4.enabled = this.oPgFrm.Page1.oPag.oPTCODCON_1_4.mCond()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPTNUMPAR_2_4.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPTNUMPAR_2_4.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPT_SEGNO_2_5.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPT_SEGNO_2_5.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPTTOTIMP_2_9.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPTTOTIMP_2_9.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPTFLSOSP_2_10.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPTFLSOSP_2_10.mCond()
    this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPTBANAPP_2_12.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oBody.oBodycol.oRow.oPTBANAPP_2_12.mCond()
    this.oPgFrm.Page1.oPag.oPTDESRIG_2_39.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPTDESRIG_2_39.mCond()
    this.oPgFrm.Page1.oPag.oPTNUMCOR_2_49.enabled = inlist(this.cFunction,"Edit","Load") and this.oPgFrm.Page1.oPag.oPTNUMCOR_2_49.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_27.enabled =this.oPgFrm.Page1.oPag.oBtn_2_27.mCond()
    this.oPgFrm.Page1.oPag.oBtn_2_31.enabled =this.oPgFrm.Page1.oPag.oBtn_2_31.mCond()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPTCODCON_1_4.visible=!this.oPgFrm.Page1.oPag.oPTCODCON_1_4.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_8.visible=!this.oPgFrm.Page1.oPag.oStr_1_8.mHide()
    this.oPgFrm.Page1.oPag.oDESCON_1_9.visible=!this.oPgFrm.Page1.oPag.oDESCON_1_9.mHide()
    this.oPgFrm.Page1.oPag.oIMPRIG_1_10.visible=!this.oPgFrm.Page1.oPag.oIMPRIG_1_10.mHide()
    this.oPgFrm.Page1.oPag.oCODICE_1_17.visible=!this.oPgFrm.Page1.oPag.oCODICE_1_17.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    this.oPgFrm.Page1.oPag.oNUMRER_1_19.visible=!this.oPgFrm.Page1.oPag.oNUMRER_1_19.mHide()
    this.oPgFrm.Page1.oPag.oCODCAU_1_21.visible=!this.oPgFrm.Page1.oPag.oCODCAU_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_22.visible=!this.oPgFrm.Page1.oPag.oStr_1_22.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    this.oPgFrm.Page1.oPag.oFLDAVE_1_25.visible=!this.oPgFrm.Page1.oPag.oFLDAVE_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_26.visible=!this.oPgFrm.Page1.oPag.oStr_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_27.visible=!this.oPgFrm.Page1.oPag.oStr_1_27.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oBANRIF_1_35.visible=!this.oPgFrm.Page1.oPag.oBANRIF_1_35.mHide()
    this.oPgFrm.Page1.oPag.oDESBAN_1_36.visible=!this.oPgFrm.Page1.oPag.oDESBAN_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_53.visible=!this.oPgFrm.Page1.oPag.oBtn_1_53.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_13.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_41.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_42.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_44.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_47.Event(cEvent)
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PTCODCON
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTCODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_API',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_PTCODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PTTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_PTTIPCON;
                     ,'ANCODICE',trim(this.w_PTCODCON))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PTCODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PTCODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oPTCODCON_1_4'),i_cWhere,'GSAR_API',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PTTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_PTTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTCODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_PTCODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_PTTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_PTTIPCON;
                       ,'ANCODICE',this.w_PTCODCON)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTCODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_PTCODCON = space(15)
      endif
      this.w_DESCON = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTCODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CONTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.ANCODICE as ANCODICE104"+ ",link_1_4.ANDESCRI as ANDESCRI104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on PAR_TITE.PTCODCON=link_1_4.ANCODICE"+" and PAR_TITE.PTTIPCON=link_1_4.ANTIPCON"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and PAR_TITE.PTCODCON=link_1_4.ANCODICE(+)"'+'+" and PAR_TITE.PTTIPCON=link_1_4.ANTIPCON(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=CODCAU
  func Link_1_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCAU) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCAU)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCFLINSO,CCFLIVDF,CCFLPART";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CODCAU);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CODCAU)
            select CCCODICE,CCFLINSO,CCFLIVDF,CCFLPART;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCAU = NVL(_Link_.CCCODICE,space(5))
      this.w_FLINSO = NVL(_Link_.CCFLINSO,space(1))
      this.w_FLIVDF = NVL(_Link_.CCFLIVDF,space(1))
      this.w_FLPART = NVL(_Link_.CCFLPART,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCAU = space(5)
      endif
      this.w_FLINSO = space(1)
      this.w_FLIVDF = space(1)
      this.w_FLPART = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCAU Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BANRIF
  func Link_1_35(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BANRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BANRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_BANRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_BANRIF)
            select BACODBAN,BADESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BANRIF = NVL(_Link_.BACODBAN,space(15))
      this.w_DESBAN = NVL(_Link_.BADESCRI,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_BANRIF = space(15)
      endif
      this.w_DESBAN = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BANRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PTSERRIF
  func Link_2_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PNT_MAST_IDX,3]
    i_lTable = "PNT_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2], .t., this.PNT_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTSERRIF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTSERRIF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PNSERIAL,PNCODCAU";
                   +" from "+i_cTable+" "+i_lTable+" where PNSERIAL="+cp_ToStrODBC(this.w_PTSERRIF);
                   +" and PNSERIAL="+cp_ToStrODBC(this.w_PTSERRIF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PNSERIAL',this.w_PTSERRIF;
                       ,'PNSERIAL',this.w_PTSERRIF)
            select PNSERIAL,PNCODCAU;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTSERRIF = NVL(_Link_.PNSERIAL,space(10))
      this.w_CAUSALE = NVL(_Link_.PNCODCAU,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_PTSERRIF = space(10)
      endif
      this.w_CAUSALE = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PNT_MAST_IDX,2])+'\'+cp_ToStr(_Link_.PNSERIAL,1)+'\'+cp_ToStr(_Link_.PNSERIAL,1)
      cp_ShowWarn(i_cKey,this.PNT_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTSERRIF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CAUSALE
  func Link_2_7(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAU_CONT_IDX,3]
    i_lTable = "CAU_CONT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2], .t., this.CAU_CONT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUSALE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUSALE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCCODICE,CCFLIVDF";
                   +" from "+i_cTable+" "+i_lTable+" where CCCODICE="+cp_ToStrODBC(this.w_CAUSALE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCCODICE',this.w_CAUSALE)
            select CCCODICE,CCFLIVDF;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUSALE = NVL(_Link_.CCCODICE,space(5))
      this.w_ESIDETT = NVL(_Link_.CCFLIVDF,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUSALE = space(5)
      endif
      this.w_ESIDETT = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAU_CONT_IDX,2])+'\'+cp_ToStr(_Link_.CCCODICE,1)
      cp_ShowWarn(i_cKey,this.CAU_CONT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUSALE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PTMODPAG
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MOD_PAGA_IDX,3]
    i_lTable = "MOD_PAGA"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2], .t., this.MOD_PAGA_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTMODPAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMP',True,'MOD_PAGA')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MPCODICE like "+cp_ToStrODBC(trim(this.w_PTMODPAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MPCODICE,MPTIPPAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MPCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MPCODICE',trim(this.w_PTMODPAG))
          select MPCODICE,MPTIPPAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MPCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PTMODPAG)==trim(_Link_.MPCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PTMODPAG) and !this.bDontReportError
            deferred_cp_zoom('MOD_PAGA','*','MPCODICE',cp_AbsName(oSource.parent,'oPTMODPAG_2_11'),i_cWhere,'GSAR_AMP',"Tipi di pagamento",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MPTIPPAG";
                     +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',oSource.xKey(1))
            select MPCODICE,MPTIPPAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTMODPAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MPCODICE,MPTIPPAG";
                   +" from "+i_cTable+" "+i_lTable+" where MPCODICE="+cp_ToStrODBC(this.w_PTMODPAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MPCODICE',this.w_PTMODPAG)
            select MPCODICE,MPTIPPAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTMODPAG = NVL(_Link_.MPCODICE,space(10))
      this.w_TIPPAG = NVL(_Link_.MPTIPPAG,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_PTMODPAG = space(10)
      endif
      this.w_TIPPAG = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])+'\'+cp_ToStr(_Link_.MPCODICE,1)
      cp_ShowWarn(i_cKey,this.MOD_PAGA_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTMODPAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_11(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MOD_PAGA_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MOD_PAGA_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_11.MPCODICE as MPCODICE211"+ ",link_2_11.MPTIPPAG as MPTIPPAG211"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_11 on PAR_TITE.PTMODPAG=link_2_11.MPCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_11"
          i_cKey=i_cKey+'+" and PAR_TITE.PTMODPAG=link_2_11.MPCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PTBANAPP
  func Link_2_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTBANAPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABA',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_PTBANAPP)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_PTBANAPP))
          select BACODBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PTBANAPP)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PTBANAPP) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oPTBANAPP_2_12'),i_cWhere,'GSAR_ABA',"Elenco banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTBANAPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_PTBANAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_PTBANAPP)
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTBANAPP = NVL(_Link_.BACODBAN,space(10))
    else
      if i_cCtrl<>'Load'
        this.w_PTBANAPP = space(10)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTBANAPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PTCODVAL
  func Link_2_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT,VADECUNI";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PTCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PTCODVAL)
            select VACODVAL,VADESVAL,VASIMVAL,VADECTOT,VADECUNI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_SIMBVA = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
      this.w_DECUNI = NVL(_Link_.VADECUNI,0)
    else
      if i_cCtrl<>'Load'
        this.w_PTCODVAL = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_SIMBVA = space(5)
      this.w_DECTOT = 0
      this.w_DECUNI = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_17(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_17.VACODVAL as VACODVAL217"+ ",link_2_17.VADESVAL as VADESVAL217"+ ",link_2_17.VASIMVAL as VASIMVAL217"+ ",link_2_17.VADECTOT as VADECTOT217"+ ",link_2_17.VADECUNI as VADECUNI217"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_17 on PAR_TITE.PTCODVAL=link_2_17.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_17"
          i_cKey=i_cKey+'+" and PAR_TITE.PTCODVAL=link_2_17.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PTBANNOS
  func Link_2_22(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTBANNOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTBANNOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_PTBANNOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_PTBANNOS)
            select BACODBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTBANNOS = NVL(_Link_.BACODBAN,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_PTBANNOS = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTBANNOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODDIS
  func Link_2_34(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DIS_TINT_IDX,3]
    i_lTable = "DIS_TINT"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2], .t., this.DIS_TINT_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODDIS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODDIS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DINUMDIS,DINUMERO,DI__ANNO,DIDATDIS,DITIPDIS";
                   +" from "+i_cTable+" "+i_lTable+" where DINUMDIS="+cp_ToStrODBC(this.w_CODDIS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DINUMDIS',this.w_CODDIS)
            select DINUMDIS,DINUMERO,DI__ANNO,DIDATDIS,DITIPDIS;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODDIS = NVL(_Link_.DINUMDIS,space(10))
      this.w_NUMDIS = NVL(_Link_.DINUMERO,0)
      this.w_ANNDIS = NVL(_Link_.DI__ANNO,space(4))
      this.w_DATDIS = NVL(cp_ToDate(_Link_.DIDATDIS),ctod("  /  /  "))
      this.w_TIPDIS = NVL(_Link_.DITIPDIS,space(2))
    else
      if i_cCtrl<>'Load'
        this.w_CODDIS = space(10)
      endif
      this.w_NUMDIS = 0
      this.w_ANNDIS = space(4)
      this.w_DATDIS = ctod("  /  /  ")
      this.w_TIPDIS = space(2)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DIS_TINT_IDX,2])+'\'+cp_ToStr(_Link_.DINUMDIS,1)
      cp_ShowWarn(i_cKey,this.DIS_TINT_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODDIS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=PTNUMCOR
  func Link_2_49(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CONTI_IDX,3]
    i_lTable = "BAN_CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2], .t., this.BAN_CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PTNUMCOR) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'BAN_CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CCCONCOR like "+cp_ToStrODBC(trim(this.w_PTNUMCOR)+"%");
                   +" and CCTIPCON="+cp_ToStrODBC(this.w_PTTIPCON);
                   +" and CCCODCON="+cp_ToStrODBC(this.w_PTCODCON);
                   +" and CCCODBAN="+cp_ToStrODBC(this.w_PTBANAPP);

          i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CCTIPCON',this.w_PTTIPCON;
                     ,'CCCODCON',this.w_PTCODCON;
                     ,'CCCODBAN',this.w_PTBANAPP;
                     ,'CCCONCOR',trim(this.w_PTNUMCOR))
          select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PTNUMCOR)==trim(_Link_.CCCONCOR) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_PTNUMCOR) and !this.bDontReportError
            deferred_cp_zoom('BAN_CONTI','*','CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR',cp_AbsName(oSource.parent,'oPTNUMCOR_2_49'),i_cWhere,'',"Elenco conti correnti",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_PTTIPCON<>oSource.xKey(1);
           .or. this.w_PTCODCON<>oSource.xKey(2);
           .or. this.w_PTBANAPP<>oSource.xKey(3);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                     +" from "+i_cTable+" "+i_lTable+" where CCCONCOR="+cp_ToStrODBC(oSource.xKey(4));
                     +" and CCTIPCON="+cp_ToStrODBC(this.w_PTTIPCON);
                     +" and CCCODCON="+cp_ToStrODBC(this.w_PTCODCON);
                     +" and CCCODBAN="+cp_ToStrODBC(this.w_PTBANAPP);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPCON',oSource.xKey(1);
                       ,'CCCODCON',oSource.xKey(2);
                       ,'CCCODBAN',oSource.xKey(3);
                       ,'CCCONCOR',oSource.xKey(4))
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PTNUMCOR)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR";
                   +" from "+i_cTable+" "+i_lTable+" where CCCONCOR="+cp_ToStrODBC(this.w_PTNUMCOR);
                   +" and CCTIPCON="+cp_ToStrODBC(this.w_PTTIPCON);
                   +" and CCCODCON="+cp_ToStrODBC(this.w_PTCODCON);
                   +" and CCCODBAN="+cp_ToStrODBC(this.w_PTBANAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CCTIPCON',this.w_PTTIPCON;
                       ,'CCCODCON',this.w_PTCODCON;
                       ,'CCCODBAN',this.w_PTBANAPP;
                       ,'CCCONCOR',this.w_PTNUMCOR)
            select CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PTNUMCOR = NVL(_Link_.CCCONCOR,space(25))
    else
      if i_cCtrl<>'Load'
        this.w_PTNUMCOR = space(25)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CONTI_IDX,2])+'\'+cp_ToStr(_Link_.CCTIPCON,1)+'\'+cp_ToStr(_Link_.CCCODCON,1)+'\'+cp_ToStr(_Link_.CCCODBAN,1)+'\'+cp_ToStr(_Link_.CCCONCOR,1)
      cp_ShowWarn(i_cKey,this.BAN_CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PTNUMCOR Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=DASERIAL
  func Link_2_61(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DATI_AGG_IDX,3]
    i_lTable = "DATI_AGG"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2], .t., this.DATI_AGG_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_DASERIAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_DASERIAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DASERIAL,DACAM_01,DACAM_02,DACAM_03,DACAM_04,DACAM_05,DACAM_06";
                   +" from "+i_cTable+" "+i_lTable+" where DASERIAL="+cp_ToStrODBC(this.w_DASERIAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DASERIAL',this.w_DASERIAL)
            select DASERIAL,DACAM_01,DACAM_02,DACAM_03,DACAM_04,DACAM_05,DACAM_06;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_DASERIAL = NVL(_Link_.DASERIAL,space(10))
      this.w_DACAM_01 = NVL(_Link_.DACAM_01,space(30))
      this.w_DACAM_02 = NVL(_Link_.DACAM_02,space(30))
      this.w_DACAM_03 = NVL(_Link_.DACAM_03,space(30))
      this.w_DACAM_04 = NVL(_Link_.DACAM_04,space(30))
      this.w_DACAM_05 = NVL(_Link_.DACAM_05,space(30))
      this.w_DACAM_06 = NVL(_Link_.DACAM_06,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_DASERIAL = space(10)
      endif
      this.w_DACAM_01 = space(30)
      this.w_DACAM_02 = space(30)
      this.w_DACAM_03 = space(30)
      this.w_DACAM_04 = space(30)
      this.w_DACAM_05 = space(30)
      this.w_DACAM_06 = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DATI_AGG_IDX,2])+'\'+cp_ToStr(_Link_.DASERIAL,1)
      cp_ShowWarn(i_cKey,this.DATI_AGG_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_DASERIAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oPTCODCON_1_4.value==this.w_PTCODCON)
      this.oPgFrm.Page1.oPag.oPTCODCON_1_4.value=this.w_PTCODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_9.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_9.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oIMPRIG_1_10.value==this.w_IMPRIG)
      this.oPgFrm.Page1.oPag.oIMPRIG_1_10.value=this.w_IMPRIG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_14.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_14.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODICE_1_17.value==this.w_CODICE)
      this.oPgFrm.Page1.oPag.oCODICE_1_17.value=this.w_CODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMRER_1_19.value==this.w_NUMRER)
      this.oPgFrm.Page1.oPag.oNUMRER_1_19.value=this.w_NUMRER
    endif
    if not(this.oPgFrm.Page1.oPag.oDATREG_1_20.value==this.w_DATREG)
      this.oPgFrm.Page1.oPag.oDATREG_1_20.value=this.w_DATREG
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCAU_1_21.value==this.w_CODCAU)
      this.oPgFrm.Page1.oPag.oCODCAU_1_21.value=this.w_CODCAU
    endif
    if not(this.oPgFrm.Page1.oPag.oFLDAVE_1_25.value==this.w_FLDAVE)
      this.oPgFrm.Page1.oPag.oFLDAVE_1_25.value=this.w_FLDAVE
    endif
    if not(this.oPgFrm.Page1.oPag.oBANRIF_1_35.value==this.w_BANRIF)
      this.oPgFrm.Page1.oPag.oBANRIF_1_35.value=this.w_BANRIF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESBAN_1_36.value==this.w_DESBAN)
      this.oPgFrm.Page1.oPag.oDESBAN_1_36.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page1.oPag.oPTDESRIG_2_39.value==this.w_PTDESRIG)
      this.oPgFrm.Page1.oPag.oPTDESRIG_2_39.value=this.w_PTDESRIG
      replace t_PTDESRIG with this.oPgFrm.Page1.oPag.oPTDESRIG_2_39.value
    endif
    if not(this.oPgFrm.Page1.oPag.oPTNUMCOR_2_49.value==this.w_PTNUMCOR)
      this.oPgFrm.Page1.oPag.oPTNUMCOR_2_49.value=this.w_PTNUMCOR
      replace t_PTNUMCOR with this.oPgFrm.Page1.oPag.oPTNUMCOR_2_49.value
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTALE_3_1.value==this.w_TOTALE)
      this.oPgFrm.Page1.oPag.oTOTALE_3_1.value=this.w_TOTALE
    endif
    if not(this.oPgFrm.Page1.oPag.oSEG_3_2.value==this.w_SEG)
      this.oPgFrm.Page1.oPag.oSEG_3_2.value=this.w_SEG
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMNAZ_3_3.value==this.w_SIMNAZ)
      this.oPgFrm.Page1.oPag.oSIMNAZ_3_3.value=this.w_SIMNAZ
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTDATSCA_2_1.value==this.w_PTDATSCA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTDATSCA_2_1.value=this.w_PTDATSCA
      replace t_PTDATSCA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTDATSCA_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSIMBVA_2_3.value==this.w_SIMBVA)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSIMBVA_2_3.value=this.w_SIMBVA
      replace t_SIMBVA with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oSIMBVA_2_3.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTNUMPAR_2_4.value==this.w_PTNUMPAR)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTNUMPAR_2_4.value=this.w_PTNUMPAR
      replace t_PTNUMPAR with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTNUMPAR_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPT_SEGNO_2_5.value==this.w_PT_SEGNO)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPT_SEGNO_2_5.value=this.w_PT_SEGNO
      replace t_PT_SEGNO with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPT_SEGNO_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTTOTIMP_2_9.value==this.w_PTTOTIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTTOTIMP_2_9.value=this.w_PTTOTIMP
      replace t_PTTOTIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTTOTIMP_2_9.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTFLSOSP_2_10.RadioValue()==this.w_PTFLSOSP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTFLSOSP_2_10.SetRadio()
      replace t_PTFLSOSP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTFLSOSP_2_10.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTMODPAG_2_11.value==this.w_PTMODPAG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTMODPAG_2_11.value=this.w_PTMODPAG
      replace t_PTMODPAG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTMODPAG_2_11.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANAPP_2_12.value==this.w_PTBANAPP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANAPP_2_12.value=this.w_PTBANAPP
      replace t_PTBANAPP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTBANAPP_2_12.value
    endif
    cp_SetControlsValueExtFlds(this,'PAR_TITE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    local i_oldarea
    i_oldarea=select()
    select count(*) as cnt from (this.cTrsName);
       where not(deleted()) and (NOT EMPTY(t_PTDATSCA) AND t_PTTOTIMP<>0 AND NOT EMPTY(t_PTNUMPAR));
        into cursor __chk__
    if not(max(0,1)<=cnt)
      do cp_ErrorMsg with MSG_YOU_MUST_INSERT_AT_LEAST_ONE_ROW
      use
      select (i_oldarea)
      return(.f.)
    endif
    use
    select (i_oldarea)
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case   empty(.w_PTNUMPAR) and (.w_FLINSO<>'S' and .w_FLIVDF <>'S') and (NOT EMPTY(.w_PTDATSCA) AND .w_PTTOTIMP<>0 AND NOT EMPTY(.w_PTNUMPAR))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTNUMPAR_2_4
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
        case   not(.w_PT_SEGNO $ "DA") and (Empty(.w_DVSERIAL) AND .w_FLINSO<>'S' and .w_FLIVDF <>'S') and (NOT EMPTY(.w_PTDATSCA) AND .w_PTTOTIMP<>0 AND NOT EMPTY(.w_PTNUMPAR))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPT_SEGNO_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Inserire segno!: Dare/avere")
        case   not(.w_PTTOTIMP>0) and (Empty(.w_DVSERIAL)  AND .w_FLINSO<>'S' and .w_FLIVDF <>'S') and (NOT EMPTY(.w_PTDATSCA) AND .w_PTTOTIMP<>0 AND NOT EMPTY(.w_PTNUMPAR))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTTOTIMP_2_9
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Attenzione, impossibile inserire importi negativi")
        case   empty(.w_PTMODPAG) and (NOT EMPTY(.w_PTDATSCA) AND .w_PTTOTIMP<>0 AND NOT EMPTY(.w_PTNUMPAR))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTMODPAG_2_11
          i_bRes = .f.
        i_bnoObbl = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Inserire tipo pagamento")
      endcase
      if NOT EMPTY(.w_PTDATSCA) AND .w_PTTOTIMP<>0 AND NOT EMPTY(.w_PTNUMPAR)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SEGNO = this.w_SEGNO
    this.o_PT_SEGNO = this.w_PT_SEGNO
    this.o_PTTOTIMP = this.w_PTTOTIMP
    this.o_PTBANAPP = this.w_PTBANAPP
    this.o_TOTRIG = this.w_TOTRIG
    return


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_PTDATSCA) AND t_PTTOTIMP<>0 AND NOT EMPTY(t_PTNUMPAR))
    select(i_nArea)
    return(i_bRes)
  Endfunc
  Func CanDeleteRow()
    local i_bRes
    i_bRes=Empty(t_DVSERIAL)
    if !i_bRes
      cp_ErrorMsg(" Attenzione, partita associata ad una valutazione att.\pass. in divisa, non eliminabile.","stop","")
    endif
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_PTDATSCA=ctod("  /  /  ")
      .w_SEGNO=space(1)
      .w_SIMBVA=space(5)
      .w_PTNUMPAR=space(31)
      .w_PT_SEGNO=space(1)
      .w_PTSERRIF=space(10)
      .w_CAUSALE=space(5)
      .w_ESIDETT=space(1)
      .w_PTTOTIMP=0
      .w_PTFLSOSP=space(1)
      .w_PTMODPAG=space(10)
      .w_PTBANAPP=space(10)
      .w_PTNUMDOC=0
      .w_PTALFDOC=space(10)
      .w_PTDATDOC=ctod("  /  /  ")
      .w_PTIMPDOC=0
      .w_PTCODVAL=space(3)
      .w_DESVAL=space(35)
      .w_PTCAOVAL=0
      .w_PTDATAPE=ctod("  /  /  ")
      .w_PTCAOAPE=0
      .w_PTBANNOS=space(15)
      .w_PTNUMEFF=0
      .w_PTFLRAGG=space(1)
      .w_PTTOTABB=0
      .w_PTFLCRSA=space(1)
      .w_DECTOT=0
      .w_IMPDAR=0
      .w_IMPAVE=0
      .w_DECUNI=0
      .w_PTNUMDIS=space(10)
      .w_CODDIS=space(10)
      .w_NUMDIS=0
      .w_ANNDIS=space(4)
      .w_DATDIS=ctod("  /  /  ")
      .w_TIPDIS=space(2)
      .w_PTDESRIG=space(50)
      .w_PTORDRIF=0
      .w_PTNUMRIF=0
      .w_PTCODAGE=space(5)
      .w_PTDATINT=ctod("  /  /  ")
      .w_PTFLVABD=space(1)
      .w_RIG=0
      .w_NUMEFF=0
      .w_FLDEFI=space(1)
      .w_PTDATREG=ctod("  /  /  ")
      .w_PTNUMCOR=space(25)
      .w_DATSCA=ctod("  /  /  ")
      .w_TOTIMP=0
      .w_OTOTIMP=0
      .w_TIPPAG=space(2)
      .w_DVSERIAL=space(10)
      .DoRTCalc(1,34,.f.)
      if not(empty(.w_PTSERRIF))
        .link_2_6('Full')
      endif
      .DoRTCalc(35,35,.f.)
      if not(empty(.w_CAUSALE))
        .link_2_7('Full')
      endif
      .DoRTCalc(36,37,.f.)
        .w_PTFLSOSP = ' '
      .DoRTCalc(39,39,.f.)
      if not(empty(.w_PTMODPAG))
        .link_2_11('Full')
      endif
      .DoRTCalc(40,40,.f.)
      if not(empty(.w_PTBANAPP))
        .link_2_12('Full')
      endif
      .DoRTCalc(41,44,.f.)
        .w_PTCODVAL = .w_CODVAL
      .DoRTCalc(45,45,.f.)
      if not(empty(.w_PTCODVAL))
        .link_2_17('Full')
      endif
      .DoRTCalc(46,46,.f.)
        .w_PTCAOVAL = .w_CAOVAL
      .DoRTCalc(48,50,.f.)
      if not(empty(.w_PTBANNOS))
        .link_2_22('Full')
      endif
      .DoRTCalc(51,53,.f.)
        .w_PTFLCRSA = IIF(.w_PTROWORD<1,'C',.w_FLPART)
      .DoRTCalc(55,55,.f.)
        .w_IMPDAR = IIF(.w_PT_SEGNO='D', .w_PTTOTIMP,0)
        .w_IMPAVE = IIF(.w_PT_SEGNO='A', .w_PTTOTIMP,0)
      .DoRTCalc(58,59,.f.)
        .w_CODDIS = IIF(.w_PTROWORD=-2, .w_PTSERIAL, .w_PTNUMDIS)
      .DoRTCalc(60,60,.f.)
      if not(empty(.w_CODDIS))
        .link_2_34('Full')
      endif
      .DoRTCalc(61,64,.f.)
        .w_PTDESRIG = .w_DESRIG
      .DoRTCalc(66,70,.f.)
        .w_RIG = IIF(.w_PT_SEGNO='A',1,-1 )*IIF( .w_PTCODVAL=.w_VALNAZ , .w_PTTOTIMP , cp_ROUND(VAL2MON(.w_PTTOTIMP, .w_PTCAOVAL, .w_CAONAZ, .w_PTDATSCA, .w_VALNAZ), .w_DECTOP))
        .w_NUMEFF = IIF(.w_PTNUMEFF<>0,.w_PTNUMEFF,.w_NUMEFF)
      .DoRTCalc(73,73,.f.)
        .w_PTDATREG = .w_DATREG
      .DoRTCalc(75,75,.f.)
      if not(empty(.w_PTNUMCOR))
        .link_2_49('Full')
      endif
      .DoRTCalc(76,77,.f.)
        .w_OTOTIMP = .w_PTTOTIMP
    endwith
    this.DoRTCalc(79,100,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_PTDATSCA = t_PTDATSCA
    this.w_SEGNO = t_SEGNO
    this.w_SIMBVA = t_SIMBVA
    this.w_PTNUMPAR = t_PTNUMPAR
    this.w_PT_SEGNO = t_PT_SEGNO
    this.w_PTSERRIF = t_PTSERRIF
    this.w_CAUSALE = t_CAUSALE
    this.w_ESIDETT = t_ESIDETT
    this.w_PTTOTIMP = t_PTTOTIMP
    this.w_PTFLSOSP = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTFLSOSP_2_10.RadioValue(.t.)
    this.w_PTMODPAG = t_PTMODPAG
    this.w_PTBANAPP = t_PTBANAPP
    this.w_PTNUMDOC = t_PTNUMDOC
    this.w_PTALFDOC = t_PTALFDOC
    this.w_PTDATDOC = t_PTDATDOC
    this.w_PTIMPDOC = t_PTIMPDOC
    this.w_PTCODVAL = t_PTCODVAL
    this.w_DESVAL = t_DESVAL
    this.w_PTCAOVAL = t_PTCAOVAL
    this.w_PTDATAPE = t_PTDATAPE
    this.w_PTCAOAPE = t_PTCAOAPE
    this.w_PTBANNOS = t_PTBANNOS
    this.w_PTNUMEFF = t_PTNUMEFF
    this.w_PTFLRAGG = t_PTFLRAGG
    this.w_PTTOTABB = t_PTTOTABB
    this.w_PTFLCRSA = t_PTFLCRSA
    this.w_DECTOT = t_DECTOT
    this.w_IMPDAR = t_IMPDAR
    this.w_IMPAVE = t_IMPAVE
    this.w_DECUNI = t_DECUNI
    this.w_PTNUMDIS = t_PTNUMDIS
    this.w_CODDIS = t_CODDIS
    this.w_NUMDIS = t_NUMDIS
    this.w_ANNDIS = t_ANNDIS
    this.w_DATDIS = t_DATDIS
    this.w_TIPDIS = t_TIPDIS
    this.w_PTDESRIG = t_PTDESRIG
    this.w_PTORDRIF = t_PTORDRIF
    this.w_PTNUMRIF = t_PTNUMRIF
    this.w_PTCODAGE = t_PTCODAGE
    this.w_PTDATINT = t_PTDATINT
    this.w_PTFLVABD = t_PTFLVABD
    this.w_RIG = t_RIG
    this.w_NUMEFF = t_NUMEFF
    this.w_FLDEFI = t_FLDEFI
    this.w_PTDATREG = t_PTDATREG
    this.w_PTNUMCOR = t_PTNUMCOR
    this.w_DATSCA = t_DATSCA
    this.w_TOTIMP = t_TOTIMP
    this.w_OTOTIMP = t_OTOTIMP
    this.w_TIPPAG = t_TIPPAG
    this.w_DVSERIAL = t_DVSERIAL
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_PTDATSCA with this.w_PTDATSCA
    replace t_SEGNO with this.w_SEGNO
    replace t_SIMBVA with this.w_SIMBVA
    replace t_PTNUMPAR with this.w_PTNUMPAR
    replace t_PT_SEGNO with this.w_PT_SEGNO
    replace t_PTSERRIF with this.w_PTSERRIF
    replace t_CAUSALE with this.w_CAUSALE
    replace t_ESIDETT with this.w_ESIDETT
    replace t_PTTOTIMP with this.w_PTTOTIMP
    replace t_PTFLSOSP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oPTFLSOSP_2_10.ToRadio()
    replace t_PTMODPAG with this.w_PTMODPAG
    replace t_PTBANAPP with this.w_PTBANAPP
    replace t_PTNUMDOC with this.w_PTNUMDOC
    replace t_PTALFDOC with this.w_PTALFDOC
    replace t_PTDATDOC with this.w_PTDATDOC
    replace t_PTIMPDOC with this.w_PTIMPDOC
    replace t_PTCODVAL with this.w_PTCODVAL
    replace t_DESVAL with this.w_DESVAL
    replace t_PTCAOVAL with this.w_PTCAOVAL
    replace t_PTDATAPE with this.w_PTDATAPE
    replace t_PTCAOAPE with this.w_PTCAOAPE
    replace t_PTBANNOS with this.w_PTBANNOS
    replace t_PTNUMEFF with this.w_PTNUMEFF
    replace t_PTFLRAGG with this.w_PTFLRAGG
    replace t_PTTOTABB with this.w_PTTOTABB
    replace t_PTFLCRSA with this.w_PTFLCRSA
    replace t_DECTOT with this.w_DECTOT
    replace t_IMPDAR with this.w_IMPDAR
    replace t_IMPAVE with this.w_IMPAVE
    replace t_DECUNI with this.w_DECUNI
    replace t_PTNUMDIS with this.w_PTNUMDIS
    replace t_CODDIS with this.w_CODDIS
    replace t_NUMDIS with this.w_NUMDIS
    replace t_ANNDIS with this.w_ANNDIS
    replace t_DATDIS with this.w_DATDIS
    replace t_TIPDIS with this.w_TIPDIS
    replace t_PTDESRIG with this.w_PTDESRIG
    replace t_PTORDRIF with this.w_PTORDRIF
    replace t_PTNUMRIF with this.w_PTNUMRIF
    replace t_PTCODAGE with this.w_PTCODAGE
    replace t_PTDATINT with this.w_PTDATINT
    replace t_PTFLVABD with this.w_PTFLVABD
    replace t_RIG with this.w_RIG
    replace t_NUMEFF with this.w_NUMEFF
    replace t_FLDEFI with this.w_FLDEFI
    replace t_PTDATREG with this.w_PTDATREG
    replace t_PTNUMCOR with this.w_PTNUMCOR
    replace t_DATSCA with this.w_DATSCA
    replace t_TOTIMP with this.w_TOTIMP
    replace t_OTOTIMP with this.w_OTOTIMP
    replace t_TIPPAG with this.w_TIPPAG
    replace t_DVSERIAL with this.w_DVSERIAL
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
     with this
        .w_TOTRIG = .w_TOTRIG-.w_rig
        .SetControlsValue()
      endwith
  EndProc
  func CanEdit()
    local i_res
    i_res=this.w_ROWNUM<>0
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Per apportare modifiche alla manutenzione partite occorre utilizzare il bottone 'modifica'. Attenzione, tale bottone � visibile solo quando 'test saldate' � impostato con 'solo parte aperta'."))
    endif
    return(i_res)
  func CanDelete()
    local i_res
    i_res=.F.
    if !i_res
      cp_ErrorMsg(thisform.msgFmt("Per cancellare una riga dalla manutenzione partite occorre utilizzare il bottone 'modifica'. Attenzione, tale bottone � visibile solo quando 'test saldate' � impostato con 'solo parte aperta'."))
    endif
    return(i_res)
enddefine

* --- Define pages as container
define class tgste_mmpPag1 as StdContainer
  Width  = 783
  height = 296
  stdWidth  = 783
  stdheight = 296
  resizeXpos=199
  resizeYpos=169
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPTCODCON_1_4 as StdField with uid="UPDVDYUIKS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PTCODCON", cQueryName = "PTCODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    HelpContextID = 70691516,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=148, Top=36, InputMask=replicate('X',15), cLinkFile="CONTI", cZoomOnZoom="GSAR_API", oKey_1_1="ANTIPCON", oKey_1_2="this.w_PTTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_PTCODCON"

  func oPTCODCON_1_4.mCond()
    with this.Parent.oContained
      if (.cFunction='Query' AND .w_PTROWORD<>-2)
        if .nLastRow>1
          return (.f.)
        endif
      else
        return (.f.)
      endif
    endwith
  endfunc

  func oPTCODCON_1_4.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PTROWORD=-2)
    endwith
    endif
  endfunc

  func oPTCODCON_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
      if .not. empty(.w_PTNUMCOR)
        bRes2=.link_2_49('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oPTCODCON_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  add object oDESCON_1_9 as StdField with uid="RRPAQHTCYG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 143768266,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=283, Top=36, InputMask=replicate('X',40)

  func oDESCON_1_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PTROWORD=-2)
    endwith
    endif
  endfunc

  add object oIMPRIG_1_10 as StdField with uid="VCWJMSBJMV",rtseq=7,rtrep=.f.,;
    cFormVar = "w_IMPRIG", cQueryName = "IMPRIG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo associato alla riga documento",;
    HelpContextID = 266527354,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=283, Top=63, cSayPict="v_PV(40+VVL)", cGetPict="v_PV(40+VVL)"

  func oIMPRIG_1_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PTROWORD=-2)
    endwith
    endif
  endfunc


  add object oObj_1_13 as cp_runprogram with uid="WXVJZGARUJ",left=-4, top=340, width=165,height=22,;
    caption='GSTE_BMP(Load)',;
   bGlobalFont=.t.,;
    prg='GSTE_BMP("LOAD")',;
    cEvent = "Load",;
    nPag=1;
    , HelpContextID = 65494474

  add object oDESCRI_1_14 as StdField with uid="VDAZFMGDKN",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(37), bMultilanguage =  .f.,;
    HelpContextID = 224508618,;
   bGlobalFont=.t.,;
    Height=21, Width=269, Left=451, Top=9, InputMask=replicate('X',37)

  add object oCODICE_1_17 as StdField with uid="LVRQAIJMMY",rtseq=12,rtrep=.f.,;
    cFormVar = "w_CODICE", cQueryName = "CODICE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Codice registrazione scadenza diversa",;
    HelpContextID = 38576346,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=148, Top=9, InputMask=replicate('X',10)

  func oCODICE_1_17.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PTROWORD<>-1)
    endwith
    endif
  endfunc

  add object oNUMRER_1_19 as StdField with uid="JTKKQRHEXF",rtseq=13,rtrep=.f.,;
    cFormVar = "w_NUMRER", cQueryName = "NUMRER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero registrazione",;
    HelpContextID = 86182442,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=148, Top=9

  func oNUMRER_1_19.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PTROWORD=-1)
    endwith
    endif
  endfunc

  add object oDATREG_1_20 as StdField with uid="WUXLGYLOMY",rtseq=14,rtrep=.f.,;
    cFormVar = "w_DATREG", cQueryName = "DATREG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data registrazione",;
    HelpContextID = 2272970,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=283, Top=9

  add object oCODCAU_1_21 as StdField with uid="JFSXIOSKSN",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CODCAU", cQueryName = "CODCAU",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Causale contabile della registrazione di primanota",;
    HelpContextID = 41066714,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=662, Top=36, InputMask=replicate('X',5), cLinkFile="CAU_CONT", oKey_1_1="CCCODICE", oKey_1_2="this.w_CODCAU"

  func oCODCAU_1_21.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PTROWORD<1)
    endwith
    endif
  endfunc

  func oCODCAU_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oFLDAVE_1_25 as StdField with uid="GLHPBPCYJV",rtseq=16,rtrep=.f.,;
    cFormVar = "w_FLDAVE", cQueryName = "FLDAVE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(1), bMultilanguage =  .f.,;
    ToolTipText = "Sezione importo di riga del documento di primanota o scadenza diversa",;
    HelpContextID = 19178410,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=257, Top=63, InputMask=replicate('X',1)

  func oFLDAVE_1_25.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PTROWORD=-2)
    endwith
    endif
  endfunc

  add object oBANRIF_1_35 as StdField with uid="KSTAOOKKUL",rtseq=22,rtrep=.f.,;
    cFormVar = "w_BANRIF", cQueryName = "BANRIF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Banca di riferimento della distinta",;
    HelpContextID = 14880490,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=148, Top=37, InputMask=replicate('X',15), cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_BANRIF"

  func oBANRIF_1_35.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PTROWORD<>-2)
    endwith
    endif
  endfunc

  func oBANRIF_1_35.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
    endwith
    return bRes
  endfunc

  add object oDESBAN_1_36 as StdField with uid="USXYZVDGNP",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 158513866,;
   bGlobalFont=.t.,;
    Height=21, Width=289, Left=283, Top=36, InputMask=replicate('X',35)

  func oDESBAN_1_36.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PTROWORD<>-2)
    endwith
    endif
  endfunc


  add object oObj_1_41 as cp_runprogram with uid="XSJOQKJDAB",left=305, top=340, width=226,height=26,;
    caption='GSTE_BMM(Query)',;
   bGlobalFont=.t.,;
    prg="GSTE_BMM('Query')",;
    cEvent = "Delete row start",;
    nPag=1;
    , HelpContextID = 50526413


  add object oObj_1_42 as cp_runprogram with uid="UGYRJSJBRF",left=305, top=365, width=226,height=26,;
    caption='GSTE_BMM(Edit)',;
   bGlobalFont=.t.,;
    prg="GSTE_BMM('Edit')",;
    cEvent = "ChkFinali",;
    nPag=1;
    , ToolTipText = "Update row start";
    , HelpContextID = 48239821


  add object oObj_1_44 as cp_runprogram with uid="FSOTZJZEAP",left=-4, top=365, width=206,height=22,;
    caption='GSTE_BMP(UPDA)',;
   bGlobalFont=.t.,;
    prg='GSTE_BMP("UPDA")',;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 104219850


  add object oObj_1_47 as cp_runprogram with uid="DHBJMNGUBJ",left=305, top=390, width=226,height=26,;
    caption='GSTE_BMM(Writ)',;
   bGlobalFont=.t.,;
    prg='GSTE_BMM("WRIT")',;
    cEvent = "Record Updated",;
    nPag=1;
    , HelpContextID = 48177869


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=4, top=85, width=779,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=8,Field1="PTDATSCA",Label1="Scadenza",Field2="PTNUMPAR",Label2="Numero partita",Field3="PT_SEGNO",Label3="D/A",Field4="PTTOTIMP",Label4="Importo",Field5="SIMBVA",Label5="Valuta",Field6="PTFLSOSP",Label6="Sosp.",Field7="PTMODPAG",Label7="Pagamento",Field8="PTBANAPP",Label8="Banca appoggio",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 118289786


  add object oBtn_1_53 as StdButton with uid="BJINHDRSKC",left=730, top=7, width=48,height=45,;
    CpPicture="BMP\DETTAGLI.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accedere ai dati aggiuntivi della partita selezionata";
    , HelpContextID = 79696729;
    , TabStop=.f., Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_53.Click()
      do GSCG_KPD with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.bHeaderUpdated=.t.
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_53.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PTROWORD<0)
    endwith
   endif
  endfunc

  add object oStr_1_6 as StdString with uid="JUTHZTJMZJ",Visible=.t., Left=37, Top=37,;
    Alignment=1, Width=109, Height=15,;
    Caption="Cliente:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (.w_PTTIPCON<>'C' OR .w_PTROWORD=-2)
    endwith
  endfunc

  add object oStr_1_7 as StdString with uid="SKSOJBBXXE",Visible=.t., Left=37, Top=37,;
    Alignment=1, Width=109, Height=15,;
    Caption="Fornitore:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (.w_PTTIPCON<>'F' OR .w_PTROWORD=-2)
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="RFVEAHKRSD",Visible=.t., Left=37, Top=37,;
    Alignment=1, Width=109, Height=15,;
    Caption="Conto:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_8.mHide()
    with this.Parent.oContained
      return (.w_PTTIPCON<>'G' OR .w_PTROWORD=-2)
    endwith
  endfunc

  add object oStr_1_16 as StdString with uid="ZKARQKHIEH",Visible=.t., Left=365, Top=9,;
    Alignment=1, Width=82, Height=15,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="QIWHFVEZWC",Visible=.t., Left=34, Top=9,;
    Alignment=1, Width=112, Height=15,;
    Caption="Scadenza diversa:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (.w_PTROWORD<>-1)
    endwith
  endfunc

  add object oStr_1_22 as StdString with uid="SXYEXVCEKM",Visible=.t., Left=13, Top=9,;
    Alignment=1, Width=133, Height=15,;
    Caption="Reg. P.N. numero:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_22.mHide()
    with this.Parent.oContained
      return (.w_PTROWORD<1)
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="AVQMLYKNKJ",Visible=.t., Left=243, Top=9,;
    Alignment=1, Width=38, Height=15,;
    Caption="Del:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_24 as StdString with uid="MVWFQTYDRV",Visible=.t., Left=580, Top=36,;
    Alignment=1, Width=77, Height=15,;
    Caption="Causale:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (.w_PTROWORD<1)
    endwith
  endfunc

  add object oStr_1_26 as StdString with uid="AXBAUYVLXO",Visible=.t., Left=17, Top=63,;
    Alignment=1, Width=234, Height=15,;
    Caption="Importo riga registrazione contabile:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_26.mHide()
    with this.Parent.oContained
      return (.w_PTROWORD<1)
    endwith
  endfunc

  add object oStr_1_27 as StdString with uid="SBHKEISRMK",Visible=.t., Left=32, Top=63,;
    Alignment=1, Width=219, Height=15,;
    Caption="Importo scadenza:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_27.mHide()
    with this.Parent.oContained
      return (.w_PTROWORD<>-1)
    endwith
  endfunc

  add object oStr_1_28 as StdString with uid="VUXWFPPKOB",Visible=.t., Left=119, Top=226,;
    Alignment=1, Width=97, Height=16,;
    Caption="Totale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="UTCDJWQVZE",Visible=.t., Left=34, Top=9,;
    Alignment=1, Width=112, Height=15,;
    Caption="Distinta n.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (.w_PTROWORD<>-2)
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="FKUGYZHSVH",Visible=.t., Left=37, Top=37,;
    Alignment=1, Width=109, Height=15,;
    Caption="Banca pres.:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (.w_PTROWORD<>-2)
    endwith
  endfunc

  add object oStr_1_43 as StdString with uid="BUKAFAOTJY",Visible=.t., Left=2, Top=253,;
    Alignment=1, Width=82, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="QZQFYVBZLI",Visible=.t., Left=565, Top=225,;
    Alignment=1, Width=24, Height=18,;
    Caption="C/C:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-6,top=104,;
    width=775+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-5,top=105,width=774+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*6*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='MOD_PAGA|BAN_CHE|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oPTDESRIG_2_39.Refresh()
      this.Parent.oPTNUMCOR_2_49.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='MOD_PAGA'
        oDropInto=this.oBodyCol.oRow.oPTMODPAG_2_11
      case cFile='BAN_CHE'
        oDropInto=this.oBodyCol.oRow.oPTBANAPP_2_12
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oBtn_2_27 as StdButton with uid="EULHWJBTEB",width=48,height=45,;
   left=733, top=250,;
    CpPicture="BMP\SOLDIT.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere alla situazione della partita selezionata";
    , HelpContextID = 59636438;
    , tabstop=.f., Caption='\<Sit.Partita';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_27.Click()
      do GSCG_KPA with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_27.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_PTNUMPAR))
    endwith
  endfunc

  add object oBtn_2_31 as StdButton with uid="AQPLDQIEMO",width=48,height=45,;
   left=682, top=250,;
    CpPicture="BMP\CONCLUSI.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per accedere ai dati aggiuntivi della partita selezionata";
    , HelpContextID = 79696729;
    , tabstop=.f., Caption='Altri \<Dati';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_31.Click()
      do GSTE_KM2 with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        select (this.parent.oContained.cTrsName)
        if I_SRV=" "
          replace I_SRV with "U"
        endif
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_2_31.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_PTNUMPAR))
    endwith
  endfunc

  add object oPTDESRIG_2_39 as StdTrsField with uid="ENKPRIWFLT",rtseq=65,rtrep=.t.,;
    cFormVar="w_PTDESRIG",value=space(50),;
    ToolTipText = "Eventuale descrizione aggiuntiva",;
    HelpContextID = 196044093,;
    cTotal="", bFixedPos=.t., cQueryName = "PTDESRIG",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=363, Left=87, Top=253, InputMask=replicate('X',50)

  func oPTDESRIG_2_39.mCond()
    with this.Parent.oContained
      return (NOT EMPTY(.w_PTDATSCA) AND .w_PTTOTIMP<>0 AND NOT EMPTY(.w_PTNUMPAR))
    endwith
  endfunc

  add object oPTNUMCOR_2_49 as StdTrsField with uid="IZKQOCLOKF",rtseq=75,rtrep=.t.,;
    cFormVar="w_PTNUMCOR",value=space(25),;
    ToolTipText = "Conto corrente",;
    HelpContextID = 60816056,;
    cTotal="", bFixedPos=.t., cQueryName = "PTNUMCOR",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=188, Left=592, Top=225, InputMask=replicate('X',25), bHasZoom = .t. , cLinkFile="BAN_CONTI", oKey_1_1="CCTIPCON", oKey_1_2="this.w_PTTIPCON", oKey_2_1="CCCODCON", oKey_2_2="this.w_PTCODCON", oKey_3_1="CCCODBAN", oKey_3_2="this.w_PTBANAPP", oKey_4_1="CCCONCOR", oKey_4_2="this.w_PTNUMCOR"

  func oPTNUMCOR_2_49.mCond()
    with this.Parent.oContained
      return (.w_PTTIPCON $ 'CF'  AND .w_FLINSO<>'S' and .w_FLIVDF <>'S')
    endwith
  endfunc

  func oPTNUMCOR_2_49.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_49('Part',this)
    endwith
    return bRes
  endfunc

  proc oPTNUMCOR_2_49.ecpDrop(oSource)
    this.Parent.oContained.link_2_49('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
  endproc

  proc oPTNUMCOR_2_49.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.BAN_CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_PTTIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODCON="+cp_ToStrODBC(this.Parent.oContained.w_PTCODCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODBAN="+cp_ToStrODBC(this.Parent.oContained.w_PTBANAPP)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCTIPCON="+cp_ToStr(this.Parent.oContained.w_PTTIPCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODCON="+cp_ToStr(this.Parent.oContained.w_PTCODCON)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"CCCODBAN="+cp_ToStr(this.Parent.oContained.w_PTBANAPP)
    endif
    do cp_zoom with 'BAN_CONTI','*','CCTIPCON,CCCODCON,CCCODBAN,CCCONCOR',cp_AbsName(this.parent,'oPTNUMCOR_2_49'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Elenco conti correnti",'',this.parent.oContained
  endproc

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1

  add object oTOTALE_3_1 as StdField with uid="JOLWMHWFYB",rtseq=81,rtrep=.f.,;
    cFormVar="w_TOTALE",value=0,enabled=.f.,;
    HelpContextID = 29597642,;
    cQueryName = "TOTALE",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=246, Top=225, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  add object oSEG_3_2 as StdField with uid="XTWEHQDQUO",rtseq=82,rtrep=.f.,;
    cFormVar="w_SEG",value=space(1),enabled=.f.,;
    HelpContextID = 70944294,;
    cQueryName = "SEG",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=23, Left=219, Top=225, InputMask=replicate('X',1)

  add object oSIMNAZ_3_3 as StdField with uid="BMZICPFRAR",rtseq=83,rtrep=.f.,;
    cFormVar="w_SIMNAZ",value=space(3),enabled=.f.,;
    HelpContextID = 224859610,;
    cQueryName = "SIMNAZ",;
    bObbl = .f. , nPag = 3, bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=378, Top=225, InputMask=replicate('X',3)
enddefine

* --- Defining Body row
define class tgste_mmpBodyRow as CPBodyRowCnt
  Width=765
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oPTDATSCA_2_1 as StdTrsField with uid="TGIGTBUDPG",rtseq=29,rtrep=.t.,;
    cFormVar="w_PTDATSCA",value=ctod("  /  /  "),;
    ToolTipText = "Data della scadenza",;
    HelpContextID = 54827721,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=-2, Top=0

  add object oSIMBVA_2_3 as StdTrsField with uid="FHESPEPFDJ",rtseq=31,rtrep=.t.,;
    cFormVar="w_SIMBVA",value=space(5),enabled=.f.,;
    HelpContextID = 86185434,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=51, Left=474, Top=0, InputMask=replicate('X',5)

  add object oPTNUMPAR_2_4 as StdTrsField with uid="HHGDXEVDFB",rtseq=32,rtrep=.t.,;
    cFormVar="w_PTNUMPAR",value=space(31),;
    ToolTipText = "Numero della partita",;
    HelpContextID = 111147704,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=230, Left=81, Top=0, InputMask=replicate('X',31)

  func oPTNUMPAR_2_4.mCond()
    with this.Parent.oContained
      return (.w_FLINSO<>'S' and .w_FLIVDF <>'S')
    endwith
  endfunc

  add object oPT_SEGNO_2_5 as StdTrsField with uid="YOYIXBQRZF",rtseq=33,rtrep=.t.,;
    cFormVar="w_PT_SEGNO",value=space(1),;
    ToolTipText = "Sezione importo partita: D= dare; A =avere",;
    HelpContextID = 2157243,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Inserire segno!: Dare/avere",;
   bGlobalFont=.t.,;
    Height=17, Width=23, Left=315, Top=0, cSayPict=["!"], cGetPict=["!"], InputMask=replicate('X',1)

  func oPT_SEGNO_2_5.mCond()
    with this.Parent.oContained
      return (Empty(.w_DVSERIAL) AND .w_FLINSO<>'S' and .w_FLIVDF <>'S')
    endwith
  endfunc

  func oPT_SEGNO_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PT_SEGNO $ "DA")
    endwith
    return bRes
  endfunc

  add object oPTTOTIMP_2_9 as StdTrsField with uid="WHWDMATNRN",rtseq=37,rtrep=.t.,;
    cFormVar="w_PTTOTIMP",value=0,;
    ToolTipText = "Importo della scadenza",;
    HelpContextID = 221616826,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Attenzione, impossibile inserire importi negativi",;
   bGlobalFont=.t.,;
    Height=17, Width=128, Left=342, Top=0, cSayPict=[v_PV(40+VVL)], cGetPict=[v_GV(40+VVL)]

  func oPTTOTIMP_2_9.mCond()
    with this.Parent.oContained
      return (Empty(.w_DVSERIAL)  AND .w_FLINSO<>'S' and .w_FLIVDF <>'S')
    endwith
  endfunc

  func oPTTOTIMP_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PTTOTIMP>0)
    endwith
    return bRes
  endfunc

  add object oPTFLSOSP_2_10 as StdTrsCheck with uid="IRLYHMCAKE",rtrep=.t.,;
    cFormVar="w_PTFLSOSP",  caption="",;
    ToolTipText = "Se attivo: partita sospesa",;
    HelpContextID = 122256058,;
    Left=535, Top=0, Width=22,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f.;
    , tabstop=.f.;
   , bGlobalFont=.t.


  func oPTFLSOSP_2_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..PTFLSOSP,&i_cF..t_PTFLSOSP),this.value)
    return(iif(xVal =1,'S',;
    ' '))
  endfunc
  func oPTFLSOSP_2_10.GetRadio()
    this.Parent.oContained.w_PTFLSOSP = this.RadioValue()
    return .t.
  endfunc

  func oPTFLSOSP_2_10.ToRadio()
    this.Parent.oContained.w_PTFLSOSP=trim(this.Parent.oContained.w_PTFLSOSP)
    return(;
      iif(this.Parent.oContained.w_PTFLSOSP=='S',1,;
      0))
  endfunc

  func oPTFLSOSP_2_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oPTFLSOSP_2_10.mCond()
    with this.Parent.oContained
      return (.w_ESIDETT <>'S')
    endwith
  endfunc

  add object oPTMODPAG_2_11 as StdTrsField with uid="XSZSLBMUUI",rtseq=39,rtrep=.t.,;
    cFormVar="w_PTMODPAG",value=space(10),;
    ToolTipText = "Codice tipo di pagamento",;
    HelpContextID = 120982211,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .t. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Inserire tipo pagamento",;
   bGlobalFont=.t.,;
    Height=17, Width=93, Left=563, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="MOD_PAGA", cZoomOnZoom="GSAR_AMP", oKey_1_1="MPCODICE", oKey_1_2="this.w_PTMODPAG"

  func oPTMODPAG_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oPTMODPAG_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPTMODPAG_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MOD_PAGA','*','MPCODICE',cp_AbsName(this.parent,'oPTMODPAG_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMP',"Tipi di pagamento",'',this.parent.oContained
  endproc
  proc oPTMODPAG_2_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MPCODICE=this.parent.oContained.w_PTMODPAG
    i_obj.ecpSave()
  endproc

  add object oPTBANAPP_2_12 as StdTrsField with uid="WQNONUMLAK",rtseq=40,rtrep=.t.,;
    cFormVar="w_PTBANAPP",value=space(10),;
    ToolTipText = "banca di appoggio del cliente/fornitore",;
    HelpContextID = 94681786,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=100, Left=660, Top=0, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_PTBANAPP"

  func oPTBANAPP_2_12.mCond()
    with this.Parent.oContained
      return (.w_PTTIPCON $ 'CF')
    endwith
  endfunc

  func oPTBANAPP_2_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_12('Part',this)
      if .not. empty(.w_PTNUMCOR)
        bRes2=.link_2_49('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oPTBANAPP_2_12.ecpDrop(oSource)
    this.Parent.oContained.link_2_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oPTBANAPP_2_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oPTBANAPP_2_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABA',"Elenco banche",'',this.parent.oContained
  endproc
  proc oPTBANAPP_2_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_PTBANAPP
    i_obj.ecpSave()
  endproc
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oPTDATSCA_2_1.When()
    return(.t.)
  proc oPTDATSCA_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oPTDATSCA_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=5
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_mmp','PAR_TITE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PTSERIAL=PAR_TITE.PTSERIAL";
  +" and "+i_cAliasName2+".PTROWORD=PAR_TITE.PTROWORD";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
