* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_ape                                                        *
*              Parametri elenchi clienti/fornitori                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-07-23                                                      *
* Last revis.: 2009-11-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg_ape"))

* --- Class definition
define class tgscg_ape as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 877
  Height = 303+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-11-16"
  HelpContextID=160048489
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=5

  * --- Constant Properties
  PAELCLFT_IDX = 0
  cFile = "PAELCLFT"
  cKeySelect = "PE__ANNO,PETIPCOM"
  cKeyWhere  = "PE__ANNO=this.w_PE__ANNO and PETIPCOM=this.w_PETIPCOM"
  cKeyWhereODBC = '"PE__ANNO="+cp_ToStrODBC(this.w_PE__ANNO)';
      +'+" and PETIPCOM="+cp_ToStrODBC(this.w_PETIPCOM)';

  cKeyWhereODBCqualified = '"PAELCLFT.PE__ANNO="+cp_ToStrODBC(this.w_PE__ANNO)';
      +'+" and PAELCLFT.PETIPCOM="+cp_ToStrODBC(this.w_PETIPCOM)';

  cPrg = "gscg_ape"
  cComment = "Parametri elenchi clienti/fornitori"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PE__ANNO = space(4)
  w_PETIPCOM = space(1)
  w_PEFLGISO = space(1)
  w_ANNO = space(4)
  w_TIPCOM = space(1)

  * --- Children pointers
  GSCG_MPE = .NULL.
  GSCG_MPX = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=3, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PAELCLFT','gscg_ape')
    stdPageFrame::Init()
    *set procedure to GSCG_MPE additive
    with this
      .Pages(1).addobject("oPag","tgscg_apePag1","gscg_ape",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Dettaglio parametri")
      .Pages(1).HelpContextID = 210810164
      .Pages(2).addobject("oPag","tgscg_apePag2","gscg_ape",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Dettaglio esclusioni")
      .Pages(2).HelpContextID = 149204590
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPE__ANNO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSCG_MPE
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='PAELCLFT'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAELCLFT_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAELCLFT_IDX,3]
  return

  function CreateChildren()
    this.GSCG_MPE = CREATEOBJECT('stdDynamicChild',this,'GSCG_MPE',this.oPgFrm.Page1.oPag.oLinkPC_1_6)
    this.GSCG_MPE.createrealchild()
    this.GSCG_MPX = CREATEOBJECT('stdDynamicChild',this,'GSCG_MPX',this.oPgFrm.Page2.oPag.oLinkPC_2_1)
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSCG_MPE)
      this.GSCG_MPE.DestroyChildrenChain()
      this.GSCG_MPE=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_6')
    if !ISNULL(this.GSCG_MPX)
      this.GSCG_MPX.DestroyChildrenChain()
      this.GSCG_MPX=.NULL.
    endif
    this.oPgFrm.Page2.oPag.RemoveObject('oLinkPC_2_1')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSCG_MPE.IsAChildUpdated()
      i_bRes = i_bRes .or. this.GSCG_MPX.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSCG_MPE.IsAChildUpdated(.t.)
      i_bRes = i_bRes .or. this.GSCG_MPX.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSCG_MPE.NewDocument()
    this.GSCG_MPX.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSCG_MPE.SetKey(;
            .w_PE__ANNO,"PE__ANNO";
            ,.w_PETIPCOM,"PETIPCOM";
            )
      this.GSCG_MPX.SetKey(;
            .w_PE__ANNO,"PE__ANNO";
            ,.w_PETIPCOM,"PETIPCOM";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSCG_MPE.ChangeRow(this.cRowID+'      1',1;
             ,.w_PE__ANNO,"PE__ANNO";
             ,.w_PETIPCOM,"PETIPCOM";
             )
      .GSCG_MPX.ChangeRow(this.cRowID+'      1',1;
             ,.w_PE__ANNO,"PE__ANNO";
             ,.w_PETIPCOM,"PETIPCOM";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSCG_MPE)
        i_f=.GSCG_MPE.BuildFilter()
        if !(i_f==.GSCG_MPE.cQueryFilter)
          i_fnidx=.GSCG_MPE.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_MPE.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_MPE.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_MPE.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_MPE.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
      if !IsNull(.GSCG_MPX)
        i_f=.GSCG_MPX.BuildFilter()
        if !(i_f==.GSCG_MPX.cQueryFilter)
          i_fnidx=.GSCG_MPX.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSCG_MPX.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSCG_MPX.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSCG_MPX.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSCG_MPX.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PE__ANNO = NVL(PE__ANNO,space(4))
      .w_PETIPCOM = NVL(PETIPCOM,space(1))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PAELCLFT where PE__ANNO=KeySet.PE__ANNO
    *                            and PETIPCOM=KeySet.PETIPCOM
    *
    i_nConn = i_TableProp[this.PAELCLFT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAELCLFT_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAELCLFT')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAELCLFT.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAELCLFT '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PE__ANNO',this.w_PE__ANNO  ,'PETIPCOM',this.w_PETIPCOM  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_PE__ANNO = NVL(PE__ANNO,space(4))
        .w_PETIPCOM = NVL(PETIPCOM,space(1))
        .w_PEFLGISO = NVL(PEFLGISO,space(1))
        .w_ANNO = .w_PE__ANNO
        .w_TIPCOM = .w_PETIPCOM
        cp_LoadRecExtFlds(this,'PAELCLFT')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PE__ANNO = space(4)
      .w_PETIPCOM = space(1)
      .w_PEFLGISO = space(1)
      .w_ANNO = space(4)
      .w_TIPCOM = space(1)
      if .cFunction<>"Filter"
        .w_PE__ANNO = iif(this.cFunction='Load', STR(YEAR(i_DATSYS)-1,4), .w_PE__ANNO)
        .w_PETIPCOM = 'C'
        .w_PEFLGISO = 'S'
        .w_ANNO = .w_PE__ANNO
        .w_TIPCOM = .w_PETIPCOM
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAELCLFT')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPE__ANNO_1_1.enabled = i_bVal
      .Page1.oPag.oPETIPCOM_1_3.enabled = i_bVal
      .Page1.oPag.oPEFLGISO_1_4.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPE__ANNO_1_1.enabled = .f.
        .Page1.oPag.oPETIPCOM_1_3.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPE__ANNO_1_1.enabled = .t.
        .Page1.oPag.oPETIPCOM_1_3.enabled = .t.
      endif
    endwith
    this.GSCG_MPE.SetStatus(i_cOp)
    this.GSCG_MPX.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'PAELCLFT',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSCG_MPE.SetChildrenStatus(i_cOp)
  *  this.GSCG_MPX.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAELCLFT_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PE__ANNO,"PE__ANNO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PETIPCOM,"PETIPCOM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PEFLGISO,"PEFLGISO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PAELCLFT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAELCLFT_IDX,2])
    i_lTable = "PAELCLFT"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PAELCLFT_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      vx_exec("QUERY\GSCG_APE.VQR, QUERY\GSCG_APE.FRX",this)
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- gscg_ape
    this.bUpdated=.t.
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAELCLFT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAELCLFT_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PAELCLFT_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PAELCLFT
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAELCLFT')
        i_extval=cp_InsertValODBCExtFlds(this,'PAELCLFT')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PE__ANNO,PETIPCOM,PEFLGISO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PE__ANNO)+;
                  ","+cp_ToStrODBC(this.w_PETIPCOM)+;
                  ","+cp_ToStrODBC(this.w_PEFLGISO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAELCLFT')
        i_extval=cp_InsertValVFPExtFlds(this,'PAELCLFT')
        cp_CheckDeletedKey(i_cTable,0,'PE__ANNO',this.w_PE__ANNO,'PETIPCOM',this.w_PETIPCOM)
        INSERT INTO (i_cTable);
              (PE__ANNO,PETIPCOM,PEFLGISO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PE__ANNO;
                  ,this.w_PETIPCOM;
                  ,this.w_PEFLGISO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PAELCLFT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAELCLFT_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PAELCLFT_IDX,i_nConn)
      *
      * update PAELCLFT
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PAELCLFT')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PEFLGISO="+cp_ToStrODBC(this.w_PEFLGISO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PAELCLFT')
        i_cWhere = cp_PKFox(i_cTable  ,'PE__ANNO',this.w_PE__ANNO  ,'PETIPCOM',this.w_PETIPCOM  )
        UPDATE (i_cTable) SET;
              PEFLGISO=this.w_PEFLGISO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSCG_MPE : Saving
      this.GSCG_MPE.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PE__ANNO,"PE__ANNO";
             ,this.w_PETIPCOM,"PETIPCOM";
             )
      this.GSCG_MPE.mReplace()
      * --- GSCG_MPX : Saving
      this.GSCG_MPX.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PE__ANNO,"PE__ANNO";
             ,this.w_PETIPCOM,"PETIPCOM";
             )
      this.GSCG_MPX.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    * --- GSCG_MPE : Deleting
    this.GSCG_MPE.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PE__ANNO,"PE__ANNO";
           ,this.w_PETIPCOM,"PETIPCOM";
           )
    this.GSCG_MPE.mDelete()
    * --- GSCG_MPX : Deleting
    this.GSCG_MPX.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PE__ANNO,"PE__ANNO";
           ,this.w_PETIPCOM,"PETIPCOM";
           )
    this.GSCG_MPX.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAELCLFT_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAELCLFT_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PAELCLFT_IDX,i_nConn)
      *
      * delete PAELCLFT
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PE__ANNO',this.w_PE__ANNO  ,'PETIPCOM',this.w_PETIPCOM  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAELCLFT_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAELCLFT_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,3,.t.)
            .w_ANNO = .w_PE__ANNO
            .w_TIPCOM = .w_PETIPCOM
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_XSDWCKVYUZ()
    with this
          * --- Messaggio di inserimento parametro
          MessageInserted(this;
             )
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Record Inserted")
          .Calculate_XSDWCKVYUZ()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPE__ANNO_1_1.value==this.w_PE__ANNO)
      this.oPgFrm.Page1.oPag.oPE__ANNO_1_1.value=this.w_PE__ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oPETIPCOM_1_3.RadioValue()==this.w_PETIPCOM)
      this.oPgFrm.Page1.oPag.oPETIPCOM_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPEFLGISO_1_4.RadioValue()==this.w_PEFLGISO)
      this.oPgFrm.Page1.oPag.oPEFLGISO_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oANNO_2_4.value==this.w_ANNO)
      this.oPgFrm.Page2.oPag.oANNO_2_4.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page2.oPag.oTIPCOM_2_5.RadioValue()==this.w_TIPCOM)
      this.oPgFrm.Page2.oPag.oTIPCOM_2_5.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'PAELCLFT')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_PE__ANNO)) or not(Val(.w_PE__ANNO)>2005 and Val(.w_PE__ANNO)<2100))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPE__ANNO_1_1.SetFocus()
            i_bnoObbl = !empty(.w_PE__ANNO)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_PETIPCOM))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPETIPCOM_1_3.SetFocus()
            i_bnoObbl = !empty(.w_PETIPCOM)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      *i_bRes = i_bRes .and. .GSCG_MPE.CheckForm()
      if i_bres
        i_bres=  .GSCG_MPE.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      *i_bRes = i_bRes .and. .GSCG_MPX.CheckForm()
      if i_bres
        i_bres=  .GSCG_MPX.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=2
        endif
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    * --- GSCG_MPE : Depends On
    this.GSCG_MPE.SaveDependsOn()
    * --- GSCG_MPX : Depends On
    this.GSCG_MPX.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgscg_apePag1 as StdContainer
  Width  = 873
  height = 306
  stdWidth  = 873
  stdheight = 306
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPE__ANNO_1_1 as StdField with uid="IGYXCNZHDV",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PE__ANNO", cQueryName = "PE__ANNO",;
    bObbl = .t. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di comunicazione",;
    HelpContextID = 118811323,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=133, Top=11, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)

  func oPE__ANNO_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (Val(.w_PE__ANNO)>2005 and Val(.w_PE__ANNO)<2100)
    endwith
    return bRes
  endfunc


  add object oPETIPCOM_1_3 as StdCombo with uid="QZVMIAVOYJ",rtseq=2,rtrep=.f.,left=327,top=11,width=130,height=21;
    , ToolTipText = "Tipo comunicazione clienti / fornitori";
    , HelpContextID = 20683453;
    , cFormVar="w_PETIPCOM",RowSource=""+"Clienti,"+"Fornitori", bObbl = .t. , nPag = 1;
  , bGlobalFont=.t.


  func oPETIPCOM_1_3.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oPETIPCOM_1_3.GetRadio()
    this.Parent.oContained.w_PETIPCOM = this.RadioValue()
    return .t.
  endfunc

  func oPETIPCOM_1_3.SetRadio()
    this.Parent.oContained.w_PETIPCOM=trim(this.Parent.oContained.w_PETIPCOM)
    this.value = ;
      iif(this.Parent.oContained.w_PETIPCOM=='C',1,;
      iif(this.Parent.oContained.w_PETIPCOM=='F',2,;
      0))
  endfunc

  add object oPEFLGISO_1_4 as StdCheck with uid="IQMIOOLCCI",rtseq=3,rtrep=.f.,left=464, top=11, caption="Escludi se nazione diversa da Italia",;
    ToolTipText = "Esportazioni / importazione solo da nazione intestatario",;
    HelpContextID = 70681925,;
    cFormVar="w_PEFLGISO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPEFLGISO_1_4.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPEFLGISO_1_4.GetRadio()
    this.Parent.oContained.w_PEFLGISO = this.RadioValue()
    return .t.
  endfunc

  func oPEFLGISO_1_4.SetRadio()
    this.Parent.oContained.w_PEFLGISO=trim(this.Parent.oContained.w_PEFLGISO)
    this.value = ;
      iif(this.Parent.oContained.w_PEFLGISO=='S',1,;
      0)
  endfunc


  add object oLinkPC_1_6 as stdDynamicChildContainer with uid="WKTBRSVVSO",left=5, top=36, width=865, height=270, bOnScreen=.t.;


  add object oStr_1_2 as StdString with uid="ROFVVBCFOA",Visible=.t., Left=2, Top=13,;
    Alignment=1, Width=130, Height=18,;
    Caption="Anno di comunicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_5 as StdString with uid="NFFQFQARJK",Visible=.t., Left=186, Top=13,;
    Alignment=1, Width=140, Height=18,;
    Caption="Tipo comunicazione:"  ;
  , bGlobalFont=.t.
enddefine
define class tgscg_apePag2 as StdContainer
  Width  = 873
  height = 306
  stdWidth  = 873
  stdheight = 306
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oLinkPC_2_1 as stdDynamicChildContainer with uid="HTJZHZCFSC",left=5, top=42, width=625, height=241, bOnScreen=.t.;


  add object oANNO_2_4 as StdField with uid="TMOAFHDEBT",rtseq=4,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno di comunicazione",;
    HelpContextID = 154530554,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=139, Top=11, cSayPict='"9999"', cGetPict='"9999"', InputMask=replicate('X',4)


  add object oTIPCOM_2_5 as StdCombo with uid="PJGQCQLDMY",rtseq=5,rtrep=.f.,left=333,top=11,width=130,height=21, enabled=.f.;
    , ToolTipText = "Tipo comunicazione clienti / fornitori";
    , HelpContextID = 145631542;
    , cFormVar="w_TIPCOM",RowSource=""+"Clienti,"+"Fornitori", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oTIPCOM_2_5.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(1))))
  endfunc
  func oTIPCOM_2_5.GetRadio()
    this.Parent.oContained.w_TIPCOM = this.RadioValue()
    return .t.
  endfunc

  func oTIPCOM_2_5.SetRadio()
    this.Parent.oContained.w_TIPCOM=trim(this.Parent.oContained.w_TIPCOM)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCOM=='C',1,;
      iif(this.Parent.oContained.w_TIPCOM=='F',2,;
      0))
  endfunc

  add object oStr_2_2 as StdString with uid="XRJROKACGV",Visible=.t., Left=6, Top=13,;
    Alignment=1, Width=130, Height=18,;
    Caption="Anno di comunicazione:"  ;
  , bGlobalFont=.t.

  add object oStr_2_3 as StdString with uid="BTJSGIPKQP",Visible=.t., Left=190, Top=13,;
    Alignment=1, Width=140, Height=18,;
    Caption="Tipo comunicazione:"  ;
  , bGlobalFont=.t.
  proc uienable(i_a)
    if this.createchildren and i_a and this.parent.parent.parent.cFunction<>'Filter'
      if type('this.oContained')='O' and at("gscg_mpx",lower(this.oContained.GSCG_MPX.class))=0
        this.oContained.GSCG_MPX.createrealchild()
      endif
      this.createchildren=.f.
    endif
  endproc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_ape','PAELCLFT','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PE__ANNO=PAELCLFT.PE__ANNO";
  +" and "+i_cAliasName2+".PETIPCOM=PAELCLFT.PETIPCOM";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- gscg_ape
proc MessageInserted(obj)
  ah_ErrorMsg("Parametro inserito")
endproc
* --- Fine Area Manuale
