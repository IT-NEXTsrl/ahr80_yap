* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_aey                                                        *
*              Dati comunicazione elenchi clienti / fornitori                  *
*                                                                              *
*      Author: Zucchetti S.p.a.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-06-26                                                      *
* Last revis.: 2009-07-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_aey")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_aey")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_aey")
  return

* --- Class definition
define class tgscg_aey as StdPCForm
  Width  = 865
  Height = 57
  Top    = 10
  Left   = 10
  cComment = "Dati comunicazione elenchi clienti / fornitori"
  cPrg = "gscg_aey"
  HelpContextID=76162409
  add object cnt as tcgscg_aey
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_aey as PCContext
  w_DESERIAL = space(10)
  w_CPROWNUM = 0
  w_DEIMPIMP = 0
  w_DEIMPAFF = 0
  w_DEIMPNOI = 0
  w_DEIMPESE = 0
  w_DEIMPNIN = 0
  w_DEIMPLOR = 0
  w_DERIFPNT = space(10)
  proc Save(oFrom)
    this.w_DESERIAL = oFrom.w_DESERIAL
    this.w_CPROWNUM = oFrom.w_CPROWNUM
    this.w_DEIMPIMP = oFrom.w_DEIMPIMP
    this.w_DEIMPAFF = oFrom.w_DEIMPAFF
    this.w_DEIMPNOI = oFrom.w_DEIMPNOI
    this.w_DEIMPESE = oFrom.w_DEIMPESE
    this.w_DEIMPNIN = oFrom.w_DEIMPNIN
    this.w_DEIMPLOR = oFrom.w_DEIMPLOR
    this.w_DERIFPNT = oFrom.w_DERIFPNT
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_DESERIAL = this.w_DESERIAL
    oTo.w_CPROWNUM = this.w_CPROWNUM
    oTo.w_DEIMPIMP = this.w_DEIMPIMP
    oTo.w_DEIMPAFF = this.w_DEIMPAFF
    oTo.w_DEIMPNOI = this.w_DEIMPNOI
    oTo.w_DEIMPESE = this.w_DEIMPESE
    oTo.w_DEIMPNIN = this.w_DEIMPNIN
    oTo.w_DEIMPLOR = this.w_DEIMPLOR
    oTo.w_DERIFPNT = this.w_DERIFPNT
    PCContext::Load(oTo)
enddefine

define class tcgscg_aey as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 865
  Height = 57
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2009-07-08"
  HelpContextID=76162409
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=9

  * --- Constant Properties
  DAELCLFD_IDX = 0
  cFile = "DAELCLFD"
  cKeySelect = "DESERIAL"
  cKeyWhere  = "DESERIAL=this.w_DESERIAL"
  cKeyWhereODBC = '"DESERIAL="+cp_ToStrODBC(this.w_DESERIAL)';

  cKeyWhereODBCqualified = '"DAELCLFD.DESERIAL="+cp_ToStrODBC(this.w_DESERIAL)';

  cPrg = "gscg_aey"
  cComment = "Dati comunicazione elenchi clienti / fornitori"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DESERIAL = space(10)
  w_CPROWNUM = 0
  w_DEIMPIMP = 0
  w_DEIMPAFF = 0
  w_DEIMPNOI = 0
  w_DEIMPESE = 0
  w_DEIMPNIN = 0
  w_DEIMPLOR = 0
  w_DERIFPNT = space(10)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_aeyPag1","gscg_aey",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 17714954
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDEIMPIMP_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='DAELCLFD'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DAELCLFD_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DAELCLFD_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_aey'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from DAELCLFD where DESERIAL=KeySet.DESERIAL
    *
    i_nConn = i_TableProp[this.DAELCLFD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFD_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DAELCLFD')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DAELCLFD.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DAELCLFD '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DESERIAL',this.w_DESERIAL  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESERIAL = NVL(DESERIAL,space(10))
        .w_CPROWNUM = NVL(CPROWNUM,0)
        .w_DEIMPIMP = NVL(DEIMPIMP,0)
        .w_DEIMPAFF = NVL(DEIMPAFF,0)
        .w_DEIMPNOI = NVL(DEIMPNOI,0)
        .w_DEIMPESE = NVL(DEIMPESE,0)
        .w_DEIMPNIN = NVL(DEIMPNIN,0)
        .w_DEIMPLOR = NVL(DEIMPLOR,0)
        .w_DERIFPNT = NVL(DERIFPNT,space(10))
        cp_LoadRecExtFlds(this,'DAELCLFD')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_DESERIAL = space(10)
      .w_CPROWNUM = 0
      .w_DEIMPIMP = 0
      .w_DEIMPAFF = 0
      .w_DEIMPNOI = 0
      .w_DEIMPESE = 0
      .w_DEIMPNIN = 0
      .w_DEIMPLOR = 0
      .w_DERIFPNT = space(10)
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'DAELCLFD')
    this.DoRTCalc(1,9,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDEIMPIMP_1_3.enabled = i_bVal
      .Page1.oPag.oDEIMPAFF_1_5.enabled = i_bVal
      .Page1.oPag.oDEIMPNOI_1_7.enabled = i_bVal
      .Page1.oPag.oDEIMPESE_1_9.enabled = i_bVal
      .Page1.oPag.oDEIMPNIN_1_11.enabled = i_bVal
      .Page1.oPag.oDEIMPLOR_1_13.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DAELCLFD',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DAELCLFD_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DESERIAL,"DESERIAL",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_CPROWNUM,"CPROWNUM",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEIMPIMP,"DEIMPIMP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEIMPAFF,"DEIMPAFF",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEIMPNOI,"DEIMPNOI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEIMPESE,"DEIMPESE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEIMPNIN,"DEIMPNIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DEIMPLOR,"DEIMPLOR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DERIFPNT,"DERIFPNT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- gscg_aey
    this.bUpdated=.t.
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DAELCLFD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFD_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.DAELCLFD_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into DAELCLFD
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DAELCLFD')
        i_extval=cp_InsertValODBCExtFlds(this,'DAELCLFD')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(DESERIAL,CPROWNUM,DEIMPIMP,DEIMPAFF,DEIMPNOI"+;
                  ",DEIMPESE,DEIMPNIN,DEIMPLOR,DERIFPNT "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_DESERIAL)+;
                  ","+cp_ToStrODBC(this.w_CPROWNUM)+;
                  ","+cp_ToStrODBC(this.w_DEIMPIMP)+;
                  ","+cp_ToStrODBC(this.w_DEIMPAFF)+;
                  ","+cp_ToStrODBC(this.w_DEIMPNOI)+;
                  ","+cp_ToStrODBC(this.w_DEIMPESE)+;
                  ","+cp_ToStrODBC(this.w_DEIMPNIN)+;
                  ","+cp_ToStrODBC(this.w_DEIMPLOR)+;
                  ","+cp_ToStrODBC(this.w_DERIFPNT)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DAELCLFD')
        i_extval=cp_InsertValVFPExtFlds(this,'DAELCLFD')
        cp_CheckDeletedKey(i_cTable,0,'DESERIAL',this.w_DESERIAL)
        INSERT INTO (i_cTable);
              (DESERIAL,CPROWNUM,DEIMPIMP,DEIMPAFF,DEIMPNOI,DEIMPESE,DEIMPNIN,DEIMPLOR,DERIFPNT  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_DESERIAL;
                  ,this.w_CPROWNUM;
                  ,this.w_DEIMPIMP;
                  ,this.w_DEIMPAFF;
                  ,this.w_DEIMPNOI;
                  ,this.w_DEIMPESE;
                  ,this.w_DEIMPNIN;
                  ,this.w_DEIMPLOR;
                  ,this.w_DERIFPNT;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.DAELCLFD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFD_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.DAELCLFD_IDX,i_nConn)
      *
      * update DAELCLFD
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'DAELCLFD')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " CPROWNUM="+cp_ToStrODBC(this.w_CPROWNUM)+;
             ",DEIMPIMP="+cp_ToStrODBC(this.w_DEIMPIMP)+;
             ",DEIMPAFF="+cp_ToStrODBC(this.w_DEIMPAFF)+;
             ",DEIMPNOI="+cp_ToStrODBC(this.w_DEIMPNOI)+;
             ",DEIMPESE="+cp_ToStrODBC(this.w_DEIMPESE)+;
             ",DEIMPNIN="+cp_ToStrODBC(this.w_DEIMPNIN)+;
             ",DEIMPLOR="+cp_ToStrODBC(this.w_DEIMPLOR)+;
             ",DERIFPNT="+cp_ToStrODBC(this.w_DERIFPNT)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'DAELCLFD')
        i_cWhere = cp_PKFox(i_cTable  ,'DESERIAL',this.w_DESERIAL  )
        UPDATE (i_cTable) SET;
              CPROWNUM=this.w_CPROWNUM;
             ,DEIMPIMP=this.w_DEIMPIMP;
             ,DEIMPAFF=this.w_DEIMPAFF;
             ,DEIMPNOI=this.w_DEIMPNOI;
             ,DEIMPESE=this.w_DEIMPESE;
             ,DEIMPNIN=this.w_DEIMPNIN;
             ,DEIMPLOR=this.w_DEIMPLOR;
             ,DERIFPNT=this.w_DERIFPNT;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DAELCLFD_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFD_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.DAELCLFD_IDX,i_nConn)
      *
      * delete DAELCLFD
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'DESERIAL',this.w_DESERIAL  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DAELCLFD_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DAELCLFD_IDX,2])
    if i_bUpd
      with this
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,9,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oDEIMPIMP_1_3.enabled = this.oPgFrm.Page1.oPag.oDEIMPIMP_1_3.mCond()
    this.oPgFrm.Page1.oPag.oDEIMPAFF_1_5.enabled = this.oPgFrm.Page1.oPag.oDEIMPAFF_1_5.mCond()
    this.oPgFrm.Page1.oPag.oDEIMPNOI_1_7.enabled = this.oPgFrm.Page1.oPag.oDEIMPNOI_1_7.mCond()
    this.oPgFrm.Page1.oPag.oDEIMPESE_1_9.enabled = this.oPgFrm.Page1.oPag.oDEIMPESE_1_9.mCond()
    this.oPgFrm.Page1.oPag.oDEIMPNIN_1_11.enabled = this.oPgFrm.Page1.oPag.oDEIMPNIN_1_11.mCond()
    this.oPgFrm.Page1.oPag.oDEIMPLOR_1_13.enabled = this.oPgFrm.Page1.oPag.oDEIMPLOR_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDEIMPIMP_1_3.value==this.w_DEIMPIMP)
      this.oPgFrm.Page1.oPag.oDEIMPIMP_1_3.value=this.w_DEIMPIMP
    endif
    if not(this.oPgFrm.Page1.oPag.oDEIMPAFF_1_5.value==this.w_DEIMPAFF)
      this.oPgFrm.Page1.oPag.oDEIMPAFF_1_5.value=this.w_DEIMPAFF
    endif
    if not(this.oPgFrm.Page1.oPag.oDEIMPNOI_1_7.value==this.w_DEIMPNOI)
      this.oPgFrm.Page1.oPag.oDEIMPNOI_1_7.value=this.w_DEIMPNOI
    endif
    if not(this.oPgFrm.Page1.oPag.oDEIMPESE_1_9.value==this.w_DEIMPESE)
      this.oPgFrm.Page1.oPag.oDEIMPESE_1_9.value=this.w_DEIMPESE
    endif
    if not(this.oPgFrm.Page1.oPag.oDEIMPNIN_1_11.value==this.w_DEIMPNIN)
      this.oPgFrm.Page1.oPag.oDEIMPNIN_1_11.value=this.w_DEIMPNIN
    endif
    if not(this.oPgFrm.Page1.oPag.oDEIMPLOR_1_13.value==this.w_DEIMPLOR)
      this.oPgFrm.Page1.oPag.oDEIMPLOR_1_13.value=this.w_DEIMPLOR
    endif
    cp_SetControlsValueExtFlds(this,'DAELCLFD')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgscg_aeyPag1 as StdContainer
  Width  = 861
  height = 57
  stdWidth  = 861
  stdheight = 57
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDEIMPIMP_1_3 as StdField with uid="CGNTGLCHIT",rtseq=3,rtrep=.t.,;
    cFormVar = "w_DEIMPIMP", cQueryName = "DEIMPIMP",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo operazioni imponibili",;
    HelpContextID = 164082822,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=5, Top=26, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oDEIMPIMP_1_3.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_DERIFPNT))
    endwith
   endif
  endfunc

  add object oDEIMPAFF_1_5 as StdField with uid="HBKNTIJHEF",rtseq=4,rtrep=.t.,;
    cFormVar = "w_DEIMPAFF", cQueryName = "DEIMPAFF",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo imposta afferente",;
    HelpContextID = 238570372,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=146, Top=26, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oDEIMPAFF_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_DERIFPNT))
    endwith
   endif
  endfunc

  add object oDEIMPNOI_1_7 as StdField with uid="ARZHHDJUJH",rtseq=5,rtrep=.t.,;
    cFormVar = "w_DEIMPNOI", cQueryName = "DEIMPNOI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo operazioni non imponibili",;
    HelpContextID = 247968895,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=287, Top=26, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oDEIMPNOI_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_DERIFPNT))
    endwith
   endif
  endfunc

  add object oDEIMPESE_1_9 as StdField with uid="AXPNZVOHWO",rtseq=6,rtrep=.t.,;
    cFormVar = "w_DEIMPESE", cQueryName = "DEIMPESE",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo operazioni esenti",;
    HelpContextID = 96973947,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=430, Top=26, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oDEIMPESE_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_DERIFPNT))
    endwith
   endif
  endfunc

  add object oDEIMPNIN_1_11 as StdField with uid="TCZAJCFAQD",rtseq=7,rtrep=.t.,;
    cFormVar = "w_DEIMPNIN", cQueryName = "DEIMPNIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo operazioni imponibile IVA non esposta",;
    HelpContextID = 20466556,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=571, Top=26, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oDEIMPNIN_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_DERIFPNT))
    endwith
   endif
  endfunc

  add object oDEIMPLOR_1_13 as StdField with uid="BGDIWMFODR",rtseq=8,rtrep=.t.,;
    cFormVar = "w_DEIMPLOR", cQueryName = "DEIMPLOR",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Imposta operazioni imponibili comprensive imposta afferente",;
    HelpContextID = 214414472,;
   bGlobalFont=.t.,;
    Height=21, Width=139, Left=714, Top=26, cSayPict="v_PV(80)", cGetPict="v_GV(80)"

  func oDEIMPLOR_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_DERIFPNT) and .oParentObject .w_DETIPSOG='F')
    endwith
   endif
  endfunc

  add object oStr_1_4 as StdString with uid="EBNVCBFSRP",Visible=.t., Left=5, Top=5,;
    Alignment=2, Width=139, Height=18,;
    Caption="Operazioni imponibili"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="EBNKEGFQVC",Visible=.t., Left=146, Top=5,;
    Alignment=2, Width=139, Height=18,;
    Caption="Imposta afferente"  ;
  , bGlobalFont=.t.

  add object oStr_1_8 as StdString with uid="IFSQDMEMWU",Visible=.t., Left=283, Top=5,;
    Alignment=2, Width=139, Height=18,;
    Caption="Non imponibili"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="JLDRVGCAUY",Visible=.t., Left=430, Top=5,;
    Alignment=2, Width=139, Height=18,;
    Caption="Operazioni esenti"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="NUFOAXYUSE",Visible=.t., Left=570, Top=5,;
    Alignment=2, Width=141, Height=18,;
    Caption="Impon. IVA non esposta"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="RLFQUZCZMM",Visible=.t., Left=714, Top=5,;
    Alignment=2, Width=139, Height=18,;
    Caption="Compr. impos. afferente"  ;
  , bGlobalFont=.t.

  add object oBox_1_15 as StdBox with uid="EPJVPSKQHH",left=4, top=2, width=851,height=24

  add object oBox_1_16 as StdBox with uid="AMTFIGHGVQ",left=145, top=2, width=2,height=51

  add object oBox_1_17 as StdBox with uid="QYWTSJSUTH",left=285, top=2, width=2,height=51

  add object oBox_1_18 as StdBox with uid="EZKHEIUVJR",left=427, top=2, width=2,height=51

  add object oBox_1_19 as StdBox with uid="VBHUFZLDHJ",left=570, top=2, width=2,height=51

  add object oBox_1_20 as StdBox with uid="PCTUQBCYXK",left=711, top=2, width=2,height=51

  add object oBox_1_21 as StdBox with uid="MWAOOAMQIM",left=4, top=24, width=851,height=29
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_aey','DAELCLFD','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DESERIAL=DAELCLFD.DESERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
