* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bsf                                                        *
*              Opzioni stampa su file                                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][101]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-07-12                                                      *
* Last revis.: 2010-04-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOpt,pFORMAT
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bsf",oParentObject,m.pOpt,m.pFORMAT)
return(i_retval)

define class tgsut_bsf as StdBatch
  * --- Local variables
  pOpt = space(4)
  pFORMAT = space(10)
  w_FORMAT = space(10)
  w_GESTIONE = space(20)
  w_PCNAME = space(20)
  w_OPZIONI = space(254)
  w_OBJ = .NULL.
  w_PARENT = .NULL.
  w_CODICE = space(20)
  w_INIT = .f.
  w_CURSOR = space(10)
  w_CPROWNUM = 0
  w_FORMAT = space(10)
  w_EXTFORMAT = space(10)
  w_TYPECL = space(20)
  w_FLGSEL = space(1)
  w_FLOPEN = space(1)
  w_MSKCFG = space(20)
  w_POSIZIONE = 0
  w_DEFA = 0
  w_TROVATO = .f.
  w_nAbsRow = 0
  w_nRelRow = 0
  * --- WorkFile variables
  STMPFILE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lancia maschera di opzioni associata al formato e alla classe (CP_CHPRN)
    *     Applica valori di default (GSUT_KSF)
    do case
      case this.pOpt="OPEN"
        this.w_FORMAT = this.pFORMAT
        * --- non deve chiamare la mcalc() dell' oggetto chiamante (darebbe errore)
        this.bUpdateParentObject=.f.
        this.w_PCNAME = Left( Sys(0), Rat("#",Sys( 0 ))-1)
        * --- Leggo Gestione
        * --- Read from STMPFILE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.STMPFILE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.STMPFILE_idx,2],.t.,this.STMPFILE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "SFMSKCFG"+;
            " from "+i_cTable+" STMPFILE where ";
                +"SFPCNAME = "+cp_ToStrODBC(this.w_PCNAME);
                +" and SFFORMAT = "+cp_ToStrODBC(this.pFORMAT);
                +" and SFFLGSEL = "+cp_ToStrODBC("S");
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            SFMSKCFG;
            from (i_cTable) where;
                SFPCNAME = this.w_PCNAME;
                and SFFORMAT = this.pFORMAT;
                and SFFLGSEL = "S";
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_GESTIONE = NVL(cp_ToDate(_read_.SFMSKCFG),cp_NullValue(_read_.SFMSKCFG))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows = 0
          * --- Configurazione stampa su file non configurata per la workstation
          * --- Read from STMPFILE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.STMPFILE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.STMPFILE_idx,2],.t.,this.STMPFILE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "SFMSKCFG"+;
              " from "+i_cTable+" STMPFILE where ";
                  +"SFPCNAME = "+cp_ToStrODBC("INSTALLAZIONE");
                  +" and SFFORMAT = "+cp_ToStrODBC(this.pFORMAT);
                  +" and SFFLGSEL = "+cp_ToStrODBC("S");
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              SFMSKCFG;
              from (i_cTable) where;
                  SFPCNAME = "INSTALLAZIONE";
                  and SFFORMAT = this.pFORMAT;
                  and SFFLGSEL = "S";
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_GESTIONE = NVL(cp_ToDate(_read_.SFMSKCFG),cp_NullValue(_read_.SFMSKCFG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
        endif
        * --- Lancio la maschera
        if rat(")",this.w_GESTIONE) = 0
          * --- Non trovo la ) quindi aggiungo io le parentesi
          GESTIONE=this.w_GESTIONE+"(this)"
        endif
        this.w_OBJ = &GESTIONE
        this.w_OBJ = NULL
        * --- Assegno le opzioni alla cp_chprn
        i_retcode = 'stop'
        i_retval = this.w_OPZIONI
        return
      case this.pOpt="DEFA"
        this.w_PARENT = this.oParentObject
        do case
          case this.w_PARENT.w_WORKSTATION=1
            this.w_CURSOR = this.w_PARENT.GSUT_MSF.cTrsname
            * --- Svuoto il cursore del transitorio
            i_del=set("DELETED")
            set deleted off
            Delete from (this.w_CURSOR) where 1=1
            set deleted &i_del
            Select (this.w_CURSOR)
            this.w_PARENT.GSUT_MSF.nLastRow = recno()
            this.w_PARENT.GSUT_MSF.nFirstrow = 1
            * --- Setto  il cprownum a 0
            this.w_PARENT.GSUT_MSF.i_nRowNum = 0
            * --- Select from STMPFILE
            i_nConn=i_TableProp[this.STMPFILE_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.STMPFILE_idx,2],.t.,this.STMPFILE_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select CPROWNUM,SFFORMAT,SFTYPECL,SFFLGSEL,SFMSKCFG,SFEXTFOR,SFFLOPEN  from "+i_cTable+" STMPFILE ";
                  +" where SFPCNAME='INSTALLAZIONE'";
                   ,"_Curs_STMPFILE")
            else
              select CPROWNUM,SFFORMAT,SFTYPECL,SFFLGSEL,SFMSKCFG,SFEXTFOR,SFFLOPEN from (i_cTable);
               where SFPCNAME="INSTALLAZIONE";
                into cursor _Curs_STMPFILE
            endif
            if used('_Curs_STMPFILE')
              select _Curs_STMPFILE
              locate for 1=1
              do while not(eof())
              this.w_CPROWNUM = _Curs_STMPFILE.CPROWNUM
              this.w_FORMAT = _Curs_STMPFILE.SFFORMAT
              this.w_EXTFORMAT = _Curs_STMPFILE.SFEXTFOR
              this.w_TYPECL = _Curs_STMPFILE.SFTYPECL
              this.w_FLGSEL = _Curs_STMPFILE.SFFLGSEL
              this.w_MSKCFG = _Curs_STMPFILE.SFMSKCFG
              this.w_FLOPEN = _Curs_STMPFILE.SFFLOPEN
              * --- Aggiorno le righe del body
              this.w_PARENT.GSUT_MSF.InitRow()     
              this.w_PARENT.GSUT_MSF.WorkFromTrs()     
              this.w_PARENT.GSUT_MSF.w_SFPCNAME = this.w_PARENT.w_PCNAME
              this.w_PARENT.GSUT_MSF.w_CPROWNUM = this.w_CPROWNUM
              this.w_PARENT.GSUT_MSF.w_SFFORMAT = this.w_FORMAT
              this.w_PARENT.GSUT_MSF.w_SFEXTFOR = this.w_EXTFORMAT
              this.w_PARENT.GSUT_MSF.w_SFTYPECL = this.w_TYPECL
              this.w_PARENT.GSUT_MSF.w_SFFLGSEL = this.w_FLGSEL
              this.w_PARENT.GSUT_MSF.w_SFMSKCFG = this.w_MSKCFG
              this.w_PARENT.GSUT_MSF.w_SFFLOPEN = this.w_FLOPEN
              this.w_PARENT.GSUT_MSF.TrsFromWork()     
              this.w_PARENT.GSUT_MSF.mCalc(.t.)     
              this.w_PARENT.GSUT_MSF.setfocus()     
              this.w_PARENT.GSUT_MSF.ChildrenChangeRow()     
              this.w_PARENT.GSUT_MSF.oPgFrm.Page1.oPag.oBody.Refresh()     
                select _Curs_STMPFILE
                continue
              enddo
              use
            endif
          case this.w_PARENT.w_WORKSTATION=2
            * --- Carico le impostazioni di default in tutte le workstation
            if ah_YesNo("Le impostazioni salvate saranno applicate a tutte le workstation gi� configurate%0Continuare?")
              * --- Select from STMPFILE
              i_nConn=i_TableProp[this.STMPFILE_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.STMPFILE_idx,2],.t.,this.STMPFILE_idx)
              if i_nConn<>0
                cp_sqlexec(i_nConn,"select DISTINCT SFPCNAME  from "+i_cTable+" STMPFILE ";
                    +" where SFPCNAME <> 'INSTALLAZIONE'";
                     ,"_Curs_STMPFILE")
              else
                select DISTINCT SFPCNAME from (i_cTable);
                 where SFPCNAME <> "INSTALLAZIONE";
                  into cursor _Curs_STMPFILE
              endif
              if used('_Curs_STMPFILE')
                select _Curs_STMPFILE
                locate for 1=1
                do while not(eof())
                this.w_CODICE = _Curs_STMPFILE.SFPCNAME
                * --- Cancello i record appartenenti alla workstation selezionata
                * --- Delete from STMPFILE
                i_nConn=i_TableProp[this.STMPFILE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.STMPFILE_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                        +"SFPCNAME = "+cp_ToStrODBC(this.w_CODICE);
                         )
                else
                  delete from (i_cTable) where;
                        SFPCNAME = this.w_CODICE;

                  i_Rows=_tally
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if bTrsErr
                  * --- Error: delete not accepted
                  i_Error=MSG_DELETE_ERROR
                  return
                endif
                * --- Inserisco i valori di default
                * --- Insert into STMPFILE
                i_nConn=i_TableProp[this.STMPFILE_idx,3]
                i_cTable=cp_SetAzi(i_TableProp[this.STMPFILE_idx,2])
                i_commit = .f.
                if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                  cp_BeginTrs()
                  i_commit = .t.
                endif
                if i_nConn<>0
                  i_Rows=cp_InsertIntoVQR(this,i_nConn,i_cTable,"query\gsut_bsf.vqr",this.STMPFILE_idx)
                else
                  error "not yet implemented!"
                endif
                if i_commit
                  cp_EndTrs(.t.)
                endif
                if i_Rows<0 or bTrsErr
                  * --- Error: insert not accepted
                  i_Error=MSG_INSERT_ERROR
                  return
                endif
                  select _Curs_STMPFILE
                  continue
                enddo
                use
              endif
            endif
        endcase
      case this.pOpt="CHKS"
        * --- Controlla Flag Selezionato da GSUT_MSF
        *     Changed SFLGSEL
        SELECT (this.oParentObject.cTrsName)
        * --- Memorizzo la posizione corrente
        this.w_POSIZIONE = IIF( Eof() , RECNO(this.oParentObject.cTrsName)-1 , RECNO(this.oParentObject.cTrsName) )
        this.w_nAbsRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nAbsRow
        this.w_nRelRow = this.oParentObject.oPgFrm.Page1.oPag.oBody.nRelRow
        if this.w_POSIZIONE<>0 And this.oParentObject.w_SFFLGSEL="S"
          * --- Nel Transitorio un flag attivo vale 1
          this.w_DEFA = 1
          * --- Verifico se presente una sede su una riga diversa dalla corrente avente il check attivo
          SELECT (this.oParentObject.cTrsName)
          Go Top
          LOCATE FOR t_SFFORMAT = this.oParentObject.w_SFFORMAT and t_SFFLGSEL = this.w_DEFA And Not Deleted() 
          this.w_TROVATO = Found()
          if this.w_TROVATO And Recno()<>this.w_POSIZIONE
            this.oParentObject.WorkFromTrs
            ah_ErrorMsg("Attenzione: %1 non sar� pi� la classe predefinita per il formato %2", 48 ,"",ALLTRIM( this.oParentObject.w_SFTYPECL ),ALLTRIM( this.oParentObject.w_SFFORMAT ))
            this.oParentObject.w_SFFLGSEL = "N"
            this.oParentObject.TrsFromWork
            if This.oParentObject.cFunction = "Edit" And I_Srv<>"A"
              SELECT (this.oParentObject.cTrsName)
              Replace I_Srv With "U"
            endif
          else
            * --- Il primo trovato potrebbe essere la classe appena selezionata, devo andare avanti nella ricerca
            if this.w_TROVATO
              Continue
              if Found() And Recno()<>this.w_POSIZIONE
                this.oParentObject.WorkFromTrs
                ah_ErrorMsg("Attenzione: %1 non sar� pi� la classe predefinita per il formato %2", 48 ,"",ALLTRIM( this.oParentObject.w_SFTYPECL ),ALLTRIM( this.oParentObject.w_SFFORMAT ))
                this.oParentObject.w_SFFLGSEL = "N"
                this.oParentObject.TrsFromWork
                if This.oParentObject.cFunction = "Edit" And I_Srv<>"A"
                  SELECT (this.oParentObject.cTrsName)
                  Replace I_Srv With "U"
                endif
              endif
            endif
          endif
        endif
        * --- Mi riposiziono nel Record 
         SELECT (this.oParentObject.cTrsName) 
 GOTO this.w_POSIZIONE 
 With this.oParentObject 
 .WorkFromTrs() 
 .SaveDependsOn() 
 .SetControlsValue() 
 .ChildrenChangeRow() 
 .oPgFrm.Page1.oPag.oBody.Refresh() 
 .oPgFrm.Page1.oPag.oBody.nAbsRow= this.w_nAbsRow 
 .oPgFrm.Page1.oPag.oBody.nRelRow= this.w_nRelRow 
 EndWith
    endcase
  endproc


  proc Init(oParentObject,pOpt,pFORMAT)
    this.pOpt=pOpt
    this.pFORMAT=pFORMAT
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='STMPFILE'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_STMPFILE')
      use in _Curs_STMPFILE
    endif
    if used('_Curs_STMPFILE')
      use in _Curs_STMPFILE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOpt,pFORMAT"
endproc
