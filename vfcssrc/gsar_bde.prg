* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bde                                                        *
*              Esplosione distinta base                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_299]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-10-30                                                      *
* Last revis.: 2015-06-16                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOpe
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bde",oParentObject,m.pTipOpe)
return(i_retval)

define class tgsar_bde as StdBatch
  * --- Local variables
  pTipOpe = space(1)
  w_TIPOPE = space(1)
  w_CODINI = space(20)
  w_CODFIN = space(20)
  w_DATVAL = ctod("  /  /  ")
  w_CODART = space(20)
  w_NUMLEV = 0
  w_NUMRIG = 0
  w_CONTADIS = 0
  w_DISPAD = space(20)
  w_OLDPAD = space(20)
  w_FLVARI = space(1)
  w_FLSTAT = space(1)
  w_CODCOM = space(20)
  w_CODRIS = space(15)
  w_DESCOM = space(40)
  w_UNIMIS = space(3)
  w_QTANET = 0
  w_QTANE1 = 0
  w_QTAMOV = 0
  w_QTAUM1 = 0
  w_QTAREC = 0
  w_QTARE1 = 0
  w_QTAMOL = 0
  w_COEIMP = space(15)
  w_PERSCA = 0
  w_RECSCA = 0
  w_PERSFR = 0
  w_RECSFR = 0
  w_PERRIC = 0
  w_FLESPL = space(1)
  w_ARTCOM = space(20)
  w_ARTVAR = space(20)
  w_CODDIS = space(20)
  w_TIPART = space(2)
  w_PROPRE = space(1)
  w_TIPGES = space(1)
  w_NLEVEL = space(4)
  w_MAGPRE = space(5)
  w_FLPREV = space(1)
  w_USCITA = .f.
  w_OKCOE = .f.
  w_TESTLV = 0
  w_MESS = space(20)
  w_MESSERR = space(20)
  w_UNMIS1 = space(3)
  w_UNMIS2 = space(3)
  w_UNMIS3 = space(3)
  w_OPERAT = space(1)
  w_OPERA3 = space(1)
  w_MOLTIP = 0
  w_MOLTI3 = 0
  w_RIFFAS = 0
  w_APPO = space(1)
  w_VERI1 = space(1)
  w_VERI2 = space(1)
  w_VERI3 = space(1)
  w_MVDESSUP = space(0)
  w_ARTPAD = space(20)
  w_TMPC = space(20)
  w_TVPADSUPREMO = .f.
  w_CODAZI = space(5)
  w_CPROWNUM = 0
  w_CPROWORD = 0
  w_CONCAR = space(1)
  w_CORLEA = 0
  w_nRec = 0
  w_nRecTot = 0
  w_BDESART = .f.
  w_RECSFRT = 0
  w_RECSCAT = 0
  w_PERREC = 0
  * --- WorkFile variables
  DISMBASE_idx=0
  PAR_DISB_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue Esplosione Distinta Base
    * --- Tipo Operazione: 
    *          pTipOpe='A' - Cicla sugli Articoli : 
    *                                             Batch stampa verifica di fattibilitą GSDS_BVF
    *                                             Batch stampa distinta base GSDS_BSD
    *                                             Batch stampa piano di produzione GSDS_BPP
    *                                             Elabora impegno componenti GSDS_BIC
    *                                             Batch stampa distinta base costificata GSDS_BDC da GSDS_SDC (distinta costificata)
    *                                             Elabora carico da produzione GSDS_BCA
    *                                             Gestione ODL/OCL GSCO_BOL
    *                                             Cambio stato ODL/OCL GSCO_BGS 
    *                                             Carica materiali terzista GSCO_BC1
    *          pTipOpe='D' - Cicla sulle Distinte :
    *                                             Esplosione treeview distinta base GSDS_BTV
    *                                             Verifica distinta base ricorsiva GSDS_BKD
    *          pTipOpe='P' - Cicla sul Cursore gia' generato :
    *          pTipOpe='S' - Ricalcola i Coefficenti di Impiego e non elimina il cursore :
    *                                             Esplosione da documenti GSAR_BEA
    *                                             Batch stampa distinta base costificata GSDS_BDC da GSDS_BCA (Carico da Produzione)
    *          pTipOpe='C' - Non ricalcola i Coefficenti di Impiego e chiude il cursore :
    *                                             Batch stampa distinta base costificata GSDS_BDC da GSAR_BEA (Esplosione da documenti)
    *          pTipOpe='T' - Non ricalcola i Coefficenti di Impiego e non chiude il cursore :
    *                                             Elabora carico da produzione GSDS_BCA
    *          pTipOpe='M' - Cicla sul Cursore gia' generato :
    *                                             Elaborazione MRP(generazione fabbisogni) GSMR_BGP
    *          pTipOpe='R' - Configuratore di caratteristiche
    *                                             Esplode solo i nodi configurabili (ARCONCAR='S')
    * --- Legge Componenti Distinta Principale
    * --- NOTA:
    * --- ================
    * --- w_MAXLEVEL 
    * --- Indica il Numero massimo di Livelli di esplosione della Distinta
    * --- I Numero massimo di Livelli e' 99, tuttavia se viene passato il valore di 999 
    * --- Verranno esplose tutte le sottodistinte comprese quelle che non hanno il flag di esplosione.
    * --- ================
    * --- w_VERIFICA
    * --- Indica se deve eseguire il controllo di congruita' nelle sottodistinte 
    * --- Se Il primo carattere: 'S' =Verifica, 'T'='Verifica+ritorna valore al programma chiamante ; <altro>= no verifica
    * --- Puo' anche essere di 2 Caratteri, in questo caso il secondo carattere se='N' disattiva il calcolo sui coefficienti di Impiego
    * --- Puo' anche essere di 3 Caratteri, in questo caso il Terzo carattere se='C' Considera anche gli articoli gestiti a Consumo
    * --- ================
    * --- Legge parametro relativo alla modalitą di definizione ed utilizzo delle varianti)
    this.w_CODAZI = i_CODAZI
    * --- Read from PAR_DISB
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_DISB_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_DISB_idx,2],.t.,this.PAR_DISB_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "PDMODVAR"+;
        " from "+i_cTable+" PAR_DISB where ";
            +"PDCODAZI = "+cp_ToStrODBC(this.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        PDMODVAR;
        from (i_cTable) where;
            PDCODAZI = this.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_TMPC = NVL(cp_ToDate(_read_.PDMODVAR),cp_NullValue(_read_.PDMODVAR))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_TVPADSUPREMO = (this.w_TMPC="S")
    * --- ================
    this.w_APPO = LEFT(ALLTRIM(this.oParentObject.w_VERIFICA)+SPACE(3), 3)
    this.w_VERI1 = SUBSTR(this.w_APPO, 1, 1)
    this.w_VERI2 = SUBSTR(this.w_APPO, 2, 1)
    this.w_VERI3 = SUBSTR(this.w_APPO, 3, 1)
    this.w_MESSERR = " "
    * --- Crea Cursore di Appoggio
    CREATE CURSOR TES_PLOS ;
    (CODART C(20), NUMRIG N(10,0), DISPAD C(20), ARTPAD C(20), FLVARI C(1), CPROWNUM N(6), CPROWORD N(6), CODCOM C(20), NUMLEV C(4), CODRIS C(15), DESCOM C(40), UNIMIS C(3), ;
    QTANET N(15,6), QTANE1 N(15,6), QTAMOV N(15,6), QTAUM1 N(15,6), QTAREC N(15,6), QTARE1 N(15,6), COEIMP C(15), PERSCA N(6,2), RECSCA N(15,6), PERSFR N(6,2), RECSFR N(15,6), ;
    PERRIC N(6,2), FLESPL C(1), ARTCOM C(20), CODDIS C(20), TIPART C(2), ARTVAR C(20), FLSTAT C(1), PROPRE C(1), TIPGES C(1), ;
    MAGPRE C(5), FLPREV C(1), RIFFAS N(4,0), UNMIS0 C(3), MVDESSUP M(10), CONCAR C(1),CORLEA N(7,2))
    * --- Legge la Distinta da Elaborare
    this.w_USCITA = .F.
    this.w_OKCOE = .F.
    this.w_CONTADIS = 0
    this.w_CODINI = this.oParentObject.w_DBCODINI
    this.w_CODFIN = this.oParentObject.w_DBCODFIN
    this.w_DATVAL = this.oParentObject.w_DATFIL
    this.w_TIPOPE = this.pTipOpe
    if this.w_TIPOPE$ "A-R-S-C-T"
      if !USED("ELABREC")
        * --- Cerca Distinte ciclando sugli Articoli
        VQ_EXEC("QUERY\GSARABED.VQR", this,"ELABREC")
      endif
    else
      if this.w_TIPOPE<>"P" and this.w_TIPOPE<>"M"
        VQ_EXEC("QUERY\GSARDBED.VQR", this,"ELABREC")
      endif
    endif
    if USED("ELABREC")
      SELECT ELABREC
      this.w_nRecTot = RECCOUNT("ELABREC")
      GO TOP
      SCAN
      this.w_DISPAD = NVL(DBCODICE,"")
      this.w_DESCOM = NVL(DBDESCRI, SPACE(40))
      this.w_CODRIS = NVL(DBCODRIS, SPACE(15))
      this.w_FLSTAT = NVL(DBFLSTAT, " ")
      this.w_CODART = NVL(DBCODART, SPACE(20))
      this.w_NUMLEV = 0
      this.w_NUMRIG = 1
      this.w_FLVARI = " "
      this.w_CPROWNUM = 0
      this.w_CPROWORD = 0
      this.w_CODCOM = SPACE(20)
      this.w_CORLEA = 0
      this.w_UNIMIS = NVL(DBUNIMIS,SPACE(3))
      SELECT ELABREC
      if type("ELABREC.QTAMOV")="N"
        this.w_QTANET = NVL(QTAMOV, 1)
        this.w_QTANE1 = NVL(QTAMOV, 1)
        this.w_QTAMOV = NVL(QTAMOV, 1)
        this.w_QTAUM1 = NVL(QTAMOV, 1)
        this.w_QTAREC = NVL(QTAMOV, 1)
        this.w_QTARE1 = NVL(QTAMOV, 1)
      else
        this.w_QTANET = 1
        this.w_QTANE1 = 1
        this.w_QTAMOV = 1
        this.w_QTAUM1 = 1
        this.w_QTAREC = 1
        this.w_QTARE1 = 1
      endif
      this.w_COEIMP = SPACE(15)
      this.w_PERSCA = 0
      this.w_RECSCA = 0
      this.w_PERSFR = 0
      this.w_RECSFR = 0
      this.w_PERRIC = 0
      this.w_FLESPL = " "
      this.w_PROPRE = ARPROPRE
      this.w_TIPGES = " "
      this.w_MAGPRE = SPACE(5)
      this.w_FLPREV = " "
      this.w_ARTCOM = SPACE(20)
      this.w_CODDIS = NVL(DBCODICE,"")
      this.w_RIFFAS = 0
      this.w_NLEVEL = RIGHT("0000"+ALLTRIM(STR(this.w_NUMLEV)),4)
      this.w_ARTPAD = this.w_CODART
      this.w_MVDESSUP = NVL(DBNOTAGG, " " )
      this.w_TIPART = NVL(ARTIPART, SPACE(2))
      this.w_CONCAR = IIF(this.pTipOpe="R", NVL(ARCONCAR, "N"), "N")
      * --- Scrive Riga Distinta
      if NOT EMPTY(this.w_DISPAD) AND this.w_USCITA=.F. AND (this.w_FLSTAT<>"P" OR this.oParentObject.w_FILSTAT<>"P")
        if this.oParentObject.w_DBCODINI<>this.oParentObject.w_DBCODFIN
          this.w_nRec = this.w_nRec + 1
          if MOD(this.w_nRec ,1*iif(this.w_nRecTot>1000,10,1)*iif(this.w_nRecTot>10000,50,1))=0
            ah_msg("Elaborazione distinta %1/%2 in corso...",.t.,.f.,.f., alltrim(str(this.w_nRec)), alltrim(str(this.w_nRecTot)) )
          endif
        endif
        INSERT INTO TES_PLOS ;
        (CODART, NUMRIG, DISPAD, ARTPAD, FLVARI, CPROWNUM, CPROWORD, CODCOM, NUMLEV, CODRIS, DESCOM, UNIMIS, QTANET, QTANE1, QTAMOV, QTAUM1, QTAREC, QTARE1, COEIMP, ;
        PERSCA, RECSCA, PERSFR, RECSFR, PERRIC, FLESPL, ARTCOM, CODDIS, TIPART, FLSTAT, PROPRE, TIPGES, MAGPRE, FLPREV, RIFFAS, UNMIS0, MVDESSUP, CONCAR, CORLEA) ;
        VALUES (this.w_CODART, this.w_NUMRIG, this.w_DISPAD, this.w_ARTPAD, this.w_FLVARI, this.w_CPROWNUM, this.w_CPROWORD, this.w_CODCOM, this.w_NLEVEL, this.w_CODRIS, this.w_DESCOM, ;
        this.w_UNIMIS, this.w_QTANET, this.w_QTANE1, this.w_QTAMOV, this.w_QTAUM1, this.w_QTAREC, this.w_QTARE1, this.w_COEIMP, this.w_PERSCA, this.w_RECSCA, this.w_PERSFR, this.w_RECSFR, this.w_PERRIC, ;
        this.w_FLESPL, this.w_ARTCOM, this.w_CODDIS, this.w_TIPART, IIF(this.w_FLSTAT="P", "*", " "), this.w_PROPRE, this.w_TIPGES, this.w_MAGPRE, this.w_FLPREV, this.w_RIFFAS, this.w_UNIMIS, this.w_MVDESSUP, this.w_CONCAR, this.w_CORLEA)
        this.w_NUMLEV = 1
        if this.w_VERI1 $ "ST"
          * --- Crea Cursore per verifiche di Ricorsivita'
          CREATE CURSOR VERFAT (DISPAD C(20), NUMLEV N(4,0))
          INSERT INTO VERFAT (DISPAD, NUMLEV) VALUES (this.w_DISPAD, 0)
        endif
        * --- Inizia ad Esplodere i Componenti
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      SELECT ELABREC
      ENDSCAN
      if this.w_USCITA=.F. AND this.w_OKCOE=.T. AND this.w_VERI2<>"N"
        if Not this.pTipOpe $ "C-T"
          SELECT CODART, CODCOM, COEIMP, MAX(QTAMOV*0) AS QTAMOL, MAX(UNIMIS) AS UNIMIS,NUMRIG*0 AS CPROWNUM ;
          FROM TES_PLOS WHERE NOT EMPTY(COEIMP) GROUP BY 1, 2, 3 ORDER BY 1, 2 INTO CURSOR APPCOE
        endif
        if USED("APPCOE")
          SELECT APPCOE
          if RECCOUNT()>0
            if Not this.pTipOpe $ "C-T"
              =WRCURSOR("APPCOE")
              * --- Gestione / Calcolo Coefficienti di Impiego per Singolo Componente
              do GSDS_BCI with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            SELECT APPCOE
            GO TOP
            SCAN FOR NOT EMPTY(CODART) AND NOT EMPTY(CODCOM) AND NOT EMPTY(COEIMP) AND QTAMOL<>0 
            this.w_CODART = CODART
            this.w_CODCOM = CODCOM
            this.w_COEIMP = COEIMP
            this.w_QTAMOL = QTAMOL
            SELECT TES_PLOS
            GO TOP
            UPDATE TES_PLOS SET QTAMOV=cp_ROUND(QTAMOV*this.w_QTAMOL, 6), ;
            QTAUM1=cp_ROUND(QTAUM1*this.w_QTAMOL, 6), ;
            QTANET=cp_ROUND(QTANET*this.w_QTAMOL, 6), ;
            QTANE1=cp_ROUND(QTANE1*this.w_QTAMOL, 6), ;
            QTAREC=cp_ROUND(QTAREC*this.w_QTAMOL, 6), ;
            QTARE1=cp_ROUND(QTARE1*this.w_QTAMOL, 6) ;
             WHERE CODART=this.w_CODART AND CODCOM=this.w_CODCOM AND COEIMP=this.w_COEIMP
            SELECT TES_PLOS
            GO TOP
            SELECT APPCOE
            ENDSCAN
          endif
          if Not this.pTipOpe $ "S-T"
            if USED("APPCOE")
              SELECT APPCOE
              USE
            endif
          endif
        endif
      endif
    endif
    if USED("VERFAT")
      SELECT VERFAT
      USE
    endif
    if USED("ELABREC")
      SELECT ELABREC
      USE
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Dichiarata come variabile locale altrimenti mantiene sempre l'ultimo valore assegnato
    *     pag2 viene chiamata ciclicamente
    * --- Procedura di Esplosione
    PRIVATE NOMCUR
    NOMCUR = "XCUR"+RIGHT("0000"+ALLTRIM(STR(this.w_NUMLEV)), 4)
    this.w_BDESART = .f.
    if this.w_TIPOPE= "A" and TYPE("this.oparentobject.w_CDESART")="C" and this.oparentobject.w_CDESART="S"
      this.w_BDESART = .t.
    endif
    if this.pTipOpe="R" And this.w_CONCAR="S" Or this.pTipOpe<>"R"
* --- Area Manuale = Ricerca
* --- gsar_bde
* --- Esegue direttamente la query via ODBC
private comando
comando="select DISMBASE.DBCODICE as CODICE,"+ cp_ToStrODBC(this.w_ARTPAD)+" as ARTPAD,"+ "DISMBASE.DBCODRIS as CODRIS,DISTBASE.CPROWNUM as NUMROW,"+ "DISTBASE.CPROWORD as ROWORD,DISTBASE.DBFLVARI as FLVARI,"+ "DISTBASE.DBCODCOM as CODCOM,DISTBASE.DBDESCOM as DESCOM,"+ "DISTBASE.DB__NOTE as NOTAGG,DISTBASE.DBUNIMIS as UNIMIS,"+ "DISTBASE.DBQTADIS as QTAMOV,"+ "DISTBASE.DBCOEIMP as COEIMP,DISTBASE.DBPERSCA as PERSCA,"+ "DISTBASE.DBRECSCA as RECSCA,DISTBASE.DBPERSFR as PERSFR,"+ "DISTBASE.DBRECSFR as RECSFR,DISTBASE.DBPERRIC as PERRIC,"+ "DISTBASE.DBFLESPL as FLESPL,DISTBASE.DBARTCOM as ARTCOM,"+ "ART_ICOL.ARCODDIS as CODDIS,ART_ICOL.ARTIPART as TIPART,COM_VARI.CVCODART,COM_VARI.CPROWNUM,"+ "ART_ICOL.ARTIPGES as TIPGES,ART_ICOL.ARPROPRE as PROPRE,ART_ICOL1.ARTIPGES as CVTIPGES,ART_ICOL1.ARTIPART AS CVTIPART,"+ "ART_ICOL1.ARPROPRE as CVPROPRE,"+ "ART_ICOL.ARMAGPRE as MAGPRE,ART_ICOL1.ARMAGPRE as CVMAGPRE,"+ "ART_ICOL.ARTIPPRE as FLPREV,ART_ICOL1.ARTIPPRE as CVFLPREV,ART_ICOL.ARCONCAR AS ARCONCAR,"+ "COM_VARI.CPROWORD,COM_VARI.CVCODCOM,COM_VARI.CVUNIMIS,"+ "COM_VARI.CVQTADIS,COM_VARI.CVCOEIMP,"+ "COM_VARI.CVFLESPL,COM_VARI.CVARTCOM,ART_ICOL1.ARCODDIS as CVCODDIS,"+ "DISTBASE.DBINIVAL,DISTBASE.DBFINVAL,DISMBASE.DBFLSTAT as FLSTAT,"+ "ART_ICOL.ARUNMIS1 as UNMIS1,ART_ICOL.ARUNMIS2 as UNMIS2,KEY_ARTI.CAUNIMIS as UNMIS3,"+ "ART_ICOL.AROPERAT as OPERAT,KEY_ARTI.CAOPERAT as OPERA3,"+ "ART_ICOL.ARMOLTIP as MOLTIP,KEY_ARTI.CAMOLTIP as MOLTI3,"+ "ART_ICOL1.ARUNMIS1 as CVUNMIS1,ART_ICOL1.ARUNMIS2 as CVUNMIS2,KEY_ARTI1.CAUNIMIS as CVUNMIS3,"+ "ART_ICOL1.AROPERAT as CVOPERAT,KEY_ARTI1.CAOPERAT as CVOPERA3,ART_ICOL1.ARMOLTIP as CVMOLTIP,"+ "KEY_ARTI1.CAMOLTIP as CVMOLTI3,DISTBASE.DBRIFFAS as RIFFAS,KEY_ARTI1.CADESART as CADES,DISTBASE.DBCORLEA as CORLEA "+ "from ((((((xxxDISTBASE DISTBASE Inner Join xxxDISMBASE DISMBASE "+ "on DISMBASE.DBCODICE=DISTBASE.DBCODICE) "+ "Left outer Join xxxART_ICOL ART_ICOL "+ "on ART_ICOL.ARCODART=DISTBASE.DBARTCOM) "+ "Left outer Join xxxCOM_VARI COM_VARI "+ "on DISTBASE.DBCODICE=COM_VARI.CVCODICE and DISTBASE.CPROWNUM=COM_VARI.CVNUMRIF) "+ "Left outer Join xxxART_ICOL ART_ICOL1 "+ "on ART_ICOL1.ARCODART=COM_VARI.CVARTCOM) "+ "Left outer Join xxxKEY_ARTI KEY_ARTI1 "+ "on KEY_ARTI1.CACODICE=COM_VARI.CVCODCOM) "+ "Left outer Join xxxKEY_ARTI KEY_ARTI "+ "on KEY_ARTI.CACODICE=DISTBASE.DBCODCOM) "
DO CASE
CASE UPPER(CP_DBTYPE)='ORACLE'
   * --- ORACLE
comando=strtran(comando,'xxx', ALLTRIM(i_CODAZI))+ "where DISMBASE.DBCODICE = "+cp_ToStrODBC(this.w_DISPAD)+ " AND (DISTBASE.DBINIVAL <= "+cp_ToStrODBC(this.w_DATVAL)+ " OR DISTBASE.DBINIVAL IS NULL) "+ " AND (DISTBASE.DBFINVAL >= "+cp_ToStrODBC(this.w_DATVAL)+ " OR DISTBASE.DBFINVAL IS NULL ) "+ "order by  1 , 5 , 3 , 32  desc, 23 "
CASE UPPER(CP_DBTYPE)='DB2'
   * --- IBM DB2
comando=strtran(comando,'xxx', ALLTRIM(i_CODAZI))+ "where DISMBASE.DBCODICE = "+cp_ToStrODBC(this.w_DISPAD)+ " AND (DISTBASE.DBINIVAL <= "+cp_ToStrODBC(this.w_DATVAL)+ " OR COALESCE(LENGTH(DISTBASE.DBINIVAL),0) = 0) "+ " AND (DISTBASE.DBFINVAL >= "+cp_ToStrODBC(this.w_DATVAL)+ " OR COALESCE(LENGTH(DISTBASE.DBFINVAL),0) = 0) "+ "order by  1 , 5 , 3 , 32  desc, 23 "

OTHERWISE
   * --- DEFAULT SQL SERVER
comando=strtran(comando,'xxx', ALLTRIM(i_CODAZI))+ "where DISMBASE.DBCODICE = "+cp_ToStrODBC(this.w_DISPAD)+ " AND (DISTBASE.DBINIVAL <= "+cp_ToStrODBC(this.w_DATVAL)+ " OR DISTBASE.DBINIVAL IS NULL) "+ " AND (DISTBASE.DBFINVAL >= "+cp_ToStrODBC(this.w_DATVAL)+ " OR DISTBASE.DBFINVAL IS NULL ) "+ "order by  1 , 5 , 3 , 32  desc, 23 "

ENDCASE

private L_errsql
i_nConn=i_TableProp[this.DISMBASE_idx,3]
L_errsql = sqlexec(i_nConn,comando,NOMCUR)
if L_errsql<0
  ah_Msg("errore SQL:%0%1",.T.,.F.,.F., message())
endif
* --- Fine Area Manuale
    endif
    if USED(NOMCUR)
      if RECCOUNT(NOMCUR)>0
        * --- Inizio Elaborazione vera e propria
        SELECT (NOMCUR)
        GO TOP
        this.w_CODRIS = NVL(CODRIS, SPACE(15))
        this.w_FLSTAT = IIF(NVL(FLSTAT, " ")="P", "P", " ") 
        if NOT EMPTY(NVL(CODICE,"")) AND this.w_FLSTAT="P" AND this.oParentObject.w_MAXLEVEL<>999
          ah_ErrorMsg("Distinta: %1 provvisoria",,"", CODICE)
        endif
        if this.w_NUMLEV>1
          if NOT EMPTY(this.w_CODRIS)
            * --- Inserisce il Ciclo Semplificato sul Padre e Aggiorna lo Status
            UPDATE TES_PLOS SET CODRIS = this.w_CODRIS, FLSTAT = IIF(this.w_FLSTAT="P", "*", " ") WHERE CODART=this.w_CODART AND ;
            NUMRIG=this.w_NUMRIG AND DISPAD=this.w_OLDPAD AND CODCOM=this.w_CODCOM AND NUMLEV=this.w_NLEVEL
          else
            * --- Aggiorna lo Status
            UPDATE TES_PLOS SET FLSTAT = IIF(this.w_FLSTAT="P", "*", " ") WHERE CODART=this.w_CODART AND ;
            NUMRIG=this.w_NUMRIG AND DISPAD=this.w_OLDPAD AND CODCOM=this.w_CODCOM AND NUMLEV=this.w_NLEVEL
          endif
        endif
        SCAN FOR NOT EMPTY(NVL(CODICE,"")) AND NVL(NUMROW,0)<>0
        this.w_DISPAD = CODICE
        this.w_ARTPAD = NVL(ARTPAD, SPACE(20))
        this.w_CPROWNUM = NUMROW
        this.w_CPROWORD = ROWORD
        this.w_FLVARI = NVL(FLVARI, " ")
        this.w_CPROWNUM = NUMROW
        this.w_CPROWORD = ROWORD
        this.w_CODCOM = NVL(CODCOM, SPACE(20))
        this.w_DESCOM = NVL(DESCOM, SPACE(40))
        this.w_UNIMIS = NVL(UNIMIS, "   ")
        this.w_QTANET = NVL(QTAMOV, 0)
        this.w_QTAMOV = NVL(QTAMOV, 0)
        this.w_QTAREC = NVL(QTAMOV, 0)
        this.w_QTANE1 = NVL(QTAMOV, 0)
        this.w_QTAUM1 = NVL(QTAMOV, 0)
        this.w_QTARE1 = NVL(QTAMOV, 0)
        this.w_UNMIS1 = NVL(UNMIS1, "   ")
        this.w_UNMIS2 = NVL(UNMIS2, "   ")
        this.w_UNMIS3 = NVL(UNMIS3, "   ")
        this.w_OPERAT = NVL(OPERAT, " ")
        this.w_OPERA3 = NVL(OPERA3, " ")
        this.w_MOLTIP = NVL(MOLTIP, 0)
        this.w_MOLTI3 = NVL(MOLTI3, 0)
        this.w_RIFFAS = NVL(RIFFAS, 0)
        this.w_MVDESSUP = NVL(NOTAGG, " ")
        this.w_TIPART = NVL(TIPART, SPACE(2))
        this.w_CORLEA = NVL(CORLEA, 0)
        this.w_RECSCAT = 0
        this.w_RECSFRT = 0
        * --- Se UM diversa, Calcola Qta alla 1^UM
        if this.w_UNIMIS<>this.w_UNMIS1
          * --- Derivato da CALMMLIS
          do case
            case this.w_UNIMIS=this.w_UNMIS3 AND this.w_MOLTI3<>0
              * --- Selezionata 3^ Unita' di Misura
              this.w_QTAUM1 = cp_ROUND((this.w_QTAUM1 * IIF(this.w_OPERA3="/", this.w_MOLTI3, 1)) / IIF(this.w_OPERA3="/", 1, this.w_MOLTI3), 6)
            case this.w_UNIMIS=this.w_UNMIS2 AND this.w_MOLTIP<>0
              * --- Selezionata 2^ Unita' di Misura
              this.w_QTAUM1 = cp_ROUND((this.w_QTAUM1 * IIF(this.w_OPERAT="/", this.w_MOLTIP, 1)) / IIF(this.w_OPERAT="/", 1, this.w_MOLTIP), 6)
          endcase
          this.w_QTANE1 = this.w_QTAUM1
          this.w_QTARE1 = this.w_QTAUM1
        endif
        this.w_COEIMP = NVL(COEIMP, SPACE(15))
        * --- Testa se il Componente della Distinta contiene almeno un Coefficiente di Impiego
        this.w_OKCOE = IIF(NOT EMPTY(this.w_COEIMP), .T., this.w_OKCOE)
        this.w_PERSCA = NVL(PERSCA, 0)
        this.w_RECSCA = NVL(RECSCA, 0)
        this.w_PERSFR = NVL(PERSFR, 0)
        this.w_RECSFR = NVL(RECSFR, 0)
        this.w_PERRIC = NVL(PERRIC, 0)
        this.w_PROPRE = NVL(PROPRE, " ")
        this.w_TIPGES = NVL(TIPGES, " ")
        this.w_ARTCOM = NVL(ARTCOM, SPACE(20))
        this.w_CODDIS = NVL(CODDIS, SPACE(20))
        this.w_TIPART = NVL(TIPART, SPACE(2))
        this.w_ARTVAR = NVL(CVCODART, SPACE(20))
        this.w_FLESPL = IIF(this.w_NUMLEV=this.oParentObject.w_MAXLEVEL," ",NVL(FLESPL, " "))
        if NOT EMPTY(this.w_CODDIS) AND this.oParentObject.w_MAXLEVEL=999
          * --- Trucco per esplodere sempre la distinta nei sottocomponenti
          this.w_FLESPL = "S"
        endif
        this.w_MAGPRE = NVL(MAGPRE, SPACE(5))
        this.w_FLPREV = NVL(FLPREV, " ")
        if this.w_FLVARI="S" 
          * --- Componente Variante assegna il i Dati del Componente alla Variante
          this.w_TMPC = IIF(this.w_TVPADSUPREMO, this.w_CODART, this.w_ARTPAD)
          if NVL(CVCODART, SPACE(20))=this.w_TMPC OR EMPTY(this.w_TMPC) OR this.w_TIPOPE="D"
            * --- Assegna solo se l'Articolo sulla Variante e'  unguale all'Articolo da Esplodere oppure no Selezione Articoli (tutti)
            this.w_CODCOM = NVL(CVCODCOM, SPACE(20))
            if this.w_BDESART
              this.w_DESCOM = NVL(CADES, SPACE(40))
            endif
            this.w_UNIMIS = NVL(CVUNIMIS, "   ")
            this.w_QTANET = NVL(CVQTADIS, 0)
            this.w_QTAMOV = NVL(CVQTADIS, 0)
            this.w_QTAREC = NVL(CVQTADIS, 0)
            this.w_QTANE1 = NVL(CVQTADIS, 0)
            this.w_QTAUM1 = NVL(CVQTADIS, 0)
            this.w_QTARE1 = NVL(CVQTADIS, 0)
            this.w_UNMIS1 = NVL(CVUNMIS1, "   ")
            this.w_UNMIS2 = NVL(CVUNMIS2, "   ")
            this.w_UNMIS3 = NVL(CVUNMIS3, "   ")
            this.w_OPERAT = NVL(CVOPERAT, " ")
            this.w_OPERA3 = NVL(CVOPERA3, " ")
            this.w_MOLTIP = NVL(CVMOLTIP, 0)
            this.w_MOLTI3 = NVL(CVMOLTI3, 0)
            * --- Se UM diversa, Calcola Qta alla 1^UM
            if this.w_UNIMIS<>this.w_UNMIS1
              * --- Derivato da CALMMLIS
              do case
                case this.w_UNIMIS=this.w_UNMIS3 AND this.w_MOLTI3<>0
                  * --- Selezionata 3^ Unita' di Misura
                  this.w_QTAUM1 = cp_ROUND((this.w_QTAUM1 * IIF(this.w_OPERA3="/", this.w_MOLTI3, 1)) / IIF(this.w_OPERA3="/", 1, this.w_MOLTI3), 6)
                case this.w_UNIMIS=this.w_UNMIS2 AND this.w_MOLTIP<>0
                  * --- Selezionata 2^ Unita' di Misura
                  this.w_QTAUM1 = cp_ROUND((this.w_QTAUM1 * IIF(this.w_OPERAT="/", this.w_MOLTIP, 1)) / IIF(this.w_OPERAT="/", 1, this.w_MOLTIP), 6)
              endcase
              this.w_QTANE1 = this.w_QTAUM1
            endif
            this.w_COEIMP = NVL(CVCOEIMP, SPACE(15))
            * --- Testa se il Componente della Distinta contiene almeno un Coefficiente di Impiego
            this.w_OKCOE = IIF(NOT EMPTY(this.w_COEIMP), .T., this.w_OKCOE)
            this.w_ARTCOM = NVL(CVARTCOM, SPACE(20))
            this.w_CODDIS = NVL(CVCODDIS, SPACE(20))
            this.w_TIPART = NVL(CVTIPART, SPACE(2))
            this.w_PROPRE = NVL(CVPROPRE, " ")
            this.w_TIPGES = NVL(CVTIPGES, " ")
            this.w_FLESPL = IIF(this.w_NUMLEV=this.oParentObject.w_MAXLEVEL," ",NVL(CVFLESPL, " "))
            if NOT EMPTY(this.w_CODDIS) AND this.oParentObject.w_MAXLEVEL=999
              * --- Trucco per esplodere sempre la distinta nei sottocomponenti
              this.w_FLESPL = "S"
            endif
            this.w_MAGPRE = NVL(CVMAGPRE, SPACE(5))
            this.w_FLPREV = NVL(CVFLPREV, "N")
          else
            LOOP
          endif
        endif
        if this.w_USCITA=.T.
          EXIT
        endif
        if NOT EMPTY(this.w_CODCOM) AND this.w_QTAMOV<>0 AND (this.w_TIPGES<>"C" OR this.w_VERI3="C")
          * --- Progressivo Riga
          this.w_NUMRIG = this.w_NUMRIG + 1
          this.w_NLEVEL = RIGHT("0000"+ALLTRIM(STR(this.w_NUMLEV)),4)
          * --- Imposta le Quantita' al Lordo
          this.w_QTAMOV = cp_ROUND(this.w_QTAMOV * (1+((this.w_PERSCA+this.w_PERSFR)/100)), 6)
          this.w_QTAUM1 = cp_ROUND(this.w_QTAUM1 * (1+((this.w_PERSCA+this.w_PERSFR)/100)), 6)
          * --- Componente Semilavorato o Distinta, Aggiorno considerando anche gli Scarti/Recuperi/Ricarichi del Semilavorato.
          if this.w_RECSCA<>0 AND this.w_PERSCA<>0
            this.w_RECSCAT = ABS(this.w_PERSCA - (this.w_PERSCA * (1+(this.w_RECSCA/100))))
          endif
          if this.w_RECSFR<>0 AND this.w_PERSFR<>0
            this.w_RECSFRT = ABS(this.w_PERSFR - (this.w_PERSFR * (1+(this.w_RECSFR/100))))
          endif
          * --- % Recupero Scarto e Sfrido (Attenzione al calcolo del Costo Totale: i valori sono normalmente Negativi)
          if this.w_RECSFR<>0 AND this.w_PERSFR<>0 or this.w_RECSCA<>0 AND this.w_PERSCA<>0
            this.w_PERREC = (this.w_RECSCA+this.w_RECSFR)/100
          else
            this.w_PERREC = 0
          endif
          * --- IIF( w_PERREC >0, cp_ROUND(w_QTARE1 * (1+((w_PERSCA+w_PERSFR)/100) - ((w_RECSCA+w_RECSFR)/100)), 6), w_QTAUM1)
          this.w_QTAREC = IIF( this.w_PERREC >0, this.w_QTAREC * (1+((this.w_PERSCA+this.w_PERSFR)/100) - ((this.w_RECSCAT+this.w_RECSFRT)/100)), this.w_QTAMOV)
          this.w_QTARE1 = IIF( this.w_PERREC >0, this.w_QTARE1 * (1+((this.w_PERSCA+this.w_PERSFR)/100) - ((this.w_RECSCAT+ this.w_RECSFRT)/100)), this.w_QTAUM1)
          if this.w_NUMLEV=1
            this.w_MESSERR = ah_Msgformat("Verificare componente: %1", ALLTRIM(this.w_CODCOM) )
          endif
          this.w_CONCAR = IIF(this.pTipOpe="R", NVL(ARCONCAR, "N"), "N")
          * --- Aggiorna il Temporaneo Esplosione
          INSERT INTO TES_PLOS ;
          (CODART, NUMRIG, DISPAD, ARTPAD, FLVARI, CPROWNUM, CPROWORD, CODCOM, NUMLEV, DESCOM, UNIMIS, QTANET, QTANE1, QTAMOV, QTAUM1, QTAREC, QTARE1, COEIMP, ;
          PERSCA, RECSCA, PERSFR, RECSFR, PERRIC, FLESPL, ARTCOM, CODDIS, TIPART, ARTVAR, FLSTAT, PROPRE, TIPGES, ;
          MAGPRE, FLPREV, RIFFAS, UNMIS0, MVDESSUP, CONCAR,CORLEA) ;
          VALUES (this.w_CODART, this.w_NUMRIG, this.w_DISPAD, this.w_ARTPAD, this.w_FLVARI, this.w_CPROWNUM, this.w_CPROWORD, this.w_CODCOM, this.w_NLEVEL, this.w_DESCOM, ;
          this.w_UNMIS1, this.w_QTANET, this.w_QTANE1, this.w_QTAMOV, this.w_QTAUM1,this.w_QTAREC, this.w_QTARE1, this.w_COEIMP, this.w_PERSCA, this.w_RECSCAT, this.w_PERSFR, this.w_RECSFRT, this.w_PERRIC, ;
          this.w_FLESPL, this.w_ARTCOM, this.w_CODDIS, this.w_TIPART, this.w_ARTVAR, " ", this.w_PROPRE, this.w_TIPGES, this.w_MAGPRE, this.w_FLPREV, this.w_RIFFAS, this.w_UNIMIS, this.w_MVDESSUP, this.w_CONCAR, this.w_CORLEA)
          if this.w_TIPART="PH" AND (UPPER(THIS.OPARENTOBJECT.CLASS) = "TGSCO_BOL" OR UPPER(THIS.OPARENTOBJECT.CLASS) = "TGSCO_BGS")
            if this.w_VERI1 $ "ST"
              * --- Verifica Loop
              INSERT INTO VERFAT (DISPAD, NUMLEV) VALUES (this.w_DISPAD, this.w_NUMLEV)
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if this.w_USCITA=.T.
                EXIT
              endif
            endif
            SELECT (NOMCUR)
            * --- Esplode Ulteriormente la Distinta Incrementando il Numero di Livello (Ricorsiva) 
            this.w_NUMLEV = this.w_NUMLEV + 1
            this.w_OLDPAD = this.w_DISPAD
            this.w_DISPAD = this.w_CODDIS
            this.w_ARTPAD = this.w_ARTCOM
            * --- ESEGUE L'ESPLOSIONE DEI FIGLI DEL FANTASMA
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Ri-Decrementa il Numero di Livello
            this.w_NUMLEV = this.w_NUMLEV - 1 
          endif
          if this.w_FLESPL="S" AND NOT EMPTY(this.w_CODDIS) AND this.w_NUMLEV<this.oParentObject.w_MAXLEVEL AND this.w_NUMLEV<99
            if this.w_VERI1 $ "ST"
              * --- Verifica Loop
              INSERT INTO VERFAT (DISPAD, NUMLEV) VALUES (this.w_DISPAD, this.w_NUMLEV)
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              if this.w_USCITA=.T.
                EXIT
              endif
            endif
            SELECT (NOMCUR)
            * --- Esplode Ulteriormente la Distinta Incrementando il Numero di Livello (Ricorsiva) 
            this.w_NUMLEV = this.w_NUMLEV + 1
            this.w_OLDPAD = this.w_DISPAD
            this.w_DISPAD = this.w_CODDIS
            this.w_ARTPAD = this.w_ARTCOM
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Ri-Decrementa il Numero di Livello
            this.w_NUMLEV = this.w_NUMLEV - 1 
          endif
        endif
        SELECT (NOMCUR)
        ENDSCAN 
      endif
      if USED(NOMCUR)
        SELECT (NOMCUR)
        USE
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica del Ciclo di Esplosione
    this.w_TESTLV = this.w_NUMLEV
    if USED("VERFAT")
      if RECCOUNT("VERFAT")>0
        * --- Risale lungo la catena dei "padri"
        SELECT VERFAT
        GO BOTTOM
        do while NOT BOF()
          if NUMLEV<this.w_TESTLV
            * --- Trovato padre
            if DISPAD=this.w_CODDIS
              * --- Se un "Padre" della stessa catena del componente da verificare ha la stessa distinta si interrompe 
              this.w_USCITA = .T.
              if this.w_VERI1="T"
                this.w_MESS = "Distinta incongruente; impossibile proseguire%0%1- Distinta: %2"
                * --- Modifica il Valore di w_VERIFICA per notificare l'errore al programma chiamante
                this.oParentObject.w_VERIFICA = "ZZZ"
                ah_ErrorMsg(this.w_MESS,,"", this.w_MESSERR, ALLTRIM(this.w_CODDIS) )
              else
                this.w_MESS = "Distinta incongruente; impossibile proseguire%0Eseguire verifica distinta: %1"
                ah_ErrorMsg(this.w_MESS,,"", this.w_CODDIS)
              endif
              EXIT
            endif
            this.w_TESTLV = NUMLEV
          endif
          SKIP -1
        enddo
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pTipOpe)
    this.pTipOpe=pTipOpe
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DISMBASE'
    this.cWorkTables[2]='PAR_DISB'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOpe"
endproc
