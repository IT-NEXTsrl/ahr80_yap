* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bli                                                        *
*              Controlla cancellazione listino                                 *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_62]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-07-14                                                      *
* Last revis.: 2006-03-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bli",oParentObject)
return(i_retval)

define class tgsar_bli as StdBatch
  * --- Local variables
  w_OK = .f.
  w_MESS = space(0)
  w_MESS1 = .f.
  w_MESS2 = .f.
  w_MESS3 = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Da GSAR_ALI: inibisce la cancellazione se il listino � presente nelle Causali documenti, nei documenti, nelle modalit� di vendita(pos), nei modelli di offerta
    this.w_MESS1 = .F.
    this.w_MESS2 = .F.
    this.w_MESS3 = .F.
    Vq_Exec ( "GSAR_ALI.Vqr", This, "LIST")
    if RECCOUNT("LIST")>0
      this.w_MESS1 = .T.
      this.w_OK = .T.
    endif
    if g_GPOS="S"
      Vq_Exec ( "Query\GSAR2ALI.Vqr", This, "LIST2")
      if RECCOUNT("LIST2")>0
        this.w_MESS2 = .T.
        this.w_OK = .T.
      endif
    endif
    if g_OFFE="S"
      Vq_Exec ( "Query\GSAR3ALI.Vqr", This, "LIST3")
      if RECCOUNT("LIST3")>0
        this.w_MESS3 = .T.
        this.w_OK = .T.
      endif
    endif
    if used("LIST")
      select LIST
      use
    endif
    if used("LIST2")
      select LIST2
      use
    endif
    if used("LIST3")
      select LIST3
      use
    endif
    if this.w_OK=.T.
      do case
        case this.w_MESS1 AND this.w_MESS2 AND this.w_MESS3
          this.w_MESS = "Il listino che si intende cancellare � presente:%0-nelle causali documenti, nei documenti o nei movimenti di magazzino%0-nelle modalit� di vendita%0-nei modelli di offerta%0Impossibile cancellare"
        case this.w_MESS1 AND this.w_MESS2 
          this.w_MESS = "Il listino che si intende cancellare � presente:%0-nelle causali documenti, nei documenti o nei movimenti di magazzino%0-nelle modalit� di vendita%0Impossibile cancellare"
        case this.w_MESS1 AND this.w_MESS3
          this.w_MESS = "Il listino che si intende cancellare � presente:%0-nelle causali documenti, nei documenti o nei movimenti di magazzino%0-nei modelli di offerta%0Impossibile cancellare"
        case this.w_MESS2 AND this.w_MESS3
          this.w_MESS = "Il listino che si intende cancellare � presente:%0-nelle modalit� di vendita%0-nei modelli di offerta%0Impossibile cancellare"
        case this.w_MESS1
          this.w_MESS = "Il listino che si intende cancellare � presente:%0-nelle causali documenti, nei documenti o nei movimenti di magazzino%0Impossibile cancellare"
        case this.w_MESS2
          this.w_MESS = "Il listino che si intende cancellare � presente:%0-nelle modalit� di vendita%0Impossibile cancellare"
        case this.w_MESS3
          this.w_MESS = "Il listino che si intende cancellare � presente:%0-nei modelli di offerta%0Impossibile cancellare"
      endcase
      ah_ErrorMsg(this.w_MESS,,"")
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=ah_MsgFormat("Eliminazione annullata")
      i_retcode = 'stop'
      return
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
