* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bkc                                                        *
*              Incassi, controlli finali                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_71]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-04-12                                                      *
* Last revis.: 2012-07-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,Pparam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bkc",oParentObject,m.Pparam)
return(i_retval)

define class tgsve_bkc as StdBatch
  * --- Local variables
  Pparam = space(1)
  w_OK = .f.
  w_MESS = space(10)
  w_CFUNC = space(10)
  w_PADRE = .NULL.
  w_RIGA = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controlli Finali sull'archivio Incassi Corrispettivi (da GSVE_MIC)
    * --- Viene eseguito in A.M. Replace e dall Evento Delete End
    this.w_PADRE = this.oParentObject
    this.w_OK = .T.
    this.w_MESS = "Transazione abbandonata"
    this.w_CFUNC = this.w_PADRE.cFunction
    * --- Controlli Preliminari
    if this.Pparam="C"
      this.w_RIGA = this.w_PADRE.search("NVL(t_IN_FLCON, SPACE(1)) ='S' ")
    else
      this.w_RIGA = this.w_PADRE.search("NVL(t_IN_FLCON, SPACE(1)) ='S' And (Deleted() OR i_SRV='U' ) ")
    endif
    if this.w_RIGA<>-1
      if this.w_CFUNC="Query"
        this.w_MESS = "Incasso contabilizzato; impossibile eliminare"
      else
        this.w_MESS = "Incasso contabilizzato; impossibile variare"
      endif
      this.w_OK = .F.
    endif
    if this.w_OK = .F.
      this.w_MESS = ah_Msgformat(this.w_MESS)
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
      i_retcode = 'stop'
      return
    endif
  endproc


  proc Init(oParentObject,Pparam)
    this.Pparam=Pparam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="Pparam"
endproc
