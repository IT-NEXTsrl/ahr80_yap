* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bmb                                                        *
*              Modifica banca appoggio/ns banca                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_24]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-01-12                                                      *
* Last revis.: 2014-04-07                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pPARAM
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bmb",oParentObject,m.pPARAM)
return(i_retval)

define class tgsve_bmb as StdBatch
  * --- Local variables
  pPARAM = space(1)
  w_PADRE = .NULL.
  w_TEST = .f.
  w_NURATA = 0
  w_SERIAL = space(10)
  w_OLDSERIAL = space(10)
  w_MVCODBAN = space(10)
  w_MVCODBA2 = space(15)
  w_MVNUMCOR = space(25)
  w_TIPPAG = space(2)
  w_MESS = space(200)
  w_MVCLADOC = space(2)
  w_MVTIPCON = space(1)
  w_POS = 0
  w_SER = space(10)
  w_MESS = space(100)
  w_CONTA = 0
  w_numBANAPP = 0
  w_numBANNOS = 0
  w_numCODCOR = 0
  * --- WorkFile variables
  DOC_RATE_idx=0
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modifica Banca d'Appoggio/Ns Banca.
    this.w_CONTA = 0
    this.w_PADRE = this.oparentobject
    this.w_TEST = .F.
    this.w_OLDSERIAL = "##########"
    * --- Controllo sulla presenza di campi sbiancati. Se l utente non specifica uno dei campi da aggiornare (C/C , ns banca, banca appoggio)
    *     tali campi verranno sbiancati e quindi si richiede all' utente se vuole continuare
    do case
      case EMPTY( this.oParentObject.w_TIPOELENCO)
        NC = this.w_PADRE.w_ZOOMSCAD.cCursor
      case this.oParentObject.w_TIPOELENCO= "S"
        NC = this.w_PADRE.w_ZOOMDOCU.cCursor
    endcase
    do case
      case this.pPARAM="A"
        * --- Controlla che i tra i campi selezionati ve ne siano alcuni non vuoti
        do case
          case EMPTY( this.oParentObject.w_TIPOELENCO)
            SELECT (NC) 
 GO TOP 
 COUNT FOR XCHK=1 AND NOT EMPTY ( NVL (RSBANAPP,"")) TO this.w_numBANAPP 
 GO TOP 
 COUNT FOR XCHK=1 AND NOT EMPTY (NVL (RSBANNOS,"")) TO this.w_numBANNOS 
 GO TOP 
 COUNT FOR XCHK=1 AND NOT EMPTY (NVL (RSCONCOR,"")) TO w_numCONCOR
          case this.oParentObject.w_TIPOELENCO= "S"
            SELECT (NC) 
 GO TOP 
 COUNT FOR XCHK=1 AND NOT EMPTY ( NVL (MVCODBAN,"")) TO this.w_numBANAPP 
 GO TOP 
 COUNT FOR XCHK=1 AND NOT EMPTY (NVL (MVCODBA2,"")) TO this.w_numBANNOS 
 GO TOP 
 COUNT FOR XCHK=1 AND NOT EMPTY (NVL (MVNUMCOR,"")) TO w_numCONCOR
        endcase
        if ( EMPTY(this.oParentObject.w_BANAPP) AND this.w_numBANAPP>0 ) OR (EMPTY(this.oParentObject.w_BANNOS) AND this.w_numBANNOS>0 )OR ( EMPTY(this.oParentObject.w_CONCOR) AND w_numCONCOR>0 )
          if ( EMPTY(this.oParentObject.w_BANAPP) AND this.w_numBANAPP>0 ) 
            this.w_CONTA = this.w_CONTA + 1
          endif
          if (EMPTY(this.oParentObject.w_BANNOS) AND this.w_numBANNOS>0 )
            this.w_CONTA = this.w_CONTA + 1
          endif
          if ( EMPTY(this.oParentObject.w_CONCOR) AND w_numCONCOR>0 )
            this.w_CONTA = this.w_CONTA + 1
          endif
          this.w_MESS = IIF ( EMPTY(this.oParentObject.w_BANAPP) AND this.w_numBANAPP>0 ,ah_msgformat("banca di appoggio"),"")
          * --- Formatta il messaggio di avviso
          this.w_MESS = this.w_MESS +IIF ( ( EMPTY(this.oParentObject.w_BANAPP) AND this.w_numBANAPP>0 ) AND ( (EMPTY(this.oParentObject.w_BANNOS) AND this.w_numBANNOS>0 ) OR ( EMPTY(this.oParentObject.w_CONCOR) AND w_numCONCOR>0 )),", ","")
          this.w_MESS = this.w_MESS +IIF (( EMPTY(this.oParentObject.w_CONCOR) AND w_numCONCOR>0 ) ,ah_msgformat("conto corrente"),"")
          this.w_MESS = this.w_MESS +IIF ( (EMPTY(this.oParentObject.w_BANNOS) AND this.w_numBANNOS>0 ) AND ( EMPTY(this.oParentObject.w_CONCOR) AND w_numCONCOR>0 ),", ","")
          this.w_MESS = this.w_MESS +IIF (EMPTY(this.oParentObject.w_BANNOS) AND this.w_numBANNOS>0 ,ah_msgformat("nostra banca"),"")
          if NOT AH_YESNO (iif(this.w_CONTA>1,"Attenzione, i campi %1 verranno sbiancati. %0Si vuole proseguire?", "Attenzione, il campo %1 verr� sbiancato. %0Si vuole proseguire?"),"",this.w_MESS)
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Try
        local bErr_035E9D68
        bErr_035E9D68=bTrsErr
        this.Try_035E9D68()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          if this.w_MESS="ERRTRS"
            ah_ErrorMsg("Errore durante aggiornamento; operazione abbandonata")
          else
            ah_ErrorMsg("Pagamento di tipo bonifico: inserire il conto corrente")
          endif
        endif
        bTrsErr=bTrsErr or bErr_035E9D68
        * --- End
      case this.pPARAM="S"
        * --- Seleziona/Deseleziona Tutto
        if this.oParentObject.w_SELEZ="S"
          UPDATE &NC SET XCHK = 1
          this.oParentObject.w_FLSELE = 1
        else
          UPDATE &NC SET XCHK = 0
          this.oParentObject.w_FLSELE = 0
        endif
        Select ( this.w_PADRE.w_ZOOMSCAD.cCursor ) 
 GO TOP
      case this.pPARAM="C"
        if upper(g_APPLICATION) = "ADHOC REVOLUTION"
          i_retcode = 'stop'
          return
        endif
        * --- Seleziona tutte le rate del documento appena selezionato
        SELECT (NC)
        this.w_POS = RECNO ( )
        GO TOP 
 UPDATE &NC SET XCHK = 1 WHERE MVSERIAL=this.oParentObject.w_SERIALE
        Select ( NC)
        if this.w_POS>0 AND this.w_POS<=reccount ()
          GO this.w_POS
        endif
      case this.pPARAM="D"
        if upper(g_APPLICATION) = "ADHOC REVOLUTION"
          i_retcode = 'stop'
          return
        endif
        * --- Deseleziona tutte le rate del documento appena selezionato
        SELECT (NC)
        this.w_POS = RECNO ( )
        GO TOP 
 UPDATE &NC SET XCHK = 0 WHERE MVSERIAL=this.oParentObject.w_SERIALE
        Select ( NC)
        if this.w_POS>0 AND this.w_POS<=reccount ()
          GO this.w_POS
        endif
      case this.pPARAM="M"
        if upper(g_APPLICATION) = "ADHOC REVOLUTION"
          i_retcode = 'stop'
          return
        endif
        * --- Per le selezioni effettuate da menu bisogna verifiacare che siano selezionate tutte le righe di un documento
        SELECT (NC)
        GO TOP 
 Select ( NC) 
 GO TOP 
 
        this.w_POS = 0
        SCAN
        this.w_POS = this.w_POS + 1
        this.w_SER = MVSERIAL
        if XCHK=1
          UPDATE &NC SET XCHK = 1 WHERE MVSERIAL=this.w_SER
        endif
        GO this.w_POS
        ENDSCAN
        if this.w_POS>0 AND this.w_POS<=reccount ()
          GO this.w_POS
        endif
    endcase
  endproc
  proc Try_035E9D68()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
     
 SELECT (NC) 
 GO TOP 
 SCAN FOR XCHK<>0
    do case
      case EMPTY( this.oParentObject.w_TIPOELENCO)
        this.w_SERIAL = RSSERIAL
        this.w_NURATA = RSNUMRAT
        this.w_MVCODBAN = NVL(RSBANAPP, SPACE(10))
        this.w_MVCODBA2 = NVL(RSBANNOS, SPACE(15))
        this.w_MVNUMCOR = NVL(RSCONCOR, SPACE(25))
        this.w_TIPPAG = NVL(TIPPAG, SPACE(2))
      case this.oParentObject.w_TIPOELENCO= "S"
        this.w_SERIAL = MVSERIAL
        this.w_MVCODBAN = NVL(MVCODBAN, SPACE(10))
        this.w_MVCODBA2 = NVL(MVCODBA2, SPACE(15))
        this.w_MVNUMCOR = NVL(MVNUMCOR, SPACE(25))
    endcase
    this.w_MVCLADOC = NVL(MVCLADOC, SPACE(2))
    this.w_MVTIPCON = NVL(MVTIPCON," ")
    this.w_TEST = .T.
    this.w_MESS = "ERRTRS"
    if EMPTY( this.oParentObject.w_TIPOELENCO )
      if (this.w_TIPPAG="BO" AND EMPTY(this.oParentObject.w_CONCOR)) AND ((this.w_MVTIPCON="C" AND this.w_MVCLADOC="NC") OR (this.w_MVTIPCON="F" AND this.w_MVCLADOC<>"NC")) AND upper(g_APPLICATION) = "ADHOC REVOLUTION"
        this.w_MESS = "ERRPGM"
        * --- Raise
        i_Error="Pagamento di tipo bonifico: inserire il conto corrente"
        return
      else
        if upper(g_APPLICATION) = "ADHOC REVOLUTION"
          * --- Write into DOC_RATE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_RATE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_RATE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"RSBANAPP ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_BANAPP),'DOC_RATE','RSBANAPP');
            +",RSBANNOS ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_BANNOS),'DOC_RATE','RSBANNOS');
            +",RSCONCOR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CONCOR),'DOC_RATE','RSCONCOR');
                +i_ccchkf ;
            +" where ";
                +"RSSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                +" and RSNUMRAT = "+cp_ToStrODBC(this.w_NURATA);
                   )
          else
            update (i_cTable) set;
                RSBANAPP = this.oParentObject.w_BANAPP;
                ,RSBANNOS = this.oParentObject.w_BANNOS;
                ,RSCONCOR = this.oParentObject.w_CONCOR;
                &i_ccchkf. ;
             where;
                RSSERIAL = this.w_SERIAL;
                and RSNUMRAT = this.w_NURATA;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Write into DOC_RATE
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_RATE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_RATE_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_RATE_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"RSCONCOR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CONCOR),'DOC_RATE','RSCONCOR');
                +i_ccchkf ;
            +" where ";
                +"RSSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                +" and RSNUMRAT = "+cp_ToStrODBC(this.w_NURATA);
                   )
          else
            update (i_cTable) set;
                RSCONCOR = this.oParentObject.w_CONCOR;
                &i_ccchkf. ;
             where;
                RSSERIAL = this.w_SERIAL;
                and RSNUMRAT = this.w_NURATA;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      endif
    endif
    if this.w_SERIAL<>this.w_OLDSERIAL AND this.oParentObject.w_TESTATA="S"
      if upper(g_APPLICATION) = "ADHOC REVOLUTION"
        * --- Write into DOC_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVCODBAN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_BANAPP),'DOC_MAST','MVCODBAN');
          +",MVNUMCOR ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_CONCOR),'DOC_MAST','MVNUMCOR');
          +",MVCODBA2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_BANNOS),'DOC_MAST','MVCODBA2');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          update (i_cTable) set;
              MVCODBAN = this.oParentObject.w_BANAPP;
              ,MVNUMCOR = this.oParentObject.w_CONCOR;
              ,MVCODBA2 = this.oParentObject.w_BANNOS;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_SERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      else
        * --- Write into DOC_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVCODBAN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_BANAPP),'DOC_MAST','MVCODBAN');
          +",MVCODBA2 ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_BANNOS),'DOC_MAST','MVCODBA2');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                 )
        else
          update (i_cTable) set;
              MVCODBAN = this.oParentObject.w_BANAPP;
              ,MVCODBA2 = this.oParentObject.w_BANNOS;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.w_SERIAL;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
      endif
      this.w_OLDSERIAL = this.w_SERIAL
    endif
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    if NOT this.w_TEST
      ah_ErrorMsg("Selezionare almeno una rata")
    else
      ah_ErrorMsg("Aggiornamento eseguito con successo")
    endif
    this.oParentObject.w_BANAPP = SPACE(10)
    this.oParentObject.w_BANNOS = SPACE(15)
    this.oParentObject.w_CONCOR = SPACE(25)
    this.oParentObject.w_DESNOS = SPACE(35)
    this.oParentObject.w_DESBAN = SPACE(35)
    this.w_PADRE.NotifyEvent("Ricerca")     
    return


  proc Init(oParentObject,pPARAM)
    this.pPARAM=pPARAM
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='DOC_RATE'
    this.cWorkTables[2]='DOC_MAST'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pPARAM"
endproc
