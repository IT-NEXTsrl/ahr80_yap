* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bai                                                        *
*              Inventario analisi venduto                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_14]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-28                                                      *
* Last revis.: 2000-02-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bai",oParentObject)
return(i_retval)

define class tgsma_bai as StdBatch
  * --- Local variables
  w_DATINV = ctod("  /  /  ")
  w_MESS = space(200)
  * --- WorkFile variables
  INVENTAR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- VALORIZZAZIONE INVENTARIO INIZIALE ED ESERCIZIO
    * --- Viene invocato dalla maschera GSMA_SAV per valorizzare i filtri
    * --- di Inventario Iniziale ed Esercizio sui quali � inutile effettuare lo zoom,
    * --- in quanto tali valori dipendono dall'inventario finale e dall'esercizio finale.
    * --- Variabili filtro della maschera,
    * --- Si assegnano i valori all'inventario iniziale e all'esercizio.
    * --- Read from INVENTAR
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.INVENTAR_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2],.t.,this.INVENTAR_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "INNUMPRE,INESEPRE"+;
        " from "+i_cTable+" INVENTAR where ";
            +"INNUMINV = "+cp_ToStrODBC(this.oParentObject.w_INVFIN);
            +" and INCODESE = "+cp_ToStrODBC(this.oParentObject.w_ESEFIN);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        INNUMPRE,INESEPRE;
        from (i_cTable) where;
            INNUMINV = this.oParentObject.w_INVFIN;
            and INCODESE = this.oParentObject.w_ESEFIN;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.oParentObject.w_INVINI = NVL(cp_ToDate(_read_.INNUMPRE),cp_NullValue(_read_.INNUMPRE))
      this.oParentObject.w_ESEINI = NVL(cp_ToDate(_read_.INESEPRE),cp_NullValue(_read_.INESEPRE))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Per effettuare la stampa, all'inventario finale deve essere associato un
    * --- inventario iniziale. In caso contrario l'inventario finale non � valido,
    * --- per cui viene azzerato, impedendo cos� la stampa.
    if empty (nvl(this.oParentObject.w_INVINI,""))
      ah_ErrorMsg("L'inventario non ha un inventario iniziale associato")
      this.oParentObject.w_INVFIN = ""
    else
      * --- Read from INVENTAR
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.INVENTAR_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2],.t.,this.INVENTAR_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "INDATINV"+;
          " from "+i_cTable+" INVENTAR where ";
              +"INNUMINV = "+cp_ToStrODBC(this.oParentObject.w_INVINI);
              +" and INCODESE = "+cp_ToStrODBC(this.oParentObject.w_ESEINI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          INDATINV;
          from (i_cTable) where;
              INNUMINV = this.oParentObject.w_INVINI;
              and INCODESE = this.oParentObject.w_ESEINI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DATINV = NVL(cp_ToDate(_read_.INDATINV),cp_NullValue(_read_.INDATINV))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if NOT((this.oParentObject.w_DATESI-1)=this.w_DATINV OR this.oParentObject.w_ESEINI=this.oParentObject.w_ESEFIN)
        this.w_MESS = "L'inventario iniziale deve essere elaborato nollo stesso esercizio di quello finale%0oppure comprendere tutti i movimenti dell'esercizio precedente"
        ah_ErrorMsg(this.w_MESS)
        this.oParentObject.w_INVFIN = ""
        this.oParentObject.w_INVINI = ""
      endif
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='INVENTAR'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
