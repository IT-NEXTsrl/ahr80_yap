* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bvi                                                        *
*              Versamenti trimestrali IRPEF                                    *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-01-10                                                      *
* Last revis.: 2005-12-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pParame
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bvi",oParentObject,m.pParame)
return(i_retval)

define class tgscg_bvi as StdBatch
  * --- Local variables
  pParame = space(1)
  w_MESS = space(100)
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Batch lanciato dall'anagrafica Gscg_Amf per impostare nei versamenti
    * --- I.R.PE.F. ,nel caso di import ritenute.il valore corretto.
    * --- Se il parametro vale 'I' sono in caricamento oppure in variazione mentre se vale 'C' sono in cancellazione.
    do case
      case this.pParame="I"
        * --- Verifico se sono stati importati versamenti di tipo Previdenziale.
        if this.oParentObject.w_Impprev=.T.
          * --- Devo verificare che non esista una causale contributo che non sia
          * --- assocciata a nessuna causale.
          * --- Questo caso si pu� verificare solo se ho importato dalle distinte di versamento.
          if Not Empty(this.oParentObject.w_Mfccoae1) And Empty(this.oParentObject.w_Mfsdent1) And this.oParentObject.w_MFCDENTE<>"0002"
            this.w_MESS = AH_MSGFORMAT ("Nessun codice sede associato alla causale contributo: %1",alltrim(this.oParentObject.w_Mfccoae1) )
            this.oParentObject.oPgFrm.ActivePage = 6
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_Mess
            i_retcode = 'stop'
            return
          endif
          if Not Empty(this.oParentObject.w_Mfccoae2) And Empty(this.oParentObject.w_Mfsdent2) And this.oParentObject.w_MFCDENTE<>"0002"
            this.w_MESS = AH_MSGFORMAT ("Nessun codice sede associato alla causale contributo%0%1%0",alltrim(this.oParentObject.w_Mfccoae2) )
            i_retcode = 'stop'
            return
          endif
        endif
        * --- Controllo se ho importato ,dal bottone ritenute, almeno una riga e se il modulo ritenute � settato.
        if "RITE" $ UPPER(i_cModules) And (this.oParentObject.w_Importa=.T. Or this.oParentObject.w_Impinps=.T. Or this.oParentObject.w_Impprev=.T.)
          do GSRI_BV2 with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pParame="C"
        if "RITE" $ UPPER(i_cModules)
          do GSRI_BV3 with this
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
    endcase
  endproc


  proc Init(oParentObject,pParame)
    this.pParame=pParame
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pParame"
endproc
