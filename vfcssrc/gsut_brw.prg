* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_brw                                                        *
*              Wizard report                                                   *
*                                                                              *
*      Author: Zucchetti S.p.A.                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-11-02                                                      *
* Last revis.: 2012-07-04                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_brw",oParentObject,m.pOPER)
return(i_retval)

define class tgsut_brw as StdBatch
  * --- Local variables
  pOPER = space(10)
  w_PROC_OK = .f.
  w_PADRE = .NULL.
  w_CTRL = .NULL.
  w_FIGLIO = .NULL.
  w_TEMPDEST = space(254)
  w_FILEFRT = space(254)
  w_REPCURSOR = space(10)
  w_ACTCURS = space(10)
  w_HEAD_VAL = space(60)
  w_HEAD_OBJ = space(20)
  w_DTL_VAL = space(240)
  w_DTL_OBJ = space(20)
  w_FTR_VAL = space(60)
  w_FTR_OBJ = space(20)
  w_VARCOUNTER = 0
  w_NOMECAMPO = space(60)
  w_TIPOCAMPO = space(0)
  w_LISTACAMPI_TIP = space(0)
  w_LISTACAMPI = space(0)
  w_MASK = .NULL.
  w_FILLDETT = 0
  w_FILLPARAM = 0
  w_FILLPARADESC = 0
  w_FILLHEADESC = 0
  w_CURREPDET = space(10)
  w_CURZOOM = space(10)
  w_TIMEOUT = 0
  w_FASE = space(1)
  w_INSNEWPA = .f.
  w_CICLO = 0
  w_GRUFIELD = space(50)
  w_VHEIGHT_T = 0
  w_VHEIGHT_D = 0
  w_VHEIGHT_P = 0
  w_NUMGRUPPI = 0
  w_LISGRPFLD = space(254)
  w_LISGRPFLDAPP = space(254)
  w_POSGRUP = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    PUBLIC L_LOGO
    L_LOGO = "bmp\logostampe.bmp"
    this.w_LISTACAMPI_TIP = ""
    this.w_LISTACAMPI = ""
    this.w_PROC_OK = .T.
    this.w_PADRE = This.oparentobject
    do case
      case this.pOPER=="NEXT"
        if empty(this.oParentObject.w_MODELLO)
          Ah_ErrorMsg("Selezionare un modello per poter procedere", 48,"")
          i_retcode = 'stop'
          return
        endif
        * --- Repor wizard attivato
        if ! USED("VARLIST")
          * --- Creo la struttura del cusore delle eventuali variabili create per gestire gli operatori aritmetici definiti sui campi di dettaglio
          CREATE CURSOR VARLIST (VARNAME C(60), VARDESC C(80), VARTYPE C(1), VARUSED C(1))
        endif
        this.oParentObject.w_OLDMODEL = this.oParentObject.w_MODELLO
        this.oParentObject.w_CUR_STEP = this.oParentObject.w_CUR_STEP + 1
        if this.oParentObject.w_CUR_STEP = 6 AND this.oParentObject.w_NOPARPAG
          this.oParentObject.w_CUR_STEP = this.oParentObject.w_CUR_STEP + 1
        endif
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOPER=="PREV"
        this.oParentObject.w_CUR_STEP = this.oParentObject.w_CUR_STEP - 1
        if this.oParentObject.w_CUR_STEP>1
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      case this.pOPER=="AGGIORNA"
        this.Page_2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        if this.oParentObject.w_CUR_STEP>1
          * --- A seguito di aggiornamento evidenzio il campo corrente
          this.oParentObject.NotifyEvent("CambioRiga")
        endif
      case this.pOPER=="SAVE"
        * --- Apro il report per eliminare campi ed edichette non utilizzati
        *     Eseguo questa operazione solo al salvataggio poich� durante il wizard
        *     la necessit� � quella di poter vedere dove le invormazioni 
        *     verranno posizionate all'esecuzione del report
        USE IN SELECT(this.oParentObject.w_TABREPOR)
        use (this.oParentObject.w_PATHREP) IN 0 ALIAS (this.oParentObject.w_TABREPOR) EXCLUSIVE
        DELETE FROM (this.oParentObject.w_TABREPOR) WHERE OBJTYPE IN (5,8,18) AND (EXPR LIKE "ZZ_HDR%" OR EXPR LIKE '"ZZ_LBL_HDR%' OR EXPR LIKE "ZZ_DTL%" OR EXPR LIKE '"ZZ_LBL_DTL%' OR EXPR LIKE "ZZ_FTR%" OR EXPR LIKE '"ZZ_LBL_FTR%' OR EXPR LIKE "ZZ_PAR%" OR EXPR LIKE '"ZZ_LBL_PAR%')
        * --- Aggiorno i campi contenenti il parametro
        this.w_FIGLIO = this.w_PADRE.GSUT_MMP
        this.w_FIGLIO.MarkPos()     
        this.w_FIGLIO.FirstRow()     
        do while Not this.w_FIGLIO.Eof_Trs()
          this.w_FIGLIO.SetRow()     
          if this.w_FIGLIO.FullRow() AND !EMPTY(NVL(this.w_FIGLIO.w_WRCURFLD, " "))
            SELECT(this.oParentObject.w_TABREPOR)
            GO TOP
            this.w_NOMECAMPO = alltrim(this.w_FIGLIO.w_WRCURFLD)
            UPDATE (this.oParentObject.w_TABREPOR) SET EXPR=STRTRAN(EXPR, this.w_NOMECAMPO , 'cp_getqueryparam("' + lower( this.w_NOMECAMPO ) + '")' ) WHERE OBJTYPE = 8 AND lower(ALLTRIM(EXPR))==lower(this.w_NOMECAMPO)
          endif
          this.w_FIGLIO.NextRow()     
        enddo
        this.w_FIGLIO.RePos()     
        SELECT(this.oParentObject.w_TABREPOR)
        GO TOP
        * --- Forzo l'eliminazione dei records cancellati
        PACK
        USE IN SELECT(this.oParentObject.w_TABREPOR)
        ERRORE = .F.
        olderror = ON("ERROR")
        ON ERROR ERRORE = .T.
        * --- .FRX
        copy file (this.oParentObject.w_PATHREP) to (this.oParentObject.w_NEWREP)
        * --- .FRT
        copy file (FORCEEXT(this.oParentObject.w_PATHREP, "FRT") ) to (FORCEEXT(this.oParentObject.w_NEWREP, "FRT") )
        On ERROR &olderror
        if ERRORE
          this.oParentObject.oParentObject.w_PATHREPORT = " "
          Ah_ErrorMsg("Errore durante la creazione del nuovo report nella cartella  %1",48,"",justpath(this.oParentObject.w_NEWREP))
        else
          this.oParentObject.oParentObject.w_PATHREPORT = this.oParentObject.w_NEWREP
          this.oParentObject.w_PATHREPORT = this.oParentObject.w_NEWREP
          Ah_ErrorMsg("Report  creato correttamente nella cartella  %1",64,"",justpath(this.oParentObject.w_NEWREP))
          this.Page_7(.T.)
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        i_retcode = 'stop'
        return
      case this.pOPER == "FLDGROUP"
        * --- Raggruppamenti - Devo popolare il cursore associato allo zoom con i campi definiti nel dettaglio del report
        this.w_CURREPDET = this.w_PADRE.GSUT_MMD.cTrsname
        this.w_CURZOOM = this.w_PADRE.w_GRUPFLDS.cCursor
        SELECT FIELDNAME, FIELDORD FROM (this.w_CURZOOM) WHERE XCHK=1 INTO CURSOR "OLDCKGRP"
        Select (this.w_CURZOOM)
        Zap
        * --- Caricamento cursore dello zoom con transitorio del dettaglio report
        SELECT * FROM (this.w_CURZOOM) INTO CURSOR "TMPCURZOOM" WHERE 1=0 READWRITE
        Insert into "TMPCURZOOM" (FIELDNAME , FIELDDESC, FIELDORD, XCHK) select T_WRCURFLD, T_WRFLDESC, T_CPROWORD, 0 from (this.w_CURREPDET) where T_WRTYPEOB = 8 and not empty(T_WRCURFLD) order by T_CPROWORD
        UPDATE "TMPCURZOOM" SET XCHK=1, FIELDORD=OLDCKGRP.FIELDORD FROM "OLDCKGRP" WHERE TMPCURZOOM.FIELDNAME=OLDCKGRP.FIELDNAME
        USE IN SELECT("OLDCKGRP")
        INSERT INTO (this.w_CURZOOM) (FIELDNAME , FIELDDESC, FIELDORD, XCHK) SELECT FIELDNAME , FIELDDESC, FIELDORD, XCHK FROM "TMPCURZOOM" ORDER BY FIELDORD
        USE IN SELECT("TMPCURZOOM")
        SELECT (this.w_CURZOOM)
        GO TOP
        this.w_PADRE.w_GRUPFLDS.grd.column3.ReadOnly = .F.
        this.w_PADRE.w_GRUPFLDS.grd.COLUMN3.BackColor = RGB(250,250,0)
        this.w_PADRE.w_GRUPFLDS.Refresh()     
        this.w_PADRE.opgfrm.page4.Refresh()     
        i_retcode = 'stop'
        return
      case this.pOPER=="EXIT"
        if !ah_YesNo("Si desidera uscire dalla configurazione guidata?")
          i_retcode = 'stop'
          return
        endif
      case this.pOPER == "FIELDZOOM"
        this.w_ACTCURS = this.w_PADRE.w_FLDLIST.cCursor
        Select (this.w_ACTCURS) 
 Zap
        if EMPTY(this.oParentObject.w_CURCAMPI) AND vartype(this.oParentObject)="O" AND vartype(this.oParentObject.oParentObject.oParentObject)="O"
          * --- Il metodo statico di acquisizione del valore associato alla variabile 'w_PCURCAMPI' ' � reso necessario dal metodo impostato per la dichiarazione/definzione degli oggetti figli
          this.oParentObject.w_CURCAMPI = this.oParentobject.oparentObject.oParentObject.w_CURCAMPI
        endif
        if EMPTY(this.oParentObject.w_CURPARAM) AND vartype(this.oParentObject)="O" AND vartype(this.oParentObject.oParentObject.oParentObject)="O"
          * --- Il metodo statico di acquisizione del valore associato alla variabile 'w_PCURPARAM' ' � reso necessario dal metodo impostato per la dichiarazione/definzione degli oggetti figli
          this.oParentObject.w_CURPARAM = this.oParentobject.oparentObject.oParentObject.w_CURPARAM
        endif
        insert into (this.w_ACTCURS) Select * from (this.oParentObject.w_CURCAMPI) WHERE FIELDUSED <> "S"
        insert into (this.w_ACTCURS) Select * from (this.oParentObject.w_CURPARAM) WHERE FIELDUSED <> "S"
        if lower(this.w_PADRE.oParentObject.class) == "tcgsut_mmf"
          * --- Sono nel piede aggiungo le eventuali variabili di 'calcolo'
          insert into (this.w_ACTCURS) Select * from VARLIST WHERE VARUSED # "S"
        endif
        * --- Cursore campi gi� utilizzati
        this.w_ACTCURS = this.w_PADRE.w_FLDLISTUSED.cCursor
        Select (this.w_ACTCURS) 
 Zap
        insert into (this.w_ACTCURS) Select * from (this.oParentObject.w_CURCAMPI) WHERE FIELDUSED = "S"
        insert into (this.w_ACTCURS) Select * from (this.oParentObject.w_CURPARAM) WHERE FIELDUSED = "S"
        if lower(this.w_PADRE.oParentObject.class) == "tcgsut_mmf"
          * --- Sono nel piede aggiungo le eventuali variabili di 'calcolo'
          insert into (this.w_ACTCURS) Select * from VARLIST WHERE VARUSED = "S"
        endif
        this.w_PADRE.Refresh()     
        i_retcode = 'stop'
        return
      case this.pOPER == "AGGLIST"
        if !EMPTY(NVL(this.w_PADRE.w_WRCURFLD," "))
          if EMPTY(this.oParentObject.w_CURCAMPI) AND vartype(this.oParentObject)="O" AND vartype(this.oParentObject.oParentObject.oParentObject)="O"
            this.oParentObject.w_CURCAMPI = this.oParentobject.oparentObject.oParentObject.w_CURCAMPI
          endif
          UPDATE (this.oParentObject.w_CURCAMPI) SET FIELDUSED="S" WHERE LOWER(FIELDNAME) == LOWER(this.w_PADRE.w_WRCURFLD)
          UPDATE VARLIST SET VARUSED="S" WHERE LOWER(VARNAME) == LOWER(this.w_PADRE.w_WRCURFLD)
        endif
        i_retcode = 'stop'
        return
      case this.pOPER == "ZOOM"
        if VARTYPE(this.oParentObject.w_OBJXFRX) = "O"
          this.w_MASK = GSUT_KZR()
          this.w_MASK.w_XFRXZOOM.PREVIEWXFF(this.oParentObject.w_OBJXFRX.GetXfDocument())     
          this.w_MASK.w_XFRXZOOM.Zoom(-1)     
          this.w_MASK = .null.
        endif
      case this.pOPER == "SELELE"
        do case
          case this.oParentObject.w_CUR_STEP=2
            this.w_FIGLIO = this.w_PADRE.GSUT_MMR
            this.w_CTRL = this.w_PADRE.w_XFRXHEAD
          case this.oParentObject.w_CUR_STEP=3
            this.w_FIGLIO = this.w_PADRE.GSUT_MMD
            this.w_CTRL = this.w_PADRE.w_XFRXBODY
          case this.oParentObject.w_CUR_STEP=5
            this.w_FIGLIO = this.w_PADRE.GSUT_MMF
            this.w_CTRL = this.w_PADRE.w_XFRXFOOT
          case this.oParentObject.w_CUR_STEP=6
            this.w_FIGLIO = this.w_PADRE.GSUT_MMP
            this.w_CTRL = this.w_PADRE.w_XFRXPARA
          otherwise
            i_retcode = 'stop'
            return
        endcase
        if VARTYPE(this.w_FIGLIO.w_WRCURFLD) <> "U" AND !EMPTY(this.w_FIGLIO.w_WRREPFLD)
          do case
            case this.w_FIGLIO.w_WRTYPEOB = 8
              if !EMPTY( NVL(ALLTRIM(this.w_FIGLIO.w_WRCURFLD), "") )
                this.w_CTRL.FindString(ALLTRIM(this.w_FIGLIO.w_WRCURFLD), .F., .T.)     
              else
                this.w_CTRL.FindString(ALLTRIM(this.w_FIGLIO.w_WRREPFLD), .F., .T.)     
              endif
            case this.w_FIGLIO.w_WRTYPEOB = 5
              if !EMPTY( NVL(ALLTRIM(this.w_FIGLIO.w_WRFLDESC), "") )
                this.w_CTRL.FindString(ALLTRIM(this.w_FIGLIO.w_WRFLDESC), .F., .T.)     
              else
                this.w_CTRL.FindString(ALLTRIM(this.w_FIGLIO.w_WRREPFLD), .F., .T.)     
              endif
          endcase
        endif
        this.bUpdateParentObject=.F.
        this.w_CTRL = .NULL.
        this.w_FIGLIO = .NULL.
        i_retcode = 'stop'
        return
      case this.pOPER == "CHKMOD"
        if !EMPTY(this.oParentObject.w_OLDMODEL) AND this.oParentObject.w_OLDMODEL<>this.oParentObject.w_MODELLO
          * --- Testata
          this.w_FIGLIO = this.w_PADRE.GSUT_MMR
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Dettaglio
          this.w_FIGLIO = this.w_PADRE.GSUT_MMD
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Piede
          this.w_FIGLIO = this.w_PADRE.GSUT_MMF
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Parametri
          this.w_FIGLIO = this.w_PADRE.GSUT_MMP
          this.Page_4()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          * --- Elimino informazioni di raggruppamento
          USE IN SELECT("VARLIST")
          * --- Sbianco l'elenco dei campi gi� selezionati
          UPDATE (this.oParentObject.w_CURCAMPI) SET FIELDUSED="N" WHERE 1=1
        endif
        i_retcode = 'stop'
        return
    endcase
    if this.pOPER=="EXIT" 
      this.oParentObject.oParentObject.w_PATHREPORT = " "
      this.Page_7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.w_PADRE.bUpdated = .F.
      this.w_PADRE.oPGFRM.ActivePage=this.oParentObject.w_CUR_STEP
      this.bUpdateParentObject=.F.
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    ERRORE = .F.
    olderror = ON("ERROR")
    ON ERROR ERRORE = .T.
    use in select(this.oParentObject.w_TABREPOR)
    * --- Cancello il modello elaborato, se presente
    this.w_TIMEOUT = seconds()+30
    do while file(this.oParentObject.w_PATHREP) AND seconds()<=this.w_TIMEOUT
      delete file (FORCEEXT(this.oParentObject.w_PATHREP, "FRX") )
      delete file (FORCEEXT(this.oParentObject.w_PATHREP, "FRT"))
    enddo
    if seconds() <= this.w_TIMEOUT
      ERRORE=.F.
    endif
    * --- Copia i files del modello report nella temp della procedura
    this.oParentObject.w_PATHREP = FORCEEXT(alltrim(addbs(alltrim(g_TEMPADHOC))+JUSTFNAME(this.oParentObject.w_PATMOD)), "FRX")
    * --- .FRX
    copy file (this.oParentObject.w_PATMOD) to (this.oParentObject.w_PATHREP)
    * --- .FRT
    copy file (FORCEEXT(this.oParentObject.w_PATMOD, "FRT")) to (FORCEEXT( this.oParentObject.w_PATHREP, "FRT" ))
    if ERRORE
      Ah_ErrorMsg("Errore durante la copia del modello report nella cartella temporanea.%0Verificare che il modello sia presente e non in uso", 48,"")
      On ERROR &olderror
      this.oParentObject.w_CUR_STEP = 1
      i_retcode = 'stop'
      return
    endif
    On ERROR &olderror
    if this.oParentObject.w_CUR_STEP > 1
      USE IN SELECT ("CURDATI")
      L_LOGO = GETBMP(i_CODAZI)
      if !USED(this.oParentObject.w_TABREPOR)
        use (this.oParentObject.w_PATHREP) IN 0 ALIAS (this.oParentObject.w_TABREPOR) EXCLUSIVE
      endif
      * --- Inserisco il Titolo, la Lingua e impostazione GDI+ inseriti a pag.2 del wizard
      * --- Barra del titolo
      update (this.oParentObject.w_TABREPOR) set USER = this.oParentObject.w_CODISOLING, COMMENT = this.oParentObject.w_TITOLO where OBJTYPE=9 and OBJCODE = 1
      * --- Barra del dettaglio
      update (this.oParentObject.w_TABREPOR) set USER = iif(this.oParentObject.w_GDI="S"," ","bnogdi") where OBJTYPE=9 and OBJCODE = 4
      UPDATE (this.oParentObject.w_TABREPOR) SET EXPR="L_TITOLO", TAG="'"+EVL(ALLTRIM(this.oParentObject.w_TITOLO), ah_MsgFormat("Titolo report") )+"'" WHERE NAME=="L_TITOLO" AND OBJTYPE = 18
      this.w_LISTACAMPI_TIP = ""
      this.w_LISTACAMPI = ""
      if VARTYPE(aVARLIST)<>"U"
        RELEASE aVARLIST
      endif
      DIMENSION aVARLIST(1,3)
      * --- Carico i dati nei vari figli con i relativi campi del report
      this.w_FIGLIO = this.w_PADRE.GSUT_MMR
      if this.w_FIGLIO.NumRow()<1
        SELECT CAST(EXPR AS VARCHAR(240)) AS NAMEOBJ,OBJTYPE FROM (this.oParentObject.w_TABREPOR) WHERE ; 
 OBJTYPE IN (5,8,18) AND (EXPR LIKE "%ZZ_HDR%" OR EXPR LIKE "%ZZ_LBL_HDR%") ORDER BY NAMEOBJ INTO CURSOR "NEWOBJ"
        this.Page_9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_FIGLIO = this.w_PADRE.GSUT_MMD
      if this.w_FIGLIO.NumRow()<1
        SELECT CAST(EXPR AS VARCHAR(240)) AS NAMEOBJ,OBJTYPE FROM (this.oParentObject.w_TABREPOR) WHERE ; 
 OBJTYPE IN (5,8,18) AND (EXPR LIKE "%ZZ_DTL%" OR EXPR LIKE "%ZZ_LBL_DTL%") INTO CURSOR "NEWOBJ"
        this.Page_9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Page_3()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_FIGLIO = this.w_PADRE.GSUT_MMF
      if this.w_FIGLIO.NumRow()<1
        SELECT CAST(EXPR AS VARCHAR(240)) AS NAMEOBJ,OBJTYPE FROM (this.oParentObject.w_TABREPOR) WHERE ; 
 OBJTYPE IN (5,8,18) AND (EXPR LIKE "%ZZ_FTR%" OR EXPR LIKE "%ZZ_LBL_FTR%") INTO CURSOR"NEWOBJ"
        this.Page_9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.w_INSNEWPA = .T.
      this.w_FIGLIO = this.w_PADRE.GSUT_MMP
      if this.w_FIGLIO.NumRow()<1
        SELECT CAST(EXPR AS VARCHAR(240)) AS NAMEOBJ,OBJTYPE FROM (this.oParentObject.w_TABREPOR) WHERE ; 
 OBJTYPE IN (5,8,18) AND (EXPR LIKE "%ZZ_PAR%" OR EXPR LIKE "%ZZ_LBL_PAR%") INTO CURSOR "NEWOBJ"
        this.Page_9()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      this.Page_8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Page_5()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      this.Page_6()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creo le variabili per i campi calcolati di dettaglio
    if VARTYPE(aVARLIST)<>"U" AND VARTYPE( aVARLIST[ 1, 1 ])<>"L"
      this.w_CICLO = ALEN( aVARLIST, 1)
    else
      this.w_CICLO = 0
    endif
     
 Select("VARLIST") 
 Zap
    * --- Prendo la parte del report inerente a campi, etichette e variabili
    SELECT * FROM (this.oParentObject.w_TABREPOR) WHERE OBJTYPE<23 INTO CURSOR "CUR_APP" READWRITE
    do while this.w_CICLO>0
      * --- Aggiungo le variabili richieste dal wizard
      INSERT INTO "CUR_APP" (PLATFORM,UNIQUEID,TIMESTAMP,OBJTYPE,NAME, ; 
 EXPR,UNIQUE,TAG,TOTALTYPE,RESETTOTAL,CURPOS) ; 
 VALUES ("WINDOWS",SYS(2015),INT(VAL(SYS(1))),18, aVARLIST[this.w_CICLO, 1 ], aVARLIST[this.w_CICLO, 2 ], .T.,"0", ; 
 INT(VAL( aVARLIST[this.w_CICLO, 3 ] ) ),1,.F.)
      * --- Aggiungo le variabili nel cursore 'VARLIST'
      INSERT INTO "VARLIST" VALUES ( aVARLIST[ this.w_CICLO, 1 ]," ","N","N")
      this.w_CICLO = this.w_CICLO - 1
    enddo
    * --- Inserisco la parte del report relativa a Font e data environment
    INSERT INTO "CUR_APP" SELECT * FROM (this.oParentObject.w_TABREPOR) WHERE OBJTYPE >=23
    * --- Cancello il contenuto attuale del report
    DELETE FROM (this.oParentObject.w_TABREPOR)
    SELECT(this.oParentObject.w_TABREPOR)
    * --- Forzo l'eliminazione dei records cancellati
    PACK
    * --- Inserisco tutti gli oggetti originari e nuove variabili nel report
    INSERT INTO (this.oParentObject.w_TABREPOR) SELECT * FROM CUR_APP
    * --- Chiudo e apporto le modifiche
    USE IN SELECT("CUR_APP")
  endproc


  procedure Page_4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do while this.w_FIGLIO.NumRow()>0
      this.w_FIGLIO.FirstRow()     
      this.w_FIGLIO.SetRow()     
      if this.w_FIGLIO.FullRow()
        this.w_FIGLIO.DeleteRow()     
      endif
    enddo
  endproc


  procedure Page_5
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Creazione gruppi
    this.w_LISGRPFLD = ""
    * --- Prendo la parte del report inerente alla definizione 
    this.w_CURZOOM = this.w_PADRE.w_GRUPFLDS.cCursor
    * --- Ordino il cursore assegnato al vzm in base alla sequenza di raggruppamento impostata
    select * from (this.w_CURZOOM) WHERE XCHK=1 order by FIELDORD into cursor "APPOGGIO" 
    if Reccount("APPOGGIO") > 0
      SELECT * FROM (this.oParentObject.w_TABREPOR) WHERE OBJTYPE=1 INTO CURSOR "CUR_APP" READWRITE
      * --- Procedo ad inserire solo i records relativi alle sezioni prima del dettaglio OBJTYPE = 9 e OBJCODE = 4
      Select(this.oParentObject.w_TABREPOR)
      Go Top
      SCAN
      do case
        case objtype=9 and objcode < 4
          this.w_VHEIGHT_T = this.w_VHEIGHT_T + 2083.3333 + Height
          Scatter Memvar Memo
          Select ("CUR_APP")
          Append blank
          Gather memvar Memo
        case objtype=9 and objcode = 4
          this.w_VHEIGHT_D = Height
          exit
      endcase
      ENDSCAN
      this.w_NUMGRUPPI = 0
      * --- Aggiungo i ragguppamenti (Parte TESTATA)
       
 Select("APPOGGIO") 
 Go Top 
 SCAN FOR XCHK=1
      this.w_GRUFIELD = FIELDNAME
      if ! empty(this.w_GRUFIELD)
        * --- Aggiungo i gruppi di testata
        INSERT INTO "CUR_APP" (PLATFORM,UNIQUEID,OBJTYPE,OBJCODE,EXPR,HEIGHT) ; 
 VALUES ("WINDOWS",SYS(2015),9,3,this.w_GRUFIELD,1667)
        this.w_NUMGRUPPI = this.w_NUMGRUPPI + 1
        this.w_LISGRPFLD = this.w_LISGRPFLD+"'"+alltrim(chrtran(this.w_GRUFIELD,"+",","))+"'"+","
      endif
      ENDSCAN
      this.w_LISGRPFLD = alltrim(substr(this.w_LISGRPFLD,1,rat(",",this.w_LISGRPFLD)-1))
      * --- Aggiungo il dettaglio del report
      INSERT INTO "CUR_APP" SELECT * FROM (this.oParentObject.w_TABREPOR) WHERE OBJTYPE=9 and OBJCODE = 4
      this.w_VHEIGHT_D = this.w_VHEIGHT_D + this.w_VHEIGHT_T + 2083.3333 
      * --- Aggiungo i ragguppamenti (Parte PIEDE)
       
 Select("APPOGGIO") 
 Go Top 
 SCAN FOR XCHK=1
      this.w_GRUFIELD = FIELDNAME
      if ! empty(this.w_GRUFIELD)
        * --- Aggiungo i gruppi di piede
        INSERT INTO "CUR_APP" (PLATFORM,UNIQUEID,OBJTYPE,OBJCODE) ; 
 VALUES ("WINDOWS",SYS(2015),9,5)
      endif
      ENDSCAN
      this.w_VHEIGHT_P = this.w_VHEIGHT_D
      * --- Inserisco la parte restante del report relativa a campi,variabili,Font e data environment
      Select(this.oParentObject.w_TABREPOR)
      Go Top
      SCAN
      do case
        case objtype=9 and objcode < 4
          * --- Nulla
        case objtype=9 and objcode = 4
          * --- Nulla
        case objtype=1
          * --- Nulla
        case objtype=9 and objcode > 4
          Scatter Memvar Memo
          Select ("CUR_APP")
          Append blank
          Gather memvar Memo
          this.w_VHEIGHT_P = this.w_VHEIGHT_P + Height + 2083.3333
        otherwise
          Scatter Memvar Memo
          Select ("CUR_APP")
          Append blank
          Gather memvar Memo
      endcase
      ENDSCAN
      * --- Modifico le coordinate verticali degli oggetti compresi negli eventuali piedi originari del report e nel page footer
      update CUR_APP set Vpos = Vpos + ((2083.3333+1667)*this.w_NUMGRUPPI+2083.3333*this.w_NUMGRUPPI) where Objtype in (5, 6, 8) and between(Vpos,this.w_VHEIGHT_D,this.w_VHEIGHT_P)
      if this.w_NUMGRUPPI > 1
        * --- Tolgo il primo campo di raggruppamento perch� gi� gestito
        this.w_LISGRPFLDAPP = alltrim(substr(this.w_LISGRPFLD,at(",",this.w_LISGRPFLD)+1))
        this.w_LISGRPFLDAPP = CHRTRAN(this.w_LISGRPFLDAPP,"'","")
        this.w_POSGRUP = this.w_NUMGRUPPI - 1
        do while Not empty(this.w_LISGRPFLDAPP)
          if at(",",this.w_LISGRPFLDAPP) > 0
            update CUR_APP set Vpos = Vpos + ((2083.3333+1667)*(this.w_NUMGRUPPI-this.w_POSGRUP)) where Objtype = 8 and between(Vpos,int(this.w_VHEIGHT_T),this.w_VHEIGHT_D) ; 
 and ALLTRIM(expr) == alltrim(substr(this.w_LISGRPFLDAPP,1,at(",",this.w_LISGRPFLDAPP)-1))
            this.w_LISGRPFLDAPP = strtran(this.w_LISGRPFLDAPP,substr(this.w_LISGRPFLDAPP,1,at(",",this.w_LISGRPFLDAPP)),"")
          else
            update CUR_APP set Vpos = Vpos + ((2083.3333+1667)*(this.w_NUMGRUPPI-this.w_POSGRUP)) where Objtype = 8 and between(Vpos,int(this.w_VHEIGHT_T),this.w_VHEIGHT_D) ; 
 and ALLTRIM(expr) == alltrim(this.w_LISGRPFLDAPP)
            * --- Esco dal ciclo
            this.w_LISGRPFLDAPP = " "
          endif
          this.w_POSGRUP = iif(this.w_POSGRUP >=1,this.w_POSGRUP - 1,0)
        enddo
      endif
      * --- Modifico le coordinate verticali degli oggetti compresi nelle eventuali testate originarie del report e nel dettaglio
      * --- Setto come carattere di separatore decimale il '.'
       
 DEFDECCHAR = SET("POINT") 
 SET POINT TO
      Op = "update CUR_APP set Vpos = Vpos + ((2083.3333+1667)*"+alltrim(str(this.w_NUMGRUPPI,10,0))+") where Objtype in (5, 6, 8) and between(Vpos,int("+alltrim(str(this.w_VHEIGHT_T,10,4))+"),"+alltrim(str(this.w_VHEIGHT_D,10,4))+") and alltrim(expr) not in ("+this.w_LISGRPFLD+') and !empty(nvl(expr," "))'
      * --- Ripristino il vecchio carattere di separatore decimale
      SET POINT TO DEFDECCHAR
      &Op
      * --- Cancello il contenuto attuale del report
      DELETE FROM (this.oParentObject.w_TABREPOR)
      SELECT(this.oParentObject.w_TABREPOR)
      * --- Forzo l'eliminazione dei records cancellati
      PACK
      * --- Inserisco tutti gli oggetti originari e nuove variabili nel report
      INSERT INTO (this.oParentObject.w_TABREPOR) SELECT * FROM CUR_APP
      * --- Chiudo i cursori usati
      USE IN SELECT("CUR_APP")
      USE IN SELECT("APPOGGIO")
    else
      USE IN SELECT("APPOGGIO")
    endif
  endproc


  procedure Page_6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Avvio anteprima report  XFRX
    if ! USED(this.oParentObject.w_TABREPOR)
      use (this.oParentObject.w_PATHREP) IN 0 ALIAS (this.oParentObject.w_TABREPOR) EXCLUSIVE
    endif
    this.w_LISTACAMPI_TIP = SUBSTR(this.w_LISTACAMPI_TIP,1,LEN(this.w_LISTACAMPI_TIP)-1)
    this.w_LISTACAMPI = SUBSTR(this.w_LISTACAMPI,1,LEN(this.w_LISTACAMPI)-1)
    if this.oParentObject.w_CUR_STEP = 7 AND EMPTY(this.w_LISTACAMPI_TIP)
      this.w_LISTACAMPI_TIP = "DUMMYFLD C(60)"
      this.w_LISTACAMPI = "' '"
    endif
    if !EMPTY(this.w_LISTACAMPI_TIP) AND !EMPTY(this.w_LISTACAMPI)
      LOCAL L_MACRO
      L_MACRO="CREATE CURSOR CURDATI ("+this.w_LISTACAMPI_TIP+")"
      &L_MACRO
      L_MACRO="INSERT INTO CURDATI VALUES("+this.w_LISTACAMPI+")"
      &L_MACRO
    endif
    use in select(this.oParentObject.w_TABREPOR)
    if VARTYPE(this.oParentObject.w_OBJXFRX) <> "O"
      this.oParentObject.w_OBJXFRX = CREATEOBJECT("ZXFRXMAKEDOCUMENT")
    endif
    if USED("CURDATI")
      this.oParentObject.w_OBJXFRX.RESETXFRX()     
      this.oParentObject.w_OBJXFRX.cReport = this.oParentObject.w_PATHREP
      this.oParentObject.w_OBJXFRX.cCursor = "CURDATI"
      this.oParentObject.w_OBJXFRX.Preview()     
      this.oParentObject.w_XFRXHEAD.Visible = this.oParentObject.w_CUR_STEP = 2
      this.oParentObject.w_XFRXBODY.Visible = this.oParentObject.w_CUR_STEP = 3
      this.oParentObject.w_XFRXRAGG.Visible = this.oParentObject.w_CUR_STEP = 4
      this.oParentObject.w_XFRXFOOT.Visible = this.oParentObject.w_CUR_STEP = 5
      this.oParentObject.w_XFRXPARA.Visible = this.oParentObject.w_CUR_STEP = 6
      this.oParentObject.w_XFRXLAST.Visible = this.oParentObject.w_CUR_STEP = 7
      do case
        case this.oParentObject.w_CUR_STEP = 2
          if this.oParentObject.w_XFRXHEAD.pages.PageCount > 0
            this.oParentObject.w_XFRXHEAD.removepage()     
          endif
          this.oParentObject.w_XFRXHEAD.PREVIEWXFF(this.oParentObject.w_OBJXFRX.GetXfDocument())     
        case this.oParentObject.w_CUR_STEP = 3
          if this.oParentObject.w_XFRXBODY.pages.PageCount > 0
            this.oParentObject.w_XFRXBODY.removepage()     
          endif
          this.oParentObject.w_XFRXBODY.PREVIEWXFF(this.oParentObject.w_OBJXFRX.GetXfDocument())     
        case this.oParentObject.w_CUR_STEP = 4
          if this.oParentObject.w_XFRXRAGG.pages.PageCount > 0
            this.oParentObject.w_XFRXRAGG.removepage()     
          endif
          this.oParentObject.w_XFRXRAGG.PREVIEWXFF(this.oParentObject.w_OBJXFRX.GetXfDocument())     
        case this.oParentObject.w_CUR_STEP = 5
          if this.oParentObject.w_XFRXFOOT.pages.PageCount > 0
            this.oParentObject.w_XFRXFOOT.removepage()     
          endif
          this.oParentObject.w_XFRXFOOT.PREVIEWXFF(this.oParentObject.w_OBJXFRX.GetXfDocument())     
        case this.oParentObject.w_CUR_STEP = 6
          if this.oParentObject.w_XFRXPARA.pages.PageCount > 0
            this.oParentObject.w_XFRXPARA.removepage()     
          endif
          this.oParentObject.w_XFRXPARA.PREVIEWXFF(this.oParentObject.w_OBJXFRX.GetXfDocument())     
        case this.oParentObject.w_CUR_STEP = 7
          if this.oParentObject.w_XFRXLAST.pages.PageCount > 0
            this.oParentObject.w_XFRXLAST.removepage()     
          endif
          this.oParentObject.w_XFRXLAST.PREVIEWXFF(this.oParentObject.w_OBJXFRX.GetXfDocument())     
      endcase
    else
      this.oParentObject.w_OBJXFRX = .NULL.
    endif
  endproc


  procedure Page_7
    param pSaveWizard
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura report wizard
    USE IN SELECT(this.oParentObject.w_TABREPOR)
    USE IN SELECT(JUSTSTEM(this.oParentObject.w_PATHREP))
    USE IN SELECT(this.oParentObject.w_CURCAMPI)
    USE IN SELECT("CURDATI")
    USE IN SELECT("VARLIST")
    if file(this.oParentObject.w_PATHREP)
      delete file (this.oParentObject.w_PATHREP)
      delete file (FORCEEXT( this.oParentObject.w_PATHREP, "FRT" ))
    endif
    * --- Elimino il messaggio di avviso su perdita modifiche nei figli integrati
    this.w_PADRE.GSUT_MMR.bUpdated = .F.
    this.w_PADRE.GSUT_MMD.bUpdated = .F.
    this.w_PADRE.GSUT_MMF.bUpdated = .F.
    this.w_PADRE.GSUT_MMP.bUpdated = .F.
    if m.pSaveWizard
      this.w_PADRE.bUpdated = .T.
      this.w_PADRE.ecpSave()     
    else
      this.w_PADRE.bUpdated = .F.
      this.w_PADRE.ecpQuit()     
    endif
  endproc


  procedure Page_8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_FIGLIO.MarkPos()     
    this.w_FIGLIO.FirstRow()     
    do while !this.w_FIGLIO.Eof_Trs()
      this.w_FIGLIO.SetRow()     
      if this.w_FIGLIO.FullRow()
        if AT(ALLTRIM(this.w_FIGLIO.w_WRREPFLD), this.w_LISTACAMPI_TIP) < 1 AND this.w_FIGLIO.w_WRTYPEOB = 8
          if !EMPTY( NVL(ALLTRIM(this.w_FIGLIO.w_WRCURFLD), "") )
            if AT(Lower(CHRTRAN(ALLTRIM(this.w_FIGLIO.w_WRCURFLD), "'", '"')), lower(this.w_LISTACAMPI))=0
              this.w_LISTACAMPI = this.w_LISTACAMPI+"'"+CHRTRAN(ALLTRIM(this.w_FIGLIO.w_WRCURFLD), "'", '"')+"',"
              this.w_LISTACAMPI_TIP = this.w_LISTACAMPI_TIP+ALLTRIM(this.Page_10(this.w_FIGLIO.w_WRCURFLD))+" C(60)," 
            endif
          else
            if AT(LOWER(ALLTRIM(this.w_FIGLIO.w_WRREPFLD)), lower(this.w_LISTACAMPI_TIP))=0
              this.w_LISTACAMPI = this.w_LISTACAMPI+"'"+CHRTRAN(ALLTRIM(this.w_FIGLIO.w_WRREPFLD), "'", '"')+"',"
              this.w_LISTACAMPI_TIP = this.w_LISTACAMPI_TIP+ALLTRIM(this.w_FIGLIO.w_WRREPFLD)+" C(60),"
            endif
          endif
        endif
        if !EMPTY(NVL(this.w_FIGLIO.w_WRCALFOR, " "))
          * --- Conto il numero di operazioni associate ai campi di dettaglio
          if VARTYPE(aVARLIST[ ALEN(aVARLIST, 1), 1] )<>"L"
            DIMENSION aVARLIST( ALEN(aVARLIST, 1) + 1, 3 )
          endif
          aVARLIST[ ALEN(aVARLIST, 1), 1 ] = alltrim(this.w_FIGLIO.w_WRCALFOR)+"_"+alltrim(this.w_FIGLIO.w_WRCURFLD)
          aVARLIST[ ALEN(aVARLIST, 1), 2 ] = alltrim(this.w_FIGLIO.w_WRCURFLD)
          do case
            case alltrim(this.w_FIGLIO.w_WRCALFOR)=="COU"
              aVARLIST[ ALEN(aVARLIST, 1), 3 ] = "1"
            case alltrim(this.w_FIGLIO.w_WRCALFOR)=="SUM"
              aVARLIST[ ALEN(aVARLIST, 1), 3 ] = "2"
            case alltrim(this.w_FIGLIO.w_WRCALFOR)=="MED"
              aVARLIST[ ALEN(aVARLIST, 1), 3 ] = "3"
            case alltrim(this.w_FIGLIO.w_WRCALFOR)=="MIN"
              aVARLIST[ ALEN(aVARLIST, 1), 3 ] = "4"
            case alltrim(this.w_FIGLIO.w_WRCALFOR)=="MAX"
              aVARLIST[ ALEN(aVARLIST, 1), 3 ] = "5"
            case alltrim(this.w_FIGLIO.w_WRCALFOR)=="DEV"
              aVARLIST[ ALEN(aVARLIST, 1), 3 ] = "6"
            case alltrim(this.w_FIGLIO.w_WRCALFOR)=="VAR"
              aVARLIST[ ALEN(aVARLIST, 1), 3 ] = "7"
          endcase
        endif
        if this.w_FIGLIO.w_WRTYPEOB = 5 AND !EMPTY(NVL( this.w_FIGLIO.w_WRFLDESC, " "))
          this.w_FTR_OBJ = '"'+lower(ALLTRIM(this.w_FIGLIO.w_WRREPFLD))+'"'
          this.w_FTR_VAL = '"'+ALLTRIM(this.w_FIGLIO.w_WRFLDESC)+'"'
          UPDATE (this.oParentObject.w_TABREPOR) SET EXPR=this.w_FTR_VAL WHERE OBJTYPE = 5 AND lower(ALLTRIM(EXPR))==this.w_FTR_OBJ
        endif
        if this.w_FIGLIO.w_WRTYPEOB = 8 AND !EMPTY(NVL( this.w_FIGLIO.w_WRCURFLD, " "))
          this.w_FTR_OBJ = lower(ALLTRIM(this.w_FIGLIO.w_WRREPFLD))
          this.w_FTR_VAL = CHRTRAN(ALLTRIM(this.w_FIGLIO.w_WRCURFLD), "'", '"')
          UPDATE (this.oParentObject.w_TABREPOR) SET EXPR=this.w_FTR_VAL WHERE OBJTYPE = 8 AND lower(ALLTRIM(EXPR))==this.w_FTR_OBJ
        endif
      endif
      this.w_FIGLIO.NextRow()     
    enddo
    this.w_FIGLIO.RePos()     
  endproc


  procedure Page_9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if USED("NEWOBJ")
      this.w_FIGLIO.MarkPos()     
      SELECT "NEWOBJ"
      GO TOP
      SCAN
      this.w_FIGLIO.AddRow()     
      if OBJTYPE = 5
        this.w_FIGLIO.w_WRREPFLD = SUBSTR(ALLTRIM(NAMEOBJ),2,LEN(ALLTRIM(NAMEOBJ))-2)
      else
        this.w_FIGLIO.w_WRREPFLD = ALLTRIM(NAMEOBJ)
      endif
      this.w_FIGLIO.w_WRCURFLD = " "
      this.w_FIGLIO.w_WRFLDESC = "  "
      if this.oParentObject.w_FILLING="S"
        if this.w_INSNEWPA
          SELECT (this.oParentObject.w_CURPARAM)
        else
          SELECT (this.oParentObject.w_CURCAMPI)
        endif
        GO TOP
        LOCATE FOR FIELDUSED="N" OR FIELDUSED=IIF(UPPER(LEFT(ALLTRIM(this.w_FIGLIO.w_WRREPFLD), 6))=="ZZ_LBL", "N", "T")
        if FOUND()
          this.w_FIGLIO.w_WRCURFLD = IIF(UPPER(LEFT(ALLTRIM(this.w_FIGLIO.w_WRREPFLD), 6))=="ZZ_LBL", "", ALLTRIM(FIELDNAME) )
          this.w_FIGLIO.w_WRFLDESC = FIELDDESC
          this.w_FIGLIO.w_FIELDTYPE = FIELDTYPE
          REPLACE FIELDUSED WITH IIF(UPPER(LEFT(ALLTRIM(this.w_FIGLIO.w_WRREPFLD), 6))=="ZZ_LBL", "T", "S" )
        endif
      endif
      SELECT "NEWOBJ"
      this.w_FIGLIO.w_WRTYPEOB = OBJTYPE
      this.w_FIGLIO.SaveRow()
      SELECT "NEWOBJ"
      ENDSCAN
      this.w_FIGLIO.RePos()     
      this.w_FIGLIO.FirstRow()     
      if this.w_INSNEWPA
        SELECT (this.oParentObject.w_CURPARAM)
      else
        SELECT (this.oParentObject.w_CURCAMPI)
      endif
      GO TOP
      if this.w_INSNEWPA
        UPDATE (this.oParentObject.w_CURPARAM) SET FIELDUSED="N" WHERE FIELDUSED="T"
      else
        UPDATE (this.oParentObject.w_CURCAMPI) SET FIELDUSED="N" WHERE FIELDUSED="T"
      endif
      USE IN SELECT("NEWOBJ")
      this.w_INSNEWPA = .F.
    endif
  endproc


  procedure Page_10
    param w_FLDTONOR
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    private w_RETVAL
    m.w_RETVAL = space(60)
    m.w_RETVAL = CHRTRAN(m.w_FLDTONOR, "()/&%$�!?^*@#'"+'["]', "")
    return(m.w_RETVAL)
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
