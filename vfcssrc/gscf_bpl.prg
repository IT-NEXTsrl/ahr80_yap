* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscf_bpl                                                        *
*              Pulizia log controllo flussi                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2011-03-14                                                      *
* Last revis.: 2011-12-20                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscf_bpl",oParentObject)
return(i_retval)

define class tgscf_bpl as StdBatch
  * --- Local variables
  w_OASERIAL = space(10)
  w_OASTAMP = space(10)
  * --- WorkFile variables
  LOGCNTUT_idx=0
  LOGCNTFL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Pulizia log controllo flussi (da gscf_kpl)
    Select (this.oParentObject.w_ZOOM.cCursor)
    scan for xchk=1
    this.w_OASERIAL = LOSERIAL
    this.w_OASTAMP = LOSTAMP
    * --- Delete from LOGCNTUT
    i_nConn=i_TableProp[this.LOGCNTUT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGCNTUT_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"OASERIAL = "+cp_ToStrODBC(this.w_OASERIAL);
            +" and OASTAMP = "+cp_ToStrODBC(this.w_OASTAMP);
             )
    else
      delete from (i_cTable) where;
            OASERIAL = this.w_OASERIAL;
            and OASTAMP = this.w_OASTAMP;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    * --- Delete from LOGCNTFL
    i_nConn=i_TableProp[this.LOGCNTFL_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGCNTFL_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"LOSERIAL = "+cp_ToStrODBC(this.w_OASERIAL);
            +" and LOSTAMP = "+cp_ToStrODBC(this.w_OASTAMP);
             )
    else
      delete from (i_cTable) where;
            LOSERIAL = this.w_OASERIAL;
            and LOSTAMP = this.w_OASTAMP;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    endscan
    this.oParentObject.NotifyEvent("Interroga")     
    ah_errormsg("Elaborazione terminata.")
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,2)]
    this.cWorkTables[1]='LOGCNTUT'
    this.cWorkTables[2]='LOGCNTFL'
    return(this.OpenAllTables(2))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
