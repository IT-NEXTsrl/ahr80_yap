* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bee                                                        *
*              Esegue Elaborazioni KPI                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-07-04                                                      *
* Last revis.: 2015-12-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCodice
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bee",oParentObject,m.pCodice)
return(i_retval)

define class tgsut_bee as StdBatch
  * --- Local variables
  pCodice = space(10)
  w_TMPCUR = space(10)
  w_RESCUR = space(10)
  w_SCNCUR = space(10)
  w_RESXML = space(0)
  w_TITLECOL = space(10)
  w_oPARAM = .NULL.
  w_CODICE = space(10)
  w_CHKATT = space(1)
  w_CNDATT = space(250)
  w_FRQEXE = 0
  w_TIPOUG = space(1)
  w_FILTROUG = 0
  w_VQR = space(250)
  w_TIPRES = space(1)
  w_TIPSIN = space(1)
  w_TIPBENCH = space(1)
  w_BENCHMARK = 0
  w_LNKKPI = space(10)
  w_LNKSIN = space(10)
  w_RKSERIAL = space(10)
  w_LKSERIAL = space(10)
  w_MAXRES = 0
  w_SINTETICO = 0
  w_NROWS = 0
  w_NEXEC = 0
  w_DTEXE = ctot("")
  w_DTLASTEXE = ctot("")
  w_WRTLOG = .f.
  w_LOGMSG = space(0)
  w_ERRMSG = space(250)
  w_OLDONERROR = space(100)
  w_FINDRESULT = .f.
  * --- WorkFile variables
  ELABKPIM_idx=0
  ELABKPID_idx=0
  RESELKPI_idx=0
  LOGELKPI_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue le elaborazioni KPI registrandone i risultati nella tabella RESELKPI
    *     
    *     Se viene passato un codice elaborazione nel parametro pCodice viene eseguita solo quella,
    *     altrimenti si recuperano tutte quelle attive
    this.w_NEXEC = 0
    * --- Memorizzo stato ON ERROR
    this.w_OLDONERROR = ON("ERROR")
    * --- Setto ON ERROR
    Private bError 
 m.bError=.f. 
 ON ERROR bError=.t.
    if !Empty(Nvl(this.pCodice,""))
      * --- Esecuzione di una singola elaborazione
      this.w_DTEXE = SetInfoDate(g_CALUTD)
      this.w_CODICE = Alltrim(this.pCodice)
      this.w_RESXML = .Null.
      this.w_LOGMSG = "Inizio elaborazione: "+This.w_CODICE
      * --- Leggo i dati dell'elaborazione
      * --- Read from ELABKPIM
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ELABKPIM_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ELABKPIM_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "EKCHKATT,EKCNDATT,EKTIPRES,EKTIPSIN,EKBENCHM,EKBENFIX,EKBENCOD,EK_QUERY,EKWRTLOG,EKFRQEXE,EKNUMRES"+;
          " from "+i_cTable+" ELABKPIM where ";
              +"EKCODICE = "+cp_ToStrODBC(this.w_CODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          EKCHKATT,EKCNDATT,EKTIPRES,EKTIPSIN,EKBENCHM,EKBENFIX,EKBENCOD,EK_QUERY,EKWRTLOG,EKFRQEXE,EKNUMRES;
          from (i_cTable) where;
              EKCODICE = this.w_CODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_CHKATT = NVL(cp_ToDate(_read_.EKCHKATT),cp_NullValue(_read_.EKCHKATT))
        this.w_CNDATT = NVL(cp_ToDate(_read_.EKCNDATT),cp_NullValue(_read_.EKCNDATT))
        this.w_TIPRES = NVL(cp_ToDate(_read_.EKTIPRES),cp_NullValue(_read_.EKTIPRES))
        this.w_TIPSIN = NVL(cp_ToDate(_read_.EKTIPSIN),cp_NullValue(_read_.EKTIPSIN))
        this.w_TIPBENCH = NVL(cp_ToDate(_read_.EKBENCHM),cp_NullValue(_read_.EKBENCHM))
        this.w_BENCHMARK = NVL(cp_ToDate(_read_.EKBENFIX),cp_NullValue(_read_.EKBENFIX))
        this.w_LNKKPI = NVL(cp_ToDate(_read_.EKBENCOD),cp_NullValue(_read_.EKBENCOD))
        this.w_VQR = NVL(cp_ToDate(_read_.EK_QUERY),cp_NullValue(_read_.EK_QUERY))
        this.w_WRTLOG = NVL(cp_ToDate(_read_.EKWRTLOG),cp_NullValue(_read_.EKWRTLOG))
        this.w_FRQEXE = NVL(cp_ToDate(_read_.EKFRQEXE),cp_NullValue(_read_.EKFRQEXE))
        this.w_MAXRES = NVL(cp_ToDate(_read_.EKNUMRES),cp_NullValue(_read_.EKNUMRES))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_CHKATT = Nvl(this.w_CHKATT,"N")
      this.w_CNDATT = Alltrim(Nvl(this.w_CNDATT, ""))
      this.w_TIPRES = Alltrim(this.w_TIPRES)
      this.w_TIPSIN = Alltrim(this.w_TIPSIN)
      this.w_TIPBENCH = Alltrim(this.w_TIPBENCH)
      this.w_BENCHMARK = Nvl(this.w_BENCHMARK,0)
      this.w_LNKKPI = Alltrim(Nvl(this.w_LNKKPI,""))
      this.w_VQR = Alltrim(this.w_VQR)
      this.w_FRQEXE = Nvl(this.w_FRQEXE,0)
      this.w_WRTLOG = Iif( Empty( Nvl(this.w_WRTLOG,"")), "N", this.w_WRTLOG)
      this.w_MAXRES = Nvl(this.w_MAXRES, 0)
      * --- leggo data e ora ultima esecuzione
      * --- Select from RESELKPI
      i_nConn=i_TableProp[this.RESELKPI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RESELKPI_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select RKDATELA  from "+i_cTable+" RESELKPI ";
            +" where RKCODKPI="+cp_ToStrODBC(this.w_CODICE)+" and (RKCHKEND='R' or RKCHKEND='E')";
            +" order by RKDATELA Desc";
             ,"_Curs_RESELKPI")
      else
        select RKDATELA from (i_cTable);
         where RKCODKPI=this.w_CODICE and (RKCHKEND="R" or RKCHKEND="E");
         order by RKDATELA Desc;
          into cursor _Curs_RESELKPI
      endif
      if used('_Curs_RESELKPI')
        select _Curs_RESELKPI
        locate for 1=1
        do while not(eof())
        this.w_DTLASTEXE = _Curs_RESELKPI.RKDATELA
        * --- Esco subito perch� i risultati sono ordinati per data e ora:
        *     mi basta leggere il primo
        exit
          select _Curs_RESELKPI
          continue
        enddo
        use
      endif
      if Empty(this.w_DTLASTEXE) or (this.w_DTLASTEXE+this.w_FRQEXE)<this.w_DTEXE
        * --- Eseguo solo se � attiva e l'eventuale condizione � verificata
        if !IsNull(this.w_TIPRES) and this.w_CHKATT="S" and (Empty(this.w_CNDATT) Or Eval(this.w_CNDATT))
          this.w_ERRMSG = Alltrim(MESSAGE())
          this.Pag5("R")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if bError
            cp_msg("Impossibile valutare la condizione di attivazione per l'elaborazione "+This.w_CODICE)
            this.w_LOGMSG = this.w_LOGMSG+CHR(13)+"Errore nella valutazione della condizione di attivazione ("+this.w_CNDATT+") :"+CHR(13)+this.w_ERRMSG
            * --- metto qua l'inserimento del record di risultato allo stato "Running" per non sporcare la MESSAGE()
            *     e perch� la registrazione dell'errore esegue un Update su tale record che deve essere quindi stato inserito precedentemente
            this.Pag7()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Pag5("E")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_NEXEC = this.w_NEXEC+1
        else
          cp_msg("Elaborazione "+This.w_CODICE+" scartata: "+Iif(Empty(this.w_VQR) ,"non trovata", Iif(this.w_CHKATT="S","non abilitata", 'condizione di attivazione "'+This.w_CNDATT+'" non verificata')))
          this.w_LOGMSG = this.w_LOGMSG+CHR(13)+"Elaborazione scartata: "+Iif(Empty(this.w_VQR), "non trovata", Iif(this.w_CHKATT="N","non abilitata", "condizione di attivazione ("+This.w_CNDATT+") non verificata"))
          this.Pag5("S")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Pag7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        cp_msg("Elaborazione "+This.w_CODICE+" scartata: gi� lanciata di recente")
        this.w_LOGMSG = this.w_LOGMSG+CHR(13)+"Elaborazione scartata: gi� lanciata di recente"
        this.Pag5("S")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
    else
      * --- Esecuzione massiva delle elaborazioni attive
      this.w_SCNCUR = Sys(2015)
      * --- Eseguo la query e mi carico su cursore tutte le elaborazioni Attive:
      *     la query ordina le elaborazioni in base alla tipologia del benchmark, prima quelle che non richiedono un valore linkato
      *     per diminuire la probabilit� di non trovare un risultato elaborato quando necessario
      do vq_exec with "query/gsut_bee.vqr", this, this.w_SCNCUR, "", .f., .t. 
 Select(this.w_SCNCUR) 
 SCAN
      this.w_DTEXE = SetInfoDate(g_CALUTD)
      this.w_CODICE = Alltrim(EKCODICE)
      this.w_RESXML = .Null.
      this.w_CNDATT = Alltrim(Nvl(EKCNDATT,""))
      this.w_FRQEXE = Nvl(EKFRQEXE,0)
      this.w_MAXRES = Nvl(EKNUMRES, 0)
      this.w_WRTLOG = Iif( Empty( Nvl(this.w_WRTLOG,"")), "N", this.w_WRTLOG)
      this.w_LOGMSG = "Inizio elaborazione: "+This.w_CODICE
      * --- leggo data e ora ultima esecuzione
      * --- Select from RESELKPI
      i_nConn=i_TableProp[this.RESELKPI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RESELKPI_idx,2])
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select RKDATELA  from "+i_cTable+" RESELKPI ";
            +" where RKCODKPI="+cp_ToStrODBC(this.w_CODICE)+" and (RKCHKEND='R' or RKCHKEND='E')";
            +" order by RKDATELA Desc";
             ,"_Curs_RESELKPI")
      else
        select RKDATELA from (i_cTable);
         where RKCODKPI=this.w_CODICE and (RKCHKEND="R" or RKCHKEND="E");
         order by RKDATELA Desc;
          into cursor _Curs_RESELKPI
      endif
      if used('_Curs_RESELKPI')
        select _Curs_RESELKPI
        locate for 1=1
        do while not(eof())
        this.w_DTLASTEXE = _Curs_RESELKPI.RKDATELA
        * --- Esco subito perch� i risultati sono ordinati per data e ora:
        *     mi basta leggere il primo
        exit
          select _Curs_RESELKPI
          continue
        enddo
        use
      endif
      if Empty(this.w_DTLASTEXE) or (this.w_DTLASTEXE+this.w_FRQEXE)<this.w_DTEXE
        * --- Eseguo solo se l'eventuale condizione � verificata
        if Empty(this.w_CNDATT) Or Eval(this.w_CNDATT)
          this.w_ERRMSG = Alltrim(MESSAGE())
          this.Pag5("R")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          if bError
            cp_msg("Impossibile valutare la condizione di attivazione per l'elaborazione "+This.w_CODICE)
            this.w_LOGMSG = this.w_LOGMSG+CHR(13)+"Errore nella valutazione della condizione di attivazione ("+this.w_CNDATT+") :"+CHR(13)+this.w_ERRMSG
            * --- metto qua l'inserimento del record di risultato allo stato "Running" per non sporcare la MESSAGE()
            *     e perch� la registrazione dell'errore esegue un Update su tale record che deve essere quindi stato inserito precedentemente
            this.Pag7()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          Select(this.w_SCNCUR)
          this.w_TIPRES = Alltrim(EKTIPRES)
          this.w_TIPSIN = Alltrim(EKTIPSIN)
          this.w_TIPBENCH = Alltrim(EKBENCHM)
          this.w_BENCHMARK = Nvl(EKBENFIX,0)
          this.w_LNKKPI = Alltrim(EKBENCOD)
          this.w_VQR = Alltrim(EK_QUERY)
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Pag5("E")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.w_NEXEC = this.w_NEXEC+1
        else
          cp_msg("Elaborazione "+This.w_CODICE+' scartata: condizione di attivazione "'+this.w_CNDATT+'"non verificata')
          this.w_LOGMSG = this.w_LOGMSG+CHR(13)+"Elaborazione scartata: condizione di attivazione ("+this.w_CNDATT+") non verificata"
          this.Pag5("S")
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          this.Pag7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        cp_msg("Elaborazione "+This.w_CODICE+" scartata: gi� lanciata di recente")
        this.w_LOGMSG = this.w_LOGMSG+CHR(13)+"Elaborazione scartata: gi� lanciata di recente"
        this.Pag5("S")
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      Select(this.w_SCNCUR) 
 ENDSCAN 
 Use In Select(this.w_SCNCUR)
    endif
    this.Pag8()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    i_retcode = 'stop'
    i_retval = this.w_NEXEC
    return
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Esegue query e salva risultato in XML
    *     
    *     * !!! *  N.B. * !!! *
    *     * --- la colonna coi valori chiave si DEVE chiamare XCODE
    *     * --- la colonna del risultato si DEVE chiamare XVALUE 
    * --- Recupero i parametri dell'elaborazione e li salvo in un array
    cp_msg("Inizio elaborazione: "+This.w_CODICE)
    this.w_RESCUR = SYS(2015)
    this.w_oPARAM = CreateObject("prm_container")
    this.w_LOGMSG = this.w_LOGMSG+CHR(13)+"Lettura parametri della query..."
    * --- Select from ELABKPID
    i_nConn=i_TableProp[this.ELABKPID_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ELABKPID_idx,2])
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ELABKPID ";
          +" where EKCODICE="+cp_ToStrODBC(this.w_CODICE)+"";
           ,"_Curs_ELABKPID")
    else
      select * from (i_cTable);
       where EKCODICE=this.w_CODICE;
        into cursor _Curs_ELABKPID
    endif
    if used('_Curs_ELABKPID')
      select _Curs_ELABKPID
      locate for 1=1
      do while not(eof())
      if NOT EMPTY ( NVL (EK_PARAM,""))
        addproperty(This.w_oParam, Alltrim(EK_PARAM), Eval(Alltrim(EKVALORE)))
        if bError
          this.w_LOGMSG = this.w_LOGMSG+CHR(13)+"Errore aggiungendo parametro "+Alltrim(EK_PARAM)+": "+MESSAGE()
          this.Pag7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          this.w_LOGMSG = this.w_LOGMSG+CHR(13)+"Aggiunto parametro "+Alltrim(EK_PARAM)+" settato al valore "+Trans(Eval(Alltrim(EKVALORE)))
        endif
      endif
        select _Curs_ELABKPID
        continue
      enddo
      use
    endif
    * --- Eseguo la query e salvo risultato in XML
    this.w_LOGMSG = this.w_LOGMSG+CHR(13)+"Esecuzione query: "+this.w_VQR+"..."
    if Empty(this.w_VQR)
      this.w_LOGMSG = this.w_LOGMSG+"Errore:  "+CHR(13)+"Manca la query"
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    Local l_i 
 
 do vq_exec with This.w_VQR, This.w_oParam, This.w_RESCUR, "", .f., .t.
    if bError
      this.w_LOGMSG = this.w_LOGMSG+"Errore:  "+CHR(13)+MESSAGE()
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.w_LOGMSG = this.w_LOGMSG+"OK"
    endif
    cp_msg("Eseguita query per: "+This.w_CODICE)
    this.Pag4()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.Pag3()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- Genera XML e lo scrive nella variabile
    this.w_LOGMSG = this.w_LOGMSG+CHR(13)+"Pulizia campi nulli..."
    * --- Ciclo sulle colonne del cursore per eliminare valori nulli che non verrebbero riportati sull'XML
    *     (in caso contrario verrebbe rovinato l'ordinamento delle colonne)
    Local iCol, nCol, cUpdate 
 Dimension aCurFields(1) 
 m.nCol = Afields(m.aCurFields, This.w_RESCUR) 
 
 For iCol=1 To nCol 
 m.cUpdate = "Update "+This.w_RESCUR+" Set "+m.aCurFields(m.iCol,1)+"="
    do case
      case m.aCurFields(m.iCol,2)=="C"
        m.cUpdate = m.cUpdate+"Space("+Trans(m.aCurFields(m.iCol,3))+")"
      case m.aCurFields(m.iCol,2)=="N" Or m.aCurFields(m.iCol,2)=="I" Or m.aCurFields(m.iCol,2)=="F" Or m.aCurFields(m.iCol,2)=="B"
        m.cUpdate = m.cUpdate+"0"
      case m.aCurFields(m.iCol,2)=="L"
        m.cUpdate = m.cUpdate+".F."
      case m.aCurFields(m.iCol,2)=="T" OR m.aCurFields(m.iCol,2)=="D"
        m.cUpdate = m.cUpdate+"ctod('  /  /  ')"
    endcase
    m.cUpdate = m.cUpdate+" where isNull("+m.aCurFields(m.iCol,1)+")" 
 &cUpdate 
 
 EndFor 
 
 Release m.cUpdate, m.iCol, m.nCol, m.aCurFields
    if bError
      this.w_LOGMSG = this.w_LOGMSG+"Errore:  "+CHR(13)+MESSAGE()
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      this.w_LOGMSG = this.w_LOGMSG+"OK"
    endif
    this.w_LOGMSG = this.w_LOGMSG+CHR(13)+"Scrittura XML dei risultati..."
    CursorToXml(This.w_RESCUR, "This.w_RESXML", 3, 14, 0, "1")
    if bError
      this.w_LOGMSG = this.w_LOGMSG+"Errore:  "+CHR(13)+MESSAGE()
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      cp_msg("Creato XML dei risultati per: "+This.w_CODICE)
      this.w_LOGMSG = this.w_LOGMSG+"OK"
    endif
    Use In Select(This.w_RESCUR)
  endproc


  procedure Pag3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcola e aggiunge benchmark
    this.w_LOGMSG = this.w_LOGMSG+CHR(13)+"Calcolo benchmark..."
    if Upper(this.w_TIPBENCH)<>"NUL"
      * --- Aggiungo colonna per benchmark per tutti i casi tranne il merge
      *     (che aggiunger� la colonna trmaite join)
      cp_msg("Calcolo benchmark per: "+This.w_CODICE)
      if Upper(this.w_TIPBENCH)<>"LRE"
        this.Pag6()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      endif
      Local l_update 
 m.l_update=""
      do case
        case Upper(this.w_TIPBENCH)=="MAX"
          l_update = "Update "+This.w_RESCUR+" Set "+This.w_TITLECOL+"=(Select Max(XVALUE) from "+This.w_RESCUR+")"
        case Upper(this.w_TIPBENCH)=="MIN"
          l_update = "Update "+This.w_RESCUR+" Set "+This.w_TITLECOL+"=(Select Min(XVALUE) from "+This.w_RESCUR+")"
        case Upper(this.w_TIPBENCH)=="AVG"
          l_update = "Update "+This.w_RESCUR+" Set "+This.w_TITLECOL+"=(Select Avg(XVALUE) from "+This.w_RESCUR+")"
        case Upper(this.w_TIPBENCH)=="MED"
          this.w_TMPCUR = SYS(2015)
          Select * from (This.w_RESCUR) into cursor (This.w_TMPCUR) Order By XVALUE
          this.w_NROWS = Reccount(this.w_TMPCUR)
          Select(This.w_TMPCUR)
          if (this.w_NROWS%2)=0
            Go Record (This.w_NROWS*0.5)
            this.w_BENCHMARK = XVALUE
            Skip
            this.w_BENCHMARK = (this.w_BENCHMARK+XVALUE)*0.5
          else
            Go Record (Round(This.w_NROWS*0.5,0))
            this.w_BENCHMARK = XVALUE
          endif
          Use In Select(This.w_TMPCUR)
          l_update = "Update "+This.w_RESCUR+" Set "+This.w_TITLECOL+"="+cp_ToStrODBC(This.w_BENCHMARK)
        case Upper(this.w_TIPBENCH)=="CNT"
          l_update = "Update "+This.w_RESCUR+" Set "+This.w_TITLECOL+"=(Select Count(*) from "+This.w_RESCUR+")"
        case Upper(this.w_TIPBENCH)=="SUM"
          l_update = "Update "+This.w_RESCUR+" Set "+This.w_TITLECOL+"=(Select Sum(XVALUE) from "+This.w_RESCUR+")"
        case Upper(this.w_TIPBENCH)=="FIX"
          l_update = "Update "+This.w_RESCUR+" Set "+This.w_TITLECOL+"="+cp_ToStrODBC(This.w_BENCHMARK)
        case Upper(this.w_TIPBENCH)=="LSI" OR Upper(this.w_TIPBENCH)=="LRE"
          this.w_BENCHMARK = .Null.
          this.w_TMPCUR = SYS(2015)
          this.w_FINDRESULT = .f.
          * --- Read from ELABKPIM
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.ELABKPIM_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ELABKPIM_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "EKTIPSIN"+;
              " from "+i_cTable+" ELABKPIM where ";
                  +"EKCODICE = "+cp_ToStrODBC(this.w_LNKKPI);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              EKTIPSIN;
              from (i_cTable) where;
                  EKCODICE = this.w_LNKKPI;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_LNKSIN = NVL(cp_ToDate(_read_.EKTIPSIN),cp_NullValue(_read_.EKTIPSIN))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Se sto cercando di linkare il risultato sintetico di un'elaborazione senza risultato sintetico uso il valore di default
          if !(Upper(this.w_TIPBENCH)=="LSI" And Upper(this.w_LNKSIN)=="NUL")
            this.w_FINDRESULT = get_result_kpi(This, this.w_LNKKPI, "E", .F., "w_BENCHMARK", this.w_TMPCUR)
            * --- Se non trovo risultati e l'elaborazione � linkata su se stessa mi fermo subito, altrimenti entrerei in un loop
            if !this.w_FINDRESULT And this.w_CODICE<>this.w_LNKKPI
              * --- TO DO : Lanciare l'elaborazione per produrre un risultato
              GSUT_BEE(this,this.w_LNKKPI)
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
              this.w_FINDRESULT = get_result_kpi(This, this.w_LNKKPI, "E", .F., "w_BENCHMARK", this.w_TMPCUR)
              * --- Se adesso trovo il risultato significa che l'esecuzione � andata a buon fine
              if this.w_FINDRESULT
                this.w_NEXEC = this.w_NEXEC+1
              endif
            endif
          endif
          * --- Se non � stato possibile produrre un risultato uso dei valori dei default
          if !this.w_FINDRESULT
            if Upper(this.w_TIPBENCH)=="LRE"
              this.Pag6()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            l_update = "Update "+This.w_RESCUR+" Set "+This.w_TITLECOL+"=-1"
          else
            if Upper(this.w_TIPBENCH)=="LSI"
              l_update = "Update "+This.w_RESCUR+" Set "+This.w_TITLECOL+"="+cp_ToStrODBC(This.w_BENCHMARK)
            else
              l_update = "Select "+This.w_RESCUR+".*, "+This.w_TMPCUR+".xvalue as LRE_VAL From "+This.w_RESCUR+" Left Join "+This.w_TMPCUR+" On "+This.w_RESCUR+".xcode="+This.w_TMPCUR+".xcode Into Cursor "+This.w_RESCUR
            endif
          endif
      endcase
      &l_update 
 Release l_update
      if !(this.w_TMPCUR==this.w_RESCUR) And Used(this.w_TMPCUR)
        Use In Select(this.w_TMPCUR)
      endif
      if bError
        this.w_LOGMSG = this.w_LOGMSG+"Errore:  "+CHR(13)+MESSAGE()
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        cp_msg("Calcolato benchmark per: "+This.w_CODICE)
        this.w_LOGMSG = this.w_LOGMSG+"OK"
      endif
    else
      cp_msg("Benchmark non richiesto per: "+This.w_CODICE)
      this.w_LOGMSG = this.w_LOGMSG+CHR(13)+"Benchmark non richiesto"
    endif
  endproc


  procedure Pag4
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo del valore sintetico
    this.w_SINTETICO = .Null.
    this.w_LOGMSG = this.w_LOGMSG+CHR(13)+"Calcolo del risultato sintetico..."
    if Upper(this.w_TIPSIN)<>"NUL"
      this.w_TMPCUR = SYS(2015)
      do case
        case Upper(this.w_TIPSIN)=="MAX"
          Select Max(XVALUE) as RESULT from (This.w_RESCUR) into cursor (This.w_TMPCUR)
        case Upper(this.w_TIPSIN)=="MIN"
          Select Min(XVALUE) as RESULT from (This.w_RESCUR) into cursor (This.w_TMPCUR)
        case Upper(this.w_TIPSIN)=="AVG"
          Select Avg(XVALUE) as RESULT from (This.w_RESCUR) into cursor (This.w_TMPCUR)
        case Upper(this.w_TIPSIN)=="MED"
          Select * from (This.w_RESCUR) into cursor (This.w_TMPCUR) Order By XVALUE
          this.w_NROWS = Reccount(this.w_TMPCUR)
          Select(This.w_TMPCUR)
          if (this.w_NROWS%2)=0
            Go Record (This.w_NROWS*0.5)
            this.w_SINTETICO = XVALUE
            Skip
            this.w_SINTETICO = (this.w_SINTETICO+XVALUE)*0.5
          else
            Go Record (Round(This.w_NROWS*0.5,0))
            this.w_SINTETICO = XVALUE
          endif
        case Upper(this.w_TIPSIN)=="CNT"
          Select Count(*) as RESULT from (This.w_RESCUR) into cursor (This.w_TMPCUR)
        case Upper(this.w_TIPSIN)=="SUM"
          Select Sum(XVALUE) as RESULT from (This.w_RESCUR) into cursor (This.w_TMPCUR)
        case Upper(this.w_TIPSIN)=="FST"
          Select XVALUE as RESULT from (This.w_RESCUR) Where Recno()=1 into cursor (This.w_TMPCUR)
      endcase
      * --- Se la variabile non � stata ancora valorizzata il risultato si trova nel cursore temporaneo
      if IsNull(this.w_SINTETICO)
        Select (This.w_TMPCUR)
        this.w_SINTETICO = RESULT
      endif
      if bError
        this.w_LOGMSG = this.w_LOGMSG+"Errore:  "+CHR(13)+MESSAGE()
        this.Pag7()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      else
        cp_msg("Calcolato risultato sintetico per: "+This.w_CODICE)
        this.w_LOGMSG = this.w_LOGMSG+"OK"
      endif
      Use In Select(This.w_TMPCUR)
    else
      cp_msg("Risultato sintetico non richiesto per: "+This.w_CODICE)
      this.w_LOGMSG = this.w_LOGMSG+"non richiesto"
    endif
  endproc


  procedure Pag5
    param pTipo
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    private w_SERIAL
    m.w_SERIAL = space(10)
    private w_DDHH
    m.w_DDHH = ctot("")
    private w_AZIENDA
    m.w_AZIENDA = space(10)
    private w_NDEL
    m.w_NDEL = 0
    * --- Inserimento risultato
    m.pTipo = Upper(m.pTipo)
    do case
      case m.pTipo=="R" or m.pTipo=="S"
        * --- Try
        local bErr_04B30378
        bErr_04B30378=bTrsErr
        this.Try_04B30378()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          cp_msg(MESSAGE())
          this.w_LOGMSG = this.w_LOGMSG+"Errore:  "+CHR(13)+MESSAGE()
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          this.Pag7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
        bTrsErr=bTrsErr or bErr_04B30378
        * --- End
        * --- Pulizia vecchi risultati (se w_MAXRES=0 li tengo tutti)
        if this.w_MAXRES>0
          m.w_NDEL = 0
          m.w_AZIENDA = Alltrim(i_CODAZI)
          * --- Select from RESELKPI
          i_nConn=i_TableProp[this.RESELKPI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.RESELKPI_idx,2])
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select RKSERIAL,Max(RKDATELA)  from "+i_cTable+" RESELKPI ";
                +" where RKCODKPI="+cp_ToStrODBC(this.w_CODICE)+"";
                +" group by RKSERIAL";
                +" order by RKDATELA";
                 ,"_Curs_RESELKPI")
          else
            select RKSERIAL,Max(RKDATELA) from (i_cTable);
             where RKCODKPI=this.w_CODICE;
             group by RKSERIAL;
             order by RKDATELA;
              into cursor _Curs_RESELKPI
          endif
          if used('_Curs_RESELKPI')
            select _Curs_RESELKPI
            locate for 1=1
            do while not(eof())
            * --- Parto dai pi� vecchi e cancello quelli di troppo
            if Reccount()>(this.w_MAXRES+m.w_NDEL)
              m.w_SERIAL = _Curs_RESELKPI.RKSERIAL
              m.w_DDHH = _Curs_RESELKPI.RKDATELA
              * --- Delete from RESELKPI
              i_nConn=i_TableProp[this.RESELKPI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.RESELKPI_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"RKSERIAL = "+cp_ToStrODBC(m.w_SERIAL);
                       )
              else
                delete from (i_cTable) where;
                      RKSERIAL = m.w_SERIAL;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
              * --- Delete from LOGELKPI
              i_nConn=i_TableProp[this.LOGELKPI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.LOGELKPI_idx,2])
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              if i_nConn<>0
                i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                      +"LKCODKPI = "+cp_ToStrODBC(this.w_CODICE);
                      +" and LKDATELA = "+cp_ToStrODBC(m.w_DDHH);
                      +" and LKCODAZI = "+cp_ToStrODBC(m.w_AZIENDA);
                       )
              else
                delete from (i_cTable) where;
                      LKCODKPI = this.w_CODICE;
                      and LKDATELA = m.w_DDHH;
                      and LKCODAZI = m.w_AZIENDA;

                i_Rows=_tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                * --- Error: delete not accepted
                i_Error=MSG_DELETE_ERROR
                return
              endif
              m.w_NDEL = m.w_NDEL+1
            else
              exit
            endif
              select _Curs_RESELKPI
              continue
            enddo
            use
          endif
        endif
        this.w_LOGMSG = this.w_LOGMSG+"OK"
      case m.pTipo=="E" or m.pTipo=="X"
        cp_msg("Aggiornamento record risultato ["+Iif(m.pTipo="E", "ESEGUITA","TERMINATA CON ERRORI")+"] per: "+This.w_CODICE)
        this.w_LOGMSG = this.w_LOGMSG+CHR(13)+"Aggiornamento record risultato ["+Iif(m.pTipo="E", "ESEGUITA","TERMINATA CON ERRORI")+"]..."
        if Upper(cp_dbType)=="ORACLE"
          this.Pag9()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          * --- Try
          local bErr_04B10808
          bErr_04B10808=bTrsErr
          this.Try_04B10808()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            this.Pag9()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
          endif
          bTrsErr=bTrsErr or bErr_04B10808
          * --- End
        endif
        cp_msg("Terminata elaborazione: "+This.w_CODICE)
        this.w_LOGMSG = this.w_LOGMSG+"OK"
        if m.pTipo<>"X"
          this.Pag7()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
    endcase
  endproc
  proc Try_04B30378()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    cp_msg("Inserimento record risultato ["+Iif(m.pTipo="R", "IN ESECUZIONE","SCARTATA")+"] per: "+This.w_CODICE)
    this.w_LOGMSG = this.w_LOGMSG+CHR(13)+"Inserimento record risultato ["+Iif(m.pTipo="R", "IN ESECUZIONE","SCARTATA")+"]..."
    this.w_RKSERIAL = Space(10)
    i_nConn=i_TableProp[this.RESELKPI_IDX, 3] 
 cp_NextTableProg(this,i_nConn,"PRRESKPI","i_CODAZI,w_RKSERIAL")
    * --- Insert into RESELKPI
    i_nConn=i_TableProp[this.RESELKPI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RESELKPI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RESELKPI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"RKSERIAL"+",CPROWNUM"+",RKCODKPI"+",RKDATELA"+",RKCHKEND"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_RKSERIAL),'RESELKPI','RKSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(1),'RESELKPI','CPROWNUM');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'RESELKPI','RKCODKPI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DTEXE),'RESELKPI','RKDATELA');
      +","+cp_NullLink(cp_ToStrODBC(m.pTipo),'RESELKPI','RKCHKEND');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'RKSERIAL',this.w_RKSERIAL,'CPROWNUM',1,'RKCODKPI',this.w_CODICE,'RKDATELA',this.w_DTEXE,'RKCHKEND',m.pTipo)
      insert into (i_cTable) (RKSERIAL,CPROWNUM,RKCODKPI,RKDATELA,RKCHKEND &i_ccchkf. );
         values (;
           this.w_RKSERIAL;
           ,1;
           ,this.w_CODICE;
           ,this.w_DTEXE;
           ,m.pTipo;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_04B10808()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into RESELKPI
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.RESELKPI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.RESELKPI_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.RESELKPI_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"RKRESSIN ="+cp_NullLink(cp_ToStrODBC(this.w_SINTETICO),'RESELKPI','RKRESSIN');
      +",RKQRYXML ="+cp_NullLink(cp_ToStrODBC(this.w_RESXML),'RESELKPI','RKQRYXML');
      +",RKCHKEND ="+cp_NullLink(cp_ToStrODBC(m.pTipo),'RESELKPI','RKCHKEND');
          +i_ccchkf ;
      +" where ";
          +"RKSERIAL = "+cp_ToStrODBC(this.w_RKSERIAL);
          +" and CPROWNUM = "+cp_ToStrODBC(1);
             )
    else
      update (i_cTable) set;
          RKRESSIN = this.w_SINTETICO;
          ,RKQRYXML = this.w_RESXML;
          ,RKCHKEND = m.pTipo;
          &i_ccchkf. ;
       where;
          RKSERIAL = this.w_RKSERIAL;
          and CPROWNUM = 1;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return


  procedure Pag6
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    this.w_TITLECOL = Upper(this.w_TIPBENCH)+"_VAL"
    this.w_TMPCUR = SYS(2015)
    * --- Aggiungo colonna benchmark (tipo colonna a seconda del tipo risultato)
    if this.w_TIPRES=="T" And (this.w_TIPBENCH=="MAX" Or this.w_TIPBENCH=="MIN")
      SELECT *, SPACE(250) as (This.w_TITLECOL) FROM (This.w_RESCUR) INTO CURSOR (This.w_TMPCUR) ReadWrite
    else
      SELECT *, 123456789123.45678 as (This.w_TITLECOL) FROM (This.w_RESCUR) INTO CURSOR (This.w_TMPCUR) ReadWrite
    endif
    if bError
      this.w_LOGMSG = this.w_LOGMSG+"Errore aggiungendo colonna benchmark:  "+CHR(13)+MESSAGE()
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    else
      cp_msg("Aggiunta colonna benchmark per: "+This.w_CODICE)
    endif
    Use In Select(This.w_RESCUR)
    this.w_RESCUR = this.w_TMPCUR
  endproc


  procedure Pag7
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Verifica se c'� stato errore
    if bError
      bError = .F.
      this.Pag5("X")
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Registrazione Log
    if this.w_WRTLOG=="S"
      * --- Try
      local bErr_04B0DB98
      bErr_04B0DB98=bTrsErr
      this.Try_04B0DB98()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        cp_msg("Errore salvataggio log")
        * --- rollback
        bTrsErr=.t.
        cp_EndTrs(.t.)
      endif
      bTrsErr=bTrsErr or bErr_04B0DB98
      * --- End
    endif
    if bError
      this.Pag8()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
      i_retcode = 'stop'
      i_retval = -1
      return
    endif
  endproc
  proc Try_04B0DB98()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    cp_msg("Salvataggio log per: "+This.w_CODICE)
    this.w_LKSERIAL = Space(10)
    i_nConn=i_TableProp[this.LOGELKPI_IDX, 3] 
 cp_NextTableProg(this,i_nConn,"PRLOGKPI","w_LKSERIAL")
    * --- Insert into LOGELKPI
    i_nConn=i_TableProp[this.LOGELKPI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.LOGELKPI_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.LOGELKPI_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"LKSERIAL"+",LKCODKPI"+",LKDATELA"+",LKCODAZI"+",LKLOGMSG"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_LKSERIAL),'LOGELKPI','LKSERIAL');
      +","+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'LOGELKPI','LKCODKPI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_DTEXE),'LOGELKPI','LKDATELA');
      +","+cp_NullLink(cp_ToStrODBC(i_CODAZI),'LOGELKPI','LKCODAZI');
      +","+cp_NullLink(cp_ToStrODBC(this.w_LOGMSG),'LOGELKPI','LKLOGMSG');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'LKSERIAL',this.w_LKSERIAL,'LKCODKPI',this.w_CODICE,'LKDATELA',this.w_DTEXE,'LKCODAZI',i_CODAZI,'LKLOGMSG',this.w_LOGMSG)
      insert into (i_cTable) (LKSERIAL,LKCODKPI,LKDATELA,LKCODAZI,LKLOGMSG &i_ccchkf. );
         values (;
           this.w_LKSERIAL;
           ,this.w_CODICE;
           ,this.w_DTEXE;
           ,i_CODAZI;
           ,this.w_LOGMSG;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Pag8
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ripristina ON ERROR
    if Empty(this.w_OLDONERROR)
      ON ERROR
    else
      Local l_OldOnError 
 m.l_OldOnError = this.w_OLDONERROR 
 ON ERROR &l_OldOnError 
 Release l_OldOnError
    endif
  endproc


  procedure Pag9
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    private w_ROWNUM
    m.w_ROWNUM = 0
    private w_NPART
    m.w_NPART = 0
    private w_MPART
    m.w_MPART = space(0)
    * --- Risultati troppo lunghi per essere accolti nel campo Memo, li spezzo ogni 4000 caratteri
    * --- Try
    local bErr_04B0BB88
    bErr_04B0BB88=bTrsErr
    this.Try_04B0BB88()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- Fallito anche con lo spezzamento
      cp_msg(MESSAGE())
      this.w_LOGMSG = this.w_LOGMSG+"Errore:  "+CHR(13)+MESSAGE()
      this.Pag7()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    bTrsErr=bTrsErr or bErr_04B0BB88
    * --- End
  endproc
  proc Try_04B0BB88()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    m.w_NPART = Int(Len(this.w_RESXML)/4000) + Iif(Len(this.w_RESXML)%4000==0, 0, 1)
    For m.w_ROWNUM=1 to m.w_NPART
    m.w_MPART = Substr(this.w_RESXML, ((m.w_ROWNUM-1)*4000)+1, 4000)
    if m.w_ROWNUM==1
      * --- Write into RESELKPI
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.RESELKPI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RESELKPI_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.RESELKPI_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"RKRESSIN ="+cp_NullLink(cp_ToStrODBC(this.w_SINTETICO),'RESELKPI','RKRESSIN');
        +",RKQRYXML ="+cp_NullLink(cp_ToStrODBC(m.w_MPART),'RESELKPI','RKQRYXML');
        +",RKCHKEND ="+cp_NullLink(cp_ToStrODBC(m.pTipo),'RESELKPI','RKCHKEND');
            +i_ccchkf ;
        +" where ";
            +"RKSERIAL = "+cp_ToStrODBC(this.w_RKSERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(m.w_ROWNUM);
               )
      else
        update (i_cTable) set;
            RKRESSIN = this.w_SINTETICO;
            ,RKQRYXML = m.w_MPART;
            ,RKCHKEND = m.pTipo;
            &i_ccchkf. ;
         where;
            RKSERIAL = this.w_RKSERIAL;
            and CPROWNUM = m.w_ROWNUM;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Insert into RESELKPI
      i_nConn=i_TableProp[this.RESELKPI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RESELKPI_idx,2])
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.RESELKPI_idx,i_nConn)
      if i_nConn<>0
        i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                    " ("+"RKSERIAL"+",CPROWNUM"+",RKCODKPI"+",RKDATELA"+",RKCHKEND"+",RKRESSIN"+",RKQRYXML"+i_ccchkf+") values ("+;
        cp_NullLink(cp_ToStrODBC(this.w_RKSERIAL),'RESELKPI','RKSERIAL');
        +","+cp_NullLink(cp_ToStrODBC(m.w_ROWNUM),'RESELKPI','CPROWNUM');
        +","+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'RESELKPI','RKCODKPI');
        +","+cp_NullLink(cp_ToStrODBC(this.w_DTEXE),'RESELKPI','RKDATELA');
        +","+cp_NullLink(cp_ToStrODBC(m.pTipo),'RESELKPI','RKCHKEND');
        +","+cp_NullLink(cp_ToStrODBC(this.w_SINTETICO),'RESELKPI','RKRESSIN');
        +","+cp_NullLink(cp_ToStrODBC(m.w_MPART),'RESELKPI','RKQRYXML');
             +i_ccchkv+")")
      else
        cp_CheckDeletedKey(i_cTable,0,'RKSERIAL',this.w_RKSERIAL,'CPROWNUM',m.w_ROWNUM,'RKCODKPI',this.w_CODICE,'RKDATELA',this.w_DTEXE,'RKCHKEND',m.pTipo,'RKRESSIN',this.w_SINTETICO,'RKQRYXML',m.w_MPART)
        insert into (i_cTable) (RKSERIAL,CPROWNUM,RKCODKPI,RKDATELA,RKCHKEND,RKRESSIN,RKQRYXML &i_ccchkf. );
           values (;
             this.w_RKSERIAL;
             ,m.w_ROWNUM;
             ,this.w_CODICE;
             ,this.w_DTEXE;
             ,m.pTipo;
             ,this.w_SINTETICO;
             ,m.w_MPART;
             &i_ccchkv. )
        i_Rows=iif(bTrsErr,0,1)
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if i_Rows<0 or bTrsErr
        * --- Error: insert not accepted
        i_Error=MSG_INSERT_ERROR
        return
      endif
    endif
    EndFor
    return


  proc Init(oParentObject,pCodice)
    this.pCodice=pCodice
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ELABKPIM'
    this.cWorkTables[2]='ELABKPID'
    this.cWorkTables[3]='RESELKPI'
    this.cWorkTables[4]='LOGELKPI'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_RESELKPI')
      use in _Curs_RESELKPI
    endif
    if used('_Curs_RESELKPI')
      use in _Curs_RESELKPI
    endif
    if used('_Curs_ELABKPID')
      use in _Curs_ELABKPID
    endif
    if used('_Curs_RESELKPI')
      use in _Curs_RESELKPI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCodice"
endproc
