* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_acs                                                        *
*              Saldi commessa                                                  *
*                                                                              *
*      Author: Zucchetti SPA CF                                                *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-12-06                                                      *
* Last revis.: 2014-10-28                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_acs"))

* --- Class definition
define class tgsma_acs as StdForm
  Top    = 3
  Left   = 10

  * --- Standard Properties
  Width  = 565
  Height = 278+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-10-28"
  HelpContextID=109738857
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Constant Properties
  SALDICOM_IDX = 0
  MAGAZZIN_IDX = 0
  ART_ICOL_IDX = 0
  CAN_TIER_IDX = 0
  cFile = "SALDICOM"
  cKeySelect = "SCCODMAG,SCCODCAN,SCCODICE"
  cKeyWhere  = "SCCODMAG=this.w_SCCODMAG and SCCODCAN=this.w_SCCODCAN and SCCODICE=this.w_SCCODICE"
  cKeyWhereODBC = '"SCCODMAG="+cp_ToStrODBC(this.w_SCCODMAG)';
      +'+" and SCCODCAN="+cp_ToStrODBC(this.w_SCCODCAN)';
      +'+" and SCCODICE="+cp_ToStrODBC(this.w_SCCODICE)';

  cKeyWhereODBCqualified = '"SALDICOM.SCCODMAG="+cp_ToStrODBC(this.w_SCCODMAG)';
      +'+" and SALDICOM.SCCODCAN="+cp_ToStrODBC(this.w_SCCODCAN)';
      +'+" and SALDICOM.SCCODICE="+cp_ToStrODBC(this.w_SCCODICE)';

  cPrg = "gsma_acs"
  cComment = "Saldi commessa"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TIPOPE = space(4)
  w_OBTEST = ctod('  /  /  ')
  w_SCCODMAG = space(5)
  w_SCCODART = space(20)
  w_SCCODCAN = space(15)
  w_SCCODICE = space(40)
  w_DESMAG = space(30)
  w_DESART = space(40)
  w_DATOBSO = ctod('  /  /  ')
  w_TIPART = space(2)
  w_DESCAN = space(30)
  w_FLCOMM = space(1)
  w_SCQTAPER = 0
  w_SCQTRPER = 0
  w_DISPOPER = 0
  w_SCQTOPER = 0
  w_SCQTIPER = 0
  w_DISPCONT = 0
  w_SCQTAPRO = 0
  w_SCQTRPRO = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'SALDICOM','gsma_acs')
    stdPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_acsPag1","gsma_acs",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Saldi")
      .Pages(1).HelpContextID = 261051610
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSCCODMAG_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='MAGAZZIN'
    this.cWorkTables[2]='ART_ICOL'
    this.cWorkTables[3]='CAN_TIER'
    this.cWorkTables[4]='SALDICOM'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.SALDICOM_IDX,5],7]
    this.nPostItConn=i_TableProp[this.SALDICOM_IDX,3]
  return

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_SCCODMAG = NVL(SCCODMAG,space(5))
      .w_SCCODCAN = NVL(SCCODCAN,space(15))
      .w_SCCODICE = NVL(SCCODICE,space(40))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_3_joined
    link_1_3_joined=.f.
    local link_1_4_joined
    link_1_4_joined=.f.
    local link_1_5_joined
    link_1_5_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from SALDICOM where SCCODMAG=KeySet.SCCODMAG
    *                            and SCCODCAN=KeySet.SCCODCAN
    *                            and SCCODICE=KeySet.SCCODICE
    *
    i_nConn = i_TableProp[this.SALDICOM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDICOM_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('SALDICOM')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "SALDICOM.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' SALDICOM '
      link_1_3_joined=this.AddJoinedLink_1_3(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_4_joined=this.AddJoinedLink_1_4(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'SCCODMAG',this.w_SCCODMAG  ,'SCCODCAN',this.w_SCCODCAN  ,'SCCODICE',this.w_SCCODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = CTOD('01-01-1900')
        .w_DESMAG = space(30)
        .w_DESART = space(40)
        .w_DATOBSO = ctod("  /  /  ")
        .w_TIPART = space(2)
        .w_DESCAN = space(30)
        .w_FLCOMM = 'S'
        .w_TIPOPE = this.cFunction
        .w_SCCODMAG = NVL(SCCODMAG,space(5))
          if link_1_3_joined
            this.w_SCCODMAG = NVL(MGCODMAG103,NVL(this.w_SCCODMAG,space(5)))
            this.w_DESMAG = NVL(MGDESMAG103,space(30))
            this.w_DATOBSO = NVL(cp_ToDate(MGDTOBSO103),ctod("  /  /  "))
          else
          .link_1_3('Load')
          endif
        .w_SCCODART = NVL(SCCODART,space(20))
          if link_1_4_joined
            this.w_SCCODART = NVL(ARCODART104,NVL(this.w_SCCODART,space(20)))
            this.w_DESART = NVL(ARDESART104,space(40))
            this.w_TIPART = NVL(ARTIPART104,space(2))
            this.w_DATOBSO = NVL(cp_ToDate(ARDTOBSO104),ctod("  /  /  "))
            this.w_FLCOMM = NVL(ARSALCOM104,space(1))
          else
          .link_1_4('Load')
          endif
        .w_SCCODCAN = NVL(SCCODCAN,space(15))
          if link_1_5_joined
            this.w_SCCODCAN = NVL(CNCODCAN105,NVL(this.w_SCCODCAN,space(15)))
            this.w_DESCAN = NVL(CNDESCAN105,space(30))
          else
          .link_1_5('Load')
          endif
        .w_SCCODICE = NVL(SCCODICE,space(40))
        .w_SCQTAPER = NVL(SCQTAPER,0)
        .w_SCQTRPER = NVL(SCQTRPER,0)
        .w_DISPOPER = .w_SCQTAPER-.w_SCQTRPER
        .w_SCQTOPER = NVL(SCQTOPER,0)
        .w_SCQTIPER = NVL(SCQTIPER,0)
        .w_DISPCONT = .w_SCQTAPER+.w_SCQTOPER-.w_SCQTIPER-.w_SCQTRPER
        .w_SCQTAPRO = NVL(SCQTAPRO,0)
        .w_SCQTRPRO = NVL(SCQTRPRO,0)
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        cp_LoadRecExtFlds(this,'SALDICOM')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_TIPOPE = space(4)
      .w_OBTEST = ctod("  /  /  ")
      .w_SCCODMAG = space(5)
      .w_SCCODART = space(20)
      .w_SCCODCAN = space(15)
      .w_SCCODICE = space(40)
      .w_DESMAG = space(30)
      .w_DESART = space(40)
      .w_DATOBSO = ctod("  /  /  ")
      .w_TIPART = space(2)
      .w_DESCAN = space(30)
      .w_FLCOMM = space(1)
      .w_SCQTAPER = 0
      .w_SCQTRPER = 0
      .w_DISPOPER = 0
      .w_SCQTOPER = 0
      .w_SCQTIPER = 0
      .w_DISPCONT = 0
      .w_SCQTAPRO = 0
      .w_SCQTRPRO = 0
      if .cFunction<>"Filter"
        .w_TIPOPE = this.cFunction
        .w_OBTEST = CTOD('01-01-1900')
        .DoRTCalc(3,3,.f.)
          if not(empty(.w_SCCODMAG))
          .link_1_3('Full')
          endif
        .DoRTCalc(4,4,.f.)
          if not(empty(.w_SCCODART))
          .link_1_4('Full')
          endif
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_SCCODCAN))
          .link_1_5('Full')
          endif
        .w_SCCODICE = IIF(.w_TIPOPE='Load', .w_SCCODART, .w_SCCODICE)
          .DoRTCalc(7,11,.f.)
        .w_FLCOMM = 'S'
          .DoRTCalc(13,14,.f.)
        .w_DISPOPER = .w_SCQTAPER-.w_SCQTRPER
          .DoRTCalc(16,17,.f.)
        .w_DISPCONT = .w_SCQTAPER+.w_SCQTOPER-.w_SCQTIPER-.w_SCQTRPER
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'SALDICOM')
    this.DoRTCalc(19,20,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oSCCODMAG_1_3.enabled = i_bVal
      .Page1.oPag.oSCCODART_1_4.enabled = i_bVal
      .Page1.oPag.oSCCODCAN_1_5.enabled = i_bVal
      .Page1.oPag.oSCQTAPER_1_19.enabled = i_bVal
      .Page1.oPag.oSCQTRPER_1_20.enabled = i_bVal
      .Page1.oPag.oSCQTOPER_1_22.enabled = i_bVal
      .Page1.oPag.oSCQTIPER_1_23.enabled = i_bVal
      .Page1.oPag.oObj_1_36.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oSCCODMAG_1_3.enabled = .f.
        .Page1.oPag.oSCCODCAN_1_5.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oSCCODMAG_1_3.enabled = .t.
        .Page1.oPag.oSCCODART_1_4.enabled = .t.
        .Page1.oPag.oSCCODCAN_1_5.enabled = .t.
      endif
    endwith
    cp_SetEnabledExtFlds(this,'SALDICOM',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.SALDICOM_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCODMAG,"SCCODMAG",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCODART,"SCCODART",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCODCAN,"SCCODCAN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCCODICE,"SCCODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTAPER,"SCQTAPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTRPER,"SCQTRPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTOPER,"SCQTOPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTIPER,"SCQTIPER",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTAPRO,"SCQTAPRO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_SCQTRPRO,"SCQTRPRO",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.SALDICOM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDICOM_IDX,2])
    i_lTable = "SALDICOM"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.SALDICOM_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSMA_SCS with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.SALDICOM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALDICOM_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.SALDICOM_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into SALDICOM
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'SALDICOM')
        i_extval=cp_InsertValODBCExtFlds(this,'SALDICOM')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(SCCODMAG,SCCODART,SCCODCAN,SCCODICE,SCQTAPER"+;
                  ",SCQTRPER,SCQTOPER,SCQTIPER,SCQTAPRO,SCQTRPRO "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBCNull(this.w_SCCODMAG)+;
                  ","+cp_ToStrODBCNull(this.w_SCCODART)+;
                  ","+cp_ToStrODBCNull(this.w_SCCODCAN)+;
                  ","+cp_ToStrODBC(this.w_SCCODICE)+;
                  ","+cp_ToStrODBC(this.w_SCQTAPER)+;
                  ","+cp_ToStrODBC(this.w_SCQTRPER)+;
                  ","+cp_ToStrODBC(this.w_SCQTOPER)+;
                  ","+cp_ToStrODBC(this.w_SCQTIPER)+;
                  ","+cp_ToStrODBC(this.w_SCQTAPRO)+;
                  ","+cp_ToStrODBC(this.w_SCQTRPRO)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'SALDICOM')
        i_extval=cp_InsertValVFPExtFlds(this,'SALDICOM')
        cp_CheckDeletedKey(i_cTable,0,'SCCODMAG',this.w_SCCODMAG,'SCCODCAN',this.w_SCCODCAN,'SCCODICE',this.w_SCCODICE)
        INSERT INTO (i_cTable);
              (SCCODMAG,SCCODART,SCCODCAN,SCCODICE,SCQTAPER,SCQTRPER,SCQTOPER,SCQTIPER,SCQTAPRO,SCQTRPRO  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_SCCODMAG;
                  ,this.w_SCCODART;
                  ,this.w_SCCODCAN;
                  ,this.w_SCCODICE;
                  ,this.w_SCQTAPER;
                  ,this.w_SCQTRPER;
                  ,this.w_SCQTOPER;
                  ,this.w_SCQTIPER;
                  ,this.w_SCQTAPRO;
                  ,this.w_SCQTRPRO;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.SALDICOM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALDICOM_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.SALDICOM_IDX,i_nConn)
      *
      * update SALDICOM
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'SALDICOM')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " SCCODART="+cp_ToStrODBCNull(this.w_SCCODART)+;
             ",SCQTAPER="+cp_ToStrODBC(this.w_SCQTAPER)+;
             ",SCQTRPER="+cp_ToStrODBC(this.w_SCQTRPER)+;
             ",SCQTOPER="+cp_ToStrODBC(this.w_SCQTOPER)+;
             ",SCQTIPER="+cp_ToStrODBC(this.w_SCQTIPER)+;
             ",SCQTAPRO="+cp_ToStrODBC(this.w_SCQTAPRO)+;
             ",SCQTRPRO="+cp_ToStrODBC(this.w_SCQTRPRO)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'SALDICOM')
        i_cWhere = cp_PKFox(i_cTable  ,'SCCODMAG',this.w_SCCODMAG  ,'SCCODCAN',this.w_SCCODCAN  ,'SCCODICE',this.w_SCCODICE  )
        UPDATE (i_cTable) SET;
              SCCODART=this.w_SCCODART;
             ,SCQTAPER=this.w_SCQTAPER;
             ,SCQTRPER=this.w_SCQTRPER;
             ,SCQTOPER=this.w_SCQTOPER;
             ,SCQTIPER=this.w_SCQTIPER;
             ,SCQTAPRO=this.w_SCQTAPRO;
             ,SCQTRPRO=this.w_SCQTRPRO;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    if not(bTrsErr)
      i_nConn = i_TableProp[this.SALDICOM_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.SALDICOM_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.SALDICOM_IDX,i_nConn)
      *
      * delete SALDICOM
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'SCCODMAG',this.w_SCCODMAG  ,'SCCODCAN',this.w_SCCODCAN  ,'SCCODICE',this.w_SCCODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.SALDICOM_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.SALDICOM_IDX,2])
    if i_bUpd
      with this
            .w_TIPOPE = this.cFunction
        .DoRTCalc(2,5,.t.)
            .w_SCCODICE = IIF(.w_TIPOPE='Load', .w_SCCODART, .w_SCCODICE)
        .DoRTCalc(7,14,.t.)
            .w_DISPOPER = .w_SCQTAPER-.w_SCQTRPER
        .DoRTCalc(16,17,.t.)
            .w_DISPCONT = .w_SCQTAPER+.w_SCQTOPER-.w_SCQTIPER-.w_SCQTRPER
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(19,20,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_36.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oSCCODART_1_4.enabled = this.oPgFrm.Page1.oPag.oSCCODART_1_4.mCond()
    this.oPgFrm.Page1.oPag.oSCCODCAN_1_5.enabled = this.oPgFrm.Page1.oPag.oSCCODCAN_1_5.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_36.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=SCCODMAG
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MAGAZZIN_IDX,3]
    i_lTable = "MAGAZZIN"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2], .t., this.MAGAZZIN_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODMAG) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AMA',True,'MAGAZZIN')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MGCODMAG like "+cp_ToStrODBC(trim(this.w_SCCODMAG)+"%");

          i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MGCODMAG","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MGCODMAG',trim(this.w_SCCODMAG))
          select MGCODMAG,MGDESMAG,MGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MGCODMAG into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODMAG)==trim(_Link_.MGCODMAG) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCODMAG) and !this.bDontReportError
            deferred_cp_zoom('MAGAZZIN','*','MGCODMAG',cp_AbsName(oSource.parent,'oSCCODMAG_1_3'),i_cWhere,'GSAR_AMA',"Magazzini",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',oSource.xKey(1))
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODMAG)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MGCODMAG,MGDESMAG,MGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where MGCODMAG="+cp_ToStrODBC(this.w_SCCODMAG);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MGCODMAG',this.w_SCCODMAG)
            select MGCODMAG,MGDESMAG,MGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODMAG = NVL(_Link_.MGCODMAG,space(5))
      this.w_DESMAG = NVL(_Link_.MGDESMAG,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.MGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODMAG = space(5)
      endif
      this.w_DESMAG = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])+'\'+cp_ToStr(_Link_.MGCODMAG,1)
      cp_ShowWarn(i_cKey,this.MAGAZZIN_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODMAG Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_3(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 3 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.MAGAZZIN_IDX,3] and i_nFlds+3<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.MAGAZZIN_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_3.MGCODMAG as MGCODMAG103"+ ",link_1_3.MGDESMAG as MGDESMAG103"+ ",link_1_3.MGDTOBSO as MGDTOBSO103"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_3 on SALDICOM.SCCODMAG=link_1_3.MGCODMAG"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_3"
          i_cKey=i_cKey+'+" and SALDICOM.SCCODMAG=link_1_3.MGCODMAG(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+3
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SCCODART
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODART) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_AAR',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_SCCODART)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARSALCOM";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_SCCODART))
          select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARSALCOM;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODART)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCODART) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oSCCODART_1_4'),i_cWhere,'GSMA_AAR',"Anagrafica articoli",'GSMA_ACS.ART_ICOL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARSALCOM";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARSALCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODART)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARSALCOM";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_SCCODART);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_SCCODART)
            select ARCODART,ARDESART,ARTIPART,ARDTOBSO,ARSALCOM;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODART = NVL(_Link_.ARCODART,space(20))
      this.w_DESART = NVL(_Link_.ARDESART,space(40))
      this.w_TIPART = NVL(_Link_.ARTIPART,space(2))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
      this.w_FLCOMM = NVL(_Link_.ARSALCOM,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODART = space(20)
      endif
      this.w_DESART = space(40)
      this.w_TIPART = space(2)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_FLCOMM = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(.w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM-FS') and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and .w_FLCOMM='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice articolo incongruente oppure obsoleto")
        endif
        this.w_SCCODART = space(20)
        this.w_DESART = space(40)
        this.w_TIPART = space(2)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_FLCOMM = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODART Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_4(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 5 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.ART_ICOL_IDX,3] and i_nFlds+5<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_4.ARCODART as ARCODART104"+ ",link_1_4.ARDESART as ARDESART104"+ ",link_1_4.ARTIPART as ARTIPART104"+ ",link_1_4.ARDTOBSO as ARDTOBSO104"+ ",link_1_4.ARSALCOM as ARSALCOM104"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_4 on SALDICOM.SCCODART=link_1_4.ARCODART"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_4"
          i_cKey=i_cKey+'+" and SALDICOM.SCCODART=link_1_4.ARCODART(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+5
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=SCCODCAN
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_SCCODCAN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ACN',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_SCCODCAN)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_SCCODCAN))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_SCCODCAN)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_SCCODCAN) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oSCCODCAN_1_5'),i_cWhere,'GSAR_ACN',"Commesse",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_SCCODCAN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_SCCODCAN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_SCCODCAN)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_SCCODCAN = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESCAN = NVL(_Link_.CNDESCAN,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_SCCODCAN = space(15)
      endif
      this.w_DESCAN = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_SCCODCAN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.CAN_TIER_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.CNCODCAN as CNCODCAN105"+ ",link_1_5.CNDESCAN as CNDESCAN105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on SALDICOM.SCCODCAN=link_1_5.CNCODCAN"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and SALDICOM.SCCODCAN=link_1_5.CNCODCAN(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSCCODMAG_1_3.value==this.w_SCCODMAG)
      this.oPgFrm.Page1.oPag.oSCCODMAG_1_3.value=this.w_SCCODMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCODART_1_4.value==this.w_SCCODART)
      this.oPgFrm.Page1.oPag.oSCCODART_1_4.value=this.w_SCCODART
    endif
    if not(this.oPgFrm.Page1.oPag.oSCCODCAN_1_5.value==this.w_SCCODCAN)
      this.oPgFrm.Page1.oPag.oSCCODCAN_1_5.value=this.w_SCCODCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMAG_1_10.value==this.w_DESMAG)
      this.oPgFrm.Page1.oPag.oDESMAG_1_10.value=this.w_DESMAG
    endif
    if not(this.oPgFrm.Page1.oPag.oDESART_1_11.value==this.w_DESART)
      this.oPgFrm.Page1.oPag.oDESART_1_11.value=this.w_DESART
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCAN_1_14.value==this.w_DESCAN)
      this.oPgFrm.Page1.oPag.oDESCAN_1_14.value=this.w_DESCAN
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTAPER_1_19.value==this.w_SCQTAPER)
      this.oPgFrm.Page1.oPag.oSCQTAPER_1_19.value=this.w_SCQTAPER
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTRPER_1_20.value==this.w_SCQTRPER)
      this.oPgFrm.Page1.oPag.oSCQTRPER_1_20.value=this.w_SCQTRPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDISPOPER_1_21.value==this.w_DISPOPER)
      this.oPgFrm.Page1.oPag.oDISPOPER_1_21.value=this.w_DISPOPER
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTOPER_1_22.value==this.w_SCQTOPER)
      this.oPgFrm.Page1.oPag.oSCQTOPER_1_22.value=this.w_SCQTOPER
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTIPER_1_23.value==this.w_SCQTIPER)
      this.oPgFrm.Page1.oPag.oSCQTIPER_1_23.value=this.w_SCQTIPER
    endif
    if not(this.oPgFrm.Page1.oPag.oDISPCONT_1_24.value==this.w_DISPCONT)
      this.oPgFrm.Page1.oPag.oDISPCONT_1_24.value=this.w_DISPCONT
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTAPRO_1_31.value==this.w_SCQTAPRO)
      this.oPgFrm.Page1.oPag.oSCQTAPRO_1_31.value=this.w_SCQTAPRO
    endif
    if not(this.oPgFrm.Page1.oPag.oSCQTRPRO_1_32.value==this.w_SCQTRPRO)
      this.oPgFrm.Page1.oPag.oSCQTRPRO_1_32.value=this.w_SCQTRPRO
    endif
    cp_SetControlsValueExtFlds(this,'SALDICOM')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_SCCODMAG))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCCODMAG_1_3.SetFocus()
            i_bnoObbl = !empty(.w_SCCODMAG)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_SCCODART)) or not((.w_TIPART $ 'PF-SE-MP-PH-MC-MA-IM-FS') and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST) and .w_FLCOMM='S'))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCCODART_1_4.SetFocus()
            i_bnoObbl = !empty(.w_SCCODART)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice articolo incongruente oppure obsoleto")
          case   (empty(.w_SCCODCAN))  and (.cFunction<>'Edit')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCCODCAN_1_5.SetFocus()
            i_bnoObbl = !empty(.w_SCCODCAN)
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

enddefine

* --- Define pages as container
define class tgsma_acsPag1 as StdContainer
  Width  = 561
  height = 278
  stdWidth  = 561
  stdheight = 278
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSCCODMAG_1_3 as StdField with uid="DFWWAZEUMQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SCCODMAG", cQueryName = "SCCODMAG",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice magazzino",;
    HelpContextID = 83296915,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=77, Top=12, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="MAGAZZIN", cZoomOnZoom="GSAR_AMA", oKey_1_1="MGCODMAG", oKey_1_2="this.w_SCCODMAG"

  proc oSCCODMAG_1_3.mAfter
    with this.Parent.oContained
      This.cQueryName='SCCODMAG'
    endwith
  endproc

  func oSCCODMAG_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODMAG_1_3.ecpDrop(oSource)
    this.Parent.oContained.link_1_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCODMAG_1_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MAGAZZIN','*','MGCODMAG',cp_AbsName(this.parent,'oSCCODMAG_1_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AMA',"Magazzini",'',this.parent.oContained
  endproc
  proc oSCCODMAG_1_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AMA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_MGCODMAG=this.parent.oContained.w_SCCODMAG
     i_obj.ecpSave()
  endproc

  add object oSCCODART_1_4 as StdField with uid="ZKYRAQYGZP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SCCODART", cQueryName = "SCCODART",;
    bObbl = .t. , nPag = 1, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice articolo incongruente oppure obsoleto",;
    ToolTipText = "Codice articolo",;
    HelpContextID = 16188038,;
   bGlobalFont=.t.,;
    Height=21, Width=167, Left=77, Top=37, cSayPict="p_ART", cGetPict="p_ART", InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_AAR", oKey_1_1="ARCODART", oKey_1_2="this.w_SCCODART"

  func oSCCODART_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oSCCODART_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODART_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCODART_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oSCCODART_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_AAR',"Anagrafica articoli",'GSMA_ACS.ART_ICOL_VZM',this.parent.oContained
  endproc
  proc oSCCODART_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSMA_AAR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_SCCODART
     i_obj.ecpSave()
  endproc

  add object oSCCODCAN_1_5 as StdField with uid="SSGBERVKJA",rtseq=5,rtrep=.f.,;
    cFormVar = "w_SCCODCAN", cQueryName = "SCCODMAG,SCCODCAN",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice commessa",;
    HelpContextID = 251069068,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=77, Top=62, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="GSAR_ACN", oKey_1_1="CNCODCAN", oKey_1_2="this.w_SCCODCAN"

  func oSCCODCAN_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.cFunction<>'Edit')
    endwith
   endif
  endfunc

  func oSCCODCAN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oSCCODCAN_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oSCCODCAN_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oSCCODCAN_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ACN',"Commesse",'',this.parent.oContained
  endproc
  proc oSCCODCAN_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ACN()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_SCCODCAN
     i_obj.ecpSave()
  endproc

  add object oDESMAG_1_10 as StdField with uid="VTWFBBMJMA",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESMAG", cQueryName = "DESMAG",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 187171274,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=140, Top=12, InputMask=replicate('X',30)

  add object oDESART_1_11 as StdField with uid="PPOWDSBJGJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_DESART", cQueryName = "DESART",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 220463562,;
   bGlobalFont=.t.,;
    Height=21, Width=307, Left=245, Top=37, InputMask=replicate('X',40)

  add object oDESCAN_1_14 as StdField with uid="BEBTOHVSHC",rtseq=11,rtrep=.f.,;
    cFormVar = "w_DESCAN", cQueryName = "DESCAN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 70386122,;
   bGlobalFont=.t.,;
    Height=21, Width=244, Left=210, Top=62, InputMask=replicate('X',30)

  add object oSCQTAPER_1_19 as StdField with uid="HBHFIZDNUI",rtseq=13,rtrep=.f.,;
    cFormVar = "w_SCQTAPER", cQueryName = "SCQTAPER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Esistenza",;
    HelpContextID = 232709496,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=150, Top=123, cSayPict="v_PQ(15)", cGetPict="v_GQ(15)"

  add object oSCQTRPER_1_20 as StdField with uid="WLUPBGAOVV",rtseq=14,rtrep=.f.,;
    cFormVar = "w_SCQTRPER", cQueryName = "SCQTRPER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantitą riservata",;
    HelpContextID = 250535288,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=150, Top=147, cSayPict="v_PQ(15)", cGetPict="v_GQ(15)"

  add object oDISPOPER_1_21 as StdField with uid="UZFQQDECWD",rtseq=15,rtrep=.f.,;
    cFormVar = "w_DISPOPER", cQueryName = "DISPOPER",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Differenza fra l'esistenza e la quantitą riservata",;
    HelpContextID = 247136904,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=150, Top=171, cSayPict="v_PQ(17)", cGetPict="v_GQ(17)"

  add object oSCQTOPER_1_22 as StdField with uid="KKUBXCTPRP",rtseq=16,rtrep=.f.,;
    cFormVar = "w_SCQTOPER", cQueryName = "SCQTOPER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantitą ordinata ai fornitori",;
    HelpContextID = 247389560,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=150, Top=195, cSayPict="v_PQ(15)", cGetPict="v_GQ(15)"

  add object oSCQTIPER_1_23 as StdField with uid="IYQHGLOZZL",rtseq=17,rtrep=.f.,;
    cFormVar = "w_SCQTIPER", cQueryName = "SCQTIPER",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantitą impegnata ai clienti",;
    HelpContextID = 241098104,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=150, Top=219, cSayPict="v_PQ(15)", cGetPict="v_GQ(15)"

  add object oDISPCONT_1_24 as StdField with uid="LDVFOBLDJM",rtseq=18,rtrep=.f.,;
    cFormVar = "w_DISPCONT", cQueryName = "DISPCONT",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Disponibilitą comprensiva dell'ordinato e dell'impegnato",;
    HelpContextID = 50658678,;
   bGlobalFont=.t.,;
    Height=21, Width=114, Left=150, Top=251, cSayPict="v_PQ(17)", cGetPict="v_GQ(17)"

  add object oSCQTAPRO_1_31 as StdField with uid="LVHUPBAFPS",rtseq=19,rtrep=.f.,;
    cFormVar = "w_SCQTAPRO", cQueryName = "SCQTAPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Esistenza movimenti fuori linea",;
    HelpContextID = 232709493,;
   bGlobalFont=.t.,;
    Height=21, Width=113, Left=381, Top=123, cSayPict="v_PQ(15)", cGetPict="v_GQ(15)"

  add object oSCQTRPRO_1_32 as StdField with uid="JEJLUSICFE",rtseq=20,rtrep=.f.,;
    cFormVar = "w_SCQTRPRO", cQueryName = "SCQTRPRO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Quantitą riservata fuori lnea",;
    HelpContextID = 250535285,;
   bGlobalFont=.t.,;
    Height=21, Width=113, Left=381, Top=147, cSayPict="v_PQ(15)", cGetPict="v_GQ(15)"


  add object oObj_1_36 as cp_runprogram with uid="UJOBPIBCFS",left=1, top=300, width=169,height=19,;
    caption='GSMA_BMS',;
   bGlobalFont=.t.,;
    prg="GSMA_BMS('COM')",;
    cEvent = "Delete start",;
    nPag=1;
    , HelpContextID = 240407367

  add object oStr_1_7 as StdString with uid="ZYBQELVOXG",Visible=.t., Left=23, Top=38,;
    Alignment=1, Width=47, Height=15,;
    Caption="Articolo:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_8 as StdString with uid="GGRLWGCGFS",Visible=.t., Left=7, Top=13,;
    Alignment=1, Width=63, Height=15,;
    Caption="Magazzino:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_9 as StdString with uid="VSFYAUAIGM",Visible=.t., Left=2, Top=63,;
    Alignment=1, Width=68, Height=15,;
    Caption="Commessa:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_16 as StdString with uid="EWZCWQUAAA",Visible=.t., Left=8, Top=91,;
    Alignment=0, Width=88, Height=18,;
    Caption="Saldi in linea"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="KIMQMNUBME",Visible=.t., Left=316, Top=91,;
    Alignment=0, Width=117, Height=18,;
    Caption="Saldi fuori linea"  ;
  , bGlobalFont=.t.

  add object oStr_1_25 as StdString with uid="MKHWSLYRMX",Visible=.t., Left=31, Top=148,;
    Alignment=1, Width=113, Height=15,;
    Caption="Quantitą riservata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="PKVTUNPKFC",Visible=.t., Left=83, Top=124,;
    Alignment=1, Width=61, Height=15,;
    Caption="Esistenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="ZJRMHMVQWL",Visible=.t., Left=20, Top=196,;
    Alignment=1, Width=124, Height=15,;
    Caption="Quantitą ordinata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="ITEVOMMUAY",Visible=.t., Left=31, Top=220,;
    Alignment=1, Width=113, Height=15,;
    Caption="Quantitą impegnata:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="NQAIUZEGIG",Visible=.t., Left=9, Top=252,;
    Alignment=1, Width=135, Height=15,;
    Caption="Disponibilitą contabile:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="YRDDOINMWV",Visible=.t., Left=59, Top=172,;
    Alignment=1, Width=85, Height=15,;
    Caption="Disponibilitą:"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="ZFUAPVOXAT",Visible=.t., Left=298, Top=148,;
    Alignment=1, Width=79, Height=18,;
    Caption="di cui:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="YLIFYMXWWI",Visible=.t., Left=298, Top=124,;
    Alignment=1, Width=79, Height=18,;
    Caption="di cui:"  ;
  , bGlobalFont=.t.

  add object oBox_1_17 as StdBox with uid="YJGBELFMFO",left=0, top=111, width=553,height=1

  add object oBox_1_35 as StdBox with uid="JLPSBDHUET",left=15, top=246, width=247,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_acs','SALDICOM','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".SCCODMAG=SALDICOM.SCCODMAG";
  +" and "+i_cAliasName2+".SCCODCAN=SALDICOM.SCCODCAN";
  +" and "+i_cAliasName2+".SCCODICE=SALDICOM.SCCODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
