* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bd3                                                        *
*              Lancia maschera evasione                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_23]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-11-13                                                      *
* Last revis.: 2003-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pEvade
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bd3",oParentObject,m.pEvade)
return(i_retval)

define class tgsve_bd3 as StdBatch
  * --- Local variables
  pEvade = space(1)
  w_EVADE = space(1)
  w_SERIAL = space(10)
  w_ROWNUM = 0
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch viene lanciato dai bottoni 'Evasioni' e 'RigheEvase' dai dati di riga dei documenti (Vendita, Acquisto, Ordini)
    this.w_SERIAL = this.oParentObject.w_SERIAL1
    this.w_ROWNUM = this.oParentObject.w_ROWNUM1
    this.w_EVADE = this.pEvade
    do GSVE_KDE with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
  endproc


  proc Init(oParentObject,pEvade)
    this.pEvade=pEvade
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pEvade"
endproc
