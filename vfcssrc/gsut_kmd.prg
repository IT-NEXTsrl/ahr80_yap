* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_kmd                                                        *
*              Nuovo documento                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-07-04                                                      *
* Last revis.: 2014-09-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_kmd",oParentObject))

* --- Class definition
define class tgsut_kmd as StdForm
  Top    = 10
  Left   = 10

  * --- Standard Properties
  Width  = 734
  Height = 315
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-09-05"
  HelpContextID=199836521
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=43

  * --- Constant Properties
  _IDX = 0
  PROMODEL_IDX = 0
  PROMCLAS_IDX = 0
  TIP_ALLE_IDX = 0
  CLA_ALLE_IDX = 0
  CAN_TIER_IDX = 0
  PAR_ALTE_IDX = 0
  cPrg = "gsut_kmd"
  cComment = "Nuovo documento"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_ARCHIVIO = space(20)
  w_Chiave = space(50)
  w_COD_AZI = space(5)
  o_COD_AZI = space(5)
  w_CLAMOD = space(15)
  o_CLAMOD = space(15)
  w_CDPUBWEB = space(1)
  w_CLASSEDOC = space(15)
  w_PATHARCLAS = space(250)
  w_CODPRAT = space(10)
  w_PATHARPRAT = space(250)
  w_CODMOD = space(10)
  o_CODMOD = space(10)
  w_TESTO = space(0)
  w_TIPOALLE = space(5)
  o_TIPOALLE = space(5)
  w_COMTIP = space(1)
  o_COMTIP = space(1)
  w_CLASSEALL = space(5)
  o_CLASSEALL = space(5)
  w_PATMOD = space(254)
  o_PATMOD = space(254)
  w_COMCLA = space(1)
  o_COMCLA = space(1)
  w_FILEORIG = space(80)
  o_FILEORIG = space(80)
  w_OGGETTO = space(200)
  w_PATARC = space(250)
  w_CLASSE = space(10)
  w_DESMOD = space(60)
  w_QUEMOD = space(60)
  w_Programma = space(10)
  w_DESCLAS = space(40)
  w_RIFTAB = space(20)
  w_TIPARC = space(1)
  w_TIPOCLA = space(1)
  w_DESTIPAL = space(40)
  w_DESCLAL = space(40)
  w_PATHTIPO = space(150)
  w_PATHCLAS = space(150)
  w_PATHSELE = space(254)
  w_QUERY = space(60)
  w_CODPRE = space(20)
  w_RAGPRE = space(10)
  w_HIDEOPENF = space(1)
  w_CodEnt = space(10)
  w_CodTip = space(10)
  w_CodMat = space(10)
  w_DtObsoModel = ctod('  /  /  ')
  w_MODTOBSO = ctod('  /  /  ')
  w_MODTINVA = ctod('  /  /  ')
  w_CDTIPRAG = space(1)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_kmdPag1","gsut_kmd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCLAMOD_1_4
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='PROMODEL'
    this.cWorkTables[2]='PROMCLAS'
    this.cWorkTables[3]='TIP_ALLE'
    this.cWorkTables[4]='CLA_ALLE'
    this.cWorkTables[5]='CAN_TIER'
    this.cWorkTables[6]='PAR_ALTE'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_ARCHIVIO=space(20)
      .w_Chiave=space(50)
      .w_COD_AZI=space(5)
      .w_CLAMOD=space(15)
      .w_CDPUBWEB=space(1)
      .w_CLASSEDOC=space(15)
      .w_PATHARCLAS=space(250)
      .w_CODPRAT=space(10)
      .w_PATHARPRAT=space(250)
      .w_CODMOD=space(10)
      .w_TESTO=space(0)
      .w_TIPOALLE=space(5)
      .w_COMTIP=space(1)
      .w_CLASSEALL=space(5)
      .w_PATMOD=space(254)
      .w_COMCLA=space(1)
      .w_FILEORIG=space(80)
      .w_OGGETTO=space(200)
      .w_PATARC=space(250)
      .w_CLASSE=space(10)
      .w_DESMOD=space(60)
      .w_QUEMOD=space(60)
      .w_Programma=space(10)
      .w_DESCLAS=space(40)
      .w_RIFTAB=space(20)
      .w_TIPARC=space(1)
      .w_TIPOCLA=space(1)
      .w_DESTIPAL=space(40)
      .w_DESCLAL=space(40)
      .w_PATHTIPO=space(150)
      .w_PATHCLAS=space(150)
      .w_PATHSELE=space(254)
      .w_QUERY=space(60)
      .w_CODPRE=space(20)
      .w_RAGPRE=space(10)
      .w_HIDEOPENF=space(1)
      .w_CodEnt=space(10)
      .w_CodTip=space(10)
      .w_CodMat=space(10)
      .w_DtObsoModel=ctod("  /  /  ")
      .w_MODTOBSO=ctod("  /  /  ")
      .w_MODTINVA=ctod("  /  /  ")
      .w_CDTIPRAG=space(1)
      .w_CLAMOD=oParentObject.w_CLAMOD
      .w_CDPUBWEB=oParentObject.w_CDPUBWEB
      .w_CODMOD=oParentObject.w_CODMOD
      .w_TESTO=oParentObject.w_TESTO
      .w_TIPOALLE=oParentObject.w_TIPOALLE
      .w_CLASSEALL=oParentObject.w_CLASSEALL
      .w_PATMOD=oParentObject.w_PATMOD
      .w_FILEORIG=oParentObject.w_FILEORIG
      .w_OGGETTO=oParentObject.w_OGGETTO
      .w_PATARC=oParentObject.w_PATARC
      .w_QUEMOD=oParentObject.w_QUEMOD
      .w_CODPRE=oParentObject.w_CODPRE
      .w_RAGPRE=oParentObject.w_RAGPRE
        .w_ARCHIVIO = IIF(TYPE('this.oParentObject.w_ARCHIVIO')='C',this.oParentObject.w_ARCHIVIO,' ')
        .w_Chiave = IIF(TYPE('this.oParentObject.w_CHIAVE')='C',this.oParentObject.w_CHIAVE,' ')
        .w_COD_AZI = i_codazi
        .DoRTCalc(3,3,.f.)
        if not(empty(.w_COD_AZI))
          .link_1_3('Full')
        endif
        .w_CLAMOD = CLASPREF(.w_ARCHIVIO)
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CLAMOD))
          .link_1_4('Full')
        endif
        .w_CDPUBWEB = 'N'
          .DoRTCalc(6,7,.f.)
        .w_CODPRAT = IIF(.w_ARCHIVIO='CAN_TIER',.w_CHIAVE,'')
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODPRAT))
          .link_1_8('Full')
        endif
        .DoRTCalc(9,10,.f.)
        if not(empty(.w_CODMOD))
          .link_1_10('Full')
        endif
        .DoRTCalc(11,12,.f.)
        if not(empty(.w_TIPOALLE))
          .link_1_12('Full')
        endif
        .w_COMTIP = iif(empty(.w_TIPOALLE),'N',.w_COMTIP)
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CLASSEALL))
          .link_1_14('Full')
        endif
          .DoRTCalc(15,15,.f.)
        .w_COMCLA = iif(empty(.w_CLASSEALL),'N',.w_COMCLA)
          .DoRTCalc(17,18,.f.)
        .w_PATARC = iif(empty(nvl(.w_PATHARPRAT,' ')),IIF(Not Empty(.w_PATHARCLAS) ,.w_PATHARCLAS,ADDBS(ALLTRIM(Justpath(.w_PATMOD)))),.w_PATHARPRAT)
          .DoRTCalc(20,22,.f.)
        .w_Programma = IIF(TYPE('this.oParentObject.w_programma')='C',this.oParentObject.w_programma,' ')
          .DoRTCalc(24,36,.f.)
        .w_CodEnt = IIF(IsAlt() AND TYPE('this.oParentObject.w_CN__ENTE')='C',this.oParentObject.w_CN__ENTE,' ')
        .w_CodTip = IIF(IsAlt() AND TYPE('this.oParentObject.w_CNTIPPRA')='C',this.oParentObject.w_CNTIPPRA,' ')
        .w_CodMat = IIF(IsAlt() AND TYPE('this.oParentObject.w_CNMATPRA')='C',this.oParentObject.w_CNMATPRA,' ')
        .w_DtObsoModel = i_DatSys
    endwith
    this.DoRTCalc(41,43,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
      .oParentObject.w_CLAMOD=.w_CLAMOD
      .oParentObject.w_CDPUBWEB=.w_CDPUBWEB
      .oParentObject.w_CODMOD=.w_CODMOD
      .oParentObject.w_TESTO=.w_TESTO
      .oParentObject.w_TIPOALLE=.w_TIPOALLE
      .oParentObject.w_CLASSEALL=.w_CLASSEALL
      .oParentObject.w_PATMOD=.w_PATMOD
      .oParentObject.w_FILEORIG=.w_FILEORIG
      .oParentObject.w_OGGETTO=.w_OGGETTO
      .oParentObject.w_PATARC=.w_PATARC
      .oParentObject.w_QUEMOD=.w_QUEMOD
      .oParentObject.w_CODPRE=.w_CODPRE
      .oParentObject.w_RAGPRE=.w_RAGPRE
      .oParentObject.SetControlsValue()
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,2,.t.)
        if .o_COD_AZI<>.w_COD_AZI
            .w_COD_AZI = i_codazi
          .link_1_3('Full')
        endif
        .DoRTCalc(4,7,.t.)
            .w_CODPRAT = IIF(.w_ARCHIVIO='CAN_TIER',.w_CHIAVE,'')
          .link_1_8('Full')
        .DoRTCalc(9,9,.t.)
        if .o_CLAMOD<>.w_CLAMOD
          .link_1_10('Full')
        endif
        .DoRTCalc(11,11,.t.)
        if .o_CLAMOD<>.w_CLAMOD
            .w_TIPOALLE = .w_TIPOALLE
          .link_1_12('Full')
        endif
        if .o_TIPOALLE<>.w_TIPOALLE
            .w_COMTIP = iif(empty(.w_TIPOALLE),'N',.w_COMTIP)
        endif
        if .o_CLAMOD<>.w_CLAMOD
            .w_CLASSEALL = .w_CLASSEALL
          .link_1_14('Full')
        endif
        .DoRTCalc(15,15,.t.)
        if .o_CLASSEALL<>.w_CLASSEALL
            .w_COMCLA = iif(empty(.w_CLASSEALL),'N',.w_COMCLA)
        endif
        if .o_PATMOD<>.w_PATMOD.or. .o_CODMOD<>.w_CODMOD
            .w_FILEORIG = JustfName(.w_PATMOD)
        endif
        if .o_CLAMOD<>.w_CLAMOD
          .Calculate_ARFBRFSYKU()
        endif
        if .o_COMTIP<>.w_COMTIP
          .Calculate_WUUZYEHIZP()
        endif
        if .o_COMCLA<>.w_COMCLA
          .Calculate_NGJDPFYZQR()
        endif
        if .o_FILEORIG<>.w_FILEORIG
          .Calculate_KQMOANSWML()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(18,43,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_ARFBRFSYKU()
    with this
          * --- Modello
          .w_CODMOD = MODPREF(.w_CLAMOD)
          .link_1_10('Full')
          .w_FILEORIG = JUSTFNAME(.w_PATMOD)
          .w_PATARC = iif(empty(nvl(.w_PATHARPRAT,' ')),IIF(Not Empty(.w_PATHARCLAS) ,.w_PATHARCLAS,ADDBS(ALLTRIM(Justpath(.w_PATMOD)))),.w_PATHARPRAT)
    endwith
  endproc
  proc Calculate_WUUZYEHIZP()
    with this
          * --- Aggirono il percorso di archiviazione se attivato il check completa path per tipologia allegato
          .w_PATARC = iif(Isalt(),iif(empty(nvl(.w_PATHARPRAT,' ')),IIF(Not Empty(.w_PATHARCLAS) ,.w_PATHARCLAS,ADDBS(ALLTRIM(Justpath(.w_PATMOD)))),.w_PATHARPRAT),IIF(.w_COMTIP='S' AND ! EMPTY(.w_PATHTIPO),addbs(alltrim(.w_PATARC))+ADDBS(ALLTRIM(.w_PATHTIPO)),STRTRAN(.w_PATARC,ADDBS(ALLTRIM(.w_PATHTIPO)),'')))
    endwith
  endproc
  proc Calculate_NGJDPFYZQR()
    with this
          * --- Aggirono il percorso di archiviazione se attivato il check completa path per classe allegato
          .w_PATARC = IIF(Isalt(),iif(empty(nvl(.w_PATHARPRAT,' ')),IIF(Not Empty(.w_PATHARCLAS) ,.w_PATHARCLAS,ADDBS(ALLTRIM(Justpath(.w_PATMOD)))),.w_PATHARPRAT),IIF(.w_COMCLA='S' AND ! EMPTY(.w_PATHCLAS),addbs(alltrim(.w_PATARC))+ADDBS(ALLTRIM(.w_PATHCLAS)),STRTRAN(.w_PATARC,ADDBS(ALLTRIM(.w_PATHCLAS)),'')))
          .w_PATARC = iif(Isalt(),DCMPATAR(.w_PATARC,.w_CDTIPRAG,i_datsys,.w_CLAMOD,.w_TIPOALLE,.w_CLASSEALL,.w_FILEORIG,.f.,.w_COMTIP,.w_COMCLA,.t.,.f.),.w_PATARC)
    endwith
  endproc
  proc Calculate_KQMOANSWML()
    with this
          * --- Gestione estensione su nome file originario
          .w_FILEORIG = iif(! empty(.w_FILEORIG) and empty(JustExt(.w_FILEORIG)),JustStem(.w_FILEORIG)+'.'+JustExt(.w_PATMOD),.w_FILEORIG)
    endwith
  endproc
  proc Calculate_RLXSMAJPQK()
    with this
          * --- Seleziona path
          .w_PATHSELE = left(cp_getdir(IIF(EMPTY(.w_PATARC),sys(5)+sys(2003),.w_PATARC),"Percorso di archiviazione")+space(200),200)
          .w_PATARC = IIF(!EMPTY(.w_PATHSELE), .w_PATHSELE, .w_PATARC)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCDPUBWEB_1_5.enabled = this.oPgFrm.Page1.oPag.oCDPUBWEB_1_5.mCond()
    this.oPgFrm.Page1.oPag.oTIPOALLE_1_12.enabled = this.oPgFrm.Page1.oPag.oTIPOALLE_1_12.mCond()
    this.oPgFrm.Page1.oPag.oCOMTIP_1_13.enabled = this.oPgFrm.Page1.oPag.oCOMTIP_1_13.mCond()
    this.oPgFrm.Page1.oPag.oCLASSEALL_1_14.enabled = this.oPgFrm.Page1.oPag.oCLASSEALL_1_14.mCond()
    this.oPgFrm.Page1.oPag.oCOMCLA_1_16.enabled = this.oPgFrm.Page1.oPag.oCOMCLA_1_16.mCond()
    this.oPgFrm.Page1.oPag.oFILEORIG_1_17.enabled = this.oPgFrm.Page1.oPag.oFILEORIG_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_29.enabled = this.oPgFrm.Page1.oPag.oBtn_1_29.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCDPUBWEB_1_5.visible=!this.oPgFrm.Page1.oPag.oCDPUBWEB_1_5.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_29.visible=!this.oPgFrm.Page1.oPag.oBtn_1_29.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Blank")
          .Calculate_ARFBRFSYKU()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_WUUZYEHIZP()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("Blank")
          .Calculate_NGJDPFYZQR()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("SelDir")
          .Calculate_RLXSMAJPQK()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=COD_AZI
  func Link_1_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PAR_ALTE_IDX,3]
    i_lTable = "PAR_ALTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2], .t., this.PAR_ALTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COD_AZI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COD_AZI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PACODAZI,PAHIDOPE";
                   +" from "+i_cTable+" "+i_lTable+" where PACODAZI="+cp_ToStrODBC(this.w_COD_AZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PACODAZI',this.w_COD_AZI)
            select PACODAZI,PAHIDOPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COD_AZI = NVL(_Link_.PACODAZI,space(5))
      this.w_HIDEOPENF = NVL(_Link_.PAHIDOPE,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_COD_AZI = space(5)
      endif
      this.w_HIDEOPENF = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PAR_ALTE_IDX,2])+'\'+cp_ToStr(_Link_.PACODAZI,1)
      cp_ShowWarn(i_cKey,this.PAR_ALTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COD_AZI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLAMOD
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMCLAS_IDX,3]
    i_lTable = "PROMCLAS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2], .t., this.PROMCLAS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLAMOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MCD',True,'PROMCLAS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CDCODCLA like "+cp_ToStrODBC(trim(this.w_CLAMOD)+"%");

          i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDCLAALL,CDDESCLA,CDMODALL,CDRIFTAB,CDTIPARC,CDPUBWEB,CDTIPALL,CDPATSTD,CDTIPRAG";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CDCODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CDCODCLA',trim(this.w_CLAMOD))
          select CDCODCLA,CDCLAALL,CDDESCLA,CDMODALL,CDRIFTAB,CDTIPARC,CDPUBWEB,CDTIPALL,CDPATSTD,CDTIPRAG;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CDCODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLAMOD)==trim(_Link_.CDCODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLAMOD) and !this.bDontReportError
            deferred_cp_zoom('PROMCLAS','*','CDCODCLA',cp_AbsName(oSource.parent,'oCLAMOD_1_4'),i_cWhere,'GSUT_MCD',"Classi documentali",'GSUT_KMD.PROMCLAS_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDCLAALL,CDDESCLA,CDMODALL,CDRIFTAB,CDTIPARC,CDPUBWEB,CDTIPALL,CDPATSTD,CDTIPRAG";
                     +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',oSource.xKey(1))
            select CDCODCLA,CDCLAALL,CDDESCLA,CDMODALL,CDRIFTAB,CDTIPARC,CDPUBWEB,CDTIPALL,CDPATSTD,CDTIPRAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLAMOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CDCODCLA,CDCLAALL,CDDESCLA,CDMODALL,CDRIFTAB,CDTIPARC,CDPUBWEB,CDTIPALL,CDPATSTD,CDTIPRAG";
                   +" from "+i_cTable+" "+i_lTable+" where CDCODCLA="+cp_ToStrODBC(this.w_CLAMOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CDCODCLA',this.w_CLAMOD)
            select CDCODCLA,CDCLAALL,CDDESCLA,CDMODALL,CDRIFTAB,CDTIPARC,CDPUBWEB,CDTIPALL,CDPATSTD,CDTIPRAG;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLAMOD = NVL(_Link_.CDCODCLA,space(15))
      this.w_CLASSE = NVL(_Link_.CDCLAALL,space(10))
      this.w_DESCLAS = NVL(_Link_.CDDESCLA,space(40))
      this.w_TIPOCLA = NVL(_Link_.CDMODALL,space(1))
      this.w_RIFTAB = NVL(_Link_.CDRIFTAB,space(20))
      this.w_TIPARC = NVL(_Link_.CDTIPARC,space(1))
      this.w_CDPUBWEB = NVL(_Link_.CDPUBWEB,space(1))
      this.w_TIPOALLE = NVL(_Link_.CDTIPALL,space(5))
      this.w_CLASSEALL = NVL(_Link_.CDCLAALL,space(5))
      this.w_PATHARCLAS = NVL(_Link_.CDPATSTD,space(250))
      this.w_CDTIPRAG = NVL(_Link_.CDTIPRAG,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CLAMOD = space(15)
      endif
      this.w_CLASSE = space(10)
      this.w_DESCLAS = space(40)
      this.w_TIPOCLA = space(1)
      this.w_RIFTAB = space(20)
      this.w_TIPARC = space(1)
      this.w_CDPUBWEB = space(1)
      this.w_TIPOALLE = space(5)
      this.w_CLASSEALL = space(5)
      this.w_PATHARCLAS = space(250)
      this.w_CDTIPRAG = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=upper(alltrim(.w_RIFTAB)) = upper(alltrim(.w_ARCHIVIO)) and .w_TIPARC = 'M' AND .w_TIPOCLA='F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Classe documentale incogruente")
        endif
        this.w_CLAMOD = space(15)
        this.w_CLASSE = space(10)
        this.w_DESCLAS = space(40)
        this.w_TIPOCLA = space(1)
        this.w_RIFTAB = space(20)
        this.w_TIPARC = space(1)
        this.w_CDPUBWEB = space(1)
        this.w_TIPOALLE = space(5)
        this.w_CLASSEALL = space(5)
        this.w_PATHARCLAS = space(250)
        this.w_CDTIPRAG = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMCLAS_IDX,2])+'\'+cp_ToStr(_Link_.CDCODCLA,1)
      cp_ShowWarn(i_cKey,this.PROMCLAS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLAMOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODPRAT
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRAT) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRAT)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNPATHFA";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRAT);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRAT)
            select CNCODCAN,CNPATHFA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRAT = NVL(_Link_.CNCODCAN,space(10))
      this.w_PATHARPRAT = NVL(_Link_.CNPATHFA,space(250))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRAT = space(10)
      endif
      this.w_PATHARPRAT = space(250)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRAT Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODMOD
  func Link_1_10(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.PROMODEL_IDX,3]
    i_lTable = "PROMODEL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.PROMODEL_IDX,2], .t., this.PROMODEL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.PROMODEL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODMOD) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_AMD',True,'PROMODEL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" MOCODICE like "+cp_ToStrODBC(trim(this.w_CODMOD)+"%");
                   +" and MOCLASSE="+cp_ToStrODBC(this.w_CLAMOD);

          i_ret=cp_SQL(i_nConn,"select MOCLASSE,MOCODICE,MODESCRI,MOPATMOD,MOQUERY,MO__NOTE,MOCODPRE,MORAGPRE,MODTOBSO,MODTINVA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by MOCLASSE,MOCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'MOCLASSE',this.w_CLAMOD;
                     ,'MOCODICE',trim(this.w_CODMOD))
          select MOCLASSE,MOCODICE,MODESCRI,MOPATMOD,MOQUERY,MO__NOTE,MOCODPRE,MORAGPRE,MODTOBSO,MODTINVA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by MOCLASSE,MOCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODMOD)==trim(_Link_.MOCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" MODESCRI like "+cp_ToStrODBC(trim(this.w_CODMOD)+"%");
                   +" and MOCLASSE="+cp_ToStrODBC(this.w_CLAMOD);

            i_ret=cp_SQL(i_nConn,"select MOCLASSE,MOCODICE,MODESCRI,MOPATMOD,MOQUERY,MO__NOTE,MOCODPRE,MORAGPRE,MODTOBSO,MODTINVA";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" MODESCRI like "+cp_ToStr(trim(this.w_CODMOD)+"%");
                   +" and MOCLASSE="+cp_ToStr(this.w_CLAMOD);

            select MOCLASSE,MOCODICE,MODESCRI,MOPATMOD,MOQUERY,MO__NOTE,MOCODPRE,MORAGPRE,MODTOBSO,MODTINVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODMOD) and !this.bDontReportError
            deferred_cp_zoom('PROMODEL','*','MOCLASSE,MOCODICE',cp_AbsName(oSource.parent,'oCODMOD_1_10'),i_cWhere,'GSUT_AMD',"Modelli documenti",'GSUT_KMD.PROMODEL_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CLAMOD<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCLASSE,MOCODICE,MODESCRI,MOPATMOD,MOQUERY,MO__NOTE,MOCODPRE,MORAGPRE,MODTOBSO,MODTINVA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select MOCLASSE,MOCODICE,MODESCRI,MOPATMOD,MOQUERY,MO__NOTE,MOCODPRE,MORAGPRE,MODTOBSO,MODTINVA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Il modello selezionato � privo del file di riferimento o non appartiene alla classe selezionata oppure � obsoleto o non valido")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCLASSE,MOCODICE,MODESCRI,MOPATMOD,MOQUERY,MO__NOTE,MOCODPRE,MORAGPRE,MODTOBSO,MODTINVA";
                     +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and MOCLASSE="+cp_ToStrODBC(this.w_CLAMOD);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCLASSE',oSource.xKey(1);
                       ,'MOCODICE',oSource.xKey(2))
            select MOCLASSE,MOCODICE,MODESCRI,MOPATMOD,MOQUERY,MO__NOTE,MOCODPRE,MORAGPRE,MODTOBSO,MODTINVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODMOD)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select MOCLASSE,MOCODICE,MODESCRI,MOPATMOD,MOQUERY,MO__NOTE,MOCODPRE,MORAGPRE,MODTOBSO,MODTINVA";
                   +" from "+i_cTable+" "+i_lTable+" where MOCODICE="+cp_ToStrODBC(this.w_CODMOD);
                   +" and MOCLASSE="+cp_ToStrODBC(this.w_CLAMOD);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'MOCLASSE',this.w_CLAMOD;
                       ,'MOCODICE',this.w_CODMOD)
            select MOCLASSE,MOCODICE,MODESCRI,MOPATMOD,MOQUERY,MO__NOTE,MOCODPRE,MORAGPRE,MODTOBSO,MODTINVA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODMOD = NVL(_Link_.MOCODICE,space(10))
      this.w_DESMOD = NVL(_Link_.MODESCRI,space(60))
      this.w_CLASSEDOC = NVL(_Link_.MOCLASSE,space(15))
      this.w_PATMOD = NVL(_Link_.MOPATMOD,space(254))
      this.w_QUEMOD = NVL(_Link_.MOQUERY,space(60))
      this.w_TESTO = NVL(_Link_.MO__NOTE,space(0))
      this.w_QUERY = NVL(_Link_.MOQUERY,space(60))
      this.w_CODPRE = NVL(_Link_.MOCODPRE,space(20))
      this.w_RAGPRE = NVL(_Link_.MORAGPRE,space(10))
      this.w_MODTOBSO = NVL(cp_ToDate(_Link_.MODTOBSO),ctod("  /  /  "))
      this.w_MODTINVA = NVL(cp_ToDate(_Link_.MODTINVA),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODMOD = space(10)
      endif
      this.w_DESMOD = space(60)
      this.w_CLASSEDOC = space(15)
      this.w_PATMOD = space(254)
      this.w_QUEMOD = space(60)
      this.w_TESTO = space(0)
      this.w_QUERY = space(60)
      this.w_CODPRE = space(20)
      this.w_RAGPRE = space(10)
      this.w_MODTOBSO = ctod("  /  /  ")
      this.w_MODTINVA = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=not empty(.w_PATMOD) and cp_fileexist(.w_PATMOD) and .w_CLAMOD = .w_CLASSEDOC AND (EMPTY(.w_MODTOBSO) OR .w_MODTOBSO>.w_DtObsoModel) AND (EMPTY(.w_MODTINVA) OR .w_MODTINVA<=.w_DtObsoModel)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Il modello selezionato � privo del file di riferimento o non appartiene alla classe selezionata oppure � obsoleto o non valido")
        endif
        this.w_CODMOD = space(10)
        this.w_DESMOD = space(60)
        this.w_CLASSEDOC = space(15)
        this.w_PATMOD = space(254)
        this.w_QUEMOD = space(60)
        this.w_TESTO = space(0)
        this.w_QUERY = space(60)
        this.w_CODPRE = space(20)
        this.w_RAGPRE = space(10)
        this.w_MODTOBSO = ctod("  /  /  ")
        this.w_MODTINVA = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.PROMODEL_IDX,2])+'\'+cp_ToStr(_Link_.MOCLASSE,1)+'\'+cp_ToStr(_Link_.MOCODICE,1)
      cp_ShowWarn(i_cKey,this.PROMODEL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODMOD Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=TIPOALLE
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_ALLE_IDX,3]
    i_lTable = "TIP_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2], .t., this.TIP_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TIPOALLE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODICE like "+cp_ToStrODBC(trim(this.w_TIPOALLE)+"%");

          i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI,TAPATTIP,TACOPTIP";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',trim(this.w_TIPOALLE))
          select TACODICE,TADESCRI,TAPATTIP,TACOPTIP;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TIPOALLE)==trim(_Link_.TACODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TIPOALLE) and !this.bDontReportError
            deferred_cp_zoom('TIP_ALLE','*','TACODICE',cp_AbsName(oSource.parent,'oTIPOALLE_1_12'),i_cWhere,'',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI,TAPATTIP,TACOPTIP";
                     +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1))
            select TACODICE,TADESCRI,TAPATTIP,TACOPTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TIPOALLE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TADESCRI,TAPATTIP,TACOPTIP";
                   +" from "+i_cTable+" "+i_lTable+" where TACODICE="+cp_ToStrODBC(this.w_TIPOALLE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_TIPOALLE)
            select TACODICE,TADESCRI,TAPATTIP,TACOPTIP;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TIPOALLE = NVL(_Link_.TACODICE,space(5))
      this.w_DESTIPAL = NVL(_Link_.TADESCRI,space(40))
      this.w_PATHTIPO = NVL(_Link_.TAPATTIP,space(150))
      this.w_COMTIP = NVL(_Link_.TACOPTIP,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TIPOALLE = space(5)
      endif
      this.w_DESTIPAL = space(40)
      this.w_PATHTIPO = space(150)
      this.w_COMTIP = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)
      cp_ShowWarn(i_cKey,this.TIP_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TIPOALLE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CLASSEALL
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CLA_ALLE_IDX,3]
    i_lTable = "CLA_ALLE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2], .t., this.CLA_ALLE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CLASSEALL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSUT_MTA',True,'CLA_ALLE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TACODCLA like "+cp_ToStrODBC(trim(this.w_CLASSEALL)+"%");
                   +" and TACODICE="+cp_ToStrODBC(this.w_TIPOALLE);

          i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TAPATCLA,TACOPCLA";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TACODICE,TACODCLA","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TACODICE',this.w_TIPOALLE;
                     ,'TACODCLA',trim(this.w_CLASSEALL))
          select TACODICE,TACODCLA,TACLADES,TAPATCLA,TACOPCLA;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TACODICE,TACODCLA into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CLASSEALL)==trim(_Link_.TACODCLA) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CLASSEALL) and !this.bDontReportError
            deferred_cp_zoom('CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(oSource.parent,'oCLASSEALL_1_14'),i_cWhere,'GSUT_MTA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPOALLE<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TAPATCLA,TACOPCLA";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select TACODICE,TACODCLA,TACLADES,TAPATCLA,TACOPCLA;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TAPATCLA,TACOPCLA";
                     +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(oSource.xKey(2));
                     +" and TACODICE="+cp_ToStrODBC(this.w_TIPOALLE);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',oSource.xKey(1);
                       ,'TACODCLA',oSource.xKey(2))
            select TACODICE,TACODCLA,TACLADES,TAPATCLA,TACOPCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CLASSEALL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TACODICE,TACODCLA,TACLADES,TAPATCLA,TACOPCLA";
                   +" from "+i_cTable+" "+i_lTable+" where TACODCLA="+cp_ToStrODBC(this.w_CLASSEALL);
                   +" and TACODICE="+cp_ToStrODBC(this.w_TIPOALLE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TACODICE',this.w_TIPOALLE;
                       ,'TACODCLA',this.w_CLASSEALL)
            select TACODICE,TACODCLA,TACLADES,TAPATCLA,TACOPCLA;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CLASSEALL = NVL(_Link_.TACODCLA,space(5))
      this.w_DESCLAL = NVL(_Link_.TACLADES,space(40))
      this.w_PATHCLAS = NVL(_Link_.TAPATCLA,space(150))
      this.w_COMCLA = NVL(_Link_.TACOPCLA,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CLASSEALL = space(5)
      endif
      this.w_DESCLAL = space(40)
      this.w_PATHCLAS = space(150)
      this.w_COMCLA = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CLA_ALLE_IDX,2])+'\'+cp_ToStr(_Link_.TACODICE,1)+'\'+cp_ToStr(_Link_.TACODCLA,1)
      cp_ShowWarn(i_cKey,this.CLA_ALLE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CLASSEALL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCLAMOD_1_4.value==this.w_CLAMOD)
      this.oPgFrm.Page1.oPag.oCLAMOD_1_4.value=this.w_CLAMOD
    endif
    if not(this.oPgFrm.Page1.oPag.oCDPUBWEB_1_5.RadioValue()==this.w_CDPUBWEB)
      this.oPgFrm.Page1.oPag.oCDPUBWEB_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODMOD_1_10.value==this.w_CODMOD)
      this.oPgFrm.Page1.oPag.oCODMOD_1_10.value=this.w_CODMOD
    endif
    if not(this.oPgFrm.Page1.oPag.oTESTO_1_11.value==this.w_TESTO)
      this.oPgFrm.Page1.oPag.oTESTO_1_11.value=this.w_TESTO
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPOALLE_1_12.value==this.w_TIPOALLE)
      this.oPgFrm.Page1.oPag.oTIPOALLE_1_12.value=this.w_TIPOALLE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMTIP_1_13.RadioValue()==this.w_COMTIP)
      this.oPgFrm.Page1.oPag.oCOMTIP_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLASSEALL_1_14.value==this.w_CLASSEALL)
      this.oPgFrm.Page1.oPag.oCLASSEALL_1_14.value=this.w_CLASSEALL
    endif
    if not(this.oPgFrm.Page1.oPag.oCOMCLA_1_16.RadioValue()==this.w_COMCLA)
      this.oPgFrm.Page1.oPag.oCOMCLA_1_16.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFILEORIG_1_17.value==this.w_FILEORIG)
      this.oPgFrm.Page1.oPag.oFILEORIG_1_17.value=this.w_FILEORIG
    endif
    if not(this.oPgFrm.Page1.oPag.oOGGETTO_1_18.value==this.w_OGGETTO)
      this.oPgFrm.Page1.oPag.oOGGETTO_1_18.value=this.w_OGGETTO
    endif
    if not(this.oPgFrm.Page1.oPag.oPATARC_1_19.value==this.w_PATARC)
      this.oPgFrm.Page1.oPag.oPATARC_1_19.value=this.w_PATARC
    endif
    if not(this.oPgFrm.Page1.oPag.oDESMOD_1_26.value==this.w_DESMOD)
      this.oPgFrm.Page1.oPag.oDESMOD_1_26.value=this.w_DESMOD
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLAS_1_31.value==this.w_DESCLAS)
      this.oPgFrm.Page1.oPag.oDESCLAS_1_31.value=this.w_DESCLAS
    endif
    if not(this.oPgFrm.Page1.oPag.oDESTIPAL_1_38.value==this.w_DESTIPAL)
      this.oPgFrm.Page1.oPag.oDESTIPAL_1_38.value=this.w_DESTIPAL
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLAL_1_39.value==this.w_DESCLAL)
      this.oPgFrm.Page1.oPag.oDESCLAL_1_39.value=this.w_DESCLAL
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   ((empty(.w_CLAMOD)) or not(upper(alltrim(.w_RIFTAB)) = upper(alltrim(.w_ARCHIVIO)) and .w_TIPARC = 'M' AND .w_TIPOCLA='F'))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCLAMOD_1_4.SetFocus()
            i_bnoObbl = !empty(.w_CLAMOD)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Classe documentale incogruente")
          case   ((empty(.w_CODMOD)) or not(not empty(.w_PATMOD) and cp_fileexist(.w_PATMOD) and .w_CLAMOD = .w_CLASSEDOC AND (EMPTY(.w_MODTOBSO) OR .w_MODTOBSO>.w_DtObsoModel) AND (EMPTY(.w_MODTINVA) OR .w_MODTINVA<=.w_DtObsoModel)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODMOD_1_10.SetFocus()
            i_bnoObbl = !empty(.w_CODMOD)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il modello selezionato � privo del file di riferimento o non appartiene alla classe selezionata oppure � obsoleto o non valido")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_COD_AZI = this.w_COD_AZI
    this.o_CLAMOD = this.w_CLAMOD
    this.o_CODMOD = this.w_CODMOD
    this.o_TIPOALLE = this.w_TIPOALLE
    this.o_COMTIP = this.w_COMTIP
    this.o_CLASSEALL = this.w_CLASSEALL
    this.o_PATMOD = this.w_PATMOD
    this.o_COMCLA = this.w_COMCLA
    this.o_FILEORIG = this.w_FILEORIG
    return

enddefine

* --- Define pages as container
define class tgsut_kmdPag1 as StdContainer
  Width  = 730
  height = 315
  stdWidth  = 730
  stdheight = 315
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCLAMOD_1_4 as StdField with uid="VKCEFTWPBY",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CLAMOD", cQueryName = "CLAMOD",;
    bObbl = .t. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Classe documentale incogruente",;
    HelpContextID = 44557018,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=154, Top=8, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="PROMCLAS", cZoomOnZoom="GSUT_MCD", oKey_1_1="CDCODCLA", oKey_1_2="this.w_CLAMOD"

  func oCLAMOD_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
      if .not. empty(.w_CODMOD)
        bRes2=.link_1_10('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCLAMOD_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLAMOD_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'PROMCLAS','*','CDCODCLA',cp_AbsName(this.parent,'oCLAMOD_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MCD',"Classi documentali",'GSUT_KMD.PROMCLAS_VZM',this.parent.oContained
  endproc
  proc oCLAMOD_1_4.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MCD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CDCODCLA=this.parent.oContained.w_CLAMOD
     i_obj.ecpSave()
  endproc

  add object oCDPUBWEB_1_5 as StdCheck with uid="RWQSQMBOSF",rtseq=5,rtrep=.f.,left=154, top=34, caption="Pubblica su Web",;
    ToolTipText = "Se attivo: di default l'indice creato verr� pubblicato su Web",;
    HelpContextID = 261162344,;
    cFormVar="w_CDPUBWEB", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCDPUBWEB_1_5.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCDPUBWEB_1_5.GetRadio()
    this.Parent.oContained.w_CDPUBWEB = this.RadioValue()
    return .t.
  endfunc

  func oCDPUBWEB_1_5.SetRadio()
    this.Parent.oContained.w_CDPUBWEB=trim(this.Parent.oContained.w_CDPUBWEB)
    this.value = ;
      iif(this.Parent.oContained.w_CDPUBWEB=='S',1,;
      0)
  endfunc

  func oCDPUBWEB_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPOCLA<>'I')
    endwith
   endif
  endfunc

  func oCDPUBWEB_1_5.mHide()
    with this.Parent.oContained
      return (g_CPIN<>'S')
    endwith
  endfunc

  add object oCODMOD_1_10 as StdField with uid="VTGZMEZQAC",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CODMOD", cQueryName = "CODMOD",;
    bObbl = .t. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    sErrorMsg = "Il modello selezionato � privo del file di riferimento o non appartiene alla classe selezionata oppure � obsoleto o non valido",;
    ToolTipText = "Modello di riferimento",;
    HelpContextID = 44543962,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=154, Top=34, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="PROMODEL", cZoomOnZoom="GSUT_AMD", oKey_1_1="MOCLASSE", oKey_1_2="this.w_CLAMOD", oKey_2_1="MOCODICE", oKey_2_2="this.w_CODMOD"

  func oCODMOD_1_10.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_10('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODMOD_1_10.ecpDrop(oSource)
    this.Parent.oContained.link_1_10('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODMOD_1_10.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.PROMODEL_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"MOCLASSE="+cp_ToStrODBC(this.Parent.oContained.w_CLAMOD)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"MOCLASSE="+cp_ToStr(this.Parent.oContained.w_CLAMOD)
    endif
    do cp_zoom with 'PROMODEL','*','MOCLASSE,MOCODICE',cp_AbsName(this.parent,'oCODMOD_1_10'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_AMD',"Modelli documenti",'GSUT_KMD.PROMODEL_VZM',this.parent.oContained
  endproc
  proc oCODMOD_1_10.mZoomOnZoom
    local i_obj
    i_obj=GSUT_AMD()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.MOCLASSE=w_CLAMOD
     i_obj.w_MOCODICE=this.parent.oContained.w_CODMOD
     i_obj.ecpSave()
  endproc

  add object oTESTO_1_11 as StdMemo with uid="KIBTRUIAQU",rtseq=11,rtrep=.f.,;
    cFormVar = "w_TESTO", cQueryName = "TESTO",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(0), bMultilanguage =  .f.,;
    ToolTipText = "Note aggiuntive",;
    HelpContextID = 111134922,;
   bGlobalFont=.t.,;
    Height=60, Width=519, Left=154, Top=170

  add object oTIPOALLE_1_12 as StdField with uid="YGWUGXJKVD",rtseq=12,rtrep=.f.,;
    cFormVar = "w_TIPOALLE", cQueryName = "TIPOALLE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Tipologia dell'allegato",;
    HelpContextID = 193262725,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=154, Top=60, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_ALLE", oKey_1_1="TACODICE", oKey_1_2="this.w_TIPOALLE"

  func oTIPOALLE_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COMTIP<>'S')
    endwith
   endif
  endfunc

  func oTIPOALLE_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
      if .not. empty(.w_CLASSEALL)
        bRes2=.link_1_14('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oTIPOALLE_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTIPOALLE_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_ALLE','*','TACODICE',cp_AbsName(this.parent,'oTIPOALLE_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'',"",'',this.parent.oContained
  endproc

  add object oCOMTIP_1_13 as StdCheck with uid="JKIHUYGQTI",rtseq=13,rtrep=.f.,left=528, top=59, caption="Completa path file",;
    ToolTipText = "Se attivo completa il path di archiviazione file con il codice tipologia allegato",;
    HelpContextID = 150986790,;
    cFormVar="w_COMTIP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOMTIP_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCOMTIP_1_13.GetRadio()
    this.Parent.oContained.w_COMTIP = this.RadioValue()
    return .t.
  endfunc

  func oCOMTIP_1_13.SetRadio()
    this.Parent.oContained.w_COMTIP=trim(this.Parent.oContained.w_COMTIP)
    this.value = ;
      iif(this.Parent.oContained.w_COMTIP=='S',1,;
      0)
  endfunc

  func oCOMTIP_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! empty(.w_TIPOALLE) and .w_COMCLA<>'S')
    endwith
   endif
  endfunc

  add object oCLASSEALL_1_14 as StdField with uid="NHMMTMWYXF",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CLASSEALL", cQueryName = "CLASSEALL",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 245244466,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=154, Top=88, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="CLA_ALLE", cZoomOnZoom="GSUT_MTA", oKey_1_1="TACODICE", oKey_1_2="this.w_TIPOALLE", oKey_2_1="TACODCLA", oKey_2_2="this.w_CLASSEALL"

  func oCLASSEALL_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_COMCLA<>'S')
    endwith
   endif
  endfunc

  func oCLASSEALL_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
    endwith
    return bRes
  endfunc

  proc oCLASSEALL_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCLASSEALL_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CLA_ALLE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStrODBC(this.Parent.oContained.w_TIPOALLE)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"TACODICE="+cp_ToStr(this.Parent.oContained.w_TIPOALLE)
    endif
    do cp_zoom with 'CLA_ALLE','*','TACODICE,TACODCLA',cp_AbsName(this.parent,'oCLASSEALL_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSUT_MTA',"",'',this.parent.oContained
  endproc
  proc oCLASSEALL_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSUT_MTA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.TACODICE=w_TIPOALLE
     i_obj.w_TACODCLA=this.parent.oContained.w_CLASSEALL
     i_obj.ecpSave()
  endproc

  add object oCOMCLA_1_16 as StdCheck with uid="RJMHGRDYCQ",rtseq=16,rtrep=.f.,left=528, top=87, caption="Completa path file",;
    ToolTipText = "Se attivo completa il path di archiviazione file con il codice classe allegato",;
    HelpContextID = 98639834,;
    cFormVar="w_COMCLA", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oCOMCLA_1_16.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oCOMCLA_1_16.GetRadio()
    this.Parent.oContained.w_COMCLA = this.RadioValue()
    return .t.
  endfunc

  func oCOMCLA_1_16.SetRadio()
    this.Parent.oContained.w_COMCLA=trim(this.Parent.oContained.w_COMCLA)
    this.value = ;
      iif(this.Parent.oContained.w_COMCLA=='S',1,;
      0)
  endfunc

  func oCOMCLA_1_16.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (! EMPTY(.w_CLASSEALL))
    endwith
   endif
  endfunc

  add object oFILEORIG_1_17 as StdField with uid="PEJXPXKMRN",rtseq=17,rtrep=.f.,;
    cFormVar = "w_FILEORIG", cQueryName = "FILEORIG",;
    bObbl = .f. , nPag = 1, value=space(80), bMultilanguage =  .f.,;
    HelpContextID = 78591331,;
   bGlobalFont=.t.,;
    Height=21, Width=519, Left=154, Top=116, InputMask=replicate('X',80)

  func oFILEORIG_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (Empty(.w_QUERY) or Isalt())
    endwith
   endif
  endfunc

  add object oOGGETTO_1_18 as StdField with uid="TSGPSJFDVI",rtseq=18,rtrep=.f.,;
    cFormVar = "w_OGGETTO", cQueryName = "OGGETTO",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione",;
    HelpContextID = 39814938,;
   bGlobalFont=.t.,;
    Height=21, Width=519, Left=154, Top=143, InputMask=replicate('X',200)

  add object oPATARC_1_19 as StdField with uid="TTOEIAEDOW",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PATARC", cQueryName = "PATARC",;
    bObbl = .f. , nPag = 1, value=space(250), bMultilanguage =  .f.,;
    HelpContextID = 58899722,;
   bGlobalFont=.t.,;
    Height=21, Width=519, Left=154, Top=235, InputMask=replicate('X',250)


  add object oBtn_1_20 as StdButton with uid="LOFQGKUWLF",left=676, top=234, width=22,height=23,;
    caption="...", nPag=1;
    , HelpContextID = 199635498;
  , bGlobalFont=.t.

    proc oBtn_1_20.Click()
      this.parent.oContained.NotifyEvent("SelDir")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_22 as StdButton with uid="VFSEADJQCS",left=624, top=263, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per confermare la storicizzazione";
    , HelpContextID = 199807770;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_24 as StdButton with uid="URECPKNNNL",left=676, top=263, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per annullare";
    , HelpContextID = 192519098;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESMOD_1_26 as StdField with uid="SILZDBVGWT",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESMOD", cQueryName = "DESMOD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(60), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione del modello",;
    HelpContextID = 44485066,;
   bGlobalFont=.t.,;
    Height=21, Width=419, Left=254, Top=34, InputMask=replicate('X',60)


  add object oBtn_1_29 as StdButton with uid="GJZUUMSDLJ",left=676, top=38, width=48,height=45,;
    CpPicture="bmp\codici.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per visualizzare il modello di riferimento selezionato";
    , HelpContextID = 192458490;
    , caption='\<Apri file';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_29.Click()
      with this.Parent.oContained
        GSUT_BMD(this.Parent.oContained,.w_PATMOD)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_29.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not (empty(.w_CODMOD) or empty(.w_PATMOD)))
      endwith
    endif
  endfunc

  func oBtn_1_29.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (empty(.w_CODMOD) or empty(.w_PATMOD) or .w_HIDEOPENF='S')
     endwith
    endif
  endfunc

  add object oDESCLAS_1_31 as StdField with uid="SIJHPESWCY",rtseq=24,rtrep=.f.,;
    cFormVar = "w_DESCLAS", cQueryName = "DESCLAS",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione classe documentale",;
    HelpContextID = 169817654,;
   bGlobalFont=.t.,;
    Height=21, Width=383, Left=290, Top=8, InputMask=replicate('X',40)

  add object oDESTIPAL_1_38 as StdField with uid="BXXBWXPFIY",rtseq=28,rtrep=.f.,;
    cFormVar = "w_DESTIPAL", cQueryName = "DESTIPAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 151008898,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=219, Top=60, InputMask=replicate('X',40)

  add object oDESCLAL_1_39 as StdField with uid="CZBSOJUFJC",rtseq=29,rtrep=.f.,;
    cFormVar = "w_DESCLAL", cQueryName = "DESCLAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 98617802,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=219, Top=88, InputMask=replicate('X',40)

  add object oStr_1_21 as StdString with uid="SKBTGWTHPK",Visible=.t., Left=51, Top=34,;
    Alignment=1, Width=101, Height=18,;
    Caption="Modello:"  ;
  , bGlobalFont=.t.

  add object oStr_1_23 as StdString with uid="QNTHKJCPKD",Visible=.t., Left=51, Top=170,;
    Alignment=1, Width=101, Height=18,;
    Caption="Note aggiuntive:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="IWTXALOVUO",Visible=.t., Left=7, Top=10,;
    Alignment=1, Width=145, Height=18,;
    Caption="Classe documentale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="NUTQEDANRH",Visible=.t., Left=51, Top=63,;
    Alignment=1, Width=101, Height=18,;
    Caption="Tipologia allegato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_37 as StdString with uid="GXPWHXDAZM",Visible=.t., Left=51, Top=89,;
    Alignment=1, Width=101, Height=18,;
    Caption="Classe allegato:"  ;
  , bGlobalFont=.t.

  add object oStr_1_40 as StdString with uid="ODKKLUEEWL",Visible=.t., Left=51, Top=144,;
    Alignment=1, Width=101, Height=18,;
    Caption="Descrizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_41 as StdString with uid="NCISCIQEAB",Visible=.t., Left=48, Top=235,;
    Alignment=1, Width=104, Height=18,;
    Caption="Percorso assoluto:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="NEBTMWAKKZ",Visible=.t., Left=51, Top=118,;
    Alignment=1, Width=101, Height=18,;
    Caption="Nome file orig.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_kmd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
