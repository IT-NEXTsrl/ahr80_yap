* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_svr                                                        *
*              Verifica ripartizione spese (vendite/acquisti)                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_81]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-02-01                                                      *
* Last revis.: 2007-10-18                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsve_svr",oParentObject))

* --- Class definition
define class tgsve_svr as StdForm
  Top    = 42
  Left   = 30

  * --- Standard Properties
  Width  = 508
  Height = 207
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-10-18"
  HelpContextID=40514153
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=14

  * --- Constant Properties
  _IDX = 0
  DOC_MAST_IDX = 0
  CONTI_IDX = 0
  TIP_DOCU_IDX = 0
  cPrg = "gsve_svr"
  cComment = "Verifica ripartizione spese (vendite/acquisti)"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CICLO = space(1)
  w_DATINI = ctod('  /  /  ')
  w_DATAFIN = ctod('  /  /  ')
  w_CAUSALE = space(5)
  o_CAUSALE = space(5)
  w_CLIFORFL = space(1)
  o_CLIFORFL = space(1)
  w_CODFORN = space(15)
  w_DESCFOR = space(30)
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_CAUSDES = space(35)
  w_CATEGO = space(2)
  w_APPCIC = space(1)
  w_CATDOC = space(2)
  w_FLVALO = space(1)
  * --- Area Manuale = Declare Variables
  * --- gsve_svr
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    local l_CICLO
    l_CICLO=this.oParentObject
    return(lower('GSVE_SVR'+IIF(Type('l_CICLO')='C',','+l_CICLO,'')))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsve_svrPag1","gsve_svr",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATINI_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='TIP_DOCU'
    return(this.OpenAllTables(3))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      vx_exec(""+alltrim(THIS.w_OQRY)+", "+alltrim(THIS.w_orep)+"",this)
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsve_svr
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CICLO=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATAFIN=ctod("  /  /  ")
      .w_CAUSALE=space(5)
      .w_CLIFORFL=space(1)
      .w_CODFORN=space(15)
      .w_DESCFOR=space(30)
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_CAUSDES=space(35)
      .w_CATEGO=space(2)
      .w_APPCIC=space(1)
      .w_CATDOC=space(2)
      .w_FLVALO=space(1)
        .w_CICLO = this.oParentObject
          .DoRTCalc(2,2,.f.)
        .w_DATAFIN = i_datsys
        .DoRTCalc(4,4,.f.)
        if not(empty(.w_CAUSALE))
          .link_1_4('Full')
        endif
        .w_CLIFORFL = iif(empty(.w_CAUSALE),IIF( .w_CICLO='V', 'C' , 'F' ),.w_CLIFORFL)
        .w_CODFORN = SPACE(15)
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_CODFORN))
          .link_1_6('Full')
        endif
          .DoRTCalc(7,7,.f.)
        .w_OBTEST = i_DATSYS
      .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
    endwith
    this.DoRTCalc(9,14,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_CAUSALE<>.w_CAUSALE
            .w_CLIFORFL = iif(empty(.w_CAUSALE),IIF( .w_CICLO='V', 'C' , 'F' ),.w_CLIFORFL)
        endif
        if .o_CLIFORFL<>.w_CLIFORFL
            .w_CODFORN = SPACE(15)
          .link_1_6('Full')
        endif
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(7,14,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_21.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oCODFORN_1_6.enabled = this.oPgFrm.Page1.oPag.oCODFORN_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsve_svr
    THIS.CAPTION='Verifica ripartizione spese '+IIF (This.w_CICLO='V','(vendite)','(acquisti)')
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_21.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CAUSALE
  func Link_1_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TIP_DOCU_IDX,3]
    i_lTable = "TIP_DOCU"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2], .t., this.TIP_DOCU_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CAUSALE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'TIP_DOCU')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TDTIPDOC like "+cp_ToStrODBC(trim(this.w_CAUSALE)+"%");

          i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE,TDCATDOC,TDFLVALO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TDTIPDOC","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TDTIPDOC',trim(this.w_CAUSALE))
          select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE,TDCATDOC,TDFLVALO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TDTIPDOC into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CAUSALE)==trim(_Link_.TDTIPDOC) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CAUSALE) and !this.bDontReportError
            deferred_cp_zoom('TIP_DOCU','*','TDTIPDOC',cp_AbsName(oSource.parent,'oCAUSALE_1_4'),i_cWhere,'',"Causali documento",'GSVE5KRS.TIP_DOCU_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE,TDCATDOC,TDFLVALO";
                     +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',oSource.xKey(1))
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE,TDCATDOC,TDFLVALO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CAUSALE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE,TDCATDOC,TDFLVALO";
                   +" from "+i_cTable+" "+i_lTable+" where TDTIPDOC="+cp_ToStrODBC(this.w_CAUSALE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TDTIPDOC',this.w_CAUSALE)
            select TDTIPDOC,TDDESDOC,TDFLVEAC,TDFLINTE,TDCATDOC,TDFLVALO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CAUSALE = NVL(_Link_.TDTIPDOC,space(5))
      this.w_CAUSDES = NVL(_Link_.TDDESDOC,space(35))
      this.w_APPCIC = NVL(_Link_.TDFLVEAC,space(1))
      this.w_CLIFORFL = NVL(_Link_.TDFLINTE,space(1))
      this.w_CATDOC = NVL(_Link_.TDCATDOC,space(2))
      this.w_FLVALO = NVL(_Link_.TDFLVALO,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CAUSALE = space(5)
      endif
      this.w_CAUSDES = space(35)
      this.w_APPCIC = space(1)
      this.w_CLIFORFL = space(1)
      this.w_CATDOC = space(2)
      this.w_FLVALO = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_APPCIC=.w_CICLO And .w_CATDOC $ 'FA-NC' And .w_FLVALO='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Causale inesistente oppure incongruente (selezionare fatture o note di credito con check modifica valore attivo)")
        endif
        this.w_CAUSALE = space(5)
        this.w_CAUSDES = space(35)
        this.w_APPCIC = space(1)
        this.w_CLIFORFL = space(1)
        this.w_CATDOC = space(2)
        this.w_FLVALO = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TIP_DOCU_IDX,2])+'\'+cp_ToStr(_Link_.TDTIPDOC,1)
      cp_ShowWarn(i_cKey,this.TIP_DOCU_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CAUSALE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODFORN
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODFORN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODFORN)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CLIFORFL);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_CLIFORFL;
                     ,'ANCODICE',trim(this.w_CODFORN))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODFORN)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODFORN) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODFORN_1_6'),i_cWhere,'',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CLIFORFL<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_CLIFORFL);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODFORN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODFORN);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_CLIFORFL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_CLIFORFL;
                       ,'ANCODICE',this.w_CODFORN)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODFORN = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCFOR = NVL(_Link_.ANDESCRI,space(30))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODFORN = space(15)
      endif
      this.w_DESCFOR = space(30)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_CODFORN = space(15)
        this.w_DESCFOR = space(30)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODFORN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_2.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_2.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATAFIN_1_3.value==this.w_DATAFIN)
      this.oPgFrm.Page1.oPag.oDATAFIN_1_3.value=this.w_DATAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUSALE_1_4.value==this.w_CAUSALE)
      this.oPgFrm.Page1.oPag.oCAUSALE_1_4.value=this.w_CAUSALE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODFORN_1_6.value==this.w_CODFORN)
      this.oPgFrm.Page1.oPag.oCODFORN_1_6.value=this.w_CODFORN
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCFOR_1_14.value==this.w_DESCFOR)
      this.oPgFrm.Page1.oPag.oDESCFOR_1_14.value=this.w_DESCFOR
    endif
    if not(this.oPgFrm.Page1.oPag.oCAUSDES_1_18.value==this.w_CAUSDES)
      this.oPgFrm.Page1.oPag.oCAUSDES_1_18.value=this.w_CAUSDES
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(empty(.w_DATINI) or (.w_DATINI <= .w_DATAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(empty(.w_DATAFIN) or (.w_DATINI <= .w_DATAFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATAFIN_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data iniziale � pi� grande di quella finale")
          case   not(.w_APPCIC=.w_CICLO And .w_CATDOC $ 'FA-NC' And .w_FLVALO='S')  and not(empty(.w_CAUSALE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAUSALE_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Causale inesistente oppure incongruente (selezionare fatture o note di credito con check modifica valore attivo)")
          case   not((EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and (.w_CLIFORFL $ ('C-F'))  and not(empty(.w_CODFORN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODFORN_1_6.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_CAUSALE = this.w_CAUSALE
    this.o_CLIFORFL = this.w_CLIFORFL
    return

enddefine

* --- Define pages as container
define class tgsve_svrPag1 as StdContainer
  Width  = 504
  height = 207
  stdWidth  = 504
  stdheight = 207
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_1_2 as StdField with uid="IIZFUHVQGQ",rtseq=2,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data iniziale dell'interrogazione.",;
    HelpContextID = 71019722,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=114, Top=34

  func oDATINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATINI) or (.w_DATINI <= .w_DATAFIN))
    endwith
    return bRes
  endfunc

  add object oDATAFIN_1_3 as StdField with uid="EOZHFNMHFD",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATAFIN", cQueryName = "DATAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data iniziale � pi� grande di quella finale",;
    ToolTipText = "Data finale dell'interrogazione.",;
    HelpContextID = 79932618,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=286, Top=34

  func oDATAFIN_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (empty(.w_DATAFIN) or (.w_DATINI <= .w_DATAFIN))
    endwith
    return bRes
  endfunc

  add object oCAUSALE_1_4 as StdField with uid="RBJDEPHCUE",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CAUSALE", cQueryName = "CAUSALE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Causale inesistente oppure incongruente (selezionare fatture o note di credito con check modifica valore attivo)",;
    ToolTipText = "Causale del documento",;
    HelpContextID = 234775334,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=114, Top=63, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TIP_DOCU", oKey_1_1="TDTIPDOC", oKey_1_2="this.w_CAUSALE"

  func oCAUSALE_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oCAUSALE_1_4.ecpDrop(oSource)
    this.Parent.oContained.link_1_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCAUSALE_1_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TIP_DOCU','*','TDTIPDOC',cp_AbsName(this.parent,'oCAUSALE_1_4'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Causali documento",'GSVE5KRS.TIP_DOCU_VZM',this.parent.oContained
  endproc

  add object oCODFORN_1_6 as StdField with uid="NTRQVFJRFN",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODFORN", cQueryName = "CODFORN",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice intestatario documento",;
    HelpContextID = 187670234,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=114, Top=92, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", oKey_1_1="ANTIPCON", oKey_1_2="this.w_CLIFORFL", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODFORN"

  func oCODFORN_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_CLIFORFL $ ('C-F'))
    endwith
   endif
  endfunc

  func oCODFORN_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODFORN_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODFORN_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_CLIFORFL)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_CLIFORFL)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODFORN_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Clienti/fornitori",'',this.parent.oContained
  endproc


  add object oBtn_1_7 as StdButton with uid="EINPMBEXTW",left=391, top=155, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , HelpContextID = 101275430;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      vx_exec(""+alltrim(THIS.parent.ocontained.w_OQRY)+", "+alltrim(THIS.parent.ocontained.w_orep)+"",this.Parent.oContained)
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_7.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (not empty(.w_OQRY))
      endwith
    endif
  endfunc


  add object oBtn_1_8 as StdButton with uid="OZRVABXVGB",left=445, top=155, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 33196730;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_8.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCFOR_1_14 as StdField with uid="HPBQHKQJGH",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DESCFOR", cQueryName = "DESCFOR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 20858678,;
   bGlobalFont=.t.,;
    Height=21, Width=253, Left=240, Top=92, InputMask=replicate('X',30)

  add object oCAUSDES_1_18 as StdField with uid="SWGHRUJUIL",rtseq=10,rtrep=.f.,;
    cFormVar = "w_CAUSDES", cQueryName = "CAUSDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 120480550,;
   bGlobalFont=.t.,;
    Height=21, Width=306, Left=185, Top=63, InputMask=replicate('X',35)


  add object oObj_1_21 as cp_outputCombo with uid="ZLERXIVPWT",left=114, top=125, width=379,height=22,;
    caption='Object',;
   bGlobalFont=.t.,;
    FontSize=8,Font="Arial",FontBold=.f.,FontItalic=.t.,;
    nPag=1;
    , HelpContextID = 137483494

  add object oStr_1_9 as StdString with uid="BHVZUBFSYP",Visible=.t., Left=9, Top=5,;
    Alignment=0, Width=269, Height=18,;
    Caption="Documento fattura/nota di credito da ripartire"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="JOWNMQNEDZ",Visible=.t., Left=6, Top=38,;
    Alignment=1, Width=104, Height=18,;
    Caption="Da data fatt/NC:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="MXBRIKYTEC",Visible=.t., Left=195, Top=38,;
    Alignment=1, Width=89, Height=18,;
    Caption="A data fatt/NC:"  ;
  , bGlobalFont=.t.

  add object oStr_1_12 as StdString with uid="ONBPNLSZJN",Visible=.t., Left=38, Top=96,;
    Alignment=1, Width=72, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_13 as StdString with uid="TORHIESAMK",Visible=.t., Left=24, Top=129,;
    Alignment=1, Width=86, Height=18,;
    Caption="Tipo di stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="SUOZHVREYT",Visible=.t., Left=32, Top=68,;
    Alignment=1, Width=78, Height=18,;
    Caption="Causale doc.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsve_svr','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
