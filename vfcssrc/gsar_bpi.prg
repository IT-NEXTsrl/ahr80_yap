* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bpi                                                        *
*              Controlli in cancellazione conti                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_45]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-04-11                                                      *
* Last revis.: 2008-07-30                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bpi",oParentObject)
return(i_retval)

define class tgsar_bpi as StdBatch
  * --- Local variables
  w_TEST = 0
  w_MESS = space(100)
  w_TIPDOC = space(5)
  w_SERIAL = space(10)
  w_CODAZI = space(5)
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_NR = space(4)
  w_SERIALE = space(10)
  w_NUMDOC = 0
  w_DATDOC = ctod("  /  /  ")
  w_TIPDOC = space(5)
  * --- WorkFile variables
  CONTROPA_idx=0
  RIFI_CADO_idx=0
  CATEGOMO_idx=0
  CAU_CONT_idx=0
  CAUIVA1_idx=0
  DOC_DETT_idx=0
  DOC_MAST_idx=0
  COC_MAST_idx=0
  CON_PAGA_idx=0
  PAR_VDET_idx=0
  GRU_COCE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da Gsar_Api (Anagrafica Conti)  esegue Controlli in Cancellazione
    * --- Controlli per la cancellazione
    this.w_TEST = 0
    this.w_CODAZI = i_CODAZI
    * --- Oggetto per messaggi incrementali
    this.w_oMess=createobject("Ah_Message")
    * --- Select from CONTROPA
    i_nConn=i_TableProp[this.CONTROPA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CONTROPA_idx,2],.t.,this.CONTROPA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" CONTROPA ";
          +" where COCODAZI="+cp_ToStrODBC(this.w_CODAZI)+" And ( COCONINC="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR COCONIMB= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR   COCONTRA="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR COCONBOL="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR   COCOCINC="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR COCOCPAG="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR   COFATEME="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR COFATRIC="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR   CORATATT="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR CORATPAS="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR   CORISATT="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR CORISPAS="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR   COIVADEB="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR COIVACRE="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR   COCREBRE="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR COCREMED="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR   CODEBMED="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR   COCONARR="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR COCONBAN="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR   COCONSTR="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR COSCACES="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR   COCONEFA="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR COSAABBA="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR   COSAABBP="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR COSADIFC="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR   COSADIFN="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR CODIFCON="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR   COCONRIT="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR COENASAR="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR   COCONCLI="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR COCONCAF="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR   COCONABF="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR COCONIVF="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR   COCONSAL="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR CODEBBRE="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" )";
           ,"_Curs_CONTROPA")
    else
      select * from (i_cTable);
       where COCODAZI=this.w_CODAZI And ( COCONINC=this.oParentObject.w_ANCODICE OR COCONIMB= this.oParentObject.w_ANCODICE OR   COCONTRA=this.oParentObject.w_ANCODICE OR COCONBOL=this.oParentObject.w_ANCODICE OR   COCOCINC=this.oParentObject.w_ANCODICE OR COCOCPAG=this.oParentObject.w_ANCODICE OR   COFATEME=this.oParentObject.w_ANCODICE OR COFATRIC=this.oParentObject.w_ANCODICE OR   CORATATT=this.oParentObject.w_ANCODICE OR CORATPAS=this.oParentObject.w_ANCODICE OR   CORISATT=this.oParentObject.w_ANCODICE OR CORISPAS=this.oParentObject.w_ANCODICE OR   COIVADEB=this.oParentObject.w_ANCODICE OR COIVACRE=this.oParentObject.w_ANCODICE OR   COCREBRE=this.oParentObject.w_ANCODICE OR COCREMED=this.oParentObject.w_ANCODICE OR   CODEBMED=this.oParentObject.w_ANCODICE OR   COCONARR=this.oParentObject.w_ANCODICE OR COCONBAN=this.oParentObject.w_ANCODICE OR   COCONSTR=this.oParentObject.w_ANCODICE OR COSCACES=this.oParentObject.w_ANCODICE OR   COCONEFA=this.oParentObject.w_ANCODICE OR COSAABBA=this.oParentObject.w_ANCODICE OR   COSAABBP=this.oParentObject.w_ANCODICE OR COSADIFC=this.oParentObject.w_ANCODICE OR   COSADIFN=this.oParentObject.w_ANCODICE OR CODIFCON=this.oParentObject.w_ANCODICE OR   COCONRIT=this.oParentObject.w_ANCODICE OR COENASAR=this.oParentObject.w_ANCODICE OR   COCONCLI=this.oParentObject.w_ANCODICE OR COCONCAF=this.oParentObject.w_ANCODICE OR   COCONABF=this.oParentObject.w_ANCODICE OR COCONIVF=this.oParentObject.w_ANCODICE OR   COCONSAL=this.oParentObject.w_ANCODICE OR CODEBBRE=this.oParentObject.w_ANCODICE );
        into cursor _Curs_CONTROPA
    endif
    if used('_Curs_CONTROPA')
      select _Curs_CONTROPA
      locate for 1=1
      do while not(eof())
      if this.w_TEST=0
        this.w_oMess.AddMsgPartNL("Impossibile cancellare, conto presente come:")     
      endif
      * --- Parametri Vendite
      do case
        case Nvl(_Curs_CONTROPA.COCONINC,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Spese incasso nei parametri vendite")     
        case Nvl(_Curs_CONTROPA.COCONIMB,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Spese imballo nei parametri vendite")     
        case Nvl(_Curs_CONTROPA.COCONTRA,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Spese trasporto nei parametri vendite")     
        case Nvl(_Curs_CONTROPA.COCONBOL,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Spese bolli nei parametri vendite")     
        case Nvl(_Curs_CONTROPA.COCOCINC,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Acconti incassati nei parametri vendite")     
      endcase
      * --- Parametri Acquisti
      if Nvl(_Curs_CONTROPA.COCOCPAG,Space(15))=this.oParentObject.w_ANCODICE
        this.w_oMess.AddMsgPartNL("Acconti pagati nei parametri acquisti")     
      endif
      * --- Parametri Contropartite di Assestamento
      do case
        case Nvl(_Curs_CONTROPA.CORISATT,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Risconti attivi nelle contropartite di assestamento")     
        case Nvl(_Curs_CONTROPA.COFATEME,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Fatture da emettere nelle contropartite di assestamento")     
        case Nvl(_Curs_CONTROPA.CORATPAS,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Ratei passivi nelle contropartite di assestamento")     
        case Nvl(_Curs_CONTROPA.CORATATT,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Ratei attivi nelle contropartite di assestamento")     
        case Nvl(_Curs_CONTROPA.COFATRIC,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Fatture da ricevere nelle contropartite di assestamento")     
        case Nvl(_Curs_CONTROPA.CORISPAS,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Risconti passivi nelle contropartite di assestamento")     
        case Nvl(_Curs_CONTROPA.COIVADEB,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("IVA debito nelle contropartite di assestamento")     
        case Nvl(_Curs_CONTROPA.COIVACRE,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("IVA credito nelle contropartite di assestamento")     
        case Nvl(_Curs_CONTROPA.COCREBRE,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Crediti a breve nelle contropartite di assestamento")     
        case Nvl(_Curs_CONTROPA.COCREMED,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Crediti a medio nelle contropartite di assestamento")     
        case Nvl(_Curs_CONTROPA.CODEBMED,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("debiti a medio nelle contropartite di assestamento")     
        case Nvl(_Curs_CONTROPA.CODEBBRE,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("debiti a breve nelle contropartite di assestamento")     
        case Nvl(_Curs_CONTROPA.COCONARR,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Arrotondamenti nelle contropartite di assestamento")     
      endcase
      * --- Parametri Saldaconto
      if Nvl(_Curs_CONTROPA.COCONSAL,Space(15))=this.oParentObject.w_ANCODICE
        this.w_oMess.AddMsgPartNL("Contropartita nei parametri saldaconto")     
      endif
      * --- Parametri Distinte
      do case
        case Nvl(_Curs_CONTROPA.COCONSTR,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Sconto tratte nei parametri distinte")     
        case Nvl(_Curs_CONTROPA.COCONBAN,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Spese bancarie nei parametri distinte")     
        case Nvl(_Curs_CONTROPA.COSCACES,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Scarto da cessione nei parametri distinte")     
        case Nvl(_Curs_CONTROPA.COCONEFA,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Effetti attivi nei parametri distinte")     
      endcase
      * --- Parametri Differenze e Abbuoni
      do case
        case Nvl(_Curs_CONTROPA.COSAABBA,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Abbuoni attivi nei parametri diff. e abbuoni")     
        case Nvl(_Curs_CONTROPA.COSAABBP,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Abbuoni passivi nei parametri diff. e abbuoni")     
        case Nvl(_Curs_CONTROPA.COSADIFC,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Diff. cambi attiva nei parametri diff. e abbuoni")     
        case Nvl(_Curs_CONTROPA.COSADIFN,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Diff. cambi passiva nei parametri diff. e abbuoni")     
        case Nvl(_Curs_CONTROPA.CODIFCON,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Differenze di conversione nei parametri diff. e abbuoni")     
      endcase
      * --- Parametri Ricevute Fiscali
      do case
        case Nvl(_Curs_CONTROPA.COCONIVF,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("IVA sospesa parametri ricevute fiscali")     
        case Nvl(_Curs_CONTROPA.COCONABF,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Abbuoni nei parametri ricevute fiscali")     
        case Nvl(_Curs_CONTROPA.COCONCAF,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Incasso corrispettivi nei parametri ricevute fiscali")     
        case Nvl(_Curs_CONTROPA.COCONCLI,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Crediti v. clienti nei parametri ricevute fiscali")     
      endcase
      * --- Parametri Ritenute
      do case
        case Nvl(_Curs_CONTROPA.COCONRIT,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Erario C/Ritenute nei parametri ritenute")     
        case Nvl(_Curs_CONTROPA.COENASAR,Space(15))=this.oParentObject.w_ANCODICE
          this.w_oMess.AddMsgPartNL("Enasarco nei parametri ritenute")     
      endcase
      this.w_TEST = 1
      this.w_MESS = this.w_oMess.ComposeMessage()
        select _Curs_CONTROPA
        continue
      enddo
      use
    endif
    if this.w_TEST=0
      * --- Select from CATEGOMO
      i_nConn=i_TableProp[this.CATEGOMO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CATEGOMO_idx,2],.t.,this.CATEGOMO_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" CATEGOMO ";
            +" where OMCONRIC="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR OMCONRIM="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+"";
             ,"_Curs_CATEGOMO")
      else
        select Count(*) As Conta from (i_cTable);
         where OMCONRIC=this.oParentObject.w_ANCODICE OR OMCONRIM=this.oParentObject.w_ANCODICE;
          into cursor _Curs_CATEGOMO
      endif
      if used('_Curs_CATEGOMO')
        select _Curs_CATEGOMO
        locate for 1=1
        do while not(eof())
        this.w_TEST = IIF( Nvl( _Curs_CATEGOMO.CONTA , 0 )>0, 2 , 0 )
          select _Curs_CATEGOMO
          continue
        enddo
        use
      endif
    endif
    if this.w_TEST=0
      * --- Controllo Presenza Conto come come Conto IVA Detraibile nella tabella Categorie Contabili 
      * --- Select from CAU_CONT
      i_nConn=i_TableProp[this.CAU_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" CAU_CONT ";
            +" where CCCONIVA="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" ";
             ,"_Curs_CAU_CONT")
      else
        select Count(*) As Conta from (i_cTable);
         where CCCONIVA=this.oParentObject.w_ANCODICE ;
          into cursor _Curs_CAU_CONT
      endif
      if used('_Curs_CAU_CONT')
        select _Curs_CAU_CONT
        locate for 1=1
        do while not(eof())
        this.w_TEST = IIF( Nvl( _Curs_CAU_CONT.CONTA , 0 )>0, 3 , 0 )
          select _Curs_CAU_CONT
          continue
        enddo
        use
      endif
    endif
    if this.w_TEST=0
      * --- Controllo Presenza Conto come Conto Contropartita  negli Automatismi iva 
      *     da Causali Contabili
      * --- Select from CAUIVA1
      i_nConn=i_TableProp[this.CAUIVA1_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAUIVA1_idx,2],.t.,this.CAUIVA1_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" CAUIVA1 ";
            +" where AICONTRO= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+"";
             ,"_Curs_CAUIVA1")
      else
        select Count(*) As Conta from (i_cTable);
         where AICONTRO= this.oParentObject.w_ANCODICE;
          into cursor _Curs_CAUIVA1
      endif
      if used('_Curs_CAUIVA1')
        select _Curs_CAUIVA1
        locate for 1=1
        do while not(eof())
        this.w_TEST = IIF( Nvl( _Curs_CAUIVA1.CONTA , 0 )>0, 4 , 0 )
          select _Curs_CAUIVA1
          continue
        enddo
        use
      endif
    endif
    if this.w_TEST=0
      * --- Select from RIFI_CADO
      i_nConn=i_TableProp[this.RIFI_CADO_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.RIFI_CADO_idx,2],.t.,this.RIFI_CADO_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" RIFI_CADO ";
            +" where RFCONCLI="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR RFCONCAF="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR   RFCONABF="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" OR RFCONIVF="+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+"";
             ,"_Curs_RIFI_CADO")
      else
        select * from (i_cTable);
         where RFCONCLI=this.oParentObject.w_ANCODICE OR RFCONCAF=this.oParentObject.w_ANCODICE OR   RFCONABF=this.oParentObject.w_ANCODICE OR RFCONIVF=this.oParentObject.w_ANCODICE;
          into cursor _Curs_RIFI_CADO
      endif
      if used('_Curs_RIFI_CADO')
        select _Curs_RIFI_CADO
        locate for 1=1
        do while not(eof())
        do case
          case Nvl(_Curs_RIFI_CADO.RFCONCLI,Space(15))=this.oParentObject.w_ANCODICE
            this.w_MESS = ah_Msgformat("Impossibile cancellare, conto presente come conto crediti v.clienti nei parametri ricevute fiscali per causali documento")
          case Nvl(_Curs_RIFI_CADO.RFCONCAF,Space(15))=this.oParentObject.w_ANCODICE
            this.w_MESS = ah_Msgformat("Impossibile cancellare, conto presente come conto incasso corrispettivi nei parametri ricevute fiscali per causali documento")
          case Nvl(_Curs_RIFI_CADO.RFCONABF,Space(15))=this.oParentObject.w_ANCODICE
            this.w_MESS = ah_Msgformat("Impossibile cancellare, conto presente come conto abbuoni nei parametri ricevute fiscali per causali documento")
          case Nvl(_Curs_RIFI_CADO.RFCONIVF,Space(15))=this.oParentObject.w_ANCODICE
            this.w_MESS = ah_Msgformat("Impossibile cancellare, conto presente come conto IVA sospesa nei parametri ricevute fiscali per causali documento")
        endcase
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
        i_retcode = 'stop'
        return
          select _Curs_RIFI_CADO
          continue
        enddo
        use
      endif
    endif
    if this.w_TEST=0
      * --- Controllo Presenza Conto come Conto Collegato nei Conti Banche 
      * --- Select from COC_MAST
      i_nConn=i_TableProp[this.COC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.COC_MAST_idx,2],.t.,this.COC_MAST_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" COC_MAST ";
            +" where BACONCOL= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+"";
             ,"_Curs_COC_MAST")
      else
        select Count(*) As Conta from (i_cTable);
         where BACONCOL= this.oParentObject.w_ANCODICE;
          into cursor _Curs_COC_MAST
      endif
      if used('_Curs_COC_MAST')
        select _Curs_COC_MAST
        locate for 1=1
        do while not(eof())
        this.w_TEST = IIF( Nvl( _Curs_COC_MAST.CONTA , 0 )>0, 6 , 0 )
          select _Curs_COC_MAST
          continue
        enddo
        use
      endif
    endif
    if this.w_TEST=0
      * --- Controllo Presenza Conto come Conto Contropartita  nei Documenti
      * --- Select from DOC_DETT
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" DOC_DETT ";
            +" where MVCONTRO= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" ";
             ,"_Curs_DOC_DETT")
      else
        select Count(*) As Conta from (i_cTable);
         where MVCONTRO= this.oParentObject.w_ANCODICE ;
          into cursor _Curs_DOC_DETT
      endif
      if used('_Curs_DOC_DETT')
        select _Curs_DOC_DETT
        locate for 1=1
        do while not(eof())
        if Nvl( _Curs_DOC_DETT.CONTA , 0 )>0
          * --- Rileggo il dato, la select per ottimizzare il controllo svolge una Count(*)
          * --- Read from DOC_DETT
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CPROWORD,MVSERIAL"+;
              " from "+i_cTable+" DOC_DETT where ";
                  +"MVCONTRO  = "+cp_ToStrODBC(this.oParentObject.w_ANCODICE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CPROWORD,MVSERIAL;
              from (i_cTable) where;
                  MVCONTRO  = this.oParentObject.w_ANCODICE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_NR = NVL(cp_ToDate(_read_.CPROWORD),cp_NullValue(_read_.CPROWORD))
            this.w_SERIALE = NVL(cp_ToDate(_read_.MVSERIAL),cp_NullValue(_read_.MVSERIAL))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVTIPDOC,MVNUMDOC,MVDATDOC"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIALE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVTIPDOC,MVNUMDOC,MVDATDOC;
              from (i_cTable) where;
                  MVSERIAL = this.w_SERIALE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
            this.w_NUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
            this.w_DATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_TEST = 7
        endif
          select _Curs_DOC_DETT
          continue
        enddo
        use
      endif
    endif
    if this.w_TEST=0
      * --- Controllo Presenza Conto come contropartita per pagamenti sui documenti
      * --- Select from CON_PAGA
      i_nConn=i_TableProp[this.CON_PAGA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CON_PAGA_idx,2],.t.,this.CON_PAGA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select MIn( CPSERIAL ) As CPSERIAL  from "+i_cTable+" CON_PAGA ";
            +" where CPCONPRE= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or CPCONCON= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or CPCONASS= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or CPCONCAR= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or CPCONFIN= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" ";
             ,"_Curs_CON_PAGA")
      else
        select MIn( CPSERIAL ) As CPSERIAL from (i_cTable);
         where CPCONPRE= this.oParentObject.w_ANCODICE Or CPCONCON= this.oParentObject.w_ANCODICE Or CPCONASS= this.oParentObject.w_ANCODICE Or CPCONCAR= this.oParentObject.w_ANCODICE Or CPCONFIN= this.oParentObject.w_ANCODICE ;
          into cursor _Curs_CON_PAGA
      endif
      if used('_Curs_CON_PAGA')
        select _Curs_CON_PAGA
        locate for 1=1
        do while not(eof())
        if Not Empty(Nvl( _Curs_CON_PAGA.CPSERIAL , "" ))
          this.w_SERIALE = _Curs_CON_PAGA.CPSERIAL
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVTIPDOC,MVNUMDOC,MVDATDOC"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIALE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVTIPDOC,MVNUMDOC,MVDATDOC;
              from (i_cTable) where;
                  MVSERIAL = this.w_SERIALE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TIPDOC = NVL(cp_ToDate(_read_.MVTIPDOC),cp_NullValue(_read_.MVTIPDOC))
            this.w_NUMDOC = NVL(cp_ToDate(_read_.MVNUMDOC),cp_NullValue(_read_.MVNUMDOC))
            this.w_DATDOC = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_TEST = 8
        endif
          select _Curs_CON_PAGA
          continue
        enddo
        use
      endif
    endif
    if this.w_TEST=0
      * --- Controllo Presenza Conto come contropartita per pagamenti sui documenti
      * --- Select from PAR_VDET
      i_nConn=i_TableProp[this.PAR_VDET_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PAR_VDET_idx,2],.t.,this.PAR_VDET_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" PAR_VDET ";
            +" where PACODAZI="+cp_ToStrODBC(this.w_CODAZI)+" And (PACONPRE= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or PACONCON= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or PACONASS= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or PACONCAR= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or PACONFIN= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" )";
             ,"_Curs_PAR_VDET")
      else
        select Count(*) As Conta from (i_cTable);
         where PACODAZI=this.w_CODAZI And (PACONPRE= this.oParentObject.w_ANCODICE Or PACONCON= this.oParentObject.w_ANCODICE Or PACONASS= this.oParentObject.w_ANCODICE Or PACONCAR= this.oParentObject.w_ANCODICE Or PACONFIN= this.oParentObject.w_ANCODICE );
          into cursor _Curs_PAR_VDET
      endif
      if used('_Curs_PAR_VDET')
        select _Curs_PAR_VDET
        locate for 1=1
        do while not(eof())
        this.w_TEST = IIF( Nvl( _Curs_PAR_VDET.CONTA , 0 )>0, 9 , 0 )
          select _Curs_PAR_VDET
          continue
        enddo
        use
      endif
    endif
    if this.w_TEST=0
      * --- Controllo Presenza Conto come contropartita nei cespiti
      * --- Select from GRU_COCE
      i_nConn=i_TableProp[this.GRU_COCE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.GRU_COCE_idx,2],.t.,this.GRU_COCE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select Count(*) As Conta  from "+i_cTable+" GRU_COCE ";
            +" where GPCONC01= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or GPCONC02= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or GPCONC03= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or GPCONC04= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or GPCONC05= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or GPCONC06= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or GPCONC07= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or GPCONC08= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or GPCONC09= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or GPCONC10= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or GPCONC11= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or   GPCONC12= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or GPCONC13= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or GPCONC14= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or GPCONC15= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+" Or GPCONC16= "+cp_ToStrODBC(this.oParentObject.w_ANCODICE)+"";
             ,"_Curs_GRU_COCE")
      else
        select Count(*) As Conta from (i_cTable);
         where GPCONC01= this.oParentObject.w_ANCODICE Or GPCONC02= this.oParentObject.w_ANCODICE Or GPCONC03= this.oParentObject.w_ANCODICE Or GPCONC04= this.oParentObject.w_ANCODICE Or GPCONC05= this.oParentObject.w_ANCODICE Or GPCONC06= this.oParentObject.w_ANCODICE Or GPCONC07= this.oParentObject.w_ANCODICE Or GPCONC08= this.oParentObject.w_ANCODICE Or GPCONC09= this.oParentObject.w_ANCODICE Or GPCONC10= this.oParentObject.w_ANCODICE Or GPCONC11= this.oParentObject.w_ANCODICE Or   GPCONC12= this.oParentObject.w_ANCODICE Or GPCONC13= this.oParentObject.w_ANCODICE Or GPCONC14= this.oParentObject.w_ANCODICE Or GPCONC15= this.oParentObject.w_ANCODICE Or GPCONC16= this.oParentObject.w_ANCODICE;
          into cursor _Curs_GRU_COCE
      endif
      if used('_Curs_GRU_COCE')
        select _Curs_GRU_COCE
        locate for 1=1
        do while not(eof())
        this.w_TEST = IIF( Nvl( _Curs_GRU_COCE.CONTA , 0 )>0, 10 , 0 )
          select _Curs_GRU_COCE
          continue
        enddo
        use
      endif
    endif
    do case
      case this.w_TEST=2
        this.w_MESS = ah_Msgformat("Impossibile cancellare, conto presente nella tabella categorie omogenee")
      case this.w_TEST=3
        this.w_MESS = ah_Msgformat("Impossibile cancellare, conto presente come conto IVA detraibile nella tabella causali contabili")
      case this.w_TEST=4
        this.w_MESS = ah_Msgformat("Impossibile cancellare, conto presente come conto contropartita negli automatismi IVA")
      case this.w_TEST=6
        this.w_MESS = ah_Msgformat("Impossibile cancellare, conto presente come conto collegato nei conti banche")
      case this.w_TEST=7
        this.w_MESS = ah_Msgformat("Impossibile cancellare, conto presente come contropartita nel documento con causale %1%0Num. %2 del %3 riga %4", Alltrim(this.w_TIPDOC), alltrim(Str(this.w_NUMDOC,15)),dtoc(this.w_DATDOC) , alltrim( Str( this.w_NR)) ) 
      case this.w_TEST=8
        this.w_MESS = ah_Msgformat("Impossibile cancellare, conto presente come contropartita incassi nel documento con causale %1%0Num. %2 del %3 ", Alltrim(this.w_TIPDOC), alltrim(Str(this.w_NUMDOC,15)),dtoc(this.w_DATDOC) , ) 
      case this.w_TEST=9
        this.w_MESS = ah_Msgformat("Impossibile cancellare, conto presente come contropartita incassi nei parametri POS") 
      case this.w_TEST=10
        this.w_MESS = ah_Msgformat("Impossibile cancellare, conto presente come contropartita nei gruppi contabili cespiti") 
    endcase
    if this.w_TEST>0
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,11)]
    this.cWorkTables[1]='CONTROPA'
    this.cWorkTables[2]='RIFI_CADO'
    this.cWorkTables[3]='CATEGOMO'
    this.cWorkTables[4]='CAU_CONT'
    this.cWorkTables[5]='CAUIVA1'
    this.cWorkTables[6]='DOC_DETT'
    this.cWorkTables[7]='DOC_MAST'
    this.cWorkTables[8]='COC_MAST'
    this.cWorkTables[9]='CON_PAGA'
    this.cWorkTables[10]='PAR_VDET'
    this.cWorkTables[11]='GRU_COCE'
    return(this.OpenAllTables(11))

  proc CloseCursors()
    if used('_Curs_CONTROPA')
      use in _Curs_CONTROPA
    endif
    if used('_Curs_CATEGOMO')
      use in _Curs_CATEGOMO
    endif
    if used('_Curs_CAU_CONT')
      use in _Curs_CAU_CONT
    endif
    if used('_Curs_CAUIVA1')
      use in _Curs_CAUIVA1
    endif
    if used('_Curs_RIFI_CADO')
      use in _Curs_RIFI_CADO
    endif
    if used('_Curs_COC_MAST')
      use in _Curs_COC_MAST
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_CON_PAGA')
      use in _Curs_CON_PAGA
    endif
    if used('_Curs_PAR_VDET')
      use in _Curs_PAR_VDET
    endif
    if used('_Curs_GRU_COCE')
      use in _Curs_GRU_COCE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
