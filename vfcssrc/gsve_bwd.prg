* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bwd                                                        *
*              Varia servizi descrittivi                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_341]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-01                                                      *
* Last revis.: 2014-09-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bwd",oParentObject,m.pOper)
return(i_retval)

define class tgsve_bwd as StdBatch
  * --- Local variables
  pOper = space(5)
  w_PROWORD = 0
  w_ZOOM = space(10)
  w_SERIAL = space(10)
  w_ROWNUM = 0
  w_FLEVAS = space(1)
  w_MVDESDOC = space(50)
  w_DESART = space(40)
  w_DESSUP = space(0)
  w_TIPRIG = space(1)
  w_ROWORD = 0
  w_ROWORD2 = 0
  w_ROWRISULT = 0
  w_ULTIMO = 0
  w_ELIMINA = space(1)
  w_REC = 0
  w_OBJ = .NULL.
  w_PROG = .NULL.
  w_OBJ1 = .NULL.
  w_ORIGI = .NULL.
  w_PADRE = .NULL.
  w_GEST = .NULL.
  w_ESISTE = .f.
  w_DOPROW = 0
  w_AGGIO = .f.
  w_SEGNO = space(1)
  w_ARTDES = space(20)
  w_NUMRIF = 0
  w_TPNSRI = space(3)
  w_DES2 = space(40)
  w_STATO = space(1)
  w_FLGEFA = space(1)
  w_EVAS = .f.
  w_MVTFRAGG = space(1)
  w_MVTCAMAG = space(5)
  w_MCAUCOL = space(5)
  w_MFLCASC = space(1)
  w_MFLORDI = space(1)
  w_MFLIMPE = space(1)
  w_MFLRISE = space(1)
  w_MF2CASC = space(1)
  w_MF2ORDI = space(1)
  w_MF2IMPE = space(1)
  w_MF2RISE = space(1)
  w_SERRIF = space(10)
  w_ROWRIF = 0
  w_LOOP = 0
  * --- WorkFile variables
  DOC_MAST_idx=0
  DOC_DETT_idx=0
  ART_ICOL_idx=0
  TIP_DOCU_idx=0
  CAM_AGAZ_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Varia note (da GSVE_KVD)
    if this.pOper<>"DOCUM"
      this.oParentObject.w_FASE = 1
      do case
        case this.pOper="SELEZ"
          this.w_ORIGI = This.oparentobject.oparentobject
          * --- Chiudo la Maschera dello Zoom Documenti (GSVE_KV1)
          if NOT EMPTY(this.oParentObject.w_SERIALE)
            * --- Seleziona il documento da modificare
            this.w_PADRE = This.oparentobject
            this.w_GEST = This.oparentobject.oparentobject
            this.w_GEST.w_SERIALE = this.w_PADRE.w_SERIALE
            this.w_GEST.w_OLDNOTE = this.w_PADRE.w_DESDOC
            this.w_GEST.w_OLDNOTEPN = this.w_PADRE.w_DESDOPN
            This.bUpdateParentObject=.f.
            this.w_GEST.NotifyEvent("Ricerca")     
            * --- Aggiorna il cursore dello zoom
            this.Page_2()
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            this.w_PADRE.EcpSave()     
            this.w_PADRE = .Null.
          endif
        case this.pOper="AGGIO"
          this.w_EVAS = .F.
          this.w_ORIGI = This.oparentobject
          this.w_PADRE = This.oparentobject
          NC = this.w_ORIGI.w_ZOOM.cCursor
          * --- Leggo dalle causali il tipo riga ref. descrizione ordine e il flag genera fatture
          * --- Leggo i campo che mi servono per l'inserimento delle righe descrittive
          * --- Read from DOC_MAST
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "MVTFRAGG,MVTCAMAG"+;
              " from "+i_cTable+" DOC_MAST where ";
                  +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              MVTFRAGG,MVTCAMAG;
              from (i_cTable) where;
                  MVSERIAL = this.oParentObject.w_SERIALE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MVTFRAGG = NVL(cp_ToDate(_read_.MVTFRAGG),cp_NullValue(_read_.MVTFRAGG))
            this.w_MVTCAMAG = NVL(cp_ToDate(_read_.MVTCAMAG),cp_NullValue(_read_.MVTCAMAG))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from TIP_DOCU
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.TIP_DOCU_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.TIP_DOCU_idx,2],.t.,this.TIP_DOCU_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "TDTPNDOC,TFFLGEFA"+;
              " from "+i_cTable+" TIP_DOCU where ";
                  +"TDTIPDOC = "+cp_ToStrODBC(this.oParentObject.w_CAUSALE);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              TDTPNDOC,TFFLGEFA;
              from (i_cTable) where;
                  TDTIPDOC = this.oParentObject.w_CAUSALE;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_TPNSRI = NVL(cp_ToDate(_read_.TDTPNDOC),cp_NullValue(_read_.TDTPNDOC))
            this.w_FLGEFA = NVL(cp_ToDate(_read_.TFFLGEFA),cp_NullValue(_read_.TFFLGEFA))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          * --- Read from CAM_AGAZ
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CMCAUCOL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE"+;
              " from "+i_cTable+" CAM_AGAZ where ";
                  +"CMCODICE = "+cp_ToStrODBC(this.w_MVTCAMAG);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CMCAUCOL,CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
              from (i_cTable) where;
                  CMCODICE = this.w_MVTCAMAG;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_MCAUCOL = NVL(cp_ToDate(_read_.CMCAUCOL),cp_NullValue(_read_.CMCAUCOL))
            this.w_MFLCASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
            this.w_MFLORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
            this.w_MFLIMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
            this.w_MFLRISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          if NOT EMPTY(NVL(this.w_MCAUCOL, " "))
            * --- Read from CAM_AGAZ
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE"+;
                " from "+i_cTable+" CAM_AGAZ where ";
                    +"CMCODICE = "+cp_ToStrODBC(this.w_MCAUCOL);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                CMFLCASC,CMFLORDI,CMFLIMPE,CMFLRISE;
                from (i_cTable) where;
                    CMCODICE = this.w_MCAUCOL;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_MF2CASC = NVL(cp_ToDate(_read_.CMFLCASC),cp_NullValue(_read_.CMFLCASC))
              this.w_MF2ORDI = NVL(cp_ToDate(_read_.CMFLORDI),cp_NullValue(_read_.CMFLORDI))
              this.w_MF2IMPE = NVL(cp_ToDate(_read_.CMFLIMPE),cp_NullValue(_read_.CMFLIMPE))
              this.w_MF2RISE = NVL(cp_ToDate(_read_.CMFLRISE),cp_NullValue(_read_.CMFLRISE))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          * --- verifica se il documento ha righe evase
          if UPPER(g_APPLICATION)="AD HOC ENTERPRISE"
            * --- Select from DOC_DETT
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_DETT ";
                  +" where MVSERIAL="+cp_ToStrODBC(this.oParentObject.w_SERIALE)+" AND (MVFLEVAS='S' OR MVQTAEVA<>0 OR MVIMPEVA<>0)";
                   ,"_Curs_DOC_DETT")
            else
              select * from (i_cTable);
               where MVSERIAL=this.oParentObject.w_SERIALE AND (MVFLEVAS="S" OR MVQTAEVA<>0 OR MVIMPEVA<>0);
                into cursor _Curs_DOC_DETT
            endif
            if used('_Curs_DOC_DETT')
              select _Curs_DOC_DETT
              locate for 1=1
              do while not(eof())
              this.w_EVAS = .T.
              exit
                select _Curs_DOC_DETT
                continue
              enddo
              use
            endif
          else
            * --- Select from DOC_DETT
            i_nConn=i_TableProp[this.DOC_DETT_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_DETT ";
                  +" where MVSERIAL="+cp_ToStrODBC(this.oParentObject.w_SERIALE)+" AND (MVFLEVAS='S' OR ( MVTIPRIG<>'F' AND (MVQTAEVA<>0 OR MVIMPEVA<>0)))";
                   ,"_Curs_DOC_DETT")
            else
              select * from (i_cTable);
               where MVSERIAL=this.oParentObject.w_SERIALE AND (MVFLEVAS="S" OR ( MVTIPRIG<>"F" AND (MVQTAEVA<>0 OR MVIMPEVA<>0)));
                into cursor _Curs_DOC_DETT
            endif
            if used('_Curs_DOC_DETT')
              select _Curs_DOC_DETT
              locate for 1=1
              do while not(eof())
              this.w_EVAS = .T.
              exit
                select _Curs_DOC_DETT
                continue
              enddo
              use
            endif
          endif
          * --- Se il documento � un ddt, ha il flag genera fatture attivo, ha almeno una riga evasa,
          *     anche la riga da inserire deve essere evasa
          if this.oParentObject.w_MVCLADOC="DT" AND this.w_FLGEFA="S" AND this.w_EVAS=.T.
            this.w_STATO = "S"
          else
            this.w_STATO = " "
          endif
          * --- Try
          local bErr_00E37540
          bErr_00E37540=bTrsErr
          this.Try_00E37540()
          * --- Catch
          if !empty(i_Error)
            i_ErrMsg=i_Error
            i_Error=''
            * --- rollback
            bTrsErr=.t.
            cp_EndTrs(.t.)
            ah_ErrorMsg("Impossibile aggiornare il documento: %1",,"", i_errmsg)
          endif
          bTrsErr=bTrsErr or bErr_00E37540
          * --- End
        case this.pOper="ELIMI"
          this.w_ORIGI = This.oparentobject
          * --- Elimina riga descrittiva
          NC = this.w_ORIGI.w_ZOOM.cCursor
          SELECT (NC)
          if RECCOUNT(NC)>0 AND NOT EMPTY(NVL(MVSERIAL," "))
            this.w_FLEVAS = NVL(MVFLEVAS,"")
            this.w_TIPRIG = NVL(MVTIPRIG,"")
            if this.w_FLEVAS<>"S" AND this.w_TIPRIG="D"
              REPLACE ELIMINA WITH "S"
            else
              ah_ErrorMsg("Riga evasa o di tipo non descrittivo: impossibile eliminare")
            endif
            SELECT (NC)
          else
            ah_ErrorMsg("Non ci sono documenti da aggiornare")
          endif
        case this.pOper="MARCA"
          * --- all'after input marca il campo SEGNO e sostituisce MVDESSUP
          this.w_ORIGI = This.oparentobject
          NC = this.w_ORIGI.w_ZOOM.cCursor
          SELECT (NC)
          if RECCOUNT(NC)>0
            REPLACE SEGNO WITH "S"
            REPLACE MVDESSUP WITH this.oParentObject.w_MEMO
            SELECT (NC)
          else
            ah_ErrorMsg("Non ci sono documenti da aggiornare")
          endif
        case this.pOper="INSER"
          if EMPTY(g_ARTDES)
            ah_ErrorMsg("Il campo articolo per riferimenti nei parametri vendite non � valorizzato: impossibile aggiungere righe descrittive")
            i_retcode = 'stop'
            return
          endif
          this.w_ORIGI = This.oparentobject
          NC = this.w_ORIGI.w_ZOOM.cCursor
          * --- Inserisce la riga descrittiva assegnando il cproword
          this.oParentObject.w_FASE = 2
          SELECT(NC)
          SELECT * FROM (NC) INTO CURSOR ASSEGN ORDER BY CPROWORD
          SELECT ASSEGN
          GO BOTTOM
          this.w_ROWORD = CPROWORD
          this.w_ROWRISULT = this.w_ROWORD+10
          SELECT(NC)
          APPEND BLANK
          REPLACE MVCODICE WITH g_ARTDES
          REPLACE MVSERIAL WITH this.oParentObject.w_SERIALE
          REPLACE CPROWORD WITH this.w_ROWRISULT
          REPLACE MVTIPRIG WITH "D"
          SELECT (NC)
          * --- Aggiorna il cursore dello zoom
          if USED("ASSEGN")
             
 SELECT ASSEGN 
 USE
          endif
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
    else
      * --- Apre masch VARIAZIONE DATI DESCRITTIVI DEL DOCUMENTO
      if this.oParentObject.w_MVFLVEAC="V"
        this.w_PROG = GSVE_KVD("V")
      else
        this.w_PROG = GSVE_KVD("A")
      endif
      * --- Controllo se ha passato il test di accesso (lo apro in interrogazione)
      if !(this.w_PROG.bSec1)
        i_retcode = 'stop'
        return
      endif
      this.w_PROG.w_MVSERIAL = this.oParentObject.w_MVSERIAL
      this.w_PROG.w_ORIGINE = "D"
      this.w_PROG.w_SERIALE = this.oParentObject.w_MVSERIAL
      this.w_PROG.w_MVCLADOC = this.oParentObject.w_MVCLADOC
      this.w_PROG.w_CATEGO = this.oParentObject.w_MVCLADOC
      this.w_PROG.w_CATEG1 = this.oParentObject.w_MVCLADOC
      this.w_PROG.w_CATEG2 = this.oParentObject.w_MVCLADOC
      this.w_PROG.w_CATEG3 = this.oParentObject.w_MVCLADOC
      this.w_PROG.w_NUMDO1 = this.oParentObject.w_MVNUMDOC
      this.w_PROG.w_ALFDO1 = this.oParentObject.w_MVALFDOC
      this.w_PROG.w_DATDO1 = this.oParentObject.w_MVDATDOC
      if g_APPLICATION="ADHOC REVOLUTION"
        this.w_PROG.w_DOCFOR = IIF(this.oParentObject.w_FLVEAC="A" AND g_ACQU<>"S", "S","N")
        this.w_PROG.w_NOTE = this.oParentObject.w_MVDESDOC
        this.w_PROG.w_OLDNOTE = this.oParentObject.w_MVDESDOC
      else
        this.w_PROG.w_DOCFOR = IIF(this.oParentObject.w_FLVEAC="A" , "S","N")
        this.w_PROG.w_NOTEPN = this.oParentObject.w_MVNOTEPN
        this.w_PROG.w_OLDNOTEPN = this.oParentObject.w_MVNOTEPN
      endif
      this.w_PROG.w_FLVEAC = this.oParentObject.w_MVFLVEAC
      this.w_PROG.w_TIPCON = this.oParentObject.w_MVTIPCON
      this.w_PROG.w_TIPCONTO = this.oParentObject.w_MVTIPCON
      this.w_PROG.w_DATINI = this.oParentObject.w_MVDATDOC
      this.w_PROG.w_DATFIN = this.oParentObject.w_MVDATDOC
      this.w_OBJ = this.w_PROG.GetcTRL("w_CAUDOC")
      this.w_OBJ.Value = this.oParentObject.w_MVTIPDOC
      this.w_OBJ.valid()     
      this.w_OBJ = this.w_PROG.GetcTRL("w_CAUSALE")
      this.w_OBJ.Value = this.oParentObject.w_MVTIPDOC
      this.w_OBJ.valid()     
      this.w_OBJ1 = this.w_PROG.GetcTRL("w_CODCON")
      this.w_OBJ1.Value = this.oParentObject.w_MVCODCON
      this.w_OBJ1.valid()     
      this.w_OBJ1 = this.w_PROG.GetcTRL("w_CODCON1")
      this.w_OBJ1.Value = this.oParentObject.w_MVCODCON
      this.w_OBJ1.valid()     
      this.w_PROG.w_OBJDOC = this.oParentObject
      this.w_PROG.NotifyEvent("Ricerca")     
      this.w_ORIGI = this.w_PROG
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc
  proc Try_00E37540()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    if UPPER(g_APPLICATION) = "ADHOC REVOLUTION"
      if this.oParentObject.w_NOTE<>this.oParentObject.w_OLDNOTE
        * --- Write into DOC_MAST
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVDESDOC ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOTE),'DOC_MAST','MVDESDOC');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
                 )
        else
          update (i_cTable) set;
              MVDESDOC = this.oParentObject.w_NOTE;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.oParentObject.w_SERIALE;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        this.w_AGGIO = .T.
      endif
    else
      * --- Write into DOC_MAST
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_MAST_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVNOTEPN ="+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_NOTEPN),'DOC_MAST','MVNOTEPN');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
               )
      else
        update (i_cTable) set;
            MVNOTEPN = this.oParentObject.w_NOTEPN;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.oParentObject.w_SERIALE;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_AGGIO = .T.
    endif
    this.w_ARTDES = LEFT(g_ARTDES,20)
    * --- Aggiorna Dati descrittivi (MVDESSUP,MVDESART) 
    SELECT (NC) 
 GO TOP
    * --- Aggiorna se mvdesart � stato modificato,  (NVL(MVDESART,SPACE(40)) <>NVL(DES2,SPACE(40))
    *      se mvdessup � variato, NVL(SEGNO,'')='S'
    *      se � stato inserita una riga o � stato modificato il num. riga di una gi� esistente, CPROWORD<>PROWORD (PROWORD in caso di inserimento � =0, altrimenti � il num. riga originario)
    *      se � stata eliminata una riga ELIMINA='S'
    SCAN FOR NOT EMPTY(NVL(MVSERIAL," ")) AND (NVL(MVDESART,SPACE(40)) <>NVL(DES2,SPACE(40)) OR NVL(SEGNO,"")="S" OR ELIMINA="S" OR CPROWORD<>PROWORD)
    this.w_AGGIO = .T.
    this.w_ROWNUM = nvl(CPROWNUM,0)
    this.w_DESART = NVL(MVDESART,SPACE(40))
    this.w_DES2 = NVL(DES2,SPACE(40))
    this.w_DESSUP = NVL(MVDESSUP,SPACE(10))
    this.w_ROWORD = CPROWORD
    this.w_PROWORD = PROWORD
    this.w_ELIMINA = ELIMINA
    this.w_SEGNO = NVL(SEGNO,"")
    this.w_NUMRIF = NVL(MVNUMRIF,0)
    * --- Se non elimino verifico la riga prima di scrivere sul database
    if this.w_ELIMINA<>"S"
      if this.w_SEGNO="S" AND NOT EMPTY(this.w_DESSUP) And Not CHKMEMO(this.w_DESSUP)
        * --- blocca la procedura se il campo memo � pi� lungo di quanto il database consenta (per oracle e db2)
        *     la raise non ha il messaggio in quanto � gi� presente nella funzione chkmemo
        * --- Raise
        i_Error=ah_Msgformat("Diminuire la lunghezza della descr.supplementare")
        return
      endif
      if this.w_ROWORD=0
        * --- Raise
        i_Error=ah_Msgformat("La riga deve essere diversa da zero")
        return
      endif
      if this.w_ROWORD<>this.w_PROWORD
        * --- Recupera il cprownum maggiore e verifica che il numero riga non sia gi� presente
        * --- Select from DOC_DETT
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CPROWNUM,CPROWORD  from "+i_cTable+" DOC_DETT ";
              +" where MVSERIAL="+cp_ToStrODBC(this.oParentObject.w_SERIALE)+"";
              +" order by CPROWNUM";
               ,"_Curs_DOC_DETT")
        else
          select CPROWNUM,CPROWORD from (i_cTable);
           where MVSERIAL=this.oParentObject.w_SERIALE;
           order by CPROWNUM;
            into cursor _Curs_DOC_DETT
        endif
        if used('_Curs_DOC_DETT')
          select _Curs_DOC_DETT
          locate for 1=1
          do while not(eof())
          this.w_ULTIMO = NVL(CPROWNUM,0)
          if this.w_ROWORD=_Curs_DOC_DETT.CPROWORD
            this.w_DOPROW = _Curs_DOC_DETT.CPROWORD
            this.w_ESISTE = .T.
          endif
            select _Curs_DOC_DETT
            continue
          enddo
          use
        endif
        if this.w_ESISTE
          * --- Raise
          i_Error=ah_Msgformat("Riga num. %1 gi� presente nel documento", ALLTRIM(STR(this.w_DOPROW)) )
          return
        endif
      endif
    endif
    do case
      case this.w_ELIMINA="S"
        * --- Se la riga � marcata come da eliminare, la elimina ( i controlli su riga evasa e riga non descrittiva sono precedenti)
        * --- Read from DOC_DETT
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "MVSERRIF,MVROWRIF"+;
            " from "+i_cTable+" DOC_DETT where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            MVSERRIF,MVROWRIF;
            from (i_cTable) where;
                MVSERIAL = this.oParentObject.w_SERIALE;
                and MVNUMRIF = this.w_NUMRIF;
                and CPROWNUM = this.w_ROWNUM;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_SERRIF = NVL(cp_ToDate(_read_.MVSERRIF),cp_NullValue(_read_.MVSERRIF))
          this.w_ROWRIF = NVL(cp_ToDate(_read_.MVROWRIF),cp_NullValue(_read_.MVROWRIF))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY(this.w_SERRIF)
          * --- Se la riga che eliminer� evade la riga di altro documento smarco l'evasione dalla riga d'origine
          * --- Write into DOC_DETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVFLEVAS ="+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLEVAS');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERRIF);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWRIF);
                   )
          else
            update (i_cTable) set;
                MVFLEVAS = " ";
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERRIF;
                and CPROWNUM = this.w_ROWRIF;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        * --- Delete from DOC_DETT
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
                +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
                 )
        else
          delete from (i_cTable) where;
                MVSERIAL = this.oParentObject.w_SERIALE;
                and CPROWNUM = this.w_ROWNUM;
                and MVNUMRIF = this.w_NUMRIF;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
      case this.w_PROWORD=0 
        * --- Se la riga � nuova inserisce riga descrittiva
        this.w_ULTIMO = this.w_ULTIMO +1
        if g_APPLICATION="ADHOC REVOLUTION"
          * --- Insert into DOC_DETT
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"MVSERIAL"+",CPROWORD"+",MVCODICE"+",MVDESART"+",MVDESSUP"+",CPROWNUM"+",MVNUMRIF"+",MVTIPRIG"+",MVCODART"+",MVCODCLA"+",MVFLEVAS"+",MVDATEVA"+",MVFLRAGG"+",MVCAUMAG"+",MVCODMAG"+",MVCODMAT"+",MVCAUCOL"+",MVFLCASC"+",MVFLORDI"+",MVFLIMPE"+",MVFLRISE"+",MVF2CASC"+",MVF2ORDI"+",MVF2IMPE"+",MVF2RISE"+",MVCODCOM"+",MVINICOM"+",MVFINCOM"+",MVKEYSAL"+",MVPREZZO"+",MVQTAMOV"+",MVQTAUM1"+",MVQTAIMP"+",MVQTAIM1"+",MVVALRIG"+",MVVALMAG"+",MVIMPNAZ"+",MVVALULT"+",MVFLULPV"+",MVIMPCOM"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SERIALE),'DOC_DETT','MVSERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'DOC_DETT','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ARTDES),'DOC_DETT','MVCODICE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DESART),'DOC_DETT','MVDESART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'DOC_DETT','MVDESSUP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ULTIMO),'DOC_DETT','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(-20),'DOC_DETT','MVNUMRIF');
            +","+cp_NullLink(cp_ToStrODBC("D"),'DOC_DETT','MVTIPRIG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ARTDES),'DOC_DETT','MVCODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TPNSRI),'DOC_DETT','MVCODCLA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_STATO),'DOC_DETT','MVFLEVAS');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATDO1),'DOC_DETT','MVDATEVA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVTFRAGG),'DOC_DETT','MVFLRAGG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCAMAG),'DOC_DETT','MVCAUMAG');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAG');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAT');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MCAUCOL),'DOC_DETT','MVCAUCOL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MFLCASC),'DOC_DETT','MVFLCASC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MFLORDI),'DOC_DETT','MVFLORDI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MFLIMPE),'DOC_DETT','MVFLIMPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MFLRISE),'DOC_DETT','MVFLRISE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MF2CASC),'DOC_DETT','MVF2CASC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MF2ORDI),'DOC_DETT','MVF2ORDI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MF2IMPE),'DOC_DETT','MVF2IMPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MF2RISE),'DOC_DETT','MVF2RISE');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'DOC_DETT','MVCODCOM');
            +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVINICOM');
            +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVFINCOM');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'DOC_DETT','MVKEYSAL');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVPREZZO');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAMOV');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAUM1');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIMP');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAIM1');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALRIG');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALMAG');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPNAZ');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALULT');
            +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLULPV');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPCOM');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'MVSERIAL',this.oParentObject.w_SERIALE,'CPROWORD',this.w_ROWORD,'MVCODICE',this.w_ARTDES,'MVDESART',this.w_DESART,'MVDESSUP',this.w_DESSUP,'CPROWNUM',this.w_ULTIMO,'MVNUMRIF',-20,'MVTIPRIG',"D",'MVCODART',this.w_ARTDES,'MVCODCLA',this.w_TPNSRI,'MVFLEVAS',this.w_STATO,'MVDATEVA',this.oParentObject.w_DATDO1)
            insert into (i_cTable) (MVSERIAL,CPROWORD,MVCODICE,MVDESART,MVDESSUP,CPROWNUM,MVNUMRIF,MVTIPRIG,MVCODART,MVCODCLA,MVFLEVAS,MVDATEVA,MVFLRAGG,MVCAUMAG,MVCODMAG,MVCODMAT,MVCAUCOL,MVFLCASC,MVFLORDI,MVFLIMPE,MVFLRISE,MVF2CASC,MVF2ORDI,MVF2IMPE,MVF2RISE,MVCODCOM,MVINICOM,MVFINCOM,MVKEYSAL,MVPREZZO,MVQTAMOV,MVQTAUM1,MVQTAIMP,MVQTAIM1,MVVALRIG,MVVALMAG,MVIMPNAZ,MVVALULT,MVFLULPV,MVIMPCOM &i_ccchkf. );
               values (;
                 this.oParentObject.w_SERIALE;
                 ,this.w_ROWORD;
                 ,this.w_ARTDES;
                 ,this.w_DESART;
                 ,this.w_DESSUP;
                 ,this.w_ULTIMO;
                 ,-20;
                 ,"D";
                 ,this.w_ARTDES;
                 ,this.w_TPNSRI;
                 ,this.w_STATO;
                 ,this.oParentObject.w_DATDO1;
                 ,this.w_MVTFRAGG;
                 ,this.w_MVTCAMAG;
                 ,SPACE(5);
                 ,SPACE(5);
                 ,this.w_MCAUCOL;
                 ,this.w_MFLCASC;
                 ,this.w_MFLORDI;
                 ,this.w_MFLIMPE;
                 ,this.w_MFLRISE;
                 ,this.w_MF2CASC;
                 ,this.w_MF2ORDI;
                 ,this.w_MF2IMPE;
                 ,this.w_MF2RISE;
                 ,SPACE(15);
                 ,cp_CharToDate("  -  -  ");
                 ,cp_CharToDate("  -  -  ");
                 ,SPACE(20);
                 ,0;
                 ,0;
                 ,0;
                 ,0;
                 ,0;
                 ,0;
                 ,0;
                 ,0;
                 ,0;
                 ," ";
                 ,0;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        else
          * --- Insert into DOC_DETT
          i_nConn=i_TableProp[this.DOC_DETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.DOC_DETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"CPROWNUM"+",CPROWORD"+",MVCAUCOL"+",MVCAUMAG"+",MVCODART"+",MVCODCLA"+",MVCODCOM"+",MVCODICE"+",MVCODMAG"+",MVCODMAT"+",MVDATEVA"+",MVDESART"+",MVDESSUP"+",MVF2CASC"+",MVF2IMPE"+",MVF2ORDI"+",MVF2RISE"+",MVFINCOM"+",MVFLCASC"+",MVFLEVAS"+",MVFLIMPE"+",MVFLORDI"+",MVFLRAGG"+",MVFLRISE"+",MVFLULPV"+",MVIMPCOM"+",MVIMPNAZ"+",MVINICOM"+",MVKEYSAL"+",MVNUMRIF"+",MVPREZZO"+",MVQTAMOV"+",MVQTAUM1"+",MVSERIAL"+",MVTIPRIG"+",MVVALMAG"+",MVVALRIG"+",MVVALULT"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_ULTIMO),'DOC_DETT','CPROWNUM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'DOC_DETT','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MCAUCOL),'DOC_DETT','MVCAUCOL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVTCAMAG),'DOC_DETT','MVCAUMAG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ARTDES),'DOC_DETT','MVCODART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_TPNSRI),'DOC_DETT','MVCODCLA');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(15)),'DOC_DETT','MVCODCOM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_ARTDES),'DOC_DETT','MVCODICE');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAG');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(5)),'DOC_DETT','MVCODMAT');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_DATDO1),'DOC_DETT','MVDATEVA');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DESART),'DOC_DETT','MVDESART');
            +","+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'DOC_DETT','MVDESSUP');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MF2CASC),'DOC_DETT','MVF2CASC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MF2IMPE),'DOC_DETT','MVF2IMPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MF2ORDI),'DOC_DETT','MVF2ORDI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MF2RISE),'DOC_DETT','MVF2RISE');
            +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVFINCOM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MFLCASC),'DOC_DETT','MVFLCASC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_STATO),'DOC_DETT','MVFLEVAS');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MFLIMPE),'DOC_DETT','MVFLIMPE');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MFLORDI),'DOC_DETT','MVFLORDI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MVTFRAGG),'DOC_DETT','MVFLRAGG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_MFLRISE),'DOC_DETT','MVFLRISE');
            +","+cp_NullLink(cp_ToStrODBC(" "),'DOC_DETT','MVFLULPV');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPCOM');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVIMPNAZ');
            +","+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'DOC_DETT','MVINICOM');
            +","+cp_NullLink(cp_ToStrODBC(SPACE(20)),'DOC_DETT','MVKEYSAL');
            +","+cp_NullLink(cp_ToStrODBC(-20),'DOC_DETT','MVNUMRIF');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVPREZZO');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAMOV');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVQTAUM1');
            +","+cp_NullLink(cp_ToStrODBC(this.oParentObject.w_SERIALE),'DOC_DETT','MVSERIAL');
            +","+cp_NullLink(cp_ToStrODBC("D"),'DOC_DETT','MVTIPRIG');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALMAG');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALRIG');
            +","+cp_NullLink(cp_ToStrODBC(0),'DOC_DETT','MVVALULT');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'CPROWNUM',this.w_ULTIMO,'CPROWORD',this.w_ROWORD,'MVCAUCOL',this.w_MCAUCOL,'MVCAUMAG',this.w_MVTCAMAG,'MVCODART',this.w_ARTDES,'MVCODCLA',this.w_TPNSRI,'MVCODCOM',SPACE(15),'MVCODICE',this.w_ARTDES,'MVCODMAG',SPACE(5),'MVCODMAT',SPACE(5),'MVDATEVA',this.oParentObject.w_DATDO1,'MVDESART',this.w_DESART)
            insert into (i_cTable) (CPROWNUM,CPROWORD,MVCAUCOL,MVCAUMAG,MVCODART,MVCODCLA,MVCODCOM,MVCODICE,MVCODMAG,MVCODMAT,MVDATEVA,MVDESART,MVDESSUP,MVF2CASC,MVF2IMPE,MVF2ORDI,MVF2RISE,MVFINCOM,MVFLCASC,MVFLEVAS,MVFLIMPE,MVFLORDI,MVFLRAGG,MVFLRISE,MVFLULPV,MVIMPCOM,MVIMPNAZ,MVINICOM,MVKEYSAL,MVNUMRIF,MVPREZZO,MVQTAMOV,MVQTAUM1,MVSERIAL,MVTIPRIG,MVVALMAG,MVVALRIG,MVVALULT &i_ccchkf. );
               values (;
                 this.w_ULTIMO;
                 ,this.w_ROWORD;
                 ,this.w_MCAUCOL;
                 ,this.w_MVTCAMAG;
                 ,this.w_ARTDES;
                 ,this.w_TPNSRI;
                 ,SPACE(15);
                 ,this.w_ARTDES;
                 ,SPACE(5);
                 ,SPACE(5);
                 ,this.oParentObject.w_DATDO1;
                 ,this.w_DESART;
                 ,this.w_DESSUP;
                 ,this.w_MF2CASC;
                 ,this.w_MF2IMPE;
                 ,this.w_MF2ORDI;
                 ,this.w_MF2RISE;
                 ,cp_CharToDate("  -  -  ");
                 ,this.w_MFLCASC;
                 ,this.w_STATO;
                 ,this.w_MFLIMPE;
                 ,this.w_MFLORDI;
                 ,this.w_MVTFRAGG;
                 ,this.w_MFLRISE;
                 ," ";
                 ,0;
                 ,0;
                 ,cp_CharToDate("  -  -  ");
                 ,SPACE(20);
                 ,-20;
                 ,0;
                 ,0;
                 ,0;
                 ,this.oParentObject.w_SERIALE;
                 ,"D";
                 ,0;
                 ,0;
                 ,0;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
        endif
      case this.w_DESART <>this.w_DES2 OR this.w_SEGNO="S" OR this.w_ROWORD<>this.w_PROWORD
        * --- Se la riga esisteva gi� aggiorna decrizione, descrizione supplementare e numero riga
        * --- Write into DOC_DETT
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.DOC_DETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"MVDESART ="+cp_NullLink(cp_ToStrODBC(this.w_DESART),'DOC_DETT','MVDESART');
          +",MVDESSUP ="+cp_NullLink(cp_ToStrODBC(this.w_DESSUP),'DOC_DETT','MVDESSUP');
          +",CPROWORD ="+cp_NullLink(cp_ToStrODBC(this.w_ROWORD),'DOC_DETT','CPROWORD');
              +i_ccchkf ;
          +" where ";
              +"MVSERIAL = "+cp_ToStrODBC(this.oParentObject.w_SERIALE);
              +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
                 )
        else
          update (i_cTable) set;
              MVDESART = this.w_DESART;
              ,MVDESSUP = this.w_DESSUP;
              ,CPROWORD = this.w_ROWORD;
              &i_ccchkf. ;
           where;
              MVSERIAL = this.oParentObject.w_SERIALE;
              and CPROWNUM = this.w_ROWNUM;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
    endcase
    SELECT (NC)
    ENDSCAN
    * --- commit
    cp_EndTrs(.t.)
    if this.w_AGGIO
      ah_ErrorMsg("Aggiornamento terminato con successo")
    endif
    * --- Se la maschera � lanciata dai documenti chiude la maschera e refresha il documento
    if type("w_OBJDOC.cPrg")="C" and this.oParentObject.w_ORIGINE ="D"
      this.oParentObject.w_OBJDOC.LoadRecWarn()     
      this.w_PADRE.ecpQuit()     
    else
      * --- Aggiorna il cursore dello zoom
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricerca
    if this.pOPER<>"DOCUM" AND this.pOPER<>"ELIMI" AND this.oParentObject.w_FASE=1
      This.OparentObject.NotifyEvent("Ricerca")
    endif
    this.w_LOOP = 1
    do while this.w_LOOP<=this.w_ORIGI.w_Zoom.grd.ColumnCount
      this.w_ORIGI.w_Zoom.grd.Columns[ this.w_LOOP ].Enabled = UPPER(this.w_ORIGI.w_Zoom.grd.Columns[ this.w_LOOP ].ControlSource)$"MVDESART-CPROWORD"
      this.w_ORIGI.w_Zoom.grd.Columns[ this.w_LOOP ].DynamicBackColor = "IIF(ELIMINA='S',RGB(255,255,0),RGB(255,255,255))"
      this.w_ORIGI.w_Zoom.grd.Columns[ this.w_LOOP ].DynamicForeColor = "IIF((MVDESART<>DES2),RGB(255,0,0),RGB(0,0,0))"
      this.w_LOOP = this.w_LOOP + 1
    enddo
    * --- Setta Proprieta' Campi del Cursore
    SELECT (this.w_ORIGI.w_ZOOM.cCursor)
    GO TOP
    this.w_ORIGI.w_ZOOM.refresh()     
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='ART_ICOL'
    this.cWorkTables[4]='TIP_DOCU'
    this.cWorkTables[5]='CAM_AGAZ'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
