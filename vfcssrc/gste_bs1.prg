* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bs1                                                        *
*              Calcolo fino a giorni scaduto                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-05-30                                                      *
* Last revis.: 2014-06-05                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_pParam
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bs1",oParentObject,m.w_pParam)
return(i_retval)

define class tgste_bs1 as StdBatch
  * --- Local variables
  w_pParam = space(10)
  w_GIOSCA = 0
  w_NUMROW = 0
  w_PADRE = .NULL.
  w_NUMMAX = 0
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo fino a giorni scaduto
    this.w_PADRE = this.oParentObject
    do case
      case this.w_pParam="SSGIOSCA"
        this.w_PADRE.MarkPos()     
        this.w_PADRE.FirstRow()     
        do while NOT this.w_PADRE.Eof_Trs() and this.w_PADRE.FullRow()
          this.w_NUMROW = this.w_PADRE.RowIndex()
          this.w_GIOSCA = this.w_PADRE.GET("t_SSGIOSCA")
          this.w_PADRE.Exec_Select("CheckRow", "Min(t_SSGIOSCA) as NUMMAX" , "t_SSGIOSCA>"+ cp_ToStrODBC(this.w_GIOSCA) ,"","","")     
          if CheckRow.NUMMAX>0
            this.w_NUMMAX = CheckRow.NUMMAX - 1
          else
            this.w_NUMMAX = 999
          endif
          Use in CheckRow
          this.w_PADRE.SetRow(this.w_NUMROW)     
          this.oParentObject.w_SSGIOSCF = this.w_NUMMAX
          this.w_PADRE.SaveRow()     
          this.w_PADRE.NextRow()     
        enddo
        this.w_PADRE.RePos()     
      case this.w_pParam="SSGIOSCF"
        this.w_PADRE.MarkPos()     
        this.w_PADRE.FirstRow()     
        do while NOT this.w_PADRE.Eof_Trs() and this.w_PADRE.FullRow()
          this.w_NUMROW = this.w_PADRE.RowIndex()
          this.w_GIOSCA = this.w_PADRE.GET("t_SSGIOSCF")
          this.w_PADRE.Exec_Select("CheckRow", "Max(t_SSGIOSCF) as NUMMAX" , "t_SSGIOSCF<"+ cp_ToStrODBC(this.w_GIOSCA) ,"","","")     
          if CheckRow.NUMMAX>0
            this.w_NUMMAX = CheckRow.NUMMAX + 1
          else
            this.w_NUMMAX = 1
          endif
          Use in CheckRow
          this.w_PADRE.SetRow(this.w_NUMROW)     
          this.oParentObject.w_SSGIOSCA = this.w_NUMMAX
          this.w_PADRE.SaveRow()     
          this.w_PADRE.NextRow()     
        enddo
        this.w_PADRE.RePos()     
      case this.w_pParam="INITROW"
        this.w_PADRE.Exec_Select("CheckRow", "Max(t_SSGIOSCF) as NUMMAX" , "" ,"","","")     
        if CheckRow.NUMMAX<999
          this.w_NUMMAX = CheckRow.NUMMAX + 1
        endif
        Use in CheckRow
        this.oParentObject.w_SSGIOSCF = 999
        this.bUpdateParentObject=.f.
        i_retcode = 'stop'
        i_retval = this.w_NUMMAX
        return
    endcase
  endproc


  proc Init(oParentObject,w_pParam)
    this.w_pParam=w_pParam
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_pParam"
endproc
