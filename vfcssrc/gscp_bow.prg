* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_bow                                                        *
*              Valorizzazione zoom controllo prezzi e sconti                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2014-09-17                                                      *
* Last revis.: 2014-10-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscp_bow",oParentObject)
return(i_retval)

define class tgscp_bow as StdBatch
  * --- Local variables
  w_SERIALE = space(10)
  w_COSTO = 0
  w_GESTPREZZO = 0
  w_GESTCONT1 = 0
  w_GESTCONT2 = 0
  w_GESTCONT3 = 0
  w_GESTCONT4 = 0
  w_ZOOM = .NULL.
  * --- WorkFile variables
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Valorizzazione dello zoom controllo prezzi e sconti
    this.w_ZOOM = This.OparentObject.w_ZoomSel
    this.w_SERIALE = This.oParentObject.oParentObject.w_ORSERIAL
    vq_exec(".\query\GSCP3BGO.VQR",this,"CHECKORDI")
    Wrcursor("CHECKORDI")
    Select CHECKORDI 
 Go top 
 Scan
    DECLARE L_ArrRet (16,1)
    this.w_COSTO = gsar_bpz(this,CHECKORDI.ORCODART,CHECKORDI.ORCODCON,CHECKORDI.ORTIPCON,CHECKORDI.ORCODLIS,CHECKORDI.ORQTAMOV,CHECKORDI.ORCODVAL,CHECKORDI.ORDATDOC,CHECKORDI.ORUNIMIS, " ")
    this.w_GESTCONT1 =  L_ArrRet[1,1]
    Replace GESTCONT1 with this.w_GESTCONT1
    this.w_GESTCONT2 =  L_ArrRet[2,1]
    Replace GESTCONT2 with this.w_GESTCONT2
    this.w_GESTCONT3 =  L_ArrRet[3,1]
    Replace GESTCONT3 with this.w_GESTCONT3
    this.w_GESTCONT4 =  L_ArrRet[4,1]
    Replace GESTCONT4 with this.w_GESTCONT4
    this.w_GESTPREZZO =  L_ArrRet[5,1]
    Replace GESTPREZZO with this.w_GESTPREZZO
    Endscan
    Select Cproword,Orcodice,Ordesart,Orunimis,Orqtamov,Orprezzo,; 
 Orscont1,Orscont2,Orscont3,Orscont4,Gestprezzo,Gestcont1,; 
 Gestcont2,Gestcont3,Gestcont4,iif(Orprezzo<>Gestprezzo,"S","N") as Flagprezzo,; 
 iif(Orscont1<>Gestcont1,"S","N") as Flagsconto1,iif(Orscont2<>Gestcont2,"S","N") as Flagsconto2,; 
 iif(Orscont3<>Gestcont3,"S","N") as Flagsconto3,iif(Orscont4<>Gestcont4,"S","N") as Flagsconto4; 
 from CheckOrdi into cursor Temp; 
 where Orprezzo<>Gestprezzo Or Orscont1<>Gestcont1 Or Orscont2<>Gestcont2 Or Orscont3<>Gestcont3 or Orscont4<>Gestcont4
    Select Temp
    Go Top
    Scan
    Scatter Memvar
    Select (this.w_ZOOM.cCursor)
    Append blank
    Gather memvar
    Endscan
    use in select("CheckOrdi")
    use in select("Temp")
    Select ( this.w_Zoom.cCursor )
    GO TOP
    * --- Refresh della griglia
    this.w_zoom.grd.refresh
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
