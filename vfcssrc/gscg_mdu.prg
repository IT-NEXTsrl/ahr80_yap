* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_mdu                                                        *
*              Dettaglio importo utilizzato                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2017-07-25                                                      *
* Last revis.: 2017-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgscg_mdu")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgscg_mdu")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgscg_mdu")
  return

* --- Class definition
define class tgscg_mdu as StdPCForm
  Width  = 582
  Height = 257
  Top    = 10
  Left   = 10
  cComment = "Dettaglio importo utilizzato"
  cPrg = "gscg_mdu"
  HelpContextID=80356713
  add object cnt as tcgscg_mdu
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgscg_mdu as PCContext
  w_DDSERIAL = space(15)
  w_CPROWORD = 0
  w_DDIMPDIC = 0
  w_DDDOCSER = space(10)
  w_DDTIPINS = space(1)
  w_DDDATOPE = space(8)
  w_DDTIPAGG = space(1)
  w_DDCODUTE = 0
  w_DDNOTOPE = space(100)
  w_SERIALE = space(10)
  w_DDVISMAN = space(1)
  w_DDRECCOL = space(10)
  proc Save(i_oFrom)
    this.w_DDSERIAL = i_oFrom.w_DDSERIAL
    this.w_CPROWORD = i_oFrom.w_CPROWORD
    this.w_DDIMPDIC = i_oFrom.w_DDIMPDIC
    this.w_DDDOCSER = i_oFrom.w_DDDOCSER
    this.w_DDTIPINS = i_oFrom.w_DDTIPINS
    this.w_DDDATOPE = i_oFrom.w_DDDATOPE
    this.w_DDTIPAGG = i_oFrom.w_DDTIPAGG
    this.w_DDCODUTE = i_oFrom.w_DDCODUTE
    this.w_DDNOTOPE = i_oFrom.w_DDNOTOPE
    this.w_SERIALE = i_oFrom.w_SERIALE
    this.w_DDVISMAN = i_oFrom.w_DDVISMAN
    this.w_DDRECCOL = i_oFrom.w_DDRECCOL
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_DDSERIAL = this.w_DDSERIAL
    i_oTo.w_CPROWORD = this.w_CPROWORD
    i_oTo.w_DDIMPDIC = this.w_DDIMPDIC
    i_oTo.w_DDDOCSER = this.w_DDDOCSER
    i_oTo.w_DDTIPINS = this.w_DDTIPINS
    i_oTo.w_DDDATOPE = this.w_DDDATOPE
    i_oTo.w_DDTIPAGG = this.w_DDTIPAGG
    i_oTo.w_DDCODUTE = this.w_DDCODUTE
    i_oTo.w_DDNOTOPE = this.w_DDNOTOPE
    i_oTo.w_SERIALE = this.w_SERIALE
    i_oTo.w_DDVISMAN = this.w_DDVISMAN
    i_oTo.w_DDRECCOL = this.w_DDRECCOL
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgscg_mdu as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 582
  Height = 257
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-11-08"
  HelpContextID=80356713
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=12

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  DICDINTE_IDX = 0
  DOC_MAST_IDX = 0
  TIP_DOCU_IDX = 0
  cFile = "DICDINTE"
  cKeySelect = "DDSERIAL"
  cKeyWhere  = "DDSERIAL=this.w_DDSERIAL"
  cKeyDetail  = "DDSERIAL=this.w_DDSERIAL"
  cKeyWhereODBC = '"DDSERIAL="+cp_ToStrODBC(this.w_DDSERIAL)';

  cKeyDetailWhereODBC = '"DDSERIAL="+cp_ToStrODBC(this.w_DDSERIAL)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"DICDINTE.DDSERIAL="+cp_ToStrODBC(this.w_DDSERIAL)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'DICDINTE.DDDATOPE,DICDINTE.CPROWORD'
  cPrg = "gscg_mdu"
  cComment = "Dettaglio importo utilizzato"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_DDSERIAL = space(15)
  w_CPROWORD = 0
  w_DDIMPDIC = 0
  w_DDDOCSER = space(10)
  w_DDTIPINS = space(1)
  w_DDDATOPE = ctod('  /  /  ')
  w_DDTIPAGG = space(1)
  w_DDCODUTE = 0
  w_DDNOTOPE = space(100)
  w_SERIALE = space(10)
  w_DDVISMAN = space(1)
  w_DDRECCOL = space(10)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg_mduPag1","gscg_mdu",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[3]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='TIP_DOCU'
    this.cWorkTables[3]='DICDINTE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(3))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.DICDINTE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.DICDINTE_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgscg_mdu'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from DICDINTE where DDSERIAL=KeySet.DDSERIAL
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.DICDINTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DICDINTE_IDX,2],this.bLoadRecFilter,this.DICDINTE_IDX,"gscg_mdu")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('DICDINTE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "DICDINTE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' DICDINTE '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'DDSERIAL',this.w_DDSERIAL  )
      select * from (i_cTable) DICDINTE where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DDSERIAL = NVL(DDSERIAL,space(15))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'DICDINTE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_SERIALE = space(10)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_CPROWORD = NVL(CPROWORD,0)
          .w_DDIMPDIC = NVL(DDIMPDIC,0)
          .w_DDDOCSER = NVL(DDDOCSER,space(10))
          .w_DDTIPINS = NVL(DDTIPINS,space(1))
          .w_DDDATOPE = NVL(cp_ToDate(DDDATOPE),ctod("  /  /  "))
          .w_DDTIPAGG = NVL(DDTIPAGG,space(1))
          .w_DDCODUTE = NVL(DDCODUTE,0)
          .w_DDNOTOPE = NVL(DDNOTOPE,space(100))
          .w_DDVISMAN = NVL(DDVISMAN,space(1))
          .w_DDRECCOL = NVL(DDRECCOL,space(10))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_DDSERIAL=space(15)
      .w_CPROWORD=10
      .w_DDIMPDIC=0
      .w_DDDOCSER=space(10)
      .w_DDTIPINS=space(1)
      .w_DDDATOPE=ctod("  /  /  ")
      .w_DDTIPAGG=space(1)
      .w_DDCODUTE=0
      .w_DDNOTOPE=space(100)
      .w_SERIALE=space(10)
      .w_DDVISMAN=space(1)
      .w_DDRECCOL=space(10)
      if .cFunction<>"Filter"
        .DoRTCalc(1,4,.f.)
        .w_DDTIPINS = 'M'
        .DoRTCalc(6,6,.f.)
        .w_DDTIPAGG = 'I'
        .w_DDCODUTE = i_Codute
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'DICDINTE')
    this.DoRTCalc(9,12,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oDDNOTOPE_2_8.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'DICDINTE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.DICDINTE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_DDSERIAL,"DDSERIAL",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_CPROWORD N(7);
      ,t_DDIMPDIC N(18,4);
      ,t_DDTIPINS N(3);
      ,t_DDDATOPE D(8);
      ,t_DDTIPAGG N(3);
      ,t_DDCODUTE N(4);
      ,t_DDNOTOPE C(100);
      ,CPROWNUM N(10);
      ,t_DDDOCSER C(10);
      ,t_SERIALE C(10);
      ,t_DDVISMAN C(1);
      ,t_DDRECCOL C(10);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgscg_mdubodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.controlsource=this.cTrsName+'.t_CPROWORD'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDDIMPDIC_2_2.controlsource=this.cTrsName+'.t_DDIMPDIC'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPINS_2_4.controlsource=this.cTrsName+'.t_DDTIPINS'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDDDATOPE_2_5.controlsource=this.cTrsName+'.t_DDDATOPE'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPAGG_2_6.controlsource=this.cTrsName+'.t_DDTIPAGG'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oDDCODUTE_2_7.controlsource=this.cTrsName+'.t_DDCODUTE'
    this.oPgFRm.Page1.oPag.oDDNOTOPE_2_8.controlsource=this.cTrsName+'.t_DDNOTOPE'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(74)
    this.AddVLine(225)
    this.AddVLine(322)
    this.AddVLine(416)
    this.AddVLine(469)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.DICDINTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DICDINTE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.DICDINTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DICDINTE_IDX,2])
      *
      * insert into DICDINTE
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'DICDINTE')
        i_extval=cp_InsertValODBCExtFlds(this,'DICDINTE')
        i_cFldBody=" "+;
                  "(DDSERIAL,CPROWORD,DDIMPDIC,DDDOCSER,DDTIPINS"+;
                  ",DDDATOPE,DDTIPAGG,DDCODUTE,DDNOTOPE,DDVISMAN"+;
                  ",DDRECCOL,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_DDSERIAL)+","+cp_ToStrODBC(this.w_CPROWORD)+","+cp_ToStrODBC(this.w_DDIMPDIC)+","+cp_ToStrODBC(this.w_DDDOCSER)+","+cp_ToStrODBC(this.w_DDTIPINS)+;
             ","+cp_ToStrODBC(this.w_DDDATOPE)+","+cp_ToStrODBC(this.w_DDTIPAGG)+","+cp_ToStrODBC(this.w_DDCODUTE)+","+cp_ToStrODBC(this.w_DDNOTOPE)+","+cp_ToStrODBC(this.w_DDVISMAN)+;
             ","+cp_ToStrODBC(this.w_DDRECCOL)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'DICDINTE')
        i_extval=cp_InsertValVFPExtFlds(this,'DICDINTE')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'DDSERIAL',this.w_DDSERIAL)
        INSERT INTO (i_cTable) (;
                   DDSERIAL;
                  ,CPROWORD;
                  ,DDIMPDIC;
                  ,DDDOCSER;
                  ,DDTIPINS;
                  ,DDDATOPE;
                  ,DDTIPAGG;
                  ,DDCODUTE;
                  ,DDNOTOPE;
                  ,DDVISMAN;
                  ,DDRECCOL;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_DDSERIAL;
                  ,this.w_CPROWORD;
                  ,this.w_DDIMPDIC;
                  ,this.w_DDDOCSER;
                  ,this.w_DDTIPINS;
                  ,this.w_DDDATOPE;
                  ,this.w_DDTIPAGG;
                  ,this.w_DDCODUTE;
                  ,this.w_DDNOTOPE;
                  ,this.w_DDVISMAN;
                  ,this.w_DDRECCOL;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.DICDINTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DICDINTE_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_DDDATOPE)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'DICDINTE')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 "CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'DICDINTE')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                 CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_DDDATOPE)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update DICDINTE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'DICDINTE')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " CPROWORD="+cp_ToStrODBC(this.w_CPROWORD)+;
                     ",DDIMPDIC="+cp_ToStrODBC(this.w_DDIMPDIC)+;
                     ",DDDOCSER="+cp_ToStrODBC(this.w_DDDOCSER)+;
                     ",DDTIPINS="+cp_ToStrODBC(this.w_DDTIPINS)+;
                     ",DDDATOPE="+cp_ToStrODBC(this.w_DDDATOPE)+;
                     ",DDTIPAGG="+cp_ToStrODBC(this.w_DDTIPAGG)+;
                     ",DDCODUTE="+cp_ToStrODBC(this.w_DDCODUTE)+;
                     ",DDNOTOPE="+cp_ToStrODBC(this.w_DDNOTOPE)+;
                     ",DDVISMAN="+cp_ToStrODBC(this.w_DDVISMAN)+;
                     ",DDRECCOL="+cp_ToStrODBC(this.w_DDRECCOL)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'DICDINTE')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      CPROWORD=this.w_CPROWORD;
                     ,DDIMPDIC=this.w_DDIMPDIC;
                     ,DDDOCSER=this.w_DDDOCSER;
                     ,DDTIPINS=this.w_DDTIPINS;
                     ,DDDATOPE=this.w_DDDATOPE;
                     ,DDTIPAGG=this.w_DDTIPAGG;
                     ,DDCODUTE=this.w_DDCODUTE;
                     ,DDNOTOPE=this.w_DDNOTOPE;
                     ,DDVISMAN=this.w_DDVISMAN;
                     ,DDRECCOL=this.w_DDRECCOL;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.DICDINTE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.DICDINTE_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_DDDATOPE)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete DICDINTE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_DDDATOPE)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.DICDINTE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.DICDINTE_IDX,2])
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,12,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_DDDOCSER with this.w_DDDOCSER
      replace t_SERIALE with this.w_SERIALE
      replace t_DDVISMAN with this.w_DDVISMAN
      replace t_DDRECCOL with this.w_DDRECCOL
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oDDNOTOPE_2_8.value==this.w_DDNOTOPE)
      this.oPgFrm.Page1.oPag.oDDNOTOPE_2_8.value=this.w_DDNOTOPE
      replace t_DDNOTOPE with this.oPgFrm.Page1.oPag.oDDNOTOPE_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value==this.w_CPROWORD)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value=this.w_CPROWORD
      replace t_CPROWORD with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oCPROWORD_2_1.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDIMPDIC_2_2.value==this.w_DDIMPDIC)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDIMPDIC_2_2.value=this.w_DDIMPDIC
      replace t_DDIMPDIC with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDIMPDIC_2_2.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPINS_2_4.RadioValue()==this.w_DDTIPINS)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPINS_2_4.SetRadio()
      replace t_DDTIPINS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPINS_2_4.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDDATOPE_2_5.value==this.w_DDDATOPE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDDATOPE_2_5.value=this.w_DDDATOPE
      replace t_DDDATOPE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDDATOPE_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPAGG_2_6.RadioValue()==this.w_DDTIPAGG)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPAGG_2_6.SetRadio()
      replace t_DDTIPAGG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPAGG_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDCODUTE_2_7.value==this.w_DDCODUTE)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDCODUTE_2_7.value=this.w_DDCODUTE
      replace t_DDCODUTE with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDCODUTE_2_7.value
    endif
    cp_SetControlsValueExtFlds(this,'DICDINTE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if NOT EMPTY(.w_DDDATOPE)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_DDDATOPE))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_CPROWORD=MIN(9999999,cp_maxroword()+10)
      .w_DDIMPDIC=0
      .w_DDDOCSER=space(10)
      .w_DDTIPINS=space(1)
      .w_DDDATOPE=ctod("  /  /  ")
      .w_DDTIPAGG=space(1)
      .w_DDCODUTE=0
      .w_DDNOTOPE=space(100)
      .w_SERIALE=space(10)
      .w_DDVISMAN=space(1)
      .w_DDRECCOL=space(10)
      .DoRTCalc(1,4,.f.)
        .w_DDTIPINS = 'M'
      .DoRTCalc(6,6,.f.)
        .w_DDTIPAGG = 'I'
        .w_DDCODUTE = i_Codute
    endwith
    this.DoRTCalc(9,12,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_CPROWORD = t_CPROWORD
    this.w_DDIMPDIC = t_DDIMPDIC
    this.w_DDDOCSER = t_DDDOCSER
    this.w_DDTIPINS = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPINS_2_4.RadioValue(.t.)
    this.w_DDDATOPE = t_DDDATOPE
    this.w_DDTIPAGG = this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPAGG_2_6.RadioValue(.t.)
    this.w_DDCODUTE = t_DDCODUTE
    this.w_DDNOTOPE = t_DDNOTOPE
    this.w_SERIALE = t_SERIALE
    this.w_DDVISMAN = t_DDVISMAN
    this.w_DDRECCOL = t_DDRECCOL
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_CPROWORD with this.w_CPROWORD
    replace t_DDIMPDIC with this.w_DDIMPDIC
    replace t_DDDOCSER with this.w_DDDOCSER
    replace t_DDTIPINS with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPINS_2_4.ToRadio()
    replace t_DDDATOPE with this.w_DDDATOPE
    replace t_DDTIPAGG with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oDDTIPAGG_2_6.ToRadio()
    replace t_DDCODUTE with this.w_DDCODUTE
    replace t_DDNOTOPE with this.w_DDNOTOPE
    replace t_SERIALE with this.w_SERIALE
    replace t_DDVISMAN with this.w_DDVISMAN
    replace t_DDRECCOL with this.w_DDRECCOL
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgscg_mduPag1 as StdContainer
  Width  = 578
  height = 257
  stdWidth  = 578
  stdheight = 257
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=3, top=2, width=562,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=6,Field1="CPROWORD",Label1="Riga",Field2="DDIMPDIC",Label2="Importo utilizzato",Field3="DDTIPINS",Label3="Tipo ins.",Field4="DDTIPAGG",Label4="Tipo agg.",Field5="DDCODUTE",Label5="Utente",Field6="DDDATOPE",Label6="Data agg.",Field7="DDCODUTE",Label7="<Etichetta>",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235734138

  add object oStr_1_3 as StdString with uid="TSJUBILSIY",Visible=.t., Left=17, Top=226,;
    Alignment=1, Width=38, Height=18,;
    Caption="Note:"  ;
  , bGlobalFont=.t.

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=-6,top=24,;
    width=558+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=-5,top=25,width=557+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile=''

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      this.Parent.oDDNOTOPE_2_8.Refresh()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
    endcase
    return(oDropInto)
  EndFunc

  add object oBeginFixedBody as FixedBodyKeyMover with nDirection=1


  add object oDDNOTOPE_2_8 as StdTrsField with uid="NVWYDPBWHH",rtseq=9,rtrep=.t.,;
    cFormVar="w_DDNOTOPE",value=space(100),;
    HelpContextID = 264897403,;
    cTotal="", bFixedPos=.t., cQueryName = "DDNOTOPE",;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=21, Width=503, Left=59, Top=223, InputMask=replicate('X',100)

  add object oEndFixedBody as FixedBodyKeyMover with nDirection=-1
  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgscg_mduBodyRow as CPBodyRowCnt
  Width=548
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oCPROWORD_2_1 as StdTrsField with uid="DHDBFXIZSQ",rtseq=2,rtrep=.t.,;
    cFormVar="w_CPROWORD",value=0,;
    HelpContextID = 268062570,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=69, Left=-2, Top=0, cSayPict=["9999999"], cGetPict=["9999999"]

  add object oDDIMPDIC_2_2 as StdTrsField with uid="MPZDYJWDEE",rtseq=3,rtrep=.t.,;
    cFormVar="w_DDIMPDIC",value=0,;
    ToolTipText = "Importo utilizzato",;
    HelpContextID = 192433287,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=146, Left=70, Top=0, cSayPict=[v_PV(80)], cGetPict=[v_GV(80)]

  add object oDDTIPINS_2_4 as StdTrsCombo with uid="CCDWTGMTIE",rtrep=.t.,;
    cFormVar="w_DDTIPINS", RowSource=""+"Diretto,"+"Indiretto,"+"Manuale,"+"Automatico" , enabled=.f.,;
    ToolTipText = "Tipo inserimento",;
    HelpContextID = 108764279,;
    Height=22, Width=90, Left=223, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDDTIPINS_2_4.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DDTIPINS,&i_cF..t_DDTIPINS),this.value)
    return(iif(xVal =1,'D',;
    iif(xVal =2,'I',;
    iif(xVal =3,'M',;
    iif(xVal =4,'A',;
    space(1))))))
  endfunc
  func oDDTIPINS_2_4.GetRadio()
    this.Parent.oContained.w_DDTIPINS = this.RadioValue()
    return .t.
  endfunc

  func oDDTIPINS_2_4.ToRadio()
    this.Parent.oContained.w_DDTIPINS=trim(this.Parent.oContained.w_DDTIPINS)
    return(;
      iif(this.Parent.oContained.w_DDTIPINS=='D',1,;
      iif(this.Parent.oContained.w_DDTIPINS=='I',2,;
      iif(this.Parent.oContained.w_DDTIPINS=='M',3,;
      iif(this.Parent.oContained.w_DDTIPINS=='A',4,;
      0)))))
  endfunc

  func oDDTIPINS_2_4.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDDDATOPE_2_5 as StdTrsField with uid="BGQNWTENBT",rtseq=6,rtrep=.t.,;
    cFormVar="w_DDDATOPE",value=ctod("  /  /  "),enabled=.f.,;
    HelpContextID = 263938939,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=76, Left=467, Top=0

  add object oDDTIPAGG_2_6 as StdTrsCombo with uid="NKTAEHCRGR",rtrep=.t.,;
    cFormVar="w_DDTIPAGG", RowSource=""+"Inserimento,"+"Modifica,"+"Cancellazione" , ;
    HelpContextID = 25453437,;
    Height=22, Width=90, Left=319, Top=0,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag=2, bIsInHeader=.f.;
  , bGlobalFont=.t.



  func oDDTIPAGG_2_6.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..DDTIPAGG,&i_cF..t_DDTIPAGG),this.value)
    return(iif(xVal =1,'I',;
    iif(xVal =2,'M',;
    iif(xVal =3,'C',;
    space(1)))))
  endfunc
  func oDDTIPAGG_2_6.GetRadio()
    this.Parent.oContained.w_DDTIPAGG = this.RadioValue()
    return .t.
  endfunc

  func oDDTIPAGG_2_6.ToRadio()
    this.Parent.oContained.w_DDTIPAGG=trim(this.Parent.oContained.w_DDTIPAGG)
    return(;
      iif(this.Parent.oContained.w_DDTIPAGG=='I',1,;
      iif(this.Parent.oContained.w_DDTIPAGG=='M',2,;
      iif(this.Parent.oContained.w_DDTIPAGG=='C',3,;
      0))))
  endfunc

  func oDDTIPAGG_2_6.SetRadio()
    this.value=this.ToRadio()
  endfunc

  add object oDDCODUTE_2_7 as StdTrsField with uid="EUESANHJSN",rtseq=8,rtrep=.t.,;
    cFormVar="w_DDCODUTE",value=0,enabled=.f.,;
    HelpContextID = 80302971,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=48, Left=411, Top=0, cSayPict=["9999"], cGetPict=["9999"]
  add object oLast as LastKeyMover with bHasFixedBody=.t.
  * ---
  func oCPROWORD_2_1.When()
    return(.t.)
  proc oCPROWORD_2_1.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oCPROWORD_2_1.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg_mdu','DICDINTE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".DDSERIAL=DICDINTE.DDSERIAL";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
