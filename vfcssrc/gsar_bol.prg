* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bol                                                        *
*              Crea/elimina/varia cliente associato a nominativo               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-01-10                                                      *
* Last revis.: 2014-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipOpe,pANCODICE,pCliFor
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bol",oParentObject,m.pTipOpe,m.pANCODICE,m.pCliFor)
return(i_retval)

define class tgsar_bol as StdBatch
  * --- Local variables
  pTipOpe = space(1)
  pANCODICE = space(15)
  pCliFor = space(1)
  w_TOTCOD = space(15)
  w_OK = .f.
  w_CLIVUOTO = space(15)
  w_CODICE = space(15)
  w_NOMCOD = space(15)
  w_NOCODICE = space(20)
  w_NOTIPNOM = space(1)
  w_ANDESCRI = space(40)
  w_ANDESCR2 = space(40)
  w_ANINDIRI = space(35)
  w_ANINDIR2 = space(35)
  w_AN___CAP = space(8)
  w_ANLOCALI = space(30)
  w_ANPROVIN = space(2)
  w_ANNAZION = space(3)
  w_ANPARIVA = space(12)
  w_ANCODFIS = space(16)
  w_ANTELEFO = space(18)
  w_ANTELFAX = space(18)
  w_ANINDWEB = space(50)
  w_AN_EMAIL = space(50)
  w_AN_EMPEC = space(50)
  w_ANCODZON = space(3)
  w_ANCODLIN = space(3)
  w_ANCODVAL = space(5)
  w_ANCODAGE = space(5)
  w_AN__NOTE = space(5)
  w_ANDTINVA = ctod("  /  /  ")
  w_ANDTOBSO = ctod("  /  /  ")
  w_ANCATCOM = space(3)
  w_ANCATSCM = space(5)
  w_ANCODPAG = space(5)
  w_ANNUMLIS = space(5)
  w_ANCODLIS = space(5)
  w_AN1SCONT = 0
  w_AN2SCONT = 0
  w_FLGNUM = space(1)
  w_CODCLI = space(20)
  w_DESCLI = space(40)
  w_NEWCOD = space(20)
  w_ESCI = .f.
  w_ANCODSAL = space(5)
  w_ANNUMCEL = space(18)
  w_ANCODBAN = space(10)
  w_ANCODBA2 = space(15)
  w_AN_SKYPE = space(50)
  w_ANTIPCON = space(1)
  w_COGNOM = space(40)
  w_NOME = space(20)
  w_PROV = space(2)
  w_NATOA = space(30)
  w_NATOIL = ctod("  /  /  ")
  w_SESSO = space(1)
  w_NUMCAR = space(18)
  w_PERFIS = space(1)
  w_CHKSTA = space(1)
  w_CHKMAIL = space(1)
  w_CHKMPEC = space(1)
  w_CHKFAX = space(1)
  w_CHKCPZ = space(1)
  w_CHKWWP = space(1)
  w_CHKPTL = space(1)
  w_CHKIVA = space(1)
  w_CONTA = 0
  w_CODNOM = space(15)
  w_TIPNOM = space(1)
  w_COLL = .f.
  w_NOMCOD = space(15)
  w_PROGR = 0
  * --- WorkFile variables
  OFF_NOMI_idx=0
  CONTI_idx=0
  PAR_OFFE_idx=0
  CONTATTI_idx=0
  NOM_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Eventi da Creazione/Eliminazione / Variazione Cliente collegato ad un Nominativo Offerte (da GSAR_ACL)
    this.w_TOTCOD = this.pANCODICE
    this.w_CLIVUOTO = SPACE(15)
    this.w_CODCLI = SPACE(20)
    this.w_FLGNUM = " "
    if TYPE("pCliFor")="C"
      * --- Cliente o fornitore come specificato nei parametri
      this.w_ANTIPCON = this.pCliFor
    else
      this.w_ANTIPCON = "C"
    endif
    if NOT EMPTY(this.w_TOTCOD)
      do case
        case this.pTipOpe="V" AND (this.w_ANTIPCON="C" Or IsAlt())
          * --- Variato Cliente: Riaggiorna campi in Anagrafica Nominativi 
          if upper(g_APPLICATION) <> "ADHOC REVOLUTION"
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "*"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
                    +" and ANCODICE = "+cp_ToStrODBC(this.w_TOTCOD);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                *;
                from (i_cTable) where;
                    ANTIPCON = this.w_ANTIPCON;
                    and ANCODICE = this.w_TOTCOD;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ANDESCRI = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
              this.w_ANDESCR2 = NVL(cp_ToDate(_read_.ANDESCR2),cp_NullValue(_read_.ANDESCR2))
              this.w_ANINDIRI = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
              this.w_ANINDIR2 = NVL(cp_ToDate(_read_.ANINDIR2),cp_NullValue(_read_.ANINDIR2))
              this.w_AN___CAP = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
              this.w_ANLOCALI = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
              this.w_ANPROVIN = NVL(cp_ToDate(_read_.ANPROVIN),cp_NullValue(_read_.ANPROVIN))
              this.w_ANNAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
              this.w_ANPARIVA = NVL(cp_ToDate(_read_.ANPARIVA),cp_NullValue(_read_.ANPARIVA))
              this.w_ANCODFIS = NVL(cp_ToDate(_read_.ANCODFIS),cp_NullValue(_read_.ANCODFIS))
              this.w_ANTELEFO = NVL(cp_ToDate(_read_.ANTELEFO),cp_NullValue(_read_.ANTELEFO))
              this.w_ANTELFAX = NVL(cp_ToDate(_read_.ANTELFAX),cp_NullValue(_read_.ANTELFAX))
              this.w_ANINDWEB = NVL(cp_ToDate(_read_.ANINDWEB),cp_NullValue(_read_.ANINDWEB))
              this.w_AN_EMAIL = NVL(cp_ToDate(_read_.AN_EMAIL),cp_NullValue(_read_.AN_EMAIL))
              this.w_AN_EMPEC = NVL(cp_ToDate(_read_.AN_EMPEC),cp_NullValue(_read_.AN_EMPEC))
              this.w_ANCODZON = NVL(cp_ToDate(_read_.ANCODZON),cp_NullValue(_read_.ANCODZON))
              this.w_ANCODLIN = NVL(cp_ToDate(_read_.ANCODLIN),cp_NullValue(_read_.ANCODLIN))
              this.w_ANCODVAL = NVL(cp_ToDate(_read_.ANCODVAL),cp_NullValue(_read_.ANCODVAL))
              this.w_ANCODAGE = NVL(cp_ToDate(_read_.ANCODAG1),cp_NullValue(_read_.ANCODAG1))
              this.w_AN__NOTE = NVL(cp_ToDate(_read_.AN__NOTE),cp_NullValue(_read_.AN__NOTE))
              this.w_ANDTINVA = NVL(cp_ToDate(_read_.ANDTINVA),cp_NullValue(_read_.ANDTINVA))
              this.w_ANDTOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
              this.w_COGNOM = NVL(cp_ToDate(_read_.ANCOGNOM),cp_NullValue(_read_.ANCOGNOM))
              this.w_NOME = NVL(cp_ToDate(_read_.AN__NOME),cp_NullValue(_read_.AN__NOME))
              this.w_NATOA = NVL(cp_ToDate(_read_.ANLOCNAS),cp_NullValue(_read_.ANLOCNAS))
              this.w_PROV = NVL(cp_ToDate(_read_.ANPRONAS),cp_NullValue(_read_.ANPRONAS))
              this.w_NATOIL = NVL(cp_ToDate(_read_.ANDATNAS),cp_NullValue(_read_.ANDATNAS))
              this.w_SESSO = NVL(cp_ToDate(_read_.AN_SESSO),cp_NullValue(_read_.AN_SESSO))
              this.w_NUMCAR = NVL(cp_ToDate(_read_.ANNUMCAR),cp_NullValue(_read_.ANNUMCAR))
              this.w_PERFIS = NVL(cp_ToDate(_read_.ANPERFIS),cp_NullValue(_read_.ANPERFIS))
              this.w_ANCODSAL = NVL(cp_ToDate(_read_.ANCODSAL),cp_NullValue(_read_.ANCODSAL))
              this.w_ANNUMCEL = NVL(cp_ToDate(_read_.ANNUMCEL),cp_NullValue(_read_.ANNUMCEL))
              this.w_CHKSTA = NVL(cp_ToDate(_read_.ANCHKSTA),cp_NullValue(_read_.ANCHKSTA))
              this.w_CHKMAIL = NVL(cp_ToDate(_read_.ANCHKMAI),cp_NullValue(_read_.ANCHKMAI))
              this.w_CHKMPEC = NVL(cp_ToDate(_read_.ANCHKPEC),cp_NullValue(_read_.ANCHKPEC))
              this.w_CHKFAX = NVL(cp_ToDate(_read_.ANCHKFAX),cp_NullValue(_read_.ANCHKFAX))
              this.w_CHKCPZ = NVL(cp_ToDate(_read_.ANCHKCPZ),cp_NullValue(_read_.ANCHKCPZ))
              this.w_CHKWWP = NVL(cp_ToDate(_read_.ANCHKWWP),cp_NullValue(_read_.ANCHKWWP))
              this.w_CHKPTL = NVL(cp_ToDate(_read_.ANCHKPTL),cp_NullValue(_read_.ANCHKPTL))
              this.w_ANCODBAN = NVL(cp_ToDate(_read_.ANCODBAN),cp_NullValue(_read_.ANCODBAN))
              this.w_ANCODBA2 = NVL(cp_ToDate(_read_.ANCODBA2),cp_NullValue(_read_.ANCODBA2))
              this.w_ANCODPAG = NVL(cp_ToDate(_read_.ANCODPAG),cp_NullValue(_read_.ANCODPAG))
              this.w_AN_SKYPE = NVL(cp_ToDate(_read_.AN_SKYPE),cp_NullValue(_read_.AN_SKYPE))
              this.w_ANCATCOM = NVL(cp_ToDate(_read_.ANCATCOM),cp_NullValue(_read_.ANCATCOM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          else
            * --- Read from CONTI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.CONTI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "*"+;
                " from "+i_cTable+" CONTI where ";
                    +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
                    +" and ANCODICE = "+cp_ToStrODBC(this.w_TOTCOD);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                *;
                from (i_cTable) where;
                    ANTIPCON = this.w_ANTIPCON;
                    and ANCODICE = this.w_TOTCOD;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              this.w_ANDESCRI = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
              this.w_ANDESCR2 = NVL(cp_ToDate(_read_.ANDESCR2),cp_NullValue(_read_.ANDESCR2))
              this.w_ANINDIRI = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
              this.w_ANINDIR2 = NVL(cp_ToDate(_read_.ANINDIR2),cp_NullValue(_read_.ANINDIR2))
              this.w_AN___CAP = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
              this.w_ANLOCALI = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
              this.w_ANPROVIN = NVL(cp_ToDate(_read_.ANPROVIN),cp_NullValue(_read_.ANPROVIN))
              this.w_ANNAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
              this.w_ANPARIVA = NVL(cp_ToDate(_read_.ANPARIVA),cp_NullValue(_read_.ANPARIVA))
              this.w_ANCODFIS = NVL(cp_ToDate(_read_.ANCODFIS),cp_NullValue(_read_.ANCODFIS))
              this.w_ANTELEFO = NVL(cp_ToDate(_read_.ANTELEFO),cp_NullValue(_read_.ANTELEFO))
              this.w_ANTELFAX = NVL(cp_ToDate(_read_.ANTELFAX),cp_NullValue(_read_.ANTELFAX))
              this.w_ANINDWEB = NVL(cp_ToDate(_read_.ANINDWEB),cp_NullValue(_read_.ANINDWEB))
              this.w_AN_EMAIL = NVL(cp_ToDate(_read_.AN_EMAIL),cp_NullValue(_read_.AN_EMAIL))
              this.w_AN_EMPEC = NVL(cp_ToDate(_read_.AN_EMPEC),cp_NullValue(_read_.AN_EMPEC))
              this.w_ANCODZON = NVL(cp_ToDate(_read_.ANCODZON),cp_NullValue(_read_.ANCODZON))
              this.w_ANCODLIN = NVL(cp_ToDate(_read_.ANCODLIN),cp_NullValue(_read_.ANCODLIN))
              this.w_ANCODVAL = NVL(cp_ToDate(_read_.ANCODVAL),cp_NullValue(_read_.ANCODVAL))
              this.w_ANCODAGE = NVL(cp_ToDate(_read_.ANCODAG1),cp_NullValue(_read_.ANCODAG1))
              this.w_AN__NOTE = NVL(cp_ToDate(_read_.AN__NOTE),cp_NullValue(_read_.AN__NOTE))
              this.w_ANDTINVA = NVL(cp_ToDate(_read_.ANDTINVA),cp_NullValue(_read_.ANDTINVA))
              this.w_ANDTOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
              this.w_COGNOM = NVL(cp_ToDate(_read_.ANCOGNOM),cp_NullValue(_read_.ANCOGNOM))
              this.w_NOME = NVL(cp_ToDate(_read_.AN__NOME),cp_NullValue(_read_.AN__NOME))
              this.w_NATOA = NVL(cp_ToDate(_read_.ANLOCNAS),cp_NullValue(_read_.ANLOCNAS))
              this.w_PROV = NVL(cp_ToDate(_read_.ANPRONAS),cp_NullValue(_read_.ANPRONAS))
              this.w_NATOIL = NVL(cp_ToDate(_read_.ANDATNAS),cp_NullValue(_read_.ANDATNAS))
              this.w_SESSO = NVL(cp_ToDate(_read_.AN_SESSO),cp_NullValue(_read_.AN_SESSO))
              this.w_NUMCAR = NVL(cp_ToDate(_read_.ANNUMCAR),cp_NullValue(_read_.ANNUMCAR))
              this.w_PERFIS = NVL(cp_ToDate(_read_.ANPERFIS),cp_NullValue(_read_.ANPERFIS))
              this.w_ANCODSAL = NVL(cp_ToDate(_read_.ANCODSAL),cp_NullValue(_read_.ANCODSAL))
              this.w_ANNUMCEL = NVL(cp_ToDate(_read_.ANNUMCEL),cp_NullValue(_read_.ANNUMCEL))
              this.w_CHKSTA = NVL(cp_ToDate(_read_.ANCHKSTA),cp_NullValue(_read_.ANCHKSTA))
              this.w_CHKMAIL = NVL(cp_ToDate(_read_.ANCHKMAI),cp_NullValue(_read_.ANCHKMAI))
              this.w_CHKMPEC = NVL(cp_ToDate(_read_.ANCHKPEC),cp_NullValue(_read_.ANCHKPEC))
              this.w_CHKFAX = NVL(cp_ToDate(_read_.ANCHKFAX),cp_NullValue(_read_.ANCHKFAX))
              this.w_CHKCPZ = NVL(cp_ToDate(_read_.ANCHKCPZ),cp_NullValue(_read_.ANCHKCPZ))
              this.w_CHKWWP = NVL(cp_ToDate(_read_.ANCHKWWP),cp_NullValue(_read_.ANCHKWWP))
              this.w_CHKPTL = NVL(cp_ToDate(_read_.ANCHKPTL),cp_NullValue(_read_.ANCHKPTL))
              this.w_ANCODBAN = NVL(cp_ToDate(_read_.ANCODBAN),cp_NullValue(_read_.ANCODBAN))
              this.w_ANCODBA2 = NVL(cp_ToDate(_read_.ANCODBA2),cp_NullValue(_read_.ANCODBA2))
              this.w_ANNUMLIS = NVL(cp_ToDate(_read_.ANNUMLIS),cp_NullValue(_read_.ANNUMLIS))
              this.w_ANCODPAG = NVL(cp_ToDate(_read_.ANCODPAG),cp_NullValue(_read_.ANCODPAG))
              this.w_AN_SKYPE = NVL(cp_ToDate(_read_.AN_SKYPE),cp_NullValue(_read_.AN_SKYPE))
              this.w_ANCATCOM = NVL(cp_ToDate(_read_.ANCATCOM),cp_NullValue(_read_.ANCATCOM))
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
          endif
          this.w_PERFIS = iif( this.w_PERFIS="S" , "PF","EN") 
          if upper(g_APPLICATION) <> "ADHOC REVOLUTION"
            * --- Write into OFF_NOMI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"NOTELFAX ="+cp_NullLink(cp_ToStrODBC(this.w_ANTELFAX),'OFF_NOMI','NOTELFAX');
              +",NOTELEFO ="+cp_NullLink(cp_ToStrODBC(this.w_ANTELEFO),'OFF_NOMI','NOTELEFO');
              +",NOPROVIN ="+cp_NullLink(cp_ToStrODBC(this.w_ANPROVIN),'OFF_NOMI','NOPROVIN');
              +",NOPARIVA ="+cp_NullLink(cp_ToStrODBC(this.w_ANPARIVA),'OFF_NOMI','NOPARIVA');
              +",NONAZION ="+cp_NullLink(cp_ToStrODBC(this.w_ANNAZION),'OFF_NOMI','NONAZION');
              +",NOLOCALI ="+cp_NullLink(cp_ToStrODBC(this.w_ANLOCALI),'OFF_NOMI','NOLOCALI');
              +",NOINDWEB ="+cp_NullLink(cp_ToStrODBC(this.w_ANINDWEB),'OFF_NOMI','NOINDWEB');
              +",NOINDIRI ="+cp_NullLink(cp_ToStrODBC(this.w_ANINDIRI),'OFF_NOMI','NOINDIRI');
              +",NOINDI_2 ="+cp_NullLink(cp_ToStrODBC(this.w_ANINDIR2),'OFF_NOMI','NOINDI_2');
              +",NODTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_ANDTOBSO),'OFF_NOMI','NODTOBSO');
              +",NODTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_ANDTINVA),'OFF_NOMI','NODTINVA');
              +",NODESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_ANDESCRI),'OFF_NOMI','NODESCRI');
              +",NODESCR2 ="+cp_NullLink(cp_ToStrODBC(this.w_ANDESCR2),'OFF_NOMI','NODESCR2');
              +",NOCODZON ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODZON),'OFF_NOMI','NOCODZON');
              +",NOCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODVAL),'OFF_NOMI','NOCODVAL');
              +",NOCODLIN ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODLIN),'OFF_NOMI','NOCODLIN');
              +",NOCODFIS ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODFIS),'OFF_NOMI','NOCODFIS');
              +",NOCODAGE ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODAGE),'OFF_NOMI','NOCODAGE');
              +",NO_EMAIL ="+cp_NullLink(cp_ToStrODBC(this.w_AN_EMAIL),'OFF_NOMI','NO_EMAIL');
              +",NO_EMPEC ="+cp_NullLink(cp_ToStrODBC(this.w_AN_EMPEC),'OFF_NOMI','NO_EMPEC');
              +",NO__NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_AN__NOTE),'OFF_NOMI','NO__NOTE');
              +",NO___CAP ="+cp_NullLink(cp_ToStrODBC(this.w_AN___CAP),'OFF_NOMI','NO___CAP');
              +",NOSOGGET ="+cp_NullLink(cp_ToStrODBC(this.w_PERFIS),'OFF_NOMI','NOSOGGET');
              +",NOLOCNAS ="+cp_NullLink(cp_ToStrODBC(this.w_NATOA),'OFF_NOMI','NOLOCNAS');
              +",NOPRONAS ="+cp_NullLink(cp_ToStrODBC(this.w_PROV),'OFF_NOMI','NOPRONAS');
              +",NODATNAS ="+cp_NullLink(cp_ToStrODBC(this.w_NATOIL),'OFF_NOMI','NODATNAS');
              +",NO_SESSO ="+cp_NullLink(cp_ToStrODBC(this.w_SESSO),'OFF_NOMI','NO_SESSO');
              +",NONUMCAR ="+cp_NullLink(cp_ToStrODBC(this.w_NUMCAR),'OFF_NOMI','NONUMCAR');
              +",NOCOGNOM ="+cp_NullLink(cp_ToStrODBC(this.w_COGNOM),'OFF_NOMI','NOCOGNOM');
              +",NO__NOME ="+cp_NullLink(cp_ToStrODBC(this.w_NOME),'OFF_NOMI','NO__NOME');
              +",NOCODSAL ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODSAL),'OFF_NOMI','NOCODSAL');
              +",NONUMCEL ="+cp_NullLink(cp_ToStrODBC(this.w_ANNUMCEL),'OFF_NOMI','NONUMCEL');
              +",NOCHKSTA ="+cp_NullLink(cp_ToStrODBC(this.w_CHKSTA),'OFF_NOMI','NOCHKSTA');
              +",NOCHKMAI ="+cp_NullLink(cp_ToStrODBC(this.w_CHKMAIL),'OFF_NOMI','NOCHKMAI');
              +",NOCHKPEC ="+cp_NullLink(cp_ToStrODBC(this.w_CHKMPEC),'OFF_NOMI','NOCHKPEC');
              +",NOCHKFAX ="+cp_NullLink(cp_ToStrODBC(this.w_CHKFAX),'OFF_NOMI','NOCHKFAX');
              +",NOCHKCPZ ="+cp_NullLink(cp_ToStrODBC("N"),'OFF_NOMI','NOCHKCPZ');
              +",NOCHKWWP ="+cp_NullLink(cp_ToStrODBC("N"),'OFF_NOMI','NOCHKWWP');
              +",NOCHKPTL ="+cp_NullLink(cp_ToStrODBC("N"),'OFF_NOMI','NOCHKPTL');
              +",NOCODBAN ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODBAN),'OFF_NOMI','NOCODBAN');
              +",NOCODBA2 ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODBA2),'OFF_NOMI','NOCODBA2');
              +",NOCODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODPAG),'OFF_NOMI','NOCODPAG');
              +",NO_SKYPE ="+cp_NullLink(cp_ToStrODBC(this.w_AN_SKYPE),'OFF_NOMI','NO_SKYPE');
              +",NOCATCOM ="+cp_NullLink(cp_ToStrODBC(this.w_ANCATCOM),'OFF_NOMI','NOCATCOM');
                  +i_ccchkf ;
              +" where ";
                  +"NOCODCLI = "+cp_ToStrODBC(this.w_TOTCOD);
                  +" and NOTIPCLI = "+cp_ToStrODBC(this.w_ANTIPCON);
                     )
            else
              update (i_cTable) set;
                  NOTELFAX = this.w_ANTELFAX;
                  ,NOTELEFO = this.w_ANTELEFO;
                  ,NOPROVIN = this.w_ANPROVIN;
                  ,NOPARIVA = this.w_ANPARIVA;
                  ,NONAZION = this.w_ANNAZION;
                  ,NOLOCALI = this.w_ANLOCALI;
                  ,NOINDWEB = this.w_ANINDWEB;
                  ,NOINDIRI = this.w_ANINDIRI;
                  ,NOINDI_2 = this.w_ANINDIR2;
                  ,NODTOBSO = this.w_ANDTOBSO;
                  ,NODTINVA = this.w_ANDTINVA;
                  ,NODESCRI = this.w_ANDESCRI;
                  ,NODESCR2 = this.w_ANDESCR2;
                  ,NOCODZON = this.w_ANCODZON;
                  ,NOCODVAL = this.w_ANCODVAL;
                  ,NOCODLIN = this.w_ANCODLIN;
                  ,NOCODFIS = this.w_ANCODFIS;
                  ,NOCODAGE = this.w_ANCODAGE;
                  ,NO_EMAIL = this.w_AN_EMAIL;
                  ,NO_EMPEC = this.w_AN_EMPEC;
                  ,NO__NOTE = this.w_AN__NOTE;
                  ,NO___CAP = this.w_AN___CAP;
                  ,NOSOGGET = this.w_PERFIS;
                  ,NOLOCNAS = this.w_NATOA;
                  ,NOPRONAS = this.w_PROV;
                  ,NODATNAS = this.w_NATOIL;
                  ,NO_SESSO = this.w_SESSO;
                  ,NONUMCAR = this.w_NUMCAR;
                  ,NOCOGNOM = this.w_COGNOM;
                  ,NO__NOME = this.w_NOME;
                  ,NOCODSAL = this.w_ANCODSAL;
                  ,NONUMCEL = this.w_ANNUMCEL;
                  ,NOCHKSTA = this.w_CHKSTA;
                  ,NOCHKMAI = this.w_CHKMAIL;
                  ,NOCHKPEC = this.w_CHKMPEC;
                  ,NOCHKFAX = this.w_CHKFAX;
                  ,NOCHKCPZ = "N";
                  ,NOCHKWWP = "N";
                  ,NOCHKPTL = "N";
                  ,NOCODBAN = this.w_ANCODBAN;
                  ,NOCODBA2 = this.w_ANCODBA2;
                  ,NOCODPAG = this.w_ANCODPAG;
                  ,NO_SKYPE = this.w_AN_SKYPE;
                  ,NOCATCOM = this.w_ANCATCOM;
                  &i_ccchkf. ;
               where;
                  NOCODCLI = this.w_TOTCOD;
                  and NOTIPCLI = this.w_ANTIPCON;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          else
            * --- Write into OFF_NOMI
            i_commit = .f.
            if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
              cp_BeginTrs()
              i_commit = .t.
            endif
            i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
            i_ccchkf=''
            this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
            if i_nConn<>0
              i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
              +"NOTELFAX ="+cp_NullLink(cp_ToStrODBC(this.w_ANTELFAX),'OFF_NOMI','NOTELFAX');
              +",NOTELEFO ="+cp_NullLink(cp_ToStrODBC(this.w_ANTELEFO),'OFF_NOMI','NOTELEFO');
              +",NOPROVIN ="+cp_NullLink(cp_ToStrODBC(this.w_ANPROVIN),'OFF_NOMI','NOPROVIN');
              +",NOPARIVA ="+cp_NullLink(cp_ToStrODBC(this.w_ANPARIVA),'OFF_NOMI','NOPARIVA');
              +",NONAZION ="+cp_NullLink(cp_ToStrODBC(this.w_ANNAZION),'OFF_NOMI','NONAZION');
              +",NOLOCALI ="+cp_NullLink(cp_ToStrODBC(this.w_ANLOCALI),'OFF_NOMI','NOLOCALI');
              +",NOINDWEB ="+cp_NullLink(cp_ToStrODBC(this.w_ANINDWEB),'OFF_NOMI','NOINDWEB');
              +",NOINDIRI ="+cp_NullLink(cp_ToStrODBC(this.w_ANINDIRI),'OFF_NOMI','NOINDIRI');
              +",NOINDI_2 ="+cp_NullLink(cp_ToStrODBC(this.w_ANINDIR2),'OFF_NOMI','NOINDI_2');
              +",NODTOBSO ="+cp_NullLink(cp_ToStrODBC(this.w_ANDTOBSO),'OFF_NOMI','NODTOBSO');
              +",NODTINVA ="+cp_NullLink(cp_ToStrODBC(this.w_ANDTINVA),'OFF_NOMI','NODTINVA');
              +",NODESCRI ="+cp_NullLink(cp_ToStrODBC(this.w_ANDESCRI),'OFF_NOMI','NODESCRI');
              +",NODESCR2 ="+cp_NullLink(cp_ToStrODBC(this.w_ANDESCR2),'OFF_NOMI','NODESCR2');
              +",NOCODZON ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODZON),'OFF_NOMI','NOCODZON');
              +",NOCODVAL ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODVAL),'OFF_NOMI','NOCODVAL');
              +",NOCODLIN ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODLIN),'OFF_NOMI','NOCODLIN');
              +",NOCODFIS ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODFIS),'OFF_NOMI','NOCODFIS');
              +",NOCODAGE ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODAGE),'OFF_NOMI','NOCODAGE');
              +",NO_EMAIL ="+cp_NullLink(cp_ToStrODBC(this.w_AN_EMAIL),'OFF_NOMI','NO_EMAIL');
              +",NO_EMPEC ="+cp_NullLink(cp_ToStrODBC(this.w_AN_EMPEC),'OFF_NOMI','NO_EMPEC');
              +",NO__NOTE ="+cp_NullLink(cp_ToStrODBC(this.w_AN__NOTE),'OFF_NOMI','NO__NOTE');
              +",NO___CAP ="+cp_NullLink(cp_ToStrODBC(this.w_AN___CAP),'OFF_NOMI','NO___CAP');
              +",NOSOGGET ="+cp_NullLink(cp_ToStrODBC(this.w_PERFIS),'OFF_NOMI','NOSOGGET');
              +",NOLOCNAS ="+cp_NullLink(cp_ToStrODBC(this.w_NATOA),'OFF_NOMI','NOLOCNAS');
              +",NOPRONAS ="+cp_NullLink(cp_ToStrODBC(this.w_PROV),'OFF_NOMI','NOPRONAS');
              +",NODATNAS ="+cp_NullLink(cp_ToStrODBC(this.w_NATOIL),'OFF_NOMI','NODATNAS');
              +",NO_SESSO ="+cp_NullLink(cp_ToStrODBC(this.w_SESSO),'OFF_NOMI','NO_SESSO');
              +",NONUMCAR ="+cp_NullLink(cp_ToStrODBC(this.w_NUMCAR),'OFF_NOMI','NONUMCAR');
              +",NOCOGNOM ="+cp_NullLink(cp_ToStrODBC(this.w_COGNOM),'OFF_NOMI','NOCOGNOM');
              +",NO__NOME ="+cp_NullLink(cp_ToStrODBC(this.w_NOME),'OFF_NOMI','NO__NOME');
              +",NOCODSAL ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODSAL),'OFF_NOMI','NOCODSAL');
              +",NONUMCEL ="+cp_NullLink(cp_ToStrODBC(this.w_ANNUMCEL),'OFF_NOMI','NONUMCEL');
              +",NOCHKSTA ="+cp_NullLink(cp_ToStrODBC(this.w_CHKSTA),'OFF_NOMI','NOCHKSTA');
              +",NOCHKMAI ="+cp_NullLink(cp_ToStrODBC(this.w_CHKMAIL),'OFF_NOMI','NOCHKMAI');
              +",NOCHKPEC ="+cp_NullLink(cp_ToStrODBC(this.w_CHKMPEC),'OFF_NOMI','NOCHKPEC');
              +",NOCHKFAX ="+cp_NullLink(cp_ToStrODBC(this.w_CHKFAX),'OFF_NOMI','NOCHKFAX');
              +",NOCHKCPZ ="+cp_NullLink(cp_ToStrODBC("N"),'OFF_NOMI','NOCHKCPZ');
              +",NOCHKWWP ="+cp_NullLink(cp_ToStrODBC("N"),'OFF_NOMI','NOCHKWWP');
              +",NOCHKPTL ="+cp_NullLink(cp_ToStrODBC("N"),'OFF_NOMI','NOCHKPTL');
              +",NOCODBAN ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODBAN),'OFF_NOMI','NOCODBAN');
              +",NOCODBA2 ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODBA2),'OFF_NOMI','NOCODBA2');
              +",NONUMLIS ="+cp_NullLink(cp_ToStrODBC(this.w_ANNUMLIS),'OFF_NOMI','NONUMLIS');
              +",NOCODPAG ="+cp_NullLink(cp_ToStrODBC(this.w_ANCODPAG),'OFF_NOMI','NOCODPAG');
              +",NO_SKYPE ="+cp_NullLink(cp_ToStrODBC(this.w_AN_SKYPE),'OFF_NOMI','NO_SKYPE');
              +",NOCATCOM ="+cp_NullLink(cp_ToStrODBC(this.w_ANCATCOM),'OFF_NOMI','NOCATCOM');
                  +i_ccchkf ;
              +" where ";
                  +"NOCODCLI = "+cp_ToStrODBC(this.w_TOTCOD);
                  +" and NOTIPCLI = "+cp_ToStrODBC(this.w_ANTIPCON);
                     )
            else
              update (i_cTable) set;
                  NOTELFAX = this.w_ANTELFAX;
                  ,NOTELEFO = this.w_ANTELEFO;
                  ,NOPROVIN = this.w_ANPROVIN;
                  ,NOPARIVA = this.w_ANPARIVA;
                  ,NONAZION = this.w_ANNAZION;
                  ,NOLOCALI = this.w_ANLOCALI;
                  ,NOINDWEB = this.w_ANINDWEB;
                  ,NOINDIRI = this.w_ANINDIRI;
                  ,NOINDI_2 = this.w_ANINDIR2;
                  ,NODTOBSO = this.w_ANDTOBSO;
                  ,NODTINVA = this.w_ANDTINVA;
                  ,NODESCRI = this.w_ANDESCRI;
                  ,NODESCR2 = this.w_ANDESCR2;
                  ,NOCODZON = this.w_ANCODZON;
                  ,NOCODVAL = this.w_ANCODVAL;
                  ,NOCODLIN = this.w_ANCODLIN;
                  ,NOCODFIS = this.w_ANCODFIS;
                  ,NOCODAGE = this.w_ANCODAGE;
                  ,NO_EMAIL = this.w_AN_EMAIL;
                  ,NO_EMPEC = this.w_AN_EMPEC;
                  ,NO__NOTE = this.w_AN__NOTE;
                  ,NO___CAP = this.w_AN___CAP;
                  ,NOSOGGET = this.w_PERFIS;
                  ,NOLOCNAS = this.w_NATOA;
                  ,NOPRONAS = this.w_PROV;
                  ,NODATNAS = this.w_NATOIL;
                  ,NO_SESSO = this.w_SESSO;
                  ,NONUMCAR = this.w_NUMCAR;
                  ,NOCOGNOM = this.w_COGNOM;
                  ,NO__NOME = this.w_NOME;
                  ,NOCODSAL = this.w_ANCODSAL;
                  ,NONUMCEL = this.w_ANNUMCEL;
                  ,NOCHKSTA = this.w_CHKSTA;
                  ,NOCHKMAI = this.w_CHKMAIL;
                  ,NOCHKPEC = this.w_CHKMPEC;
                  ,NOCHKFAX = this.w_CHKFAX;
                  ,NOCHKCPZ = "N";
                  ,NOCHKWWP = "N";
                  ,NOCHKPTL = "N";
                  ,NOCODBAN = this.w_ANCODBAN;
                  ,NOCODBA2 = this.w_ANCODBA2;
                  ,NONUMLIS = this.w_ANNUMLIS;
                  ,NOCODPAG = this.w_ANCODPAG;
                  ,NO_SKYPE = this.w_AN_SKYPE;
                  ,NOCATCOM = this.w_ANCATCOM;
                  &i_ccchkf. ;
               where;
                  NOCODCLI = this.w_TOTCOD;
                  and NOTIPCLI = this.w_ANTIPCON;

              i_Rows = _tally
            endif
            if i_commit
              cp_EndTrs(.t.)
            endif
            if bTrsErr
              i_Error=MSG_WRITE_ERROR
              return
            endif
          endif
        case this.pTipOpe="D"
          * --- Eliminato Cliente: toglie il riferimento in Anagrafica Nominativi e setta flag a Da valutare/Nominativo
          this.w_NOTIPNOM = "T"
          * --- Select from OFF_NOMI
          i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select NOCODICE  from "+i_cTable+" OFF_NOMI ";
                +" where NOCODCLI="+cp_ToStrODBC(this.w_TOTCOD)+" AND NOTIPCLI="+cp_ToStrODBC(this.w_ANTIPCON)+"";
                 ,"_Curs_OFF_NOMI")
          else
            select NOCODICE from (i_cTable);
             where NOCODCLI=this.w_TOTCOD AND NOTIPCLI=this.w_ANTIPCON;
              into cursor _Curs_OFF_NOMI
          endif
          if used('_Curs_OFF_NOMI')
            select _Curs_OFF_NOMI
            locate for 1=1
            do while not(eof())
            this.w_NOMCOD = NVL(_Curs_OFF_NOMI.NOCODICE,"")
            if NOT EMPTY(this.w_NOMCOD)
              * --- Write into OFF_NOMI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"NOTIPNOM ="+cp_NullLink(cp_ToStrODBC(this.w_NOTIPNOM),'OFF_NOMI','NOTIPNOM');
                +",NOCODCLI ="+cp_NullLink(cp_ToStrODBC(this.w_CLIVUOTO),'OFF_NOMI','NOCODCLI');
                    +i_ccchkf ;
                +" where ";
                    +"NOCODICE = "+cp_ToStrODBC(this.w_NOMCOD);
                       )
              else
                update (i_cTable) set;
                    NOTIPNOM = this.w_NOTIPNOM;
                    ,NOCODCLI = this.w_CLIVUOTO;
                    &i_ccchkf. ;
                 where;
                    NOCODICE = this.w_NOMCOD;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
            endif
              select _Curs_OFF_NOMI
              continue
            enddo
            use
          endif
        case this.pTipOpe="C"
          * --- Caricamento nuovo cliente
          this.w_CODICE = this.w_TOTCOD
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
      endcase
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Caricamento nuovo cliente offerte
    * --- Lettura tipo codifica cliente numerica/alfanumerica
    this.w_OK = .T.
    * --- Read from PAR_OFFE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.PAR_OFFE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PAR_OFFE_idx,2],.t.,this.PAR_OFFE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "POFLCOOF,POCHKIVA"+;
        " from "+i_cTable+" PAR_OFFE where ";
            +"POCODAZI = "+cp_ToStrODBC(i_codazi);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        POFLCOOF,POCHKIVA;
        from (i_cTable) where;
            POCODAZI = i_codazi;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLGNUM = NVL(cp_ToDate(_read_.POFLCOOF),cp_NullValue(_read_.POFLCOOF))
      this.w_CHKIVA = NVL(cp_ToDate(_read_.POCHKIVA),cp_NullValue(_read_.POCHKIVA))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Lettura valori del cliente in azienda da copiare nell'archivio clienti delle offerte
    if upper(g_APPLICATION) <> "ADHOC REVOLUTION"
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_CODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              ANTIPCON = this.w_ANTIPCON;
              and ANCODICE = this.w_CODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANDESCRI = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
        this.w_ANINDIRI = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
        this.w_AN___CAP = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
        this.w_ANLOCALI = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
        this.w_ANPROVIN = NVL(cp_ToDate(_read_.ANPROVIN),cp_NullValue(_read_.ANPROVIN))
        this.w_ANNAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
        this.w_ANTELEFO = NVL(cp_ToDate(_read_.ANTELEFO),cp_NullValue(_read_.ANTELEFO))
        this.w_ANTELFAX = NVL(cp_ToDate(_read_.ANTELFAX),cp_NullValue(_read_.ANTELFAX))
        this.w_ANNUMCEL = NVL(cp_ToDate(_read_.ANNUMCEL),cp_NullValue(_read_.ANNUMCEL))
        this.w_ANCODFIS = NVL(cp_ToDate(_read_.ANCODFIS),cp_NullValue(_read_.ANCODFIS))
        this.w_ANPARIVA = NVL(cp_ToDate(_read_.ANPARIVA),cp_NullValue(_read_.ANPARIVA))
        this.w_ANCODLIS = NVL(cp_ToDate(_read_.ANNUMLIS),cp_NullValue(_read_.ANNUMLIS))
        this.w_ANINDWEB = NVL(cp_ToDate(_read_.ANINDWEB),cp_NullValue(_read_.ANINDWEB))
        this.w_AN_EMAIL = NVL(cp_ToDate(_read_.AN_EMAIL),cp_NullValue(_read_.AN_EMAIL))
        this.w_AN_EMPEC = NVL(cp_ToDate(_read_.AN_EMPEC),cp_NullValue(_read_.AN_EMPEC))
        this.w_ANDTINVA = NVL(cp_ToDate(_read_.ANDTINVA),cp_NullValue(_read_.ANDTINVA))
        this.w_ANDTOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        this.w_ANCODZON = NVL(cp_ToDate(_read_.ANCODZON),cp_NullValue(_read_.ANCODZON))
        this.w_ANCODLIN = NVL(cp_ToDate(_read_.ANCODLIN),cp_NullValue(_read_.ANCODLIN))
        this.w_ANCODVAL = NVL(cp_ToDate(_read_.ANCODVAL),cp_NullValue(_read_.ANCODVAL))
        this.w_ANCODAGE = NVL(cp_ToDate(_read_.ANCODAG1),cp_NullValue(_read_.ANCODAG1))
        this.w_ANDESCR2 = NVL(cp_ToDate(_read_.ANDESCR2),cp_NullValue(_read_.ANDESCR2))
        this.w_ANINDIR2 = NVL(cp_ToDate(_read_.ANINDIR2),cp_NullValue(_read_.ANINDIR2))
        this.w_COGNOM = NVL(cp_ToDate(_read_.ANCOGNOM),cp_NullValue(_read_.ANCOGNOM))
        this.w_NOME = NVL(cp_ToDate(_read_.AN__NOME),cp_NullValue(_read_.AN__NOME))
        this.w_NATOA = NVL(cp_ToDate(_read_.ANLOCNAS),cp_NullValue(_read_.ANLOCNAS))
        this.w_PROV = NVL(cp_ToDate(_read_.ANPRONAS),cp_NullValue(_read_.ANPRONAS))
        this.w_NATOIL = NVL(cp_ToDate(_read_.ANDATNAS),cp_NullValue(_read_.ANDATNAS))
        this.w_SESSO = NVL(cp_ToDate(_read_.AN_SESSO),cp_NullValue(_read_.AN_SESSO))
        this.w_NUMCAR = NVL(cp_ToDate(_read_.ANNUMCAR),cp_NullValue(_read_.ANNUMCAR))
        this.w_PERFIS = NVL(cp_ToDate(_read_.ANPERFIS),cp_NullValue(_read_.ANPERFIS))
        this.w_CHKSTA = NVL(cp_ToDate(_read_.ANCHKSTA),cp_NullValue(_read_.ANCHKSTA))
        this.w_CHKMAIL = NVL(cp_ToDate(_read_.ANCHKMAI),cp_NullValue(_read_.ANCHKMAI))
        this.w_CHKMPEC = NVL(cp_ToDate(_read_.ANCHKPEC),cp_NullValue(_read_.ANCHKPEC))
        this.w_CHKFAX = NVL(cp_ToDate(_read_.ANCHKFAX),cp_NullValue(_read_.ANCHKFAX))
        this.w_CHKCPZ = NVL(cp_ToDate(_read_.ANCHKCPZ),cp_NullValue(_read_.ANCHKCPZ))
        this.w_CHKWWP = NVL(cp_ToDate(_read_.ANCHKWWP),cp_NullValue(_read_.ANCHKWWP))
        this.w_CHKPTL = NVL(cp_ToDate(_read_.ANCHKPTL),cp_NullValue(_read_.ANCHKPTL))
        this.w_ANCODSAL = NVL(cp_ToDate(_read_.ANCODSAL),cp_NullValue(_read_.ANCODSAL))
        this.w_ANCODBAN = NVL(cp_ToDate(_read_.ANCODBAN),cp_NullValue(_read_.ANCODBAN))
        this.w_ANCODBA2 = NVL(cp_ToDate(_read_.ANCODBA2),cp_NullValue(_read_.ANCODBA2))
        this.w_AN_SKYPE = NVL(cp_ToDate(_read_.AN_SKYPE),cp_NullValue(_read_.AN_SKYPE))
        this.w_AN__NOTE = NVL(cp_ToDate(_read_.AN__NOTE),cp_NullValue(_read_.AN__NOTE))
        this.w_ANCODPAG = NVL(cp_ToDate(_read_.ANCODPAG),cp_NullValue(_read_.ANCODPAG))
        this.w_ANCATCOM = NVL(cp_ToDate(_read_.ANCATCOM),cp_NullValue(_read_.ANCATCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    else
      * --- Read from CONTI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CONTI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "*"+;
          " from "+i_cTable+" CONTI where ";
              +"ANTIPCON = "+cp_ToStrODBC(this.w_ANTIPCON);
              +" and ANCODICE = "+cp_ToStrODBC(this.w_CODICE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          *;
          from (i_cTable) where;
              ANTIPCON = this.w_ANTIPCON;
              and ANCODICE = this.w_CODICE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_ANDESCRI = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
        this.w_ANINDIRI = NVL(cp_ToDate(_read_.ANINDIRI),cp_NullValue(_read_.ANINDIRI))
        this.w_AN___CAP = NVL(cp_ToDate(_read_.AN___CAP),cp_NullValue(_read_.AN___CAP))
        this.w_ANLOCALI = NVL(cp_ToDate(_read_.ANLOCALI),cp_NullValue(_read_.ANLOCALI))
        this.w_ANPROVIN = NVL(cp_ToDate(_read_.ANPROVIN),cp_NullValue(_read_.ANPROVIN))
        this.w_ANNAZION = NVL(cp_ToDate(_read_.ANNAZION),cp_NullValue(_read_.ANNAZION))
        this.w_ANTELEFO = NVL(cp_ToDate(_read_.ANTELEFO),cp_NullValue(_read_.ANTELEFO))
        this.w_ANTELFAX = NVL(cp_ToDate(_read_.ANTELFAX),cp_NullValue(_read_.ANTELFAX))
        this.w_ANNUMCEL = NVL(cp_ToDate(_read_.ANNUMCEL),cp_NullValue(_read_.ANNUMCEL))
        this.w_ANCODFIS = NVL(cp_ToDate(_read_.ANCODFIS),cp_NullValue(_read_.ANCODFIS))
        this.w_ANPARIVA = NVL(cp_ToDate(_read_.ANPARIVA),cp_NullValue(_read_.ANPARIVA))
        this.w_ANCODLIS = NVL(cp_ToDate(_read_.ANNUMLIS),cp_NullValue(_read_.ANNUMLIS))
        this.w_ANINDWEB = NVL(cp_ToDate(_read_.ANINDWEB),cp_NullValue(_read_.ANINDWEB))
        this.w_AN_EMAIL = NVL(cp_ToDate(_read_.AN_EMAIL),cp_NullValue(_read_.AN_EMAIL))
        this.w_AN_EMPEC = NVL(cp_ToDate(_read_.AN_EMPEC),cp_NullValue(_read_.AN_EMPEC))
        this.w_ANDTINVA = NVL(cp_ToDate(_read_.ANDTINVA),cp_NullValue(_read_.ANDTINVA))
        this.w_ANDTOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
        this.w_ANCODZON = NVL(cp_ToDate(_read_.ANCODZON),cp_NullValue(_read_.ANCODZON))
        this.w_ANCODLIN = NVL(cp_ToDate(_read_.ANCODLIN),cp_NullValue(_read_.ANCODLIN))
        this.w_ANCODVAL = NVL(cp_ToDate(_read_.ANCODVAL),cp_NullValue(_read_.ANCODVAL))
        this.w_ANCODAGE = NVL(cp_ToDate(_read_.ANCODAG1),cp_NullValue(_read_.ANCODAG1))
        this.w_ANDESCR2 = NVL(cp_ToDate(_read_.ANDESCR2),cp_NullValue(_read_.ANDESCR2))
        this.w_ANINDIR2 = NVL(cp_ToDate(_read_.ANINDIR2),cp_NullValue(_read_.ANINDIR2))
        this.w_COGNOM = NVL(cp_ToDate(_read_.ANCOGNOM),cp_NullValue(_read_.ANCOGNOM))
        this.w_NOME = NVL(cp_ToDate(_read_.AN__NOME),cp_NullValue(_read_.AN__NOME))
        this.w_NATOA = NVL(cp_ToDate(_read_.ANLOCNAS),cp_NullValue(_read_.ANLOCNAS))
        this.w_PROV = NVL(cp_ToDate(_read_.ANPRONAS),cp_NullValue(_read_.ANPRONAS))
        this.w_NATOIL = NVL(cp_ToDate(_read_.ANDATNAS),cp_NullValue(_read_.ANDATNAS))
        this.w_SESSO = NVL(cp_ToDate(_read_.AN_SESSO),cp_NullValue(_read_.AN_SESSO))
        this.w_NUMCAR = NVL(cp_ToDate(_read_.ANNUMCAR),cp_NullValue(_read_.ANNUMCAR))
        this.w_PERFIS = NVL(cp_ToDate(_read_.ANPERFIS),cp_NullValue(_read_.ANPERFIS))
        this.w_CHKSTA = NVL(cp_ToDate(_read_.ANCHKSTA),cp_NullValue(_read_.ANCHKSTA))
        this.w_CHKMAIL = NVL(cp_ToDate(_read_.ANCHKMAI),cp_NullValue(_read_.ANCHKMAI))
        this.w_CHKMPEC = NVL(cp_ToDate(_read_.ANCHKPEC),cp_NullValue(_read_.ANCHKPEC))
        this.w_CHKFAX = NVL(cp_ToDate(_read_.ANCHKFAX),cp_NullValue(_read_.ANCHKFAX))
        this.w_CHKCPZ = NVL(cp_ToDate(_read_.ANCHKCPZ),cp_NullValue(_read_.ANCHKCPZ))
        this.w_CHKWWP = NVL(cp_ToDate(_read_.ANCHKWWP),cp_NullValue(_read_.ANCHKWWP))
        this.w_CHKPTL = NVL(cp_ToDate(_read_.ANCHKPTL),cp_NullValue(_read_.ANCHKPTL))
        this.w_ANCODSAL = NVL(cp_ToDate(_read_.ANCODSAL),cp_NullValue(_read_.ANCODSAL))
        this.w_ANCODBAN = NVL(cp_ToDate(_read_.ANCODBAN),cp_NullValue(_read_.ANCODBAN))
        this.w_ANCODBA2 = NVL(cp_ToDate(_read_.ANCODBA2),cp_NullValue(_read_.ANCODBA2))
        this.w_AN_SKYPE = NVL(cp_ToDate(_read_.AN_SKYPE),cp_NullValue(_read_.AN_SKYPE))
        this.w_AN__NOTE = NVL(cp_ToDate(_read_.AN__NOTE),cp_NullValue(_read_.AN__NOTE))
        this.w_ANCODPAG = NVL(cp_ToDate(_read_.ANCODPAG),cp_NullValue(_read_.ANCODPAG))
        this.w_ANCATCOM = NVL(cp_ToDate(_read_.ANCATCOM),cp_NullValue(_read_.ANCATCOM))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
    endif
    this.w_CODCLI = this.w_CODICE
    if NOT EMPTY(this.w_ANPARIVA)
      * --- Controllo su  partita iva escludendo i nominativi obsoleti ad oggi
      this.w_CONTA = 0
      * --- Select from OFF_NOMI
      i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select * from "+i_cTable+" OFF_NOMI ";
            +" where NOPARIVA="+cp_ToStrODBC(this.w_ANPARIVA)+"";
             ,"_Curs_OFF_NOMI")
      else
        select * from (i_cTable);
         where NOPARIVA=this.w_ANPARIVA;
          into cursor _Curs_OFF_NOMI
      endif
      if used('_Curs_OFF_NOMI')
        select _Curs_OFF_NOMI
        locate for 1=1
        do while not(eof())
        this.w_OK = Not ( Empty( nvl(_Curs_OFF_NOMI.NODTOBSO, cp_chartodate("  -  -   ") )) Or nvl(_Curs_OFF_NOMI.NODTOBSO, cp_chartodate("  -  -   ") )> i_DATSYS )
        this.w_CODNOM = nvl(_Curs_OFF_NOMI.NOCODICE, space(15))
        this.w_TIPNOM = nvl(_Curs_OFF_NOMI.NOTIPNOM, space(1))
        if this.w_TIPNOM<>"C" AND this.w_TIPNOM<>"F" AND !this.w_OK
          * --- Nominativo non associato n� a cliente n� a fornitore
          this.w_CONTA = this.w_CONTA +1 
        endif
          select _Curs_OFF_NOMI
          continue
        enddo
        use
      endif
      if this.w_CHKIVA<>"S"
        * --- Controllo partita Iva spento
        do case
          case this.w_CONTA=0
            * --- Se trovo un nominativo gi� associato ad altro cliente inserisco un altro nominativo 
            this.w_OK = .T.
          case this.w_CONTA=1
            * --- Se trovo un nominativo libero d� la possibilit� di collegarlo al cliente
            if AH_YESNO("Attenzione: � gi� presente un nominativo con stessa partita IVA e non associato a cliente%0Si desidera associare al cliente questo nominativo?")
              * --- Write into OFF_NOMI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"NOCODCLI ="+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'OFF_NOMI','NOCODCLI');
                +",NOTIPNOM ="+cp_NullLink(cp_ToStrODBC(this.w_ANTIPCON),'OFF_NOMI','NOTIPNOM');
                    +i_ccchkf ;
                +" where ";
                    +"NOCODICE = "+cp_ToStrODBC(this.w_CODNOM);
                       )
              else
                update (i_cTable) set;
                    NOCODCLI = this.w_CODICE;
                    ,NOTIPNOM = this.w_ANTIPCON;
                    &i_ccchkf. ;
                 where;
                    NOCODICE = this.w_CODNOM;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              AH_ErrorMsg("Cliente associato a nominativo",48)
              this.w_COLL = .T.
            else
              * --- Se non collego inserisco un altro nominativo con stessa partita iva
              this.w_OK = .T.
            endif
          case this.w_CONTA>1
            if AH_YESNO("Attenzione: sono gi� presenti nominativi con stessa partita IVA non associati a clienti%0Si desidera visualizzare l'elenco dei nominativi associabili?")
              do GSAR_KOL with this
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            * --- Se trovo pi� nominativi liberi d� la possibilit� di sceglierli
            if NOT EMPTY(this.w_NOMCOD)
              * --- Se ne ho selezionato uno lo associo
              * --- Write into OFF_NOMI
              i_commit = .f.
              if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
                cp_BeginTrs()
                i_commit = .t.
              endif
              i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
              i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
              i_ccchkf=''
              this.SetCCCHKVarsWrite(@i_ccchkf,this.OFF_NOMI_idx,i_nConn)
              if i_nConn<>0
                i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
                +"NOCODCLI ="+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'OFF_NOMI','NOCODCLI');
                +",NOTIPNOM ="+cp_NullLink(cp_ToStrODBC("C"),'OFF_NOMI','NOTIPNOM');
                    +i_ccchkf ;
                +" where ";
                    +"NOCODICE = "+cp_ToStrODBC(this.w_NOMCOD);
                       )
              else
                update (i_cTable) set;
                    NOCODCLI = this.w_CODICE;
                    ,NOTIPNOM = "C";
                    &i_ccchkf. ;
                 where;
                    NOCODICE = this.w_NOMCOD;

                i_Rows = _tally
              endif
              if i_commit
                cp_EndTrs(.t.)
              endif
              if bTrsErr
                i_Error=MSG_WRITE_ERROR
                return
              endif
              AH_ErrorMsg("Cliente associato a nominativo",48)
              this.w_COLL = .T.
            else
              * --- Se non collego inserisco un altro nominativo con stessa partita iva
              this.w_OK = .T.
            endif
        endcase
      else
        if Not this.w_OK
          AH_ErrorMsg("Nominativo non generato, cliente con stessa partita IVA gi� presente tra i nominativi",48)
          i_retcode = 'stop'
          return
        endif
      endif
    endif
    if !this.oParentObject.w_DANOM
      if this.w_FLGNUM="S"
        * --- Try
        local bErr_0348BF48
        bErr_0348BF48=bTrsErr
        this.Try_0348BF48()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          AH_ErrorMsg("Cliente non inserito nei nominativi (errore determinazione progressivo)",48)
          i_retcode = 'stop'
          return
        endif
        bTrsErr=bTrsErr or bErr_0348BF48
        * --- End
      else
        * --- Read from OFF_NOMI
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "*"+;
            " from "+i_cTable+" OFF_NOMI where ";
                +"NOCODICE = "+cp_ToStrODBC(this.w_CODICE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            *;
            from (i_cTable) where;
                NOCODICE = this.w_CODICE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows<>0
          AH_ErrorMsg("Attenzione:%0La codifica � gi� presente nell'archivio nominativi",48)
          * --- Nel caso esiste gi� la codifica chiedo di inserirne una nuova
          this.w_OK = .F.
          * --- Cicla finch� non si immette un codice valido o si preme Annulla
          this.w_ESCI = .F.
          do while this.w_ESCI=.F.
            this.w_OK = .F.
            do GSOF_KCP with this
            if i_retcode='stop' or !empty(i_Error)
              return
            endif
            * --- Read from OFF_NOMI
            i_nOldArea=select()
            if used('_read_')
              select _read_
              use
            endif
            i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
            i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
            if i_nConn<>0
              cp_sqlexec(i_nConn,"select "+;
                "*"+;
                " from "+i_cTable+" OFF_NOMI where ";
                    +"NOCODICE = "+cp_ToStrODBC(this.w_NEWCOD);
                     ,"_read_")
              i_Rows=iif(used('_read_'),reccount(),0)
            else
              select;
                *;
                from (i_cTable) where;
                    NOCODICE = this.w_NEWCOD;
                 into cursor _read_
              i_Rows=_tally
            endif
            if used('_read_')
              locate for 1=1
              use
            else
              * --- Error: sql sentence error.
              i_Error = MSG_READ_ERROR
              return
            endif
            select (i_nOldArea)
            if i_Rows=0 OR NOT(this.w_OK)
              this.w_ESCI = .T.
            else
              AH_ErrorMsg("Codifica gi� presente in archivio nominativi",48)
            endif
          enddo
          if this.w_OK
            this.w_CODCLI = IIF(NOT EMPTY(this.w_NEWCOD),this.w_NEWCOD,this.w_CODCLI)
            this.w_CODICE = LEFT(this.w_CODCLI,15)
          endif
        endif
      endif
    endif
    * --- Inserimento nuovo cliente offerte
    if this.w_OK
      if g_CFNUME="S"
        if LEN(ALLTRIM(p_CLF))<>0
          this.w_CODICE = RIGHT(this.w_CODICE, LEN(ALLTRIM(p_CLF)))
        endif
      else
        if LEN(ALLTRIM(p_CLF))<>0
          this.w_CODICE = LEFT(this.w_CODICE, LEN(ALLTRIM(p_CLF)))
        endif
      endif
      * --- Se cliente persona fisica allora valorizzo soggetto a persona fisica altrimenti Ente
      this.w_PERFIS = iif( this.w_PERFIS="S" , "PF","EN") 
      * --- Attenzione! In caso di variazioni nella insert modificare anche gsar_bgn
      if !this.oParentObject.w_DANOM
        * --- Insert into OFF_NOMI
        i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.OFF_NOMI_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"NOTIPNOM"+",NOTIPCLI"+",NOCHKWWP"+",NOCHKPTL"+",NOCHKCPZ"+",NO___CAP"+",NO__NOTE"+",NO_EMAIL"+",NO_EMPEC"+",NO_SKYPE"+",NOCODAGE"+",NOCODBA2"+",NOCODBAN"+",NOCODFIS"+",NOCODLIN"+",NOCODSAL"+",NOCODVAL"+",NOCODZON"+",NODESCR2"+",NODESCRI"+",NODTINVA"+",NODTOBSO"+",NOINDI_2"+",NOINDIRI"+",NOINDWEB"+",NOLOCALI"+",NONAZION"+",NONUMCEL"+",NOPARIVA"+",NOPROVIN"+",NOTELEFO"+",NOTELFAX"+",NOCHKFAX"+",NOCHKMAI"+",NOCHKPEC"+",NOCHKSTA"+",NOCODICE"+",NOCODCLI"+",NOCOGNOM"+",NOLOCNAS"+",NODATNAS"+",NO__NOME"+",NONUMCAR"+",NOSOGGET"+",NOPRONAS"+",NO_SESSO"+",NONUMLIS"+",NOCODPAG"+",NOCATCOM"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_ANTIPCON),'OFF_NOMI','NOTIPNOM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANTIPCON),'OFF_NOMI','NOTIPCLI');
          +","+cp_NullLink(cp_ToStrODBC("N"),'OFF_NOMI','NOCHKWWP');
          +","+cp_NullLink(cp_ToStrODBC("N"),'OFF_NOMI','NOCHKPTL');
          +","+cp_NullLink(cp_ToStrODBC("N"),'OFF_NOMI','NOCHKCPZ');
          +","+cp_NullLink(cp_ToStrODBC(this.w_AN___CAP),'OFF_NOMI','NO___CAP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_AN__NOTE),'OFF_NOMI','NO__NOTE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_AN_EMAIL),'OFF_NOMI','NO_EMAIL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_AN_EMPEC),'OFF_NOMI','NO_EMPEC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_AN_SKYPE),'OFF_NOMI','NO_SKYPE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODAGE),'OFF_NOMI','NOCODAGE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODBA2),'OFF_NOMI','NOCODBA2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODBAN),'OFF_NOMI','NOCODBAN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODFIS),'OFF_NOMI','NOCODFIS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODLIN),'OFF_NOMI','NOCODLIN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODSAL),'OFF_NOMI','NOCODSAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODVAL),'OFF_NOMI','NOCODVAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODZON),'OFF_NOMI','NOCODZON');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANDESCR2),'OFF_NOMI','NODESCR2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANDESCRI),'OFF_NOMI','NODESCRI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANDTINVA),'OFF_NOMI','NODTINVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANDTOBSO),'OFF_NOMI','NODTOBSO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANINDIR2),'OFF_NOMI','NOINDI_2');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANINDIRI),'OFF_NOMI','NOINDIRI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANINDWEB),'OFF_NOMI','NOINDWEB');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANLOCALI),'OFF_NOMI','NOLOCALI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANNAZION),'OFF_NOMI','NONAZION');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANNUMCEL),'OFF_NOMI','NONUMCEL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANPARIVA),'OFF_NOMI','NOPARIVA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANPROVIN),'OFF_NOMI','NOPROVIN');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANTELEFO),'OFF_NOMI','NOTELEFO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANTELFAX),'OFF_NOMI','NOTELFAX');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CHKFAX),'OFF_NOMI','NOCHKFAX');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CHKMAIL),'OFF_NOMI','NOCHKMAI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CHKMPEC),'OFF_NOMI','NOCHKPEC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CHKSTA),'OFF_NOMI','NOCHKSTA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODCLI),'OFF_NOMI','NOCODICE');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CODICE),'OFF_NOMI','NOCODCLI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_COGNOM),'OFF_NOMI','NOCOGNOM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NATOA),'OFF_NOMI','NOLOCNAS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NATOIL),'OFF_NOMI','NODATNAS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NOME),'OFF_NOMI','NO__NOME');
          +","+cp_NullLink(cp_ToStrODBC(this.w_NUMCAR),'OFF_NOMI','NONUMCAR');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PERFIS),'OFF_NOMI','NOSOGGET');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PROV),'OFF_NOMI','NOPRONAS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_SESSO),'OFF_NOMI','NO_SESSO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODLIS),'OFF_NOMI','NONUMLIS');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANCODPAG),'OFF_NOMI','NOCODPAG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_ANCATCOM),'OFF_NOMI','NOCATCOM');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'NOTIPNOM',this.w_ANTIPCON,'NOTIPCLI',this.w_ANTIPCON,'NOCHKWWP',"N",'NOCHKPTL',"N",'NOCHKCPZ',"N",'NO___CAP',this.w_AN___CAP,'NO__NOTE',this.w_AN__NOTE,'NO_EMAIL',this.w_AN_EMAIL,'NO_EMPEC',this.w_AN_EMPEC,'NO_SKYPE',this.w_AN_SKYPE,'NOCODAGE',this.w_ANCODAGE,'NOCODBA2',this.w_ANCODBA2)
          insert into (i_cTable) (NOTIPNOM,NOTIPCLI,NOCHKWWP,NOCHKPTL,NOCHKCPZ,NO___CAP,NO__NOTE,NO_EMAIL,NO_EMPEC,NO_SKYPE,NOCODAGE,NOCODBA2,NOCODBAN,NOCODFIS,NOCODLIN,NOCODSAL,NOCODVAL,NOCODZON,NODESCR2,NODESCRI,NODTINVA,NODTOBSO,NOINDI_2,NOINDIRI,NOINDWEB,NOLOCALI,NONAZION,NONUMCEL,NOPARIVA,NOPROVIN,NOTELEFO,NOTELFAX,NOCHKFAX,NOCHKMAI,NOCHKPEC,NOCHKSTA,NOCODICE,NOCODCLI,NOCOGNOM,NOLOCNAS,NODATNAS,NO__NOME,NONUMCAR,NOSOGGET,NOPRONAS,NO_SESSO,NONUMLIS,NOCODPAG,NOCATCOM &i_ccchkf. );
             values (;
               this.w_ANTIPCON;
               ,this.w_ANTIPCON;
               ,"N";
               ,"N";
               ,"N";
               ,this.w_AN___CAP;
               ,this.w_AN__NOTE;
               ,this.w_AN_EMAIL;
               ,this.w_AN_EMPEC;
               ,this.w_AN_SKYPE;
               ,this.w_ANCODAGE;
               ,this.w_ANCODBA2;
               ,this.w_ANCODBAN;
               ,this.w_ANCODFIS;
               ,this.w_ANCODLIN;
               ,this.w_ANCODSAL;
               ,this.w_ANCODVAL;
               ,this.w_ANCODZON;
               ,this.w_ANDESCR2;
               ,this.w_ANDESCRI;
               ,this.w_ANDTINVA;
               ,this.w_ANDTOBSO;
               ,this.w_ANINDIR2;
               ,this.w_ANINDIRI;
               ,this.w_ANINDWEB;
               ,this.w_ANLOCALI;
               ,this.w_ANNAZION;
               ,this.w_ANNUMCEL;
               ,this.w_ANPARIVA;
               ,this.w_ANPROVIN;
               ,this.w_ANTELEFO;
               ,this.w_ANTELFAX;
               ,this.w_CHKFAX;
               ,this.w_CHKMAIL;
               ,this.w_CHKMPEC;
               ,this.w_CHKSTA;
               ,this.w_CODCLI;
               ,this.w_CODICE;
               ,this.w_COGNOM;
               ,this.w_NATOA;
               ,this.w_NATOIL;
               ,this.w_NOME;
               ,this.w_NUMCAR;
               ,this.w_PERFIS;
               ,this.w_PROV;
               ,this.w_SESSO;
               ,this.w_ANCODLIS;
               ,this.w_ANCODPAG;
               ,this.w_ANCATCOM;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
        this.w_PROGR = 1
        * --- Select from CONTATTI
        i_nConn=i_TableProp[this.CONTATTI_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CONTATTI_idx,2],.t.,this.CONTATTI_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select COINDMAI,COINDPEC,CONUMTEL,COTIPDAT,CODESDIV,CORIFPER,CONUMCEL,CO_SKYPE  from "+i_cTable+" CONTATTI ";
              +" where COTIPCON="+cp_ToStrODBC(this.w_ANTIPCON)+" AND COCODCON="+cp_ToStrODBC(this.w_CODICE)+"";
               ,"_Curs_CONTATTI")
        else
          select COINDMAI,COINDPEC,CONUMTEL,COTIPDAT,CODESDIV,CORIFPER,CONUMCEL,CO_SKYPE from (i_cTable);
           where COTIPCON=this.w_ANTIPCON AND COCODCON=this.w_CODICE;
            into cursor _Curs_CONTATTI
        endif
        if used('_Curs_CONTATTI')
          select _Curs_CONTATTI
          locate for 1=1
          do while not(eof())
          * --- Insert into NOM_CONT
          i_nConn=i_TableProp[this.NOM_CONT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.NOM_CONT_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.NOM_CONT_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"NCCODICE"+",NCCODCON"+",NCPERSON"+",NCTELEFO"+",NC_EMAIL"+",NC_EMPEC"+",NCNUMCEL"+",CPROWORD"+",NC_SKYPE"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_CODCLI),'NOM_CONT','NCCODICE');
            +","+cp_NullLink(cp_ToStrODBC(PADL(alltrim(str(this.w_PROGR, 5, 0)), 5, "0")),'NOM_CONT','NCCODCON');
            +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(_Curs_CONTATTI.CORIFPER)),'NOM_CONT','NCPERSON');
            +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(_Curs_CONTATTI.CONUMTEL)),'NOM_CONT','NCTELEFO');
            +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(_Curs_CONTATTI.COINDMAI)),'NOM_CONT','NC_EMAIL');
            +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(_Curs_CONTATTI.COINDPEC)),'NOM_CONT','NC_EMPEC');
            +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(_Curs_CONTATTI.CONUMCEL)),'NOM_CONT','NCNUMCEL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PROGR*10),'NOM_CONT','CPROWORD');
            +","+cp_NullLink(cp_ToStrODBC(ALLTRIM(_Curs_CONTATTI.CO_SKYPE)),'NOM_CONT','NC_SKYPE');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'NCCODICE',this.w_CODCLI,'NCCODCON',PADL(alltrim(str(this.w_PROGR, 5, 0)), 5, "0"),'NCPERSON',ALLTRIM(_Curs_CONTATTI.CORIFPER),'NCTELEFO',ALLTRIM(_Curs_CONTATTI.CONUMTEL),'NC_EMAIL',ALLTRIM(_Curs_CONTATTI.COINDMAI),'NC_EMPEC',ALLTRIM(_Curs_CONTATTI.COINDPEC),'NCNUMCEL',ALLTRIM(_Curs_CONTATTI.CONUMCEL),'CPROWORD',this.w_PROGR*10,'NC_SKYPE',ALLTRIM(_Curs_CONTATTI.CO_SKYPE))
            insert into (i_cTable) (NCCODICE,NCCODCON,NCPERSON,NCTELEFO,NC_EMAIL,NC_EMPEC,NCNUMCEL,CPROWORD,NC_SKYPE &i_ccchkf. );
               values (;
                 this.w_CODCLI;
                 ,PADL(alltrim(str(this.w_PROGR, 5, 0)), 5, "0");
                 ,ALLTRIM(_Curs_CONTATTI.CORIFPER);
                 ,ALLTRIM(_Curs_CONTATTI.CONUMTEL);
                 ,ALLTRIM(_Curs_CONTATTI.COINDMAI);
                 ,ALLTRIM(_Curs_CONTATTI.COINDPEC);
                 ,ALLTRIM(_Curs_CONTATTI.CONUMCEL);
                 ,this.w_PROGR*10;
                 ,ALLTRIM(_Curs_CONTATTI.CO_SKYPE);
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_PROGR = this.w_PROGR + 1
            select _Curs_CONTATTI
            continue
          enddo
          use
        endif
        this.w_ANTIPCON = " "
        if !IsAlt()
          AH_ErrorMsg("Caricamento cliente nei nominativi completata",64)
        endif
      endif
    else
      if !this.w_COLL
        AH_ErrorMsg("Cliente non inserito nei nominativi",48)
      endif
    endif
  endproc
  proc Try_0348BF48()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_NOCODICE = SPACE(20)
    * --- Nel caso di codifica numerica ricalcolo il progressivo
    i_Conn=i_TableProp[this.OFF_NOMI_IDX, 3]
    cp_NextTableProg(this, i_Conn, "PRNUNMOF", "i_codazi,w_NOCODICE")
    p_LL=LEN(ALLTRIM(p_NOM))
    * --- Ridimensiono il codice in base alla 'maschera'  impostata sui parametri negozio principale
    this.w_CODCLI = RIGHT(ALLTRIM(this.w_NOCODICE),p_LL)
    * --- commit
    cp_EndTrs(.t.)
    return


  proc Init(oParentObject,pTipOpe,pANCODICE,pCliFor)
    this.pTipOpe=pTipOpe
    this.pANCODICE=pANCODICE
    this.pCliFor=pCliFor
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='OFF_NOMI'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='PAR_OFFE'
    this.cWorkTables[4]='CONTATTI'
    this.cWorkTables[5]='NOM_CONT'
    return(this.OpenAllTables(5))

  proc CloseCursors()
    if used('_Curs_OFF_NOMI')
      use in _Curs_OFF_NOMI
    endif
    if used('_Curs_OFF_NOMI')
      use in _Curs_OFF_NOMI
    endif
    if used('_Curs_CONTATTI')
      use in _Curs_CONTATTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipOpe,pANCODICE,pCliFor"
endproc
