* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bdt                                                        *
*              Driver per file di testo                                        *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_52]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2003-05-16                                                      *
* Last revis.: 2013-05-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bdt",oParentObject,m.pCursore,m.pTXTFile,m.pObjMsg,m.p_ArrayFieldName,m.p_ArrayFieldPosition)
return(i_retval)

define class tgsut_bdt as StdBatch
  * --- Local variables
  pCursore = space(10)
  pTXTFile = space(254)
  pObjMsg = .NULL.
  p_ArrayFieldName = space(40)
  p_ArrayFieldPosition = space(40)
  w_DHELIFIL = space(1)
  w_DHTRAPER = space(0)
  w_LOOP = 0
  w_NumRip = 0
  w_Errore = .f.
  W_FileOut = space(250)
  w_FileHandle = 0
  w_FileEnd = 0
  w_FileTop = 0
  w_StrLetta = space(250)
  w_RecTot = 0
  w_RecErrati = 0
  w_LOGWriteHandle = space(12)
  w_StrDaIns = space(0)
  w_ErrLOG = space(250)
  w_NUltimoRec = 0
  w_AttStr = space(250)
  w_MESS = space(250)
  w_Loop = 0
  w_RESULT = space(100)
  w_CHECK = space(100)
  * --- WorkFile variables
  DIS_HARD_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Driver per file di testo.
    *     ========================================================
    *     Utilizzare questo batch come driver per caricare dati da file di testo
    *     tramite l'INSERIMENTO DATI (GSAR_BPG=>GSAR_MPG).
    *     
    *     All'interno del codice � cablato un array per definire posizione e contenuto dei vari campi.
    *     
    *     Questo array � mantenuto per poter configurare pi� facilmente la procedura
    *     
    *     
    *     La procedura riceve come parametro il nome del cursore che dovr� riempire.
    *     La struttura del cursore � fissa ed �:
    *     
    *     Create Cursor ( w_CURSORE ) ( CODICE C(20) NULL, UNIMIS C(3) NULL, QTAMOV N(12,3) NULL, PREZZO N(18,5) NULL,;
    *     LOTTO CHAR(20) NULL , CODMAG C(5) NULL,UBICAZIONE C(20) NULL , MATRICOLA C(40) NULL,SERRIF C(10) NULL )
    *     
    *     All'interno del batch vi � una chiamata per aprire una maschera di selezione
    *     file di testo se valorizzato il secondo parametro questa non apparir�, il secondo parametro rappresenta il file di testo (con path relativo o assoluto)
    *     dal quale la procedura prelever� i dati. (Da specificare, se fisso, nell'anagrafica
    *     dispositivi nella casella parametro)
    *     
    *     Il terzo parametro rappresenta l'oggetto da utilizzare per visualizzare i messaggi (deve contenere un campo memo w_MSG).
    *     Il quarto e quinto parametro contengono le informazioni riguardanti posizione e nome campo (sono array passati come riferimento)
    * --- Definizione array che configura file di testo
    *     Possibile definirlo prima della chiamata al Driver...
    *     
    *     Se non passati li definisco di Default...
    *     
    *     A T T E N Z I O N E  Utilizzare il nome degli Array con [ adiacente al nome (senza
    *     spazi intermedi, Gli array non devono essere propriet� del Batch (con lo spazio lo divengono)
    if Type( "this.p_ArrayFieldName") ="L"
      this.w_LOOP = 0
      * --- Se trovo definito l'array utente utilizzo quello per interpretare il file di testo
      * --- Read from DIS_HARD
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.DIS_HARD_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DIS_HARD_idx,2],.t.,this.DIS_HARD_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "DHTRAPER,DHELIFIL"+;
          " from "+i_cTable+" DIS_HARD where ";
              +"DHCODICE = "+cp_ToStrODBC(g_CODPEN);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          DHTRAPER,DHELIFIL;
          from (i_cTable) where;
              DHCODICE = g_CODPEN;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DHTRAPER = NVL(cp_ToDate(_read_.DHTRAPER),cp_NullValue(_read_.DHTRAPER))
        this.w_DHELIFIL = NVL(cp_ToDate(_read_.DHELIFIL),cp_NullValue(_read_.DHELIFIL))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      if vartype(this.w_DHTRAPER)<>"U" and !empty(this.w_DHTRAPER)
        execscript(this.w_DHTRAPER)
      else
        if Type("g_ArrayFieldName")="C" And Type("g_ArrayFieldPosition")="N"
          if ALen(g_ArrayFieldName)>0 And ALen(g_ArrayFieldName)=ALen(g_ArrayFieldPosition)
            * --- Valorizzo gli Array con gli array di definizione
            this.w_LOOP = 1
             
 Dimension p_ArrayFieldName[ALen(g_ArrayFieldName)], p_ArrayFieldPosition[ALen(g_ArrayFieldName)]
            do while this.w_LOOP<=ALen(g_ArrayFieldName)
               
 p_ArrayFieldPosition[ this.w_LOOP ] = g_ArrayFieldPosition[this.w_LOOP] 
 p_ArrayFieldName[ this.w_LOOP ] = g_ArrayFieldName[this.w_LOOP]
              this.w_LOOP = this.w_LOOP + 1
            enddo
          endif
        endif
        if this.w_LOOP=0
           
 Dimension p_ArrayFieldName[7], p_ArrayFieldPosition[7] 
 p_ArrayFieldPosition[1]=1 
 p_ArrayFieldPosition[2]=24 
 p_ArrayFieldPosition[3]=38 
 p_ArrayFieldPosition[4]=57 
 p_ArrayFieldPosition[5]=87 
 p_ArrayFieldPosition[6]=128 
 p_ArrayFieldPosition[7]=0 
 * 
 p_ArrayFieldName[1]="CODICE" 
 p_ArrayFieldName[2]="QTAMOV" 
 p_ArrayFieldName[3]="PREZZO" 
 p_ArrayFieldName[4]="LOTTO" 
 p_ArrayFieldName[5]="MATRICOLA" 
 p_ArrayFieldName[6]="SERRIF" 
 p_ArrayFieldName[7]=""
        endif
      endif
    endif
    L_OldError = on("ERROR")
    this.w_NumRip = 60
    this.w_Errore = .F.
    * --- Scrivo nella directory temporanea eventuale file di errori
    this.w_ErrLOG = tempadhoc()+"\ERR_LOG.TXT"
    if Empty( this.pTXTFile )
      this.w_FileOut = GETFILE("txt",ah_Msgformat("File di testo:"),"",0,g_APPLICATION)
      if Empty( this.w_FileOut )
        AddMsgNL("File di testo non selezionato%0%1",this.oparentobject, REPLICATE("-",this.w_NumRip) )
        i_retcode = 'stop'
        return
      endif
    else
      this.W_FileOut = this.pTXTFile
    endif
    AddMsgNL("%1", this.oparentobject, REPLICATE("-",this.w_NumRip) )
    AddMsgNL("Fase 1: caricamento file%0%1", this.oparentobject, REPLICATE("-",this.w_NumRip) )
    AddMsgNL("File selezionato: %1",this.oparentobject, ALLTRIM(this.w_FileOut) )
    AddMsgNL("%1", this.oparentobject, REPLICATE("-",this.w_NumRip) )
    AddMsgNL("Fase 2: verifica file%0%1", this.oparentobject, REPLICATE("-",this.w_NumRip) )
    this.w_FileHandle = FOPEN(this.w_FileOut,0) 
    this.w_FileEnd = FSEEK(this.w_FileHandle, 0, 2)
    this.w_FileTop = FSEEK(this.w_FileHandle, 0)
    do case
      case NOT FILE(this.w_FileOut)
        AddMsgNL("FILE NON TROVATO",this.oparentobject)
        this.w_Errore = .T.
      case this.w_FileEnd<=0
        AddMsgNL("FILE VUOTO O DATI INCONSISTENTI",this.oparentobject)
        this.w_Errore = .T.
      otherwise
        AddMsgNL("Verifica...OK",this.oparentobject)
    endcase
    FCLOSE(this.w_FileHandle) 
    if this.w_Errore
      AddMsgNL("***Riscontrati errori nel file procedura interrotta *** ",this.oparentobject)
    else
      AddMsgNL("%1", this.oparentobject, REPLICATE("-",this.w_NumRip) )
      AddMsgNL("Fase 3: inserimento informazioni...%0%1", this.oparentobject, REPLICATE("-",this.w_NumRip) )
      this.w_FileHandle = FOPEN(this.w_FileOut,0)
      this.w_FileEnd = FSEEK(this.w_FileHandle, 0, 2)
      this.w_FileTop = FSEEK(this.w_FileHandle, 0)
      this.w_StrLetta = "XXX"
      this.w_RecTot = 0
      this.w_RecErrati = 0
      this.w_LOGWriteHandle = FCREATE(this.w_ErrLOG,0)
      do while NOT EMPTY(this.w_StrLetta) AND NOT EOF(this.w_FileHandle)
        this.w_StrLetta = FGETS(this.w_FileHandle, 8192) 
        if NOT EMPTY(this.w_StrLetta)
          this.w_NUltimoRec = 1
          * --- Inizializzo tutte le varibili della INSERT (capi della tabella) perch� se l'utente 
          *     non li definisce nel vettore di configurazione la insert restituirebbe errore.
          *     In questo modo � possibile parametrizzare i campi da leggere.
          *     A T T E N Z I O N E 
          *     Le variabili debbono essere scritte come L_+Nome campo
           
 L_CODICE="" 
 L_UNIMIS="" 
 L_QTAMOV=0 
 L_PREZZO=0 
 L_LOTTO="" 
 L_CODMAG="" 
 L_UBICAZIONE="" 
 L_MATRICOLA="" 
 L_SERRIF="" 
 L_CODMA2="" 
 L_UBICAZION2="" 
 L_COMMESSA="" 
 L_ATTIVITA="" 
 L_CENTRO="" 
 L_QTAUM1=0 
 L_UNILOG=""
          do while p_ArrayFieldPosition[this.w_NUltimoRec]<>0
            if TYPE("p_ArrayFieldPosition[this.w_NUltimorec+1]") <> "N"
              ah_ErrorMsg("Impossibile identificare la posizione relativa al campo %1",,"", ALLTRIM(STR(this.w_NUltimorec+1)) )
              i_retcode = 'stop'
              return
            else
              if p_ArrayFieldPosition[this.w_NUltimorec+1]<>0
                this.w_AttStr = SUBST(this.w_StrLetta,p_ArrayFieldPosition[this.w_NUltimorec],p_ArrayFieldPosition[this.w_NUltimorec+1]-p_ArrayFieldPosition[this.w_NUltimorec])
              else
                this.w_AttStr = SUBST(this.w_StrLetta,p_ArrayFieldPosition[this.w_NUltimorec],LEN(this.w_StrLetta))
              endif
              this.w_AttStr = ALLTRIM(this.w_AttStr)
              if p_ArrayFieldName[this.w_NUltimoRec]="PREZZO" OR p_ArrayFieldName[this.w_NUltimoRec]="QTAMOV" OR p_ArrayFieldName[this.w_NUltimoRec]="QTAUM1"
                L_Mac="L_"+p_ArrayFieldName[this.w_NUltimoRec]+"="+STRTRAN(IIF(EMPTY(this.w_AttStr),"0",this.w_AttStr),",",".")
              else
                * --- Pulisco eventuali caratteri 'sporchi'
                this.Pag2()
                if i_retcode='stop' or !empty(i_Error)
                  return
                endif
                L_Mac="L_"+p_ArrayFieldName[this.w_NUltimoRec]+"=( this.w_AttStr )"
              endif
              w_ErroreVet=.F.
              ON ERROR w_ErroreVet=.T.
              &L_Mac
              on error &L_OldError 
              if w_ErroreVet
                this.w_StrDaIns = "Dati inseriti non validi o impostazioni vettore importazione errate"
                AddMsgNL(this.w_StrDaIns,this.oparentobject)
              endif
              this.w_NUltimoRec = this.w_NUltimoRec+1
            endif
          enddo
          if Type("pObjMsg")="O"
            this.w_MESS = Ah_MsgFormat("Inserimento record")
            if AT(this.w_MESS,this.pObjMsg.w_Msg)>0
              this.w_StrDaIns = LEFT(this.pObjMsg.w_Msg,AT(this.w_MESS,this.pObjMsg.w_Msg)-1)
              this.pObjMsg.w_MSG=""
            else
              this.w_StrDaIns = ""
            endif
          endif
          this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Inserimento record")
          this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("%0Codice ricerca: %1",L_CODICE)
          this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("-u.mis.: %1%0",L_UNIMIS)
          this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Quantit�: %1", str(L_QTAMOV,12,7) )
          this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("-prezzo: %1%0", str(L_PREZZO,20,4) )
          this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Lotto: %1%0",L_LOTTO)
          this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Magazzino: %1%0", L_CODMAG)
          this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Ubicazione: %1%0", L_UBICAZIONE)
          this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Matricola: %1%0", L_MATRICOLA)
          this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Rif origine: %1%0", L_SERRIF)
          this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Magazzino collegato: %1%0", L_CODMA2)
          this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Ubicazione collegata: %1%0", L_UBICAZION2)
          this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Commessa: %1%0", L_COMMESSA)
          this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Attivit�: %1%0", L_ATTIVITA)
          this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Centro di costo/ricavo: %1%0", L_CENTRO)
          this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Quantit� nell'unit� di misura principale: %1%0", str(L_QTAUM1,12,7) )
          this.w_StrDaIns = this.w_StrDaIns+ah_Msgformat("Unit� logistica: %1", L_UNILOG)
          AddMsgNL("%1", this.oparentobject, this.w_StrDaIns)
          this.w_RecTot = this.w_RecTot+1
          ON ERROR w_ErroreVet=.T.
           
 INSERT INTO ( this.pCursore ) (CODICE,UNIMIS,QTAMOV,PREZZO,LOTTO,CODMAG,UBICAZIONE,MATRICOLA,SERRIF,CODMA2,UBICAZION2,COMMESSA,ATTIVITA,CENTRO,QTAUM1,UNILOG) ; 
 VALUES (L_CODICE,L_UNIMIS,L_QTAMOV,L_PREZZO,L_LOTTO,L_CODMAG,L_UBICAZIONE,L_MATRICOLA,L_SERRIF,L_CODMA2,L_UBICAZION2,L_COMMESSA,L_ATTIVITA,L_CENTRO,L_QTAUM1,L_UNILOG)
          if w_ErroreVet
            this.w_StrDaIns = ah_Msgformat("Errore inserimento cursore: %1 %2",message(), Message(1) )
            this.w_RecErrati = this.w_RecErrati+1
            FPUTS(this.w_LOGWriteHandle,REPLICATE("-",this.w_NumRip))
            FPUTS(this.w_LOGWriteHandle, ah_Msgformat("Record nr.%1", str(this.w_RecTot)) )
            FPUTS(this.w_LOGWriteHandle,ah_Msgformat("Codice ricerca: %1",L_CODICE) )
            FPUTS(this.w_LOGWriteHandle,ah_Msgformat("U.mis.: %1 quantit� esistente: %2",L_UNIMIS, str(L_QTAMOV,12,7)) )
            FPUTS(this.w_LOGWriteHandle, ah_Msgformat("Prezzo: %1",str(L_PREZZO,20,4)) )
            FPUTS(this.w_LOGWriteHandle, ah_Msgformat("Lotto: %1", L_LOTTO) )
            FPUTS(this.w_LOGWriteHandle, ah_Msgformat("Magazzino: %1", L_CODMAG) )
            FPUTS(this.w_LOGWriteHandle,ah_Msgformat("Ubicazione: %1",L_UBICAZIONE))
            FPUTS(this.w_LOGWriteHandle, ah_Msgformat("Matricola: %1",L_MATRICOLA) )
            FPUTS(this.w_LOGWriteHandle, ah_Msgformat("Rif origine: %1", L_SERRIF))
            FPUTS(this.w_LOGWriteHandle, ah_Msgformat("Magazzino collegato: %1", L_CODMA2) )
            FPUTS(this.w_LOGWriteHandle,ah_Msgformat("Ubicazione collegata: %1",L_UBICAZION2))
            FPUTS(this.w_LOGWriteHandle,ah_Msgformat("Commessa: %1",L_COMMESSA))
            FPUTS(this.w_LOGWriteHandle,ah_Msgformat("Attivit�: %1",L_ATTIVITA))
            FPUTS(this.w_LOGWriteHandle,ah_Msgformat("Centro di costo/ricavo: %1",L_CENTRO))
            FPUTS(this.w_LOGWriteHandle,ah_Msgformat("Quantit� nell'unit� di misura principale: %1", str(L_QTAUM1,12,7)) )
            FPUTS(this.w_LOGWriteHandle,ah_Msgformat("Unit� logistica: %1", L_UNILOG) )
            FPUTS(this.w_LOGWriteHandle,Message()+" <--> " +Message(1))
            FPUTS(this.w_LOGWriteHandle,REPLICATE("-",this.w_NumRip))
            AddMsgNL("%1",this.oparentobject, this.w_StrDaIns)
          endif
          on error &L_OldError 
        endif
      enddo
      FCLOSE(this.w_FileHandle) 
      FCLOSE(this.w_LOGWriteHandle)
      if this.w_RecErrati>0
        this.w_StrDaIns = ah_Msgformat("%1%0Importazione dati terminata:%0Record totali: %2%0Record inseriti: %3%0Record errati: %4%0%1%0%0Consultare il file di log (%5) contenente l'elenco degli errori", REPLICATE("*",this.w_NumRip), chr(9)+chr(9)+ALLTRIM(Str(this.w_RecTot)), chr(9)+chr(9)+ALLTRIM(str(this.w_RecTot-this.w_RecErrati)), chr(9)+chr(9)+ALLTRIM(Str(this.w_RecErrati)), this.w_ErrLOG )
        if ah_YesNo("Vuoi visualizzare il file di log degli errori?")
          RunThis='! /N3 notepad "'+this.w_ErrLOG+'"'
          &RunThis
        endif
      else
        this.w_StrDaIns = ah_Msgformat("%1%0Importazione dati terminata:%0Record totali: %2%0Record inseriti: %3%0Record errati: %4%0%1%0", REPLICATE("*",this.w_NumRip), chr(9)+chr(9)+ALLTRIM(Str(this.w_RecTot)), chr(9)+chr(9)+ALLTRIM(str(this.w_RecTot-this.w_RecErrati)), chr(9)+chr(9)+ALLTRIM(Str(this.w_RecErrati)) )
        ERASE (this.w_ErrLOG)
        if this.w_DHELIFIL="S"
          ERASE (this.w_FileOut)
        endif
      endif
      AddMsg("%1", this.oparentobject,this.w_StrDaIns)
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Pulisce la stringa alfanumerica da eventuali caratteri "proibiti" 
    *     Codice ASC minore di 33 sostituendoli con spazi
    this.w_RESULT = ""
    this.w_CHECK = ( this.w_AttStr )
    this.w_Loop = 1
    do while this.w_LOOP<= Len( this.w_Check ) 
      if asc(Substr(this.w_check, this.w_LOOP ,1))<=32
        this.w_RESULT = this.w_RESULT + " "
      else
        this.w_RESULT = this.w_RESULT + Substr(this.w_check, this.w_LOOP ,1)
      endif
      this.w_Loop = this.w_Loop + 1
    enddo
    this.w_AttStr = ALLTRIM(this.w_Result)
  endproc


  proc Init(oParentObject,pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition)
    this.pCursore=pCursore
    this.pTXTFile=pTXTFile
    this.pObjMsg=pObjMsg
    this.p_ArrayFieldName=p_ArrayFieldName
    this.p_ArrayFieldPosition=p_ArrayFieldPosition
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DIS_HARD'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCursore,pTXTFile,pObjMsg,p_ArrayFieldName,p_ArrayFieldPosition"
endproc
