* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_mtb                                                        *
*              Tabella ritenute                                                *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_79]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-15                                                      *
* Last revis.: 2011-02-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_mtb")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_mtb")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_mtb")
  return

* --- Class definition
define class tgsar_mtb as StdPCForm
  Width  = 428
  Height = 280
  Top    = 16
  Left   = 16
  cComment = "Tabella ritenute"
  cPrg = "gsar_mtb"
  HelpContextID=80312169
  add object cnt as tcgsar_mtb
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_mtb as PCContext
  w_TRTIPRIT = space(1)
  w_OBTEST = space(8)
  w_TRTIPCON = space(1)
  w_TRCODCON = space(15)
  w_TRFLINPS = space(1)
  w_TRFLIRPE = space(1)
  w_TRCODTRI = space(5)
  w_TRPERRIT = 0
  w_TRPERIMP = 0
  w_TRDATINI = space(8)
  w_TRDATFIN = space(8)
  w_DTOBSO = space(8)
  w_TRCODAZI = space(5)
  w_TRCODINP = 0
  w_TRDATULT = space(8)
  w_DESCRI = space(30)
  w_TRSEDINP = space(5)
  w_FLACON = space(1)
  w_TRFLGSTO = space(1)
  w_TRFLGST1 = space(1)
  proc Save(i_oFrom)
    this.w_TRTIPRIT = i_oFrom.w_TRTIPRIT
    this.w_OBTEST = i_oFrom.w_OBTEST
    this.w_TRTIPCON = i_oFrom.w_TRTIPCON
    this.w_TRCODCON = i_oFrom.w_TRCODCON
    this.w_TRFLINPS = i_oFrom.w_TRFLINPS
    this.w_TRFLIRPE = i_oFrom.w_TRFLIRPE
    this.w_TRCODTRI = i_oFrom.w_TRCODTRI
    this.w_TRPERRIT = i_oFrom.w_TRPERRIT
    this.w_TRPERIMP = i_oFrom.w_TRPERIMP
    this.w_TRDATINI = i_oFrom.w_TRDATINI
    this.w_TRDATFIN = i_oFrom.w_TRDATFIN
    this.w_DTOBSO = i_oFrom.w_DTOBSO
    this.w_TRCODAZI = i_oFrom.w_TRCODAZI
    this.w_TRCODINP = i_oFrom.w_TRCODINP
    this.w_TRDATULT = i_oFrom.w_TRDATULT
    this.w_DESCRI = i_oFrom.w_DESCRI
    this.w_TRSEDINP = i_oFrom.w_TRSEDINP
    this.w_FLACON = i_oFrom.w_FLACON
    this.w_TRFLGSTO = i_oFrom.w_TRFLGSTO
    this.w_TRFLGST1 = i_oFrom.w_TRFLGST1
    PCContext::Save(i_oFrom)
  proc Load(i_oTo)
    i_oTo.w_TRTIPRIT = this.w_TRTIPRIT
    i_oTo.w_OBTEST = this.w_OBTEST
    i_oTo.w_TRTIPCON = this.w_TRTIPCON
    i_oTo.w_TRCODCON = this.w_TRCODCON
    i_oTo.w_TRFLINPS = this.w_TRFLINPS
    i_oTo.w_TRFLIRPE = this.w_TRFLIRPE
    i_oTo.w_TRCODTRI = this.w_TRCODTRI
    i_oTo.w_TRPERRIT = this.w_TRPERRIT
    i_oTo.w_TRPERIMP = this.w_TRPERIMP
    i_oTo.w_TRDATINI = this.w_TRDATINI
    i_oTo.w_TRDATFIN = this.w_TRDATFIN
    i_oTo.w_DTOBSO = this.w_DTOBSO
    i_oTo.w_TRCODAZI = this.w_TRCODAZI
    i_oTo.w_TRCODINP = this.w_TRCODINP
    i_oTo.w_TRDATULT = this.w_TRDATULT
    i_oTo.w_DESCRI = this.w_DESCRI
    i_oTo.w_TRSEDINP = this.w_TRSEDINP
    i_oTo.w_FLACON = this.w_FLACON
    i_oTo.w_TRFLGSTO = this.w_TRFLGSTO
    i_oTo.w_TRFLGST1 = this.w_TRFLGST1
    i_oTo.oPgFrm.Page1.oPag.oBody.nAbsRow = 1
    PCContext::Load(i_oTo)
enddefine

define class tcgsar_mtb as StdPCTrsContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 428
  Height = 280
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2011-02-25"
  HelpContextID=80312169
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=20

  * --- Detail File Properties
  cTrsName=''

  * --- Constant Properties
  TAB_RITE_IDX = 0
  AZIENDA_IDX = 0
  CONTI_IDX = 0
  TRI_BUTI_IDX = 0
  MOD_PAGA_IDX = 0
  SED_INPS_IDX = 0
  cFile = "TAB_RITE"
  cKeySelect = "TRTIPRIT,TRCODAZI"
  cKeyWhere  = "TRTIPRIT=this.w_TRTIPRIT and TRCODAZI=this.w_TRCODAZI"
  cKeyDetail  = "TRTIPRIT=this.w_TRTIPRIT and TRCODAZI=this.w_TRCODAZI"
  cKeyWhereODBC = '"TRTIPRIT="+cp_ToStrODBC(this.w_TRTIPRIT)';
      +'+" and TRCODAZI="+cp_ToStrODBC(this.w_TRCODAZI)';

  cKeyDetailWhereODBC = '"TRTIPRIT="+cp_ToStrODBC(this.w_TRTIPRIT)';
      +'+" and TRCODAZI="+cp_ToStrODBC(this.w_TRCODAZI)';
      +'+" and CPROWNUM="+cp_ToStrODBC( i_TN.->CPROWNUM )'    
  cKeyWhereODBCqualified = '"TAB_RITE.TRTIPRIT="+cp_ToStrODBC(this.w_TRTIPRIT)';
      +'+" and TAB_RITE.TRCODAZI="+cp_ToStrODBC(this.w_TRCODAZI)';

  cOrderByDett = ''
  cOrderByDettCustom = ''
  bApplyOrderDetail=.t.
  bApplyOrderDetailCustom=.t.
  cOrderByDett = 'TAB_RITE.CPROWNUM '
  cPrg = "gsar_mtb"
  cComment = "Tabella ritenute"
  i_nRowNum = 0
  w_CPROWNUM = 0
  i_nRowPerPage = 10
  icon = "movi.ico"
  i_lastcheckrow = 0
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_TRTIPRIT = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_TRTIPCON = space(1)
  w_TRCODCON = space(15)
  w_TRFLINPS = space(1)
  w_TRFLIRPE = space(1)
  w_TRCODTRI = space(5)
  w_TRPERRIT = 0
  w_TRPERIMP = 0
  w_TRDATINI = ctod('  /  /  ')
  w_TRDATFIN = ctod('  /  /  ')
  w_DTOBSO = ctod('  /  /  ')
  w_TRCODAZI = space(5)
  w_TRCODINP = 0
  w_TRDATULT = ctod('  /  /  ')
  w_DESCRI = space(30)
  w_TRSEDINP = space(5)
  w_FLACON = space(1)
  w_TRFLGSTO = space(1)
  w_TRFLGST1 = space(1)
  w_oHeaderDetail = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_mtbPag1","gsar_mtb",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oTRDATULT_1_5
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_oHeaderDetail = this.oPgFrm.Pages(1).oPag.oHeaderDetail
    DoDefault()
    proc Destroy()
      this.w_oHeaderDetail = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='TRI_BUTI'
    this.cWorkTables[4]='MOD_PAGA'
    this.cWorkTables[5]='SED_INPS'
    this.cWorkTables[6]='TAB_RITE'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(6))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.TAB_RITE_IDX,5],7]
    this.nPostItConn=i_TableProp[this.TAB_RITE_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_mtb'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_cTF,i_nRes,i_cTable,i_nConn,i_cOrder
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_2_5_joined
    link_2_5_joined=.f.
    local link_1_9_joined
    link_1_9_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    * --- Select reading the record
    *
    * select * from TAB_RITE where TRTIPRIT=KeySet.TRTIPRIT
    *                            and TRCODAZI=KeySet.TRCODAZI
    *
      i_cOrder = ''
      if this.bApplyOrderDetail
           if this.bApplyOrderDetailCustom and not empty(this.cOrderByDettCustom)
                i_cOrder = 'order by '+this.cOrderByDettCustom
           else
                if not empty(this.cOrderByDett)
                     i_cOrder = 'order by '+this.cOrderByDett
                endif
           endif
      endif
      * --- Area Manuale = Before Load Detail
      * --- Fine Area Manuale
    If Used(this.cTrsName+'_bis')
        select (this.cTrsName+'_bis')
        zap
    endif
    i_nConn = i_TableProp[this.TAB_RITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_RITE_IDX,2],this.bLoadRecFilter,this.TAB_RITE_IDX,"gsar_mtb")
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('TAB_RITE')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "TAB_RITE.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' TAB_RITE '
      link_2_5_joined=this.AddJoinedLink_2_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_9_joined=this.AddJoinedLink_1_9(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey+" "+i_cOrder,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'TRTIPRIT',this.w_TRTIPRIT  ,'TRCODAZI',this.w_TRCODAZI  )
      select * from (i_cTable) TAB_RITE where &i_cKey &i_cOrder into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    i_cTF = this.cCursor
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_OBTEST = i_datsys
        .w_DESCRI = space(30)
        .w_TRTIPRIT = NVL(TRTIPRIT,space(1))
        .w_TRCODAZI = NVL(TRCODAZI,space(5))
        .w_TRCODINP = NVL(TRCODINP,0)
        .w_TRDATULT = NVL(cp_ToDate(TRDATULT),ctod("  /  /  "))
        .w_TRSEDINP = NVL(TRSEDINP,space(5))
          if link_1_9_joined
            this.w_TRSEDINP = NVL(PSCODICE109,NVL(this.w_TRSEDINP,space(5)))
            this.w_DESCRI = NVL(PSDESCRI109,space(30))
          else
          .link_1_9('Load')
          endif
        .w_TRFLGSTO = NVL(TRFLGSTO,space(1))
        .w_TRFLGST1 = NVL(TRFLGST1,space(1))
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        cp_LoadRecExtFlds(this,'TAB_RITE')
      endwith
      * === TEMPORARY
      select (this.cTrsName)
      zap
      select (this.cCursor)
      this.i_nRowNum = 0
      scan
        with this
          .w_DTOBSO = ctod("  /  /  ")
          .w_FLACON = space(1)
          .w_CPROWNUM = &i_cTF..CPROWNUM
          .w_TRTIPCON = NVL(TRTIPCON,space(1))
          .w_TRCODCON = NVL(TRCODCON,space(15))
          .w_TRFLINPS = NVL(TRFLINPS,space(1))
          .w_TRFLIRPE = NVL(TRFLIRPE,space(1))
          .w_TRCODTRI = NVL(TRCODTRI,space(5))
          if link_2_5_joined
            this.w_TRCODTRI = NVL(TRCODTRI205,NVL(this.w_TRCODTRI,space(5)))
            this.w_FLACON = NVL(TRFLACON205,space(1))
          else
          .link_2_5('Load')
          endif
          .w_TRPERRIT = NVL(TRPERRIT,0)
          .w_TRPERIMP = NVL(TRPERIMP,0)
          .w_TRDATINI = NVL(cp_ToDate(TRDATINI),ctod("  /  /  "))
          .w_TRDATFIN = NVL(cp_ToDate(TRDATFIN),ctod("  /  /  "))
          select (this.cTrsName)
          append blank
          replace CPCCCHK with &i_cTF..CPCCCHK
          .TrsFromWork()
          replace CPROWNUM with &i_cTF..CPROWNUM
          replace I_SRV with " "
          .nLastRow = recno()
          select (this.cCursor)
          .i_nRowNum = max(.i_nRowNum,CPROWNUM)
        endwith
      endscan
      this.nFirstrow=1
      select (this.cCursor)
      go top
      select (this.cTrsName)
      with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endwith
      go top
      with this
        .oPgFrm.Page1.oPag.oBody.Refresh()
        .WorkFromTrs()
        .mCalcRowObjs()
        .SaveDependsOn()
        .SetControlsValue()
        .mHideControls()
        .ChildrenChangeRow()
        .oPgFrm.Page1.oPag.oBody.nAbsRow=1
        .oPgFrm.Page1.oPag.oBody.nRelRow=1
        .NotifyEvent('Load')
      endwith
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blanking Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bHeaderUpdated=.f.
    this.bLoaded=.f.
      If Used(this.cTrsName+'_bis')
    select (this.cTrsName+'_bis')
    zap
      endif
    select (this.cTrsName)
    zap
    append blank
    this.nLastRow  = recno()
    this.nFirstrow = 1
    this.i_nRowNum = 1
    this.w_CPROWNUM = 1
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    with this
      .w_TRTIPRIT=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_TRTIPCON=space(1)
      .w_TRCODCON=space(15)
      .w_TRFLINPS=space(1)
      .w_TRFLIRPE=space(1)
      .w_TRCODTRI=space(5)
      .w_TRPERRIT=0
      .w_TRPERIMP=0
      .w_TRDATINI=ctod("  /  /  ")
      .w_TRDATFIN=ctod("  /  /  ")
      .w_DTOBSO=ctod("  /  /  ")
      .w_TRCODAZI=space(5)
      .w_TRCODINP=0
      .w_TRDATULT=ctod("  /  /  ")
      .w_DESCRI=space(30)
      .w_TRSEDINP=space(5)
      .w_FLACON=space(1)
      .w_TRFLGSTO=space(1)
      .w_TRFLGST1=space(1)
      if .cFunction<>"Filter"
        .w_TRTIPRIT = This.oparentobject .w_TIPRIT
        .w_OBTEST = i_datsys
        .w_TRTIPCON = 'G'
        .DoRTCalc(4,7,.f.)
        if not(empty(.w_TRCODTRI))
         .link_2_5('Full')
        endif
        .DoRTCalc(8,17,.f.)
        if not(empty(.w_TRSEDINP))
         .link_1_9('Full')
        endif
        .DoRTCalc(18,18,.f.)
        .w_TRFLGSTO = 'S'
        .w_TRFLGST1 = 'S'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
      endif
    endwith
    cp_BlankRecExtFlds(this,'TAB_RITE')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.TrsFromWork()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=0
    this.oPgFrm.Page1.oPag.oBody.nRelRow=1
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disabling List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oTRDATULT_1_5.enabled = i_bVal
      .Page1.oPag.oTRSEDINP_1_9.enabled = i_bVal
      .Page1.oPag.oTRFLGSTO_1_10.enabled = i_bVal
      .Page1.oPag.oTRFLGST1_1_11.enabled = i_bVal
      .Page1.oPag.oBody.enabled = .t.
      .Page1.oPag.oBody.oBodyCol.enabled = i_bVal
    endwith
    cp_SetEnabledExtFlds(this,'TAB_RITE',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate  filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.TAB_RITE_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRTIPRIT,"TRTIPRIT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODAZI,"TRCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRCODINP,"TRCODINP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRDATULT,"TRDATULT",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRSEDINP,"TRSEDINP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRFLGSTO,"TRFLGSTO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_TRFLGST1,"TRFLGST1",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  *
  *  --- Transaction procedures
  *
  proc CreateTrs
    this.cTrsName=sys(2015)
    create cursor (this.cTrsName) (I_SRV C(1),I_READ L(1),CPCCCHK C(10),I_TRSDEL C(1),I_RECNO N(6,0) ;
      ,t_TRCODTRI C(5);
      ,t_TRPERRIT N(6,2);
      ,t_TRPERIMP N(6,2);
      ,t_TRDATINI D(8);
      ,t_TRDATFIN D(8);
      ,CPROWNUM N(10);
      ,t_TRTIPCON C(1);
      ,t_TRCODCON C(15);
      ,t_TRFLINPS C(1);
      ,t_TRFLIRPE C(1);
      ,t_DTOBSO D(8);
      ,t_FLACON C(1);
      )
    this.oPgFrm.Page1.oPag.oBody.ColumnCount=0
    this.oPgFrm.Page1.oPag.oBody.RecordSource=this.cTrsName
    this.oPgFrm.Page1.oPag.oBody.AddObject('oBodyCol','CPcolumn')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.RemoveObject('Text1')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.AddObject('oRow','tgsar_mtbbodyrow')
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.width=this.oPgFrm.Page1.oPag.oBody.width
    this.oPgFrm.Page1.oPag.oBody.rowheight=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.height
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.Sparse=.f.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.visible=.t.
    this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oContained=this
    * --- Row charateristics
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODTRI_2_5.controlsource=this.cTrsName+'.t_TRCODTRI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRPERRIT_2_6.controlsource=this.cTrsName+'.t_TRPERRIT'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRPERIMP_2_7.controlsource=this.cTrsName+'.t_TRPERIMP'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRDATINI_2_8.controlsource=this.cTrsName+'.t_TRDATINI'
    this.oPgFRm.Page1.oPag.oBody.oBodyCol.oRow.oTRDATFIN_2_9.controlsource=this.cTrsName+'.t_TRDATFIN'
    this.oPgFrm.Page1.oPag.oBody.Zorder(1)
    this.oPgFrm.Page1.oPag.oBody3D.Zorder(1)
    this.oPgFrm.Page1.oPag.pagebmp.Zorder(1)
    this.AddVLine(73)
    this.AddVLine(136)
    this.AddVLine(201)
    this.AddVLine(285)
    this.oPgFrm.Page1.oPag.oBody.oFirstControl=this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODTRI_2_5
    * --- New table already open in exclusive mode
    * --- Area Manuale = Create Trs
    * --- Fine Area Manuale

  function mInsert()
    local i_nConn,i_cTable,i_extfld,i_extval
    * --- Area Manuale = Insert Master Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.TAB_RITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_RITE_IDX,2])
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
    * --- Area Manuale = Insert Master End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Insert new Record in Detail table
  function mInsertDetail(i_nCntLine)
    local i_cKey,i_nConn,i_cTable,i_TN
    * --- Area Manuale = Insert Detail Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.TAB_RITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_RITE_IDX,2])
      *
      * insert into TAB_RITE
      *
      i_TN = this.cTrsName
      this.NotifyEvent('Insert row start')
      local i_cFldBody,i_cFldValBody
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'TAB_RITE')
        i_extval=cp_InsertValODBCExtFlds(this,'TAB_RITE')
        i_cFldBody=" "+;
                  "(TRTIPRIT,TRTIPCON,TRCODCON,TRFLINPS,TRFLIRPE"+;
                  ",TRCODTRI,TRPERRIT,TRPERIMP,TRDATINI,TRDATFIN"+;
                  ",TRCODAZI,TRCODINP,TRDATULT,TRSEDINP,TRFLGSTO"+;
                  ",TRFLGST1,CPROWNUM,CPCCCHK"+i_extfld+")"
       i_cFldValBody=" "+;
             "("+cp_ToStrODBC(this.w_TRTIPRIT)+","+cp_ToStrODBC(this.w_TRTIPCON)+","+cp_ToStrODBC(this.w_TRCODCON)+","+cp_ToStrODBC(this.w_TRFLINPS)+","+cp_ToStrODBC(this.w_TRFLIRPE)+;
             ","+cp_ToStrODBCNull(this.w_TRCODTRI)+","+cp_ToStrODBC(this.w_TRPERRIT)+","+cp_ToStrODBC(this.w_TRPERIMP)+","+cp_ToStrODBC(this.w_TRDATINI)+","+cp_ToStrODBC(this.w_TRDATFIN)+;
             ","+cp_ToStrODBC(this.w_TRCODAZI)+","+cp_ToStrODBC(this.w_TRCODINP)+","+cp_ToStrODBC(this.w_TRDATULT)+","+cp_ToStrODBCNull(this.w_TRSEDINP)+","+cp_ToStrODBC(this.w_TRFLGSTO)+;
             ","+cp_ToStrODBC(this.w_TRFLGST1)+","+cp_ToStrODBC(i_nCntLine)+","+cp_ToStrODBC(cp_NewCCChk())+i_extval+")"
        =cp_TrsSQL(i_nConn,"INSERT INTO "+i_cTable+" "+ i_cFldBody+" Values "+i_cFldValBody)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'TAB_RITE')
        i_extval=cp_InsertValVFPExtFlds(this,'TAB_RITE')
        cp_CheckDeletedKey(i_cTable,i_nCntLine,'TRTIPRIT',this.w_TRTIPRIT,'TRCODAZI',this.w_TRCODAZI)
        INSERT INTO (i_cTable) (;
                   TRTIPRIT;
                  ,TRTIPCON;
                  ,TRCODCON;
                  ,TRFLINPS;
                  ,TRFLIRPE;
                  ,TRCODTRI;
                  ,TRPERRIT;
                  ,TRPERIMP;
                  ,TRDATINI;
                  ,TRDATFIN;
                  ,TRCODAZI;
                  ,TRCODINP;
                  ,TRDATULT;
                  ,TRSEDINP;
                  ,TRFLGSTO;
                  ,TRFLGST1;
                  ,CPROWNUM,CPCCCHK &i_extfld.) VALUES (;
                  this.w_TRTIPRIT;
                  ,this.w_TRTIPCON;
                  ,this.w_TRCODCON;
                  ,this.w_TRFLINPS;
                  ,this.w_TRFLIRPE;
                  ,this.w_TRCODTRI;
                  ,this.w_TRPERRIT;
                  ,this.w_TRPERIMP;
                  ,this.w_TRDATINI;
                  ,this.w_TRDATFIN;
                  ,this.w_TRCODAZI;
                  ,this.w_TRCODINP;
                  ,this.w_TRDATULT;
                  ,this.w_TRSEDINP;
                  ,this.w_TRFLGSTO;
                  ,this.w_TRFLGST1;
                  ,i_nCntLine,cp_NewCCChk() &i_extval. )
      endif
      this.NotifyEvent('Insert row end')
    endif
    * --- Area Manuale = Insert Detail End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_TN,i_NR,i_NF,i_OldCCCHK,i_extfld
    local i_nModRow,i_nConn,i_cTable,i_bUpdAll,i_nRec,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated
      i_nConn = i_TableProp[this.TAB_RITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_RITE_IDX,2])
      i_nModRow = 1
      if i_bEditing .and. this.bHeaderUpdated
        this.mRestoreTrs()
      endif
      if this.bHeaderUpdated
        this.mUpdateTrs()
      endif
      if this.bHeaderUpdated and i_bEditing
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        go top
        if i_bEditing and I_SRV<>'A'
        endif
        if not(i_bUpdAll)
          scan for (NOT EMPTY(t_TRCODTRI)) and (I_SRV<>"U" and I_SRV<>"A")
            i_OldCCCHK=iif(i_bEditing,&i_TN..CPCCCHK,'')
            * --- Updating Master table
            this.NotifyEvent('Update start')
            if i_nConn<>0
              i_extfld=cp_ReplaceODBCExtFlds(this,'TAB_RITE')
              i_cWhere = this.cKeyWhereODBC
              i_nnn="UPDATE "+i_cTable+" SET "+;
                 " TRCODINP="+cp_ToStrODBC(this.w_TRCODINP)+;
                 ",TRDATULT="+cp_ToStrODBC(this.w_TRDATULT)+;
                 ",TRSEDINP="+cp_ToStrODBCNull(this.w_TRSEDINP)+;
                 ",TRFLGSTO="+cp_ToStrODBC(this.w_TRFLGSTO)+;
                 ",TRFLGST1="+cp_ToStrODBC(this.w_TRFLGST1)+;
                 ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+" WHERE "+&i_cWhere+;
                 " and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                 " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
              i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
            else
              i_extfld=cp_ReplaceVFPExtFlds(this,'TAB_RITE')
              i_cWhere = this.cKeyWhere
              UPDATE (i_cTable) SET ;
                  TRCODINP=this.w_TRCODINP;
                 ,TRDATULT=this.w_TRDATULT;
                 ,TRSEDINP=this.w_TRSEDINP;
                 ,TRFLGSTO=this.w_TRFLGSTO;
                 ,TRFLGST1=this.w_TRFLGST1;
                 ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere;
                      and CPROWNUM=&i_TN.->CPROWNUM;
                      and CPCCCHK==i_OldCCCHK
              i_nModRow=_tally
            endif
            this.NotifyEvent('Update end')
            if i_nModRow<1
              exit
            endif
          endscan
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
        endif
      endif

      * --- Update Detail table
      if i_nModRow>0   && Master table updated with success
        set delete off
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_bUpdAll = .f.
        scan for (NOT EMPTY(t_TRCODTRI)) and ((I_SRV="U" or i_bUpdAll) or I_SRV="A" or deleted())
          this.WorkFromTrs()
          this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
          i_OldCCCHK=iif(i_bEditing.or.I_SRV<>"A",&i_TN..CPCCCHK,'')
          i_nRec = recno()
          set delete on
          if deleted()
            if I_SRV<>"A"
              * --- Delete from database an erased row
              this.NotifyEvent('Delete row start')
              this.mRestoreTrsDetail()
              if i_nConn<>0
                i_cWhere = this.cKeyWhereODBC
                i_nModRow=cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
              else
                i_cWhere = this.cKeyWhere
                DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                            and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Delete row end')
            endif
          else
            if I_SRV="A"
              * --- Insert new row in database
              this.mUpdateTrsDetail()
              i_NR = CPROWNUM
              =this.mInsertDetail(i_NR)
            else
            * --- Area Manuale = Replace Loop
            * --- Fine Area Manuale
              *
              * update TAB_RITE
              *
              this.NotifyEvent('Update row start')
              this.mRestoreTrsDetail()
              this.mUpdateTrsDetail()
              if i_nConn<>0
                i_extfld=cp_ReplaceODBCExtFlds(this,'TAB_RITE')
                i_cWhere = this.cKeyWhereODBC
                i_nnn="UPDATE "+i_cTable+" SET "+;
                     " TRTIPCON="+cp_ToStrODBC(this.w_TRTIPCON)+;
                     ",TRCODCON="+cp_ToStrODBC(this.w_TRCODCON)+;
                     ",TRFLINPS="+cp_ToStrODBC(this.w_TRFLINPS)+;
                     ",TRFLIRPE="+cp_ToStrODBC(this.w_TRFLIRPE)+;
                     ",TRCODTRI="+cp_ToStrODBCNull(this.w_TRCODTRI)+;
                     ",TRPERRIT="+cp_ToStrODBC(this.w_TRPERRIT)+;
                     ",TRPERIMP="+cp_ToStrODBC(this.w_TRPERIMP)+;
                     ",TRDATINI="+cp_ToStrODBC(this.w_TRDATINI)+;
                     ",TRDATFIN="+cp_ToStrODBC(this.w_TRDATFIN)+;
                     ",TRCODINP="+cp_ToStrODBC(this.w_TRCODINP)+;
                     ",TRDATULT="+cp_ToStrODBC(this.w_TRDATULT)+;
                     ",TRSEDINP="+cp_ToStrODBCNull(this.w_TRSEDINP)+;
                     ",TRFLGSTO="+cp_ToStrODBC(this.w_TRFLGSTO)+;
                     ",TRFLGST1="+cp_ToStrODBC(this.w_TRFLGST1)+;
                     ",CPCCCHK="+cp_ToStrODBC(cp_NewCCChk())+i_extfld+;
                     " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                             " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK)
                i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
              else
                i_extfld=cp_ReplaceVFPExtFlds(this,'TAB_RITE')
                i_cWhere = this.cKeyWhere
                UPDATE (i_cTable) SET ;
                      TRTIPCON=this.w_TRTIPCON;
                     ,TRCODCON=this.w_TRCODCON;
                     ,TRFLINPS=this.w_TRFLINPS;
                     ,TRFLIRPE=this.w_TRFLIRPE;
                     ,TRCODTRI=this.w_TRCODTRI;
                     ,TRPERRIT=this.w_TRPERRIT;
                     ,TRPERIMP=this.w_TRPERIMP;
                     ,TRDATINI=this.w_TRDATINI;
                     ,TRDATFIN=this.w_TRDATFIN;
                     ,TRCODINP=this.w_TRCODINP;
                     ,TRDATULT=this.w_TRDATULT;
                     ,TRSEDINP=this.w_TRSEDINP;
                     ,TRFLGSTO=this.w_TRFLGSTO;
                     ,TRFLGST1=this.w_TRFLGST1;
                     ,CPCCCHK=cp_NewCCChk() &i_extfld. WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                                      and CPCCCHK==i_OldCCCHK
                i_nModRow=_tally
              endif
              this.NotifyEvent('Update row end')
            endif
          endif
          if i_nModRow<1
            exit
          endif
          set delete off
        endscan
        set delete on
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      =cp_CheckMultiuser(i_nModRow)
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_NF,i_TN,i_OldCCCHK,i_nModRow,i_nConn,i_cTable
    local i_cDel,i_nRec
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && Record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && Force loadrec
      this.LoadRec()             && Record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.TAB_RITE_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.TAB_RITE_IDX,2])
      this.NotifyEvent("Delete Start")
      select (this.cTrsName)
      i_TN = this.cTrsName
      i_nModRow = 1
      i_cDel = set('DELETED')
      set delete off
      scan for (NOT EMPTY(t_TRCODTRI)) and I_SRV<>'A'
        this.WorkFromTrs()
        i_OldCCCHK=&i_TN..CPCCCHK
        *
        * delete TAB_RITE
        *
        this.NotifyEvent('Delete row start')
        if i_nConn<>0
          i_cWhere = this.cKeyWhereODBC
          i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                  " WHERE "+&i_cWhere+" and CPROWNUM="+cp_ToStrODBC(&i_TN.->CPROWNUM)+;
                            " and CPCCCHK="+cp_ToStrODBC(i_OldCCCHK))
        else
          i_cWhere = this.cKeyWhere
          DELETE FROM (i_cTable) WHERE &i_cWhere and CPROWNUM=&i_TN.->CPROWNUM;
                              and CPCCCHK==i_OldCCCHK
          i_nModRow=_tally
        endif
        this.NotifyEvent('Delete row end')
        if i_nModRow<1
          exit
        endif
      endscan
      set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
        select (this.cTrsName)
        i_TN = this.cTrsName
        i_cDel = set('DELETED')
        set delete off
        scan for (NOT EMPTY(t_TRCODTRI)) and I_SRV<>'A'
          i_OldCCCHK=&i_TN..CPCCCHK
          this.mRestoreTrsDetail()
        endscan
        set delete &i_cDel
      * --- Make the last record the actual position (mcalc problem with double transitory)
      this.SetRow(RecCount(this.ctrsname), .f.)
      this.SetControlsValue()
      endif
      this.NotifyEvent("Delete End")
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.TAB_RITE_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.TAB_RITE_IDX,2])
    if i_bUpd
      with this
          .w_TRTIPRIT = This.oparentobject .w_TIPRIT
        .DoRTCalc(2,2,.t.)
          .w_TRTIPCON = 'G'
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(4,20,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
      replace t_TRTIPCON with this.w_TRTIPCON
      replace t_TRCODCON with this.w_TRCODCON
      replace t_TRFLINPS with this.w_TRFLINPS
      replace t_TRFLIRPE with this.w_TRFLIRPE
      replace t_DTOBSO with this.w_DTOBSO
      replace t_FLACON with this.w_FLACON
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oHeaderDetail.Calculate()
    endwith
  return

  proc mCalcRowObjs()
    with this
    endwith
  return

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    this.mHideControls()
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  * --- Enable controls under condition for Grid and for FixedPos fields
  procedure mEnableControlsFixed()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf	
    this.mHideControls()
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_7.visible=!this.oPgFrm.Page1.oPag.oStr_1_7.mHide()
    this.oPgFrm.Page1.oPag.oDESCRI_1_8.visible=!this.oPgFrm.Page1.oPag.oDESCRI_1_8.mHide()
    this.oPgFrm.Page1.oPag.oTRSEDINP_1_9.visible=!this.oPgFrm.Page1.oPag.oTRSEDINP_1_9.mHide()
    this.oPgFrm.Page1.oPag.oTRFLGSTO_1_10.visible=!this.oPgFrm.Page1.oPag.oTRFLGSTO_1_10.mHide()
    this.oPgFrm.Page1.oPag.oTRFLGST1_1_11.visible=!this.oPgFrm.Page1.oPag.oTRFLGST1_1_11.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_12.visible=!this.oPgFrm.Page1.oPag.oStr_1_12.mHide()
    this.mHideRowControls()
    DoDefault()
  return

  procedure mHideRowControls()
    DoDefault()
  return


  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oHeaderDetail.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=TRCODTRI
  func Link_2_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.TRI_BUTI_IDX,3]
    i_lTable = "TRI_BUTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2], .t., this.TRI_BUTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRCODTRI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ATB',True,'TRI_BUTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" TRCODTRI like "+cp_ToStrODBC(trim(this.w_TRCODTRI)+"%");

          i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRFLACON";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by TRCODTRI","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'TRCODTRI',trim(this.w_TRCODTRI))
          select TRCODTRI,TRFLACON;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by TRCODTRI into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRCODTRI)==trim(_Link_.TRCODTRI) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRCODTRI) and !this.bDontReportError
            deferred_cp_zoom('TRI_BUTI','*','TRCODTRI',cp_AbsName(oSource.parent,'oTRCODTRI_2_5'),i_cWhere,'GSAR_ATB',"Codici tributi",'GSRI1AMR.TRI_BUTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRFLACON";
                     +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',oSource.xKey(1))
            select TRCODTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRCODTRI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select TRCODTRI,TRFLACON";
                   +" from "+i_cTable+" "+i_lTable+" where TRCODTRI="+cp_ToStrODBC(this.w_TRCODTRI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'TRCODTRI',this.w_TRCODTRI)
            select TRCODTRI,TRFLACON;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRCODTRI = NVL(_Link_.TRCODTRI,space(5))
      this.w_FLACON = NVL(_Link_.TRFLACON,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_TRCODTRI = space(5)
      endif
      this.w_FLACON = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_FLACON='R'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Inserire solo tributi di tipo IRPEF")
        endif
        this.w_TRCODTRI = space(5)
        this.w_FLACON = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])+'\'+cp_ToStr(_Link_.TRCODTRI,1)
      cp_ShowWarn(i_cKey,this.TRI_BUTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRCODTRI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_2_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.TRI_BUTI_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.TRI_BUTI_IDX,2])
      i_cNewSel = i_cSel+ ",link_2_5.TRCODTRI as TRCODTRI205"+ ",link_2_5.TRFLACON as TRFLACON205"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_2_5 on TAB_RITE.TRCODTRI=link_2_5.TRCODTRI"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_2_5"
          i_cKey=i_cKey+'+" and TAB_RITE.TRCODTRI=link_2_5.TRCODTRI(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=TRSEDINP
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.SED_INPS_IDX,3]
    i_lTable = "SED_INPS"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2], .t., this.SED_INPS_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_TRSEDINP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSCG_APS',True,'SED_INPS')
        if i_nConn<>0
          i_cWhere = i_cFlt+" PSCODICE like "+cp_ToStrODBC(trim(this.w_TRSEDINP)+"%");

          i_ret=cp_SQL(i_nConn,"select PSCODICE,PSDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by PSCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'PSCODICE',trim(this.w_TRSEDINP))
          select PSCODICE,PSDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by PSCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_TRSEDINP)==trim(_Link_.PSCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_TRSEDINP) and !this.bDontReportError
            deferred_cp_zoom('SED_INPS','*','PSCODICE',cp_AbsName(oSource.parent,'oTRSEDINP_1_9'),i_cWhere,'GSCG_APS',"Sedi INPS",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE,PSDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',oSource.xKey(1))
            select PSCODICE,PSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_TRSEDINP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select PSCODICE,PSDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where PSCODICE="+cp_ToStrODBC(this.w_TRSEDINP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'PSCODICE',this.w_TRSEDINP)
            select PSCODICE,PSDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_TRSEDINP = NVL(_Link_.PSCODICE,space(5))
      this.w_DESCRI = NVL(_Link_.PSDESCRI,space(30))
    else
      if i_cCtrl<>'Load'
        this.w_TRSEDINP = space(5)
      endif
      this.w_DESCRI = space(30)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])+'\'+cp_ToStr(_Link_.PSCODICE,1)
      cp_ShowWarn(i_cKey,this.SED_INPS_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_TRSEDINP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_9(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 2 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.SED_INPS_IDX,3] and i_nFlds+2<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.SED_INPS_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_9.PSCODICE as PSCODICE109"+ ",link_1_9.PSDESCRI as PSDESCRI109"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_9 on TAB_RITE.TRSEDINP=link_1_9.PSCODICE"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_9"
          i_cKey=i_cKey+'+" and TAB_RITE.TRSEDINP=link_1_9.PSCODICE(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+2
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    select (this.cTrsName)
    if not(this.oPgFrm.Page1.oPag.oTRDATULT_1_5.value==this.w_TRDATULT)
      this.oPgFrm.Page1.oPag.oTRDATULT_1_5.value=this.w_TRDATULT
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI_1_8.value==this.w_DESCRI)
      this.oPgFrm.Page1.oPag.oDESCRI_1_8.value=this.w_DESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oTRSEDINP_1_9.value==this.w_TRSEDINP)
      this.oPgFrm.Page1.oPag.oTRSEDINP_1_9.value=this.w_TRSEDINP
    endif
    if not(this.oPgFrm.Page1.oPag.oTRFLGSTO_1_10.RadioValue()==this.w_TRFLGSTO)
      this.oPgFrm.Page1.oPag.oTRFLGSTO_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTRFLGST1_1_11.RadioValue()==this.w_TRFLGST1)
      this.oPgFrm.Page1.oPag.oTRFLGST1_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODTRI_2_5.value==this.w_TRCODTRI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODTRI_2_5.value=this.w_TRCODTRI
      replace t_TRCODTRI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODTRI_2_5.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRPERRIT_2_6.value==this.w_TRPERRIT)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRPERRIT_2_6.value=this.w_TRPERRIT
      replace t_TRPERRIT with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRPERRIT_2_6.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRPERIMP_2_7.value==this.w_TRPERIMP)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRPERIMP_2_7.value=this.w_TRPERIMP
      replace t_TRPERIMP with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRPERIMP_2_7.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRDATINI_2_8.value==this.w_TRDATINI)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRDATINI_2_8.value=this.w_TRDATINI
      replace t_TRDATINI with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRDATINI_2_8.value
    endif
    if not(this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRDATFIN_2_9.value==this.w_TRDATFIN)
      this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRDATFIN_2_9.value=this.w_TRDATFIN
      replace t_TRDATFIN with this.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRDATFIN_2_9.value
    endif
    cp_SetControlsValueExtFlds(this,'TAB_RITE')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          otherwise
            i_bRes = .CheckRow()
            if .not. i_bRes
              if !isnull(.oNewFocus)
                if .oPgFrm.ActivePage>1
                  .oPgFrm.ActivePage=1
                endif
                .oNewFocus.SetFocus()
                .oNewFocus=.NULL.
              endif
            endif
            * return(i_bRes)
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
        return(i_bRes)
      endif
      if not(i_bnoChk)
        cp_ErrorMsg(i_cErrorMsg)
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- CheckRow
  func CheckRow()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      do case
        case not(Not ((.w_TRFLIRPE='S' or .w_TRFLINPS='S') AND EMPTY(.w_TRCODTRI)))
          i_bnoChk = .f.
          i_bRes = .f.
          i_cErrorMsg = cp_Translate("Il codice tributo � obbligatorio se il Conto Contabile associato � soggetto a Ritenuta di tipo IRPEF o a Contributi Prevvidenziali!")
        case   not(.w_FLACON='R') and not(empty(.w_TRCODTRI)) and (NOT EMPTY(.w_TRCODTRI))
          .oNewFocus=.oPgFrm.Page1.oPag.oBody.oBodyCol.oRow.oTRCODTRI_2_5
          i_bRes = .f.
          i_bnoChk = .f.
          i_cErrorMsg = thisform.msgFmt("Inserire solo tributi di tipo IRPEF")
      endcase
      if NOT EMPTY(.w_TRCODTRI)
        * --- Area Manuale = Check Row
        * --- Fine Area Manuale
      else
        if this.oPgFrm.Page1.oPag.oBody.nAbsRow<>0 and this.oPgFrm.Page1.oPag.oBody.nAbsRow<>this.nLastRow and (type('i_bCheckEmptyRows')='U' or i_bCheckEmptyRows)
          i_cErrorMsg=MSG_EMPTY_TRANSITORY_ROW
          i_bres=.f.
          i_bnoChk=.f.
        endif
      endif
      if not(i_bnoChk)
        do cp_ErrorMsg with i_cErrorMsg
        return(i_bRes)
      endif
      if not(i_bnoObbl)
        do cp_ErrorMsg with MSG_FIELD_CANNOT_BE_NULL_QM
        return(i_bRes)
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc


  * --- FullRow
  Func FullRow()
    local i_bRes,i_nArea
    i_nArea=select()
    select (this.cTrsName)
    i_bRes=(NOT EMPTY(t_TRCODTRI))
    select(i_nArea)
    return(i_bRes)
  Endfunc

  * --- InitRow
  Proc InitRow()
  this.NotifyEvent("Before Init Row")
    select (this.cTrsName)
    append blank
    this.nLastRow = recno()
    this.i_nRowNum = this.i_nRowNum+1
    this.w_CPROWNUM = this.i_nRowNum
    replace CPROWNUM with this.i_nRowNum
    replace I_SRV    with "A"
    replace I_RECNO  with recno()
    with this
      .w_TRTIPCON=space(1)
      .w_TRCODCON=space(15)
      .w_TRFLINPS=space(1)
      .w_TRFLIRPE=space(1)
      .w_TRCODTRI=space(5)
      .w_TRPERRIT=0
      .w_TRPERIMP=0
      .w_TRDATINI=ctod("  /  /  ")
      .w_TRDATFIN=ctod("  /  /  ")
      .w_DTOBSO=ctod("  /  /  ")
      .w_FLACON=space(1)
      .DoRTCalc(1,2,.f.)
        .w_TRTIPCON = 'G'
      .DoRTCalc(4,7,.f.)
      if not(empty(.w_TRCODTRI))
        .link_2_5('Full')
      endif
    endwith
    this.DoRTCalc(8,20,.f.)
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
    this.TrsFromWork()
    this.ChildrenChangeRow()
    this.NotifyEvent("Init Row")
  endproc

  * --- WorkFromTrs
  Proc WorkFromTrs()
    select (this.cTrsName)
    this.w_TRTIPCON = t_TRTIPCON
    this.w_TRCODCON = t_TRCODCON
    this.w_TRFLINPS = t_TRFLINPS
    this.w_TRFLIRPE = t_TRFLIRPE
    this.w_TRCODTRI = t_TRCODTRI
    this.w_TRPERRIT = t_TRPERRIT
    this.w_TRPERIMP = t_TRPERIMP
    this.w_TRDATINI = t_TRDATINI
    this.w_TRDATFIN = t_TRDATFIN
    this.w_DTOBSO = t_DTOBSO
    this.w_FLACON = t_FLACON
    this.w_CPROWNUM = CPROWNUM
    this.oPgFrm.Page1.oPag.oBody.nAbsRow=recno()
  EndProc

  * --- TrsFromWork
  Proc TrsFromWork()
    select (this.cTrsName)
    replace I_RECNO with recno()
    replace t_TRTIPCON with this.w_TRTIPCON
    replace t_TRCODCON with this.w_TRCODCON
    replace t_TRFLINPS with this.w_TRFLINPS
    replace t_TRFLIRPE with this.w_TRFLIRPE
    replace t_TRCODTRI with this.w_TRCODTRI
    replace t_TRPERRIT with this.w_TRPERRIT
    replace t_TRPERIMP with this.w_TRPERIMP
    replace t_TRDATINI with this.w_TRDATINI
    replace t_TRDATFIN with this.w_TRDATFIN
    replace t_DTOBSO with this.w_DTOBSO
    replace t_FLACON with this.w_FLACON
  EndProc

  * --- SubtractTotals
  Proc SubtractTotals()
  EndProc
enddefine

* --- Define pages as container
define class tgsar_mtbPag1 as StdContainer
  Width  = 424
  height = 280
  stdWidth  = 424
  stdheight = 280
  resizeXpos=400
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oTRDATULT_1_5 as StdField with uid="HPCBUIZIMF",rtseq=15,rtrep=.f.,;
    cFormVar = "w_TRDATULT", cQueryName = "TRDATULT",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di calcolo",;
    HelpContextID = 172220278,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=296, Top=33

  add object oDESCRI_1_8 as StdField with uid="EEYXVLBQAD",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESCRI", cQueryName = "DESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .f.,;
    HelpContextID = 107019722,;
   bGlobalFont=.t.,;
    Height=21, Width=232, Left=188, Top=6, InputMask=replicate('X',30)

  func oDESCRI_1_8.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPRIT='V')
    endwith
    endif
  endfunc

  add object oTRSEDINP_1_9 as StdField with uid="ZLXWHCSBLC",rtseq=17,rtrep=.f.,;
    cFormVar = "w_TRSEDINP", cQueryName = "TRSEDINP",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice sede INPS F24",;
    HelpContextID = 121565050,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=127, Top=6, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="SED_INPS", cZoomOnZoom="GSCG_APS", oKey_1_1="PSCODICE", oKey_1_2="this.w_TRSEDINP"

  func oTRSEDINP_1_9.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPRIT='V')
    endwith
    endif
  endfunc

  func oTRSEDINP_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRSEDINP_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.parent.oContained.bHeaderUpdated=.t.
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oTRSEDINP_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'SED_INPS','*','PSCODICE',cp_AbsName(this.parent,'oTRSEDINP_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSCG_APS',"Sedi INPS",'',this.parent.oContained
  endproc
  proc oTRSEDINP_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSCG_APS()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_PSCODICE=this.parent.oContained.w_TRSEDINP
    i_obj.ecpSave()
  endproc

  add object oTRFLGSTO_1_10 as StdCheck with uid="YYMNAWXSPG",rtseq=19,rtrep=.f.,left=21, top=31, caption="Storno immediato ritenute",;
    ToolTipText = "Se attivo storna immediatamente l'importo delle ritenute dal debito verso percipiente",;
    HelpContextID = 49758341,;
    cFormVar="w_TRFLGSTO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTRFLGSTO_1_10.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TRFLGSTO,&i_cF..t_TRFLGSTO),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oTRFLGSTO_1_10.GetRadio()
    this.Parent.oContained.w_TRFLGSTO = this.RadioValue()
    return .t.
  endfunc

  func oTRFLGSTO_1_10.ToRadio()
    this.Parent.oContained.w_TRFLGSTO=trim(this.Parent.oContained.w_TRFLGSTO)
    return(;
      iif(this.Parent.oContained.w_TRFLGSTO=='S',1,;
      0))
  endfunc

  func oTRFLGSTO_1_10.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTRFLGSTO_1_10.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPRIT='V')
    endwith
    endif
  endfunc

  add object oTRFLGST1_1_11 as StdCheck with uid="ZAUYBRBWTP",rtseq=20,rtrep=.f.,left=21, top=31, caption="Storno immediato ritenute",;
    ToolTipText = "se attivo storna immediatamente l'importo delle ritenute dal credito verso cliente",;
    HelpContextID = 49758311,;
    cFormVar="w_TRFLGST1", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oTRFLGST1_1_11.RadioValue(i_bTrs,i_bOld)
    local xVal,i_cF
    i_cF=this.Parent.oContained.cTrsName
    xVal=iif(i_bTrs,iif(i_bOld,&i_cF..TRFLGST1,&i_cF..t_TRFLGST1),this.value)
    return(iif(xVal =1,'S',;
    'N'))
  endfunc
  func oTRFLGST1_1_11.GetRadio()
    this.Parent.oContained.w_TRFLGST1 = this.RadioValue()
    return .t.
  endfunc

  func oTRFLGST1_1_11.ToRadio()
    this.Parent.oContained.w_TRFLGST1=trim(this.Parent.oContained.w_TRFLGST1)
    return(;
      iif(this.Parent.oContained.w_TRFLGST1=='S',1,;
      0))
  endfunc

  func oTRFLGST1_1_11.SetRadio()
    this.value=this.ToRadio()
  endfunc

  func oTRFLGST1_1_11.mHide()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TRTIPRIT='A')
    endwith
    endif
  endfunc


  add object oHeaderDetail as cp_DetailHeader with uid="XXPOLFABXX",left=20, top=62, width=364,height=19,;
    caption='oHeaderDetail',;
   bGlobalFont=.t.,;
    nNumberColumn=5,Field1="TRCODTRI",Label1="Tributo",Field2="TRPERRIT",Label2="% Riten.",Field3="TRPERIMP",Label3="% Impon.",Field4="TRDATINI",Label4="Data inizio",Field5="TRDATFIN",Label5="Data fine",;
    cEvent = "Init",;
    nPag=1;
    , HelpContextID = 235778682

  add object oStr_1_6 as StdString with uid="FZZOVTFOEE",Visible=.t., Left=197, Top=34,;
    Alignment=1, Width=97, Height=15,;
    Caption="Data ult. calcolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="NDRUUPSAFU",Visible=.t., Left=8, Top=7,;
    Alignment=1, Width=117, Height=15,;
    Caption="Cod.sede INPS F24:"  ;
  , bGlobalFont=.t.

  func oStr_1_7.mHide()
    with this.Parent.oContained
      return (.w_TRTIPRIT='V')
    endwith
  endfunc

  add object oStr_1_12 as StdString with uid="FTQFPYIBRR",Visible=.t., Left=12, Top=291,;
    Alignment=0, Width=319, Height=18,;
    Caption="Attenzione esistono 2 flag storno immediato sovrapposti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_12.mHide()
    with this.Parent.oContained
      return (1=1)
    endwith
  endfunc

  add object oEndHeader as BodyKeyMover with nDirection=1
  *
  * --- BODY transaction
  *
  add object oBody3D as shape with left=10,top=81,;
    width=360+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-2,specialeffect=0

  add object oBody as StdBody noinit with ;
    left=11,top=82,width=359+Sysmetric(5),height=int(fontmetric(1,"Arial",9,"")*10*1.3000000000000000)-4,columncount=0,GridLines=1,;
    HeaderHeight=0,DeleteMark=.f.,scrollbars=2,enabled=.f.,;
    cLinkFile='TRI_BUTI|'

  Proc oBody.SetCurrentRow()
    thisform.lockscreen=.t.
    select (this.parent.oContained.cTrsName)
    if recno()<>this.nAbsRow
      if this.nAbsRow<>0 and inlist(this.parent.oContained.cFunction,'Edit','Load')
        if this.nAbsRow<>this.parent.oContained.i_lastcheckrow and !this.parent.oContained.CheckRow()
          this.parent.oContained.i_lastcheckrow=0
          this.Parent.oContained.__dummy__.enabled=.t.
          this.Parent.oContained.__dummy__.SetFocus()
          select (this.parent.oContained.cTrsName)
          go (this.nAbsRow)
          this.SetFullFocus()
          if !isnull(this.parent.oContained.oNewFocus)
            this.parent.oContained.oNewFocus.SetFocus()
            this.parent.oContained.oNewFocus=.NULL.
          endif
          this.Parent.oContained.__dummy__.enabled=.f.
          thisform.lockscreen=.f.
          return
        endif
      endif
      this.nAbsRow=recno()
      this.Parent.oContained.WorkFromTrs()
      this.Parent.oContained.mEnableControlsFixed()
      this.Parent.oContained.mCalcRowObjs()
      * --- Area Manuale = Set Current Row
      * --- Fine Area Manuale
    else
      this.Parent.oContained.mEnableControlsFixed()
    endif
    if !isnull(this.parent.oContained.oNewFocus)
      this.parent.oContained.oNewFocus.SetFocus()
      this.parent.oContained.oNewFocus=.NULL.
    endif
    if this.RelativeRow<>0
      this.nRelRow=this.RelativeRow
    endif
    if this.nBeforeAfter>0 && and Version(5)<700
      this.nBeforeAfter=this.nBeforeAfter-1
    endif
    thisform.lockscreen=.f.
    this.parent.oContained.i_lastcheckrow=0
  EndProc
  Func oBody.GetDropTarget(cFile,nX,nY)
    local oDropInto
    oDropInto=.NULL.
    do case
      case cFile='TRI_BUTI'
        oDropInto=this.oBodyCol.oRow.oTRCODTRI_2_5
    endcase
    return(oDropInto)
  EndFunc


  add object oBeginFooter as BodyKeyMover with nDirection=-1
enddefine

* --- Defining Body row
define class tgsar_mtbBodyRow as CPBodyRowCnt
  Width=350
  Height=int(fontmetric(1,"Arial",9,"")*1*1.3000000000000000)
  BackStyle=0                                                && 0=trasparente
  BorderWidth=0                                              && Spessore Bordo
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  oContained = .NULL.

  add object oTRCODTRI_2_5 as StdTrsField with uid="LXKAVQDMLN",rtseq=7,rtrep=.t.,;
    cFormVar="w_TRCODTRI",value=space(5),;
    ToolTipText = "Codice tributo associato",;
    HelpContextID = 63574143,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
    sErrorMsg = "Inserire solo tributi di tipo IRPEF",;
   bGlobalFont=.t.,;
    Height=17, Width=51, Left=-2, Top=0, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="TRI_BUTI", cZoomOnZoom="GSAR_ATB", oKey_1_1="TRCODTRI", oKey_1_2="this.w_TRCODTRI"

  func oTRCODTRI_2_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oTRCODTRI_2_5.ecpDrop(oSource)
    this.Parent.oContained.link_2_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
    select (this.parent.oContained.cTrsname)
    if I_SRV<>'A'
      replace I_SRV with 'U'
    endif
    this.SetFocus()
  endproc

  proc oTRCODTRI_2_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'TRI_BUTI','*','TRCODTRI',cp_AbsName(this.parent,'oTRCODTRI_2_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ATB',"Codici tributi",'GSRI1AMR.TRI_BUTI_VZM',this.parent.oContained
  endproc
  proc oTRCODTRI_2_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ATB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_TRCODTRI=this.parent.oContained.w_TRCODTRI
    i_obj.ecpSave()
  endproc

  add object oTRPERRIT_2_6 as StdTrsField with uid="QWHNBHFHXG",rtseq=8,rtrep=.t.,;
    cFormVar="w_TRPERRIT",value=0,;
    ToolTipText = "Percentuale importo ritenuta IRPEF",;
    HelpContextID = 224337782,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=58, Left=54, Top=0, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oTRPERIMP_2_7 as StdTrsField with uid="ETRHYVQRSH",rtseq=9,rtrep=.t.,;
    cFormVar="w_TRPERIMP",value=0,;
    ToolTipText = "Percentuale imponibile ritenuta IRPEF",;
    HelpContextID = 106897274,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=60, Left=117, Top=0, cSayPict=["999.99"], cGetPict=["999.99"]

  add object oTRDATINI_2_8 as StdTrsField with uid="JCTVPZXQQO",rtseq=10,rtrep=.t.,;
    cFormVar="w_TRDATINI",value=ctod("  /  /  "),;
    ToolTipText = "Data di inizio validit� del conto ai fini della gestione ritenute",;
    HelpContextID = 105111425,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=182, Top=0

  add object oTRDATFIN_2_9 as StdTrsField with uid="AGSUWPSPFK",rtseq=11,rtrep=.t.,;
    cFormVar="w_TRDATFIN",value=ctod("  /  /  "),;
    ToolTipText = "Data di fine validit� del conto ai fini della gestione ritenute",;
    HelpContextID = 112992388,;
    cTotal = "", SpecialEffect=1, BorderStyle=0,;
    bObbl = .f. , nPag = 2, bIsInHeader=.f., bMultilanguage =  .f.,;
   bGlobalFont=.t.,;
    Height=17, Width=79, Left=266, Top=0
  add object oLast as LastKeyMover
  * ---
  func oTRCODTRI_2_5.When()
    return(.t.)
  proc oTRCODTRI_2_5.GotFocus()
    if inlist(this.parent.oContained.cFunction,'Edit','Load')
      this.Parent.Parent.Parent.SetCurrentRow()
      this.parent.oContained.SetControlsValue()
    endif
    DoDefault()
  proc oTRCODTRI_2_5.KeyPress(nKeyCode,nShift)
    DoDefault(nKeyCode,nShift)
    do case
      case inlist(nKeyCode,5,15)
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()>this.parent.oContained.nFirstRow
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            thisform.lockscreen=.t.
            skip -1
            this.parent.parent.parent.SetFullFocus()
          else
            this.Parent.Parent.Parent.Parent.oEndHeader.MoveFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
      case nKeyCode=24
        nodefault
        if this.valid()=0
          return
        endif
        if this.parent.oContained.CheckRow()
          if recno()=this.Parent.oContained.nLastRow
            if this.Parent.oContained.FullRow() and this.Parent.oContained.CanAddRow()
              thisform.lockscreen=.t.
              this.Parent.oContained.__dummy__.enabled=.t.
              this.Parent.oContained.__dummy__.SetFocus()
              this.Parent.oContained.InitRow()
              this.Parent.Parent.Parent.SetFocus()
              this.Parent.oContained.__dummy__.enabled=.f.
            else
              this.Parent.Parent.Parent.Parent.oBeginFooter.MoveFocus()
            endif
          else
            thisform.lockscreen=.t.
            this.parent.oContained.i_lastcheckrow=this.parent.parent.parent.nAbsRow
            if this.parent.parent.parent.RelativeRow=9
              this.parent.parent.parent.DoScroll(1)
            endif
            skip
            this.parent.parent.parent.SetFullFocus()
          endif
        else
          if !isnull(this.parent.oContained.oNewFocus)
            this.Parent.oContained.oNewFocus.SetFocus()
            this.Parent.oContained.oNewFocus=.NULL.
          endif
        endif
    endcase
* ---
  proc BackStyle_Access()
    DoDefault()
    * --- Area Manuale = BodyRow BackStyle Access
    * --- Fine Area Manuale
    return this.BackStyle
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_mtb','TAB_RITE','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".TRTIPRIT=TAB_RITE.TRTIPRIT";
  +" and "+i_cAliasName2+".TRCODAZI=TAB_RITE.TRCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Detail File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
