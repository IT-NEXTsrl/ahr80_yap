* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsac_kdc                                                        *
*              Duplicazione contratti di acquisto                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [146] [VRS_226]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-05-05                                                      *
* Last revis.: 2008-10-21                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsac_kdc",oParentObject))

* --- Class definition
define class tgsac_kdc as StdForm
  Top    = 9
  Left   = 10

  * --- Standard Properties
  Width  = 656
  Height = 367+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2008-10-21"
  HelpContextID=82470761
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=56

  * --- Constant Properties
  _IDX = 0
  AZIENDA_IDX = 0
  VALUTE_IDX = 0
  CON_TRAM_IDX = 0
  CATECOMM_IDX = 0
  CONTI_IDX = 0
  ART_ICOL_IDX = 0
  GRUMERC_IDX = 0
  cPrg = "gsac_kdc"
  cComment = "Duplicazione contratti di acquisto"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_NUMERO = space(15)
  o_NUMERO = space(15)
  w_VALUTA = space(3)
  o_VALUTA = space(3)
  w_CAMBIO1 = 0
  w_CONUMERO = space(15)
  w_CODESCON = space(50)
  w_CODATCON = ctod('  /  /  ')
  w_CODATINI = ctod('  /  /  ')
  w_CODATFIN = ctod('  /  /  ')
  w_COCODCLF = space(15)
  w_COAFFIDA = 0
  w_OBTEST = ctod('  /  /  ')
  w_COCODVAL = space(3)
  o_COCODVAL = space(3)
  w_CAMBIO2 = 0
  w_RICPE1 = 0
  w_RICVA1 = 0
  w_ARROT1 = 0
  w_VALORIN = 0
  w_ARROT2 = 0
  w_VALOR2IN = 0
  w_ARROT3 = 0
  w_VALOR3IN = 0
  w_ARROT4 = 0
  w_VALOFIN = 0
  w_AZIENDA = space(5)
  w_DATOBSO = ctod('  /  /  ')
  w_COTIPCON = space(1)
  w_CODESCRI = space(50)
  w_DECTOT = 0
  w_CALCPICT = 0
  w_DATCONIN = ctod('  /  /  ')
  w_DATCONFI = ctod('  /  /  ')
  w_NOTE = space(0)
  w_CATCOM = space(3)
  w_CODCLF = space(15)
  w_DATACON = ctod('  /  /  ')
  w_ARTINI = space(20)
  w_ARTFIN = space(20)
  w_GRUMER = space(5)
  w_GRUMER1 = space(5)
  w_DESINI = space(40)
  w_DESFIN = space(40)
  w_GDESC = space(35)
  w_GDESC1 = space(35)
  w_FLSCAG = space(1)
  w_COCATCOM = space(3)
  w_COFLUCOA = space(1)
  w_COIVALIS = space(1)
  w_CO_NOTE = space(0)
  w_DTOBSO = ctod('  /  /  ')
  w_DECTOT1 = 0
  w_COTIPCLF = space(1)
  w_SIMVAL = space(5)
  w_SIMVAL2 = space(5)
  w_COFLARTI = space(1)
  w_CAOVAL2 = 0
  w_CAOVAL1 = 0
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsac_kdcPag1","gsac_kdc",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni principali")
      .Pages(2).addobject("oPag","tgsac_kdcPag2","gsac_kdc",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni ulteriori")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNUMERO_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[7]
    this.cWorkTables[1]='AZIENDA'
    this.cWorkTables[2]='VALUTE'
    this.cWorkTables[3]='CON_TRAM'
    this.cWorkTables[4]='CATECOMM'
    this.cWorkTables[5]='CONTI'
    this.cWorkTables[6]='ART_ICOL'
    this.cWorkTables[7]='GRUMERC'
    return(this.OpenAllTables(7))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NUMERO=space(15)
      .w_VALUTA=space(3)
      .w_CAMBIO1=0
      .w_CONUMERO=space(15)
      .w_CODESCON=space(50)
      .w_CODATCON=ctod("  /  /  ")
      .w_CODATINI=ctod("  /  /  ")
      .w_CODATFIN=ctod("  /  /  ")
      .w_COCODCLF=space(15)
      .w_COAFFIDA=0
      .w_OBTEST=ctod("  /  /  ")
      .w_COCODVAL=space(3)
      .w_CAMBIO2=0
      .w_RICPE1=0
      .w_RICVA1=0
      .w_ARROT1=0
      .w_VALORIN=0
      .w_ARROT2=0
      .w_VALOR2IN=0
      .w_ARROT3=0
      .w_VALOR3IN=0
      .w_ARROT4=0
      .w_VALOFIN=0
      .w_AZIENDA=space(5)
      .w_DATOBSO=ctod("  /  /  ")
      .w_COTIPCON=space(1)
      .w_CODESCRI=space(50)
      .w_DECTOT=0
      .w_CALCPICT=0
      .w_DATCONIN=ctod("  /  /  ")
      .w_DATCONFI=ctod("  /  /  ")
      .w_NOTE=space(0)
      .w_CATCOM=space(3)
      .w_CODCLF=space(15)
      .w_DATACON=ctod("  /  /  ")
      .w_ARTINI=space(20)
      .w_ARTFIN=space(20)
      .w_GRUMER=space(5)
      .w_GRUMER1=space(5)
      .w_DESINI=space(40)
      .w_DESFIN=space(40)
      .w_GDESC=space(35)
      .w_GDESC1=space(35)
      .w_FLSCAG=space(1)
      .w_COCATCOM=space(3)
      .w_COFLUCOA=space(1)
      .w_COIVALIS=space(1)
      .w_CO_NOTE=space(0)
      .w_DTOBSO=ctod("  /  /  ")
      .w_DECTOT1=0
      .w_COTIPCLF=space(1)
      .w_SIMVAL=space(5)
      .w_SIMVAL2=space(5)
      .w_COFLARTI=space(1)
      .w_CAOVAL2=0
      .w_CAOVAL1=0
        .DoRTCalc(1,1,.f.)
        if not(empty(.w_NUMERO))
          .link_1_1('Full')
        endif
        .DoRTCalc(2,2,.f.)
        if not(empty(.w_VALUTA))
          .link_1_2('Full')
        endif
        .w_CAMBIO1 = GETCAM(.w_VALUTA, .w_OBTEST,7)
          .DoRTCalc(4,4,.f.)
        .w_CODESCON = .w_CODESCRI
        .w_CODATCON = .w_OBTEST
        .w_CODATINI = i_DATSYS
          .DoRTCalc(8,8,.f.)
        .w_COCODCLF = .w_CODCLF
        .DoRTCalc(9,9,.f.)
        if not(empty(.w_COCODCLF))
          .link_1_9('Full')
        endif
          .DoRTCalc(10,10,.f.)
        .w_OBTEST = IIF(EMPTY(.w_CODATFIN), .w_CODATINI, .w_CODATFIN)
        .w_COCODVAL = .w_VALUTA
        .DoRTCalc(12,12,.f.)
        if not(empty(.w_COCODVAL))
          .link_1_12('Full')
        endif
        .w_CAMBIO2 = GETCAM(.w_COCODVAL, .w_OBTEST,7)
          .DoRTCalc(14,22,.f.)
        .w_VALOFIN = MAX(.w_VALORIN, .w_VALOR2IN, .w_VALOR3IN)
        .w_AZIENDA = i_CODAZI
          .DoRTCalc(25,25,.f.)
        .w_COTIPCON = 'F'
          .DoRTCalc(27,28,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
          .DoRTCalc(30,31,.f.)
        .w_NOTE = ' '
        .DoRTCalc(33,36,.f.)
        if not(empty(.w_ARTINI))
          .link_2_1('Full')
        endif
        .DoRTCalc(37,37,.f.)
        if not(empty(.w_ARTFIN))
          .link_2_2('Full')
        endif
        .DoRTCalc(38,38,.f.)
        if not(empty(.w_GRUMER))
          .link_2_3('Full')
        endif
        .DoRTCalc(39,39,.f.)
        if not(empty(.w_GRUMER1))
          .link_2_4('Full')
        endif
          .DoRTCalc(40,44,.f.)
        .w_COCATCOM = SPACE(3)
          .DoRTCalc(46,47,.f.)
        .w_CO_NOTE = .w_NOTE
          .DoRTCalc(49,51,.f.)
        .w_SIMVAL = IIF(EMPTY(.w_VALUTA),' ',.w_SIMVAL)
        .w_SIMVAL2 = IIF(EMPTY(.w_COCODVAL),' ',.w_SIMVAL2)
    endwith
    this.DoRTCalc(54,56,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_37.enabled = this.oPgFrm.Page1.oPag.oBtn_1_37.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_38.enabled = this.oPgFrm.Page1.oPag.oBtn_1_38.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,1,.t.)
          .link_1_2('Full')
        if .o_VALUTA<>.w_VALUTA
            .w_CAMBIO1 = GETCAM(.w_VALUTA, .w_OBTEST,7)
        endif
        .DoRTCalc(4,4,.t.)
        if .o_NUMERO<>.w_NUMERO
            .w_CODESCON = .w_CODESCRI
        endif
        if .o_NUMERO<>.w_NUMERO
            .w_CODATCON = .w_OBTEST
        endif
        .DoRTCalc(7,8,.t.)
        if .o_NUMERO<>.w_NUMERO
            .w_COCODCLF = .w_CODCLF
          .link_1_9('Full')
        endif
        .DoRTCalc(10,10,.t.)
            .w_OBTEST = IIF(EMPTY(.w_CODATFIN), .w_CODATINI, .w_CODATFIN)
        if .o_NUMERO<>.w_NUMERO
            .w_COCODVAL = .w_VALUTA
          .link_1_12('Full')
        endif
        if .o_COCODVAL<>.w_COCODVAL
            .w_CAMBIO2 = GETCAM(.w_COCODVAL, .w_OBTEST,7)
        endif
        .DoRTCalc(14,22,.t.)
            .w_VALOFIN = MAX(.w_VALORIN, .w_VALOR2IN, .w_VALOR3IN)
        .DoRTCalc(24,28,.t.)
        if .o_COCODVAL<>.w_COCODVAL
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        .DoRTCalc(30,31,.t.)
            .w_NOTE = ' '
        .DoRTCalc(33,47,.t.)
        if .o_NUMERO<>.w_NUMERO
            .w_CO_NOTE = .w_NOTE
        endif
        .DoRTCalc(49,51,.t.)
        if .o_VALUTA<>.w_VALUTA
            .w_SIMVAL = IIF(EMPTY(.w_VALUTA),' ',.w_SIMVAL)
        endif
        if .o_COCODVAL<>.w_COCODVAL
            .w_SIMVAL2 = IIF(EMPTY(.w_COCODVAL),' ',.w_SIMVAL2)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(54,56,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oRICPE1_1_14.enabled = this.oPgFrm.Page1.oPag.oRICPE1_1_14.mCond()
    this.oPgFrm.Page1.oPag.oRICVA1_1_15.enabled = this.oPgFrm.Page1.oPag.oRICVA1_1_15.mCond()
    this.oPgFrm.Page1.oPag.oARROT2_1_18.enabled = this.oPgFrm.Page1.oPag.oARROT2_1_18.mCond()
    this.oPgFrm.Page1.oPag.oVALOR2IN_1_19.enabled = this.oPgFrm.Page1.oPag.oVALOR2IN_1_19.mCond()
    this.oPgFrm.Page1.oPag.oARROT3_1_20.enabled = this.oPgFrm.Page1.oPag.oARROT3_1_20.mCond()
    this.oPgFrm.Page1.oPag.oVALOR3IN_1_21.enabled = this.oPgFrm.Page1.oPag.oVALOR3IN_1_21.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=NUMERO
  func Link_1_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CON_TRAM_IDX,3]
    i_lTable = "CON_TRAM"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2], .t., this.CON_TRAM_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_NUMERO) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BCZ',True,'CON_TRAM')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CONUMERO like "+cp_ToStrODBC(trim(this.w_NUMERO)+"%");

          i_ret=cp_SQL(i_nConn,"select CONUMERO,CODESCON,COCODVAL,CO__NOTE,COCODCLF,CODATCON,CODATINI,CODATFIN,COQUANTI,COAFFIDA,COFLUCOA,COIVALIS,COTIPCLF,COFLARTI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CONUMERO","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CONUMERO',trim(this.w_NUMERO))
          select CONUMERO,CODESCON,COCODVAL,CO__NOTE,COCODCLF,CODATCON,CODATINI,CODATFIN,COQUANTI,COAFFIDA,COFLUCOA,COIVALIS,COTIPCLF,COFLARTI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CONUMERO into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_NUMERO)==trim(_Link_.CONUMERO) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_NUMERO) and !this.bDontReportError
            deferred_cp_zoom('CON_TRAM','*','CONUMERO',cp_AbsName(oSource.parent,'oNUMERO_1_1'),i_cWhere,'GSAR_BCZ',"Contratti",'GSAC_MCO.CON_TRAM_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CONUMERO,CODESCON,COCODVAL,CO__NOTE,COCODCLF,CODATCON,CODATINI,CODATFIN,COQUANTI,COAFFIDA,COFLUCOA,COIVALIS,COTIPCLF,COFLARTI";
                     +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CONUMERO',oSource.xKey(1))
            select CONUMERO,CODESCON,COCODVAL,CO__NOTE,COCODCLF,CODATCON,CODATINI,CODATFIN,COQUANTI,COAFFIDA,COFLUCOA,COIVALIS,COTIPCLF,COFLARTI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_NUMERO)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CONUMERO,CODESCON,COCODVAL,CO__NOTE,COCODCLF,CODATCON,CODATINI,CODATFIN,COQUANTI,COAFFIDA,COFLUCOA,COIVALIS,COTIPCLF,COFLARTI";
                   +" from "+i_cTable+" "+i_lTable+" where CONUMERO="+cp_ToStrODBC(this.w_NUMERO);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CONUMERO',this.w_NUMERO)
            select CONUMERO,CODESCON,COCODVAL,CO__NOTE,COCODCLF,CODATCON,CODATINI,CODATFIN,COQUANTI,COAFFIDA,COFLUCOA,COIVALIS,COTIPCLF,COFLARTI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_NUMERO = NVL(_Link_.CONUMERO,space(15))
      this.w_CODESCRI = NVL(_Link_.CODESCON,space(50))
      this.w_VALUTA = NVL(_Link_.COCODVAL,space(3))
      this.w_NOTE = NVL(_Link_.CO__NOTE,space(0))
      this.w_CODCLF = NVL(_Link_.COCODCLF,space(15))
      this.w_DATACON = NVL(cp_ToDate(_Link_.CODATCON),ctod("  /  /  "))
      this.w_DATCONIN = NVL(cp_ToDate(_Link_.CODATINI),ctod("  /  /  "))
      this.w_DATCONFI = NVL(cp_ToDate(_Link_.CODATFIN),ctod("  /  /  "))
      this.w_FLSCAG = NVL(_Link_.COQUANTI,space(1))
      this.w_COAFFIDA = NVL(_Link_.COAFFIDA,0)
      this.w_COFLUCOA = NVL(_Link_.COFLUCOA,space(1))
      this.w_COIVALIS = NVL(_Link_.COIVALIS,space(1))
      this.w_COTIPCLF = NVL(_Link_.COTIPCLF,space(1))
      this.w_COFLARTI = NVL(_Link_.COFLARTI,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_NUMERO = space(15)
      endif
      this.w_CODESCRI = space(50)
      this.w_VALUTA = space(3)
      this.w_NOTE = space(0)
      this.w_CODCLF = space(15)
      this.w_DATACON = ctod("  /  /  ")
      this.w_DATCONIN = ctod("  /  /  ")
      this.w_DATCONFI = ctod("  /  /  ")
      this.w_FLSCAG = space(1)
      this.w_COAFFIDA = 0
      this.w_COFLUCOA = space(1)
      this.w_COIVALIS = space(1)
      this.w_COTIPCLF = space(1)
      this.w_COFLARTI = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=.w_COTIPCLF='F'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        endif
        this.w_NUMERO = space(15)
        this.w_CODESCRI = space(50)
        this.w_VALUTA = space(3)
        this.w_NOTE = space(0)
        this.w_CODCLF = space(15)
        this.w_DATACON = ctod("  /  /  ")
        this.w_DATCONIN = ctod("  /  /  ")
        this.w_DATCONFI = ctod("  /  /  ")
        this.w_FLSCAG = space(1)
        this.w_COAFFIDA = 0
        this.w_COFLUCOA = space(1)
        this.w_COIVALIS = space(1)
        this.w_COTIPCLF = space(1)
        this.w_COFLARTI = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CON_TRAM_IDX,2])+'\'+cp_ToStr(_Link_.CONUMERO,1)
      cp_ShowWarn(i_cKey,this.CON_TRAM_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_NUMERO Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_1_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   +" and VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADECUNI,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT1 = NVL(_Link_.VADECUNI,0)
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DECTOT1 = 0
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCODCLF
  func Link_1_9(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AFR',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_COCODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_COTIPCON;
                     ,'ANCODICE',trim(this.w_COCODCLF))
          select ANTIPCON,ANCODICE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODCLF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCODCLF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCOCODCLF_1_9'),i_cWhere,'GSAR_AFR',"Elenco fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_COTIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_COCODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_COTIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_COTIPCON;
                       ,'ANCODICE',this.w_COCODCLF)
            select ANTIPCON,ANCODICE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODCLF = NVL(_Link_.ANCODICE,space(15))
    else
      if i_cCtrl<>'Load'
        this.w_COCODCLF = space(15)
      endif
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=COCODVAL
  func Link_1_12(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_COCODVAL) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_COCODVAL)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI,VADTOBSO,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_COCODVAL))
          select VACODVAL,VADECUNI,VADTOBSO,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_COCODVAL)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_COCODVAL) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oCOCODVAL_1_12'),i_cWhere,'GSAR_AVL',"",'VALUZOOM.VALUTE_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI,VADTOBSO,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADECUNI,VADTOBSO,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_COCODVAL)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADECUNI,VADTOBSO,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_COCODVAL);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_COCODVAL)
            select VACODVAL,VADECUNI,VADTOBSO,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_COCODVAL = NVL(_Link_.VACODVAL,space(3))
      this.w_DECTOT = NVL(_Link_.VADECUNI,0)
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.VADTOBSO),ctod("  /  /  "))
      this.w_SIMVAL2 = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_COCODVAL = space(3)
      endif
      this.w_DECTOT = 0
      this.w_DTOBSO = ctod("  /  /  ")
      this.w_SIMVAL2 = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
        endif
        this.w_COCODVAL = space(3)
        this.w_DECTOT = 0
        this.w_DTOBSO = ctod("  /  /  ")
        this.w_SIMVAL2 = space(5)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_COCODVAL Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTINI
  func Link_2_1(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTINI) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ARTINI)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ARTINI))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTINI)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARTINI) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oARTINI_2_1'),i_cWhere,'GSMA_BZA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTINI)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARTINI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARTINI)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTINI = NVL(_Link_.ARCODART,space(20))
      this.w_DESINI = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARTINI = space(20)
      endif
      this.w_DESINI = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((empty(.w_ARTFIN)) OR  (.w_ARTINI<=.w_ARTFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale superiore al codice finale oppure obsoleto")
        endif
        this.w_ARTINI = space(20)
        this.w_DESINI = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTINI Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=ARTFIN
  func Link_2_2(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ART_ICOL_IDX,3]
    i_lTable = "ART_ICOL"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2], .t., this.ART_ICOL_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ARTFIN) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSMA_BZA',True,'ART_ICOL')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ARCODART like "+cp_ToStrODBC(trim(this.w_ARTFIN)+"%");

          i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ARCODART","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ARCODART',trim(this.w_ARTFIN))
          select ARCODART,ARDESART,ARDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ARCODART into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ARTFIN)==trim(_Link_.ARCODART) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ARTFIN) and !this.bDontReportError
            deferred_cp_zoom('ART_ICOL','*','ARCODART',cp_AbsName(oSource.parent,'oARTFIN_2_2'),i_cWhere,'GSMA_BZA',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',oSource.xKey(1))
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ARTFIN)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ARCODART,ARDESART,ARDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where ARCODART="+cp_ToStrODBC(this.w_ARTFIN);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ARCODART',this.w_ARTFIN)
            select ARCODART,ARDESART,ARDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ARTFIN = NVL(_Link_.ARCODART,space(20))
      this.w_DESFIN = NVL(_Link_.ARDESART,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ARDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ARTFIN = space(20)
      endif
      this.w_DESFIN = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((.w_ARTFIN>=.w_ARTINI) or (empty(.w_ARTINI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale superiore al codice finale oppure obsoleto")
        endif
        this.w_ARTFIN = space(20)
        this.w_DESFIN = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ART_ICOL_IDX,2])+'\'+cp_ToStr(_Link_.ARCODART,1)
      cp_ShowWarn(i_cKey,this.ART_ICOL_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ARTFIN Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUMER
  func Link_2_3(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUMER) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUMER)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUMER))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUMER)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUMER) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUMER_2_3'),i_cWhere,'GSAR_AGM',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUMER)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUMER);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUMER)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUMER = NVL(_Link_.GMCODICE,space(5))
      this.w_GDESC = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUMER = space(5)
      endif
      this.w_GDESC = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_GRUMER1)) OR (.w_GRUMER<=.w_GRUMER1))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale superiore al codice finale oppure errato")
        endif
        this.w_GRUMER = space(5)
        this.w_GDESC = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUMER Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=GRUMER1
  func Link_2_4(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.GRUMERC_IDX,3]
    i_lTable = "GRUMERC"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2], .t., this.GRUMERC_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_GRUMER1) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGM',True,'GRUMERC')
        if i_nConn<>0
          i_cWhere = i_cFlt+" GMCODICE like "+cp_ToStrODBC(trim(this.w_GRUMER1)+"%");

          i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by GMCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'GMCODICE',trim(this.w_GRUMER1))
          select GMCODICE,GMDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by GMCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_GRUMER1)==trim(_Link_.GMCODICE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_GRUMER1) and !this.bDontReportError
            deferred_cp_zoom('GRUMERC','*','GMCODICE',cp_AbsName(oSource.parent,'oGRUMER1_2_4'),i_cWhere,'GSAR_AGM',"",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                     +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',oSource.xKey(1))
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_GRUMER1)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select GMCODICE,GMDESCRI"+cp_TransInsFldName("GMDESCRI")+"";
                   +" from "+i_cTable+" "+i_lTable+" where GMCODICE="+cp_ToStrODBC(this.w_GRUMER1);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'GMCODICE',this.w_GRUMER1)
            select GMCODICE,GMDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_GRUMER1 = NVL(_Link_.GMCODICE,space(5))
      this.w_GDESC1 = NVL(cp_TransLoadField('_Link_.GMDESCRI'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_GRUMER1 = space(5)
      endif
      this.w_GDESC1 = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=((EMPTY(.w_GRUMER)) OR (.w_GRUMER1>=.w_GRUMER))
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Codice iniziale superiore al codice finale oppure errato")
        endif
        this.w_GRUMER1 = space(5)
        this.w_GDESC1 = space(35)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.GRUMERC_IDX,2])+'\'+cp_ToStr(_Link_.GMCODICE,1)
      cp_ShowWarn(i_cKey,this.GRUMERC_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_GRUMER1 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNUMERO_1_1.value==this.w_NUMERO)
      this.oPgFrm.Page1.oPag.oNUMERO_1_1.value=this.w_NUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oVALUTA_1_2.value==this.w_VALUTA)
      this.oPgFrm.Page1.oPag.oVALUTA_1_2.value=this.w_VALUTA
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMBIO1_1_3.value==this.w_CAMBIO1)
      this.oPgFrm.Page1.oPag.oCAMBIO1_1_3.value=this.w_CAMBIO1
    endif
    if not(this.oPgFrm.Page1.oPag.oCONUMERO_1_4.value==this.w_CONUMERO)
      this.oPgFrm.Page1.oPag.oCONUMERO_1_4.value=this.w_CONUMERO
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESCON_1_5.value==this.w_CODESCON)
      this.oPgFrm.Page1.oPag.oCODESCON_1_5.value=this.w_CODESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATCON_1_6.value==this.w_CODATCON)
      this.oPgFrm.Page1.oPag.oCODATCON_1_6.value=this.w_CODATCON
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATINI_1_7.value==this.w_CODATINI)
      this.oPgFrm.Page1.oPag.oCODATINI_1_7.value=this.w_CODATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oCODATFIN_1_8.value==this.w_CODATFIN)
      this.oPgFrm.Page1.oPag.oCODATFIN_1_8.value=this.w_CODATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODCLF_1_9.value==this.w_COCODCLF)
      this.oPgFrm.Page1.oPag.oCOCODCLF_1_9.value=this.w_COCODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oCOAFFIDA_1_10.value==this.w_COAFFIDA)
      this.oPgFrm.Page1.oPag.oCOAFFIDA_1_10.value=this.w_COAFFIDA
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODVAL_1_12.value==this.w_COCODVAL)
      this.oPgFrm.Page1.oPag.oCOCODVAL_1_12.value=this.w_COCODVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oCAMBIO2_1_13.value==this.w_CAMBIO2)
      this.oPgFrm.Page1.oPag.oCAMBIO2_1_13.value=this.w_CAMBIO2
    endif
    if not(this.oPgFrm.Page1.oPag.oRICPE1_1_14.value==this.w_RICPE1)
      this.oPgFrm.Page1.oPag.oRICPE1_1_14.value=this.w_RICPE1
    endif
    if not(this.oPgFrm.Page1.oPag.oRICVA1_1_15.value==this.w_RICVA1)
      this.oPgFrm.Page1.oPag.oRICVA1_1_15.value=this.w_RICVA1
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT1_1_16.value==this.w_ARROT1)
      this.oPgFrm.Page1.oPag.oARROT1_1_16.value=this.w_ARROT1
    endif
    if not(this.oPgFrm.Page1.oPag.oVALORIN_1_17.value==this.w_VALORIN)
      this.oPgFrm.Page1.oPag.oVALORIN_1_17.value=this.w_VALORIN
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT2_1_18.value==this.w_ARROT2)
      this.oPgFrm.Page1.oPag.oARROT2_1_18.value=this.w_ARROT2
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOR2IN_1_19.value==this.w_VALOR2IN)
      this.oPgFrm.Page1.oPag.oVALOR2IN_1_19.value=this.w_VALOR2IN
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT3_1_20.value==this.w_ARROT3)
      this.oPgFrm.Page1.oPag.oARROT3_1_20.value=this.w_ARROT3
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOR3IN_1_21.value==this.w_VALOR3IN)
      this.oPgFrm.Page1.oPag.oVALOR3IN_1_21.value=this.w_VALOR3IN
    endif
    if not(this.oPgFrm.Page1.oPag.oARROT4_1_22.value==this.w_ARROT4)
      this.oPgFrm.Page1.oPag.oARROT4_1_22.value=this.w_ARROT4
    endif
    if not(this.oPgFrm.Page1.oPag.oVALOFIN_1_23.value==this.w_VALOFIN)
      this.oPgFrm.Page1.oPag.oVALOFIN_1_23.value=this.w_VALOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODESCRI_1_27.value==this.w_CODESCRI)
      this.oPgFrm.Page1.oPag.oCODESCRI_1_27.value=this.w_CODESCRI
    endif
    if not(this.oPgFrm.Page2.oPag.oARTINI_2_1.value==this.w_ARTINI)
      this.oPgFrm.Page2.oPag.oARTINI_2_1.value=this.w_ARTINI
    endif
    if not(this.oPgFrm.Page2.oPag.oARTFIN_2_2.value==this.w_ARTFIN)
      this.oPgFrm.Page2.oPag.oARTFIN_2_2.value=this.w_ARTFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUMER_2_3.value==this.w_GRUMER)
      this.oPgFrm.Page2.oPag.oGRUMER_2_3.value=this.w_GRUMER
    endif
    if not(this.oPgFrm.Page2.oPag.oGRUMER1_2_4.value==this.w_GRUMER1)
      this.oPgFrm.Page2.oPag.oGRUMER1_2_4.value=this.w_GRUMER1
    endif
    if not(this.oPgFrm.Page2.oPag.oDESINI_2_7.value==this.w_DESINI)
      this.oPgFrm.Page2.oPag.oDESINI_2_7.value=this.w_DESINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDESFIN_2_8.value==this.w_DESFIN)
      this.oPgFrm.Page2.oPag.oDESFIN_2_8.value=this.w_DESFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oGDESC_2_9.value==this.w_GDESC)
      this.oPgFrm.Page2.oPag.oGDESC_2_9.value=this.w_GDESC
    endif
    if not(this.oPgFrm.Page2.oPag.oGDESC1_2_10.value==this.w_GDESC1)
      this.oPgFrm.Page2.oPag.oGDESC1_2_10.value=this.w_GDESC1
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_67.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_67.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL2_1_68.value==this.w_SIMVAL2)
      this.oPgFrm.Page1.oPag.oSIMVAL2_1_68.value=this.w_SIMVAL2
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_COTIPCLF='F')  and not(empty(.w_NUMERO))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMERO_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CAMBIO1))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMBIO1_1_3.SetFocus()
            i_bnoObbl = !empty(.w_CAMBIO1)
            i_bnoChk = .f.
            i_bRes = .f.
          case   (empty(.w_CODATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODATINI_1_7.SetFocus()
            i_bnoObbl = !empty(.w_CODATINI)
            i_bnoChk = .f.
            i_bRes = .f.
          case   ((empty(.w_CODATFIN)) or not(NOT .w_CODATFIN<.w_CODATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODATFIN_1_8.SetFocus()
            i_bnoObbl = !empty(.w_CODATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data di inizio validit� � maggiore della data finale")
          case   ((empty(.w_COCODVAL)) or not(CHKDTOBS(.w_DTOBSO,.w_OBTEST,"Valuta obsoleta alla data Attuale!", .F.)))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCOCODVAL_1_12.SetFocus()
            i_bnoObbl = !empty(.w_COCODVAL)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valuta inesistente o incongruente o obsoleta")
          case   (empty(.w_CAMBIO2))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCAMBIO2_1_13.SetFocus()
            i_bnoObbl = !empty(.w_CAMBIO2)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_VALOR2IN>.w_VALORIN)  and (.w_VALORIN<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVALOR2IN_1_19.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_VALOR3IN>.w_VALORIN AND .w_VALOR3IN>.w_VALOR2IN)  and (.w_VALORIN<>0 AND .w_VALOR2IN<>0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oVALOR3IN_1_21.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(((empty(.w_ARTFIN)) OR  (.w_ARTINI<=.w_ARTFIN)) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_ARTINI))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARTINI_2_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale superiore al codice finale oppure obsoleto")
          case   not(((.w_ARTFIN>=.w_ARTINI) or (empty(.w_ARTINI))) and (EMPTY(.w_DATOBSO) OR .w_DATOBSO>.w_OBTEST))  and not(empty(.w_ARTFIN))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oARTFIN_2_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale superiore al codice finale oppure obsoleto")
          case   not(((EMPTY(.w_GRUMER1)) OR (.w_GRUMER<=.w_GRUMER1)))  and not(empty(.w_GRUMER))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGRUMER_2_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale superiore al codice finale oppure errato")
          case   not(((EMPTY(.w_GRUMER)) OR (.w_GRUMER1>=.w_GRUMER)))  and not(empty(.w_GRUMER1))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oGRUMER1_2_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Codice iniziale superiore al codice finale oppure errato")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_NUMERO = this.w_NUMERO
    this.o_VALUTA = this.w_VALUTA
    this.o_COCODVAL = this.w_COCODVAL
    return

enddefine

* --- Define pages as container
define class tgsac_kdcPag1 as StdContainer
  Width  = 652
  height = 367
  stdWidth  = 652
  stdheight = 367
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNUMERO_1_1 as StdField with uid="YNJAORDGVJ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_NUMERO", cQueryName = "NUMERO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice contratto da duplicare",;
    HelpContextID = 8404266,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=149, Top=14, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CON_TRAM", cZoomOnZoom="GSAR_BCZ", oKey_1_1="CONUMERO", oKey_1_2="this.w_NUMERO"

  func oNUMERO_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oNUMERO_1_1.ecpDrop(oSource)
    this.Parent.oContained.link_1_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oNUMERO_1_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CON_TRAM','*','CONUMERO',cp_AbsName(this.parent,'oNUMERO_1_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BCZ',"Contratti",'GSAC_MCO.CON_TRAM_VZM',this.parent.oContained
  endproc
  proc oNUMERO_1_1.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BCZ()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CONUMERO=this.parent.oContained.w_NUMERO
     i_obj.ecpSave()
  endproc

  add object oVALUTA_1_2 as StdField with uid="NFSILTCDYY",rtseq=2,rtrep=.f.,;
    cFormVar = "w_VALUTA", cQueryName = "VALUTA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta del contratto di riferimento",;
    HelpContextID = 240148650,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=149, Top=40, InputMask=replicate('X',3), cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALUTA", oKey_2_1="VACODVAL", oKey_2_2="this.w_VALUTA"

  func oVALUTA_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_VALUTA)
        bRes2=.link_1_2('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCAMBIO1_1_3 as StdField with uid="URECSNLHDK",rtseq=3,rtrep=.f.,;
    cFormVar = "w_CAMBIO1", cQueryName = "CAMBIO1",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio inseribile se non in valuta EMU",;
    HelpContextID = 18043354,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=374, Top=40, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  add object oCONUMERO_1_4 as StdField with uid="VIFVZNIHMV",rtseq=4,rtrep=.f.,;
    cFormVar = "w_CONUMERO", cQueryName = "CONUMERO",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice contratto da creare o modificare",;
    HelpContextID = 88067189,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=143, Top=74, InputMask=replicate('X',15)

  add object oCODESCON_1_5 as StdField with uid="FBSGYTBHCU",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODESCON", cQueryName = "CODESCON",;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 208720780,;
   bGlobalFont=.t.,;
    Height=21, Width=347, Left=268, Top=74, InputMask=replicate('X',50)

  add object oCODATCON_1_6 as StdField with uid="YZXZHQUXAG",rtseq=6,rtrep=.f.,;
    cFormVar = "w_CODATCON", cQueryName = "CODATCON",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data contratto",;
    HelpContextID = 207934348,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=143, Top=100

  add object oCODATINI_1_7 as StdField with uid="RGFOHMIJDB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODATINI", cQueryName = "CODATINI",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit� contratto",;
    HelpContextID = 107271057,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=374, Top=100

  add object oCODATFIN_1_8 as StdField with uid="HUNVFNESTE",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODATFIN", cQueryName = "CODATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data di inizio validit� � maggiore della data finale",;
    ToolTipText = "Data di fine validit� contratto",;
    HelpContextID = 110832756,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=507, Top=100

  func oCODATFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (NOT .w_CODATFIN<.w_CODATINI)
    endwith
    return bRes
  endfunc

  add object oCOCODCLF_1_9 as StdField with uid="LBAKIBAWTB",rtseq=9,rtrep=.f.,;
    cFormVar = "w_COCODCLF", cQueryName = "COCODCLF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice fornitore",;
    HelpContextID = 223798164,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=143, Top=126, cSayPict="p_FOR", cGetPict="p_FOR", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_AFR", oKey_1_1="ANTIPCON", oKey_1_2="this.w_COTIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_COCODCLF"

  func oCOCODCLF_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_9('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODCLF_1_9.ecpDrop(oSource)
    this.Parent.oContained.link_1_9('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODCLF_1_9.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_COTIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_COTIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCOCODCLF_1_9'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AFR',"Elenco fornitori",'',this.parent.oContained
  endproc
  proc oCOCODCLF_1_9.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AFR()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_COTIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_COCODCLF
     i_obj.ecpSave()
  endproc

  add object oCOAFFIDA_1_10 as StdField with uid="UQETRWRHFF",rtseq=10,rtrep=.f.,;
    cFormVar = "w_COAFFIDA", cQueryName = "COAFFIDA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Livello di affidabilit� del fornitore (da 0 a 9)",;
    HelpContextID = 146799719,;
   bGlobalFont=.t.,;
    Height=21, Width=20, Left=374, Top=126, cSayPict='"9"', cGetPict='"9"'

  add object oCOCODVAL_1_12 as StdField with uid="MKWAMSENYY",rtseq=12,rtrep=.f.,;
    cFormVar = "w_COCODVAL", cQueryName = "COCODVAL",;
    bObbl = .t. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    sErrorMsg = "Valuta inesistente o incongruente o obsoleta",;
    ToolTipText = "Codice valuta del nuovo contratto",;
    HelpContextID = 94968946,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=143, Top=152, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_COCODVAL"

  func oCOCODVAL_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_12('Part',this)
    endwith
    return bRes
  endfunc

  proc oCOCODVAL_1_12.ecpDrop(oSource)
    this.Parent.oContained.link_1_12('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCOCODVAL_1_12.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oCOCODVAL_1_12'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"",'VALUZOOM.VALUTE_VZM',this.parent.oContained
  endproc
  proc oCOCODVAL_1_12.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_COCODVAL
     i_obj.ecpSave()
  endproc

  add object oCAMBIO2_1_13 as StdField with uid="PUHCPMYNXD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_CAMBIO2", cQueryName = "CAMBIO2",;
    bObbl = .t. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Cambio inseribile se non in valuta EMU",;
    HelpContextID = 250392102,;
   bGlobalFont=.t.,;
    Height=21, Width=97, Left=374, Top=152, cSayPict='"99999.999999"', cGetPict='"99999.999999"'

  add object oRICPE1_1_14 as StdField with uid="JCWLSRROBT",rtseq=14,rtrep=.f.,;
    cFormVar = "w_RICPE1", cQueryName = "RICPE1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di ricalcolo del contratto",;
    HelpContextID = 256239850,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=133, Top=216, cSayPict='"999.99"', cGetPict='"999.99"'

  func oRICPE1_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RICVA1=0)
    endwith
   endif
  endfunc

  add object oRICVA1_1_15 as StdField with uid="RAFGTLVTMZ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_RICVA1", cQueryName = "RICVA1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Ricalcolo in valore del contratto",;
    HelpContextID = 260040938,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=133, Top=241, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oRICVA1_1_15.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_RICPE1=0)
    endwith
   endif
  endfunc

  add object oARROT1_1_16 as StdField with uid="RVCKWZFISF",rtseq=16,rtrep=.f.,;
    cFormVar = "w_ARROT1", cQueryName = "ARROT1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Arrotondamento al valore monetario desiderato (decine, centinaia, ecc., 0=nessun arrotondamento)",;
    HelpContextID = 240513274,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=278, Top=216, cSayPict="v_PV(33+VVL)", cGetPict="v_GV(33+VVL)"

  add object oVALORIN_1_17 as StdField with uid="MHNGXGXYBX",rtseq=17,rtrep=.f.,;
    cFormVar = "w_VALORIN", cQueryName = "VALORIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore fino al quale si vuole arrotondare",;
    HelpContextID = 108421290,;
   bGlobalFont=.t.,;
    Height=21, Width=152, Left=384, Top=216, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oARROT2_1_18 as StdField with uid="RHGJBSFYAZ",rtseq=18,rtrep=.f.,;
    cFormVar = "w_ARROT2", cQueryName = "ARROT2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Arrotondamento al valore monetario desiderato (decine, centinaia, ecc., 0=nessun arrotondamento)",;
    HelpContextID = 223736058,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=278, Top=241, cSayPict="v_PV(33+VVL)", cGetPict="v_GV(33+VVL)"

  func oARROT2_1_18.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORIN<>0)
    endwith
   endif
  endfunc

  add object oVALOR2IN_1_19 as StdField with uid="NMSIQUJYRO",rtseq=19,rtrep=.f.,;
    cFormVar = "w_VALOR2IN", cQueryName = "VALOR2IN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore fino al quale si vuole arrotondare",;
    HelpContextID = 42573732,;
   bGlobalFont=.t.,;
    Height=21, Width=152, Left=384, Top=241, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oVALOR2IN_1_19.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORIN<>0)
    endwith
   endif
  endfunc

  func oVALOR2IN_1_19.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VALOR2IN>.w_VALORIN)
    endwith
    return bRes
  endfunc

  add object oARROT3_1_20 as StdField with uid="HXTPQSAAPP",rtseq=20,rtrep=.f.,;
    cFormVar = "w_ARROT3", cQueryName = "ARROT3",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Arrotondamento al valore monetario desiderato (decine, centinaia, ecc., 0=nessun arrotondamento)",;
    HelpContextID = 206958842,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=278, Top=266, cSayPict="v_PV(33+VVL)", cGetPict="v_GV(33+VVL)"

  func oARROT3_1_20.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORIN<>0 AND .w_VALOR2IN<>0)
    endwith
   endif
  endfunc

  add object oVALOR3IN_1_21 as StdField with uid="GVJHJMJPXN",rtseq=21,rtrep=.f.,;
    cFormVar = "w_VALOR3IN", cQueryName = "VALOR3IN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Valore fino al quale si vuole arrotondare",;
    HelpContextID = 59350948,;
   bGlobalFont=.t.,;
    Height=21, Width=152, Left=384, Top=266, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  func oVALOR3IN_1_21.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VALORIN<>0 AND .w_VALOR2IN<>0)
    endwith
   endif
  endfunc

  func oVALOR3IN_1_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_VALOR3IN>.w_VALORIN AND .w_VALOR3IN>.w_VALOR2IN)
    endwith
    return bRes
  endfunc

  add object oARROT4_1_22 as StdField with uid="RPOAXMKGRV",rtseq=22,rtrep=.f.,;
    cFormVar = "w_ARROT4", cQueryName = "ARROT4",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Arrotondamento per importi diversi dai precedenti (0 = nessun arrotondamento)",;
    HelpContextID = 190181626,;
   bGlobalFont=.t.,;
    Height=21, Width=93, Left=278, Top=291, cSayPict="v_PV(33+VVL)", cGetPict="v_GV(33+VVL)"

  add object oVALOFIN_1_23 as StdField with uid="ZZMRLWXAIF",rtseq=23,rtrep=.f.,;
    cFormVar = "w_VALOFIN", cQueryName = "VALOFIN",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    HelpContextID = 121004202,;
   bGlobalFont=.t.,;
    Height=21, Width=128, Left=522, Top=291, cSayPict="v_PV(40+VVL)", cGetPict="v_GV(40+VVL)"

  add object oCODESCRI_1_27 as StdField with uid="VHVFQRRGPI",rtseq=27,rtrep=.f.,;
    cFormVar = "w_CODESCRI", cQueryName = "CODESCRI",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 59714671,;
   bGlobalFont=.t.,;
    Height=21, Width=346, Left=269, Top=14, InputMask=replicate('X',50)


  add object oBtn_1_37 as StdButton with uid="YSRFFHTDMW",left=552, top=321, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per eseguire la duplicazione\modifica contratto";
    , HelpContextID = 82442010;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_37.Click()
      with this.Parent.oContained
        do GSVE_BCV with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_38 as StdButton with uid="OJYROTBZFC",left=602, top=321, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 75153338;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_38.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSIMVAL_1_67 as StdField with uid="ORVCZTYUDA",rtseq=52,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 75450586,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=199, Top=40, InputMask=replicate('X',5)

  add object oSIMVAL2_1_68 as StdField with uid="CCIFSGAAZH",rtseq=53,rtrep=.f.,;
    cFormVar = "w_SIMVAL2", cQueryName = "SIMVAL2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 192984870,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=193, Top=152, InputMask=replicate('X',5)

  add object oStr_1_32 as StdString with uid="GPCTDMFHWZ",Visible=.t., Left=6, Top=14,;
    Alignment=1, Width=141, Height=18,;
    Caption="Contratto di riferimento:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_33 as StdString with uid="GOPKQRRHJT",Visible=.t., Left=6, Top=187,;
    Alignment=0, Width=252, Height=15,;
    Caption="Parametri di calcolo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_34 as StdString with uid="NYHQXBQHJL",Visible=.t., Left=3, Top=216,;
    Alignment=1, Width=128, Height=15,;
    Caption="Percentuale ricalcolo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_35 as StdString with uid="SVPRRTBRIR",Visible=.t., Left=3, Top=241,;
    Alignment=1, Width=128, Height=15,;
    Caption="Ricalcolo in valore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_36 as StdString with uid="JEFECLEULP",Visible=.t., Left=278, Top=187,;
    Alignment=2, Width=104, Height=15,;
    Caption="Arrotondamenti"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_40 as StdString with uid="MHEWZFMOQE",Visible=.t., Left=387, Top=187,;
    Alignment=0, Width=146, Height=15,;
    Caption="Per importi fino a"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_41 as StdString with uid="HFEZTPBADH",Visible=.t., Left=376, Top=291,;
    Alignment=1, Width=143, Height=15,;
    Caption="Per importi superiori a:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_46 as StdString with uid="TSBYRTJPCN",Visible=.t., Left=38, Top=76,;
    Alignment=1, Width=103, Height=18,;
    Caption="Nuovo contratto:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_47 as StdString with uid="ALBXKDEAND",Visible=.t., Left=38, Top=126,;
    Alignment=1, Width=103, Height=18,;
    Caption="Fornitore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_48 as StdString with uid="ZHBYKPWMPC",Visible=.t., Left=38, Top=152,;
    Alignment=1, Width=103, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="ADEDAMJECR",Visible=.t., Left=298, Top=100,;
    Alignment=1, Width=74, Height=15,;
    Caption="Valido dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_50 as StdString with uid="BDIUCGIEVT",Visible=.t., Left=482, Top=100,;
    Alignment=1, Width=23, Height=15,;
    Caption="al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_51 as StdString with uid="QQFDPBJRFZ",Visible=.t., Left=38, Top=100,;
    Alignment=1, Width=103, Height=18,;
    Caption="Data:"  ;
  , bGlobalFont=.t.

  add object oStr_1_55 as StdString with uid="HJLYRUHKPK",Visible=.t., Left=307, Top=40,;
    Alignment=1, Width=65, Height=18,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_56 as StdString with uid="SOEHYVDHCC",Visible=.t., Left=307, Top=152,;
    Alignment=1, Width=65, Height=18,;
    Caption="Cambio:"  ;
  , bGlobalFont=.t.

  add object oStr_1_57 as StdString with uid="ZVDEJYIIKT",Visible=.t., Left=86, Top=40,;
    Alignment=1, Width=61, Height=18,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_1_59 as StdString with uid="TWZTBQTVLQ",Visible=.t., Left=268, Top=126,;
    Alignment=1, Width=104, Height=18,;
    Caption="Affidabilit�:"  ;
  , bGlobalFont=.t.

  add object oBox_1_39 as StdBox with uid="LFYMCQCNFX",left=2, top=204, width=649,height=2

  add object oBox_1_52 as StdBox with uid="WGRPGCHLBP",left=0, top=66, width=649,height=2

  add object oBox_1_53 as StdBox with uid="QIRMMQGHWV",left=269, top=184, width=2,height=183

  add object oBox_1_58 as StdBox with uid="VDCXXBPQXD",left=3, top=182, width=648,height=2
enddefine
define class tgsac_kdcPag2 as StdContainer
  Width  = 652
  height = 367
  stdWidth  = 652
  stdheight = 367
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oARTINI_2_1 as StdField with uid="HMGALVAKSP",rtseq=36,rtrep=.f.,;
    cFormVar = "w_ARTINI", cQueryName = "ARTINI",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale superiore al codice finale oppure obsoleto",;
    ToolTipText = "Codice articolo/servizio di inizio selezione",;
    HelpContextID = 112972026,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=88, Top=35, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_ARTINI"

  func oARTINI_2_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_1('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTINI_2_1.ecpDrop(oSource)
    this.Parent.oContained.link_2_1('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTINI_2_1.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oARTINI_2_1'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"",'',this.parent.oContained
  endproc
  proc oARTINI_2_1.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_ARTINI
     i_obj.ecpSave()
  endproc

  add object oARTFIN_2_2 as StdField with uid="URVSNOISDS",rtseq=37,rtrep=.f.,;
    cFormVar = "w_ARTFIN", cQueryName = "ARTFIN",;
    bObbl = .f. , nPag = 2, value=space(20), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale superiore al codice finale oppure obsoleto",;
    ToolTipText = "Codice articolo/servizio di fine selezione",;
    HelpContextID = 34525434,;
   bGlobalFont=.t.,;
    Height=21, Width=153, Left=88, Top=61, InputMask=replicate('X',20), bHasZoom = .t. , cLinkFile="ART_ICOL", cZoomOnZoom="GSMA_BZA", oKey_1_1="ARCODART", oKey_1_2="this.w_ARTFIN"

  func oARTFIN_2_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_2('Part',this)
    endwith
    return bRes
  endfunc

  proc oARTFIN_2_2.ecpDrop(oSource)
    this.Parent.oContained.link_2_2('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oARTFIN_2_2.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'ART_ICOL','*','ARCODART',cp_AbsName(this.parent,'oARTFIN_2_2'),iif(empty(i_cWhere),.f.,i_cWhere),'GSMA_BZA',"",'',this.parent.oContained
  endproc
  proc oARTFIN_2_2.mZoomOnZoom
    local i_obj
    i_obj=GSMA_BZA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_ARCODART=this.parent.oContained.w_ARTFIN
     i_obj.ecpSave()
  endproc

  add object oGRUMER_2_3 as StdField with uid="GMEHYOKZIG",rtseq=38,rtrep=.f.,;
    cFormVar = "w_GRUMER", cQueryName = "GRUMER",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale superiore al codice finale oppure errato",;
    ToolTipText = "Codice gruppo merc. di inizio selezione",;
    HelpContextID = 239583386,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=122, Top=116, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUMER"

  func oGRUMER_2_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_3('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUMER_2_3.ecpDrop(oSource)
    this.Parent.oContained.link_2_3('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUMER_2_3.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUMER_2_3'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"",'',this.parent.oContained
  endproc
  proc oGRUMER_2_3.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_GRUMER
     i_obj.ecpSave()
  endproc

  add object oGRUMER1_2_4 as StdField with uid="IAFJIBBWDR",rtseq=39,rtrep=.f.,;
    cFormVar = "w_GRUMER1", cQueryName = "GRUMER1",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice iniziale superiore al codice finale oppure errato",;
    ToolTipText = "Codice gruppo merc. di fine selezione",;
    HelpContextID = 28852070,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=122, Top=143, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="GRUMERC", cZoomOnZoom="GSAR_AGM", oKey_1_1="GMCODICE", oKey_1_2="this.w_GRUMER1"

  func oGRUMER1_2_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_4('Part',this)
    endwith
    return bRes
  endfunc

  proc oGRUMER1_2_4.ecpDrop(oSource)
    this.Parent.oContained.link_2_4('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oGRUMER1_2_4.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'GRUMERC','*','GMCODICE',cp_AbsName(this.parent,'oGRUMER1_2_4'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGM',"",'',this.parent.oContained
  endproc
  proc oGRUMER1_2_4.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGM()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_GMCODICE=this.parent.oContained.w_GRUMER1
     i_obj.ecpSave()
  endproc

  add object oDESINI_2_7 as StdField with uid="ZPVCFMBQDL",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DESINI", cQueryName = "DESINI",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 112979402,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=249, Top=35, InputMask=replicate('X',40)

  add object oDESFIN_2_8 as StdField with uid="XTURFUOWYR",rtseq=41,rtrep=.f.,;
    cFormVar = "w_DESFIN", cQueryName = "DESFIN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 34532810,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=249, Top=61, InputMask=replicate('X',40)

  add object oGDESC_2_9 as StdField with uid="OXDFLLORMT",rtseq=42,rtrep=.f.,;
    cFormVar = "w_GDESC", cQueryName = "GDESC",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 6475418,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=187, Top=116, InputMask=replicate('X',35)

  add object oGDESC1_2_10 as StdField with uid="ABIYHFEEVG",rtseq=43,rtrep=.f.,;
    cFormVar = "w_GDESC1", cQueryName = "GDESC1",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 258133658,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=188, Top=143, InputMask=replicate('X',35)

  add object oStr_2_5 as StdString with uid="VZEXXSYYLH",Visible=.t., Left=10, Top=36,;
    Alignment=1, Width=74, Height=18,;
    Caption="Da articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_6 as StdString with uid="EIKQGZWUMR",Visible=.t., Left=10, Top=62,;
    Alignment=1, Width=74, Height=18,;
    Caption="Ad articolo:"  ;
  , bGlobalFont=.t.

  add object oStr_2_11 as StdString with uid="JJUESOTAPJ",Visible=.t., Left=7, Top=117,;
    Alignment=1, Width=111, Height=18,;
    Caption="Da gruppo merc.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_12 as StdString with uid="CHNECJYBZU",Visible=.t., Left=7, Top=144,;
    Alignment=1, Width=111, Height=18,;
    Caption="A gruppo merc.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsac_kdc','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
