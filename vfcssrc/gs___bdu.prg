* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gs___bdu                                                        *
*              Delete utente                                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][39]                                                      *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-03-25                                                      *
* Last revis.: 2005-03-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pCODE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgs___bdu",oParentObject,m.pCODE)
return(i_retval)

define class tgs___bdu as StdBatch
  * --- Local variables
  pCODE = 0
  w_MESS = space(100)
  w_CODE = 0
  w_FLUTEGRP = space(1)
  * --- WorkFile variables
  POL_UTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Questo batch scatta alla cancellazione di un utente, il batch � sotto transazione
    *     per bloccare la cancellazione in caso di errore occore utilizzare una 
    *     "Transaction Error"
    * --- Parametri:
    *     pCODE = Codice utente da cancellare
    this.w_CODE = this.pCODE
    * --- Cancello le politiche di sicurezza legate all'utente
    * --- Try
    local bErr_00F3DDE8
    bErr_00F3DDE8=bTrsErr
    this.Try_00F3DDE8()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_MESS = AH_MSGFORMAT("Impossibile cancellare l'utente dalle politiche di sicurezza")
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
    bTrsErr=bTrsErr or bErr_00F3DDE8
    * --- End
    * --- Cancello le autorizzazioni query di infopublisher legate all'utente
    this.w_FLUTEGRP = "U"
    if Type ("g_IRDR") = "C" AND g_IRDR = "S"
      * --- Try
      local bErr_035E9EB8
      bErr_035E9EB8=bTrsErr
      this.Try_035E9EB8()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        this.w_MESS = AH_MSGFORMAT("Impossibile cancellare l'utente dalle autorizzazioni %1","InfoPublisher")
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=this.w_MESS
      endif
      bTrsErr=bTrsErr or bErr_035E9EB8
      * --- End
    endif
    * --- Affinch� il batch non ritorni errore "Property MCALLEDBATCHEND is not found"
    *     devo porre bUpdateParentObject a false
    this.bUpdateParentObject = .F.
  endproc
  proc Try_00F3DDE8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Delete from POL_UTE
    i_nConn=i_TableProp[this.POL_UTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.POL_UTE_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
            +"PUCODUTE = "+cp_ToStrODBC(this.pCODE);
             )
    else
      delete from (i_cTable) where;
            PUCODUTE = this.pCODE;

      i_Rows=_tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      * --- Error: delete not accepted
      i_Error=MSG_DELETE_ERROR
      return
    endif
    return
  proc Try_035E9EB8()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    do GS___BDW with this
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    return


  proc Init(oParentObject,pCODE)
    this.pCODE=pCODE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='POL_UTE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pCODE"
endproc
