* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bei                                                        *
*              Generazione elenchi INTRA                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_577]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-03-14                                                      *
* Last revis.: 2018-03-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bei",oParentObject)
return(i_retval)

define class tgsar_bei as StdBatch
  * --- Local variables
  w_APPO = space(10)
  w_PINAZDES = space(3)
  w_DECTOT = 0
  w_MVCODVAL = space(3)
  w_FLERR = .f.
  w_FLER1 = .f.
  w_NUDOC = 0
  w_PIPRODES = space(2)
  w_DECTOP = 0
  w_APPO3 = 0
  w_PISERIAL = space(10)
  w_PINAZORI = space(3)
  w_PINOMENC = space(8)
  w_PARIVA = space(12)
  w_PINUMREG = 0
  w_PIPROORI = space(2)
  w_PINATTRA = space(3)
  w_DATEMU = ctod("  /  /  ")
  w_PI__ANNO = space(4)
  w_PIRIFDOC = space(10)
  w_PIIMPNAZ = 0
  w_TOTIMPNA = 0
  w_TIPORIGA = space(1)
  w_PIDATREG = ctod("  /  /  ")
  w_PIVALNAZ = space(3)
  w_PIIMPVAL = 0
  w_PIVALSTO = 0
  w_CPROWSTA = 0
  w_PIDATDOC = ctod("  /  /  ")
  w_CPROWNUM = 0
  w_APPO1 = 0
  w_APPSTA = 0
  w_PIDATCOM = ctod("  /  /  ")
  w_CPROWORD = 0
  w_APPO2 = 0
  w_TOTSTA = 0
  w_PIPERRET = 0
  w_ANNAZION = space(3)
  w_CAONAZ = 0
  w_TRIGAPP = space(1)
  w_PIANNRET = space(4)
  w_PITIPPER = space(1)
  w_ANPROVIN = space(2)
  w_NOTIPR = .f.
  w_CCTIPDOC = space(2)
  w_PITIPCON = space(1)
  w_OSERIAL = space(10)
  w_PERIODO = space(1)
  w_PITIPMOV = space(2)
  w_PICODCON = space(15)
  w_PINAZPRO = space(3)
  w_NOFORF = .f.
  w_PITIPRET = space(1)
  w_PIVALSTA = 0
  w_TRAINT = 0
  w_DECQT = 0
  w_DTOBBL = space(1)
  w_PINUMDOC = 0
  w_PIQTASUP = 0
  w_TRADET = 0
  w_UNIMIS = space(3)
  w_PIALFDOC = space(10)
  w_PIMASNET = 0
  w_TOTIMP = 0
  w_PIVALORI = space(3)
  w_PINAZORI = space(3)
  w_RESTO = 0
  w_PICONDCO = space(1)
  w_NUREC = 0
  w_PIMODTRA = space(3)
  w_MVCLADOC = space(2)
  w_SPEACC = 0
  w_SPEACC1 = 0
  w_SPEDET = 0
  w_TOTNAZ = 0
  w_MVSPEINC = 0
  w_MVSPETRA = 0
  w_MVSPEIMB = 0
  w_MVSPEBOL = 0
  w_MVFLOMAG = space(1)
  w_MVFLTRAS = space(1)
  w_REST1 = 0
  w_UMSUPP = space(3)
  w_ROWORD = 0
  w_OK = .f.
  w_SEROLD = space(10)
  w_PREC = 0
  w_TESTTRAS = 0
  w_PIPORDES = space(10)
  w_ANCODPOR = space(10)
  w_PIPORORI = space(10)
  w_AZCODPOR = space(10)
  w_DDCODNAZ = space(3)
  w_DDPROVIN = space(2)
  w_CODISO = space(3)
  w_PIUTISER = space(1)
  w_NUMRIGA = 0
  w_PIPERSER = space(1)
  w_PIMODINC = space(1)
  w_PIQTASUP = 0
  w_PIMASNET = 0
  w_PIPAEPAG = space(3)
  w_NUMBENI = 0
  w_INSERTED = .f.
  w_PIROWORD = 0
  w_AITIPE = space(1)
  w_AITIPV = space(1)
  w_AITIPS = space(1)
  w_PERIODO_S = space(1)
  w_AITIAS = space(1)
  w_oERRORLOG = .NULL.
  w_oMess = .NULL.
  w_oPart = .NULL.
  w_KODOC = 0
  w_MESS = space(200)
  w_CONTA = 0
  w_TOTIMPACC = 0
  w_IMPACC = 0
  * --- WorkFile variables
  ELEIMAST_idx=0
  ELEIDETT_idx=0
  NAZIONI_idx=0
  VALUTE_idx=0
  DOC_MAST_idx=0
  CONTI_idx=0
  AZIENDA_idx=0
  NOMENCLA_idx=0
  TMPVEND1_idx=0
  AZDATINT_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Parametro Esegue: I = Non Spunta Documenti; D = Spunta Documenti
    * --- Generazione Elenchi INTRA (Acquisti o Cessioni)
    * --- Parametri dalla Maschera
    * --- La variabile w_Gen_Serv viene utilizzata all'interno della query Gsve_Qci. 
    *          ... Se viene valorizzata ad 'S' la query Gsve_Qci verifica se esistono documenti che hanno gi� generato
    *          ... INTRA ma hanno righe di servizi con la combo Utilizzo intra valorizzata a
    *          ... "Servizi" che non hanno generato INTRA
    * --- Legge Dati del Documento per INTRA
    ah_Msg("Ricerca documenti da generare...",.T.)
    * --- Create temporary table TMPVEND1
    i_nIdx=cp_AddTableDef('TMPVEND1') && aggiunge la definizione nella lista delle tabelle
    i_cTempTable=i_TableProp[i_nIdx,2] && recupera il nome assegnato
    vq_exec('QUERY\GSVE0QCI',this,.null.,'',.f.,.t.,.f.,.f.,i_cTempTable,.f.,.f.)
    this.TMPVEND1_idx=i_nIdx
    i_TableProp[i_nIdx,4]=1  && segna come usato 1 volta
    * --- Write into TMPVEND1
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.TMPVEND1_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.TMPVEND1_idx,2])
    if i_nConn<>0
      local i_cQueryTable,i_cWhere,i_aIndex,i_cDB
      declare i_aIndex[1]
      i_cQueryTable=cp_getTempTableName(i_nConn)
      i_aIndex(1)="MVSERIAL"
      do vq_exec with 'GSVE3QCI',this,.null.,'',.f.,.t.,.f.,.f.,i_cQueryTable,i_aIndex
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.TMPVEND1_idx,i_nConn)	
      i_cDB=cp_GetDatabaseType(i_nConn)
      do case
      case i_cDB="SQLServer"
        i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTIMPNA = _t2.TOTIMPNA ";
          +",TOTIMPACC = _t2.TOTIMPACC";
          +",MVSERIAL1 = _t2.MVSERIAL";
          +i_ccchkf;
          +" from "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 where "+i_cWhere)
      case i_cDB="MySQL"
        i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1, "+i_cQueryTable+" _t2 set ";
          +"TMPVEND1.TOTIMPNA = _t2.TOTIMPNA ";
          +",TMPVEND1.TOTIMPACC = _t2.TOTIMPACC";
          +",TMPVEND1.MVSERIAL1 = _t2.MVSERIAL";
          +Iif(Empty(i_ccchkf),"",",TMPVEND1.cpccchk="+cp_ToStrODBC(cp_NewCCChk()));
          +" where "+i_cWhere)
      case i_cDB="Oracle"
        i_cWhere="TMPVEND1.MVSERIAL = t2.MVSERIAL";
        
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set (";
          +"TOTIMPNA,";
          +"TOTIMPACC,";
          +"MVSERIAL1";
          +iif(Not Empty(i_ccchkf),",cpccchk","")+") = (select ";
          +"t2.TOTIMPNA ,";
          +"t2.TOTIMPACC,";
          +"t2.MVSERIAL";
          +iif(Not Empty(i_ccchkf),","+cp_ToStrODBC(cp_NewCCChk()),"")+" from "+i_cQueryTable+" t2 where "+i_cWhere+")";
          +" where exists(select 1 from "+i_cQueryTable+" t2 where "+i_cWhere+")")
      case i_cDB="PostgreSQL"
        i_cWhere="TMPVEND1.MVSERIAL = _t2.MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" TMPVEND1 set ";
          +"TOTIMPNA = _t2.TOTIMPNA ";
          +",TOTIMPACC = _t2.TOTIMPACC";
          +",MVSERIAL1 = _t2.MVSERIAL";
          +i_ccchkf;
          +" from "+i_cQueryTable+" _t2 where "+i_cWhere)
      otherwise
        i_cWhere=i_cTable+".MVSERIAL = "+i_cQueryTable+".MVSERIAL";
    
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"TOTIMPNA = (select TOTIMPNA  from "+i_cQueryTable+" where "+i_cWhere+")";
          +",TOTIMPACC = (select TOTIMPACC from "+i_cQueryTable+" where "+i_cWhere+")";
          +",MVSERIAL1 = (select MVSERIAL from "+i_cQueryTable+" where "+i_cWhere+")";
          +i_ccchkf;
          +" where exists(select 1 from "+i_cQueryTable+" where "+i_cWhere+")")
      endcase
      cp_DropTempTable(i_nConn,i_cQueryTable)
    else
      error "not yet implemented!"
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    vq_exec("query\GSVE_QCI.VQR",this,"DOCUINTR")
    * --- Drop temporary table TMPVEND1
    i_nIdx=cp_GetTableDefIdx('TMPVEND1')
    if i_nIdx<>0
      cp_DropTempTable(i_TableProp[i_nIdx,3],i_TableProp[i_nIdx,2])
      cp_RemoveTableDef('TMPVEND1')
    endif
    if USED("DOCUINTR")
      this.w_DTOBBL = "N"
      * --- Read from AZDATINT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZDATINT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZDATINT_idx,2],.t.,this.AZDATINT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ITAITIPE,ITAITIPV,ITAITIPS,ITAITIAS"+;
          " from "+i_cTable+" AZDATINT where ";
              +"ITCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ITAITIPE,ITAITIPV,ITAITIPS,ITAITIAS;
          from (i_cTable) where;
              ITCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_AITIPE = NVL(cp_ToDate(_read_.ITAITIPE),cp_NullValue(_read_.ITAITIPE))
        this.w_AITIPV = NVL(cp_ToDate(_read_.ITAITIPV),cp_NullValue(_read_.ITAITIPV))
        this.w_AITIPS = NVL(cp_ToDate(_read_.ITAITIPS),cp_NullValue(_read_.ITAITIPS))
        this.w_AITIAS = NVL(cp_ToDate(_read_.ITAITIAS),cp_NullValue(_read_.ITAITIAS))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZPERPQT,AZDTOBBL,AZCODPOR"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(i_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZPERPQT,AZDTOBBL,AZCODPOR;
          from (i_cTable) where;
              AZCODAZI = i_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_DECQT = NVL(cp_ToDate(_read_.AZPERPQT),cp_NullValue(_read_.AZPERPQT))
        this.w_DTOBBL = NVL(cp_ToDate(_read_.AZDTOBBL),cp_NullValue(_read_.AZDTOBBL))
        this.w_AZCODPOR = NVL(cp_ToDate(_read_.AZCODPOR),cp_NullValue(_read_.AZCODPOR))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- controllo periodicit� intra
      if this.oParentObject.w_FLVEAC ="V"
        this.w_PERIODO = this.w_AITIPE
        this.w_PERIODO_S = this.w_AITIPS
      else
        this.w_PERIODO = this.w_AITIPV
        this.w_PERIODO_S = this.w_AITIAS
      endif
      * --- Leggo i decimali per le quantit�
      * --- Stanzia l'oggetto per la gestione delle messaggistiche di errore
      this.w_oERRORLOG=createobject("AH_ErrorLog")
      * --- Oggetto per messaggi incrementali
      this.w_oMess=createobject("Ah_Message")
      this.w_NUDOC = 0
      this.w_KODOC = 0
      if RECCOUNT("DOCUINTR") > 0
        * --- Inizializza Messaggistica di Errore
        this.w_FLERR = .F.
        ah_Msg("Inizio fase di generazione...",.T.)
         
 SELECT MVSERIAL, MAX(MVDATDOC) AS DATDOC, SUM(IIF (MVTIPRIG="F",0,ABS(PIVALSTA))) AS TOTIMP FROM DOCUINTR ; 
 WHERE NOT EMPTY(NVL(MVSERIAL,"")) AND (NVL(MVFLTRAS,"") <> "S" or NVL(ARUTISER,"N") = "S") GROUP BY MVSERIAL ; 
 ORDER BY DATDOC INTO CURSOR MASTINTR
        GO TOP
        SCAN
        * --- Try
        local bErr_04A772D0
        bErr_04A772D0=bTrsErr
        this.Try_04A772D0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          this.w_oERRORLOG.AddMsgLog("Errore %1 comando %2 durante la generazione del movimento INTRA", Message(), Message(1))     
        endif
        bTrsErr=bTrsErr or bErr_04A772D0
        * --- End
        SELECT MASTINTR
        ENDSCAN
        * --- Rimuove Cursore
         
 Select MASTINTR 
 Use
      endif
      * --- Verifico se esistono documenti generalizzabili non contabilizzati / confermati
      *     (Considera anche documenti contabilizzati ma senza attivo il flag FLINTR)
      * --- Select from GSVE_BEI
      do vq_exec with 'GSVE_BEI',this,'_Curs_GSVE_BEI','',.f.,.t.
      if used('_Curs_GSVE_BEI')
        select _Curs_GSVE_BEI
        locate for 1=1
        do while not(eof())
        this.w_CONTA = Nvl( _Curs_GSVE_BEI.CONTA , 0 )
          select _Curs_GSVE_BEI
          continue
        enddo
        use
      endif
      if this.w_NUDOC + this.w_CONTA= 0
        this.w_oMess.AddMsgPartNL("Per l'intervallo selezionato non esistono documenti da generare")     
      else
        * --- Escludo i documenti non generati...
        this.w_CONTA = this.w_CONTA - this.w_KODOC
        this.w_oPart = this.w_oMess.AddMsgPartNL("Operazione completata%0N.%1 documenti generati")
        this.w_oPart.AddParam(ALLTRIM(STR(this.w_NUDOC)))     
        if this.w_KODOC>0
          this.w_oPart = this.w_oMess.AddMsgPartNL("N.%1 documenti respinti (vedi stampa)")
          this.w_oPart.AddParam(ALLTRIM(STR(this.w_KODOC)))     
        endif
        if this.w_CONTA>0
          if g_COGE="S"
            this.w_oPart = this.w_oMess.AddMsgPartNL("Eseguire prima la contabilizzazione di %1 documenti")
            this.w_oPart.AddParam(Alltrim(Str(this.w_CONTA)))     
          else
            this.w_oPart = this.w_oMess.AddMsgPartNL("Eseguire prima la conferma di %1 documenti")
            this.w_oPart.AddParam(Alltrim(Str(this.w_CONTA)))     
          endif
        endif
      endif
      this.w_oMess.Ah_ErrorMsg()     
      this.w_oERRORLOG.PrintLog(this.oParentObject,"Errori riscontrati")     
      * --- Rimuove Cursore
       
 Select DOCUINTR 
 Use
    endif
  endproc
  proc Try_04A772D0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    this.w_NOTIPR = .t.
    this.w_PIRIFDOC = MASTINTR.MVSERIAL
    this.w_TOTIMP = NVL(MASTINTR.TOTIMP,0)
    ah_Msg("Documenti del: %1",.T.,.F.,.F., DTOC(CP_TODATE(MASTINTR.DATDOC)) )
    this.Page_2()
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Se periodo mensile considero anche le righe con importo in valuta nazionale uguale a zero
    *     e non nazione SAN MARINO
    this.w_OSERIAL = "DFSDFSD"
    * --- Calcolo il numero di record da considerare ai fini della ripartizione delle spese di confine.
    SELECT MVSERIAL FROM DOCUINTR ;
    WHERE NVL(MVSERIAL,"")=this.w_PIRIFDOC AND (NVL(PIIMPNAZ,0)<>0 OR (NVL(PIMASNET,0)<>0 AND this.w_PERIODO="M" AND NVL(NACODISO,"   ")<>"SM") AND NVL(ARUTISER,"N") = "N") ;
    INTO CURSOR TEMPOR
    SELECT TEMPOR
    this.w_NUREC = RECCOUNT()
    USE
    SELECT count(*) as NUMREC FROM DOCUINTR ;
    WHERE NVL(MVSERIAL,"")=this.w_PIRIFDOC AND (NVL(PIIMPNAZ,0)<>0 OR (NVL(PIMASNET,0)<>0 AND this.w_PERIODO="M" AND NVL(NACODISO,"   ")<>"SM") AND NVL(ARUTISER,"N") = "N") AND MVFLTRAS<>"S" ;
    INTO ARRAY NUMRECBENI
    if TYPE("NUMRECBENI(1)") = "N"
      this.w_NUMBENI = NUMRECBENI(1)
    else
      this.w_NUMBENI = 0
    endif
    Release NUMRECBENI
    this.w_SEROLD = "XZXXZXZX"
    * --- Verifica se manca l'importo o la massa netta
    SELECT MVSERIAL, MVNUMDOC, MVALFDOC, MVDATREG, CPROWORD FROM DOCUINTR ;
    WHERE NVL(PIIMPNAZ, 0)=0 AND (NVL(PIMASNET,0)=0 OR (IIF(NVL(MVFLTRAS," ") <> "S",this.w_PERIODO,this.w_PERIODO_S)<>"M")) AND (NVL(MVFLTRAS," ") <> "S" or NVL(ARUTISER,"N") = "S");
    INTO CURSOR TEMPOR
    SELECT TEMPOR
    if RECCOUNT()>0
      GO TOP
      SCAN FOR NOT EMPTY(NVL(MVSERIAL,""))
      if NVL(MVSERIAL,SPACE(10))<>this.w_SEROLD
        this.w_OK = .T.
      endif
      this.w_SEROLD = NVL(MVSERIAL,SPACE(10))
      this.w_PIDATREG = CP_TODATE(MVDATREG)
      this.w_PINUMDOC = NVL(MVNUMDOC, 0)
      this.w_PIALFDOC = NVL(MVALFDOC, Space(10))
      this.w_ROWORD = NVL(CPROWORD,0)
      if this.w_OK=.T.
        this.w_oERRORLOG.AddMsgLog("Verificare documento: del %1 n. %2", DTOC(this.w_PIDATREG), ALLTR(STR(this.w_PINUMDOC,15))+" "+Alltrim(this.w_PIALFDOC))     
        this.w_KODOC = this.w_KODOC + 1
      endif
      this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "Riga %1 manca l'importo in valuta nazionale o la massa netta" , ALLTRIM(STR(this.w_ROWORD)))     
      this.w_OK = .F.
      SELECT TEMPOR
      ENDSCAN
      * --- Procedo alla cancellazione dei documenti non coerenti alle specifiche richieste
      DELETE FROM DOCUINTR WHERE MVSERIAL IN (SELECT MVSERIAL FROM TEMPOR)
    endif
    USE
    * --- Se il Periodo � trimestrale verifico se esistono documenti con soli articoli 
    *     con flag "Manutenzione elenchi INTRA" uguale a solo dati statistici
    this.w_NOFORF = .f.
    if this.w_PERIODO <> "M"
      SELECT MVSERIAL FROM DOCUINTR ;
       WHERE NVL(MVSERIAL,"")=this.w_PIRIFDOC AND MVFLTRAS <> "Z" ;
      INTO CURSOR FORFET
      SELECT FORFET
      if RECCOUNT() = 0
        this.w_NOFORF = .t.
      endif
      USE
    endif
    this.w_FLER1 = .F.
    this.w_REST1 = 0
    this.w_SPEACC = 0
    this.w_PIUTISER = "X"
    SELECT DOCUINTR
    GO TOP
    SCAN FOR NVL(MVSERIAL,"")=this.w_PIRIFDOC AND ((MVFLTRAS = "S" and NVL(ARUTISER,"N") = "N") OR NVL(PIIMPNAZ,0)<>0 OR (NVL(PIMASNET,0)<>0 AND IIF(NVL(ARUTISER,"N") = "N",this.w_PERIODO,this.w_PERIODO_S)="M")) AND NOT this.w_NOFORF AND NOT DELETED()
    if this.w_OSERIAL<>this.w_PIRIFDOC or DOCUINTR.ARUTISER<>this.w_PIUTISER
      this.w_INSERTED = .F.
      * --- Rinizializzo la variabile FLERR al variare del serial del documento.
      if this.w_OSERIAL<>this.w_PIRIFDOC 
        this.w_FLERR = .F.
      endif
      * --- Calcola PISERIAL, PINUMREG, PI__ANNO
      this.w_PISERIAL = SPACE(10)
      this.w_PINUMREG = 0
      this.w_CPROWNUM = 0
      this.w_CPROWORD = 0
      this.w_PREC = 0
      this.w_PIDATREG = CP_TODATE(MVDATREG)
      this.w_PIDATDOC = CP_TODATE(MVDATDOC)
      this.w_PINUMDOC = NVL(MVNUMDOC, 0)
      this.w_PIALFDOC = NVL(MVALFDOC, Space(10))
      if this.oParentObject.w_FLVEAC="V"
        this.w_PIDATCOM = CP_TODATE(NVL(MVDATDOC, this.w_PIDATREG))
      else
        this.w_PIDATCOM = CP_TODATE(NVL(MVDATREG, this.w_PIDATREG))
      endif
      this.w_PI__ANNO = STR(YEAR(this.w_PIDATCOM), 4, 0)
      this.w_PIPERRET = NVL(MVPERRET, 0)
      this.w_PIANNRET = NVL(MVANNRET, SPACE(4))
      this.w_PITIPPER = NVL(MVTIPPER, " ")
      this.w_CCTIPDOC = NVL(CCTIPDOC, "  ")
      this.w_MVCLADOC = NVL(MVCLADOC,SPACE(2))
      * --- Tipo Movimento
      if this.oParentObject.w_FLVEAC="V"
        * --- Controllo se la riga del documento che st� analizzando ha il flag "Utilizzo INTRA"  valorizzato a "Servizi"
        if DOCUINTR.ARUTISER="S"
          this.w_PITIPMOV = IIF((g_COGE<>"S" OR this.w_CCTIPDOC $ "NE-FE") AND this.w_PIPERRET<>0 AND NOT EMPTY(this.w_PIANNRET), "RS", "CS")
          * --- Se la riga del documento che st� analizzando ha il flag "Utilizzo INTRA"  valorizzato a "Servizi"sbianco i campi "Periodo e anno di rettifica"
          this.w_PIPERRET = 0
          this.w_PIANNRET = SPACE(4)
          this.w_PITIPPER = " "
        else
          this.w_PITIPMOV = IIF((g_COGE<>"S" OR this.w_CCTIPDOC $ "NE-FE") AND NOT EMPTY(this.w_PITIPPER) AND NOT EMPTY(this.w_PIANNRET), "RC", "CE")
        endif
      else
        * --- Controllo se la riga del documento che st� analizzando ha il flag "Utilizzo INTRA" valorizzato a "Servizi".
        if DOCUINTR.ARUTISER="S"
          this.w_PITIPMOV = IIF((g_COGE<>"S" OR this.w_CCTIPDOC $ "NE-FE") AND this.w_PIPERRET<>0 AND NOT EMPTY(this.w_PIANNRET), "RX", "AS")
          * --- Se la riga del documento che st� analizzando ha il flag "Utilizzo INTRA"  valorizzato a "Servizi"sbianco i campi "Periodo e anno di rettifica"
          this.w_PIPERRET = 0
          this.w_PIANNRET = SPACE(4)
          this.w_PITIPPER = " "
          this.w_PINUMDOC = NVL(MVNUMEST, 0)
          this.w_PIALFDOC = NVL(MVALFEST, Space(10))
          this.w_PIDATDOC = CP_TODATE(MVDATREG)
        else
          this.w_PITIPMOV = IIF((g_COGE<>"S" OR this.w_CCTIPDOC $ "NE-FE") AND NOT EMPTY(this.w_PITIPPER) AND NOT EMPTY(this.w_PIANNRET), "RA", "AC")
        endif
      endif
      * --- Se Rettifica
      * --- Se il doc. � di tipo NC allora rettifica di segno negativo altrimenti positivo
      if this.w_MVCLADOC="NC"
        this.w_PITIPRET = IIF(this.w_PITIPMOV $ "RC-RA", "-", " ")
      else
        this.w_PITIPRET = IIF(this.w_PITIPMOV $ "RC-RA", "+", " ")
      endif
      this.w_PINAZDES = "   "
      this.w_PIPRODES = "  "
      this.w_PINAZORI = "   "
      this.w_PINAZPRO = "   "
      this.w_PIPROORI = "  "
      this.w_PIPAEPAG = "   "
      this.w_ANNAZION = IIF(NOT EMPTY(NVL(DDCODNAZ,SPACE(2))),NVL(DDCODNAZ,SPACE(2)),NVL(ANNAZION, g_CODNAZ))
      this.w_CODISO = NVL(NACODISO,SPACE(3))
      this.w_ANPROVIN = NVL(ANPROVIN, "  ")
      this.w_DDCODNAZ = NVL( DDCODNAZ , SPACE( 3 ) )
      this.w_DDPROVIN = NVL( DDPROVIN , SPACE ( 2 ) )
      this.w_PITIPCON = NVL(MVTIPCON, " ")
      this.w_PICODCON = NVL(MVCODCON, SPACE(15))
      this.w_PARIVA = NVL(ANPARIVA, SPACE(12))
      this.w_PIPORDES = SPACE(10)
      this.w_PIPORORI = SPACE(10)
      this.w_ANCODPOR = IIF(g_ISONAZ<>"ESP",SPACE(10),NVL(ANCODPOR,SPACE(10)))
      * --- Controllo l'esistenza della parita iva
      if EMPTY(NVL(this.w_PARIVA,"")) AND this.w_OSERIAL<>this.w_PIRIFDOC
        this.w_oERRORLOG.AddMsgLog("Verificare documento: del %1 n. %2", DTOC(this.w_PIDATREG), ALLTR(STR(this.w_PINUMDOC,15))+" "+Alltrim(this.w_PIALFDOC))     
        if this.w_PITIPCON="C"
          this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "Per il cliente %1 manca la partita IVA" , ALLTRIM(this.w_PICODCON))     
        else
          this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "Per il fornitore %1 manca la partita IVA" , ALLTRIM(this.w_PICODCON))     
        endif
        SELECT DOCUINTR
        this.w_FLERR = .T.
      endif
      * --- Controllo la congruenza della tipologia conto
      if this.oParentObject.w_FLVEAC="V" AND this.w_PITIPCON="F" 
        this.w_oERRORLOG.AddMsgLog("Verificare documento del %1 seriale %2",DTOC(this.w_PIDATREG),this.w_PIRIFDOC+chr(13))     
        this.w_oERRORLOG.AddMsgLogPartNoTrans(SPACE(5),"Documento ciclo vendite intestato a fornitore")     
      endif
      if this.oParentObject.w_FLVEAC="A" AND this.w_PITIPCON="C"
        this.w_oERRORLOG.AddMsgLog("Verificare documento del %1 seriale %2",DTOC(this.w_PIDATREG),this.w_PIRIFDOC+chr(13))     
        this.w_oERRORLOG.AddMsgLogPartNoTrans(SPACE(5),"Documento ciclo acquisti intestato a cliente")     
      endif
      this.w_DECTOT = NVL(VADECTOT, 0)
      * --- Controllo se � un movimento di acquisto oppure di vendita
      if this.oParentObject.w_FLVEAC="V"
        this.w_PIVALORI = NVL(MVCODVAL, g_PERVAL)
      else
        * --- Controllo se � valorizzata la valuta del fornitore.
        if EMPTY(NVL(MVVALINT,"")) AND this.w_OSERIAL<>this.w_PIRIFDOC
          this.w_oERRORLOG.AddMsgLog("Verificare documento: del %1 n. %2", DTOC(this.w_PIDATREG), ALLTR(STR(this.w_PINUMDOC,15))+" "+Alltrim(this.w_PIALFDOC))     
          this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "Valuta del fornitore non inserita" )     
          SELECT DOCUINTR
          this.w_FLERR = .T.
        else
          this.w_PIVALORI = NVL(MVVALINT, "")
        endif
      endif
      * --- Controllo se � valorizzata la Natura Transazione
      * --- Controllo se non sono stati generati degli errori
      if this.w_FLERR=.F.
        this.w_PIVALNAZ = NVL(MVVALNAZ, g_PERVAL)
        this.w_CAONAZ = GETCAM(g_PERVAL,i_DATSYS)
        this.w_DECTOP = g_PERPVL
        * --- Se altra Valuta di Esercizio
        if this.w_PIVALNAZ<>g_PERVAL
          * --- Read from VALUTE
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.VALUTE_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "VADECTOT"+;
              " from "+i_cTable+" VALUTE where ";
                  +"VACODVAL = "+cp_ToStrODBC(this.w_PIVALNAZ);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              VADECTOT;
              from (i_cTable) where;
                  VACODVAL = this.w_PIVALNAZ;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_DECTOP = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.w_CAONAZ = GETCAM(this.w_PIVALNAZ,i_DATSYS)
          SELECT DOCUINTR
        endif
        * --- Totale Spese da Ripartire
        this.w_MVSPEINC = NVL(MVSPEINC,0)
        this.w_MVSPETRA = NVL(MVSPETRA,0)
        this.w_MVSPEIMB = NVL(MVSPEIMB,0)
        this.w_MVSPEBOL = NVL(MVSPEBOL,0)
        this.w_TOTIMPNA = NVL(TOTIMPNA,0)
        this.w_TOTIMPACC = NVL(TOTIMPACC,0)
        this.w_SPEACC = this.w_MVSPEINC+this.w_MVSPEIMB+ this.w_MVSPETRA + this.w_MVSPEBOL
        this.w_SPEACC1 = this.w_MVSPEINC+this.w_MVSPEIMB + this.w_MVSPEBOL
        if this.w_OSERIAL<>this.w_PIRIFDOC 
          this.w_REST1 = this.w_SPEACC
        endif
        this.w_TESTTRAS = NVL(TESTTRAS,0)
        * --- Trasporto INTRA
        this.w_MVCODVAL = NVL(MVCODVAL,0)
        this.w_TRAINT = NVL(MVTRAINT, 0)
        if this.w_MVCODVAL<>this.w_PIVALNAZ
          this.w_APPO1 = NVL(MVCAOVAL,1)
          if this.w_TRAINT<>0 
            this.w_TRAINT = VAL2MON(this.w_TRAINT, this.w_APPO1, this.w_CAONAZ, cp_CharToDate("01-01-99"), this.w_PIVALNAZ, this.w_DECTOT)
          endif
          if this.w_SPEACC<>0 
            this.w_SPEACC = VAL2MON(this.w_SPEACC, this.w_APPO1, this.w_CAONAZ, cp_CharToDate("01-01-99"), this.w_PIVALNAZ, this.w_DECTOT)
            this.w_SPEACC1 = VAL2MON(this.w_SPEACC1, this.w_APPO1, this.w_CAONAZ, cp_CharToDate("01-01-99"), this.w_PIVALNAZ, this.w_DECTOT)
            if this.w_OSERIAL<>this.w_PIRIFDOC 
              this.w_REST1 = this.w_SPEACC
            endif
          endif
          if this.w_TOTIMPACC<>0
            * --- Converto eventuali spese ripartite nella valuta di conto prima di detrarla..
            this.w_TOTIMPACC = VAL2MON(this.w_TOTIMPACC, this.w_APPO1, this.w_CAONAZ, cp_CharToDate("01-01-99"), this.w_PIVALNAZ, this.w_DECTOT)
          endif
          SELECT DOCUINTR
        endif
        this.w_TOTIMPNA = this.w_TOTIMPNA - this.w_TOTIMPACC
        this.w_RESTO = this.w_TRAINT
        if this.oParentObject.w_FLVEAC="V"
          * --- Valuta Originaria e' quella della Nazione Cli/For
          this.w_APPO = NVL(MVVALINT, "")
          this.w_PIVALORI = IIF(EMPTY(this.w_APPO), this.w_PIVALORI, this.w_APPO)
        endif
        * --- Aggiorna Elenchi INTRA (Se Tipo Doc. FE o NE)
        i_Conn=i_TableProp[this.ELEIMAST_IDX, 3]
        cp_NextTableProg(this, i_Conn, "SEINT", "i_codazi,w_PISERIAL")
        cp_NextTableProg(this, i_Conn, "PRINT", "i_codazi,w_PI__ANNO,w_PINUMREG")
        if DOCUINTR.ARUTISER="S" or (DOCUINTR.ARUTISER="N" and this.w_NUMBENI > 0)
          * --- Scrive il Master
          * --- Insert into ELEIMAST
          i_nConn=i_TableProp[this.ELEIMAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ELEIMAST_idx,2])
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_ccchkf=''
          i_ccchkv=''
          this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ELEIMAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                        " ("+"PISERIAL"+",PIDATREG"+",PI__ANNO"+",PINUMREG"+",PIDATCOM"+",PIDATDOC"+",PINUMDOC"+",PIALFDOC"+",PITIPMOV"+",PIPERRET"+",PIANNRET"+",PITIPCON"+",PICODCON"+",PIVALORI"+",PIVALNAZ"+",PIRIFDOC"+",PITIPPER"+i_ccchkf+") values ("+;
            cp_NullLink(cp_ToStrODBC(this.w_PISERIAL),'ELEIMAST','PISERIAL');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PIDATREG),'ELEIMAST','PIDATREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PI__ANNO),'ELEIMAST','PI__ANNO');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PINUMREG),'ELEIMAST','PINUMREG');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PIDATCOM),'ELEIMAST','PIDATCOM');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PIDATDOC),'ELEIMAST','PIDATDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PINUMDOC),'ELEIMAST','PINUMDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PIALFDOC),'ELEIMAST','PIALFDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PITIPMOV),'ELEIMAST','PITIPMOV');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PIPERRET),'ELEIMAST','PIPERRET');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PIANNRET),'ELEIMAST','PIANNRET');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PITIPCON),'ELEIMAST','PITIPCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PICODCON),'ELEIMAST','PICODCON');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PIVALORI),'ELEIMAST','PIVALORI');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PIVALNAZ),'ELEIMAST','PIVALNAZ');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PIRIFDOC),'ELEIMAST','PIRIFDOC');
            +","+cp_NullLink(cp_ToStrODBC(this.w_PITIPPER),'ELEIMAST','PITIPPER');
                 +i_ccchkv+")")
          else
            cp_CheckDeletedKey(i_cTable,0,'PISERIAL',this.w_PISERIAL,'PIDATREG',this.w_PIDATREG,'PI__ANNO',this.w_PI__ANNO,'PINUMREG',this.w_PINUMREG,'PIDATCOM',this.w_PIDATCOM,'PIDATDOC',this.w_PIDATDOC,'PINUMDOC',this.w_PINUMDOC,'PIALFDOC',this.w_PIALFDOC,'PITIPMOV',this.w_PITIPMOV,'PIPERRET',this.w_PIPERRET,'PIANNRET',this.w_PIANNRET,'PITIPCON',this.w_PITIPCON)
            insert into (i_cTable) (PISERIAL,PIDATREG,PI__ANNO,PINUMREG,PIDATCOM,PIDATDOC,PINUMDOC,PIALFDOC,PITIPMOV,PIPERRET,PIANNRET,PITIPCON,PICODCON,PIVALORI,PIVALNAZ,PIRIFDOC,PITIPPER &i_ccchkf. );
               values (;
                 this.w_PISERIAL;
                 ,this.w_PIDATREG;
                 ,this.w_PI__ANNO;
                 ,this.w_PINUMREG;
                 ,this.w_PIDATCOM;
                 ,this.w_PIDATDOC;
                 ,this.w_PINUMDOC;
                 ,this.w_PIALFDOC;
                 ,this.w_PITIPMOV;
                 ,this.w_PIPERRET;
                 ,this.w_PIANNRET;
                 ,this.w_PITIPCON;
                 ,this.w_PICODCON;
                 ,this.w_PIVALORI;
                 ,this.w_PIVALNAZ;
                 ,this.w_PIRIFDOC;
                 ,this.w_PITIPPER;
                 &i_ccchkv. )
            i_Rows=iif(bTrsErr,0,1)
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if i_Rows<0 or bTrsErr
            * --- Error: insert not accepted
            i_Error=MSG_INSERT_ERROR
            return
          endif
          this.w_INSERTED = .T.
        endif
        * --- Effettuo la scrittura sul documenti solo al variare del seriale.  Aggiorno il flag "Genarazione INTRA"
        if this.w_OSERIAL<>this.w_PIRIFDOC
          * --- Flag su Documento Origine
          * --- Write into DOC_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVFLINTR ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_MAST','MVFLINTR');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_PIRIFDOC);
                   )
          else
            update (i_cTable) set;
                MVFLINTR = "S";
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_PIRIFDOC;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
        this.w_FLER1 = .F.
      endif
      if this.w_FLERR
        this.w_KODOC = this.w_KODOC + 1
      else
        if this.w_INSERTED
          this.w_NUDOC = this.w_NUDOC + 1
        endif
      endif
      SELECT DOCUINTR
    endif
    * --- Controllo se non sono presenti errori nel master
    if this.w_FLERR=.F.
      * --- Scrive il Detail
      this.w_PINAZDES = "   "
      this.w_PIPRODES = "  "
      this.w_PINAZORI = "   "
      this.w_PINAZPRO = "   "
      this.w_PIPROORI = "  "
      this.w_PIPAEPAG = "   "
      this.w_CPROWNUM = this.w_CPROWNUM + 1
      this.w_CPROWORD = this.w_CPROWORD + 10
      this.w_TIPORIGA = NVL(MVTIPRIG, " ")
      this.w_MVFLTRAS = MVFLTRAS
      * --- Valorizzo la modalit� di incasso e la modalit� di erogazione. Nel caso di modalit�
      *                  ... di incasso la valorizzo sempre ad 'X' (Altro) invece per la modalit� di erogazione
      *                  ... la leggo dall'anagrafica Servizi
      if this.w_PITIPMOV $ "CS-RS-AS-RX"
        this.w_PIPERSER = NVL(DOCUINTR.ARPERSER, " ")
        this.w_PIMODINC = "X"
      else
        this.w_PIPERSER = " "
        this.w_PIMODINC = " "
      endif
      * --- Se annuale cessioni o acquisti la nomenclatura non � avvalorata
      this.w_PINOMENC = NVL(PINOMENC, SPACE(8))
      this.w_PICONDCO = NVL(MVCONCON, " ")
      this.w_PIMODTRA = NVL(MVCODSPE, " ")
      this.w_PINATTRA = NVL(PINATTRA, SPACE(3))
      this.w_PIROWORD = NVL(CPROWORD,0)
      if this.w_PITIPMOV $ "CE-RC-AC-RA"
        if MVCLADOC = "NC"
          * --- LE SEGUENTI CONDIZIONI POTREBBERO ESSERE MESSE IN OR
          *     I RAMI IF SONO DIVISI PER FAVORIRE LA LEGGIBILIT�
          * --- Nelle note di credito, la natura della transazione deve essere uguale alle fatture
          *     se la data di registrazione appartiene al periodo di rettifica
          if MVTIPPER = "M" AND YEAR(MVDATREG)=VAL(MVANNRET) AND MONTH(MVDATREG)=MVPERRET
            this.w_PINATTRA = IIF( EMPTY( NVL(PINATTR2, SPACE(3)) ) , NVL(PINATTRA, SPACE(3)) , NVL(PINATTR2, SPACE(3)) )
          endif
          if MVTIPPER = "T" AND YEAR(MVDATREG)=VAL(MVANNRET) AND INT(MONTH(MVDATREG)*4/12 +.8)=MVPERRET
            * --- INT(MONTH(MVDATREG)*4/12 +.8) � IL TRIMESTRE
            this.w_PINATTRA = IIF( EMPTY( NVL(PINATTR2, SPACE(3)) ) , NVL(PINATTRA, SPACE(3)) , NVL(PINATTR2, SPACE(3)) )
          endif
          * --- sE L'ANNO DI RETTIFICA � VUOTO,
          *     la natura della transazione viene comunque modificata
          if EMPTY(MVANNRET)
            this.w_PINATTRA = NVL(PINATTR2, SPACE(3))
          endif
        endif
      endif
      if this.w_SPEACC<>0 AND ((MVFLTRAS <> "S" And MVFLTRAS<>"Z") or NVL(ARUTISER,"N") = "S") 
        this.w_PREC = IIF(this.w_PREC=0 AND this.w_SPEACC<>0, this.w_CPROWNUM, this.w_PREC)
      endif
      * --- Nel caso la riga documento abbia il   flag "Manutenzione elenchi INTRA" uguale a solo dati statistici
      *     non valorizzo l'importo in valuta nazionale/originaria
      if MVFLTRAS="Z" 
        * --- Nel caso la riga abbia impostato 
        this.w_PIIMPNAZ = 0
        this.w_PIIMPVAL = 0
      else
        this.w_IMPACC = cp_ROUND(NVL(MVIMPACC, 0), this.w_DECTOP)
        if this.w_MVCODVAL<>this.w_PIVALNAZ And this.w_IMPACC<>0
          this.w_APPO1 = NVL(MVCAOVAL,1)
          * --- Converto eventuali spese ripartite nella valuta di conto prima di detrarla..
          this.w_IMPACC = VAL2MON(this.w_IMPACC, this.w_APPO1, this.w_CAONAZ, cp_CharToDate("01-01-99"), this.w_PIVALNAZ, this.w_DECTOT)
        endif
        this.w_PIIMPNAZ = cp_ROUND(NVL(PIIMPNAZ, 0), this.w_DECTOP) - this.w_IMPACC
        if this.w_PIIMPNAZ < 0 And (MVFLTRAS <> "S" or NVL(ARUTISER,"N") = "S")
          this.w_oERRORLOG.AddMsgLog("Verificare reg. elenchi INTRA: %1 del %2", ALLTRIM(STR(this.w_PINUMREG)), DTOC(this.w_PIDATREG))     
          this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "Riga %1 importo negativo convertito: %2", ALLTRIM(STR(this.w_CPROWORD)), alltrim(STR(this.w_PIIMPNAZ)) )     
          this.w_PIIMPNAZ = ABS(this.w_PIIMPNAZ)
          if this.w_PITIPMOV $ "RC-RA"
            this.w_PITIPRET = "+"
          endif
        endif
      endif
      this.w_PIVALSTA = cp_ROUND(NVL(PIVALSTA, 0), this.w_DECTOP)
      if this.w_PIVALSTA < 0
        this.w_PIVALSTA = ABS(this.w_PIVALSTA)
      endif
      this.w_PIVALSTO = this.w_PIVALSTA
      if this.w_SPEACC<>0 AND this.w_TOTIMPNA<>0 and NVL(ARTIPSER,"") <> "S" and NVL(MVFLOMAG,"") = "X"
        * --- Ripartisce le spese Accessorie  (il Resto va Resto sull'ultima Riga)
        this.w_SPEDET = (this.w_SPEACC * this.w_PIIMPNAZ) / this.w_TOTIMPNA
        this.w_SPEDET = cp_ROUND(this.w_SPEDET, this.w_DECTOP)
        this.w_PIIMPNAZ = this.w_PIIMPNAZ + this.w_SPEDET
        this.w_REST1 = this.w_REST1 - ABS(this.w_SPEDET)
        this.w_SPEDET = (this.w_SPEACC1 * this.w_PIVALSTA) / this.w_TOTIMPNA
        this.w_SPEDET = cp_ROUND(this.w_SPEDET, this.w_DECTOP)
        this.w_PIVALSTA = this.w_PIVALSTA + this.w_SPEDET
      endif
      if this.w_PITIPMOV $ "AC-RA-AS-RX"
        * --- Read from VALUTE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.VALUTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "VACAOVAL,VADECTOT"+;
            " from "+i_cTable+" VALUTE where ";
                +"VACODVAL = "+cp_ToStrODBC(this.w_PIVALORI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            VACAOVAL,VADECTOT;
            from (i_cTable) where;
                VACODVAL = this.w_PIVALORI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_APPO1 = NVL(cp_ToDate(_read_.VACAOVAL),cp_NullValue(_read_.VACAOVAL))
          this.w_APPO2 = NVL(cp_ToDate(_read_.VADECTOT),cp_NullValue(_read_.VADECTOT))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        SELECT DOCUINTR
        if this.w_APPO1 <> 0
          * --- Se la valuta � a cambio fisso, l'importo in valuta originaria non deve essere compilato
          this.w_PIIMPVAL = 0
        else
          this.w_PIIMPVAL = this.w_PIIMPNAZ
          if this.w_PIVALORI<>this.w_PIVALNAZ
            this.w_APPO3 = GETCAM(this.w_PIVALORI, this.w_PIDATDOC, 0)
            * --- Converte nella Valuta della Nazione
            this.w_APPO1 = IIF(this.w_APPO3=0, IIF(this.w_APPO1=0, 1, this.w_APPO1), this.w_APPO3)
            this.w_PIIMPVAL = cp_ROUND(MON2VAL(this.w_PIIMPVAL, this.w_APPO1, this.w_CAONAZ, cp_CharToDate("01-01-99"), this.w_PIVALNAZ), this.w_APPO2)
            SELECT DOCUINTR
          endif
        endif
      else
        this.w_PIIMPVAL = 0
      endif
      if this.w_PERIODO = "M" AND this.w_CODISO<>"SM"
        if this.w_TRAINT<>0 AND this.w_TOTIMP<>0 and MVFLTRAS <> "S" 
          * --- Ripartisce le spese Trasporto sino al Confine (il Resto va Resto sull'ultima Riga)
          this.w_TRADET = IIF(this.w_CPROWNUM=this.w_NUREC, this.w_RESTO, (this.w_TRAINT * this.w_PIVALSTO) / this.w_TOTIMP)
          this.w_TRADET = cp_ROUND(this.w_TRADET, this.w_DECTOP)
          this.w_PIVALSTA = this.w_PIVALSTA + this.w_TRADET
          this.w_RESTO = this.w_RESTO - ABS(this.w_TRADET)
        endif
        * --- Read from NOMENCLA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.NOMENCLA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.NOMENCLA_idx,2],.t.,this.NOMENCLA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "NMUNISUP"+;
            " from "+i_cTable+" NOMENCLA where ";
                +"NMCODICE = "+cp_ToStrODBC(this.w_PINOMENC);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            NMUNISUP;
            from (i_cTable) where;
                NMCODICE = this.w_PINOMENC;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_UMSUPP = NVL(cp_ToDate(_read_.NMUNISUP),cp_NullValue(_read_.NMUNISUP))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Riporto sempre la massa netta che trovo sul documento.
        this.w_PIMASNET = cp_ROUND(NVL(PIMASNET,0), this.w_DECQT)
        this.w_PIQTASUP = IIF(EMPTY(ALLTRIM(this.w_UMSUPP)) OR this.w_UMSUPP="ZZZ",0,cp_ROUND(NVL(PIQTASUP,0), this.w_DECQT))
        * --- Controllo la valorizzazione del campo "Manutenzione elenchi INTRA".
        *     Se valorizzato ad "I" - Solo dati fiscali azzero i campi importo statistico,
        *     quantit� supplementare e massa netta
        if this.w_MVFLTRAS = "I"
          this.w_PIVALSTA = 0
          this.w_PIQTASUP = 0
          this.w_PIMASNET = 0
        endif
      else
        this.w_PIVALSTA = 0
        this.w_PIQTASUP = 0
        this.w_PIMASNET = 0
      endif
      * --- Se periodo trimestrale, azzero i campi massa netta e quantit� supplementare
      *     La stessa operazione viene effettuata se il periodo � mensile 
      *     e la tipologia operazioni � uguale a rettifiche oppure 
      *     cessioni/acquisti di servizi
      if this.w_PERIODO = "T" or (this.w_PERIODO = "M" and this.w_PITIPMOV $ "CS-AS-RX-RS")
        this.w_PIQTASUP = 0
        this.w_PIMASNET = 0
      endif
      if this.oParentObject.w_FLVEAC="V"
        if IIF(this.w_PITIPMOV $ "CE-RC",this.w_PERIODO,this.w_PERIODO_S)="M" and this.w_CODISO<>"SM" And this.w_PITIPMOV $ "CE-RC-AC-RA" And (this.w_MVFLTRAS = "Z" OR EMPTY(this.w_MVFLTRAS))
          this.w_PINAZDES = IIF( EMPTY(NVL( MVNAZPRO , "" )) , this.w_ANNAZION , MVNAZPRO )
          this.w_PIPRODES = IIF( EMPTY( this.w_DDPROVIN ) , this.w_ANPROVIN , this.w_DDPROVIN )
          this.w_PIPROORI = IIF(EMPTY(NVL(MVPROORD,"")), g_PROAZI, MVPROORD)
          if g_ISONAZ="ESP"
            this.w_PIPORDES = this.w_ANCODPOR
            this.w_PIPORORI = IIF(EMPTY(NVL(MVAIRPOR,"")), this.w_AZCODPOR, MVAIRPOR)
          endif
        else
          this.w_PICONDCO = IIF(g_ISONAZ<>"ESP","N"," ")
          this.w_PIMODTRA = "   "
          this.w_PIVALSTA = 0
        endif
        * --- Nel caso la tipologia operazione � uguale a cessioni/acquisti di servizi oppure
        *     rettifica di acquisti/cessioni di servizi il paese di pagamento viene valorizzato andandolo a leggere la nazione
        *     dai dati azienda
        if this.w_PITIPMOV $ "CS-RX-AS-RS"
          this.w_PIPAEPAG = g_CODNAZ
        else
          this.w_PIPAEPAG = space(3)
        endif
      else
        if this.w_PERIODO="M" and this.w_CODISO<>"SM" And this.w_PITIPMOV $ "CE-RC-AC-RA" And (this.w_MVFLTRAS = "Z" OR EMPTY(this.w_MVFLTRAS))
          this.w_PINAZORI = IIF( EMPTY( NVL( MVPAEFOR , SPACE( 3 ) ) ) , NVL( ANNAZION , SPACE( 3 ) ) , NVL( MVPAEFOR , SPACE( 3 ) ) )
          this.w_PINAZPRO = IIF( EMPTY(NVL(MVNAZPRO,"")) , IIF(EMPTY(this.w_DDCODNAZ),this.w_ANNAZION,this.w_DDCODNAZ), MVNAZPRO )
          this.w_PIPRODES = IIF(EMPTY(NVL(MVPROORD,"")), g_PROAZI, MVPROORD)
          if g_ISONAZ="ESP"
            this.w_PIPORDES = IIF(EMPTY(NVL(MVAIRPOR,"")), this.w_AZCODPOR, MVAIRPOR)
          endif
        else
          this.w_PICONDCO = IIF(g_ISONAZ<>"ESP","N"," ")
          this.w_PIMODTRA = "   "
          this.w_PIVALSTA = 0
        endif
        * --- Nel caso la tipologia operazione � uguale a cessioni/acquisti di servizi oppure
        *     rettifica di acquisti/cessioni di servizi il paese di pagamento viene valorizzato andandolo a leggere
        *     dai dati aggiuntivi di riga il paese di destinazioni 
        if this.w_PITIPMOV $ "CS-RX-AS-RS"
          this.w_PIPAEPAG = NVL(ANNAZION, g_CODNAZ)
        else
          this.w_PIPAEPAG = Space(3)
        endif
      endif
      * --- Valore Statistico in base alla periodicit� ed al valore della combo nei Dati Azienda
      *     SOLO ACQUISTI / CESSIONI PERIODICITA' MENSILE
      *     e nazione non SAN MARINO
      if this.w_PERIODO="M" AND this.w_CODISO<>"SM"
        * --- Verifico l'obbligatoriet� dei campi Condizione di Consegna, Modalit� di Trasporto
        *     se non obbligatori nei dati azienda non eseguo controlli e azzero i dati
        if ((this.w_PITIPMOV $ "CE-RC" AND this.w_DTOBBL $ "EC") OR (this.w_PITIPMOV $ "AC-RA" AND this.w_DTOBBL $ "EA")) 
          if (this.w_PITIPMOV $ "CE-AC" OR (VAL(this.w_PI__ANNO)=VAL(this.w_PIANNRET) AND this.w_PIPERRET=MONTH(this.w_PIDATCOM))) And ( this.w_MVFLTRAS = "Z" OR Empty(this.w_MVFLTRAS) )
            * --- Se cessione o Acquisto o Rettifica dello stesso periodo
            if this.w_PICONDCO="N" OR EMPTY(this.w_PICONDCO) 
              this.w_oERRORLOG.AddMsgLog("Verificare reg. elenchi INTRA: %1 del %2", ALLTRIM(STR(this.w_PINUMREG)), DTOC(this.w_PIDATREG))     
              this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "Riga %1 condizioni di consegna mancanti", ALLTRIM(STR(this.w_CPROWORD)))     
            endif
            if EMPTY(NVL(this.w_PIMODTRA,""))
              this.w_oERRORLOG.AddMsgLog("Verificare reg. elenchi INTRA: %1 del %2", ALLTRIM(STR(this.w_PINUMREG)), DTOC(this.w_PIDATREG))     
              this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "Riga %1 modalit� di trasporto mancante", ALLTRIM(STR(this.w_CPROWORD)))     
            endif
            if this.w_PIVALSTA = 0 
              this.w_oERRORLOG.AddMsgLog("Verificare reg. elenchi INTRA: %1 del %2", ALLTRIM(STR(this.w_PINUMREG)), DTOC(this.w_PIDATREG))     
              this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "Riga %1 valore statistico uguale a zero", ALLTRIM(STR(this.w_CPROWORD)))     
            endif
          endif
        else
          this.w_PICONDCO = IIF(g_ISONAZ<>"ESP","N"," ")
          this.w_PIMODTRA = "   "
          this.w_PIVALSTA = 0
        endif
      endif
      * --- Effettuo tutti queste controlli solo nel caso di acquisti o cessioni Mensili. Non effettuo nessun controllo nelle rettifiche
      *     e nazione no SAN MARINO
      if this.w_PITIPMOV $ "CE-AC" AND this.w_PERIODO = "M" AND this.w_CODISO<>"SM"
        if this.w_PIQTASUP=0 and !EMPTY(this.w_UMSUPP) and this.w_UMSUPP<>"ZZZ" And ( this.w_MVFLTRAS = "Z" OR Empty(this.w_MVFLTRAS) )
          this.w_oERRORLOG.AddMsgLog("Verificare reg. elenchi INTRA: %1 del %2", ALLTRIM(STR(this.w_PINUMREG)), DTOC(this.w_PIDATREG))     
          this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "Riga %1 quantit� associata all'unit� di misura supplementare %2 uguale a zero", ALLTRIM(STR(this.w_CPROWORD)), this.w_UMSUPP )     
        endif
      endif
      * --- Controllo su Natura Transazione.Il controllo deve essere effettuato solo nel caso
      *     di cessioni/acquisto di beni oppure rettifiche di cessione/acquisto di beni
      if EMPTY(this.w_PINATTRA) And this.w_PITIPMOV $ "CE-RC-AC-RA"
        this.w_oERRORLOG.AddMsgLog("Verificare reg. elenchi INTRA: %1 del %2", ALLTRIM(STR(this.w_PINUMREG)), DTOC(this.w_PIDATREG))     
        this.w_oERRORLOG.AddMsgLogPartNoTrans(space(5), "Riga %1 natura della transazione mancante (solo warning)", ALLTRIM(STR(this.w_CPROWORD)))     
      endif
      * --- Controllo su Nomenclatura
      if EMPTY(this.w_PINOMENC) And (MVFLTRAS <> "S" or NVL(ARUTISER,"N") = "S")
        this.w_oERRORLOG.AddMsgLog("Verificare reg. elenchi INTRA: %1 del %2", ALLTRIM(STR(this.w_PINUMREG)), DTOC(this.w_PIDATREG))     
        if DOCUINTR.ARUTISER="S"
          this.w_oERRORLOG.AddMsgLogPartNoTrans(SPACE(5),"Riga %1 codice servizio mancante (solo warning)",ALLTRIM(STR(this.w_CPROWORD)))     
        else
          this.w_oERRORLOG.AddMsgLogPartNoTrans(SPACE(5),"Riga %1 nomenclatura mancante (solo warning)",ALLTRIM(STR(this.w_CPROWORD)))     
        endif
      endif
      if this.w_PITIPMOV $ "CS-AS" And this.w_MVFLTRAS<>"S"
        this.w_oERRORLOG.AddMsgLog("Verificare reg. elenchi INTRA %1 del %2",ALLTRIM(STR(this.w_PINUMREG)),DTOC(this.w_PIDATREG))     
        this.w_oERRORLOG.AddMsgLogPartNoTrans(SPACE(5),"Riga %1 codice servizio non congruente con valorizzazione combo utilizzo INTRA (solo warning)",ALLTRIM(STR(this.w_CPROWORD)))     
      endif
      * --- Se tipologia operazione uguale a "Rettifica cessione/acquisti servizi" bisogna
      *     avvertire l'utente 
      if this.w_PITIPMOV $ "RS-RX" And ( this.w_OSERIAL<>this.w_PIRIFDOC OR DOCUINTR.ARUTISER<>this.w_PIUTISER )
        this.w_oERRORLOG.AddMsgLog("Verificare reg. elenchi INTRA %1 del %2",ALLTRIM(STR(this.w_PINUMREG)),DTOC(this.w_PIDATREG))     
        this.w_oERRORLOG.AddMsgLogPartNoTrans(SPACE(5),"Generato documento di rettifica senza riferimenti a documento di origine (solo warning)")     
      endif
      * --- Se forfettario e periodo non mensile non scrivo il record nella manutenzione INTRA
      if !(this.w_PERIODO <> "M" AND this.w_MVFLTRAS = "Z" ) AND (MVFLTRAS <> "S" or NVL(ARUTISER,"N") = "S")
        * --- Insert into ELEIDETT
        i_nConn=i_TableProp[this.ELEIDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ELEIDETT_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_ccchkf=''
        i_ccchkv=''
        this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.ELEIDETT_idx,i_nConn)
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                      " ("+"PISERIAL"+",CPROWNUM"+",CPROWORD"+",PITIPRET"+",PIIMPNAZ"+",PIIMPVAL"+",PINATTRA"+",PINOMENC"+",PIMASNET"+",PIQTASUP"+",PIVALSTA"+",PICONDCO"+",PIMODTRA"+",PINAZDES"+",PIPRODES"+",PINAZPRO"+",PINAZORI"+",PIPROORI"+",PIPORDES"+",PIPORORI"+",PIPERSER"+",PIMODINC"+",PIPAEPAG"+",PIROWORD"+i_ccchkf+") values ("+;
          cp_NullLink(cp_ToStrODBC(this.w_PISERIAL),'ELEIDETT','PISERIAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWNUM),'ELEIDETT','CPROWNUM');
          +","+cp_NullLink(cp_ToStrODBC(this.w_CPROWORD),'ELEIDETT','CPROWORD');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PITIPRET),'ELEIDETT','PITIPRET');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PIIMPNAZ),'ELEIDETT','PIIMPNAZ');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PIIMPVAL),'ELEIDETT','PIIMPVAL');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PINATTRA),'ELEIDETT','PINATTRA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PINOMENC),'ELEIDETT','PINOMENC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PIMASNET),'ELEIDETT','PIMASNET');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PIQTASUP),'ELEIDETT','PIQTASUP');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PIVALSTA),'ELEIDETT','PIVALSTA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PICONDCO),'ELEIDETT','PICONDCO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PIMODTRA),'ELEIDETT','PIMODTRA');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PINAZDES),'ELEIDETT','PINAZDES');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PIPRODES),'ELEIDETT','PIPRODES');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PINAZPRO),'ELEIDETT','PINAZPRO');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PINAZORI),'ELEIDETT','PINAZORI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PIPROORI),'ELEIDETT','PIPROORI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PIPORDES),'ELEIDETT','PIPORDES');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PIPORORI),'ELEIDETT','PIPORORI');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PIPERSER),'ELEIDETT','PIPERSER');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PIMODINC),'ELEIDETT','PIMODINC');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PIPAEPAG),'ELEIDETT','PIPAEPAG');
          +","+cp_NullLink(cp_ToStrODBC(this.w_PIROWORD),'ELEIDETT','PIROWORD');
               +i_ccchkv+")")
        else
          cp_CheckDeletedKey(i_cTable,0,'PISERIAL',this.w_PISERIAL,'CPROWNUM',this.w_CPROWNUM,'CPROWORD',this.w_CPROWORD,'PITIPRET',this.w_PITIPRET,'PIIMPNAZ',this.w_PIIMPNAZ,'PIIMPVAL',this.w_PIIMPVAL,'PINATTRA',this.w_PINATTRA,'PINOMENC',this.w_PINOMENC,'PIMASNET',this.w_PIMASNET,'PIQTASUP',this.w_PIQTASUP,'PIVALSTA',this.w_PIVALSTA,'PICONDCO',this.w_PICONDCO)
          insert into (i_cTable) (PISERIAL,CPROWNUM,CPROWORD,PITIPRET,PIIMPNAZ,PIIMPVAL,PINATTRA,PINOMENC,PIMASNET,PIQTASUP,PIVALSTA,PICONDCO,PIMODTRA,PINAZDES,PIPRODES,PINAZPRO,PINAZORI,PIPROORI,PIPORDES,PIPORORI,PIPERSER,PIMODINC,PIPAEPAG,PIROWORD &i_ccchkf. );
             values (;
               this.w_PISERIAL;
               ,this.w_CPROWNUM;
               ,this.w_CPROWORD;
               ,this.w_PITIPRET;
               ,this.w_PIIMPNAZ;
               ,this.w_PIIMPVAL;
               ,this.w_PINATTRA;
               ,this.w_PINOMENC;
               ,this.w_PIMASNET;
               ,this.w_PIQTASUP;
               ,this.w_PIVALSTA;
               ,this.w_PICONDCO;
               ,this.w_PIMODTRA;
               ,this.w_PINAZDES;
               ,this.w_PIPRODES;
               ,this.w_PINAZPRO;
               ,this.w_PINAZORI;
               ,this.w_PIPROORI;
               ,this.w_PIPORDES;
               ,this.w_PIPORORI;
               ,this.w_PIPERSER;
               ,this.w_PIMODINC;
               ,this.w_PIPAEPAG;
               ,this.w_PIROWORD;
               &i_ccchkv. )
          i_Rows=iif(bTrsErr,0,1)
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if i_Rows<0 or bTrsErr
          * --- Error: insert not accepted
          i_Error=MSG_INSERT_ERROR
          return
        endif
      endif
      SELECT DOCUINTR
    endif
    this.w_OSERIAL = this.w_PIRIFDOC
    this.w_PIUTISER = DOCUINTR.ARUTISER
    ENDSCAN
    if this.w_REST1<>0 AND this.w_PREC<>0 
      * --- Memorizzo resto sulla prima riga ,non posso utilizzare la Write del 
      *     valore statistico perch� le righe possono non coincidere.
      * --- Write into ELEIDETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ELEIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ELEIDETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ELEIDETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"PIIMPNAZ =PIIMPNAZ+ "+cp_ToStrODBC(this.w_REST1);
            +i_ccchkf ;
        +" where ";
            +"PISERIAL = "+cp_ToStrODBC(this.w_PISERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_PREC);
               )
      else
        update (i_cTable) set;
            PIIMPNAZ = PIIMPNAZ + this.w_REST1;
            &i_ccchkf. ;
         where;
            PISERIAL = this.w_PISERIAL;
            and CPROWNUM = this.w_PREC;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,10)]
    this.cWorkTables[1]='ELEIMAST'
    this.cWorkTables[2]='ELEIDETT'
    this.cWorkTables[3]='NAZIONI'
    this.cWorkTables[4]='VALUTE'
    this.cWorkTables[5]='DOC_MAST'
    this.cWorkTables[6]='CONTI'
    this.cWorkTables[7]='AZIENDA'
    this.cWorkTables[8]='NOMENCLA'
    this.cWorkTables[9]='*TMPVEND1'
    this.cWorkTables[10]='AZDATINT'
    return(this.OpenAllTables(10))

  proc CloseCursors()
    if used('_Curs_GSVE_BEI')
      use in _Curs_GSVE_BEI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
