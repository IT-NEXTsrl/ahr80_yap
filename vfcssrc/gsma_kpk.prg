* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_kpk                                                        *
*              Packing List                                                    *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_133]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-11-16                                                      *
* Last revis.: 2007-07-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsma_kpk",oParentObject))

* --- Class definition
define class tgsma_kpk as StdForm
  Top    = 10
  Left   = 7

  * --- Standard Properties
  Width  = 811
  Height = 473
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2007-07-13"
  HelpContextID=118850711
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=28

  * --- Constant Properties
  _IDX = 0
  CONTI_IDX = 0
  VETTORI_IDX = 0
  MODASPED_IDX = 0
  DES_DIVE_IDX = 0
  cPrg = "gsma_kpk"
  cComment = "Packing List"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_DATINI = ctod('  /  /  ')
  o_DATINI = ctod('  /  /  ')
  w_NUMINI = 0
  o_NUMINI = 0
  w_ALFINI = space(2)
  o_ALFINI = space(2)
  w_TIPDOC = space(1)
  o_TIPDOC = space(1)
  w_TOGLIPAC = space(10)
  w_TOGLIDOC = space(10)
  w_DATFIN = ctod('  /  /  ')
  w_NUMFIN = 0
  w_ALFFIN = space(2)
  w_TIPSEL = space(1)
  o_TIPSEL = space(1)
  w_TIPSELE = space(10)
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_TIPCLF = space(1)
  w_CODCLF = space(15)
  w_CODVET = space(5)
  w_CODSPE = space(3)
  w_CODDES = space(5)
  w_SELEZI = space(1)
  w_CHECK = .F.
  w_DESCLF = space(40)
  w_DESVET = space(35)
  w_NOMDES = space(35)
  w_DESSPE = space(35)
  w_SERIAL = space(10)
  w_PARAME = space(1)
  w_OBTEST = ctod('  /  /  ')
  w_PLSERIAL = space(10)
  w_DTOBSO = ctod('  /  /  ')
  w_ZoomDoc = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsma_kpkPag1","gsma_kpk",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oDATINI_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomDoc = this.oPgFrm.Pages(1).oPag.ZoomDoc
    DoDefault()
    proc Destroy()
      this.w_ZoomDoc = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='CONTI'
    this.cWorkTables[2]='VETTORI'
    this.cWorkTables[3]='MODASPED'
    this.cWorkTables[4]='DES_DIVE'
    return(this.OpenAllTables(4))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_DATINI=ctod("  /  /  ")
      .w_NUMINI=0
      .w_ALFINI=space(2)
      .w_TIPDOC=space(1)
      .w_TOGLIPAC=space(10)
      .w_TOGLIDOC=space(10)
      .w_DATFIN=ctod("  /  /  ")
      .w_NUMFIN=0
      .w_ALFFIN=space(2)
      .w_TIPSEL=space(1)
      .w_TIPSELE=space(10)
      .w_TIPCON=space(1)
      .w_TIPCLF=space(1)
      .w_CODCLF=space(15)
      .w_CODVET=space(5)
      .w_CODSPE=space(3)
      .w_CODDES=space(5)
      .w_SELEZI=space(1)
      .w_CHECK=.f.
      .w_DESCLF=space(40)
      .w_DESVET=space(35)
      .w_NOMDES=space(35)
      .w_DESSPE=space(35)
      .w_SERIAL=space(10)
      .w_PARAME=space(1)
      .w_OBTEST=ctod("  /  /  ")
      .w_PLSERIAL=space(10)
      .w_DTOBSO=ctod("  /  /  ")
          .DoRTCalc(1,3,.f.)
        .w_TIPDOC = 'C'
        .w_TOGLIPAC = IIF(.w_TIPDOC='C',SPACE(10),'XXXYYYKKKJ')
        .w_TOGLIDOC = IIF(.w_TIPDOC='S',SPACE(10),'XXXYYYKKKJ')
          .DoRTCalc(7,9,.f.)
        .w_TIPSEL = 'T'
        .w_TIPSELE = IIF(.w_TIPSEL='T',' ',IIF(.w_TIPSEL='C','S','N'))
        .w_TIPCON = 'T'
        .w_TIPCLF = IIF(.w_TIPCON='T',' ',IIF(.w_TIPCON='C','C','F'))
        .DoRTCalc(14,14,.f.)
        if not(empty(.w_CODCLF))
          .link_1_14('Full')
        endif
        .DoRTCalc(15,15,.f.)
        if not(empty(.w_CODVET))
          .link_1_15('Full')
        endif
        .DoRTCalc(16,16,.f.)
        if not(empty(.w_CODSPE))
          .link_1_16('Full')
        endif
        .DoRTCalc(17,17,.f.)
        if not(empty(.w_CODDES))
          .link_1_17('Full')
        endif
        .w_SELEZI = 'D'
        .w_CHECK = .T.
      .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
          .DoRTCalc(20,23,.f.)
        .w_SERIAL = iif(empty(.w_ZoomDoc.getVar('SERIAL')),space(10),.w_ZoomDoc.getVar('SERIAL'))
        .w_PARAME = .w_ZoomDoc.getVar('TIPCON')+.w_ZoomDoc.getVar('CLADOC')
        .w_OBTEST = i_DATSYS
      .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .w_PLSERIAL = iif(empty(.w_ZoomDoc.getVar('PLSERIAL')),space(10),.w_ZoomDoc.getVar('PLSERIAL'))
    endwith
    this.DoRTCalc(28,28,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_TIPDOC<>.w_TIPDOC
            .w_TOGLIPAC = IIF(.w_TIPDOC='C',SPACE(10),'XXXYYYKKKJ')
        endif
        if .o_TIPDOC<>.w_TIPDOC
            .w_TOGLIDOC = IIF(.w_TIPDOC='S',SPACE(10),'XXXYYYKKKJ')
        endif
        .DoRTCalc(7,9,.t.)
        if .o_TIPDOC<>.w_TIPDOC
            .w_TIPSEL = 'T'
        endif
        if .o_TIPSEL<>.w_TIPSEL
            .w_TIPSELE = IIF(.w_TIPSEL='T',' ',IIF(.w_TIPSEL='C','S','N'))
        endif
        .DoRTCalc(12,12,.t.)
        if .o_TIPCON<>.w_TIPCON
            .w_TIPCLF = IIF(.w_TIPCON='T',' ',IIF(.w_TIPCON='C','C','F'))
        endif
        .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
        .DoRTCalc(14,23,.t.)
            .w_SERIAL = iif(empty(.w_ZoomDoc.getVar('SERIAL')),space(10),.w_ZoomDoc.getVar('SERIAL'))
            .w_PARAME = .w_ZoomDoc.getVar('TIPCON')+.w_ZoomDoc.getVar('CLADOC')
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
        .DoRTCalc(26,26,.t.)
            .w_PLSERIAL = iif(empty(.w_ZoomDoc.getVar('PLSERIAL')),space(10),.w_ZoomDoc.getVar('PLSERIAL'))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(28,28,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomDoc.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_47.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTIPSEL_1_10.enabled = this.oPgFrm.Page1.oPag.oTIPSEL_1_10.mCond()
    this.oPgFrm.Page1.oPag.oCODCLF_1_14.enabled = this.oPgFrm.Page1.oPag.oCODCLF_1_14.mCond()
    this.oPgFrm.Page1.oPag.oCODDES_1_17.enabled = this.oPgFrm.Page1.oPag.oCODDES_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oTIPSEL_1_10.visible=!this.oPgFrm.Page1.oPag.oTIPSEL_1_10.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_22.visible=!this.oPgFrm.Page1.oPag.oBtn_1_22.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_23.visible=!this.oPgFrm.Page1.oPag.oBtn_1_23.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_24.visible=!this.oPgFrm.Page1.oPag.oBtn_1_24.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_25.visible=!this.oPgFrm.Page1.oPag.oBtn_1_25.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomDoc.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_47.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCLF
  func Link_1_14(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCLF) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCLF))
          select ANTIPCON,ANCODICE,ANDESCRI;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCLF)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCLF)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCLF) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCLF_1_14'),i_cWhere,'GSAR_BZC',"Clienti/fornitori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCLF)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCLF);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCLF)
            select ANTIPCON,ANCODICE,ANDESCRI;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCLF = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCLF = NVL(_Link_.ANDESCRI,space(40))
    else
      if i_cCtrl<>'Load'
        this.w_CODCLF = space(15)
      endif
      this.w_DESCLF = space(40)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCLF Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODVET
  func Link_1_15(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VETTORI_IDX,3]
    i_lTable = "VETTORI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2], .t., this.VETTORI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODVET) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVT',True,'VETTORI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VTCODVET like "+cp_ToStrODBC(trim(this.w_CODVET)+"%");

          i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VTCODVET","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VTCODVET',trim(this.w_CODVET))
          select VTCODVET,VTDESVET;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VTCODVET into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODVET)==trim(_Link_.VTCODVET) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODVET) and !this.bDontReportError
            deferred_cp_zoom('VETTORI','*','VTCODVET',cp_AbsName(oSource.parent,'oCODVET_1_15'),i_cWhere,'GSAR_AVT',"Vettori",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                     +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',oSource.xKey(1))
            select VTCODVET,VTDESVET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODVET)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VTCODVET,VTDESVET";
                   +" from "+i_cTable+" "+i_lTable+" where VTCODVET="+cp_ToStrODBC(this.w_CODVET);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VTCODVET',this.w_CODVET)
            select VTCODVET,VTDESVET;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODVET = NVL(_Link_.VTCODVET,space(5))
      this.w_DESVET = NVL(_Link_.VTDESVET,space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODVET = space(5)
      endif
      this.w_DESVET = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VETTORI_IDX,2])+'\'+cp_ToStr(_Link_.VTCODVET,1)
      cp_ShowWarn(i_cKey,this.VETTORI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODVET Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODSPE
  func Link_1_16(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.MODASPED_IDX,3]
    i_lTable = "MODASPED"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2], .t., this.MODASPED_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODSPE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ASP',True,'MODASPED')
        if i_nConn<>0
          i_cWhere = i_cFlt+" SPCODSPE like "+cp_ToStrODBC(trim(this.w_CODSPE)+"%");

          i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by SPCODSPE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'SPCODSPE',trim(this.w_CODSPE))
          select SPCODSPE,SPDESSPE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by SPCODSPE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODSPE)==trim(_Link_.SPCODSPE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODSPE) and !this.bDontReportError
            deferred_cp_zoom('MODASPED','*','SPCODSPE',cp_AbsName(oSource.parent,'oCODSPE_1_16'),i_cWhere,'GSAR_ASP',"Spedizioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                     +" from "+i_cTable+" "+i_lTable+" where SPCODSPE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODSPE',oSource.xKey(1))
            select SPCODSPE,SPDESSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODSPE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select SPCODSPE,SPDESSPE"+cp_TransInsFldName("SPDESSPE")+"";
                   +" from "+i_cTable+" "+i_lTable+" where SPCODSPE="+cp_ToStrODBC(this.w_CODSPE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'SPCODSPE',this.w_CODSPE)
            select SPCODSPE,SPDESSPE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODSPE = NVL(_Link_.SPCODSPE,space(3))
      this.w_DESSPE = NVL(cp_TransLoadField('_Link_.SPDESSPE'),space(35))
    else
      if i_cCtrl<>'Load'
        this.w_CODSPE = space(3)
      endif
      this.w_DESSPE = space(35)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.MODASPED_IDX,2])+'\'+cp_ToStr(_Link_.SPCODSPE,1)
      cp_ShowWarn(i_cKey,this.MODASPED_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODSPE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODDES
  func Link_1_17(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.DES_DIVE_IDX,3]
    i_lTable = "DES_DIVE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2], .t., this.DES_DIVE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODDES) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'DES_DIVE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" DDCODDES like "+cp_ToStrODBC(trim(this.w_CODDES)+"%");
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODCLF);

          i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by DDTIPCON,DDCODICE,DDCODDES","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'DDTIPCON',this.w_TIPCLF;
                     ,'DDCODICE',this.w_CODCLF;
                     ,'DDCODDES',trim(this.w_CODDES))
          select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by DDTIPCON,DDCODICE,DDCODDES into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODDES)==trim(_Link_.DDCODDES) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODDES) and !this.bDontReportError
            deferred_cp_zoom('DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(oSource.parent,'oCODDES_1_17'),i_cWhere,'',"Destinazioni",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCLF<>oSource.xKey(1);
           .or. this.w_CODCLF<>oSource.xKey(2);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Codice destinazione inesistente")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(oSource.xKey(3));
                     +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                     +" and DDCODICE="+cp_ToStrODBC(this.w_CODCLF);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',oSource.xKey(1);
                       ,'DDCODICE',oSource.xKey(2);
                       ,'DDCODDES',oSource.xKey(3))
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODDES)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where DDCODDES="+cp_ToStrODBC(this.w_CODDES);
                   +" and DDTIPCON="+cp_ToStrODBC(this.w_TIPCLF);
                   +" and DDCODICE="+cp_ToStrODBC(this.w_CODCLF);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'DDTIPCON',this.w_TIPCLF;
                       ,'DDCODICE',this.w_CODCLF;
                       ,'DDCODDES',this.w_CODDES)
            select DDTIPCON,DDCODICE,DDCODDES,DDNOMDES,DDDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODDES = NVL(_Link_.DDCODDES,space(5))
      this.w_NOMDES = NVL(_Link_.DDNOMDES,space(35))
      this.w_DTOBSO = NVL(cp_ToDate(_Link_.DDDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODDES = space(5)
      endif
      this.w_NOMDES = space(35)
      this.w_DTOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.DES_DIVE_IDX,2])+'\'+cp_ToStr(_Link_.DDTIPCON,1)+'\'+cp_ToStr(_Link_.DDCODICE,1)+'\'+cp_ToStr(_Link_.DDCODDES,1)
      cp_ShowWarn(i_cKey,this.DES_DIVE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODDES Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_1.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_1.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMINI_1_2.value==this.w_NUMINI)
      this.oPgFrm.Page1.oPag.oNUMINI_1_2.value=this.w_NUMINI
    endif
    if not(this.oPgFrm.Page1.oPag.oALFINI_1_3.value==this.w_ALFINI)
      this.oPgFrm.Page1.oPag.oALFINI_1_3.value=this.w_ALFINI
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDOC_1_4.RadioValue()==this.w_TIPDOC)
      this.oPgFrm.Page1.oPag.oTIPDOC_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_7.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_7.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMFIN_1_8.value==this.w_NUMFIN)
      this.oPgFrm.Page1.oPag.oNUMFIN_1_8.value=this.w_NUMFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oALFFIN_1_9.value==this.w_ALFFIN)
      this.oPgFrm.Page1.oPag.oALFFIN_1_9.value=this.w_ALFFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPSEL_1_10.RadioValue()==this.w_TIPSEL)
      this.oPgFrm.Page1.oPag.oTIPSEL_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_12.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCLF_1_14.value==this.w_CODCLF)
      this.oPgFrm.Page1.oPag.oCODCLF_1_14.value=this.w_CODCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oCODVET_1_15.value==this.w_CODVET)
      this.oPgFrm.Page1.oPag.oCODVET_1_15.value=this.w_CODVET
    endif
    if not(this.oPgFrm.Page1.oPag.oCODSPE_1_16.value==this.w_CODSPE)
      this.oPgFrm.Page1.oPag.oCODSPE_1_16.value=this.w_CODSPE
    endif
    if not(this.oPgFrm.Page1.oPag.oCODDES_1_17.value==this.w_CODDES)
      this.oPgFrm.Page1.oPag.oCODDES_1_17.value=this.w_CODDES
    endif
    if not(this.oPgFrm.Page1.oPag.oSELEZI_1_19.RadioValue()==this.w_SELEZI)
      this.oPgFrm.Page1.oPag.oSELEZI_1_19.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCLF_1_36.value==this.w_DESCLF)
      this.oPgFrm.Page1.oPag.oDESCLF_1_36.value=this.w_DESCLF
    endif
    if not(this.oPgFrm.Page1.oPag.oDESVET_1_37.value==this.w_DESVET)
      this.oPgFrm.Page1.oPag.oDESVET_1_37.value=this.w_DESVET
    endif
    if not(this.oPgFrm.Page1.oPag.oNOMDES_1_40.value==this.w_NOMDES)
      this.oPgFrm.Page1.oPag.oNOMDES_1_40.value=this.w_NOMDES
    endif
    if not(this.oPgFrm.Page1.oPag.oDESSPE_1_41.value==this.w_DESSPE)
      this.oPgFrm.Page1.oPag.oDESSPE_1_41.value=this.w_DESSPE
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not((.w_DATINI<=.w_DATFIN) or empty(.w_DATFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_1.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di inizio selezione superiore a quella di fine selezione")
          case   not((.w_NUMINI<=.w_NUMFIN) OR EMPTY(.w_NUMFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMINI_1_2.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero di inizio selezione superiore a quello di fine selezione")
          case   not((.w_ALFINI<=.w_ALFFIN) OR EMPTY(.w_ALFFIN))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oALFINI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Seriale di inizio selezione superiore a quello di fine selezione")
          case   not((.w_DATINI<=.w_DATFIN) OR EMPTY(.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_7.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di inizio selezione superiore a quella di fine selezione")
          case   not((.w_NUMINI<=.w_NUMFIN) OR EMPTY(.w_NUMINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oNUMFIN_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Numero di inizio selezione superiore a quello di fine selezione")
          case   not((.w_ALFINI<=.w_ALFFIN) OR EMPTY(.w_ALFINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oALFFIN_1_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Seriale di inizio selezione superiore a quello di fine selezione")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_DATINI = this.w_DATINI
    this.o_NUMINI = this.w_NUMINI
    this.o_ALFINI = this.w_ALFINI
    this.o_TIPDOC = this.w_TIPDOC
    this.o_TIPSEL = this.w_TIPSEL
    this.o_TIPCON = this.w_TIPCON
    return

enddefine

* --- Define pages as container
define class tgsma_kpkPag1 as StdContainer
  Width  = 807
  height = 473
  stdWidth  = 807
  stdheight = 473
  resizeXpos=386
  resizeYpos=378
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oDATINI_1_1 as StdField with uid="IHLXVRZOGS",rtseq=1,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data di inizio selezione superiore a quella di fine selezione",;
    ToolTipText = "Data inizio selezione",;
    HelpContextID = 88345142,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=98, Top=10

  func oDATINI_1_1.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATINI<=.w_DATFIN) or empty(.w_DATFIN))
    endwith
    return bRes
  endfunc

  add object oNUMINI_1_2 as StdField with uid="FWSPRIYVFD",rtseq=2,rtrep=.f.,;
    cFormVar = "w_NUMINI", cQueryName = "NUMINI",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero di inizio selezione superiore a quello di fine selezione",;
    ToolTipText = "Numero documento di inizio selezione",;
    HelpContextID = 88321750,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=276, Top=10, cSayPict='"999999"', cGetPict='"999999"'

  func oNUMINI_1_2.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_NUMINI<=.w_NUMFIN) OR EMPTY(.w_NUMFIN))
    endwith
    return bRes
  endfunc

  add object oALFINI_1_3 as StdField with uid="RXMXTMDPMQ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ALFINI", cQueryName = "ALFINI",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Seriale di inizio selezione superiore a quello di fine selezione",;
    HelpContextID = 88290566,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=359, Top=10, InputMask=replicate('X',2)

  func oALFINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ALFINI<=.w_ALFFIN) OR EMPTY(.w_ALFFIN))
    endwith
    return bRes
  endfunc


  add object oTIPDOC_1_4 as StdCombo with uid="YTBPAZCXQX",rtseq=4,rtrep=.f.,left=651,top=10,width=138,height=21;
    , ToolTipText = "Selezionare documenti con o senza Packing List generata";
    , HelpContextID = 256824118;
    , cFormVar="w_TIPDOC",RowSource=""+"Con Packing List,"+"Senza Packing List", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPDOC_1_4.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'S',;
    space(1))))
  endfunc
  func oTIPDOC_1_4.GetRadio()
    this.Parent.oContained.w_TIPDOC = this.RadioValue()
    return .t.
  endfunc

  func oTIPDOC_1_4.SetRadio()
    this.Parent.oContained.w_TIPDOC=trim(this.Parent.oContained.w_TIPDOC)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDOC=='C',1,;
      iif(this.Parent.oContained.w_TIPDOC=='S',2,;
      0))
  endfunc

  add object oDATFIN_1_7 as StdField with uid="BVGYRJZJFT",rtseq=7,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data di inizio selezione superiore a quella di fine selezione",;
    ToolTipText = "Data fine selezione",;
    HelpContextID = 166791734,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=98, Top=39

  proc oDATFIN_1_7.mDefault
    with this.Parent.oContained
      if empty(.w_DATFIN)
        .w_DATFIN = .w_DATINI
      endif
    endwith
  endproc

  func oDATFIN_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_DATINI<=.w_DATFIN) OR EMPTY(.w_DATINI))
    endwith
    return bRes
  endfunc

  add object oNUMFIN_1_8 as StdField with uid="DPSYRGCCQA",rtseq=8,rtrep=.f.,;
    cFormVar = "w_NUMFIN", cQueryName = "NUMFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Numero di inizio selezione superiore a quello di fine selezione",;
    ToolTipText = "Numero documento di fine selezione",;
    HelpContextID = 166768342,;
   bGlobalFont=.t.,;
    Height=21, Width=69, Left=276, Top=39, cSayPict='"999999"', cGetPict='"999999"'

  proc oNUMFIN_1_8.mDefault
    with this.Parent.oContained
      if empty(.w_NUMFIN)
        .w_NUMFIN = .w_NUMINI
      endif
    endwith
  endproc

  func oNUMFIN_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_NUMINI<=.w_NUMFIN) OR EMPTY(.w_NUMINI))
    endwith
    return bRes
  endfunc

  add object oALFFIN_1_9 as StdField with uid="PYSJNUVMIA",rtseq=9,rtrep=.f.,;
    cFormVar = "w_ALFFIN", cQueryName = "ALFFIN",;
    bObbl = .f. , nPag = 1, value=space(2), bMultilanguage =  .f.,;
    sErrorMsg = "Seriale di inizio selezione superiore a quello di fine selezione",;
    HelpContextID = 166737158,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=359, Top=39, InputMask=replicate('X',2)

  proc oALFFIN_1_9.mDefault
    with this.Parent.oContained
      if empty(.w_ALFFIN)
        .w_ALFFIN = .w_ALFINI
      endif
    endwith
  endproc

  func oALFFIN_1_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_ALFINI<=.w_ALFFIN) OR EMPTY(.w_ALFINI))
    endwith
    return bRes
  endfunc


  add object oTIPSEL_1_10 as StdCombo with uid="PRITZOCVQQ",rtseq=10,rtrep=.f.,left=651,top=39,width=138,height=21;
    , ToolTipText = "Selezionare Packing List confermate o da confermare";
    , HelpContextID = 129880886;
    , cFormVar="w_TIPSEL",RowSource=""+"Tutte,"+"Confermate,"+"Da confermare", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPSEL_1_10.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'C',;
    iif(this.value =3,'D',;
    space(1)))))
  endfunc
  func oTIPSEL_1_10.GetRadio()
    this.Parent.oContained.w_TIPSEL = this.RadioValue()
    return .t.
  endfunc

  func oTIPSEL_1_10.SetRadio()
    this.Parent.oContained.w_TIPSEL=trim(this.Parent.oContained.w_TIPSEL)
    this.value = ;
      iif(this.Parent.oContained.w_TIPSEL=='T',1,;
      iif(this.Parent.oContained.w_TIPSEL=='C',2,;
      iif(this.Parent.oContained.w_TIPSEL=='D',3,;
      0)))
  endfunc

  func oTIPSEL_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDOC='C')
    endwith
   endif
  endfunc

  func oTIPSEL_1_10.mHide()
    with this.Parent.oContained
      return (.w_TIPDOC<>'C')
    endwith
  endfunc


  add object oTIPCON_1_12 as StdCombo with uid="BAALLSBZQW",rtseq=12,rtrep=.f.,left=98,top=68,width=110,height=21;
    , HelpContextID = 172872502;
    , cFormVar="w_TIPCON",RowSource=""+"Tutti,"+"Clienti,"+"Fornitori", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_12.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'C',;
    iif(this.value =3,'F',;
    space(1)))))
  endfunc
  func oTIPCON_1_12.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_12.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='T',1,;
      iif(this.Parent.oContained.w_TIPCON=='C',2,;
      iif(this.Parent.oContained.w_TIPCON=='F',3,;
      0)))
  endfunc

  func oTIPCON_1_12.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCLF)
        bRes2=.link_1_14('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODCLF_1_14 as StdField with uid="ZQIZPLVBFJ",rtseq=14,rtrep=.f.,;
    cFormVar = "w_CODCLF", cQueryName = "CODCLF",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice cliente/fornitore",;
    HelpContextID = 35461158,;
   bGlobalFont=.t.,;
    Height=21, Width=132, Left=210, Top=68, cSayPict="p_CLF", cGetPict="p_CLF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCLF"

  func oCODCLF_1_14.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCON<>'T')
    endwith
   endif
  endfunc

  func oCODCLF_1_14.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_14('Part',this)
      if .not. empty(.w_CODDES)
        bRes2=.link_1_17('Full')
      endif
    endwith
    return bRes
  endfunc

  proc oCODCLF_1_14.ecpDrop(oSource)
    this.Parent.oContained.link_1_14('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCLF_1_14.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCLF_1_14'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori",'',this.parent.oContained
  endproc
  proc oCODCLF_1_14.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCLF
     i_obj.ecpSave()
  endproc

  add object oCODVET_1_15 as StdField with uid="MMGFOMWOBV",rtseq=15,rtrep=.f.,;
    cFormVar = "w_CODVET", cQueryName = "CODVET",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Selezione codice vettore",;
    HelpContextID = 264247334,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=98, Top=96, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="VETTORI", cZoomOnZoom="GSAR_AVT", oKey_1_1="VTCODVET", oKey_1_2="this.w_CODVET"

  func oCODVET_1_15.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_15('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODVET_1_15.ecpDrop(oSource)
    this.Parent.oContained.link_1_15('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODVET_1_15.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VETTORI','*','VTCODVET',cp_AbsName(this.parent,'oCODVET_1_15'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVT',"Vettori",'',this.parent.oContained
  endproc
  proc oCODVET_1_15.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVT()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VTCODVET=this.parent.oContained.w_CODVET
     i_obj.ecpSave()
  endproc

  add object oCODSPE_1_16 as StdField with uid="MBFEFFWCCG",rtseq=16,rtrep=.f.,;
    cFormVar = "w_CODSPE", cQueryName = "CODSPE",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Selezione codice spedizione",;
    HelpContextID = 23926822,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=487, Top=96, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="MODASPED", cZoomOnZoom="GSAR_ASP", oKey_1_1="SPCODSPE", oKey_1_2="this.w_CODSPE"

  func oCODSPE_1_16.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_16('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODSPE_1_16.ecpDrop(oSource)
    this.Parent.oContained.link_1_16('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODSPE_1_16.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'MODASPED','*','SPCODSPE',cp_AbsName(this.parent,'oCODSPE_1_16'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ASP',"Spedizioni",'',this.parent.oContained
  endproc
  proc oCODSPE_1_16.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ASP()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_SPCODSPE=this.parent.oContained.w_CODSPE
     i_obj.ecpSave()
  endproc

  add object oCODDES_1_17 as StdField with uid="NBYNPSWLGL",rtseq=17,rtrep=.f.,;
    cFormVar = "w_CODDES", cQueryName = "CODDES",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    sErrorMsg = "Codice destinazione inesistente",;
    ToolTipText = "Selezione codice destinazione",;
    HelpContextID = 246290470,;
   bGlobalFont=.t.,;
    Height=21, Width=62, Left=98, Top=125, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="DES_DIVE", oKey_1_1="DDTIPCON", oKey_1_2="this.w_TIPCLF", oKey_2_1="DDCODICE", oKey_2_2="this.w_CODCLF", oKey_3_1="DDCODDES", oKey_3_2="this.w_CODDES"

  func oCODDES_1_17.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_CODCLF))
    endwith
   endif
  endfunc

  proc oCODDES_1_17.mAfter
    with this.Parent.oContained
      .w_CODDES=chkdesdiv(.w_CODDES, .w_TIPCLF, .w_CODCLF)
    endwith
  endproc

  func oCODDES_1_17.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_17('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODDES_1_17.ecpDrop(oSource)
    this.Parent.oContained.link_1_17('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODDES_1_17.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.DES_DIVE_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCLF)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStrODBC(this.Parent.oContained.w_CODCLF)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCLF)
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"DDCODICE="+cp_ToStr(this.Parent.oContained.w_CODCLF)
    endif
    do cp_zoom with 'DES_DIVE','*','DDTIPCON,DDCODICE,DDCODDES',cp_AbsName(this.parent,'oCODDES_1_17'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Destinazioni",'',this.parent.oContained
  endproc


  add object oBtn_1_18 as StdButton with uid="VIIDLKOSDX",left=737, top=125, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la ricerca con le nuove selezioni";
    , HelpContextID = 241097962;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        GSMA_BPK(this.Parent.oContained,"R")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oSELEZI_1_19 as StdRadio with uid="VNICEXLLTR",rtseq=18,rtrep=.f.,left=4, top=422, width=136,height=32;
    , tabstop=.f.;
    , ToolTipText = "Seleziona/deseleziona i documenti";
    , cFormVar="w_SELEZI", ButtonCount=2, bObbl=.f., nPag=1;
  , bGlobalFont=.t.

    proc oSELEZI_1_19.init
      local i_coord
      i_coord = 0
      this.Buttons(1).Caption="Seleziona tutti"
      this.Buttons(1).HelpContextID = 100634406
      this.Buttons(1).Top=0
      this.Buttons(2).Caption="Deseleziona tutti"
      this.Buttons(2).HelpContextID = 100634406
      this.Buttons(2).Top=15
      this.SetAll("Width",134)
      this.SetAll("Height",17)
      this.SetAll("BackStyle",0)
      this.SetAll("ToolTipText", "Seleziona/deseleziona i documenti")
      StdRadio::init()
    endproc

  func oSELEZI_1_19.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    space(1))))
  endfunc
  func oSELEZI_1_19.GetRadio()
    this.Parent.oContained.w_SELEZI = this.RadioValue()
    return .t.
  endfunc

  func oSELEZI_1_19.SetRadio()
    this.Parent.oContained.w_SELEZI=trim(this.Parent.oContained.w_SELEZI)
    this.value = ;
      iif(this.Parent.oContained.w_SELEZI=='S',1,;
      iif(this.Parent.oContained.w_SELEZI=='D',2,;
      0))
  endfunc


  add object oBtn_1_21 as StdButton with uid="SWNOKMTEFZ",left=160, top=422, width=48,height=45,;
    CpPicture="BMP\DOCUMENTI.BMP ", caption="", nPag=1;
    , ToolTipText = "Premere per accedere al documento di origine";
    , HelpContextID = 56700826;
    , Caption='\<Docum.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        GSMA_BPK(this.Parent.oContained,"D",.w_SERIAL, .w_PARAME)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIAL))
      endwith
    endif
  endfunc


  add object oBtn_1_22 as StdButton with uid="MAGZWXRXKT",left=211, top=422, width=48,height=45,;
    CpPicture="bmp\elimina.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per eliminare la Packing List sul documento selezionato";
    , HelpContextID = 65436346;
    , Caption='\<Elimina';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        GSMA_BPK(this.Parent.oContained,"E")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPDOC='C')
      endwith
    endif
  endfunc

  func oBtn_1_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TIPDOC='S')
     endwith
    endif
  endfunc


  add object oBtn_1_23 as StdButton with uid="MPAIYXZMXP",left=262, top=422, width=48,height=45,;
    CpPicture="BMP\STAMPA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per stampare la Packing List";
    , HelpContextID = 7795162;
    , Caption='S\<tampa';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      with this.Parent.oContained
        GSMA_BPK(this.Parent.oContained,"F")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPDOC='C')
      endwith
    endif
  endfunc

  func oBtn_1_23.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TIPDOC='S')
     endwith
    endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="TLHJSBKGMU",left=684, top=422, width=48,height=45,;
    CpPicture="bmp\auto.bmp", caption="", nPag=1;
    , ToolTipText = "Se selezionato record genera Packing List altrimenti manutenzione Packing List";
    , HelpContextID = 6173082;
    , Caption='\<Genera';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        do GSMA_BP1 with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPDOC='S')
      endwith
    endif
  endfunc

  func oBtn_1_24.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TIPDOC='C')
     endwith
    endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="MSTLUBLVZR",left=313, top=422, width=48,height=45,;
    CpPicture="bmp\Caratter.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per entrare nella Packing List del documento selezionato";
    , HelpContextID = 210942166;
    , Caption='\<Pack.List';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        GSMA_BPK(this.Parent.oContained,"P",.w_PLSERIAL)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (.w_TIPDOC='C')
      endwith
    endif
  endfunc

  func oBtn_1_25.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_TIPDOC='S')
     endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="VRONCUQWAW",left=737, top=422, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 126168134;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCLF_1_36 as StdField with uid="EUTQGRTTFR",rtseq=20,rtrep=.f.,;
    cFormVar = "w_DESCLF", cQueryName = "DESCLF",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 35520054,;
   bGlobalFont=.t.,;
    Height=21, Width=293, Left=347, Top=68, InputMask=replicate('X',40)

  add object oDESVET_1_37 as StdField with uid="LPSWCEGSXH",rtseq=21,rtrep=.f.,;
    cFormVar = "w_DESVET", cQueryName = "DESVET",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 264306230,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=162, Top=96, InputMask=replicate('X',35)

  add object oNOMDES_1_40 as StdField with uid="XBDOGUMVUD",rtseq=22,rtrep=.f.,;
    cFormVar = "w_NOMDES", cQueryName = "NOMDES",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 246327510,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=162, Top=125, InputMask=replicate('X',35)

  add object oDESSPE_1_41 as StdField with uid="ADVHPHYJRQ",rtseq=23,rtrep=.f.,;
    cFormVar = "w_DESSPE", cQueryName = "DESSPE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 23985718,;
   bGlobalFont=.t.,;
    Height=21, Width=258, Left=531, Top=96, InputMask=replicate('X',35)


  add object ZoomDoc as cp_szoombox with uid="ZTZHSWISKN",left=-5, top=170, width=810,height=250,;
    caption='Object',;
   bGlobalFont=.t.,;
    cZoomFile="PACKING",bOptions=.f.,bAdvOptions=.f.,bReadOnly=.f.,bQueryOnLoad=.f.,cTable="PAC_MAST",;
    cEvent = "Ricerca",;
    nPag=1;
    , ToolTipText = "Documenti con o senza Packing List associata";
    , HelpContextID = 240022554


  add object oObj_1_47 as cp_runprogram with uid="PQANNDANBN",left=20, top=474, width=227,height=19,;
    caption='GSMA_BPK(S)',;
   bGlobalFont=.t.,;
    prg="GSMA_BPK('S')",;
    cEvent = "w_SELEZI Changed",;
    nPag=1;
    , HelpContextID = 256807473

  add object oStr_1_27 as StdString with uid="NGWCXSSTBT",Visible=.t., Left=9, Top=10,;
    Alignment=1, Width=87, Height=18,;
    Caption="Da data doc:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="ONDYELNNIY",Visible=.t., Left=28, Top=39,;
    Alignment=1, Width=68, Height=18,;
    Caption="a data doc:"  ;
  , bGlobalFont=.t.

  add object oStr_1_29 as StdString with uid="CZAZYTEYXJ",Visible=.t., Left=190, Top=10,;
    Alignment=1, Width=83, Height=18,;
    Caption="Da numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="KFNQZVDYTC",Visible=.t., Left=213, Top=39,;
    Alignment=1, Width=60, Height=18,;
    Caption="a numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_31 as StdString with uid="HNIXVPJGZU",Visible=.t., Left=350, Top=12,;
    Alignment=0, Width=13, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="FYSWZEJFVJ",Visible=.t., Left=350, Top=42,;
    Alignment=0, Width=14, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_33 as StdString with uid="VSPWYCHDQZ",Visible=.t., Left=572, Top=10,;
    Alignment=1, Width=76, Height=18,;
    Caption="Documenti:"  ;
  , bGlobalFont=.t.

  add object oStr_1_34 as StdString with uid="NPHDNYGOXD",Visible=.t., Left=521, Top=39,;
    Alignment=1, Width=127, Height=18,;
    Caption="Selezione Packing List:"  ;
  , bGlobalFont=.t.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (.w_TIPDOC<>'C')
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="RIJLBFNMFA",Visible=.t., Left=23, Top=68,;
    Alignment=1, Width=72, Height=18,;
    Caption="Intestatario:"  ;
  , bGlobalFont=.t.

  add object oStr_1_38 as StdString with uid="CUKAAHKLXY",Visible=.t., Left=54, Top=96,;
    Alignment=1, Width=43, Height=18,;
    Caption="Vettore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_39 as StdString with uid="DCQNVLFWZA",Visible=.t., Left=20, Top=125,;
    Alignment=1, Width=75, Height=18,;
    Caption="Destinazione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="XYCWQUKHCW",Visible=.t., Left=423, Top=96,;
    Alignment=1, Width=64, Height=18,;
    Caption="Spedizione:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="NHQJGSHCLY",Visible=.t., Left=485, Top=139,;
    Alignment=0, Width=188, Height=17,;
    Caption="In blu: Packing List da confermare"    , ForeColor=RGB(0,0,255);
  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (.w_TIPDOC<>'C')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsma_kpk','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
