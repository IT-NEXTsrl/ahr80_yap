* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bge                                                        *
*              Controlli in cancellazione agenti                               *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_63]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-07-18                                                      *
* Last revis.: 2013-03-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipo
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bge",oParentObject,m.pTipo)
return(i_retval)

define class tgsar_bge as StdBatch
  * --- Local variables
  pTipo = space(1)
  w_AGDTOBS1 = ctod("  /  /  ")
  w_TESAGE = .f.
  w_MESS = space(100)
  w_TIPDOC = space(5)
  w_Messaggio = space(10)
  w_SERIAL = space(10)
  w_ERRORE = .f.
  * --- WorkFile variables
  DOC_MAST_idx=0
  MOP_MAST_idx=0
  OFF_NOMI_idx=0
  AGENTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da Gsar_Age (Anagrafica Agenti)  esegue Controlli in Cancellazione  e sulla data d'obsolescenza
    do case
      case this.pTipo="C"
        * --- Controlli per la cancellazione
        this.w_TESAGE = .T.
        * --- Select from DOC_MAST
        i_nConn=i_TableProp[this.DOC_MAST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DOC_MAST ";
              +" where MVCODAGE="+cp_ToStrODBC(this.oParentObject.w_AGCODAGE)+" OR MVCODAG2= "+cp_ToStrODBC(this.oParentObject.w_AGCODAGE)+"";
               ,"_Curs_DOC_MAST")
        else
          select * from (i_cTable);
           where MVCODAGE=this.oParentObject.w_AGCODAGE OR MVCODAG2= this.oParentObject.w_AGCODAGE;
            into cursor _Curs_DOC_MAST
        endif
        if used('_Curs_DOC_MAST')
          select _Curs_DOC_MAST
          locate for 1=1
          do while not(eof())
          this.w_TESAGE = .F.
          if EMPTY(this.w_TIPDOC)
            this.w_TIPDOC = _Curs_DOC_MAST.MVTIPDOC
            if not empty(nvl(_Curs_DOC_MAST.MVALFDOC,SPACE(10)))
              this.w_MESS = ah_MsgFormat("%1 numero: %2/%3 del: %4", alltrim(this.w_TIPDOC), alltrim(str(_Curs_DOC_MAST.MVNUMDOC,15)), ALLTRIM(_Curs_DOC_MAST.MVALFDOC), dtoc(_Curs_DOC_MAST.MVDATREG))
            else
              this.w_MESS = ah_MsgFormat("%1 numero: %2 del: %3", alltrim(this.w_TIPDOC), alltrim(str(_Curs_DOC_MAST.MVNUMDOC,15)), dtoc(_Curs_DOC_MAST.MVDATREG))
            endif
          endif
            select _Curs_DOC_MAST
            continue
          enddo
          use
        endif
        this.w_ERRORE = .F.
        if this.w_TESAGE
          * --- Select from OFF_NOMI
          i_nConn=i_TableProp[this.OFF_NOMI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.OFF_NOMI_idx,2],.t.,this.OFF_NOMI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT(NOCODICE) AS COUNT  from "+i_cTable+" OFF_NOMI ";
                +" where NOCODAGE="+cp_ToStrODBC(this.oParentObject.w_AGCODAGE)+"";
                 ,"_Curs_OFF_NOMI")
          else
            select COUNT(NOCODICE) AS COUNT from (i_cTable);
             where NOCODAGE=this.oParentObject.w_AGCODAGE;
              into cursor _Curs_OFF_NOMI
          endif
          if used('_Curs_OFF_NOMI')
            select _Curs_OFF_NOMI
            locate for 1=1
            do while not(eof())
            this.w_ERRORE = NVL(_Curs_OFF_NOMI.COUNT, 0) <> 0
              select _Curs_OFF_NOMI
              continue
            enddo
            use
          endif
          if this.w_ERRORE
            this.w_MESS = ah_MsgFormat("Impossibile cancellare, agente/capo area presente nei nominativi")
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
          endif
        endif
        if this.w_TESAGE
          * --- Select from AGENTI
          i_nConn=i_TableProp[this.AGENTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.AGENTI_idx,2],.t.,this.AGENTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select COUNT(AGCODAGE) AS COUNT  from "+i_cTable+" AGENTI ";
                +" where AGCZOAGE="+cp_ToStrODBC(this.oParentObject.w_AGCODAGE)+"";
                 ,"_Curs_AGENTI")
          else
            select COUNT(AGCODAGE) AS COUNT from (i_cTable);
             where AGCZOAGE=this.oParentObject.w_AGCODAGE;
              into cursor _Curs_AGENTI
          endif
          if used('_Curs_AGENTI')
            select _Curs_AGENTI
            locate for 1=1
            do while not(eof())
            this.w_ERRORE = NVL(_Curs_AGENTI.COUNT, 0) <> 0
              select _Curs_AGENTI
              continue
            enddo
            use
          endif
          if this.w_ERRORE
            this.w_MESS = ah_MsgFormat("Impossibile cancellare, capo area presente in anagrafica agenti")
            * --- transaction error
            bTrsErr=.t.
            i_TrsMsg=this.w_MESS
          endif
        endif
        if NOT this.w_TESAGE
          this.w_MESS = ah_MsgFormat("Impossibile cancellare, agente/capo area presente nel documento: %1", alltrim(this.w_MESS))
          * --- transaction error
          bTrsErr=.t.
          i_TrsMsg=this.w_MESS
        endif
      case this.pTipo="O"
        if this.oParentObject.w_AGDTOBSO<>this.oParentObject.w_OLDTOBSO
          this.w_AGDTOBS1 = IIF(EMPTY(this.oParentObject.w_AGDTOBSO),i_INIDAT, CP_TODATE(this.oParentObject.w_AGDTOBSO))
          * --- Controlli per l'obsolescenza e la sospensione dei movimenti di provvigione
          if (this.w_AGDTOBS1<>i_INIDAT) 
            * --- Verifica se ci sono movimenti da sospendere
            vq_exec("QUERY\GSAR2BGE.VQR",this,"NOLIQ")
            if RECCOUNT("NOLIQ") > 0
              this.w_Messaggio = "Attenzione%0Vuoi sospendere i movimenti provvigione ancora non liquidati per l'agente%0con data registrazione maggiore o uguale alla data d'obsolescenza?%0"
              if ah_YesNo(this.w_Messaggio)
                * --- Try
                local bErr_03D708E0
                bErr_03D708E0=bTrsErr
                this.Try_03D708E0()
                * --- Catch
                if !empty(i_Error)
                  i_ErrMsg=i_Error
                  i_Error=''
                  * --- rollback
                  bTrsErr=.t.
                  cp_EndTrs(.t.)
                  ah_ErrorMsg("Impossibile effettuare l'aggiornamento%0Operazione annullata",,"")
                endif
                bTrsErr=bTrsErr or bErr_03D708E0
                * --- End
              endif
            endif
          endif
          * --- Verifica se ci sono movimenti da confermare
          this.w_AGDTOBS1 = IIF(this.w_AGDTOBS1= i_INIDAT, i_FINDAT,this.w_AGDTOBS1)
          vq_exec("QUERY\GSAR_BGE.VQR",this,"NOLIQ1")
          if RECCOUNT("NOLIQ1") > 0
            if this.w_AGDTOBS1= i_FINDAT
              this.w_Messaggio = "Attenzione%0Vuoi confermare i movimenti provvigione ancora non liquidati per l'agente?%0"
            else
              this.w_Messaggio = "Attenzione%0Vuoi confermare i movimenti provvigione ancora non liquidati per l'agente%0con data registrazione minore della data d'obsolescenza?%0"
            endif
            if ah_YesNo(this.w_Messaggio)
              * --- Try
              local bErr_03EDFC00
              bErr_03EDFC00=bTrsErr
              this.Try_03EDFC00()
              * --- Catch
              if !empty(i_Error)
                i_ErrMsg=i_Error
                i_Error=''
                * --- rollback
                bTrsErr=.t.
                cp_EndTrs(.t.)
                ah_ErrorMsg("Impossibile effettuare l'aggiornamento%0Operazione annullata",,"")
              endif
              bTrsErr=bTrsErr or bErr_03EDFC00
              * --- End
            endif
          endif
          if used("NOLIQ")
            select NOLIQ
            use
          endif
          if used("NOLIQ1")
            select NOLIQ1
            use
          endif
        endif
      case this.pTipo="M"
        * --- memorizza la data iniziale in una var. d'appoggio
        this.oParentObject.w_OLDTOBSO = this.oParentObject.w_AGDTOBSO
    endcase
  endproc
  proc Try_03D708E0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    SELECT NOLIQ
    GO TOP
    SCAN FOR NOT EMPTY(MPSERIAL)
    this.w_SERIAL = NVL(MPSERIAL,SPACE(10))
    * --- Write into MOP_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOP_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOP_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MOP_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MPAGSOSP ="+cp_NullLink(cp_ToStrODBC("S"),'MOP_MAST','MPAGSOSP');
          +i_ccchkf ;
      +" where ";
          +"MPSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             )
    else
      update (i_cTable) set;
          MPAGSOSP = "S";
          &i_ccchkf. ;
       where;
          MPSERIAL = this.w_SERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ENDSCAN
    ah_ErrorMsg("Operazione completata",,"")
    return
  proc Try_03EDFC00()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    SELECT NOLIQ1
    GO TOP
    SCAN FOR NOT EMPTY(MPSERIAL)
    this.w_SERIAL = NVL(MPSERIAL,SPACE(10))
    * --- Write into MOP_MAST
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.MOP_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.MOP_MAST_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.MOP_MAST_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"MPAGSOSP ="+cp_NullLink(cp_ToStrODBC("C"),'MOP_MAST','MPAGSOSP');
          +i_ccchkf ;
      +" where ";
          +"MPSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             )
    else
      update (i_cTable) set;
          MPAGSOSP = "C";
          &i_ccchkf. ;
       where;
          MPSERIAL = this.w_SERIAL;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    ENDSCAN
    ah_ErrorMsg("Operazione completata",,"")
    return


  proc Init(oParentObject,pTipo)
    this.pTipo=pTipo
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='MOP_MAST'
    this.cWorkTables[3]='OFF_NOMI'
    this.cWorkTables[4]='AGENTI'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_DOC_MAST')
      use in _Curs_DOC_MAST
    endif
    if used('_Curs_OFF_NOMI')
      use in _Curs_OFF_NOMI
    endif
    if used('_Curs_AGENTI')
      use in _Curs_AGENTI
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipo"
endproc
