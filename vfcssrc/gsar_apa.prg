* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_apa                                                        *
*              Pagamenti                                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1992-05-25                                                      *
* Last revis.: 2017-07-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_apa"))

* --- Class definition
define class tgsar_apa as StdForm
  Top    = 17
  Left   = 24

  * --- Standard Properties
  Width  = 656
  Height = 364+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2017-07-14"
  HelpContextID=160003945
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=27

  * --- Constant Properties
  PAG_AMEN_IDX = 0
  PAG_2AME_IDX = 0
  LINGUE_IDX = 0
  VALUTE_IDX = 0
  cFile = "PAG_AMEN"
  cKeySelect = "PACODICE"
  cKeyWhere  = "PACODICE=this.w_PACODICE"
  cKeyWhereODBC = '"PACODICE="+cp_ToStrODBC(this.w_PACODICE)';

  cKeyWhereODBCqualified = '"PAG_AMEN.PACODICE="+cp_ToStrODBC(this.w_PACODICE)';

  cPrg = "gsar_apa"
  cComment = "Pagamenti"
  icon = "anag.ico"
  cAutoZoom = 'GSAR0APA'
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_PACODICE = space(5)
  w_PADESCRI = space(30)
  w_PASCONTO = 0
  w_PASCOFIN = 0
  w_PAVALINC = space(3)
  o_PAVALINC = space(3)
  w_PASPEINC = 0
  o_PASPEINC = 0
  w_PASPEINA = 0
  w_PAVALIN2 = space(3)
  o_PAVALIN2 = space(3)
  w_PASPEIN2 = 0
  o_PASPEIN2 = 0
  w_PASPEIA2 = 0
  w_PAINCASS = space(1)
  o_PAINCASS = space(1)
  w_PASALDO = space(1)
  w_PAANNCIV = space(1)
  w_PAFLFMDP = space(1)
  w_UTCC = 0
  w_UTCV = 0
  w_UTDC = ctot('')
  w_UTDV = ctot('')
  w_PADTOBSO = ctod('  /  /  ')
  w_PADTINVA = ctod('  /  /  ')
  w_DESAPP = space(35)
  w_SIMVAL = space(5)
  w_SIMVAL2 = space(5)
  w_DECTOT = 0
  w_DECTOT2 = 0
  w_CALCPICT = 0
  w_CALCPICP = 0

  * --- Children pointers
  GSAR_MPA = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_AddExtFldsTab(this,'PAG_AMEN','gsar_apa')
    stdPageFrame::Init()
    *set procedure to GSAR_MPA additive
    with this
      .Pages(1).addobject("oPag","tgsar_apaPag1","gsar_apa",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Generali")
      .Pages(1).HelpContextID = 16592177
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oPACODICE_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    local qf
    QueryFilter(@qf)
    this.parent.cQueryFilter=qf
    *release procedure GSAR_MPA
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
    this.parent.resize()
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[4]
    this.cWorkTables[1]='PAG_2AME'
    this.cWorkTables[2]='LINGUE'
    this.cWorkTables[3]='VALUTE'
    this.cWorkTables[4]='PAG_AMEN'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(4))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.PAG_AMEN_IDX,5],7]
    this.nPostItConn=i_TableProp[this.PAG_AMEN_IDX,3]
  return

  function CreateChildren()
    this.GSAR_MPA = CREATEOBJECT('stdDynamicChild',this,'GSAR_MPA',this.oPgFrm.Page1.oPag.oLinkPC_1_16)
    this.GSAR_MPA.createrealchild()
    return

  procedure DestroyChildren()
    if !ISNULL(this.GSAR_MPA)
      this.GSAR_MPA.DestroyChildrenChain()
      this.GSAR_MPA=.NULL.
    endif
    this.oPgFrm.Page1.oPag.RemoveObject('oLinkPC_1_16')
    return

  function IsAChildUpdated(bDetail)
    local i_bRes
    i_bRes = this.bUpdated
    if vartype(i_bOldIsAChildUpdated)='L' and i_bOldIsAChildUpdated
      i_bRes = i_bRes .or. this.GSAR_MPA.IsAChildUpdated()
    else
      i_bRes = i_bRes .or. this.GSAR_MPA.IsAChildUpdated(.t.)
    endif
    return(i_bRes)

  procedure ChildrenNewDocument()
    this.GSAR_MPA.NewDocument()
    return

  procedure SetChildrenKeys()
    with this
      this.GSAR_MPA.SetKey(;
            .w_PACODICE,"P2CODICE";
            )
    endwith
    return

  procedure ChildrenChangeRow()
    with this
      .GSAR_MPA.ChangeRow(this.cRowID+'      1',1;
             ,.w_PACODICE,"P2CODICE";
             )
    endwith
    return

  function AddSonsFilter(i_cFlt,i_oTopObject)
    local i_f,i_fnidx,i_cDatabaseType
    with this
      if !IsNull(.GSAR_MPA)
        i_f=.GSAR_MPA.BuildFilter()
        if !(i_f==.GSAR_MPA.cQueryFilter)
          i_fnidx=.GSAR_MPA.cFile+'_IDX'
          i_cDatabaseType = i_ServerConn[i_TableProp[.GSAR_MPA.&i_fnidx.,5],6]
          i_f='('+cp_FixSubqueryKey(i_oTopObject.cKeySelect,i_cDatabaseType)+') in (select '+cp_FixSubqueryKey(cp_PartialKey(.GSAR_MPA.cKeySelect,i_oTopObject.cKeySelect),i_cDatabaseType)+' from '+cp_SetAzi(i_TableProp[.GSAR_MPA.&i_fnidx.,2])+' where '+i_f+')'
        else
          i_f=''
        endif
        i_f=.GSAR_MPA.AddSonsFilter(i_f,i_oTopObject)
        if !empty(i_f)
          i_cFlt=i_cFlt+iif(Empty(i_cFlt),'',' AND ')+i_f
        endif
      endif
    endwith
    return(i_cFlt)

  procedure SetWorkFromKeySet()
    * --- Initialize work variables from KeySet. They will be used to load the record
    select (this.cKeySet)
    with this
      .w_PACODICE = NVL(PACODICE,space(5))
    endwith
  endproc

  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    local link_1_5_joined
    link_1_5_joined=.f.
    local link_1_8_joined
    link_1_8_joined=.f.
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from PAG_AMEN where PACODICE=KeySet.PACODICE
    *
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('PAG_AMEN')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "PAG_AMEN.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' PAG_AMEN '
      link_1_5_joined=this.AddJoinedLink_1_5(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      link_1_8_joined=this.AddJoinedLink_1_8(@i_cSel,@i_cTable,@i_cKey,i_nConn,i_cDatabaseType,@i_nFlds)
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'PACODICE',this.w_PACODICE  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_DESAPP = space(35)
        .w_DECTOT = 0
        .w_DECTOT2 = 0
        .w_PACODICE = NVL(PACODICE,space(5))
        .w_PADESCRI = NVL( cp_TransLoadField('PADESCRI') ,space(30))
        .w_PASCONTO = NVL(PASCONTO,0)
        .w_PASCOFIN = NVL(PASCOFIN,0)
        .w_PAVALINC = NVL(PAVALINC,space(3))
          if link_1_5_joined
            this.w_PAVALINC = NVL(VACODVAL105,NVL(this.w_PAVALINC,space(3)))
            this.w_DESAPP = NVL(VADESVAL105,space(35))
            this.w_SIMVAL = NVL(VASIMVAL105,space(5))
            this.w_DECTOT = NVL(VADECTOT105,0)
          else
          .link_1_5('Load')
          endif
        .w_PASPEINC = NVL(PASPEINC,0)
        .w_PASPEINA = NVL(PASPEINA,0)
        .w_PAVALIN2 = NVL(PAVALIN2,space(3))
          if link_1_8_joined
            this.w_PAVALIN2 = NVL(VACODVAL108,NVL(this.w_PAVALIN2,space(3)))
            this.w_DESAPP = NVL(VADESVAL108,space(35))
            this.w_SIMVAL2 = NVL(VASIMVAL108,space(5))
            this.w_DECTOT2 = NVL(VADECTOT108,0)
          else
          .link_1_8('Load')
          endif
        .w_PASPEIN2 = NVL(PASPEIN2,0)
        .w_PASPEIA2 = NVL(PASPEIA2,0)
        .w_PAINCASS = NVL(PAINCASS,space(1))
        .w_PASALDO = NVL(PASALDO,space(1))
        .w_PAANNCIV = NVL(PAANNCIV,space(1))
        .w_PAFLFMDP = NVL(PAFLFMDP,space(1))
        .w_UTCC = NVL(UTCC,0)
        .w_UTCV = NVL(UTCV,0)
        .w_UTDC = NVL(UTDC,ctot(""))
        .w_UTDV = NVL(UTDV,ctot(""))
        .w_PADTOBSO = NVL(cp_ToDate(PADTOBSO),ctod("  /  /  "))
        .w_PADTINVA = NVL(cp_ToDate(PADTINVA),ctod("  /  /  "))
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .w_SIMVAL = IIF(EMPTY(.w_PAVALINC),' ',.w_SIMVAL)
        .w_SIMVAL2 = IIF(EMPTY(.w_PAVALIN2),' ',.w_SIMVAL2)
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CALCPICP = DEFPIP(.w_DECTOT2)
        cp_LoadRecExtFlds(this,'PAG_AMEN')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PACODICE = space(5)
      .w_PADESCRI = space(30)
      .w_PASCONTO = 0
      .w_PASCOFIN = 0
      .w_PAVALINC = space(3)
      .w_PASPEINC = 0
      .w_PASPEINA = 0
      .w_PAVALIN2 = space(3)
      .w_PASPEIN2 = 0
      .w_PASPEIA2 = 0
      .w_PAINCASS = space(1)
      .w_PASALDO = space(1)
      .w_PAANNCIV = space(1)
      .w_PAFLFMDP = space(1)
      .w_UTCC = 0
      .w_UTCV = 0
      .w_UTDC = ctot("")
      .w_UTDV = ctot("")
      .w_PADTOBSO = ctod("  /  /  ")
      .w_PADTINVA = ctod("  /  /  ")
      .w_DESAPP = space(35)
      .w_SIMVAL = space(5)
      .w_SIMVAL2 = space(5)
      .w_DECTOT = 0
      .w_DECTOT2 = 0
      .w_CALCPICT = 0
      .w_CALCPICP = 0
      if .cFunction<>"Filter"
          .DoRTCalc(1,4,.f.)
        .w_PAVALINC = g_PERVAL
        .DoRTCalc(5,5,.f.)
          if not(empty(.w_PAVALINC))
          .link_1_5('Full')
          endif
        .w_PASPEINC = iif(empty(.w_PAVALINC),0,.w_PASPEINC)
        .w_PASPEINA = iif(empty(.w_PAVALINC),0,.w_PASPEINA)
        .DoRTCalc(8,8,.f.)
          if not(empty(.w_PAVALIN2))
          .link_1_8('Full')
          endif
        .w_PASPEIN2 = iif(empty(.w_PAVALIN2),0,.w_PASPEIN2)
        .w_PASPEIA2 = iif(empty(.w_PAVALIN2),0,.w_PASPEIA2)
          .DoRTCalc(11,11,.f.)
        .w_PASALDO = IIF(.w_PAINCASS<>'S', 'N', .w_PASALDO)
        .w_PAANNCIV = ' '
        .w_PAFLFMDP = 'N'
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
          .DoRTCalc(15,21,.f.)
        .w_SIMVAL = IIF(EMPTY(.w_PAVALINC),' ',.w_SIMVAL)
        .w_SIMVAL2 = IIF(EMPTY(.w_PAVALIN2),' ',.w_SIMVAL2)
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
          .DoRTCalc(24,25,.f.)
        .w_CALCPICT = DEFPIC(.w_DECTOT)
        .w_CALCPICP = DEFPIP(.w_DECTOT2)
      endif
    endwith
    cp_BlankRecExtFlds(this,'PAG_AMEN')
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query"
    * --- Disable List page when <> from Query
    if type('this.oPgFrm.Pages(this.oPgfrm.PageCount).autozoom')='O'
      this.oPgFrm.Pages(this.oPgFrm.PageCount).Enabled=not(i_bVal)
    else
      local i_nPageCount
      for i_nPageCount=this.oPgFrm.PageCount to 1 step -1
        if type('this.oPgFrm.Pages(i_nPageCount).autozoom')='O'
          this.oPgFrm.Pages(i_nPageCount).Enabled=not(i_bVal)
          exit
        endif
      next
    endif
    with this.oPgFrm
      .Page1.oPag.oPACODICE_1_1.enabled = i_bVal
      .Page1.oPag.oPADESCRI_1_2.enabled = i_bVal
      .Page1.oPag.oPASCONTO_1_3.enabled = i_bVal
      .Page1.oPag.oPASCOFIN_1_4.enabled = i_bVal
      .Page1.oPag.oPAVALINC_1_5.enabled = i_bVal
      .Page1.oPag.oPASPEINC_1_6.enabled = i_bVal
      .Page1.oPag.oPASPEINA_1_7.enabled = i_bVal
      .Page1.oPag.oPAVALIN2_1_8.enabled = i_bVal
      .Page1.oPag.oPASPEIN2_1_9.enabled = i_bVal
      .Page1.oPag.oPASPEIA2_1_10.enabled = i_bVal
      .Page1.oPag.oPAINCASS_1_11.enabled = i_bVal
      .Page1.oPag.oPASALDO_1_12.enabled = i_bVal
      .Page1.oPag.oPAANNCIV_1_14.enabled = i_bVal
      .Page1.oPag.oPAFLFMDP_1_15.enabled = i_bVal
      .Page1.oPag.oPADTOBSO_1_22.enabled = i_bVal
      .Page1.oPag.oPADTINVA_1_27.enabled = i_bVal
      .Page1.oPag.oObj_1_33.enabled = i_bVal
      .Page1.oPag.oObj_1_34.enabled = i_bVal
      .Page1.oPag.oObj_1_40.enabled = i_bVal
      if i_cOp = "Edit"
        .Page1.oPag.oPACODICE_1_1.enabled = .f.
      endif
      if i_cOp = "Query"
        .Page1.oPag.oPACODICE_1_1.enabled = .t.
        .Page1.oPag.oPADESCRI_1_2.enabled = .t.
      endif
    endwith
    this.GSAR_MPA.SetStatus(i_cOp)
    cp_SetEnabledExtFlds(this,'PAG_AMEN',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *  this.GSAR_MPA.SetChildrenStatus(i_cOp)
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PACODICE,"PACODICE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADESCRI,"PADESCRI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PASCONTO,"PASCONTO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PASCOFIN,"PASCOFIN",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAVALINC,"PAVALINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PASPEINC,"PASPEINC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PASPEINA,"PASPEINA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAVALIN2,"PAVALIN2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PASPEIN2,"PASPEIN2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PASPEIA2,"PASPEIA2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAINCASS,"PAINCASS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PASALDO,"PASALDO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAANNCIV,"PAANNCIV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PAFLFMDP,"PAFLFMDP",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCC,"UTCC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTCV,"UTCV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDC,"UTDC",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_UTDV,"UTDV",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADTOBSO,"PADTOBSO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_PADTINVA,"PADTINVA",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Create cursor cKeySet with primary key only
  proc QueryKeySet(i_cWhere,i_cOrderBy)
    local i_cKey,i_cTable,i_lTable,i_nConn,i_cDatabaseType
    this.cLastWhere = i_cWhere
    this.cLastOrderBy = i_cOrderBy
    i_cWhere = iif(not(empty(i_cWhere)),' where '+i_cWhere,'')
    i_cOrderBy = iif(not(empty(i_cOrderBy)),' order by '+i_cOrderBy,'')
    i_cKey = this.cKeySelect
    * --- Area Manuale = Query Key Set Init
    * --- Fine Area Manuale
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    i_lTable = "PAG_AMEN"
    i_cDatabaseType = i_ServerConn[i_TableProp[this.PAG_AMEN_IDX,5],6]
    if i_nConn<>0
      LOCAL i_oldzr
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_oldzr=i_nZoomMaxRows
        i_nZoomMaxRows=10000
      ENDIF
      if !empty(i_cOrderBy) and at(substr(i_cOrderBy,11),i_cKey)=0
        i_cKey=i_cKey+','+substr(i_cOrderBy,11)
      endif
      thisform.__Sqlx__.SQLExec(i_nConn,"select "+i_cKey+" from "+i_cTable+" "+i_lTable+" "+i_cWhere+" "+i_cOrderBy,this.cKeySet,i_cDatabaseType)
      IF VARTYPE(i_nZoomMaxRows)='N'
        i_nZoomMaxRows=i_oldzr
      ENDIF
    else
      select &i_cKey from (i_cTable) as (i_lTable) &i_cWhere &i_cOrderBy into cursor (this.cKeySet)
      locate for 1=1 && per il problema dei cursori che riusano il file principale
    endif
    * --- Area Manuale = Query Key Set End
    * --- Fine Area Manuale
  endproc
  proc ecpPrint()
    if this.cFunction='Query'
      do GSAR_SPA with this
    endif
    return

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.PAG_AMEN_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into PAG_AMEN
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'PAG_AMEN')
        i_extval=cp_InsertValODBCExtFlds(this,'PAG_AMEN')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(PACODICE,PADESCRI"+cp_TransInsFldName('PADESCRI')+",PASCONTO,PASCOFIN,PAVALINC"+;
                  ",PASPEINC,PASPEINA,PAVALIN2,PASPEIN2,PASPEIA2"+;
                  ",PAINCASS,PASALDO,PAANNCIV,PAFLFMDP,UTCC"+;
                  ",UTCV,UTDC,UTDV,PADTOBSO,PADTINVA "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_PACODICE)+;
                  ","+cp_ToStrODBC(this.w_PADESCRI)+cp_TransInsFldValue(this.w_PADESCRI)+;
                  ","+cp_ToStrODBC(this.w_PASCONTO)+;
                  ","+cp_ToStrODBC(this.w_PASCOFIN)+;
                  ","+cp_ToStrODBCNull(this.w_PAVALINC)+;
                  ","+cp_ToStrODBC(this.w_PASPEINC)+;
                  ","+cp_ToStrODBC(this.w_PASPEINA)+;
                  ","+cp_ToStrODBCNull(this.w_PAVALIN2)+;
                  ","+cp_ToStrODBC(this.w_PASPEIN2)+;
                  ","+cp_ToStrODBC(this.w_PASPEIA2)+;
                  ","+cp_ToStrODBC(this.w_PAINCASS)+;
                  ","+cp_ToStrODBC(this.w_PASALDO)+;
                  ","+cp_ToStrODBC(this.w_PAANNCIV)+;
                  ","+cp_ToStrODBC(this.w_PAFLFMDP)+;
                  ","+cp_ToStrODBC(this.w_UTCC)+;
                  ","+cp_ToStrODBC(this.w_UTCV)+;
                  ","+cp_ToStrODBC(this.w_UTDC)+;
                  ","+cp_ToStrODBC(this.w_UTDV)+;
                  ","+cp_ToStrODBC(this.w_PADTOBSO)+;
                  ","+cp_ToStrODBC(this.w_PADTINVA)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'PAG_AMEN')
        i_extval=cp_InsertValVFPExtFlds(this,'PAG_AMEN')
        cp_CheckDeletedKey(i_cTable,0,'PACODICE',this.w_PACODICE)
        INSERT INTO (i_cTable);
              (PACODICE,PADESCRI,PASCONTO,PASCOFIN,PAVALINC,PASPEINC,PASPEINA,PAVALIN2,PASPEIN2,PASPEIA2,PAINCASS,PASALDO,PAANNCIV,PAFLFMDP,UTCC,UTCV,UTDC,UTDV,PADTOBSO,PADTINVA  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_PACODICE;
                  ,this.w_PADESCRI;
                  ,this.w_PASCONTO;
                  ,this.w_PASCOFIN;
                  ,this.w_PAVALINC;
                  ,this.w_PASPEINC;
                  ,this.w_PASPEINA;
                  ,this.w_PAVALIN2;
                  ,this.w_PASPEIN2;
                  ,this.w_PASPEIA2;
                  ,this.w_PAINCASS;
                  ,this.w_PASALDO;
                  ,this.w_PAANNCIV;
                  ,this.w_PAFLFMDP;
                  ,this.w_UTCC;
                  ,this.w_UTCV;
                  ,this.w_UTDC;
                  ,this.w_UTDV;
                  ,this.w_PADTOBSO;
                  ,this.w_PADTINVA;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.bUpdated and i_bEditing
      this.mRestoreTrs(.t.)
      this.mUpdateTrs(.t.)
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.PAG_AMEN_IDX,i_nConn)
      *
      * update PAG_AMEN
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'PAG_AMEN')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " PADESCRI="+cp_TransUpdFldName('PADESCRI',this.w_PADESCRI)+;
             ",PASCONTO="+cp_ToStrODBC(this.w_PASCONTO)+;
             ",PASCOFIN="+cp_ToStrODBC(this.w_PASCOFIN)+;
             ",PAVALINC="+cp_ToStrODBCNull(this.w_PAVALINC)+;
             ",PASPEINC="+cp_ToStrODBC(this.w_PASPEINC)+;
             ",PASPEINA="+cp_ToStrODBC(this.w_PASPEINA)+;
             ",PAVALIN2="+cp_ToStrODBCNull(this.w_PAVALIN2)+;
             ",PASPEIN2="+cp_ToStrODBC(this.w_PASPEIN2)+;
             ",PASPEIA2="+cp_ToStrODBC(this.w_PASPEIA2)+;
             ",PAINCASS="+cp_ToStrODBC(this.w_PAINCASS)+;
             ",PASALDO="+cp_ToStrODBC(this.w_PASALDO)+;
             ",PAANNCIV="+cp_ToStrODBC(this.w_PAANNCIV)+;
             ",PAFLFMDP="+cp_ToStrODBC(this.w_PAFLFMDP)+;
             ",UTCC="+cp_ToStrODBC(this.w_UTCC)+;
             ",UTCV="+cp_ToStrODBC(this.w_UTCV)+;
             ",UTDC="+cp_ToStrODBC(this.w_UTDC)+;
             ",UTDV="+cp_ToStrODBC(this.w_UTDV)+;
             ",PADTOBSO="+cp_ToStrODBC(this.w_PADTOBSO)+;
             ",PADTINVA="+cp_ToStrODBC(this.w_PADTINVA)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'PAG_AMEN')
        i_cWhere = cp_PKFox(i_cTable  ,'PACODICE',this.w_PACODICE  )
        UPDATE (i_cTable) SET;
              PADESCRI=this.w_PADESCRI;
             ,PASCONTO=this.w_PASCONTO;
             ,PASCOFIN=this.w_PASCOFIN;
             ,PAVALINC=this.w_PAVALINC;
             ,PASPEINC=this.w_PASPEINC;
             ,PASPEINA=this.w_PASPEINA;
             ,PAVALIN2=this.w_PAVALIN2;
             ,PASPEIN2=this.w_PASPEIN2;
             ,PASPEIA2=this.w_PASPEIA2;
             ,PAINCASS=this.w_PAINCASS;
             ,PASALDO=this.w_PASALDO;
             ,PAANNCIV=this.w_PAANNCIV;
             ,PAFLFMDP=this.w_PAFLFMDP;
             ,UTCC=this.w_UTCC;
             ,UTCV=this.w_UTCV;
             ,UTDC=this.w_UTDC;
             ,UTDV=this.w_UTDV;
             ,PADTOBSO=this.w_PADTOBSO;
             ,PADTINVA=this.w_PADTINVA;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
      * --- GSAR_MPA : Saving
      this.GSAR_MPA.ChangeRow(this.cRowID+'      1',0;
             ,this.w_PACODICE,"P2CODICE";
             )
      this.GSAR_MPA.mReplace()
    endif
    * --- Area Manuale = Replace End
    * --- gsar_apa
    if NOT bTrsErr
        * ---- Verifica esistenza Dettaglio Pagamenti
        this.NotifyEvent('ControlliFinali')
    endif
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- gsar_apa
    if isahe()
       this.NotifyEvent('ControlloCancellazione')
    else
       this.NotifyEvent('ControlloDelete')
    endif
    * --- Fine Area Manuale
    * --- GSAR_MPA : Deleting
    this.GSAR_MPA.ChangeRow(this.cRowID+'      1',0;
           ,this.w_PACODICE,"P2CODICE";
           )
    this.GSAR_MPA.mDelete()
    if not(bTrsErr)
      i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.PAG_AMEN_IDX,i_nConn)
      *
      * delete PAG_AMEN
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'PACODICE',this.w_PACODICE  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.PAG_AMEN_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.PAG_AMEN_IDX,2])
    if i_bUpd
      with this
        .DoRTCalc(1,4,.t.)
        if .o_PASPEINC<>.w_PASPEINC
          .link_1_5('Full')
        endif
        if .o_PAVALINC<>.w_PAVALINC
            .w_PASPEINC = iif(empty(.w_PAVALINC),0,.w_PASPEINC)
        endif
        if .o_PAVALINC<>.w_PAVALINC
            .w_PASPEINA = iif(empty(.w_PAVALINC),0,.w_PASPEINA)
        endif
        if .o_PASPEIN2<>.w_PASPEIN2
          .link_1_8('Full')
        endif
        if .o_PAVALIN2<>.w_PAVALIN2
            .w_PASPEIN2 = iif(empty(.w_PAVALIN2),0,.w_PASPEIN2)
        endif
        if .o_PAVALIN2<>.w_PAVALIN2
            .w_PASPEIA2 = iif(empty(.w_PAVALIN2),0,.w_PASPEIA2)
        endif
        .DoRTCalc(11,11,.t.)
        if .o_PAINCASS<>.w_PAINCASS
            .w_PASALDO = IIF(.w_PAINCASS<>'S', 'N', .w_PASALDO)
        endif
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .DoRTCalc(13,21,.t.)
            .w_SIMVAL = IIF(EMPTY(.w_PAVALINC),' ',.w_SIMVAL)
            .w_SIMVAL2 = IIF(EMPTY(.w_PAVALIN2),' ',.w_SIMVAL2)
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
        .DoRTCalc(24,25,.t.)
        if .o_PAVALINC<>.w_PAVALINC
            .w_CALCPICT = DEFPIC(.w_DECTOT)
        endif
        if .o_PAVALIN2<>.w_PAVALIN2
            .w_CALCPICP = DEFPIP(.w_DECTOT2)
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.oObj_1_33.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_34.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_40.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oPASCOFIN_1_4.enabled = this.oPgFrm.Page1.oPag.oPASCOFIN_1_4.mCond()
    this.oPgFrm.Page1.oPag.oPASPEINC_1_6.enabled = this.oPgFrm.Page1.oPag.oPASPEINC_1_6.mCond()
    this.oPgFrm.Page1.oPag.oPASPEINA_1_7.enabled = this.oPgFrm.Page1.oPag.oPASPEINA_1_7.mCond()
    this.oPgFrm.Page1.oPag.oPAVALIN2_1_8.enabled = this.oPgFrm.Page1.oPag.oPAVALIN2_1_8.mCond()
    this.oPgFrm.Page1.oPag.oPASPEIN2_1_9.enabled = this.oPgFrm.Page1.oPag.oPASPEIN2_1_9.mCond()
    this.oPgFrm.Page1.oPag.oPASPEIA2_1_10.enabled = this.oPgFrm.Page1.oPag.oPASPEIA2_1_10.mCond()
    this.oPgFrm.Page1.oPag.oPASALDO_1_12.enabled = this.oPgFrm.Page1.oPag.oPASALDO_1_12.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oPASCOFIN_1_4.visible=!this.oPgFrm.Page1.oPag.oPASCOFIN_1_4.mHide()
    this.oPgFrm.Page1.oPag.oPAVALINC_1_5.visible=!this.oPgFrm.Page1.oPag.oPAVALINC_1_5.mHide()
    this.oPgFrm.Page1.oPag.oPASPEINC_1_6.visible=!this.oPgFrm.Page1.oPag.oPASPEINC_1_6.mHide()
    this.oPgFrm.Page1.oPag.oPASPEINA_1_7.visible=!this.oPgFrm.Page1.oPag.oPASPEINA_1_7.mHide()
    this.oPgFrm.Page1.oPag.oPAVALIN2_1_8.visible=!this.oPgFrm.Page1.oPag.oPAVALIN2_1_8.mHide()
    this.oPgFrm.Page1.oPag.oPASPEIN2_1_9.visible=!this.oPgFrm.Page1.oPag.oPASPEIN2_1_9.mHide()
    this.oPgFrm.Page1.oPag.oPASPEIA2_1_10.visible=!this.oPgFrm.Page1.oPag.oPASPEIA2_1_10.mHide()
    this.oPgFrm.Page1.oPag.oPAINCASS_1_11.visible=!this.oPgFrm.Page1.oPag.oPAINCASS_1_11.mHide()
    this.oPgFrm.Page1.oPag.oPASALDO_1_12.visible=!this.oPgFrm.Page1.oPag.oPASALDO_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_28.visible=!this.oPgFrm.Page1.oPag.oStr_1_28.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_29.visible=!this.oPgFrm.Page1.oPag.oStr_1_29.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_30.visible=!this.oPgFrm.Page1.oPag.oStr_1_30.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_31.visible=!this.oPgFrm.Page1.oPag.oStr_1_31.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oSIMVAL_1_38.visible=!this.oPgFrm.Page1.oPag.oSIMVAL_1_38.mHide()
    this.oPgFrm.Page1.oPag.oSIMVAL2_1_39.visible=!this.oPgFrm.Page1.oPag.oSIMVAL2_1_39.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsar_apa
    If cEvent='w_PASALDO Changed' AND this.w_PASALDO='S' AND this.w_PASCOFIN<>0
      if Ah_YesNo('Attenzione, attivando il check a saldo la percentuale di sconto finanziario applicata verr� azzerata, si desidera continuare?')
       this.w_PASCOFIN=0
      Else
       this.w_PASALDO=' '
      Endif
    endif
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.oObj_1_33.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_34.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_40.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=PAVALINC
  func Link_1_5(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAVALINC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_PAVALINC)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_PAVALINC))
          select VACODVAL,VADESVAL,VASIMVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PAVALINC)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_PAVALINC)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_PAVALINC)+"%");

            select VACODVAL,VADESVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PAVALINC) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oPAVALINC_1_5'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAVALINC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PAVALINC);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PAVALINC)
            select VACODVAL,VADESVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAVALINC = NVL(_Link_.VACODVAL,space(3))
      this.w_DESAPP = NVL(_Link_.VADESVAL,space(35))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECTOT = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_PAVALINC = space(3)
      endif
      this.w_DESAPP = space(35)
      this.w_SIMVAL = space(5)
      this.w_DECTOT = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAVALINC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_5(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_5.VACODVAL as VACODVAL105"+ ",link_1_5.VADESVAL as VADESVAL105"+ ",link_1_5.VASIMVAL as VASIMVAL105"+ ",link_1_5.VADECTOT as VADECTOT105"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_5 on PAG_AMEN.PAVALINC=link_1_5.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_5"
          i_cKey=i_cKey+'+" and PAG_AMEN.PAVALINC=link_1_5.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  * --- Link procedure for entity name=PAVALIN2
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_PAVALIN2) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_PAVALIN2)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_PAVALIN2))
          select VACODVAL,VADESVAL,VASIMVAL,VADECTOT;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_PAVALIN2)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_PAVALIN2)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_PAVALIN2)+"%");

            select VACODVAL,VADESVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_PAVALIN2) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oPAVALIN2_1_8'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_PAVALIN2)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL,VADECTOT";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_PAVALIN2);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_PAVALIN2)
            select VACODVAL,VADESVAL,VASIMVAL,VADECTOT;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_PAVALIN2 = NVL(_Link_.VACODVAL,space(3))
      this.w_DESAPP = NVL(_Link_.VADESVAL,space(35))
      this.w_SIMVAL2 = NVL(_Link_.VASIMVAL,space(5))
      this.w_DECTOT2 = NVL(_Link_.VADECTOT,0)
    else
      if i_cCtrl<>'Load'
        this.w_PAVALIN2 = space(3)
      endif
      this.w_DESAPP = space(35)
      this.w_SIMVAL2 = space(5)
      this.w_DECTOT2 = 0
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_PAVALIN2 Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  func AddJoinedLink_1_8(i_cSel,i_cTable,i_cKey,i_nConn,i_cDatabaseType,i_nFlds)
    * questo link aggiunge 4 campi al cursore risultato
    local i_cLinkedTable,i_cNewSel,i_res
    i_res=.f.
    if i_nConn=i_TableProp[this.VALUTE_IDX,3] and i_nFlds+4<this.nMaxFieldsJoin &&200
      i_cLinkedTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
      i_cNewSel = i_cSel+ ",link_1_8.VACODVAL as VACODVAL108"+ ",link_1_8.VADESVAL as VADESVAL108"+ ",link_1_8.VASIMVAL as VASIMVAL108"+ ",link_1_8.VADECTOT as VADECTOT108"
      do case
        case inlist(i_cDatabaseType,"SQLServer","Interbase","Oracle","PostgreSQL") or left(i_cDatabaseType,3)="DB2"
          i_cTable = i_cTable+" left outer join "+i_cLinkedTable+" link_1_8 on PAG_AMEN.PAVALIN2=link_1_8.VACODVAL"
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
        case inlist(i_cDatabaseType,"Adabas","SAPDB")
          i_cTable = i_cTable+","+i_cLinkedTable+" link_1_8"
          i_cKey=i_cKey+'+" and PAG_AMEN.PAVALIN2=link_1_8.VACODVAL(+)"'
          i_cSel = i_cNewSel
          i_res = .t.
          i_nFlds = i_nFlds+4
      endcase
    endif
    return(i_res)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oPACODICE_1_1.value==this.w_PACODICE)
      this.oPgFrm.Page1.oPag.oPACODICE_1_1.value=this.w_PACODICE
    endif
    if not(this.oPgFrm.Page1.oPag.oPADESCRI_1_2.value==this.w_PADESCRI)
      this.oPgFrm.Page1.oPag.oPADESCRI_1_2.value=this.w_PADESCRI
    endif
    if not(this.oPgFrm.Page1.oPag.oPASCONTO_1_3.value==this.w_PASCONTO)
      this.oPgFrm.Page1.oPag.oPASCONTO_1_3.value=this.w_PASCONTO
    endif
    if not(this.oPgFrm.Page1.oPag.oPASCOFIN_1_4.value==this.w_PASCOFIN)
      this.oPgFrm.Page1.oPag.oPASCOFIN_1_4.value=this.w_PASCOFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oPAVALINC_1_5.value==this.w_PAVALINC)
      this.oPgFrm.Page1.oPag.oPAVALINC_1_5.value=this.w_PAVALINC
    endif
    if not(this.oPgFrm.Page1.oPag.oPASPEINC_1_6.value==this.w_PASPEINC)
      this.oPgFrm.Page1.oPag.oPASPEINC_1_6.value=this.w_PASPEINC
    endif
    if not(this.oPgFrm.Page1.oPag.oPASPEINA_1_7.value==this.w_PASPEINA)
      this.oPgFrm.Page1.oPag.oPASPEINA_1_7.value=this.w_PASPEINA
    endif
    if not(this.oPgFrm.Page1.oPag.oPAVALIN2_1_8.value==this.w_PAVALIN2)
      this.oPgFrm.Page1.oPag.oPAVALIN2_1_8.value=this.w_PAVALIN2
    endif
    if not(this.oPgFrm.Page1.oPag.oPASPEIN2_1_9.value==this.w_PASPEIN2)
      this.oPgFrm.Page1.oPag.oPASPEIN2_1_9.value=this.w_PASPEIN2
    endif
    if not(this.oPgFrm.Page1.oPag.oPASPEIA2_1_10.value==this.w_PASPEIA2)
      this.oPgFrm.Page1.oPag.oPASPEIA2_1_10.value=this.w_PASPEIA2
    endif
    if not(this.oPgFrm.Page1.oPag.oPAINCASS_1_11.RadioValue()==this.w_PAINCASS)
      this.oPgFrm.Page1.oPag.oPAINCASS_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPASALDO_1_12.RadioValue()==this.w_PASALDO)
      this.oPgFrm.Page1.oPag.oPASALDO_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAANNCIV_1_14.RadioValue()==this.w_PAANNCIV)
      this.oPgFrm.Page1.oPag.oPAANNCIV_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPAFLFMDP_1_15.RadioValue()==this.w_PAFLFMDP)
      this.oPgFrm.Page1.oPag.oPAFLFMDP_1_15.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oPADTOBSO_1_22.value==this.w_PADTOBSO)
      this.oPgFrm.Page1.oPag.oPADTOBSO_1_22.value=this.w_PADTOBSO
    endif
    if not(this.oPgFrm.Page1.oPag.oPADTINVA_1_27.value==this.w_PADTINVA)
      this.oPgFrm.Page1.oPag.oPADTINVA_1_27.value=this.w_PADTINVA
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL_1_38.value==this.w_SIMVAL)
      this.oPgFrm.Page1.oPag.oSIMVAL_1_38.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oSIMVAL2_1_39.value==this.w_SIMVAL2)
      this.oPgFrm.Page1.oPag.oSIMVAL2_1_39.value=this.w_SIMVAL2
    endif
    cp_SetControlsValueExtFlds(this,'PAG_AMEN')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   (empty(.w_PACODICE))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPACODICE_1_1.SetFocus()
            i_bnoObbl = !empty(.w_PACODICE)
            i_bnoChk = .f.
            i_bRes = .f.
          case   not(.w_PASCOFIN>=0)  and not(!isahe())  and (.w_PASALDO<>'S')
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oPASCOFIN_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Il valore della percentuale deve essere maggiore di zero")
        endcase
      endif
      *i_bRes = i_bRes .and. .GSAR_MPA.CheckForm()
      if i_bres
        i_bres=  .GSAR_MPA.CheckForm()
        if not(i_bres)
          this.oPGFRM.ActivePage=1
        endif
      endif
      * --- Area Manuale = Check Form
      * --- gsar_apa
      * Se � attivo il modulo revi si esegue il controllo per verificare che i codici di pagamento tra le diverse aziende siano uguali
      If g_REVI='S' And Alltrim(i_CODAZI) $ AssocLst()
      	Local risultato
      	risultato = IAHR_CHK_BCP(This)
      	If Not risultato
      		i_bnoChk = .F.
      		i_bRes = .F.
      		i_cErrorMsg = Ah_MsgFormat("Operazione annullata")
      	Endif
      Endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_PAVALINC = this.w_PAVALINC
    this.o_PASPEINC = this.w_PASPEINC
    this.o_PAVALIN2 = this.w_PAVALIN2
    this.o_PASPEIN2 = this.w_PASPEIN2
    this.o_PAINCASS = this.w_PAINCASS
    * --- GSAR_MPA : Depends On
    this.GSAR_MPA.SaveDependsOn()
    return

enddefine

* --- Define pages as container
define class tgsar_apaPag1 as StdContainer
  Width  = 656
  height = 365
  stdWidth  = 656
  stdheight = 365
  resizeXpos=167
  resizeYpos=224
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPACODICE_1_1 as StdField with uid="DULVOEUOHW",rtseq=1,rtrep=.f.,;
    cFormVar = "w_PACODICE", cQueryName = "PACODICE",;
    bObbl = .t. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Digitare il codice primario del pagamento.",;
    HelpContextID = 67764027,;
    FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.,;
    Height=21, Width=58, Left=62, Top=10, InputMask=replicate('X',5)

  add object oPADESCRI_1_2 as StdField with uid="SZOWLLULYX",rtseq=2,rtrep=.f.,;
    cFormVar = "w_PADESCRI", cQueryName = "PADESCRI",;
    bObbl = .f. , nPag = 1, value=space(30), bMultilanguage =  .t.,;
    ToolTipText = "Indicare la descrizione del pagamento.",;
    HelpContextID = 17821889,;
   bGlobalFont=.t.,;
    Height=21, Width=242, Left=121, Top=10, InputMask=replicate('X',30)

  add object oPASCONTO_1_3 as StdField with uid="OTNWTOCVGL",rtseq=3,rtrep=.f.,;
    cFormVar = "w_PASCONTO", cQueryName = "PASCONTO",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Percentuale di maggiorazione (se positiva) o sconto (se negativa) applicata",;
    HelpContextID = 162463557,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=312, Top=37, cSayPict='"999.99"', cGetPict='"999.99"'

  add object oPASCOFIN_1_4 as StdField with uid="BKAXHPAQIC",rtseq=4,rtrep=.f.,;
    cFormVar = "w_PASCOFIN", cQueryName = "PASCOFIN",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    sErrorMsg = "Il valore della percentuale deve essere maggiore di zero",;
    ToolTipText = "Percentuale di sconto applicata",;
    HelpContextID = 28245828,;
   bGlobalFont=.t.,;
    Height=21, Width=51, Left=552, Top=37, cSayPict='"999.99"', cGetPict='"999.99"'

  func oPASCOFIN_1_4.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PASALDO<>'S')
    endwith
   endif
  endfunc

  func oPASCOFIN_1_4.mHide()
    with this.Parent.oContained
      return (!isahe())
    endwith
  endfunc

  func oPASCOFIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_PASCOFIN>=0)
    endwith
    return bRes
  endfunc

  add object oPAVALINC_1_5 as StdField with uid="PINZMTVNLF",rtseq=5,rtrep=.f.,;
    cFormVar = "w_PAVALINC", cQueryName = "PAVALINC",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta di riferimento delle spese di incasso inserite sulla riga",;
    HelpContextID = 193122503,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=65, Top=65, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_PAVALINC"

  func oPAVALINC_1_5.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oPAVALINC_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_5('Part',this)
    endwith
    return bRes
  endfunc

  proc oPAVALINC_1_5.ecpDrop(oSource)
    this.Parent.oContained.link_1_5('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPAVALINC_1_5.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oPAVALINC_1_5'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oPAVALINC_1_5.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_PAVALINC
     i_obj.ecpSave()
  endproc

  add object oPASPEINC_1_6 as StdField with uid="ZBMQAAXHJW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_PASPEINC", cQueryName = "PASPEINC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo delle spese di incasso riferite alla valuta corrispondente",;
    HelpContextID = 199491783,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=312, Top=66, cSayPict="v_PV(32+VVL)", cGetPict="v_GV(32+VVL)"

  func oPASPEINC_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_PAVALINC))
    endwith
   endif
  endfunc

  func oPASPEINC_1_6.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oPASPEINA_1_7 as StdField with uid="EIKDBWXKYK",rtseq=7,rtrep=.f.,;
    cFormVar = "w_PASPEINA", cQueryName = "PASPEINA",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo delle spese di incasso riferite alla valuta corrispondente per il ciclo acquisti",;
    HelpContextID = 199491785,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=552, Top=66, cSayPict="v_PV(32+VVL)", cGetPict="v_GV(32+VVL)"

  func oPASPEINA_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_PAVALINC))
    endwith
   endif
  endfunc

  func oPASPEINA_1_7.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oPAVALIN2_1_8 as StdField with uid="BUFXDIMHNJ",rtseq=8,rtrep=.f.,;
    cFormVar = "w_PAVALIN2", cQueryName = "PAVALIN2",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Valuta di riferimento delle spese di incasso inserite sulla riga",;
    HelpContextID = 193122520,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=65, Top=89, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_PAVALIN2"

  func oPAVALIN2_1_8.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_PAVALINC))
    endwith
   endif
  endfunc

  func oPAVALIN2_1_8.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  func oPAVALIN2_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oPAVALIN2_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oPAVALIN2_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oPAVALIN2_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oPAVALIN2_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_PAVALIN2
     i_obj.ecpSave()
  endproc

  add object oPASPEIN2_1_9 as StdField with uid="MYOJYFBUEH",rtseq=9,rtrep=.f.,;
    cFormVar = "w_PASPEIN2", cQueryName = "PASPEIN2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo delle spese di incasso riferite alla valuta corrispondente",;
    HelpContextID = 199491800,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=312, Top=90, cSayPict="v_PV(32+VVP)", cGetPict="v_GV(32+VVP)"

  func oPASPEIN2_1_9.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_PAVALIN2))
    endwith
   endif
  endfunc

  func oPASPEIN2_1_9.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oPASPEIA2_1_10 as StdField with uid="RSVERGSBNJ",rtseq=10,rtrep=.f.,;
    cFormVar = "w_PASPEIA2", cQueryName = "PASPEIA2",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Importo delle spese di incasso riferite alla valuta corrispondente per il ciclo acquisti",;
    HelpContextID = 199491800,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=552, Top=90, cSayPict="v_PV(32+VVP)", cGetPict="v_GV(32+VVP)"

  func oPASPEIA2_1_10.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (NOT EMPTY(.w_PAVALIN2))
    endwith
   endif
  endfunc

  func oPASPEIA2_1_10.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oPAINCASS_1_11 as StdCheck with uid="JWWDWOBKTZ",rtseq=11,rtrep=.f.,left=65, top=116, caption="Incassato",;
    ToolTipText = "Se attivo, registra contestualmente l'acconto",;
    HelpContextID = 67543223,;
    cFormVar="w_PAINCASS", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAINCASS_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oPAINCASS_1_11.GetRadio()
    this.Parent.oContained.w_PAINCASS = this.RadioValue()
    return .t.
  endfunc

  func oPAINCASS_1_11.SetRadio()
    this.Parent.oContained.w_PAINCASS=trim(this.Parent.oContained.w_PAINCASS)
    this.value = ;
      iif(this.Parent.oContained.w_PAINCASS=='S',1,;
      0)
  endfunc

  func oPAINCASS_1_11.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oPASALDO_1_12 as StdCheck with uid="FAHBFQEINQ",rtseq=12,rtrep=.f.,left=191, top=116, caption="A saldo",;
    ToolTipText = "Se attivo, registra contestualmente all�incasso lo sconto finanziario",;
    HelpContextID = 8585482,;
    cFormVar="w_PASALDO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPASALDO_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPASALDO_1_12.GetRadio()
    this.Parent.oContained.w_PASALDO = this.RadioValue()
    return .t.
  endfunc

  func oPASALDO_1_12.SetRadio()
    this.Parent.oContained.w_PASALDO=trim(this.Parent.oContained.w_PASALDO)
    this.value = ;
      iif(this.Parent.oContained.w_PASALDO=='S',1,;
      0)
  endfunc

  func oPASALDO_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_PAINCASS='S')
    endwith
   endif
  endfunc

  func oPASALDO_1_12.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oPAANNCIV_1_14 as StdCheck with uid="RHKGMTUDBW",rtseq=13,rtrep=.f.,left=312, top=116, caption="Anno civile",;
    ToolTipText = "Se attivo calcolo i giorni secondo anno civile",;
    HelpContextID = 245948236,;
    cFormVar="w_PAANNCIV", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAANNCIV_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPAANNCIV_1_14.GetRadio()
    this.Parent.oContained.w_PAANNCIV = this.RadioValue()
    return .t.
  endfunc

  func oPAANNCIV_1_14.SetRadio()
    this.Parent.oContained.w_PAANNCIV=trim(this.Parent.oContained.w_PAANNCIV)
    this.value = ;
      iif(this.Parent.oContained.w_PAANNCIV=='S',1,;
      0)
  endfunc

  add object oPAFLFMDP_1_15 as StdCheck with uid="JQNMHVOXEI",rtseq=14,rtrep=.f.,left=463, top=116, caption="Fine mese data partenza",;
    ToolTipText = "Se attivo applica il calcolo di fine mese alla data di partenza invece che alla data finale",;
    HelpContextID = 136785734,;
    cFormVar="w_PAFLFMDP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oPAFLFMDP_1_15.RadioValue()
    return(iif(this.value =1,'S',;
    space(1)))
  endfunc
  func oPAFLFMDP_1_15.GetRadio()
    this.Parent.oContained.w_PAFLFMDP = this.RadioValue()
    return .t.
  endfunc

  func oPAFLFMDP_1_15.SetRadio()
    this.Parent.oContained.w_PAFLFMDP=trim(this.Parent.oContained.w_PAFLFMDP)
    this.value = ;
      iif(this.Parent.oContained.w_PAFLFMDP=='S',1,;
      0)
  endfunc


  add object oLinkPC_1_16 as stdDynamicChildContainer with uid="BVTKJZIHIV",left=3, top=163, width=653, height=164, bOnScreen=.t.;


  add object oPADTOBSO_1_22 as StdField with uid="STRPZSPABP",rtseq=19,rtrep=.f.,;
    cFormVar = "w_PADTOBSO", cQueryName = "PADTOBSO",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Ultima data di validit�",;
    HelpContextID = 37810363,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=528, Top=343, tabstop=.f.

  add object oPADTINVA_1_27 as StdField with uid="YSMELVIOPW",rtseq=20,rtrep=.f.,;
    cFormVar = "w_PADTINVA", cQueryName = "PADTINVA",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di inizio validit�",;
    HelpContextID = 157224759,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=312, Top=343, tabstop=.f.


  add object oObj_1_33 as cp_runprogram with uid="MVUMOTMNBX",left=1, top=413, width=213,height=21,;
    caption='GSAR_BPK',;
   bGlobalFont=.t.,;
    prg="GSAR_BPK",;
    cEvent = "ControlliFinali",;
    nPag=1;
    , HelpContextID = 21172047


  add object oObj_1_34 as cp_runprogram with uid="PXHMXBRLCM",left=1, top=374, width=213,height=21,;
    caption='GSAR_BBC',;
   bGlobalFont=.t.,;
    prg="GSAR_BBC",;
    cEvent = "ControlloCancellazione",;
    nPag=1;
    , HelpContextID = 21172055

  add object oSIMVAL_1_38 as StdField with uid="ZJDFAUMXJD",rtseq=22,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 152983770,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=115, Top=65, InputMask=replicate('X',5)

  func oSIMVAL_1_38.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oSIMVAL2_1_39 as StdField with uid="WBLGYZIHDH",rtseq=23,rtrep=.f.,;
    cFormVar = "w_SIMVAL2", cQueryName = "SIMVAL2",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 152983770,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=115, Top=89, InputMask=replicate('X',5)

  func oSIMVAL2_1_39.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc


  add object oObj_1_40 as cp_runprogram with uid="QIJYITDZNJ",left=1, top=394, width=213,height=20,;
    caption='GSAR_BPA',;
   bGlobalFont=.t.,;
    prg="GSAR_BPA",;
    cEvent = "ControlloDelete",;
    nPag=1;
    , HelpContextID = 21172057

  add object oStr_1_13 as StdString with uid="OJZKGHVLJY",Visible=.t., Left=14, Top=10,;
    Alignment=1, Width=42, Height=15,;
    Caption="Codice:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_21 as StdString with uid="QRCMGZOGDO",Visible=.t., Left=3, Top=144,;
    Alignment=0, Width=159, Height=15,;
    Caption="Dettaglio pagamento"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_23 as StdString with uid="WUUDCLAWAI",Visible=.t., Left=400, Top=345,;
    Alignment=1, Width=124, Height=15,;
    Caption="Data obsolescenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_24 as StdString with uid="BXFWWLTPWT",Visible=.t., Left=396, Top=144,;
    Alignment=0, Width=129, Height=15,;
    Caption="Parametri"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_25 as StdString with uid="UELNWVAKWN",Visible=.t., Left=109, Top=39,;
    Alignment=1, Width=198, Height=18,;
    Caption="% Maggior./sconto commerciale:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="KFIISQMGHR",Visible=.t., Left=215, Top=345,;
    Alignment=1, Width=92, Height=15,;
    Caption="Data validit�:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="NLSJTLFVKB",Visible=.t., Left=172, Top=69,;
    Alignment=1, Width=135, Height=18,;
    Caption="Spese di inc. vendite:"  ;
  , bGlobalFont=.t.

  func oStr_1_28.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_29 as StdString with uid="ASSVDFEIQV",Visible=.t., Left=12, Top=68,;
    Alignment=1, Width=49, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  func oStr_1_29.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_30 as StdString with uid="NBOSVMDBAX",Visible=.t., Left=172, Top=91,;
    Alignment=1, Width=135, Height=18,;
    Caption="Spese di inc. vendite:"  ;
  , bGlobalFont=.t.

  func oStr_1_30.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_31 as StdString with uid="FFETOLPKVD",Visible=.t., Left=12, Top=90,;
    Alignment=1, Width=49, Height=15,;
    Caption="Valuta:"  ;
  , bGlobalFont=.t.

  func oStr_1_31.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="IREIFTBWHK",Visible=.t., Left=400, Top=39,;
    Alignment=1, Width=148, Height=18,;
    Caption="% Sconto finanziario:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (!isahe())
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="NUNDZRIXIP",Visible=.t., Left=400, Top=69,;
    Alignment=1, Width=148, Height=18,;
    Caption="Spese di inc. acquisto:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="MHRMDHJENB",Visible=.t., Left=400, Top=91,;
    Alignment=1, Width=148, Height=18,;
    Caption="Spese di inc. acquisto:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (Isalt())
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_apa','PAG_AMEN','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".PACODICE=PAG_AMEN.PACODICE";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
