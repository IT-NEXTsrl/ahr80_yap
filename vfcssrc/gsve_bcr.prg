* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bcr                                                        *
*              Controllo causale                                               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_15]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-10-13                                                      *
* Last revis.: 2004-10-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bcr",oParentObject)
return(i_retval)

define class tgsve_bcr as StdBatch
  * --- Local variables
  w_OK = .f.
  w_MESS = space(10)
  w_Padre = .NULL.
  * --- WorkFile variables
  DOC_COLL_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo causali documenti per Flag "Raggruppa" sulle Origini
    this.w_Padre = this.oParentObject
    this.w_OK = .F.
    * --- Se la causale di magazzino diminuisce l'esistenza o aumenta il riservato,
    *     non � possibile attivare il flag Raggruppa nelle origini
    if this.oParentObject.w_FLCASC="-" Or this.oParentObject.w_FLRISE="+"
      if this.oParentObject.w_TDFLQRIO="S"
        this.w_MESS = ah_Msgformat("Impossibile attivare il check calcolo qt� riordino%0su causale di magazzino che diminuisce l'esistenza o che aumenta il riservato")
        this.w_OK = .T.
      else
        * --- Select from DOC_COLL
        i_nConn=i_TableProp[this.DOC_COLL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DOC_COLL_idx,2],.t.,this.DOC_COLL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select DCCOLLEG  from "+i_cTable+" DOC_COLL ";
              +" where DCCODICE = "+cp_ToStrODBC(this.oParentObject.w_TDTIPDOC)+" And DCFLGROR='S'";
               ,"_Curs_DOC_COLL")
        else
          select DCCOLLEG from (i_cTable);
           where DCCODICE = this.oParentObject.w_TDTIPDOC And DCFLGROR="S";
            into cursor _Curs_DOC_COLL
        endif
        if used('_Curs_DOC_COLL')
          select _Curs_DOC_COLL
          locate for 1=1
          do while not(eof())
          this.w_MESS = ah_Msgformat("Documento di origine: %1. Impossibile attivare flag raggruppa%0La causale di magazzino della causale %2%0non deve diminuire l'esistenza o aumentare il riservato", Alltrim(_Curs_DOC_COLL.DCCOLLEG), Alltrim(this.oParentObject.w_TDTIPDOC) )
          this.w_OK = .T.
            select _Curs_DOC_COLL
            continue
          enddo
          use
        endif
      endif
    else
      * --- Se attivo check Calcola Qt� Riordino, devo attivare i check No Evasione
      *     e Raggruppa su tutti i documenti di Origine
      if this.oParentObject.w_TDFLQRIO="S" And this.oParentObject.w_OFLQRIO<>"S"
        this.w_MESS = "Attivato check calcolo qt� riordino%0Verranno attivati check no evasione e raggruppa nei documenti di origine%0Si desidera continuare?"
        if ah_YesNo(this.w_MESS)
          * --- Write into DOC_COLL
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_COLL_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_COLL_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_COLL_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"DCFLGROR ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_COLL','DCFLGROR');
            +",DCFLEVAS ="+cp_NullLink(cp_ToStrODBC("S"),'DOC_COLL','DCFLEVAS');
                +i_ccchkf ;
            +" where ";
                +"DCCODICE = "+cp_ToStrODBC(this.oParentObject.w_TDTIPDOC);
                   )
          else
            update (i_cTable) set;
                DCFLGROR = "S";
                ,DCFLEVAS = "S";
                &i_ccchkf. ;
             where;
                DCCODICE = this.oParentObject.w_TDTIPDOC;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          * --- Reinterrogo 
          this.w_Padre.LoadRec()     
        else
          this.w_OK = .T.
          this.w_MESS = ah_Msgformat("Transazione abbandonata")
        endif
      endif
    endif
    if this.w_OK
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_COLL'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_DOC_COLL')
      use in _Curs_DOC_COLL
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
