* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_sdi                                                        *
*              Stampa distinte                                                 *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_41]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 1997-10-29                                                      *
* Last revis.: 2015-03-17                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_sdi",oParentObject))

* --- Class definition
define class tgste_sdi as StdForm
  Top    = 5
  Left   = 26

  * --- Standard Properties
  Width  = 650
  Height = 459
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-03-17"
  HelpContextID=74069097
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Constant Properties
  _IDX = 0
  DIS_TINT_IDX = 0
  dis_tint_IDX = 0
  cPrg = "gste_sdi"
  cComment = "Stampa distinte"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_STAMPA = space(1)
  o_STAMPA = space(1)
  w_NUMDIST = 0
  w_ANNO = space(4)
  w_DATDIS = ctod('  /  /  ')
  w_TIPDIS = space(2)
  w_TIPSCA = space(2)
  w_NUMERO1 = 0
  w_ANNO1 = space(4)
  w_DATA1 = ctod('  /  /  ')
  w_DATVAL = ctod('  /  /  ')
  o_DATVAL = ctod('  /  /  ')
  w_FLEURO = space(1)
  w_DETDOCD = space(1)
  w_FLGORDAT = space(1)
  w_NUMDISM = space(10)
  w_DEFI = space(1)
  w_DESCRI1 = space(35)
  w_DESCEST = space(1)
  w_TEST = space(1)
  w_ZoomDi = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_sdiPag1","gste_sdi",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSTAMPA_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomDi = this.oPgFrm.Pages(1).oPag.ZoomDi
    DoDefault()
    proc Destroy()
      this.w_ZoomDi = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='DIS_TINT'
    this.cWorkTables[2]='dis_tint'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gste_sdi
    this.bUpdated=.t.
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_STAMPA=space(1)
      .w_NUMDIST=0
      .w_ANNO=space(4)
      .w_DATDIS=ctod("  /  /  ")
      .w_TIPDIS=space(2)
      .w_TIPSCA=space(2)
      .w_NUMERO1=0
      .w_ANNO1=space(4)
      .w_DATA1=ctod("  /  /  ")
      .w_DATVAL=ctod("  /  /  ")
      .w_FLEURO=space(1)
      .w_DETDOCD=space(1)
      .w_FLGORDAT=space(1)
      .w_NUMDISM=space(10)
      .w_DEFI=space(1)
      .w_DESCRI1=space(35)
      .w_DESCEST=space(1)
      .w_TEST=space(1)
        .w_STAMPA = 'S'
          .DoRTCalc(2,4,.f.)
        .w_TIPDIS = ''
        .w_TIPSCA = 'C'
      .oPgFrm.Page1.oPag.ZoomDi.Calculate(.F.)
        .w_NUMERO1 = Nvl( .w_zoomDI.getvar('DINUMERO') , 0 )
        .w_ANNO1 = Nvl( .w_zoomDI.getvar('DI__ANNO') , Space(4) )
        .w_DATA1 = CP_TODATE(.w_zoomDI.getvar('DIDATDIS'))
        .w_DATVAL = CP_TODATE(.w_zoomDI.getvar('DIDATVAL'))
        .w_FLEURO = IIF(.w_DATVAL<cp_CharToDate('01-01-2002') AND NOT EMPTY(.w_DATVAL), ' ', 'S')
        .w_DETDOCD = 'N'
        .w_FLGORDAT = 'N'
        .w_NUMDISM = .w_ZoomDI.getVar('DINUMDIS')
        .w_DEFI = IIF( .w_STAMPA ='R','S','N')
        .w_DESCRI1 = Nvl( .w_zoomDI.getvar('DIDESCRI') , Space(35))
        .w_DESCEST = ' '
        .w_TEST = .T.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_16.enabled = this.oPgFrm.Page1.oPag.oBtn_1_16.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.ZoomDi.Calculate(.F.)
        .DoRTCalc(1,6,.t.)
            .w_NUMERO1 = Nvl( .w_zoomDI.getvar('DINUMERO') , 0 )
            .w_ANNO1 = Nvl( .w_zoomDI.getvar('DI__ANNO') , Space(4) )
            .w_DATA1 = CP_TODATE(.w_zoomDI.getvar('DIDATDIS'))
            .w_DATVAL = CP_TODATE(.w_zoomDI.getvar('DIDATVAL'))
        if .o_DATVAL<>.w_DATVAL
            .w_FLEURO = IIF(.w_DATVAL<cp_CharToDate('01-01-2002') AND NOT EMPTY(.w_DATVAL), ' ', 'S')
        endif
        .DoRTCalc(12,13,.t.)
            .w_NUMDISM = .w_ZoomDI.getVar('DINUMDIS')
        if .o_STAMPA<>.w_STAMPA
            .w_DEFI = IIF( .w_STAMPA ='R','S','N')
        endif
            .w_DESCRI1 = Nvl( .w_zoomDI.getvar('DIDESCRI') , Space(35))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(17,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomDi.Calculate(.F.)
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oFLEURO_1_12.enabled = this.oPgFrm.Page1.oPag.oFLEURO_1_12.mCond()
    this.oPgFrm.Page1.oPag.oDESCEST_1_33.enabled = this.oPgFrm.Page1.oPag.oDESCEST_1_33.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLEURO_1_12.visible=!this.oPgFrm.Page1.oPag.oFLEURO_1_12.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomDi.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSTAMPA_1_1.RadioValue()==this.w_STAMPA)
      this.oPgFrm.Page1.oPag.oSTAMPA_1_1.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDIST_1_2.value==this.w_NUMDIST)
      this.oPgFrm.Page1.oPag.oNUMDIST_1_2.value=this.w_NUMDIST
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO_1_3.value==this.w_ANNO)
      this.oPgFrm.Page1.oPag.oANNO_1_3.value=this.w_ANNO
    endif
    if not(this.oPgFrm.Page1.oPag.oDATDIS_1_4.value==this.w_DATDIS)
      this.oPgFrm.Page1.oPag.oDATDIS_1_4.value=this.w_DATDIS
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPDIS_1_5.RadioValue()==this.w_TIPDIS)
      this.oPgFrm.Page1.oPag.oTIPDIS_1_5.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPSCA_1_6.RadioValue()==this.w_TIPSCA)
      this.oPgFrm.Page1.oPag.oTIPSCA_1_6.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMERO1_1_8.value==this.w_NUMERO1)
      this.oPgFrm.Page1.oPag.oNUMERO1_1_8.value=this.w_NUMERO1
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO1_1_9.value==this.w_ANNO1)
      this.oPgFrm.Page1.oPag.oANNO1_1_9.value=this.w_ANNO1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATA1_1_10.value==this.w_DATA1)
      this.oPgFrm.Page1.oPag.oDATA1_1_10.value=this.w_DATA1
    endif
    if not(this.oPgFrm.Page1.oPag.oDATVAL_1_11.value==this.w_DATVAL)
      this.oPgFrm.Page1.oPag.oDATVAL_1_11.value=this.w_DATVAL
    endif
    if not(this.oPgFrm.Page1.oPag.oFLEURO_1_12.RadioValue()==this.w_FLEURO)
      this.oPgFrm.Page1.oPag.oFLEURO_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDETDOCD_1_13.RadioValue()==this.w_DETDOCD)
      this.oPgFrm.Page1.oPag.oDETDOCD_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLGORDAT_1_14.RadioValue()==this.w_FLGORDAT)
      this.oPgFrm.Page1.oPag.oFLGORDAT_1_14.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCRI1_1_31.value==this.w_DESCRI1)
      this.oPgFrm.Page1.oPag.oDESCRI1_1_31.value=this.w_DESCRI1
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCEST_1_33.RadioValue()==this.w_DESCEST)
      this.oPgFrm.Page1.oPag.oDESCEST_1_33.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(val(.w_ANNO)>=0)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oANNO_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Anno non valido")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gste_sdi
      IF i_bres
       DO gste_bdi with this
       i_bres=this.w_TEST
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_STAMPA = this.w_STAMPA
    this.o_DATVAL = this.w_DATVAL
    return

enddefine

* --- Define pages as container
define class tgste_sdiPag1 as StdContainer
  Width  = 646
  height = 459
  stdWidth  = 646
  stdheight = 459
  resizeXpos=384
  resizeYpos=309
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oSTAMPA_1_1 as StdCombo with uid="FXLDYUMJIE",rtseq=1,rtrep=.f.,left=66,top=5,width=112,height=21;
    , ToolTipText = "Tipo di stampa selezionata";
    , HelpContextID = 31929638;
    , cFormVar="w_STAMPA",RowSource=""+"Simulata,"+"Definitiva,"+"Ristampa", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oSTAMPA_1_1.RadioValue()
    return(iif(this.value =1,'S',;
    iif(this.value =2,'D',;
    iif(this.value =3,'R',;
    space(1)))))
  endfunc
  func oSTAMPA_1_1.GetRadio()
    this.Parent.oContained.w_STAMPA = this.RadioValue()
    return .t.
  endfunc

  func oSTAMPA_1_1.SetRadio()
    this.Parent.oContained.w_STAMPA=trim(this.Parent.oContained.w_STAMPA)
    this.value = ;
      iif(this.Parent.oContained.w_STAMPA=='S',1,;
      iif(this.Parent.oContained.w_STAMPA=='D',2,;
      iif(this.Parent.oContained.w_STAMPA=='R',3,;
      0)))
  endfunc

  add object oNUMDIST_1_2 as StdField with uid="XAAECVNHKW",rtseq=2,rtrep=.f.,;
    cFormVar = "w_NUMDIST", cQueryName = "NUMDIST",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero distinta selezionata",;
    HelpContextID = 210831914,;
   bGlobalFont=.t.,;
    Height=21, Width=58, Left=271, Top=29, cSayPict='"999999"', cGetPict='"999999"'

  add object oANNO_1_3 as StdField with uid="VBVKBFWCEI",rtseq=3,rtrep=.f.,;
    cFormVar = "w_ANNO", cQueryName = "ANNO",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    sErrorMsg = "Anno non valido",;
    ToolTipText = "Anno di riferimento selezionata",;
    HelpContextID = 68551162,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=354, Top=29, InputMask=replicate('X',4)

  func oANNO_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (val(.w_ANNO)>=0)
    endwith
    return bRes
  endfunc

  add object oDATDIS_1_4 as StdField with uid="ORPWORPKCP",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATDIS", cQueryName = "DATDIS",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data distinta selezionata",;
    HelpContextID = 57626934,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=452, Top=29


  add object oTIPDIS_1_5 as StdCombo with uid="DHWDYRZKQB",value=12,rtseq=5,rtrep=.f.,left=453,top=57,width=128,height=21;
    , ToolTipText = "Tipo della distinta selezionata";
    , HelpContextID = 57612854;
    , cFormVar="w_TIPDIS",RowSource=""+"Ric.bancarie,"+"R.I.D.,"+"M.AV.,"+"Cambiali/tratte,"+"Bonifici nazionali,"+"Bonifici esteri,"+"Rimesse dirette,"+"Cessioni crediti,"+"SDD - Sepa Direct Debit,"+"SCT - Sepa Credit Transfer,"+"SCT estero - Sepa Credit Transfer,"+"Tutte", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPDIS_1_5.RadioValue()
    return(iif(this.value =1,'RB',;
    iif(this.value =2,'RI',;
    iif(this.value =3,'MA',;
    iif(this.value =4,'CA',;
    iif(this.value =5,'BO',;
    iif(this.value =6,'BE',;
    iif(this.value =7,'RD',;
    iif(this.value =8,'CE',;
    iif(this.value =9,'SD',;
    iif(this.value =10,'SC',;
    iif(this.value =11,'SE',;
    iif(this.value =12,'',;
    space(2))))))))))))))
  endfunc
  func oTIPDIS_1_5.GetRadio()
    this.Parent.oContained.w_TIPDIS = this.RadioValue()
    return .t.
  endfunc

  func oTIPDIS_1_5.SetRadio()
    this.Parent.oContained.w_TIPDIS=trim(this.Parent.oContained.w_TIPDIS)
    this.value = ;
      iif(this.Parent.oContained.w_TIPDIS=='RB',1,;
      iif(this.Parent.oContained.w_TIPDIS=='RI',2,;
      iif(this.Parent.oContained.w_TIPDIS=='MA',3,;
      iif(this.Parent.oContained.w_TIPDIS=='CA',4,;
      iif(this.Parent.oContained.w_TIPDIS=='BO',5,;
      iif(this.Parent.oContained.w_TIPDIS=='BE',6,;
      iif(this.Parent.oContained.w_TIPDIS=='RD',7,;
      iif(this.Parent.oContained.w_TIPDIS=='CE',8,;
      iif(this.Parent.oContained.w_TIPDIS=='SD',9,;
      iif(this.Parent.oContained.w_TIPDIS=='SC',10,;
      iif(this.Parent.oContained.w_TIPDIS=='SE',11,;
      iif(this.Parent.oContained.w_TIPDIS=='',12,;
      0))))))))))))
  endfunc


  add object oTIPSCA_1_6 as StdCombo with uid="VHZDQPCWPY",rtseq=6,rtrep=.f.,left=453,top=83,width=128,height=21;
    , ToolTipText = "Tipo scadenza selezionata";
    , HelpContextID = 18750006;
    , cFormVar="w_TIPSCA",RowSource=""+"Attive,"+"Passive", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPSCA_1_6.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    space(2))))
  endfunc
  func oTIPSCA_1_6.GetRadio()
    this.Parent.oContained.w_TIPSCA = this.RadioValue()
    return .t.
  endfunc

  func oTIPSCA_1_6.SetRadio()
    this.Parent.oContained.w_TIPSCA=trim(this.Parent.oContained.w_TIPSCA)
    this.value = ;
      iif(this.Parent.oContained.w_TIPSCA=='C',1,;
      iif(this.Parent.oContained.w_TIPSCA=='F',2,;
      0))
  endfunc


  add object ZoomDi as cp_zoombox with uid="YZPQSYZYPY",left=1, top=129, width=643,height=245,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable='DIS_TINT',cZoomFile='GSTE_ZSD',bOptions=.f.,bQueryOnLoad=.f.,;
    cEvent = "Esegui",;
    nPag=1;
    , HelpContextID = 103928550

  add object oNUMERO1_1_8 as StdField with uid="QPSEWMTDEB",rtseq=7,rtrep=.f.,;
    cFormVar = "w_NUMERO1", cQueryName = "NUMERO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero della distinta selezionata",;
    HelpContextID = 268432854,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=151, Top=379, cSayPict='"999999"', cGetPict='"999999"'

  add object oANNO1_1_9 as StdField with uid="JZEWVRJWZL",rtseq=8,rtrep=.f.,;
    cFormVar = "w_ANNO1", cQueryName = "ANNO1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno della distinta selezionata",;
    HelpContextID = 17170938,;
   bGlobalFont=.t.,;
    Height=21, Width=44, Left=228, Top=379, InputMask=replicate('X',4)

  add object oDATA1_1_10 as StdField with uid="LVSGJJIUZY",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DATA1", cQueryName = "DATA1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data della distinta selezionata",;
    HelpContextID = 18067146,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=326, Top=379

  add object oDATVAL_1_11 as StdField with uid="GFMDOBRDPK",rtseq=10,rtrep=.f.,;
    cFormVar = "w_DATVAL", cQueryName = "DATVAL",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data di presentazione della distinta selezionata",;
    HelpContextID = 201412918,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=559, Top=379

  add object oFLEURO_1_12 as StdCheck with uid="XKNGRAJSLT",rtseq=11,rtrep=.f.,left=20, top=36, caption="Importi in Euro",;
    ToolTipText = "Se attivo: gli importi delle scadenze in valuta EMU saranno stampati convertiti in Euro",;
    HelpContextID = 1010774,;
    cFormVar="w_FLEURO", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLEURO_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFLEURO_1_12.GetRadio()
    this.Parent.oContained.w_FLEURO = this.RadioValue()
    return .t.
  endfunc

  func oFLEURO_1_12.SetRadio()
    this.Parent.oContained.w_FLEURO=trim(this.Parent.oContained.w_FLEURO)
    this.value = ;
      iif(this.Parent.oContained.w_FLEURO=='S',1,;
      0)
  endfunc

  func oFLEURO_1_12.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_DATVAL<cp_CharToDate('01-01-2002') AND NOT EMPTY(.w_DATVAL))
    endwith
   endif
  endfunc

  func oFLEURO_1_12.mHide()
    with this.Parent.oContained
      return (NOT (.w_DATVAL<cp_CharToDate('01-01-2002') AND NOT EMPTY(.w_DATVAL)))
    endwith
  endfunc

  add object oDETDOCD_1_13 as StdCheck with uid="TANZHKUOWP",rtseq=12,rtrep=.f.,left=20, top=59, caption="Dettaglio documenti diversi",;
    ToolTipText = "Stampa dettaglio documenti diversi",;
    HelpContextID = 204516042,;
    cFormVar="w_DETDOCD", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDETDOCD_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oDETDOCD_1_13.GetRadio()
    this.Parent.oContained.w_DETDOCD = this.RadioValue()
    return .t.
  endfunc

  func oDETDOCD_1_13.SetRadio()
    this.Parent.oContained.w_DETDOCD=trim(this.Parent.oContained.w_DETDOCD)
    this.value = ;
      iif(this.Parent.oContained.w_DETDOCD=='S',1,;
      0)
  endfunc

  add object oFLGORDAT_1_14 as StdCheck with uid="SHFLGIUPUZ",rtseq=13,rtrep=.f.,left=20, top=82, caption="Ordinamento per data documento",;
    ToolTipText = "Esegue ordinamento per data documento",;
    HelpContextID = 84511914,;
    cFormVar="w_FLGORDAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLGORDAT_1_14.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLGORDAT_1_14.GetRadio()
    this.Parent.oContained.w_FLGORDAT = this.RadioValue()
    return .t.
  endfunc

  func oFLGORDAT_1_14.SetRadio()
    this.Parent.oContained.w_FLGORDAT=trim(this.Parent.oContained.w_FLGORDAT)
    this.value = ;
      iif(this.Parent.oContained.w_FLGORDAT=='S',1,;
      0)
  endfunc


  add object oBtn_1_15 as StdButton with uid="JUVEIQXEBU",left=542, top=408, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , HelpContextID = 74040346;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      with this.Parent.oContained
        do GSTE_BDI with this.Parent.oContained
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_16 as StdButton with uid="HFTTWFUQYM",left=592, top=408, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , HelpContextID = 66751674;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_16.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_22 as StdButton with uid="APXDQPLGTC",left=592, top=29, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premi per aggiornare le distinte";
    , HelpContextID = 102853142;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      with this.Parent.oContained
        .notifyevent('Esegui')
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESCRI1_1_31 as StdField with uid="MZSDBMGIQI",rtseq=16,rtrep=.f.,;
    cFormVar = "w_DESCRI1", cQueryName = "DESCRI1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(35), bMultilanguage =  .f.,;
    ToolTipText = "Descrizione della distinta selezionata",;
    HelpContextID = 167658806,;
   bGlobalFont=.t.,;
    Height=21, Width=251, Left=151, Top=403, InputMask=replicate('X',35)

  add object oDESCEST_1_33 as StdCheck with uid="RAZGSZOTZO",rtseq=17,rtrep=.f.,left=20, top=105, caption="Riporta descrizione parametrica",;
    ToolTipText = "Se attivo, riporta la descrizione parametrica presente sui documenti",;
    HelpContextID = 215071434,;
    cFormVar="w_DESCEST", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oDESCEST_1_33.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oDESCEST_1_33.GetRadio()
    this.Parent.oContained.w_DESCEST = this.RadioValue()
    return .t.
  endfunc

  func oDESCEST_1_33.SetRadio()
    this.Parent.oContained.w_DESCEST=trim(this.Parent.oContained.w_DESCEST)
    this.value = ;
      iif(this.Parent.oContained.w_DESCEST=='S',1,;
      0)
  endfunc

  func oDESCEST_1_33.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPDIS<>'RD' AND .w_TIPDIS<>'CA')
    endwith
   endif
  endfunc

  add object oStr_1_17 as StdString with uid="JUJHOLLLGF",Visible=.t., Left=406, Top=29,;
    Alignment=1, Width=44, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="TUVVHLHXIX",Visible=.t., Left=4, Top=5,;
    Alignment=1, Width=59, Height=15,;
    Caption="Stampa:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="FAGFHJMQPG",Visible=.t., Left=335, Top=29,;
    Alignment=0, Width=10, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_21 as StdString with uid="UCIMRQDGEH",Visible=.t., Left=186, Top=5,;
    Alignment=0, Width=393, Height=15,;
    Caption="Criteri di selezione"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_23 as StdString with uid="VABBSPWTMV",Visible=.t., Left=355, Top=83,;
    Alignment=1, Width=95, Height=15,;
    Caption="Scadenza:"  ;
  , bGlobalFont=.t.

  add object oStr_1_26 as StdString with uid="KMXTFDJTLZ",Visible=.t., Left=154, Top=29,;
    Alignment=1, Width=115, Height=15,;
    Caption="Distinta n.:"  ;
  , bGlobalFont=.t.

  add object oStr_1_27 as StdString with uid="WEAPIWVDLZ",Visible=.t., Left=272, Top=57,;
    Alignment=1, Width=178, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_28 as StdString with uid="PRTIRNEVYI",Visible=.t., Left=9, Top=379,;
    Alignment=1, Width=139, Height=15,;
    Caption="Distinta selezionata:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_29 as StdString with uid="PLVXTBHFQT",Visible=.t., Left=220, Top=379,;
    Alignment=0, Width=12, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_1_30 as StdString with uid="JFNRHJRZQS",Visible=.t., Left=275, Top=379,;
    Alignment=1, Width=49, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_1_32 as StdString with uid="OQOPBZUJBH",Visible=.t., Left=409, Top=379,;
    Alignment=1, Width=148, Height=18,;
    Caption="Data presentazione:"  ;
  , bGlobalFont=.t.

  add object oBox_1_25 as StdBox with uid="OPBGPBEEVQ",left=185, top=22, width=451,height=1
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_sdi','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
