* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_bct                                                        *
*              CONTROLLI SUI  CODICI TRIBUTI                                   *
*                                                                              *
*      Author: Zucchetti spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][207]                                                     *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-06                                                      *
* Last revis.: 2012-06-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pDATI,w_ERRORE
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_bct",oParentObject,m.pDATI,m.w_ERRORE)
return(i_retval)

define class tgste_bct as StdBatch
  * --- Local variables
  pDATI = .NULL.
  w_ERRORE = 0
  w_TOTCRE = 0
  w_TOTDEB = 0
  w_oERRORLOG = .NULL.
  w_CODICEENTE = space(5)
  w_CONTA = 0
  w_TROVATO = .f.
  w_PRESENZACODICI = space(10)
  w_DAREMSG = .f.
  w_CODTRIB = space(60)
  w_oPART4 = .NULL.
  w_oMESS4 = .NULL.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- In presenza di �codice tributo� 3900, la sommatoria dei campi �importo a credito compensato� di tale tributo non deve essere maggiore di 200 euro.
    *     In presenza di �codice tributo� relativo all�ICI, la sommatoria dei campi �importo a credito compensato� non pu� essere maggiore della sommatoria
    *      dei campi �importo a debito versato� relativo ai tributi ICI 3901 e 3904, nell�ambito dello stesso �codice ente� (stesso Comune).  
    *     In caso uno dei controlli non avesse successo allora viene visualizzato un report contenente i warning e richiesto all utente se vuole abbandonare 
    *     l elaborazione. Nel caso in cui l utente abbandona l elaborazione viene valorizzata la variabile w_ERRORE=-1 (la variabile viene passata per riferimento
    *     cosicch� il batch/gestione chiamante possa abbortire l elaborazione o il salvataggio)
    *     
    *     Larray dei DATI ricevuto come parametro contiene 5 righe con le segiuenti informazioni
    *       dati(i,1)='.IFCODEL'+alltrim(str(i))
    *       dati(i,2)='.IFTRIEL'+alltrim(str(i))
    *       dati(i,3='.TIPTRI1'+alltrim(str(i))
    *       dati(i,4)='.IFIMDEL1'+alltrim(str(i))
    *       dati(i,5)='.IFIMCEL1'+alltrim(str(i))
    *     
    this.w_TROVATO = .F.
    this.w_CONTA = 1
    this.w_oERRORLOG=createobject("AH_ErrorLog")
    * --- In presenza di �codice tributo� 3900, la sommatoria dei campi �importo a credito compensato� di tale tributo non deve essere maggiore di 200 euro.
    if ( IIF (m.pDATI(1,2) ="3900",m.pDATI(1,5),0)+IIF (m.pDATI(2,2)="3900",m.pDATI(2,5),0)+IIF (m.pDATI(3,2)="3900",m.pDATI(3,5),0)+IIF (m.pDATI(4,2)="3900",m.pDATI(4,5),0) )>200
      this.w_oERRORLOG.AddMsgLog(" - Sezione ICI/ENTI LOCALI: in presenza di codice tributo ICI 3900, la sommatoria dei campi 'Importo a credito compensato' non deve essere maggiore di euro 200,00.")     
    endif
    * --- In presenza di �codice tributo� relativo all�ICI, la sommatoria dei campi �importo a credito compensato� non pu� essere maggiore della sommatoria
    *      dei campi �importo a debito versato� relativo ai tributi ICI 3901 e 3904, nell�ambito dello stesso �codice ente� (stesso Comune).  
    do while this.w_CONTA<=4
      this.w_CODICEENTE = m.pDATI(this.w_CONTA,1)
      this.w_TOTCRE = IIF (m.pDATI(1,1)=this.w_CODICEENTE AND (m.pDATI(1,2)="3940" OR m.pDATI(1,2)="3943"),m.pDATI(1,5),0)+IIF (m.pDATI(2,1)=this.w_CODICEENTE AND (m.pDATI(2,2)="3940" OR m.pDATI(2,2)="3943"),m.pDATI(2,5),0)+IIF (m.pDATI(3,1)=this.w_CODICEENTE AND (m.pDATI(3,2)="3940" OR m.pDATI(3,2)="3943"),m.pDATI(3,5),0)+IIF (m.pDATI(4,1)=this.w_CODICEENTE AND (m.pDATI(4,2)="3940" OR m.pDATI(4,2)="3943"),m.pDATI(4,5),0) 
      this.w_TOTDEB = IIF (m.pDATI(1,1)=this.w_CODICEENTE AND (m.pDATI(1,2)="3940" OR m.pDATI(1,2)="3943"),m.pDATI(1,4),0)+IIF (m.pDATI(2,1)=this.w_CODICEENTE AND (m.pDATI(2,2)="3940" OR m.pDATI(2,2)="3943"),m.pDATI(2,4),0)+IIF (m.pDATI(3,1)=this.w_CODICEENTE AND (m.pDATI(3,2)="3940" OR m.pDATI(3,2)="3943"),m.pDATI(3,4),0)+IIF (m.pDATI(4,1)=this.w_CODICEENTE AND (m.pDATI(4,2)="3940" OR m.pDATI(4,2)="3943"),m.pDATI(4,4),0) 
      this.w_PRESENZACODICI = IIF (m.pDATI(1,4)<>0 AND m.pDATI(1,1)=this.w_CODICEENTE AND (m.pDATI(1,2)="3940" OR m.pDATI(1,2)="3943" ),1,0)+IIF (m.pDATI(2,4)<>0 AND m.pDATI(2,1)=this.w_CODICEENTE AND (m.pDATI(2,2)="3940" OR m.pDATI(2,2)="3943"),1,0)+IIF (m.pDATI(3,4)<>0 AND m.pDATI(3,1)=this.w_CODICEENTE AND (m.pDATI(3,2)="3940" OR m.pDATI(3,2)="3943"),1,0)+IIF (m.pDATI(4,4)<>0 AND m.pDATI(4,1)=this.w_CODICEENTE AND (m.pDATI(4,2)="3940" OR m.pDATI(4,2)="3943"),1,0) 
      this.w_CONTA = this.w_CONTA + 1
      * --- Controlli relativi ad ogni ente 
      if this.w_PRESENZACODICI>0 AND this.w_TOTCRE >this.w_TOTDEB AND NOT this.w_TROVATO
        this.w_oERRORLOG.AddMsgLog(" -Sezione ICI/ENTI LOCALI: in presenza di codici tributo ICI 3940 e/o 3943, la sommatoria dei campi 'Importo a credito compensato'  ")     
        this.w_oERRORLOG.AddMsgLog("non pu� essere maggiore della sommatoria dei campi 'Importo a debito versato' nell'ambito dello stesso codice ente / comune.")     
        this.w_TROVATO = .T.
      endif
    enddo
    * --- Se si sono riscontrati errori visualizza il report
    if this.w_oErrorLog.IsFullLog()
      this.w_oERRORLOG.PrintLog(this,"Si sono riscontrati alcuni errori nella compilazione del modello :",.T., "Stampo situazione messaggi di errore?")     
      if ah_YesNo("Si vuole interrompere l'elaborazione?")
        m.w_ERRORE=-1
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo che siano valorizzzati i campi "immobili variati" per i tributi ICI
    this.w_oMESS4=createobject("AH_MESSAGE")
    this.w_DAREMSG = .F.
    this.w_CODTRIB = space(25)+"- "
    if INLIST (this.oParentObject.w_IFTRIEL1,"3901","3902","3903","3904","3905","3906","3907") and empty(this.oParentObject.w_IFNUMFA1)
      this.w_DAREMSG = .T.
      this.w_CODTRIB = this.w_CODTRIB+this.oParentObject.w_IFTRIEL1+" - "
    endif
    if INLIST (this.oParentObject.w_IFTRIEL2,"3901","3902","3903","3904","3905","3906","3907") and empty(this.oParentObject.w_IFNUMFA2)
      this.w_DAREMSG = .T.
      this.w_CODTRIB = this.w_CODTRIB+this.oParentObject.w_IFTRIEL2+" - "
    endif
    if INLIST (this.oParentObject.w_IFTRIEL3,"3901","3902","3903","3904","3905","3906","3907") and empty(this.oParentObject.w_IFNUMFA3)
      this.w_DAREMSG = .T.
      this.w_CODTRIB = this.w_CODTRIB+this.oParentObject.w_IFTRIEL3+" - "
    endif
    if INLIST (this.oParentObject.w_IFTRIEL4,"3901","3902","3903","3904","3905","3906","3907") and empty(this.oParentObject.w_IFNUMFA4)
      this.w_DAREMSG = .T.
      this.w_CODTRIB = this.w_CODTRIB+this.oParentObject.w_IFTRIEL4+" - "
    endif
    if this.w_DAREMSG
      AH_ERRORMSG("Non � stato impostato il campo numero immobili nei seguenti cod. tributo: %1",48,,chr(13)+this.w_CODTRIB)
    endif
    * --- Controlli richiesti dal 10/01/08
    I="1"
    this.w_oPART4 = this.w_oMESS4.AddMsgPartNL("Sez. ICI ed altri tributi locali:")
    do while VAL(I) <= 4
      if this.oParentObject.w_CHKOBBME&I="S"
        if empty(this.oParentObject.w_IFMESER&I)
          this.oParentObject.w_RESCHK = -1
          this.w_oPART4 = this.w_oMESS4.AddMsgPartNL("Attenzione: non � stato valorizzato il campo 'Mese di riferimento' per il codice tributo: %1")
          this.w_oPART4.AddParam(this.oParentObject.w_IFTRIEL&I)     
        endif
        if ! empty(this.oParentObject.w_IFRATEL&I)
          this.oParentObject.w_RESCHK = -1
          this.w_oPART4 = this.w_oMESS4.AddMsgPartNL("Attenzione: errata valorizzazione del campo 'rateazione' in corrispondenza del codice tributo:  %1")
          this.w_oPART4.AddParam(this.oParentObject.w_IFTRIEL&I)     
        endif
      endif
      I=ALLTRIM(STR(VAL(I)+1))
    enddo
    if this.oParentObject.w_RESCHK=-1
      this.w_oMESS4.AH_ERRORMSG()     
    endif
  endproc


  proc Init(oParentObject,pDATI,w_ERRORE)
    this.pDATI=pDATI
    this.w_ERRORE=w_ERRORE
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pDATI,w_ERRORE"
endproc
