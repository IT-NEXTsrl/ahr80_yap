* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_ble                                                        *
*              Gestione lettere intento comunicate                             *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2015-02-03                                                      *
* Last revis.: 2017-02-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTIPO
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_ble",oParentObject,m.pTIPO)
return(i_retval)

define class tgscg_ble as StdBatch
  * --- Local variables
  pTIPO = 0
  w_CLIFOR = space(1)
  w_CODICE = space(15)
  w_CODICE1 = space(15)
  w_NUMERO1 = 0
  w_NUMERO2 = 0
  w_DATA1 = ctod("  /  /  ")
  w_DATA2 = ctod("  /  /  ")
  w_TIPIVA = space(1)
  w_STATO01 = space(1)
  w_STATO02 = space(1)
  w_SERINI = space(1)
  w_SERFIN = space(1)
  w_DISER = space(10)
  w_STATO03 = space(1)
  w_COMUNIC = .NULL.
  * --- WorkFile variables
  GEDICDEM_idx=0
  runtime_filters = 2

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    do case
      case this.pTipo=10
        this.w_CLIFOR = "E"
        this.w_STATO03 = "R"
        this.w_STATO01 = "S"
        this.w_STATO02 = "N"
        this.w_DISER = this.oparentobject.w_DISERIAL
        * --- Select from GSCG_SDE
        do vq_exec with 'GSCG_SDE',this,'_Curs_GSCG_SDE','',.f.,.t.
        if used('_Curs_GSCG_SDE')
          select _Curs_GSCG_SDE
          locate for 1=1
          do while not(eof())
          this.oParentObject.w_SERIALE = NVL (DGSERIAL, " ")
          exit
            select _Curs_GSCG_SDE
            continue
          enddo
          use
        endif
        this.oparentobject.mHideControls()
      case this.pTipo=11
        this.w_COMUNIC = GSCG_ADE()
        this.w_COMUNIC.w_DGSERIAL = this.oParentObject.w_SERIALE
        this.w_COMUNIC.QueryKeySet("DGSERIAL='"+this.oParentObject.w_SERIALE+ "'","")     
        this.w_COMUNIC.LoadRecWarn()     
        if TYPE ("this.w_COMUNIC.otABMENU.OTABMENU")="O"
          this.w_COMUNIC.otABMENU.OTABMENU.MenuItem003.CMDMENUITEM.Click()     
        else
          this.w_COMUNIC.opgfrm.activepage = 3
        endif
        SELECT (this.w_COMUNIC.w_RettZoom.ccursor) 
 Locate FOR DISERIAL=this.oparentobject.w_DISERIAL
        SELECT (this.w_COMUNIC.w_IntZoom.ccursor) 
 Locate FOR DISERIAL=this.oparentobject.w_DISERIAL
        SELECT (this.w_COMUNIC.w_CalcZoom.ccursor) 
 Locate FOR DISERIAL=this.oparentobject.w_DISERIAL
      case this.pTipo=12
        * --- Cancella comunicazioni
        * --- Delete from GEDICDEM
        i_nConn=i_TableProp[this.GEDICDEM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GEDICDEM_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"DDSERDIC = "+cp_ToStrODBC(this.oParentObject.w_DISERIAL);
                 )
        else
          delete from (i_cTable) where;
                DDSERDIC = this.oParentObject.w_DISERIAL;

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
        * --- Con il passaggio da vecchie release DDSERDIC potrebbe essere a 10 caratteri
        * --- Delete from GEDICDEM
        i_nConn=i_TableProp[this.GEDICDEM_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.GEDICDEM_idx,2])
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        if i_nConn<>0
          i_Rows=cp_TrsSQL(i_nConn,"delete from "+i_cTable+" where ";
                +"DDSERDIC = "+cp_ToStrODBC(right (alltrim(this.oParentObject.w_DISERIAL),10));
                 )
        else
          delete from (i_cTable) where;
                DDSERDIC = right (alltrim(this.oParentObject.w_DISERIAL),10);

          i_Rows=_tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          * --- Error: delete not accepted
          i_Error=MSG_DELETE_ERROR
          return
        endif
    endcase
  endproc


  proc Init(oParentObject,pTIPO)
    this.pTIPO=pTIPO
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='GEDICDEM'
    return(this.OpenAllTables(1))

  proc CloseCursors()
    if used('_Curs_GSCG_SDE')
      use in _Curs_GSCG_SDE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTIPO"
endproc
