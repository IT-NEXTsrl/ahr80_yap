* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_ao8                                                        *
*              Gestione percorsi                                               *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_7]                                                   *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-12                                                      *
* Last revis.: 2012-11-02                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,i_cType
* --- Area Manuale = Header
* --- Fine Area Manuale
local i_Obj
if vartype(m.oParentObject)<>'O'
  wait wind cp_Translate(MSG_CANNOT_EXEC_PROGRAM_FROM_MENU)
  return(.NULL.)
endif
if i_cType='edit'
  return(.NULL.)
else
  if i_cType='paint'
    i_Obj=createobject("tgsar_ao8")
    i_Obj.cnt.oParentObject=oParentObject
    i_Obj.LinkPCClick()
  else
    i_obj=CREATEOBJECT('stdLazyChild',oparentobject,program())
  endif
endif
return(i_Obj)

proc _CreateObject(retobj)
  retobj=CreateObject("tcgsar_ao8")
  return

proc _AddObject(i_oCnt,i_name)
  i_oCnt.AddObject(i_name,"tcgsar_ao8")
  return

* --- Class definition
define class tgsar_ao8 as StdPCForm
  Width  = 797
  Height = 436
  Top    = 5
  Left   = 4
  cComment = "Gestione percorsi"
  cPrg = "gsar_ao8"
  HelpContextID=176781161
  add object cnt as tcgsar_ao8
  * --- Area Manuale = Declare Variables StdPCForm
  * --- Fine Area Manuale
enddefine

define class tsgsar_ao8 as PCContext
  w_COPATHDE = space(200)
  w_COPATHSJ = space(254)
  w_COPATHB2 = space(200)
  w_COPATHBO = space(200)
  w_COPATHED = space(200)
  w_COPATCBI = space(254)
  w_COCODAZI = space(5)
  w_COPATHF4 = space(200)
  w_COPATHOU = space(200)
  w_COPATSOS = space(254)
  w_COPATHPR = space(254)
  w_COPATHPS = space(200)
  w_COPATHDS = space(254)
  w_COPATHFA = space(200)
  w_COCODIDE = space(5)
  w_COCODIDA = space(4)
  w_COPROATT = space(3)
  w_LPATH = space(1)
  proc Save(oFrom)
    this.w_COPATHDE = oFrom.w_COPATHDE
    this.w_COPATHSJ = oFrom.w_COPATHSJ
    this.w_COPATHB2 = oFrom.w_COPATHB2
    this.w_COPATHBO = oFrom.w_COPATHBO
    this.w_COPATHED = oFrom.w_COPATHED
    this.w_COPATCBI = oFrom.w_COPATCBI
    this.w_COCODAZI = oFrom.w_COCODAZI
    this.w_COPATHF4 = oFrom.w_COPATHF4
    this.w_COPATHOU = oFrom.w_COPATHOU
    this.w_COPATSOS = oFrom.w_COPATSOS
    this.w_COPATHPR = oFrom.w_COPATHPR
    this.w_COPATHPS = oFrom.w_COPATHPS
    this.w_COPATHDS = oFrom.w_COPATHDS
    this.w_COPATHFA = oFrom.w_COPATHFA
    this.w_COCODIDE = oFrom.w_COCODIDE
    this.w_COCODIDA = oFrom.w_COCODIDA
    this.w_COPROATT = oFrom.w_COPROATT
    this.w_LPATH = oFrom.w_LPATH
    PCContext::Save(oFrom)
  proc Load(oTo)
    oTo.w_COPATHDE = this.w_COPATHDE
    oTo.w_COPATHSJ = this.w_COPATHSJ
    oTo.w_COPATHB2 = this.w_COPATHB2
    oTo.w_COPATHBO = this.w_COPATHBO
    oTo.w_COPATHED = this.w_COPATHED
    oTo.w_COPATCBI = this.w_COPATCBI
    oTo.w_COCODAZI = this.w_COCODAZI
    oTo.w_COPATHF4 = this.w_COPATHF4
    oTo.w_COPATHOU = this.w_COPATHOU
    oTo.w_COPATSOS = this.w_COPATSOS
    oTo.w_COPATHPR = this.w_COPATHPR
    oTo.w_COPATHPS = this.w_COPATHPS
    oTo.w_COPATHDS = this.w_COPATHDS
    oTo.w_COPATHFA = this.w_COPATHFA
    oTo.w_COCODIDE = this.w_COCODIDE
    oTo.w_COCODIDA = this.w_COCODIDA
    oTo.w_COPROATT = this.w_COPROATT
    oTo.w_LPATH = this.w_LPATH
    PCContext::Load(oTo)
enddefine

define class tcgsar_ao8 as StdPCContainer
  Top    = 0
  Left   = 0

  * --- Standard Properties
  Width  = 797
  Height = 436
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2012-11-02"
  HelpContextID=176781161
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Constant Properties
  CONTROPA_IDX = 0
  cFile = "CONTROPA"
  cKeySelect = "COCODAZI"
  cKeyWhere  = "COCODAZI=this.w_COCODAZI"
  cKeyWhereODBC = '"COCODAZI="+cp_ToStrODBC(this.w_COCODAZI)';

  cKeyWhereODBCqualified = '"CONTROPA.COCODAZI="+cp_ToStrODBC(this.w_COCODAZI)';

  cPrg = "gsar_ao8"
  cComment = "Gestione percorsi"
  icon = "anag.ico"
  * --- Area Manuale = Properties
  * --- Fine Area Manuale

  * --- Local Variables
  w_COPATHDE = space(200)
  o_COPATHDE = space(200)
  w_COPATHSJ = space(254)
  o_COPATHSJ = space(254)
  w_COPATHB2 = space(200)
  o_COPATHB2 = space(200)
  w_COPATHBO = space(200)
  o_COPATHBO = space(200)
  w_COPATHED = space(200)
  o_COPATHED = space(200)
  w_COPATCBI = space(254)
  o_COPATCBI = space(254)
  w_COCODAZI = space(5)
  w_COPATHF4 = space(200)
  o_COPATHF4 = space(200)
  w_COPATHOU = space(200)
  o_COPATHOU = space(200)
  w_COPATSOS = space(254)
  o_COPATSOS = space(254)
  w_COPATHPR = space(254)
  o_COPATHPR = space(254)
  w_COPATHPS = space(200)
  o_COPATHPS = space(200)
  w_COPATHDS = space(254)
  o_COPATHDS = space(254)
  w_COPATHFA = space(200)
  o_COPATHFA = space(200)
  w_COCODIDE = space(5)
  w_COCODIDA = space(4)
  w_COPROATT = space(3)
  w_LPATH = .F.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_ao8Pag1","gsar_ao8",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).HelpContextID = 118333706
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCOPATHDE_1_2
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc

  * --- Opening Tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='CONTROPA'
    * --- Area Manuale = Open Work Table
    * --- Fine Area Manuale
  return(this.OpenAllTables(1))

  procedure SetPostItConn()
    this.bPostIt=i_ServerConn[i_TableProp[this.CONTROPA_IDX,5],7]
    this.nPostItConn=i_TableProp[this.CONTROPA_IDX,3]
  return

  procedure NewContext()
    return(createobject('tsgsar_ao8'))


  * --- Read record and initialize Form variables
  procedure LoadRec()
    local i_cKey,i_nRes,i_cTable,i_nConn
    local i_cSel,i_cDatabaseType,i_nFlds
    * --- Area Manuale = Load Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=1
      return
    endif
    if this.cFunction='Load'
      this.BlankRec()
      return
    endif
    this.nDeferredFillRec=0
    this.bF10=.f.
    this.bUpdated=.f.
    * --- The following Select reads the record
    *
    * select * from CONTROPA where COCODAZI=KeySet.COCODAZI
    *
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    if i_nConn<>0
      i_nFlds = i_dcx.getfieldscount('CONTROPA')
      i_cDatabaseType = cp_GetDatabaseType(i_nConn)
      i_cSel = "CONTROPA.*"
      i_cKey = this.cKeyWhereODBCqualified
      i_cTable = i_cTable+' CONTROPA '
      i_nRes = cp_SQL(i_nConn,"select "+i_cSel+" from "+i_cTable+" where "+&i_cKey,this.cCursor)
      this.bLoaded = i_nRes<>-1 and not(eof())
    else
      * i_cKey = this.cKeyWhere
      i_cKey = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
      select * from (i_cTable) where &i_cKey into cursor (this.cCursor) nofilter
      this.bLoaded = not(eof())
    endif
    * --- Copy values in work variables
    if this.bLoaded
      with this
        .w_LPATH = .f.
        .w_COPATHDE = NVL(COPATHDE,space(200))
        .w_COPATHSJ = NVL(COPATHSJ,space(254))
        .w_COPATHB2 = NVL(COPATHB2,space(200))
        .w_COPATHBO = NVL(COPATHBO,space(200))
        .w_COPATHED = NVL(COPATHED,space(200))
        .w_COPATCBI = NVL(COPATCBI,space(254))
        .w_COCODAZI = NVL(COCODAZI,space(5))
        .w_COPATHF4 = NVL(COPATHF4,space(200))
        .w_COPATHOU = NVL(COPATHOU,space(200))
        .w_COPATSOS = NVL(COPATSOS,space(254))
        .w_COPATHPR = NVL(COPATHPR,space(254))
        .w_COPATHPS = NVL(COPATHPS,space(200))
        .w_COPATHDS = NVL(COPATHDS,space(254))
        .w_COPATHFA = NVL(COPATHFA,space(200))
        .w_COCODIDE = NVL(COCODIDE,space(5))
        .w_COCODIDA = NVL(COCODIDA,space(4))
        .w_COPROATT = NVL(COPROATT,space(3))
        cp_LoadRecExtFlds(this,'CONTROPA')
      endwith
      this.SaveDependsOn()
      this.SetControlsValue()
      this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
      this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
      this.mHideControls()
      this.ChildrenChangeRow()
      this.NotifyEvent('Load')
    else
      this.BlankRec()
    endif
    * --- Area Manuale = Load Record End
    * --- Fine Area Manuale
  endproc

  * --- Blank form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    if not(this.bOnScreen)
      this.nDeferredFillRec=2
      return
    endif
    this.nDeferredFillRec=0
    this.bUpdated=.f.
    this.bLoaded=.f.
    with this
      .w_COPATHDE = space(200)
      .w_COPATHSJ = space(254)
      .w_COPATHB2 = space(200)
      .w_COPATHBO = space(200)
      .w_COPATHED = space(200)
      .w_COPATCBI = space(254)
      .w_COCODAZI = space(5)
      .w_COPATHF4 = space(200)
      .w_COPATHOU = space(200)
      .w_COPATSOS = space(254)
      .w_COPATHPR = space(254)
      .w_COPATHPS = space(200)
      .w_COPATHDS = space(254)
      .w_COPATHFA = space(200)
      .w_COCODIDE = space(5)
      .w_COCODIDA = space(4)
      .w_COPROATT = space(3)
      .w_LPATH = .f.
      if .cFunction<>"Filter"
      endif
    endwith
    cp_BlankRecExtFlds(this,'CONTROPA')
    this.DoRTCalc(1,18,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_3.enabled = this.oPgFrm.Page1.oPag.oBtn_1_3.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_5.enabled = this.oPgFrm.Page1.oPag.oBtn_1_5.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_8.enabled = this.oPgFrm.Page1.oPag.oBtn_1_8.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_10.enabled = this.oPgFrm.Page1.oPag.oBtn_1_10.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_12.enabled = this.oPgFrm.Page1.oPag.oBtn_1_12.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_28.enabled = this.oPgFrm.Page1.oPag.oBtn_1_28.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_30.enabled = this.oPgFrm.Page1.oPag.oBtn_1_30.mCond()
    this.mHideControls()
    this.ChildrenChangeRow()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = setting operation
  *     Allowed parameters  : Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    local i_bVal
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    i_bVal = i_cOp<>"Query" && and i_cOp<>"Filter"
    * --- Disable List page when <> from Query
    with this.oPgFrm
      .Page1.oPag.oCOPATHDE_1_2.enabled = i_bVal
      .Page1.oPag.oCOPATHSJ_1_4.enabled = i_bVal
      .Page1.oPag.oCOPATHB2_1_7.enabled = i_bVal
      .Page1.oPag.oCOPATHBO_1_9.enabled = i_bVal
      .Page1.oPag.oCOPATHED_1_11.enabled = i_bVal
      .Page1.oPag.oCOPATCBI_1_13.enabled = i_bVal
      .Page1.oPag.oCOPATHF4_1_16.enabled = i_bVal
      .Page1.oPag.oCOPATHOU_1_19.enabled = i_bVal
      .Page1.oPag.oCOPATSOS_1_21.enabled = i_bVal
      .Page1.oPag.oCOPATHPR_1_23.enabled = i_bVal
      .Page1.oPag.oCOPATHPS_1_25.enabled = i_bVal
      .Page1.oPag.oCOPATHDS_1_27.enabled = i_bVal
      .Page1.oPag.oCOPATHFA_1_29.enabled = i_bVal
      .Page1.oPag.oCOCODIDE_1_31.enabled = i_bVal
      .Page1.oPag.oCOCODIDA_1_32.enabled = i_bVal
      .Page1.oPag.oCOPROATT_1_33.enabled = i_bVal
      .Page1.oPag.oBtn_1_3.enabled = .Page1.oPag.oBtn_1_3.mCond()
      .Page1.oPag.oBtn_1_5.enabled = .Page1.oPag.oBtn_1_5.mCond()
      .Page1.oPag.oBtn_1_8.enabled = .Page1.oPag.oBtn_1_8.mCond()
      .Page1.oPag.oBtn_1_10.enabled = .Page1.oPag.oBtn_1_10.mCond()
      .Page1.oPag.oBtn_1_12.enabled = .Page1.oPag.oBtn_1_12.mCond()
      .Page1.oPag.oBtn_1_14.enabled = .Page1.oPag.oBtn_1_14.mCond()
      .Page1.oPag.oBtn_1_17.enabled = .Page1.oPag.oBtn_1_17.mCond()
      .Page1.oPag.oBtn_1_20.enabled = .Page1.oPag.oBtn_1_20.mCond()
      .Page1.oPag.oBtn_1_22.enabled = i_bVal
      .Page1.oPag.oBtn_1_24.enabled = .Page1.oPag.oBtn_1_24.mCond()
      .Page1.oPag.oBtn_1_26.enabled = .Page1.oPag.oBtn_1_26.mCond()
      .Page1.oPag.oBtn_1_28.enabled = .Page1.oPag.oBtn_1_28.mCond()
      .Page1.oPag.oBtn_1_30.enabled = .Page1.oPag.oBtn_1_30.mCond()
    endwith
    cp_SetEnabledExtFlds(this,'CONTROPA',i_cOp)
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt,i_nConn
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cFlt = this.cQueryFilter
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPATHDE,"COPATHDE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPATHSJ,"COPATHSJ",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPATHB2,"COPATHB2",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPATHBO,"COPATHBO",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPATHED,"COPATHED",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPATCBI,"COPATCBI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODAZI,"COCODAZI",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPATHF4,"COPATHF4",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPATHOU,"COPATHOU",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPATSOS,"COPATSOS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPATHPR,"COPATHPR",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPATHPS,"COPATHPS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPATHDS,"COPATHDS",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPATHFA,"COPATHFA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODIDE,"COCODIDE",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COCODIDA,"COCODIDA",i_nConn)
    i_cFlt = cp_BuildWhere(i_cFlt,this.w_COPROATT,"COPROATT",i_nConn)
    * --- Area Manuale = Build Filter
    * --- Fine Area Manuale
    return (i_cFlt)
  endfunc

  * --- Insert of new Record
  function mInsert()
    local i_cKey,i_nConn,i_cTable,i_extfld,i_extval,i_ccchkf,i_cccchkv,i_nnn
    * --- Area Manuale = Insert Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if this.bUpdated .or. this.IsAChildUpdated()
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkf=''
      i_ccchkv=''
      this.SetCCCHKVarInsert(@i_ccchkf,@i_ccchkv,this.CONTROPA_IDX,i_nConn)
      * --- Area Manuale = Autonum Assigned
      * --- Fine Area Manuale
      *
      * insert into CONTROPA
      *
      this.NotifyEvent('Insert start')
      this.mUpdateTrs()
      if i_nConn<>0
        i_extfld=cp_InsertFldODBCExtFlds(this,'CONTROPA')
        i_extval=cp_InsertValODBCExtFlds(this,'CONTROPA')
        i_nnn="INSERT INTO "+i_cTable+" "+;
                  "(COPATHDE,COPATHSJ,COPATHB2,COPATHBO,COPATHED"+;
                  ",COPATCBI,COCODAZI,COPATHF4,COPATHOU,COPATSOS"+;
                  ",COPATHPR,COPATHPS,COPATHDS,COPATHFA,COCODIDE"+;
                  ",COCODIDA,COPROATT "+i_extfld+i_ccchkf+") VALUES ("+;
                  cp_ToStrODBC(this.w_COPATHDE)+;
                  ","+cp_ToStrODBC(this.w_COPATHSJ)+;
                  ","+cp_ToStrODBC(this.w_COPATHB2)+;
                  ","+cp_ToStrODBC(this.w_COPATHBO)+;
                  ","+cp_ToStrODBC(this.w_COPATHED)+;
                  ","+cp_ToStrODBC(this.w_COPATCBI)+;
                  ","+cp_ToStrODBC(this.w_COCODAZI)+;
                  ","+cp_ToStrODBC(this.w_COPATHF4)+;
                  ","+cp_ToStrODBC(this.w_COPATHOU)+;
                  ","+cp_ToStrODBC(this.w_COPATSOS)+;
                  ","+cp_ToStrODBC(this.w_COPATHPR)+;
                  ","+cp_ToStrODBC(this.w_COPATHPS)+;
                  ","+cp_ToStrODBC(this.w_COPATHDS)+;
                  ","+cp_ToStrODBC(this.w_COPATHFA)+;
                  ","+cp_ToStrODBC(this.w_COCODIDE)+;
                  ","+cp_ToStrODBC(this.w_COCODIDA)+;
                  ","+cp_ToStrODBC(this.w_COPROATT)+;
                 i_extval+i_ccchkv+")"
         =cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_InsertFldVFPExtFlds(this,'CONTROPA')
        i_extval=cp_InsertValVFPExtFlds(this,'CONTROPA')
        cp_CheckDeletedKey(i_cTable,0,'COCODAZI',this.w_COCODAZI)
        INSERT INTO (i_cTable);
              (COPATHDE,COPATHSJ,COPATHB2,COPATHBO,COPATHED,COPATCBI,COCODAZI,COPATHF4,COPATHOU,COPATSOS,COPATHPR,COPATHPS,COPATHDS,COPATHFA,COCODIDE,COCODIDA,COPROATT  &i_extfld. &i_ccchkf.) VALUES (;
                  this.w_COPATHDE;
                  ,this.w_COPATHSJ;
                  ,this.w_COPATHB2;
                  ,this.w_COPATHBO;
                  ,this.w_COPATHED;
                  ,this.w_COPATCBI;
                  ,this.w_COCODAZI;
                  ,this.w_COPATHF4;
                  ,this.w_COPATHOU;
                  ,this.w_COPATSOS;
                  ,this.w_COPATHPR;
                  ,this.w_COPATHPS;
                  ,this.w_COPATHDS;
                  ,this.w_COPATHFA;
                  ,this.w_COCODIDE;
                  ,this.w_COCODIDA;
                  ,this.w_COPROATT;
            &i_extval. &i_ccchkv.)
      endif
      this.NotifyEvent('Insert end')
    endif
    * --- Area Manuale = Insert End
    * --- Fine Area Manuale
  return(not(bTrsErr))

  * --- Update Database
  function mReplace(i_bEditing)
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_extfld,i_ccchkf,i_ccchkw,i_nnn
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    if this.nDeferredFillRec<>0
      return
    endif
    if not(this.bLoaded)
      this.mInsert()
      i_bEditing=.f.
    else
      i_bEditing=.t.
    endif
    if this.bUpdated and i_bEditing
      this.mRestoreTrs()
      this.mUpdateTrs()
    endif
    if this.bUpdated and i_bEditing
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkf=''
      i_ccchkw=''
      this.SetCCCHKVarReplace(@i_ccchkf,@i_ccchkw,i_bEditing,this.CONTROPA_IDX,i_nConn)
      *
      * update CONTROPA
      *
      this.NotifyEvent('Update start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_extfld=cp_ReplaceODBCExtFlds(this,'CONTROPA')
        i_nnn="UPDATE "+i_cTable+" SET"+;
             " COPATHDE="+cp_ToStrODBC(this.w_COPATHDE)+;
             ",COPATHSJ="+cp_ToStrODBC(this.w_COPATHSJ)+;
             ",COPATHB2="+cp_ToStrODBC(this.w_COPATHB2)+;
             ",COPATHBO="+cp_ToStrODBC(this.w_COPATHBO)+;
             ",COPATHED="+cp_ToStrODBC(this.w_COPATHED)+;
             ",COPATCBI="+cp_ToStrODBC(this.w_COPATCBI)+;
             ",COPATHF4="+cp_ToStrODBC(this.w_COPATHF4)+;
             ",COPATHOU="+cp_ToStrODBC(this.w_COPATHOU)+;
             ",COPATSOS="+cp_ToStrODBC(this.w_COPATSOS)+;
             ",COPATHPR="+cp_ToStrODBC(this.w_COPATHPR)+;
             ",COPATHPS="+cp_ToStrODBC(this.w_COPATHPS)+;
             ",COPATHDS="+cp_ToStrODBC(this.w_COPATHDS)+;
             ",COPATHFA="+cp_ToStrODBC(this.w_COPATHFA)+;
             ",COCODIDE="+cp_ToStrODBC(this.w_COCODIDE)+;
             ",COCODIDA="+cp_ToStrODBC(this.w_COCODIDA)+;
             ",COPROATT="+cp_ToStrODBC(this.w_COPROATT)+;
           i_extfld+i_ccchkf+" WHERE "+&i_cWhere+i_ccchkw
          i_nModRow=cp_TrsSQL(i_nConn,i_nnn)
      else
        i_extfld=cp_ReplaceVFPExtFlds(this,'CONTROPA')
        i_cWhere = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
        UPDATE (i_cTable) SET;
              COPATHDE=this.w_COPATHDE;
             ,COPATHSJ=this.w_COPATHSJ;
             ,COPATHB2=this.w_COPATHB2;
             ,COPATHBO=this.w_COPATHBO;
             ,COPATHED=this.w_COPATHED;
             ,COPATCBI=this.w_COPATCBI;
             ,COPATHF4=this.w_COPATHF4;
             ,COPATHOU=this.w_COPATHOU;
             ,COPATSOS=this.w_COPATSOS;
             ,COPATHPR=this.w_COPATHPR;
             ,COPATHPS=this.w_COPATHPS;
             ,COPATHDS=this.w_COPATHDS;
             ,COPATHFA=this.w_COPATHFA;
             ,COCODIDE=this.w_COCODIDE;
             ,COCODIDA=this.w_COCODIDA;
             ,COPROATT=this.w_COPROATT;
             &i_extfld. &i_ccchkf. WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      this.NotifyEvent('Update end')
    endif
    if not(bTrsErr)
    endif
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(bTrsErr)

  * --- Delete Records
  function mDelete()
    local i_cWhere,i_nModRow,i_nConn,i_cTable,i_ccchkw
    * --- Area Manuale = Delete Init
    * --- Fine Area Manuale
    local i_bLoadNow
    if this.nDeferredFillRec<>0  && record is not loaded
      i_bLoadNow=.t.
      this.bOnScreen=.t.         && force loadrec in future
      this.LoadRec()             && record loaded, timestamp correct
    endif
    if this.nDeferredFillRec=0 and this.bLoaded=.f.
      if i_bLoadNow
        this.bOnScreen=.f.
      endif
      return
    endif
    *
    if not(bTrsErr)
      i_nConn = i_TableProp[this.CONTROPA_IDX,3]
      i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
      i_ccchkw=''
      this.SetCCCHKVarDelete(@i_ccchkw,this.CONTROPA_IDX,i_nConn)
      *
      * delete CONTROPA
      *
      this.NotifyEvent('Delete start')
      if i_nConn<>0
        i_cWhere = this.cKeyWhereODBC
        i_nModRow = cp_TrsSQL(i_nConn,"DELETE FROM "+i_cTable+;
                       " WHERE "+&i_cWhere+i_ccchkw)
      else
        * i_cWhere = this.cKeyWhere
        i_cWhere = cp_PKFox(i_cTable  ,'COCODAZI',this.w_COCODAZI  )
        DELETE FROM (i_cTable) WHERE &i_cWhere. &i_ccchkw.
        i_nModRow=_tally
      endif
      =cp_CheckMultiuser(i_nModRow)
      if not(bTrsErr)
        this.mRestoreTrs()
      endif
      this.NotifyEvent('Delete end')
    endif
    *
    if i_bLoadNow
      this.bOnScreen=.f.
    endif
    *
    this.mDeleteWarnings()
    * --- Area Manuale = Delete End
    * --- Fine Area Manuale
  return

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    local i_cTable,i_nConn
    i_nConn = i_TableProp[this.CONTROPA_IDX,3]
    i_cTable = cp_SetAzi(i_TableProp[this.CONTROPA_IDX,2])
    if i_bUpd
      with this
        if .o_COPATCBI<>.w_COPATCBI
          .Calculate_SSVLYDPHKD()
        endif
        if .o_COPATHDE<>.w_COPATHDE
          .Calculate_RSPSVRUYYG()
        endif
        if .o_COPATHF4<>.w_COPATHF4
          .Calculate_HSCYDJDQFF()
        endif
        if .o_COPATHSJ<>.w_COPATHSJ
          .Calculate_BTEGICSNBN()
        endif
        if .o_COPATHB2<>.w_COPATHB2
          .Calculate_TNREWXKOUN()
        endif
        if .o_COPATHBO<>.w_COPATHBO
          .Calculate_NDHHIXOJSC()
        endif
        if .o_COPATHED<>.w_COPATHED
          .Calculate_BZXJUPSOJI()
        endif
        if .o_COPATHOU<>.w_COPATHOU
          .Calculate_VOSLAXVLRW()
        endif
        if .o_COPATSOS<>.w_COPATSOS
          .Calculate_DXPAFKACLZ()
        endif
        if .o_COPATHPR<>.w_COPATHPR
          .Calculate_FMIOXIMNVG()
        endif
        if .o_COPATHFA<>.w_COPATHFA
          .Calculate_UGUREVVHBL()
        endif
        if .o_COPATHPS<>.w_COPATHPS
          .Calculate_YKHQAJWKKX()
        endif
        if .o_COPATHDS<>.w_COPATHDS
          .Calculate_MOVEHCSGGJ()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_SFONVUKBBV()
    with this
          * --- Aggiorna path edi
          GSVA_BAP(this;
              ,'P';
              ,.w_COPATHED;
             )
    endwith
  endproc
  proc Calculate_SSVLYDPHKD()
    with this
          * --- Controllo path CBI
          .w_COPATCBI = IIF(right(alltrim(.w_COPATCBI),1)='\' or empty(.w_COPATCBI),.w_COPATCBI,alltrim(.w_COPATCBI)+iif(len(alltrim(.w_COPATCBI))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_COPATCBI,'F')
    endwith
  endproc
  proc Calculate_RSPSVRUYYG()
    with this
          * --- Controllo path dati esterni
          .w_COPATHDE = IIF(right(alltrim(.w_COPATHDE),1)='\' or empty(.w_COPATHDE),.w_COPATHDE,alltrim(.w_COPATHDE)+iif(len(alltrim(.w_COPATHDE))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_COPATHDE,'F')
    endwith
  endproc
  proc Calculate_HSCYDJDQFF()
    with this
          * --- Controllo path F24
          .w_COPATHF4 = IIF(right(alltrim(.w_COPATHF4),1)='\' or empty(.w_COPATHF4),.w_COPATHF4,alltrim(.w_COPATHF4)+iif(len(alltrim(.w_COPATHF4))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_COPATHF4,'F')
    endwith
  endproc
  proc Calculate_BTEGICSNBN()
    with this
          * --- Controllo path schedulatore
          .w_COPATHSJ = IIF(right(alltrim(.w_COPATHSJ),1)='\' or empty(.w_COPATHSJ),.w_COPATHSJ,alltrim(.w_COPATHSJ)+iif(len(alltrim(.w_COPATHSJ))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_COPATHSJ,'F')
    endwith
  endproc
  proc Calculate_TNREWXKOUN()
    with this
          * --- Controllo path xml Basilea2
          .w_COPATHB2 = IIF(right(alltrim(.w_COPATHB2),1)='\' or empty(.w_COPATHB2),.w_COPATHB2,alltrim(.w_COPATHB2)+iif(len(alltrim(.w_COPATHB2))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_COPATHB2,'F')
    endwith
  endproc
  proc Calculate_NDHHIXOJSC()
    with this
          * --- Controllo path bilancio e oltre
          .w_COPATHBO = IIF(right(alltrim(.w_COPATHBO),1)='\' or empty(.w_COPATHBO),.w_COPATHBO,alltrim(.w_COPATHBO)+iif(len(alltrim(.w_COPATHBO))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_COPATHBO,'F')
    endwith
  endproc
  proc Calculate_BZXJUPSOJI()
    with this
          * --- Controllo path EDI
          .w_COPATHED = IIF(right(alltrim(.w_COPATHED),1)='\' or empty(.w_COPATHED),.w_COPATHED,alltrim(.w_COPATHED)+iif(len(alltrim(.w_COPATHED))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_COPATHED,'F')
    endwith
  endproc
  proc Calculate_VOSLAXVLRW()
    with this
          * --- Controllo path output utente
          .w_COPATHOU = IIF(right(alltrim(.w_COPATHOU),1)='\' or empty(.w_COPATHOU),.w_COPATHOU,alltrim(.w_COPATHOU)+iif(len(alltrim(.w_COPATHOU))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_COPATHOU,'F')
    endwith
  endproc
  proc Calculate_DXPAFKACLZ()
    with this
          * --- Controllo path SOStitutiva
          .w_COPATSOS = IIF(right(alltrim(.w_COPATSOS),1)='\' or empty(.w_COPATSOS),.w_COPATSOS,alltrim(.w_COPATSOS)+iif(len(alltrim(.w_COPATSOS))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_COPATSOS,'F')
    endwith
  endproc
  proc Calculate_FMIOXIMNVG()
    with this
          * --- Controllo path prelevamento
          .w_COPATHPR = IIF(right(alltrim(.w_COPATHPR),1)='\' or empty(.w_COPATHPR),.w_COPATHPR,alltrim(.w_COPATHPR)+iif(len(alltrim(.w_COPATHPR))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_COPATHPR,'F')
    endwith
  endproc
  proc Calculate_UGUREVVHBL()
    with this
          * --- Controllo path fisco azienda
          .w_COPATHFA = IIF(right(alltrim(.w_COPATHFA),1)='\' or empty(.w_COPATHFA),.w_COPATHFA,alltrim(.w_COPATHFA)+iif(len(alltrim(.w_COPATHFA))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_COPATHFA,'F')
    endwith
  endproc
  proc Calculate_YKHQAJWKKX()
    with this
          * --- Controllo path print system
          .w_COPATHPS = IIF(right(alltrim(.w_COPATHPS),1)='\' or empty(.w_COPATHPS),.w_COPATHPS,alltrim(.w_COPATHPS)+iif(len(alltrim(.w_COPATHPS))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_COPATHPS,'F')
    endwith
  endproc
  proc Calculate_MOVEHCSGGJ()
    with this
          * --- Controllo path Xml descrittore
          .w_COPATHDS = IIF(right(alltrim(.w_COPATHDS),1)='\' or empty(.w_COPATHDS),.w_COPATHDS,alltrim(.w_COPATHDS)+iif(len(alltrim(.w_COPATHDS))=1,':\','\' ) )
          .w_LPATH = chknfile(.w_COPATHDS,'F')
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oStr_1_6.visible=!this.oPgFrm.Page1.oPag.oStr_1_6.mHide()
    this.oPgFrm.Page1.oPag.oCOPATHB2_1_7.visible=!this.oPgFrm.Page1.oPag.oCOPATHB2_1_7.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_8.visible=!this.oPgFrm.Page1.oPag.oBtn_1_8.mHide()
    this.oPgFrm.Page1.oPag.oCOPATHBO_1_9.visible=!this.oPgFrm.Page1.oPag.oCOPATHBO_1_9.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_10.visible=!this.oPgFrm.Page1.oPag.oBtn_1_10.mHide()
    this.oPgFrm.Page1.oPag.oCOPATHED_1_11.visible=!this.oPgFrm.Page1.oPag.oCOPATHED_1_11.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_12.visible=!this.oPgFrm.Page1.oPag.oBtn_1_12.mHide()
    this.oPgFrm.Page1.oPag.oCOPATCBI_1_13.visible=!this.oPgFrm.Page1.oPag.oCOPATCBI_1_13.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_14.visible=!this.oPgFrm.Page1.oPag.oBtn_1_14.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_18.visible=!this.oPgFrm.Page1.oPag.oStr_1_18.mHide()
    this.oPgFrm.Page1.oPag.oCOPATSOS_1_21.visible=!this.oPgFrm.Page1.oPag.oCOPATSOS_1_21.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_22.visible=!this.oPgFrm.Page1.oPag.oBtn_1_22.mHide()
    this.oPgFrm.Page1.oPag.oCOPATHFA_1_29.visible=!this.oPgFrm.Page1.oPag.oCOPATHFA_1_29.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_30.visible=!this.oPgFrm.Page1.oPag.oBtn_1_30.mHide()
    this.oPgFrm.Page1.oPag.oCOCODIDE_1_31.visible=!this.oPgFrm.Page1.oPag.oCOCODIDE_1_31.mHide()
    this.oPgFrm.Page1.oPag.oCOCODIDA_1_32.visible=!this.oPgFrm.Page1.oPag.oCOCODIDA_1_32.mHide()
    this.oPgFrm.Page1.oPag.oCOPROATT_1_33.visible=!this.oPgFrm.Page1.oPag.oCOPROATT_1_33.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_34.visible=!this.oPgFrm.Page1.oPag.oStr_1_34.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_37.visible=!this.oPgFrm.Page1.oPag.oStr_1_37.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_38.visible=!this.oPgFrm.Page1.oPag.oStr_1_38.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_39.visible=!this.oPgFrm.Page1.oPag.oStr_1_39.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_40.visible=!this.oPgFrm.Page1.oPag.oStr_1_40.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_44.visible=!this.oPgFrm.Page1.oPag.oStr_1_44.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_46.visible=!this.oPgFrm.Page1.oPag.oStr_1_46.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- gsar_ao8
    IF ( cevent='Insert end' OR cevent='Update end') and g_VEFA='S'
       This.Notifyevent('Aggiorna')
    Endif
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Aggiorna")
          .Calculate_SFONVUKBBV()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCOPATHDE_1_2.value==this.w_COPATHDE)
      this.oPgFrm.Page1.oPag.oCOPATHDE_1_2.value=this.w_COPATHDE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPATHSJ_1_4.value==this.w_COPATHSJ)
      this.oPgFrm.Page1.oPag.oCOPATHSJ_1_4.value=this.w_COPATHSJ
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPATHB2_1_7.value==this.w_COPATHB2)
      this.oPgFrm.Page1.oPag.oCOPATHB2_1_7.value=this.w_COPATHB2
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPATHBO_1_9.value==this.w_COPATHBO)
      this.oPgFrm.Page1.oPag.oCOPATHBO_1_9.value=this.w_COPATHBO
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPATHED_1_11.value==this.w_COPATHED)
      this.oPgFrm.Page1.oPag.oCOPATHED_1_11.value=this.w_COPATHED
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPATCBI_1_13.value==this.w_COPATCBI)
      this.oPgFrm.Page1.oPag.oCOPATCBI_1_13.value=this.w_COPATCBI
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPATHF4_1_16.value==this.w_COPATHF4)
      this.oPgFrm.Page1.oPag.oCOPATHF4_1_16.value=this.w_COPATHF4
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPATHOU_1_19.value==this.w_COPATHOU)
      this.oPgFrm.Page1.oPag.oCOPATHOU_1_19.value=this.w_COPATHOU
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPATSOS_1_21.value==this.w_COPATSOS)
      this.oPgFrm.Page1.oPag.oCOPATSOS_1_21.value=this.w_COPATSOS
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPATHPR_1_23.value==this.w_COPATHPR)
      this.oPgFrm.Page1.oPag.oCOPATHPR_1_23.value=this.w_COPATHPR
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPATHPS_1_25.value==this.w_COPATHPS)
      this.oPgFrm.Page1.oPag.oCOPATHPS_1_25.value=this.w_COPATHPS
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPATHDS_1_27.value==this.w_COPATHDS)
      this.oPgFrm.Page1.oPag.oCOPATHDS_1_27.value=this.w_COPATHDS
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPATHFA_1_29.value==this.w_COPATHFA)
      this.oPgFrm.Page1.oPag.oCOPATHFA_1_29.value=this.w_COPATHFA
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODIDE_1_31.value==this.w_COCODIDE)
      this.oPgFrm.Page1.oPag.oCOCODIDE_1_31.value=this.w_COCODIDE
    endif
    if not(this.oPgFrm.Page1.oPag.oCOCODIDA_1_32.value==this.w_COCODIDA)
      this.oPgFrm.Page1.oPag.oCOCODIDA_1_32.value=this.w_COCODIDA
    endif
    if not(this.oPgFrm.Page1.oPag.oCOPROATT_1_33.value==this.w_COPROATT)
      this.oPgFrm.Page1.oPag.oCOPROATT_1_33.value=this.w_COPROATT
    endif
    cp_SetControlsValueExtFlds(this,'CONTROPA')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case not((Right( Alltrim(.w_COPATHB2), 1)='\' Or Empty( .w_COPATHB2) ) And (Right( Alltrim(.w_COPATHSJ), 1)='\' Or Empty( .w_COPATHSJ) ))
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Tutti i percorsi debbono terminare per '\'")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gsar_ao8
      if i_bRes
      	i_cErrorMsg=''
              if !empty(this.w_COPATHDE) and !directory( alltrim(this.w_COPATHDE) )
                i_cErrorMsg= iif(empty(i_cErrorMsg),"",+alltrim(i_cErrorMsg)+", ")+Ah_MsgFormat("dati esterni")
              endif
              if !empty(this.w_COPATHSJ) and !directory( alltrim(this.w_COPATHSJ) )
                i_cErrorMsg= iif(empty(i_cErrorMsg),"",+alltrim(i_cErrorMsg)+", "+CHR(13))+Ah_MsgFormat("schedulatore")
              endif
              if !empty(this.w_COPATHB2) and !directory( alltrim(this.w_COPATHB2) )
                i_cErrorMsg= iif(empty(i_cErrorMsg),"",+alltrim(i_cErrorMsg)+", "+CHR(13))+Ah_MsgFormat("xml Sdi-Basilea2")
              endif
              if !empty(this.w_COPATHBO) and !directory( alltrim(this.w_COPATHBO) )
                i_cErrorMsg= iif(empty(i_cErrorMsg),"",+alltrim(i_cErrorMsg)+", "+CHR(13))+Ah_MsgFormat("xls bilancio&ltre")
              endif
              if !empty(this.w_COPATHED) and !directory( alltrim(this.w_COPATHED) )
                i_cErrorMsg= iif(empty(i_cErrorMsg),"",+alltrim(i_cErrorMsg)+", "+CHR(13))+Ah_MsgFormat("file EDI")
              endif
              if !empty(this.w_COPATCBI) and !directory( alltrim(this.w_COPATCBI) )
                i_cErrorMsg= iif(empty(i_cErrorMsg),"",+alltrim(i_cErrorMsg)+", "+CHR(13))+Ah_MsgFormat("file CBI")
              endif
              if !empty(this.w_COPATHF4) and !directory( alltrim(this.w_COPATHF4) )
                i_cErrorMsg= iif(empty(i_cErrorMsg),"",+alltrim(i_cErrorMsg)+", "+CHR(13))+Ah_MsgFormat("file F24")
              endif
              if !empty(this.w_COPATHOU) and !directory( alltrim(this.w_COPATHOU) )
                i_cErrorMsg= iif(empty(i_cErrorMsg),"",+alltrim(i_cErrorMsg)+", "+CHR(13))+Ah_MsgFormat("file output utente")
              endif
              if !empty(this.w_COPATSOS) and !directory( alltrim(this.w_COPATSOS) )
                i_cErrorMsg= iif(empty(i_cErrorMsg),"",+alltrim(i_cErrorMsg)+", "+CHR(13))+Ah_MsgFormat("file SOStitutiva")
              endif
              if !empty(this.w_COPATHPR) and !directory( alltrim(this.w_COPATHPR) )
                i_cErrorMsg= iif(empty(i_cErrorMsg),"",+alltrim(i_cErrorMsg)+", "+CHR(13))+Ah_MsgFormat("prelevamento")
              endif
              if !empty(this.w_COPATHPS) and !directory( alltrim(this.w_COPATHPS) )
                i_cErrorMsg= iif(empty(i_cErrorMsg),"",+alltrim(i_cErrorMsg)+", "+CHR(13))+Ah_MsgFormat("file print system")
              endif
              if !empty(this.w_COPATHDS) and !directory( alltrim(this.w_COPATHDS) )
                i_cErrorMsg= iif(empty(i_cErrorMsg),"",+alltrim(i_cErrorMsg)+", "+CHR(13))+Ah_MsgFormat("Xml descrittore")
              endif
              if !empty(this.w_COPATHFA) and !directory( alltrim(this.w_COPATHFA) )
                i_cErrorMsg= iif(empty(i_cErrorMsg),"",+alltrim(i_cErrorMsg)+", "+CHR(13))+Ah_MsgFormat("fisco azienda")
              endif
              if !empty(i_cErrorMsg)
                i_bRes = .f.
          	  i_bnoChk = .f.
                i_cErrorMsg=ah_msgformat("La cartella impostata per il path: %0%1 %0non esiste",alltrim(i_cErrorMsg))
              endif
      ENDIF
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_COPATHDE = this.w_COPATHDE
    this.o_COPATHSJ = this.w_COPATHSJ
    this.o_COPATHB2 = this.w_COPATHB2
    this.o_COPATHBO = this.w_COPATHBO
    this.o_COPATHED = this.w_COPATHED
    this.o_COPATCBI = this.w_COPATCBI
    this.o_COPATHF4 = this.w_COPATHF4
    this.o_COPATHOU = this.w_COPATHOU
    this.o_COPATSOS = this.w_COPATSOS
    this.o_COPATHPR = this.w_COPATHPR
    this.o_COPATHPS = this.w_COPATHPS
    this.o_COPATHDS = this.w_COPATHDS
    this.o_COPATHFA = this.w_COPATHFA
    return

enddefine

* --- Define pages as container
define class tgsar_ao8Pag1 as StdContainer
  Width  = 793
  height = 436
  stdWidth  = 793
  stdheight = 436
  resizeXpos=446
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oCOPATHDE_1_2 as StdField with uid="WJYXTENHPQ",rtseq=1,rtrep=.f.,;
    cFormVar = "w_COPATHDE", cQueryName = "COPATHDE",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Percorso carica e salva dati esterni",;
    HelpContextID = 218309525,;
   bGlobalFont=.t.,;
    Height=21, Width=629, Left=133, Top=11, InputMask=replicate('X',200)


  add object oBtn_1_3 as StdButton with uid="CSXZOPISIH",left=764, top=12, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 176580138;
  , bGlobalFont=.t.

    proc oBtn_1_3.Click()
      with this.Parent.oContained
        .w_COPATHDE=left(cp_getdir(IIF(EMPTY(.w_COPATHDE),sys(5)+sys(2003),.w_COPATHDE),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCOPATHSJ_1_4 as StdField with uid="ZMPEKGABJU",rtseq=2,rtrep=.f.,;
    cFormVar = "w_COPATHSJ", cQueryName = "COPATHSJ",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Percorso stampe schedulatore",;
    HelpContextID = 50125936,;
   bGlobalFont=.t.,;
    Height=21, Width=629, Left=133, Top=37, InputMask=replicate('X',254)


  add object oBtn_1_5 as StdButton with uid="YWMDNIBBZZ",left=764, top=38, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 176580138;
  , bGlobalFont=.t.

    proc oBtn_1_5.Click()
      with this.Parent.oContained
        .w_COPATHSJ=left(cp_getdir(IIF(EMPTY(.w_COPATHSJ),sys(5)+sys(2003),.w_COPATHSJ),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCOPATHB2_1_7 as StdField with uid="BQHUQWGQWZ",rtseq=3,rtrep=.f.,;
    cFormVar = "w_COPATHB2", cQueryName = "COPATHB2",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Percorso di salvataggio file xml Sdi-Basilea2",;
    HelpContextID = 50125912,;
   bGlobalFont=.t.,;
    Height=21, Width=629, Left=133, Top=64, InputMask=replicate('X',200)

  func oCOPATHB2_1_7.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oBtn_1_8 as StdButton with uid="TJUFUKFIPV",left=764, top=65, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 176580138;
  , bGlobalFont=.t.

    proc oBtn_1_8.Click()
      with this.Parent.oContained
        .w_COPATHB2=left(cp_getdir(IIF(EMPTY(.w_COPATHB2),sys(5)+sys(2003),.w_COPATHB2),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_8.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt())
     endwith
    endif
  endfunc

  add object oCOPATHBO_1_9 as StdField with uid="BBXQFKCZZQ",rtseq=4,rtrep=.f.,;
    cFormVar = "w_COPATHBO", cQueryName = "COPATHBO",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Percorso file bilancio e oltre per salvataggio file xls",;
    HelpContextID = 50125941,;
   bGlobalFont=.t.,;
    Height=21, Width=629, Left=133, Top=91, InputMask=replicate('X',200)

  func oCOPATHBO_1_9.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oBtn_1_10 as StdButton with uid="BFEVXGJKUT",left=764, top=92, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 176580138;
  , bGlobalFont=.t.

    proc oBtn_1_10.Click()
      with this.Parent.oContained
        .w_COPATHBO=left(cp_getdir(IIF(EMPTY(.w_COPATHBO),sys(5)+sys(2003),.w_COPATHBO),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_10.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt())
     endwith
    endif
  endfunc

  add object oCOPATHED_1_11 as StdField with uid="XHPMBSIOGP",rtseq=5,rtrep=.f.,;
    cFormVar = "w_COPATHED", cQueryName = "COPATHED",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Percorso di generazione file EDI",;
    HelpContextID = 218309526,;
   bGlobalFont=.t.,;
    Height=21, Width=629, Left=133, Top=118, InputMask=replicate('X',200)

  func oCOPATHED_1_11.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S' or IsAlt())
    endwith
  endfunc


  add object oBtn_1_12 as StdButton with uid="VBHSWEUXSG",left=764, top=119, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 176580138;
  , bGlobalFont=.t.

    proc oBtn_1_12.Click()
      with this.Parent.oContained
        .w_COPATHED=left(cp_getdir(IIF(EMPTY(.w_COPATHED),sys(5)+sys(2003),.w_COPATHED),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_12.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_VEFA<>'S' or IsAlt())
     endwith
    endif
  endfunc

  add object oCOPATCBI_1_13 as StdField with uid="EAZGCLWRTW",rtseq=6,rtrep=.f.,;
    cFormVar = "w_COPATCBI", cQueryName = "COPATCBI",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Percorso per generazione file fatture CBI",;
    HelpContextID = 234675311,;
   bGlobalFont=.t.,;
    Height=21, Width=629, Left=133, Top=145, InputMask=replicate('X',254)

  func oCOPATCBI_1_13.mHide()
    with this.Parent.oContained
      return (g_FAEL<>'S' or IsAlt())
    endwith
  endfunc


  add object oBtn_1_14 as StdButton with uid="AVVNWRYYOI",left=764, top=146, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 176580138;
  , bGlobalFont=.t.

    proc oBtn_1_14.Click()
      with this.Parent.oContained
        .w_COPATCBI=left(cp_getdir(IIF(EMPTY(.w_COPATCBI),sys(5)+sys(2003),.w_COPATCBI),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_14.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_FAEL<>'S' or IsAlt())
     endwith
    endif
  endfunc

  add object oCOPATHF4_1_16 as StdField with uid="HQFTTWTLTH",rtseq=8,rtrep=.f.,;
    cFormVar = "w_COPATHF4", cQueryName = "COPATHF4",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Percorso generazione flussi F24",;
    HelpContextID = 218309542,;
   bGlobalFont=.t.,;
    Height=21, Width=629, Left=133, Top=172, InputMask=replicate('X',200)


  add object oBtn_1_17 as StdButton with uid="DGPLSCNLDZ",left=764, top=173, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 176580138;
  , bGlobalFont=.t.

    proc oBtn_1_17.Click()
      with this.Parent.oContained
        .w_COPATHF4=left(cp_getdir(IIF(EMPTY(.w_COPATHF4),sys(5)+sys(2003),.w_COPATHF4),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCOPATHOU_1_19 as StdField with uid="VLDWQXVEEJ",rtseq=9,rtrep=.f.,;
    cFormVar = "w_COPATHOU", cQueryName = "COPATHOU",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Percorso di salvataggio dei file dell' output utente",;
    HelpContextID = 50125947,;
   bGlobalFont=.t.,;
    Height=21, Width=629, Left=133, Top=201, InputMask=replicate('X',200)


  add object oBtn_1_20 as StdButton with uid="UAWLIEQHHT",left=764, top=201, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 176580138;
  , bGlobalFont=.t.

    proc oBtn_1_20.Click()
      with this.Parent.oContained
        .w_COPATHOU=left(cp_getdir(IIF(EMPTY(.w_COPATHOU),sys(5)+sys(2003),.w_COPATHOU),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCOPATSOS_1_21 as StdField with uid="NVENNRSTER",rtseq=10,rtrep=.f.,;
    cFormVar = "w_COPATSOS", cQueryName = "COPATSOS",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Percorso archiviazione documenti invio a SOStitutiva",;
    HelpContextID = 234675321,;
   bGlobalFont=.t.,;
    Height=21, Width=629, Left=133, Top=229, InputMask=replicate('X',254)

  func oCOPATSOS_1_21.mHide()
    with this.Parent.oContained
      return (g_DOCM#'S' or IsAlt())
    endwith
  endfunc


  add object oBtn_1_22 as StdButton with uid="WCMZURYTFI",left=764, top=230, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 176580138;
  , bGlobalFont=.t.

    proc oBtn_1_22.Click()
      with this.Parent.oContained
        .w_COPATSOS=left(cp_getdir(IIF(EMPTY(.w_COPATSOS),sys(5)+sys(2003),.w_COPATSOS),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (g_DOCM#'S' or IsAlt())
     endwith
    endif
  endfunc

  add object oCOPATHPR_1_23 as StdField with uid="ELTWMSIKDQ",rtseq=11,rtrep=.f.,;
    cFormVar = "w_COPATHPR", cQueryName = "COPATHPR",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Percorso di prelevamento file per archiviazione",;
    HelpContextID = 50125944,;
   bGlobalFont=.t.,;
    Height=21, Width=629, Left=133, Top=257, InputMask=replicate('X',254)


  add object oBtn_1_24 as StdButton with uid="TWIMIXDREE",left=764, top=259, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 176580138;
  , bGlobalFont=.t.

    proc oBtn_1_24.Click()
      with this.Parent.oContained
        .w_COPATHPR=left(cp_getdir(IIF(EMPTY(.w_COPATHPR),sys(5)+sys(2003),.w_COPATHPR),"Percorso di prelevamento")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCOPATHPS_1_25 as StdField with uid="ENTJWSHUHD",rtseq=12,rtrep=.f.,;
    cFormVar = "w_COPATHPS", cQueryName = "COPATHPS",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Percorso di salvataggio file print system",;
    HelpContextID = 50125945,;
   bGlobalFont=.t.,;
    Height=21, Width=629, Left=133, Top=284, InputMask=replicate('X',200)


  add object oBtn_1_26 as StdButton with uid="LTQXFOLDBS",left=764, top=286, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 176580138;
  , bGlobalFont=.t.

    proc oBtn_1_26.Click()
      with this.Parent.oContained
        .w_COPATHPS=left(cp_getdir(IIF(EMPTY(.w_COPATHPS),sys(5)+sys(2003),.w_COPATHPS),"Percorso di salvataggio")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCOPATHDS_1_27 as StdField with uid="XDVOCPKQJD",rtseq=13,rtrep=.f.,;
    cFormVar = "w_COPATHDS", cQueryName = "COPATHDS",;
    bObbl = .f. , nPag = 1, value=space(254), bMultilanguage =  .f.,;
    ToolTipText = "Percorso di salvataggio per file xml descrittore per Infinity stand alone",;
    HelpContextID = 218309511,;
   bGlobalFont=.t.,;
    Height=21, Width=629, Left=133, Top=311, InputMask=replicate('X',254)


  add object oBtn_1_28 as StdButton with uid="CIEWHBRUDV",left=764, top=313, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 176580138;
  , bGlobalFont=.t.

    proc oBtn_1_28.Click()
      with this.Parent.oContained
        .w_COPATHDS=padr(cp_getdir(IIF(EMPTY(.w_COPATHDS),sys(5)+sys(2003),.w_COPATHDS),"Percorso di salvataggio"),254)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCOPATHFA_1_29 as StdField with uid="ECKHFSVAFH",rtseq=14,rtrep=.f.,;
    cFormVar = "w_COPATHFA", cQueryName = "COPATHFA",;
    bObbl = .f. , nPag = 1, value=space(200), bMultilanguage =  .f.,;
    ToolTipText = "Percorso di salvataggio file txt fisco azienda",;
    HelpContextID = 218309529,;
   bGlobalFont=.t.,;
    Height=21, Width=629, Left=133, Top=365, InputMask=replicate('X',200)

  func oCOPATHFA_1_29.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc


  add object oBtn_1_30 as StdButton with uid="EPESPDJBEA",left=764, top=365, width=19,height=18,;
    caption="...", nPag=1;
    , HelpContextID = 176580138;
  , bGlobalFont=.t.

    proc oBtn_1_30.Click()
      with this.Parent.oContained
        .w_COPATHFA=left(cp_getdir(IIF(EMPTY(.w_COPATHFA),sys(5)+sys(2003),.w_COPATHFA),"Percorso di destinazione")+space(200),200)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_30.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (IsAlt())
     endwith
    endif
  endfunc

  add object oCOCODIDE_1_31 as StdField with uid="OOJJAADOVQ",rtseq=15,rtrep=.f.,;
    cFormVar = "w_COCODIDE", cQueryName = "COCODIDE",;
    bObbl = .f. , nPag = 1, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "codice identificativo ditta",;
    HelpContextID = 217445269,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=133, Top=395, cSayPict="'9999'", cGetPict="'9999'", InputMask=replicate('X',5)

  func oCOCODIDE_1_31.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oCOCODIDA_1_32 as StdField with uid="GJYWYJNIEZ",rtseq=16,rtrep=.f.,;
    cFormVar = "w_COCODIDA", cQueryName = "COCODIDA",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "codice identificativo contabilitą analitica",;
    HelpContextID = 217445273,;
   bGlobalFont=.t.,;
    Height=21, Width=41, Left=391, Top=395, InputMask=replicate('X',4)

  func oCOCODIDA_1_32.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oCOPROATT_1_33 as StdField with uid="ZQFCPIGKGP",rtseq=17,rtrep=.f.,;
    cFormVar = "w_COPROATT", cQueryName = "COPROATT",;
    bObbl = .f. , nPag = 1, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Progressivo attivitą",;
    HelpContextID = 196992122,;
   bGlobalFont=.t.,;
    Height=21, Width=34, Left=585, Top=395, InputMask=replicate('X',3)

  func oCOPROATT_1_33.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_1 as StdString with uid="NYTFAGEZHB",Visible=.t., Left=3, Top=37,;
    Alignment=1, Width=126, Height=18,;
    Caption="Schedulatore:"  ;
  , bGlobalFont=.t.

  add object oStr_1_6 as StdString with uid="VUHBZMAPIA",Visible=.t., Left=3, Top=64,;
    Alignment=1, Width=126, Height=18,;
    Caption="Xml Sdi-Basilea2:"  ;
  , bGlobalFont=.t.

  func oStr_1_6.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_18 as StdString with uid="WQKHDSYUIU",Visible=.t., Left=22, Top=365,;
    Alignment=1, Width=107, Height=18,;
    Caption="Percorso"  ;
  , bGlobalFont=.t.

  func oStr_1_18.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_34 as StdString with uid="VGMZKAPXMA",Visible=.t., Left=8, Top=340,;
    Alignment=0, Width=351, Height=19,;
    Caption="Parametri fisco azienda"  ;
    , FontName = "Arial", FontSize = 10, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_34.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="NPZVQEFVXG",Visible=.t., Left=32, Top=395,;
    Alignment=1, Width=97, Height=18,;
    Caption="Codice ditta:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_37 as StdString with uid="KCDVVOLHUG",Visible=.t., Left=186, Top=395,;
    Alignment=1, Width=201, Height=18,;
    Caption="Codice contabilitą analitica:"  ;
  , bGlobalFont=.t.

  func oStr_1_37.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_38 as StdString with uid="LJYAIPZGTM",Visible=.t., Left=446, Top=395,;
    Alignment=1, Width=135, Height=18,;
    Caption="Progressivo attivitą:"  ;
  , bGlobalFont=.t.

  func oStr_1_38.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_39 as StdString with uid="FDUOXYGWNH",Visible=.t., Left=3, Top=91,;
    Alignment=1, Width=126, Height=18,;
    Caption="  Xls bilancio\<\<oltre:"  ;
  , bGlobalFont=.t.

  func oStr_1_39.mHide()
    with this.Parent.oContained
      return (IsAlt())
    endwith
  endfunc

  add object oStr_1_40 as StdString with uid="EOJBAAHKKY",Visible=.t., Left=3, Top=120,;
    Alignment=1, Width=126, Height=18,;
    Caption="File EDI:"  ;
  , bGlobalFont=.t.

  func oStr_1_40.mHide()
    with this.Parent.oContained
      return (g_VEFA<>'S' or IsAlt())
    endwith
  endfunc

  add object oStr_1_41 as StdString with uid="YYHEAGFKLP",Visible=.t., Left=3, Top=178,;
    Alignment=1, Width=126, Height=18,;
    Caption="File F24:"  ;
  , bGlobalFont=.t.

  add object oStr_1_42 as StdString with uid="SBHOSDTJCQ",Visible=.t., Left=3, Top=11,;
    Alignment=1, Width=126, Height=18,;
    Caption="Dati esterni:"  ;
  , bGlobalFont=.t.

  add object oStr_1_43 as StdString with uid="FHATKCNUYO",Visible=.t., Left=3, Top=203,;
    Alignment=1, Width=126, Height=18,;
    Caption="File output utente:"  ;
  , bGlobalFont=.t.

  add object oStr_1_44 as StdString with uid="LDCBIROCJZ",Visible=.t., Left=3, Top=149,;
    Alignment=1, Width=126, Height=18,;
    Caption="File CBI:"  ;
  , bGlobalFont=.t.

  func oStr_1_44.mHide()
    with this.Parent.oContained
      return (g_FAEL<>'S' or IsAlt())
    endwith
  endfunc

  add object oStr_1_45 as StdString with uid="WXCIJFFHEE",Visible=.t., Left=3, Top=258,;
    Alignment=1, Width=126, Height=18,;
    Caption="Prelevamento:"  ;
  , bGlobalFont=.t.

  add object oStr_1_46 as StdString with uid="DXCYSVQNOC",Visible=.t., Left=3, Top=230,;
    Alignment=1, Width=126, Height=18,;
    Caption="SOStitutiva:"  ;
  , bGlobalFont=.t.

  func oStr_1_46.mHide()
    with this.Parent.oContained
      return (g_DOCM#'S' or IsAlt())
    endwith
  endfunc

  add object oStr_1_47 as StdString with uid="OBVGULZASQ",Visible=.t., Left=3, Top=285,;
    Alignment=1, Width=126, Height=18,;
    Caption="File print system:"  ;
  , bGlobalFont=.t.

  add object oStr_1_49 as StdString with uid="HMUSWLIGYX",Visible=.t., Left=3, Top=312,;
    Alignment=1, Width=126, Height=18,;
    Caption="Xml descrittore:"  ;
  , bGlobalFont=.t.

  add object oBox_1_35 as StdBox with uid="VQYJXNYKDU",left=6, top=357, width=781,height=69
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_ao8','CONTROPA','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +" "+i_cAliasName2+".COCODAZI=CONTROPA.COCODAZI";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Master File"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
