* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bac                                                        *
*              Aggiornamento ultimo costo/prezzo                               *
*                                                                              *
*      Author: Nicola Gorlandi                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_7]                                              *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2004-10-27                                                      *
* Last revis.: 2007-03-09                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_SERIAL,w_ROWNUM,w_NUMRIF,w_IMPAGG,w_AGGDIFF
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bac",oParentObject,m.w_SERIAL,m.w_ROWNUM,m.w_NUMRIF,m.w_IMPAGG,m.w_AGGDIFF)
return(i_retval)

define class tgsar_bac as StdBatch
  * --- Local variables
  w_SERIAL = space(10)
  w_ROWNUM = 0
  w_NUMRIF = 0
  w_IMPAGG = 0
  w_AGGDIFF = .f.
  w_FLCASC = space(1)
  w_FLOMAG = space(1)
  w_DATDOCU = ctod("  /  /  ")
  w_CAUMAG = space(5)
  w_FLAVA1 = space(1)
  w_KEYSAL = space(20)
  w_CODMAG = space(5)
  w_ULTCAR = ctod("  /  /  ")
  w_VALULT = 0
  w_QTAUM1 = 0
  w_IMPNAZ = 0
  w_DECTOU = 0
  w_FLULCA = space(1)
  w_ULTSCA = ctod("  /  /  ")
  w_CLADOC = space(2)
  w_FLULPV = space(1)
  w_PREZZO = 0
  w_VALNAZ = space(3)
  * --- WorkFile variables
  DOC_MAST_idx=0
  DOC_DETT_idx=0
  CAM_AGAZ_idx=0
  SALDIART_idx=0
  VALUTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Aggiorna ultimo costo / ultimo prezzo sui saldi e sulla riga documento
    *     ========================================================
    *     Riceve come parametro la chiave della riga da aggiornare pi� l'importo da aggiungere
    *     w_SERIAL , w_ROWNUM , w_NUMRIF  = Chiave riga documento da valutare
    *     w_IMPAGG =  importo da utilizzare in aggiunta o totale
    *     w_AGGDIFF se .T. l'importo passato va aggiunto al valore di riga a quanto presente, altrimenti l'importo � il nuovo valore di riga
    *     ========================================================
    *     Utilizzato per rivalorizzazioni
    * --- Read from DOC_MAST
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_MAST_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2],.t.,this.DOC_MAST_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVDATDOC,MVCLADOC,MVVALNAZ"+;
        " from "+i_cTable+" DOC_MAST where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVDATDOC,MVCLADOC,MVVALNAZ;
        from (i_cTable) where;
            MVSERIAL = this.w_SERIAL;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATDOCU = NVL(cp_ToDate(_read_.MVDATDOC),cp_NullValue(_read_.MVDATDOC))
      this.w_CLADOC = NVL(cp_ToDate(_read_.MVCLADOC),cp_NullValue(_read_.MVCLADOC))
      this.w_VALNAZ = NVL(cp_ToDate(_read_.MVVALNAZ),cp_NullValue(_read_.MVVALNAZ))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from DOC_DETT
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.DOC_DETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "MVFLCASC,MVFLOMAG,MVCAUMAG,MVKEYSAL,MVCODMAG,MVQTAUM1,MVIMPNAZ,MVPREZZO"+;
        " from "+i_cTable+" DOC_DETT where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
            +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        MVFLCASC,MVFLOMAG,MVCAUMAG,MVKEYSAL,MVCODMAG,MVQTAUM1,MVIMPNAZ,MVPREZZO;
        from (i_cTable) where;
            MVSERIAL = this.w_SERIAL;
            and CPROWNUM = this.w_ROWNUM;
            and MVNUMRIF = this.w_NUMRIF;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLCASC = NVL(cp_ToDate(_read_.MVFLCASC),cp_NullValue(_read_.MVFLCASC))
      this.w_FLOMAG = NVL(cp_ToDate(_read_.MVFLOMAG),cp_NullValue(_read_.MVFLOMAG))
      this.w_CAUMAG = NVL(cp_ToDate(_read_.MVCAUMAG),cp_NullValue(_read_.MVCAUMAG))
      this.w_KEYSAL = NVL(cp_ToDate(_read_.MVKEYSAL),cp_NullValue(_read_.MVKEYSAL))
      this.w_CODMAG = NVL(cp_ToDate(_read_.MVCODMAG),cp_NullValue(_read_.MVCODMAG))
      this.w_QTAUM1 = NVL(cp_ToDate(_read_.MVQTAUM1),cp_NullValue(_read_.MVQTAUM1))
      this.w_IMPNAZ = NVL(cp_ToDate(_read_.MVIMPNAZ),cp_NullValue(_read_.MVIMPNAZ))
      this.w_PREZZO = NVL(cp_ToDate(_read_.MVPREZZO),cp_NullValue(_read_.MVPREZZO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from CAM_AGAZ
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CAM_AGAZ_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CAM_AGAZ_idx,2],.t.,this.CAM_AGAZ_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CMFLAVAL"+;
        " from "+i_cTable+" CAM_AGAZ where ";
            +"CMCODICE = "+cp_ToStrODBC(this.w_CAUMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CMFLAVAL;
        from (i_cTable) where;
            CMCODICE = this.w_CAUMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_FLAVA1 = NVL(cp_ToDate(_read_.CMFLAVAL),cp_NullValue(_read_.CMFLAVAL))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from SALDIART
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.SALDIART_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "SLDATUCA,SLDATUPV"+;
        " from "+i_cTable+" SALDIART where ";
            +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
            +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        SLDATUCA,SLDATUPV;
        from (i_cTable) where;
            SLCODICE = this.w_KEYSAL;
            and SLCODMAG = this.w_CODMAG;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_ULTCAR = NVL(cp_ToDate(_read_.SLDATUCA),cp_NullValue(_read_.SLDATUCA))
      this.w_ULTSCA = NVL(cp_ToDate(_read_.SLDATUPV),cp_NullValue(_read_.SLDATUPV))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Read from VALUTE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.VALUTE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.VALUTE_idx,2],.t.,this.VALUTE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "VADECUNI"+;
        " from "+i_cTable+" VALUTE where ";
            +"VACODVAL = "+cp_ToStrODBC(this.w_VALNAZ);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        VADECUNI;
        from (i_cTable) where;
            VACODVAL = this.w_VALNAZ;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DECTOU = NVL(cp_ToDate(_read_.VADECUNI),cp_NullValue(_read_.VADECUNI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    this.w_VALULT = IIF( this.w_QTAUM1=0 , 0 , cp_ROUND((IIF( this.w_AGGDIFF , this.w_IMPNAZ + this.w_IMPAGG , this.w_IMPAGG ) )/this.w_QTAUM1, this.w_DECTOU) )
    do case
      case this.w_FLAVA1="A" AND this.w_FLCASC="+" AND this.w_DATDOCU>=this.w_ULTCAR AND this.w_FLOMAG<>"S" And this.w_PREZZO<>0
        * --- Aggiorno l'ultimo costo
        this.w_FLULCA = "="
        this.w_FLULPV = " "
      case this.w_FLAVA1="V" AND this.w_FLCASC="-" AND this.w_DATDOCU>=this.w_ULTSCA AND this.w_FLOMAG<>"S" AND (this.w_CLADOC<>"DT" OR this.w_PREZZO<>0)
        * --- Aggiorno l'ultimo prezzo
        this.w_FLULPV = "="
        this.w_FLULCA = " "
      otherwise
        this.w_FLULCA = " "
        this.w_FLULPV = " "
    endcase
    * --- Devo aggiornare i saldi articoli ?
    if Not Empty( this.w_FLULPV + this.w_FLULCA ) 
      * --- Non gestito il caso in cui il saldo non c'� in quanto il documento
      *     che stiamo modificando al momento della sua creazione lo deve
      *     aver creato.
      * --- Write into SALDIART
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2])
        i_cOp1=cp_SetTrsOp(this.w_FLULPV,'SLVALUPV','this.w_VALULT',this.w_VALULT,'update',i_nConn)
        i_cOp2=cp_SetTrsOp(this.w_FLULPV,'SLDATUPV','this.w_DATDOCU',this.w_DATDOCU,'update',i_nConn)
        i_cOp3=cp_SetTrsOp(this.w_FLULCA,'SLVALUCA','this.w_VALULT',this.w_VALULT,'update',i_nConn)
        i_cOp4=cp_SetTrsOp(this.w_FLULCA,'SLDATUCA','this.w_DATDOCU',this.w_DATDOCU,'update',i_nConn)
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.SALDIART_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"SLVALUPV ="+cp_NullLink(i_cOp1,'SALDIART','SLVALUPV');
        +",SLDATUPV ="+cp_NullLink(i_cOp2,'SALDIART','SLDATUPV');
        +",SLVALUCA ="+cp_NullLink(i_cOp3,'SALDIART','SLVALUCA');
        +",SLDATUCA ="+cp_NullLink(i_cOp4,'SALDIART','SLDATUCA');
            +i_ccchkf ;
        +" where ";
            +"SLCODICE = "+cp_ToStrODBC(this.w_KEYSAL);
            +" and SLCODMAG = "+cp_ToStrODBC(this.w_CODMAG);
               )
      else
        update (i_cTable) set;
            SLVALUPV = &i_cOp1.;
            ,SLDATUPV = &i_cOp2.;
            ,SLVALUCA = &i_cOp3.;
            ,SLDATUCA = &i_cOp4.;
            &i_ccchkf. ;
         where;
            SLCODICE = this.w_KEYSAL;
            and SLCODMAG = this.w_CODMAG;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      * --- Write into DOC_DETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_DETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"MVVALULT ="+cp_NullLink(cp_ToStrODBC(this.w_VALULT),'DOC_DETT','MVVALULT');
        +",MVFLULCA ="+cp_NullLink(cp_ToStrODBC(this.w_FLULCA),'DOC_DETT','MVFLULCA');
        +",MVFLULPV ="+cp_NullLink(cp_ToStrODBC(this.w_FLULPV),'DOC_DETT','MVFLULPV');
            +i_ccchkf ;
        +" where ";
            +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
            +" and CPROWNUM = "+cp_ToStrODBC(this.w_ROWNUM);
            +" and MVNUMRIF = "+cp_ToStrODBC(this.w_NUMRIF);
               )
      else
        update (i_cTable) set;
            MVVALULT = this.w_VALULT;
            ,MVFLULCA = this.w_FLULCA;
            ,MVFLULPV = this.w_FLULPV;
            &i_ccchkf. ;
         where;
            MVSERIAL = this.w_SERIAL;
            and CPROWNUM = this.w_ROWNUM;
            and MVNUMRIF = this.w_NUMRIF;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
  endproc


  proc Init(oParentObject,w_SERIAL,w_ROWNUM,w_NUMRIF,w_IMPAGG,w_AGGDIFF)
    this.w_SERIAL=w_SERIAL
    this.w_ROWNUM=w_ROWNUM
    this.w_NUMRIF=w_NUMRIF
    this.w_IMPAGG=w_IMPAGG
    this.w_AGGDIFF=w_AGGDIFF
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,5)]
    this.cWorkTables[1]='DOC_MAST'
    this.cWorkTables[2]='DOC_DETT'
    this.cWorkTables[3]='CAM_AGAZ'
    this.cWorkTables[4]='SALDIART'
    this.cWorkTables[5]='VALUTE'
    return(this.OpenAllTables(5))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_SERIAL,w_ROWNUM,w_NUMRIF,w_IMPAGG,w_AGGDIFF"
endproc
