* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg1kcf                                                        *
*              Contabilizzazione fittizia                                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_86]                                                  *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-03-23                                                      *
* Last revis.: 2013-06-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgscg1kcf",oParentObject))

* --- Class definition
define class tgscg1kcf as StdForm
  Top    = 7
  Left   = 7

  * --- Standard Properties
  Width  = 612
  Height = 193
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2013-06-26"
  HelpContextID=102245737
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=15

  * --- Constant Properties
  _IDX = 0
  CAN_TIER_IDX = 0
  DIPENDEN_IDX = 0
  cPrg = "gscg1kcf"
  cComment = "Contabilizzazione fittizia"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_NUMDOC = 0
  w_TIPOMSK = space(1)
  w_DATINI = ctod('  /  /  ')
  w_DATFIN = ctod('  /  /  ')
  w_CODPRA = space(15)
  w_ANTICIP = space(1)
  o_ANTICIP = space(1)
  w_ALFADOC = space(10)
  w_ACCONTI = space(1)
  o_ACCONTI = space(1)
  w_ESCACC = space(1)
  o_ESCACC = space(1)
  w_FATTURE = space(1)
  o_FATTURE = space(1)
  w_ESCFAT = space(1)
  o_ESCFAT = space(1)
  w_CODAZI = space(5)
  w_DESPRA = space(100)
  w_OBTEST = ctod('  /  /  ')
  w_CODRESP = space(5)
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgscg1kcfPag1","gscg1kcf",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oNUMDOC_1_1
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[2]
    this.cWorkTables[1]='CAN_TIER'
    this.cWorkTables[2]='DIPENDEN'
    return(this.OpenAllTables(2))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      with this
        do GSCG1BCF with this
      endwith
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_NUMDOC=0
      .w_TIPOMSK=space(1)
      .w_DATINI=ctod("  /  /  ")
      .w_DATFIN=ctod("  /  /  ")
      .w_CODPRA=space(15)
      .w_ANTICIP=space(1)
      .w_ALFADOC=space(10)
      .w_ACCONTI=space(1)
      .w_ESCACC=space(1)
      .w_FATTURE=space(1)
      .w_ESCFAT=space(1)
      .w_CODAZI=space(5)
      .w_DESPRA=space(100)
      .w_OBTEST=ctod("  /  /  ")
      .w_CODRESP=space(5)
        .w_NUMDOC = IIF(.w_FATTURE<>'S',0,.w_NUMDOC)
        .w_TIPOMSK = LEFT(this.oParentObject,1)
        .w_DATINI = IIF(.w_TIPOMSK='C',i_INIDAT,g_iniese)
          .DoRTCalc(4,4,.f.)
        .w_CODPRA = iif(.w_ANTICIP<>'S',Space(15),.w_CODPRA)
        .DoRTCalc(5,5,.f.)
        if not(empty(.w_CODPRA))
          .link_1_6('Full')
        endif
          .DoRTCalc(6,6,.f.)
        .w_ALFADOC = iif(.w_FATTURE<>'S',Space(10),.w_ALFADOC)
        .w_ACCONTI = iif(.w_FATTURE='S' and .w_TIPOMSK<>'C','S',.w_ACCONTI)
        .w_ESCACC = IIF(.w_ACCONTI='S','S',' ')
          .DoRTCalc(10,10,.f.)
        .w_ESCFAT = IIF(.w_FATTURE='S','S',' ')
        .w_CODAZI = i_CODAZI
          .DoRTCalc(13,13,.f.)
        .w_OBTEST = i_DATSYS
    endwith
    this.DoRTCalc(15,15,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_14.enabled = this.oPgFrm.Page1.oPag.oBtn_1_14.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_15.enabled = this.oPgFrm.Page1.oPag.oBtn_1_15.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- gscg1kcf
    If Type('This.oparentobject')='O'
      This.oparentobject.w_OBJ_LOG=this.w_OBJ_LOG
    endif
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        if .o_FATTURE<>.w_FATTURE
            .w_NUMDOC = IIF(.w_FATTURE<>'S',0,.w_NUMDOC)
        endif
        .DoRTCalc(2,4,.t.)
        if .o_ANTICIP<>.w_ANTICIP
            .w_CODPRA = iif(.w_ANTICIP<>'S',Space(15),.w_CODPRA)
          .link_1_6('Full')
        endif
        .DoRTCalc(6,6,.t.)
        if .o_FATTURE<>.w_FATTURE
            .w_ALFADOC = iif(.w_FATTURE<>'S',Space(10),.w_ALFADOC)
        endif
        if .o_FATTURE<>.w_FATTURE
            .w_ACCONTI = iif(.w_FATTURE='S' and .w_TIPOMSK<>'C','S',.w_ACCONTI)
        endif
        if .o_ACCONTI<>.w_ACCONTI
            .w_ESCACC = IIF(.w_ACCONTI='S','S',' ')
        endif
        .DoRTCalc(10,10,.t.)
        if .o_FATTURE<>.w_FATTURE
            .w_ESCFAT = IIF(.w_FATTURE='S','S',' ')
        endif
        if .o_ESCACC<>.w_ESCACC
          .Calculate_VEWNACVBKT()
        endif
        if .o_ESCFAT<>.w_ESCFAT
          .Calculate_RGUZUVXIAS()
        endif
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(12,15,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
    endwith
  return

  proc Calculate_VEWNACVBKT()
    with this
          * --- gscg2bcf - Messaggi
          gscg2bcf(this;
              ,'A';
             )
    endwith
  endproc
  proc Calculate_RGUZUVXIAS()
    with this
          * --- gscg2bcf - Messaggi
          gscg2bcf(this;
              ,'F';
             )
    endwith
  endproc
  proc Calculate_NHGZPPOKMU()
    with this
          * --- Cambia caption form
          .Caption = IIF(.w_TIPOMSK='C', Ah_MsgFormat('Contabilizzazione fittizia'), Ah_MsgFormat('Rimozione contabilizzazione fittizia'))
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oESCACC_1_11.enabled = this.oPgFrm.Page1.oPag.oESCACC_1_11.mCond()
    this.oPgFrm.Page1.oPag.oESCFAT_1_13.enabled = this.oPgFrm.Page1.oPag.oESCFAT_1_13.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oNUMDOC_1_1.visible=!this.oPgFrm.Page1.oPag.oNUMDOC_1_1.mHide()
    this.oPgFrm.Page1.oPag.oCODPRA_1_6.visible=!this.oPgFrm.Page1.oPag.oCODPRA_1_6.mHide()
    this.oPgFrm.Page1.oPag.oALFADOC_1_9.visible=!this.oPgFrm.Page1.oPag.oALFADOC_1_9.mHide()
    this.oPgFrm.Page1.oPag.oESCACC_1_11.visible=!this.oPgFrm.Page1.oPag.oESCACC_1_11.mHide()
    this.oPgFrm.Page1.oPag.oESCFAT_1_13.visible=!this.oPgFrm.Page1.oPag.oESCFAT_1_13.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_20.visible=!this.oPgFrm.Page1.oPag.oStr_1_20.mHide()
    this.oPgFrm.Page1.oPag.oDESPRA_1_21.visible=!this.oPgFrm.Page1.oPag.oDESPRA_1_21.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_23.visible=!this.oPgFrm.Page1.oPag.oStr_1_23.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_24.visible=!this.oPgFrm.Page1.oPag.oStr_1_24.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
        if lower(cEvent)==lower("Init")
          .Calculate_NHGZPPOKMU()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODPRA
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CAN_TIER_IDX,3]
    i_lTable = "CAN_TIER"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2], .t., this.CAN_TIER_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODPRA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('gspr_acn',True,'CAN_TIER')
        if i_nConn<>0
          i_cWhere = i_cFlt+" CNCODCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

          i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by CNCODCAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'CNCODCAN',trim(this.w_CODPRA))
          select CNCODCAN,CNDESCAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by CNCODCAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODPRA)==trim(_Link_.CNCODCAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStrODBC(trim(this.w_CODPRA)+"%");

            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" CNDESCAN like "+cp_ToStr(trim(this.w_CODPRA)+"%");

            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODPRA) and !this.bDontReportError
            deferred_cp_zoom('CAN_TIER','*','CNCODCAN',cp_AbsName(oSource.parent,'oCODPRA_1_6'),i_cWhere,'gspr_acn',"Pratiche",'GSPR_ACN.CAN_TIER_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                     +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',oSource.xKey(1))
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODPRA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select CNCODCAN,CNDESCAN";
                   +" from "+i_cTable+" "+i_lTable+" where CNCODCAN="+cp_ToStrODBC(this.w_CODPRA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'CNCODCAN',this.w_CODPRA)
            select CNCODCAN,CNDESCAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODPRA = NVL(_Link_.CNCODCAN,space(15))
      this.w_DESPRA = NVL(_Link_.CNDESCAN,space(100))
    else
      if i_cCtrl<>'Load'
        this.w_CODPRA = space(15)
      endif
      this.w_DESPRA = space(100)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CAN_TIER_IDX,2])+'\'+cp_ToStr(_Link_.CNCODCAN,1)
      cp_ShowWarn(i_cKey,this.CAN_TIER_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODPRA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oNUMDOC_1_1.value==this.w_NUMDOC)
      this.oPgFrm.Page1.oPag.oNUMDOC_1_1.value=this.w_NUMDOC
    endif
    if not(this.oPgFrm.Page1.oPag.oDATINI_1_3.value==this.w_DATINI)
      this.oPgFrm.Page1.oPag.oDATINI_1_3.value=this.w_DATINI
    endif
    if not(this.oPgFrm.Page1.oPag.oDATFIN_1_5.value==this.w_DATFIN)
      this.oPgFrm.Page1.oPag.oDATFIN_1_5.value=this.w_DATFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oCODPRA_1_6.value==this.w_CODPRA)
      this.oPgFrm.Page1.oPag.oCODPRA_1_6.value=this.w_CODPRA
    endif
    if not(this.oPgFrm.Page1.oPag.oANTICIP_1_8.RadioValue()==this.w_ANTICIP)
      this.oPgFrm.Page1.oPag.oANTICIP_1_8.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oALFADOC_1_9.value==this.w_ALFADOC)
      this.oPgFrm.Page1.oPag.oALFADOC_1_9.value=this.w_ALFADOC
    endif
    if not(this.oPgFrm.Page1.oPag.oACCONTI_1_10.RadioValue()==this.w_ACCONTI)
      this.oPgFrm.Page1.oPag.oACCONTI_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESCACC_1_11.RadioValue()==this.w_ESCACC)
      this.oPgFrm.Page1.oPag.oESCACC_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFATTURE_1_12.RadioValue()==this.w_FATTURE)
      this.oPgFrm.Page1.oPag.oFATTURE_1_12.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oESCFAT_1_13.RadioValue()==this.w_ESCFAT)
      this.oPgFrm.Page1.oPag.oESCFAT_1_13.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDESPRA_1_21.value==this.w_DESPRA)
      this.oPgFrm.Page1.oPag.oDESPRA_1_21.value=this.w_DESPRA
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(.w_DATINI<=.w_DATFIN)
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATINI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di inizio selezione maggiore della data di fine selezione")
          case   ((empty(.w_DATFIN)) or not(.w_DATFIN>=.w_DATINI))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oDATFIN_1_5.SetFocus()
            i_bnoObbl = !empty(.w_DATFIN)
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Data di fine selezione minore della data di inizio selezione")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ANTICIP = this.w_ANTICIP
    this.o_ACCONTI = this.w_ACCONTI
    this.o_ESCACC = this.w_ESCACC
    this.o_FATTURE = this.w_FATTURE
    this.o_ESCFAT = this.w_ESCFAT
    return

enddefine

* --- Define pages as container
define class tgscg1kcfPag1 as StdContainer
  Width  = 608
  height = 193
  stdWidth  = 608
  stdheight = 193
  resizeXpos=347
  resizeYpos=188
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oNUMDOC_1_1 as StdField with uid="LUOYQBPYZN",rtseq=1,rtrep=.f.,;
    cFormVar = "w_NUMDOC", cQueryName = "NUMDOC",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di selezione",;
    HelpContextID = 35718358,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=211, Top=99, cSayPict='"@Z"+"999999999999999"', cGetPict='"@Z"+"999999999999999"', nMaxValue = 999999999999999

  func oNUMDOC_1_1.mHide()
    with this.Parent.oContained
      return (.w_TIPOMSK='C' or .w_FATTURE<>'S')
    endwith
  endfunc

  add object oDATINI_1_3 as StdField with uid="OZSRJXYNQE",rtseq=3,rtrep=.f.,;
    cFormVar = "w_DATINI", cQueryName = "DATINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data di inizio selezione maggiore della data di fine selezione",;
    ToolTipText = "Data di inizio selezione dei documenti da marcare come contabilizzati",;
    HelpContextID = 135684150,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=395, Top=14

  func oDATINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATINI<=.w_DATFIN)
    endwith
    return bRes
  endfunc

  add object oDATFIN_1_5 as StdField with uid="MAKHUBCZMS",rtseq=4,rtrep=.f.,;
    cFormVar = "w_DATFIN", cQueryName = "DATFIN",;
    bObbl = .t. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "Data di fine selezione minore della data di inizio selezione",;
    ToolTipText = "Data di fine selezione dei documenti da marcare come contabilizzati",;
    HelpContextID = 214130742,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=520, Top=14

  func oDATFIN_1_5.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (.w_DATFIN>=.w_DATINI)
    endwith
    return bRes
  endfunc

  add object oCODPRA_1_6 as StdField with uid="LNZCMMZIKS",rtseq=5,rtrep=.f.,;
    cFormVar = "w_CODPRA", cQueryName = "CODPRA",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    ToolTipText = "Codice pratica",;
    HelpContextID = 6057510,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=212, Top=50, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CAN_TIER", cZoomOnZoom="gspr_acn", oKey_1_1="CNCODCAN", oKey_1_2="this.w_CODPRA"

  func oCODPRA_1_6.mHide()
    with this.Parent.oContained
      return (.w_TIPOMSK='C'  or .w_ANTICIP<>'S')
    endwith
  endfunc

  func oCODPRA_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODPRA_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODPRA_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'CAN_TIER','*','CNCODCAN',cp_AbsName(this.parent,'oCODPRA_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'gspr_acn',"Pratiche",'GSPR_ACN.CAN_TIER_VZM',this.parent.oContained
  endproc
  proc oCODPRA_1_6.mZoomOnZoom
    local i_obj
    i_obj=gspr_acn()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_CNCODCAN=this.parent.oContained.w_CODPRA
     i_obj.ecpSave()
  endproc

  add object oANTICIP_1_8 as StdCheck with uid="TFCNBIFRAL",rtseq=6,rtrep=.f.,left=15, top=49, caption="Anticipazioni",;
    ToolTipText = "Se attivo, le anticipazioni del periodo selezionato, verranno marcate come contabilizzate",;
    HelpContextID = 124153094,;
    cFormVar="w_ANTICIP", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oANTICIP_1_8.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oANTICIP_1_8.GetRadio()
    this.Parent.oContained.w_ANTICIP = this.RadioValue()
    return .t.
  endfunc

  func oANTICIP_1_8.SetRadio()
    this.Parent.oContained.w_ANTICIP=trim(this.Parent.oContained.w_ANTICIP)
    this.value = ;
      iif(this.Parent.oContained.w_ANTICIP=='S',1,;
      0)
  endfunc

  add object oALFADOC_1_9 as StdField with uid="XAIATIFDMP",rtseq=7,rtrep=.f.,;
    cFormVar = "w_ALFADOC", cQueryName = "ALFADOC",;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Alfa documento di selezione",;
    HelpContextID = 225282822,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=338, Top=99, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  func oALFADOC_1_9.mHide()
    with this.Parent.oContained
      return (.w_TIPOMSK='C' or .w_FATTURE<>'S')
    endwith
  endfunc

  add object oACCONTI_1_10 as StdCheck with uid="XTBLIVWQCN",rtseq=8,rtrep=.f.,left=15, top=75, caption="Acconti contestuali",;
    ToolTipText = "Se attivo, gli acconti contestuali del periodo selezionato, verranno marcati come contabilizzati",;
    HelpContextID = 216313338,;
    cFormVar="w_ACCONTI", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oACCONTI_1_10.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oACCONTI_1_10.GetRadio()
    this.Parent.oContained.w_ACCONTI = this.RadioValue()
    return .t.
  endfunc

  func oACCONTI_1_10.SetRadio()
    this.Parent.oContained.w_ACCONTI=trim(this.Parent.oContained.w_ACCONTI)
    this.value = ;
      iif(this.Parent.oContained.w_ACCONTI=='S',1,;
      0)
  endfunc

  add object oESCACC_1_11 as StdCheck with uid="NYRUHXXPTY",rtseq=9,rtrep=.f.,left=158, top=75, caption="Esclusione acconti contestuali riferiti a fatture incassate parz.",;
    ToolTipText = "Se attivo, non verranno marcati come contabilizzati gli acconti contestuali riferiti a fatture incassate parzialmente",;
    HelpContextID = 22897222,;
    cFormVar="w_ESCACC", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESCACC_1_11.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oESCACC_1_11.GetRadio()
    this.Parent.oContained.w_ESCACC = this.RadioValue()
    return .t.
  endfunc

  func oESCACC_1_11.SetRadio()
    this.Parent.oContained.w_ESCACC=trim(this.Parent.oContained.w_ESCACC)
    this.value = ;
      iif(this.Parent.oContained.w_ESCACC=='S',1,;
      0)
  endfunc

  func oESCACC_1_11.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_ACCONTI='S')
    endwith
   endif
  endfunc

  func oESCACC_1_11.mHide()
    with this.Parent.oContained
      return (.w_ACCONTI<>'S' or .w_TIPOMSK<>'C')
    endwith
  endfunc

  add object oFATTURE_1_12 as StdCheck with uid="RRAIHVUZZX",rtseq=10,rtrep=.f.,left=15, top=101, caption="Fatture",;
    ToolTipText = "Se attivo, le fatture del periodo selezionato, verranno marcate come contabilizzate",;
    HelpContextID = 26304598,;
    cFormVar="w_FATTURE", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFATTURE_1_12.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oFATTURE_1_12.GetRadio()
    this.Parent.oContained.w_FATTURE = this.RadioValue()
    return .t.
  endfunc

  func oFATTURE_1_12.SetRadio()
    this.Parent.oContained.w_FATTURE=trim(this.Parent.oContained.w_FATTURE)
    this.value = ;
      iif(this.Parent.oContained.w_FATTURE=='S',1,;
      0)
  endfunc

  add object oESCFAT_1_13 as StdCheck with uid="IDLYMGASQA",rtseq=11,rtrep=.f.,left=158, top=101, caption="Esclusione fatture non incassate",;
    ToolTipText = "Se attivo, non verranno marcate come contabilizzate le fatture non incassate",;
    HelpContextID = 37904966,;
    cFormVar="w_ESCFAT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oESCFAT_1_13.RadioValue()
    return(iif(this.value =1,'S',;
    ' '))
  endfunc
  func oESCFAT_1_13.GetRadio()
    this.Parent.oContained.w_ESCFAT = this.RadioValue()
    return .t.
  endfunc

  func oESCFAT_1_13.SetRadio()
    this.Parent.oContained.w_ESCFAT=trim(this.Parent.oContained.w_ESCFAT)
    this.value = ;
      iif(this.Parent.oContained.w_ESCFAT=='S',1,;
      0)
  endfunc

  func oESCFAT_1_13.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_FATTURE='S')
    endwith
   endif
  endfunc

  func oESCFAT_1_13.mHide()
    with this.Parent.oContained
      return (.w_FATTURE<>'S' or .w_TIPOMSK<>'C')
    endwith
  endfunc


  add object oBtn_1_14 as StdButton with uid="KJMPMBPFXU",left=498, top=142, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Inizio marcatura documenti come contabilizzati";
    , HelpContextID = 102216986;
    , Caption='\<Ok';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_14.Click()
      =cp_StandardFunction(this,"Save")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_15 as StdButton with uid="BUEPILAUOQ",left=549, top=142, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 94928314;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_15.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oDESPRA_1_21 as StdField with uid="USQXBJRXFB",rtseq=13,rtrep=.f.,;
    cFormVar = "w_DESPRA", cQueryName = "DESPRA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(100), bMultilanguage =  .f.,;
    HelpContextID = 6116406,;
   bGlobalFont=.t.,;
    Height=21, Width=252, Left=348, Top=50, InputMask=replicate('X',100)

  func oDESPRA_1_21.mHide()
    with this.Parent.oContained
      return (.w_TIPOMSK='C' or .w_ANTICIP<>'S')
    endwith
  endfunc

  add object oStr_1_4 as StdString with uid="SLQFLACGJL",Visible=.t., Left=258, Top=16,;
    Alignment=1, Width=133, Height=18,;
    Caption="Doc. emessi dal:"  ;
  , bGlobalFont=.t.

  add object oStr_1_7 as StdString with uid="OPPGPTJZNI",Visible=.t., Left=488, Top=16,;
    Alignment=1, Width=28, Height=18,;
    Caption="al:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="HZALIZNENK",Visible=.t., Left=151, Top=52,;
    Alignment=1, Width=55, Height=18,;
    Caption="Pratica:"  ;
  , bGlobalFont=.t.

  func oStr_1_20.mHide()
    with this.Parent.oContained
      return (.w_TIPOMSK='C' or .w_ANTICIP<>'S')
    endwith
  endfunc

  add object oStr_1_23 as StdString with uid="RJEHNTLHYE",Visible=.t., Left=151, Top=101,;
    Alignment=1, Width=55, Height=18,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  func oStr_1_23.mHide()
    with this.Parent.oContained
      return (.w_TIPOMSK='C' or .w_FATTURE<>'S')
    endwith
  endfunc

  add object oStr_1_24 as StdString with uid="ZWQSDVCNVM",Visible=.t., Left=328, Top=102,;
    Alignment=1, Width=8, Height=18,;
    Caption="/"  ;
  , bGlobalFont=.t.

  func oStr_1_24.mHide()
    with this.Parent.oContained
      return (.w_TIPOMSK='C' or .w_FATTURE<>'S')
    endwith
  endfunc
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gscg1kcf','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
