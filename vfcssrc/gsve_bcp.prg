* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsve_bcp                                                        *
*              Calcola percentuale provvigione da tabelle provv.               *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_269]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-14                                                      *
* Last revis.: 2016-09-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsve_bcp",oParentObject,m.pOPER)
return(i_retval)

define class tgsve_bcp as StdBatch
  * --- Local variables
  w_PARAM = space(3)
  pOPER = space(1)
  w_PADRE = .NULL.
  w_ARRPROV = 0
  w_TIPPRO = space(2)
  w_CODAGE = space(5)
  w_LAGEPRO = space(5)
  w_LAGSCOPAG = space(1)
  w_AGOCAP = 0
  w_LISRIFSCO = space(5)
  w_SPERIP = .f.
  w_ARRIMPPRO = 0
  * --- WorkFile variables
  runtime_filters = 2

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Da GSVE_MDV calcola in automatico la percentuale provvigione dalla tabella provvigioni
    * ---   pOper='T' = calcola provvigioni solo sulla riga attuale
    *       pOper='D' = ricalcola o se il caso azzerra valori provvigioni su tutto il detttaglio
    *       pOper='C'  = ricalcola le provvigioni su tutte le righe (solo se lanciato da GSAR_BFA non ricalcola i prezzi)
    * --- Spese
    * --- Verifico se ci sono spese ripartite
    this.w_SPERIP = IIF((this.oParentObject.w_MVSPEINC<>0 AND this.oParentObject.w_MVFLRINC="S") OR (this.oParentObject.w_MVSPEIMB<>0 AND this.oParentObject.w_MVFLRIMB="S") OR (this.oParentObject.w_MVSPETRA<>0 AND this.oParentObject.w_MVFLRTRA="S"), .T.,.F.)
    * --- Se non � presente il listino di riferimento nei parametri provvigioni prende quello della testata documenti
    this.w_LISRIFSCO = IIF(NOT EMPTY(NVL(g_LISRIF,SPACE(5))), g_LISRIF, this.oParentObject.w_MVTCOLIS)
    * --- Setta per non eseguire ricalcoli
    this.bUpdateParentObject=.f.
    this.w_PADRE = this.oParentObject
    if NOT EMPTY(this.oParentObject.w_MVCODAGE) 
      DECLARE pARRPROV (21)
      * --- Non avviso se sto svolgendo un caricamento rapido...
      * --- In ogni caso ricalcolo w_MVSCOCL2 poich� la SaveDependsOn a fondo pagina 
      *     non farebbe scattare la mCalc sul campo stesso
      if this.oParentObject.w_MVSCOCL1=0
        this.oParentObject.w_MVSCOCL2 = 0
      endif
      if this.pOPER$"TX" 
        * --- effettuo il calcolo solo se � gi� stata inserita anche la quantit�
        if this.oParentObject.w_MVTIPRIG<>"D" AND this.oParentObject.w_MVQTAMOV<>0
          this.Page_2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        endif
      else
        * --- Effettuo il calcolo percentuale  su tutto il transitorio (al cambio di un campo di testata) 
        if EMPTY(NVL(this.w_LISRIFSCO,SPACE(5))) And this.oParentObject.w_NUMGES<>1 And this.pOper="C" AND g_CALSCO="S" and g_CALPRO<>"DI"
          ah_ErrorMsg("Attenzione: mancano il codice listino di riferimento (parametri provvigione) e/o quello di testata del documento:%0Impossibile calcolare lo sconto applicato sulla riga","i")
          this.w_PADRE.w_RESCHK = -1
          i_retcode = 'stop'
          return
        endif
        this.w_PADRE.MarkPos()     
        this.w_PADRE.FirstRow()     
        * --- Primo giro sulle righe non cancellate...
        do while Not this.w_PADRE.Eof_Trs()
          this.w_PADRE.SetRow()     
          if this.w_PADRE.FullRow() AND this.oParentObject.w_MVTIPRIG<>"D"
            if this.POPER<>"C" or (this.pOPER="C" AND g_CALSCO="S" AND NOT EMPTY(NVL(this.w_LISRIFSCO,SPACE(5))) )
              this.Page_2()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            if this.POPER="C" AND !(g_CALSCO="S" AND NOT EMPTY(NVL(this.w_LISRIFSCO,SPACE(5))) )
              this.Page_3()
              if i_retcode='stop' or !empty(i_Error)
                return
              endif
            endif
            * --- Scrivo sul transitorio...
            this.w_PADRE.Set("w_MVPERPRO", this.oParentObject.w_MVPERPRO)     
            this.w_PADRE.Set("w_MVIMPPRO", this.oParentObject.w_MVIMPPRO)     
            this.w_PADRE.Set("w_MVTIPPRO", this.oParentObject.w_MVTIPPRO)     
            this.w_PADRE.Set("w_MVPROCAP", this.oParentObject.w_MVPROCAP)     
            this.w_PADRE.Set("w_MVIMPCAP", this.oParentObject.w_MVIMPCAP)     
            this.w_PADRE.Set("w_MVTIPPR2", this.oParentObject.w_MVTIPPR2)     
          endif
          this.w_PADRE.NextRow()     
        enddo
        * --- Mi rimetto nella riga di partenza
        this.w_PADRE.RePos(.T.)     
      endif
    else
      if NOT EMPTY(this.oParentObject.o_MVCODAGE) And this.pOper="D"
        this.w_PADRE.MarkPos()     
        this.w_PADRE.FirstRow()     
        * --- Primo giro sulle righe non cancellate...
        do while Not this.w_PADRE.Eof_Trs() 
          this.w_PADRE.SetRow()     
          if this.w_PADRE.FullRow() AND this.oParentObject.w_MVTIPRIG<>"D"
            * --- Scrivo sul transitorio...
            this.w_PADRE.Set("w_MVPERPRO", 0)     
            this.w_PADRE.Set("w_MVIMPPRO", 0)     
            this.w_PADRE.Set("w_MVPROCAP", 0)     
            this.w_PADRE.Set("w_MVIMPCAP", 0)     
          endif
          this.w_PADRE.NextRow()     
        enddo
        * --- Mi rimetto nella riga di partenza
        this.w_PADRE.RePos(.T.)     
      endif
    endif
  endproc


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Nel caso in cui ci sono spese ripartite la procedura calcola comunque le provvigioni 
    *     come se nell''anagrafica dell'agente fosse attivo il flag sconto pagamento.
    pArrprov [1] = this.oParentObject.w_mvtippro 
 pArrprov [2] = this.oParentObject.w_mvperpro 
 pArrprov [3] = this.oParentObject.w_mvtippr2 
 pArrprov [4] = this.oParentObject.w_mvprocap 
 pArrprov [5] = this.oParentObject.w_grupro 
 pArrprov [6] = this.oParentObject.w_catcli 
 pArrprov [7] = this.oParentObject.w_agepro 
 pArrprov [8] = IIF(this.w_SPERIP, "S", this.oParentObject.w_agscopag ) 
 pArrprov [9] = this.oParentObject.w_artpro 
 pArrprov [10] = this.oParentObject.w_mvflomag 
 pArrprov [11] = this.oParentObject.w_dectot 
 pArrprov [12] = this.oParentObject.w_mvscont1 
 pArrprov [13] = this.oParentObject.w_mvscont2 
 pArrprov [14] = this.oParentObject.w_mvscont3 
 pArrprov [15] = this.oParentObject.w_mvscont4 
 pArrprov [16] = this.oParentObject.w_mvscocl1 
 pArrprov [17] = this.oParentObject.w_mvscocl2 
 pArrprov [18] = this.oParentObject.w_mvscopag 
 pArrprov [19] = this.oParentObject.w_MVVALMAG *IIF(this.oParentObject.w_MVCLADOC="NC",-1,1) 
 pArrprov [20] = 0 
 pArrprov [21] = 0 
 
    if this.pOPER="C" AND g_CALSCO="S" AND NOT EMPTY(NVL(this.w_LISRIFSCO,SPACE(5))) 
      this.w_PARAM = "CCD"
      DIMENSION ARRPROV2[11]
      Arrprov2[ 1 ] = this.oParentObject.w_MVCODVAL 
 Arrprov2[ 2 ] = this.oParentObject.w_MVCODART 
 Arrprov2[ 3 ] = this.w_LISRIFSCO 
 Arrprov2[ 4 ] = this.oParentObject.w_MVDATREG 
 Arrprov2[ 5 ] = this.oParentObject.w_MVQTAMOV 
 Arrprov2[ 6 ] = this.oParentObject.w_MVCODICE 
 Arrprov2[ 7 ] = this.oParentObject.w_MVUNIMIS 
 Arrprov2[ 8 ] = this.oParentObject.w_MVQTAUM1 
 Arrprov2[ 9 ] = this.oParentObject.w_MVFLSCOR 
 Arrprov2[ 10 ] = this.oParentObject.w_MVPREZZO 
 Arrprov2[ 11 ] = this.oParentObject.w_PERIVA
    else
      do case
        case this.pOPER="C"
          * --- Calcoli finali
          this.w_PARAM = "NCD"
        case this.pOPER="X"
          * --- modifica flag omaggio
          this.w_PARAM = "NXD"
        otherwise
          this.w_PARAM = "NND"
      endcase
      * --- Se non devo calcolare il prezzo di listino non valorizzo il secondo array
      Arrprov2=.F.
    endif
    this.w_ARRPROV = CAL_PROV(this.w_PARAM,@pArrprov,@Arrprov2)
    * --- Agente
    this.oParentObject.w_MVTIPPRO = pArrprov(1)
    this.oParentObject.w_MVPERPRO = pArrprov(2)
    this.oParentObject.w_MVIMPPRO = IIF(this.oParentObject.w_MVTIPPRO<>"FO", pArrprov(20),this.oParentObject.w_MVIMPPRO)
    * --- Capoarea
    this.oParentObject.w_MVTIPPR2 = pArrprov(3)
    if NOT EMPTY(this.oParentObject.w_MVCODAG2)
      this.oParentObject.w_MVPROCAP = pArrprov(4)
      this.oParentObject.w_MVIMPCAP = IIF(this.oParentObject.w_MVTIPPR2<>"FO",pArrprov(21),this.oParentObject.w_MVIMPCAP)
    else
      this.oParentObject.w_MVPROCAP = 0
      this.oParentObject.w_MVIMPCAP = 0
    endif
  endproc


  procedure Page_3
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    DIMENSION pARRIMPPRO[10]
    pARRIMPPRO [1] = this.oParentObject.w_mvtippro 
 pARRIMPPRO [2] = this.oParentObject.w_mvperpro 
 pARRIMPPRO [3] = this.oParentObject.w_mvtippr2 
 pARRIMPPRO [4] = this.oParentObject.w_mvprocap 
 pARRIMPPRO [5] = IIF(this.w_SPERIP, "S", this.oParentObject.w_agscopag ) 
 pARRIMPPRO [6] = this.oParentObject.w_mvscopag 
 pARRIMPPRO [7] = this.oParentObject.w_MVVALMAG *IIF(this.oParentObject.w_MVCLADOC="NC",-1,1) 
 pARRIMPPRO [8] = this.oParentObject.w_DECTOT 
 pARRIMPPRO [9] = 0 
 pARRIMPPRO[10] =0
    this.w_ARRIMPPRO = CAL_IMPPRO(@pArrimppro)
    this.oParentObject.w_MVIMPPRO = IIF(this.oParentObject.w_MVTIPPRO<>"FO", pARRIMPPRO(9),this.oParentObject.w_MVIMPPRO)
    if NOT EMPTY(this.oParentObject.w_MVCODAG2)
      this.oParentObject.w_MVIMPCAP = IIF(this.oParentObject.w_MVTIPPR2<>"FO",pARRIMPPRO(10),this.oParentObject.w_MVIMPCAP)
    else
      this.oParentObject.w_MVIMPCAP = 0
    endif
  endproc

  proc Pag1()
      this.Page_1()      
  endproc

  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
