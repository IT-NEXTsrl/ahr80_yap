* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsca1bcc                                                        *
*              Controllo modifica livello centro di costo                      *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2012-02-07                                                      *
* Last revis.: 2012-02-15                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsca1bcc",oParentObject)
return(i_retval)

define class tgsca1bcc as StdBatch
  * --- Local variables
  w_OK = .f.
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Contralla se viene modificato il livello di un centro di costo/ricavo appartenente ad una ripartizione.
    this.w_OK = .T.
    if this.oParentObject.w_OLDLIVE<>this.oParentObject.w_CCNUMLIV
      * --- Select from gsca_acc
      do vq_exec with 'gsca_acc',this,'_Curs_gsca_acc','',.f.,.t.
      if used('_Curs_gsca_acc')
        select _Curs_gsca_acc
        locate for 1=1
        do while not(eof())
        if Nvl( _Curs_gsca_acc.CONTA, 0 )>0
          if !Ah_Yesno("Attenzione, modificato il livello di un centro di costo/ricavo appartenente ad una ripartizione %0Si desidera proseguire?")
            this.w_OK = .F.
          endif
          exit
        endif
          select _Curs_gsca_acc
          continue
        enddo
        use
      endif
      if !this.w_OK
        this.oParentObject.w_CCNUMLIV = this.oParentObject.w_OLDLIVE
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=MSG_TRANSACTION_ERROR
      endif
      this.oParentObject.w_OLDLIVE = 0
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  proc CloseCursors()
    if used('_Curs_gsca_acc')
      use in _Curs_gsca_acc
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
