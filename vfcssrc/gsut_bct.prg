* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_bct                                                        *
*              Cambio tipologia allegato                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_13]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2005-05-26                                                      *
* Last revis.: 2010-10-01                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pTipAll,pPadre
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsut_bct",oParentObject,m.pTipAll,m.pPadre)
return(i_retval)

define class tgsut_bct as StdBatch
  * --- Local variables
  pTipAll = space(5)
  pPadre = space(1)
  w_CLAALL = space(5)
  w_LDESCLA = space(40)
  w_PUBWEB = space(1)
  w_CONTROL = .NULL.
  w_OBJ = .NULL.
  * --- WorkFile variables
  CLA_ALLE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Calcolo Classe di Default da GSUT_AID, GSUT_AES, GSUT_AGF
    *     Parametro : Tipologia Allegato
    * --- Cambio Tipologia Allegato: ricarico la Classe Di Default
    * --- Read from CLA_ALLE
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CLA_ALLE_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CLA_ALLE_idx,2],.t.,this.CLA_ALLE_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "TACODCLA,TACLADES,TDPUBWEB"+;
        " from "+i_cTable+" CLA_ALLE where ";
            +"TACODICE = "+cp_ToStrODBC(this.pTipAll);
            +" and TAFLDEFA = "+cp_ToStrODBC("S");
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        TACODCLA,TACLADES,TDPUBWEB;
        from (i_cTable) where;
            TACODICE = this.pTipAll;
            and TAFLDEFA = "S";
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_CLAALL = NVL(cp_ToDate(_read_.TACODCLA),cp_NullValue(_read_.TACODCLA))
      this.w_LDESCLA = NVL(cp_ToDate(_read_.TACLADES),cp_NullValue(_read_.TACLADES))
      this.w_PUBWEB = NVL(cp_ToDate(_read_.TDPUBWEB),cp_NullValue(_read_.TDPUBWEB))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    do case
      case this.pPadre="A"
        * --- Librerie Immagini
        this.oParentObject.w_LICLAALL = this.w_CLAALL
        this.oParentObject.w_DESCLA = this.w_LDESCLA
      case this.pPadre="B"
        * --- Estensioni
        this.oParentObject.w_EXCLAALL = this.w_CLAALL
        this.oParentObject.w_DESCLA = this.w_LDESCLA
      case this.pPadre="C"
        * --- Gestione File Allegati
        *     - Attribuzione impostazioni classe  predefinita se presente.
        this.oParentObject.w_IDCLAALL = this.w_CLAALL
        this.oParentObject.w_DESCLA = this.w_LDESCLA
        this.oParentObject.w_IDPUBWEB = iif(this.w_PUBWEB="S" and g_IZCP $ "SA","S","N")
      case this.pPadre="D"
        * --- Read from CLA_ALLE
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.CLA_ALLE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.CLA_ALLE_idx,2],.t.,this.CLA_ALLE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "TDPUBWEB"+;
            " from "+i_cTable+" CLA_ALLE where ";
                +"TACODICE = "+cp_ToStrODBC(this.pTipAll);
                +" and TACODCLA = "+cp_ToStrODBC(this.oParentObject.w_IDCLAALL);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            TDPUBWEB;
            from (i_cTable) where;
                TACODICE = this.pTipAll;
                and TACODCLA = this.oParentObject.w_IDCLAALL;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_PUBWEB = NVL(cp_ToDate(_read_.TDPUBWEB),cp_NullValue(_read_.TDPUBWEB))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        * --- Gestione pubblica su Web
        this.oParentObject.w_IDPUBWEB = iif(this.w_PUBWEB="S" and g_IZCP $ "SA","S","N")
    endcase
  endproc


  proc Init(oParentObject,pTipAll,pPadre)
    this.pTipAll=pTipAll
    this.pPadre=pPadre
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CLA_ALLE'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pTipAll,pPadre"
endproc
