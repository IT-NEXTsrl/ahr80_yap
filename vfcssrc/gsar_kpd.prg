* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_kpd                                                        *
*              Progressivi numero                                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_217]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-10-02                                                      *
* Last revis.: 2014-11-25                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsar_kpd",oParentObject))

* --- Class definition
define class tgsar_kpd as StdForm
  Top    = 7
  Left   = 12

  * --- Standard Properties
  Width  = 530
  Height = 244
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2014-11-25"
  HelpContextID=149518185
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=18

  * --- Constant Properties
  _IDX = 0
  ESERCIZI_IDX = 0
  cPrg = "gsar_kpd"
  cComment = "Progressivi numero"
  oParentObject = .Null.
  icon = "mask.ico"
  windowtype = 1
  minbutton = .f.
  *closable = .f.

  * --- Local Variables
  w_CICLO = space(1)
  w_CODAZI = space(5)
  w_CLADOC = space(2)
  w_CLADOC = space(2)
  w_ANNO1 = space(4)
  o_ANNO1 = space(4)
  w_ESERC = space(4)
  w_CODANN1 = space(4)
  w_SERDOC1 = space(10)
  w_CODKEY2 = space(50)
  w_CODKEY1 = space(50)
  o_CODKEY1 = space(50)
  w_NUMDOC1 = 0
  w_NUMDOC2 = 0
  w_DATAINI = ctod('  /  /  ')
  w_DATAFIN = ctod('  /  /  ')
  w_ANNO = 0
  w_CLADOC1 = space(2)
  w_CLADOC1 = space(2)
  w_CLADOC = space(2)
  w_ZoomPDoc = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsar_kpd
  * --- Per gestione sicurezza procedure con parametri
  func getSecuritycode()
    local l_CICLO
    l_CICLO=this.oParentObject
    return(lower('GSAR_KPD'+IIF(Type('l_CICLO')='C',','+l_CICLO,'')))
  EndFunc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=1, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgsar_kpdPag1","gsar_kpd",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oCLADOC_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gsar_kpd
    * -- Modifico la caption della maschera
    If oParentObject= 'V'
       this.parent.cComment = Ah_MsgFormat('Progressivi numero documento')
    Else
       this.parent.cComment = Ah_MsgFormat('Progressivi numero protocollo')
    Endif
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomPDoc = this.oPgFrm.Pages(1).oPag.ZoomPDoc
    DoDefault()
    proc Destroy()
      this.w_ZoomPDoc = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[1]
    this.cWorkTables[1]='ESERCIZI'
    return(this.OpenAllTables(1))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.Hide()
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CICLO=space(1)
      .w_CODAZI=space(5)
      .w_CLADOC=space(2)
      .w_CLADOC=space(2)
      .w_ANNO1=space(4)
      .w_ESERC=space(4)
      .w_CODANN1=space(4)
      .w_SERDOC1=space(10)
      .w_CODKEY2=space(50)
      .w_CODKEY1=space(50)
      .w_NUMDOC1=0
      .w_NUMDOC2=0
      .w_DATAINI=ctod("  /  /  ")
      .w_DATAFIN=ctod("  /  /  ")
      .w_ANNO=0
      .w_CLADOC1=space(2)
      .w_CLADOC1=space(2)
      .w_CLADOC=space(2)
        .w_CICLO = this.oParentObject
        .w_CODAZI = i_CODAZI
        .w_CLADOC = 'FV'
        .w_CLADOC = 'FV'
        .w_ANNO1 = ALLTRIM(STR(YEAR(i_DATSYS)))
        .DoRTCalc(6,6,.f.)
        if not(empty(.w_ESERC))
          .link_1_6('Full')
        endif
        .w_CODANN1 = .w_ZoomPDoc.getVar('CODANN')
        .w_SERDOC1 = .w_ZoomPDoc.getVar('SERDOC')
        .w_CODKEY2 = .w_CODKEY1
        .w_CODKEY1 = .w_ZoomPDoc.getVar('CODKEY')
        .w_NUMDOC1 = .w_ZoomPDoc.getVar("NUMDOC")
        .w_NUMDOC2 = .w_ZoomPDoc.getVar("NUMDOC")
          .DoRTCalc(13,14,.f.)
        .w_ANNO = INT(VAL(.w_ANNO1))
      .oPgFrm.Page1.oPag.ZoomPDoc.Calculate()
        .w_CLADOC1 = 'AC'
        .w_CLADOC1 = 'AC'
        .w_CLADOC = 'FV'
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_7.enabled = this.oPgFrm.Page1.oPag.oBtn_1_7.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .DoRTCalc(1,6,.t.)
            .w_CODANN1 = .w_ZoomPDoc.getVar('CODANN')
            .w_SERDOC1 = .w_ZoomPDoc.getVar('SERDOC')
            .w_CODKEY2 = .w_CODKEY1
            .w_CODKEY1 = .w_ZoomPDoc.getVar('CODKEY')
        if .o_CODKEY1<>.w_CODKEY1
            .w_NUMDOC1 = .w_ZoomPDoc.getVar("NUMDOC")
        endif
            .w_NUMDOC2 = .w_ZoomPDoc.getVar("NUMDOC")
        .DoRTCalc(13,14,.t.)
        if .o_ANNO1<>.w_ANNO1
            .w_ANNO = INT(VAL(.w_ANNO1))
        endif
        .oPgFrm.Page1.oPag.ZoomPDoc.Calculate()
        * --- Area Manuale = Calculate
        * --- gsar_kpd
        if  .w_CODKEY1<>.w_CODKEY2
                  .w_NUMDOC1 = .w_ZoomPDoc.getVar("NUMDOC")
                  .w_CODKEY2 = .w_CODKEY1
        endif
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(16,18,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomPDoc.Calculate()
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oANNO1_1_5.enabled = this.oPgFrm.Page1.oPag.oANNO1_1_5.mCond()
    this.oPgFrm.Page1.oPag.oESERC_1_6.enabled = this.oPgFrm.Page1.oPag.oESERC_1_6.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oCLADOC_1_3.visible=!this.oPgFrm.Page1.oPag.oCLADOC_1_3.mHide()
    this.oPgFrm.Page1.oPag.oCLADOC_1_4.visible=!this.oPgFrm.Page1.oPag.oCLADOC_1_4.mHide()
    this.oPgFrm.Page1.oPag.oCLADOC1_1_28.visible=!this.oPgFrm.Page1.oPag.oCLADOC1_1_28.mHide()
    this.oPgFrm.Page1.oPag.oCLADOC1_1_29.visible=!this.oPgFrm.Page1.oPag.oCLADOC1_1_29.mHide()
    this.oPgFrm.Page1.oPag.oCLADOC_1_30.visible=!this.oPgFrm.Page1.oPag.oCLADOC_1_30.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomPDoc.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=ESERC
  func Link_1_6(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.ESERCIZI_IDX,3]
    i_lTable = "ESERCIZI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2], .t., this.ESERCIZI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_ESERC) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('',True,'ESERCIZI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ESCODESE like "+cp_ToStrODBC(trim(this.w_ESERC)+"%");
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);

          i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ESCODAZI,ESCODESE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ESCODAZI',this.w_CODAZI;
                     ,'ESCODESE',trim(this.w_ESERC))
          select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ESCODAZI,ESCODESE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_ESERC)==trim(_Link_.ESCODESE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_ESERC) and !this.bDontReportError
            deferred_cp_zoom('ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(oSource.parent,'oESERC_1_6'),i_cWhere,'',"Esercizi",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_CODAZI<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with MSG_VALUE_NOT_CORRECT_QM
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                     +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',oSource.xKey(1);
                       ,'ESCODESE',oSource.xKey(2))
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_ESERC)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ESCODAZI,ESCODESE,ESINIESE,ESFINESE";
                   +" from "+i_cTable+" "+i_lTable+" where ESCODESE="+cp_ToStrODBC(this.w_ESERC);
                   +" and ESCODAZI="+cp_ToStrODBC(this.w_CODAZI);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ESCODAZI',this.w_CODAZI;
                       ,'ESCODESE',this.w_ESERC)
            select ESCODAZI,ESCODESE,ESINIESE,ESFINESE;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_ESERC = NVL(_Link_.ESCODESE,space(4))
      this.w_DATAINI = NVL(cp_ToDate(_Link_.ESINIESE),ctod("  /  /  "))
      this.w_DATAFIN = NVL(cp_ToDate(_Link_.ESFINESE),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_ESERC = space(4)
      endif
      this.w_DATAINI = ctod("  /  /  ")
      this.w_DATAFIN = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.ESERCIZI_IDX,2])+'\'+cp_ToStr(_Link_.ESCODAZI,1)+'\'+cp_ToStr(_Link_.ESCODESE,1)
      cp_ShowWarn(i_cKey,this.ESERCIZI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_ESERC Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oCLADOC_1_3.RadioValue()==this.w_CLADOC)
      this.oPgFrm.Page1.oPag.oCLADOC_1_3.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLADOC_1_4.RadioValue()==this.w_CLADOC)
      this.oPgFrm.Page1.oPag.oCLADOC_1_4.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oANNO1_1_5.value==this.w_ANNO1)
      this.oPgFrm.Page1.oPag.oANNO1_1_5.value=this.w_ANNO1
    endif
    if not(this.oPgFrm.Page1.oPag.oESERC_1_6.value==this.w_ESERC)
      this.oPgFrm.Page1.oPag.oESERC_1_6.value=this.w_ESERC
    endif
    if not(this.oPgFrm.Page1.oPag.oCODANN1_1_12.value==this.w_CODANN1)
      this.oPgFrm.Page1.oPag.oCODANN1_1_12.value=this.w_CODANN1
    endif
    if not(this.oPgFrm.Page1.oPag.oSERDOC1_1_13.value==this.w_SERDOC1)
      this.oPgFrm.Page1.oPag.oSERDOC1_1_13.value=this.w_SERDOC1
    endif
    if not(this.oPgFrm.Page1.oPag.oNUMDOC1_1_16.value==this.w_NUMDOC1)
      this.oPgFrm.Page1.oPag.oNUMDOC1_1_16.value=this.w_NUMDOC1
    endif
    if not(this.oPgFrm.Page1.oPag.oCLADOC1_1_28.RadioValue()==this.w_CLADOC1)
      this.oPgFrm.Page1.oPag.oCLADOC1_1_28.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLADOC1_1_29.RadioValue()==this.w_CLADOC1)
      this.oPgFrm.Page1.oPag.oCLADOC1_1_29.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCLADOC_1_30.RadioValue()==this.w_CLADOC)
      this.oPgFrm.Page1.oPag.oCLADOC_1_30.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_ANNO1 = this.w_ANNO1
    this.o_CODKEY1 = this.w_CODKEY1
    return

enddefine

* --- Define pages as container
define class tgsar_kpdPag1 as StdContainer
  Width  = 526
  height = 244
  stdWidth  = 526
  stdheight = 244
  resizeXpos=274
  resizeYpos=161
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object oCLADOC_1_3 as StdCombo with uid="CVQJLSJMVE",rtseq=3,rtrep=.f.,left=45,top=7,width=221,height=21;
    , ToolTipText = "Tipo di documento selezionato";
    , HelpContextID = 11605722;
    , cFormVar="w_CLADOC",RowSource=""+"Ven - fatture/NC,"+"Ven - doc.interni/ord.previsionali,"+"Ven - documenti di trasporto (DDT),"+"Ven - ordini,"+"Acq - fatture/NC,"+"Acq - doc.interni/ord.previsionali,"+"Acq - documenti di trasporto (DDT),"+"Acq - ordini", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLADOC_1_3.RadioValue()
    return(iif(this.value =1,'FV',;
    iif(this.value =2,'IV',;
    iif(this.value =3,'DV',;
    iif(this.value =4,'OV',;
    iif(this.value =5,'FA',;
    iif(this.value =6,'IA',;
    iif(this.value =7,'DA',;
    iif(this.value =8,'OA',;
    space(2))))))))))
  endfunc
  func oCLADOC_1_3.GetRadio()
    this.Parent.oContained.w_CLADOC = this.RadioValue()
    return .t.
  endfunc

  func oCLADOC_1_3.SetRadio()
    this.Parent.oContained.w_CLADOC=trim(this.Parent.oContained.w_CLADOC)
    this.value = ;
      iif(this.Parent.oContained.w_CLADOC=='FV',1,;
      iif(this.Parent.oContained.w_CLADOC=='IV',2,;
      iif(this.Parent.oContained.w_CLADOC=='DV',3,;
      iif(this.Parent.oContained.w_CLADOC=='OV',4,;
      iif(this.Parent.oContained.w_CLADOC=='FA',5,;
      iif(this.Parent.oContained.w_CLADOC=='IA',6,;
      iif(this.Parent.oContained.w_CLADOC=='DA',7,;
      iif(this.Parent.oContained.w_CLADOC=='OA',8,;
      0))))))))
  endfunc

  func oCLADOC_1_3.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE" Or .w_CICLO = 'A' Or IsAlt())
    endwith
  endfunc


  add object oCLADOC_1_4 as StdCombo with uid="BVONBRRYZR",rtseq=4,rtrep=.f.,left=45,top=7,width=126,height=21;
    , ToolTipText = "Tipo di documento selezionato";
    , HelpContextID = 11605722;
    , cFormVar="w_CLADOC",RowSource=""+"Ric.fiscali,"+"Doc.interni,"+"DDT,"+"Fatture/NC,"+"Ordini da cliente,"+"Ordini a fornitore", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLADOC_1_4.RadioValue()
    return(iif(this.value =1,'RV',;
    iif(this.value =2,'IV',;
    iif(this.value =3,'DV',;
    iif(this.value =4,'FV',;
    iif(this.value =5,'OV',;
    iif(this.value =6,'OA',;
    space(2))))))))
  endfunc
  func oCLADOC_1_4.GetRadio()
    this.Parent.oContained.w_CLADOC = this.RadioValue()
    return .t.
  endfunc

  func oCLADOC_1_4.SetRadio()
    this.Parent.oContained.w_CLADOC=trim(this.Parent.oContained.w_CLADOC)
    this.value = ;
      iif(this.Parent.oContained.w_CLADOC=='RV',1,;
      iif(this.Parent.oContained.w_CLADOC=='IV',2,;
      iif(this.Parent.oContained.w_CLADOC=='DV',3,;
      iif(this.Parent.oContained.w_CLADOC=='FV',4,;
      iif(this.Parent.oContained.w_CLADOC=='OV',5,;
      iif(this.Parent.oContained.w_CLADOC=='OA',6,;
      0))))))
  endfunc

  func oCLADOC_1_4.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ADHOC REVOLUTION" Or .w_CICLO = 'A' Or IsAlt())
    endwith
  endfunc

  add object oANNO1_1_5 as StdField with uid="KETFXUJBCQ",rtseq=5,rtrep=.f.,;
    cFormVar = "w_ANNO1", cQueryName = "ANNO1",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno selezionato",;
    HelpContextID = 92620026,;
   bGlobalFont=.t.,;
    Height=21, Width=47, Left=392, Top=8, InputMask=replicate('X',4)

  func oANNO1_1_5.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_ESERC))
    endwith
   endif
  endfunc

  add object oESERC_1_6 as StdField with uid="WJPSWBJOPR",rtseq=6,rtrep=.f.,;
    cFormVar = "w_ESERC", cQueryName = "ESERC",;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Esercizio selezionato",;
    HelpContextID = 73584570,;
   bGlobalFont=.t.,;
    Height=21, Width=47, Left=392, Top=41, InputMask=replicate('X',4), bHasZoom = .t. , cLinkFile="ESERCIZI", oKey_1_1="ESCODAZI", oKey_1_2="this.w_CODAZI", oKey_2_1="ESCODESE", oKey_2_2="this.w_ESERC"

  func oESERC_1_6.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (EMPTY(.w_ANNO1))
    endwith
   endif
  endfunc

  func oESERC_1_6.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_6('Part',this)
    endwith
    return bRes
  endfunc

  proc oESERC_1_6.ecpDrop(oSource)
    this.Parent.oContained.link_1_6('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oESERC_1_6.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.ESERCIZI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStrODBC(this.Parent.oContained.w_CODAZI)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ESCODAZI="+cp_ToStr(this.Parent.oContained.w_CODAZI)
    endif
    do cp_zoom with 'ESERCIZI','*','ESCODAZI,ESCODESE',cp_AbsName(this.parent,'oESERC_1_6'),iif(empty(i_cWhere),.f.,i_cWhere),'',"Esercizi",'',this.parent.oContained
  endproc


  add object oBtn_1_7 as StdButton with uid="FPLYYPSZRA",left=469, top=18, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per effettuare la ricerca";
    , HelpContextID = 27404054;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_7.Click()
      with this.Parent.oContained
        GSAR_BPD(this.Parent.oContained,"A", .w_CICLO)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oCODANN1_1_12 as StdField with uid="WIPQMWBTQO",rtseq=7,rtrep=.f.,;
    cFormVar = "w_CODANN1", cQueryName = "CODANN1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(4), bMultilanguage =  .f.,;
    ToolTipText = "Anno selezionato",;
    HelpContextID = 171711526,;
   bGlobalFont=.t.,;
    Height=21, Width=47, Left=392, Top=104, InputMask=replicate('X',4)

  add object oSERDOC1_1_13 as StdField with uid="WRUFSBHPWY",rtseq=8,rtrep=.f.,;
    cFormVar = "w_SERDOC1", cQueryName = "SERDOC1",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento selezionata",;
    HelpContextID = 256897830,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=392, Top=136, InputMask=replicate('X',10)

  add object oNUMDOC1_1_16 as StdField with uid="ORYXTLZYHF",rtseq=11,rtrep=.f.,;
    cFormVar = "w_NUMDOC1", cQueryName = "NUMDOC1",;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento selezionato",;
    HelpContextID = 256881366,;
   bGlobalFont=.t.,;
    Height=21, Width=118, Left=392, Top=166, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999


  add object oBtn_1_24 as StdButton with uid="TJYEXGSXMN",left=417, top=194, width=48,height=45,;
    CpPicture="BMP\OK.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per aggiornare la numerazione progressiva";
    , HelpContextID = 7758951;
    , Caption='\<Aggiorna';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        GSAR_BPD(this.Parent.oContained,"U", .w_CICLO)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(NVL(.w_CODKEY1,'')))
      endwith
    endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="LFPHMETYMR",left=469, top=194, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 142200762;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object ZoomPDoc as cp_zoombox with uid="FVHJTPGALZ",left=4, top=45, width=300,height=192,;
    caption='Object',;
   bGlobalFont=.t.,;
    bReadOnly=.f.,bQueryOnLoad=.f.,bAdvOptions=.f.,bQueryOnDblClick=.f.,cZoomFile="GSAR_KPD.AZIENDA_VZM",bOptions=.f.,cTable="AZIENDA",;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 28479462


  add object oCLADOC1_1_28 as StdCombo with uid="YVVYFFUURP",rtseq=16,rtrep=.f.,left=45,top=7,width=161,height=21;
    , ToolTipText = "Tipo di documento selezionato";
    , HelpContextID = 256829734;
    , cFormVar="w_CLADOC1",RowSource=""+"Fatture/NC,"+"Altro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLADOC1_1_28.RadioValue()
    return(iif(this.value =1,'AC',;
    iif(this.value =2,'NN',;
    space(2))))
  endfunc
  func oCLADOC1_1_28.GetRadio()
    this.Parent.oContained.w_CLADOC1 = this.RadioValue()
    return .t.
  endfunc

  func oCLADOC1_1_28.SetRadio()
    this.Parent.oContained.w_CLADOC1=trim(this.Parent.oContained.w_CLADOC1)
    this.value = ;
      iif(this.Parent.oContained.w_CLADOC1=='AC',1,;
      iif(this.Parent.oContained.w_CLADOC1=='NN',2,;
      0))
  endfunc

  func oCLADOC1_1_28.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ad hoc ENTERPRISE" Or .w_CICLO = 'V' Or IsAlt())
    endwith
  endfunc


  add object oCLADOC1_1_29 as StdCombo with uid="WSAVEORSWJ",rtseq=17,rtrep=.f.,left=45,top=7,width=105,height=21;
    , ToolTipText = "Tipo di documento selezionato";
    , HelpContextID = 256829734;
    , cFormVar="w_CLADOC1",RowSource=""+"Fatture/NC,"+"Altro", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLADOC1_1_29.RadioValue()
    return(iif(this.value =1,'AC',;
    iif(this.value =2,'NN',;
    space(2))))
  endfunc
  func oCLADOC1_1_29.GetRadio()
    this.Parent.oContained.w_CLADOC1 = this.RadioValue()
    return .t.
  endfunc

  func oCLADOC1_1_29.SetRadio()
    this.Parent.oContained.w_CLADOC1=trim(this.Parent.oContained.w_CLADOC1)
    this.value = ;
      iif(this.Parent.oContained.w_CLADOC1=='AC',1,;
      iif(this.Parent.oContained.w_CLADOC1=='NN',2,;
      0))
  endfunc

  func oCLADOC1_1_29.mHide()
    with this.Parent.oContained
      return (g_APPLICATION <> "ADHOC REVOLUTION" Or .w_CICLO = 'V' Or IsAlt())
    endwith
  endfunc


  add object oCLADOC_1_30 as StdCombo with uid="UGDWEBKRPK",rtseq=18,rtrep=.f.,left=45,top=7,width=126,height=21;
    , ToolTipText = "Tipo di documento selezionato";
    , HelpContextID = 11605722;
    , cFormVar="w_CLADOC",RowSource=""+"Altri documenti,"+"Fatture/NC", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oCLADOC_1_30.RadioValue()
    return(iif(this.value =1,'IV',;
    iif(this.value =2,'FV',;
    space(2))))
  endfunc
  func oCLADOC_1_30.GetRadio()
    this.Parent.oContained.w_CLADOC = this.RadioValue()
    return .t.
  endfunc

  func oCLADOC_1_30.SetRadio()
    this.Parent.oContained.w_CLADOC=trim(this.Parent.oContained.w_CLADOC)
    this.value = ;
      iif(this.Parent.oContained.w_CLADOC=='IV',1,;
      iif(this.Parent.oContained.w_CLADOC=='FV',2,;
      0))
  endfunc

  func oCLADOC_1_30.mHide()
    with this.Parent.oContained
      return (Not IsAlt())
    endwith
  endfunc

  add object oStr_1_8 as StdString with uid="MGJFGTFSSS",Visible=.t., Left=312, Top=77,;
    Alignment=0, Width=69, Height=18,;
    Caption="Progressivo"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .t., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  add object oStr_1_9 as StdString with uid="BARBNSUMQK",Visible=.t., Left=306, Top=104,;
    Alignment=1, Width=84, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_10 as StdString with uid="FZJBXOLPLF",Visible=.t., Left=306, Top=136,;
    Alignment=1, Width=84, Height=18,;
    Caption="Serie:"  ;
  , bGlobalFont=.t.

  add object oStr_1_11 as StdString with uid="IKYTWJUCOL",Visible=.t., Left=342, Top=168,;
    Alignment=1, Width=48, Height=18,;
    Caption="Numero:"  ;
  , bGlobalFont=.t.

  add object oStr_1_18 as StdString with uid="IYGKOXEUNW",Visible=.t., Left=306, Top=8,;
    Alignment=1, Width=84, Height=18,;
    Caption="Anno:"  ;
  , bGlobalFont=.t.

  add object oStr_1_19 as StdString with uid="KOVBKZXEQJ",Visible=.t., Left=7, Top=8,;
    Alignment=1, Width=31, Height=18,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  add object oStr_1_20 as StdString with uid="FJVXFCAENE",Visible=.t., Left=306, Top=41,;
    Alignment=1, Width=84, Height=18,;
    Caption="Esercizio:"  ;
  , bGlobalFont=.t.

  add object oBox_1_26 as StdBox with uid="NYBARCTBZZ",left=307, top=93, width=212,height=3
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsar_kpd','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
