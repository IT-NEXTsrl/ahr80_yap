* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bip                                                        *
*              Controlla inventari di riferimento                              *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_63]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2001-09-26                                                      *
* Last revis.: 2001-09-26                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pScelta
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bip",oParentObject,m.pScelta)
return(i_retval)

define class tgsma_bip as StdBatch
  * --- Local variables
  pScelta = space(10)
  w_APPO = space(6)
  w_POS1 = 0
  w_POS2 = 0
  * --- WorkFile variables
  INVENTAR_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Lanciato da GSMA_AIN
    do case
      case this.pScelta="A"
        * --- All'evento Load assegna la var. w_INSTAINV per il controllo successivo sugli stati
        this.oParentObject.w_OLDSTAINV = this.oParentObject.w_INSTAINV
        * --- Controlla se l'inventario � gi� usato come riferimento e in caso affermativo impedisce l'elaborazione
        * --- Read from INVENTAR
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.INVENTAR_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.INVENTAR_idx,2],.t.,this.INVENTAR_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "INNUMPRE"+;
            " from "+i_cTable+" INVENTAR where ";
                +"INNUMPRE = "+cp_ToStrODBC(this.oParentObject.w_INNUMINV);
                +" and INESEPRE = "+cp_ToStrODBC(this.oParentObject.w_INCODESE);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            INNUMPRE;
            from (i_cTable) where;
                INNUMPRE = this.oParentObject.w_INNUMINV;
                and INESEPRE = this.oParentObject.w_INCODESE;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_APPO = NVL(cp_ToDate(_read_.INNUMPRE),cp_NullValue(_read_.INNUMPRE))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if i_Rows<>0 
          this.oParentObject.w_RESCHK = -1
        else
          this.oParentObject.w_RESCHK = 0
        endif
      case this.pScelta="B"
        * --- Controlla i cambiamenti di stato
        this.w_POS1 = at(this.oParentObject.w_INSTAINV,"PDS")
        this.w_POS2 = at(this.oParentObject.w_OLDSTAINV,"PDS")
        if this.w_POS1<this.w_POS2
          ah_ErrorMsg("Errore: impossibile impostare questo stato","!")
          this.oParentObject.w_INSTAINV = this.oParentObject.w_OLDSTAINV
        endif
    endcase
  endproc


  proc Init(oParentObject,pScelta)
    this.pScelta=pScelta
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='INVENTAR'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pScelta"
endproc
