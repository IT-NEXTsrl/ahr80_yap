* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bel                                                        *
*              Messaggio di variaizone identificativo                          *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2007-07-23                                                      *
* Last revis.: 2007-09-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bel",oParentObject)
return(i_retval)

define class tgscg_bel as StdBatch
  * --- Local variables
  w_Recpos = 0
  w_nAbsRow = 0
  w_nRelRow = 0
  w_MESS = .f.
  * --- WorkFile variables
  CONTI_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    if upper(this.oParentObject.cPrg)="GSCG_AEX"
      if not empty(this.oParentObject.GSCG_AEY.w_DERIFPNT)
        if not ah_YesNo("Attenzione la variazione dei riferimenti al soggetto far� perdere il collegamento alla primanota estratta. Si vuole procedere con l'operazione?")
          this.oParentObject.w_DECODCON = this.oParentObject.o_DECODCON
          this.oParentObject.w_DETIPCON = this.oParentObject.o_DETIPCON
          this.oParentObject.w_DETIPSOG = this.oParentObject.o_DETIPSOG
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANDESCRI,ANDTOBSO"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_DETIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_DECODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANDESCRI,ANDTOBSO;
              from (i_cTable) where;
                  ANTIPCON = this.oParentObject.w_DETIPCON;
                  and ANCODICE = this.oParentObject.w_DECODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCLF = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
            this.oParentObject.w_DTOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.oParentObject.w_DEDESCON = this.oParentObject.w_DESCLF
          i_retcode = 'stop'
          return
        endif
        this.oParentObject.GSCG_AEY.w_DERIFPNT = space(10) 
 this.oParentObject.GSCG_AEY.bupdated = .t.
      endif
    else
      SELECT (this.oParentObject.cTrsName)
      this.w_Recpos = IIF( Eof () , Recno(this.oParentObject.cTrsName)-1, Recno (This.oParentObject.cTrsName) )
      this.w_nAbsRow = this.oParentobject.oPgFrm.Page1.oPag.oBody.nAbsRow
      this.w_nRelRow = this.oParentobject.oPgFrm.Page1.oPag.oBody.nRelRow
      GO TOP
      SCAN FOR NOT DELETED() and not empty(nvl(t_DERIFPNT,""))
      if NOT this.w_MESS
        this.w_MESS = .t.
        if not ah_YesNo("Attenzione la variazione dei riferimenti al soggetto far� perdere il collegamento alla primanota estratta. Si vuole procedere con l'operazione?")
          this.oParentObject.w_DECODCON = this.oParentObject.o_DECODCON
          this.oParentObject.w_DETIPCON = this.oParentObject.o_DETIPCON
          this.oParentObject.w_DETIPSOG = this.oParentObject.o_DETIPSOG
          * --- Read from CONTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CONTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CONTI_idx,2],.t.,this.CONTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "ANDESCRI,ANDTOBSO"+;
              " from "+i_cTable+" CONTI where ";
                  +"ANTIPCON = "+cp_ToStrODBC(this.oParentObject.w_DETIPCON);
                  +" and ANCODICE = "+cp_ToStrODBC(this.oParentObject.w_DECODCON);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              ANDESCRI,ANDTOBSO;
              from (i_cTable) where;
                  ANTIPCON = this.oParentObject.w_DETIPCON;
                  and ANCODICE = this.oParentObject.w_DECODCON;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.oParentObject.w_DESCLF = NVL(cp_ToDate(_read_.ANDESCRI),cp_NullValue(_read_.ANDESCRI))
            this.oParentObject.w_DTOBSO = NVL(cp_ToDate(_read_.ANDTOBSO),cp_NullValue(_read_.ANDTOBSO))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.oParentObject.w_DEDESCON = this.oParentObject.w_DESCLF
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
          i_retcode = 'stop'
          return
        endif
      endif
      * --- Legge i Dati del Temporaneo
      this.oParentObject.WorkFromTrs()
      this.oParentObject.SaveDependsOn()
      this.oParentObject.w_DERIFPNT = space(10)
      * --- Carica il Temporaneo dei Dati
      this.oParentObject.TrsFromWork()
      * --- Flag Notifica Riga Variata
      if i_SRV<>"A"
        replace i_SRV with "U"
      endif
      ENDSCAN
      this.Pag2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    SELECT (this.oParentObject.cTrsName)
    GO this.w_Recpos
    With this.oParentObject
    .WorkFromTrs()
    .SaveDependsOn()
    .SetControlsValue()
    .ChildrenChangeRow()
    .oPgFrm.Page1.oPag.oBody.Refresh()
    .oPgFrm.Page1.oPag.oBody.nAbsRow=this.w_nAbsRow
    .oPgFrm.Page1.oPag.oBody.nRelRow=this.w_nRelRow
    EndWith
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CONTI'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
