* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsma_bvm                                                        *
*              Visualizzazione schede magazzi                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_63]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-23                                                      *
* Last revis.: 2007-11-27                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsma_bvm",oParentObject)
return(i_retval)

define class tgsma_bvm as StdBatch
  * --- Local variables
  w_SALDOPAR = 0
  w_SALDOPAR1 = 0
  w_SALPROG = 0
  w_LOOP = 0
  w_IMPORTO = 0
  * --- WorkFile variables
  SALDIART_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- LANCIA AL NOTIFYEVENT (da GSMA_SZM)
    if EMPTY(this.oParentObject.w_CODART) OR EMPTY(this.oParentObject.w_CODART2)
      ah_ErrorMsg("Inserire i codici articolo di selezione")
      i_retcode = 'stop'
      return
    endif
    this.oParentObject.w_MCODART = IIF(this.oParentObject.w_FLMOVI="S", this.oParentObject.w_CODART, REPL("@", 20))
    this.oParentObject.w_DCODART = IIF(this.oParentObject.w_FLDOCU="S", this.oParentObject.w_CODART, REPL("@", 20))
    this.oParentObject.w_MCODART2 = IIF(this.oParentObject.w_FLMOVI="S", this.oParentObject.w_CODART2, REPL("@", 20))
    this.oParentObject.w_DCODART2 = IIF(this.oParentObject.w_FLDOCU="S", this.oParentObject.w_CODART2, REPL("@", 20))
    This.OparentObject.NotifyEvent("Esegui")
    * --- Converto direttamente 
    if VALNAZ<>g_PERVAL
      SELECT ( this.oParentObject.w_ZOOMMAG.cCursor )
      GO TOP
      SCAN
      this.w_IMPORTO = VAL2MON(VALMAG, GETCAM(VALNAZ,I_DATSYS),1,I_DATSYS,G_PERVAL)
      replace VALMAG with this.w_IMPORTO
      ENDSCAN
    endif
    * --- Calcolo totale carichi e scarichi
    this.oParentObject.w_TOTCAR = 0
    this.oParentObject.w_TOTSCA = 0
    * --- Legge Totali Carichi, Scarichi
    SELECT ( this.oParentObject.w_ZOOMMAG.cCursor )
    GO TOP
    SUM QTACAR, QTASCA TO this.oParentObject.w_TOTCAR, this.oParentObject.w_TOTSCA
    * --- Se attivi i saldi progressivi disabilito l'ordinamento dell'elenco 
    *     (i saldi progressivi sono calcolati direttamente sul cursore dello zoom)
    this.oParentObject.w_ZOOMMAG.bQueryOnDblClick = this.oParentObject.w_SALDOPR<>"S"
    * --- Calcolo i saldi progressivi
    if this.oParentObject.w_SALDOPR="S"
      * --- Query in union (Mov.magazzino + Documenti) eseguo la Sum
      *     per determinare il totale esistenza fino alla data inizio selezione
      vq_exec("query\GSMA_SZ1",this,"SaldoIni")
       
 Select SaldoIni 
 Go Top 
 Sum TOTCASC1 To this.w_SALDOPAR
      * --- Rimouvo il temporaneo...
      if Used("saldoini")
         
 Select Saldoini 
 Use
      endif
      * --- Leggo l'esistenza attuale
      * --- Read from SALDIART
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.SALDIART_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.SALDIART_idx,2],.t.,this.SALDIART_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "SLQTAPER"+;
          " from "+i_cTable+" SALDIART where ";
              +"SLCODICE = "+cp_ToStrODBC(this.oParentObject.w_CODART);
              +" and SLCODMAG = "+cp_ToStrODBC(this.oParentObject.w_CODMAG);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          SLQTAPER;
          from (i_cTable) where;
              SLCODICE = this.oParentObject.w_CODART;
              and SLCODMAG = this.oParentObject.w_CODMAG;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_SALDOPAR1 = NVL(cp_ToDate(_read_.SLQTAPER),cp_NullValue(_read_.SLQTAPER))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Saldo iniziale dato da saldo magazzino - saldo inizio periodo
      this.oParentObject.w_SALDOINI = this.w_SALDOPAR1-this.w_SALDOPAR
      this.w_SALPROG = this.oParentObject.w_SALDOINI
      * --- Rendo visibile il campo SALPROG
      this.w_LOOP = 1
      do while this.w_LOOP<= this.oParentObject.w_ZOOMMAG.grd.ColumnCount
        if this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].ControlSource= "SALPROG" 
          this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].Visible = .T.
          this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].Width = 90
          this.w_LOOP = this.oParentObject.w_ZOOMMAG.grd.ColumnCount+1
        else
          this.w_LOOP = this.w_LOOP + 1
        endif
      enddo
      * --- Calcolo il saldo progressivo per ogni riga
      SELECT ( this.oParentObject.w_ZOOMMAG.cCursor )
      GO TOP
      SCAN
      * --- Moltiplico per 1.00000 per non perdere eventuali decimali....
      replace SALPROG with ( this.w_SALPROG+( QTACAR-QTASCA ) )*1.00000
      this.w_SALPROG=SALPROG
      ENDSCAN
      * --- Leggo il saldo finale (l'ultimo progressivo)
      SELECT ( this.oParentObject.w_ZOOMMAG.cCursor )
      GO BOTTOM
      this.oParentObject.w_SALDOFIN = IIF(Reccount()=0,this.oParentObject.w_SALDOINI,SALPROG)
    else
      * --- Se non calcolo i saldi azzero tutto...
      this.oParentObject.w_SALDOINI = 0
      this.oParentObject.w_SALDOFIN = 0
      * --- Nascondo il campo SALPROG
      this.w_LOOP = 1
      do while this.w_LOOP<= this.oParentObject.w_ZOOMMAG.grd.ColumnCount
        if this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].ControlSource= "SALPROG"
          this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].Visible = .F.
          * --- Nel caso della variante nasconde la caption del titolo ma la colonna rimane.
          this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].Width = 0
          this.w_LOOP = this.oParentObject.w_ZOOMMAG.grd.ColumnCount+1
        else
          this.w_LOOP = this.w_LOOP + 1
        endif
      enddo
    endif
    if this.oParentObject.w_CODART=this.oParentObject.w_CODART2
      * --- Rendo invisibile il campo CODART
      this.w_LOOP = 1
      do while this.w_LOOP<= this.oParentObject.w_ZOOMMAG.grd.ColumnCount
        do case
          case this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].ControlSource= "CODART" 
            this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].Visible = .F.
            this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].Width = 1
          case this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].ControlSource= "QTACAR" 
            this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].Hdr.Caption = iiF(this.oParentObject.w_FLELGM ="S",ah_Msgformat("Carichi"),iiF(this.oParentObject.w_FLELGM="T", ah_Msgformat("Car./Ord."),ah_Msgformat("Ordinato")))
          case this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].ControlSource= "QTASCA" 
            this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].Hdr.Caption = iiF(this.oParentObject.w_FLELGM ="S",ah_Msgformat("Scarichi"),iiF(this.oParentObject.w_FLELGM="T", ah_Msgformat("Sca./Imp./Ris."),ah_Msgformat("Imp./Ris.")))
        endcase
        this.w_LOOP = this.w_LOOP + 1
      enddo
    else
      * --- Rendo visibile il campo CODART
      this.w_LOOP = 1
      do while this.w_LOOP<= this.oParentObject.w_ZOOMMAG.grd.ColumnCount
        do case
          case this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].ControlSource= "CODART" 
            this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].Visible = .T.
            this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].Width = 95
          case this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].ControlSource= "QTACAR" 
            this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].Hdr.Caption = iiF(this.oParentObject.w_FLELGM ="S",ah_Msgformat("Carichi"),iiF(this.oParentObject.w_FLELGM="T", ah_Msgformat("Car./Ord."),ah_Msgformat("Ordinato")))
          case this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].ControlSource= "QTASCA" 
            this.oParentObject.w_ZOOMMAG.grd.Columns[ This.w_LOOP ].Hdr.Caption = iiF(this.oParentObject.w_FLELGM ="S",ah_Msgformat("Scarichi"),iiF(this.oParentObject.w_FLELGM="T", ah_Msgformat("Sca./Imp./Ris."),ah_Msgformat("Imp./Ris.")))
        endcase
        this.w_LOOP = this.w_LOOP + 1
      enddo
    endif
    * --- Mi rimetto all'inizio dello zoom...
    GO TOP
    this.oParentObject.w_ZOOMMAG.Refresh()     
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='SALDIART'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
