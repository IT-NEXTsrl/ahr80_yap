* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscp_bge                                                        *
*              Dichiarazione di intento - AHE                                  *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_25]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-03-22                                                      *
* Last revis.: 2002-03-22                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,w_OPERAZ
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscp_bge",oParentObject,m.w_OPERAZ)
return(i_retval)

define class tgscp_bge as StdBatch
  * --- Local variables
  w_OPERAZ = space(30)
  w_TIPOPE = space(1)
  w_IMPDIC = 0
  w_IMPUTI = 0
  w_CODIVE = space(5)
  w_RIFDIC = space(10)
  w_NDIC = 0
  w_ADIC = space(4)
  w_DDIC = ctod("  /  /  ")
  w_TDIC = space(1)
  w_DDIC = ctod("  /  /  ")
  w_TDIC = space(1)
  w_TIVA = space(1)
  w_CODCAA = space(5)
  w_CODVOC = space(15)
  w_CODVOR = space(15)
  * --- WorkFile variables
  ART_ICOL_idx=0
  CAM_AGAZ_idx=0
  CAANARTI_idx=0
  DIC_INTE_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Tipo operazione
    * --- Variabili caller
    * --- Variabili locali
    * --- Assegnamenti
    this.w_CODIVE = SPACE(5)
    this.w_RIFDIC = SPACE(10)
    do case
      case this.w_OPERAZ = "DICHIARAZIONE INTENTO"
        * --- Se Cliente/Fornitore e no Codice Iva Non Imponibile
        * --- La procedura prende sempre quella con data lettera pi� alta (Esclude dichiarazioni ad operazione specifica)
        * --- Select from DIC_INTE
        i_nConn=i_TableProp[this.DIC_INTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.DIC_INTE_idx,2],.t.,this.DIC_INTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" DIC_INTE ";
              +" where DICODICE="+cp_ToStrODBC(this.oParentObject.w_MVCODCON)+" AND DITIPCON="+cp_ToStrODBC(this.oParentObject.w_MVTIPCON)+" AND DIDATINI<="+cp_ToStrODBC(this.oParentObject.w_MVDATDOC)+" AND DIDATFIN>="+cp_ToStrODBC(this.oParentObject.w_MVDATDOC)+"";
              +" order by DIDATLET,DIDATDIC";
               ,"_Curs_DIC_INTE")
        else
          select * from (i_cTable);
           where DICODICE=this.oParentObject.w_MVCODCON AND DITIPCON=this.oParentObject.w_MVTIPCON AND DIDATINI<=this.oParentObject.w_MVDATDOC AND DIDATFIN>=this.oParentObject.w_MVDATDOC;
           order by DIDATLET,DIDATDIC;
            into cursor _Curs_DIC_INTE
        endif
        if used('_Curs_DIC_INTE')
          select _Curs_DIC_INTE
          locate for 1=1
          do while not(eof())
          this.w_TIPOPE = NVL(_Curs_DIC_INTE.DITIPOPE, " ")
          this.w_IMPDIC = _Curs_DIC_INTE.DIIMPDIC
          this.w_IMPUTI = _Curs_DIC_INTE.DIIMPUTI
          this.w_CODIVE = _Curs_DIC_INTE.DICODIVA
          this.w_RIFDIC = _Curs_DIC_INTE.DISERIAL
          this.w_NDIC = _Curs_DIC_INTE.DINUMDOC
          this.w_ADIC = _Curs_DIC_INTE.DI__ANNO
          this.w_DDIC = CP_TODATE(_Curs_DIC_INTE.DIDATLET)
          this.w_TDIC = _Curs_DIC_INTE.DITIPCON
          this.w_TIVA = _Curs_DIC_INTE.DITIPIVA
            select _Curs_DIC_INTE
            continue
          enddo
          use
        endif
        * --- Nel caso sia importo definito
        if this.w_TIPOPE = "I" and not(Empty(this.w_CODIVE))
          if this.w_IMPDIC > this.w_IMPUTI
            * --- Importo Definito - Applico dichiarazione di intento specificata
            this.oParentObject.w_MVRIFDIC = this.w_RIFDIC
            this.oParentObject.w_MVCODIVE = this.w_CODIVE
            * --- Assegno w_codice all'imballo, trasporto e incasso 
            this.oParentObject.w_MVIVAINC = this.w_CODIVE
            this.oParentObject.w_MVIVAIMB = this.w_CODIVE
            this.oParentObject.w_MVIVATRA = this.w_CODIVE
          else
            * --- Importo Disponibile della Dichiarazione di Esenzione Esaurito! - Non applico dichiarazione di intento
            this.oParentObject.w_MVRIFDIC = this.w_RIFDIC
            this.oParentObject.w_MVCODIVE = ""
            this.oParentObject.w_MVIVAINC = ""
            this.oParentObject.w_MVIVAIMB = ""
            this.oParentObject.w_MVIVATRA = ""
          endif
        endif
        * --- Nel caso sia operazione pianificata o definizione periodo
        if this.w_TIPOPE = "O" and not(Empty(this.w_CODIVE)) or this.w_TIPOPE = "D" and not(Empty(this.w_CODIVE))
          * --- Nel caso in cui la dichiarazione di intento � presente la passo a MVCODICE 
          this.oParentObject.w_MVRIFDIC = this.w_RIFDIC
          this.oParentObject.w_MVCODIVE = this.w_CODIVE
          * --- Assegno w_codice all'imballo, trasporto e incasso 
          this.oParentObject.w_MVIVAINC = this.w_CODIVE
          this.oParentObject.w_MVIVAIMB = this.w_CODIVE
          this.oParentObject.w_MVIVATRA = this.w_CODIVE
        else
          * --- Nel caso in cui la dichiarazione di intento � vuota passo MVCODICE vuoto
          if Empty(this.w_CODIVE)
            this.oParentObject.w_MVRIFDIC = this.w_RIFDIC
            this.oParentObject.w_MVCODIVE = ""
            this.oParentObject.w_MVIVAINC = ""
            this.oParentObject.w_MVIVAIMB = ""
            this.oParentObject.w_MVIVATRA = ""
          endif
        endif
      case this.w_OPERAZ = "VOCI ANALITICA"
        * --- Gestione Analitica
        * --- Legge Cat.Analitica Articoli
        * --- Read from ART_ICOL
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.ART_ICOL_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ART_ICOL_idx,2],.t.,this.ART_ICOL_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "ARCODCAA"+;
            " from "+i_cTable+" ART_ICOL where ";
                +"ARCODART = "+cp_ToStrODBC(this.oParentObject.w_MVCODART);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            ARCODCAA;
            from (i_cTable) where;
                ARCODART = this.oParentObject.w_MVCODART;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_CODCAA = NVL(cp_ToDate(_read_.ARCODCAA),cp_NullValue(_read_.ARCODCAA))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        if NOT EMPTY(this.w_CODCAA)
          this.w_CODVOC = SPACE(15)
          this.w_CODVOR = SPACE(15)
          * --- Read from CAANARTI
          i_nOldArea=select()
          if used('_read_')
            select _read_
            use
          endif
          i_nConn=i_TableProp[this.CAANARTI_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.CAANARTI_idx,2],.t.,this.CAANARTI_idx)
          if i_nConn<>0
            cp_sqlexec(i_nConn,"select "+;
              "CAVOCCOS,CAVOCRIC"+;
              " from "+i_cTable+" CAANARTI where ";
                  +"CACODICE = "+cp_ToStrODBC(this.w_CODCAA);
                   ,"_read_")
            i_Rows=iif(used('_read_'),reccount(),0)
          else
            select;
              CAVOCCOS,CAVOCRIC;
              from (i_cTable) where;
                  CACODICE = this.w_CODCAA;
               into cursor _read_
            i_Rows=_tally
          endif
          if used('_read_')
            locate for 1=1
            this.w_CODVOC = NVL(cp_ToDate(_read_.CAVOCCOS),cp_NullValue(_read_.CAVOCCOS))
            this.w_CODVOR = NVL(cp_ToDate(_read_.CAVOCRIC),cp_NullValue(_read_.CAVOCRIC))
            use
          else
            * --- Error: sql sentence error.
            i_Error = MSG_READ_ERROR
            return
          endif
          select (i_nOldArea)
          this.oParentObject.w_MVVOCCEN = IIF(this.oParentObject.w_MTIPVOC="C", this.w_CODVOC, this.w_CODVOR)
        endif
    endcase
  endproc


  proc Init(oParentObject,w_OPERAZ)
    this.w_OPERAZ=w_OPERAZ
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,4)]
    this.cWorkTables[1]='ART_ICOL'
    this.cWorkTables[2]='CAM_AGAZ'
    this.cWorkTables[3]='CAANARTI'
    this.cWorkTables[4]='DIC_INTE'
    return(this.OpenAllTables(4))

  proc CloseCursors()
    if used('_Curs_DIC_INTE')
      use in _Curs_DIC_INTE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="w_OPERAZ"
endproc
