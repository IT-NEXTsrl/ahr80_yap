* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bsp                                                        *
*              Chiusura partite da primanota                                   *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_55]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-24                                                      *
* Last revis.: 2011-03-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOPER
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bsp",oParentObject,m.pOPER)
return(i_retval)

define class tgscg_bsp as StdBatch
  * --- Local variables
  pOPER = space(1)
  w_FLAGAC = space(1)
  w_TIPGES = space(1)
  w_CAUSALE = space(5)
  w_FLDOC = space(1)
  w_MESS = space(10)
  w_OBJCTRL = .NULL.
  w_GSCG_MSC = .NULL.
  * --- WorkFile variables
  CAU_CONT_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Chiusura Partite da Gestione Partite in Primanota (da GSCG_MPA,GSCG_MSC)
    * --- Definisce l'Oggetto per la Chiusura delle Partite da Primanota (Deriv. MOVIMENTAZIONI)
    * --- P: Gestione Partite da Primanota  GSCG_MPN
    *     S: Saldaconto GSCG_MSC\Storno Conti SBF
    if this.pOPER="P"
      WITH this.oParentObject.oParentObject
      if (.w_PNFLPART<>"S" and .w_FLAGAC <>"S" ) OR .w_PARCAR="S" OR NOT EMPTY(.w_PNFLABAN) OR .cFunction="Query" 
        * --- Solo se Salda partite
        i_retcode = 'stop'
        return
      endif
      this.w_TIPGES = IIF(.w_FLAGAC="S","A","P")
      * --- Disabilita gli Accessi Automatici successivi
      .w_PARSAL="."
      ENDWITH
      PRIVATE p_PUNTAT
      p_PUNTAT = this.oParentObject
      * --- Puntatore all Oggetto Chiamante
      this.oParentObject.w_OGG = GSCG_MSP(this.w_TIPGES)
      * --- Controllo se ha passato il test di accesso
      if !(this.oParentObject.w_OGG.bSec1)
        i_retcode = 'stop'
        return
      endif
      * --- Lancia la Chiusura Partite (in Caricamento)
      this.oParentObject.w_OGG.ecpLoad()     
    else
      * --- Puntatore all Oggetto Chiamante da Saldaconto
      this.oParentObject.w_PARSAL = "."
      this.w_CAUSALE = IIF(this.oParentObject.w_SCTIPCON="C", this.oParentObject.w_SCCAUCON, IIF(this.oParentObject.w_SCTIPCON="F", this.oParentObject.w_SCCAUCOF, this.oParentObject.w_SCCAUCOC))
      * --- Read from CAU_CONT
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.CAU_CONT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CAU_CONT_idx,2],.t.,this.CAU_CONT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "CCNUMDOC"+;
          " from "+i_cTable+" CAU_CONT where ";
              +"CCCODICE = "+cp_ToStrODBC(this.w_CAUSALE);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          CCNUMDOC;
          from (i_cTable) where;
              CCCODICE = this.w_CAUSALE;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_FLDOC = NVL(cp_ToDate(_read_.CCNUMDOC),cp_NullValue(_read_.CCNUMDOC))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      * --- Disabilita gli Accessi Automatici successivi
      if  this.w_FLDOC="O" AND( this.oParentObject.w_SCNUMDOC=0 OR Empty(this.oParentObject.w_SCDATDOC))
        this.w_GSCG_MSC = this.oParentObject
        do case
          case Empty(this.oParentObject.w_SCDATDOC) AND this.oParentObject.w_SCNUMDOC=0
            this.w_MESS = "Inserire gli estremi del documento (numero e data)"
            this.w_OBJCTRL = this.w_GSCG_MSC.GetCtrl( "w_SCNUMDOC" )
          case Empty(this.oParentObject.w_SCDATDOC)
            this.w_MESS = "Inserire la data documento"
            this.w_OBJCTRL = this.w_GSCG_MSC.GetCtrl( "w_SCDATDOC" )
          case this.oParentObject.w_SCNUMDOC=0
            this.w_MESS = "Inserire il numero documento"
            this.w_OBJCTRL = this.w_GSCG_MSC.GetCtrl( "w_SCNUMDOC" )
        endcase
        ah_ErrorMsg(this.w_MESS,,"")
        * --- Punto il focus sul numero documento
        this.w_OBJCTRL.Setfocus()     
        i_retcode = 'stop'
        return
      else
        * --- Puntatore all Oggetto Chiamante
        PRIVATE p_PUNTAT
        p_PUNTAT = this
        this.oParentObject.w_OGG = GSCG_MSP(this.oParentObject.w_SCTIPGES)
        * --- Controllo se ha passato il test di accesso
        if !(this.oParentObject.w_OGG.bSec1)
          i_retcode = 'stop'
          return
        endif
        * --- Lancia la Chiusura Partite (in Caricamento)
        this.oParentObject.w_OGG.ecpLoad()     
      endif
    endif
  endproc


  proc Init(oParentObject,pOPER)
    this.pOPER=pOPER
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='CAU_CONT'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOPER"
endproc
