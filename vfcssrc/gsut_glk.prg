* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsut_glk                                                        *
*              Gadget Shortcut                                                 *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS:                                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2013-12-16                                                      *
* Last revis.: 2015-11-13                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgsut_glk",oParentObject))

* --- Class definition
define class tgsut_glk as StdGadgetForm
  Top    = 1
  Left   = 2

  * --- Standard Properties
  Width  = 145
  Height = 117
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-11-13"
  HelpContextID=47627415
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=10

  * --- Constant Properties
  _IDX = 0
  cPrg = "gsut_glk"
  cComment = "Gadget Shortcut"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_PROGRAM = space(254)
  w_IMAGE = space(254)
  w_RET = .F.
  w_TOOLTIP = space(254)
  w_IMGOLDW = 0
  w_IMGOLDH = 0
  w_LABEL = space(254)
  o_LABEL = space(254)
  w_FONTCLR = 0
  w_IMAGETMP = space(254)
  w_APPLYTHEMEIMG = .F.
  w_IMGOBJ = .NULL.
  w_TITLELBL = .NULL.
  * --- Area Manuale = Declare Variables
  * --- gsut_glk
  *--- Click lancio programma
  Proc Click()
    If !This.oMoveShape.Visible And !Empty(This.w_PROGRAM)
      Local l_oldError,ggname
      m.ggname = 'frm'+Alltrim(This.Name)
      This.cAlertMsg = ""
      m.l_oldError = On("Error")
      On Error &ggname..cAlertMsg=Message()
      ExecScript(This.w_PROGRAM)
      On error &l_oldError
    EndIf
  EndProc
  
  *--- Resize
  Proc Resize()
     *--- Centro immagine e label
     *--- disponendoli uno sopra l'altro o di fianco a seconda dello spazio
  	 With This
     If !.bDisabledResize
       DoDefault()
       Local l_Height,l_MinSize
       *--- Calcolo l'atezza del gadget da considerare per i calcoli
       m.l_Height = .Height-Iif(Pemstatus(this,'oHeader',5), .oHeader.Height, 0)
       
       *--- Disancoro i controlli prima di spostarli
       .w_TITLELBL.Anchor = 0
       .w_TITLELBL.Visible = !Empty(.w_LABEL)
       If .w_IMGOBJ.Visible
         .w_IMGOBJ.Anchor = 0
         *--- ridimensiono l'immagine se � pi� grande delle dimensioni del gadget
         If(.w_IMGOLDH>m.l_Height OR .w_IMGOLDW>.Width)
           Do Case
             Case .Width<m.l_Height And .Width>=4
               .w_IMGOBJ.Move(2, .w_IMGOBJ.Top, .Width-4, .Width-4)
             Case m.l_Height<.Width And m.l_Height>=4
               .w_IMGOBJ.Move(.w_IMGOBJ.Left, 2, m.l_Height-4, m.l_Height-4)
             Case .Width==m.l_Height And .Width>=4
               .w_IMGOBJ.Move(2, 2, .Width-4, .Width-4)
           Endcase
         Else
           *--- riallargo solo se necessario
           If (.w_IMGOLDH>.w_IMGOBJ.Height OR .w_IMGOLDW>.w_IMGOBJ.Width)
             .w_IMGOBJ.Move(.w_IMGOBJ.Left, .w_IMGOBJ.Top, .w_IMGOLDW, .w_IMGOLDH)
           EndIf
         EndIf
         *--- Allineo immagine e label a seconda della forma del gadget
         .w_TITLELBL.Width = 1 && larghezza minima per contenere la lbl
         If !Empty(.w_LABEL) And .Width==m.l_Height OR .Width<.w_IMGOBJ.Width+5+.w_TITLELBL.Width
           .w_IMGOBJ.Move((.Width*0.5)-(.w_IMGOBJ.Width*0.5), (m.l_Height*0.5)-((.w_IMGOBJ.Height+.w_TITLELBL.Height+5)*0.5))
           .w_TITLELBL.Width =  Max(.Width-6,1)
           .w_TITLELBL.Move(Max((.Width*0.5)-(.w_TITLELBL.Width*0.5),1), .w_IMGOBJ.Top+.w_IMGOBJ.Height+3)
           .w_TITLELBL.Visible = .Width>.w_TITLELBL.Width And .Height>.w_IMGOBJ.Height+5+.w_TITLELBL.Height
         Endif
         If !Empty(.w_LABEL) And !(.Width==m.l_Height OR .Width<.w_IMGOBJ.Width+5+.w_TITLELBL.Width)
           .w_TITLELBL.Width =  Max(.Width-(.w_IMGOBJ.Left+.w_IMGOBJ.Width+5),1)
           .w_IMGOBJ.Move(.Width*0.5-(.w_IMGOBJ.Width+.w_TITLELBL.Width+5)*0.5, l_Height*0.5-.w_IMGOBJ.Height*0.5)   
           .w_TITLELBL.Move(.w_IMGOBJ.Left+.w_IMGOBJ.Width+3, m.l_Height*0.5-.w_TITLELBL.Height*0.5)
           .w_TITLELBL.Visible = .Width>.w_IMGOBJ.Width+5+.w_TITLELBL.Width And .Height>.w_TITLELBL.Height
         Endif
         If !.w_TITLELBL.Visible
           .w_IMGOBJ.Move((.Width*0.5)-(.w_IMGOBJ.Width*0.5), (m.l_Height*0.5)-(.w_IMGOBJ.Height*0.5))
         Endif
         .w_IMGOBJ.Anchor = 768
       Else
         .w_TITLELBL.Move(3, (m.l_Height*0.5)-(.w_TITLELBL.Height*0.5), Max(.Width-6,1), m.l_Height-(m.l_Height*0.5)-(.w_TITLELBL.Height*0.5))
         .w_TITLELBL.Visible = !Empty(.w_LABEL) And .Width>=.w_TITLELBL.Width+2
       Endif
       .w_TITLELBL.Anchor = 768
     Endif
     Endwith
  EndProc
  
  *--- ApplyTheme
  Proc ApplyTheme()
     DoDefault()
     Local l_oldErr, l_OnErrorMsg
     With This
       m.l_OnErrorMsg = ""
       m.l_oldErr = On("Error")
       On Error m.l_OnErrorMsg=ah_MsgFormat("%1%0%2",Iif(Empty(m.l_OnErrorMsg), "Aggiornamento gadget:",m.l_OnErrorMsg),Message())
       .w_FONTCLR = Iif(This.nFontColor<>-1, This.nFontColor, Rgb(243,243,243))
       .w_IMAGETMP = Iif(.w_APPLYTHEMEIMG, cp_TmpColorizeImg(.w_IMAGE, .w_FONTCLR), .w_IMAGE)
       On Error &l_oldErr
       If !Empty(m.l_OnErrorMsg) && c'� stato un errore di programma
          .cAlertMsg = Iif(Empty(.cAlertMsg), m.l_OnErrorMsg, ah_MsgFormat("%1%0%2",.cAlertMsg,m.l_OnErrorMsg))
       Endif
       .mCalc(.T.)
       .SaveDependsOn()
     Endwith
  EndProc
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as cp_oPgFrm with PageCount=1, Top=0, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    cp_oPgFrm::Init()
    with this
      .Pages(1).addobject("oPag","tgsut_glkPag1","gsut_glk",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Pag.1")
      .Pages(1).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = .NULL.
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_IMGOBJ = this.oPgFrm.Pages(1).oPag.IMGOBJ
    this.w_TITLELBL = this.oPgFrm.Pages(1).oPag.TITLELBL
    DoDefault()
    proc Destroy()
      this.w_IMGOBJ = .NULL.
      this.w_TITLELBL = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    return(this.OpenAllTables(0))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- gsut_glk
    *--- Click su immagine, titolo o pagina
    BindEvent(This.oPgFrm.Page1.oPag, "Click", This, "Click")
    BindEvent(This.w_IMGOBJ, "Click", This, "Click")
    BindEvent(This.w_TITLELBL, "Click", This, "Click")
    
    *--- Puntatore a manina
    This.w_IMGOBJ.MousePointer = 15
    This.w_TITLELBL.WordWrap = .T.
    This.w_TITLELBL.Autosize = .T.
    This.w_TITLELBL.MousePointer = 15
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_PROGRAM=space(254)
      .w_IMAGE=space(254)
      .w_RET=.f.
      .w_TOOLTIP=space(254)
      .w_IMGOLDW=0
      .w_IMGOLDH=0
      .w_LABEL=space(254)
      .w_FONTCLR=0
      .w_IMAGETMP=space(254)
      .w_APPLYTHEMEIMG=.f.
      .oPgFrm.Page1.oPag.IMGOBJ.Calculate(.w_IMAGETMP)
          .DoRTCalc(1,1,.f.)
        .w_IMAGE = "OpenGest('A', [gsma_aar])"
      .oPgFrm.Page1.oPag.TITLELBL.Calculate(.w_LABEL,.w_FONTCLR)
          .DoRTCalc(3,7,.f.)
        .w_FONTCLR = Rgb(243,243,243)
          .DoRTCalc(9,9,.f.)
        .w_APPLYTHEMEIMG = .t.
    endwith
    this.SaveDependsOn()
    this.SetControlsValue()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
        .oPgFrm.Page1.oPag.IMGOBJ.Calculate(.w_IMAGETMP)
        .oPgFrm.Page1.oPag.TITLELBL.Calculate(.w_LABEL,.w_FONTCLR)
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(1,10,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.IMGOBJ.Calculate(.w_IMAGETMP)
        .oPgFrm.Page1.oPag.TITLELBL.Calculate(.w_LABEL,.w_FONTCLR)
    endwith
  return

  proc Calculate_YCUUTVASEB()
    with this
          * --- Gadget Option
          .w_RET = This.GadgetOption()
          .w_RET = ApplyOptions(This)
    endwith
  endproc
  proc Calculate_CERLBHQYCD()
    with this
          * --- Init
          .w_RET = DefaultImageSize(This)
          .w_RET = ApplyOptions(This)
    endwith
  endproc

  * --- Enable controls under condition
  procedure mEnableControls()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.IMGOBJ.Event(cEvent)
      .oPgFrm.Page1.oPag.TITLELBL.Event(cEvent)
        if lower(cEvent)==lower("GadgetOption")
          .Calculate_YCUUTVASEB()
          bRefresh=.t.
        endif
        if lower(cEvent)==lower("GadgetArranged")
          .Calculate_CERLBHQYCD()
          bRefresh=.t.
        endif
    endwith
    * --- Area Manuale = Notify Event End
    * --- gsut_glk
    *--- valorizzo il MousePointer della pagina dopo l'ultimo evento perch� prima me lo sovrascriverebbe la cp_forms
    If Upper(cEvent)=="CFGLOADED"
       This.oPgFrm.Page1.oPag.MousePointer = 15
    Endif
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  function SetControlsValue()
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      * --- Area Manuale = Check Form
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_LABEL = this.w_LABEL
    return

enddefine

* --- Define pages as container
define class tgsut_glkPag1 as StdContainer
  Width  = 145
  height = 117
  stdWidth  = 145
  stdheight = 117
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.


  add object IMGOBJ as cp_showimage with uid="WHGUQPPEKG",left=27, top=2, width=86,height=84,;
    caption='IMGOBJ',;
   bGlobalFont=.t.,;
    stretch=1,default="",;
    nPag=1;
    , HelpContextID = 21659270


  add object TITLELBL as cp_calclbl with uid="MHNZOVMJBZ",left=1, top=89, width=139,height=25,;
    caption='TITLELBL',;
   bGlobalFont=.t.,;
    caption="",fontUnderline=.f.,bGlobalFont=.f.,alignment=2,fontname="Open Sans",fontsize=10,fontBold=.f.,fontItalic=.f.,;
    nPag=1;
    , HelpContextID = 210220158
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gsut_glk','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- gsut_glk
*--- Refresh
Proc ApplyOptions(pParent)
   Local l_oldErr, l_OnErrorMsg, l_AlertMsg
   With m.pParent
     m.l_AlertMsg = ""
     m.l_OnErrorMsg = ""
     m.l_oldErr = On("Error")
     On Error m.l_OnErrorMsg=ah_MsgFormat("%1%0%2",Iif(Empty(m.l_OnErrorMsg), "Aggiornamento gadget:",m.l_OnErrorMsg),Message())
     
     *--- Trimmo i valori ricevuti
     .w_LABEL = ALLTRIM(.w_LABEL)
     .w_IMAGE = ALLTRIM(.w_IMAGE)
     .w_TOOLTIP = ALLTRIM(.w_TOOLTIP)

     *--- Setto il nuovo tooltip
     .w_TITLELBL.ToolTipText = .w_TOOLTIP
     .w_IMGOBJ.ToolTipText = .w_TOOLTIP
      
     *--- Controllo se � stata passata un'immagine, altrimenti nascondo il controllo
     .w_IMAGETMP = Iif(.w_APPLYTHEMEIMG, cp_TmpColorizeImg(.w_IMAGE, .w_FONTCLR), .w_IMAGE)
     .w_IMGOBJ.Visible = !Empty(.w_IMAGETMP) And cp_FileExist(.w_IMAGETMP)
     .w_TITLELBL.Visible = !Empty(.w_LABEL)
      
     *--- Verifico che il w_PROGRAM sia valorizzato
     If Empty(.w_PROGRAM)
       m.l_AlertMsg = cp_Translate('Comando per il link non definito')
     Endif
     
     *--- Eventuale messaggio di errore
     .cAlertMsg = m.l_AlertMsg
      
     On Error &l_oldErr
     If !Empty(m.l_OnErrorMsg) && c'� stato un errore di programma
        .cAlertMsg = Iif(Empty(.cAlertMsg), m.l_OnErrorMsg, ah_MsgFormat("%1%0%2",.cAlertMsg,m.l_OnErrorMsg))
     Endif
     
     *--- Aggiorno i controlli
     .mCalc(.T.)
     .SaveDependsOn()
   EndWith
   .Resize()
EndProc

*--- Save Default Image Dimension
Proc DefaultImageSize(pParent)
   With m.pParent
      .w_IMGOLDW = .w_IMGOBJ.Width
      .w_IMGOLDH = .w_IMGOBJ.Height
   EndWith
EndProc
* --- Fine Area Manuale
