* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_kms                                                        *
*              Gestione partite/scadenze                                       *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [111] [VRS_264]                                                 *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-01-27                                                      *
* Last revis.: 2015-02-23                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
return(createobject("tgste_kms",oParentObject))

* --- Class definition
define class tgste_kms as StdForm
  Top    = 0
  Left   = 4

  * --- Standard Properties
  Width  = 791
  Height = 481+35
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.
  infodaterev="2015-02-23"
  HelpContextID=199898217
  nMaxFieldsJoin = 200			  && indica il numero massimo di campi per AddJoinedLink
  max_rt_seq=70

  * --- Constant Properties
  _IDX = 0
  VALUTE_IDX = 0
  CONTI_IDX = 0
  BAN_CHE_IDX = 0
  COC_MAST_IDX = 0
  AGENTI_IDX = 0
  CAU_CONT_IDX = 0
  cPrg = "gste_kms"
  cComment = "Gestione partite/scadenze"
  oParentObject = .Null.
  icon = "mask.ico"
  *closable = .f.

  * --- Local Variables
  w_CALCPIP = 0
  w_CODAZI = space(5)
  w_SCAINI = ctod('  /  /  ')
  o_SCAINI = ctod('  /  /  ')
  w_SCAFIN = ctod('  /  /  ')
  w_VISUAL = .F.
  w_ROWSCAD = 0
  w_TIPCON = space(1)
  o_TIPCON = space(1)
  w_CODCON = space(15)
  w_DESCON = space(40)
  w_FLSALD = space(1)
  o_FLSALD = space(1)
  w_FLPART = space(1)
  w_DATPAR = ctod('  /  /  ')
  w_OBTEST = ctod('  /  /  ')
  w_DATOBSO = ctod('  /  /  ')
  w_SERIALE = space(35)
  w_TIPMOV = 0
  w_ROWORD = 0
  w_ROWRIF = 0
  w_NEWSCA = ctod('  /  /  ')
  w_NEWPAR = space(31)
  w_PAGRD = space(2)
  w_PAGBO = space(2)
  w_PAGRB = space(2)
  w_PAGRI = space(2)
  w_PAGCA = space(2)
  w_PAGMA = space(2)
  w_PAGNO = space(2)
  w_REGINI = ctod('  /  /  ')
  w_REGFIN = ctod('  /  /  ')
  w_NUMPAR = space(31)
  w_BANAPP = space(10)
  w_DESBAN = space(50)
  w_BANNOS = space(15)
  w_DESNOS = space(35)
  w_NDOINI = 0
  w_ADOINI = space(10)
  w_DDOINI = ctod('  /  /  ')
  w_NDOFIN = 0
  w_ADOFIN = space(10)
  w_DDOFIN = ctod('  /  /  ')
  w_VALUTA = space(3)
  w_DESVAL = space(35)
  w_FLSOSP = space(1)
  w_CODAGE = space(5)
  w_FLGSOS = space(1)
  w_FLGNSO = space(1)
  w_SIMVAL = space(5)
  w_NEWAGE = space(5)
  w_DESAGE = space(35)
  w_AGEOBSO = ctod('  /  /  ')
  w_NEWREG = ctod('  /  /  ')
  w_TOTIMP = 0
  w_PARTSN = space(1)
  w_SERDIS = space(10)
  w_ORDRIF = 0
  w_NUMRIF = 0
  w_SERRIF = space(10)
  w_NEWCON = space(15)
  w_NEWVAL = space(5)
  w_NUMPAR1 = space(31)
  w_FLPART1 = space(1)
  w_TIPPAG = space(2)
  w_ROWNUM = 0
  w_CODCLF = space(15)
  w_TOTDAR = 0
  w_TOTAVE = 0
  w_SALDOD = 0
  w_SALDOA = 0
  w_FLAGTOT = space(1)
  w_VALSAL = space(5)
  w_ZoomScad = .NULL.
  * --- Area Manuale = Declare Variables
  * --- Fine Area Manuale

  * --- Define Page Frame
  add object oPgFrm as StdPCPageFrame with PageCount=2, Width=this.Width, Height=this.Height
  proc oPgFrm.Init
    stdPCPageFrame::Init()
    with this
      .Pages(1).addobject("oPag","tgste_kmsPag1","gste_kms",1)
      .Pages(1).oPag.Visible=.t.
      .Pages(1).Caption=cp_Translate("Selezioni")
      .Pages(2).addobject("oPag","tgste_kmsPag2","gste_kms",2)
      .Pages(2).oPag.Visible=.t.
      .Pages(2).Caption=cp_Translate("Selezioni aggiuntive")
      .Pages(2).oPag.oContained=thisform
    endwith
    this.Parent.oFirstControl = this.Page1.oPag.oSCAINI_1_3
    this.parent.cComment=cp_Translate(this.parent.cComment)
    * --- Area Manuale = Init Page Frame
    * --- gste_kms
    If Vartype(OPARENTOBJECT)='C' and OPARENTOBJECT = 'E'
        This.parent.cComment = "Ricerca incassi"
    Endif
    
    * --- Fine Area Manuale
  endproc
  proc Init(oParentObject)
    if vartype(m.oParentObject)<>'L'
      this.oParentObject=oParentObject
    endif
    this.w_ZoomScad = this.oPgFrm.Pages(1).oPag.ZoomScad
    DoDefault()
    proc Destroy()
      this.w_ZoomScad = .NULL.
      DoDefault()

  * --- Open tables
  function OpenWorkTables()
    dimension this.cWorkTables[6]
    this.cWorkTables[1]='VALUTE'
    this.cWorkTables[2]='CONTI'
    this.cWorkTables[3]='BAN_CHE'
    this.cWorkTables[4]='COC_MAST'
    this.cWorkTables[5]='AGENTI'
    this.cWorkTables[6]='CAU_CONT'
    return(this.OpenAllTables(6))

  procedure SetPostItConn()
    return


  * --- Read record and initialize Form variables
  procedure LoadRec()
  endproc

  * --- ecpQuery
  procedure ecpQuery()
    this.BlankRec()
    this.cFunction="Edit"
    this.SetStatus()
    if this.cFunction="Edit"
      this.mEnableControls()
    endif
  endproc
  * --- Esc
  proc ecpQuit()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      this.cFunction = "Edit"
      this.oFirstControl.SetFocus()
      return
    endif
    * ---
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  endproc
  proc QueryUnload()
    * --- Move without activating controls
    this.cFunction='Filter'
    this.__dummy__.enabled=.t.
    this.__dummy__.Setfocus()
    if !this.OkToQuit()
      this.__dummy__.enabled=.f.
      nodefault
      return
    endif
    this.Hide()
    this.NotifyEvent("Edit Aborted")
    this.NotifyEvent("Done")
    this.Release()
  proc ecpSave()
    if this.TerminateEdit() and this.CheckForm()
      this.lockscreen=.t.
      this.mReplace(.t.)
      this.lockscreen=.f.
      this.NotifyEvent("Done")
      this.Release()
    endif
  endproc
  func OkToQuit()
    return .t.
  endfunc

  * --- Blank Form variables
  procedure BlankRec()
    * --- Area Manuale = Blank Record Init
    * --- Fine Area Manuale
    this.ChildrenNewDocument()
    with this
      .w_CALCPIP=0
      .w_CODAZI=space(5)
      .w_SCAINI=ctod("  /  /  ")
      .w_SCAFIN=ctod("  /  /  ")
      .w_VISUAL=.f.
      .w_ROWSCAD=0
      .w_TIPCON=space(1)
      .w_CODCON=space(15)
      .w_DESCON=space(40)
      .w_FLSALD=space(1)
      .w_FLPART=space(1)
      .w_DATPAR=ctod("  /  /  ")
      .w_OBTEST=ctod("  /  /  ")
      .w_DATOBSO=ctod("  /  /  ")
      .w_SERIALE=space(35)
      .w_TIPMOV=0
      .w_ROWORD=0
      .w_ROWRIF=0
      .w_NEWSCA=ctod("  /  /  ")
      .w_NEWPAR=space(31)
      .w_PAGRD=space(2)
      .w_PAGBO=space(2)
      .w_PAGRB=space(2)
      .w_PAGRI=space(2)
      .w_PAGCA=space(2)
      .w_PAGMA=space(2)
      .w_PAGNO=space(2)
      .w_REGINI=ctod("  /  /  ")
      .w_REGFIN=ctod("  /  /  ")
      .w_NUMPAR=space(31)
      .w_BANAPP=space(10)
      .w_DESBAN=space(50)
      .w_BANNOS=space(15)
      .w_DESNOS=space(35)
      .w_NDOINI=0
      .w_ADOINI=space(10)
      .w_DDOINI=ctod("  /  /  ")
      .w_NDOFIN=0
      .w_ADOFIN=space(10)
      .w_DDOFIN=ctod("  /  /  ")
      .w_VALUTA=space(3)
      .w_DESVAL=space(35)
      .w_FLSOSP=space(1)
      .w_CODAGE=space(5)
      .w_FLGSOS=space(1)
      .w_FLGNSO=space(1)
      .w_SIMVAL=space(5)
      .w_NEWAGE=space(5)
      .w_DESAGE=space(35)
      .w_AGEOBSO=ctod("  /  /  ")
      .w_NEWREG=ctod("  /  /  ")
      .w_TOTIMP=0
      .w_PARTSN=space(1)
      .w_SERDIS=space(10)
      .w_ORDRIF=0
      .w_NUMRIF=0
      .w_SERRIF=space(10)
      .w_NEWCON=space(15)
      .w_NEWVAL=space(5)
      .w_NUMPAR1=space(31)
      .w_FLPART1=space(1)
      .w_TIPPAG=space(2)
      .w_ROWNUM=0
      .w_CODCLF=space(15)
      .w_TOTDAR=0
      .w_TOTAVE=0
      .w_SALDOD=0
      .w_SALDOA=0
      .w_FLAGTOT=space(1)
      .w_VALSAL=space(5)
        .w_CALCPIP = DEFPIP(g_PERPVL)
        .w_CODAZI = i_CODAZI
        .w_SCAINI = i_datsys
        .w_SCAFIN = i_datsys+365
        .w_VISUAL = .T.
          .DoRTCalc(6,6,.f.)
        .w_TIPCON = 'C'
        .DoRTCalc(8,8,.f.)
        if not(empty(.w_CODCON))
          .link_1_8('Full')
        endif
          .DoRTCalc(9,9,.f.)
        .w_FLSALD = 'R'
        .w_FLPART = 'T'
        .w_DATPAR = cp_CharToDate('  -  -  ')
        .w_OBTEST = iif(empty(.w_SCAINI),i_INIDAT,.w_SCAINI)
      .oPgFrm.Page1.oPag.ZoomScad.Calculate()
          .DoRTCalc(14,14,.f.)
        .w_SERIALE = .w_ZoomScad.getVar('PTSERIAL')
        .w_TIPMOV = .w_ZoomScad.getVar('PTROWORD')
        .w_ROWORD = .w_ZoomScad.getVar('PTROWORD')
        .w_ROWRIF = .w_ZoomScad.getVar('CPROWNUM')
        .w_NEWSCA = CP_TODATE(.w_ZoomScad.getVar('DATSCA'))
        .w_NEWPAR = IIF(Occurs('/',ALLTRIM(.w_ZoomScad.getVar('NUMPAR')))=1 Or Occurs('/',ALLTRIM(.w_ZoomScad.getVar('NUMPAR')))=0, ALLTRIM(.w_ZoomScad.getVar('NUMPAR')), Left(ALLTRIM(.w_ZoomScad.getVar('NUMPAR')),AT('/',ALLTRIM(.w_ZoomScad.getVar('NUMPAR')),2)-1))+'/R'
        .w_PAGRD = 'RD'
        .w_PAGBO = 'BO'
        .w_PAGRB = 'RB'
        .w_PAGRI = 'RI'
        .w_PAGCA = 'CA'
        .w_PAGMA = 'MA'
        .w_PAGNO = 'XX'
        .DoRTCalc(28,31,.f.)
        if not(empty(.w_BANAPP))
          .link_2_11('Full')
        endif
        .DoRTCalc(32,33,.f.)
        if not(empty(.w_BANNOS))
          .link_2_13('Full')
        endif
        .DoRTCalc(34,41,.f.)
        if not(empty(.w_VALUTA))
          .link_2_21('Full')
        endif
          .DoRTCalc(42,42,.f.)
        .w_FLSOSP = 'T'
        .w_CODAGE = SPACE(5)
        .DoRTCalc(44,44,.f.)
        if not(empty(.w_CODAGE))
          .link_2_24('Full')
        endif
        .w_FLGSOS = IIF(.w_FLSOSP $ 'TN', ' ', 'S')
        .w_FLGNSO = IIF(.w_FLSOSP $ 'TS', ' ', 'S')
      .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .w_SIMVAL = IIF(EMPTY(.w_VALUTA),' ',.w_SIMVAL)
        .w_NEWAGE = ALLTRIM(.w_ZoomScad.getVar('CODAGE'))
          .DoRTCalc(49,50,.f.)
        .w_NEWREG = CP_TODATE(.w_ZoomScad.getVar('DATREG'))
        .w_TOTIMP = abs(nvl(.w_ZoomScad.getVar('TOTIMP'),0))
          .DoRTCalc(53,53,.f.)
        .w_SERDIS = NVL(.w_ZoomScad.getVar('RIFDIS'),SPACE(10))
        .w_ORDRIF = .w_ZoomScad.getVar('PTORDRIF')
        .w_NUMRIF = .w_ZoomScad.getVar('PTNUMRIF')
        .w_SERRIF = .w_ZoomScad.getVar('PTSERRIF')
        .w_NEWCON = ALLTRIM(.w_ZoomScad.getVar('CODCON'))
        .w_NEWVAL = ALLTRIM(.w_ZoomScad.getVar('CODVAL'))
        .w_NUMPAR1 = ALLTRIM(.w_ZoomScad.getVar('NUMPAR'))
        .w_FLPART1 = IIF(.w_FLPART='T', ' ', .w_FLPART)
        .w_TIPPAG = .w_ZoomScad.getVar('TIPPAG')
        .w_ROWNUM = .w_ZoomScad.getVar('CPROWNUM')
        .w_CODCLF = .w_ZoomScad.getVar('CODCON')
      .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .w_TOTDAR = 0
        .w_TOTAVE = 0
        .w_SALDOD = .w_TOTDAR-.w_TOTAVE
        .w_SALDOA = ABS(.w_TOTDAR-.w_TOTAVE)
        .w_FLAGTOT = 'N'
      .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
      .oPgFrm.Page1.oPag.oObj_1_63.Calculate(iif(.w_FLSALD<>'R' OR .w_FLAGTOT='N','',AH_Msgformat('Saldo partite/scadenze espresso in %1:',LEFT(.w_VALSAL+SPACE(5),5)) ))
    endwith
    this.DoRTCalc(70,70,.f.)
    this.SaveDependsOn()
    this.SetControlsValue()
    this.oPgFrm.Page1.oPag.oBtn_1_17.enabled = this.oPgFrm.Page1.oPag.oBtn_1_17.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_26.enabled = this.oPgFrm.Page1.oPag.oBtn_1_26.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_31.enabled = this.oPgFrm.Page1.oPag.oBtn_1_31.mCond()
    this.oPgFrm.Page2.oPag.oBtn_2_45.enabled = this.oPgFrm.Page2.oPag.oBtn_2_45.mCond()
    this.mHideControls()
    this.NotifyEvent('Blank')
    * --- Area Manuale = Blank Record End
    * --- Fine Area Manuale
  endproc

  * --- Procedure for field enabling
  *     cOp = Setting operation
  *     Allowed Parameters: Load - Query - Edit - Filter
  *     Load and Filter set all fields
  *
  procedure SetEnabled(i_cOp)
    * --- Area Manuale = Enable Controls Init
    * --- Fine Area Manuale
    * --- Deactivating List page when <> da Query
    * --- Area Manuale = Enable Controls End
    * --- Fine Area Manuale
  endproc

  *procedure SetChildrenStatus
  *endproc

  * ------ Redefined procedures from Standard Form

  * --- Generate filter
  func BuildFilter()
    local i_cFlt
    i_cFlt =""
    return (i_cFlt)
  endfunc

  proc QueryKeySet(i_cWhere,i_cOrderBy)
  endproc

  proc SelectCursor
  proc GetXKey

  * --- Update Database
  function mReplace(i_bEditing)
    * --- Area Manuale = Replace Init
    * --- Fine Area Manuale
    this.NotifyEvent('Update start')
    with this
    endwith
    this.NotifyEvent('Update end')
    * --- Area Manuale = Replace End
    * --- Fine Area Manuale
  return(.t.)

  * --- Calculations
  function mCalc(i_bUpd)
    this.bCalculating=.t.
    if i_bUpd
      with this
            .w_CALCPIP = DEFPIP(g_PERPVL)
        .DoRTCalc(2,10,.t.)
        if .o_FLSALD<>.w_FLSALD
            .w_FLPART = 'T'
        endif
        if .o_FLSALD<>.w_FLSALD
            .w_DATPAR = cp_CharToDate('  -  -  ')
        endif
        if .o_SCAINI<>.w_SCAINI
            .w_OBTEST = iif(empty(.w_SCAINI),i_INIDAT,.w_SCAINI)
        endif
        .oPgFrm.Page1.oPag.ZoomScad.Calculate()
        .DoRTCalc(14,14,.t.)
            .w_SERIALE = .w_ZoomScad.getVar('PTSERIAL')
            .w_TIPMOV = .w_ZoomScad.getVar('PTROWORD')
            .w_ROWORD = .w_ZoomScad.getVar('PTROWORD')
            .w_ROWRIF = .w_ZoomScad.getVar('CPROWNUM')
            .w_NEWSCA = CP_TODATE(.w_ZoomScad.getVar('DATSCA'))
            .w_NEWPAR = IIF(Occurs('/',ALLTRIM(.w_ZoomScad.getVar('NUMPAR')))=1 Or Occurs('/',ALLTRIM(.w_ZoomScad.getVar('NUMPAR')))=0, ALLTRIM(.w_ZoomScad.getVar('NUMPAR')), Left(ALLTRIM(.w_ZoomScad.getVar('NUMPAR')),AT('/',ALLTRIM(.w_ZoomScad.getVar('NUMPAR')),2)-1))+'/R'
        .DoRTCalc(21,43,.t.)
        if .o_TIPCON<>.w_TIPCON
            .w_CODAGE = SPACE(5)
          .link_2_24('Full')
        endif
            .w_FLGSOS = IIF(.w_FLSOSP $ 'TN', ' ', 'S')
            .w_FLGNSO = IIF(.w_FLSOSP $ 'TS', ' ', 'S')
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
            .w_SIMVAL = IIF(EMPTY(.w_VALUTA),' ',.w_SIMVAL)
            .w_NEWAGE = ALLTRIM(.w_ZoomScad.getVar('CODAGE'))
        .DoRTCalc(49,50,.t.)
            .w_NEWREG = CP_TODATE(.w_ZoomScad.getVar('DATREG'))
            .w_TOTIMP = abs(nvl(.w_ZoomScad.getVar('TOTIMP'),0))
        .DoRTCalc(53,53,.t.)
            .w_SERDIS = NVL(.w_ZoomScad.getVar('RIFDIS'),SPACE(10))
            .w_ORDRIF = .w_ZoomScad.getVar('PTORDRIF')
            .w_NUMRIF = .w_ZoomScad.getVar('PTNUMRIF')
            .w_SERRIF = .w_ZoomScad.getVar('PTSERRIF')
            .w_NEWCON = ALLTRIM(.w_ZoomScad.getVar('CODCON'))
            .w_NEWVAL = ALLTRIM(.w_ZoomScad.getVar('CODVAL'))
            .w_NUMPAR1 = ALLTRIM(.w_ZoomScad.getVar('NUMPAR'))
            .w_FLPART1 = IIF(.w_FLPART='T', ' ', .w_FLPART)
            .w_TIPPAG = .w_ZoomScad.getVar('TIPPAG')
            .w_ROWNUM = .w_ZoomScad.getVar('CPROWNUM')
            .w_CODCLF = .w_ZoomScad.getVar('CODCON')
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        if .o_FLSALD<>.w_FLSALD
            .w_TOTDAR = 0
        endif
        if .o_FLSALD<>.w_FLSALD
            .w_TOTAVE = 0
        endif
        if .o_FLSALD<>.w_FLSALD
            .w_SALDOD = .w_TOTDAR-.w_TOTAVE
        endif
        if .o_FLSALD<>.w_FLSALD
            .w_SALDOA = ABS(.w_TOTDAR-.w_TOTAVE)
        endif
        if .o_FLSALD<>.w_FLSALD
            .w_FLAGTOT = 'N'
        endif
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate(iif(.w_FLSALD<>'R' OR .w_FLAGTOT='N','',AH_Msgformat('Saldo partite/scadenze espresso in %1:',LEFT(.w_VALSAL+SPACE(5),5)) ))
        * --- Area Manuale = Calculate
        * --- Fine Area Manuale
      endwith
      this.DoRTCalc(70,70,.t.)
      this.SetControlsValue()
      if inlist(this.cFunction,'Edit','Load')
        this.bUpdated=.t.
        this.mEnableControls()
      endif
    endif
    this.bCalculating=.f.
  return

  proc mCalcObjs()
    with this
        .oPgFrm.Page1.oPag.ZoomScad.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_37.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_55.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_62.Calculate()
        .oPgFrm.Page1.oPag.oObj_1_63.Calculate(iif(.w_FLSALD<>'R' OR .w_FLAGTOT='N','',AH_Msgformat('Saldo partite/scadenze espresso in %1:',LEFT(.w_VALSAL+SPACE(5),5)) ))
    endwith
  return


  * --- Enable controls under condition
  procedure mEnableControls()
    this.oPgFrm.Page1.oPag.oTIPCON_1_7.enabled = this.oPgFrm.Page1.oPag.oTIPCON_1_7.mCond()
    this.oPgFrm.Page2.oPag.oCODAGE_2_24.enabled = this.oPgFrm.Page2.oPag.oCODAGE_2_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_18.enabled = this.oPgFrm.Page1.oPag.oBtn_1_18.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_20.enabled = this.oPgFrm.Page1.oPag.oBtn_1_20.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_21.enabled = this.oPgFrm.Page1.oPag.oBtn_1_21.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_22.enabled = this.oPgFrm.Page1.oPag.oBtn_1_22.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_23.enabled = this.oPgFrm.Page1.oPag.oBtn_1_23.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_24.enabled = this.oPgFrm.Page1.oPag.oBtn_1_24.mCond()
    this.oPgFrm.Page1.oPag.oBtn_1_25.enabled = this.oPgFrm.Page1.oPag.oBtn_1_25.mCond()
    this.mHideControls()
    If i_cHlOblColor='S'
      this.mObblControls()
    EndIf
    DoDefault()
  return

  procedure mObblControls()
    local oField, cCShape
    DoDefault()
  return

  procedure mHideControls()
    this.oPgFrm.Page1.oPag.oFLSALD_1_10.visible=!this.oPgFrm.Page1.oPag.oFLSALD_1_10.mHide()
    this.oPgFrm.Page1.oPag.oFLPART_1_11.visible=!this.oPgFrm.Page1.oPag.oFLPART_1_11.mHide()
    this.oPgFrm.Page1.oPag.oDATPAR_1_12.visible=!this.oPgFrm.Page1.oPag.oDATPAR_1_12.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_15.visible=!this.oPgFrm.Page1.oPag.oStr_1_15.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_18.visible=!this.oPgFrm.Page1.oPag.oBtn_1_18.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_20.visible=!this.oPgFrm.Page1.oPag.oBtn_1_20.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_21.visible=!this.oPgFrm.Page1.oPag.oBtn_1_21.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_22.visible=!this.oPgFrm.Page1.oPag.oBtn_1_22.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_23.visible=!this.oPgFrm.Page1.oPag.oBtn_1_23.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_25.visible=!this.oPgFrm.Page1.oPag.oBtn_1_25.mHide()
    this.oPgFrm.Page1.oPag.oBtn_1_26.visible=!this.oPgFrm.Page1.oPag.oBtn_1_26.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_35.visible=!this.oPgFrm.Page1.oPag.oStr_1_35.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_36.visible=!this.oPgFrm.Page1.oPag.oStr_1_36.mHide()
    this.oPgFrm.Page2.oPag.oCODAGE_2_24.visible=!this.oPgFrm.Page2.oPag.oCODAGE_2_24.mHide()
    this.oPgFrm.Page2.oPag.oDESAGE_2_40.visible=!this.oPgFrm.Page2.oPag.oDESAGE_2_40.mHide()
    this.oPgFrm.Page2.oPag.oStr_2_41.visible=!this.oPgFrm.Page2.oPag.oStr_2_41.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_49.visible=!this.oPgFrm.Page1.oPag.oStr_1_49.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_51.visible=!this.oPgFrm.Page1.oPag.oStr_1_51.mHide()
    this.oPgFrm.Page1.oPag.oTOTDAR_1_56.visible=!this.oPgFrm.Page1.oPag.oTOTDAR_1_56.mHide()
    this.oPgFrm.Page1.oPag.oTOTAVE_1_57.visible=!this.oPgFrm.Page1.oPag.oTOTAVE_1_57.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_58.visible=!this.oPgFrm.Page1.oPag.oStr_1_58.mHide()
    this.oPgFrm.Page1.oPag.oSALDOD_1_59.visible=!this.oPgFrm.Page1.oPag.oSALDOD_1_59.mHide()
    this.oPgFrm.Page1.oPag.oSALDOA_1_60.visible=!this.oPgFrm.Page1.oPag.oSALDOA_1_60.mHide()
    this.oPgFrm.Page1.oPag.oFLAGTOT_1_61.visible=!this.oPgFrm.Page1.oPag.oFLAGTOT_1_61.mHide()
    this.oPgFrm.Page1.oPag.oStr_1_65.visible=!this.oPgFrm.Page1.oPag.oStr_1_65.mHide()
    DoDefault()
  return

  * --- NotifyEvent
  function NotifyEvent(cEvent)
    local bRefresh,i_oldevt
    bRefresh=.f.
    i_oldevt=this.currentEvent
    this.currentEvent=cEvent
    * --- Area Manuale = Notify Event Init
    * --- Fine Area Manuale
    with this
      .oPgFrm.Page1.oPag.ZoomScad.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_37.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_55.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_62.Event(cEvent)
      .oPgFrm.Page1.oPag.oObj_1_63.Event(cEvent)
    endwith
    * --- Area Manuale = Notify Event End
    * --- Fine Area Manuale
    DoDefault(cEvent)
    if bRefresh
      this.SetControlsValue()
    endif
    this.currentEvent=i_oldevt
  return

  * --- Link procedure for entity name=CODCON
  func Link_1_8(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.CONTI_IDX,3]
    i_lTable = "CONTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2], .t., this.CONTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODCON) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_BZC',True,'CONTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" ANCODICE like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

          i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by ANTIPCON,ANCODICE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'ANTIPCON',this.w_TIPCON;
                     ,'ANCODICE',trim(this.w_CODCON))
          select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by ANTIPCON,ANCODICE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODCON)==trim(_Link_.ANCODICE) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStrODBC(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);

            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" ANDESCRI like "+cp_ToStr(trim(this.w_CODCON)+"%");
                   +" and ANTIPCON="+cp_ToStr(this.w_TIPCON);

            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_CODCON) and !this.bDontReportError
            deferred_cp_zoom('CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(oSource.parent,'oCODCON_1_8'),i_cWhere,'GSAR_BZC',"Clienti/fornitori/conti",'GSTE_KMS.CONTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
        if this.w_TIPCON<>oSource.xKey(1);

          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where 1=2","_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where 1=2 into cursor _Link_
            i_reccount = _tally
          endif
          do cp_ErrorMsg with thisform.msgFmt("Valore inesistente, non gestito a partite oppure obsoleto")
        else
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                     +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(oSource.xKey(2));
                     +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',oSource.xKey(1);
                       ,'ANCODICE',oSource.xKey(2))
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODCON)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN";
                   +" from "+i_cTable+" "+i_lTable+" where ANCODICE="+cp_ToStrODBC(this.w_CODCON);
                   +" and ANTIPCON="+cp_ToStrODBC(this.w_TIPCON);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'ANTIPCON',this.w_TIPCON;
                       ,'ANCODICE',this.w_CODCON)
            select ANTIPCON,ANCODICE,ANDESCRI,ANDTOBSO,ANPARTSN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODCON = NVL(_Link_.ANCODICE,space(15))
      this.w_DESCON = NVL(_Link_.ANDESCRI,space(40))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.ANDTOBSO),ctod("  /  /  "))
      this.w_PARTSN = NVL(_Link_.ANPARTSN,space(1))
    else
      if i_cCtrl<>'Load'
        this.w_CODCON = space(15)
      endif
      this.w_DESCON = space(40)
      this.w_DATOBSO = ctod("  /  /  ")
      this.w_PARTSN = space(1)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) And .w_PARTSN='S'
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Valore inesistente, non gestito a partite oppure obsoleto")
        endif
        this.w_CODCON = space(15)
        this.w_DESCON = space(40)
        this.w_DATOBSO = ctod("  /  /  ")
        this.w_PARTSN = space(1)
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.CONTI_IDX,2])+'\'+cp_ToStr(_Link_.ANTIPCON,1)+'\'+cp_ToStr(_Link_.ANCODICE,1)
      cp_ShowWarn(i_cKey,this.CONTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODCON Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BANAPP
  func Link_2_11(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.BAN_CHE_IDX,3]
    i_lTable = "BAN_CHE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2], .t., this.BAN_CHE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BANAPP) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_ABA',True,'BAN_CHE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_BANAPP)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_BANAPP))
          select BACODBAN,BADESBAN;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BANAPP)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESBAN like "+cp_ToStrODBC(trim(this.w_BANAPP)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESBAN like "+cp_ToStr(trim(this.w_BANAPP)+"%");

            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_BANAPP) and !this.bDontReportError
            deferred_cp_zoom('BAN_CHE','*','BACODBAN',cp_AbsName(oSource.parent,'oBANAPP_2_11'),i_cWhere,'GSAR_ABA',"Elenco banche",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BANAPP)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESBAN";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_BANAPP);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_BANAPP)
            select BACODBAN,BADESBAN;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BANAPP = NVL(_Link_.BACODBAN,space(10))
      this.w_DESBAN = NVL(_Link_.BADESBAN,space(50))
    else
      if i_cCtrl<>'Load'
        this.w_BANAPP = space(10)
      endif
      this.w_DESBAN = space(50)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.BAN_CHE_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.BAN_CHE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BANAPP Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=BANNOS
  func Link_2_13(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.COC_MAST_IDX,3]
    i_lTable = "COC_MAST"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2], .t., this.COC_MAST_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_BANNOS) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSTE_ACB',True,'COC_MAST')
        if i_nConn<>0
          i_cWhere = i_cFlt+" BACODBAN like "+cp_ToStrODBC(trim(this.w_BANNOS)+"%");

          i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by BACODBAN","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'BACODBAN',trim(this.w_BANNOS))
          select BACODBAN,BADESCRI,BADTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by BACODBAN into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_BANNOS)==trim(_Link_.BACODBAN) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStrODBC(trim(this.w_BANNOS)+"%");

            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" BADESCRI like "+cp_ToStr(trim(this.w_BANNOS)+"%");

            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_BANNOS) and !this.bDontReportError
            deferred_cp_zoom('COC_MAST','*','BACODBAN',cp_AbsName(oSource.parent,'oBANNOS_2_13'),i_cWhere,'GSTE_ACB',"Conti banca",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',oSource.xKey(1))
            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_BANNOS)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select BACODBAN,BADESCRI,BADTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where BACODBAN="+cp_ToStrODBC(this.w_BANNOS);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'BACODBAN',this.w_BANNOS)
            select BACODBAN,BADESCRI,BADTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_BANNOS = NVL(_Link_.BACODBAN,space(15))
      this.w_DESNOS = NVL(_Link_.BADESCRI,space(35))
      this.w_DATOBSO = NVL(cp_ToDate(_Link_.BADTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_BANNOS = space(15)
      endif
      this.w_DESNOS = space(35)
      this.w_DATOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Load'
      with this
        i_bRes=(EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO)
      endwith
      if not(i_bRes)
        if i_cCtrl='Drop'
          do cp_ErrorMsg with thisform.msgFmt("Conto inesistente oppure obsoleto")
        endif
        this.w_BANNOS = space(15)
        this.w_DESNOS = space(35)
        this.w_DATOBSO = ctod("  /  /  ")
      endif
    endif
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.COC_MAST_IDX,2])+'\'+cp_ToStr(_Link_.BACODBAN,1)
      cp_ShowWarn(i_cKey,this.COC_MAST_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_BANNOS Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=VALUTA
  func Link_2_21(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.VALUTE_IDX,3]
    i_lTable = "VALUTE"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2], .t., this.VALUTE_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_VALUTA) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AVL',True,'VALUTE')
        if i_nConn<>0
          i_cWhere = i_cFlt+" VACODVAL like "+cp_ToStrODBC(trim(this.w_VALUTA)+"%");

          i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by VACODVAL","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'VACODVAL',trim(this.w_VALUTA))
          select VACODVAL,VADESVAL,VASIMVAL;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by VACODVAL into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_VALUTA)==trim(_Link_.VACODVAL) 
          i_reccount=1
        endif
        if i_reccount=0
          if i_nConn<>0
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStrODBC(trim(this.w_VALUTA)+"%");

            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere,"_Link_",2)
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = i_cFlt+" VADESVAL like "+cp_ToStr(trim(this.w_VALUTA)+"%");

            select VACODVAL,VADESVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere into cursor _Link_
            i_reccount = _tally
          endif
        endif
        if i_reccount>1
          if !empty(this.w_VALUTA) and !this.bDontReportError
            deferred_cp_zoom('VALUTE','*','VACODVAL',cp_AbsName(oSource.parent,'oVALUTA_2_21'),i_cWhere,'GSAR_AVL',"Valute",'',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL";
                     +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',oSource.xKey(1))
            select VACODVAL,VADESVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_VALUTA)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select VACODVAL,VADESVAL,VASIMVAL";
                   +" from "+i_cTable+" "+i_lTable+" where VACODVAL="+cp_ToStrODBC(this.w_VALUTA);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'VACODVAL',this.w_VALUTA)
            select VACODVAL,VADESVAL,VASIMVAL;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_VALUTA = NVL(_Link_.VACODVAL,space(3))
      this.w_DESVAL = NVL(_Link_.VADESVAL,space(35))
      this.w_SIMVAL = NVL(_Link_.VASIMVAL,space(5))
    else
      if i_cCtrl<>'Load'
        this.w_VALUTA = space(3)
      endif
      this.w_DESVAL = space(35)
      this.w_SIMVAL = space(5)
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.VALUTE_IDX,2])+'\'+cp_ToStr(_Link_.VACODVAL,1)
      cp_ShowWarn(i_cKey,this.VALUTE_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_VALUTA Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  * --- Link procedure for entity name=CODAGE
  func Link_2_24(i_cCtrl,oSource)
    local i_nArea, i_bRes, i_cWhere, i_nConn, i_cTable, i_lTable, i_cKey, i_reccount, i_ret,i_bEmpty,i_cFlt
    i_nConn = i_TableProp[this.AGENTI_IDX,3]
    i_lTable = "AGENTI"
    if this.cFunction='Load'
      i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2], .t., this.AGENTI_IDX)
    else
    i_cTable = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])
    endif
    i_nArea = select()
    i_bEmpty = .f.
    if used("_Link_")
      select _Link_
      use
    endif
    do case
      case empty(this.w_CODAGE) and i_cCtrl<>"Drop"
        i_bEmpty = .t.
        i_reccount=0
      case i_cCtrl='Part'
        i_cFlt=cp_QueryEntityFilter('GSAR_AGE',True,'AGENTI')
        if i_nConn<>0
          i_cWhere = i_cFlt+" AGCODAGE like "+cp_ToStrODBC(trim(this.w_CODAGE)+"%");

          i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                  +" from "+i_cTable+" "+i_lTable+" where"+i_cWhere+" order by AGCODAGE","_Link_",2)
          i_reccount = iif(i_ret=-1,0,reccount())
        else
          i_cWhere = i_cFlt+cp_PKFox(i_cTable;
                     ,'AGCODAGE',trim(this.w_CODAGE))
          select AGCODAGE,AGDESAGE,AGDTOBSO;
              from (i_cTable) as (i_lTable) where &i_cWhere. order by AGCODAGE into cursor _Link_
          i_reccount = _tally
        endif
        if trim(this.w_CODAGE)==trim(_Link_.AGCODAGE) 
          i_reccount=1
        endif
        if i_reccount>1
          if !empty(this.w_CODAGE) and !this.bDontReportError
            deferred_cp_zoom('AGENTI','*','AGCODAGE',cp_AbsName(oSource.parent,'oCODAGE_2_24'),i_cWhere,'GSAR_AGE',"Elenco agenti",'AGE1ZOOM.AGENTI_VZM',this)
          endif
          this.bDontReportError = .t.
          i_reccount = 0

        endif
      case i_cCtrl='Drop'
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                     +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(oSource.xKey(1));
                     ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',oSource.xKey(1))
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
      case i_cCtrl='Full' or i_cCtrl='Load' or i_cCtrl='Extend'
        if .not. empty(this.w_CODAGE)
          if i_nConn<>0
            i_ret=cp_SQL(i_nConn,"select AGCODAGE,AGDESAGE,AGDTOBSO";
                   +" from "+i_cTable+" "+i_lTable+" where AGCODAGE="+cp_ToStrODBC(this.w_CODAGE);
                   ,"_Link_")
            i_reccount = iif(i_ret=-1,0,reccount())
          else
            i_cWhere = cp_PKFox(i_cTable;
                       ,'AGCODAGE',this.w_CODAGE)
            select AGCODAGE,AGDESAGE,AGDTOBSO;
                   from (i_cTable) as (i_lTable) where &i_cWhere. into cursor _Link_
            i_reccount = _tally
          endif
        else
          i_reccount = 0
        endif
    endcase
    if i_reccount>0 and used("_Link_")
      this.w_CODAGE = NVL(_Link_.AGCODAGE,space(5))
      this.w_DESAGE = NVL(_Link_.AGDESAGE,space(35))
      this.w_AGEOBSO = NVL(cp_ToDate(_Link_.AGDTOBSO),ctod("  /  /  "))
    else
      if i_cCtrl<>'Load'
        this.w_CODAGE = space(5)
      endif
      this.w_DESAGE = space(35)
      this.w_AGEOBSO = ctod("  /  /  ")
    endif
    i_bRes=i_reccount=1 
    if i_bRes and i_cCtrl<>'Full' and i_cCtrl<>'Load' and used("_Link_")
      i_cKey = cp_SetAzi(i_TableProp[this.AGENTI_IDX,2])+'\'+cp_ToStr(_Link_.AGCODAGE,1)
      cp_ShowWarn(i_cKey,this.AGENTI_IDX)
    endif
    if i_cCtrl='Drop'
      this.TrsFromWork()
      this.NotifyEvent('w_CODAGE Changed')
    endif
    select (i_nArea)
    return(i_bRes or i_bEmpty)
  endfunc

  function SetControlsValue()
    if not(this.oPgFrm.Page1.oPag.oSCAINI_1_3.value==this.w_SCAINI)
      this.oPgFrm.Page1.oPag.oSCAINI_1_3.value=this.w_SCAINI
    endif
    if not(this.oPgFrm.Page1.oPag.oSCAFIN_1_4.value==this.w_SCAFIN)
      this.oPgFrm.Page1.oPag.oSCAFIN_1_4.value=this.w_SCAFIN
    endif
    if not(this.oPgFrm.Page1.oPag.oTIPCON_1_7.RadioValue()==this.w_TIPCON)
      this.oPgFrm.Page1.oPag.oTIPCON_1_7.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oCODCON_1_8.value==this.w_CODCON)
      this.oPgFrm.Page1.oPag.oCODCON_1_8.value=this.w_CODCON
    endif
    if not(this.oPgFrm.Page1.oPag.oDESCON_1_9.value==this.w_DESCON)
      this.oPgFrm.Page1.oPag.oDESCON_1_9.value=this.w_DESCON
    endif
    if not(this.oPgFrm.Page1.oPag.oFLSALD_1_10.RadioValue()==this.w_FLSALD)
      this.oPgFrm.Page1.oPag.oFLSALD_1_10.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oFLPART_1_11.RadioValue()==this.w_FLPART)
      this.oPgFrm.Page1.oPag.oFLPART_1_11.SetRadio()
    endif
    if not(this.oPgFrm.Page1.oPag.oDATPAR_1_12.value==this.w_DATPAR)
      this.oPgFrm.Page1.oPag.oDATPAR_1_12.value=this.w_DATPAR
    endif
    if not(this.oPgFrm.Page2.oPag.oPAGRD_2_1.RadioValue()==this.w_PAGRD)
      this.oPgFrm.Page2.oPag.oPAGRD_2_1.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPAGBO_2_2.RadioValue()==this.w_PAGBO)
      this.oPgFrm.Page2.oPag.oPAGBO_2_2.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPAGRB_2_3.RadioValue()==this.w_PAGRB)
      this.oPgFrm.Page2.oPag.oPAGRB_2_3.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPAGRI_2_4.RadioValue()==this.w_PAGRI)
      this.oPgFrm.Page2.oPag.oPAGRI_2_4.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPAGCA_2_5.RadioValue()==this.w_PAGCA)
      this.oPgFrm.Page2.oPag.oPAGCA_2_5.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPAGMA_2_6.RadioValue()==this.w_PAGMA)
      this.oPgFrm.Page2.oPag.oPAGMA_2_6.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oPAGNO_2_7.RadioValue()==this.w_PAGNO)
      this.oPgFrm.Page2.oPag.oPAGNO_2_7.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oREGINI_2_8.value==this.w_REGINI)
      this.oPgFrm.Page2.oPag.oREGINI_2_8.value=this.w_REGINI
    endif
    if not(this.oPgFrm.Page2.oPag.oREGFIN_2_9.value==this.w_REGFIN)
      this.oPgFrm.Page2.oPag.oREGFIN_2_9.value=this.w_REGFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oNUMPAR_2_10.value==this.w_NUMPAR)
      this.oPgFrm.Page2.oPag.oNUMPAR_2_10.value=this.w_NUMPAR
    endif
    if not(this.oPgFrm.Page2.oPag.oBANAPP_2_11.value==this.w_BANAPP)
      this.oPgFrm.Page2.oPag.oBANAPP_2_11.value=this.w_BANAPP
    endif
    if not(this.oPgFrm.Page2.oPag.oDESBAN_2_12.value==this.w_DESBAN)
      this.oPgFrm.Page2.oPag.oDESBAN_2_12.value=this.w_DESBAN
    endif
    if not(this.oPgFrm.Page2.oPag.oBANNOS_2_13.value==this.w_BANNOS)
      this.oPgFrm.Page2.oPag.oBANNOS_2_13.value=this.w_BANNOS
    endif
    if not(this.oPgFrm.Page2.oPag.oDESNOS_2_14.value==this.w_DESNOS)
      this.oPgFrm.Page2.oPag.oDESNOS_2_14.value=this.w_DESNOS
    endif
    if not(this.oPgFrm.Page2.oPag.oNDOINI_2_15.value==this.w_NDOINI)
      this.oPgFrm.Page2.oPag.oNDOINI_2_15.value=this.w_NDOINI
    endif
    if not(this.oPgFrm.Page2.oPag.oADOINI_2_16.value==this.w_ADOINI)
      this.oPgFrm.Page2.oPag.oADOINI_2_16.value=this.w_ADOINI
    endif
    if not(this.oPgFrm.Page2.oPag.oDDOINI_2_17.value==this.w_DDOINI)
      this.oPgFrm.Page2.oPag.oDDOINI_2_17.value=this.w_DDOINI
    endif
    if not(this.oPgFrm.Page2.oPag.oNDOFIN_2_18.value==this.w_NDOFIN)
      this.oPgFrm.Page2.oPag.oNDOFIN_2_18.value=this.w_NDOFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oADOFIN_2_19.value==this.w_ADOFIN)
      this.oPgFrm.Page2.oPag.oADOFIN_2_19.value=this.w_ADOFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oDDOFIN_2_20.value==this.w_DDOFIN)
      this.oPgFrm.Page2.oPag.oDDOFIN_2_20.value=this.w_DDOFIN
    endif
    if not(this.oPgFrm.Page2.oPag.oVALUTA_2_21.value==this.w_VALUTA)
      this.oPgFrm.Page2.oPag.oVALUTA_2_21.value=this.w_VALUTA
    endif
    if not(this.oPgFrm.Page2.oPag.oDESVAL_2_22.value==this.w_DESVAL)
      this.oPgFrm.Page2.oPag.oDESVAL_2_22.value=this.w_DESVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oFLSOSP_2_23.RadioValue()==this.w_FLSOSP)
      this.oPgFrm.Page2.oPag.oFLSOSP_2_23.SetRadio()
    endif
    if not(this.oPgFrm.Page2.oPag.oCODAGE_2_24.value==this.w_CODAGE)
      this.oPgFrm.Page2.oPag.oCODAGE_2_24.value=this.w_CODAGE
    endif
    if not(this.oPgFrm.Page2.oPag.oSIMVAL_2_39.value==this.w_SIMVAL)
      this.oPgFrm.Page2.oPag.oSIMVAL_2_39.value=this.w_SIMVAL
    endif
    if not(this.oPgFrm.Page2.oPag.oDESAGE_2_40.value==this.w_DESAGE)
      this.oPgFrm.Page2.oPag.oDESAGE_2_40.value=this.w_DESAGE
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTDAR_1_56.value==this.w_TOTDAR)
      this.oPgFrm.Page1.oPag.oTOTDAR_1_56.value=this.w_TOTDAR
    endif
    if not(this.oPgFrm.Page1.oPag.oTOTAVE_1_57.value==this.w_TOTAVE)
      this.oPgFrm.Page1.oPag.oTOTAVE_1_57.value=this.w_TOTAVE
    endif
    if not(this.oPgFrm.Page1.oPag.oSALDOD_1_59.value==this.w_SALDOD)
      this.oPgFrm.Page1.oPag.oSALDOD_1_59.value=this.w_SALDOD
    endif
    if not(this.oPgFrm.Page1.oPag.oSALDOA_1_60.value==this.w_SALDOA)
      this.oPgFrm.Page1.oPag.oSALDOA_1_60.value=this.w_SALDOA
    endif
    if not(this.oPgFrm.Page1.oPag.oFLAGTOT_1_61.RadioValue()==this.w_FLAGTOT)
      this.oPgFrm.Page1.oPag.oFLAGTOT_1_61.SetRadio()
    endif
    cp_SetControlsValueExtFlds(this,'')
  endfunc


  * --- CheckForm
  func CheckForm()
    local i_bnoObbl, i_bRes, i_bnoChk, i_cErrorMsg
    i_cErrorMsg = MSG_VALUE_NOT_CORRECT_QM
    i_bRes = .t.
    i_bnoChk = .t.
    i_bnoObbl = .t.
    with this
      if .bUpdated
        do case
          case   not(((.w_scaini<=.w_scafin) or (empty(.w_scafin))) AND (G_DATMIN<=.w_scaini))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCAINI_1_3.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La scadenza iniziale � maggiore della scadenza finale o � minore della data minima (01-01-1980)")
          case   not(((.w_scaini<=.w_scafin) or (empty(.w_scaini))) AND (G_DATMIN<=.w_scafin))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oSCAFIN_1_4.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La scadenza iniziale � maggiore della scadenza finale o quest'ultima � minore della data minima (01-01-1980)")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO) And .w_PARTSN='S')  and not(empty(.w_CODCON))
            .oPgFrm.ActivePage = 1
            .oPgFrm.Page1.oPag.oCODCON_1_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Valore inesistente, non gestito a partite oppure obsoleto")
          case   not((.w_REGINI<=.w_REGFIN) or (empty(.w_REGFIN)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oREGINI_2_8.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data registrazione iniziale � maggiore della scadenza finale")
          case   not((.w_regini<=.w_regfin) or (empty(.w_regini)))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oREGFIN_2_9.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("La data registrazione iniziale � maggiore della scadenza finale")
          case   not((EMPTY(.w_DATOBSO) OR .w_OBTEST<.w_DATOBSO))  and not(empty(.w_BANNOS))
            .oPgFrm.ActivePage = 2
            .oPgFrm.Page2.oPag.oBANNOS_2_13.SetFocus()
            i_bnoChk = .f.
            i_bRes = .f.
            i_cErrorMsg = thisform.msgFmt("Conto inesistente oppure obsoleto")
        endcase
      endif
      * --- Area Manuale = Check Form
      * --- gste_kms
      * --- Disabilita tasto F10
      if !cp_yesno('Confermi Uscita ?')
         i_bRes=.f.
      else
         i_bRes=.t.
      endif
      * --- Fine Area Manuale
      if not(i_bnoObbl)
        cp_ErrorMsg(MSG_FIELD_CANNOT_BE_NULL_QM)
      else
        if not(i_bnoChk)
          cp_ErrorMsg(i_cErrorMsg)
        endif
      endif
    endwith
    if i_bRes
      i_bRes=DoDefault()
    endif
    return(i_bRes)
  endfunc

  * --- SaveDependsOn
  proc SaveDependsOn()
    this.o_SCAINI = this.w_SCAINI
    this.o_TIPCON = this.w_TIPCON
    this.o_FLSALD = this.w_FLSALD
    return

enddefine

* --- Define pages as container
define class tgste_kmsPag1 as StdContainer
  Width  = 787
  height = 481
  stdWidth  = 787
  stdheight = 481
  resizeXpos=692
  resizeYpos=241
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oSCAINI_1_3 as StdField with uid="ZWBQZGVIZH",rtseq=3,rtrep=.f.,;
    cFormVar = "w_SCAINI", cQueryName = "SCAINI",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La scadenza iniziale � maggiore della scadenza finale o � minore della data minima (01-01-1980)",;
    ToolTipText = "Data iniziale scadenza da ricercare",;
    HelpContextID = 230480858,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=104, Top=8

  func oSCAINI_1_3.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_scaini<=.w_scafin) or (empty(.w_scafin))) AND (G_DATMIN<=.w_scaini))
    endwith
    return bRes
  endfunc

  add object oSCAFIN_1_4 as StdField with uid="KEJOHVUNZR",rtseq=4,rtrep=.f.,;
    cFormVar = "w_SCAFIN", cQueryName = "SCAFIN",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La scadenza iniziale � maggiore della scadenza finale o quest'ultima � minore della data minima (01-01-1980)",;
    ToolTipText = "Data finale scadenza da ricercare",;
    HelpContextID = 152034266,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=104, Top=32

  func oSCAFIN_1_4.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = (((.w_scaini<=.w_scafin) or (empty(.w_scaini))) AND (G_DATMIN<=.w_scafin))
    endwith
    return bRes
  endfunc


  add object oTIPCON_1_7 as StdCombo with uid="FSJCLTVKNQ",rtseq=7,rtrep=.f.,left=253,top=8,width=92,height=21;
    , ToolTipText = "Tipo partite/scadenze da selezionare, clienti, fornitori, conti generici";
    , HelpContextID = 145876426;
    , cFormVar="w_TIPCON",RowSource=""+"Clienti,"+"Fornitori,"+"Conti", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oTIPCON_1_7.RadioValue()
    return(iif(this.value =1,'C',;
    iif(this.value =2,'F',;
    iif(this.value =3,'G',;
    space(1)))))
  endfunc
  func oTIPCON_1_7.GetRadio()
    this.Parent.oContained.w_TIPCON = this.RadioValue()
    return .t.
  endfunc

  func oTIPCON_1_7.SetRadio()
    this.Parent.oContained.w_TIPCON=trim(this.Parent.oContained.w_TIPCON)
    this.value = ;
      iif(this.Parent.oContained.w_TIPCON=='C',1,;
      iif(this.Parent.oContained.w_TIPCON=='F',2,;
      iif(this.Parent.oContained.w_TIPCON=='G',3,;
      0)))
  endfunc

  func oTIPCON_1_7.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_VISUAL)
    endwith
   endif
  endfunc

  func oTIPCON_1_7.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      if .not. empty(.w_CODCON)
        bRes2=.link_1_8('Full')
      endif
    endwith
    return bRes
  endfunc

  add object oCODCON_1_8 as StdField with uid="GOUKRNSZUT",rtseq=8,rtrep=.f.,;
    cFormVar = "w_CODCON", cQueryName = "CODCON",;
    bObbl = .f. , nPag = 1, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Valore inesistente, non gestito a partite oppure obsoleto",;
    ToolTipText = "Cliente/fornitore/conto di selezione scadenze",;
    HelpContextID = 145924314,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=357, Top=8, cSayPict="p_CCF", cGetPict="p_CCF", InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="CONTI", cZoomOnZoom="GSAR_BZC", oKey_1_1="ANTIPCON", oKey_1_2="this.w_TIPCON", oKey_2_1="ANCODICE", oKey_2_2="this.w_CODCON"

  func oCODCON_1_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_1_8('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODCON_1_8.ecpDrop(oSource)
    this.Parent.oContained.link_1_8('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODCON_1_8.mZoom
    private i_cWhere
    i_cWhere = ""
    if i_TableProp[this.parent.oContained.CONTI_idx,3]<>0
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStrODBC(this.Parent.oContained.w_TIPCON)
    else
      i_cWhere = i_cWhere+iif(empty(i_cWhere),''," and ")+"ANTIPCON="+cp_ToStr(this.Parent.oContained.w_TIPCON)
    endif
    do cp_zoom with 'CONTI','*','ANTIPCON,ANCODICE',cp_AbsName(this.parent,'oCODCON_1_8'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_BZC',"Clienti/fornitori/conti",'GSTE_KMS.CONTI_VZM',this.parent.oContained
  endproc
  proc oCODCON_1_8.mZoomOnZoom
    local i_obj
    i_obj=GSAR_BZC()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
    i_obj.ANTIPCON=w_TIPCON
     i_obj.w_ANCODICE=this.parent.oContained.w_CODCON
     i_obj.ecpSave()
  endproc

  add object oDESCON_1_9 as StdField with uid="BDFUROQGIK",rtseq=9,rtrep=.f.,;
    cFormVar = "w_DESCON", cQueryName = "DESCON",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=space(40), bMultilanguage =  .f.,;
    HelpContextID = 145865418,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=500, Top=8, InputMask=replicate('X',40)


  add object oFLSALD_1_10 as StdCombo with uid="RZKVWQRZLP",rtseq=10,rtrep=.f.,left=373,top=32,width=119,height=21;
    , ToolTipText = "Ricerca le scadenze saldate, non ancora saldate o tutte";
    , HelpContextID = 48477098;
    , cFormVar="w_FLSALD",RowSource=""+"Tutte,"+"Non saldate,"+"Saldate,"+"Solo parte aperta", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLSALD_1_10.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    iif(this.value =3,'S',;
    iif(this.value =4,'R',;
    space(1))))))
  endfunc
  func oFLSALD_1_10.GetRadio()
    this.Parent.oContained.w_FLSALD = this.RadioValue()
    return .t.
  endfunc

  func oFLSALD_1_10.SetRadio()
    this.Parent.oContained.w_FLSALD=trim(this.Parent.oContained.w_FLSALD)
    this.value = ;
      iif(this.Parent.oContained.w_FLSALD=='T',1,;
      iif(this.Parent.oContained.w_FLSALD=='N',2,;
      iif(this.Parent.oContained.w_FLSALD=='S',3,;
      iif(this.Parent.oContained.w_FLSALD=='R',4,;
      0))))
  endfunc

  func oFLSALD_1_10.mHide()
    with this.Parent.oContained
      return (! .w_VISUAL)
    endwith
  endfunc


  add object oFLPART_1_11 as StdCombo with uid="PRTIZEDRCX",rtseq=11,rtrep=.f.,left=579,top=32,width=119,height=21;
    , HelpContextID = 42197930;
    , cFormVar="w_FLPART",RowSource=""+"Tutte,"+"Saldo,"+"Creazione,"+"Acconto", bObbl = .f. , nPag = 1;
  , bGlobalFont=.t.


  func oFLPART_1_11.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'S',;
    iif(this.value =3,'C',;
    iif(this.value =4,'A',;
    space(1))))))
  endfunc
  func oFLPART_1_11.GetRadio()
    this.Parent.oContained.w_FLPART = this.RadioValue()
    return .t.
  endfunc

  func oFLPART_1_11.SetRadio()
    this.Parent.oContained.w_FLPART=trim(this.Parent.oContained.w_FLPART)
    this.value = ;
      iif(this.Parent.oContained.w_FLPART=='T',1,;
      iif(this.Parent.oContained.w_FLPART=='S',2,;
      iif(this.Parent.oContained.w_FLPART=='C',3,;
      iif(this.Parent.oContained.w_FLPART=='A',4,;
      0))))
  endfunc

  func oFLPART_1_11.mHide()
    with this.Parent.oContained
      return (.w_FLSALD<>'R' or ! .w_VISUAL)
    endwith
  endfunc

  add object oDATPAR_1_12 as StdField with uid="WCZJEEQMBZ",rtseq=12,rtrep=.f.,;
    cFormVar = "w_DATPAR", cQueryName = "DATPAR",;
    bObbl = .f. , nPag = 1, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Filtra le partite aperte alla data",;
    HelpContextID = 92581578,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=579, Top=60

  func oDATPAR_1_12.mHide()
    with this.Parent.oContained
      return (.w_FLSALD<>'R' OR ! .w_VISUAL)
    endwith
  endfunc


  add object oBtn_1_17 as StdButton with uid="OSKJUCDDJL",left=724, top=34, width=48,height=45,;
    CpPicture="BMP\REQUERY.BMP", caption="", nPag=1;
    , ToolTipText = "Riesegue la query";
    , HelpContextID = 245459478;
    , Caption='\<Ricerca';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_17.Click()
      with this.Parent.oContained
        GSTE_BIS(this.Parent.oContained,"BTN")
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oBtn_1_18 as StdButton with uid="SWNOKMTEFZ",left=5, top=435, width=48,height=45,;
    CpPicture="bmp\dettagli.bmp", caption="", nPag=1;
    , ToolTipText = "Premere per accedere alla manutenzione delle partite";
    , HelpContextID = 241076833;
    , Caption='\<Dettagli';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_18.Click()
      with this.Parent.oContained
        GSTE_BZP(this.Parent.oContained,.w_SERIALE, .w_TIPMOV,.w_ROWNUM, 0,3,0)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_18.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc

  func oBtn_1_18.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (! .w_VISUAL)
     endwith
    endif
  endfunc


  add object ZoomScad as cp_szoombox with uid="TIHUYNVHMX",left=5, top=84, width=785,height=301,;
    caption='Object',;
   bGlobalFont=.t.,;
    cTable="PAR_TITE",cZoomFile="GSTE_KMS",bOptions=.f.,bQueryOnLoad=.f.,bQueryOnDblClick=.f.,bAdvOptions=.t.,bReadOnly=.t.,cMenuFile="",cZoomOnZoom="GSZM_BZP",bNoMenuHeaderProperty=.t.,;
    cEvent = "Ricerca",;
    nPag=1;
    , HelpContextID = 246534886


  add object oBtn_1_20 as StdButton with uid="GBGZRKFEFH",left=55, top=435, width=48,height=45,;
    CpPicture="BMP\Modifica.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per modificare i dati relativi alla singola scadenza selezionata";
    , HelpContextID = 65372199;
    , Caption='\<Modifica';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_20.Click()
      with this.Parent.oContained
        GSTE_BZP(this.Parent.oContained,.w_SERIALE, .w_TIPMOV,.w_ROWNUM, .w_TOTIMP,1,0)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_20.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc

  func oBtn_1_20.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (.w_FLSALD<>'R' OR ! .w_VISUAL)
     endwith
    endif
  endfunc


  add object oBtn_1_21 as StdButton with uid="MAGZWXRXKT",left=105, top=435, width=48,height=45,;
    CpPicture="BMP\SEPARA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per dividere le scadenze/partite";
    , HelpContextID = 63346998;
    , Caption='D\<ividi';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_21.Click()
      with this.Parent.oContained
        GSTE_BZP(this.Parent.oContained,.w_SERIALE, .w_TIPMOV, .w_ROWNUM,0,2,0)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_21.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE) And .w_FLSALD='R')
      endwith
    endif
  endfunc

  func oBtn_1_21.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (! .w_VISUAL)
     endwith
    endif
  endfunc


  add object oBtn_1_22 as StdButton with uid="MPAIYXZMXP",left=155, top=435, width=48,height=45,;
    CpPicture="BMP\abbscadenze.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accorpare scadenze (filtri pre impostati in base alla scadenza sulla quale si � posizionati)";
    , HelpContextID = 250050490;
    , Caption='Acc. \<Scad.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_22.Click()
      do GSTE_KUS with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_22.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE) And .w_FLSALD='R')
      endwith
    endif
  endfunc

  func oBtn_1_22.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (! .w_VISUAL)
     endwith
    endif
  endfunc


  add object oBtn_1_23 as StdButton with uid="TLHJSBKGMU",left=205, top=435, width=48,height=45,;
    CpPicture="BMP\abbpartite.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per accorpare le partite selezionate";
    , HelpContextID = 206646794;
    , Caption='Acc. \<Part.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_23.Click()
      do GSTE_KUP with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_23.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc

  func oBtn_1_23.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (! .w_VISUAL)
     endwith
    endif
  endfunc


  add object oBtn_1_24 as StdButton with uid="MSTLUBLVZR",left=255, top=435, width=48,height=45,;
    CpPicture="bmp\ORIGINE.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza l'origine della scadenza (primanota, scadenze diverse)";
    , HelpContextID = 152294118;
    , Caption='Ori\<gine';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_24.Click()
      with this.Parent.oContained
        GSTE_BZP(this.Parent.oContained,.w_SERIALE, .w_TIPMOV, .w_ROWNUM,0,0,0)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_24.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE) AND .w_ROWORD<>-2)
      endwith
    endif
  endfunc


  add object oBtn_1_25 as StdButton with uid="HDMUOTDQGW",left=305, top=435, width=48,height=45,;
    CpPicture="BMP\requery.bmp", caption="", nPag=1;
    , ToolTipText = "Visualizza l'origine della scadenza (primanota, scadenze diverse)";
    , HelpContextID = 153184663;
    , Caption='Dis\<tinta';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_25.Click()
      with this.Parent.oContained
        GSTE_BZP(this.Parent.oContained,.w_SERIALE, -2, .w_ROWNUM,0,0,.w_ROWORD)
      endwith
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_25.mCond()
    if !isnull(this.Parent.oContained)
      with this.Parent.oContained
        return (NOT EMPTY(.w_SERIALE))
      endwith
    endif
  endfunc

  func oBtn_1_25.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (EMPTY(.w_SERDIS) AND .w_ROWORD<>-2 OR ! .w_VISUAL)
     endwith
    endif
  endfunc


  add object oBtn_1_26 as StdButton with uid="SOTUKPQDDO",left=355, top=435, width=48,height=45,;
    CpPicture="BMP\MODIFICA.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per modificare ns.banca, banca appogio, C/C di tutte le scadenze selezionate.";
    , HelpContextID = 76878935;
    , Caption='Mod.\<Banc.';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_26.Click()
      do GSTE_KMB with this.Parent.oContained
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  func oBtn_1_26.mHide()
  if(vartype(this.Parent.oContained)<>'O')
     return .t.
    else
     with this.Parent.oContained
      return (NOT(NOT EMPTY(.w_SERIALE) And .w_FLSALD='R') OR ! .w_VISUAL)
     endwith
    endif
  endfunc


  add object oBtn_1_31 as StdButton with uid="VRONCUQWAW",left=724, top=435, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=1;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 192580794;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_1_31.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc


  add object oObj_1_37 as cp_runprogram with uid="EJGYZWXGXP",left=9, top=514, width=334,height=21,;
    caption='GSTE_BIS(BTN)',;
   bGlobalFont=.t.,;
    prg="GSTE_BIS('BTN')",;
    cEvent = "LanciaCLI , Riesegui,w_FLSALD Changed",;
    nPag=1;
    , HelpContextID = 255060025


  add object oObj_1_55 as cp_runprogram with uid="UOQSPXJLYU",left=9, top=534, width=334,height=21,;
    caption='GSTE_BZP(w_SERIALE)',;
   bGlobalFont=.t.,;
    prg="GSTE_BZP(w_SERIALE, w_TIPMOV, w_ROWNUM,0,0,0)",;
    cEvent = "w_zoomscad selected",;
    nPag=1;
    , HelpContextID = 50112311

  add object oTOTDAR_1_56 as StdField with uid="SLBJBHJGPQ",rtseq=65,rtrep=.f.,;
    cFormVar = "w_TOTDAR", cQueryName = "TOTDAR",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale dare",;
    HelpContextID = 93364170,;
   bGlobalFont=.t.,;
    Height=21, Width=140, Left=398, Top=387, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oTOTDAR_1_56.mHide()
    with this.Parent.oContained
      return (.w_FLSALD<>'R' OR .w_FLAGTOT='N')
    endwith
  endfunc

  add object oTOTAVE_1_57 as StdField with uid="ATRRCDXXLF",rtseq=66,rtrep=.f.,;
    cFormVar = "w_TOTAVE", cQueryName = "TOTAVE",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Totale avere",;
    HelpContextID = 21209034,;
   bGlobalFont=.t.,;
    Height=21, Width=140, Left=546, Top=387, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oTOTAVE_1_57.mHide()
    with this.Parent.oContained
      return (.w_FLSALD<>'R' OR .w_FLAGTOT='N')
    endwith
  endfunc

  add object oSALDOD_1_59 as StdField with uid="AAOJRHPSYS",rtseq=67,rtrep=.f.,;
    cFormVar = "w_SALDOD", cQueryName = "SALDOD",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo movimenti visualizzati in dare",;
    HelpContextID = 45166042,;
   bGlobalFont=.t.,;
    Height=21, Width=140, Left=398, Top=411, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oSALDOD_1_59.mHide()
    with this.Parent.oContained
      return (.w_TOTDAR-.w_TOTAVE<0 OR .w_FLSALD<>'R' OR .w_FLAGTOT='N')
    endwith
  endfunc

  add object oSALDOA_1_60 as StdField with uid="UOLAWPLNYX",rtseq=68,rtrep=.f.,;
    cFormVar = "w_SALDOA", cQueryName = "SALDOA",enabled=.f.,;
    bObbl = .f. , nPag = 1, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Saldo movimenti visualizzati in avere",;
    HelpContextID = 95497690,;
   bGlobalFont=.t.,;
    Height=21, Width=140, Left=546, Top=411, cSayPict="v_PV(40+VVP)", cGetPict="v_GV(40+VVP)"

  func oSALDOA_1_60.mHide()
    with this.Parent.oContained
      return (.w_TOTDAR-.w_TOTAVE>0 OR .w_FLSALD<>'R' OR .w_FLAGTOT='N')
    endwith
  endfunc

  add object oFLAGTOT_1_61 as StdCheck with uid="HZMGYCPWQF",rtseq=69,rtrep=.f.,left=9, top=389, caption="Totali partite\scadenze",;
    ToolTipText = "Se attivo visualizza totali",;
    HelpContextID = 144780374,;
    cFormVar="w_FLAGTOT", bObbl = .f. , nPag = 1;
   , bGlobalFont=.t.


  func oFLAGTOT_1_61.RadioValue()
    return(iif(this.value =1,'S',;
    'N'))
  endfunc
  func oFLAGTOT_1_61.GetRadio()
    this.Parent.oContained.w_FLAGTOT = this.RadioValue()
    return .t.
  endfunc

  func oFLAGTOT_1_61.SetRadio()
    this.Parent.oContained.w_FLAGTOT=trim(this.Parent.oContained.w_FLAGTOT)
    this.value = ;
      iif(this.Parent.oContained.w_FLAGTOT=='S',1,;
      0)
  endfunc

  func oFLAGTOT_1_61.mHide()
    with this.Parent.oContained
      return (.w_FLSALD<>'R')
    endwith
  endfunc


  add object oObj_1_62 as cp_runprogram with uid="NLNFILTIWF",left=9, top=494, width=334,height=21,;
    caption='GSTE_BIS(TOT)',;
   bGlobalFont=.t.,;
    prg="GSTE_BIS('TOT')",;
    cEvent = "w_FLAGTOT Changed",;
    nPag=1;
    , HelpContextID = 255437369


  add object oObj_1_63 as cp_calclbl with uid="LDPZDEQNAD",left=112, top=411, width=284,height=20,;
    caption='Object',;
   bGlobalFont=.t.,;
    caption="label text",;
    nPag=1;
    , HelpContextID = 246534886

  add object oStr_1_13 as StdString with uid="MGESAHNQBJ",Visible=.t., Left=8, Top=8,;
    Alignment=1, Width=94, Height=15,;
    Caption="Scadenze da:"  ;
  , bGlobalFont=.t.

  add object oStr_1_14 as StdString with uid="JBBQEGAXNK",Visible=.t., Left=18, Top=32,;
    Alignment=1, Width=84, Height=15,;
    Caption="A:"  ;
  , bGlobalFont=.t.

  add object oStr_1_15 as StdString with uid="EDAIOSXGFJ",Visible=.t., Left=97, Top=55,;
    Alignment=0, Width=319, Height=13,;
    Caption="Le scadenze in rosso sono accorpate"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_15.mHide()
    with this.Parent.oContained
      return (! .w_VISUAL)
    endwith
  endfunc

  add object oStr_1_35 as StdString with uid="DHNSFNFDJQ",Visible=.t., Left=242, Top=32,;
    Alignment=1, Width=127, Height=15,;
    Caption="Test saldate:"  ;
  , bGlobalFont=.t.

  func oStr_1_35.mHide()
    with this.Parent.oContained
      return (! .w_VISUAL)
    endwith
  endfunc

  add object oStr_1_36 as StdString with uid="RIVUMPJWCO",Visible=.t., Left=183, Top=8,;
    Alignment=1, Width=67, Height=15,;
    Caption="Tipo:"  ;
  , bGlobalFont=.t.

  func oStr_1_36.mHide()
    with this.Parent.oContained
      return ( ! .w_VISUAL)
    endwith
  endfunc

  add object oStr_1_49 as StdString with uid="HBZSJYGTCN",Visible=.t., Left=497, Top=32,;
    Alignment=1, Width=77, Height=18,;
    Caption="Tipo partite:"  ;
  , bGlobalFont=.t.

  func oStr_1_49.mHide()
    with this.Parent.oContained
      return (.w_FLSALD<>'R' or ! .w_VISUAL)
    endwith
  endfunc

  add object oStr_1_51 as StdString with uid="LZOAPMJTTR",Visible=.t., Left=436, Top=60,;
    Alignment=1, Width=138, Height=18,;
    Caption="Partite aperte alla data:"  ;
  , bGlobalFont=.t.

  func oStr_1_51.mHide()
    with this.Parent.oContained
      return (.w_FLSALD<>'R' OR ! .w_VISUAL)
    endwith
  endfunc

  add object oStr_1_58 as StdString with uid="SIXLQFSAWO",Visible=.t., Left=252, Top=387,;
    Alignment=1, Width=143, Height=15,;
    Caption="Totali:"  ;
    , FontName = "Arial", FontSize = 9, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_58.mHide()
    with this.Parent.oContained
      return (.w_FLSALD<>'R' OR .w_FLAGTOT='N')
    endwith
  endfunc

  add object oStr_1_65 as StdString with uid="YLMCYZKEVR",Visible=.t., Left=97, Top=69,;
    Alignment=0, Width=319, Height=13,;
    Caption="Le scadenze in blu sono partite riaperte con insoluto"  ;
    , FontName = "Arial", FontSize = 8, FontBold = .f., FontItalic=.t., FontUnderline=.f., FontStrikeThru=.f.

  func oStr_1_65.mHide()
    with this.Parent.oContained
      return (! .w_VISUAL)
    endwith
  endfunc
enddefine
define class tgste_kmsPag2 as StdContainer
  Width  = 787
  height = 481
  stdWidth  = 787
  stdheight = 481
  resizeXpos=429
  bGlobalFont=.t.
  FontName      = "Arial"
  FontSize      = 9
  FontBold      = .f.
  FontItalic    = .f.
  FontUnderline = .f.
  FontStrikeThru= .f.

  add object oPAGRD_2_1 as StdCheck with uid="CCURCTTLBD",rtseq=21,rtrep=.f.,left=139, top=23, caption="Rimessa diretta",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti con rimessa diretta",;
    HelpContextID = 122912266,;
    cFormVar="w_PAGRD", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPAGRD_2_1.RadioValue()
    return(iif(this.value =1,'RD',;
    'XX'))
  endfunc
  func oPAGRD_2_1.GetRadio()
    this.Parent.oContained.w_PAGRD = this.RadioValue()
    return .t.
  endfunc

  func oPAGRD_2_1.SetRadio()
    this.Parent.oContained.w_PAGRD=trim(this.Parent.oContained.w_PAGRD)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRD=='RD',1,;
      0)
  endfunc

  add object oPAGBO_2_2 as StdCheck with uid="ZFDFSLISQO",rtseq=22,rtrep=.f.,left=139, top=41, caption="Bonifico",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite bonifico",;
    HelpContextID = 112426506,;
    cFormVar="w_PAGBO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPAGBO_2_2.RadioValue()
    return(iif(this.value =1,'BO',;
    'XX'))
  endfunc
  func oPAGBO_2_2.GetRadio()
    this.Parent.oContained.w_PAGBO = this.RadioValue()
    return .t.
  endfunc

  func oPAGBO_2_2.SetRadio()
    this.Parent.oContained.w_PAGBO=trim(this.Parent.oContained.w_PAGBO)
    this.value = ;
      iif(this.Parent.oContained.w_PAGBO=='BO',1,;
      0)
  endfunc

  add object oPAGRB_2_3 as StdCheck with uid="TMXADRXEDY",rtseq=23,rtrep=.f.,left=139, top=59, caption="Ric.bancaria/Ri.Ba.",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite ricevuta bancaria o RiBa",;
    HelpContextID = 125009418,;
    cFormVar="w_PAGRB", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPAGRB_2_3.RadioValue()
    return(iif(this.value =1,'RB',;
    'XX'))
  endfunc
  func oPAGRB_2_3.GetRadio()
    this.Parent.oContained.w_PAGRB = this.RadioValue()
    return .t.
  endfunc

  func oPAGRB_2_3.SetRadio()
    this.Parent.oContained.w_PAGRB=trim(this.Parent.oContained.w_PAGRB)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRB=='RB',1,;
      0)
  endfunc

  add object oPAGRI_2_4 as StdCheck with uid="UZIACQYPFF",rtseq=24,rtrep=.f.,left=316, top=23, caption="R.I.D.",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite R.I.D.",;
    HelpContextID = 117669386,;
    cFormVar="w_PAGRI", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPAGRI_2_4.RadioValue()
    return(iif(this.value =1,'RI',;
    'XX'))
  endfunc
  func oPAGRI_2_4.GetRadio()
    this.Parent.oContained.w_PAGRI = this.RadioValue()
    return .t.
  endfunc

  func oPAGRI_2_4.SetRadio()
    this.Parent.oContained.w_PAGRI=trim(this.Parent.oContained.w_PAGRI)
    this.value = ;
      iif(this.Parent.oContained.w_PAGRI=='RI',1,;
      0)
  endfunc

  add object oPAGCA_2_5 as StdCheck with uid="JWYKQSDHNC",rtseq=25,rtrep=.f.,left=316, top=41, caption="Cambiale",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite cambiale",;
    HelpContextID = 127041034,;
    cFormVar="w_PAGCA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPAGCA_2_5.RadioValue()
    return(iif(this.value =1,'CA',;
    'XX'))
  endfunc
  func oPAGCA_2_5.GetRadio()
    this.Parent.oContained.w_PAGCA = this.RadioValue()
    return .t.
  endfunc

  func oPAGCA_2_5.SetRadio()
    this.Parent.oContained.w_PAGCA=trim(this.Parent.oContained.w_PAGCA)
    this.value = ;
      iif(this.Parent.oContained.w_PAGCA=='CA',1,;
      0)
  endfunc

  add object oPAGMA_2_6 as StdCheck with uid="QNXYXNHHDD",rtseq=26,rtrep=.f.,left=316, top=59, caption="M.AV.",;
    ToolTipText = "Se attivo: seleziona i tipi pagamenti tramite M.AV. (mediante avviso)",;
    HelpContextID = 126385674,;
    cFormVar="w_PAGMA", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPAGMA_2_6.RadioValue()
    return(iif(this.value =1,'MA',;
    'XX'))
  endfunc
  func oPAGMA_2_6.GetRadio()
    this.Parent.oContained.w_PAGMA = this.RadioValue()
    return .t.
  endfunc

  func oPAGMA_2_6.SetRadio()
    this.Parent.oContained.w_PAGMA=trim(this.Parent.oContained.w_PAGMA)
    this.value = ;
      iif(this.Parent.oContained.w_PAGMA=='MA',1,;
      0)
  endfunc

  add object oPAGNO_2_7 as StdCheck with uid="PRMXYWFOMF",rtseq=27,rtrep=.f.,left=139, top=84, caption="Nessun pagamento",;
    ToolTipText = "Se attivo: seleziona le partite non associate ad alcun pagamento",;
    HelpContextID = 111640074,;
    cFormVar="w_PAGNO", bObbl = .f. , nPag = 2;
   , bGlobalFont=.t.


  func oPAGNO_2_7.RadioValue()
    return(iif(this.value =1,'NO',;
    'XX'))
  endfunc
  func oPAGNO_2_7.GetRadio()
    this.Parent.oContained.w_PAGNO = this.RadioValue()
    return .t.
  endfunc

  func oPAGNO_2_7.SetRadio()
    this.Parent.oContained.w_PAGNO=trim(this.Parent.oContained.w_PAGNO)
    this.value = ;
      iif(this.Parent.oContained.w_PAGNO=='NO',1,;
      0)
  endfunc

  add object oREGINI_2_8 as StdField with uid="JPZWSXEPCG",rtseq=28,rtrep=.f.,;
    cFormVar = "w_REGINI", cQueryName = "REGINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data registrazione iniziale � maggiore della scadenza finale",;
    ToolTipText = "Data registrazione iniziale da ricercare",;
    HelpContextID = 230455786,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=139, Top=110

  func oREGINI_2_8.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_REGINI<=.w_REGFIN) or (empty(.w_REGFIN)))
    endwith
    return bRes
  endfunc

  add object oREGFIN_2_9 as StdField with uid="PDJGLOUCJU",rtseq=29,rtrep=.f.,;
    cFormVar = "w_REGFIN", cQueryName = "REGFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    sErrorMsg = "La data registrazione iniziale � maggiore della scadenza finale",;
    ToolTipText = "Data registrazione finale da ricercare",;
    HelpContextID = 152009194,;
   bGlobalFont=.t.,;
    Height=21, Width=76, Left=139, Top=138

  func oREGFIN_2_9.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes = ((.w_regini<=.w_regfin) or (empty(.w_regini)))
    endwith
    return bRes
  endfunc

  add object oNUMPAR_2_10 as StdField with uid="ZZOAKWQTBU",rtseq=30,rtrep=.f.,;
    cFormVar = "w_NUMPAR", cQueryName = "NUMPAR",;
    bObbl = .f. , nPag = 2, value=space(31), bMultilanguage =  .f.,;
    ToolTipText = "Numero partita di selezione",;
    HelpContextID = 92604970,;
   bGlobalFont=.t.,;
    Height=21, Width=230, Left=139, Top=166, InputMask=replicate('X',31)

  add object oBANAPP_2_11 as StdField with uid="SYMGGKUHVX",rtseq=31,rtrep=.f.,;
    cFormVar = "w_BANAPP", cQueryName = "BANAPP",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Banca di appoggio del cliente/fornitore",;
    HelpContextID = 111415018,;
   bGlobalFont=.t.,;
    Height=21, Width=100, Left=139, Top=195, InputMask=replicate('X',10), bHasZoom = .t. , cLinkFile="BAN_CHE", cZoomOnZoom="GSAR_ABA", oKey_1_1="BACODBAN", oKey_1_2="this.w_BANAPP"

  func oBANAPP_2_11.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_11('Part',this)
    endwith
    return bRes
  endfunc

  proc oBANAPP_2_11.ecpDrop(oSource)
    this.Parent.oContained.link_2_11('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBANAPP_2_11.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'BAN_CHE','*','BACODBAN',cp_AbsName(this.parent,'oBANAPP_2_11'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_ABA',"Elenco banche",'',this.parent.oContained
  endproc
  proc oBANAPP_2_11.mZoomOnZoom
    local i_obj
    i_obj=GSAR_ABA()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_BANAPP
     i_obj.ecpSave()
  endproc

  add object oDESBAN_2_12 as StdField with uid="BMQBKBVXAX",rtseq=32,rtrep=.f.,;
    cFormVar = "w_DESBAN", cQueryName = "DESBAN",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(50), bMultilanguage =  .f.,;
    HelpContextID = 160611018,;
   bGlobalFont=.t.,;
    Height=21, Width=359, Left=284, Top=195, InputMask=replicate('X',50)

  add object oBANNOS_2_13 as StdField with uid="RMPAKTWDGR",rtseq=33,rtrep=.f.,;
    cFormVar = "w_BANNOS", cQueryName = "BANNOS",;
    bObbl = .f. , nPag = 2, value=space(15), bMultilanguage =  .f.,;
    sErrorMsg = "Conto inesistente oppure obsoleto",;
    ToolTipText = "Nostro C/C. comunicato al fornitore per RB o al cliente per bonifico",;
    HelpContextID = 61279978,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=139, Top=224, InputMask=replicate('X',15), bHasZoom = .t. , cLinkFile="COC_MAST", cZoomOnZoom="GSTE_ACB", oKey_1_1="BACODBAN", oKey_1_2="this.w_BANNOS"

  func oBANNOS_2_13.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_13('Part',this)
    endwith
    return bRes
  endfunc

  proc oBANNOS_2_13.ecpDrop(oSource)
    this.Parent.oContained.link_2_13('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oBANNOS_2_13.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'COC_MAST','*','BACODBAN',cp_AbsName(this.parent,'oBANNOS_2_13'),iif(empty(i_cWhere),.f.,i_cWhere),'GSTE_ACB',"Conti banca",'',this.parent.oContained
  endproc
  proc oBANNOS_2_13.mZoomOnZoom
    local i_obj
    i_obj=GSTE_ACB()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_BACODBAN=this.parent.oContained.w_BANNOS
     i_obj.ecpSave()
  endproc

  add object oDESNOS_2_14 as StdField with uid="KGPOJPSGGA",rtseq=34,rtrep=.f.,;
    cFormVar = "w_DESNOS", cQueryName = "DESNOS",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 61258442,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=284, Top=224, InputMask=replicate('X',35)

  add object oNDOINI_2_15 as StdField with uid="KGWWVHMLPE",rtseq=35,rtrep=.f.,;
    cFormVar = "w_NDOINI", cQueryName = "NDOINI",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di inizio selezione",;
    HelpContextID = 230423338,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=139, Top=253, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oADOINI_2_16 as StdField with uid="CHJUPBBDKL",rtseq=36,rtrep=.f.,;
    cFormVar = "w_ADOINI", cQueryName = "ADOINI",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di inizio selezione",;
    HelpContextID = 230423546,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=284, Top=253, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oDDOINI_2_17 as StdField with uid="POCRTPVHNT",rtseq=37,rtrep=.f.,;
    cFormVar = "w_DDOINI", cQueryName = "DDOINI",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento inizio selezione",;
    HelpContextID = 230423498,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=487, Top=253

  add object oNDOFIN_2_18 as StdField with uid="MURWGCRMYK",rtseq=38,rtrep=.f.,;
    cFormVar = "w_NDOFIN", cQueryName = "NDOFIN",;
    bObbl = .f. , nPag = 2, value=0, bMultilanguage =  .f.,;
    ToolTipText = "Numero documento di fine selezione",;
    HelpContextID = 151976746,;
   bGlobalFont=.t.,;
    Height=21, Width=135, Left=139, Top=282, cSayPict='"999999999999999"', cGetPict='"999999999999999"', nMaxValue = 999999999999999

  add object oADOFIN_2_19 as StdField with uid="UDFMAGZZLL",rtseq=39,rtrep=.f.,;
    cFormVar = "w_ADOFIN", cQueryName = "ADOFIN",;
    bObbl = .f. , nPag = 2, value=space(10), bMultilanguage =  .f.,;
    ToolTipText = "Serie documento di fine selezione",;
    HelpContextID = 151976954,;
   bGlobalFont=.t.,;
    Height=21, Width=83, Left=284, Top=282, cSayPict='"!!!!!!!!!!"', cGetPict='"!!!!!!!!!!"', InputMask=replicate('X',10)

  add object oDDOFIN_2_20 as StdField with uid="XDRFNNHKBO",rtseq=40,rtrep=.f.,;
    cFormVar = "w_DDOFIN", cQueryName = "DDOFIN",;
    bObbl = .f. , nPag = 2, value=ctod("  /  /  "), bMultilanguage =  .f.,;
    ToolTipText = "Data documento fine selezione",;
    HelpContextID = 151976906,;
   bGlobalFont=.t.,;
    Height=21, Width=79, Left=487, Top=282

  add object oVALUTA_2_21 as StdField with uid="OZUORMJRIC",rtseq=41,rtrep=.f.,;
    cFormVar = "w_VALUTA", cQueryName = "VALUTA",;
    bObbl = .f. , nPag = 2, value=space(3), bMultilanguage =  .f.,;
    ToolTipText = "Codice valuta, se vuota tutte",;
    HelpContextID = 89140650,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=139, Top=311, InputMask=replicate('X',3), bHasZoom = .t. , cLinkFile="VALUTE", cZoomOnZoom="GSAR_AVL", oKey_1_1="VACODVAL", oKey_1_2="this.w_VALUTA"

  func oVALUTA_2_21.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_21('Part',this)
    endwith
    return bRes
  endfunc

  proc oVALUTA_2_21.ecpDrop(oSource)
    this.Parent.oContained.link_2_21('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oVALUTA_2_21.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'VALUTE','*','VACODVAL',cp_AbsName(this.parent,'oVALUTA_2_21'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AVL',"Valute",'',this.parent.oContained
  endproc
  proc oVALUTA_2_21.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AVL()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_VACODVAL=this.parent.oContained.w_VALUTA
     i_obj.ecpSave()
  endproc

  add object oDESVAL_2_22 as StdField with uid="MRUCBMOTHB",rtseq=42,rtrep=.f.,;
    cFormVar = "w_DESVAL", cQueryName = "DESVAL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 192854730,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=284, Top=311, InputMask=replicate('X',35)


  add object oFLSOSP_2_23 as StdCombo with uid="JPWRXLSRGK",rtseq=43,rtrep=.f.,left=139,top=340,width=92,height=21;
    , ToolTipText = "Ricerca le scadenze per il flag sospeso";
    , HelpContextID = 107328426;
    , cFormVar="w_FLSOSP",RowSource=""+"Tutte,"+"Non sospese,"+"Sospese", bObbl = .f. , nPag = 2;
  , bGlobalFont=.t.


  func oFLSOSP_2_23.RadioValue()
    return(iif(this.value =1,'T',;
    iif(this.value =2,'N',;
    iif(this.value =3,'S',;
    space(1)))))
  endfunc
  func oFLSOSP_2_23.GetRadio()
    this.Parent.oContained.w_FLSOSP = this.RadioValue()
    return .t.
  endfunc

  func oFLSOSP_2_23.SetRadio()
    this.Parent.oContained.w_FLSOSP=trim(this.Parent.oContained.w_FLSOSP)
    this.value = ;
      iif(this.Parent.oContained.w_FLSOSP=='T',1,;
      iif(this.Parent.oContained.w_FLSOSP=='N',2,;
      iif(this.Parent.oContained.w_FLSOSP=='S',3,;
      0)))
  endfunc

  add object oCODAGE_2_24 as StdField with uid="ANBLNSHDPI",rtseq=44,rtrep=.f.,;
    cFormVar = "w_CODAGE", cQueryName = "CODAGE",;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    ToolTipText = "Codice agente di selezione",;
    HelpContextID = 37003482,;
   bGlobalFont=.t.,;
    Height=21, Width=65, Left=139, Top=369, InputMask=replicate('X',5), bHasZoom = .t. , cLinkFile="AGENTI", cZoomOnZoom="GSAR_AGE", oKey_1_1="AGCODAGE", oKey_1_2="this.w_CODAGE"

  func oCODAGE_2_24.mCond()
    if(VARTYPE(this.Parent.oContained)<>'O')
      return .t.
    else
    with this.Parent.oContained
      return (.w_TIPCON='C')
    endwith
   endif
  endfunc

  func oCODAGE_2_24.mHide()
    with this.Parent.oContained
      return (! .w_VISUAL)
    endwith
  endfunc

  func oCODAGE_2_24.Check()
    local bRes, bRes2
    bRes = .t.
    with this.Parent.oContained
      bRes=.link_2_24('Part',this)
    endwith
    return bRes
  endfunc

  proc oCODAGE_2_24.ecpDrop(oSource)
    this.Parent.oContained.link_2_24('Drop',oSource)
    this.Parent.oContained.mCalc(.t.)
  endproc

  proc oCODAGE_2_24.mZoom
    private i_cWhere
    i_cWhere = ""
    do cp_zoom with 'AGENTI','*','AGCODAGE',cp_AbsName(this.parent,'oCODAGE_2_24'),iif(empty(i_cWhere),.f.,i_cWhere),'GSAR_AGE',"Elenco agenti",'AGE1ZOOM.AGENTI_VZM',this.parent.oContained
  endproc
  proc oCODAGE_2_24.mZoomOnZoom
    local i_obj
    i_obj=GSAR_AGE()
    i_obj.cFunction="Filter"
    i_obj.BlankRec()
     i_obj.w_AGCODAGE=this.parent.oContained.w_CODAGE
     i_obj.ecpSave()
  endproc

  add object oSIMVAL_2_39 as StdField with uid="EYXMJXKZZW",rtseq=47,rtrep=.f.,;
    cFormVar = "w_SIMVAL", cQueryName = "SIMVAL",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(5), bMultilanguage =  .f.,;
    HelpContextID = 192878042,;
   bGlobalFont=.t.,;
    Height=21, Width=48, Left=200, Top=311, InputMask=replicate('X',5)

  add object oDESAGE_2_40 as StdField with uid="ABLOIHLJTT",rtseq=49,rtrep=.f.,;
    cFormVar = "w_DESAGE", cQueryName = "DESAGE",enabled=.f.,;
    bObbl = .f. , nPag = 2, value=space(35), bMultilanguage =  .f.,;
    HelpContextID = 36944586,;
   bGlobalFont=.t.,;
    Height=21, Width=282, Left=284, Top=369, InputMask=replicate('X',35)

  func oDESAGE_2_40.mHide()
    with this.Parent.oContained
      return (! .w_VISUAL)
    endwith
  endfunc


  add object oBtn_2_45 as StdButton with uid="OYTJEFHLHJ",left=724, top=435, width=48,height=45,;
    CpPicture="BMP\ESC.BMP", caption="", nPag=2;
    , ToolTipText = "Premere per uscire";
    , HelpContextID = 192580794;
    , Caption='\<Esci';
    , FontName = "Arial", FontSize = 7, FontBold = .f., FontItalic=.f., FontUnderline=.f., FontStrikeThru=.f.
    proc oBtn_2_45.Click()
      =cp_StandardFunction(this,"Quit")
      if !isnull(this.parent.oContained) and inlist(this.parent.oContained.cfunction,"Edit","Load")
        this.parent.oContained.mCalc(.t.)
      endif
    endproc

  add object oStr_2_25 as StdString with uid="ZXUEGERPDB",Visible=.t., Left=274, Top=253,;
    Alignment=2, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_26 as StdString with uid="DUYDMQHLSD",Visible=.t., Left=428, Top=253,;
    Alignment=1, Width=57, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_2_27 as StdString with uid="VBNYFPBAFC",Visible=.t., Left=274, Top=282,;
    Alignment=2, Width=9, Height=15,;
    Caption="/"  ;
  , bGlobalFont=.t.

  add object oStr_2_28 as StdString with uid="MSQTGBLKMH",Visible=.t., Left=428, Top=282,;
    Alignment=1, Width=57, Height=15,;
    Caption="Del:"  ;
  , bGlobalFont=.t.

  add object oStr_2_29 as StdString with uid="UAUJXLJWRU",Visible=.t., Left=5, Top=311,;
    Alignment=1, Width=131, Height=15,;
    Caption="Codice valuta:"  ;
  , bGlobalFont=.t.

  add object oStr_2_30 as StdString with uid="CGCSMWQNRW",Visible=.t., Left=5, Top=166,;
    Alignment=1, Width=131, Height=15,;
    Caption="Numero partita:"  ;
  , bGlobalFont=.t.

  add object oStr_2_31 as StdString with uid="MDFRADOWOF",Visible=.t., Left=5, Top=224,;
    Alignment=1, Width=131, Height=15,;
    Caption="Nostra banca:"  ;
  , bGlobalFont=.t.

  add object oStr_2_32 as StdString with uid="WZFZIYWSMO",Visible=.t., Left=5, Top=195,;
    Alignment=1, Width=131, Height=15,;
    Caption="Banca di appoggio:"  ;
  , bGlobalFont=.t.

  add object oStr_2_33 as StdString with uid="FGNHTPOEBA",Visible=.t., Left=5, Top=253,;
    Alignment=1, Width=131, Height=15,;
    Caption="Da n. documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_34 as StdString with uid="LNGVKJNQKG",Visible=.t., Left=5, Top=282,;
    Alignment=1, Width=131, Height=15,;
    Caption="A n. documento:"  ;
  , bGlobalFont=.t.

  add object oStr_2_37 as StdString with uid="ZDCZOSBMTF",Visible=.t., Left=5, Top=340,;
    Alignment=1, Width=131, Height=15,;
    Caption="Test sospese:"  ;
  , bGlobalFont=.t.

  add object oStr_2_38 as StdString with uid="MGJSGMLWJX",Visible=.t., Left=5, Top=23,;
    Alignment=1, Width=131, Height=15,;
    Caption="Pagamenti:"  ;
  , bGlobalFont=.t.

  add object oStr_2_41 as StdString with uid="OMMPRNQWNZ",Visible=.t., Left=5, Top=369,;
    Alignment=1, Width=131, Height=18,;
    Caption="Codice agente:"  ;
  , bGlobalFont=.t.

  func oStr_2_41.mHide()
    with this.Parent.oContained
      return (! .w_VISUAL)
    endwith
  endfunc

  add object oStr_2_42 as StdString with uid="NVBJZFZWIX",Visible=.t., Left=5, Top=110,;
    Alignment=1, Width=131, Height=18,;
    Caption="Da data reg.:"  ;
  , bGlobalFont=.t.

  add object oStr_2_43 as StdString with uid="BHEBGIAIYV",Visible=.t., Left=5, Top=138,;
    Alignment=1, Width=131, Height=18,;
    Caption="A data reg.:"  ;
  , bGlobalFont=.t.
enddefine

procedure QueryFilter(result)
  local i_res, i_cAliasName,i_cAliasName2
  result=''
  i_res=cp_AppQueryFilter('gste_kms','','')
  if !Empty(i_res)
    if lower(right(i_res,4))='.vqr'
       i_cAliasName2 = "cp"+Right(SYS(2015),8)
      i_res=" exists (select 1 from ("+cp_GetSQLFromQuery(i_res,.t.)+") "+i_cAliasName2+" where ";
  +")"
    endif
    result=iif(Empty(result),i_res,'('+result+') AND ('+i_res+')')
  endif
endproc

procedure getEntityType(result)
  result="Dialog Window"
endproc

* --- Area Manuale = Functions & Procedures
* --- Fine Area Manuale
