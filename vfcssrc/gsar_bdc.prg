* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bdc                                                        *
*              Controlli cancellazione commessa                                *
*                                                                              *
*      Author: Zucchetti Spa                                                   *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][161][VRS_61]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2002-11-20                                                      *
* Last revis.: 2007-11-08                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bdc",oParentObject)
return(i_retval)

define class tgsar_bdc as StdBatch
  * --- Local variables
  w_TROV = 0
  w_TROVA = 0
  w_MESS = space(10)
  w_OK = .f.
  * --- WorkFile variables
  DOC_DETT_idx=0
  ATTIVITA_idx=0
  MOVICOST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Controllo Presenza della Commessa nei Documenti in Cancellazione (da GSCA_ACN)
    this.w_TROV = 0
    this.w_TROVA = 0
    this.w_OK = .T.
    * --- Select from ATTIVITA
    i_nConn=i_TableProp[this.ATTIVITA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIVITA_idx,2],.t.,this.ATTIVITA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIVITA ";
          +" where ATCODCOM="+cp_ToStrODBC(this.oParentObject.w_CNCODCAN)+"";
           ,"_Curs_ATTIVITA")
    else
      select * from (i_cTable);
       where ATCODCOM=this.oParentObject.w_CNCODCAN;
        into cursor _Curs_ATTIVITA
    endif
    if used('_Curs_ATTIVITA')
      select _Curs_ATTIVITA
      locate for 1=1
      do while not(eof())
      this.w_OK = .F.
      this.w_MESS = ah_MsgFormat("Esistono attivit� di commessa associate, impossibile eliminare")
        select _Curs_ATTIVITA
        continue
      enddo
      use
    endif
    if this.w_OK
      ah_Msg("Verifica esistenza commessa nei documenti...",.T.)
      * --- Select from DOC_DETT
      i_nConn=i_TableProp[this.DOC_DETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.DOC_DETT_idx,2],.t.,this.DOC_DETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select COUNT(*) AS TOTALE  from "+i_cTable+" DOC_DETT ";
            +" where MVCODCOM="+cp_ToStrODBC(this.oParentObject.w_CNCODCAN)+"";
             ,"_Curs_DOC_DETT")
      else
        select COUNT(*) AS TOTALE from (i_cTable);
         where MVCODCOM=this.oParentObject.w_CNCODCAN;
          into cursor _Curs_DOC_DETT
      endif
      if used('_Curs_DOC_DETT')
        select _Curs_DOC_DETT
        locate for 1=1
        do while not(eof())
        this.w_TROV = NVL(TOTALE, 0)
          select _Curs_DOC_DETT
          continue
        enddo
        use
      endif
      if this.w_TROV<>0
        this.w_MESS = ah_MsgFormat("Esistono documenti associati alla commessa ; impossibile eliminare%0(N.righe doc.: %1)", ALLTR(STR(this.w_TROV))) 
        this.w_OK = .F.
      else
        ah_Msg("Verifica esistenza commessa nei movimenti di analitica...",.T.)
        * --- Select from MOVICOST
        i_nConn=i_TableProp[this.MOVICOST_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.MOVICOST_idx,2],.t.,this.MOVICOST_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select COUNT(*) AS TOTALE1  from "+i_cTable+" MOVICOST ";
              +" where MRCODCOM="+cp_ToStrODBC(this.oParentObject.w_CNCODCAN)+"";
               ,"_Curs_MOVICOST")
        else
          select COUNT(*) AS TOTALE1 from (i_cTable);
           where MRCODCOM=this.oParentObject.w_CNCODCAN;
            into cursor _Curs_MOVICOST
        endif
        if used('_Curs_MOVICOST')
          select _Curs_MOVICOST
          locate for 1=1
          do while not(eof())
          this.w_TROVA = NVL(TOTALE1, 0)
            select _Curs_MOVICOST
            continue
          enddo
          use
        endif
        if this.w_TROVA<>0
          this.w_MESS = ah_MsgFormat("Esistono movimenti di analitica associati alla commessa ; impossibile eliminare%0(N.righe movimento: %1)", ALLTR(STR(this.w_TROVA)))
          this.w_OK = .F.
        endif
      endif
    endif
    if Not this.w_OK
      * --- transaction error
      bTrsErr=.t.
      i_TrsMsg=this.w_MESS
    endif
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,3)]
    this.cWorkTables[1]='DOC_DETT'
    this.cWorkTables[2]='ATTIVITA'
    this.cWorkTables[3]='MOVICOST'
    return(this.OpenAllTables(3))

  proc CloseCursors()
    if used('_Curs_ATTIVITA')
      use in _Curs_ATTIVITA
    endif
    if used('_Curs_DOC_DETT')
      use in _Curs_DOC_DETT
    endif
    if used('_Curs_MOVICOST')
      use in _Curs_MOVICOST
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
