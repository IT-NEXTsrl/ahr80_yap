* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gsar_bmd                                                        *
*              Aggiorna spese trasporto fino al confine                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_159]                                            *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-01                                                      *
* Last revis.: 2000-06-14                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject,pOper
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgsar_bmd",oParentObject,m.pOper)
return(i_retval)

define class tgsar_bmd as StdBatch
  * --- Local variables
  pOper = space(5)
  w_ZOOM = space(10)
  w_SERIAL = space(10)
  w_SPESE = 0
  * --- WorkFile variables
  DOC_MAST_idx=0
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Gestione Spese Trasporto fino al Confine (da GSAR_KMD)
    this.w_ZOOM = this.oParentObject.w_Zoom
    NC = this.w_ZOOM.cCursor
    do case
      case this.pOper="RICER"
        this.Pag2()
        if i_retcode='stop' or !empty(i_Error)
          return
        endif
      case this.pOper="SELEZ"
        * --- Seleziona/Deseleziona Tutto
        if this.oParentObject.w_SELEZI="S"
          UPDATE &NC SET XCHK = 1
        else
          UPDATE &NC SET XCHK = 0
        endif
      case this.pOper="AGGIO"
        * --- Aggiorna Spese Trasporto fino al Confine nei Documenti
        SELECT (NC)
        if RECCOUNT(NC)>0
          GO TOP
          SCAN FOR XCHK<>0 AND NOT EMPTY(NVL(MVSERIAL," "))
          this.w_SERIAL = MVSERIAL
          this.w_SPESE = MDTRAINT
          * --- Write into DOC_MAST
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.DOC_MAST_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.DOC_MAST_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.DOC_MAST_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"MVTRAINT ="+cp_NullLink(cp_ToStrODBC(this.w_SPESE),'DOC_MAST','MVTRAINT');
                +i_ccchkf ;
            +" where ";
                +"MVSERIAL = "+cp_ToStrODBC(this.w_SERIAL);
                   )
          else
            update (i_cTable) set;
                MVTRAINT = this.w_SPESE;
                &i_ccchkf. ;
             where;
                MVSERIAL = this.w_SERIAL;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          SELECT (NC)
          ENDSCAN
          * --- Aggiorna il cursore dello zoom
          this.Pag2()
          if i_retcode='stop' or !empty(i_Error)
            return
          endif
        else
          ah_ErrorMsg("Non ci sono documenti da aggiornare",,"")
        endif
    endcase
  endproc


  procedure Pag2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Ricerca
    This.OparentObject.NotifyEvent("Ricerca")
    * --- Setta Proprieta' Campi del Cursore
    FOR I=1 TO This.w_Zoom.grd.ColumnCount
    NC = ALLTRIM(STR(I))
    if NOT ("XCHK" $ UPPER(This.w_Zoom.grd.Column&NC..ControlSource) OR "MDTRAINT" $ UPPER(This.w_Zoom.grd.Column&NC..ControlSource)) 
      This.w_Zoom.grd.Column&NC..Enabled=.f.
    else
      This.w_Zoom.grd.Column&NC..Enabled=.t.
      This.w_Zoom.grd.Column&NC..Format="KR"
    endif
    ENDFOR
    SELECT (this.w_ZOOM.cCursor)
    GO TOP
    this.w_ZOOM.refresh()
  endproc


  proc Init(oParentObject,pOper)
    this.pOper=pOper
    DoDefault(oParentObject)
    return
  function OpenTables()
    dimension this.cWorkTables[max(1,1)]
    this.cWorkTables[1]='DOC_MAST'
    return(this.OpenAllTables(1))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result="pOper"
endproc
