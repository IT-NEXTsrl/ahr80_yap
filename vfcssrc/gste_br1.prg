* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gste_br1                                                        *
*              Modifica path gen: disk                                         *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF][111][VRS_20]                                             *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-02-03                                                      *
* Last revis.: 2000-02-03                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgste_br1",oParentObject)
return(i_retval)

define class tgste_br1 as StdBatch
  * --- Local variables
  * --- WorkFile variables
  runtime_filters = 1

  procedure Pag1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Modifica Path (da GSTE_KRF)
    this.oParentObject.w_NOMEFILE = IIF(NOT EMPTY(this.oParentObject.w_NOMEFILE5) , this.oParentObject.w_NOMEFILE5, ALLTRIM(this.oParentObject.w_NOMEFILE2)+ALLTRIM(this.oParentObject.w_NOMEFILE3)+ALLTRIM(this.oParentObject.w_NOMEFILE1)+IIF(this.oParentObject.w_NOMEFILE4="0", " ", ALLTRIM(this.oParentObject.w_NOMEFILE4)))
    NC = this.oParentObject.w_ZOOMEF.cCursor
    SELECT (NC)
    ZAP
  endproc


  function OpenTables()
    dimension this.cWorkTables[max(1,0)]
    return(this.OpenAllTables(0))

  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
