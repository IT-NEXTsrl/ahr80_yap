* ---------------------------------------------------------------------------- *
* #%&%#Build:  60
*                                                                              *
*   Procedure: gscg_bda                                                        *
*              Liquidazione annuale IVA                                        *
*                                                                              *
*      Author:                                                                 *
*      Client:                                                                 *
*                                                                              *
*    Language:                                                                 *
*          OS: [TRCF] [116] [VRS_501]                                          *
*                                                                              *
*     Version:                                                                 *
* Date creat.: 2000-07-20                                                      *
* Last revis.: 2015-02-11                                                      *
*                                                                              *
*                                                                              *
* ---------------------------------------------------------------------------- *
#include "cp_app_lang.inc"
parameters oParentObject
* --- Area Manuale = Header
* --- Fine Area Manuale
private i_retcode,i_retval,i_Error,i_Rows
i_retcode = 'go'
i_retval = ''
i_Error = ''
i_Rows = 0
createobject("tgscg_bda",oParentObject)
return(i_retval)

define class tgscg_bda as StdBatch
  * --- Local variables
  w_FLAGRI = space(1)
  w_TIPAGR = space(1)
  w_ALIAGR = 0
  DETAGR = space(10)
  w_PERCOM = 0
  w_DENMAGPI = 0
  w_TOTIMP = 0
  w_TOTIVD = 0
  w_TOTIVI = 0
  w_TOTMAC = 0
  w_GLOMAC = 0
  w_PERMAC = 0
  w_TIPLIQ = space(1)
  w_F2DEFI = space(1)
  w_TIPDIC = space(1)
  w_FLTEST = space(1)
  w_DATBLO = ctod("  /  /  ")
  w_DATBLO2 = ctod("  /  /  ")
  w_DATFI2 = ctod("  /  /  ")
  w_DESCRI = space(35)
  w_MESS = space(90)
  w_CODVAL = space(5)
  w_APPO = space(10)
  w_APPO1 = 0
  w_APPO2 = 0
  w_CONFER = space(1)
  w_IVADEB = 0
  w_IVACRE = 0
  w_CREVAL = space(3)
  w_KEYATT = space(5)
  w_OLDPER = 0
  w_PERPRO = 0
  w_OLDPRO = 0
  w_FLPROR = space(1)
  w_VP__ANNO = space(4)
  w_VPPERIOD = 0
  w_VPCODATT = space(5)
  w_VPKEYATT = space(5)
  w_CODIVA = space(5)
  w_DESIVA = space(35)
  w_TIPREG = space(1)
  w_NUMREG = 0
  w_CONTA = 0
  w_ULTPER = 0
  w_CORVEN = 0
  w_PERIVA = 0
  w_PERIND = 0
  w_VLIMPO04 = 0
  w_VLIMPCRE = 0
  w_VLIMPCRP = 0
  w_VLIMPO07 = 0
  w_VLIMPO08 = 0
  w_VLIMPO09 = 0
  w_VLIMPO10 = 0
  w_VLIMPO11 = 0
  w_VLIMPO12 = 0
  w_VLIMPO13 = 0
  w_VLIMPO14 = 0
  w_VLIMPO15 = 0
  w_VLIMPO16 = 0
  w_VLIMPO17 = 0
  w_VLIMPO18 = 0
  w_VLIMPO19 = 0
  w_VLIMPO20 = 0
  w_VLIMPO21 = 0
  w_VLIMPO23 = 0
  w_TOTALE = 0
  w_VPCRERIM = 0
  w_VPCREUTI = 0
  w_VPDICSOC = space(1)
  w_VPCODVAL = space(1)
  w_VPIMPO12 = 0
  w_VPIMPO13 = 0
  w_VPIMPO14 = 0
  w_VPIMPO15 = 0
  w_VPIMPVER = 0
  w_VPIMPO16 = 0
  w_VALSUC = space(3)
  w_PAGINE = 0
  w_INDAZI = space(25)
  w_LOCAZI = space(35)
  w_CAPAZI = space(5)
  w_PROAZI = space(2)
  w_COFAZI = space(16)
  w_PIVAZI = space(12)
  w_LETAZI = .f.
  w_PRIREG = space(1)
  w_PRINUM = 0
  w_IVSPLPAY = space(1)
  w_IVACREA = 0
  w_IVACREB = 0
  w_IVACREC = 0
  w_VE37 = 0
  w_VE38 = 0
  w_VF16 = 0
  w_VF17 = 0
  w_INIANNO = ctod("  /  /  ")
  w_FINANNO = ctod("  /  /  ")
  w_OPPART = 0
  w_OPESENT = 0
  w_VLIMPO36 = 0
  w_VPRORATA = space(1)
  w_ANNDIC = space(4)
  w_LCODATT = space(8)
  w_LDESATT = space(35)
  w_CODATTS = space(8)
  w_DATINI = ctod("  /  /  ")
  * --- WorkFile variables
  ATTIDETT_idx=0
  ATTIMAST_idx=0
  AZIENDA_idx=0
  CREDDIVA_idx=0
  CREDMIVA_idx=0
  ESERCIZI_idx=0
  IVA_PERI_idx=0
  PRO_RATA_idx=0
  ATT_ALTE_idx=0
  runtime_filters = 1

  procedure Page_1
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Elaborazione Liquidazione IVA Annuale (da GSCG_SDA)
    DIMENSION DETAGR[90, 3]
    this.w_DENMAGPI = this.oParentObject.w_DENMAG
    do case
      case EMPTY(this.oParentObject.w_ANNO)
        ah_ErrorMsg("Anno di riferimento non definito",,"")
        i_retcode = 'stop'
        return
      case EMPTY(this.oParentObject.w_CODATT) AND this.oParentObject.w_MTIPLIQ<>"R"
        ah_ErrorMsg("Codice attivit� non definito",,"")
        i_retcode = 'stop'
        return
    endcase
    * --- Testata Report
    L_TESTATA=""
    if this.oParentObject.w_FLDEFI<>"S"
      L_TESTATA=ah_MsgFormat("Stampa di prova della liquidazione IVA annuale")
    endif
    L_PERIODO=ah_MsgFormat("Conguaglio liquidazione")
    L_DESMESE=Ah_MsgFormat("Conguaglio liquidazione %1",this.oParentObject.w_ANNO)
    * --- Numero Periodo e Attivit�
    L_TIPLIQ = this.oParentObject.w_MTIPLIQ
    * --- Dati Dichiarazione Annuale
    * --- Dati Dichiarazioni Periodiche
    * --- PER NUMERAZIONE PAGINE
    this.w_PRIREG = this.oParentObject.w_ATTIPREG
    this.w_PRINUM = this.oParentObject.w_ATNUMREG
    * --- Blocco della Primanota e dei registri associati alla Attivita'
    this.w_DATFI2 = i_DATSYS
    * --- Se Stampa reg.IVA la Primanota viene gia' bloccata in GSCG_BRI
    * --- Try
    local bErr_00E8B670
    bErr_00E8B670=bTrsErr
    this.Try_00E8B670()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      * --- rollback
      bTrsErr=.t.
      cp_EndTrs(.t.)
      ah_ErrorMsg(i_errmsg,,"")
      i_retcode = 'stop'
      return
    endif
    bTrsErr=bTrsErr or bErr_00E8B670
    * --- End
    this.w_TIPLIQ = this.oParentObject.w_MTIPLIQ
    this.w_F2DEFI = this.oParentObject.w_FLDEFI
    this.w_TIPDIC = "N"
    this.w_FLTEST = this.oParentObject.w_MFLTEST
    this.w_VLIMPO04 = 0
    this.w_VLIMPCRE = 0
    this.w_VLIMPCRP = 0
    this.w_VLIMPO07 = 0
    this.w_VLIMPO08 = 0
    this.w_VLIMPO09 = 0
    this.w_VLIMPO10 = 0
    this.w_VLIMPO11 = 0
    this.w_VLIMPO12 = 0
    this.w_VLIMPO13 = 0
    this.w_VLIMPO14 = 0
    this.w_VLIMPO15 = 0
    this.w_VLIMPO16 = 0
    this.w_VLIMPO17 = 0
    this.w_VLIMPO18 = 0
    this.w_VLIMPO19 = 0
    this.w_VLIMPO20 = 0
    this.w_VLIMPO21 = 0
    this.w_VLIMPO23 = 0
    this.w_VP__ANNO = this.oParentObject.w_ANNO
    this.w_VPCODATT = IIF(this.w_TIPLIQ="R", "     ", this.oParentObject.w_CODATT)
    this.w_VPKEYATT = IIF(this.w_TIPLIQ="R", "#####", this.oParentObject.w_CODATT)
    * --- Crea il Cursore per la Liquidazione IVA
     
 CREATE CURSOR LIQIVA (CODATT C(5), DESCRI C(35), TIPREG C(1), NUMREG N(2,0), CODIVA C(5), PERIVA N(6,2), PERIND N(6,2), ; 
 DESIVA C(35),TOTIMP N(18,4), TOTIVD N(18,4), TOTIVI N(18,4), TOTMAC N(18,4), PERMAC N(10,6), FLPROR C(1), PERPRO N(5,0), OLDPRO N(5,0),FLAGRI C(1), PERCOM N(5,2), TIPAGR C(1),IMPAGR N(18,4),IVSPLPAY C(1)) 
    * --- Crea il Cursore per le Percentuali Prorata (dettaglio su Attivita')
    CREATE CURSOR DETPRO (CODATT C(5), PERPRO N(5))
    * --- Lettura Credito di Inizio Anno (VL14)
    this.w_VLIMPO14 = 0
    this.w_VLIMPO11 = 0
    this.w_CREVAL = g_PERVAL
    * --- Read from CREDMIVA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.CREDMIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CREDMIVA_idx,2],.t.,this.CREDMIVA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "CICREINI,CIVALINI"+;
        " from "+i_cTable+" CREDMIVA where ";
            +"CI__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        CICREINI,CIVALINI;
        from (i_cTable) where;
            CI__ANNO = this.oParentObject.w_ANNO;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_VLIMPO14 = NVL(cp_ToDate(_read_.CICREINI),cp_NullValue(_read_.CICREINI))
      this.w_CREVAL = NVL(cp_ToDate(_read_.CIVALINI),cp_NullValue(_read_.CIVALINI))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    * --- Se La valuta dell'esercizio diversa da quella dei Crediti converte
    if this.w_VLIMPO14<>0 AND this.w_CREVAL<>g_PERVAL AND g_CAOEUR<>0 
      if this.w_CREVAL=g_CODLIR
        this.w_VLIMPO14 = cp_ROUND(this.w_VLIMPO14/g_CAOEUR, g_PERPVL)
      else
        this.w_VLIMPO14 = cp_ROUND(this.w_VLIMPO14*g_CAOEUR, g_PERPVL)
      endif
    endif
    * --- Legge gli Utilizzi dell'Anno relativi al Mod.F24
    * --- Select from CREDDIVA
    i_nConn=i_TableProp[this.CREDDIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CREDDIVA_idx,2],.t.,this.CREDDIVA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select CICREF24,CICRERIM,CIVALPER  from "+i_cTable+" CREDDIVA ";
          +" where CI__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNO)+"";
           ,"_Curs_CREDDIVA")
    else
      select CICREF24,CICRERIM,CIVALPER from (i_cTable);
       where CI__ANNO=this.oParentObject.w_ANNO;
        into cursor _Curs_CREDDIVA
    endif
    if used('_Curs_CREDDIVA')
      select _Curs_CREDDIVA
      locate for 1=1
      do while not(eof())
      * --- Cicla sui Crediti del Periodo
      this.w_CODVAL = IIF(EMPTY(NVL(_Curs_CREDDIVA.CIVALPER, "")), g_PERVAL, _Curs_CREDDIVA.CIVALPER)
      this.w_APPO1 = NVL(_Curs_CREDDIVA.CICREF24, 0)
      this.w_APPO2 = NVL(_Curs_CREDDIVA.CICRERIM, 0)
      * --- Se La valuta dell'esercizio diversa da quella dei Crediti converte
      if (this.w_APPO1<>0 OR this.w_APPO2<>0) AND this.w_CODVAL<>g_PERVAL AND g_CAOEUR<>0 
        if this.w_CODVAL=g_CODLIR
          this.w_APPO1 = cp_ROUND(this.w_APPO1/g_CAOEUR, g_PERPVL)
          this.w_APPO2 = cp_ROUND(this.w_APPO2/g_CAOEUR, g_PERPVL)
        else
          this.w_APPO1 = cp_ROUND(this.w_APPO1*g_CAOEUR, g_PERPVL)
          this.w_APPO2 = cp_ROUND(this.w_APPO2*g_CAOEUR, g_PERPVL)
        endif
      endif
      this.w_VLIMPO11 = this.w_VLIMPO11 + this.w_APPO1
      * --- Storna dal Credito iniziale gli eventuali Rimborsi
      this.w_VLIMPO14 = this.w_VLIMPO14 - this.w_APPO2
        select _Curs_CREDDIVA
        continue
      enddo
      use
    endif
    * --- Importi definiti in KiloLire o all'Unita' di Euro
    this.w_APPO = IIF(g_PERVAL=g_CODLIR, 1000, 1)
    * --- VL22 - Credito IVA Risultante dalla Dichiarazione per il periodo precedente Compensata mod. F24
    this.w_VLIMPO11 = (INTROUND(this.w_VLIMPO11, IIF(g_PERVAL=g_CODLIR, 1000, 1)) / this.w_APPO)
    * --- VL26 - Credito Risultante dalla Dichiarazione per l'anno precedente 
    this.w_VLIMPO14 = (INTROUND(this.w_VLIMPO14, IIF(g_PERVAL=g_CODLIR, 1000, 1)) / this.w_APPO)
    * --- Inizia Elaborazione ,Cicla sulle Attivita' (Quella Selezionata o Tutte se Liquidazione Riepilogativa)
    this.w_KEYATT = IIF(this.w_TIPLIQ="R", "     ", this.oParentObject.w_CODATT)
    VQ_EXEC("QUERY\GSCG1BLP.VQR",this,"ATTIVITA")
    if USED("ATTIVITA")
      this.w_CONFER = " "
      this.Page_2()
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
    if USED("ATTIVITA")
      SELECT ATTIVITA
      USE
    endif
    if USED("SALDIIVA")
      SELECT SALDIIVA
      USE
    endif
    if USED("LIQIVA")
      SELECT LIQIVA
      USE
    endif
    if USED("DETPRO")
      SELECT DETPRO
      USE
    endif
    if USED("__TMP__")
      SELECT __TMP__
      USE
    endif
    if USED("APPO")
      SELECT APPO
      USE
    endif
    * --- Toglie Blocchi per Primanota e Attivita'
    * --- Try
    local bErr_00E94CD0
    bErr_00E94CD0=bTrsErr
    this.Try_00E94CD0()
    * --- Catch
    if !empty(i_Error)
      i_ErrMsg=i_Error
      i_Error=''
      this.w_MESS = "Impossibile rimuovere le date di blocco presenti in primanota e registri IVA.%0provvedere a rimuoverle manualmente dai dati azienda e attivit�"
      ah_ErrorMsg(this.w_MESS,,"")
    endif
    bTrsErr=bTrsErr or bErr_00E94CD0
    * --- End
  endproc
  proc Try_00E8B670()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- Leggo la data dei blocco e la data stampa del libro giornale
    this.w_DATBLO = cp_CharToDate("  -  -  ")
    * --- Read from AZIENDA
    i_nOldArea=select()
    if used('_read_')
      select _read_
      use
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select "+;
        "AZDATBLO"+;
        " from "+i_cTable+" AZIENDA where ";
            +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             ,"_read_")
      i_Rows=iif(used('_read_'),reccount(),0)
    else
      select;
        AZDATBLO;
        from (i_cTable) where;
            AZCODAZI = this.oParentObject.w_CODAZI;
         into cursor _read_
      i_Rows=_tally
    endif
    if used('_read_')
      locate for 1=1
      this.w_DATBLO = NVL(cp_ToDate(_read_.AZDATBLO),cp_NullValue(_read_.AZDATBLO))
      use
    else
      * --- Error: sql sentence error.
      i_Error = MSG_READ_ERROR
      return
    endif
    select (i_nOldArea)
    if EMPTY(this.w_DATBLO)
      * --- Verifica le Attivita'
      this.w_DATBLO2 = cp_CharToDate("  -  -  ")
      if this.w_TIPLIQ="R"
        * --- Se Riepilogativa; Blocca Tutti i Registri
        * --- Select from ATTIDETT
        i_nConn=i_TableProp[this.ATTIDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIDETT ";
              +" where ATNUMREG>0";
               ,"_Curs_ATTIDETT")
        else
          select * from (i_cTable);
           where ATNUMREG>0;
            into cursor _Curs_ATTIDETT
        endif
        if used('_Curs_ATTIDETT')
          select _Curs_ATTIDETT
          locate for 1=1
          do while not(eof())
          this.w_DATBLO2 = IIF(EMPTY(CP_TODATE(_Curs_ATTIDETT.ATDATBLO)), this.w_DATBLO2, CP_TODATE(_Curs_ATTIDETT.ATDATBLO))
            select _Curs_ATTIDETT
            continue
          enddo
          use
        endif
      else
        * --- Select from ATTIDETT
        i_nConn=i_TableProp[this.ATTIDETT_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select * from "+i_cTable+" ATTIDETT ";
              +" where ATCODATT="+cp_ToStrODBC(this.oParentObject.w_CODATT)+"";
               ,"_Curs_ATTIDETT")
        else
          select * from (i_cTable);
           where ATCODATT=this.oParentObject.w_CODATT;
            into cursor _Curs_ATTIDETT
        endif
        if used('_Curs_ATTIDETT')
          select _Curs_ATTIDETT
          locate for 1=1
          do while not(eof())
          this.w_DATBLO2 = IIF(EMPTY(CP_TODATE(_Curs_ATTIDETT.ATDATBLO)), this.w_DATBLO2, CP_TODATE(_Curs_ATTIDETT.ATDATBLO))
            select _Curs_ATTIDETT
            continue
          enddo
          use
        endif
      endif
      if EMPTY(this.w_DATBLO2) 
        * --- Inserisce <Blocco> per Primanota
        * --- Write into AZIENDA
        i_commit = .f.
        if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
          cp_BeginTrs()
          i_commit = .t.
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
        i_ccchkf=''
        this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
        if i_nConn<>0
          i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
          +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_DATFI2),'AZIENDA','AZDATBLO');
              +i_ccchkf ;
          +" where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                 )
        else
          update (i_cTable) set;
              AZDATBLO = this.w_DATFI2;
              &i_ccchkf. ;
           where;
              AZCODAZI = this.oParentObject.w_CODAZI;

          i_Rows = _tally
        endif
        if i_commit
          cp_EndTrs(.t.)
        endif
        if bTrsErr
          i_Error=MSG_WRITE_ERROR
          return
        endif
        if this.w_TIPLIQ="R"
          * --- Se Riepilogativa, Inserisce <Blocco> in Tutte le Attivita'
          this.w_APPO = "V"
          * --- Write into ATTIDETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_DATFI2),'ATTIDETT','ATDATBLO');
                +i_ccchkf ;
            +" where ";
                +"ATTIPREG = "+cp_ToStrODBC(this.w_APPO);
                   )
          else
            update (i_cTable) set;
                ATDATBLO = this.w_DATFI2;
                &i_ccchkf. ;
             where;
                ATTIPREG = this.w_APPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_APPO = "A"
          * --- Write into ATTIDETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_DATFI2),'ATTIDETT','ATDATBLO');
                +i_ccchkf ;
            +" where ";
                +"ATTIPREG = "+cp_ToStrODBC(this.w_APPO);
                   )
          else
            update (i_cTable) set;
                ATDATBLO = this.w_DATFI2;
                &i_ccchkf. ;
             where;
                ATTIPREG = this.w_APPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_APPO = "C"
          * --- Write into ATTIDETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_DATFI2),'ATTIDETT','ATDATBLO');
                +i_ccchkf ;
            +" where ";
                +"ATTIPREG = "+cp_ToStrODBC(this.w_APPO);
                   )
          else
            update (i_cTable) set;
                ATDATBLO = this.w_DATFI2;
                &i_ccchkf. ;
             where;
                ATTIPREG = this.w_APPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
          this.w_APPO = "E"
          * --- Write into ATTIDETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_DATFI2),'ATTIDETT','ATDATBLO');
                +i_ccchkf ;
            +" where ";
                +"ATTIPREG = "+cp_ToStrODBC(this.w_APPO);
                   )
          else
            update (i_cTable) set;
                ATDATBLO = this.w_DATFI2;
                &i_ccchkf. ;
             where;
                ATTIPREG = this.w_APPO;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        else
          * --- Inserisce <Blocco> per Attivita'
          * --- Write into ATTIDETT
          i_commit = .f.
          if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
            cp_BeginTrs()
            i_commit = .t.
          endif
          i_nConn=i_TableProp[this.ATTIDETT_idx,3]
          i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
          i_ccchkf=''
          this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
          if i_nConn<>0
            i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
            +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(this.w_DATFI2),'ATTIDETT','ATDATBLO');
                +i_ccchkf ;
            +" where ";
                +"ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
                   )
          else
            update (i_cTable) set;
                ATDATBLO = this.w_DATFI2;
                &i_ccchkf. ;
             where;
                ATCODATT = this.oParentObject.w_CODATT;

            i_Rows = _tally
          endif
          if i_commit
            cp_EndTrs(.t.)
          endif
          if bTrsErr
            i_Error=MSG_WRITE_ERROR
            return
          endif
        endif
      else
        if NOT EMPTY(this.w_DATBLO2)
          * --- Raise
          i_Error="Archivio attivit� bloccato - verificare semaforo bollati in attivita"
          return
        else
          * --- Raise
          i_Error=this.w_MESS
          return
        endif
      endif
    else
      * --- Raise
      i_Error="Prima nota bloccata - verificare semaforo bollati in dati azienda"
      return
    endif
    * --- commit
    cp_EndTrs(.t.)
    return
  proc Try_00E94CD0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into AZIENDA
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.AZIENDA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.AZIENDA_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"AZDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'AZIENDA','AZDATBLO');
          +i_ccchkf ;
      +" where ";
          +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
             )
    else
      update (i_cTable) set;
          AZDATBLO = cp_CharToDate("  -  -  ");
          &i_ccchkf. ;
       where;
          AZCODAZI = this.oParentObject.w_CODAZI;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    if this.w_TIPLIQ="R"
      * --- Se Riepilogativa, Inserisce <Blocco> in Tutte le Attivita'
      this.w_APPO = "V"
      * --- Write into ATTIDETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'ATTIDETT','ATDATBLO');
            +i_ccchkf ;
        +" where ";
            +"ATTIPREG = "+cp_ToStrODBC(this.w_APPO);
               )
      else
        update (i_cTable) set;
            ATDATBLO = cp_CharToDate("  -  -  ");
            &i_ccchkf. ;
         where;
            ATTIPREG = this.w_APPO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_APPO = "A"
      * --- Write into ATTIDETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'ATTIDETT','ATDATBLO');
            +i_ccchkf ;
        +" where ";
            +"ATTIPREG = "+cp_ToStrODBC(this.w_APPO);
               )
      else
        update (i_cTable) set;
            ATDATBLO = cp_CharToDate("  -  -  ");
            &i_ccchkf. ;
         where;
            ATTIPREG = this.w_APPO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_APPO = "C"
      * --- Write into ATTIDETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'ATTIDETT','ATDATBLO');
            +i_ccchkf ;
        +" where ";
            +"ATTIPREG = "+cp_ToStrODBC(this.w_APPO);
               )
      else
        update (i_cTable) set;
            ATDATBLO = cp_CharToDate("  -  -  ");
            &i_ccchkf. ;
         where;
            ATTIPREG = this.w_APPO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      this.w_APPO = "E"
      * --- Write into ATTIDETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'ATTIDETT','ATDATBLO');
            +i_ccchkf ;
        +" where ";
            +"ATTIPREG = "+cp_ToStrODBC(this.w_APPO);
               )
      else
        update (i_cTable) set;
            ATDATBLO = cp_CharToDate("  -  -  ");
            &i_ccchkf. ;
         where;
            ATTIPREG = this.w_APPO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    else
      * --- Inserisce <Blocco> per Attivita'
      * --- Write into ATTIDETT
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"ATDATBLO ="+cp_NullLink(cp_ToStrODBC(cp_CharToDate("  -  -  ")),'ATTIDETT','ATDATBLO');
            +i_ccchkf ;
        +" where ";
            +"ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
               )
      else
        update (i_cTable) set;
            ATDATBLO = cp_CharToDate("  -  -  ");
            &i_ccchkf. ;
         where;
            ATCODATT = this.oParentObject.w_CODATT;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    return


  procedure Page_2
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_nIdx,i_cTempTable,i_ccchkf,i_ccchkv
    * --- Cicla Sui Cursori delle Queries e Calcola i valori per il Temporaneo di Elaborazione
     
 L_FLPROR=" " 
 L_PRA=0 
 L_PRB=0 
 L_PRP=0
    this.w_ALIAGR = 0
    SELECT ATTIVITA
    GO TOP
    SCAN FOR NOT EMPTY(NVL(CODATT," "))
    this.w_KEYATT = CODATT
    this.w_FLPROR = NVL(PERPRO," ")
    this.w_FLAGRI = NVL(FLAGRI, " ")
    this.w_PERPRO = 0
    this.w_OLDPRO = 0
    this.w_IVADEB = 0
    this.w_IVACRE = 0
    this.w_ALIAGR = 0
    this.w_IVACREC = 0
    this.w_IVACREA = 0
    this.w_IVACREB = 0
    this.w_IVACREC = 0
    * --- ...operazioni esenti
     
 Dimension aResult(6) 
 aResult[1]=0 
 aResult[2]=0 
 aResult[3]=0 
 aResult[4]=0 
 aResult[5]=0 
 aResult[6]=0
    GSCG_BAQ(this,this.w_KEYATT,this.w_VP__ANNO,@aResult)
    if i_retcode='stop' or !empty(i_Error)
      return
    endif
    this.w_VE37 = this.w_VE37+ aResult[1]
    this.w_VE38 = this.w_VE38 + aResult[2]
    this.w_VF16 = this.w_VF16+ aResult[3]
    this.w_VF17 = this.w_VF17+ aResult[4]
    this.w_INIANNO = cp_CharToDate("01-01"+"-"+ALLTRIM(this.w_VP__ANNO))
    this.w_FINANNO = cp_CharToDate("31-12"+"-"+ALLTRIM(this.w_VP__ANNO))
    this.w_OPPART = aResult[5]
    this.w_OPESENT = aResult[6]
    * --- Variabili per Stampa Liquidazione
    * --- Azzera dettagli delle Singole Attivita'
    * --- Legge Saldi Registri IVA (tranne Corrispettivi/Ventilazione, elaborati a parte)
    VQ_EXEC("QUERY\GSCG_BLA.VQR",this,"SALDIIVA")
    if USED("SALDIIVA")
      * --- %Prorata (Attenzione: se Dichiarazione Riepilogo ricalcola per le Attivita')
      if this.w_FLPROR="S"
        * --- Legge il Vecchio Prorata
        * --- Read from PRO_RATA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.PRO_RATA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.PRO_RATA_idx,2],.t.,this.PRO_RATA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AIPERPRO"+;
            " from "+i_cTable+" PRO_RATA where ";
                +"AICODATT = "+cp_ToStrODBC(this.w_KEYATT);
                +" and AI__ANNO = "+cp_ToStrODBC(this.oParentObject.w_ANNO);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AIPERPRO;
            from (i_cTable) where;
                AICODATT = this.w_KEYATT;
                and AI__ANNO = this.oParentObject.w_ANNO;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.w_OLDPRO = NVL(cp_ToDate(_read_.AIPERPRO),cp_NullValue(_read_.AIPERPRO))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_APPO1 = 0
        this.w_APPO2 = 0
        SELECT SALDIIVA
        GO TOP
        SCAN FOR NVL(TIPREG, "A")<>"A" AND NVL(PROIVA, " ") $ "SN"
        this.w_CODVAL = IIF(EMPTY(NVL(CODVAL,"")), g_PERVAL, CODVAL)
        this.w_APPO = (NVL(IMPSTA,0) + NVL(IMPSEG,0) + NVL(IMPIND,0)) - (NVL(IMPPRE,0) + NVL(IMPFAD,0))
        * --- Se La valuta del Periodo diversa da quella di Conto converte
        if this.w_APPO<>0 AND this.w_CODVAL<>g_PERVAL AND g_CAOEUR<>0 
          if this.w_CODVAL=g_CODLIR
            this.w_APPO = cp_ROUND(this.w_APPO/g_CAOEUR, g_PERPVL)
          else
            this.w_APPO = cp_ROUND(this.w_APPO*g_CAOEUR, g_PERPVL)
          endif
        endif
        this.w_APPO1 = this.w_APPO1 + IIF(PROIVA = "N", this.w_APPO, 0)
        this.w_APPO2 = this.w_APPO2 + IIF(PROIVA $ "SN", this.w_APPO, 0)
        ENDSCAN
        * --- Considero anche le fatture a esigibilit� differita per il calcolo prorata
        *     w_OPPART= Operazioni che partecipano al calcolo 
        *     w_OPESENT = Operazioni esenti
        this.w_APPO1 = this.w_APPO1 + this.w_OPPART
        this.w_APPO2 = this.w_APPO2 + this.w_OPPART + this.w_OPESENT
        * --- Se numeratore e Denominatore =0 -> Percentuale Prorata=100
        *     Se solo denominatore = 0  -> Percentuale Prorata =0 .....
        *     ....altrimenti calcolo la percentuale
        this.w_PERPRO = IIF(this.w_APPO1=0 And this.w_APPO2=0,100,IIF(this.w_APPO2=0,0,cp_ROUND((this.w_APPO1/this.w_APPO2) * 100, 3)))
        * --- % Prorata arrotondata all'intero superiore a .500
        this.w_PERPRO = IIF(this.w_PERPRO-INT(this.w_PERPRO)<=.500, INT(this.w_PERPRO), cp_ROUND(this.w_PERPRO, 0))
        * --- Assegno le variabili con valori per il calcolo Prorata da Passare al Report
        *     Solo se Per singola Attivit�
         
 L_PRA=cp_ROUND(this.w_APPO1, 3) 
 L_PRB=cp_ROUND(this.w_APPO2, 3) 
 L_PRP=this.w_PERPRO
        INSERT INTO DETPRO (CODATT, PERPRO) VALUES (this.w_KEYATT, this.w_PERPRO)
      endif
      * --- Se Dichiarazione Riepilogativa, evidenzia Credito al netto prorata solo se almeno una attivita' gestisce il Prorata
      if this.w_TIPLIQ="R"
        L_FLPROR=IIF(this.w_FLPROR="S", "S", L_FLPROR)
      else
        L_FLPROR=this.w_FLPROR
      endif
      * --- Se Dichiarazione Riepilogativa, la % Prorata non e' significativa essendo riferita a una attivita'
      L_PERPRO=IIF(this.w_TIPLIQ="R", 0, this.w_PERPRO)
      L_OLDPRO=IIF(this.w_TIPLIQ="R", 0, this.w_OLDPRO)
      * --- Cicla sul Cursore dei Saldi IVA
      SELECT SALDIIVA
      GO TOP
      SCAN FOR NVL(TIPREG," ") $ "VAC" AND NVL(NUMREG, 0)<>0 AND NOT EMPTY(NVL(CODIVA, " "))
      * --- Per Dettagli Liquidazione
      this.w_DESCRI = DESCRI
      this.w_TIPREG = TIPREG
      this.w_NUMREG = NUMREG
      this.w_CODVAL = IIF(EMPTY(NVL(CODVAL,"")), g_PERVAL, CODVAL)
      this.w_CODIVA = CODIVA
      this.w_DESIVA = NVL(DESIVA, " ")
      this.w_PERIVA = NVL(PERIVA, 0)
      this.w_PERIND = NVL(PERIND, 0)
      this.w_PERCOM = NVL(PERCOM, 0)
      this.w_IVSPLPAY = NVL(SPLPAY,"N")
      this.w_TIPAGR = NVL(TIPAGR, "N")
      this.w_TOTIMP = (NVL(IMPSTA,0)+NVL(IMPSEG,0)+NVL(IMPIND,0))-(NVL(IMPPRE,0)+NVL(IMPFAD,0))
      this.w_TOTIVD = (NVL(IVASTA,0)+NVL(IVASEG,0)+NVL(IVAIND,0))-(NVL(IVAPRE,0)+NVL(IVAFAD,0))
      this.w_TOTIVI = (NVL(IVISTA,0)+NVL(IVISEG,0)+NVL(IVIIND,0))-(NVL(IVIPRE,0)+NVL(IVIFAD,0))
      this.w_TOTMAC = (NVL(MACSTA,0)+NVL(MACSEG,0)+NVL(MACIND,0))-(NVL(MACPRE,0)+NVL(MACFAD,0))
      * --- Se La valuta del Periodo diversa da quella di Conto converte
      if this.w_CODVAL<>g_PERVAL AND g_CAOEUR<>0 
        if this.w_CODVAL=g_CODLIR
          this.w_TOTIMP = cp_ROUND(this.w_TOTIMP/g_CAOEUR, g_PERPVL)
          this.w_TOTIVD = cp_ROUND(this.w_TOTIVD/g_CAOEUR, g_PERPVL)
          this.w_TOTIVI = cp_ROUND(this.w_TOTIVI/g_CAOEUR, g_PERPVL)
          this.w_TOTMAC = cp_ROUND(this.w_TOTMAC/g_CAOEUR, g_PERPVL)
        else
          this.w_TOTIMP = cp_ROUND(this.w_TOTIMP*g_CAOEUR, g_PERPVL)
          this.w_TOTIVD = cp_ROUND(this.w_TOTIVD*g_CAOEUR, g_PERPVL)
          this.w_TOTIVI = cp_ROUND(this.w_TOTIVI*g_CAOEUR, g_PERPVL)
          this.w_TOTMAC = cp_ROUND(this.w_TOTMAC*g_CAOEUR, g_PERPVL)
        endif
      endif
      this.w_PERMAC = 0
      if this.w_TIPREG="A"
        * --- Totale IVA Detraibile a Credito 
        * --- IVA Detraibile a Credito del Periodo
        if this.w_FLAGRI="P" AND this.w_TIPAGR="A"
          * --- Aggiorna Totalizzatore IVA Acquisti Agevolata x Reg.IVA Agricola
          this.w_IVACREA = this.w_IVACREA + IIF(NVL(FLVAFF,"N")<>"E",((NVL(IVASTA,0)+NVL(IVASEG,0)+NVL(IVAIND,0))-(NVL(IVAPRE,0)+NVL(IVAFAD,0))),0)
        else
          * --- Se Attivita no Produttore Agricolo o Prod.Agricolo e Aliq.IVA non Agevolata)
          this.w_IVACRE = this.w_IVACRE + IIF(NVL(FLVAFF,"N")<>"E", this.w_TOTIVD, 0)
        endif
      else
        if this.w_TIPREG<>"V" OR (this.w_TIPREG="V" And NVL(SPLPAY,"N")<>"S")
          * --- Totale IVA a Debito (Tranne Corrispettivi da Ventilare, elaborati a parte)
          this.w_IVADEB = this.w_IVADEB + IIF(NVL(FLVAFF,"N")<>"E", (this.w_TOTIVD+this.w_TOTIVI), 0)
        endif
      endif
      this.w_APPO1 = 0
      if this.w_TIPREG $ "VC" AND this.w_TOTIMP<>0 AND this.w_FLAGRI="P" AND this.w_PERCOM>0
        * --- Se Tipo Reg.IVA Vendite o Corrisp.Scorporo e Attivita Produttore Agricolo e Aliq.IVA Compensazione, calcola IVA Detraibile 
        this.w_APPO1 = cp_ROUND(((this.w_TOTIMP * this.w_PERCOM) / 100), IIF(g_PERVAL=g_CODLIR, 0, 2))
        this.w_IVACREC = this.w_IVACREC + this.w_APPO1
        * --- Aggiorna Array per Riepilogo Stampa (se Liquodazione Periodica sing. Attivita')
        this.w_ALIAGR = this.w_ALIAGR + 1
        DETAGR[this.w_ALIAGR, 1] = this.w_PERCOM
        DETAGR[this.w_ALIAGR, 2] = this.w_TOTIMP
        DETAGR[this.w_ALIAGR, 3] = this.w_APPO1
      endif
      * --- Scrive il Cursore di Elaborazione
      SELECT LIQIVA
      GO TOP
      LOCATE FOR TIPREG=this.w_TIPREG AND NUMREG=this.w_NUMREG AND CODIVA=this.w_CODIVA
      if FOUND()
        REPLACE TOTIMP WITH (TOTIMP+this.w_TOTIMP), TOTIVD WITH (TOTIVD+this.w_TOTIVD), TOTIVI WITH (TOTIVI+this.w_TOTIVI), TOTMAC WITH (TOTMAC+this.w_TOTMAC),IMPAGR with (iif(this.w_TIPLIQ="R",this.w_IVACREC,this.w_IVACREC*0))
      else
         
 INSERT INTO LIQIVA (CODATT, DESCRI, TIPREG, NUMREG, CODIVA, PERIVA, PERIND, DESIVA, TOTIMP, TOTIVD, TOTIVI, TOTMAC, PERMAC, FLPROR, PERPRO, OLDPRO,FLAGRI, PERCOM, TIPAGR,IMPAGR,IVSPLPAY) ; 
 VALUES (this.w_KEYATT, this.w_DESCRI, this.w_TIPREG, this.w_NUMREG, this.w_CODIVA, this.w_PERIVA, this.w_PERIND, this.w_DESIVA, ; 
 this.w_TOTIMP, this.w_TOTIVD, this.w_TOTIVI, this.w_TOTMAC, this.w_PERMAC, this.w_FLPROR, this.w_PERPRO, this.w_OLDPRO,this.w_FLAGRI, this.w_PERCOM, this.w_TIPAGR,iif(this.w_TIPLIQ="R",this.w_APPO1,this.w_APPO1*0),this.w_IVSPLPAY)
      endif
      SELECT LIQIVA
      GO TOP
      SELECT SALDIIVA
      ENDSCAN
      * --- Se IVA Agricola: Eventuale IVA Detraibile NO Regime Speciale Agricolo
      this.w_IVACREB = this.w_IVACRE
      * --- Se IVA Agricola Aggiunge all'IVA a Credito l'eventuale Compensazione Vendite. 
      this.w_IVACRE = this.w_IVACRE + this.w_IVACREC
      this.w_TIPREG = "E"
      * --- Select from ATTIDETT
      i_nConn=i_TableProp[this.ATTIDETT_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2],.t.,this.ATTIDETT_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select ATNUMREG  from "+i_cTable+" ATTIDETT ";
            +" where ATCODATT="+cp_ToStrODBC(this.w_KEYATT)+" AND ATTIPREG="+cp_ToStrODBC(this.w_TIPREG)+"";
            +" order by ATNUMREG";
             ,"_Curs_ATTIDETT")
      else
        select ATNUMREG from (i_cTable);
         where ATCODATT=this.w_KEYATT AND ATTIPREG=this.w_TIPREG;
         order by ATNUMREG;
          into cursor _Curs_ATTIDETT
      endif
      if used('_Curs_ATTIDETT')
        select _Curs_ATTIDETT
        locate for 1=1
        do while not(eof())
        this.w_NUMREG = _Curs_ATTIDETT.ATNUMREG
        * --- Legge i Saldi dei Corrispettivi Ventilazione
        VQ_EXEC("QUERY\GSCGEBLA.VQR",this,"SALDIIVA")
        if USED("SALDIIVA")
          * --- Legge Corispettivi Ventilazione
          this.w_ULTPER = 0
          SELECT SALDIIVA
          GO TOP
          SCAN FOR NVL(TIPREG," ") = "E" AND NVL(NUMREG, 0)<>0 AND NOT EMPTY(NVL(CODIVA, " ")) AND NVL(NUMPER, 0)<>0
          * --- Per Dettagli Liquidazione
          this.w_DESCRI = DESCRI
          this.w_CODIVA = CODIVA
          this.w_DESIVA = NVL(DESIVA, " ")
          this.w_PERIVA = NVL(PERIVA, 0)
          this.w_PERIND = NVL(PERIND, 0)
          this.w_CODVAL = IIF(EMPTY(NVL(CODVAL,"")), g_PERVAL, CODVAL)
          this.w_TOTIMP = 0
          this.w_TOTIVD = 0
          this.w_TOTIVI = 0
          this.w_TOTMAC = (NVL(MACSTA,0)+NVL(MACSEG,0)+NVL(MACIND,0))-(NVL(MACPRE,0)+NVL(MACFAD,0))
          * --- Se La valuta del Periodo diversa da quella di Conto converte
          if this.w_TOTMAC<>0 AND this.w_CODVAL<>g_PERVAL AND g_CAOEUR<>0 
            if this.w_CODVAL=g_CODLIR
              this.w_TOTMAC = cp_ROUND(this.w_TOTMAC/g_CAOEUR, g_PERPVL)
            else
              this.w_TOTMAC = cp_ROUND(this.w_TOTMAC*g_CAOEUR, g_PERPVL)
            endif
          endif
          this.w_PERMAC = 0
          if NUMPER>=this.w_ULTPER
            this.w_ULTPER = NUMPER
            * --- Scrive il Cursore di Elaborazione (solo per il periodo piu' recente)
             
 INSERT INTO LIQIVA (CODATT, DESCRI, TIPREG, NUMREG, CODIVA, PERIVA, PERIND, DESIVA, TOTIMP, TOTIVD, ; 
 TOTIVI, TOTMAC, PERMAC, FLPROR, PERPRO, OLDPRO) ; 
 VALUES (this.w_KEYATT, this.w_DESCRI, this.w_TIPREG, this.w_NUMREG, this.w_CODIVA, this.w_PERIVA, this.w_PERIND, this.w_DESIVA, ; 
 this.w_TOTIMP, this.w_TOTIVD, this.w_TOTIVI, this.w_TOTMAC, this.w_PERMAC, this.w_FLPROR, this.w_PERPRO, this.w_OLDPRO)
            SELECT LIQIVA
            GO TOP
          endif
          SELECT SALDIIVA
          ENDSCAN
          * --- Riesegue la Ventilazione dei Totali Corrispettivi da ventilare dell'Anno per il Monte Acquisti dell'Ultima Liquidazione
          * --- Legge i Totali Corrispettivi da Ventilare
          this.w_CORVEN = 0
          VQ_EXEC("QUERY\GSCG1BLA.VQR",this,"SALDIVEN")
          if USED("SALDIVEN")
            SELECT SALDIVEN
            GO TOP
            SCAN FOR NUMREG<>0
            this.w_CODVAL = IIF(EMPTY(NVL(CODVAL,"")), g_PERVAL, CODVAL)
            this.w_APPO1 = NVL(IMPVEN, 0)
            * --- Se La valuta del Periodo diversa da quella di Conto converte
            if this.w_APPO1<>0 AND this.w_CODVAL<>g_PERVAL AND g_CAOEUR<>0 
              if this.w_CODVAL=g_CODLIR
                this.w_APPO1 = cp_ROUND(this.w_APPO1/g_CAOEUR, g_PERPVL)
              else
                this.w_APPO1 = cp_ROUND(this.w_APPO1*g_CAOEUR, g_PERPVL)
              endif
            endif
            this.w_CORVEN = this.w_CORVEN + this.w_APPO1
            ENDSCAN 
            USE
          endif
          SELECT LIQIVA
          GO TOP
          this.w_GLOMAC = 0
          * --- Calcola il Totale Monte Acquisti dei Corrispettivi in Ventilazione
          SUM TOTMAC TO this.w_GLOMAC FOR TIPREG="E" AND NUMREG=this.w_NUMREG AND TOTMAC>=0
          if this.w_GLOMAC<>0 AND this.w_CORVEN<>0
            * --- Calcola gli Importi per ciascuna aliquota (ATTENZIONE: se w_CORVEN<>0 e GLOMAC=0 deve esserci un errore!)
            this.w_APPO = 0
            SELECT LIQIVA
            GO TOP
            SCAN FOR TIPREG="E" AND NUMREG=this.w_NUMREG AND TOTMAC>=0
            this.w_TOTMAC = NVL(TOTMAC, 0)
            this.w_PERIVA = NVL(PERIVA, 0)
            this.w_PERIND = NVL(PERIND, 0)
            this.w_APPO1 = cp_ROUND((this.w_CORVEN*this.w_TOTMAC)/this.w_GLOMAC, g_PERPVL)
            this.w_TOTIVD = cp_ROUND(this.w_APPO1 - (this.w_APPO1/(1+(this.w_PERIVA/100))), g_PERPVL)
            this.w_TOTIVI = cp_ROUND((this.w_TOTIVD * this.w_PERIND) / 100, g_PERPVL)
            this.w_TOTIVD = this.w_TOTIVD - this.w_TOTIVI
            this.w_TOTIMP = this.w_APPO1 - (this.w_TOTIVD+this.w_TOTIVI)
            * --- Aggiorna i Totali IVA Dich. (a Debito)
            this.w_IVADEB = this.w_IVADEB + (this.w_TOTIVD+this.w_TOTIVI)
            * --- Per Gestione Resti
            this.w_APPO = this.w_APPO + (this.w_TOTIMP + this.w_TOTIVD + this.w_TOTIVI)
            REPLACE TOTIMP WITH this.w_TOTIMP, TOTIVD WITH this.w_TOTIVD, TOTIVI WITH this.w_TOTIVI 
            ENDSCAN
            if this.w_APPO<>this.w_CORVEN
              * --- Inserisce l'eventuale resto sulla prima % Monte Acquisti valorizzata
              GO TOP
              LOCATE FOR TIPREG="E" AND NUMREG=this.w_NUMREG AND TOTIVD<>0
              if FOUND()
                this.w_IVADEB = this.w_IVADEB + (this.w_CORVEN-this.w_APPO)
                this.w_TOTIVD = TOTIVD + (this.w_CORVEN-this.w_APPO)
                REPLACE TOTIVD WITH this.w_TOTIVD
              endif
            endif
            * --- Adesso ricalcola le percentuauali Monte Acquisti per ciascuna Aliquota
            this.w_APPO = 0
            SELECT LIQIVA
            GO TOP
            SCAN FOR TIPREG="E" AND NUMREG=this.w_NUMREG AND TOTMAC>=0
            this.w_PERMAC = cp_ROUND((TOTMAC * 100) / this.w_GLOMAC, 6)
            this.w_APPO = this.w_APPO + this.w_PERMAC
            REPLACE PERMAC WITH this.w_PERMAC
            ENDSCAN
            if this.w_APPO<>100
              * --- Inserisce l'eventuale resto sulla prima % Monte Acquisti valorizzata
              GO TOP
              LOCATE FOR TIPREG="E" AND NUMREG=this.w_NUMREG AND PERMAC<>0
              if FOUND()
                this.w_PERMAC = PERMAC + (100-this.w_APPO)
                REPLACE PERMAC WITH this.w_PERMAC
              endif
            endif
          endif
        endif
          select _Curs_ATTIDETT
          continue
        enddo
        use
      endif
      * --- Importi definiti in KiloLire o all'Unita' di Euro
      this.w_APPO = IIF(g_PERVAL=g_CODLIR, 1000, 1)
      this.w_VLIMPO04 = this.w_VLIMPO04 + (INTROUND(this.w_IVADEB, IIF(g_PERVAL=g_CODLIR, 1000, 1)) / this.w_APPO)
      this.w_VLIMPCRE = this.w_VLIMPCRE + (INTROUND(this.w_IVACRE, IIF(g_PERVAL=g_CODLIR, 1000, 1)) / this.w_APPO)
      * --- Se Gestito Prorata Storna IVA Detraibile a Credito per la % Prorata (Per ciascuna Attivita')
      if this.w_FLPROR="S" AND this.w_IVACRE<>0 AND this.w_PERPRO<>100
        this.w_IVACRE = cp_ROUND((this.w_IVACRE * this.w_PERPRO) / 100, g_PERPVL)
      endif
      this.w_VLIMPCRP = this.w_VLIMPCRP + (INTROUND(this.w_IVACRE, IIF(g_PERVAL=g_CODLIR, 1000, 1)) / this.w_APPO)
      this.w_VLIMPO07 = this.w_VLIMPO07 + (INTROUND(this.w_IVACRE, IIF(g_PERVAL=g_CODLIR, 1000, 1)) / this.w_APPO)
    endif
    SELECT ATTIVITA
    ENDSCAN
    * --- Se non ci sono records aggiunge un record fittizio per la stampa
    SELECT LIQIVA
    if RECCOUNT()=0
      INSERT INTO LIQIVA (CODATT, TIPREG, NUMREG, CODIVA) VALUES ("xxxxx", "B", 0, "     ")
    else
      * --- Regime Agricolo
      L_FLAGRI=IIF(this.oParentObject.w_MTIPLIQ="R", " ", this.w_FLAGRI)
      if this.oParentObject.w_MTIPLIQ<>"R" AND L_FLAGRI="P"
        * --- Se Attivita' Liquidazione IVA Agricola aggiunge il Dettaglio Acquisti IVA a Regime Agricolo
        * --- IVA Acquisti a Regime Speciale Agricolo
        INSERT INTO LIQIVA (CODATT, TIPREG, NUMREG, CODIVA, TOTIMP, TOTIVD) ;
        VALUES (this.w_KEYATT, "Z", -1, "     ", 0, this.w_IVACREA)
        * --- IVA Acquisti NO Regime Speciale Agricolo
        INSERT INTO LIQIVA (CODATT, TIPREG, NUMREG, CODIVA, TOTIMP, TOTIVD) ;
        VALUES (this.w_KEYATT, "Z", 0, "     ", 0, this.w_IVACREB)
        if this.w_ALIAGR>0
          * --- Vendite Aliquota con % Compensazione
          FOR L_I= 1 TO this.w_ALIAGR
          INSERT INTO LIQIVA (CODATT, TIPREG, NUMREG, CODIVA, TOTIMP, TOTIVD, PERCOM) ;
          VALUES (this.w_KEYATT, "Z", L_I, "     ", DETAGR[L_I, 2], DETAGR[L_I, 3], DETAGR[L_I, 1])
          ENDFOR
        endif
      endif
    endif
    * --- Alla Fine Raggruppa tutti i Valori presenti nel Cursore LIQIVA e  Ordina
     
 SELECT CODATT, MAX(DESCRI) AS DESCRI,TIPREG, NUMREG, CODIVA, MAX(DESIVA) AS DESIVA, ; 
 SUM(TOTIMP) AS TOTIMP, SUM(TOTIVD) AS TOTIVD, SUM(TOTIVI) AS TOTIVI, SUM(TOTMAC) AS TOTMAC, SUM(PERMAC) AS PERMAC, ; 
 MAX(FLPROR) AS FLPROR, MAX(PERPRO) AS PERPRO, MAX(OLDPRO) AS OLDPRO,MAX(FLAGRI) AS FLAGRI, MAX(PERCOM) AS PERCOM, MAX(TIPAGR) AS TIPAGR,SUM(IMPAGR) AS IMPAGR,MAX(IVSPLPAY) AS IVSPLPAY ; 
 FROM LIQIVA GROUP BY CODATT, TIPREG, NUMREG, CODIVA ORDER BY TIPREG,NUMREG, CODIVA INTO CURSOR LIQIVA noFilter
    * --- IVA Dovuta/Credito (se Positiva a Debito altrimenti a Credito)
    this.w_VLIMPO08 = this.w_VLIMPO04 - this.w_VLIMPCRP
    if g_TIPDEN<>"M" AND this.oParentObject.w_DENMAG<>0 AND NOT (g_ATTIVI="S" AND this.oParentObject.w_MTIPLIQ<>"R")
      * --- Se Riepilogativa o una sola Attivita' somma gli interessi dovuti dei trimestri precedenti
      * --- Select from IVA_PERI
      i_nConn=i_TableProp[this.IVA_PERI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.IVA_PERI_idx,2],.t.,this.IVA_PERI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select VPIMPO14  from "+i_cTable+" IVA_PERI ";
            +" where VP__ANNO="+cp_ToStrODBC(this.oParentObject.w_ANNO)+" AND VPKEYATT="+cp_ToStrODBC(this.w_VPKEYATT)+" AND VPPERIOD<>4";
            +" order by VPPERIOD";
             ,"_Curs_IVA_PERI")
      else
        select VPIMPO14 from (i_cTable);
         where VP__ANNO=this.oParentObject.w_ANNO AND VPKEYATT=this.w_VPKEYATT AND VPPERIOD<>4;
         order by VPPERIOD;
          into cursor _Curs_IVA_PERI
      endif
      if used('_Curs_IVA_PERI')
        select _Curs_IVA_PERI
        locate for 1=1
        do while not(eof())
        this.w_VLIMPO12 = this.w_VLIMPO12 + _Curs_IVA_PERI.VPIMPO14
          select _Curs_IVA_PERI
          continue
        enddo
        use
      endif
    endif
    * --- Importi Non Calcolati, Inseriti a mano
    this.w_VLIMPO13 = 0
    this.w_VLIMPO15 = 0
    this.w_VLIMPO19 = 0
    * --- Cicla su tutte le Liquidazioni Periodiche dell'Anno (Attivita' Selezionata o #### Riepilogativa)
    * --- Select from IVA_PERI
    i_nConn=i_TableProp[this.IVA_PERI_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.IVA_PERI_idx,2],.t.,this.IVA_PERI_idx)
    if i_nConn<>0
      cp_sqlexec(i_nConn,"select * from "+i_cTable+" IVA_PERI ";
          +" where VP__ANNO="+cp_ToStrODBC(this.w_VP__ANNO)+" AND VPKEYATT="+cp_ToStrODBC(this.w_VPKEYATT)+"";
          +" order by VPPERIOD";
           ,"_Curs_IVA_PERI")
    else
      select * from (i_cTable);
       where VP__ANNO=this.w_VP__ANNO AND VPKEYATT=this.w_VPKEYATT;
       order by VPPERIOD;
        into cursor _Curs_IVA_PERI
    endif
    if used('_Curs_IVA_PERI')
      select _Curs_IVA_PERI
      locate for 1=1
      do while not(eof())
      this.w_VPCRERIM = NVL(_Curs_IVA_PERI.VPCRERIM, 0)
      this.w_VPCREUTI = NVL(_Curs_IVA_PERI.VPCREUTI, 0)
      this.w_VPDICSOC = NVL(_Curs_IVA_PERI.VPDICSOC, " ")
      this.w_VPIMPO12 = NVL(_Curs_IVA_PERI.VPIMPO12, 0)
      this.w_VPIMPO13 = NVL(_Curs_IVA_PERI.VPIMPO13, 0)
      this.w_VPIMPO14 = NVL(_Curs_IVA_PERI.VPIMPO14, 0)
      this.w_VPIMPO15 = NVL(_Curs_IVA_PERI.VPIMPO15, 0)
      this.w_VPIMPO16 = NVL(_Curs_IVA_PERI.VPIMPO16, 0)
      this.w_VPIMPVER = NVL(_Curs_IVA_PERI.VPIMPVER, 0)
      this.w_VPCODVAL = NVL(_Curs_IVA_PERI.VPCODVAL, " ")
      this.w_VLIMPO19 = this.w_VLIMPO19 - NVL(_Curs_IVA_PERI.VPIMPOR8, 0)
      * --- Se La valuta dell'esercizio diversa da quella della Dichiarazione converte
      this.w_APPO = IIF(this.w_VPCODVAL="S", g_CODEUR, g_CODLIR)
      * --- Prevediamo solo LIRE o EURO
      if this.w_APPO<>g_PERVAL AND g_CAOEUR<>0 
        do case
          case this.w_APPO=g_CODLIR
            * --- Converte in Euro
            this.w_VPCRERIM = cp_ROUND((this.w_VPCRERIM*1000) / g_CAOEUR, g_PERPVL)
            this.w_VPCREUTI = cp_ROUND((this.w_VPCREUTI*1000) / g_CAOEUR, g_PERPVL)
            this.w_VPIMPO12 = cp_ROUND((this.w_VPIMPO12*1000) / g_CAOEUR, g_PERPVL)
            this.w_VPIMPO13 = cp_ROUND((this.w_VPIMPO13*1000) / g_CAOEUR, g_PERPVL)
            this.w_VPIMPO14 = cp_ROUND((this.w_VPIMPO14*1000) / g_CAOEUR, g_PERPVL)
            this.w_VPIMPO15 = cp_ROUND((this.w_VPIMPO15*1000) / g_CAOEUR, g_PERPVL)
            this.w_VPIMPO16 = cp_ROUND((this.w_VPIMPO16*1000) / g_CAOEUR, g_PERPVL)
            this.w_VPIMPVER = cp_ROUND((this.w_VPIMPVER*1000) / g_CAOEUR, g_PERPVL)
          case this.w_APPO=g_CODEUR
            * --- Converte in LIRE (?)
            this.w_VPCRERIM = cp_ROUND(this.w_VPCRERIM*g_CAOEUR, g_PERPVL)
            this.w_VPCREUTI = cp_ROUND(this.w_VPCREUTI*g_CAOEUR, g_PERPVL)
            this.w_VPIMPO12 = cp_ROUND(this.w_VPIMPO12*g_CAOEUR, g_PERPVL)
            this.w_VPIMPO14 = cp_ROUND(this.w_VPIMPO14*g_CAOEUR, g_PERPVL)
            this.w_VPIMPO13 = cp_ROUND(this.w_VPIMPO13*g_CAOEUR, g_PERPVL)
            this.w_VPIMPO15 = cp_ROUND(this.w_VPIMPO15*g_CAOEUR, g_PERPVL)
            this.w_VPIMPO16 = cp_ROUND(this.w_VPIMPO16*g_CAOEUR, g_PERPVL)
            this.w_VPIMPVER = cp_ROUND(this.w_VPIMPVER*g_CAOEUR, g_PERPVL)
        endcase
      endif
      * --- VL20 - Rimborsi Infraannuali (art. 38 - bis comma 2)
      this.w_VLIMPO09 = this.w_VLIMPO09 + this.w_VPCRERIM
      * --- VL23 - Credito IVA Risultante dai primi 3 Trimestri Compensata mod F4
      this.w_VLIMPO23 = this.w_VLIMPO23 + this.w_VPCREUTI
      * --- VL21 - Totale Crediti Trasferiti
      this.w_VLIMPO10 = this.w_VLIMPO10 + IIF(this.w_VPDICSOC="S" AND this.w_VPIMPO12<0, ABS(this.w_VPIMPO12), 0)
      * --- VL24 - Interessi per le Liquidazioni Trimestrali
      if g_ATTIVI="S" AND this.oParentObject.w_MTIPLIQ<>"R"
        * --- Calcolato su somma delle precedenti dichiarazioni se non riepilogativa o singola attivita'
        * --- In caso contrario e' calcolato subito all'inizio da VLIMPO08
        this.w_VLIMPO12 = this.w_VLIMPO12 + this.w_VPIMPO14
      endif
      * --- VL28 - Crediti d'Imposta utilizzati ...
      this.w_VLIMPO16 = this.w_VLIMPO16 + this.w_VPIMPO13
      * --- VL29 - Totale Versamenti Periodici Incluso l'Acconto ...
      this.w_VLIMPO17 = this.w_VLIMPO17 + (this.w_VPIMPVER+this.w_VPIMPO15)
      * --- VL30 - Ammontare dei Debiti Trasferiti
      this.w_VLIMPO18 = this.w_VLIMPO18 + IIF(this.w_VPDICSOC="S", this.w_VPIMPO16, 0)
        select _Curs_IVA_PERI
        continue
      enddo
      use
    endif
    if g_PERVAL=g_CODEUR
      * --- Converte all'unita' di EURO
      this.w_VLIMPO09 = INTROUND(this.w_VLIMPO09, 1)
      this.w_VLIMPO23 = INTROUND(this.w_VLIMPO23, 1)
      this.w_VLIMPO10 = INTROUND(this.w_VLIMPO10, 1)
      this.w_VLIMPO12 = INTROUND(this.w_VLIMPO12, 1)
      this.w_VLIMPO16 = INTROUND(this.w_VLIMPO16, 1)
      this.w_VLIMPO17 = INTROUND(this.w_VLIMPO17, 1)
      this.w_VLIMPO18 = INTROUND(this.w_VLIMPO18, 1)
    endif
    * --- VL32 - IVA a Credito
    this.w_VLIMPO20 = 0
    * --- VL33 - IVA a Debito
    this.w_VLIMPO21 = 0
    this.w_APPO1 = this.w_VLIMPO09 + this.w_VLIMPO10 + this.w_VLIMPO11 + this.w_VLIMPO12 + this.w_VLIMPO13 + this.w_VLIMPO23
    this.w_APPO2 = this.w_VLIMPO14 + this.w_VLIMPO15 + this.w_VLIMPO16 + this.w_VLIMPO17 + this.w_VLIMPO18 + this.w_VLIMPO19
    this.w_TOTALE = (this.w_VLIMPO08 + this.w_APPO1) - this.w_APPO2
    if this.w_TOTALE<0
      * --- Credito
      this.w_VLIMPO20 = ABS(this.w_TOTALE)
    else
      * --- Debito
      this.w_VLIMPO21 = this.w_TOTALE
    endif
    this.w_VPCODVAL = IIF(g_PERVAL=g_CODEUR, "S", " ")
    this.w_VLIMPO36 = IIF(this.w_TOTALE>=0 AND g_TIPDEN<>"M" AND this.oParentObject.w_DENMAG<>0,IIF(g_PERVAL=g_CODEUR,INTROUND(cp_ROUND((this.w_TOTALE*this.oParentObject.w_DENMAG)/100,g_PERPVL),1),cp_ROUND((this.w_TOTALE*this.oParentObject.w_DENMAG)/100,g_PERPVL)), 0)
    if g_ATTIVI="S" AND this.oParentObject.w_MTIPLIQ<>"R"
      * --- Se gestite piu' Attivita' e selezionata una sola Attivita' stampa direttamente la Liquidazione
      this.w_CONFER = "S"
      this.w_FLTEST = this.oParentObject.w_MFLTEST
    else
      * --- VPRORATA � utilizzata per nascondere il Prorata nella maschera se tutte le attivit� non hanno il flag Prorata attivato
      this.w_VPRORATA = L_FLPROR
      * --- ANNDIC � utilizzata per le etichette della maschera
      this.w_ANNDIC = this.oParentObject.w_ANNO
      do gscg_sia with this
      if i_retcode='stop' or !empty(i_Error)
        return
      endif
    endif
    * --- Stampa Liquidazione Annuale
    this.w_PAGINE = 0
    if this.oParentObject.w_MTIPLIQ="R"
      L_Attivita="Riepilogo Attivit�"
    else
      * --- Applico Codice Attivit� alternativo
      *     Esiste procedura di conversione che crea record di default cod codice attivit�
      *     pari al codice attivit� padre
      this.w_LCODATT = this.oParentObject.w_CODATT
      this.w_LDESATT = this.oParentObject.w_DESATT
      * --- Select from ATT_ALTE
      i_nConn=i_TableProp[this.ATT_ALTE_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ATT_ALTE_idx,2],.t.,this.ATT_ALTE_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select CAATTIVI,CADESCRI  from "+i_cTable+" ATT_ALTE ";
            +" where CADATATT<="+cp_ToStrODBC(this.w_INIANNO)+" AND CACODATT="+cp_ToStrODBC(this.oParentObject.w_CODATT)+"";
            +" order by CADATATT Desc";
             ,"_Curs_ATT_ALTE")
      else
        select CAATTIVI,CADESCRI from (i_cTable);
         where CADATATT<=this.w_INIANNO AND CACODATT=this.oParentObject.w_CODATT;
         order by CADATATT Desc;
          into cursor _Curs_ATT_ALTE
      endif
      if used('_Curs_ATT_ALTE')
        select _Curs_ATT_ALTE
        locate for 1=1
        do while not(eof())
        this.w_LCODATT = Nvl(_Curs_ATT_ALTE.CAATTIVI,Space(8))
        this.w_LDESATT = Nvl(_Curs_ATT_ALTE.CADESCRI,Space(35))
        Exit
          select _Curs_ATT_ALTE
          continue
        enddo
        use
      endif
      L_Attivita=this.w_LCODATT + " " + this.w_LDESATT
    endif
    if this.w_CONFER="S"
      * --- Aggiorna Archivi se Liquidazione Definitiva
      * --- L_Stampato viene messo a 1 dai Report
      L_STAMPATO=0
      * --- Passo le variabili presenti sulla maschera ai vari report di stampa.
      L_VLIMPO04=this.w_VLIMPO04
      L_VLIMPCRE=this.w_VLIMPCRE
      L_VLIMPCRP=this.w_VLIMPCRP
      L_VLIMPO07=this.w_VLIMPO07
      L_VLIMPO08=this.w_VLIMPO08
      L_VLIMPO09=this.w_VLIMPO09
      L_VLIMPO10=this.w_VLIMPO10
      L_VLIMPO11=this.w_VLIMPO11
      L_VLIMPO12=this.w_VLIMPO12
      L_VLIMPO13=this.w_VLIMPO13
      L_VLIMPO14=this.w_VLIMPO14
      L_VLIMPO15=this.w_VLIMPO15
      L_VLIMPO16=this.w_VLIMPO16
      L_VLIMPO17=this.w_VLIMPO17
      L_VLIMPO18=this.w_VLIMPO18
      L_VLIMPO19=this.w_VLIMPO19
      L_VLIMPO23=this.w_VLIMPO23
      L_TOTALE=this.w_TOTALE
      L_ANNO=this.oParentObject.w_ANNO
      L_VLIMPO20=this.w_VLIMPO20
      L_VLIMPO21=this.w_VLIMPO21
      L_IMPVAL=g_PERVAL
      L_DENMAGPI=this.w_DENMAGPI
      * --- PER NUMERAZIONE PAGINE IN TESTATA
      * --- Read from AZIENDA
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.AZIENDA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI"+;
          " from "+i_cTable+" AZIENDA where ";
              +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          AZINDAZI,AZLOCAZI,AZCAPAZI,AZPROAZI,AZPIVAZI,AZCOFAZI;
          from (i_cTable) where;
              AZCODAZI = this.oParentObject.w_CODAZI;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_INDAZI = NVL(cp_ToDate(_read_.AZINDAZI),cp_NullValue(_read_.AZINDAZI))
        this.w_LOCAZI = NVL(cp_ToDate(_read_.AZLOCAZI),cp_NullValue(_read_.AZLOCAZI))
        this.w_CAPAZI = NVL(cp_ToDate(_read_.AZCAPAZI),cp_NullValue(_read_.AZCAPAZI))
        this.w_PROAZI = NVL(cp_ToDate(_read_.AZPROAZI),cp_NullValue(_read_.AZPROAZI))
        this.w_PIVAZI = NVL(cp_ToDate(_read_.AZPIVAZI),cp_NullValue(_read_.AZPIVAZI))
        this.w_COFAZI = NVL(cp_ToDate(_read_.AZCOFAZI),cp_NullValue(_read_.AZCOFAZI))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      L_PAGINE=0
      L_PRPARI=this.oParentObject.w_PRPARI
      L_INTLIG=this.oParentObject.w_INTLIG
      L_PREFIS=this.oParentObject.w_PREFIS
      L_INDAZI=this.w_INDAZI
      L_LOCAZI=this.w_LOCAZI
      L_CAPAZI=this.w_CAPAZI
      L_PROAZI=this.w_PROAZI
      L_COFAZI=this.w_COFAZI
      L_PIVAZI=this.w_PIVAZI
      L_ATNUMREG= this.w_PRINUM
      L_ATTIPREG= this.w_PRIREG
      * --- Stampa Normale (Prima Solo Testo seconda Grafica)
      if this.w_TIPLIQ="R" 
        * --- Ricalcolo l'attivit� da stampare
        SELECT distinct CODATT as CODATTP, MAX(DESCRI) as DESCRIS, MAX(left(CODATT+space(8),8)) as CODATTS FROM LIQIVA INTO CURSOR LIQIVA_CODATT group by CODATTP 
 wrLC=wrcursor("LIQIVA_CODATT")
        this.w_DATINI = CTOD("01-01-"+this.oParentObject.w_ANNO)
        Select LIQIVA_CODATT 
 SCAN FOR NOT EMPTY(CODATTP)
        this.w_CODATTS = LIQIVA_CODATT.CODATTP
        * --- Select from ATT_ALTE
        i_nConn=i_TableProp[this.ATT_ALTE_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.ATT_ALTE_idx,2],.t.,this.ATT_ALTE_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select CAATTIVI,CADESCRI  from "+i_cTable+" ATT_ALTE ";
              +" where CADATATT<="+cp_ToStrODBC(this.w_DATINI)+" AND CACODATT="+cp_ToStrODBC(this.w_CODATTS)+"";
              +" order by CADATATT Desc";
               ,"_Curs_ATT_ALTE")
        else
          select CAATTIVI,CADESCRI from (i_cTable);
           where CADATATT<=this.w_DATINI AND CACODATT=this.w_CODATTS;
           order by CADATATT Desc;
            into cursor _Curs_ATT_ALTE
        endif
        if used('_Curs_ATT_ALTE')
          select _Curs_ATT_ALTE
          locate for 1=1
          do while not(eof())
          Select LIQIVA_CODATT 
 replace CODATTS with Nvl(_Curs_ATT_ALTE.CAATTIVI,Space(8)) 
 replace DESCRIS with Nvl(_Curs_ATT_ALTE.CADESCRI,Space(35)) 
 Select _Curs_ATT_ALTE
          Exit
            select _Curs_ATT_ALTE
            continue
          enddo
          use
        endif
        Select LIQIVA_CODATT
        ENDSCAN
        Select * From LIQIVA left join LIQIVA_CODATT on LIQIVA.CODATT=LIQIVA_CODATT.CODATTP Into Cursor LIQIVA NoFilter
        if Used("LIQIVA_CODATT")
          Select LIQIVA_CODATT 
 USE
        endif
        * --- Stampa Riepilogativa (Prima Solo Testo seconda Grafica)
         
 Select CODATT, DESCRI, ; 
 SUM(IIF(TIPREG="A" AND (FLAGRI<>"P" OR TIPAGR<>"A"), IIF(FLPROR="S" AND PERPRO<>100 AND TOTIVD<>0, ROUND((TOTIVD*PERPRO)/100, g_PERPVL), TOTIVD), Nvl(IMPAGR,0)+TOTIVD*0)) AS IVACRE, ; 
 SUM(IIF(TIPREG<>"A" AND ( (TIPREG="V" AND IVSPLPAY<>"S") OR TIPREG<>"V"),TOTIVI+TOTIVD,TOTIVD* 0)) AS IVADEB, MAX(FLPROR) AS FLPROR, CODATTS, DESCRIS ; 
 From LIQIVA GROUP BY CODATT, DESCRI Into Cursor APPO ORDER BY CODATT NoFilter
        * --- Inserisco riga vuota con ORDINE = 2 per problemi di stampa. Raggruppamento su Ordine in pagina nuova stampa i totali.
        *     ORDINE = 1 le normali registraizoni Iva.
        *     '{' in TIPREG per avere questa riga in fondo al cursore poich� TIPREG pu� assumere 'Z'
        SELECT * , 1 AS ORDINE FROM APPO INTO CURSOR LIQIVA
        WRCURSOR("LIQIVA")
        SELECT LIQIVA
        GO BOTTOM
        INSERT INTO LIQIVA;
        (CODATT, DESCRI, IVACRE, IVADEB, ;
        FLPROR, ORDINE) ;
        VALUES ;
        ("{", " " , 0, 0, ;
        " ", 2) 
        SELECT LIQIVA
        GO TOP
         SELECT * From LIQIVA Into Cursor __TMP__ ORDER BY CODATT NoFilter
        if this.w_FLTEST="S"
          Cp_ChPrn("QUERY\LIQANNR1.FRX", " ", this)
        else
          Cp_ChPrn("QUERY\LIQANNR.FRX", " ", this)
        endif
        this.w_PAGINE = L_PAGINE+this.oParentObject.w_PRPARI
      else
        * --- Inserisco riga vuota con ORDINE = 2 per problemi di stampa. Raggruppamento su Ordine in pagina nuova stampa i totali.
        *     ORDINE = 1 le normali registraizoni Iva.
        *     '{' in TIPREG per avere questa riga in fondo al cursore poich� TIPREG pu� assumere 'Z'
        SELECT * , 1 AS ORDINE FROM LIQIVA INTO CURSOR LIQIVA
        WRCURSOR("LIQIVA")
        SELECT LIQIVA
        GO BOTTOM
        INSERT INTO LIQIVA;
        (CODATT, DESCRI,TIPREG, NUMREG, CODIVA, DESIVA, ;
        TOTIMP, TOTIVD, TOTIVI, TOTMAC, PERMAC, ;
        FLPROR, PERPRO, OLDPRO, ORDINE) ;
        VALUES ;
        (SPACE(5), " ","{", 0, " " , " ", ;
        0, 0, 0, 0, 0, ;
        " ", 0, 0, 2) 
        SELECT LIQIVA
        GO TOP
        * --- Stampa Normale (Prima Solo Testo seconda Grafica)
        Select * From LIQIVA Into Cursor __Tmp__ NoFilter
        if this.w_FLTEST="S"
          Cp_ChPrn("QUERY\LIQANN1.FRX", " ", this)
        else
          Cp_ChPrn("QUERY\LIQANN.FRX", " ", this)
        endif
        this.w_PAGINE = L_PAGINE+this.oParentObject.w_PRPARI
      endif
      * --- Aggiornamento Archivi (se Definitiva)
      if this.oParentObject.w_FLDEFI="S" 
        * --- Try
        local bErr_00E4DFC0
        bErr_00E4DFC0=bTrsErr
        this.Try_00E4DFC0()
        * --- Catch
        if !empty(i_Error)
          i_ErrMsg=i_Error
          i_Error=''
          * --- rollback
          bTrsErr=.t.
          cp_EndTrs(.t.)
          ah_ErrorMsg("Errore durante elaborazione: %1. Operazione abbandonata",,"",Message())
        endif
        bTrsErr=bTrsErr or bErr_00E4DFC0
        * --- End
      endif
    else
      ah_ErrorMsg("Elaborazione interrotta",,"")
    endif
  endproc
  proc Try_00E4DFC0()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- begin transaction
    cp_BeginTrs()
    * --- 1) Percentuale Prorata
    if USED("DETPRO")
      SELECT DETPRO
      GO TOP
      SCAN FOR NOT EMPTY(NVL(CODATT," "))
      this.w_APPO = CODATT
      this.w_APPO1 = ALLTRIM(STR(VAL(this.oParentObject.w_ANNO)+1,4,0))
      this.w_APPO2 = NVL(PERPRO, 0)
      * --- Try
      local bErr_00E42808
      bErr_00E42808=bTrsErr
      this.Try_00E42808()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_00E42808
      * --- End
      * --- Write into PRO_RATA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.PRO_RATA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.PRO_RATA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.PRO_RATA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"AIPERPRO ="+cp_NullLink(cp_ToStrODBC(this.w_APPO2),'PRO_RATA','AIPERPRO');
            +i_ccchkf ;
        +" where ";
            +"AICODATT = "+cp_ToStrODBC(this.w_APPO);
            +" and AI__ANNO = "+cp_ToStrODBC(this.w_APPO1);
               )
      else
        update (i_cTable) set;
            AIPERPRO = this.w_APPO2;
            &i_ccchkf. ;
         where;
            AICODATT = this.w_APPO;
            and AI__ANNO = this.w_APPO1;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
      SELECT DETPRO
      ENDSCAN
    endif
    * --- 2) Riporta il Credito iniziale per il Periodo Successivo (anno)
    if this.w_VLIMPO20<>0 AND (g_ATTIVI<>"S" OR this.w_TIPLIQ="R")
      * --- Attenzione: Se Esercizio in Lire,  gli Importi di VL32 sono espressi in KLire
      this.w_APPO = STR(VAL(this.oParentObject.w_ANNO)+1, 4, 0)
      this.w_APPO2 = CALCESER(cp_CharToDate("01-01-"+ALLTRIM(this.w_APPO)), g_CODESE)
      * --- Read from ESERCIZI
      i_nOldArea=select()
      if used('_read_')
        select _read_
        use
      endif
      i_nConn=i_TableProp[this.ESERCIZI_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.ESERCIZI_idx,2],.t.,this.ESERCIZI_idx)
      if i_nConn<>0
        cp_sqlexec(i_nConn,"select "+;
          "ESVALNAZ"+;
          " from "+i_cTable+" ESERCIZI where ";
              +"ESCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
              +" and ESCODESE = "+cp_ToStrODBC(this.w_APPO2);
               ,"_read_")
        i_Rows=iif(used('_read_'),reccount(),0)
      else
        select;
          ESVALNAZ;
          from (i_cTable) where;
              ESCODAZI = this.oParentObject.w_CODAZI;
              and ESCODESE = this.w_APPO2;
           into cursor _read_
        i_Rows=_tally
      endif
      if used('_read_')
        locate for 1=1
        this.w_VALSUC = NVL(cp_ToDate(_read_.ESVALNAZ),cp_NullValue(_read_.ESVALNAZ))
        use
      else
        * --- Error: sql sentence error.
        i_Error = MSG_READ_ERROR
        return
      endif
      select (i_nOldArea)
      this.w_VALSUC = IIF(EMPTY(this.w_VALSUC), g_PERVAL, this.w_VALSUC)
      if g_PERVAL=this.w_VALSUC OR i_Rows = 0
        this.w_APPO1 = cp_ROUND(this.w_VLIMPO20 * IIF(g_PERVAL=g_CODLIR, 1000, 1), IIF(this.w_VALSUC=g_CODEUR, 2, 0))
      else
        if this.w_VALSUC= g_CODEUR
          this.w_APPO1 = cp_ROUND(this.w_VLIMPO20*1000/g_CAOEUR, 2)
        else
          this.w_APPO1 = cp_ROUND(this.w_VLIMPO20*g_CAOEUR, 0)
        endif
      endif
      * --- Try
      local bErr_00E46A50
      bErr_00E46A50=bTrsErr
      this.Try_00E46A50()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- accept error
        bTrsErr=.f.
      endif
      bTrsErr=bTrsErr or bErr_00E46A50
      * --- End
      * --- Write into CREDMIVA
      i_commit = .f.
      if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
        cp_BeginTrs()
        i_commit = .t.
      endif
      i_nConn=i_TableProp[this.CREDMIVA_idx,3]
      i_cTable=cp_SetAzi(i_TableProp[this.CREDMIVA_idx,2])
      i_ccchkf=''
      this.SetCCCHKVarsWrite(@i_ccchkf,this.CREDMIVA_idx,i_nConn)
      if i_nConn<>0
        i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
        +"CICREINI ="+cp_NullLink(cp_ToStrODBC(this.w_APPO1),'CREDMIVA','CICREINI');
        +",CIVALINI ="+cp_NullLink(cp_ToStrODBC(this.w_VALSUC),'CREDMIVA','CIVALINI');
            +i_ccchkf ;
        +" where ";
            +"CI__ANNO = "+cp_ToStrODBC(this.w_APPO);
               )
      else
        update (i_cTable) set;
            CICREINI = this.w_APPO1;
            ,CIVALINI = this.w_VALSUC;
            &i_ccchkf. ;
         where;
            CI__ANNO = this.w_APPO;

        i_Rows = _tally
      endif
      if i_commit
        cp_EndTrs(.t.)
      endif
      if bTrsErr
        i_Error=MSG_WRITE_ERROR
        return
      endif
    endif
    * --- 3) Aggionamento Progressivo pagine ATTIVITA'
    if this.oParentObject.w_INTLIG="S"
      this.w_PAGINE = IIF(this.w_PAGINE=0,this.oParentObject.w_PRPARI,this.w_PAGINE)
      this.oParentObject.w_PRPARI = this.w_PAGINE
      * --- In caso di Liquidazione Annuale Riepilogativa, recupero l'attivit� principale per aggiornamento numero pagine stampate
      this.w_LETAZI = .F.
      if this.oParentObject.w_MTIPLIQ="R" AND this.oParentObject.w_CODATT=SPACE(5)
        * --- Read from AZIENDA
        i_nOldArea=select()
        if used('_read_')
          select _read_
          use
        endif
        i_nConn=i_TableProp[this.AZIENDA_idx,3]
        i_cTable=cp_SetAzi(i_TableProp[this.AZIENDA_idx,2],.t.,this.AZIENDA_idx)
        if i_nConn<>0
          cp_sqlexec(i_nConn,"select "+;
            "AZCATAZI"+;
            " from "+i_cTable+" AZIENDA where ";
                +"AZCODAZI = "+cp_ToStrODBC(this.oParentObject.w_CODAZI);
                 ,"_read_")
          i_Rows=iif(used('_read_'),reccount(),0)
        else
          select;
            AZCATAZI;
            from (i_cTable) where;
                AZCODAZI = this.oParentObject.w_CODAZI;
             into cursor _read_
          i_Rows=_tally
        endif
        if used('_read_')
          locate for 1=1
          this.oParentObject.w_CODATT = NVL(cp_ToDate(_read_.AZCATAZI),cp_NullValue(_read_.AZCATAZI))
          use
        else
          * --- Error: sql sentence error.
          i_Error = MSG_READ_ERROR
          return
        endif
        select (i_nOldArea)
        this.w_LETAZI = .T.
      endif
      * --- Try
      local bErr_00E4A800
      bErr_00E4A800=bTrsErr
      this.Try_00E4A800()
      * --- Catch
      if !empty(i_Error)
        i_ErrMsg=i_Error
        i_Error=''
        * --- transaction error
        bTrsErr=.t.
        i_TrsMsg=CIAO
      endif
      bTrsErr=bTrsErr or bErr_00E4A800
      * --- End
      * --- In caso di Liquidazione Periodica Riepilogativa, sbianco la variabile attivit� letta dai Dati Azienda
      if this.oParentObject.w_MTIPLIQ="R" AND this.w_LETAZI
        this.oParentObject.w_CODATT = SPACE(5)
      endif
    endif
    * --- commit
    cp_EndTrs(.t.)
    ah_ErrorMsg("Elaborazione completata",,"")
    return
  proc Try_00E42808()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into PRO_RATA
    i_nConn=i_TableProp[this.PRO_RATA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.PRO_RATA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.PRO_RATA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"AICODATT"+",AI__ANNO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_APPO),'PRO_RATA','AICODATT');
      +","+cp_NullLink(cp_ToStrODBC(this.w_APPO1),'PRO_RATA','AI__ANNO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'AICODATT',this.w_APPO,'AI__ANNO',this.w_APPO1)
      insert into (i_cTable) (AICODATT,AI__ANNO &i_ccchkf. );
         values (;
           this.w_APPO;
           ,this.w_APPO1;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_00E46A50()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Insert into CREDMIVA
    i_nConn=i_TableProp[this.CREDMIVA_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.CREDMIVA_idx,2])
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_ccchkf=''
    i_ccchkv=''
    this.SetCCCHKVarsInsert(@i_ccchkf,@i_ccchkv,this.CREDMIVA_idx,i_nConn)
    if i_nConn<>0
      i_Rows=cp_TrsSQL(i_nConn,"insert into "+i_cTable+;
                  " ("+"CI__ANNO"+i_ccchkf+") values ("+;
      cp_NullLink(cp_ToStrODBC(this.w_APPO),'CREDMIVA','CI__ANNO');
           +i_ccchkv+")")
    else
      cp_CheckDeletedKey(i_cTable,0,'CI__ANNO',this.w_APPO)
      insert into (i_cTable) (CI__ANNO &i_ccchkf. );
         values (;
           this.w_APPO;
           &i_ccchkv. )
      i_Rows=iif(bTrsErr,0,1)
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if i_Rows<0 or bTrsErr
      * --- Error: insert not accepted
      i_Error=MSG_INSERT_ERROR
      return
    endif
    return
  proc Try_00E4A800()
    local i_nConn,i_cTable,i_nOldArea,i_ErrMsg,i_ccchkf,i_ccchkv
    * --- Write into ATTIDETT
    i_commit = .f.
    if vartype(nTrsConnCnt)="U" or nTrsConnCnt=0
      cp_BeginTrs()
      i_commit = .t.
    endif
    i_nConn=i_TableProp[this.ATTIDETT_idx,3]
    i_cTable=cp_SetAzi(i_TableProp[this.ATTIDETT_idx,2])
    i_ccchkf=''
    this.SetCCCHKVarsWrite(@i_ccchkf,this.ATTIDETT_idx,i_nConn)
    if i_nConn<>0
      i_Rows = cp_TrsSQL(i_nConn,"update "+i_cTable+" set ";
      +"ATPRPARI ="+cp_NullLink(cp_ToStrODBC(this.w_PAGINE),'ATTIDETT','ATPRPARI');
          +i_ccchkf ;
      +" where ";
          +"ATCODATT = "+cp_ToStrODBC(this.oParentObject.w_CODATT);
          +" and ATNUMREG = "+cp_ToStrODBC(this.oParentObject.w_ATNUMREG);
          +" and ATTIPREG = "+cp_ToStrODBC(this.oParentObject.w_ATTIPREG);
             )
    else
      update (i_cTable) set;
          ATPRPARI = this.w_PAGINE;
          &i_ccchkf. ;
       where;
          ATCODATT = this.oParentObject.w_CODATT;
          and ATNUMREG = this.oParentObject.w_ATNUMREG;
          and ATTIPREG = this.oParentObject.w_ATTIPREG;

      i_Rows = _tally
    endif
    if i_commit
      cp_EndTrs(.t.)
    endif
    if bTrsErr
      i_Error=MSG_WRITE_ERROR
      return
    endif
    return

  proc Pag1()
      this.Page_1()      
  endproc

  function OpenTables()
    dimension this.cWorkTables[max(1,9)]
    this.cWorkTables[1]='ATTIDETT'
    this.cWorkTables[2]='ATTIMAST'
    this.cWorkTables[3]='AZIENDA'
    this.cWorkTables[4]='CREDDIVA'
    this.cWorkTables[5]='CREDMIVA'
    this.cWorkTables[6]='ESERCIZI'
    this.cWorkTables[7]='IVA_PERI'
    this.cWorkTables[8]='PRO_RATA'
    this.cWorkTables[9]='ATT_ALTE'
    return(this.OpenAllTables(9))

  proc CloseCursors()
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    if used('_Curs_CREDDIVA')
      use in _Curs_CREDDIVA
    endif
    if used('_Curs_ATTIDETT')
      use in _Curs_ATTIDETT
    endif
    if used('_Curs_IVA_PERI')
      use in _Curs_IVA_PERI
    endif
    if used('_Curs_IVA_PERI')
      use in _Curs_IVA_PERI
    endif
    if used('_Curs_ATT_ALTE')
      use in _Curs_ATT_ALTE
    endif
    if used('_Curs_ATT_ALTE')
      use in _Curs_ATT_ALTE
    endif
    return
  * --- Area Manuale = Functions & Procedures
  * --- Fine Area Manuale
enddefine

procedure getEntityType(result)
  result="Routine"
endproc
procedure getEntityParmList(result)
  result=""
endproc
